<%@page import="java.util.Set"%>
<%@page import="java.util.Map"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@page import="com.liferay.portal.service.PortletLocalServiceUtil"%>
<%@page import="com.liferay.portal.model.Portlet"%>
<%@page import="java.util.List"%>
<%@page import="com.alm.rivaledge.util.ALMConstants.CHART_MODULE_DISPLAY_COLUMNS"%>

<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%> 
<%@ taglib prefix="c" 		uri="http://java.sun.com/jsp/jstl/core"%>

<portlet:defineObjects/>

<%@ include file="./common.jsp" %>

<portlet:actionURL var="submitURL">
	<portlet:param name="action" value="addChartToPage"/>
</portlet:actionURL>

<%
	String 				thisPage 		 = (String) request.getAttribute("thisPage");
	String	 			thatPage 		 = (String) request.getAttribute("thatPage");
	Set<Portlet>    	availablePortlets = (Set<Portlet>) request.getAttribute("availablePortlets");
	Map<String, List>   chartCollections = (Map<String, List>) request.getAttribute("chartCollection");
	
%>
<script type="text/javascript">
$(function() 
{
	$("#<%= thatPage %>").removeClass("active-menu");
	$("#<%= thisPage %>").addClass("active-menu");
});

var isHideAnalysisPopup = true;
function displayAddAnalysis()
{
   $("div#selectchartadd").show();
   isHideAnalysisPopup = false;
}

var isHideContentPopup = true;
function displayAddContent()
{
   $("div#Add-Content").show();
   isHideContentPopup = false;
}
	
$(document).ready(function() 
{
	$(document).click(function(e)
	{	
		$('div#selectchartadd').click(function(e)
		{
			isHideAnalysisPopup = false;
		});
		
		if(isHideAnalysisPopup)
		{
			$("div#selectchartadd").hide(); 
	 	}
	 	isHideAnalysisPopup = true;	
		
		$('div#Add-Content').click(function(e)
		{
			isHideContentPopup = false;
		});
		
		if(isHideContentPopup)
		{
			$("div#Add-Content").hide(); 
	 	}
	 	isHideContentPopup = true;
	});
	
	$('#Firms-Group-Toggle').click(function()
	{
		$("#Firms-Group-Toggle-Content").slideToggle();
	});
	
	$('#People-Group-Toggle').click(function()
	{
		$("#People-Group-Toggle-Content").slideToggle();
	});
	
	$('#Clients-Group-Toggle').click(function()
	{
		$("#Clients-Group-Toggle-Content").slideToggle();
	});
	
	$('#Events-Group-Toggle').click(function()
	{
		$("#Events-Group-Toggle-Content").slideToggle();
	});
	
	$("[id^='portId_']").click(function()
	{
		var selectedId = $(this).attr("portletId");
		$("#selectedChart").val(selectedId);
		$("div#Add-Content").hide(); 
		
		Liferay.Portlet.showBusyIcon("#bodyId","Loading..."); // show Busy Icon
		$("#chartManagerForm").submit();
	})
	
});
	

</script>

<div class="add-buttons">
<form method="post" action="${submitURL}" id="chartManagerForm">
<input type="hidden" name="selectedChart" value="" id="selectedChart">

<c:choose>
	<c:when test="<%=isHomePage%>" >
		<div style="position:relative; z-index:999;" class="flLeft mart2 buttonOne">
			<input onclick="displayAddContent();" type="button" class="Add-Content-Button" id="chartAdd" style="min-height:0px;" value="Add Content"><span class="plus-icon-White"></span></input>
		   	<div id="Add-Content" class="Add-Content" style="display: none;">
		       	<h2>Select a chart to add to the home page</h2>
		<%
				if(!chartCollections.isEmpty())
				{
					//Put News and Publications charts
					String key = CHART_MODULE_DISPLAY_COLUMNS.firms.getDisplayValue();
					List<Portlet> chartList = (List<Portlet>)chartCollections.get(key);
		%>			
					<div class="Firms-Group">
			           	<h3 id="Firms-Group-Toggle"><%=key%>&nbsp;(<%=chartList.size()%>)</h3>
			           	<ul id="Firms-Group-Toggle-Content" class="reset list3" style="display: none;">
		<%
							for (Portlet thisPortlet : chartList)
							{	
		%>  	  
			           		<li style="min-height: 0px;">
			              		<div class="Chart1"><img style="margin-top:5px;" src="<%=thisPortlet.getIcon()%>" /></div>
			              		<div class="disc">
			                			<h4 id="portId_<%=thisPortlet.getPortletId()%>" portletId="<%=thisPortlet.getPortletId()%>"><%=thisPortlet.getDisplayName()%></h4>
			              		</div>
			           		</li>
		<%
							}
		%>           		
			           	</ul>
						<div class="clear">&nbsp;</div>
					</div>
		<%		
					//Put People (Firm Stats, Attorney Search, Attorney Moves and Changes) charts
					key = CHART_MODULE_DISPLAY_COLUMNS.people.getDisplayValue();
					chartList = (List<Portlet>)chartCollections.get(key);
		%>			
					<div class="People-Group">
			           	<h3 id="People-Group-Toggle"><%=key%>&nbsp;(<%=chartList.size()%>)</h3>
			           	<ul id="People-Group-Toggle-Content" class="reset list3" style="display: none;">
		<%
							for (Portlet thisPortlet : chartList)
							{	
		%>  	  
			           		<li style="min-height: 0px;">
			              		<div class="Chart1"><img style="margin-top:5px;" src="<%=thisPortlet.getIcon()%>" /></div>
			              		<div class="disc">
			                			<h4 id="portId_<%=thisPortlet.getPortletId()%>" portletId="<%=thisPortlet.getPortletId()%>"><%=thisPortlet.getDisplayName()%></h4>
			              		</div>
			           		</li>
		<%
							}
		%>           		
			           	</ul>
						<div class="clear">&nbsp;</div>
					</div>
		<%			
					//Put Events charts
					key = CHART_MODULE_DISPLAY_COLUMNS.events.getDisplayValue();
					chartList = (List<Portlet>)chartCollections.get(key);
		%>			
					<div class="Events-Group">
			           	<h3 id="Events-Group-Toggle"><%=key%>&nbsp;(<%=chartList.size()%>)</h3>
			           	<ul id="Events-Group-Toggle-Content" class="reset list3" style="display: none;">
		<%
							for (Portlet thisPortlet : chartList)
							{	
		%>  	  
			           		<li style="min-height: 0px;">
			              		<div class="Chart1"><img style="margin-top:5px;" src="<%=thisPortlet.getIcon()%>" /></div>
			              		<div class="disc">
			                			<h4 id="portId_<%=thisPortlet.getPortletId()%>" portletId="<%=thisPortlet.getPortletId()%>"><%=thisPortlet.getDisplayName()%></h4>
			              		</div>
			           		</li>
		<%
							}
		%>           		
			           	</ul>
						<div class="clear">&nbsp;</div>
					</div>
		<%		
				}
		 %>	
			</div>
		</div>
	</c:when>
	<c:otherwise>
		
		<div style="position:relative; z-index:999;" class="flLeft mart2 buttonTwo">
        	<span class="btn icon addgry"></span><input type="button" onclick="displayAddAnalysis();" class="mybutton" value="Add Analysis"><span class="btn icon drpdwngry"></span></button>
        	<div id="selectchartadd" class="selectchartadd" style="display: none;">
        	<h2>Select a chart to add to your analysis</h2>
        	<%
				if(!chartCollections.isEmpty())
				{
        			//Put News and Publications charts
					String key = CHART_MODULE_DISPLAY_COLUMNS.specific.getDisplayValue();
					List<Portlet> chartList = (List<Portlet>)chartCollections.get(key);
			%>
					<ul class="reset list3">
			<%		
        			for (Portlet thisPortlet : chartList)
					{
			%>
						<li>
                  			<div class="imgdiv"><img src="<%=thisPortlet.getIcon()%>"></div>
                  			<div class="disc">
                    			<h4 id="portId_<%=thisPortlet.getPortletId()%>" portletId="<%=thisPortlet.getPortletId()%>"><%=thisPortlet.getDisplayName()%></h4>
                    			<p style="color:#777777;"><%=thisPortlet.getPortletInfo().getShortTitle()%></p>
                  			</div>
                		</li>
			<%			
					}
        		}
			%>
					</ul>
					<div class="clear">&nbsp;</div>
        	</div>
		</div>
			
	</c:otherwise>
</c:choose>

</form>
</div>