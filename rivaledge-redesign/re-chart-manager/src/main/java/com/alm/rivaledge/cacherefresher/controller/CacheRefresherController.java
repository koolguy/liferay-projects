package com.alm.rivaledge.cacherefresher.controller;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * Controller that provides links to refresh cache tables
 * 
 * @author FL867
 * @version 1.0
 * @since Sprint 9
 */
@Controller
@RequestMapping(value = "VIEW")
public class CacheRefresherController
{
	@RenderMapping
	public String defaultView(RenderRequest request, RenderResponse response) throws Exception
	{
		return ("cacherefresher");
	}	
}
