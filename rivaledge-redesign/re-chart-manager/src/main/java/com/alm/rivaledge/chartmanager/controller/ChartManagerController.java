package com.alm.rivaledge.chartmanager.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.alm.rivaledge.util.ALMConstants.CHART_MODULE_DISPLAY_COLUMNS;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.LayoutTypePortlet;
import com.liferay.portal.model.Portlet;
import com.liferay.portal.service.LayoutLocalServiceUtil;
import com.liferay.portal.service.PortletLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;

/**
 * Controller that determines which charts should be made available
 * on a particular page.
 * 
 * @author FL867
 * @version 1.0
 * @since Sprint 9
 */
@Controller
@RequestMapping(value = "VIEW")
public class ChartManagerController
{
	@RenderMapping
	public String defaultView(RenderRequest request, RenderResponse response) throws Exception
	{
		ThemeDisplay  themeDisplay 		= (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		List<Portlet> currentPortlets 	= themeDisplay.getLayoutTypePortlet().getPortlets();
		List<Portlet> allPortlets 		= PortletLocalServiceUtil.getPortlets();
		Set<Portlet>  availablePortlets = new TreeSet<Portlet>();
		Map<String, List> chartCollection = new HashMap<String, List>();
		
		if (themeDisplay.getLayout().getFriendlyURL().equals("/firms-analysis"))
		{
			List<Portlet> chartList = new ArrayList<Portlet>();
			String thisPage = themeDisplay.getLayout().getFriendlyURL().substring(1);
			String thatPage = thisPage.replace("analysis", "details");
			request.setAttribute("thisPage", thisPage);
			request.setAttribute("thatPage", thatPage);
			for (Portlet thisPortlet : allPortlets)
			{
				if ((thisPortlet.getPortletName().startsWith("re-firm")) && (!currentPortlets.contains(thisPortlet)))
				{
					if (thisPortlet.getPortletId().startsWith("refirmchart"))
					{
						availablePortlets.add(thisPortlet);
						chartList.add(thisPortlet);
					}
				}
			}
			chartCollection.put(CHART_MODULE_DISPLAY_COLUMNS.specific.getDisplayValue(), chartList);
		}
		else if (themeDisplay.getLayout().getFriendlyURL().equals("/events-analysis"))
		{
			List<Portlet> chartList = new ArrayList<Portlet>();
			String thisPage = themeDisplay.getLayout().getFriendlyURL().substring(1);
			String thatPage = thisPage.replace("analysis", "details");
			request.setAttribute("thisPage", thisPage);
			request.setAttribute("thatPage", thatPage);
			for (Portlet thisPortlet : allPortlets)
			{
				if ((thisPortlet.getPortletName().startsWith("re-event")) && (!currentPortlets.contains(thisPortlet)))
				{
					if (thisPortlet.getPortletId().startsWith("reeventchart"))
					{
						availablePortlets.add(thisPortlet);
						chartList.add(thisPortlet);
					}
				}
			}
			chartCollection.put(CHART_MODULE_DISPLAY_COLUMNS.specific.getDisplayValue(), chartList);
		}
		else if (themeDisplay.getLayout().getFriendlyURL().equals("/attorney-moves-analysis"))
		{
			List<Portlet> chartList = new ArrayList<Portlet>();
			String thisPage = themeDisplay.getLayout().getFriendlyURL().substring(1);
			String thatPage = thisPage.replace("analysis", "details");
			request.setAttribute("thisPage", thisPage);
			request.setAttribute("thatPage", thatPage);
			for (Portlet thisPortlet : allPortlets)
			{
				if ((thisPortlet.getPortletName().startsWith("re-attorney-moveschanges")) && (!currentPortlets.contains(thisPortlet)))
				{
					if (thisPortlet.getPortletId().startsWith("reattorneymoveschangeschart"))
					{
						availablePortlets.add(thisPortlet);
						chartList.add(thisPortlet);
					}
				}
			}
			chartCollection.put(CHART_MODULE_DISPLAY_COLUMNS.specific.getDisplayValue(), chartList);
		}
		else if (themeDisplay.getLayout().getFriendlyURL().equals("/attorney-analysis"))
		{
			List<Portlet> chartList = new ArrayList<Portlet>();
			String thisPage = themeDisplay.getLayout().getFriendlyURL().substring(1);
			String thatPage = thisPage.replace("analysis", "details");
			request.setAttribute("thisPage", thisPage);
			request.setAttribute("thatPage", thatPage);
			for (Portlet thisPortlet : allPortlets)
			{
				if ((thisPortlet.getPortletName().startsWith("re-attorney")) && (!currentPortlets.contains(thisPortlet)))
				{
					if (thisPortlet.getPortletId().startsWith("reattorneychart"))
					{
						availablePortlets.add(thisPortlet);
						chartList.add(thisPortlet);
					}
				}
			}
			chartCollection.put(CHART_MODULE_DISPLAY_COLUMNS.specific.getDisplayValue(), chartList);
		}
		else if (themeDisplay.getLayout().getFriendlyURL().equals("/firm-statistics-analysis"))
		{
			List<Portlet> chartList = new ArrayList<Portlet>();
			String thisPage = themeDisplay.getLayout().getFriendlyURL().substring(1);
			String thatPage = thisPage.replace("analysis", "details");
			request.setAttribute("thisPage", thisPage);
			request.setAttribute("thatPage", thatPage);
			for (Portlet thisPortlet : allPortlets)
			{
				if ((thisPortlet.getPortletName().startsWith("re-people-firmstats")) && (!currentPortlets.contains(thisPortlet)))
				{
					if (thisPortlet.getPortletId().startsWith("repeoplefirmstatschart"))
					{
						availablePortlets.add(thisPortlet);
						chartList.add(thisPortlet);
					}
				}
			}
			chartCollection.put(CHART_MODULE_DISPLAY_COLUMNS.specific.getDisplayValue(), chartList);
		}
		
		else if (themeDisplay.getLayout().getFriendlyURL().equals("/home")
				|| themeDisplay.getLayout().getFriendlyURL().equals("/almost") )
		{
			List<Portlet> chartListFirm = new ArrayList<Portlet>();
			List<Portlet> chartListPeople = new ArrayList<Portlet>();
			List<Portlet> chartListEvent = new ArrayList<Portlet>();
			for (Portlet thisPortlet : allPortlets)
			{
				if (!currentPortlets.contains(thisPortlet))
				{
					if (thisPortlet.getPortletId().startsWith("refirmchart") )
					{
						availablePortlets.add(thisPortlet);
						chartListFirm.add(thisPortlet);
					}
					else if ( thisPortlet.getPortletId().startsWith("reattorneymoveschangeschart") 
							/*|| thisPortlet.getPortletId().startsWith("reattorneychart")*/
							|| thisPortlet.getPortletId().startsWith("repeoplefirmstatschart") )
					{
						availablePortlets.add(thisPortlet);
						chartListPeople.add(thisPortlet);
					}
					else if (thisPortlet.getPortletId().startsWith("reeventchart"))
					{
						availablePortlets.add(thisPortlet);
						chartListEvent.add(thisPortlet);
					}
				}
			}
			chartCollection.put(CHART_MODULE_DISPLAY_COLUMNS.firms.getDisplayValue(), chartListFirm);
			chartCollection.put(CHART_MODULE_DISPLAY_COLUMNS.people.getDisplayValue(), chartListPeople);
			chartCollection.put(CHART_MODULE_DISPLAY_COLUMNS.events.getDisplayValue(), chartListEvent);
		}
		
		request.setAttribute("availablePortlets", availablePortlets);
		request.setAttribute("chartCollection", chartCollection);
		return ("chartmanager");
	}
	
	@ActionMapping
	public void addChart(ActionRequest request, ActionResponse response) throws Exception
	{
		//String[] selectedCharts = request.getParameterValues("chartSelection");
		String selectedChart = request.getParameter("selectedChart");
		
		if (selectedChart != null)
		{
			// Get the id of the selected portlet
			String selectedPortletId = selectedChart;
			System.out.println("ChartManagerController::addChart::selectedPortletId:::"+selectedPortletId);
			
			// Plug it into the layout
			ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
			LayoutTypePortlet thisLayout   = themeDisplay.getLayoutTypePortlet();
			
			String friendlyURL = themeDisplay.getLayout().getFriendlyURL();
			
			//On attorney search analysis page the layout is 1 column
			if( friendlyURL.equalsIgnoreCase("/attorney-analysis") 
					/*|| friendlyURL.equalsIgnoreCase("/home")*/ )
			{
				thisLayout.addPortletId(new Long(request.getRemoteUser()), selectedPortletId);
			}
			else //All other pages has 1-2-1 column layout
			{
				List<Portlet> currentPortlets = themeDisplay.getLayoutTypePortlet().getPortlets();
				int currentPortletSize = (currentPortlets .size()-1); //remove chart portlet from list
				if(friendlyURL.equalsIgnoreCase("/home"))
				{
					//currentPortletSize = currentPortletSize - 5; //5 search portlets are on home page
				}
				System.out.println("ChartManagerController::addChart::currentPortlets :::"+currentPortletSize);
				
				//Template's default layout is 1-2-1. On 1st column there is chart portlet
				if( (currentPortletSize % 2) == 0 )
				{
					System.out.println("ChartManagerController::addChart::adding portlet on 2nd column:::");
					thisLayout.addPortletId(new Long(request.getRemoteUser()), selectedPortletId, "column-2", -1);
				}
				else
				{
					System.out.println("ChartManagerController::addChart::adding portlet on 3rd column:::");
					thisLayout.addPortletId(new Long(request.getRemoteUser()), selectedPortletId, "column-3", -1);
				}
			}
			
			// Update the layout
			LayoutLocalServiceUtil.updateLayout(themeDisplay.getLayout().getGroupId(), themeDisplay.getLayout().isPrivateLayout(), themeDisplay.getLayout().getLayoutId(), themeDisplay.getLayout().getTypeSettings());
			System.out.println("ChartManagerController::addChart::after updating layout:::END:::");
		}
	}
}
