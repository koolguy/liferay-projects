<%@page import="com.alm.rivaledge.transferobject.EventResultDTO"%>
<%@page import="com.alm.rivaledge.model.EventSearchModelBean"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%> 
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="java.util.Map"%>
<portlet:defineObjects/>

<%						
	PortletURL printPublicationsURL = renderResponse.createRenderURL();
	printPublicationsURL.setWindowState(LiferayWindowState.POP_UP);
	printPublicationsURL.setParameter("displayPrintPage", "true");						
	Map<String, Integer> firmCounters = null;
	Map<String, Integer> locCounters = null;
	Map<String, Integer> practiceAreaCounters = null;
	if( request.getAttribute("firmsGroupCounters") != null) 
	{
		firmCounters = (Map<String, Integer>) request.getAttribute("firmsGroupCounters");
	}
	
	if( request.getAttribute("locsGroupCounters") != null) 
	{
		locCounters = (Map<String, Integer>) request.getAttribute("locsGroupCounters");
	}
	
	if( request.getAttribute("practiceAreasGroupCounters") != null) 
	{
		practiceAreaCounters = (Map<String, Integer>) request.getAttribute("practiceAreasGroupCounters");
	}

%>

<script type="text/javascript">
function setFileType(fileType)
{
	document.getElementById('fileType').value = fileType;
	var r=confirm("Click OK to export or Cancel");
	if (r == true)
  	{
  		document.getElementById('exportForm').submit();
  	}
	else
	{
	  return false;
	}
}

function openPopUp()
{
popupWindow = window.open(
				'<%= printPublicationsURL.toString() %>',
				'popUpWindow','height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes');
}

$(document).ready(function(){
	
	$("#events-analysis").removeClass("active-menu");
	$("#events-details").addClass("active-menu");
	
});

</script>

 <div class="detailsPage">
	<h4 class="heading">Detailed Results</h4>
	
	<div>
	<div class="flLeft resultsec"><strong>${eventSearchModelBean.currentPageSize}&nbsp;of&nbsp;${eventSearchModelBean.totalResultCount} Results</strong> 
  		 <span>(
          		<c:choose>
	      				 <c:when test="${fn:length(eventSearchModelBean.selectedFirms) gt 100}">
	      				 	${fn:substring(eventSearchModelBean.selectedFirms, 0, 99)}..,&nbsp;
	      				 </c:when>
	      				 <c:otherwise>
	      				 	${eventSearchModelBean.selectedFirms},&nbsp;
	      				 </c:otherwise>		        		  
				</c:choose>	
				<c:choose>
	      				 <c:when test="${fn:length(eventSearchModelBean.selectedPracticeArea) gt 100}">
	      				 	${fn:substring(eventSearchModelBean.selectedPracticeArea, 0, 99)}..,&nbsp;
	      				 </c:when>
	      				 <c:otherwise>
	      				 	${eventSearchModelBean.selectedPracticeArea},&nbsp;
	      				 </c:otherwise>		        		  
				</c:choose> 			
	      		 ${eventSearchModelBean.selectedLocation},&nbsp;	      				
				 ${eventSearchModelBean.dateText},&nbsp;				
				<c:choose>
	      				 <c:when test="${fn:length(eventSearchModelBean.keywords) gt 100}">
	      				 	,&nbsp;${fn:substring(eventSearchModelBean.keywords, 0, 99)}..
	      				 </c:when>
	      				  <c:otherwise>
	      				 	<c:if test="${not empty eventSearchModelBean.keywords}">
	      				 		,&nbsp;${eventSearchModelBean.keywords} 
	      				 	</c:if>
	      				 </c:otherwise>		        		  
				</c:choose>    				 
	      				    		
          )</span>
  		
  		</div>
    <div class="flRight">   	  
      		<ul class="menuRight flRight">
        		<li><a class="btn icon alert" href="#">Create Alert</a></li>
         		<li><a class="btn icon print" href="#" onClick="openPopUp();" >Print</a></li>
        		<li><a id="exports" class="btn icon export" href="#">Export</a>
	          		<div id="actionBox" class="actionSec" style="">
	            		<h5>Actions</h5>
	            		<ul class="reset">
	              			<li><span>E</span><a href="#"  onclick="setFileType('xls')">Export to Excel</a></li>
	              			<li><span>E</span><a href="#"  onclick="setFileType('csv')">Export to CSV</a></li>
	            		</ul>
	            		<div class="clear">&nbsp;</div>
	          		</div>
        		</li>
    		   <li>
	             <%--
	              <div class="viewSetting flRight" style="margin-top:0px">
	             	<span>
	             		<span class="btn icon dropdownarrow" id="viewSettings" onclick="toggleViewSetting(this)">View Settings</span>
	             	</span>
		          </div>
		            --%>
		          
		          <div class="viewSetting flRight rel" style="margin-top:0px">
					  		<a class="btn icon settings" href="javascript:void(0)" onclick="toggleViewSettings(this);">
					  			<span>
					  				<span class="btn icon dropdownarrow">View Settings</span>
					  			</span>
					  		</a>
	      			 </div>
	          </li>
          </ul>
         
        </div>
        </div>
        
		<div class="clear">&nbsp;</div>
		
		<c:if test="${not isAdminUser}">
			<c:if test="${displayMessage}">
		  		<div style="color:#b63334;">
		  			Search Results are too large to display in their entirety. Only the first 5,000 results are displayed below. To view an entire search result, please narrow your search criteria above and rerun your search.
		  		</div>
		  	</c:if>
	  	</c:if>

	<table width="100%" border="0" class="tblOne" cellspacing="0"	cellpadding="0">
		<colgroup>
			<!--<col width="25"> --> <!-- Sachin: code commented to fix bug RER-305  -->
			<!-- adjusting the width to other columns -->
			<col width="100"><!--changed the width from 90 to 100 (adjusting the width of the commented column  --> 
			<col width="105"><!--changed the width from 90 to 105 (adjusting the width of the commented column  -->
			<col width="500">
			<col width="200">
			<col width="100">
		</colgroup>
		<thead>
			<tr>				
				<th align="center"><a href="javascript:void(0);" onclick="sortResults(3)">Date</a></th>				
				<th align="center"><a href="javascript:void(0);" onclick="sortResults(9)">Firm</a></th>				
				<th align="center"><a href="javascript:void(0);" onclick="sortResults(5)">Event</a></th>
				<th align="center"><a href="javascript:void(0);" onclick="sortResults(8)">Practices</a></th>
				<th align="center"><a href="javascript:void(0);" onclick="sortResults(7)">Location</a></th>			
			</tr>
		</thead>
	 </table>
          
          <table class="tbleTwo" width="100%" border="0" cellspacing="0" cellpadding="0">
          
          <colgroup>            	
				<!-- adjusting the width to other columns -->
				<col width="100"><!--changed the width from 90 to 100 (adjusting the width of the commented column  --> 
				<col width="105"><!--changed the width from 90 to 105 (adjusting the width of the commented column  -->
				<col width="500">
				<col width="200">
				<col width="100">
				
                
          </colgroup>
           <tbody>
		<c:choose>
			<c:when test="${fn:length(eventSearchResults) gt 0}">
				<c:forEach items="${eventSearchResults}" var="results" varStatus="i">
					<c:set var="firmName" value="${results.resultFirm.company}"/>
					<c:set var="locName" value="${results.location}"/>
					<c:set var="practiceName" value="${results.practiceArea}"/>
				 <%-- orderBy values 	2 = companyId
				 						7 = location
					      	 			8 = practiceArea
					      	 				
				 --%> 
				 <%--The below choose cond'n  is responsible for grouping the results in Table in UI--%>
				<c:choose>
						<%-- For first row in Table --%>
   						 <c:when test="${(i.first) && (eventSearchModelBean.orderBy == 9)}">
   						 	<%
   						 		String firmName = (String) pageContext.getAttribute("firmName");
   						 		Integer firmCounter = null;
   						 		if(firmCounters != null)
   						 		{
   						 		 	firmCounter = firmCounters.get(firmName);
   						 		}
   						 	 %>
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.resultFirm.company} (<%= firmCounter %>)</strong></div></td>      		 		
	      		 				</tr>
	      		 				
   						 </c:when>
   						  <c:when test="${(i.first) && (eventSearchModelBean.orderBy == 7)}">
   						  	 <%
   						 		String locName = (String) pageContext.getAttribute("locName");
   						 		Integer locCounter = null;
   						 		if(locCounters != null)
   						 		{
   						 			locCounter = locCounters.get(locName);
   						 		}
   						 	 %>
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.location} (<%= locCounter %>)</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(i.first) && (eventSearchModelBean.orderBy == 8)}">
   						   	<%
   						 		String practiceName = (String) pageContext.getAttribute("practiceName");
   						 		Integer practiceCounter = null;
   						 		if(practiceAreaCounters != null)
   						 		{
   						 		 	practiceCounter = practiceAreaCounters.get(practiceName);
   						 		}
   						 	 %>
  
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.practiceArea} (<%= practiceCounter %>)</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 <%-- For subsequent rows in Table only when there is change in the CompanName or practiceArea or location in 2 consecutive records --%>
	   						 <c:when test="${(i.index > 0) && (eventSearchModelBean.orderBy == 9) && (eventSearchResults[i.index - 1].resultFirm.company != eventSearchResults[i.index].resultFirm.company)}">
	   						 
	   						 	<%
   						 		String firmName = (String) pageContext.getAttribute("firmName");
   						 		Integer firmCounter = null;
   						 		if(firmCounters != null)
   						 		{
   						 		 	firmCounter = firmCounters.get(firmName);
   						 		}
   						 	 	%>
	     						    <tr class="oddone">
		      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.resultFirm.company} (<%= firmCounter %>)</strong></div></td>      		 		
		      		 				</tr>
	   						 </c:when>
   						  <c:when test="${(i.index > 0) && (eventSearchModelBean.orderBy == 8) && (eventSearchResults[i.index - 1].practiceArea != eventSearchResults[i.index].practiceArea)}">
   						     <%
   						 		String practiceName = (String) pageContext.getAttribute("practiceName");
   						 		Integer practiceCounter = null;
   						 		if(practiceAreaCounters != null)
   						 		{
   						 		 	practiceCounter = practiceAreaCounters.get(practiceName);
   						 		}
   						 	 %>
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.practiceArea} (<%= practiceCounter %>)</strong></div></td>   
	      		 				</tr>
   						 </c:when>
   						 <c:when test="${(i.index > 0) && (eventSearchModelBean.orderBy == 7) && (eventSearchResults[i.index - 1].location != eventSearchResults[i.index].location)}">
   						  	 <%
   						 		String locName = (String) pageContext.getAttribute("locName");
   						 		Integer locCounter = null;
   						 		if(locCounters != null)
   						 		{
   						 			locCounter = locCounters.get(locName);
   						 		}
   						 	 %>
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1">
	      		 					<strong>
	      		 					<c:choose>
										<c:when test="${empty results.location or results.location == ''}">
				      				 	 	Location Not Identified (<%= locCounter %>)
				      					</c:when>
				      					<c:when test="${fn:contains(results.location, '-')}"> 
				      				 	 	${fn:substring(results.location, 3, fn:length(results.location))} (<%= locCounter %>)
				      					</c:when>
										<c:otherwise>
				      				 	 	${results.location} (<%= locCounter %>)
				      					</c:otherwise>
									</c:choose>
	      		 					</strong>
	      		 					</div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 
				</c:choose>
				
					<!-- <tr class="odd"> -->
					 <tr class="${i.index % 2 == 0 ? 'odd' : 'even'} ">
						<!--  <td align="center"><input type="checkbox" value="${results.eventId}"/></td> --> <!-- Sachin: Code changed to fix the bug RER 305  -->
						<%-- <td align="center">${results.dataType}</td> --%>
						<td><fmt:formatDate pattern="MM/dd/yyyy"
								value="${results.eventDate}" /></td>
						<td><a href="${results.resultFirm.homeUrl}" target="_blank">${results.resultFirm.company}</a></td>
						
						<td><font color="red"><a href="${results.eventURL}" target="_blank">${results.eventTitle}</a></font>
							<br> <c:choose>
								<c:when test="${fn:length(results.eventDescription) gt 254}">
				      				 	${fn:substring(results.eventDescription, 0, 251)}...
				      				 </c:when>
								<c:otherwise>
				      				 	${results.eventDescription} 
				      				 </c:otherwise>
							</c:choose></td>
						<td>${results.practiceArea}</td>
						
						<td>
						
							<c:choose>
								<c:when test="${empty results.location or results.location == ''}">
				      				 	 Location Not Identified 
				      			</c:when>
				      			<c:when test="${fn:contains(results.location, '-')}"> 
				      				 	 ${fn:substring(results.location, 3, fn:length(results.location))} 
				      			</c:when>
								<c:otherwise>
				      				 	 ${results.location} 
				      			</c:otherwise>
							</c:choose>
						</td>
						
					 </tr> 
				</c:forEach>
				<!-- Missing CODE -->
				
				
   			
			</c:when>
			<c:otherwise>
				<tr class="odd">
					<td colspan="7" align="center">0 Results, Please try a	different search</td>
				</tr>
			</c:otherwise>

		</c:choose>
	
	</tbody>
	</table>

	<div class="PaginationScroll">
		      	  	
		      	  <ul>
					<%
					EventSearchModelBean esm = 	((EventSearchModelBean) request.getAttribute("eventSearchModelBean"));
							System.out.println("esm : " + esm);
					int pageNumber 	= esm.getGoToPage();
					int lastPage	= esm.getLastPage();
						
						System.out.println("\n\n*** pageNumber = " + pageNumber + "; lastPage = " + lastPage);
						if (lastPage != 1)
						{
						%>
								Page:&nbsp;&nbsp;&nbsp;
						<%
								if (pageNumber > 5)
								{
						%>
									<li class="active"><a href="#" onclick="setPage(<%= 1 %>)"><%= 1 %></a>&nbsp;&nbsp;&nbsp;...&nbsp;&nbsp;&nbsp;</li>
						<%
								}
								for (int i = Math.max(1, pageNumber - 5); i <= Math.min(pageNumber + 5, lastPage); i++)
								{
									if (pageNumber == i)
									{
						%>
									<li><strong><%= i %>&nbsp;&nbsp;&nbsp;</strong> </li>
						<%
									}
									else
									{
						%>
										<li><a href="#" onclick="setPage(<%= i %>)"><%= i %></a>&nbsp;&nbsp;&nbsp;</li>
						<%				
									}
								}
								
								if ((lastPage - pageNumber) > 5)
								{
						%>
									...&nbsp;&nbsp;&nbsp;<li><a href="#" onclick="setPage(<%= lastPage %>)"><%= lastPage %></a></li>
						<%			
								}
						}
						else
						{
						%>
								Page:&nbsp;&nbsp;&nbsp;1
						<%				
						}
						%>
		      	  	
		     	</ul>	
		   		 
   		</div>
   		<!--  
   		<div>
   			<ul>
				<%
					EventSearchModelBean esmNew = 	((EventSearchModelBean) request.getAttribute("eventSearchModelBean"));
							System.out.println("esm : " + esm);
					int pageNumberNew 	= esm.getGoToPage();
					int lastPageNew	= esm.getLastPage();
					int resultsPerPage = esmNew.getSearchResultsPerPage();
					int totalResulsVal = esmNew.getTotalResultCount();
						System.out.println("\n\n*** pageNumberNew = " + pageNumberNew + "; lastPageNew = " + lastPageNew);
						if (lastPageNew == 1)
						{
							
						%>
							1
						<%	
						}
						else
						{
							if(pageNumberNew == 1)
							{
							%>
								1&nbsp;-&nbsp;<%=(pageNumberNew * resultsPerPage)%><li class="active"><a href="#" onclick="setPage(<%= (pageNumberNew + 1) %>)">></a></li>	
							<%
							}else if(pageNumberNew == lastPageNew){
							%>
								<li class="active"><a href="#" onclick="setPage(<%= (pageNumberNew - 1) %>)"><</a></li><%= ((pageNumberNew - 1) * resultsPerPage) %>&nbsp;-&nbsp;<%= totalResulsVal %>	
							<%	
							}else{
							%>
								<li class="active"><a href="#" onclick="setPage(<%= (pageNumberNew - 1) %>)"><</a></li>
								&nbsp;&nbsp;<%= ((pageNumberNew - 1) * resultsPerPage) %>&nbsp;-&nbsp;<%= (pageNumberNew * resultsPerPage) %>
								<li class="active"><a href="#" onclick="setPage(<%= (pageNumberNew + 1) %>)">></a></li>	
							<%	
							}
						}
						%>
		      	  	
		     	</ul>	
   		</div>
   		-->
 </div>

<form name="exportForm" method="post" id="exportForm" action="<portlet:resourceURL id="exportFile"/>">
	<input type="hidden" name="fileType" value="" id="fileType"/>
	<input type="hidden" name="groupByColumn" value="${eventSearchModelBean.orderBy}" id="groupByColumn"/>
  </form>
   			