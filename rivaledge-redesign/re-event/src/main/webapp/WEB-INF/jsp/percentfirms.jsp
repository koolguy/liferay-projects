<%@page import="com.alm.rivaledge.model.chart.BaseChartModelBean"%>
<%@page import="java.util.Map"%>
<%@ taglib prefix="portlet" 		uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" 			 	uri="http://java.sun.com/jsp/jstl/core" %>

<portlet:defineObjects/>

<%@ include file="./common.jsp" %>

<portlet:actionURL var="updatePercentEventFirm">
	<portlet:param name="action" value="updatePercentEventFirm"/>
</portlet:actionURL>

<portlet:actionURL var="removePortletURL">
	<portlet:param name="action" value="removePortleFromThePage"/>
</portlet:actionURL>

<script type="text/javascript">

function removeThisChartPercentFirms(){
	
	var removePortletAction = '<%= removePortletURL.toString()%>';
	var r=confirm("Are you sure you want to delete this chart?");
	if (r == true)
  	{
		Liferay.Portlet.showBusyIcon("#bodyId", "Loading...");
		$("#removeThisChartPercentFirms").attr('action', removePortletAction);
		$("#removeThisChartPercentFirms").submit();
  	}
	else
	{
	  return false;
	}
}

(function () {

<%@ include file="./theme.jsp" %>
	var percentFirmsChartType 				= '${percentFirmsChartType}'; 			 	//This will be type of chart.
	var percentFirmsChartTypeTitle 			= '${percentFirmsChartTypeTitle}'; 			 	// This will be title of chart.
	var percentFirmsNameList 				= [${percentFirmsNameList}]; 		 	// This will be list of firms name.
	var percentCountByFirmList 				= [${percentCountByFirmList}]; 	// This will be news count of individual firms.
	
	var amtByFirmLocationsList	 			= [${amtByFirmLocationsLists}];
	var amtByFirmPracticesList				= [${amtByFirmPracticesLists}];
	
	
$(function()
{

	$(".AnalyzeResultHeader").show();
	
	// When user clicks on analysis tab active-menu css class is removed from details tab.
	// And same class added to analysis tab.
	$("#events-details").removeClass("active-menu");
	$("#events-analysis").addClass("active-menu");
	
	$("#resetChartEventPercentFirm").click(function(){
		$("input[value=pieChart]").prop("checked", true);
	});
		
	$(".closewhite").click(function(){
			$("#amountPercentFirmViewSettings").hide();
		});
	
	$("#rightChartSettingApply").click(function(){
		$("#amountPercentFirmViewSettings").hide();
		if($('input[name=graphType2]:radio:checked').val() == 'verticalBar')
		{
			displayBarGraph('column');
		} else if($('input[name=graphType2]:radio:checked').val() == 'HorizontalBar')
		{
			displayBarGraph('bar');
		}else if($('input[name=graphType2]:radio:checked').val() == 'pieChart')
		{
			displayPieChart();
		}
	});
	
	$("#percentFirmsCancel").click(function(){
		$("#amountPercentFirmViewSettings").hide();
	});
	
	$("div#noEventsByFirmOrgPopupDiv").mouseover(function(){
		$("#noEventsByFirmOrgPopupDiv").show();
	}).mouseout(function(){
		$("#noEventsByFirmOrgPopupDiv").hide();
	});
	
	$('#percentFirmPrintCharts').click(function() {
        var chart = $('#percentageFirmsChartContainer').highcharts();
        chart.print();
    });
	$('#percentFirmExportJPG').click(function() {
        var chart = $('#percentageFirmsChartContainer').highcharts();
        chart.exportChart({type: 'image/jpeg'});
    });
	$('#percentFirmExportPNG').click(function() {
        var chart = $('#percentageFirmsChartContainer').highcharts();
        chart.exportChart({type: 'image/png'});
    });
	
	displayPieChart();
	
	if(<%=!isHomePage%>)
	{
		if(percentFirmsNameList==null || percentFirmsNameList == '')
		{
			//$("#percentfirmsDiv").removeClass("hideClass");	
			//$("#percentfirmsDiv").parent().find(".charts").hide(); // hide the settings icon
			
			$("#p_p_id<portlet:namespace/>").hide();
			$(".cph").show();
			return;
		}
	}
	else
	{
		if(percentFirmsNameList==null || percentFirmsNameList == '')
		{
			$("#percentfirmsDiv").removeClass("hideClass");
			$("#percentageFirmsChartContainer").addClass("hideClass");
			
			$("#No-Data-percentFirm").addClass("No-Data-Charts");
			$("#percent_firm_settings").hide();
		}
		else
		{
			$("#No-Data-percentFirm").removeClass("No-Data-Charts");
			$("#percentfirmsDiv").addClass("hideClass");
		}
	}
	
	$(document).click(function() {
		$("div#noEventsByFirmOrgPopupDiv").hide();
	});
	
	Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function(color) {
	    return {
	        radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
	        stops: [
	            [0, color],
	            [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
	        ]
	    };
	});
	
});

function displayPieChart()
{
	// Set limit of number of data to display on chart.
	/*  var limit = 0;
	if(percentFirmsNameList.length > 15)
	{
		limit = 15;
	}
	else
	{
		limit = percentFirmsNameList.length;
	} */
	var percentFirmsCountList = [];
	
	for(var index = 0; index < percentFirmsNameList.length; index++)
	{
		percentFirmsCountList.push(percentFirmsNameList[index]);
	}

	var percentFirmsPieChartSeries = []; // This will be series of data.
	 
	for(var index=0; index < percentFirmsNameList.length; index++)
	{
		// This will be pie chart data.
	 	// First attribute firm name and second will be total count.
		var percentFirmsPieChartData = [];
		percentFirmsPieChartData.push(percentFirmsNameList[index]);
		percentFirmsPieChartData.push(percentCountByFirmList[index]);
		percentFirmsPieChartSeries.push(percentFirmsPieChartData);
	} 
	
	var showLegend = false;
 	if(percentFirmsNameList.length <= 15)
 	{
  		showLegend = true;
 	}
	// Generate graph.
	$('#percentageFirmsChartContainer').highcharts(
	{
        chart:
		{
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title:
		{
            text: percentFirmsChartTypeTitle
        },
        tooltip:
		{
    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
			formatter: function(e){
         		showEventsByFirmOrgPopupData(this.key);
				$("#noEventsByFirmOrgPopupDiv").show();
				return false;
         	}
        },
        plotOptions:
		{
            pie:
			{
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                  distance: -30,
                     enabled: true,
                     color: '#ffffff',
                     formatter: function(){
                         return parseFloat(this.percentage).toFixed(0)+"%";
                     }
                 },
				showInLegend: true,
				point: {
                     events: {
                         mouseOver: function(e) { 
                            $("#popupPieChart").show();
							$("#popupPieChartDiv").show();
							$(document).click(function() {
								$("div#popupPieChartDiv").hide();
							});
                         },
						legendItemClick: function () {
							return false; // <== returning false will cancel the default action
						}
                     }
                     
                 }
			}
        },
		legend: {
            enabled: showLegend,
			borderColor: '#fff',
			itemStyle: {
                paddingBottom: '7px'
            },
            layout: 'vertical',
            align: 'right',
            width: 200,
            verticalAlign: 'middle',
            useHTML: true,
            labelFormatter: function() {
                return '<div style="text-align: left; width:130px;">' + this.name + '</div>';
            }
         },
        series: [
		{
            type: 'pie',
            name: percentFirmsChartTypeTitle,
            data: percentFirmsPieChartSeries
        }],
		navigation: {
            buttonOptions: {
                enabled: false
            }
        }
    });
}


function displayBarGraph(type)
{
	var labelXPosition = 0;
	var labelYPosition = 0;
	var labelRotation = 0;
	if(type == 'bar')
	{
		labelXPosition = 0;
		labelYPosition = 0;
		labelRotation = 0;
	}
	else
	{
		labelXPosition = -15;
		labelYPosition = 30;
		labelRotation = -45;
	}
	$('#percentageFirmsChartContainer').highcharts({
        chart: {
            type: type
        },
        title:
		{
            text: percentFirmsChartTypeTitle
        },
        xAxis: {
            categories: percentFirmsNameList,
            labels: 
            {
                rotation: labelRotation,
                y: labelYPosition,
				x: labelXPosition,
				formatter: function()
				{
					var firmName = this.value;
					if(firmName.length > 10)
					{
						firmName = firmName.substring(0, 10) + "...";
					}
					return firmName;
				}
			}
        },
        yAxis: {
            min: 0,
            gridLineWidth: 1,
			gridLineColor: '#cecece',
			minorGridLineColor: '#cecece',
			lineWidth: 1,
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
        	enabled: true
    	},
        tooltip: {
        	formatter: function() {
				showEventsByFirmOrgPopupData(this.key);
				$("#noEventsByFirmOrgPopupDiv").show();
				return false;
			}
        },
        plotOptions: {
        	column: {
                
                dataLabels: {
                    enabled: false,
                    color: '#000000',
                    formatter: function(){
                    	if(this.y > 0){
                    		return this.y;
                    	}
                    }
                },
                point: {
                    events: {
                        mouseOver: function(e) {}
                    }
                    
                }
            }
        },
        series: [
        {
            name: 'Firms',
            color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#506a85'],
					[1, '#15375c']
				]
			},
            data: percentCountByFirmList
        }],
		navigation: {
            buttonOptions: {
                enabled: false
            }
        }
    });
}

function showEventsByFirmOrgPopupData(firmName)
{
	$(".settingsgry, .highcharts-button").mouseover(function(){
		$("#noEventsByFirmOrgPopupDiv").hide();
	});
	
	var pointerPosition = 0;
	for(var index = 0; index < percentFirmsNameList.length; index++)
	{
		if(firmName == percentFirmsNameList[index])
		{
			pointerPosition = index;
		}
	}
	$("#percentageFirmsChartContainer").mousemove(function(event){
		
		var parentOffset = $(this).parent().offset();
        var relativeXPosition = (event.pageX);
        var relativeYPosition = (event.pageY);
		
		$("#noEventsByFirmOrgPopupDiv").css("top", relativeYPosition-255).css("left", relativeXPosition-100)
	});
	
	var url = "events-details?drilldownFirmName=" + encodeURIComponent(firmName) + searchCriteria;
	
	var tooltipOption = '';
	
	tooltipOption += '<div class="Tooltip-Heading">'+firmName+'</div><div class="Tooltip-Recent">'+ percentCountByFirmList[pointerPosition]+' Total Events</div><div class="Tooltip-Recent">Top Practice Area</div>';
	for(var index=0; index< amtByFirmPracticesList[pointerPosition].length; index++)
	{
		tooltipOption += '<div class="Tooltip-Rows"  title="'+ amtByFirmPracticesList[pointerPosition][index] +'">' + (amtByFirmPracticesList[pointerPosition][index].length > 30 ? amtByFirmPracticesList[pointerPosition][index].substring(0, 30) + '...' : amtByFirmPracticesList[pointerPosition][index]) +'</div>';
	}
	
	tooltipOption += '<div class="Tooltip-Recent">Top Locations</div>';
	for(var index=0; index< amtByFirmLocationsList[pointerPosition].length; index++)
	{
		tooltipOption += '<div class="Tooltip-Rows"  title="'+ amtByFirmLocationsList[pointerPosition][index] +'">' + (amtByFirmLocationsList[pointerPosition][index].length > 30 ? amtByFirmLocationsList[pointerPosition][index].substring(0, 30) + '...' : amtByFirmLocationsList[pointerPosition][index]) +'</div>';
	}
	
	
	tooltipOption += '<div class="clickToViewDetails"><a href="'+url+'" class="Tooltip-ClickToViewDetails">Click to View Details</a></div>';
	
	$("#noEventsByFirmOrgPopupData").html(tooltipOption);
	
	$("#percentageFirmsChartContainer").mouseout(function(e){
		$("#noEventsByFirmOrgPopupDiv").hide();
	});
}

$(document).ready(function()
{	
    $("#percent_firm").click(function()
    {
    	Liferay.Portlet.showPopup(
        		{
        			uri : '${chartSearchPortletURL}', // defined in common.jsp
        			title: "Search Criteria"
        		});
		
	});
	
	
});
}());

</script>


 
<div class="newspublicationPage marbtm4">
		<div class="colMin flLeft leftDynamicDiv">
			<div class="topHeader ForChartsTopHeader">
				<span title="Remove this chart" onclick="removeThisChartPercentFirms();" style="float: right; font-weight: bold; color: rgb(255, 255, 255); cursor: pointer; font-family: verdana; margin: 2px 5px; padding: 3px 8px;">X</span>
			</div>
			<div id="noEventsByFirmOrgPopupDiv" style="z-index: 9999; width: 200px; position: absolute; background:#ffffff; padding: 10px 10px 10px 10px; border: 1px solid #1A1A1A; display: none; box-shadow:1px 4px 8px #444;">
				<div id="noEventsByFirmOrgPopupData" class="Popup-Tooltip"></div>
			</div>
			<div id="percentfirmsDiv" class="hideClass">0 Results, Please try a different search</div>
				<div class="flRight charts" id="No-Data-percentFirm">
					<ul class="reset listView">
						<c:if test="<%=isHomePage%>" >
							<li id="percent_firm" style="overflow:hidden;"><a href="javascript:void(0);" class="filter-icon" >&nbsp;</a></li>
						</c:if>	
						<li id="percent_firm_settings"><a href="#amountPercentFirmViewSettings"
							class="btn icon settingsgry rightViewSetting login-window chartViewSetting"
							onclick="return false;">&nbsp;</a></li>
						<li>
							<a href="javascript:void(0);" id="percentFirmPrintCharts" onclick="return false;" class="printChartClass"></a>
						</li>
						<li>
							<a href="javascript:void(0);" onclick="return false;" class="exportChartClass"></a>
                        <div class="actionSec">
                        <h5>Actions</h5>
                        <ul class="reset">
                            <li class="exportChartImage"><span id="percentFirmExportJPG">Export as JPG</span></li>
                            <li class="exportChartImage"><span id="percentFirmExportPNG">Export as PNG</span></li>
                        </ul>
                        <div class="clear">&nbsp;</div>
                        </div>
						</li>
					</ul>
						
					<div id="amountPercentFirmViewSettings" class="viewBox popusdiv ClickPopup" style="display:none; width:400px !important;">
                         <div class="popHeader"> <a href="javascript:void(0);" class="btn icon closewhite closeOne flRight" style="margin-top: 2px">Close</a> SETTINGS: % OF EVENTS BY FIRM/ORG
                              <div class="clear">&nbsp;</div>
                         </div>
                       	<div class="popMiddle">
							<h6>Chart Type</h6>
							<form action="#">
								<ul class="reset list2">
									<li style="width: 125px">
										<input type="radio" name="graphType2" value="pieChart" class="graphType" checked="checked" id="rightGraphDefaultChecked">
										<span class="btn icon piechart">Pie</span>
									</li>
									<!-- As per client requirement this options are hide. -->
									<li style="display: none;">
										<input type="radio" name="graphType2" value="verticalBar" class="graphType">
										<span class="btn icon barchartvert">Vertical Bar</span>
									</li>
									<li style="display: none;">
										<input type="radio" name="graphType2" value="HorizontalBar" class="graphType">
										<span class="btn icon barcharthori">Horizontal Bar</span>
									</li>
								</ul>
							</form>
						</div>
                        <div class="popFooter">
							<hr>
							<div>
								<input type="button" value="Reset All" class="buttonTwo flLeft rightReset" id="resetChartEventPercentFirm">
								<input type="button" value="Cancel" class="buttonTwo flRight rightReset" id="percentFirmsCancel">
								<input type="button" value="Apply" class="buttonOne flRight" id="rightChartSettingApply" style="margin: 0 5px 0 0;">
								<div class="clear">&nbsp;</div>
							</div>
						</div>
                   </div>
               </div>
			   
			<div id="percentageFirmsChartContainer" class="charts-spacing">
		</div>
	</div>
</div>	
<form id="removeThisChartPercentFirms" method="post"></form>