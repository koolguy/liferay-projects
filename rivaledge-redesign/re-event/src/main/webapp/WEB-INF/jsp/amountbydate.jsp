<%@page import="com.alm.rivaledge.util.WebUtil"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="java.util.Map"%>
<%@ taglib prefix="portlet" 		uri="http://java.sun.com/portlet_2_0"%> 
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<portlet:defineObjects/>

<%@ include file="./common.jsp" %>

<portlet:actionURL var="removePortletURL">
	<portlet:param name="action" value="removePortleFromThePage"/>
</portlet:actionURL>


<script type="text/javascript">


function removeThisChartAmountsByDate(){
	
	var removePortletAction = '<%= removePortletURL.toString()%>';
	var r=confirm("Are you sure you want to delete this chart?");
	if (r == true)
  	{
		Liferay.Portlet.showBusyIcon("#bodyId", "Loading...");
		$("#removeThisChartAmountsByDate").attr('action', removePortletAction);
  		$("#removeThisChartAmountsByDate").submit();
  	}
	else
	{
	  return false;
	}
}
(function () {

var dateArr = new Array(); 
<%@ include file="./theme.jsp" %>

	var amtByDateChartType 		= '${chartType}'; 		//This will be type of chart.
	var amtByDateChartTitle 	= '${chartTitle}'; 		// This will be title of chart.
	var dateList 				= [${dateList}]; 			// This will be list of firms name.
	var dateCounts 				= [${datesCount}]; 		// This will be Practice Areas count of individual firms.
	var eventsTitles 			= [${eventsTitles}]; 		// This will be titles of the events.
	
	<c:choose>
		<c:when test="${(not empty comparisonDataCount) and (comparisonDataCount > 0)}">
			var splitLineCountVariable = ${comparisonDataCount};
		</c:when> 	
		<c:otherwise>
			var splitLineCountVariable = 0;
		</c:otherwise>
	</c:choose>
	<c:choose>
		<c:when test="${(not empty locSetChart)}">
			var locSetChart = ${locSetChart};
		</c:when> 	
		<c:otherwise>
			var locSetChart = [];
		</c:otherwise>
	</c:choose>
	
	<c:choose>
	<c:when test="${(not empty practiceAreaSetChart) and (fn:length(practiceAreaSetChart) > 0)}">
			var practiceAreaSetChart = ${practiceAreaSetChart};
		</c:when> 	
		<c:otherwise>
			var practiceAreaSetChart = 0;
		</c:otherwise>
	</c:choose>
		
	
	var pointWidth	= 15;
	
	for(i=0;i<dateList.length;i++){
		dateArr[dateList[i]] = i;
	}
	for(i=0;i<dateArr.length;i++){
		dateArr[i] = dateArr[i].length>20 ? dateArr[i].substring(0,20) + ".." : dateArr[i];
	}
	
$(function()
{

	
	$(".AnalyzeResultHeader").show();
	
	$("#events-details").removeClass("active-menu");
	$("#events-analysis").addClass("active-menu");
	
	$("#rightChartSettingApplyDate").click(function(){
		$("#amountDateViewSettings").hide();
		if($('input[name=graphType2Date]:radio:checked').val() == 'verticalBar')
		{
			displayBarGraphDate('column');
		} else if($('input[name=graphType2Date]:radio:checked').val() == 'HorizontalBar')
		{
			displayBarGraphDate('bar');
		}
	});
	
	$("#amountByDateCancel").click(function(){
		$("#amountDateViewSettings").hide();
	});
	
	$(".closewhite").click(function(){
			$("#amountDateViewSettings").hide();
	});
		
	$("#resetChartOptionDate").click(function(){
			$("input[id=amountEventDateVerticalBar]").prop("checked", true);
	});
	
	$("div#noEventsByDatePopupDiv").mouseover(function(){
		$("#noEventsByDatePopupDiv").show();
	}).mouseout(function(){
		$("#noEventsByDatePopupDiv").hide();
	});
	
	if(<%=!isHomePage%>)
	{
		if(dateList==null || dateList == '')
		{
			$("#p_p_id<portlet:namespace/>").hide();
			$(".cph").show();
			return;
		}
	}
	else
	{
		if(dateList==null || dateList == '')
		{
			$("#amountbydateDiv").removeClass("hideClass");
			$("#amountOfDatesChartContainer").addClass("hideClass");
			
			$("#No-Data-amountDate").addClass("No-Data-Charts");
			$("#amount_bydate_settings").hide();
		}
		else
		{
			$("#No-Data-amountDate").removeClass("No-Data-Charts");
			$("#amountbydateDiv").addClass("hideClass");
		}
	}
	
	$(document).click(function() {
		$("div#noEventsByDatePopupDiv").hide();
	});
	
	$('#amntByDatePrintCharts').click(function() {
        var chart = $('#amountOfDatesChartContainer').highcharts();
        chart.print();
    });
	$('#amntByDateJPG').click(function() {
        var chart = $('#amountOfDatesChartContainer').highcharts();
        chart.exportChart({type: 'image/jpeg'});
    });
	$('#amntByDatePNG').click(function() {
        var chart = $('#amountOfDatesChartContainer').highcharts();
        chart.exportChart({type: 'image/png'});
    });
    

	var labelXPosition = 0;
	var labelYPosition = 0;
	var labelRotation = 0;
	
	if(amtByDateChartType == 'bar')
	{
		labelXPosition = -5;
		labelYPosition = 0;
		labelRotation = 0;
	}
	else
	{
		labelXPosition = 5;
		labelYPosition = 10;
		labelRotation = -45;
	}
	
	var splitLineCount = parseFloat(splitLineCountVariable) - parseFloat(0.5);
	$('#amountOfDatesChartContainer').highcharts(
	{
        chart: 
        {
            type: amtByDateChartType
        },
        title: 
        {
            text: amtByDateChartTitle
        },
        xAxis: 
        {
            categories: dateList,
            labels: 
            {
                rotation: labelRotation,
				y: labelYPosition,
				x: labelXPosition,
				align: 'right',
				
			},
			plotLines: [{
                color: '#000000',
                width: 2,
                dashStyle: 'Solid',
                value: parseFloat(splitLineCount)
            }]
        },
        yAxis: 
        {
        	gridLineWidth: 1,
			gridLineColor: '#cecece',
			minorGridLineColor: '#cecece',
			lineWidth: 1,
			stackLabels: 
            {
                enabled: false,
                style: 
                {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: 
        {
        	enabled: true
    	},
    	tooltip : {
			useHTML : true,
			positioner : function(boxWidth, boxHeight,
					point) {
					
					var xPosition = point.plotX - 10;
					var yPosition = point.plotY - boxHeight + 38;
					
					if(amtByDateChartType == 'column')
					{
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(50))
						{
							xPosition = point.plotX-45;
						}
						
						if(parseInt(point.plotY) < parseInt(boxHeight))
						{
							yPosition = point.plotY+80;
						}
						
					}
					else
					{
						xPosition = point.plotX + 103;
						yPosition = point.plotY - boxHeight + 100;
						
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(40))
						{
							xPosition = point.plotX-50;
						}
						
						if((parseInt(boxHeight) - parseInt(point.plotY)) > parseInt(100))
						{
							yPosition = point.plotY - boxHeight + 200;
						}
					}
					
				return {
					x : xPosition,
					y : yPosition
				};
			},
			formatter: function()
			{
				var url = "events-details?drilldownDate=" + encodeURIComponent(this.key) + searchCriteria;
				
				var pointerPosition = 0;
				for(var i=0; i< dateList.length; i++){
					if(this.key == dateList[i]){
						pointerPosition = i;
					}
				}
				
				var tooltipOption = '';
				
				tooltipOption += '<div class="Tooltip-Heading">'+this.key+'</div><div class="Tooltip-Recent">'+ dateCounts[pointerPosition]+' Total Events</div><div class="Tooltip-Recent">Top Practice Area</div>';
				for(var index=0; index< practiceAreaSetChart[pointerPosition].length; index++)
				{
					tooltipOption += '<div class="Tooltip-Rows"  title="'+ practiceAreaSetChart[pointerPosition][index] +'">' + (practiceAreaSetChart[pointerPosition][index].length > 15 ? practiceAreaSetChart[pointerPosition][index].substring(0, 15) + '...' : practiceAreaSetChart[pointerPosition][index]) +'</div>';
				}
				
				tooltipOption += '<div class="Tooltip-Recent">Top Locations</div>';
				
				if(locSetChart[pointerPosition] != undefined)
				{
					for(var index=0; index< locSetChart[pointerPosition].length; index++)
					{
						tooltipOption += '<div class="Tooltip-Rows"  title="'+ locSetChart[pointerPosition][index] +'">' + (locSetChart[pointerPosition][index].length > 15 ? locSetChart[pointerPosition][index].substring(0, 15) + '...' : locSetChart[pointerPosition][index]) +'</div>';
					}
				}
				
				
				tooltipOption += '<div class="clickToViewDetails"><a href="'+url+'" class="Tooltip-ClickToViewDetails">Click to View Details</a></div>';
				return tooltipOption;
			}
		},
        plotOptions: 
        {
            series: 
            {
            	stacking: 'normal',
            	dataLabels: 
            	{
                    enabled: false,
                    color: '#FFFFFF'
                },
                pointWidth: pointWidth,
            }
        },
        series: [
        {
            name: 'Date',
            color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#506a85'],
					[1, '#15375c']
				]
			},
            data: dateCounts
        }],
		navigation: {
            buttonOptions: {
                enabled: false
            }
        }
    });
});

function displayBarGraphDate(type)
{
	var labelXPosition = 0;
	var labelYPosition = 0;
	var labelRotation = 0;
	if(type == 'bar')
	{
		labelXPosition = 0;
		labelYPosition = 0;
		labelRotation = 0;
	}
	else
	{
		labelXPosition = -15;
		labelYPosition = 30;
		labelRotation = -45;
	}
	$('#amountOfDatesChartContainer').highcharts({
        chart: {
            type: type
        },
        title:
		{
            text: amtByDateChartTitle
        },
        xAxis: {
            categories: dateList,
            labels: 
            {
                rotation: labelRotation,
                y: labelYPosition,
				x: labelXPosition,
				formatter: function()
				{
					var firmName = this.value;
					if(firmName.length > 10)
					{
						firmName = firmName.substring(0, 10) + "...";
					}
					return firmName;
				}
			}
        },
        yAxis: {
            min: 0,
            gridLineWidth: 1,
			gridLineColor: '#cecece',
			minorGridLineColor: '#cecece',
			lineWidth: 1,
            stackLabels: {
                enabled: false,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
        	enabled: true
    	},
        tooltip : {
			useHTML : true,
			positioner : function(boxWidth, boxHeight,
					point) {
					
					var xPosition = point.plotX - 10;
					var yPosition = point.plotY - boxHeight + 38;
					
					if(type == 'column')
					{
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(200))
						{
							xPosition = point.plotX-100;
						}
						
						if(parseInt(point.plotY) < parseInt(boxHeight))
						{
							yPosition = point.plotY+90;
						}
					}
					else
					{
						xPosition = point.plotX + 103;
						yPosition = point.plotY - boxHeight + 100;
						
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(40))
						{
							xPosition = point.plotX-50;
						}
						
						if((parseInt(boxHeight) - parseInt(point.plotY)) > parseInt(100))
						{
							yPosition = point.plotY - boxHeight + 200;
						}
					}
					
				return {
					x : xPosition,
					y : yPosition
				};
			},
			formatter: function()
			{
			
				var url = "events-details?drilldownDate=" + encodeURIComponent(this.key) + searchCriteria;
									
				var eventsStringArr = eventsTitles[dateArr[this.x]];
				var eventsStr = "";
				for(i=0;i<eventsStringArr.length;i++){
					eventsStr += '<div class="Tooltip-Rows" title="'+ eventsTitles[dateArr[this.x]][i] +'">' + (eventsTitles[dateArr[this.x]][i].length > 10 ? eventsTitles[dateArr[this.x]][i].substring(0, 10) + '...' : eventsTitles[dateArr[this.x]][i]) +'</div>';
				}
				
				var tooltipOption = '';
				tooltipOption += '<div class="Tooltip-Heading">Corporate and Business</div>'+
				
				'<div class="Tooltip-Recent">Recent Publications</div>';
				tooltipOption += eventsStr;
				tooltipOption +='<div class="clickToViewDetails"><a href="'+url+'" class="Tooltip-ClickToViewDetails">Click to View Details</a></div>'
				eventsStr = "";
				return tooltipOption;
			}
		},
        plotOptions: {
        	column: {
                
                dataLabels: {
                    enabled: false,
                    color: '#000000',
                    formatter: function(){
                    	if(this.y > 0){
                    		return this.y;
                    	}
                    }
                }
            },
			series: {
				pointWidth: pointWidth,
			}
        },
        series: [
        {
            name: 'Dates',
            color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#506a85'],
					[1, '#15375c']
				]
			},
            data: dateCounts
        }],
		navigation: {
            buttonOptions: {
                enabled: false
            }
        }
    });
}

$(document).ready(function()
{	
    $("#amount_bydate").click(function()
    {
    	Liferay.Portlet.showPopup(
        		{
        			uri : '${chartSearchPortletURL}', // defined in common.jsp
        			title: "Search Criteria"
        		});
		
	});
	
	
});
}());

</script>

<div class="newspublicationPage marbtm4">
		<div class="colMin flLeft leftDynamicDiv">
			<div class="topHeader ForChartsTopHeader">
				<span title="Remove this chart" onclick="removeThisChartAmountsByDate();" style="float: right; font-weight: bold; color: rgb(255, 255, 255); cursor: pointer; font-family: verdana; margin: 2px 5px; padding: 3px 8px;">X</span>
			</div>
			<div id="noEventsByDatePopupDiv" style="z-index: 999; width: 200px; position: absolute; background:#ffffff; padding: 10px 10px 10px 10px; border: 1px solid #1A1A1A; display: none;">
				<div id="noEventsByDatePopupData" class="Popup-Tooltip"></div>
			</div>
			<div id="amountbydateDiv" class="hideClass">0 Results, Please try a different search</div>
				<div class="flRight charts" id="No-Data-amountDate">
				
				<ul class="reset listView">
					<c:if test="<%=isHomePage%>" >
						<li id="amount_bydate" style="overflow:hidden;"><a href="javascript:void(0);" class="filter-icon" >&nbsp;</a></li>
					</c:if>
					<li id="amount_bydate_settings"><a href="#amountDateViewSettings"
						class="btn icon settingsgry rightViewSetting login-window chartViewSetting"
						onclick="return false;">&nbsp;</a></li>
					<li>
						<a href="javascript:void(0);" id="amntByDatePrintCharts" onclick="return false;" class="printChartClass"></a>
					</li>
					<li>
						<a href="javascript:void(0);" onclick="return false;" class="exportChartClass"></a>
                        <div class="actionSec">
                        <h5>Actions</h5>
                        <ul class="reset">
                            <li class="exportChartImage"><span id="amntByDateJPG">Export as JPG</span></li>
                            <li class="exportChartImage"><span id="amntByDatePNG">Export as PNG</span></li>
                        </ul>
                        <div class="clear">&nbsp;</div>
                        </div>
					</li>
				</ul>
						
				<div id="amountDateViewSettings"" class="viewBox popusdiv ClickPopup" style="display:none; width:400px !important;">
                    <div class="popHeader"> <a href="javascript:void(0);" class="btn icon closewhite closeOne flRight" style="margin-top: 2px">Close</a> SETTINGS: EVENTS BY DATE
                    	<div class="clear">&nbsp;</div>
                	</div>
	                <div class="popMiddle">
						<h6>Chart Type</h6>
							
						<form action="#">
							<ul class="reset list2">
								<li>
									<input type="radio" id="amountEventDateVerticalBar" name="graphType2Date" value="verticalBar" class="graphType" checked="checked">
									<span class="btn icon barchartvert">Vertical Bar</span>
								</li>
								<li>
									<input type="radio" id="graphType2Date" name="graphType2Date" value="HorizontalBar" class="graphType">
									<span class="btn icon barcharthori">Horizontal Bar</span>
								</li>
							</ul>
						</form>
							<div class="clear">&nbsp;</div>
					</div>
	                
	                <div class="popFooter">
						<hr>
						<div>
							<input type="button" value="Reset All" class="buttonTwo flLeft rightReset" id="resetChartOptionDate">
							<input type="button" value="Cancel" class="buttonTwo flRight rightReset" id="amountByDateCancel">
							<input type="button" value="Apply" class="buttonOne flRight" id="rightChartSettingApplyDate" style="margin: 0 5px 0 0;">
							<div class="clear">&nbsp;</div>
						</div>
					</div>
               </div>
                    			
			</div>
			
			<div id="amountOfDatesChartContainer" class="charts-spacing"></div>
		</div>
	</div>
<form id="removeThisChartAmountsByDate" method="post">
</form>	
