<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ taglib prefix="portlet" 		uri="http://java.sun.com/portlet_2_0"%> 
<%@ taglib prefix="c" 			 	uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"          uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form"            uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="liferay-ui" 		uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="liferay-theme"	uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="liferay-portlet" uri="http://liferay.com/tld/portlet"%>
<%@ taglib prefix="aui" 			uri="http://liferay.com/tld/aui"%>

<portlet:defineObjects/>

<%
	boolean isHomePage = false;
	ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	if( themeDisplay != null)
	{
		String friendlyURL = themeDisplay.getLayout().getFriendlyURL();
		if( friendlyURL != null && !friendlyURL.equalsIgnoreCase("")
			&& ( friendlyURL.equalsIgnoreCase("/almost") || friendlyURL.equalsIgnoreCase("/home") ) )
		{
			isHomePage = true;
		}
	}
	
	pageContext.setAttribute("isHomePage", isHomePage); // by this way it can be used via EL as well
%>

<liferay-portlet:renderURL
    var="chartSearchPortletURL"
    portletName="reeventsearchportlet_WAR_reevent"
    windowState="<%=LiferayWindowState.EXCLUSIVE.toString() %>">
    <liferay-portlet:param name="renderPage" value="searchCriteriaForChart" />
    <liferay-portlet:param name="chartPortletId" value="<%= themeDisplay.getPortletDisplay().getId() %>" />
</liferay-portlet:renderURL>