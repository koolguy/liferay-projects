<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="java.util.Map"%>
<%@ taglib prefix="portlet" 		uri="http://java.sun.com/portlet_2_0"%> 
<%@ taglib prefix="c" 			 	uri="http://java.sun.com/jsp/jstl/core" %>

<portlet:defineObjects/>

<%@ include file="./common.jsp" %>

<portlet:actionURL var="removePortletURL">
	<portlet:param name="action" value="removePortleFromThePage"/>
</portlet:actionURL>

<script type="text/javascript">

function removeThisChartAmountPracticeArea(){
	
	var removePortletAction = '<%= removePortletURL.toString()%>';
	var r=confirm("Are you sure you want to delete this chart?");
	if (r == true)
  	{

		Liferay.Portlet.showBusyIcon("#bodyId", "Loading...");
		$("#removeThisChartAmountPracticeArea").attr('action', removePortletAction);
		$("#removeThisChartAmountPracticeArea").submit();
  	}
	else
	{
	  return false;
	}
}
(function () {

<%@ include file="./theme.jsp" %>

	var eventsByPracticeChartType 		= '${chartType}'; 
	var eventsByPracticeChartTitle 		= '${chartTitle}';
	var eventsByPracticeAreasOfPractice = [${eventsByPracticeAreasOfPractice}]; 	
	var eventsByPracticeEventsCount 	= [${eventsByPracticeEventCount}]; 	 
	var eventsByPracticeEventTitles 	= [${eventsByPracticeEventTitles}]; 	 
	var pointWidth						= 15;

$(function()
{


	$(".AnalyzeResultHeader").show();
	$("#events-details").removeClass("active-menu");
	$("#events-analysis").addClass("active-menu");	
	
	$("#rightChartSettingApplyPracticeArea").click(function(){
		$("#amountPracticeAreaViewSettings").hide();
		if($('input[name=graphType2PracticeArea]:radio:checked').val() == 'verticalBar')
		{
			displayBarGraphPracticeArea('column');
		} else if($('input[name=graphType2PracticeArea]:radio:checked').val() == 'HorizontalBar')
		{
			displayBarGraphPracticeArea('bar');
		}
	});
	
	$("#amountPracticeAreaCancel").click(function(){
		$("#amountPracticeAreaViewSettings").hide();
	});
	
	$("div#noEventsByPracticePopupDiv").mouseover(function(){
		$("#noEventsByPracticePopupDiv").show();
	}).mouseout(function(){
		$("#noEventsByPracticePopupDiv").hide();
	});
	
	if(<%=!isHomePage%>)
	{
		if(eventsByPracticeAreasOfPractice == null || eventsByPracticeAreasOfPractice == '')
		{
			$("#p_p_id<portlet:namespace/>").hide();
			$(".cph").show();
			return;
		}	
	}
	else
	{
		if(eventsByPracticeAreasOfPractice == null || eventsByPracticeAreasOfPractice == '')
		{
			$("#amountPracticeArea").removeClass("hideClass");
			$("#amountOfPracticeAreasChartContainer").addClass("hideClass");
			
			$("#No-Data-amountPracticeAres").addClass("No-Data-Charts");
			$("#amount_practiceArea_settings").hide();
		}
		else
		{
			$("#No-Data-amountPracticeAres").removeClass("No-Data-Charts");
			$("#amountPracticeArea").addClass("hideClass");
		}	
	}	
	
	
	$(".closewhite").click(function(){
			$("#amountPracticeAreaViewSettings").hide();
	});
		
		
	$("#resetChartOptionPracticeArea").click(function(){
			$("input[value=verticalBar]").prop("checked", true);
	});
	
	$(document).click(function() {
		$("div#noEventsByPracticePopupDiv").hide();
	});
	
	$('#amntByPracticePrintCharts').click(function() {
        var chart = $('#amountOfPracticeAreasChartContainer').highcharts();
        chart.print();
    });
	$('#amntByPracticePrintJPG').click(function() {
        var chart = $('#amountOfPracticeAreasChartContainer').highcharts();
        chart.exportChart({type: 'image/jpeg'});
    });
	$('#amntByPracticePrintPNG').click(function() {
        var chart = $('#amountOfPracticeAreasChartContainer').highcharts();
        chart.exportChart({type: 'image/png'});
    });
	
	var labelXPosition = 0;
	var labelYPosition = 0;
	var labelRotation = 0;
	
	if(eventsByPracticeChartType == 'bar')
	{
		labelXPosition = -5;
		labelYPosition = 0;
		labelRotation = 0;
	}
	else
	{
		labelXPosition = 5;
		labelYPosition = 10;
		labelRotation = -45;
	}
	
	$('#amountOfPracticeAreasChartContainer').highcharts(
	{
	
	 chart:
		{
		 	type: eventsByPracticeChartType,
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: 
        {
            text: eventsByPracticeChartTitle
        },
        xAxis: 
        {
            categories: eventsByPracticeAreasOfPractice,
            labels : {
					rotation: labelRotation,
				y: labelYPosition,
				x: labelXPosition,
				align: 'right',
					formatter : function() {
						var firmName = this.value;
						if (firmName.length > 10) {
							firmName = firmName
									.substring(0, 10)
									+ "...";
						}
						return firmName;
					}
				},
        },
        yAxis: 
        {
        	gridLineWidth: 1,
			gridLineColor: '#cecece',
			minorGridLineColor: '#cecece',
			lineWidth: 1,
            stackLabels: 
            {
                enabled: false,
                style: 
                {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: 
        {
        	enabled: true
    	},
    	tooltip : {
			useHTML : true,
			positioner : function(boxWidth, boxHeight,
					point) {
					
					var xPosition = point.plotX - 10;
					var yPosition = point.plotY - boxHeight + 38;
					
					if(eventsByPracticeChartType == 'column')
					{
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(130))
						{
							xPosition = point.plotX-60;
						}
						
						if(parseInt(point.plotY) < parseInt(boxHeight))
						{
							yPosition = point.plotY+90;
						}
					}
					else
					{
						xPosition = point.plotX + 130;
						yPosition = point.plotY - boxHeight + 100;
						
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(100))
						{
							xPosition = point.plotX-50;
						}
						
						if((parseInt(boxHeight) - parseInt(point.plotY)) > parseInt(100))
						{
							yPosition = point.plotY - boxHeight + 150;
						}
					}
					
				return {
					x : xPosition,
					y : yPosition
				};
			},
			formatter: function()
			{
									
				var pointerPosition = 0;
				for(var index = 0; index < eventsByPracticeAreasOfPractice.length; index++)
				{
					if(this.key == eventsByPracticeAreasOfPractice[index])
					{
						pointerPosition = index;
					}
				}
				
				var url = "events-details?drilldownPracticeArea=" + encodeURIComponent(this.key) + searchCriteria;
				
				var tooltipOption = '';
				tooltipOption += '<div class="Tooltip-Heading">'+this.key+'</div><div class="Tooltip-Recent">Recent Events</div>';
				for(var index=0; index< eventsByPracticeEventTitles[pointerPosition].length; index++)
				{
					tooltipOption += '<div class="Tooltip-Rows"  title="'+ eventsByPracticeEventTitles[pointerPosition][index] +'">' + (eventsByPracticeEventTitles[pointerPosition][index].length > 15 ? eventsByPracticeEventTitles[pointerPosition][index].substring(0, 15) + '...' : eventsByPracticeEventTitles[pointerPosition][index]) +'</div>';
				}
				tooltipOption += '<div class="clickToViewDetails"><a href="'+url+'" class="Tooltip-ClickToViewDetails">Click to View Details</a></div>'

				return tooltipOption;
			}
		},
        plotOptions: 
        {
            series: 
            {
            	stacking: 'normal',
            	dataLabels: 
            	{
                    enabled: false,
                    color: '#FFFFFF'
                },
                pointWidth: pointWidth,
            }
        },
        series: [
        {
            name: 'Practice Area',
            color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#506a85'],
					[1, '#15375c']
				]
			},
            data: eventsByPracticeEventsCount
        }],
		navigation: {
            buttonOptions: {
                enabled: false
            }
        }
    });
});

function displayBarGraphPracticeArea(type)
{
	var labelXPosition = 0;
	var labelYPosition = 0;
	var labelRotation = 0;
	if(type == 'bar')
	{
		labelXPosition = 0;
		labelYPosition = 0;
		labelRotation = 0;
	}
	else
	{
		labelXPosition = -15;
		labelYPosition = 30;
		labelRotation = -45;
	}
	$('#amountOfPracticeAreasChartContainer').highcharts({
        chart: {
            type: type
        },
        title:
		{
            text: eventsByPracticeChartTitle
        },
        xAxis: {
            categories: eventsByPracticeAreasOfPractice,
            labels: 
            {
                rotation: labelRotation,
                y: labelYPosition,
				x: labelXPosition,
				formatter: function()
				{
					var firmName = this.value;
					if(firmName.length > 10)
					{
						firmName = firmName.substring(0, 10) + "...";
					}
					return firmName;
				}
			}
        },
        yAxis: {
            min: 0,
            gridLineWidth: 1,
			gridLineColor: '#cecece',
			minorGridLineColor: '#cecece',
			lineWidth: 1,
            stackLabels: {
                enabled: false,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
        	enabled: true
    	},
        tooltip : {
			useHTML : true,
			positioner : function(boxWidth, boxHeight,
					point) {
					
					var xPosition = point.plotX - 20;
					var yPosition = point.plotY - boxHeight + 38;

					xPosition = point.plotX + 103;
					yPosition = point.plotY - boxHeight + 100;
					
					if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(40))
					{
						xPosition = point.plotX-50;
					}
					
					if((parseInt(boxHeight) - parseInt(point.plotY)) > parseInt(100))
					{
						yPosition = point.plotY - boxHeight + 200;
					}
						
				return {
					x : xPosition,
					y : yPosition
				};
			},
			formatter: function()
			{
				var pointerPosition = 0;
				for(var index = 0; index < eventsByPracticeAreasOfPractice.length; index++)
				{
					if(this.key == eventsByPracticeAreasOfPractice[index])
					{
						pointerPosition = index;
					}
				}
				
				var url = "events-details?drilldownPracticeArea=" + encodeURIComponent(this.key) + searchCriteria;
				
				var tooltipOption = '';
				tooltipOption += '<div class="Tooltip-Heading">'+this.key+'</div><div class="Tooltip-Recent">Recent Events</div>';
				for(var index=0; index< eventsByPracticeEventTitles[pointerPosition].length; index++)
				{
					tooltipOption += '<div class="Tooltip-Rows"  title="'+ eventsByPracticeEventTitles[pointerPosition][index] +'">' + (eventsByPracticeEventTitles[pointerPosition][index].length > 15 ? eventsByPracticeEventTitles[pointerPosition][index].substring(0, 15) + '...' : eventsByPracticeEventTitles[pointerPosition][index]) +'</div>';
				}
				tooltipOption += '<div class="clickToViewDetails"><a href="'+url+'" class="Tooltip-ClickToViewDetails">Click to View Details</a></div>'
	
				return tooltipOption;
			}
		},
        plotOptions: {
        	column: {
                dataLabels: {
                    enabled: false,
                    color: '#000000',
                    formatter: function(){
                    	if(this.y > 0){
                    		return this.y;
                    	}
                    }
                }
            },
			series: 
            {
            	stacking: 'normal',
            	dataLabels: 
            	{
                    enabled: false,
                    color: '#FFFFFF'
                },
                pointWidth: pointWidth,
            }
        },
        series: [
        {
            name: 'Practice Area',
            color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#506a85'],
					[1, '#15375c']
				]
			},
            data: eventsByPracticeEventsCount
        }],
		navigation: {
            buttonOptions: {
                enabled: false
            }
        }
    });
}

$(document).ready(function()
{	
    $("#amount_practiceArea").click(function()
    {
    	Liferay.Portlet.showPopup(
        		{
        			uri : '${chartSearchPortletURL}', // defined in common.jsp
        			title: "Search Criteria"
        		});	
	});
	
	
});

}());
</script>

<div class="newspublicationPage marbtm4">
		<div class="colMin flLeft leftDynamicDiv">
			<div class="topHeader ForChartsTopHeader">
				<span title="Remove this chart" onclick="removeThisChartAmountPracticeArea();" style="float: right; font-weight: bold; color: rgb(255, 255, 255); cursor: pointer; font-family: verdana; margin: 2px 5px; padding: 3px 8px;">X</span>
			</div>
			<div id="noEventsByPracticePopupDiv" style="z-index: 999; width: 200px; position: absolute; background:#ffffff; padding: 10px 10px 10px 10px; border: 1px solid #1A1A1A; display: none;">
				<div id="noEventsByPracticePopupData" class="Popup-Tooltip"></div>
			</div>
			<div id="amountPracticeArea" class="hideClass">0 Results, Please try a different search</div>
			<div class="flRight charts" id="No-Data-amountPracticeAres">
				<ul class="reset listView">
					<c:if test="<%=isHomePage%>" >
						<li id="amount_practiceArea" style="overflow:hidden;"><a href="javascript:void(0);" class="filter-icon" >&nbsp;</a></li>
					</c:if>
					<li id="amount_practiceArea_settings"><a href="#amountPracticeAreaViewSettings"
						class="btn icon settingsgry rightViewSetting login-window chartViewSetting"
						onclick="return false;">&nbsp;</a></li>
					<li>
						<a href="javascript:void(0);" id="amntByPracticePrintCharts" onclick="return false;" class="printChartClass"></a>
					</li>
					<li>
						<a href="javascript:void(0);" onclick="return false;" class="exportChartClass"></a>
                        <div class="actionSec">
                        <h5>Actions</h5>
                        <ul class="reset">
                            <li class="exportChartImage"><span id="amntByPracticePrintJPG">Export as JPG</span></li>
                            <li class="exportChartImage"><span id="amntByPracticePrintPNG">Export as PNG</span></li>
                        </ul>
                        <div class="clear">&nbsp;</div>
                        </div>
					</li>
				</ul>
						
				<div id="amountPracticeAreaViewSettings"" class="viewBox popusdiv ClickPopup" style="display:none; width:400px !important;">
                	<div class="popHeader"> <a href="javascript:void(0);" class="btn icon closewhite closeOne flRight" style="margin-top: 2px">Close</a> SETTINGS: NUMBER OF EVENTS BY PRACTICE AREA
                    	<div class="clear">&nbsp;</div>
                	</div>
	                <div class="popMiddle">
						<h6>Chart Type</h6>
							
						<form action="#">
							<ul class="reset list2">
								<li>
									<input type="radio" id="graphType2PracticeArea" name="graphType2PracticeArea" value="verticalBar" class="graphType" checked="checked">
									<span class="btn icon barchartvert">Vertical Bar</span>
								</li>
								<li>
									<input type="radio" id="graphType2PracticeArea" name="graphType2PracticeArea" value="HorizontalBar" class="graphType">
									<span class="btn icon barcharthori">Horizontal Bar</span>
								</li>
							</ul>
						</form>
							<div class="clear">&nbsp;</div>
					</div>
	                
	                <div class="popFooter">
						<hr>
						<div>
							<input type="button" value="Reset All" class="buttonTwo flLeft rightReset" id="resetChartOptionPracticeArea">
							<input type="button" value="Cancel" class="buttonTwo flRight rightReset" id="amountPracticeAreaCancel">
							<input type="button" value="Apply" class="buttonOne flRight" id="rightChartSettingApplyPracticeArea" style="margin: 0 5px 0 0;">
							<div class="clear">&nbsp;</div>
						</div>
					</div>
               </div>
                    			
			</div>
			
			<div id="amountOfPracticeAreasChartContainer" class="charts-spacing">
		</div>
	</div>
</div>	
	<form id="removeThisChartAmountPracticeArea" method="post"></form>