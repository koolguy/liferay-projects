<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"      uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %> 


<portlet:defineObjects/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>ALM Rival Edge</title>
<link rel="shortcut icon" type="<%= renderRequest.getContextPath() %>/images/x-icon" href="images/favicon.ico"/>
</head>
<body style="background:#fff">

<div class="printSec">
	  <div class="topBar">
		    <ul class="tplist reset flRight">
		      <li><a href="#" onClick="window.print()">Print</a></li>
		      <li><a href="#" onClick="window.close()">Close</a></li>
		    </ul>
		    <div class="clear">&nbsp;</div>
      </div>
      <div>
		   <div class="dateSec flRight">Date Printed : ${printedDateStr} at ${formattedTime}</div>
		   <h1 id="logo"><a href="#"><img src="<%= renderRequest.getContextPath() %>/images/logo1.png" width="154" height="27" alt="logo"/></a></h1>
		   <div class="clear">&nbsp;</div>
  	 </div>
  		 <h4>Events</h4>
  		   <div class="flLeft resultsec"><strong>${eventSearchModelBean.totalResultCount}&nbsp;of&nbsp;${eventSearchModelBean.totalResultCount} Results</strong> 
  		 <span>(
          		<c:choose>
	      				 <c:when test="${fn:length(eventSearchModelBean.selectedFirms) gt 100}">
	      				 	${fn:substring(eventSearchModelBean.selectedFirms, 0, 99)}..,&nbsp;
	      				 </c:when>
	      				 <c:otherwise>
	      				 	${eventSearchModelBean.selectedFirms},&nbsp;
	      				 </c:otherwise>		        		  
				</c:choose>				
	      		${eventSearchModelBean.selectedLocation},&nbsp;	      				
				 ${eventSearchModelBean.dateText},&nbsp;  
				<c:choose>
	      				 <c:when test="${fn:length(eventSearchModelBean.keywords) gt 100}">
	      				 	${fn:substring(eventSearchModelBean.keywords, 0, 99)}..,&nbsp;
	      				 </c:when>
	      				  <c:otherwise>
	      				 	<c:if test="${not empty eventSearchModelBean.keywords}">
	      				 		${eventSearchModelBean.keywords}, 
	      				 	</c:if>
	      				 </c:otherwise>		        		  
				</c:choose>
				<c:choose>
	      				 <c:when test="${fn:length(eventSearchModelBean.selectedPracticeArea) gt 100}">
	      				 	${fn:substring(eventSearchModelBean.selectedPracticeArea, 0, 99)}..,&nbsp;
	      				 </c:when>
	      				 <c:otherwise>
	      				 	${eventSearchModelBean.selectedPracticeArea},&nbsp;
	      				 </c:otherwise>		        		  
				</c:choose>      				 
	      				    		
          )</span>
  		
  		</div>
  		<div class="clear">&nbsp;</div>
  		   <table class="tbleTwo" width="100%" border="0" cellspacing="0" cellpadding="0">
          
          <colgroup>
            	
				<col width="10%"/>
				<col width="10%"/>
				<col width="50%"/>
				<col width="20%"/>
				<col width="10%"/>
				
                
          </colgroup>
           <thead>
           <tr>
           		<th>Date</th>
           		<th>Firm</th>
           		<th>Event</th>
           		<th>Practices</th>
           		<th>Location</th>
           </tr>
           </thead>
           <tbody>
		<c:choose>
			<c:when test="${fn:length(eventPrintResultsList) gt 0}">
				<c:forEach items="${eventPrintResultsList}" var="results" varStatus="i">
				
				<!-- START GROUP -->
				
				<c:choose>
						<%-- For first row in Table --%>
   						 <c:when test="${(i.first) && (eventSearchModelBean.orderBy == 9)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.resultFirm.company}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(i.first) && (eventSearchModelBean.orderBy == 7)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.location}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(i.first) && (eventSearchModelBean.orderBy == 8)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.practiceArea}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 <%-- For subsequent rows in Table only when there is change in the CompanName or practiceArea or location in 2 consecutive records --%>
   						 <c:when test="${(i.index > 0) && (eventSearchModelBean.orderBy == 9) && (eventSearchResults[i.index - 1].resultFirm.company != eventSearchResults[i.index].resultFirm.company)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.resultFirm.company}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(i.index > 0) && (eventSearchModelBean.orderBy == 8) && (eventSearchResults[i.index - 1].practiceArea != eventSearchResults[i.index].practiceArea)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.practiceArea}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 <c:when test="${(i.index > 0) && (eventSearchModelBean.orderBy == 7) && (eventSearchResults[i.index - 1].location != eventSearchResults[i.index].location)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1">
	      		 					<strong>
	      		 					<c:choose>
										<c:when test="${empty results.location or results.location == ''}">
				      				 	 	Location Not Identified 
				      					</c:when>
				      					<c:when test="${fn:contains(results.location, '-')}"> 
				      				 	 	${fn:substring(results.location, 3, fn:length(results.location))} 
				      					</c:when>
										<c:otherwise>
				      				 	 	${results.location} 
				      					</c:otherwise>
									</c:choose>
	      		 					</strong>
	      		 					</div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 
				</c:choose>
				
				<!-- END GROUP -->
			
					<!-- <tr class="odd"> -->
					 <tr class="${i.index % 2 == 0 ? 'odd' : 'even'}">
					
						<%-- <td align="center">${results.dataType}</td> --%>
						<td><fmt:formatDate pattern="MM/dd/yyyy"
								value="${results.eventDate}" /></td>
						<td><a href="${results.resultFirm.domain}" target="_blank">${results.resultFirm.company}</a></td>
						
						<td><font color="red"><a href="${results.eventURL}" target="_blank">${results.eventTitle}</a></font>
							<br/> <c:choose>
								<c:when test="${fn:length(results.eventDescription) gt 254}">
				      				 	${fn:substring(results.eventDescription, 0, 251)}...
				      				 </c:when>
								<c:otherwise>
				      				 	${results.eventDescription} 
				      				 </c:otherwise>
							</c:choose></td>
						<td>${results.practiceArea}</td>
						<td>${results.location}</td>
						
					 </tr> 
				</c:forEach>
   			
			</c:when>
			<c:otherwise>
				<tr class="odd">
					<td colspan="7" align="center">0 Results, Please try a	different search</td>
				</tr>
			</c:otherwise>

		</c:choose>
	
	</tbody>
	</table>
  		
</div>
</body>
</html>

