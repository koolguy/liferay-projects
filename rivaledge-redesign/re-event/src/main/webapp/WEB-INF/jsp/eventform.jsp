<%@page import="com.alm.rivaledge.util.ALMConstants"%>
<%@page import="com.alm.rivaledge.transferobject.EventSearchDTO"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>


<%@ include file="./common.jsp" %>
<%--
<c:if test="${isHomePage}">

	<script src="/re-theme/js/jquery-1.9.1.js"></script>
	<script src="/re-theme/js/jquery-ui.js"></script> 
	<script src="/re-theme/js/popup.js?v=${buildVersionId}"></script>
	<script src="/re-theme/js/s_code.js"></script> 
	<script src="/re-theme/js/webwidget_tab.js"></script> 

</c:if>

 --%>
<%						
PortletURL printPublicationsURL = renderResponse.createRenderURL();
printPublicationsURL.setWindowState(LiferayWindowState.POP_UP);
printPublicationsURL.setParameter("displayPrintPage", "true");						
%>

<portlet:actionURL var="submitURL">
	<portlet:param name="action" value="changeSearchCriteria"/>
</portlet:actionURL>


<portlet:actionURL var="clickToViewURL">
	<portlet:param name="action" value="clickToView"/>
</portlet:actionURL>

<portlet:resourceURL var="persistSearchCriteriaForChartURL" id="persistSearchCriteriaForChart">
</portlet:resourceURL>

<%--
<style>

.ui-autocomplete-category {
	font-weight: bold;
	padding: .2em .4em;
	margin: .8em 0 .2em;
	line-height: 1.5;
}

.ui-autocomplete {
	max-height: 200px;
	width:220px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
}
</style>
--%>
<script type="text/javascript">
    $(function() {
        $(".webwidget_tab").webwidget_tab({
            head_text_color: '#888',
            head_current_text_color: '#222'
        });
    });
</script>

<script type="text/javascript">

/**
*Autoselect previously selected search critria
*/
	function initializeSearchCriteria()
	{
		<portlet:namespace/>applyFirms();
		initializeDate();
		<portlet:namespace/>applyLocations();
		<portlet:namespace/>applyPracticeArea();
		
		refreshCounters();
	}
/**
 * Initialized the user selected Date from previous search and this is retained through out the session
 */
	function initializeDate()
		{
			
			var dateTextStr = '${eventSearchModelBean.dateText}';
			
			if(dateTextStr.match('^Any') || dateTextStr.match('^Last') || dateTextStr.match('^Next'))
				{
					$("#<portlet:namespace/>datePeriodSelect option[value='"+dateTextStr+"']").attr('selected','selected'); 
					$('#<portlet:namespace/>period').prop("checked", true);
				}
			else{ // we have date range parse it and set the Date on datePicker
					$( "#<portlet:namespace/>from" ).datepicker( "setDate", dateTextStr.split("-")[0]);
					$( "#<portlet:namespace/>to" ).datepicker( "setDate", dateTextStr.split("-")[1]);
				}
			
				$("#<portlet:namespace/>dateText").val(dateTextStr);
	
		}

	function <portlet:namespace/>applyFirms()
	{
		var allSelectedValue = [];
		var allSelectedIds = [];
		
		 $("#<portlet:namespace/>Firmstext").val('');
		 $("#<portlet:namespace/>selectedFirms").val('');
		 
		var isFirmChecked = false;
		var allWatchListCounter = 0;
		
		$('.individualfirmsEventsWatchList input[type=checkbox]:checked').each(function() {
			 allSelectedValue.push($(this).attr('labelAttr'));
			 allSelectedIds.push($(this).val());
			 $("#<portlet:namespace/>firm_watchlist").prop("checked", true);
			 $('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
			 
			 $('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').each(function() 
				{
					allWatchListCounter++;
				});
				 		
			$(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (' + allWatchListCounter +' Selected)');
			isFirmChecked = true;
	 	});
		
		$('.individualEventsfirms input[type=checkbox]:checked').each(function() {
			 allSelectedValue.push($(this).attr('labelAttr'));
			 allSelectedIds.push($(this).val());
			 isFirmChecked = true;
	 	});

		$('.individualfirmsEventsOrganization input[type=checkbox]:checked').each(function() {
			 allSelectedValue.push($(this).attr('labelAttr'));
			 allSelectedIds.push($(this).val());
			 isFirmChecked = true;
	 	});
		
		if(allSelectedValue != ''  &&  allSelectedIds != '') 
			{
				$("#<portlet:namespace/>Firmstext").val(allSelectedValue.join(",")); //Use a comma separator for values which show up on UI
				$("#<portlet:namespace/>selectedFirms").val(allSelectedIds.join(";"));
				 isFirmChecked = true;
			}
		else if($(".rivaledgeListEventsAMLAW_100").is(":checked"))
		{
			$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.AMLAW_100 %>');
			$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.AMLAW_100 %>');
			$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
			$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
			isFirmChecked = true;
		} 
		else if($(".rivaledgeListEventsAMLAW_200").is(":checked"))
		{
			$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.AMLAW_200 %>');
			$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.AMLAW_200 %>');
			$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
			$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
			isFirmChecked = true;
		} 
		else if($(".rivaledgeListEventsNLJ_250").is(":checked"))
		{
			$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.NLJ_250 %>');
			$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.NLJ_250 %>');
			$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
			$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
			isFirmChecked = true;
		} 
		else
		{
			$("#<portlet:namespace/>all_firm_org, #<portlet:namespace/>all_firm, #<portlet:namespace/>all_org, #<portlet:namespace/>firm_watchlist, #<portlet:namespace/>firm_individual, #<portlet:namespace/>org_individual").each(function(){
				
				 $(".rivaledgeListEventsAMLAW_100").prop("checked", false);
				 $(".rivaledgeListEventsAMLAW_200").prop("checked", false);
				 $(".rivaledgeListEventsNLJ_250").prop("checked", false);
				 $('#<portlet:namespace/>individualfirms input[type=checkbox]:checked').removeAttr('checked'); 
				 $("#<portlet:namespace/>firm_watchlist").prop("checked", false);
				
				if($("#<portlet:namespace/>all_firm").is(":checked"))
					{
						$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.ALL_FIRMS %>');
						$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.ALL_FIRMS %>');
						$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.ALL_FIRMS %>");
						isFirmChecked = true;
					} 
				else if($("#<portlet:namespace/>all_org").is(":checked"))
				{
					$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.ALL_ORGANIZATIONS %>');
					$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.ALL_ORGANIZATIONS %>');
					$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.ALL_ORGANIZATIONS %>");
					isFirmChecked = true;
				} 
				else if($("#<portlet:namespace/>all_firm_org").is(":checked"))
				{
					$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.ALL_FIRMS_ORGANIZATION %>');
					$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.ALL_FIRMS_ORGANIZATION %>');
					$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.ALL_FIRMS_ORGANIZATION %>");
					isFirmChecked = true;
				} else
					{
						var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
						$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]').each(function() {
							
							 if(this.value == watchListId)
								{
									$(this).prop("checked", true);
									$("#<portlet:namespace/>Firmstext").val($(this).attr('labelAttr'));
									$("#<portlet:namespace/>selectedFirms").val($(this).val());

									$("#<portlet:namespace/>firm_watchlist").prop("checked", true);
									$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
									isFirmChecked = true;
								}
						});
						
					}
			});
			
		}
		
		if(isFirmChecked == false)
			{
			
				$("#<portlet:namespace/>Firmstext").val('AmLaw 100');
				$("#<portlet:namespace/>selectedFirms").val('AmLaw 100');
				$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
				 $("#<portlet:namespace/>firm_watchlist").prop("checked", true);
				 $(".rivaledgeListEventsAMLAW_100").prop("checked", true);
			}
	}

	function <portlet:namespace/>applyLocations()
	{
	
	var checked= false;
		
		 if($("#<portlet:namespace/>loc_all").is(":checked"))
		 {
				$("#<portlet:namespace/>selectedLocation").val($("#<portlet:namespace/>loc_all").val());
				checked = true;
		 } 
		 if($("#<portlet:namespace/>loc_inperson").is(":checked"))
		 {
				$("#<portlet:namespace/>selectedLocation").val($("#<portlet:namespace/>loc_inperson").val());	
				checked = true;
		 } 
		 if($("#<portlet:namespace/>loc_webinars").is(":checked"))
		 {
				$("#<portlet:namespace/>selectedLocation").val($("#<portlet:namespace/>loc_webinars").val());		
				checked = true;
		 } 
		if(!checked)
		 {
			 var allValsLocation = [];
			
				 $('#<portlet:namespace/>allOtherLocationsDiv input[type=checkbox]:checked').each(function() { 
					 allValsLocation.push($(this).attr('locationValue'));
			    });
			if(allValsLocation.length > 0){
				$("#<portlet:namespace/>loc_individual").prop("checked", true);
			    $("#<portlet:namespace/>selectedLocation").val(allValsLocation.join(";"));		
			}				 
		}
		 
	}


	function <portlet:namespace/>applyPracticeArea()
	{

		 if($(".allEventsPracticeArea").is(":checked"))
		 {
				$("#<portlet:namespace/>selectedPracticeArea").val($(".allEventsPracticeArea").val());			
		 } 
		 else
		 {
			var allValsPracticeArea = [];
				$("#<portlet:namespace/>allPracticeAreaIndividual").prop("checked", true);
			
				 $('#<portlet:namespace/>practiceAreaDiv input[type=checkbox]:checked').each(function() { 
				 	allValsPracticeArea.push($(this).val());
			    });
				 
			    $("#<portlet:namespace/>selectedPracticeArea").val(allValsPracticeArea.join(";"));		
		}
			
	}
	
	// Checking for selected date validation
	function ValidateDate(txtDate)
	{
	    var currVal = txtDate;
	    if(currVal == '')
	        return false;
	    
	    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
	    var dtArray = currVal.match(rxDatePattern); // is format OK?
	    
	    if (dtArray == null) 
	        return false;
	    
	    //Checks for mm/dd/yyyy format.
	    dtMonth = dtArray[1];
	    dtDay= dtArray[3];
	    dtYear = dtArray[5];        
	    
	    if (dtMonth < 1 || dtMonth > 12) 
	        return false;
	    else if (dtDay < 1 || dtDay> 31) 
	        return false;
	    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
	        return false;
	    else if (dtMonth == 2) 
	    {
	        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
	        if (dtDay> 29 || (dtDay ==29 && !isleap)) 
	                return false;
	    }
	    return true;
	}
	
	function applyDate()
		{
		  var  checkDate=false;
		  var dateValue;
		  
		  if($("#<portlet:namespace/>dateRange").is(":checked"))
		   {
		   
		   var fromDate = new Date($("#<portlet:namespace/>from").val());
		   var toDate = new Date($("#<portlet:namespace/>to").val()); 
		   if(!ValidateDate($("#<portlet:namespace/>from").val()))
		   {
		     checkDate=true;
		     $('#<portlet:namespace/>fromdateError').css('display','block');      
		   }
		   else
		   {
		    checkDate=false;
		    $('#<portlet:namespace/>fromdateError').css('display','none');
		   }  
		   if(!ValidateDate($("#<portlet:namespace/>to").val()))
		    {
		    checkDate=true;
		    $('#<portlet:namespace/>todateError').css('display','block');
		    
		    }  
		   else
		   {
		    if(!checkDate)
		    {
		     checkDate=false;
		     $('#<portlet:namespace/>todateError').css('display','none'); 
		    }
		   
		   }
		   if(!(toDate >= fromDate) && !checkDate)
		   {
		    $('#<portlet:namespace/>dateValidError').css('display','block');
		    checkDate=true;
		   } 
		   else
		   {
		    $('#<portlet:namespace/>dateValidError').css('display','none');
		   }
		   if(checkDate)
		   {
		   return checkDate; 
		   }
		  
	  }
	  
	  if($("#<portlet:namespace/>period").is(":checked"))
	  {
	   
	   dateValue=$("#<portlet:namespace/>datePeriodSelect").val();
	   
	  } 
	  else if($("#<portlet:namespace/>dateRange").is(":checked"))
	  {
	   dateValue=$("#<portlet:namespace/>from").val() + "-"+ $("#<portlet:namespace/>to").val();
	  } 
	  
	  $("#<portlet:namespace/>dateText").val(dateValue);
	}
	
	
	function toggleViewSettings(vsId)
	{
		
		//get the position of the placeholder element
	   var vsPos   = $(vsId).offset();
	   var vsHeight = $(vsId).height();
	   var vsWidth = $(vsId).width();
	   var popupWidth = $("#<portlet:namespace/>view_settings").width();
	   $("#<portlet:namespace/>selectedGroupName").val($("#homePageShowEvent input[name=orderBy]:checked").val());
	   $("#<portlet:namespace/>selectedPerPage").val($("#homePageShowEvent input[name=searchResultsPerPage]:checked").val());
	    //show the menu directly over the placeholder
	   $("#<portlet:namespace/>view_settings").css({  position: "absolute", "left": (vsPos.left - popupWidth + vsWidth) + "px", "top":(vsPos.top + vsHeight)  + "px" });
	   $("#<portlet:namespace/>view_settings").toggle();
	}
	
	
	function applyViewSettings()
	{
		$("#<portlet:namespace/>view_settings").toggle();
		search();
	}
	
	function resetViewSettings()
	{
		$('input:radio[name=orderBy]:nth(1)').prop('checked',true);  //firms/orgs, the 2nd radiobutton
		$('input:radio[name=searchResultsPerPage]:nth(2)').prop('checked',true);  //100, the 3rd radiobutton
		//$("#view_settings").toggle(); // dont close the popup, give a chance to end user to know what the default values are let him click apply
		//search();
	}
	
	function cancelViewSettings()
	{
		var groupName = $("#<portlet:namespace/>selectedGroupName").val();
		var pageNo = $("#<portlet:namespace/>selectedPerPage").val();
		
		$('input:radio[name=orderBy]').each(function() {
			$('input:radio[name=orderBy]').attr('checked',false);
		});
		
		$('input:radio[name=searchResultsPerPage]').each(function() {
			$('input:radio[name=searchResultsPerPage]').attr('checked',false);
		});
		// Reset previous value for group name and records per page , while click on Cancel button
		$('input:radio[name=orderBy][value="' + groupName + '"]').prop('checked', 'checked');
		$('input:radio[name=searchResultsPerPage][value="' + pageNo + '"]').prop('checked', 'checked');
		
		$("#<portlet:namespace/>selectedGroupName").val(groupName);
		$("#<portlet:namespace/>selectedPerPage").val(pageNo);
		   
		$("#<portlet:namespace/>view_settings").toggle();
		
	}
	
	function changeOnWatchList()
	{
		
		var allWatchListCounter = 0;
		 $('#<portlet:namespace/>individualfirmsWatchList input[type=checkbox]:checked').each(function() {
			 allWatchListCounter++;
		    });
			
			$(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists ('+allWatchListCounter+') Selected');
			
			if(allWatchListCounter == 0)
			{
				
				$('#<portlet:namespace/>all_firm_org').prop("checked", false);
				$('#<portlet:namespace/>firm_watchlist').prop("checked", false);
				$('#<portlet:namespace/>all_firm').prop("checked", false);
				$('#<portlet:namespace/>firm_individual').prop("checked", false);
				$('#<portlet:namespace/>all_org').prop("checked", false);
				$('#<portlet:namespace/>org_individual').prop("checked", false);
				$('#<portlet:namespace/>individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
				$('#<portlet:namespace/>individualfirms input[type=checkbox]:checked').removeAttr('checked');
				$('#<portlet:namespace/>individualfirmsOrganization input[type=checkbox]:checked').removeAttr('checked');
			}
		else
			{
				$('#<portlet:namespace/>firm_watchlist').prop("checked", true);
				$('#<portlet:namespace/>firm_individual').prop("checked", false);
				$('#<portlet:namespace/>all_firm_org').prop("checked", false);
				$('#<portlet:namespace/>all_firm').prop("checked", false);
				$('#<portlet:namespace/>org_individual').prop("checked", false);
				$('#<portlet:namespace/>individualfirms input[type=checkbox]:checked').removeAttr('checked');
				$('#<portlet:namespace/>individualfirmsOrganization input[type=checkbox]:checked').removeAttr('checked');
				$("#<portlet:namespace/>firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms (0) Selected');
				$(".individualfirms-Events-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="<portlet:namespace/>selectAllOrganizations" /> Organizations (0) Selected');
			}
			
	}
	
	function refreshCounters()
	{
		
		var allWatchListCounter = 0;
		var allFirmsCounterTemp = 0;
		var allOrgsCounterTemp = 0;
		var allWatchListSelectedCounter = 0;
		var counter = 0;
		var orgsCounter = 0;
		var watchlistCounter = 0;
		
		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').each(function() {
		 	allWatchListCounter++;
	    });
		
		$(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists ('+allWatchListCounter+') Selected');
	
		var allFirmsCounter = 0;
		$('#<portlet:namespace/>individualfirms input[type=checkbox]:checked').each(function() {
		 allFirmsCounter++;
		});
		$("#<portlet:namespace/>firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms ('+allFirmsCounter+') Selected');
		
		var allOrgsCounter = 0;
		$('#<portlet:namespace/>individualfirmsOrganization input[type=checkbox]:checked').each(function() {
			 allOrgsCounter++;
	    });
				
		$(".individualfirms-Events-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="<portlet:namespace/>selectAllOrganizations" /> Organizations ('+allOrgsCounter+') Selected');
		
		var allLocationCounter = 0;
		 $('#<portlet:namespace/>allOtherLocationsDiv input[class=allEventsLocationsCheckBox]:checked').each(function() {
			 allLocationCounter++;
		    });
		$("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="<portlet:namespace/>selectAllLocations" /> Locations ('+allLocationCounter+') Selected');

		var allPracticeCounter = 0;
		$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:checked').each(function() {
			 allPracticeCounter++;
		    });
			
		$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="<portlet:namespace/>selectAllPracticeAreas" /> PracticeAreas ('+allPracticeCounter+') Selected');
	
		$('#<portlet:namespace/>individualfirms input[type=checkbox]:checked').each(function() {
			 allFirmsCounterTemp++;
		});
		 
		$('#<portlet:namespace/>individualfirms input[type=checkbox]:not(:checked)').each(function() {
					 	counter++;
		});
			 
		if(counter == 0)
		{
			 $("#<portlet:namespace/>firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms" checked="true" />  Firms ('+allFirmsCounterTemp+') Selected');
		}
		else
		{
			 $("#<portlet:namespace/>firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms" />  Firms ('+allFirmsCounterTemp+') Selected');
		}
			 
		$('#<portlet:namespace/>individualfirmsOrganization input[type=checkbox]:checked').each(function() {
			 allOrgsCounterTemp++;
		});
			 
		$('#<portlet:namespace/>individualfirmsOrganization input[type=checkbox]:not(:checked)').each(function() 
		{
		 	orgsCounter++;
		});
				 
		if(orgsCounter == 0)
		{
			 $("#<portlet:namespace/>organizationsCounter").html('<input type="checkbox" value="selectAllOrganizations" id="<portlet:namespace/>selectAllOrganizations" checked="true" />  Organizations ('+allOrgsCounterTemp+') Selected');
		}
		else
		{
			 $("#<portlet:namespace/>organizationsCounter").html('<input type="checkbox" value="selectAllOrganizations" id="<portlet:namespace/>selectAllOrganizations" />  Organizations ('+allOrgsCounterTemp+') Selected');
		}
				 
		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').each(function() {
			 allWatchListSelectedCounter++;
		});
				 
		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:not(:checked)').each(function() {
		 	watchlistCounter++;
		});
					 
		if(watchlistCounter == 0 && allWatchListSelectedCounter != 0)
		{
			 $(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" checked="true"/> Watchlists ('+allWatchListSelectedCounter+') Selected');
		}
		else
		{
			 $(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList"/> Watchlists ('+allWatchListSelectedCounter+') Selected');
		}
					 
	}
					
	function changeOnIndividualFirms()
	{
		var allFirmsCounter = 0;
		var allNotSelectedFirms = 0;
		 $('#<portlet:namespace/>individualfirms input[type=checkbox]:checked').each(function() {
			 allFirmsCounter++;
		    });
		 
		 $('#<portlet:namespace/>individualfirms input[type=checkbox]:not(:checked)').each(function() {
			 allNotSelectedFirms++;
			});
			
		 if(allNotSelectedFirms == 0)
		  {
			$("#<portlet:namespace/>firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms" checked="true"/> Firms ('+allFirmsCounter+') Selected');
		  }
		 else
		  {
			$("#<portlet:namespace/>firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms ('+allFirmsCounter+') Selected');
		  }
			if(allFirmsCounter == 0)
			{
				$('#<portlet:namespace/>firm_watchlist').prop("checked", false);
				$('#<portlet:namespace/>firm_individual').prop("checked", false);
				$('#<portlet:namespace/>all_firm').prop("checked", false);
				$('#<portlet:namespace/>org_individual').prop("checked", false);
				$('#<portlet:namespace/>individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
				$('#<portlet:namespace/>individualfirms input[type=checkbox]:checked').removeAttr('checked');
				$('#<portlet:namespace/>individualfirmsOrganization input[type=checkbox]:checked').removeAttr('checked');

				var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
				
				$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]').each(function() {
					 if(this.value == watchListId)
						{
							$(this).prop("checked", true);
						}
				});
			}
		else
			{
				$('#<portlet:namespace/>firm_individual').prop("checked", true);
				$('#<portlet:namespace/>firm_watchlist').prop("checked", false);
				$('#<portlet:namespace/>all_firm').prop("checked", false);
				$('#<portlet:namespace/>org_individual').prop("checked", false);
				$('#<portlet:namespace/>individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
				$('#<portlet:namespace/>individualfirmsOrganization input[type=checkbox]:checked').removeAttr('checked');
				$(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0) Selected');
				$(".individualfirms-Events-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="<portlet:namespace/>selectAllOrganizations" /> Organizations (0) Selected');
				
				$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
				
			}
	}
	
	
	function changeOnIndividualOrgs()
	{
		var allOrgsCounter = 0;
		var allOrgsNotSelectedCounter = 0;
		 $('#<portlet:namespace/>individualfirmsOrganization input[type=checkbox]:checked').each(function() {
			 allOrgsCounter++;
		    });
			
		 $('#<portlet:namespace/>individualfirmsOrganization input[type=checkbox]:not(:checked)').each(function() {
			 allOrgsNotSelectedCounter++;
		 });	
		 
		 if(allOrgsNotSelectedCounter == 0)
		 {
			 $(".individualfirms-Events-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="<portlet:namespace/>selectAllOrganizations" checked="true" /> Organizations ('+allOrgsCounter+') Selected');
		 }
		 else
		 {
			 $(".individualfirms-Events-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="<portlet:namespace/>selectAllOrganizations" /> Organizations ('+allOrgsCounter+') Selected');
		 }
			
			if(allOrgsCounter == 0)
			{
				$('#<portlet:namespace/>firm_watchlist').prop("checked", false);
				$('#<portlet:namespace/>firm_individual').prop("checked", false);
				$('#<portlet:namespace/>all_firm').prop("checked", false);
				$('#<portlet:namespace/>org_individual').prop("checked", false);
				$('#<portlet:namespace/>individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
				$('#<portlet:namespace/>individualfirms input[type=checkbox]:checked').removeAttr('checked');
				$('#<portlet:namespace/>individualfirmsOrganization input[type=checkbox]:checked').removeAttr('checked');
				
				var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
				
				$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]').each(function() {
					 if(this.value == watchListId)
						{
							$(this).prop("checked", true);
						}
				});
			}
		else
			{
				$('#<portlet:namespace/>firm_watchlist').prop("checked", false);
				$('#<portlet:namespace/>firm_individual').prop("checked", false);
				$('#<portlet:namespace/>all_firm').prop("checked", false);
				$('#<portlet:namespace/>org_individual').prop("checked", true);
				$('#<portlet:namespace/>individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
				$('#<portlet:namespace/>individualfirms input[type=checkbox]:checked').removeAttr('checked');
				$("#<portlet:namespace/>firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms (0) Selected');
				$(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0) Selected');
			}
		
	}
	
	function <portlet:namespace/>changeOnIndividualLocations()
	{
		var allLocationCounter = 0;
		var loclistCounter = 0;
		
		$('#<portlet:namespace/>allOtherLocationsDiv input[class=allEventsLocationsCheckBox]:checked').each(function() {
			allLocationCounter++;
		});
		 $('#<portlet:namespace/>allOtherLocationsDiv input[type=checkbox]:not(:checked)').each(function() {
			loclistCounter++;
		});	
		 
		 if(loclistCounter == 0)
		 {
			 $("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="<portlet:namespace/>selectAllLocations" checked="true"/> Locations ('+allLocationCounter+') Selected');
		 }
		 else
		 {
			 $("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="<portlet:namespace/>selectAllLocations" /> Locations ('+allLocationCounter+') Selected');
		 }
		
			
		if(allLocationCounter == 0)
			{
				$('#<portlet:namespace/>loc_individual').prop("checked", false);
				$('#<portlet:namespace/>loc_all').prop("checked", true);
				$('#<portlet:namespace/>selectedLocation').val('<%=ALMConstants.ALL_LOCATIONS %>');
			}
		else
			{
			$('#<portlet:namespace/>loc_all, #<portlet:namespace/>loc_inperson, #<portlet:namespace/>loc_webinars').prop("checked", false);
				$('#<portlet:namespace/>loc_individual').prop("checked", true);
			}
	}
	
	function changeOnIndivudualPracticeAreas()
	{
		var allPracticeCounter = 0;
		var practicelistCounter = 0;
		
		$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:checked').each(function() {
			allPracticeCounter++;
		});
		
		 $('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:not(:checked)').each(function() 
			{
				practicelistCounter++;
			});
			
		if(allPracticeCounter == 0)
		{
			$('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", false);
			$('.allEventsPracticeArea').prop("checked", true);
			$('#<portlet:namespace/>selectedPracticeArea').val('<%=ALMConstants.ALL_PRACTICE_AREAS %>');
		}
		else
		{
			$('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", true);
			$('.allEventsPracticeArea').prop("checked", false);
		}
			
		if(practicelistCounter == 0)
		{
			$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="<portlet:namespace/>selectAllPracticeAreas" checked="true"/> PracticeAreas ('+allPracticeCounter+') Selected');
		}
		else
		{
			 $("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="<portlet:namespace/>selectAllPracticeAreas" /> PracticeAreas ('+allPracticeCounter+') Selected');
		}
			
	}
	
	function clearDate()
	{
		$('#<portlet:namespace/>popupDate').find("option").attr("selected", false);
		$('#<portlet:namespace/>popupDate').find('input[type=text]').val('');
		$('#<portlet:namespace/>popupDate').find('input[type=radio]:checked').removeAttr('checked');				
		$("#<portlet:namespace/>dateText").val('<%=ALMConstants.LAST_WEEK%>');	
		$("#<portlet:namespace/>period").prop("checked", true); 
		$("#<portlet:namespace/>datePeriodSelect").val('<%=ALMConstants.LAST_WEEK%>');	
		$("#<portlet:namespace/>from").datepicker('enable');
		$("#<portlet:namespace/>to").datepicker('enable');
		applyDate();
	}
	
	function <portlet:namespace/>clearFirms()
	{
		$('#<portlet:namespace/>popup').find("option").attr("selected", false);
		$('#<portlet:namespace/>popup').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>popup').find('input[type=radio]:checked').removeAttr('checked');
		
		$("#<portlet:namespace/>firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms (0) Selected');
		$(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0) Selected');
		$(".individualfirms-Events-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="<portlet:namespace/>selectAllOrganizations" /> Organizations (0) Selected');
		<portlet:namespace/>applyFirms();
		changeOnWatchList();
	}
	
	function <portlet:namespace/>clearLocations()
	{
		$('#<portlet:namespace/>popupLocation').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>popupLocation').find('input[type=radio]:checked').removeAttr('checked');
		$("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="<portlet:namespace/>selectAllLocations" /> Locations (0) Selected');
		$('#<portlet:namespace/>loc_all').prop("checked", true);
		<portlet:namespace/>applyLocations();
	}
	
	
	function <portlet:namespace/>clearPracticeAreas()
	{
		$('#<portlet:namespace/>popupPracticeArea').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>popupPracticeArea').find('input[type=radio]:checked').removeAttr('checked');
		$('#<portlet:namespace/>allOtherPracticeArea input').find('input[type=checkbox]:checked').removeAttr('checked');
		$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="<portlet:namespace/>selectAllPracticeAreas" /> Practice Area (0) Selected');
		$('.allEventsPracticeArea').prop('checked', true);	
		<portlet:namespace/>applyPracticeArea();
	}
	
	function clearKeywords()
	{
		//keyword reset
		$( "#<portlet:namespace/>keywords" ).val("");
	}
	
	function <portlet:namespace/>clearAll()
	{
		<portlet:namespace/>clearFirms();
		<portlet:namespace/>clearLocations();
		<portlet:namespace/>clearPracticeAreas();
		clearKeywords();
		clearDate();
	}
	
	function setPage(goToPage)
	{
		// Sets the page number to the one selected by the user
		// and fires an AJAX submit to refresh with the new page
		// Does NOT tinker with the sort settings
		$("#<portlet:namespace/>goToPage").val(goToPage);
		ajaxSubmit();
	}
	
	function sortResults(sortColumn)
	{
		// Changes the sort order to the new column selected by
		// the user along with the sort direction (ascending)
		// Also RESETS the page to 1		
		var lastSortColumn  =  $("#<portlet:namespace/>sortColumn").val();		
		if(lastSortColumn==sortColumn)
		{
			var lastSortOrder= $("#<portlet:namespace/>sortOrder").val();
			if(lastSortOrder=="asc")
			{
				$("#<portlet:namespace/>sortOrder").val("desc");
			}
			else if(lastSortOrder=="desc")
			{
				$("#<portlet:namespace/>sortOrder").val("asc");
			}
			else
			{
				$("#<portlet:namespace/>sortOrder").val("asc");
			}
		}
		else
		{
			$("#<portlet:namespace/>sortOrder").val("asc");
			
		}
		
		$("#<portlet:namespace/>sortColumn").val(sortColumn);
		$("#<portlet:namespace/>goToPage").val(1);
		search();
	}
	
	function sortDesc(sortColumn)
	{
		// Changes the sort order to the new column selected by
		// the user along with the sort direction (descending)
		// Also RESETS the page to 1
		$("#<portlet:namespace/>sortColumn").val(sortColumn);
		$("#<portlet:namespace/>sortOrder").val("desc");
		$("#<portlet:namespace/>goToPage").val(1);
		ajaxSubmit();
	}
	
	/**
	* Single point to fire search action
	*/
	function search()
	{
		
		
		
		var isC2VPage = ${isC2VPage};  // its always gauranteed it either evaluates to true or false, but not empty
		var isHomePage = ${isHomePage};
		
		if(isC2VPage)
		{
			
			var actionURL = "${clickToViewURL}" +  "&drilldownFirmName=blah"; // we care the existence of param not its value;
			$('#eventSearchModelBean').attr("action", actionURL);
			//$('#eventSearchModelBean').attr("name", "eventSearchModelBean_c2v");
		}
		
		if(isHomePage)
		{
			<portlet:namespace/>ajaxPersist(); // ajaxSubmit for Charts on HomePage
		}
		else
		{
			Liferay.Portlet.showBusyIcon("#bodyId","Loading..."); // show Busy Icon
			
			$('#eventSearchModelBean').submit(); // normal form Submit 
		}
		

	}
	
	/**
	* Persist the search Criteria for Chart on Home Page
	*/
	function <portlet:namespace/>ajaxPersist()
	{
		window.parent.Liferay.Portlet.showBusyIcon("#bodyId","Loading...");
		 $.ajax({
			    url: "${persistSearchCriteriaForChartURL}",
			    method: "POST",
			    traditional: true,
			    data: $('#eventSearchModelBean').serializeObject(),
			    success: function(data){
			    	
			    	window.parent.Liferay.Portlet.hideBusyIcon("#chartSearchPortletPopupId"); // hide popup loading icon
			    	window.parent.location.href = "home"; // refresh home page
			    	window.parent.Liferay.Portlet.showBusyIcon("#bodyId","Loading..."); // show the loading icon
			    	window.parent.AUI().DialogManager.closeByChild('#chartSearchPortletPopupId'); // close the popup
			    	
			    //	Liferay.Portlet.refreshPortlet('#p_p_id_${chartPortletId}_'); 
			        
			    },
			    error: function(jqXHR, textStatus, errorThrown) {
			    	window.parent.Liferay.Portlet.hideBusyIcon("#chartSearchPortletPopupId");
			         	 alert("error:" + textStatus + " - exception:" + errorThrown);
			   		}
			    }); 
	}

	
 	function ajaxSubmit()
	{
		 search();
	}
</script>
		
<script>

$.widget( "custom.catcomplete", $.ui.autocomplete, {
	_renderMenu: function( ul, items ) {
		var that = this,
		currentCategory = "";
		$.each( items, function( index, item ) {
			if ( item.category != currentCategory ) {
			ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
			currentCategory = item.category;
			}
	  that._renderItemData( ul, item );
	});
}
});

$(function() {

	//var practiceData = ${practiceJson};
	//var locationData = ${allOtherLocations};

	function split( val ) {
		return val.split( /,\s*/ );
	}

	function extractLast( term ) {
		return split( term ).pop();
	}

});

</script>

<script>

$(document).ready(function(){

	$("#eventSearchModelBean input, #eventSearchModelBean select, #eventSearchModelBean span").each(function(){
		
		var idVal = $(this).attr("id");
		 $(this).attr("id","<portlet:namespace/>" + idVal );
	});

	// Show or hide popup
	$('#<portlet:namespace/>hide, #<portlet:namespace/>popup').click(function(e){	
		 $("#<portlet:namespace/>popupPracticeArea").hide();
		 $("#<portlet:namespace/>popupFirmSize").hide();
		 $("#<portlet:namespace/>popupLocation").hide();
		 $("#<portlet:namespace/>popupDate").hide();
	     e.stopPropagation();	   
	});  
	// Show or hide PracticeArea popup
	 $('#<portlet:namespace/>practiceAreaId, #<portlet:namespace/>popupPracticeArea').click(function(e){
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupFirmSize").hide();
		 $("#<portlet:namespace/>popupLocation").hide();
		 $("#<portlet:namespace/>popupDate").hide();
	     e.stopPropagation();   
	});
	// Show or hide Firm popup
	$('#<portlet:namespace/>firmSizeId, #<portlet:namespace/>popupFirmSize').click(function(e){
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupPracticeArea").hide();
		 $("#<portlet:namespace/>popupLocation").hide();
		 $("#<portlet:namespace/>popupDate").hide();
	     e.stopPropagation();   
	});
	
	// Show or hide Location popup
	$('#<portlet:namespace/>locationId, #<portlet:namespace/>popupLocation').click(function(e){
	
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupPracticeArea").hide();
		 $("#<portlet:namespace/>popupFirmSize").hide();
		 $("#<portlet:namespace/>popupDate").hide();
	     e.stopPropagation();   
	});

 	  $("input[id='<portlet:namespace/>allOtherLocation']").click(function(){
 		$('#<portlet:namespace/>allOtherLocationsDiv input[type=checkbox]:checked').removeAttr('checked');
 		$("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="<portlet:namespace/>selectAllLocations" /> Locations (0) Selected');
		});  
	
	 $('.rivaledgeListEventsAMLAW_100').bind('click', function (event) {
			
		 $('.rivaledgeListEventsAMLAW_100').prop("checked", true);
		 $('.rivaledgeListEventsAMLAW_200').prop("checked", false);
		 $('.rivaledgeListEventsNLJ_250').prop("checked", false);
		 $('#<portlet:namespace/>firm_watchlist').prop("checked", true);
		 $('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
	 	 $(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0 Selected)');
	 	
	 	$("#<portlet:namespace/>firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms (0) Selected');
		$(".individualfirms-Events-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="<portlet:namespace/>selectAllOrganizations" /> Organizations (0) Selected');
		$('#<portlet:namespace/>individualfirms input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>individualfirmsOrganization input[type=checkbox]:checked').removeAttr('checked');

		$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.AMLAW_100 %>');
		$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.AMLAW_100 %>');
		$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
		$('#<portlet:namespace/>individualfirms input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
		
	});
	
	$('.rivaledgeListEventsAMLAW_200').bind('click', function (event) {
		
		 $('.rivaledgeListEventsAMLAW_100').prop("checked", false);
		 $('.rivaledgeListEventsAMLAW_200').prop("checked", true);
		 $('.rivaledgeListEventsNLJ_250').prop("checked", false);
		 $('#<portlet:namespace/>firm_watchlist').prop("checked", true);
		 $('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
	 	 $(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0 Selected)');
		 	
	 	 $("#<portlet:namespace/>firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms (0) Selected');
			$(".individualfirms-Events-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="<portlet:namespace/>selectAllOrganizations" /> Organizations (0) Selected');
			$('#<portlet:namespace/>individualfirms input[type=checkbox]:checked').removeAttr('checked');
			
			$('#<portlet:namespace/>individualfirmsOrganization input[type=checkbox]:checked').removeAttr('checked');
			$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.AMLAW_200 %>');
			$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.AMLAW_200 %>');
			$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
			$('#<portlet:namespace/>individualfirms input[type=checkbox]:checked').removeAttr('checked');
			$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');

	});
	
	$('.rivaledgeListEventsNLJ_250').bind('click', function (event) {
		
		 $('.rivaledgeListEventsAMLAW_100').prop("checked", false);
		 $('.rivaledgeListEventsAMLAW_200').prop("checked", false);
		 $('.rivaledgeListEventsNLJ_250').prop("checked", true);
		 $('#<portlet:namespace/>firm_watchlist').prop("checked", true);
		 $('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
	 	 $(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0 Selected)');
		 	
	 	 $("#<portlet:namespace/>firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms (0) Selected');
			$(".individualfirms-Events-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="<portlet:namespace/>selectAllOrganizations" /> Organizations (0) Selected');
			$('#<portlet:namespace/>individualfirms input[type=checkbox]:checked').removeAttr('checked');
			$('#<portlet:namespace/>individualfirmsOrganization input[type=checkbox]:checked').removeAttr('checked');

			$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.NLJ_250 %>');
			$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.NLJ_250 %>');
			$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
			$('#<portlet:namespace/>individualfirms input[type=checkbox]:checked').removeAttr('checked');
			$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
	});
	
	//Firm Size's toggle off
	$('#<portlet:namespace/>firmSizeId').click(
			function () {
			    $("#<portlet:namespace/>popupFirmSize").stop().slideToggle(500);    
		}); 
	//Practice Area's toggle off
	$('#<portlet:namespace/>practiceAreaId').click(
			function () {
			    $("#<portlet:namespace/>popupPracticeArea").stop().slideToggle(500);    
		});
	//Location's toggle off
	 $('#<portlet:namespace/>locationId').click(
		    function () {
		        $("#<portlet:namespace/>popupLocation").stop().slideToggle(500);    
		}); 
	
	$('#<portlet:namespace/>fromdateError').hide();
	$('#<portlet:namespace/>todateError').hide();
	$('#<portlet:namespace/>dateValidError').hide();
	
	$("#<portlet:namespace/>popupDate").hide(); 

	$('#<portlet:namespace/>contentTypeId, #<portlet:namespace/>popupContentType').click(function(e){
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupDate").hide();
		 $("#<portlet:namespace/>popupPracticeArea").hide();
	    e.stopPropagation();   
	});

	$('#<portlet:namespace/>datenone, #<portlet:namespace/>popupDate').click(function(e){
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupContentType").hide();
		 $("#<portlet:namespace/>popupPracticeArea").hide();
		 $("#<portlet:namespace/>popupLocation").hide();
	   e.stopPropagation();   
	});  
	
	$('#ui-datepicker-div').click(function(e){
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupContentType").hide();
		 $("#<portlet:namespace/>popupPracticeArea").hide();
	   e.stopPropagation();   
	});


$(document).click(function(){
	var isHidePopUpDate = true;
    $("#<portlet:namespace/>popup").hide(); 
    $("#<portlet:namespace/>popupContentType").hide();
	
	var isDatePickerHidden = $( "#ui-datepicker-div" ).is(':visible');
	if(isDatePickerHidden){
		var isHide = $("#<portlet:namespace/>popupDate").is(':visible');
		if(isHide){
			isHidePopUpDate = false;
		}
	}
	
	if(isHidePopUpDate){
		$("#<portlet:namespace/>popupDate").hide();
	}
    $("#<portlet:namespace/>popupPracticeArea").hide();
	$("#<portlet:namespace/>popup").hide(); 
	$("#<portlet:namespace/>popupPracticeArea").hide();
	$("#<portlet:namespace/>popupFirmSize").hide();
	$("#<portlet:namespace/>popupLocation").hide();
	
});

var date = new Date();
currentDate = (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear();
date.setDate(date.getDate() - 7);
lastWeekDate = (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear();

$('#<portlet:namespace/>btnAdd').click(function(){
	$('.filtersPage').hide();
	$('#<portlet:namespace/>additional').show();
})
$('#<portlet:namespace/>btnSave').click(function(){
	$('.filtersPage').show();
	$('#<portlet:namespace/>additional').hide();
});

	    	
$('#<portlet:namespace/>hide').click(
    function () {
        $("#<portlet:namespace/>popup").stop().slideToggle(500); 
    });
    
	$('#<portlet:namespace/>contentTypeId').click(
    function () {
        //show its submenu        
        $("#<portlet:namespace/>popupContentType").stop().slideToggle(500);    
    });

	$('#<portlet:namespace/>datenone').click(
	function () {
	    //show its submenu
	    $("#<portlet:namespace/>popupDate").stop().slideToggle(500);    
	}); 

// Function for Event search based on selected criteria like Firms,Practice Area,Location, Dates and Keywords
 $("#<portlet:namespace/>applySearch").click(function() {	
	 if($('#<portlet:namespace/>selectedLocation').val() == '')
	 {
		$('#<portlet:namespace/>loc_all').prop("checked", true);
		$('#<portlet:namespace/>selectedLocation').val('<%=ALMConstants.ALL_LOCATIONS %>');
	 }
	 if($('#<portlet:namespace/>selectedPracticeArea').val() == '')
	 {
		$('.allEventsPracticeArea').prop("checked", true);
		$('#<portlet:namespace/>selectedPracticeArea').val('<%=ALMConstants.ALL_PRACTICE_AREAS %>');
	 }		
	
	 $("#<portlet:namespace/>goToPage").val(1);
	search();
}); 

	$("input[id='<portlet:namespace/>allfirms']").change(function(){
	var checked = $("#<portlet:namespace/>allfirms").is(":checked");
		if(checked)
		{   
		 $("#<portlet:namespace/>allOtherFirmDiv input").attr("disabled", true);
		 $('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
		 $('#<portlet:namespace/>popup option:selected').removeAttr("selected");
		 $('#<portlet:namespace/>ApplyFirm').removeAttr("disabled");
		 $('#<portlet:namespace/>ApplyFirm').removeClass("ui-state-disabled");
		}
	});
	
	$("input[id='<portlet:namespace/>allOtherFirm']").change(function(){
	var checked = $("#<portlet:namespace/>allOtherFirm").is(":checked");
	if(checked)
	{   
	 $("#<portlet:namespace/>allOtherFirmDiv input").removeAttr('disabled');
	   $('#<portlet:namespace/>popup option:selected').removeAttr("selected");
	}else
	{
	 $("#<portlet:namespace/>allOtherFirmDiv input").attr("disabled", true);
	}
	
	});
	
	$("input[id='<portlet:namespace/>allFirmsCheckBox']").change(function(){
		var lengthofCheckBoxes = $("input[id='<portlet:namespace/>allFirmsCheckBox']:checked").length;
		if(lengthofCheckBoxes>0)
		{
			$('#<portlet:namespace/>ApplyFirm').removeAttr("disabled");
			$('#<portlet:namespace/>ApplyFirm').removeClass("ui-state-disabled");
		}
	});
	
	/* Date period selection logic*/
	$("#<portlet:namespace/>datePeriodSelect").change(function() {
		$("#<portlet:namespace/>period").prop("checked", true);
		$("#<portlet:namespace/>dateRange").removeAttr("checked");
		applyDate();
	});

	$("#<portlet:namespace/>from, #<portlet:namespace/>to").change(function() {
		 $("#<portlet:namespace/>dateRange").prop("checked", true);
		 $("#<portlet:namespace/>period").removeAttr("checked");
		 applyDate();
	});
	
	 $('.allFirmsCheckBoxEventsWatchList').click(function() {
		 
	 var allWatchListCounter = 0;	
	 $('#<portlet:namespace/>firm_watchlist').prop("checked", true);
	 $('#<portlet:namespace/>org_individual').prop("checked", false);
	 
	 $('#<portlet:namespace/>all_firm_org').prop("checked", false);
	 $('#<portlet:namespace/>all_firm').prop("checked", false);
	 $('#<portlet:namespace/>firm_individual').prop("checked", false);
	 $('#<portlet:namespace/>all_org').prop("checked", false);
	 $('#<portlet:namespace/>org_individual').prop("checked", false);
	 
	 $('#<portlet:namespace/>individualfirms').find('input[type=checkbox]:checked').removeAttr('checked');
	 $('#<portlet:namespace/>individualfirmsOrganization').find('input[type=checkbox]:checked').removeAttr('checked');
	 $("#<portlet:namespace/>firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms (0 Selected)');
	 $(".individualfirms-Events-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="<portlet:namespace/>selectAllOrganizations" /> Organizations (0 Selected)');
	 $('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
	 
	 
	 $('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').each(function() {
		 allWatchListCounter++;
	 });
	
	 if(allWatchListCounter == 0)
	 {
		$("#<portlet:namespace/>Firmstext").val('AmLaw 100');
		$("#<portlet:namespace/>selectedFirms").val('AmLaw 100');
		$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
		$("#<portlet:namespace/>firm_watchlist").prop("checked", true);
		$(".rivaledgeListEventsAMLAW_100").prop("checked", true);
	 }
	 
	});
  
 $('.individualfirms-Events-Watchlist').click(function() {
		var checked = $("#<portlet:namespace/>selectAllWatchList").is(":checked");
		var allWatchListCounter = 0;
		var watchlistCounter = 0;
		var isFlag = true;
		
		if(checked){   
			 $('#<portlet:namespace/>individualfirmsWatchListDIV .allFirmsCheckBoxEventsWatchList').prop('checked', true);
			 $('#<portlet:namespace/>selectAllWatchList').prop("checked", true);
			 $('#<portlet:namespace/>firm_watchlist').prop("checked", true);
			 $('#<portlet:namespace/>all_firm_org').prop("checked", false);
			 $('#<portlet:namespace/>all_firm').prop("checked", false);
			 $('#<portlet:namespace/>firm_individual').prop("checked", false);
			 $('#<portlet:namespace/>all_org').prop("checked", false);
			 $('#<portlet:namespace/>org_individual').prop("checked", false);
			 $('#<portlet:namespace/>individualfirms').find('input[type=checkbox]:checked').removeAttr('checked');
			 $('#<portlet:namespace/>individualfirmsOrganization').find('input[type=checkbox]:checked').removeAttr('checked');
			 $('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
			 
		}else{
			 $('#<portlet:namespace/>individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
			 $('#<portlet:namespace/>selectAllWatchList').prop("checked", false);
			 
			 var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
				
				$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]').each(function() {
					 if(this.value == watchListId)
						{
							$(this).prop("checked", true);
							isFlag = false;
						}
				});
				$('#<portlet:namespace/>firm_watchlist').prop("checked", true);
				if(isFlag == true)
				{
					$(".rivaledgeListEventsAMLAW_100").prop("checked", true);
				}
		}
		
		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').each(function() {
			 allWatchListCounter++;
		});
		 
		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:not(:checked)').each(function() {
			 watchlistCounter++;
		});
			 
		if(watchlistCounter == 0)
		{
			$(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" checked="true"/> Watchlists ('+allWatchListCounter+') Selected');
		}
		else
		{
			$(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList"/> Watchlists ('+allWatchListCounter+') Selected');
		}
	});

 
 $('#<portlet:namespace/>firmsCounter').click(function() {
	 
		var checked = $("#<portlet:namespace/>selectAllFirms").is(":checked");
		var allFirmsListCounter = 0;
		var isFlag = true;
		if(checked)
		{   
			 $('#<portlet:namespace/>individualfirms .allFirmsEventsCheckBoxFirms').prop('checked', true);
		}
		else
		{
			$('#<portlet:namespace/>individualfirms .allFirmsEventsCheckBoxFirms').prop('checked', false);
		}
		
		 $('#<portlet:namespace/>individualfirms input[type=checkbox]:checked').each(function() {
			 allFirmsListCounter++;
		 });
		 
		 if(allFirmsListCounter == 0)
			 {
				 $("#<portlet:namespace/>firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms ('+allFirmsListCounter+') Selected');
				 $('#<portlet:namespace/>firm_individual').prop("checked", false);
				 $('#<portlet:namespace/>selectAllFirms').prop("checked", false);
				 
					var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
					
					$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]').each(function() {
						 if(this.value == watchListId)
							{
								$(this).prop("checked", true);
								$(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (1) Selected');
								isFlag = false;
							}
					});
					$('#<portlet:namespace/>firm_watchlist').prop("checked", true);
					if(isFlag == true)
					{
						$(".rivaledgeListEventsAMLAW_100").prop("checked", true);
					}
			 }
		 else
			 {
			 
				 $("#<portlet:namespace/>firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms ('+allFirmsListCounter+') Selected');
				 $('#<portlet:namespace/>firm_individual').prop("checked", true);
				 $('#<portlet:namespace/>selectAllFirms').prop("checked", true);
				 $('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
				 $('#<portlet:namespace/>firm_watchlist').prop("checked", false);
				 $(".rivaledgeListEventsAMLAW_100").prop("checked", false);
				 $(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0) Selected');
				 $('#<portlet:namespace/>individualfirmsOrganization input[type=checkbox]:checked').removeAttr('checked');
				 $(".individualfirms-Events-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="<portlet:namespace/>selectAllOrganizations" /> Organizations (0) Selected');
			 }
		 
		
	});
 
 $('#<portlet:namespace/>locationsCounter').click(function() {
	 
		var checked = $("#<portlet:namespace/>selectAllLocations").is(":checked");
		var allLocationsListCounter = 0;
		
		if(checked)
		{   
			 $('#<portlet:namespace/>allOtherLocationsDiv .allEventsLocationsCheckBox').prop('checked', true);
		}
		else
		{
			$('#<portlet:namespace/>allOtherLocationsDiv .allEventsLocationsCheckBox').prop('checked', false);
		}
		
		 $('#<portlet:namespace/>allOtherLocationsDiv input[type=checkbox]:checked').each(function() {
			 allLocationsListCounter++;
		 });
		 
		 if(allLocationsListCounter == 0)
			 {
				 $("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="<portlet:namespace/>selectAllLocations" /> Locations ('+allLocationsListCounter+') Selected');
				 $('#<portlet:namespace/>loc_individual').prop("checked", false);
				 $('#<portlet:namespace/>selectAllLocations').prop("checked", false);
				 $('#<portlet:namespace/>loc_all').prop("checked", true);
				 $('#<portlet:namespace/>selectedLocation').val('<%=ALMConstants.ALL_LOCATIONS %>');
				 
			 }
		 else
			 {
				 $("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="<portlet:namespace/>selectAllLocations" /> Locations ('+allLocationsListCounter+') Selected');
				 $('#<portlet:namespace/>loc_individual').prop("checked", true);
				 $('#<portlet:namespace/>selectAllLocations').prop("checked", true);
				 $('#<portlet:namespace/>loc_all').prop("checked", false);
				 $('#<portlet:namespace/>loc_inperson').prop("checked", false);
				 $('#<portlet:namespace/>loc_webinars').prop("checked", false);
			 }
		
	});
 
 $('#<portlet:namespace/>practiceAreaCounter').click(function() {
	 
		var checked = $("#<portlet:namespace/>selectAllPracticeAreas").is(":checked");
		var allPracticeAreasListCounter = 0;
		
		if(checked)
		{   
			 $('#<portlet:namespace/>allOtherPracticeArea #<portlet:namespace/>practiceId').prop('checked', true);
			 $('#<portlet:namespace/>practiceArea1').prop('checked', false);
		}
		else
		{
			$('#<portlet:namespace/>allOtherPracticeArea #<portlet:namespace/>practiceId').prop('checked', false);
		}
		
		 $('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:checked').each(function(){
			 allPracticeAreasListCounter++;
		 });
		 
		 if(allPracticeAreasListCounter == 0)
			 {
				 $("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="<portlet:namespace/>selectAllPracticeAreas" /> Practice Areas ('+allPracticeAreasListCounter+') Selected');
				 $('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", false);
				 $('#<portlet:namespace/>selectAllPracticeAreas').prop("checked", false);
				 $('.allEventsPracticeArea').prop("checked", true);
				 $('#<portlet:namespace/>selectedPracticeArea').val('<%=ALMConstants.ALL_PRACTICE_AREAS %>');

			 }
		 else
			 {
				 $("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="<portlet:namespace/>selectAllPracticeAreas" /> Practice Areas  ('+allPracticeAreasListCounter+') Selected');
				 $('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", true);
				 $('#<portlet:namespace/>selectAllPracticeAreas').prop("checked", true);
			 }
	}); 
	
$('#<portlet:namespace/>organizationsCounter').click(function() {
	 
	var checked = $("#<portlet:namespace/>selectAllOrganizations").is(":checked");
	var allOrgCounter = 0;
	var isFlag = true;
	
	if(checked)
	{   
		 $('#<portlet:namespace/>individualfirmsOrganization .allFirmsEventsCheckBoxOrg').prop('checked', true);
	}
	else
	{
		$('#<portlet:namespace/>individualfirmsOrganization .allFirmsEventsCheckBoxOrg').prop('checked', false);
	}
	
	 $('#<portlet:namespace/>individualfirmsOrganization input[type=checkbox]:checked').each(function() {
		 allOrgCounter++;
	 });
	 
	 if(allOrgCounter == 0)
		 {
			 $("#<portlet:namespace/>organizationsCounter").html('<input type="checkbox" value="selectAllOrganizations" id="<portlet:namespace/>selectAllOrganizations" /> Organizations ('+allOrgCounter+') Selected');
			 $('#<portlet:namespace/>org_individual').prop('checked', false);
			 
				var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
				
				$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]').each(function() {
					 if(this.value == watchListId)
						{
							$(this).prop("checked", true);
							$(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (1) Selected');
							isFlag = false;
						}
				});
				$('#<portlet:namespace/>firm_watchlist').prop("checked", true);
				
				if(isFlag == true)
				{
					$(".rivaledgeListEventsAMLAW_100").prop("checked", true);
				}
		 }
	 else
		 {
			 $("#<portlet:namespace/>organizationsCounter").html('<input type="checkbox" value="selectAllOrganizations" id="<portlet:namespace/>selectAllOrganizations" /> Organizations ('+allOrgCounter+') Selected');
			 $('#<portlet:namespace/>org_individual').prop('checked', true);
			 $('#<portlet:namespace/>selectAllOrganizations').prop('checked', true);
			 $('#<portlet:namespace/>all_firm_org').prop('checked', false);
			 $('#<portlet:namespace/>firm_watchlist').prop('checked', false);
			 $('#<portlet:namespace/>all_firm').prop('checked', false);
			 $('#<portlet:namespace/>firm_individual').prop('checked', false);
			 $('#<portlet:namespace/>all_org').prop('checked', false);
			 $("#<portlet:namespace/>firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms" /> Firms (0) Selected');
			 $('#<portlet:namespace/>individualfirms input[type=checkbox]:checked').removeAttr('checked');
			 $('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
			 $('#<portlet:namespace/>firm_watchlist').prop("checked", false);
			 $(".rivaledgeListEventsAMLAW_100").prop("checked", false);
			 $(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0) Selected');
		 }
	
}); 	
 
$('#<portlet:namespace/>resetAll').bind('click', function (event) {
	<portlet:namespace/>clearAll();
});

	$('#<portlet:namespace/>clearButton').bind('click', function (event) {
		<portlet:namespace/>clearFirms();
	});
	
	$('#<portlet:namespace/>clearButtonContentType').bind('click', function (event) {
		$('#<portlet:namespace/>popupContentType').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>allContentType').prop('checked', true);
		$("#<portlet:namespace/>allOtherContentType input").attr("disabled", true);	
	});
	
	$('#<portlet:namespace/>clearButtonDate').bind('click', function (event) {
		clearDate();
	}); 

	$('#<portlet:namespace/>clearPracticeArea').bind('click', function (event) {
		<portlet:namespace/>clearPracticeAreas();	
	});
	
	$('#<portlet:namespace/>clearButtonLocations').bind('click', function (event) {
		<portlet:namespace/>clearLocations();
	});
	
// Function for showing selecting firms 	
	$('#<portlet:namespace/>ApplyFirm').bind('click', function (event) {
		<portlet:namespace/>applyFirms();
			$("#<portlet:namespace/>popup").toggle();
		});
	
// Function for showing selecting date 
$('#<portlet:namespace/>ApplyDate').bind('click', function (event) {
	var checkDate = applyDate();
	if(!checkDate){
	 $("#<portlet:namespace/>popupDate").toggle();
	}

	});
	
//Function for showing selected Practice Area 
$('#<portlet:namespace/>ApplyPracticeArea').bind('click', function (event) {
	<portlet:namespace/>applyPracticeArea();
	 $("#<portlet:namespace/>popupPracticeArea").toggle();

});

//Function for showing selected Location 
$('#<portlet:namespace/>ApplyLocations').bind('click', function (event) {		
	<portlet:namespace/>applyLocations();
	 $("#<portlet:namespace/>popupLocation").toggle();	
});

//Function for showing selected Location counter
$("input[class='allEventsLocationsCheckBox']").change(function(){
	<portlet:namespace/>changeOnIndividualLocations();
	<portlet:namespace/>applyLocations();
});  

//Function for showing selected PracticeArea counter
$("#<portlet:namespace/>practiceAreaDiv input[type=checkbox]").change(function(){
	changeOnIndivudualPracticeAreas();
	<portlet:namespace/>applyPracticeArea();
});

//Function for showing selected Organizations and their counter for firms drop down
$("input[class='allFirmsEventsCheckBoxOrg']").change(function(){
	changeOnIndividualOrgs();
	<portlet:namespace/>applyFirms();
});
//Function for showing selected firms and their counter for firms drop down
$("input[class='allFirmsEventsCheckBoxFirms']").change(function(){
	changeOnIndividualFirms();
	<portlet:namespace/>applyFirms();
});

//Function for showing selected Watchlist and their counter for firms drop down
$("input[class='allFirmsCheckBoxEventsWatchList']").change(function(){
	 var allWatchListCounter = 0;
	 var watchlistCounter = 0;
	 $('.rivaledgeListEventsAMLAW_100').prop("checked", false);
	 $('.rivaledgeListEventsAMLAW_200').prop("checked", false);
	 $('.rivaledgeListEventsNLJ_250').prop("checked", false);
	 
	 $('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').each(function() {
		 allWatchListCounter++;
	 });
		
	 $('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:not(:checked)').each(function() 
		{
			watchlistCounter++;
		});
			 
	 if(watchlistCounter == 0)
	 {
		$(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" checked="true"/> Watchlists ('+allWatchListCounter+') Selected');
	 }
	 else
	 {
		$(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList"/> Watchlists ('+allWatchListCounter+') Selected');
	 }
	 if(allWatchListCounter == 0)
	 {
		 $('.rivaledgeListEventsAMLAW_100').prop("checked", true);
	 }
	});

//Firms selection

//When "All-" radio button(usually starts with "All " is selected then deselect all the children checkboxes(starts with Indivudual) 
//Note : The individual radio buttons are deselected automatically
$("#<portlet:namespace/>all_firm_org, #<portlet:namespace/>all_firm, #<portlet:namespace/>all_org").change(function(){
	if($(this).is(":checked"))
	{
		$("#<portlet:namespace/>firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/>  Firms (0) Selected');
		$(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0) Selected');
		$(".individualfirms-Events-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="<portlet:namespace/>selectAllOrganizations" /> Organizations (0) Selected');
		$('#<portlet:namespace/>individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>individualfirms input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>individualfirmsOrganization input[type=checkbox]:checked').removeAttr('checked');
		<portlet:namespace/>applyFirms();
	}
	
});

// This is for "Individual" radio buttons
$("#<portlet:namespace/>firm_watchlist, #<portlet:namespace/>firm_individual, #<portlet:namespace/>org_individual").change(function(){
	if($(this).is(":checked"))
	{
		changeOnWatchList();
		changeOnIndividualFirms();
		changeOnIndividualOrgs();
	}
});

//Practice Area  selection
$(".allEventsPracticeArea").change(function(){
	if($(this).is(":checked"))
	{
		
		$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="<portlet:namespace/>selectAllPracticeAreas" /> Practice Area (0) Selected');
		$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>allPracticeAreaIndividual').removeAttr('checked');
		<portlet:namespace/>applyPracticeArea();
	}
});


$("#<portlet:namespace/>allPracticeAreaIndividual").change(function(){
	if($(this).is(":checked"))
	{
		changeOnIndivudualPracticeAreas();
	}
});

//Locations  selection

$("#<portlet:namespace/>loc_all, #<portlet:namespace/>loc_inperson, #<portlet:namespace/>loc_webinars").change(function(){
	if($(this).is(":checked"))
	{
		
		$("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="<portlet:namespace/>selectAllLocations" /> Locations (0) Selected');
		$('#<portlet:namespace/>allOtherLocationsDiv input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>loc_individual').removeAttr('checked');
		<portlet:namespace/>applyLocations();
	}
	
});

//This is for "Individual" radio buttons
$("#<portlet:namespace/>loc_individual").change(function(){
	if($(this).is(":checked"))
	{
		<portlet:namespace/>changeOnIndividualLocations();
	}
	
});

 $("#<portlet:namespace/>additionalFilter").click(function () {
  var link = $(this);

  $('#<portlet:namespace/>additionalFilterDiv').slideToggle('slow', function() {
            if ($(this).is(":visible")) {
                 link.text('Hide Filter');                
            } else {
                 link.text('Additional Filter');                
            }        
        });
});
 
	//Spring will add <input> tags with names prefixed with "_" for type "radio" and "checkbox".
	//On form POST submit they all are carried to server which is unneccessary
	//hence removing all such input box to make page and jQuery selections less heavy :) on document ready.
	
	//NOTE : If any inputs added with name prefixed with "_" purposefully will also get removed. So be cautious
	$("#eventSearchModelBean input[name^=_]").remove();
	
	$(function() 
	{
		
		
		var data = ${firmJson};
		var practiceData = ${practiceJson};
		var locationData = ${allOtherLocations};
		
		function split( val ) 
		{
			return val.split( /,\s*/ );
		}

		function extractLast( term ) 
		{
			return split( term ).pop();
		}

		$( "#<portlet:namespace/>selectedPracticeArea" )
			//don't navigate away from the field on tab when selecting an item
			.bind( "keydown", function( event ) 
			{
				if ( event.keyCode === $.ui.keyCode.TAB && $( this ).data( "ui-autocomplete" ).menu.active ) 
				{
					event.preventDefault();
				}
			})
			.autocomplete({minLength: 3, source: function( request, response ) 
			{
				//delegate back to autocomplete, but extract the last term
				response( $.ui.autocomplete.filter(practiceData, extractLast( request.term )));
			},
			focus: function() 
			{
				//prevent value inserted on focus
				return false;
			},
			change: function(event, ui) {		
				$('#<portlet:namespace/>popup').find('input[type=checkbox]:checked').removeAttr('checked');
				$('#<portlet:namespace/>popup').find('input[type=radio]:checked').removeAttr('checked');
				this.value = this.value.substring(0, this.value.length - 1);
				var selectedValues = this.value.split(',');
				var output="";
				var currentvalue="";
				
				$.each(selectedValues, function(key, line) {
					line=line.trim();	
					
					if(line!=""){	
						currentvalue=$("input[value='"+line+"']#<portlet:namespace/>practiceId").val();
						
						if(typeof(currentvalue)!="undefined"){
							output= output + line + ",";
							$("input[value='"+line+"']#<portlet:namespace/>practiceId").prop("checked", true);
							allOtherPracticeAreasChange();
						}	
					}
						
				});		
				$('#<portlet:namespace/>selectedPracticeArea').val(output);
					
		    },
			select: function( event, ui ) 
			{
				var terms = split( this.value );
				//remove the current input
				terms.pop();
				//add the selected item
				terms.push( ui.item.value );
				//add placeholder to get the comma-and-space at the end
				terms.push( "" );
				this.value = terms.join( "," );
				return false;
			}
		});


	$( "#<portlet:namespace/>Firmstext" ).catcomplete({
		minLength: 3,
		source: function( request, response ) {
			// delegate back to autocomplete, but extract the last term
			response( $.ui.autocomplete.filter(
			data, extractLast( request.term ) ) );
		},
		focus: function() {
			// prevent value inserted on focus
			return false;
		},
		change: function(event, ui) {		
			
			$('#<portlet:namespace/>popup').find('input[type=checkbox]:checked').removeAttr('checked');
			$('#<portlet:namespace/>popup').find('input[type=radio]:checked').removeAttr('checked');
			
			this.value = this.value.substring(0, this.value.length - 2);
			var selectedValues = this.value.split(',');
			
			var output = '';
			var currentvalue="";		
			$.each(selectedValues, function(key, line) {
				line=line.trim();
				
				if(line!=""){				
					currentvalue=$("input[labelattr='"+line+"'].allFirmsEventsCheckBoxFirms").val();
					if(typeof(currentvalue)!="undefined"){
						output= output + currentvalue+ ",";
						$("input[labelattr='"+line+"'].allFirmsEventsCheckBoxFirms").prop("checked", true);
						allOtherFirmChange();
					}	
								
				}			
			});		
			
			if(output == '')
			{
				$.each(selectedValues, function(key, line) {
					line=line.trim();		
					if(line!=""){				
						currentvalue=$("input[labelattr='"+line+"'].allFirmsCheckBoxEventsWatchList").val();
						if(typeof(currentvalue)!="undefined"){
								output= output + currentvalue+ ",";	
							$("input[labelattr='"+line+"'].allFirmsCheckBoxEventsWatchList").prop("checked", true);
							allOtherWatchlistChange();
						}	
									
					}			
				});
			}
			
			if(output == '')
			{
				$.each(selectedValues, function(key, line) {
					line=line.trim();		
					if(line!=""){				
						currentvalue=$("input[labelattr='"+line+"'].allFirmsEventsCheckBoxOrg").val();
						if(typeof(currentvalue)!="undefined"){
							output= output + currentvalue+ ",";	
							$("input[labelattr='"+line+"'].allFirmsEventsCheckBoxOrg").prop("checked", true);
							allOtherOrgsChange();
						}	
									
					}			
				});
			}
			output = output.substring(0, output.length - 1);
			$('#<portlet:namespace/>selectedFirms').val(output);		
	    },
		select: function( event, ui ) {
			
			var terms = split( this.value );		
			var termsValue = split( this.id );
			
			if( ui.item.value.indexOf("AmLaw 100") !== -1){
				return false;
			}
			
			// remove the current input
			terms.pop();
			termsValue.pop();
			// add the selected item
			
			terms.push( ui.item.value );
			termsValue.push( ui.item.id );
			// add placeholder to get the comma-and-space at the end
			terms.push( "" );
			termsValue.push( "" );
			this.value = terms.join( ", " );
			
			$('#<portlet:namespace/>selectedFirms').val($('#<portlet:namespace/>selectedFirms').val().replace("AmLaw 100","") + "," + ui.item.id);
			var sleVar=$('#<portlet:namespace/>selectedFirms').val();
			return false;
		}
	});
	
	$("#<portlet:namespace/>selectedLocation")
	.catcomplete({
	  minLength: 3,
	  source: function( request, response ) {
	    // delegate back to autocomplete, but extract the last term
	    response( $.ui.autocomplete.filter(
	    		locationData, extractLast( request.term ) ) );
	  },
		change: function(event, ui) {		
			$('#<portlet:namespace/>popup').find('input[type=checkbox]:checked').removeAttr('checked');
			$('#<portlet:namespace/>popup').find('input[type=radio]:checked').removeAttr('checked');
			this.value = this.value.substring(0, this.value.length - 1);
			var selectedValues = this.value.split(',');
			var output = "";
			var currentvalue="";
			$.each(selectedValues, function(key, line) {
				line=line.trim();	
				
				if(line!=""){	
					currentvalue=$("input[locationvalue='"+line+"'].allEventsLocationsCheckBox").val();
					if(typeof(currentvalue)!="undefined"){
						output= output + line + ",";
						$("input[locationvalue='"+line+"'].allEventsLocationsCheckBox").prop("checked", true);
						allOtherLocsChange();
					}	
				}
				
			});	
			$('#<portlet:namespace/>selectedLocation').val(output);		
	    },
	  focus: function() {
	    // prevent value inserted on focus
	    return false;
	  },
	  select: function( event, ui ) {
	    var terms = split( this.value );
	    // remove the current input
	    terms.pop();
	    // add the selected item
	    terms.push( ui.item.value );
	    // add placeholder to get the comma-and-space at the end
	    terms.push("");
		
	    this.value = terms.join(",");
	    return false;
	  }
	});	

	function allOtherFirmChange()
	{
		var allFirmsCounter = 0;
		$('#<portlet:namespace/>individualfirms input[type=checkbox]:checked').each(function() 
		{
			allFirmsCounter++;
		});
		
		$("#<portlet:namespace/>firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms" /> Firms ('+allFirmsCounter+') Selected');
		
		if(allFirmsCounter > 0)
		{
			 $('#<portlet:namespace/>firm_individual').prop("checked", true);
			 $(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0) Selected');
			 $("#organizationsCounter").html('<input type="checkbox" value="selectAllOrganizations" id="<portlet:namespace/>selectAllOrganizations" />  Organizations (0) Selected');
		}
		else
		{
			$('#<portlet:namespace/>firm_individual').prop("checked", false);
		}
	}

	function allOtherWatchlistChange()
	{
		var allWatchListsCounter = 0;
		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').each(function() 
		{
			allWatchListsCounter++;
		});
		$(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists ('+allWatchListsCounter+') Selected');
		
		if(allWatchListsCounter > 0)
		{
			 $('#<portlet:namespace/>firm_watchlist').prop("checked", true);
			 $("#<portlet:namespace/>firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms" /> Firms (0) Selected');
			 $("#organizationsCounter").html('<input type="checkbox" value="selectAllOrganizations" id="<portlet:namespace/>selectAllOrganizations" />  Organizations (0) Selected');
		}
		else
		{
			$('#<portlet:namespace/>firm_watchlist').prop("checked", false);
		}
	}
	function allOtherOrgsChange()
	{
		var allOrgsCounter = 0;
		$('#<portlet:namespace/>individualfirmsOrganization input[type=checkbox]:checked').each(function() 
		{
			allOrgsCounter++;
		});
		$("#<portlet:namespace/>organizationsCounter").html('<input type="checkbox" value="selectAllOrganizations" id="<portlet:namespace/>selectAllOrganizations" />  Organizations ('+allOrgsCounter+') Selected');
		
		if(allOrgsCounter > 0)
		{
			 $('#<portlet:namespace/>org_individual').prop("checked", true);
			 $("#<portlet:namespace/>firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms" /> Firms (0) Selected');
			 $(".individualfirms-Events-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0) Selected');
		}
		else
		{
			$('#<portlet:namespace/>org_individual').prop("checked", false);
		}
	}

	function allOtherPracticeAreasChange()
	{
		var allPracticeAreasCounter = 0;
		$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:checked').each(function() 
		{
			allPracticeAreasCounter++;
		});
		$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="<portlet:namespace/>selectAllPracticeAreas" /> Practice Areas ('+allPracticeAreasCounter+') Selected');
		
		if(allPracticeAreasCounter > 0)
		{
			 $('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", true);
			 $('.allEventsPracticeArea').prop("checked", false);
		}
		else
		{
			$('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", false);
		}
	}
	
	
	
	function allOtherLocsChange()
	{
		var allLocsCounter = 0;
		$('#<portlet:namespace/>allOtherLocationsDiv input[type=checkbox]:checked').each(function() 
		{
			allLocsCounter++;
		});
		$("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="<portlet:namespace/>selectAllLocations" /> Locations ('+allLocsCounter+') Selected');
		
		if(allLocsCounter > 0)
		{
			 $('#<portlet:namespace/>loc_individual').prop("checked", true);
 			 $('#<portlet:namespace/>loc_all').prop("checked", false);
			 $('#<portlet:namespace/>loc_inperson').prop("checked", false);
			 $('#<portlet:namespace/>loc_webinars').prop("checked", false);
		}
		else
		{
			$('#<portlet:namespace/>loc_individual').prop("checked", false);
		}
	}


		$( "#<portlet:namespace/>from" ).datepicker({
			changeMonth: true,
			changeYear: true,
			showOn: "button",
			buttonImage: "<%=renderRequest.getContextPath()%>/images/calendar.gif",
			buttonImageOnly: true,
			defaultDate: "+1w",
			onClose: function( selectedDate ) {
			$( "#<portlet:namespace/>to" ).datepicker( "option", "minDate", selectedDate );
			}
		});
		
		$( "#<portlet:namespace/>to" ).datepicker({
			changeMonth: true,
			changeYear: true,
			showOn: "button",
			buttonImage: "<%=renderRequest.getContextPath()%>/images/calendar.gif",
			buttonImageOnly: true,
			defaultDate: "+1w",
			onClose: function( selectedDate ) {
			$( "#<portlet:namespace/>from" ).datepicker( "option", "maxDate", selectedDate );
			}
		});
	});
	
	//Spring will add <input> tags with names prefixed with "_" for type "radio" and "checkbox".
	//On form POST submit they all are carried to server which is unneccessary
	//hence removing all such input box to make page and jQuery selections less heavy :) on document ready.
	
	//NOTE : If any inputs added with name prefixed with "_" purposefully will also get removed. So be cautious
	$("#eventSearchModelBean input[name^=_]").remove();

	//autoselect previously selected search critria
	initializeSearchCriteria();
	
	//fire a search for the default search criteria
	//search();
});
 
	
</script>

<div id="homePageShowEvent" >
<form:form  commandName="eventSearchModelBean" method="post" action="${submitURL}" id="eventSearchModelBean"  autocomplete="off">
 	<form:hidden path="goToPage"  id="goToPage" />
 	<form:hidden path="sortColumn" id="sortColumn" />
 	<form:hidden path="sortOrder"  id="sortOrder" />
	<form:hidden path="addToSession"	id="addToSession"/>
<div class="breadcrumbs"><span>EVENTS</span></div>
<div id="dropdown">

<ul id="droplist">
 
 <li>
        <label>Firm(s) and/or Organizations</label>
        <div class="srchBox">
        
        <!-- Condition for showing Firm/Watchlist default view for Firm(s) drop down -->
        
           <c:choose>
              <c:when test="${empty allWatchListsDefaultList or allWatchListsDefaultList == ''}">
          			<input type="text" name="Firmstext" id="Firmstext"  value="<%=ALMConstants.AMLAW_100 %>" style="text-overflow:ellipsis;" class="input"  autocomplete="off"/>
          	  </c:when>
          	  <c:otherwise>
          			<input type="text" name="Firmstext" id="Firmstext"  value="${allWatchListsDefaultList.groupName}" style="text-overflow:ellipsis;" class="input"  autocomplete="off"/>
          	  </c:otherwise>
          	</c:choose>
          	<input type="button" name="search" value="" class="srchBack" id="testDiv" />
          <div class="clear">&nbsp;</div>
        </div>
        <input type="button" name="search" id="hide"  value="" class="typeSel" />
       
        <div class="rel">
		 <div  id="<portlet:namespace/>popup" class="firmPage">
			<p>
                <form:radiobutton path="firmType" id="all_firm_org" value ="<%=ALMConstants.ALL_FIRMS_ORGANIZATION %>"/><%=ALMConstants.ALL_FIRMS_ORGANIZATION %></p>
              	<p> <form:radiobutton path="firmType" id="firm_watchlist" value=""/>Select Watchlist/RivalEdge List</p>
             	<div class="individualfirms-Events-Watchlist" >
                	<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0 Selected)<br />
               </div>
 				 <div  class="individualfirmsEventsWatchList" id="<portlet:namespace/>individualfirmsWatchList"> 
 				 
 				 	<form:checkbox  class="rivaledgeListEventsAMLAW_100" path="firmList"  value="<%=ALMConstants.AMLAW_100 %>" /><%=ALMConstants.AMLAW_100 %><br>
					<form:checkbox  class="rivaledgeListEventsAMLAW_200" path="firmList"  value="<%=ALMConstants.AMLAW_200 %>" /><%=ALMConstants.AMLAW_200 %><br>
					<form:checkbox  class="rivaledgeListEventsNLJ_250" path="firmList"  value="<%=ALMConstants.NLJ_250 %>" /><%=ALMConstants.NLJ_250 %><br>
  
 				 	<div  class="individualfirmsWatchListDIV" id="<portlet:namespace/>individualfirmsWatchListDIV"> 
		                <c:forEach var="watchlist"  items="${allWatchLists}">     
	    	  				 <form:checkbox  class="allFirmsCheckBoxEventsWatchList" path="firmListWatchList"  value="${watchlist.groupId}" labelAttr="${watchlist.groupName}" />${watchlist.groupName}<br>
	  				 	</c:forEach>
					</div>
			 	  </div> 
              <p>
                <form:radiobutton path="firmType" id="all_firm" value ="<%=ALMConstants.ALL_FIRMS %>"/><%=ALMConstants.ALL_FIRMS %>
              <p>
              <p>
                <!-- <input type="radio" name="firmIndividualsValue" id="firmIndividualsValue"/> -->
                 <form:radiobutton  id="firm_individual" path="firmType"  value="<%=ALMConstants.INDIVIDUAL_FIRMS %>"/>Select Individual Firms
                </p>
              <div class="individualfirms-first" id="<portlet:namespace/>firmsCounter">
                <input type="checkbox" value="selectAllFirms" id="selectAllFirms" /> Firms (0 Selected)<br />
              </div>
              <div class="individualEventsfirms" id="<portlet:namespace/>individualfirms">
                <c:forEach var="firm"  items="${allFirms}">      
      				 <form:checkbox  class="allFirmsEventsCheckBoxFirms" path="firmList"  value="${firm.companyId}" labelAttr="${firm.company}"/>${firm.company}<br>
  			 </c:forEach>
			 <br />
              </div>
              <p>
              	<form:radiobutton path="firmType" id="all_org" value ="<%=ALMConstants.ALL_ORGANIZATIONS %>"/><%=ALMConstants.ALL_ORGANIZATIONS %>
              </p>
              <p>
              	<form:radiobutton path="firmType"  id="org_individual" value="<%=ALMConstants.INDIVIDUAL_ORGS%>"/>Select Individual Organizations
              </p>
			  <div class="individualfirms-Events-Organization" id="<portlet:namespace/>organizationsCounter">
                <input type="checkbox" value="selectAllOrganizations" id="selectAllOrganizations" />  Organizations (0 Selected)<br />
              </div>
              <div class="individualfirmsEventsOrganization" id="<portlet:namespace/>individualfirmsOrganization">
                <c:forEach var="org"  items="${orgList}">      
      				  <form:checkbox  path="firmList" class="allFirmsEventsCheckBoxOrg" value="${org.companyId}" labelAttr="${org.company}" />${org.company}<br>
  			 	</c:forEach>
              </div>
                <div class="popupsubmit">
                	<input type="button" class="buttonOne" value="Apply" id="ApplyFirm">
                	<input type="button" class="buttonTwo" value="Clear All" id="clearButton">
                </div>
		</div>
	   </div>
	   <c:if test="${not empty allWatchListsDefaultList}">
	    	<input type="hidden" name="defaultWatchListId" id="defaultWatchListId" value="${allWatchListsDefaultList.groupId}" />
	   </c:if>
 </li>
               <li>
					  <label>Practice Area(s)</label>
						  <div class="srchBox">
							<input type="text"  class="input" name="selectedPracticeArea" id="selectedPracticeArea"  style="text-overflow:ellipsis;"  autocomplete="off"/>
							<input type="button" name="search" value="" class="srchBack" />
							<div class="clear">&nbsp;</div>
						  </div>
						 <input type="button" name="search" value=""  id="practiceAreaId" class="typeSel" />
						<div class="rel">
							  <div  id="<portlet:namespace/>popupPracticeArea" class="firmPage">					
									
								<p><form:radiobutton path="practiceArea" class="allEventsPracticeArea" value="<%=ALMConstants.ALL_PRACTICE_AREAS %>" /><%=ALMConstants.ALL_PRACTICE_AREAS%></p>
						   		<p> <input  type="radio"  name="allPracticeArea"  id="allPracticeAreaIndividual" value="Select Individual Practice Areas"> Select Individual Practice Areas</p>

									<div class="individualfirms-first" id="<portlet:namespace/>practiceAreaCounter"> <input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" /> Practice Areas (0 Selected)<br> </div>
								  <div style="background-color:#FFFFFF;" id="<portlet:namespace/>practiceAreaDiv" class="practiceAreaEventDiv">										 

										 <div style="background-color:#FFFFFF;" id="<portlet:namespace/>allOtherPracticeArea">
										    <c:forEach var="practice" items="${allPracticeArea}">								
												<form:checkbox path="practiceArea" id="practiceId" value="${practice.practiceArea}"/> ${practice.practiceArea}<br>
											</c:forEach>
										 </div>	
									</div>
								<div class="popupsubmit">
								 <input type="button" class="buttonOne" value="Apply" id="ApplyPracticeArea" />
								 <input type="button" class="buttonTwo" value="Clear All" id="clearPracticeArea" />
								</div>	
						</div>
						</div>
				</li>
				
 		<li>
        		<label>Location(s)</label>
					  <div class="srchBox">
						<input type="text" class="input" id="selectedLocation"  style="text-overflow:ellipsis;"  autocomplete="off" />
						<input type="button" name="search" value="" class="srchBack" />
						<div class="clear">&nbsp;</div>
					  </div>
					 <input type="button" name="search" value=""  id="locationId" class="typeSel" />
					<div class="rel">	 
						<div  id="<portlet:namespace/>popupLocation" class="firmPage">
							<p><form:radiobutton path="locations"  id="loc_all" value="<%=ALMConstants.ALL_LOCATIONS%>" /> <%=ALMConstants.ALL_LOCATIONS %></p>	
							<p><form:radiobutton path="locations"  id="loc_inperson" value="In Person" /> All In-Person</p>
							<p><form:radiobutton path="locations"  id="loc_webinars" value="Webinar"/> All Webinars</p>
							<p><input type="radio"  id="loc_individual" value="Select Individual Locations" /> Select Individual Locations</p>
							<div class="individualfirms-first" id="<portlet:namespace/>locationsCounter"> 

								<input type="checkbox" value="selectAllLocations" id="selectAllLocations" /> Locations (0) Selected<br> </div>
							<div style="background-color:#FFFFFF;" id="<portlet:namespace/>allOtherLocationsDiv" class="allOtherEventLocationsDiv">		

							<c:forEach items="${locationsMapValues}" var="loc">
								<form:checkbox path="locations" class="allEventsLocationsCheckBox" value="${loc.value}" locationValue="${loc.key}"/>${loc.key}<br>
							</c:forEach>	
							   
							</div>
							<div class="popupsubmit">				
							<input type="button" class="buttonOne" value="Apply" id="ApplyLocations">
							<input type="button" class="buttonTwo" value="Clear All" id="clearButtonLocations">
							</div>
						</div>
					</div>
 		</li>
 
   <li>
        <label>Dates</label>
        <div class="srchBox">
           <form:input path="dateText" id="dateText" readonly="true" cssClass="input" cssStyle="width:140px"  autocomplete="off"/>
        <div class="clear">&nbsp;</div>
        </div>
        <input type="button" name="search" id="datenone" value="" class="drpDwn" />
        <div class="rel">
        <div id="<portlet:namespace/>popupDate" class="datePage">
        	<input type="radio" name="date" id="period"  />&nbsp;Period<br>
			<select id="datePeriodSelect" class="marlt4">
				<option value="Any">Any</option>
				<option value="<%=ALMConstants.NEXT_WEEK%>">Next Week</option>
				<option value="<%=ALMConstants.NEXT_30_DAYS%>">Next 30 Days</option>
				<option value="<%=ALMConstants.NEXT_6_MONTHS%>">Next 6 Months</option>
				<option value="<%=ALMConstants.NEXT_YEAR%>">Next Year</option>
				<option value="Last Week">Last Week</option>
				<option value="Last 30 Days">Last 30 Days</option>
				<option value="Last 90 Days">Last 90 Days</option>
				<option value="Last 6 Months">Last 6 Months</option>
				<option value="Last Year">Last Year</option>  
				
			</select>
			<br>
			<input type="radio" name="date" id="dateRange" checked="checked"/>&nbsp;Date Range<br>

			<div class="flLeft marlt4"><label for="from">From</label>
			<input type="text" id="from" name="from"  /></div>
			<div class="flLeft marlt3"><label for="to">To</label>
			<input type="text" id="to" name="to" /></div>
			<div class="clear"></div>
			<span class="error" id="fromdateError"> Invalid  From Date</span>
			<span class="error" id="todateError"> Invalid  To Date</span>
			<span class="error" id="dateValidError"> To date should be more than from date</span>
            <div class="popupsubmit Popup-Calendar">				
                <input type="button" class="buttonOne" value="Apply" id="ApplyDate">
                <input type="button" class="buttonTwo" value="Clear All" id="clearButtonDate">
			</div>
        </div>
        </div>
    </li>
	
	<%-- Sachin: RER 317: Moved the Keyword option from addition filters to first row --%>
	<li>
		<label>Keywords</label>
   		<div class="srchBox">
	        <form:input path="keywords" id="keywords" cssClass="input"  autocomplete="off"/>
	        <input type="button" name="search" value="" class="srchBack" />
	     	<div class="clear">&nbsp;</div>
		</div>
	</li> 
	

	  <li>
        <label>&nbsp;</label>
         <input type="button" value="Apply" class="buttonOne" style="background:url(<%=renderRequest.getContextPath()%>/images/btn1.png) 0 0 repeat-x; border:1px solid #bf8d1f; -webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px; font:normal 12px Arial, Helvetica, sans-serif; color:#fff; padding:5px 10px; cursor:pointer; text-shadow: 0px 0px #FFF;" id="applySearch"/>
        
      </li>
      <li>
        <label>&nbsp;</label>
        <input type="button" value="Reset All" style="background:url(<%=renderRequest.getContextPath()%>/images/btn2.png) 0 0 repeat-x; border:1px solid #565656; -webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px; font:normal 12px Arial, Helvetica, sans-serif; color:#fff; padding:5px 10px; cursor:pointer; text-shadow: 0px 0px #FFF;" class="buttonTwo" id="resetAll" />
      </li>
	   <li>
        <label>&nbsp;</label>
        <input type="hidden"  name="selectedFirms" id="selectedFirms"  title="" customAttr="" />
      </li>
      </ul>
 <div class="clear">&nbsp;</div>
</div>

  <%--Remove this Div when Addition Filter div is enabled, because this div will add additional space which is not desired when the Additional filter is restored back --%>
       <div style="height: 30px;">
       </div>
		  <%--<div class="filtersPage">
		  	<div class="barSec" style="background:url(<%=renderRequest.getContextPath()%>/images/additinalBack.png) center 0 no-repeat; width:100%; height:24px; margin:10px 0">
			  <!-- <a href="javascript:void(0);" class="dwnBx" id="btnAdd">Additional Filters</a>   --> <!-- Removed Additional filter option to fix Bug RER 317 -->
			</div>	
		  </div>
		  --%>
		  
	<%-- Sachin: RER 317 Move the Keyword search up to the first row  and Eliminate the tab for the Additional Filters pull down.  --%>
	  
	<%--  	  <div class="filtersPage" id="additional" style="display:none">
			<div id="dropdown">
			  <ul id="droplist">
				<li>
		        <label>Keywords</label>
		        <div class="srchBox">
		           <form:input path="keywords" id="keywords" cssClass="input" />
		          <input type="button" name="search" value="" class="srchBack" />
		          <div class="clear">&nbsp;</div>
		        </div>
		       
		      </li>
		      
			  </ul>
			  <div class="clear">&nbsp;</div>
			</div>
			<div class="barSec" style="background:url(<%=renderRequest.getContextPath()%>/images/additinalBack.png) center 0 no-repeat; width:100%; height:24px; margin:10px 0"><a href="javascript:void(0);" class="upBx" id="btnSave">Hide Filter</a></div>
		--%>
  
  <%--    <div class="flRight">   	  
      <ul class="menuRight flRight">
        <li><a class="btn icon alert" href="javascript:void(0);">Create Alert</a></li>
         <li><a class="btn icon print" href="javascript:void(0);" onClick="openPopUp();" >Print</a></li>
        <li><a id="exports" class="btn icon export" href="javascript:void(0);">Export</a>
          <div id="actionBox" class="actionSec" style="">
            <h5>Actions</h5>
            <ul class="reset">
              <li><span>&nbsp;</span><a href="javascript:void(0);"  onclick="setFileType('xls')">Export to Excel</a></li>
              <li><span>&nbsp;</span><a href="javascript:void(0);"  onclick="setFileType('csv')">Export to CSV</a></li>
            </ul>
            <div class="clear">&nbsp;</div>
          </div>
        </li>
      </ul>
      <div class="clear">&nbsp;</div>
       </div>--%>
  
   <div id="<portlet:namespace/>view_settings" style="display: none; position:absolute; background:#F0F0F0; width:310px !important" class="viewBox popusdiv ClickPopup newspublicationPage charts">
					<div class="popHeader">
						<a href="javascript:void(0);" class="btn icon closewhite closeOne flRight" style="margin-top: 2px" onClick="cancelViewSettings();">&nbsp;Close</a>
							SETTINGS: Events
					</div>
                    <div class="section-two">
                      <h6>Group Results By</h6>
                        <ul class="reset list4">
                             <li><form:radiobutton path="orderBy" value="-1"/><span>None</span>
                             <li><form:radiobutton path="orderBy" value="9" /><span>Firm/Organization</span></li>
                             <li><form:radiobutton path="orderBy" value="7"/><span>Location</span></li>
                             <li><form:radiobutton path="orderBy" value="8"/><span>Practice</span></li>
                        </ul>
                        <div class="clear">&nbsp;</div>
                    </div>
					<div class="section-two" style="width:140px;">
							<h6>Display No of Results</h6>
							<ul class="reset list4">
								<li><form:radiobutton path="searchResultsPerPage"  value="25"  /><span>25</span></li>
                                <li><form:radiobutton path="searchResultsPerPage"  value="50"  /><span>50</span></li>
                                <li><form:radiobutton path="searchResultsPerPage"  value="100" /><span>100</span></li>
                                <li><form:radiobutton path="searchResultsPerPage"  value="500" /><span>500</span></li>
							</ul>
							<div class="clear">&nbsp;</div>
					</div>
					<div class="clear">&nbsp;</div>
					<hr>
					<div class="btmdiv">
						<input type="button" value="Reset All" class="buttonTwo flLeft settingReset" onClick="resetViewSettings();" />
						<input type="button" class="buttonTwo flRight rightReset" value="Cancel"  onClick="cancelViewSettings();" />
						<input type="button" value="Apply" class="buttonOne flRight" style="margin: 0 5px 0 0;" onClick="applyViewSettings();"  />
						<div class="clear">&nbsp;</div>
					</div>
					<input type="hidden" id="selectedGroupName" value=""/>
					<input type="hidden" id="selectedPerPage" value="" />
				</div>


</form:form>
<!-- Showing Deatils and Analysis tab -->
</div>

<%-- 
 <form name="exportForm" method="post" id="exportForm" action="<portlet:resourceURL id="exportFile"/>">
	<input type="hidden" name="fileType" value="" id="fileType" />
 </form> --%>
<script>

	// Omniture SiteCatalyst - START
	s.prop22 = "premium";
	s.pageName="rer:events-form";
	s.channel="rer:events-form";
	s.server="rer";
	s.prop1="events-form"
	s.prop2="events-form";
	s.prop3="events-form";
	s.prop4="events-form";
	s.prop21=s.eVar21='current.user@foo.bar';	
	s.events="event2";
	s.events="event27";
	
	s.t();
	// Omniture SiteCatalyst - END
</script> 
