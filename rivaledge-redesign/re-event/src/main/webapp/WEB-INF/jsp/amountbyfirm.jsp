<%@page import="com.alm.rivaledge.model.chart.BaseChartModelBean"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="java.util.Map"%>
<%@ taglib prefix="portlet" 		uri="http://java.sun.com/portlet_2_0"%> 
<%@ taglib prefix="c" 			 	uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"          uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form"            uri="http://www.springframework.org/tags/form"%>

<portlet:defineObjects/>

<%@ include file="./common.jsp" %>

<portlet:actionURL var="updateChartURL">
	<portlet:param name="action" value="updateChart"/>
</portlet:actionURL>

<portlet:actionURL var="removePortletURL">
	<portlet:param name="action" value="removePortleFromThePage"/>
</portlet:actionURL>

<script type="text/javascript">
(function () {
  

<%@ include file="./theme.jsp" %>

	<c:choose>
		<c:when test="${amtByFirmChartType eq 'horizontal_bar' }">
			var amtByFirmChartType = 'bar';		
		</c:when>
		<c:otherwise>
			var amtByFirmChartType = 'column';		 
		</c:otherwise>	
	</c:choose>

	var amtByFirmChartTitle 	 = '${amtByFirmChartTitle}';  // This will be title of chart.
	var amtOfEventsFirmsNameList = [${firmsNameList}]; 		 // This will be list of firms name.
	var firmsCountByFirmList 	 = [${firmsCountByFirmList}]; // This will be Practice Areas count of individual firms.
	
	var amtByFirmLocations		 = [${amtByFirmLocations}];
	var amtByFirmPractices		 = [${amtByFirmPractices}];
	var avgLocationList		 	 = [${avgLocationList}];
	var avgPracticeAreaList		 = [${avgPracticeAreaList}];
	var pointWidth				 = 15;
	
	
	<c:choose>
	<c:when test="${(not empty amtByFirmCompareCount) and (amtByFirmCompareCount > 0)}">
		var amtByFirmCompareCount = ${amtByFirmCompareCount};
	</c:when> 	
	<c:otherwise>
		var amtByFirmCompareCount = 0;
	</c:otherwise>	
	</c:choose>

$(function()
{
	
	
	$(".AnalyzeResultHeader").show();
	$("#events-details").removeClass("active-menu");
	$("#events-analysis").addClass("active-menu");
	
	if(<%=!isHomePage%>)
	{
		if(amtOfEventsFirmsNameList==null || amtOfEventsFirmsNameList == '')
		{
			$("#p_p_id<portlet:namespace/>").hide();
			$(".cph").show();
			return;
		}	
	}
	else
	{
		if(amtOfEventsFirmsNameList==null || amtOfEventsFirmsNameList == '')
		{
			$("#amountbyfirmDiv").removeClass("hideClass");
			$("#amountOfFirmsChartContainer").addClass("hideClass");
			
			$("#No-Data-amountFirm").addClass("No-Data-Charts");
			$("#amount_byfirm_settings").hide();
		}
		else
		{
			$("#No-Data-amountFirm").removeClass("No-Data-Charts");
			$("#amountbyfirmDiv").addClass("hideClass");
		}	
	}	
	
	
	$("div#noEventsByFirmPopupDiv").mouseover(function(){
		$("#noEventsByFirmPopupDiv").show();
	}).mouseout(function(){
		$("#noEventsByFirmPopupDiv").hide();
	});
	
	$("#amountByFirmApply, .closewhite").click(function(){
		$("#amountOfEventsViewSetting").hide();
	});
	
	$(document).click(function() {
		$("div#noEventsByFirmPopupDiv").hide();
	});
	
	$("#amountByFirmCancel, .closewhite").click(function(){
		$("#amountOfEventsViewSetting").hide();
	});
	
	$("#amountByFirmResetAll").click(function(){
		$("input[value=vertical_bar]").prop("checked", true);
		$("input[value=top_15]").prop("checked", true);
		$("#searchResultsFirmList > option").attr("selected",false);
		$("#firmList > option").attr("selected",false);
		$("input[value=Average]").prop("checked", true);
		$("input[value='RivalEdge Average']").prop("checked", true);
		$("input[class=allLocationsCheckBox]").prop("checked", false);
		$("input[value='My Firm']").prop("checked", false);
		$("input[value='Watchlist Average']").prop("checked", false);
	});
	
	$(".allLocationsCheckBox").click(function(){
		console.log($('.allLocationsCheckBox').prop('checked'));
		if($('.allLocationsCheckBox').prop('checked')){
			$("input[value=firm]").prop("checked", true);
		}
	});
	$("#firmList").change(function(){
		$("input[value='RivalEdge Average']").prop("checked", true);
	});
	$("#watchList").change(function(){
		if($("#watchList option:selected")){
			$("input[value='Watchlist Average']").prop("checked", true);
		}
	});
	
	$('#amntByFirmPrintCharts').click(function() {
        var chart = $('#amountOfFirmsChartContainer').highcharts();
        chart.print();
    });
	$('#amntByFirmExportJPG').click(function() {
        var chart = $('#amountOfFirmsChartContainer').highcharts();
        chart.exportChart({type: 'image/jpeg'});
    });
	$('#amntByFirmExportPNG').click(function() {
        var chart = $('#amountOfFirmsChartContainer').highcharts();
        chart.exportChart({type: 'image/png'});
    });
	
	var labelXPosition = 0;
	var labelYPosition = 0;
	var labelRotation = 0;
	
	if(amtByFirmChartType == 'bar')
	{
		labelXPosition = -5;
		labelYPosition = 0;
		labelRotation = 0;
	}
	else
	{
		labelXPosition = 5;
		labelYPosition = 10;
		labelRotation = -45;
	}
	
	
	var splitLineCount = parseFloat(amtByFirmCompareCount) - parseFloat(0.5);
	
	$('#amountOfFirmsChartContainer').highcharts(
	{
        chart: 
        {
            type: amtByFirmChartType
        },
        title: 
        {
            text: amtByFirmChartTitle
        },
        xAxis: 
        {
            categories: amtOfEventsFirmsNameList,
            labels: 
            {
                rotation: labelRotation,
				y: labelYPosition,
				x: labelXPosition,
				align: 'right',
				formatter: function()
				{
					var firmName = this.value;
					if(firmName.length > 10)
					{
						firmName = firmName.substring(0, 10) + "...";
					}
					return firmName;
				}
			},
			
			plotLines: [{
                color: '#000000',
                width: 2,
                dashStyle: 'Solid',
                value: parseFloat(splitLineCount)
            }]
        },
        yAxis: 
        {
        	gridLineWidth: 1,
			gridLineColor: '#cecece',
			minorGridLineColor: '#cecece',
			lineWidth: 1,
            stackLabels: 
            {
                enabled: false,
                style: 
                {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: 
        {
        	enabled: true
    	},
    	tooltip : {
			useHTML : true,
			positioner : function(boxWidth, boxHeight,
					point) {
					
					var xPosition = point.plotX - 10;
					var yPosition = point.plotY - boxHeight + 38;
					
					if(amtByFirmChartType == 'column')
					{
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(130))
						{
							xPosition = point.plotX-60;
						}
						
						if((parseInt(point.plotY) - parseInt(boxHeight)) < -40)
						{
							yPosition = point.plotY+90;
						}
					}
					else
					{
						xPosition = point.plotX + 103;
						yPosition = point.plotY - boxHeight + 100;
						
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(40))
						{
							xPosition = point.plotX-50;
						}
						
						if((parseInt(boxHeight) - parseInt(point.plotY)) > parseInt(100))
						{
							yPosition = point.plotY - boxHeight + 250;
						}
					}
					
				return {
					x : xPosition,
					y : yPosition
				};
			},
			formatter: function()
			{
				var url = "events-details?drilldownFirmName=" + encodeURIComponent(this.key) + searchCriteria;
				
				var pointerPosition = 0;
				for(var index = 0; index < amtOfEventsFirmsNameList.length; index++)
				{
					if(this.key == amtOfEventsFirmsNameList[index])
					{
						pointerPosition = index-amtByFirmCompareCount;
					}
				}
				var tooltipOption = '';
				if(pointerPosition >= 0)
				{
				
					tooltipOption += '<div class="Tooltip-Heading">'+this.key+'</div><div class="Tooltip-Recent">'+this.total+' Total Events</div><div class="Tooltip-Recent">Top Practice Area</div>';
					for(var index=0; index< amtByFirmPractices[pointerPosition].length; index++)
					{
						tooltipOption += '<div class="Tooltip-Rows"  title="'+ amtByFirmPractices[pointerPosition][index] +'">' + (amtByFirmPractices[pointerPosition][index].length > 15 ? amtByFirmPractices[pointerPosition][index].substring(0, 15) + '...' : amtByFirmPractices[pointerPosition][index]) +'</div>';
					}
					
					tooltipOption += '<div class="Tooltip-Recent">Top Locations</div>';
					for(var index=0; index< amtByFirmLocations[pointerPosition].length; index++)
					{
						tooltipOption += '<div class="Tooltip-Rows"  title="'+ amtByFirmLocations[pointerPosition][index] +'">' + (amtByFirmLocations[pointerPosition][index].length > 15 ? amtByFirmLocations[pointerPosition][index].substring(0, 15) + '...' : amtByFirmLocations[pointerPosition][index]) +'</div>';
					}
					
					tooltipOption += '<div class="clickToViewDetails"><a href="'+url+'" class="Tooltip-ClickToViewDetails">Click to View Details</a></div>';
					
					return tooltipOption;
				}
				else
				{
						tooltipOption += '<div class="Tooltip-Heading">'+this.key+'</div><div class="Tooltip-Recent">'+this.total+' Total Events</div><div class="Tooltip-Recent">Top Practice Area</div>';
						for(var index=0; index< avgPracticeAreaList.length; index++)
						{
							tooltipOption += '<div class="Tooltip-Rows"  title="'+ avgPracticeAreaList[index] +'">' + (avgPracticeAreaList[index].length > 15 ? avgPracticeAreaList[index].substring(0, 15) + '...' : avgPracticeAreaList[index]) +'</div>';
						}
						
						tooltipOption += '<div class="Tooltip-Recent">Top Locations</div>';
						for(var index=0; index< avgLocationList.length; index++)
						{
							tooltipOption += '<div class="Tooltip-Rows"  title="'+ avgLocationList[index] +'">' + (avgLocationList[index].length > 15 ? avgLocationList[index].substring(0, 15) + '...' : avgLocationList[index]) +'</div>';
						}
						
						
						return tooltipOption;
				}
				
			}
		},
        plotOptions: 
        {
            series: 
            {
            	stacking: 'normal',
            	dataLabels: 
            	{
                    enabled: false,
                    color: '#FFFFFF'
                },
                pointWidth: pointWidth,
            }
        },
        series: [
        {
            name: 'Firms',
            color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#506a85'],
					[1, '#15375c']
				]
			},
            data: firmsCountByFirmList
        }],
		navigation: {
            buttonOptions: {
                enabled: false
            }
        }
    });
});

$(document).ready(function()
{	
    $("#amount_byfirm").click(function()
    {
    	Liferay.Portlet.showPopup(
    		{
    			uri : '${chartSearchPortletURL}', // defined in common.jsp
    			title: "Search Criteria"
    		});
		
	});
	
	
});



}());


function removeThisChartAmountByFirm(){
	var removePortletAction = '<%= removePortletURL.toString()%>';
	var r=confirm("Are you sure you want to delete this chart?");
	if (r == true)
  	{

		Liferay.Portlet.showBusyIcon("#bodyId", "Loading...");
		$("#amtByFirmChartModelBean").attr('action', removePortletAction);
		$("#amtByFirmChartModelBean").submit();
  	}
	else
	{
	  return false;
	}
}
</script>

<div class="newspublicationPage marbtm4">
		<div class="colMin flLeft leftDynamicDiv">
			<div class="topHeader ForChartsTopHeader">
				<span title="Remove this chart" onclick="removeThisChartAmountByFirm();" style="float: right; font-weight: bold; color: rgb(255, 255, 255); cursor: pointer; font-family: verdana; margin: 2px 5px; padding: 3px 8px;">X</span>
			</div>
			<div id="noEventsByFirmPopupDiv" style="z-index: 999; width: 200px; position: absolute; background:#ffffff; padding: 10px 10px 10px 10px; border: 1px solid #1A1A1A; display: none;">
				<div id="noEventsByFirmPopupData" class="Popup-Tooltip"></div>
			</div>
			<div id="amountbyfirmDiv" class="hideClass">0 Results, Please try a different search</div>
				<div class="flRight charts" id="No-Data-amountFirm">
				<form:form  commandName="amtByFirmChartModelBean" method="post" action="${updateChartURL}" id="amtByFirmChartModelBean">
					<ul class="reset listView">
						<c:if test="<%=isHomePage%>" >
							<li id="amount_byfirm" style="overflow:hidden;"><a href="javascript:void(0);" class="filter-icon" >&nbsp;</a></li>
						</c:if>
						<li id="amount_byfirm_settings"><a href="#amountOfEventsViewSetting"
							class="btn icon settingsgry rightViewSetting login-window chartViewSetting"
							onclick="return false;">&nbsp;</a></li>
						<li>
							<a href="javascript:void(0);" id="amntByFirmPrintCharts" onclick="return false;" class="printChartClass"></a>
						</li>
						<li>
							<a href="javascript:void(0);" onclick="return false;" class="exportChartClass"></a>
                        <div class="actionSec">
                        <h5>Actions</h5>
                        <ul class="reset">
                            <li class="exportChartImage"><span id="amntByFirmExportJPG">Export as JPG</span></li>
                            <li class="exportChartImage"><span id="amntByFirmExportPNG">Export as PNG</span></li>
                        </ul>
                        <div class="clear">&nbsp;</div>
                        </div>
						</li>
					</ul>
							<div style="display: none" class="viewBox popusdiv ClickPopup" id="amountOfEventsViewSetting" style="width:580px !important;">
                            <div class="popHeader"> <a href="javascript:void(0);" class="btn icon closewhite closeOne flRight" style="margin-top: 2px">Close</a> SETTINGS: EVENTS BY FIRM
                              <div class="clear">&nbsp;</div>
                            </div>
                            <div class="section-one" style="width: 165px;">
                              <h6>Chart Type</h6>
                                <ul class="reset list4">
                                    <li>
                                    <form:radiobutton path="chartType" class="graphType" value="<%=BaseChartModelBean.ChartType.VERTICAL_BAR.getValue()%>" />
                                    <span class="btn icon barchartvert">Vertical Bar</span>
                                   </li>
                                    <li>
                                    <form:radiobutton path="chartType" class="graphType" value="<%= BaseChartModelBean.ChartType.HORIZONTAL_BAR.getValue() %>" />
                                    <span class="btn icon barcharthori">Horizontal Bar</span>
                                   </li>
                                </ul>
                                <div class="clear">&nbsp;</div>
                            </div>
                            <div class="section-five">
                                <h6>Firm Data (Limit of 15)</h6>
                                <ul class="reset list4">
                                  <li>
                                    <form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.TOP_15.getValue() %>" />
                                    <span class="">Top 15</span></li>
                                  <li>
                                     <form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.BOTTOM_15.getValue() %>" />
                                    <span class="">Bottom 15</span></li>
                                  <li>
                                     <form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.FIRM.getValue() %>" />
                                    <span class="">Selected Firms:</span>
                                     <%--
                                     <form:select path="searchResultsFirmList" style="width:150px;">
										<form:options items="${allSearchResultsFirmList}" itemLabel="company" itemValue="companyId"/>
									 </form:select>
                                     --%>
                                     <%--<div class="Selected-Firms-Popup-Header"> 
                                        <input type="checkbox" /> Firms (0 Selected)
                                    </div>--%>
                                     <div class="Selected-Firms-Popup">
                                     <c:forEach items="${allSearchResultsFirmList}" var="searchFirmList">
                                        <form:checkbox path="searchResultsFirmList" class="allLocationsCheckBox" value="${searchFirmList.companyId}" />${searchFirmList.company}<br>
                                    </c:forEach>
                                     </div>
                                    </li>
                                </ul>
                                <div class="clear">&nbsp;</div>
                            </div>
                            <div class="section-three"  style="width:210px;">
                              <div class="martp2">
                                <h6>Comparison Data</h6>
                                <div class="marbtm2 martp1">
                                <!-- As per client requirement this options are hide. -->
								<p class="marbtm1" style="display: none;">
                                    <form:checkbox path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.MY_FIRM.getValue() %>" />
                                    My Firm </p>
                                  <p class="marbtm1">
                                   <form:checkbox path="comparisonDataTypeList" value="<%=BaseChartModelBean.ComparisonDataType.RIVAL_EDGE.getValue()%>" />
                                     <form:select path="firmList" multiple="false">
										<form:options items="${allRankingList}" />
									 </form:select>
                                   </p>
                                  <p class="marbtm1">
                                   <form:checkbox path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.WATCHLIST_AVG.getValue() %>" />
                                    Watchlist<span>&nbsp;&nbsp;&nbsp;</span>
                                    <form:select path="watchList" multiple="false">
                                   		 <form:options items="${allWatchLists}" itemValue="groupId" itemLabel="groupName"/>
									</form:select>
                                  </p>
                                   <p class="marbtm1-last">
                                   <form:checkbox cssClass="mart2" path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.AVERAGE.getValue() %>" />
                                    Average of Firms in Search </p>
                                </div>
						<div class="clear">&nbsp;</div>
                              </div>
                              <div class="clear">&nbsp;</div>
                            </div>
                            <div class="clear">&nbsp;</div>
                            <hr>
                            <div class="btmdiv">
                            <input value="Reset All" class="buttonTwo flLeft settingReset" type="button" id="amountByFirmResetAll">
                            <input type="button" class="buttonTwo flRight rightReset" value="Cancel" id="amountByFirmCancel">
                            <input value="Apply" class="buttonOne flRight" id="amountByFirmApply" type="button" style="margin: 0 5px 0 0;" onclick="applyChartSettings('#amtByFirmChartModelBean');">
                            <div class="clear">&nbsp;</div>
                            </div>
                          </div>
                        </form:form>
                    </div>				
			
			<div id="amountOfFirmsChartContainer" class="charts-spacing">
		</div>
	</div>
</div>