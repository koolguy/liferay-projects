package com.alm.rivaledge.controller.search;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.alm.rivaledge.controller.AbstractEventController;
import com.alm.rivaledge.model.ClickToView;
import com.alm.rivaledge.model.EventSearchModelBean;
import com.alm.rivaledge.persistence.dao.CacheDAO;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.Practice;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.service.WatchlistService;
import com.alm.rivaledge.transferobject.EventSearchDTO;
import com.alm.rivaledge.transferobject.UserGroupDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.FirmJsonUtility;
import com.alm.rivaledge.util.LocationJsonUtility;
import com.alm.rivaledge.util.PracticeJsonUtility;
import com.alm.rivaledge.util.WebUtil;
import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;

/**
 * Event controller responsible for all operation on Event search form and results
 * screen
 * 
 * @author FL613
 * 
 */
@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "eventSearchModelBean", "eventSearchDTO", "eventSearchResults" })
public class EventSearchController extends AbstractEventController
{
	protected Log _log = LogFactoryUtil.getLog(EventSearchController.class.getName());
	
	@Autowired
	private FirmService firmService;
	
	@Autowired
	private WatchlistService watchlistService;
	
	@Autowired
	private CacheDAO	cacheDAO;
	
	@RenderMapping
	protected ModelAndView eventSearchForm(@ModelAttribute("eventSearchModelBean") EventSearchModelBean eventSearchModelBean,
			ModelMap model, RenderRequest request, RenderResponse response) throws Exception
	{
		ClickToView<EventSearchModelBean, EventSearchDTO> c2v = new ClickToView<EventSearchModelBean, EventSearchDTO>();
		c2v.setSearchModelBean(eventSearchModelBean);

		honorClick2View(request, model, c2v); // eventSearchModelBean is affected only when you are directed here from C2V
		
		return renderSearchPage(eventSearchModelBean, model, request, response);
	}
	
	private ModelAndView renderSearchPage(EventSearchModelBean eventSearchModelBean, ModelMap model, RenderRequest request,
			RenderResponse response)
	{

		Gson 						gson 					= new Gson();
		FirmJsonUtility 			firmJsonUtility 		= null;
		PracticeJsonUtility 		practiceJsonUtility 	= null;
		List<UserGroupDTO> 			allWatchLists 			= null;
		List<UserGroup> 			allWatchListsAutoComplete 			= null;
		List<FirmJsonUtility> 		firmJsonUtilityList 	= new ArrayList<FirmJsonUtility>();
		List<LocationJsonUtility> 	locationJsonUtilityList = new ArrayList<LocationJsonUtility>();
		List<PracticeJsonUtility> 	practiceJsonUtilityList = new ArrayList<PracticeJsonUtility>();
		List<Firm> 					firmsSet 				= firmService.getAllFirms();
		List<Firm> 					orgList 				= firmService.getOrganizationsList();
		List<Practice> 				practiceList 			= firmService.getAllPractices();
		User						currentUser				= WebUtil.getCurrentUser(request, userService);
		
		if (currentUser != null)
		{
			allWatchLists = (List<UserGroupDTO>) watchlistService.getAllFirmWatchList(currentUser.getId());
			allWatchListsAutoComplete = (List<UserGroup>) watchlistService.getAllWatchListForAutoComplete(currentUser.getId());
			
			for (UserGroup userGroup : allWatchListsAutoComplete)
			{
				firmJsonUtility = new FirmJsonUtility();
				firmJsonUtility.setId(String.valueOf(userGroup.getGroupId()));
				firmJsonUtility.setLabel(userGroup.getGroupName());
				firmJsonUtility.setCategory("WatchLists");
				firmJsonUtilityList.add(firmJsonUtility);
			}
		}	
		
		for (Firm firm : firmsSet)
		{
			firmJsonUtility = new FirmJsonUtility();
			firmJsonUtility.setId(firm.getCompanyId().toString());
			firmJsonUtility.setLabel(firm.getCompany());
			firmJsonUtility.setCategory("Single Firms");
			firmJsonUtilityList.add(firmJsonUtility);
		}
		
		for (Firm firm : orgList)
		{
			firmJsonUtility = new FirmJsonUtility();
			firmJsonUtility.setId(firm.getCompanyId().toString());
			firmJsonUtility.setLabel(firm.getCompany());
			firmJsonUtility.setCategory("Organizations");
			firmJsonUtilityList.add(firmJsonUtility);
		}

		for (Practice practice : practiceList)
		{
			practiceJsonUtility = new PracticeJsonUtility();
			practiceJsonUtility.setLabel(practice.getPracticeArea());
			practiceJsonUtilityList.add(practiceJsonUtility);
		}
		// converting all firms to JSON string
		String jsonAllFirms = gson.toJson(firmService.getRivalEdgeList(firmJsonUtilityList));
		// converting all Practices to JSON string
		String jsonAllPractics = gson.toJson(practiceJsonUtilityList);

		TreeMap<String,String> locationsMapValues = ALMConstants.getAllLocationsConstant();
		
		for (Map.Entry<String, String> entry : locationsMapValues.entrySet())
		{
			LocationJsonUtility locationJsonUtility = new LocationJsonUtility();
			locationJsonUtility.setId(entry.getValue());
			locationJsonUtility.setLabel(entry.getKey());
			locationJsonUtilityList.add(locationJsonUtility);
		}
		// converting all locations to JSON string
		String jsonAllLocations = gson.toJson(locationJsonUtilityList);
		model.addAttribute("allOtherLocations", jsonAllLocations);
		model.addAttribute("locationsMapValues", locationsMapValues);
		
		model.addAttribute("firmJson", jsonAllFirms);
		model.addAttribute("practiceJson", jsonAllPractics);
		model.addAttribute("allFirms", firmsSet);
		model.addAttribute("allPracticeArea", practiceList);
		model.addAttribute("allWatchLists", allWatchLists);
		model.addAttribute("orgList", orgList);

		List<UserGroup> allWatchListsDefaultList = null;
		if (currentUser != null)
		{
			allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
			if (allWatchListsDefaultList.size() == 0)
			{
				model.addAttribute("allWatchListsDefaultList", "");
			}
			else
			{
				model.addAttribute("allWatchListsDefaultList", allWatchListsDefaultList.get(0));
			}
		}

		return new ModelAndView("eventform", model);
	
		
	}
	
	@ActionMapping(params = "action=changeSearchCriteria")
	protected void eventSubmitForm(@ModelAttribute("eventSearchModelBean") EventSearchModelBean eventSearchModelBean,
								   ModelMap model, 
								   ActionRequest request, 
								   ActionResponse response) throws Exception
	{
		EventSearchDTO eventSearchDTO = getDTOFromModelBean(model, eventSearchModelBean,request);
		
		User			currentUser		= WebUtil.getCurrentUser(request, userService); 
		
		// The user changed the search criteria, so we will have to persist
		// the changed criteria into the database.
		// However, if this is the Home page, then we will persist these as
		// the search criteria for the specific chart instead, and not
		// generically as the module search criteria.
		String currentPortletId 	= eventSearchModelBean.getPortletId();
		String 	currentPage			= WebUtil.getCurrentPage(request);
		boolean	isHomePage			= ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		boolean clickToViewPage = WebUtil.isClickToViewPage(request);
		if ((currentUser != null) && (currentPortletId != null) && (currentPortletId.length() > 0) && !clickToViewPage)
		{
			if (!isHomePage)
			{
				currentPage = "EVENTS";
				currentPortletId = "EVENTS";
			}
			
			userService.saveUserPreferences(currentUser.getId(), 
											currentPortletId, 
											WebUtil.getJson(eventSearchModelBean), 
											null, 
											currentPage);
		}
				
			
		//set the new SearchDTO back into the model, as userchanged the search criteria
		model.addAttribute("eventSearchDTO", eventSearchDTO);
	}
	
	
	
	@ActionMapping(params = "action=clickToView")
	protected void clickToView(@ModelAttribute("eventSearchModelBean_c2v") EventSearchModelBean eventSearchModelBean_c2v, 
												 ModelMap model, 
												 ActionRequest request,
												 ActionResponse response) throws Exception
	{
		String eventSearchModelBeanJson = WebUtil.getJson(eventSearchModelBean_c2v);
		response.setRenderParameter("eventSearchModelBeanJson", eventSearchModelBeanJson);
		response.setRenderParameter("renderPage", "clickToView");
	}
	
	@RenderMapping(params = "renderPage=clickToView")
	protected ModelAndView clickToView(@RequestParam("eventSearchModelBeanJson") String eventSearchModelBeanJson, 
												 ModelMap model, 
												 RenderRequest request,
												 RenderResponse response) throws Exception
	{
		EventSearchModelBean esmb = WebUtil.getObject(eventSearchModelBeanJson, EventSearchModelBean.class);
		esmb.setAddToSession(false);
		model.addAttribute("eventSearchModelBean", esmb);
		return renderSearchPage(esmb, model, request, response);
	}
	
	
	@RenderMapping(params = "renderPage=searchCriteriaForChart")
	protected ModelAndView renderSearchCriteriaForChart(@RequestParam("chartPortletId") String chartPortletId, 
												 ModelMap model, 
												 RenderRequest request,
												 RenderResponse response) throws Exception
	{
		User currentUser = (model.get("currentUser") != null) ? ((User) model.get("currentUser")) :	WebUtil.getCurrentUser(request, userService);

		EventSearchModelBean esmb = getUserPreference(currentUser.getId(), chartPortletId, ALMConstants.HOME_PAGE);
		if(esmb == null)
		{
			esmb = getDefaultEventSearchModelBean(currentUser);
		}
		esmb.setAddToSession(false);
		model.addAttribute("eventSearchModelBean", esmb);
		model.addAttribute("chartPortletId", chartPortletId);
		return renderSearchPage(esmb, model, request, response);
	}
	
	
	
	
	/**
	 * 
	 * Persists the Search Criteria for a given ChartId
	 * @param eventSearchModelBean use a different model Name ("eventSearchModelBean_chart") rather than the one which goes into session
	 * Because Spring retrieves the model from Session first then updates them with form values submitted by client, we need a new Object rather one from session
	 * @param chartPortletId
	 * @param model
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	
	@ResourceMapping("persistSearchCriteriaForChart")
	protected void persistSearchCriteriaForChartOnHomePage(@ModelAttribute("eventSearchModelBean_chart") EventSearchModelBean eventSearchModelBean, 
												@RequestParam("chartPortletId") String chartPortletId,
												ModelMap model, 
												ResourceRequest request,
												ResourceResponse response) throws Exception
	{
		User currentUser = (model.get("currentUser") != null) ? ((User) model.get("currentUser")) :	WebUtil.getCurrentUser(request, userService);


		userService.saveUserPreferences(currentUser.getId(), chartPortletId, WebUtil.getJson(eventSearchModelBean), null, ALMConstants.HOME_PAGE);

		
	}
}
