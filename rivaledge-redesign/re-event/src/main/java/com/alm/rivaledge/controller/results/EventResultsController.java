package com.alm.rivaledge.controller.results;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.Format;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFHyperlink;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.alm.rivaledge.controller.AbstractEventController;
import com.alm.rivaledge.model.ClickToView;
import com.alm.rivaledge.model.EventSearchModelBean;
import com.alm.rivaledge.persistence.dao.CacheDAO;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.transferobject.EventResultDTO;
import com.alm.rivaledge.transferobject.EventSearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.FastDateFormatFactoryUtil;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.servlet.ServletResponseUtil;

/**
 * Event controller responsible for all operation on Event search form and results
 * screen
 * 
 * @author FL613
 * 
 */
@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "eventSearchModelBean", "eventSearchDTO", "eventSearchResults" })
public class EventResultsController extends AbstractEventController
{
	protected Log _log = LogFactoryUtil.getLog(EventResultsController.class.getName());
	
	@Autowired
	private CacheDAO	cacheDAO;

	/**
     * Comma separated characters
     */
    private static final String CVS_SEPERATOR_CHAR=",";

    /**
     * New line character for CSV file
     */
    private static final String NEW_LINE_CHARACTER="\r\n";
    
    /**
     * firm value for FIRM based grouping 
     */
    private static final String FIRM_SORTING_CONSTANT="9";
    
    /**
     * location value for LOCATION based grouping
     */
    private static final String LOCATION_SORTING_CONSTANT="7";
    
    /**
     * practice area value for PRACTICES AREA based grouping
     */
    private static final String PRACTICESAREA_SORTING_CONSTANT="8";

	@RenderMapping
	public String defaultDetailsView(@ModelAttribute("eventSearchModelBean") EventSearchModelBean eventSearchModelBean,
									 @ModelAttribute("eventSearchDTO") EventSearchDTO eventSearchDTO,
									 ModelMap model,  
									 RenderRequest request, 
									 RenderResponse response) throws Exception
	{
		ClickToView<EventSearchModelBean, EventSearchDTO> c2v = new ClickToView<EventSearchModelBean, EventSearchDTO>();
		c2v.setSearchModelBean(eventSearchModelBean);
		c2v.setSearchDTO(eventSearchDTO);
		honorClick2View(request, model, c2v); // eventSearchModelBean is affected only when you are directed here from C2V
		
		eventSearchDTO = c2v.getSearchDTO();
		eventSearchModelBean = c2v.getSearchModelBean();
		
		return renderResultsPage(eventSearchModelBean, eventSearchDTO, model, request, response);
	}
	
	@RenderMapping(params = "renderPage=clickToView")
	protected String clickToView(@RequestParam("eventSearchModelBeanJson") String eventSearchModelBeanJson, 
												ModelMap model, 
												 RenderRequest request,
												 RenderResponse response) throws Exception
	{
		EventSearchModelBean esmb = WebUtil.getObject(eventSearchModelBeanJson, EventSearchModelBean.class);
		esmb.setAddToSession(false);
		
		model.addAttribute("eventSearchModelBean", esmb);
		
		EventSearchDTO eventSearchDTO = getDTOFromModelBean(model, esmb, request);
		
		return renderResultsPage(esmb, eventSearchDTO, model, request, response);
	}
	
	protected String renderResultsPage(EventSearchModelBean eventSearchModelBean,EventSearchDTO eventSearchDTO, ModelMap model, RenderRequest request,
			RenderResponse response)
	{
		boolean displayMessage = false;
		
		// Call the service implementation to get the search results count
		List<EventResultDTO> eventSearchResults = new ArrayList<EventResultDTO>(); //null is not allowed in session using spring
		
		int resultSize = (eventSearchDTO != null) ? firmService.getEventSearchCount(eventSearchDTO, null) : 0;
		
		if (resultSize > 0)
		{
			// We have results that match this search criteria
			// Return the first page of results
			eventSearchResults = cacheDAO.findEventData(eventSearchDTO);
			
			if(resultSize >= 5000){
				
				displayMessage = true;
				model.addAttribute("displayMessage", displayMessage);
			}
		}

		int lastPage = 0;
		if (resultSize > 0)
		{
			if (resultSize > eventSearchDTO.getResultPerPage())
			{
				lastPage = (int) Math.ceil(new Double(resultSize) / eventSearchDTO.getResultPerPage());
			}
			else
			{
				lastPage = 1;
			}
		}
		// Put the total number of pages into the model
		eventSearchModelBean.setLastPage(lastPage);
		
		// Put the total number of results into the model
		eventSearchModelBean.setTotalResultCount(resultSize);
		// Put the current page size (this is not the page number but the number of records on this page)
		// into the model
		if (eventSearchResults != null && !eventSearchResults.isEmpty())
		{
			eventSearchModelBean.setCurrentPageSize(eventSearchResults.size());
		}
		else
		{
			eventSearchModelBean.setCurrentPageSize(0);
		}
		
		// Put the results into the model
		//PortletSession session = request.getPortletSession();
		//session.setAttribute("eventSearchResults", eventSearchResults, PortletSession.APPLICATION_SCOPE);
		model.addAttribute("eventSearchResults", eventSearchResults);
		
		if(FIRM_SORTING_CONSTANT.equals(String.valueOf(eventSearchDTO.getOrderBy())))
		{
			// firmCounters map store firm name and their total count
			Map<String, Integer> firmsGroupCounters = new TreeMap<String, Integer>();
			// store firm name and their total count as a key-value pair
			for(EventResultDTO eventSearchResult : eventSearchResults)
			{
				if(firmsGroupCounters.containsKey(eventSearchResult.getResultFirm().getCompany()))
				{
					Integer compCounter = firmsGroupCounters.get(eventSearchResult.getResultFirm().getCompany());
					firmsGroupCounters.put(eventSearchResult.getResultFirm().getCompany(), ++compCounter);
				}
				else
				{
					firmsGroupCounters.put(eventSearchResult.getResultFirm().getCompany(), 1);
				}
			}
			model.addAttribute("firmsGroupCounters", firmsGroupCounters);
		}
		
		if(LOCATION_SORTING_CONSTANT.equals(String.valueOf(eventSearchDTO.getOrderBy())))
		{
			// firmCounters map store location name and their total count
			Map<String, Integer> locsGroupCounters = new TreeMap<String, Integer>();
			// store location name and their total count as a key-value pair
			for(EventResultDTO eventSearchResult : eventSearchResults)
			{
				if(locsGroupCounters.containsKey(eventSearchResult.getLocation()))
				{
					Integer compCounter = locsGroupCounters.get(eventSearchResult.getLocation());
					locsGroupCounters.put(eventSearchResult.getLocation(), ++compCounter);
				}
				else
				{
					locsGroupCounters.put(eventSearchResult.getLocation(), 1);
				}
			}
			model.addAttribute("locsGroupCounters", locsGroupCounters);
		}		
		
		if(PRACTICESAREA_SORTING_CONSTANT.equals(String.valueOf(eventSearchDTO.getOrderBy())))
		{
			// firmCounters map store practice area name and their total count
			Map<String, Integer> practiceAreasGroupCounters = new TreeMap<String, Integer>();
			// store practice area name and their total count as a key-value pair
			for(EventResultDTO eventSearchResult : eventSearchResults)
			{
				if(practiceAreasGroupCounters.containsKey(eventSearchResult.getPracticeArea()))
				{
					Integer compCounter = practiceAreasGroupCounters.get(eventSearchResult.getPracticeArea());
					practiceAreasGroupCounters.put(eventSearchResult.getPracticeArea(), ++compCounter);
				}
				else
				{
					practiceAreasGroupCounters.put(eventSearchResult.getPracticeArea(), 1);
				}
			}
			model.addAttribute("practiceAreasGroupCounters", practiceAreasGroupCounters);
		}	
		//return the view 
		return "eventsearchresults";
	}
	

	@RenderMapping(params = "displayPrintPage=true")
	protected String printPublications(@ModelAttribute("eventSearchDTO") EventSearchDTO esDTO,
									   ModelMap map , 
									   RenderRequest request, 
									   RenderResponse response) throws Exception
	{
		List<EventResultDTO> eventPrintResultsList = getSearchResultsForPrintAndExport(esDTO, request);//(List<EventResultDTO>) map.get("eventSearchResults");
		
		map.addAttribute("eventPrintResultsList", eventPrintResultsList);
		WebUtil.populatePrintTimings(map);
		return "printEvents";
	}
	
	/**
	 * Runs the search Query and returns the list without any pagination limit
	 * @param esDTO
	 * @param request
	 * @return
	 */
	private List<EventResultDTO> getSearchResultsForPrintAndExport(EventSearchDTO esDTO, PortletRequest request)
	{

		EventSearchDTO serviceEventSearchDTO = esDTO;
		try
		{
			serviceEventSearchDTO = (EventSearchDTO) esDTO.clone();
		}
		catch (CloneNotSupportedException e)
		{
			_log.error("Cannot clone AttorneySearchDTO. Hence, using the original => " + e.getMessage());
		}

		User currentUser = WebUtil.getCurrentUser(request, userService);

		serviceEventSearchDTO.setPageNumber(0);
		serviceEventSearchDTO.setResultPerPage(WebUtil.getResultSizeLimitForCharts(currentUser));

		List<EventResultDTO> eventResults = cacheDAO.findEventData(serviceEventSearchDTO);

		return eventResults;
	}

	@ResourceMapping("exportFile")
	public void exportFile(@RequestParam String fileType,@RequestParam String groupByColumn,
						  @ModelAttribute("eventSearchDTO") EventSearchDTO esDTO,
						  ResourceRequest request, 
						  ResourceResponse response)
	{
		/* Please assign list of eventResultDTO to eventResultsDTOList */
		//PortletSession session = request.getPortletSession();
		List<EventResultDTO> eventResultsDTOList = getSearchResultsForPrintAndExport(esDTO, request);// (List<EventResultDTO>) session.getAttribute("eventSearchResults", PortletSession.APPLICATION_SCOPE);

		_log.info("---------------------------------- eventResultsDTOList ---------"+eventResultsDTOList.size());
		String fileName = getFileName();

		/* Code for export events to csv file starts */

		if (fileType != null && fileType.equalsIgnoreCase(ALMConstants.CSV_EXT))
		{
			StringBuilder userInfo = new StringBuilder();

			/* Following code set header in mvc file */
			userInfo = getUserInfo(eventResultsDTOList);
			
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet(ALMConstants.EVENT_WORKBOOK_NAME);
			// excel sheet data based on grouping
			sheet = getSheet(sheet, eventResultsDTOList, workbook,groupByColumn);
	        
			fileName = fileName + StringPool.PERIOD + ALMConstants.CSV_EXT;
			//String csv = userInfo.toString();
			//byte[] bytes = csv.getBytes();
			HttpServletRequest servletRequest = PortalUtil.getHttpServletRequest(request);
			HttpServletResponse servletResponse = PortalUtil.getHttpServletResponse(response);
			// convert excel sheet into csv file format
			try
			{
				Iterator  rowIter =  sheet.rowIterator();
		        String csvData="";
		        while (rowIter.hasNext()) 
		        {
		            HSSFRow myRow = (HSSFRow) rowIter.next();
		            for ( int i=0;i<myRow.getLastCellNum();i++)
		            {
		                csvData += getCellData(myRow.getCell(i));
		            }
		                csvData+=NEW_LINE_CHARACTER;
		        }
				ServletResponseUtil.sendFile(servletRequest, servletResponse, fileName, csvData.getBytes(), ContentTypes.TEXT_CSV_UTF8);
			}
			catch (Exception e)
			{
				_log.error("GOT ERROR WHILE SENDING SERVLET RESPONSE " + e.getMessage());
			}
		}
		else
		{
			/* Code for export events to excel file starts */
			FileInputStream fileInputStreamReader = null;
			FileOutputStream fileOutPutStreamReader = null;
			File xlsFile = null;

			try
			{
				xlsFile = FileUtil.createTempFile("xls");
				try
				{
					xlsFile.createNewFile();
				}
				catch (IOException e)
				{
					_log.error("ERROR WHILE CREATING NEW FILE " + e.getMessage());
				}
				if (xlsFile.exists())
				{
					fileInputStreamReader = new FileInputStream(xlsFile);
				}
			}
			catch (FileNotFoundException e1)
			{
				_log.info("Error while creating and reading file " + e1.getMessage());
			}

			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet(ALMConstants.EVENT_WORKBOOK_NAME);

			sheet = getSheet(sheet, eventResultsDTOList, workbook,groupByColumn);

			HttpServletRequest servletRequest = PortalUtil.getHttpServletRequest(request);
			HttpServletResponse servletResponse = PortalUtil.getHttpServletResponse(response);

			fileName = fileName + StringPool.PERIOD + ALMConstants.XLS_EXT;

			try
			{
				fileInputStreamReader.close();

				fileOutPutStreamReader = new FileOutputStream(xlsFile);

				workbook.write(fileOutPutStreamReader);

				fileOutPutStreamReader.close();

				byte[] bytes = read(xlsFile);

				servletResponse.setContentType(ALMConstants.CONTENT_TYPE_XLS);

				ServletResponseUtil.sendFile(servletRequest, servletResponse, fileName, bytes, ALMConstants.CONTENT_TYPE_XLS);
			}
			catch (FileNotFoundException e)
			{
				_log.error("FILE NOT FOUND EXCEPTION " + e.getMessage());
			}
			catch (IOException e)
			{
				_log.error("IO EXCEPTION XLS " + e.getMessage());
			}
			finally
			{

				try
				{
					fileInputStreamReader.close();
					fileOutPutStreamReader.close();
				}
				catch (IOException e)
				{

					_log.error("ERROR WHILE CLOSING INPUT OUTPUT STREAM READER " + e.getMessage());
				}
			}
			/* Code for export events to excel file ends */
		}

	}
	
	private String getFileName()
	{

		String fileName = ALMConstants.EVENT_FILE_NAME;

		Format dateFormatDate = FastDateFormatFactoryUtil.getSimpleDateFormat("MM.dd.yy", Locale.US);

		fileName = fileName + dateFormatDate.format(CalendarFactoryUtil.getCalendar(Locale.US).getTime());

		return fileName;
	}
	
	private StringBuilder getUserInfo(List<EventResultDTO> eventResultsDTOList)
	{

		StringBuilder userInfo = new StringBuilder();

		/* Following code set header in mvc file */
		userInfo.append(ALMConstants.TITLE_NO + StringPool.COMMA);
		userInfo.append(ALMConstants.TITLE_DATE + StringPool.COMMA);
		userInfo.append(ALMConstants.FIRM + StringPool.COMMA);
		userInfo.append(ALMConstants.EVENT + StringPool.COMMA);
		userInfo.append(ALMConstants.PRACTICES + StringPool.COMMA);
		userInfo.append(ALMConstants.ATTORNEY_LOCATION + StringPool.NEW_LINE);

		/* Following code populated list values from second row */
		
		int keyCVS = 0;
	
		for (EventResultDTO eventResultDTO : eventResultsDTOList)
		{
			
			keyCVS++;
			userInfo.append(String.valueOf(keyCVS) + StringPool.COMMA);
			userInfo.append(eventResultDTO.getEventDate() + StringPool.COMMA);
			userInfo.append(eventResultDTO.getResultFirm().getCompany() + StringPool.COMMA);
			userInfo.append(eventResultDTO.getEventTitle() + StringPool.COMMA);
			userInfo.append(eventResultDTO.getPracticeArea() + StringPool.COMMA);
			userInfo.append(eventResultDTO.getLocation() + StringPool.NEW_LINE);

		}

		return userInfo;
	}
	
	private HSSFSheet getSheet(HSSFSheet sheet, List<EventResultDTO> eventResultsDTOList, HSSFWorkbook workbook,String groupByColumn)
	{

		int key = 0;

		Map<String, Object[]> data = new HashMap<String, Object[]>();

		/* Following code set header in excel file */

		data.put(String.valueOf(key), new Object[] {
			ALMConstants.TITLE_NO,
			ALMConstants.TITLE_DATE,
			ALMConstants.FIRM, 
			ALMConstants.EVENT,
			ALMConstants.PRACTICES,
			ALMConstants.ATTORNEY_LOCATION ,
			 });
		
		

		if (eventResultsDTOList != null && eventResultsDTOList.size() > 0)
		{
			
			for (EventResultDTO eventResultDTO : eventResultsDTOList)
			{
				key++;
				
				data.put(
						String.valueOf(key),
						new Object[] { String.valueOf(key),
							String.valueOf(eventResultDTO.getEventDate()),
							String.valueOf(eventResultDTO.getResultFirm().getCompany()),
							String.valueOf(eventResultDTO.getEventTitle()),
							String.valueOf(eventResultDTO.getPracticeArea()),
							String.valueOf(eventResultDTO.getLocation())
							
							});
			}
		}

		int rownum = 0;
		Object[] previousObjArr1 = null;
		Object[] previousObjArr2 = null;
		if(key >0)
		{
			previousObjArr1 = data.get(String.valueOf(1));
		}
		for (key = 0; key < data.size(); key++)
		{
			HSSFRow row = sheet.createRow((rownum));
			Object[] objArr = data.get(String.valueOf(key));
			// grouping event result based on first firm name
			if(key == 1 && FIRM_SORTING_CONSTANT.equals(groupByColumn))
			{
				previousObjArr1 = data.get(String.valueOf(key));
				HSSFCell cell1 = row.createCell(0);
					if (previousObjArr1[2] instanceof String)
					{
						cell1.setCellValue(previousObjArr1[2].toString());
						HSSFCellStyle cellStyle = workbook.createCellStyle();
						HSSFFont hssFont = workbook.createFont();
						hssFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
						cellStyle.setFont(hssFont);
						cell1.setCellStyle(cellStyle);
						
						rownum++;
						row = sheet.createRow((rownum));
					}
			}
			// grouping event result based on remaining firm names
			if(key > 1 && FIRM_SORTING_CONSTANT.equals(groupByColumn))
			{
				previousObjArr1 = data.get(String.valueOf(key));
				previousObjArr2 = data.get(String.valueOf(key-1));
				HSSFCell cell1 = row.createCell(0);
				if(!previousObjArr1[2].equals(previousObjArr2[2]))
				{
					cell1.setCellValue(previousObjArr1[2].toString());
					HSSFCellStyle cellStyle = workbook.createCellStyle();
					HSSFFont hssFont = workbook.createFont();
					hssFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
					cellStyle.setFont(hssFont);
					cell1.setCellStyle(cellStyle);
					
					rownum++;
					row = sheet.createRow((rownum));
				}
					
			}
			// grouping event result based on first location names
			if(key == 1 && LOCATION_SORTING_CONSTANT.equals(groupByColumn))
			{
				previousObjArr1 = data.get(String.valueOf(key));
				HSSFCell cell1 = row.createCell(0);
					if (previousObjArr1[5] instanceof String)
					{
						cell1.setCellValue(previousObjArr1[5].toString());
						HSSFCellStyle cellStyle = workbook.createCellStyle();
						HSSFFont hssFont = workbook.createFont();
						hssFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
						cellStyle.setFont(hssFont);
						cell1.setCellStyle(cellStyle);
						
						rownum++;
						row = sheet.createRow((rownum));
					}
			}
			// grouping event result based on remaining location names
			if(key > 1 && LOCATION_SORTING_CONSTANT.equals(groupByColumn))
			{
				previousObjArr1 = data.get(String.valueOf(key));
				previousObjArr2 = data.get(String.valueOf(key-1));
				HSSFCell cell1 = row.createCell(0);
				if(!(previousObjArr1[5].equals(previousObjArr2[5])) && previousObjArr1[5].equals(""))
				{
					cell1.setCellValue("Location Not Available");
					HSSFCellStyle cellStyle = workbook.createCellStyle();
					HSSFFont hssFont = workbook.createFont();
					hssFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
					cellStyle.setFont(hssFont);
					cell1.setCellStyle(cellStyle);
					
					rownum++;
					row = sheet.createRow((rownum));
				}
				if(!previousObjArr1[5].equals(previousObjArr2[5]) && !previousObjArr1[5].equals(""))
				{
					cell1.setCellValue(previousObjArr1[5].toString());
					HSSFCellStyle cellStyle = workbook.createCellStyle();
					HSSFFont hssFont = workbook.createFont();
					hssFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
					cellStyle.setFont(hssFont);
					cell1.setCellStyle(cellStyle);
					
					rownum++;
					row = sheet.createRow((rownum));
				}
					
			}
			// grouping event result based on first practice area name
			if(key == 1 && PRACTICESAREA_SORTING_CONSTANT.equals(groupByColumn))
			{
				previousObjArr1 = data.get(String.valueOf(key));
				HSSFCell cell1 = row.createCell(0);
					if (previousObjArr1[4] instanceof String)
					{
						cell1.setCellValue(previousObjArr1[4].toString());
						HSSFCellStyle cellStyle = workbook.createCellStyle();
						HSSFFont hssFont = workbook.createFont();
						hssFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
						cellStyle.setFont(hssFont);
						cell1.setCellStyle(cellStyle);
						
						rownum++;
						row = sheet.createRow((rownum));
					}
			}
			// grouping event result based on remaining practice area names
			if(key > 1 && PRACTICESAREA_SORTING_CONSTANT.equals(groupByColumn))
			{
				previousObjArr1 = data.get(String.valueOf(key));
				previousObjArr2 = data.get(String.valueOf(key-1));
				HSSFCell cell1 = row.createCell(0);
				if(!previousObjArr1[4].equals(previousObjArr2[4]))
				{
					cell1.setCellValue(previousObjArr1[4].toString());
					HSSFCellStyle cellStyle = workbook.createCellStyle();
					HSSFFont hssFont = workbook.createFont();
					hssFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
					cellStyle.setFont(hssFont);
					cell1.setCellStyle(cellStyle);
					
					rownum++;
					row = sheet.createRow((rownum));
				}
			}
			
			HSSFCellStyle hyperlinkCellStyle = workbook.createCellStyle();
			HSSFFont hyperlinkFont = workbook.createFont();
			hyperlinkFont.setUnderline(HSSFFont.U_SINGLE);
			hyperlinkFont.setColor(HSSFColor.BLUE.index);
			hyperlinkCellStyle.setFont(hyperlinkFont);
			
			HSSFCellStyle boldCellStyle = workbook.createCellStyle();
			HSSFFont boldCellFont = workbook.createFont();
			boldCellFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
			boldCellStyle.setFont(boldCellFont);
			
			int cellnum = 0;
			if(objArr != null)
			{
				//Writing event result data into excel sheet's cell
				for (Object obj : objArr)
				{
					HSSFCell cell = row.createCell(cellnum);
					if (obj instanceof Date)
					{
						cell.setCellValue((Date) obj);
					}
					else if (obj instanceof Boolean)
					{
						cell.setCellValue((Boolean) obj);
					}
					else if (obj instanceof String)
					{
						String str = (String) obj;
						if (str.startsWith(ALMConstants.HTTP))
						{
							HSSFHyperlink hyperLink = new HSSFHyperlink(HSSFHyperlink.LINK_URL);
							hyperLink.setAddress(str);
							cell.setHyperlink(hyperLink);
							HSSFRichTextString textString = new HSSFRichTextString(str);
							cell.setCellStyle(hyperlinkCellStyle);
							cell.setCellValue(textString);
						}
						else
						{
							HSSFRichTextString textString = new HSSFRichTextString(str);
							if (key == 0)
							{
								cell.setCellStyle(boldCellStyle);
							}
							cell.setCellValue(textString);
						}
	
					}
					else if (obj instanceof Double)
					{
						cell.setCellValue((Double) obj);
					}
					cellnum++;
				} 
			}
			rownum++;
		}

		return sheet;
	}
	
	private byte[] read(File file)
	{

		byte[] buffer = new byte[(int) file.length()];
		InputStream ios = null;
		try
		{
			try
			{
				ios = new FileInputStream(file);
			}
			catch (FileNotFoundException e)
			{

				_log.error("FILE NOT FOUND EXCEPTION " + e.getMessage());
			}
			try
			{
				if (ios.read(buffer) == -1)
				{
				}
			}
			catch (IOException e)
			{

				_log.error("ERROR WHILE READING FILE " + e.getMessage());
			}
		}
		finally
		{
			try
			{
				if (ios != null)
					ios.close();
			}
			catch (IOException e)
			{
			}
		}

		return buffer;
	}
	
	private static String getCellData(HSSFCell myCell) throws Exception{
        String cellData="";
         if ( myCell== null){
             cellData += CVS_SEPERATOR_CHAR;;
         }else{
             switch(myCell.getCellType() ){
                 case  HSSFCell.CELL_TYPE_STRING  :
                 case  HSSFCell.CELL_TYPE_BOOLEAN  :
                          cellData +=  myCell.getRichStringCellValue ()+CVS_SEPERATOR_CHAR;
                          break;
             default:
                 cellData += CVS_SEPERATOR_CHAR;;
             }
         }
         return cellData;
    }
}