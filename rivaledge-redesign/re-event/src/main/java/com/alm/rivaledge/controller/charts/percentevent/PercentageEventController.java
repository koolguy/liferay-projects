package com.alm.rivaledge.controller.charts.percentevent;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.alm.rivaledge.controller.AbstractEventController;
import com.alm.rivaledge.dto.EventPracticeLocation;
import com.alm.rivaledge.model.EventSearchModelBean;
import com.alm.rivaledge.persistence.dao.CacheDAO;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.service.UserService;
import com.alm.rivaledge.transferobject.EventResultDTO;
import com.alm.rivaledge.transferobject.EventSearchDTO;
import com.alm.rivaledge.util.WebUtil;

/**
 * PercentageEvent controller responsible for displaying charts for Event on percentage
 * 
 * @author FL613
 * 
 */
@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "eventSearchDTO" })
public class PercentageEventController extends AbstractEventController
{
	@Autowired
	private FirmService firmService;

	@Autowired
	private UserService userService;
	
	@Autowired
	private CacheDAO	cacheDAO;

	public static final int NO_OF_LOC_PRAC_IN_TOOLTIP = 3;
	
	@RenderMapping
	public ModelAndView defaultChartView(@ModelAttribute("eventSearchModelBean") EventSearchModelBean eventSearchModelBean, ModelMap model, RenderRequest request, RenderResponse response) throws Exception
	{

		 EventSearchDTO eventSearchDTO = getDTOFromModelBean(model, eventSearchModelBean, request);
		 
		EventSearchDTO chartEventSearchDTO = (EventSearchDTO) eventSearchDTO.clone();
		
		// Get the current user from the render request
		User currentUser = WebUtil.getCurrentUser(request, userService);
		
		// Override the max result count;
		// set to either 1,000 or 10,000
		
		
		List<EventResultDTO> eventSearchResults = new ArrayList<EventResultDTO>(); 
		
		//TODO need to calculate total no. of Events
		//int resultSize = (chartEventSearchDTO != null) ? firmService.getEventSearchCount(chartEventSearchDTO, null) : 0;
		/*if(chartEventSearchDTO.getSelectedFirmsOrganizations() != null)
		{
			for(Firm fm : chartEventSearchDTO.getSelectedFirmsOrganizations())
			{
				int co = cacheDAO.findSingleEventDataCount(fm.getCompanyId());
			}
		}*/
		
		int resultSize = firmService.getEventSearchCount(chartEventSearchDTO, null);
		 
		if (resultSize > 0)
		{
			chartEventSearchDTO.setPageNumber(0);
			chartEventSearchDTO.setResultPerPage(WebUtil.getResultSizeLimitForCharts(currentUser));
			// We have results that match this search criteria
			// Return the first page of results
			eventSearchResults = cacheDAO.findEventData(chartEventSearchDTO);
			
			// Set the chart type
			model.put("percentFirmsChartType", "pie");
			// Set the chart title
			model.put("percentFirmsChartTypeTitle", "PERCENT OF EVENTS BY FIRM/ORG");
						
			//Map<Integer, EventDTO> chartMap = new HashMap<Integer, EventDTO>();
			
			List<String> firmsLists = new ArrayList<String>();
			
			// we need a map as we do a lookup of firmId to get Firm Names for
			// chart data
			Map<String, Firm> allSearchResultsFirmMap = new HashMap<String, Firm>();
			
			// Prepare the list of top 5 locations and practices for each
			// firm, based on the number of events
			Map<Integer, EventPracticeLocation> firmPracticesLocationsMap = new HashMap<Integer, EventPracticeLocation>();

			// Calculate the data series
			for (EventResultDTO frDTO : eventSearchResults)
			{
				if ((frDTO.getResultFirm() != null) && (frDTO.getResultFirm().getCompany() != null))
				{
					firmsLists.add(frDTO.getResultFirm().getCompany());
				}
				// Setting firm lookup based on firm name
				if ((frDTO.getResultFirm() != null) && (frDTO.getResultFirm().getCompany() != null) && (frDTO.getResultFirm().getCompany().length() > 0))
				{
					allSearchResultsFirmMap.put(frDTO.getResultFirm().getCompany(), frDTO.getResultFirm());
				}
				EventPracticeLocation thisPracticeLocation = null;
							
				// Is this result's firm in the map or not?
				if (firmPracticesLocationsMap.containsKey(frDTO.getCompanyId()))
				{
					// Existing entry
					thisPracticeLocation = firmPracticesLocationsMap.get(frDTO.getCompanyId());
				}
				else
				{
					thisPracticeLocation = new EventPracticeLocation();
				}
							
				// Add the current location and practice
				thisPracticeLocation.addLocation(frDTO.getLocation());
				thisPracticeLocation.addPractice(frDTO.getPracticeArea());
							
				// Put this back in the map
				firmPracticesLocationsMap.put(frDTO.getResultFirm().getCompanyId(), thisPracticeLocation);
			}
			
			Set<String> 		 hashSetFirms 	= new HashSet<String>(firmsLists);
			Map<String, Integer> sortedMap 		= new HashMap<String, Integer>();
			for (String thisFirm : hashSetFirms)
			{
				int counter = Collections.frequency(firmsLists, thisFirm);
				sortedMap.put(thisFirm, counter);
			}
			
			StringBuffer sbLocations = new StringBuffer();
			StringBuffer sbPractices = new StringBuffer();
			
			// Generate the series data
			StringBuffer sbFirm 	 = new StringBuffer();
			StringBuffer sbFirmCount = new StringBuffer();
			for (String firmName : sortedMap.keySet())
			{
				if (sbFirm.length() > 0)
				{
					sbFirm.append(", ");
					sbFirmCount.append(", ");
				}
				sbFirm.append("\"" + firmName + "\"");
				sbFirmCount.append(sortedMap.get(firmName));
				// Adding Events for practice areas
				Map<String, Integer> topPractices = (firmPracticesLocationsMap.get(allSearchResultsFirmMap.get(firmName).getCompanyId()) != null) ? firmPracticesLocationsMap.get(allSearchResultsFirmMap.get(firmName).getCompanyId()).getTopPractices(NO_OF_LOC_PRAC_IN_TOOLTIP) : null;
				
				if (topPractices == null)
				{
					sbPractices.append("[\"Practice Area Not Available\"]");
				}
				else
				{
					sbPractices.append("[");
					
					for (String tKey : topPractices.keySet())
					{
						if (!sbPractices.toString().endsWith("["))
						{
							sbPractices.append(", ");
						}
						sbPractices.append("\"").append(" ").append(tKey).append("\"");
					}
					sbPractices.append("],");
				}
				// Adding Events for locations 
				Map<String, Integer> topLocations = (firmPracticesLocationsMap.get(allSearchResultsFirmMap.get(firmName).getCompanyId()) != null) ? firmPracticesLocationsMap.get(allSearchResultsFirmMap.get(firmName).getCompanyId()).getTopLocations(NO_OF_LOC_PRAC_IN_TOOLTIP) : null;
				if (topLocations == null)
				{
					sbLocations.append("[\"Unknown Location\"]");
				}
				else
				{
					sbLocations.append("[");
					for (String tKey : topLocations.keySet())
					{
						if (!sbLocations.toString().endsWith("["))
						{
							sbLocations.append(", ");
						}
						sbLocations.append("\"").append(" ").append(tKey).append("\"");
					}
					sbLocations.append("],");
				}
			}
			
			model.put("percentCountByFirmList", sbFirmCount.toString());
			model.put("percentFirmsNameList", sbFirm.toString());
			model.put("amtByFirmLocationsLists", sbLocations.toString());
			model.put("amtByFirmPracticesLists", sbPractices.toString());
		}

		return new ModelAndView("percentfirms");
	}
	
	/*
	 * Following method remove rendering portlet from the page. And redirects to same page.
	 *
	 */

	 @ActionMapping(params = "action=removePortleFromThePage")
	 public void removePortletFromPage(ActionRequest request, ActionResponse response) throws Exception
	 {
		 WebUtil.removePortlet(request);
	 }
}
