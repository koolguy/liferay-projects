package com.alm.rivaledge.controller.charts.amounteventdate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.alm.rivaledge.controller.AbstractEventController;
import com.alm.rivaledge.model.EventSearchModelBean;
import com.alm.rivaledge.persistence.dao.CacheDAO;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.transferobject.EventResultDTO;
import com.alm.rivaledge.transferobject.EventSearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;

/**
 * AmountEventFirm controller responsible for displaying charts for Event based
 * on dates
 * 
 * @author FL613
 * 
 */
@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "eventSearchModelBean" })
public class AmountEventDateController extends AbstractEventController
{

	@Autowired
	private CacheDAO	cacheDAO;

	
	private SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yy");
	
	// UNKNOWN_VALUE used for comparing blank value of location and practice area
	public static final String UNKNOWN_VALUE = "";
	
	// NO_OF_TOP_LOC_PRAC_IN_TOOLTIP used to define number of locations and practice areas for popup-chart
	public static final int NO_OF_TOP_LOC_PRAC_IN_TOOLTIP = 3;
	
	
	
	
	@RenderMapping
	public ModelAndView defaultChartView(@ModelAttribute("eventSearchModelBean") EventSearchModelBean eventSearchModelBean,
										 ModelMap model,
										 RenderRequest request, 
										 RenderResponse response) throws Exception
	{

		EventSearchDTO eventSearchDTO = getDTOFromModelBean(model, eventSearchModelBean, request);
		
		EventSearchDTO chartEventSearchDTO = (EventSearchDTO) eventSearchDTO.clone();
		
		// Get the current user from the render request
		User currentUser = WebUtil.getCurrentUser(request, userService);
		
		//Niranjan -> To whom so ever written this. What the hell happening here? Why are u fetch the same user again by Id?
		
		/*if (currentUser != null)
		{
			// Get this Liferay/DB_Reg user
			currentUser = userService.getUserById(currentUser.getId());
		}*/

		// Call the service implementation to get the search results count
		List<EventResultDTO> eventSearchResults = new ArrayList<EventResultDTO>();

		int resultSize = (chartEventSearchDTO != null) ? firmService.getEventSearchCount(chartEventSearchDTO, currentUser) : 0;

		if (resultSize > 0)
		{
			
			chartEventSearchDTO.setPageNumber(0);
			chartEventSearchDTO.setResultPerPage(WebUtil.getResultSizeLimitForCharts(currentUser));
			// We have results that match this search criteria
			// Return all the results
			// TODO Change this to eventService and pass in the currentUser
			
			eventSearchResults = cacheDAO.findEventData(chartEventSearchDTO);

			// Set the chart type
			model.addAttribute("chartType", "column");

			// Set the chart title
			model.addAttribute("chartTitle", "TOTAL EVENTS BY DATE");

			Comparator<EventResultDTO> c = new Comparator<EventResultDTO>()
			{
				public int compare(EventResultDTO dto1, EventResultDTO dto2) {
					
					return WebUtil.compare(dto1.getEventDate(), dto2.getEventDate(), false); // true - descending 
				};
			};
			
			Collections.sort(eventSearchResults, c); //Sort on Event Dates
		
		
			// Calculate the data series
			Map<String, Integer> eventsMap = new LinkedHashMap<String, Integer>(); //Use linked hashmap to maintain the order of keys as they are sorted on dates
			Map<String, List<String>> eventsTitleMap = new LinkedHashMap<String, List<String>>(); //Use linked hashmap to maintain the order of keys as they are sorted on dates
			Map<Date,Set<String>> locSetCharts = new HashMap<Date,Set<String>>(); // Use locSetCharts to keep top three locations for a particular date as key-value pair  
			Map<Date,Set<String>> practiceAreaSetCharts = new HashMap<Date,Set<String>>(); // Use practiceAreaSetCharts to keep top three practice areas for a particular date as key-value pair
			
			for (EventResultDTO frDTO : eventSearchResults)
			{
				String eventDate = "Unknown Date";
				if ((frDTO.getEventDate() != null))
				{
					eventDate = formatter.format(frDTO.getEventDate());
				}
				if (eventsMap.containsKey(eventDate))
				{
					Integer eventCtr = eventsMap.get(eventDate);
					eventsMap.put(eventDate, (eventCtr + 1));
					
					List<String> strEvents = eventsTitleMap.get(eventDate);
					if(strEvents!= null && strEvents.size()<5){
						strEvents.add(frDTO.getEventTitle());
						eventsTitleMap.put(eventDate, strEvents);
					}
				}
				else
				{
					eventsMap.put(eventDate, 1);
					List<String> strEvents = new ArrayList<String>();
					strEvents.add(frDTO.getEventTitle());
					eventsTitleMap.put(eventDate, strEvents);
				}
				
				//Storing three locations for a specific date 
				if(locSetCharts.containsKey(frDTO.getEventDate()))
				{
					Set<String> valueList = locSetCharts.get(frDTO.getEventDate());
					if(valueList.size() < NO_OF_TOP_LOC_PRAC_IN_TOOLTIP && !valueList.contains(frDTO.getLocation().toString()))
					{
						if(UNKNOWN_VALUE.equals(frDTO.getLocation().toString()))
						{
							valueList.add("\"" + "Location Not Available" + "\"");
						}
						else
						{
							if(frDTO.getLocation().toString().contains("-"))
							{
								valueList.add("\"" + frDTO.getLocation().toString().substring(3, frDTO.getLocation().toString().length()) + "\"");
							}
							else
							{
								valueList.add("\"" + frDTO.getLocation().toString() + "\"");
							}
						}
						
						locSetCharts.put(frDTO.getEventDate(), valueList);
					}
				}
				else
				{
					Set<String> tempLoc = new HashSet<String>();
					if(UNKNOWN_VALUE.equals(frDTO.getLocation().toString()))
					{
						tempLoc.add("\"" + "Location Not Available" + "\"");
					}
					else
					{
						tempLoc.add("\"" + frDTO.getLocation().toString() + "\"" );
					}
					locSetCharts.put(frDTO.getEventDate(), tempLoc);
				}
			
				//Storing three practice areas for a specific date
					if(practiceAreaSetCharts.containsKey(frDTO.getEventDate()))
					{
						Set<String> valueList = practiceAreaSetCharts.get(frDTO.getEventDate());
						if(valueList.size() < NO_OF_TOP_LOC_PRAC_IN_TOOLTIP && !valueList.contains(frDTO.getPracticeArea().toString()))
						{
							if(UNKNOWN_VALUE.equals(frDTO.getPracticeArea().toString()))
							{
								valueList.add("\"" + "Practice Area Not Available" + "\"");
							}
							else
							{
								valueList.add("\"" + frDTO.getPracticeArea().toString() + "\"");
							}
							
							practiceAreaSetCharts.put(frDTO.getEventDate(), valueList);
						}
					}
					else
					{
						Set<String> tempPractice = new HashSet<String>();
						tempPractice.add("\"" + frDTO.getPracticeArea().toString() + "\"" );
						practiceAreaSetCharts.put(frDTO.getEventDate(), tempPractice);
					}
			}
			//StringBuffer sbLocs used for storing filtered location
			StringBuffer sbLocs = new StringBuffer();
			//StringBuffer sbPracticeAreas used for storing filtered practice areas
			StringBuffer sbPracticeAreas = new StringBuffer();
			sbLocs.append("[");
			for (Map.Entry<Date,Set<String>> entry : locSetCharts.entrySet()) {
				if(entry.getValue().toString() != null && sbLocs.toString() != null)
				{
					sbLocs.append(",");
				}
				sbLocs.append(entry.getValue().toString());
			 }
			sbLocs.append("]");
			sbPracticeAreas.append("[");
			for (Map.Entry<Date,Set<String>> entry : practiceAreaSetCharts.entrySet()) {
				sbPracticeAreas.append(entry.getValue().toString());
				if(entry.getValue().toString() != null)
				{
					sbPracticeAreas.append(",");
				}
				
			 }
			sbPracticeAreas.append("]");
			
			List<String> topEventsKey = WebUtil.top(new ArrayList<String>(eventsMap.keySet()), ALMConstants.DEFAULT_NO_OF_COLUMNS_CHART); 
			// Generate the series data
			StringBuffer sbDate 		= new StringBuffer();
			StringBuffer sbDateCounter 	= new StringBuffer();
			StringBuffer sbEventsTitles = new StringBuffer();
		
			for (String thisKey : topEventsKey)
			{
				if (sbDate.length() > 0)
				{
					sbDate.append(", ");
					sbDateCounter.append(", ");
					sbEventsTitles.append(", ");
				}
				sbDate.append("\"" + thisKey + "\"");
				sbDateCounter.append(eventsMap.get(thisKey));
				StringBuffer tempTitleString = new StringBuffer();
				List<String> strEvents = eventsTitleMap.get(thisKey);
				for(String title: strEvents){
					title= title.replaceAll("\"", "");
					title= title.replaceAll("\'", "");
					title = title.replaceAll("(\r\n|\n)", " ");
					//title = title.length() > 30 ? title.substring(0,30) + ".." : title;
					title = "\'" + title + "\'";
					if(tempTitleString.length()>0){
						tempTitleString.append(",");
					}
					tempTitleString.append( title );
				}
				sbEventsTitles.append("[" + tempTitleString.toString()+ "]");
				tempTitleString = new StringBuffer();
			}
			model.addAttribute("datesCount", sbDateCounter.toString());
			model.addAttribute("dateList", sbDate.toString());
			model.addAttribute("eventsTitles", sbEventsTitles.toString());
			model.addAttribute("locSetChart",  sbLocs.toString()); // locSetChart will be keep locations for popup chart.
			model.addAttribute("practiceAreaSetChart", sbPracticeAreas.toString()); // practiceAreaSetChart will be keep practiceAreas for popup chart.


		}

		return new ModelAndView("amountbydate");
	}
	
	/*
	 * Following method remove rendering portlet from the page. And redirects to same page.
	 *
	 */

	 @ActionMapping(params = "action=removePortleFromThePage")
	 public void removePortletFromPage(ActionRequest request, ActionResponse response) throws Exception
	 {
		 WebUtil.removePortlet(request);
	 }
}
