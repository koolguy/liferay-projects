package com.alm.rivaledge.controller.charts.amounteventlocations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.alm.rivaledge.controller.AbstractEventController;
import com.alm.rivaledge.model.EventSearchModelBean;
import com.alm.rivaledge.persistence.dao.CacheDAO;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.service.UserService;
import com.alm.rivaledge.transferobject.EventResultDTO;
import com.alm.rivaledge.transferobject.EventSearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.util.StringPool;

/**
 * AmountEventLocation controller responsible for displaying charts for Event based on locations
 * 
 * @author FL613
 * 
 */
@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "eventSearchModelBean" })
public class AmountEventLocationController extends AbstractEventController
{
	@Autowired
	private FirmService firmService;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private CacheDAO	cacheDAO;

	

	@RenderMapping
	public ModelAndView defaultChartView(@ModelAttribute("eventSearchModelBean") EventSearchModelBean eventSearchModelBean, ModelMap model, RenderRequest request, RenderResponse response) throws Exception
	{
	
		EventSearchDTO eventSearchDTO = getDTOFromModelBean(model, eventSearchModelBean, request);
		
		EventSearchDTO chartEventSearchDTO = (EventSearchDTO) eventSearchDTO.clone();

		// Get the current user from the render request
		User currentUser = WebUtil.getCurrentUser(request, userService);
		
		//Niranjan -> To whom so ever written this. What the hell happening here? Why are u fetch the same user again by Id?
		
		/*if (currentUser != null)
		{
			// Get this Liferay/DB_Reg user
			currentUser = userService.getUserById(currentUser.getId());
		}*/

		// Call the service implementation to get the search results count
		List<EventResultDTO> eventSearchResults = new ArrayList<EventResultDTO>(); 

		int resultSize = (chartEventSearchDTO != null) ? firmService.getEventSearchCount(chartEventSearchDTO, currentUser) : 0;
		if (resultSize > 0)
		{
			
			chartEventSearchDTO.setPageNumber(0);
			chartEventSearchDTO.setResultPerPage(WebUtil.getResultSizeLimitForCharts(currentUser));
			// We have results that match this search criteria
			// Return the first page of results
			// Replace with EventService; controllers must not call DAOs
			// directly
			eventSearchResults = cacheDAO.findEventData(chartEventSearchDTO);

			// Set the chart type
			model.put("chartType", "column");

			// Set the chart title
			model.put("chartTitle", "NUMBER OF EVENTS BY LOCATION");
			
			Map<String, Integer> locationMap = new HashMap<String, Integer>();
		
			Map<String, List<EventResultDTO>> locationEventResultMap = new HashMap<String, List<EventResultDTO>>();
			// Calculate the data series
			for (EventResultDTO frDTO : eventSearchResults)
			{
				String eventLocation = "Unknown Location";
				if ((frDTO.getLocation() != null) && (frDTO.getLocation().length() > 0))
				{
					eventLocation = frDTO.getLocation();
				}
				
				Integer currentCount = 0;
				List<EventResultDTO> dtoList = null;
				if (locationMap.containsKey(eventLocation))
				{
					currentCount = locationMap.get(eventLocation) + 1;
					dtoList = locationEventResultMap.get(eventLocation);
				}
				else
				{
					currentCount = 1;
					dtoList = new ArrayList<EventResultDTO>();
					locationEventResultMap.put(eventLocation, dtoList);
				}
				
				locationMap.put(eventLocation, currentCount); 
				dtoList.add(frDTO);
			}
			
			// Sorting
			// We want to sort by value and not by key
			// We will use the TreeBidiMap for this
			Map<String, Integer> sortedLocationMap = WebUtil.sortMapByIntegerValue(locationMap, ALMConstants.DEFAULT_NO_OF_COLUMNS_CHART, true);

			// Generate the series data
			StringBuffer sbLocations 	= new StringBuffer();
			StringBuffer sbEventCounts	= new StringBuffer();
			StringBuffer sbToolTip		= new StringBuffer();
			
			Comparator<EventResultDTO> c = new Comparator<EventResultDTO>()
			{
				public int compare(EventResultDTO dto1, EventResultDTO dto2) {
					
					return WebUtil.compare(dto1.getEventDate(), dto2.getEventDate(), true); // recent date first
				};
			};
					
			for (String thisKey : sortedLocationMap.keySet())
			{
				if (sbLocations.length() > 0)
				{
					sbLocations.append(", ");
					sbEventCounts.append(", ");
					sbToolTip.append(", ");
				}
				sbLocations.append(StringPool.QUOTE + thisKey + StringPool.QUOTE);
				sbEventCounts.append(sortedLocationMap.get(thisKey));
				
				sbToolTip.append("[");
			
				List<EventResultDTO> eventTooltipList = new ArrayList<EventResultDTO>(locationEventResultMap.get(thisKey));
				
						
				Collections.sort(eventTooltipList, c); //Sort on Event Dates
						
				for (EventResultDTO tooltip : WebUtil.top(eventTooltipList, ALMConstants.NO_OF_EVENTS_IN_TOOLTIP))
				{
					if (!sbToolTip.toString().endsWith("["))
					{
						sbToolTip.append(", ");
					}
					sbToolTip.append(StringPool.QUOTE).append(WebUtil.cleanString(tooltip.getEventTitle())).append(StringPool.QUOTE);
				}
				
				sbToolTip.append("]");
			}
			
			model.put("eventsByLocationOffices", sbLocations.toString());
			model.put("eventsByLocationEventCount", sbEventCounts.toString());
			model.put("eventsByLocationEventToolTip", sbToolTip.toString());
		}
		return new ModelAndView("amounteventlocation");
	}
	
	/*
	 * Following method remove rendering portlet from the page. And redirects to same page.
	 *
	 */

	 @ActionMapping(params = "action=removePortleFromThePage")
	 public void removePortletFromPage(ActionRequest request, ActionResponse response) throws Exception
	 {
		 WebUtil.removePortlet(request);
	 }
}
