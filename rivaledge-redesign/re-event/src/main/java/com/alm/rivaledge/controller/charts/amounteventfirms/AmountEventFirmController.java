package com.alm.rivaledge.controller.charts.amounteventfirms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.alm.rivaledge.controller.AbstractEventController;
import com.alm.rivaledge.dto.EventPracticeLocation;
import com.alm.rivaledge.model.EventSearchModelBean;
import com.alm.rivaledge.model.chart.BaseChartModelBean.ChartType;
import com.alm.rivaledge.model.chart.BaseChartModelBean.ComparisonDataType;
import com.alm.rivaledge.model.chart.BaseChartModelBean.FirmDataType;
import com.alm.rivaledge.model.chart.StandardChartModelBean;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.service.EventService;
import com.alm.rivaledge.service.WatchlistService;
import com.alm.rivaledge.transferobject.EventResultDTO;
import com.alm.rivaledge.transferobject.EventSearchDTO;
import com.alm.rivaledge.transferobject.UserGroupDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;

/**
 * AmountEventFirm controller responsible for displaying charts for Event based
 * on firms
 * 
 * @author FL613
 */
@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "eventSearchModelBean", "amtByFirmChartModelBean" })
public class AmountEventFirmController extends AbstractEventController
{
	private Log					_log	= LogFactoryUtil.getLog(AmountEventFirmController.class.getName());
	public static final int NO_OF_LOC_PRAC_IN_TOOLTIP = 3;

	@Autowired
	protected WatchlistService	watchlistService;

	@Autowired
	private EventService		eventService;

	@ModelAttribute
	public void populateChartModelBean(ModelMap model, PortletRequest request)
	{
		StandardChartModelBean 	amtByFirmChartModelBean = null;
		String					currentPortletId			 = WebUtil.getCurrentPortletId(request);
		String					currentPage				 	 = WebUtil.getCurrentPage(request);
		boolean					isHomePage				 	 = ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		User 					currentUser 			 	 = (model.get("currentUser") != null) ? ((User) model.get("currentUser")) : WebUtil.getCurrentUser(request, userService); 
		if (!isHomePage)
		{
			currentPage			= "EVENTS";
		}
		
		amtByFirmChartModelBean = getUserChartPreference(currentUser.getId(), currentPortletId, currentPage);
		
		if (amtByFirmChartModelBean == null)
		{
			// Create and initialize the chartModelBean
			amtByFirmChartModelBean = new StandardChartModelBean();
			amtByFirmChartModelBean.init();
			amtByFirmChartModelBean.setChartType(ChartType.VERTICAL_BAR.getValue());
		}
			// Bind this bean to the session
			model.put("amtByFirmChartModelBean", amtByFirmChartModelBean);
		
	}

	@RenderMapping
	public ModelAndView defaultChartView(@ModelAttribute("eventSearchModelBean") EventSearchModelBean eventSearchModelBean, 
										 @ModelAttribute("amtByFirmChartModelBean") StandardChartModelBean afcmb,
										 ModelMap model, 
										 RenderRequest request, 
										 RenderResponse response) throws Exception
	{
		
		EventSearchDTO eventSearchDTO = getDTOFromModelBean(model, eventSearchModelBean, request);
		// clone the DTO as the changes to the DTO should not reflect elsewhere
		EventSearchDTO chartEventSearchDTO = (EventSearchDTO) eventSearchDTO.clone();

		// Get the current user from the render request
		User currentUser = WebUtil.getCurrentUser(request, userService);
		//Niranjan -> To whom so ever written this. What the hell happening here? Why are u fetch the same user again by Id?
		/*
		if (currentUser != null)
		{
			// Get this user from LMD
			currentUser = userService.getUserById(currentUser.getId());
		}*/


		// Call the service implementation to get the search results count
		List<EventResultDTO> defaultSearchResults = new ArrayList<EventResultDTO>();

		int resultSize = (chartEventSearchDTO != null) ? eventService.getEventDataCount(chartEventSearchDTO, currentUser) : 0;

		if (resultSize > 0)
		{
			int resultPerPage = WebUtil.getResultSizeLimitForCharts(currentUser);
			chartEventSearchDTO.setResultPerPage(resultPerPage);
			// We have results that match this search criteria
			defaultSearchResults = eventService.getEventData(chartEventSearchDTO, currentUser);
			
			// Map holds <firmId(as String), eventCount>
			Map<String, Integer> eventsMap = getEventCountMapFromSearchResults(defaultSearchResults);

			// we need a map as we do a lookup of firmId to get Firm Names for
			// chart data
			Map<String, Firm> allSearchResultsFirmMap = new HashMap<String, Firm>();
			
			// Prepare the list of top 5 locations and practices for each
			// firm, based on the number of events
			Map<Integer, EventPracticeLocation> firmPracticesLocationsMap = new HashMap<Integer, EventPracticeLocation>();

			for (EventResultDTO frDTO : defaultSearchResults)
			{
				if ((frDTO.getResultFirm() != null) && (frDTO.getResultFirm().getCompany() != null) && (frDTO.getResultFirm().getCompany().length() > 0))
				{
					allSearchResultsFirmMap.put("" + frDTO.getCompanyId(), frDTO.getResultFirm());
				}
				EventPracticeLocation thisPracticeLocation = null;
				
				// Is this result's firm in the map or not?
				if (firmPracticesLocationsMap.containsKey(frDTO.getCompanyId()))
				{
					// Existing entry
					thisPracticeLocation = firmPracticesLocationsMap.get(frDTO.getCompanyId());
				}
				else
				{
					thisPracticeLocation = new EventPracticeLocation();
				}
				
				// Add the current location and practice
				thisPracticeLocation.addLocation(frDTO.getLocation());
				thisPracticeLocation.addPractice(frDTO.getPracticeArea());
				
				// Put this back in the map
				firmPracticesLocationsMap.put(frDTO.getCompanyId(), thisPracticeLocation);
			}
			
			// Map holds <firmId(as String), eventCount> of refined results
			Map<String, Integer> refinedEventsMap = refineSearchResultsByLimit(afcmb, eventsMap);

			ChartData chartData = new ChartData();

			prepareComparisonData(currentUser, afcmb, chartEventSearchDTO, defaultSearchResults, chartData);

			// Set the chart type
			model.put("amtByFirmChartType", afcmb.getChartType());

			// Set the chart title
			model.put("amtByFirmChartTitle", "TOTAL EVENTS BY FIRM");
			
			StringBuffer sbLocations = new StringBuffer();
			StringBuffer sbPractices = new StringBuffer();
			
			List<String> allLocationNameLists = new ArrayList<String>();
			List<String> allPracticeAreaLists = new ArrayList<String>();
			// Generate the series data
			for (String thisKey : refinedEventsMap.keySet())
			{
				if (chartData.sbFirm.length() > 0)
				{
					chartData.sbFirm.append(", ");
					chartData.sbFirmCount.append(", ");
				}
				if (sbLocations.length() > 0)
				{
					sbLocations.append(", ");
					sbPractices.append(", ");
				}
				chartData.sbFirm.append("\"" + allSearchResultsFirmMap.get(thisKey).getCompany() + "\""); 
				chartData.sbFirmCount.append(refinedEventsMap.get(thisKey));
			
				Map<String, Integer> topPractices = (firmPracticesLocationsMap.get(allSearchResultsFirmMap.get(thisKey).getCompanyId()) != null) ? firmPracticesLocationsMap.get(allSearchResultsFirmMap.get(thisKey).getCompanyId()).getTopPractices(NO_OF_LOC_PRAC_IN_TOOLTIP) : null;
				
				if (topPractices == null)
				{
					sbPractices.append("[\"Practice Area Not Available\"]");
				}
				else
				{
					sbPractices.append("[");
					
					for (String tKey : topPractices.keySet())
					{
						if (!sbPractices.toString().endsWith("["))
						{
							sbPractices.append(", ");
						}
						sbPractices.append("\"").append(" ").append(tKey).append("\"");
						//List used for storing all practice areas
						allPracticeAreaLists.add(tKey);
					}
					sbPractices.append("]");
				}
				
				Map<String, Integer> topLocations = (firmPracticesLocationsMap.get(allSearchResultsFirmMap.get(thisKey).getCompanyId()) != null) ? firmPracticesLocationsMap.get(allSearchResultsFirmMap.get(thisKey).getCompanyId()).getTopLocations(NO_OF_LOC_PRAC_IN_TOOLTIP) : null;
				
				if (topLocations == null)
				{
					sbLocations.append("[\"Unknown Location\"]");
				}
				else
				{
					sbLocations.append("[");
					for (String tKey : topLocations.keySet())
					{
						if (!sbLocations.toString().endsWith("["))
						{
							sbLocations.append(", ");
						}
						sbLocations.append("\"").append(" ").append(tKey).append("\"");
						//List used for storing all locations
						allLocationNameLists.add(tKey);
					}
					sbLocations.append("]");
				}
			}
			// Generate all unique locations set
			Set<String> uniqueLocationNameLists = new HashSet<String>(allLocationNameLists);
			// Generate all unique practice areas set
			Set<String> uniquePracticeAreaLists = new HashSet<String>(allPracticeAreaLists);
			
			Map<Integer,String> mapUniqueLocationNameLists = new TreeMap<Integer,String>(Collections.reverseOrder());
			Map<Integer,String> mapUniquePracticeAreaLists = new TreeMap<Integer,String>(Collections.reverseOrder());
			// Count total no. of occurrence for each location
			for (String key : uniqueLocationNameLists) {
				if("".equals(key))
				{
					mapUniqueLocationNameLists.put(Collections.frequency(allLocationNameLists, key), "Location Not Available");
				}
				else
				{
					mapUniqueLocationNameLists.put(Collections.frequency(allLocationNameLists, key), key);
				}
			}
			// Count total no. of occurrence for each practice areas
			for (String key : uniquePracticeAreaLists) {
				mapUniquePracticeAreaLists.put(Collections.frequency(allPracticeAreaLists, key), key);
			}
			// Get a set of all the entries (key - value pairs) contained in the TreeMap for location and practice area
		    Collection<Entry<Integer, String>>  locationEntrySet = mapUniqueLocationNameLists.entrySet();
		    Collection<Entry<Integer, String>>  practiceAreaEntrySet = mapUniquePracticeAreaLists.entrySet();
				  
			// Obtain an Iterator for the location and practice area entries Set 
			Iterator<Entry<Integer, String>> locationIterator = locationEntrySet.iterator();
			Iterator<Entry<Integer, String>> practiceAreaIterator = practiceAreaEntrySet.iterator();
			int indexCounter = 1;
			List<String> avgLocationList = new ArrayList<String>();
			List<String> avgPracticeAreaList = new ArrayList<String>();
			// Iterate through location TreeMap entries and select TOP 3 elements
			while(locationIterator.hasNext())
			{
					Entry<Integer, String> eachLocation = locationIterator.next(); 
					if(eachLocation != null && indexCounter <= 3)
					{
						avgLocationList.add("\""+ eachLocation.getValue().toString()+"\"");
						indexCounter++;
					}
					else
					{
						break;
					}
			}
			indexCounter = 1;
			// Iterate through practice area TreeMap entries and select TOP 3 elements
			while(practiceAreaIterator.hasNext())
			{
				Entry<Integer, String> eachPracticeArea = practiceAreaIterator.next(); 
				if(eachPracticeArea != null && indexCounter <= 3)
				{
					avgPracticeAreaList.add("\""+ eachPracticeArea.getValue().toString()+"\"");
					indexCounter++;
				}
				else
				{
					break;
				}	
			}	 
			
			List<UserGroupDTO> allWatchLists = new ArrayList<UserGroupDTO>();

			if (watchlistService != null && currentUser != null)
			{
				allWatchLists = (List<UserGroupDTO>) watchlistService.getAllFirmWatchList(currentUser.getId());
			}

			model.put("allRankingList", ALMConstants.RIVALEDGE_RANKING_LIST);
			model.put("allWatchLists", allWatchLists);
			model.put("allSearchResultsFirmList", allSearchResultsFirmMap.values());

			model.put("firmsNameList", chartData.sbFirm.toString());
			model.put("firmsCountByFirmList", chartData.sbFirmCount.toString());
			model.put("amtByFirmCompareCount", chartData.compareCount);
			model.put("amtByFirmLocations", sbLocations.toString());
			model.put("amtByFirmPractices", sbPractices.toString());
			model.put("avgLocationList", avgLocationList.toString());
			model.put("avgPracticeAreaList", avgPracticeAreaList.toString());
		}

		return new ModelAndView("amountbyfirm");
	}

	private Map<String, Integer> getEventCountMapFromSearchResults(List<EventResultDTO> eventSearchResults)
	{
		Map<String, Integer> eventsMap = new HashMap<String, Integer>();

		// Calculate the data series
		for (EventResultDTO frDTO : eventSearchResults)
		{
			if ((frDTO.getResultFirm() != null) && (frDTO.getResultFirm().getCompany() != null) && (frDTO.getResultFirm().getCompany().length() > 0))
			{
				String key = "" + frDTO.getResultFirm().getCompanyId();
				if (eventsMap.containsKey(key))
				{
					Integer currentCount = eventsMap.get(key);
					eventsMap.put(key, (currentCount + 1));
				}
				else
				{
					eventsMap.put(key, 1);
				}
			}
		}
		return eventsMap;
	}

	private Map<String, Integer> refineSearchResultsByLimit(StandardChartModelBean afcmb, Map<String, Integer> eventsMap)
	{

		// Map holds <firmId(as String), eventCount> of refined results
		Map<String, Integer> refinedEventsMap = new HashMap<String, Integer>();

		// User selected individual firms
		if (FirmDataType.FIRM.getValue().equals(afcmb.getLimitType()) && afcmb.getSearchResultsFirmList() != null && !afcmb.getSearchResultsFirmList().isEmpty())
		{
			// run through the selected firmIds
			for (String firmId : afcmb.getSearchResultsFirmList())
			{
				if (eventsMap.containsKey(firmId))
				{
					// add only user Selected firms to the refined Map
					refinedEventsMap.put(firmId, eventsMap.get(firmId));
				}

			}
		}
		else
		{
			// user select either top or bottom results
			if (FirmDataType.TOP_15.getValue().equals(afcmb.getLimitType())) 
			{
				refinedEventsMap = WebUtil.sortMapByIntegerValue(eventsMap, 15, true); 
			}
			else if (FirmDataType.BOTTOM_15.getValue().equals(afcmb.getLimitType()))
			{
				refinedEventsMap = WebUtil.sortMapByIntegerValue(eventsMap, 15, false); 
			}
			else if (FirmDataType.TOP_10.getValue().equals(afcmb.getLimitType())) 
			{
				refinedEventsMap = WebUtil.sortMapByIntegerValue(eventsMap, 10, true); 
			}
			else if (FirmDataType.BOTTOM_10.getValue().equals(afcmb.getLimitType()))
			{
				refinedEventsMap = WebUtil.sortMapByIntegerValue(eventsMap, 10, false); 
			}
		}
		return refinedEventsMap;
	}

	protected void prepareComparisonData(User currentUser, StandardChartModelBean afcmb, EventSearchDTO eventSearchDTO, List<EventResultDTO> defaultSearchResults,
			ChartData chartData) throws CloneNotSupportedException
	{
		// User is interest in his own Firm
		if (afcmb.getComparisonDataTypeList() != null && afcmb.getComparisonDataTypeList().contains(ComparisonDataType.MY_FIRM.getValue()) && currentUser != null)
		{
			EventSearchDTO dto = (EventSearchDTO) eventSearchDTO.clone();
			// get the currentUser Firm
			Firm firm = firmService.getFirmById(Integer.parseInt(currentUser.getCompany()));

			List<Firm> firmList = new ArrayList<Firm>();
			firmList.add(firm);
			dto.setSelectedFirmsOrganizations(firmList);
			List<EventResultDTO> eventSearchResults = new ArrayList<EventResultDTO>();

			int resultSize = firmService.getEventSearchCount(dto, currentUser);

			if (resultSize > 0)
			{
				eventSearchResults = eventService.getEventData(dto, currentUser);
				// MyFirm means a single firm
				prepareComparisonChartData(eventSearchResults, chartData, 1, false); 
				chartData.sbFirm.append("\"" + firm.getCompany() + "\"");
			}
		}

		// User selected One of the AMLAW 100, AMLAW 200 or NLJ 250
		if (afcmb.getComparisonDataTypeList() != null && afcmb.getComparisonDataTypeList().contains(ComparisonDataType.RIVAL_EDGE.getValue()))
		{
			EventSearchDTO dto = (EventSearchDTO) eventSearchDTO.clone();
			List<Firm> firmList = new ArrayList<Firm>();
			firmList.addAll(firmService.getRanking(afcmb.getFirmList().get(0)));
			dto.setSelectedFirmsOrganizations(firmList);
			dto.setResultPerPage(100000); // explicitly set high value for RivalEdge list
			List<EventResultDTO> eventSearchResults = new ArrayList<EventResultDTO>();

			int resultSize = eventService.getEventDataCount(dto, currentUser);

			if (resultSize > 0)
			{
				eventSearchResults = eventService.getEventData(dto, currentUser);
				prepareComparisonChartData(eventSearchResults, chartData, firmList.size(), false); // send
																									// the
																									// No.
																									// of
																									// Firms
																									// for
																									// selected
																									// Ranking
				chartData.sbFirm.append("\"" + afcmb.getFirmList().get(0) + "\"");
			}
		}
		// User selected WatchList
		if (afcmb.getComparisonDataTypeList() != null && afcmb.getComparisonDataTypeList().contains(ComparisonDataType.WATCHLIST_AVG.getValue()) && afcmb.getWatchList() != null)
		{
			EventSearchDTO dto = (EventSearchDTO) eventSearchDTO.clone();
			for (String watchListId : afcmb.getWatchList())
			{
				List<Firm> firmList = new ArrayList<Firm>();
				//String currentUserId = Long.toBinaryString(currentUser.getId());
				
				if(currentUser.getId() != null)
				{
					firmList.addAll(watchlistService.findAllFirmsInOneWatchlist(Integer.parseInt(watchListId),currentUser.getId()));
				}
				dto.setSelectedFirmsOrganizations(firmList);
				dto.setResultPerPage(100000);
				UserGroup watchList = watchlistService.getWatchList(Integer.parseInt(watchListId),currentUser.getId());

				int resultSize = eventService.getEventDataCount(dto, currentUser);

				List<EventResultDTO> eventSearchResults = new ArrayList<EventResultDTO>();
				if (resultSize > 0)
				{
					eventSearchResults = eventService.getEventData(dto, currentUser);
					prepareComparisonChartData(eventSearchResults, chartData, firmList.size(), false); // send
																										// the
																										// No.
																										// of
																										// Firms
																										// for
																										// selected
																										// WatchList

					if (watchList != null)
					{
						chartData.sbFirm.append("\"" + watchList.getGroupName() + "\"");
					}
					else
					{
						// We may not enter into this condition at all as we are
						// always sure there exist a watchlist for a given id
						// its just to be on safer side
						chartData.sbFirm.append("\"Watch List " + watchListId + "\""); // render
																			// the
																			// id
																			// on
																			// UI
					}
				}
			}
			
		}

		if (afcmb.getComparisonDataTypeList() != null && afcmb.getComparisonDataTypeList().contains(ComparisonDataType.AVERAGE.getValue()))
		{
			prepareComparisonChartData(defaultSearchResults, chartData, 1, true);
			chartData.sbFirm.append("\"Average\""); // hard coded for the time
													// being, TODO get from the
													// Bean if required
		}
	}

	protected class ChartData
	{
		public ChartData()
		{
		}

		public int			type;									// this is not Highchart type, It is the type of ALM Chart like No
																	// of
																	// Attorneys,
																	// Percent
																	// Attorneys
		public int			compareCount	= 0;
		public StringBuffer	sbFirm			= new StringBuffer();
		public StringBuffer	sbFirmCount		= new StringBuffer();
	}

	/**
	 * Pushes the data for comparison graphs in the respective buffers
	 * 
	 * @param results
	 * @param chartData
	 *            data to stuff in
	 * @param firmCountForAvgCalc
	 *            average the results by this value
	 */
	protected void prepareComparisonChartData(List<EventResultDTO> results, ChartData chartData, int firmCountForAvgCalc, boolean avgComparison)
	{

		int firmCount = 0;

		// to escape divide by zero error
		if (firmCountForAvgCalc <= 0)
		{
			firmCountForAvgCalc = 1;
		}

		chartData.compareCount++; // increment the comparison data count

		Map<String, Integer> eventsMap = getEventCountMapFromSearchResults(results);
		for (Integer value : eventsMap.values())
		{
			firmCount += value;
		}

		if (avgComparison) // Comparison is on Average of search results
		{
			firmCountForAvgCalc = eventsMap.keySet().size();
		}

		firmCount = (int) Math.round(((double) firmCount) / firmCountForAvgCalc);

		if (chartData.sbFirm.length() > 0)
		{
			chartData.sbFirm.append(", ");
			chartData.sbFirmCount.append(", ");
		}

		chartData.sbFirmCount.append(firmCount);
	}

	@ActionMapping(params = "action=updateChart")
	public void updateChart(@ModelAttribute("amtByFirmChartModelBean") StandardChartModelBean amtByFirmChartModelBean, ActionRequest request, ActionResponse response)
	{
		// Persist the changes to the chart settings
		// If this is the home page, save them there, else
		// save with the generic NEWS_PUBS tag.
		String 	currentPortletId 	= WebUtil.getCurrentPortletId(request);
		String 	currentPage			= WebUtil.getCurrentPage(request);
		boolean	isHomePage			= ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		User	currentUser 		= WebUtil.getCurrentUser(request, userService);
		
		if (!isHomePage)
		{
			currentPage = "EVENTS";
		}
		
		userService.saveUserPreferences(currentUser.getId(), 
										currentPortletId, 
										null, 
										WebUtil.getJson(amtByFirmChartModelBean), 
										currentPage);
	}
	
	/*
	 * Following method remove rendering portlet from the page. And redirects to same page.
	 *
	 */

	 @ActionMapping(params = "action=removePortleFromThePage")
	 public void removePortletFromPage(ActionRequest request, ActionResponse response) throws Exception
	 {
		 WebUtil.removePortlet(request);
	  }
}
