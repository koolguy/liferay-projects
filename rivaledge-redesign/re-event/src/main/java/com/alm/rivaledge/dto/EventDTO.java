package com.alm.rivaledge.dto;

/**
 * EventDTO responsible for storing firm, firm counter, location counter, and
 * practice area counter screen
 * 
 * @author FL613
 * 
 */
public class EventDTO
{
	private String	firmName;
	private Integer	firmsCount;
	private String	toDate;
	private Integer	dateCount;
	private Integer	practiceAreasCount;
	private Integer	locationsCount;

	public String getFirmName()
	{
		return firmName;
	}

	public Integer getLocationsCount()
	{
		return locationsCount;
	}

	public void setLocationsCount(Integer locationsCount)
	{
		this.locationsCount = locationsCount;
	}

	public void setFirmName(String firmName)
	{
		this.firmName = firmName;
	}

	public Integer getPracticeAreasCount()
	{
		return practiceAreasCount;
	}

	public void setPracticeAreasCount(Integer practiceAreasCount)
	{
		this.practiceAreasCount = practiceAreasCount;
	}

	public Integer getFirmsCount()
	{
		return firmsCount;
	}

	public void setFirmsCount(Integer firmsCount)
	{
		this.firmsCount = firmsCount;
	}

	public String getToDate()
	{
		return toDate;
	}

	public void setToDate(String toDate)
	{
		this.toDate = toDate;
	}

	public Integer getDateCount()
	{
		return dateCount;
	}

	public void setDateCount(Integer dateCount)
	{
		this.dateCount = dateCount;
	}
}