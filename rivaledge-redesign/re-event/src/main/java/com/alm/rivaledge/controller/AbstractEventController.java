package com.alm.rivaledge.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.alm.rivaledge.model.ClickToView;
import com.alm.rivaledge.model.EventSearchModelBean;
import com.alm.rivaledge.model.chart.StandardChartModelBean;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserPreferences;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.service.UserService;
import com.alm.rivaledge.service.WatchlistService;
import com.alm.rivaledge.transferobject.EventResultDTO;
import com.alm.rivaledge.transferobject.EventSearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;

/**
 * Event controller responsible for all operation on Event search form and results
 * screen
 * 
 * @author FL613
 * 
 */
public abstract class AbstractEventController
{
	protected Log _log = LogFactoryUtil.getLog(AbstractEventController.class.getName());
	
	@Autowired
	protected FirmService firmService;

	@Autowired
	protected WatchlistService watchlistService;
	
	@Autowired
	protected UserService userService;
	/**
	 * Populates the Model which holds the Search criteria in search form initializing to default values
	 * 
	 * @param model
	 */

	@ModelAttribute
	public void populateEventSearchModelBean(ModelMap model, PortletRequest request)
	{

		String				currentPortletId		 = WebUtil.getCurrentPortletId(request);
		String				currentPage				 = WebUtil.getCurrentPage(request);
		boolean				isHomePage				 = ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		boolean 			isC2VPage				 = 	WebUtil.isClickToViewPage(request);
		model.addAttribute("isC2VPage", isC2VPage);
		
		
		User 				currentUser 			 = (model.get("currentUser") != null) ? ((User) model.get("currentUser")) : WebUtil.getCurrentUser(request, userService); 
		boolean				isAdminUser				 = WebUtil.isAdminUser(currentUser);									

		EventSearchModelBean eventSearchModelBean		 =  (EventSearchModelBean) model.get("eventSearchModelBean");
		
		if (currentUser != null)
		{
			model.put("currentUser", currentUser);
			model.put("isAdminUser", isAdminUser);
		}
		
		// There are three handling options here.
		// 1. If this is the Home page, get the current portlet id,
		//    which can be either that of the search portlet or a 
		//    chart portlet.
		//
		// 2. If this is the details-drilldown page, then we let the 
		//    details come from session, and let the results controller
		//    replace the search criteria with the request parameter.
		//
		// 3. If neither condition, then it must be the standard search
		//    and results page, so we will load the search criteria from 
		//    the database.
		
		
			if(isHomePage)
			{
				eventSearchModelBean = getUserPreference(currentUser.getId(), currentPortletId, currentPage);
			}
			
			if (!isHomePage && model.get("eventSearchModelBean") == null)
			{
				currentPortletId 	= "EVENTS";
				currentPage			= "EVENTS";
				eventSearchModelBean = getUserPreference(currentUser.getId(), currentPortletId, currentPage);
			}
			
			
			if (eventSearchModelBean == null)
			{
				eventSearchModelBean = getDefaultEventSearchModelBean(currentUser);
			}
			
			
			eventSearchModelBean.setPortletId(currentPortletId);
			
			if(isHomePage || isC2VPage)
			{
				eventSearchModelBean.setAddToSession(false);
			}

			model.addAttribute("eventSearchModelBean", eventSearchModelBean);
			
		
			EventSearchDTO eventSearchDTO = getDTOFromModelBean(model, eventSearchModelBean,request);
			model.addAttribute("eventSearchDTO", eventSearchDTO);

			if (model.get("eventSearchResults") == null)
			{
				model.addAttribute("eventSearchResults", new ArrayList<EventResultDTO>());
			}
		
	}

	protected EventSearchModelBean getDefaultEventSearchModelBean(User currentUser)
	{
		List<UserGroup> allWatchListsDefaultList = null;
		
		EventSearchModelBean eventSearchModelBean = new EventSearchModelBean();
		eventSearchModelBean.init(); //do an explicit init for defualt values

		
		if (currentUser != null)
		{
			allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
		}
		
		
		List<String> firmList = new ArrayList<String>();
		List<Integer> firmListWatchList = new ArrayList<Integer>();
		
		if (allWatchListsDefaultList == null || allWatchListsDefaultList.isEmpty())
		{
			//FirmType
			eventSearchModelBean.setFirmType(ALMConstants.RIVALEDGE_LIST);
			firmList.add(ALMConstants.AMLAW_100);
		} 
		else
		{
			eventSearchModelBean.setFirmType(ALMConstants.WATCH_LIST);
			firmListWatchList.add(allWatchListsDefaultList.get(0).getGroupId());
		}
		eventSearchModelBean.setFirmList(firmList);
		eventSearchModelBean.setFirmListWatchList(firmListWatchList);
		return eventSearchModelBean;
	}

	
	protected EventSearchModelBean getUserPreference(Integer userId, String portletId, String currentPage)
	{
		UserPreferences uP = userService.getUserPreferenceByUserIdAndPortletId(userId, portletId, currentPage);
		if ((uP != null) && (uP.getSearchCriteriaJSON() != null) && (uP.getSearchCriteriaJSON().trim().length() > 0))
		{
			return (WebUtil.getObject(uP.getSearchCriteriaJSON(), EventSearchModelBean.class));
		}
		return (null);
	}
	
	protected StandardChartModelBean getUserChartPreference(Integer userId, String portletId, String currentPage)
	{
		UserPreferences uP = userService.getUserPreferenceByUserIdAndPortletId(userId, portletId, currentPage);
		if ((uP != null) && (uP.getChartCriteriaJSON() != null) && (uP.getChartCriteriaJSON().trim().length() > 0))
		{
			return (WebUtil.getObject(uP.getChartCriteriaJSON(), StandardChartModelBean.class));
		}
		return (null);
	}

	/**
	 * Creates a EventSearchDTO from  EventSearchModelBean
	 * @param model
	 * @param eventSearchModelBean
	 * @return
	 */
	protected EventSearchDTO getDTOFromModelBean(ModelMap model, EventSearchModelBean eventSearchModelBean, PortletRequest request)
	{

		EventSearchDTO eventSearchDTO = new EventSearchDTO();

		// Get the page number from the model bean
		int pageNumber = eventSearchModelBean.getGoToPage();
		int orderBy = eventSearchModelBean.getOrderBy();
		int searchResultsPerPage = eventSearchModelBean.getSearchResultsPerPage();

		eventSearchDTO.setOrderBy(orderBy);
		eventSearchDTO.setResultPerPage(searchResultsPerPage);

		// Plug the page number into the search DTO
		//This sets the starting limit for the paginated search results in the sql query
		eventSearchDTO.setPageNumber(pageNumber < 0 ? 0 : (pageNumber - 1) * searchResultsPerPage);

		// Set the sort column and order into the search DTO
		eventSearchDTO.setSortColumn(eventSearchModelBean.getSortColumn());
		eventSearchDTO.setSortOrder(eventSearchModelBean.getSortOrder());

		// Get the selected firms, if any, and set them into the search DTO
		setSelectedFirms(model, eventSearchDTO, eventSearchModelBean, request);
		
		// Set the date range criteria
		// There will always be a date range criteria, as the form view
		// does not allow this to be null
		setDateRange(model, eventSearchDTO, eventSearchModelBean);

		// Set the remaining search criteria, such as practice areas, etc.
		// See the setOtherSearchParameter method for more details
		setOtherSearchParameter(model, eventSearchDTO, eventSearchModelBean,request);
		return eventSearchDTO;
	}

	/**
	 * Based on the User Action, where the request is coming from, This will alter the searchBean, the searchDTO if required 
	 * and put them back to C2V.
	 * @param request
	 * @param model
	 * @param c2v
	 * @throws UnsupportedEncodingException
	 */
	protected void honorClick2View(PortletRequest request, ModelMap model, ClickToView<EventSearchModelBean, EventSearchDTO> c2v) throws UnsupportedEncodingException
	{
		HttpServletRequest originalRequest = WebUtil.getHttpServletRequest(request);
		
		EventSearchModelBean eventSearchModelBean = null;
		
		if(!WebUtil.isClickToViewPage(request))
		{
			return; // its not C2V, then what the hell will I do here I'm returning, bye!!
		}
		
		// I'm here because I have to respect the C2V request of the user
		String searchCriteria = originalRequest.getParameter("searchCriteria");
		
		if (StringUtils.isNotBlank(searchCriteria))
		{
			String searchJSON = URLDecoder.decode(searchCriteria, "UTF-8");
			eventSearchModelBean = WebUtil.getObject(searchJSON, EventSearchModelBean.class); // get the SearchBean
		}
		else
		{
			eventSearchModelBean =  WebUtil.cloneBean(c2v.getSearchModelBean(),  EventSearchModelBean.class); // clone the bean, because we don't want to alter the original search criteria
		}
		
		String dateText = originalRequest.getParameter("drilldownDate");
		if (StringUtils.isNotBlank(dateText)) //do we have dateText in the request?
		{
			dateText = URLDecoder.decode(dateText, "UTF-8");
			
			SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
			try {
				Date d = new SimpleDateFormat("MM-dd-yy").parse(dateText);
				dateText = formatter.format(d);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				_log.error("Exception: Not a proper date "+dateText, e);
			}
			
			dateText = dateText.replaceAll("-", "/");
			
			dateText = dateText + "-" + dateText;
			eventSearchModelBean.setDateText(dateText);
			
		}
		
		
		String firmName = originalRequest.getParameter("drilldownFirmName");

		if (StringUtils.isNotBlank(firmName)) //do we have firmName in the request?
		{
			String drilldownFirm = URLDecoder.decode(firmName, "UTF-8");
			List<Firm> matchingFirms = firmService.getFirmByName(drilldownFirm);
			
			if((matchingFirms != null) && (!matchingFirms.isEmpty()))
			{
				Firm firm = matchingFirms.get(0);
				
				eventSearchModelBean.setFirmType(ALMConstants.INDIVIDUAL_FIRMS); // set it to Individual as we know there is single firm

				List<String> selectedFirmsList = new ArrayList<String>(1);
				selectedFirmsList.add(""+firm.getCompanyId()); // add Firm Id as String
				
				eventSearchModelBean.setFirmList(selectedFirmsList); // actual Ids to get DTO
				eventSearchModelBean.setSelectedFirms(firm.getCompany()); // this will go into SearchCriteria String
				
			}
		}
		
		String practiceArea = originalRequest.getParameter("drilldownPracticeArea");

		if (StringUtils.isNotBlank(practiceArea))
		{
			String searchPractice = URLDecoder.decode(practiceArea, "UTF-8");
			eventSearchModelBean.setSelectedPracticeArea(searchPractice);
			List<String> selectedPractices = new ArrayList<String>(1);
			selectedPractices.add(searchPractice);
			eventSearchModelBean.setPracticeArea(selectedPractices);
		}
		
		String location = originalRequest.getParameter("drilldownLocation");

		if (StringUtils.isNotBlank(location))
		{
			String searchLocation = URLDecoder.decode(location, "UTF-8");
			eventSearchModelBean.setSelectedLocation(searchLocation);
			List<String> selectedLocations = new ArrayList<String>(1);
			selectedLocations.add(searchLocation);
			eventSearchModelBean.setLocations(selectedLocations);
		}
		
		eventSearchModelBean.setGoToPage(1);
		eventSearchModelBean.setAddToSession(false); // don't add this to session. But, only to model
		
		model.addAttribute("eventSearchModelBean", eventSearchModelBean); 
		EventSearchDTO eventSearchDTO = getDTOFromModelBean(model, eventSearchModelBean, request);
		c2v.setSearchModelBean(eventSearchModelBean); // set the changed bean back to C2V
		c2v.setSearchDTO(eventSearchDTO); // set the changed DTO back to C2V
	}

	
	/***
	 * Transforms , and ; separated values at 0-index to List values <br>
	 * We need values in List rather than CSV, so that they can bind to <code>Spring Form<code> 
	 * later on from session
	 * 
	 * @param eventSearchModelBean
	 */
	protected void transformBean(EventSearchModelBean eventSearchModelBean)
	{

		List<String> firmList = eventSearchModelBean.getFirmList();

		if (firmList != null && !firmList.isEmpty())
		{
			String firm = firmList.get(0);
			eventSearchModelBean.setFirmList(ListUtil.fromArray(StringUtil.split(firm, ";"))); //firmList is ; separated
		}

		List<String> locationList = eventSearchModelBean.getLocations();

		if (locationList != null && !locationList.isEmpty())
		{
			String loc = locationList.get(0);
			eventSearchModelBean.setLocations(ListUtil.fromArray(StringUtil.split(loc, ";")));
		}

		List<String> practiceAreaList = eventSearchModelBean.getPracticeArea();

		if (practiceAreaList != null && !practiceAreaList.isEmpty())
		{
			String practiceArea = practiceAreaList.get(0);
			eventSearchModelBean.setPracticeArea(ListUtil.fromArray(StringUtil.split(practiceArea, ";")));
		}

	}

	/**
	 * Set date range in EventSearchDTO reference based on user input on Event
	 * search Screen
	 * 
	 * @param model
	 * @param request
	 * @param response
	 * @param eventSearchDTO
	 */
	protected void setDateRange(ModelMap model, EventSearchDTO eventSearchDTO, EventSearchModelBean eventSearchModelBean)
	{
		String dateText = eventSearchModelBean.getDateText();

		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		try
		{
			if (dateText != null && !dateText.contains("-"))
			{
	
				if (dateText.equals(ALMConstants.ANY)) 
				{
					eventSearchDTO.setFromDate(null);
					eventSearchDTO.setToDate(null);
				}
				else if (dateText.equals(ALMConstants.NEXT_WEEK)) 
				{
					eventSearchDTO.setFromDate(dateFormat.parse(dateFormat.format(date)));
					calendar.add(Calendar.WEEK_OF_MONTH, +1);
					eventSearchDTO.setToDate(dateFormat.parse(dateFormat.format(calendar.getTime())));
				}
				else if (dateText.equals(ALMConstants.NEXT_30_DAYS))
				{
					eventSearchDTO.setFromDate(dateFormat.parse(dateFormat.format(date)));
					calendar.add(Calendar.MONTH, +1);
					eventSearchDTO.setToDate(dateFormat.parse(dateFormat.format(calendar.getTime())));
				}
				else if (dateText.equals(ALMConstants.NEXT_6_MONTHS))
				{
					eventSearchDTO.setFromDate(dateFormat.parse(dateFormat.format(date)));
					calendar.add(Calendar.MONTH, +6);
					eventSearchDTO.setToDate(dateFormat.parse(dateFormat.format(calendar.getTime())));
					
				}
				else if (dateText.equals(ALMConstants.NEXT_YEAR))
				{
					eventSearchDTO.setFromDate(dateFormat.parse(dateFormat.format(date)));
					calendar.add(Calendar.YEAR, +1);
					eventSearchDTO.setToDate(dateFormat.parse(dateFormat.format(calendar.getTime())));
				}
				else if (dateText.equals(ALMConstants.LAST_WEEK))
				{
					
					eventSearchDTO.setToDate(dateFormat.parse(dateFormat.format(date)));
					calendar.add(Calendar.WEEK_OF_MONTH, -1);
					eventSearchDTO.setFromDate(dateFormat.parse(dateFormat.format(calendar.getTime())));
				}
				else if (dateText.equals(ALMConstants.LAST_30_DAYS))
				{
					eventSearchDTO.setToDate(dateFormat.parse(dateFormat.format(date)));
					calendar.add(Calendar.MONTH, -1);
					eventSearchDTO.setFromDate(dateFormat.parse(dateFormat.format(calendar.getTime())));
				}
				else if (dateText.equals(ALMConstants.LAST_90_DAYS))
				{
					eventSearchDTO.setToDate(dateFormat.parse(dateFormat.format(date)));
					calendar.add(Calendar.MONTH, -3);
					eventSearchDTO.setFromDate(dateFormat.parse(dateFormat.format(calendar.getTime())));
				}
				else if (dateText.equals(ALMConstants.LAST_6_MONTHS))
				{
					eventSearchDTO.setToDate(dateFormat.parse(dateFormat.format(date)));
					calendar.add(Calendar.MONTH, -6);
					eventSearchDTO.setFromDate(dateFormat.parse(dateFormat.format(calendar.getTime())));
				}
				else 
				{
					eventSearchDTO.setToDate(dateFormat.parse(dateFormat.format(date)));
					calendar.add(Calendar.YEAR, -1);
					eventSearchDTO.setFromDate(dateFormat.parse(dateFormat.format(calendar.getTime())));
				}
	
			}
			else
			{
				String[] dateRange = dateText.split("-");
					eventSearchDTO.setFromDate(dateFormat.parse(dateRange[0]));
					eventSearchDTO.setToDate(dateFormat.parse(dateRange[1]));
			}
		} 
		catch (ParseException e)
		{
			e.printStackTrace();
		}
	}

	/**
	 * Set Practice Area, Locations, keywords and  in
	 * EventSearchDTO reference based on user input on Event search Screen
	 * 
	 * @param request
	 * @param response
	 * @param eventSearchDTO
	 */
	protected void setOtherSearchParameter(ModelMap model, EventSearchDTO eventSearchDTO, EventSearchModelBean eventSearchModelBean,PortletRequest request)
	{
		String locationString = null;
		if(eventSearchModelBean.getLocations() != null)
		{
			locationString = eventSearchModelBean.getLocations().toString().replace("[", "").replace("]", "");
		}
		final String WEBINAR = "Webinar"; 
		final String INPERSON = "In Person";
		final String INTERNATIONAL = "International";
		final String LOCATION_NOT_AVAILABLE = "Location Not Available";
		if (eventSearchModelBean.getLocations() != null
				&& !eventSearchModelBean.getLocations().contains(ALMConstants.ALL_LOCATIONS))
		{
			eventSearchDTO.setLocations(eventSearchModelBean.getLocations());
		}
		if (eventSearchModelBean.getPracticeArea() != null
				&& !eventSearchModelBean.getPracticeArea().contains(ALMConstants.ALL_PRACTICE_AREAS))
		{
			eventSearchDTO.setPracticeArea(eventSearchModelBean.getPracticeArea());
		}
		
		if(locationString != null 
				&& !(ALMConstants.ALL_LOCATIONS.equals(locationString) || WEBINAR.equals(locationString)
						|| INPERSON.equals(locationString) || INTERNATIONAL.equals(locationString) || LOCATION_NOT_AVAILABLE.equals(locationString)))
		{
			List<String> locationDisplayLabel = eventSearchModelBean.getLocations();
			String allLocs = "";
			//Convert selected location into location label name 
			for(String location : locationDisplayLabel)
			{
				allLocs += location.substring(3, location.length());
				allLocs += ",";
			}
			eventSearchModelBean.setSelectedLocation(allLocs.substring(0, allLocs.length()-1));
		}
		else
		{
			eventSearchModelBean.setSelectedLocation(locationString);
		}

		if(eventSearchModelBean.getPracticeArea() != null)
		{
			eventSearchModelBean.setSelectedPracticeArea(eventSearchModelBean.getPracticeArea().toString().replace("[", "").replace("]", ""));
		}
		
		eventSearchDTO.setKeyword(StringUtils.defaultIfBlank(eventSearchModelBean.getKeywords(), null));
	}

	/**
	 * Set Firms information in EventSearchDTO reference based on user input on
	 * Event search Screen
	 * 
	 * @param model
	 * @param eventSearchDTO
	 * @param eventSearchModelBean
	 */
	protected void setSelectedFirms(ModelMap model, EventSearchDTO eventSearchDTO, EventSearchModelBean eventSearchModelBean,PortletRequest request)
	{
		String 			firmType 			= eventSearchModelBean.getFirmType();
		List<Firm> 		firmsList 			= new ArrayList<Firm>();
		List<String> 	firmsListForResults	= new ArrayList<String>();
		
		if (ALMConstants.ALL_FIRMS.equals(firmType))
		{
			firmsList.addAll(firmService.getAllFirms());
			eventSearchDTO.setSelectedFirmsOrganizations(firmsList);
			firmsListForResults.add(firmType);
			eventSearchModelBean.setFirmListWatchList(null);
			eventSearchModelBean.setFirmList(null);
		}
		else if (ALMConstants.ALL_ORGANIZATIONS.equals(firmType))
		{
			firmsList.addAll(firmService.getOrganizationsList());
			eventSearchDTO.setSelectedFirmsOrganizations(firmsList);
			firmsListForResults.add(firmType);
			eventSearchModelBean.setFirmListWatchList(null);
			eventSearchModelBean.setFirmList(null);
		}
		else if (ALMConstants.ALL_FIRMS_ORGANIZATION.equals(firmType))
		{
			firmsList.addAll(firmService.getAllFirms());
			firmsList.addAll(firmService.getOrganizationsList());
			eventSearchDTO.setSelectedFirmsOrganizations(firmsList);
			firmsListForResults.add(firmType);
			eventSearchModelBean.setFirmListWatchList(null);
			eventSearchModelBean.setFirmList(null);
		}
		else if (ALMConstants.WATCH_LIST.equals(firmType))
		{
			List<Firm> 		firmsWatchList 	= null;
			List<UserGroup> allWatchListsDefaultList = null;
			String 			watchListsName = request.getParameter("Firmstext");
			User 			currentUser	= (model.get("currentUser") != null) ? ((User) model.get("currentUser")) : WebUtil.getCurrentUser(request, userService);
			
			if (currentUser != null)
			{
				firmsWatchList = watchlistService.findAllFirmsInManyWatchlists(eventSearchModelBean.getFirmListWatchList(), currentUser.getId());
			}
			
			if (firmsWatchList.size() == 0)
			{
				List<Firm> notAvailableFirms = new ArrayList<Firm>();
				Firm tempFirm = new Firm();
				tempFirm.setCompanyId(-1);
				notAvailableFirms.add(tempFirm);
				eventSearchDTO.setSelectedFirmsOrganizations(notAvailableFirms);
			}
			else
			{
				eventSearchDTO.setSelectedFirmsOrganizations(firmsWatchList);
			}
			eventSearchModelBean.setFirmList(null);
			if(watchListsName == null)
			{
				allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
				if(allWatchListsDefaultList != null)
				{
					firmsListForResults.add(allWatchListsDefaultList.get(0).getGroupName());
				}
			}
			else
			{
				firmsListForResults.add(watchListsName);
			}
		}
		else if (ALMConstants.RIVALEDGE_LIST.equals(firmType))
		{
			firmsList.addAll(firmService.getRanking(eventSearchModelBean.getFirmList().get(0)));
			eventSearchDTO.setSelectedFirmsOrganizations(firmsList);
			firmsListForResults.add(eventSearchModelBean.getFirmList().get(0));
			eventSearchDTO.setSelectedFirmsOrganizations(firmsList); 
			eventSearchModelBean.setFirmListWatchList(null);
		}
		else
		{
			for (String firmId : eventSearchModelBean.getFirmList())
			{
				Firm firmObject = firmService.getFirmById(Integer.parseInt(firmId));
				firmsList.add(firmObject);
				firmsListForResults.add(firmObject.getCompany());
			}
			eventSearchDTO.setSelectedFirmsOrganizations(firmsList);
			eventSearchModelBean.setFirmListWatchList(null);
		}
		eventSearchModelBean.setSelectedFirms(firmsListForResults.toString().replace("[", "").replace("]", ""));
	}
}
