package com.alm.rivaledge.controller.charts.amounteventpracticearea;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.alm.rivaledge.controller.AbstractEventController;
import com.alm.rivaledge.model.EventSearchModelBean;
import com.alm.rivaledge.persistence.dao.CacheDAO;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.service.UserService;
import com.alm.rivaledge.transferobject.EventResultDTO;
import com.alm.rivaledge.transferobject.EventSearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.StringUtil;

/**
 * AmountEventPracticeArea controller responsible for displaying charts for
 * Event based on practice areas
 * 
 * @author FL613
 */
@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "eventSearchModelBean" })
public class AmountEventPracticeAreaController extends AbstractEventController
{

 @Autowired
 private FirmService firmService;

 @Autowired
 private UserService userService;

 @Autowired
 private CacheDAO cacheDAO;

 @RenderMapping
 public ModelAndView defaultChartView(@ModelAttribute("eventSearchModelBean") EventSearchModelBean eventSearchModelBean, 
   ModelMap model,
   RenderRequest request,
   RenderResponse response) throws Exception
 {

  EventSearchDTO eventSearchDTO = getDTOFromModelBean(model, eventSearchModelBean, request);
		
  EventSearchDTO chartEventSearchDTO = (EventSearchDTO) eventSearchDTO.clone();

  // Get the current user from the render request
  User currentUser = WebUtil.getCurrentUser(request, userService);
 
//Niranjan -> To whom so ever written this. What the hell happening here? Why are u fetch the same user again by Id?
	
  /*if (currentUser != null)
  {
   // Get this Liferay/DB_Reg user
   currentUser = userService.getUserById(currentUser.getId());
  }
*/
  // Call the service implementation to get the search results count
  List<EventResultDTO> eventSearchResults = new ArrayList<EventResultDTO>();

  int resultSize = (chartEventSearchDTO != null) ? firmService.getEventSearchCount(chartEventSearchDTO, currentUser) : 0;
  if (resultSize > 0)
  {
	  
	  chartEventSearchDTO.setPageNumber(0);
		chartEventSearchDTO.setResultPerPage(WebUtil.getResultSizeLimitForCharts(currentUser));
   // We have results that match this search criteria
   // Return the first page of results
   // Replace with EventService; controllers must not call DAOs
   // directly
   eventSearchResults = cacheDAO.findEventData(chartEventSearchDTO);

   // Set the chart type
   model.addAttribute("chartType", "column");

   // Set the chart title
   model.addAttribute("chartTitle", "NUMBER OF EVENTS BY PRACTICE");

   Map<String, Integer> practiceAreaMap = new HashMap<String, Integer>();
   Map<String, List<String>> practiceAreaAndEventsMap = new HashMap<String, List<String>>();
   // Calculate the data series
   for (EventResultDTO frDTO : eventSearchResults)
   {
    List<String> practiceList = new ArrayList<String>();
    if ((frDTO.getPracticeArea() == null) || (frDTO.getPracticeArea().trim().length() == 0))
    {
     practiceList.add("Practice Area Not Available");
    }
    else if (!frDTO.getPracticeArea().contains(";"))
    {
     practiceList.add(frDTO.getPracticeArea());
    }
    else
    {
     // multiple practice area is separated by ";" adding all
     // individuals to practiceList
     practiceList.addAll(ListUtil.fromArray(StringUtil.split(frDTO.getPracticeArea(), ";")));
    }

    for (String practiceArea : practiceList)
    {
     List<String> eventTitleList = null;
     int eventCount = 0;

     if (practiceAreaMap.containsKey(practiceArea))
     {
      eventCount = practiceAreaMap.get(practiceArea) + 1;
      eventTitleList = practiceAreaAndEventsMap.get(practiceArea);
     }
     else
     {
      eventCount = 1;
      eventTitleList = new ArrayList<String>();
      practiceAreaAndEventsMap.put(practiceArea, eventTitleList);

     }
     
     // we have a limit for No. of titles in tooltip
     if (eventTitleList.size() < ALMConstants.NO_OF_EVENTS_IN_TOOLTIP)
     {
      eventTitleList.add(frDTO.getEventTitle()); 
     }
     practiceAreaMap.put(practiceArea, eventCount);
    }
   }

   // Sorting
   Map<String, Integer> sortedEventsMap = WebUtil.sortMapByIntegerValue(practiceAreaMap,
     ALMConstants.DEFAULT_NO_OF_COLUMNS_CHART, true);

   // Generate the series data
   StringBuffer sbPracticeAreas = new StringBuffer();
   StringBuffer sbEventCounts = new StringBuffer();
   StringBuffer sbEventTitles = new StringBuffer();

   for (String thisKey : sortedEventsMap.keySet())
   {
    if (sbPracticeAreas.length() > 0)
    {
     sbPracticeAreas.append(", ");
     sbEventCounts.append(", ");
     sbEventTitles.append(", ");
    }

    sbEventTitles.append("[");

    for (String title : practiceAreaAndEventsMap.get(thisKey))
    {
     if (!sbEventTitles.toString().endsWith("["))
     {
      sbEventTitles.append(", ");
     }
     sbEventTitles.append("\"").append(WebUtil.cleanString(title)).append("\"");
    }
    sbEventTitles.append("]");

    sbPracticeAreas.append("\"" + thisKey + "\"");
    sbEventCounts.append(sortedEventsMap.get(thisKey));
   }

   model.addAttribute("eventsByPracticeAreasOfPractice", sbPracticeAreas.toString());
   model.addAttribute("eventsByPracticeEventCount", sbEventCounts.toString());
   model.addAttribute("eventsByPracticeEventTitles", sbEventTitles.toString()); //Array of Array
  }
  return new ModelAndView("amountpracticearea");
 }

 /*
  * Following method remove rendering portlet from the page. And redirects to same page.
  *
  */

 @ActionMapping(params = "action=removePortleFromThePage")
 public void removePortletFromPage(ActionRequest request, ActionResponse response) throws Exception
 {
	 WebUtil.removePortlet(request);
 }
}