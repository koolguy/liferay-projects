package com.alm.rivaledge.dto;

import java.io.Serializable;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.collections4.list.TreeList;

public class EventPracticeLocation implements Serializable
{
	private static final long		serialVersionUID	= -512167570432277958L;
	private Integer					firmId;
	private Map<String, Integer>	eventLocationMap;
	private Map<String, Integer>	eventPracticeMap;

	public Map<String, Integer> getTopLocations(int size)
	{
		Map<String, Integer> returnMap = getTop(eventLocationMap);
		if (returnMap != null)
		{
			if (returnMap.size() > size)
			{
				Map<String, Integer> truncatedMap = new TreeMap<String, Integer>();
				TreeList<String> keysList = new TreeList<String>(returnMap.keySet());
				for (int i = 0; i < size; i++)
				{
					truncatedMap.put(keysList.get(i), returnMap.get(keysList.get(i)));
				}
				return (truncatedMap);
			}
		}
		return (returnMap);
	}
	
	public Map<String, Integer> getTopPractices(int size)
	{
		Map<String, Integer> returnMap = getTop(eventPracticeMap);
		if (returnMap != null)
		{
			if (returnMap.size() > size)
			{
				Map<String, Integer> truncatedMap = new TreeMap<String, Integer>();
				TreeList<String> keysList = new TreeList<String>(returnMap.keySet());
				for (int i = 0; i < size; i++)
				{
					truncatedMap.put(keysList.get(i), returnMap.get(keysList.get(i)));
				}
				return (truncatedMap);
			}
		}
		return (returnMap);
	}
	
	private Map<String, Integer> getTop(Map<String, Integer> candidateMap)
	{
		if ((candidateMap == null) || (candidateMap.isEmpty()))
		{
			return (null);
		}
		
		StringKeyComparator  thisComparator = new StringKeyComparator(candidateMap);
		Map<String, Integer> returnMap 		= new TreeMap<String, Integer>(thisComparator);
		returnMap.putAll(candidateMap);
		
		return (returnMap);
	}
	
	public void addLocation(String newLocation)
	{
		if (eventLocationMap == null)
		{
			eventLocationMap = new HashMap<String, Integer>();
		}
		Integer currentCount = 0;
		if (eventLocationMap.containsKey(newLocation))
		{
			currentCount = eventLocationMap.get(newLocation);
		}
		eventLocationMap.put(newLocation, (currentCount + 1));
	}
	
	public void addPractice(String newPractice)
	{
		if (eventPracticeMap == null)
		{
			eventPracticeMap = new HashMap<String, Integer>();
		}
		Integer currentCount = 0;
		if (eventPracticeMap.containsKey(newPractice))
		{
			currentCount = eventPracticeMap.get(newPractice);
		}
		eventPracticeMap.put(newPractice, (currentCount + 1));
	}
	
	public Integer getFirmId()
	{
		return firmId;
	}

	public void setFirmId(Integer firmId)
	{
		this.firmId = firmId;
	}

	public Map<String, Integer> getEventLocationMap()
	{
		return eventLocationMap;
	}

	public void setEventLocationMap(Map<String, Integer> eventLocationMap)
	{
		this.eventLocationMap = eventLocationMap;
	}

	public Map<String, Integer> getEventPracticeMap()
	{
		return eventPracticeMap;
	}

	public void setEventPracticeMap(Map<String, Integer> eventPracticeMap)
	{
		this.eventPracticeMap = eventPracticeMap;
	}
}

class StringKeyComparator implements Comparator<String>
{
	Map<String, Integer> thisMap;

	public StringKeyComparator(Map<String, Integer> newMap)
	{
		super();
		this.thisMap = newMap;
	}

	@Override
	public int compare(String keyOne, String keyTwo)
	{
		// NEVER return 0 or we will lose keys
		if ((keyOne != null) && (keyTwo != null) && (thisMap.get(keyOne) != null) && (thisMap.get(keyTwo) != null))
		{
			return ((thisMap.get(keyOne) >= thisMap.get(keyTwo)) ? -1 : 1);
		}
		return (-1);
	}
}