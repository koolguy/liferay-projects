package com.alm.rivaledge.controller.shareofvoice;

import java.util.HashMap;
import java.util.Map;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.alm.rivaledge.service.FirmService;

@Controller
@RequestMapping(value = "VIEW")
public class ShareOfVoiceController
{
	@Autowired
	private FirmService firmService;
	
	@RenderMapping
	public ModelAndView defaultChartView(RenderRequest request, RenderResponse response) throws Exception
	{
		// Check if there is a current search criteria in session.
		// If not, then run with the user's default watch list.
		// If the user doesn't have a watch list, then run against
		// the AmLaw 100 by default.
		Map<String, String> chartProperties = new HashMap<String, String>();
		return new ModelAndView("shareofvoice");
	}
}
