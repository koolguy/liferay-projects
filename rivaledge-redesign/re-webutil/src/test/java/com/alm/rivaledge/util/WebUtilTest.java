
package com.alm.rivaledge.util;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;

/**
 * 
 * @author FL889
 * @since Sprint 4
 */

public class WebUtilTest
{
	protected transient final Log	testLog	= LogFactory.getLog(getClass());
	private DateFormat				sDF		= new SimpleDateFormat("hh:MM:ss:SS");

	/**
	 * Utility method that prints the class name and method name along with the time stamp.
	 */
	protected void printTitle()
	{
		String callingMethod = new Exception().getStackTrace()[1].getMethodName();
		System.out.println(("\n******* " + callingMethod + " | " + sDF.format(Calendar.getInstance().getTime())) + " ********");
	}
	
	
	@Test
	public void testGetDefaultDateRangeTextForSearch()
	{
		printTitle();
		String dateText = WebUtil.getDefaultDateRangeTextForSearch();
		System.out.println(dateText);
		Assert.assertNotNull(dateText);
	}
	
	@Test
	public void testBottom()
	{
		printTitle();
		List<String> list = new ArrayList<String>();
			for(int i = 0; i< 100; i++)
				{
					list.add("ALM");
				}
		List<String> results = WebUtil.bottom(list, 15);
		System.out.println("Bottom results = " + results.size());
		Assert.assertEquals(15, results.size());
	}
	
	@Test
	public void testTop()
	{
		printTitle();
		List<String> list = new ArrayList<String>();
			for(int i = 0; i< 100; i++)
				{
					list.add("ALM");
				}
		List<String> results = WebUtil.top(list, 15);
		System.out.println("Top results = " + results.size());
		Assert.assertEquals(15, results.size());
	}
	
	@Test
	public void testPercentageRatio()
	{
		printTitle();
		
		int[] results = WebUtil.getPercentageRatio(2291, false, 836, 1155, 300); //  yielded 99 as per normal java calc
		System.out.println("Percentage results = " + Arrays.toString(results));
		
		results = WebUtil.getPercentageRatio(2127, false, 649, 1316, 162); //  yielded 101 as per normal java calc
		System.out.println("Percentage results = " + Arrays.toString(results));
		
		results = WebUtil.getPercentageRatio(1633, false, 871, 580, 182); //  yielded 100 as per normal java calc
		System.out.println("Percentage results = " + Arrays.toString(results));
		//Assert.assertEquals(15, results.size());
	}
	
	
	
	@Test
	public void testClassMappingTest()
	{
	
		
		printTitle();
		
		
		System.out.println("16 = " + testingCategory(16));
		System.out.println("0 = " + testingCategory(0));
		System.out.println("2 = " + testingCategory(2));
		System.out.println("-1 = " + testingCategory(-1));
		System.out.println("305 = " + testingCategory(305));
		System.out.println("-28 = " + testingCategory(-28));
		System.out.println("-21 = " + testingCategory(-21));
	}
	
	
	public String testingCategory(int value)
	{
		
		
		TreeMap<Integer, String> map = new TreeMap<Integer	,String>();
		
		map.put(25, ".plus-25");
		map.put(20, ".plus-20");
		map.put(15, ".plus-15");
		map.put(10, ".plus-10");
		map.put(5, ".plus-5");
		map.put(0, ".zero");
		map.put(-5, ".minus-5");
		map.put(-10, ".minus-10");
		map.put(-15, ".minus-15");
		map.put(-20, ".minus-20");
		map.put(-25, ".minus-25");
	
		
		if(value > 25)
		{
			value = 25;
			
		}else if(value < -25)
		{
			value = -25;
			
		}
		
		String category = null;
		
		if(value >=0)
		{
			category = map.ceilingEntry(value).getValue();
		}
		else {
			category = map.floorEntry(value).getValue();
		}
		
		return category;
	}
	
}
