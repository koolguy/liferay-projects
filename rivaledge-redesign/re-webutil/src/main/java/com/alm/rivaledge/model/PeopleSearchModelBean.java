package com.alm.rivaledge.model;

import java.util.ArrayList;
import java.util.List;

import com.alm.rivaledge.model.chart.BaseChartModelBean.ComparisonDataType;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;

/**
 * Model Bean carrying search criteria back and forth between UI and Controller on People Portlet
 * @author FL889
 * @since 10-Sep-2013
 * @version 1.0
 */

public class PeopleSearchModelBean extends BaseSearchModelBean
{
	private static final long serialVersionUID = 4064848772421507139L;
	
	public static final int GROUPBY_TITLE = -1;
	public static final int GROUPBY_HEAD_COUNT = -2;
	/* variables carries values bidirectionally 
	 * JSP -> Controller and vice versa
	 * 
	 */
	private String firmType;
	private List<String> firmList;
	private List<Integer> firmListWatchList;
	private List<String> locations;
	private List<String> practiceArea;
	private String dateText;
	private List<String> firmSizeList;
	

	/* variables carries values unidirectionally 
	 * Controller -> JSP 
	 * Basically these carries information about displaying searchCriteria String in results jsp. and need not require any default values
	 * as these are populated while sending the search results to view from Controller
	 * 
	 */
	private String selectedFirms;
	private String selectedLocations;
	private String selectedPracticeArea;
	private String selectedFirmSize;
	
	
	
	//View Settings attributes
	
	private List<String> displayColumns;
	
	private List<String> moves;
	
	private List<String> rivalEdgeList;
	private List<String> watchList;
	private List<String> comparisonDataTypeList;
	
	
	
	//Misc attributes
	/**
	 * Just to maintain the state if the action is in drilling phase.
	 * = Display, Print Or Export of drill down Results
	 * */
	private boolean drilling;
	
	public void init()
	{
		
		//FirmType
		firmType = ALMConstants.RIVALEDGE_LIST;
		
		firmList = new ArrayList<String>();
		firmList.add(ALMConstants.AMLAW_100);
		
		locations = new ArrayList<String>();
		locations.add(ALMConstants.ALL_LOCATIONS);
		
		dateText = WebUtil.getDefaultDateRangeTextForSearch();
		
		practiceArea = new ArrayList<String>();
		practiceArea.add(ALMConstants.ALL_PRACTICE_AREAS);
		
		
		firmSizeList = new ArrayList<String>();
		firmSizeList.add(ALMConstants.ALL_FIRM_SIZES);

		
		displayColumns = new ArrayList<String>();
		
		displayColumns.add(ALMConstants.FIRM_STATS_DISPLAY_COLUMNS.partners.getValue());
		displayColumns.add(ALMConstants.FIRM_STATS_DISPLAY_COLUMNS.associates.getValue());
		displayColumns.add(ALMConstants.FIRM_STATS_DISPLAY_COLUMNS.other_counsel.getValue());
		displayColumns.add(ALMConstants.FIRM_STATS_DISPLAY_COLUMNS.admin.getValue());
		displayColumns.add(ALMConstants.FIRM_STATS_DISPLAY_COLUMNS.other.getValue());
		
		
		moves = new ArrayList<String>();
		
		moves.add(ALMConstants.ADDITIONS);
		moves.add(ALMConstants.REMOVALS);
		
		comparisonDataTypeList = new ArrayList<String>();
		
		comparisonDataTypeList.add(ComparisonDataType.MY_FIRM.getValue());
		comparisonDataTypeList.add(ComparisonDataType.RIVAL_EDGE.getValue());
		comparisonDataTypeList.add(ComparisonDataType.AVERAGE.getValue());
				
		rivalEdgeList = new ArrayList<String>();
		rivalEdgeList.add(ALMConstants.AMLAW_100);
		
		goToPage = 1;
		sortColumn = 3;
		sortOrder = "asc";
		orderBy = -1; //-1 =  no grouping"
		searchResultsPerPage = 500;// fix for RER 400
		
	}

	public String getFirmType()
	{
		return firmType;
	}

	public void setFirmType(String firmType)
	{
		this.firmType = firmType;
	}
	

	public List<String> getFirmList()
	{
		return firmList;
	}

	public void setFirmList(List<String> firmList)
	{
		this.firmList = firmList;
	}

	public String getDateText()
	{
		return dateText;
	}

	public void setDateText(String dateText)
	{
		this.dateText = dateText;
	}


	public List<String> getPracticeArea()
	{
		return practiceArea;
	}

	public void setPracticeArea(List<String> practiceArea)
	{
		this.practiceArea = practiceArea;
	}

	
	public String getSelectedFirms() {
		return selectedFirms;
	}

	public void setSelectedFirms(String selectedFirms) {
		this.selectedFirms = selectedFirms;
	}

	public String getSelectedPracticeArea() {
		return selectedPracticeArea;
	}

	public void setSelectedPracticeArea(String selectedPracticeArea) {
		this.selectedPracticeArea = selectedPracticeArea;
	}

	public List<String> getLocations()
	{
		return locations;
	}

	public void setLocations(List<String> locations)
	{
		this.locations = locations;
	}

	public String getSelectedLocations()
	{
		return selectedLocations;
	}

	public void setSelectedLocations(String selectedLocations)
	{
		this.selectedLocations = selectedLocations;
	}

	public String getSelectedFirmSize()
	{
		return selectedFirmSize;
	}

	public void setSelectedFirmSize(String selectedFirmSize)
	{
		this.selectedFirmSize = selectedFirmSize;
	}

	public List<String> getFirmSizeList()
	{
		return firmSizeList;
	}

	public void setFirmSizeList(List<String> firmSizeList)
	{
		this.firmSizeList = firmSizeList;
	}

	public List<String> getDisplayColumns()
	{
		return displayColumns;
	}

	public void setDisplayColumns(List<String> displayColumns)
	{
		this.displayColumns = displayColumns;
	}

	public List<String> getMoves()
	{
		return moves;
	}

	public void setMoves(List<String> moves)
	{
		this.moves = moves;
	}

	public List<String> getRivalEdgeList()
	{
		return rivalEdgeList;
	}

	public void setRivalEdgeList(List<String> rivalEdgeList)
	{
		this.rivalEdgeList = rivalEdgeList;
	}

	public List<String> getWatchList()
	{
		return watchList;
	}

	public void setWatchList(List<String> watchList)
	{
		this.watchList = watchList;
	}

	public List<String> getComparisonDataTypeList()
	{
		return comparisonDataTypeList;
	}

	public void setComparisonDataTypeList(List<String> comparisonDataTypeList)
	{
		this.comparisonDataTypeList = comparisonDataTypeList;
	}


	
	//for boolean values, required in Javascript to hide/display columns
	
	public boolean isAttorneysChecked()
	{
		return isPartnersChecked() || isAssociatesChecked() || isOtherCounselChecked() || isAdminChecked() ||  isOtherChecked();
	}

	public boolean isAddedChecked()
	{
		return moves!= null &&  moves.contains(ALMConstants.ADDITIONS);
	}

	public boolean isRemovedChecked()
	{
		return moves!= null && 	moves.contains(ALMConstants.REMOVALS);
	}

	
	public boolean isHeadCountGroupChecked()
	{
		return orderBy == GROUPBY_HEAD_COUNT; // This is for HeadCount
	}
	
	public boolean isPartnersChecked()
	{
		return displayColumns!= null && displayColumns.contains(ALMConstants.FIRM_STATS_DISPLAY_COLUMNS.partners.getValue());
	}
	public boolean isAssociatesChecked()
	{
		return displayColumns!= null && displayColumns.contains(ALMConstants.FIRM_STATS_DISPLAY_COLUMNS.associates.getValue());
	}
	public boolean isOtherCounselChecked()
	{
		return displayColumns!= null &&  displayColumns.contains(ALMConstants.FIRM_STATS_DISPLAY_COLUMNS.other_counsel.getValue());
	}
	public boolean isAdminChecked()
	{
		return displayColumns!= null &&  displayColumns.contains(ALMConstants.FIRM_STATS_DISPLAY_COLUMNS.admin.getValue());
	}
	public boolean isOtherChecked()
	{
		return displayColumns!= null &&  displayColumns.contains(ALMConstants.FIRM_STATS_DISPLAY_COLUMNS.other.getValue());
	}

	public boolean isDrilling()
	{
		return drilling;
	}

	public void setDrilling(boolean drilling)
	{
		this.drilling = drilling;
	}

	public List<Integer> getFirmListWatchList() {
		return firmListWatchList;
	}

	public void setFirmListWatchList(List<Integer> firmListWatchList) {
		this.firmListWatchList = firmListWatchList;
	}
	
	

}
