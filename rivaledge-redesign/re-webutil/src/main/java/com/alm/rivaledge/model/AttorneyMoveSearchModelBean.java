package com.alm.rivaledge.model;

import java.util.ArrayList;
import java.util.List;

import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;

/**
 * Bean which holds the Search Criteria on JSP for Attorney Move Portlet
 * 
 * @author FL605
 * 
 */
public class AttorneyMoveSearchModelBean extends BaseSearchModelBean
{
	private static final long	serialVersionUID	= 4064848772421507239L;

	/**
	 * variables carries values bidirectionally JSP -> Controller and vice versa
	 */
	private List<String>		firmList;
	private String				firmType;
	private List<Integer>       firmListWatchList;
	private List<String>		changeType;
	private List<String>		locations;
	private List<String>		practiceArea;
	private String				dateText;
	private List<String>		titles;
	private String				name;
	private String				keywords;

	/**
	 * variables carries values unidirectionally Controller -> JSP Basically
	 * these carries information about displaying searchCriteria String in
	 * results jsp. and need not require any default values as these are
	 * populated while sending the search results to view from Controller
	 * 
	 * Sorry for the naming convention, its too late for me to amend things!!!
	 * :(
	 */
	private String				selectedFirms;
	private String				selectedLocation;
	private String				selectedPracticeArea;
	private String				selectedChangeTypes;
	private String				selectedTitles;
	
	

	public void init()
	{
		// FirmType
		/*firmType = ALMConstants.RIVALEDGE_LIST;

		firmList = new ArrayList<String>();
		firmList.add(ALMConstants.AMLAW_100);
		
		firmListWatchList = new ArrayList<Integer>();*/
		
		firmList = new ArrayList<String>();
		firmListWatchList = new ArrayList<Integer>();
		
		titles = new ArrayList<String>();
		titles.add(ALMConstants.ALL_TITLES);

		locations = new ArrayList<String>();
		locations.add(ALMConstants.ALL_LOCATIONS);

		

		dateText = WebUtil.getDefaultDateRangeTextForSearch();

		practiceArea = new ArrayList<String>();
		practiceArea.add(ALMConstants.ALL_PRACTICE_AREAS);
		changeType = new ArrayList<String>();
		changeType.add(ALMConstants.ADDITIONS);
		changeType.add(ALMConstants.REMOVALS);
		
	
		
		goToPage = 1;
		sortColumn = 3; // default sorting based on Attorney name
		sortOrder = "asc";
		orderBy = 6; // default grouping based on firms
		searchResultsPerPage = 100;

	}

	public List<Integer> getFirmListWatchList() {
		return firmListWatchList;
	}

	public void setFirmListWatchList(List<Integer> firmListWatchList) {
		this.firmListWatchList = firmListWatchList;
	}

	public List<String> getFirmList()
	{
		return firmList;
	}

	public void setFirmList(List<String> firmList)
	{
		this.firmList = firmList;
	}

	public String getFirmType()
	{
		return firmType;
	}

	public void setFirmType(String firmType)
	{
		this.firmType = firmType;
	}

	public List<String> getChangeType()
	{
		return changeType;
	}

	public void setChangeType(List<String> changeType)
	{
		this.changeType = changeType;
	}

	public List<String> getTitles()
	{
		return titles;
	}

	public void setTitles(List<String> titles)
	{
		this.titles = titles;
	}

	public String getDateText()
	{
		return dateText;
	}

	public void setDateText(String dateText)
	{
		this.dateText = dateText;
	}

	public String getKeywords()
	{
		return keywords;
	}

	public void setKeywords(String keywords)
	{
		this.keywords = keywords;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public List<String> getPracticeArea()
	{
		return practiceArea;
	}

	public void setPracticeArea(List<String> practiceArea)
	{
		this.practiceArea = practiceArea;
	}

	public List<String> getLocations()
	{
		return locations;
	}

	public void setLocations(List<String> location)
	{
		this.locations = location;
	}

	public String getSelectedFirms()
	{
		return selectedFirms;
	}

	public void setSelectedFirms(String selectedFirms)
	{
		this.selectedFirms = selectedFirms;
	}

	public String getSelectedPracticeArea()
	{
		return selectedPracticeArea;
	}

	public void setSelectedPracticeArea(String selectedPracticeArea)
	{
		this.selectedPracticeArea = selectedPracticeArea;
	}

	public String getSelectedLocation()
	{
		return selectedLocation;
	}

	public void setSelectedLocation(String selectedLocation)
	{
		this.selectedLocation = selectedLocation;
	}

	public String getSelectedChangeTypes()
	{
		return selectedChangeTypes;
	}

	public void setSelectedChangeTypes(String selectedChangeTypes)
	{
		this.selectedChangeTypes = selectedChangeTypes;
	}

	public String getSelectedTitles()
	{
		return selectedTitles;
	}

	public void setSelectedTitles(String selectedTitles)
	{
		this.selectedTitles = selectedTitles;
	}

	

}
