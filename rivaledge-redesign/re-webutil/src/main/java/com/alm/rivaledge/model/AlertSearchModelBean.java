package com.alm.rivaledge.model;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;

import com.alm.rivaledge.util.ALMConstants;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.StringUtil;

public class AlertSearchModelBean extends BaseSearchModelBean {

	/**
	 * 
	 */
	private static final long serialVersionUID = 583303142317121653L;

	private String alertType;
	private Integer alertFrequency;
	private String alertName;
	private List<String> alertEmails;
	private boolean isActive;

	private String alertEmailsStr;
	
	public void init() {
		alertType = ALMConstants.NEWS_AND_PUBS;
		alertFrequency = ALMConstants.DAILY_VALUE;
	}

	public String getAlertType() {
		return alertType;
	}

	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}

	public Integer getAlertFrequency() {
		return alertFrequency;
	}

	public void setAlertFrequency(Integer alertFrequency) {
		this.alertFrequency = alertFrequency;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public List<String> getAlertEmails() {
		return alertEmails;
	}

	public void setAlertEmails(List<String> alertEmails) {
		this.alertEmails = alertEmails;
	}

	public String getAlertName() {
		return alertName;
	}

	public void setAlertName(String alertName) {
		this.alertName = alertName;
	}
	
	public void printBean(){
		System.out.println("AlertSearchModelBean :: ");
		System.out.println("Name: " + this.alertName);
		System.out.println("Type: " + this.alertType);
		System.out.println("Freq: " + this.alertFrequency);
		if(this.alertEmails!=null){
			for(String email: this.alertEmails){
				System.out.println("Email: " + email);
			}
		}
		System.out.println("Active:" + this.isActive);
	}
	
	public void createBeanFromRequest(ActionRequest request){
		if(request!=null){
			String alertName = request.getParameter("selectedAlertName").trim();
			String alertType = request.getParameter("selectedAlertType").trim();
			String alertFrequencyStr = request.getParameter("selectedAlertFrequency").trim();
			String isActiveStr = request.getParameter("isActive");
			String alertEmailsStr = request.getParameter("selectedAlertEmail").trim();
			if(alertEmailsStr != null){
				this.alertEmails = new ArrayList<String>();
				this.alertEmails.addAll(ListUtil.fromArray(StringUtil.split(alertEmailsStr, ";")));
			}
			if(alertName!=null){
				this.alertName = alertName.trim();
			}
			if(alertType!=null){
				this.alertType = alertType.trim();
			}
			if(alertFrequencyStr!=null && alertFrequencyStr.length()>0){
				try{
					this.alertFrequency = Integer.parseInt(alertFrequencyStr);
				}catch(NumberFormatException ex){
					if(ALMConstants.DAILY.equalsIgnoreCase(alertFrequencyStr)){
						this.alertFrequency = ALMConstants.DAILY_VALUE;
					}
					else if(ALMConstants.BI_WEEKLY.equalsIgnoreCase(alertFrequencyStr)){
						this.alertFrequency = ALMConstants.BI_WEEKLY_VALUE;
					}
					else if(ALMConstants.MONTHLY.equalsIgnoreCase(alertFrequencyStr)){
						this.alertFrequency = ALMConstants.MONTHLY_VALUE;
					}
					else if(ALMConstants.WEEKLY.equalsIgnoreCase(alertFrequencyStr)){
						this.alertFrequency = ALMConstants.WEEKLY_VALUE;
					}
				}
				
			}
			if(isActiveStr!=null){
				this.isActive = "true".equalsIgnoreCase(isActiveStr.trim()) ? true : false;
			}
		}
	}

	public AlertSearchModelBean(ActionRequest request) {
		createBeanFromRequest(request);
	}
	
	public AlertSearchModelBean() {
		// TODO Auto-generated constructor stub
	}
}
