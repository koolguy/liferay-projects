package com.alm.rivaledge.model;

/**
 * Model Objects declared in @SessionAttributes can extend this class to let Spring know <br>
 * if the instance of this class needs to be added/extended to
 * <code>PortletSession</code> or end their life in <code>Model</code> <br>
 * Cause of Inception : Changes to Search Criteria (held in *ModelBean) in
 * Search Portlets of all the modules in Alerts and Dashboards <br>
 * should end its life after Request <==> Response and should not span to
 * Session.<br>
 * Usage : Refer
 * {@link org.springframework.web.bind.support.custom.PortletSessionAttributeStore#storeAttribute()}
 */
public abstract class SessionAwareBean
{
	/**
	 * Checks if instance of this class has to be pushed to session from model or to
	 * stop its journey in model
	 */
	protected boolean addToSession = true; // add to session by default

	public boolean isAddToSession()
	{
		return addToSession;
	}

	public void setAddToSession(boolean addToSession)
	{
		this.addToSession = addToSession;
	}
}
