package com.alm.rivaledge.model;

import com.alm.rivaledge.transferobject.BaseSearchDTO;

/**
 * The instance of this class is responsible to hold the SearchModelBean ,
 * SearchDTO and any additional search criteria to show up the Search Results on
 * Details page when "Click To View Details" is clicked on Charts.
 * 
 * The attributes of this class are populate judiciously depending on the Chart and its location (Home or Analysis page)
 * 
 * @author Niranjan
 * @since 14-Nov-2013
 * @version 1.0
 * @param <Bean> SearchModelBean specific to Module
 * @param <DTO> SearcDTO specific to Module
 */
public class ClickToView<Bean extends BaseSearchModelBean, DTO extends BaseSearchDTO>
{
	
	private Bean searchModelBean;
	private DTO searchDTO;
	private String firmName;
	private String searchCriteriaJson;

	public Bean getSearchModelBean()
	{
		return searchModelBean;
	}

	public void setSearchModelBean(Bean searchModelBean)
	{
		this.searchModelBean = searchModelBean;
	}

	public DTO getSearchDTO()
	{
		return searchDTO;
	}

	public void setSearchDTO(DTO searchDTO)
	{
		this.searchDTO = searchDTO;
	}

	public String getFirmName()
	{
		return firmName;
	}

	public void setFirmName(String firmName)
	{
		this.firmName = firmName;
	}

	public String getSearchCriteriaJson()
	{
		return searchCriteriaJson;
	}

	public void setSearchCriteriaJson(String searchCriteriaJson)
	{
		this.searchCriteriaJson = searchCriteriaJson;
	}

}
