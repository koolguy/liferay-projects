package com.alm.rivaledge.model;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;


/**
 * Bean which holds the Search Criteria on JSP for Events Portlet
 * @author FL605
 *
 */
public class EventSearchModelBean extends BaseSearchModelBean
{
	private static final long serialVersionUID = 4064848772421507139L;
	
	/* variables carries values bidirectionally 
	 * JSP -> Controller and vice versa
	 * 
	 */
	private List<String> firmList;
	private List<Integer> firmListWatchList;
	private String firmType;
	private String dateText;

	private List<String> locations;
	private String keywords;
	private List<String> practiceArea;
	
	
	/* variables carries values unidirectionally 
	 * Controller -> JSP 
	 * Basically these carries information about displaying searchCriteria String in results jsp. and need not require any default values
	 * as these are populated while sending the search results to view from Controller
	 * 
	 * Sorry for the naming convention, its too late for me to amend things!!! :(
	 */
	private String selectedFirms;
	private String selectedLocation;
	private String selectedPracticeArea;
	
	
	public void init()
	{
		
		/*firmList = new ArrayList<String>();

		List<UserGroup> allWatchListsDefaultList = firmService.getWatchListSortedByDate();
		
		if(allWatchListsDefaultList == null || allWatchListsDefaultList.isEmpty())
		{
			//FirmType
			firmType = ALMConstants.RIVALEDGE_LIST;
			firmList.add(ALMConstants.AMLAW_100);
		} 
		else
		{
			firmType = ALMConstants.WATCH_LIST;
			firmList.add("" + allWatchListsDefaultList.get(0).getGroupId());
		}*/
		firmList = new ArrayList<String>();
		firmListWatchList = new ArrayList<Integer>();
		
		locations = new ArrayList<String>();
		locations.add(ALMConstants.ALL_LOCATIONS);
		
		
		//DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		//Date date = new Date();
		
		dateText = WebUtil.getDefaultDateRangeTextForSearch();
		
		practiceArea = new ArrayList<String>();
		practiceArea.add(ALMConstants.ALL_PRACTICE_AREAS);
		
		
		
		goToPage = 1;
		sortColumn = 9; //default will be on Firm Name
		sortOrder = "asc";
		orderBy = 9; //9 =  FirmName
		searchResultsPerPage = 100;
		
	}

	public String getFirmType()
	{
		return firmType;
	}

	public void setFirmType(String firmType)
	{
		this.firmType = firmType;
	}


	public List<String> getFirmList()
	{
		return firmList;
	}

	public void setFirmList(List<String> firmList)
	{
		this.firmList = firmList;
	}

	public String getDateText()
	{
		return dateText;
	}

	public void setDateText(String dateText)
	{
		this.dateText = dateText;
	}


	public String getKeywords()
	{
		return keywords;
	}

	public void setKeywords(String keywords)
	{
		this.keywords = keywords;
	}


	public List<String> getPracticeArea()
	{
		return practiceArea;
	}

	public void setPracticeArea(List<String> practiceArea)
	{
		this.practiceArea = practiceArea;
	}

	public List<String> getLocations() {
		return locations;
	}

	public void setLocations(List<String> location) {
		this.locations = location;
	}
	
	public String getSelectedFirms() {
		return selectedFirms;
	}

	public void setSelectedFirms(String selectedFirms) {
		this.selectedFirms = selectedFirms;
	}


	public String getSelectedPracticeArea() {
		return selectedPracticeArea;
	}

	public void setSelectedPracticeArea(String selectedPracticeArea) {
		this.selectedPracticeArea = selectedPracticeArea;
	}

	public String getSelectedLocation() {
		return selectedLocation;
	}

	public void setSelectedLocation(String selectedLocation) {
		this.selectedLocation = selectedLocation;
	}
	public List<Integer> getFirmListWatchList() {
		return firmListWatchList;
	}

	public void setFirmListWatchList(List<Integer> firmListWatchList) {
		this.firmListWatchList = firmListWatchList;
	}


}
