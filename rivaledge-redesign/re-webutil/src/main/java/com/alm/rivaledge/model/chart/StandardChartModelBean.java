package com.alm.rivaledge.model.chart;

import java.util.ArrayList;
import java.util.List;

import com.alm.rivaledge.util.ALMConstants;

public class StandardChartModelBean extends BaseChartModelBean
{
	private static final long serialVersionUID = 4034848772221507139L;

	private List<String> firmList;
	private List<String> watchList;
	private List<String> searchResultsFirmList;
	private List<String> practiceAreaList;
	/**
	 * Initialize the Chart model bean with default values
	 */
	public void init()
	{
		chartType = ChartType.STACKED_VERTICAL_BAR.getValue();
		
		if(ALMConstants.DEFAULT_NO_OF_COLUMNS_CHART == 10)
		{
			limitType = FirmDataType.TOP_10.getValue();
		}
		else
		{
			limitType = FirmDataType.TOP_15.getValue();
		}
		
		comparisonDataTypeList = new ArrayList<String>();
		
		comparisonDataTypeList.add(ComparisonDataType.RIVAL_EDGE.getValue());
		comparisonDataTypeList.add(ComparisonDataType.AVERAGE.getValue());
		
		firmList = new ArrayList<String>();
		firmList.add(ALMConstants.AMLAW_100);
	}
	
	public List<String> getFirmList()
	{
		return firmList;
	}

	public void setFirmList(List<String> firmList)
	{
		this.firmList = firmList;
	}

	public List<String> getWatchList()
	{
		return watchList;
	}

	public void setWatchList(List<String> watchList)
	{
		this.watchList = watchList;
	}

	public List<String> getSearchResultsFirmList()
	{
		return searchResultsFirmList;
	}

	public void setSearchResultsFirmList(List<String> searchResultsFirmList)
	{
		this.searchResultsFirmList = searchResultsFirmList;
	}

	public List<String> getPracticeAreaList()
	{
		return practiceAreaList;
	}

	public void setPracticeAreaList(List<String> practiceAreaList)
	{
		this.practiceAreaList = practiceAreaList;
	}
}