package com.alm.rivaledge.model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import com.alm.rivaledge.util.ALMConstants;


public class AttorneySearchModelBean extends BaseSearchModelBean{

	private static final long serialVersionUID = 4064848772421507139L;
	
	/* variables carries values bidirectionally 
	 * JSP -> Controller and vice versa
	 * 
	 */
	
	private List<String> firmList;
	private List<Integer> firmListWatchList;
	private String firmType;
	private String fromDate;
	private String toDate;
	private String admissions;
	private String lawSchools;
	private String name;
	private String keywords;
	private String firstListedPractice;
	
	private List<String> locations;
	private List<String> practiceArea;
	private List<String> title;
	private List<String> displayColumns;
	
	/* variables carries values unidirectionally 
	 * Controller -> JSP 
	 * Basically these carries information about displaying searchCriteria String in results jsp. and need not require any default values
	 * as these are populated while sending the search results to view from Controller
	 * 
	 */
	
	private String selectedFirms;
	private String selectedLocation;
	private String selectedPracticeArea;
	private String selectedTitles;
	
	//RER-422
	private boolean showResult;
	
	public void init()
	{
		
		firmType = ALMConstants.RIVALEDGE_LIST;

		firmList = new ArrayList<String>();
		firmList.add(ALMConstants.AMLAW_100);
		
		firmListWatchList = new ArrayList<Integer>();
		
		locations = new ArrayList<String>();
		locations.add(ALMConstants.ALL_LOCATIONS);
		
		
		practiceArea = new ArrayList<String>();
		practiceArea.add(ALMConstants.ALL_PRACTICE_AREAS);
		
		title = new ArrayList<String>();
		title.add(ALMConstants.ALL);
		
		//fromDate = "1900";
		//toDate = "" + Calendar.getInstance().get(Calendar.YEAR); //always get the current Year
		fromDate = ALMConstants.ANY;
		toDate = ALMConstants.ANY;
		
		
		selectedLocation=ALMConstants.ALL_LOCATIONS;
		
		displayColumns = new ArrayList<String>();
		
		displayColumns.add(ALMConstants.ATTORNEY_DISPLAY_COLUMNS.firm.getValue());
		displayColumns.add(ALMConstants.ATTORNEY_DISPLAY_COLUMNS.title.getValue());
		displayColumns.add(ALMConstants.ATTORNEY_DISPLAY_COLUMNS.location.getValue());
		displayColumns.add(ALMConstants.ATTORNEY_DISPLAY_COLUMNS.practice.getValue());
		displayColumns.add(ALMConstants.ATTORNEY_DISPLAY_COLUMNS.education.getValue());
		
		goToPage = 1;
		sortColumn = 2; //Attorney Name
		sortOrder = "asc";
		orderBy = 5; //firm Name
		searchResultsPerPage = 100;
		
		showResult = Boolean.FALSE;
		
	}

	public List<String> getFirmList() {
		return firmList;
	}

	public void setFirmList(List<String> firmList) {
		this.firmList = firmList;
	}

	public String getFirmType() {
		return firmType;
	}

	public void setFirmType(String firmType) {
		this.firmType = firmType;
	}

	public List<String> getLocations() {
		return locations;
	}

	public void setLocations(List<String> locations) {
		this.locations = locations;
	}

	public List<String> getPracticeArea() {
		return practiceArea;
	}

	public void setPracticeArea(List<String> practiceArea) {
		this.practiceArea = practiceArea;
	}

	public List<String> getTitle() {
		return title;
	}

	public void setTitle(List<String> title) {
		this.title = title;
	}

	public String getSelectedFirms() {
		return selectedFirms;
	}

	public void setSelectedFirms(String selectedFirms) {
		this.selectedFirms = selectedFirms;
	}

	public String getSelectedLocation() {
		return selectedLocation;
	}

	public void setSelectedLocation(String selectedLocation) {
		this.selectedLocation = selectedLocation;
	}

	public String getSelectedPracticeArea() {
		return selectedPracticeArea;
	}

	public void setSelectedPracticeArea(String selectedPracticeArea) {
		this.selectedPracticeArea = selectedPracticeArea;
	}
	public String getFromDate() {
		return fromDate;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public String getAdmissions() {
		return admissions;
	}

	public void setAdmissions(String admissions) {
		this.admissions = admissions;
	}

	public String getLawSchools() {
		return lawSchools;
	}

	public void setLawSchools(String lawSchools) {
		this.lawSchools = lawSchools;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKeywords() {
		return keywords;
	}

	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	public List<String> getDisplayColumns()
	{
		return displayColumns;
	}

	public void setDisplayColumns(List<String> displayColumns)
	{
		this.displayColumns = displayColumns;
	}

	public String getFirstListedPractice()
	{
		return firstListedPractice;
	}

	public void setFirstListedPractice(String firstListedPractice)
	{
		this.firstListedPractice = firstListedPractice;
	}
	
	public boolean isShowResult() {
		return showResult;
	}

	public List<Integer> getFirmListWatchList() {
		return firmListWatchList;
	}

	public void setFirmListWatchList(List<Integer> firmListWatchList) {
		this.firmListWatchList = firmListWatchList;
	}

	public void setShowResult(boolean showResult) {
		this.showResult = showResult;
	}

	public String getSelectedTitles() {
		return selectedTitles;
	}

	public void setSelectedTitles(String selectedTitles) {
		this.selectedTitles = selectedTitles;
	}

}