package com.alm.rivaledge.model.chart;

import java.io.Serializable;
import java.util.List;

public class BaseChartModelBean implements Serializable, Cloneable
{
	private static final long serialVersionUID = 4064848772221507139L;
	/**
	 * Chart Type Values on View Settings of Charts
	 * 
	 * @author Niranjan
	 * @since 22-Sep-2013
	 * @version 1.0
	 */
	public static enum ChartType
	{

		PIE("pie", "pie"),
		LINE("line", "line"),
		VERTICAL_BAR("vertical_bar", "column"),
		HORIZONTAL_BAR("horizontal_bar", "bar"),
		STACKED_VERTICAL_BAR("stacked_vertical_bar", "column"),
		STACKED_HORIZONTAL_BAR("stacked_horizontal_bar", "bar");

		private String displayValue;
		private String value;

		ChartType(String value, String displayValue)
		{
			this.value = value;
			this.displayValue = displayValue;
		}

		public String getHighChartValue()
		{
			return displayValue;
		}

		public String getValue()
		{
			return value;
		}

		@Override
		public String toString()
		{
			return name();
		}
	}

	/**
	 * Limit Values for Firms on View Settings of Charts
	 * @author Niranjan
	 * @since 22-Sep-2013
	 * @version 1.0
	 */
	public static enum FirmDataType
	{
		TOP_5("top_5"),
		TOP_10("top_10"),
		TOP_15("top_15"),
		BOTTOM_5("bottom_5"),
		BOTTOM_10("bottom_10"),
		BOTTOM_15("bottom_15"),
		FIRM("firm"),
		PRACTICE("practice");
		
		private String value;

		FirmDataType(String value)
		{
			this.value = value;
		}

		public String getValue()
		{
			return value;
		}

		@Override
		public String toString()
		{
			return name();
		}
	}

	/**
	 * Comparison Type Values on View Settings of Charts
	 * @author Niranjan
	 * @since 22-Sep-2013
	 * @version 1.0
	 */
	public static enum ComparisonDataType
	{
		MY_FIRM("My Firm"),
		RIVAL_EDGE("RivalEdge Average"),
		WATCHLIST_AVG("Watchlist Average"),
		AVERAGE("Average");

		private String value;

		ComparisonDataType(String value)
		{
			this.value = value;
		}

		public String getValue()
		{
			return value;
		}

		@Override
		public String toString()
		{
			return name();
		}
	}

	public BaseChartModelBean()
	{
		super();
	}

	protected String chartType;
	protected String limitType;
	protected List<String> comparisonDataTypeList;

	public String getChartType()
	{
		return chartType;
	}

	public void setChartType(String chartType)
	{
		this.chartType = chartType;
	}

	public String getLimitType()
	{
		return limitType;
	}

	public void setLimitType(String limitType)
	{
		this.limitType = limitType;
	}

	public List<String> getComparisonDataTypeList()
	{
		return comparisonDataTypeList;
	}

	public void setComparisonDataTypeList(List<String> comparisonDataTypeList)
	{
		this.comparisonDataTypeList = comparisonDataTypeList;
	}
	
	
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}
}
