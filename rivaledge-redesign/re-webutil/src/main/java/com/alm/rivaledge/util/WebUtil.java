
package com.alm.rivaledge.util;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;

import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.ui.ModelMap;

import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.service.UserService;
import com.alm.rivaledge.service.WatchlistService;
import com.alm.rivaledge.transferobject.PeopleResult;
import com.alm.rivaledge.transferobject.UserGroupDTO;
import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.LayoutTypePortlet;
import com.liferay.portal.service.LayoutLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;

/**
 * @author FL889
 * @since 12 Sep 2013
 * @version 1.0
 */
public class WebUtil
{
	
	//private static DozerBeanMapper mapper = new DozerBeanMapper();
	private static Log			_log	= LogFactoryUtil.getLog(WebUtil.class.getName());
	
	private static Gson gson = new Gson();

	public static Map<String, Integer> sortMapByIntegerValue(Map<String, Integer> rawMap, int maxSize, final boolean isDescending)
	{
		List<Entry<String, Integer>> rawList = new LinkedList<Entry<String, Integer>>(rawMap.entrySet());

		Collections.sort(rawList, new Comparator<Entry<String, Integer>>()
		{
			@Override
			public int compare(Entry<String, Integer> thisEntry, Entry<String, Integer> thatEntry)
			{
				if (isDescending)
				{
					return (thatEntry.getValue().compareTo(thisEntry.getValue()));
				}
				else
				{
					return (thisEntry.getValue().compareTo(thatEntry.getValue()));
				}
			}
		});

		if (rawList.size() > maxSize)
		{
			rawList = rawList.subList(0, maxSize);
		}

		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>(rawList.size());
		for (Entry<String, Integer> thisEntry : rawList)
		{
			sortedMap.put(thisEntry.getKey(), thisEntry.getValue());
		}

		return sortedMap;
	}
	
	public static Map<String, Integer> sortMapByIntegerValue(Map<String, Integer> rawMap, final boolean isDescending)
	{
		List<Entry<String, Integer>> rawList = new LinkedList<Entry<String, Integer>>(rawMap.entrySet());

		Collections.sort(rawList, new Comparator<Entry<String, Integer>>()
		{
			@Override
			public int compare(Entry<String, Integer> thisEntry, Entry<String, Integer> thatEntry)
			{
				if (isDescending)
				{
					return (thatEntry.getValue().compareTo(thisEntry.getValue()));
				}
				else
				{
					return (thisEntry.getValue().compareTo(thatEntry.getValue()));
				}
			}
		});

		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>(rawList.size());
		for (Entry<String, Integer> thisEntry : rawList)
		{
			sortedMap.put(thisEntry.getKey(), thisEntry.getValue());
		}

		return sortedMap;
	}

	/**
	 * Utility method to access the ThemeDisplay object from any PortletRequest
	 * 
	 * @param portletRequest
	 * @return
	 */
	public static ThemeDisplay getThemeDisplay(PortletRequest portletRequest)
	{
		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest
				.getAttribute(com.liferay.portal.kernel.util.WebKeys.THEME_DISPLAY);
		return themeDisplay;
	}

	/**
	 * Returns the original HttpServletRequest
	 * 
	 * @param portletRequest
	 * @return
	 */
	public static HttpServletRequest getHttpServletRequest(PortletRequest portletRequest)
	{
		HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(portletRequest);
		httpRequest = PortalUtil.getOriginalServletRequest(httpRequest);
		return httpRequest;
	}

	public static String getDefaultDateRangeTextForSearch()
	{
		return ALMConstants.LAST_WEEK;
	}

	/**
	 * Copy properties using default DozerMapper
	 * 
	 * @param source
	 * @param target
	 *            Class Type
	 * @return
	 */
	/*public static <T> T copyProperties(Object source, Class<T> target)
	{

		return mapper.map(source, target);
	}
*/
	/**
	 * Copy properties using default DozerMapper
	 * 
	 * @param <T>
	 *            Type of Object
	 * @param source
	 * @param target
	 * @return
	 */
	/*public static <T> void copyProperties(Object source, T target)
	{
		mapper.map(source, target);
	}*/

	/**
	 * Compares two integer Objects either in asc or desc order taking null into
	 * account
	 * 
	 * @param mine
	 * @param your
	 * @param isDescending
	 *            true = descending Order
	 *            false = ascending Order
	 * @return compare result
	 */
	public static int compare(Integer mine, Integer your, boolean isDescending)
	{
		if (mine == null || your == null)
		{
			return 0;
		}
		if (isDescending)
		{
			return your - mine;

		}
		else
		{
			return mine - your;
		}

	}
	

	/**
	 * Compares two Date Objects either in asc or desc order taking null into
	 * account
	 * 
	 * @param mine
	 * @param your
	 * @param isDescending
	 *            true = descending Order
	 *            false = ascending Order
	 * @return compare result
	 */
	public static int compare(Date mine, Date your, boolean isDescending)
	{
		if (mine == null || your == null)
		{
			return 0;
		}
		if (isDescending)
		{
			return your.compareTo(mine);

		}
		else
		{
			return mine.compareTo(your);
		}

	}

	/**
	 * @param <T>
	 * @param searchResults
	 * @param cursor
	 * @return List
	 */
	public static <T> List<T> top(List<T> searchResults, int cursor)
	{
		List<T> returnMe = new ArrayList<T>(cursor);

		if (cursor > searchResults.size())
		{
			cursor = searchResults.size();
		}

		for (int index = 0; index < cursor; index++)
		{
			returnMe.add(searchResults.get(index));
		}

		return returnMe;
	}

	/**
	 * @param searchResults
	 * @param cursor
	 * @return
	 */
	public static <T> List<T> bottom(List<T> searchResults, int cursor)
	{

		List<T> returnMe = new ArrayList<T>(cursor);

		if (cursor > searchResults.size())
		{
			cursor = searchResults.size();
		}

		for (int index = searchResults.size() - 1; index >= searchResults.size() - cursor; index--)
		{
			returnMe.add(searchResults.get(index));

		}

		return returnMe;
	}

	/**
	 * Returns the cap for searchresults for Charts
	 * 
	 * @param currentUser
	 * @return
	 */
	public static int getResultSizeLimitForCharts(User currentUser)
	{
		int resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_SUBSCRIBER; // Default
		if ((currentUser != null) && (currentUser.getUsertype().equals(ALMConstants.RIVALEDGE_ADMIN)))
		{
			// This is an admin
			resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_RE_ADMIN;
		}
		return resultSetSizeCap;
	}

	/**
	 * Returns the ALM User Object
	 * 
	 * @param request
	 * @param userService
	 * @return
	 */
	public static User getCurrentUser(PortletRequest request, UserService userService)
	{
		HttpServletRequest servletRequest = PortalUtil.getHttpServletRequest(request);
		return getCurrentUser(servletRequest, userService);
	}
	
	/**
	 * Returns the ALM User Object
	 * 
	 * @param request
	 * @param userService
	 * @return
	 */
	public static User getCurrentUser(HttpServletRequest request, UserService userService)
	{
		User   currentUser 				= null;
		String currentUserEmailAddress 	= null;
		try
		{
			currentUserEmailAddress = PortalUtil.getUser(request).getEmailAddress();
			currentUser 			= userService.getUserByEmail(currentUserEmailAddress);
		}
		catch (Exception e) // fallback approach to handle NPE as well..
		{
			_log.fatal("Exception while fetching ALM User : " + e.getMessage(), e);
			currentUser = null;
		}
		return currentUser;
	}
	
	/**
	 * Returns whether user is alm Administrator  
	 * 
	 * @param User
	 * @return
	 */
	public static boolean isAdminUser(User currentUser)
	{
		boolean isAdminUser = false;
		
		if ((currentUser != null) && (currentUser.getUsertype().equals(ALMConstants.RIVALEDGE_ADMIN)))
		{
			
			isAdminUser = true;
		}
		
		return isAdminUser;
	}
	
	/**
	 * Returns the WatchList of the Current User
	 * 
	 * @param currentUser
	 * @param watchlistService
	 * @return Empty List when the User has no WatchList
	 */
	public static List<UserGroupDTO> getUserWatchLists(User currentUser, WatchlistService watchlistService)
	{

		List<UserGroupDTO> allWatchLists = new ArrayList<UserGroupDTO>();

		if (watchlistService != null && currentUser != null)
		{
			allWatchLists = (List<UserGroupDTO>) watchlistService.getAllFirmWatchList(currentUser.getId());
		}
		return allWatchLists;
	}

	/**
	 * Check if User is assigned to any firm
	 * 
	 * @param request
	 * @param userService
	 * @return
	 */
	public static boolean doesUserBelongToAnyFirm(PortletRequest request, UserService userService)
	{
		User currentUser = getCurrentUser(request, userService);
		return doesUserBelongToAnyFirm(currentUser);
	}

	/**
	 * Check if User is assigned to any firm
	 * 
	 * @param currentUser
	 * @return
	 */
	public static boolean doesUserBelongToAnyFirm(User currentUser)
	{
		boolean doIHaveFirm = false;
		if (currentUser != null && currentUser.getCompany() != null)
		{
			doIHaveFirm = true;
		}
		return doIHaveFirm;
	}

	/**
	 * The logic to get the firms of current user is still inconclusive and
	 * subjected to change.
	 * Its encouraged to fetch the MyFirm from this Util method so that it fill
	 * be a refactor at single point
	 * 
	 * @param currentUser
	 * @param firmService
	 * @return Empty List or list of Firms but never returns null
	 */
	public static List<Firm> getCurrentUserFirm(User currentUser, FirmService firmService)
	{
		List<Firm> myFirmList = new ArrayList<Firm>();
		if (currentUser != null)
		{
			try
			{
				Firm myFirm = firmService.getFirmById(Integer.parseInt(currentUser.getCompany()));
				if (myFirm != null)
				{
					myFirmList.add(myFirm);
				}
				
			}
			catch(Exception e)
			{
				
			}
		}
		return myFirmList;
	}

	/**
	 * This method calculates the percentage values and makes sure that <br>
	 * the summation of all the percentages after calculation are equated to 100
	 * 
	 * @param total
	 * @param calculateTotals currently not implemented, If implemented<br>
	 *            true ==> total = sum(parts) and ignore the given total
	 *            value while calculating percentages<br>
	 *            false ==> consider the given total value while calculating
	 *            percentages
	 * @param parts
	 * @return array of percentages of size equal to the given parts size
	 */
	public static int[] getPercentageRatio(int total, boolean calculateTotals, int... parts)
	{

		float[] decimals = new float[parts.length];
		int[] wholeNumbers = new int[parts.length];

		for (int i = 0; i < parts.length; i++)
		{
			//percentage calculation using integers yields dangerous results. Consider explicit float or double i.e., 100f
			//Reference : http://stackoverflow.com/a/7855404/781610

			float ratio = (float) parts[i] * 100 / total; // calculate the percentage as per our elementary school knowledge (Ex. 10.65)
			wholeNumbers[i] = (int) Math.floor(ratio); // get the number part (10)
			decimals[i] = ratio - wholeNumbers[i]; // get the decimal part(0.65)
		}

		float[] sortedDecimals = ArrayUtils.clone(decimals); // create a copy //Ex [0.32, 0.65, 0.11]
		Arrays.sort(sortedDecimals); // sort them, default is ascending order //Ex [0.11, 0.32, 0.65]
		ArrayUtils.reverse(sortedDecimals); // reverse them, we need descending order	//Ex [0.65, 0.32, 0.11]	 																																																																																																																																																																		
		int[] roundedNumbers = ArrayUtils.clone(wholeNumbers); // create a copy, just to print and compare in log

		for (int i = 0; i < sortedDecimals.length; i++)
		{
			int correspondingNumberIndex = ArrayUtils.indexOf(decimals, sortedDecimals[i]); // get the index from Original Decimal array corresponding to the sorted array value
			if (incrementTo100(roundedNumbers, correspondingNumberIndex))
			{
				break;
			}
		}
		// uncomment below code for debugging purpose
		/*_log.debug("Original Values : " + Arrays.toString(parts));
		_log.debug("Number Values : " + Arrays.toString(wholeNumbers));
		_log.debug("Decimals Values : " + Arrays.toString(decimals));
		_log.debug("Rounded Numbers: " + Arrays.toString(roundedNumbers));*/

		return roundedNumbers;
	}

	/**
	 * Increments the value in the given array @ given index if the total sum is not 100
	 * @param decimals
	 * @param index
	 * @return false if sum is not 100 in this run, false otherwise
	 */
	private static boolean incrementTo100(int[] decimals, int index)
	{
		int sum = 0;

		for (int decimal : decimals)
		{
			sum += decimal;
		}

		if (sum < 100)
		{
			decimals[index]++;
			return false;
		}
		return true;
	}

	/**
	 * Populates the Model with print time attributes*/
	public static void populatePrintTimings(ModelMap model)
	{
		
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat jDF = new SimpleDateFormat("hh:mm a");
		TimeZone zoneNYC = TimeZone.getTimeZone("America/New_York");
		jDF.setTimeZone(zoneNYC);

		Calendar rightNow = Calendar.getInstance();

		String printedDateStr = dateFormat.format(rightNow.getTime());
		String formattedTime = jDF.format(rightNow.getTime());
		
		model.addAttribute("printedDateStr", printedDateStr);
		model.addAttribute("formattedTime", formattedTime);
	}
	
	/**
	 * This method will return the start and end range of result for pagination
	 * 
	 * @param resultPerPage
	 * @param currentPage
	 * @param resultSize
	 * @return String[]
	 */
	public static String[] getResultRanges(int resultPerPage, int currentPage, int resultSize)
	{
		String[] resultRange = new String[2];
		
		int startRange = 0;
		int endRange = 0;
		
		if (resultSize > 0)
		{
			startRange = 1;
			int endValue = (resultSize < resultPerPage) ? resultSize : resultPerPage;  
			if(currentPage > 1)
			{
				startRange = ((currentPage-1)*resultPerPage) + 1;
			}
			endRange = startRange + (endValue-1);
			if( endValue > ((resultSize-startRange)+1) )
			{
				endRange = resultSize;
			}
		}
		
		resultRange[0] = String.valueOf(startRange);
		resultRange[1] = String.valueOf(endRange);
		
		return resultRange;
	}
	
	
	public static double calculatePercentage(PeopleResult ppl)
	{
		double percent = 0.0d;

		if (ppl != null)
		{
			double partnerPlus = (ppl.getPartnerCountPlus() != null) ? ((double) (ppl.getPartnerCountPlus())) : 0.0;
			double partnerMinus = (ppl.getPartnerCountMinus() != null) ? ((double) (ppl.getPartnerCountMinus())) : 0.0;
			double associatePlus = (ppl.getAssociateCountPlus() != null) ? ((double) (ppl.getAssociateCountPlus())) : 0.0;
			double associateMinus = (ppl.getAssociateCountMinus() != null) ? ((double) (ppl.getAssociateCountMinus())) : 0.0;
			double otherCounselPlus = (ppl.getOtherCounselCountPlus() != null) ? ((double) (ppl.getOtherCounselCountPlus()))
					: 0.0;
			double otherCounselMinus = (ppl.getOtherCounselCountMinus() != null) ? ((double) (ppl.getOtherCounselCountMinus()))
					: 0.0;
			double headCount = (ppl.getHeadCount() != null && ppl.getHeadCount() != 0) ? ((double) (ppl.getHeadCount())) : 1.0;
			/*
			percent = (partnerPlus + associatePlus + otherCounselPlus - partnerMinus - associateMinus - otherCounselMinus) 
					/ (headCount * 100.0d);
			*/

			// previous  calculation produces  values in this pattern (-9.932088432833552E-4)  
			// current calculation produces  values in this pattern (-9.932088285229202)  
			// Checkout the difference in code :-)
			// I don't know the reason but above calculation always yield 0 when formatted using DecimalFormat("###.#");
			//Investigate the problem, as it seems interesting, when you have ample time.
			percent = (partnerPlus + associatePlus + otherCounselPlus - partnerMinus - associateMinus - otherCounselMinus)
					/ headCount * 100.0d;
		}

		return percent;
		//return -25 + (int)(Math.random() * ((50) + 1)); // for testing purpose
	}
	
	/**
	 * Removes New Line, Carriage return, Tabs and Double Quotes from a given String
	 * @param title
	 * @return blank string if given string is null
	 */
	public static String cleanString(String title)
	{
		if (title == null)
		{
			return StringPool.BLANK;
		}

		return title.replaceAll("[\n\r\t\"]", StringPool.BLANK); 
	}
	
	/**
	 * Returns the json String representation of the given Object
	 * @param object
	 * @return
	 */
	public static String getJson(Object object)
	{
		return gson.toJson(object);
	}
	
	/**
	 * Returns the Object representation of the given type from the given json String
	 * @param json
	 * @param type
	 * @return
	 */
	public static <T> T getObject(String json, Class<T> type)
	{
		return gson.fromJson(json, type);
	}

	public static String getCurrentPortletId(PortletRequest request)
	{
		return (PortalUtil.getPortletId(PortalUtil.getHttpServletRequest(request)));
	}

	public static String getCurrentPage(PortletRequest request)
	{
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		return StringUtils.defaultIfBlank(themeDisplay.getLayout().getFriendlyURL().substring(1), ALMConstants.HOME_PAGE);
	}

	public static void removePortlet(PortletRequest request) throws PortalException, SystemException
	{
		// Plug it into the layout
		ThemeDisplay       themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	
		//Get portlet ID from PortalUtil
		String portletId = PortalUtil.getPortletId(PortalUtil.getHttpServletRequest(request));

		LayoutTypePortlet thisLayout   = themeDisplay.getLayoutTypePortlet();
		thisLayout.removePortletId(themeDisplay.getUserId(), portletId);
	
		// Update the layout
		LayoutLocalServiceUtil.updateLayout(themeDisplay.getLayout().getGroupId(), 
											themeDisplay.getLayout().isPrivateLayout(), 
											themeDisplay.getLayout().getLayoutId(), 
											themeDisplay.getLayout().getTypeSettings());
	}

	/**
	 * Clones (deep copy) the searchModelBean via the json String 
	 * @param searchBean
	 * @param type
	 * @return
	 */
	public static <T> T cloneBean(Object searchBean, Class<T> type)
	{
		String cloneString = getJson(searchBean);
		T clone = getObject(cloneString, type);
		return clone;
	}

	/**
	 * Check if the request is for Click To View details from Chart
	 * @param request
	 * @return
	 */
	public static boolean isClickToViewPage(PortletRequest request)
	{
		HttpServletRequest originalRequest = getHttpServletRequest(request);
		
		// Collect the  minimum info required  to decide if this is a C2V request.
		String searchCriteria = originalRequest.getParameter("searchCriteria");
		String firmName 	  = originalRequest.getParameter("drilldownFirmName");
		String dateText 	  = originalRequest.getParameter("drilldownDate");
		String practiceArea   = originalRequest.getParameter("drilldownPracticeArea");
		String location 	  = originalRequest.getParameter("drilldownLocation");
		String firmStats 	  = originalRequest.getParameter("firmStatsDrilldown");
		
		
		boolean isC2V = (searchCriteria!= null) || (dateText !=null) || (firmName !=null) || (practiceArea !=null) || (location !=null) || (firmStats != null);
		return isC2V;
		//return StringUtils.isNotBlank(firmName);
	}
}
