package com.alm.rivaledge.model;

import java.util.ArrayList;
import java.util.List;

import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;



public class FirmSearchModelBean extends BaseSearchModelBean
{
	private static final long serialVersionUID = 4064848772421507139L;
	
	/* variables carries values bidirectionally 
	 * JSP -> Controller and vice versa
	 * 
	 */
	private List<String> firmList;
	private List<Integer> firmListWatchList;
	private String firmType;
	private String dateText;
	private List<String> contentType;
	private String keywords;
	private List<String> twitterType;
	private List<String> practiceArea;
	

	/* variables carries values unidirectionally 
	 * Controller -> JSP 
	 * Basically these carries information about displaying searchCriteria String in results jsp. and need not require any default values
	 * as these are populated while sending the search results to view from Controller
	 * 
	 * Sorry for the naming convention, its too late for me to amend things!!! :(
	 */
	private String selectedFirms;
	private String selectedContentType;
	private String selectedPracticeArea;
	private String selectedTwitterPostType;
	
	
	public void init()
	{
		
		//FirmType
		firmType = ALMConstants.RIVALEDGE_LIST;
		
		firmList = new ArrayList<String>();
		firmList.add(ALMConstants.AMLAW_100);
		
		contentType = new ArrayList<String>();
		contentType.add(ALMConstants.ALL_Types);
		
		dateText = WebUtil.getDefaultDateRangeTextForSearch();
		
		practiceArea = new ArrayList<String>();
		practiceArea.add(ALMConstants.ALL_PRACTICE_AREAS);
		
		
		twitterType = new ArrayList<String>();
		twitterType.add(ALMConstants.ALL);
		
		goToPage = 1;
		sortColumn = 10;
		sortOrder = "desc";
		orderBy = -1; //-1 =  no grouping"
		searchResultsPerPage = 100;
		
	}

	public String getFirmType()
	{
		return firmType;
	}

	public void setFirmType(String firmType)
	{
		this.firmType = firmType;
	}

	public List<String> getFirmList()
	{
		return firmList;
	}

	public void setFirmList(List<String> firmList)
	{
		this.firmList = firmList;
	}

	public String getDateText()
	{
		return dateText;
	}

	public void setDateText(String dateText)
	{
		this.dateText = dateText;
	}

	public List<String> getContentType()
	{
		return contentType;
	}

	public void setContentType(List<String> contentType)
	{
		this.contentType = contentType;
	}

	public String getKeywords()
	{
		return keywords;
	}

	public void setKeywords(String keywords)
	{
		this.keywords = keywords;
	}

	public List<String> getTwitterType()
	{
		return twitterType;
	}

	public void setTwitterType(List<String> twitterType)
	{
		this.twitterType = twitterType;
	}

	public List<String> getPracticeArea()
	{
		return practiceArea;
	}

	public void setPracticeArea(List<String> practiceArea)
	{
		this.practiceArea = practiceArea;
	}

	
	public String getSelectedFirms() {
		return selectedFirms;
	}

	public void setSelectedFirms(String selectedFirms) {
		this.selectedFirms = selectedFirms;
	}

	public String getSelectedContentType() {
		return selectedContentType;
	}

	public void setSelectedContentType(String selectedContentType) {
		this.selectedContentType = selectedContentType;
	}

	public String getSelectedPracticeArea() {
		return selectedPracticeArea;
	}

	public void setSelectedPracticeArea(String selectedPracticeArea) {
		this.selectedPracticeArea = selectedPracticeArea;
	}

	public String getSelectedTwitterPostType() {
		return selectedTwitterPostType;
	}

	public void setSelectedTwitterPostType(String selectedTwitterPostType) {
		this.selectedTwitterPostType = selectedTwitterPostType;
	}

	public List<Integer> getFirmListWatchList() {
		return firmListWatchList;
	}

	public void setFirmListWatchList(List<Integer> firmListWatchList) {
		this.firmListWatchList = firmListWatchList;
	}

}
