package com.alm.rivaledge.model;

import java.io.Serializable;

public class BaseSearchModelBean extends SessionAwareBean implements Serializable, Cloneable
{
	private static final long	serialVersionUID	= 4064848772421507138L;
	/*
	 * variables carries values bidirectionally JSP -> Controller and vice versa
	 */
	protected int				goToPage;
	protected int				sortColumn;
	protected String			sortOrder;
	protected int				orderBy;
	protected int				searchResultsPerPage;

	protected String			portletId;

	/**
	 * This is used while rendering the search criteria String on Search Results
	 * and Printing
	 */
	protected boolean			printing			= false;

	private String				resultStartRange;
	private String				resultEndRange;

	/*
	 * variables carries values unidirectionally Controller -> JSP
	 */
	protected int				currentPageSize;
	protected int				totalResultCount;
	protected int				lastPage;

	public int getGoToPage()
	{
		return goToPage;
	}

	public void setGoToPage(int goToPage)
	{
		this.goToPage = goToPage;
	}

	public int getSortColumn()
	{
		return sortColumn;
	}

	public void setSortColumn(int sortColumn)
	{
		this.sortColumn = sortColumn;
	}

	public String getSortOrder()
	{
		return sortOrder;
	}

	public void setSortOrder(String sortOrder)
	{
		this.sortOrder = sortOrder;
	}

	public int getOrderBy()
	{
		return orderBy;
	}

	public void setOrderBy(int orderBy)
	{
		this.orderBy = orderBy;
	}

	public int getSearchResultsPerPage()
	{
		return searchResultsPerPage;
	}

	public void setSearchResultsPerPage(int searchResultsPerPage)
	{
		this.searchResultsPerPage = searchResultsPerPage;
	}

	public int getCurrentPageSize()
	{
		return currentPageSize;
	}

	public void setCurrentPageSize(int currentPageSize)
	{
		this.currentPageSize = currentPageSize;
	}

	public int getTotalResultCount()
	{
		return totalResultCount;
	}

	public void setTotalResultCount(int totalResultCount)
	{
		this.totalResultCount = totalResultCount;
	}

	public int getLastPage()
	{
		return lastPage;
	}

	public void setLastPage(int lastPage)
	{
		this.lastPage = lastPage;
	}

	public boolean isPrinting()
	{
		return printing;
	}

	/**
	 * This is used while rendering the search criteria String on Search Results
	 * and Printing
	 * 
	 * @param printing
	 */
	public void setPrinting(boolean printing)
	{
		this.printing = printing;
	}

	public String getResultStartRange()
	{
		return resultStartRange;
	}

	public void setResultStartRange(String resultStartRange)
	{
		this.resultStartRange = resultStartRange;
	}

	public String getResultEndRange()
	{
		return resultEndRange;
	}

	public void setResultEndRange(String resultEndRange)
	{
		this.resultEndRange = resultEndRange;
	}

	public String getPortletId()
	{
		return portletId;
	}

	public void setPortletId(String portletId)
	{
		this.portletId = portletId;
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}
}
