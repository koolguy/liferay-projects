package org.springframework.web.bind.support.custom;

import org.springframework.util.Assert;
import org.springframework.web.bind.support.DefaultSessionAttributeStore;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.portlet.context.PortletWebRequest;

import com.alm.rivaledge.model.SessionAwareBean;

public class PortletSessionAttributeStore extends DefaultSessionAttributeStore
{

	
	public PortletSessionAttributeStore()
	{
		super();
	}
	
	@Override
	public void storeAttribute(WebRequest request, String attributeName, Object attributeValue) {
		Assert.notNull(request, "WebRequest must not be null");
		Assert.notNull(attributeName, "Attribute name must not be null");
		Assert.notNull(attributeValue, "Attribute value must not be null");
		String storeAttributeName = getAttributeNameInSession(request, attributeName);
		
		boolean addToSession = true; // default flow
		
		if(attributeValue instanceof SessionAwareBean)
		{
			SessionAwareBean bean = (SessionAwareBean) attributeValue;
			addToSession = bean.isAddToSession(); // check if bean is interested in extending its life into session
		}
		
		if(addToSession) //yes, I'm Interested!
		{
			request.setAttribute(storeAttributeName, attributeValue, PortletWebRequest.SCOPE_GLOBAL_SESSION);
			
		}
	}

	@Override
	public Object retrieveAttribute(WebRequest request, String attributeName) {
		Assert.notNull(request, "WebRequest must not be null");
		Assert.notNull(attributeName, "Attribute name must not be null");
		String storeAttributeName = getAttributeNameInSession(request, attributeName);
		
		return request.getAttribute(storeAttributeName, PortletWebRequest.SCOPE_GLOBAL_SESSION);
	}

}