AUI().ready(

	/*
	This function gets loaded when all the HTML, not including the portlets, is
	loaded.
	*/

	function() {
	}
);

Liferay.Portlet.ready(

	/*
	This function gets loaded after each and every portlet on the page.

	portletId: the current portlet's id
	node: the Alloy Node object of the current portlet
	*/

	function(portletId, node) {
	}
);

Liferay.on(
	'allPortletsReady',
	/*
	This function gets loaded when everything, including the portlets, is on
	the page.
	*/

	function() {
	}
);

//Live Feed Page
$(document).ready(function() {
	$('#btnAdd').click(function(){
		$('.filtersPage').hide()
		$('#additional').show()
	})
	$('#btnSave').click(function(){
		$('.filtersPage').show()
		$('#additional').hide()
	});
	
$('#hide').click(
  function () {
      //show its submenu
      $("#firmPage").stop().slideToggle(500);    
  });
	
	$('#datenone').click(
  function () {
      //show its submenu
      $("#datePage").stop().slideToggle(500);    
  });
	
	$('a.login-window').click(function() {
		// Getting the variable's value from a link 
		var loginBox = $(this).attr('href');
		//Fade in the Popup and add close button
		$(loginBox).fadeIn(300);
		//Set the center alignment padding + border
		var popMargTop = ($(loginBox).height() + 24) / 2; 
		var popMargLeft = ($(loginBox).width() + 24) / 2; 
		$(loginBox).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		// Add the mask to body
		//$('body').append('<div id="mask"></div>');
		//$('#mask').fadeIn(300);
		//return false;
	});
	// When clicking on the button close or the mask layer the popup closed
	$('a.closewhite, #mask').bind('click', function() { 
	  $('#mask , .login-popup, .login-popupOne').fadeOut(300 , function() {
		$('#mask').remove();  
	}); 
	return false;
	});	
	
	(function(a){
	    a.fn.webwidget_tab=function(p){
	        var p=p||{};
	        var s_w_p=p&&p.window_padding?p.window_padding:"5";
	        var s_h_t_c=p&&p.head_text_color?p.head_text_color:"blue";
	        var s_h_c_t_c=p&&p.head_current_text_color?p.head_current_text_color:"black";
	        var dom=a(this);
	        s_w_p += "px";
	        
	        if(dom.find("ul").length==0||dom.find("li").length==0){
	            dom.append("Require content");
	            return null;
	        }
	        begin();
	        function begin(){
	            dom.children(".tabBody").children("ul").children("li").children("p").css("padding",s_w_p);
	            dom.children(".tabContainer").children(".tabHead").children("li").children("a").css("color",s_h_t_c);
	            dom.children(".tabBody").children("ul").children("li").hide();
	            dom.children(".tabBody").children("ul").children("li").eq(0).show();
	            dom.children(".tabContainer").children(".tabHead").children("li").children("a").click(function(){
	                var current = dom.children(".tabContainer").children(".tabHead").children("li").index($(this).parent());
	                dom.children(".tabContainer").children(".tabHead").children("li").children("a").css("color",s_h_t_c);
	                dom.children(".tabContainer").children(".tabHead").children("li").removeClass("currentBtn")
	                dom.children(".tabContainer").children(".tabHead").children("li").eq(current).children("a").css("color",s_h_c_t_c);
	                dom.children(".tabContainer").children(".tabHead").children("li").eq(current).addClass("currentBtn");
	                dom.children(".tabBody").children("ul").children("li").hide();
	                dom.children(".tabBody").children("ul").children("li").eq(current).show();
	            });
	        }
	    }
	})(jQuery);
});