	// JavaScript Document
$(document).ready(function() {
	
	
	
	$('a.login-window').click(function() {
		// Getting the variable's value from a link 
		var loginBox = $(this).attr('href');
		//Fade in the Popup and add close button
		$(loginBox).fadeIn(300);
		//Set the center alignment padding + border
		var popMargTop = ($(loginBox).height() + 24) / 2; 
		var popMargLeft = ($(loginBox).width() + 24) / 2; 
		$(loginBox).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		// Add the mask to body
		//$('body').append('<div id="mask"></div>');
		//$('#mask').fadeIn(300);
		//return false;
	});
	
	
	
	// When clicking on the button close or the mask layer the popup closed
	$('a.closewhite, #mask').bind('click', function() { 
	  $('#mask , .login-popup, .login-popupOne').fadeOut(300 , function() {
		$('#mask').remove();  
	}); 
	return false;
	});
	
	
	//Responsible for displaying page loading icon on clicking the hyperlinks on Navigation
	$('#nav a, #tabs>li>a, ul.topNav li a, .Tooltip-ClickToViewDetails, .clickToViewDetails a').click( function(){
		Liferay.Portlet.showBusyIcon("#bodyId","Loading..."); // show Busy Icon
	});
	
		
	/**
	 * This method is responsible for centering the Loading Icon...
	 */
	window.onresize = function(event) {
		var loadingMaskNode = $('.aui-loadingmask-message');
		//now loadingMask is a jQuery wrapped Node. 
			if(loadingMaskNode.length) // does loading mask exist?
			{
				var vpw = $(window).width();
				var vph = $(window).height();
				loadingMaskNode.css('top',  (vph/2) + 'px')
								.css('left', (vpw/2) + 'px');
				//console.log("applying busyIcon height("+ (vph/2) + ") and width("+(vpw/2)+")");
			}
		}

	
	
	
});


/**
 * @author Niranjan
 * @since 19-Nov-2013
 * serializeObject: Is used to prepare the param Map and do an ajax submit of form
 * Ex usage : $.ajax({
			    url: "${persistSearchCriteriaForChartURL}",
			    method: "POST",
			    traditional: true, // required in our case, refer to the jquery API 
			    data: $('#firmSearchModelBean').serializeObject(),
 */
(function($){
    $.fn.serializeObject = function () {
    	 var o = {};
    	    var a = this.serializeArray();
    	    $.each(a, function() {
    	    	// If node with same name exists already, need to convert it to an array as it
    	        // is a multi-value field (i.e., checkboxes)
    	        if (o[this.name] !== undefined) {
    	            if (!o[this.name].push) {
    	                o[this.name] = [o[this.name]];
    	            }
    	            o[this.name].push(this.value || '');
    	        } else {
    	            o[this.name] = this.value || '';
    	        }
    	       
    	    });
    	    return o;
    };
})(jQuery);

/**
 * @author Niranjan
 * @since 19-Nov-2013
 * showBusyIcon :  shows the Loading mask
 * Ex usage : Liferay.Portlet.showBusyIcon("#chartSearchPortletPopupId","Loading Search Criteria...");
 * 
 */

Liferay.provide(
		Liferay.Portlet,
		'showBusyIcon',
		function(busyIconId, msg) {
			var A = AUI();
			var message = msg || "Loading...";
			 if (A.one(busyIconId).loadingmask == null)
				 {
				 
				   A.one(busyIconId).plug(A.LoadingMask, {
					   background: "#000", 
					   strings: {
								loading: message
					    }   
				   });
				 }
			  A.one(busyIconId).loadingmask.show();
		},
		['aui-loading-mask']
	);

/**
 * @author Niranjan
 * @since 19-Nov-2013
 * 
 * hideBusyIcon : Hides the Loading mask
 * Ex usage : Liferay.Portlet.hideBusyIcon("#chartSearchPortletPopupId");
 */

Liferay.provide(
		Liferay.Portlet,
		'hideBusyIcon',
		function(busyIconId) {
			var A = AUI();
			 if (A.one(busyIconId).loadingmask == null)
				 {
				   A.one(busyIconId).plug(A.LoadingMask, {
					   background: '#000' 
				   });
				 }
			  A.one(busyIconId).loadingmask.hide();
		},
		['aui-loading-mask']
	);


/**
 * @author Niranjan
 * @since 19-Nov-2013
 * 
 * showPopup : Is responsible to load the given url content into a popup
 * 
 * Wrap all the params into a json object and send as param to this function while invoking.
 * @param options json object with all he configuration params
 * @param uri is mandatory
 * @param rest all params are optional
 * 
 * @usage 
 */
// this is main

Liferay.provide(
		Liferay.Portlet,
        'showPopup',
        function(options) {
			
			var A = AUI();
			
            // this is another way of calling a iframe pop-up
            // this way is not specific to liferay
           var popupDialog = new A.Dialog(
                {
                    id: options.id || 'chartSearchPortletPopupId',
                    centered: options.centered || true,
                    draggable: options.draggable || true,
                    resizable: options.resizable || false,
                    width: options.width || 1100,
                    height: options.height || 350,
                    stack: options.stack || true,
                    title: options.title || 'Chart Search Criteria',
                    destroyOnClose: options.destroyOnClose || true,
                    modal: options.modal || true,
                    constrain2view: options.constrain2view || true,
                    close : false,
                   /* buttons: [ {
                        text: 'Close X',
                        handler: function() {
                            this.close();
                        }
                    }],*/
                   // footerContent: '<div class="alm-footer-content">Hello Hero!!</div>',
                    on: {
                    	close: options.close || function(){}
                    },
                    after: {
                    	render: function(){
                    		A.DialogMask.set('target', document); // Mask the whole page
                    		A.DialogMask.refreshMask();
                    	},
	                	renderedChange:function(e){
	                		
	                		if(e.newVal) // rendered == true
	                			{
	                			var delayed = new A.DelayedTask(
	                				  function() {
	                					  var popupToolbar = A.one("#chartSearchPortletPopupId .aui-toolbar-content");
	                					  if(popupToolbar)
	                					  {
	                						  popupToolbar.setContent('X');
	                						  popupToolbar.on('click', function(){popupDialog.close();})
	                					  }
	                				 	 }
	                				   );
	                				 
	                				 	// executes after 500ms the callback
	                				delayed.delay(500);
	                			}
	                		
	                	}
                    }
                }
            );
            
            popupDialog.plug(
                A.Plugin.IO,
                {
                    uri: options.uri,
                    iframeCssClass: 'alm-chart-dialog-iframe' // does this have any effect?
                }
            );
            popupDialog.render();
        },
    ['aui-dialog','aui-dialog-iframe', 'aui-loading-mask']
);




/*
Liferay.provide(
		Liferay.Portlet,
        'showPopup',
        function(options) {
			
			
			
			var A = AUI();
			
            // this is another way of calling a iframe pop-up
            // this way is not specific to liferay
           var popupDialog = new A.Dialog(
                {
                	bodyContent: '<style>iframe{overflow:hidden;}</style><iframe style="border: 0px; " src="' + options.uri + '" width="100%" height="100%"></iframe>',
                	id: options.id || 'chartSearchPortletPopupId',
                    centered: options.centered || true,
                    draggable: options.draggable || true,
                    resizable: options.resizable || true,
                    width: options.width || 1100,
                    height: options.height || 350,
                    stack: options.stack || true,
                    title: options.title || 'Chart Search Criteria',
                    destroyOnClose: options.destroyOnClose || true,
                    modal: options.modal || true,
                    on: {
                    	close: options.close || function(){}
                    }
                }
            ).render();
         
			$("<style>iframe{  overflow:hidden;}</style><div id='chartSearchPortletPopupId'></div>")
            .html('<iframe style="border: 0px; " src="' + options.uri + '" width="100%" height="100%"></iframe>')
            .dialog({
            	 autoOpen: true,
	       	      show: 'fade',
	       	      hide: 'fade',
	       	      title:'Filter Criteria',
	       	      position: 'center',
	       	      modal: true,
	       	      width: 1100,
	       	      closeOnEscape: true,
	       	      height: 350,
	       	      minHeight: 350,
	       	      closeText: 'x CLOSE',
	       	      dialogClass: "myClass",
	       	      resizable: true,
	       	      draggable:false
            });
            
			
			
		},
    ['aui-dialog','aui-dialog-iframe', 'aui-loading-mask']
);
*/

/**
 * @author Niranjan
 * @since 20-Nov-2013
 * This a wrapper around Liferay.Portlet.refresh
 * @param portlet portlet ID
 * @param msg Message to display while loading the portlet
 * function responsible to display customized loading icon while refreshing the portlet content
 */
Liferay.provide(
		Liferay.Portlet,
		'refreshPortlet',
		function(portlet, msg) {
			var instance = this;

			var A = AUI();
			var portletId = portlet;
			portlet = A.one(portlet);

			if (portlet && portlet.refreshURL) {
				var url = portlet.refreshURL;
				var id = portlet.attr('portlet');

				var placeHolder = A.Node.create('<div id="p_load' + id + '" />');

				portlet.placeBefore(placeHolder);
				portlet.remove(true);
				//Liferay.Portlet.showBusyIcon(portletId, msg || "Loading...."); 
				Liferay.Portlet.showBusyIcon("#p_load"+  id, msg || "Loading...."); 
				
				instance.addHTML(
					{
						data: {
							p_p_state: 'normal'
						},
						onComplete: function(portlet, portletId) {
							portlet.refreshURL = url;
						},
						placeHolder: placeHolder,
						url: url
					}
				);
			}
		},
		['aui-base']
	);


/**
 * @author Niranjan
 * @since 20-Nov-2013
 * 
 * This a wrapper around Liferay.Portlet.close
 * @param portlet portlet ID
 * function responsible to remove the chart
 */

Liferay.provide(
		Liferay.Portlet,
		'removePortlet',
		function(portlet, skipConfirm, options) {
			var instance = this;

			var A = AUI();

			portlet = A.one(portlet);

			if (portlet && (skipConfirm || confirm(Liferay.Language.get(options.msg)))) {
				options = options || {};

				options.plid = options.plid || themeDisplay.getPlid();
				options.doAsUserId = options.doAsUserId || themeDisplay.getDoAsUserIdEncoded();
				options.portlet = portlet;
				options.portletId = portlet.portletId;

				Liferay.fire('closePortlet', options);
			}
			else {
				self.focus();
			}
		},
		['aui-base']
	);

	
/**
 * Removes a chart. For that matters any portlet
 */
	Liferay.provide(
		window,
		'removeChartPortlet',
		function(portletId) {
			
			Liferay.Portlet.removePortlet(portletId, false, {msg : "Are you sure you want to remove this Chart?"});
			location.reload();
			//Liferay.Portlet.refresh("#p_p_id_rechartmanager_WAR_rechartmanager_", true); // default liferay refresh
			
		},
		['aui-base']
	);
	
	/**
	 * Attaches the method to window so that can be used directly
	 */
	Liferay.provide(
			window,
			'applyChartSettings',
			function(formId, modalId, loadingMsg) {
				modalId = modalId || "#bodyId";
				loadingMsg = loadingMsg || "Loading...";
				Liferay.Portlet.showBusyIcon(modalId, loadingMsg);
				
				AUI().one(formId).submit();
			},
			['aui-base']
		);
	
	/**
	 * Convinient method to show the busy Icon
	 */
	Liferay.provide(
			window,
			'showLoadingIcon',
			function() {
				Liferay.Portlet.showBusyIcon("#bodyId","Loading..."); // show Busy Icon
			},
			['aui-base']
		);
	
