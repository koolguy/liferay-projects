package com.alm.rivaledge.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.portlet.MockRenderRequest;
import org.springframework.mock.web.portlet.MockRenderResponse;
import org.springframework.mock.web.portlet.MockResourceRequest;
import org.springframework.mock.web.portlet.MockResourceResponse;
import org.springframework.web.portlet.ModelAndView;

import com.alm.rivaledge.BaseALMTest;
import com.alm.rivaledge.controller.firmstatistics.search.FirmStatisticsSearchController;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.transferobject.PeopleSearchDTO;
import com.alm.rivaledge.util.ALMConstants;

/**
 * Unit test cases for People Controller
 * @author FL605
 * @since Sprint 3
 *
 */
public class PeopleControllerTest extends BaseALMTest
{
	@Autowired
	private FirmStatisticsSearchController testController;
	
	/**
	 * Test that the form view returns everything needed for the search.
	 */
	@Test
	@SuppressWarnings("unchecked")
	public void testDefaultFormView()
	{
		printTitle();
		assertNotNull(testController);		
		MockRenderRequest testRequest = new MockRenderRequest();
		MockRenderResponse testResponse = new MockRenderResponse();
		ModelAndView testMAV = null;
		try
		{
//			testMAV = testController.peopleSearchForm(testRequest, testResponse);
		}
		catch (Exception ex)
		{
			// Do nothing
		}
		assertTrue(testMAV.getViewName().equals("peoplesearch"));
		assertTrue(testMAV.getModel().keySet().contains("peopleModel"));

		Map<String, Collection<?>> testModel = (Map<String, Collection<?>>) testMAV.getModel().get("peopleModel");
		assertTrue(testModel.keySet().contains("allFirms"));
		assertTrue(testModel.keySet().contains("allPracticeArea"));
		
		Collection<Firm> testFirms = (Collection<Firm>) testModel.get("allFirms");
		assertNotNull(testFirms);
	}
	
	/**
	 * Test default values of People search Form
	 */
	@Test	
	public void testSetOtherSearchParameterWithDefaultValues()
	{
		printTitle();
		PeopleSearchDTO peopleSearchDTO =new PeopleSearchDTO();
		MockResourceRequest testRequest = new MockResourceRequest();
		MockResourceResponse testResponse = new MockResourceResponse();
		
		testRequest.setParameter("selectedPracticeArea", "All Practice Areas");
		testRequest.setParameter("selectedLocation", ALMConstants.ALL_LOCATIONS);
		testRequest.setParameter("selectedFirmSize", ALMConstants.ALL_FIRMS);
		
		try
		{
//		testController.setOtherSearchParameter(testRequest, testResponse, peopleSearchDTO);
		} 
		catch (Exception ex)
		{
			// Do nothing
		}
		assertNull(peopleSearchDTO.getPracticeArea());
		assertNull(peopleSearchDTO.getLocations());
		assertNull(peopleSearchDTO.getFirmSizeList());
			
		
	}
	
	
	/**
	 * Test  People search Form values apart form default
	 */
	@Test	
	public void testSetOtherSearchParameterWithoutDefaultValues()
	{
		printTitle();
		PeopleSearchDTO peopleSearchDTO =new PeopleSearchDTO();
		MockResourceRequest testRequest = new MockResourceRequest();
		MockResourceResponse testResponse = new MockResourceResponse();
		
		testRequest.setParameter("selectedPracticeArea", "Admiralty, Aviation and Transportation;Aerospace and Defense");
		testRequest.setParameter("selectedLocation", "Boston, MA;");
		testRequest.setParameter("selectedFirmSize", "1000-1499"); 
		try
		{
//			testController.setOtherSearchParameter(testRequest, testResponse, peopleSearchDTO);
		} 
		catch (Exception ex)
		{
			// Do nothing
		}
		assertTrue(peopleSearchDTO.getPracticeArea().size()==2);
		assertTrue(peopleSearchDTO.getLocations().size()==1);
		assertNotNull(peopleSearchDTO.getFirmSizeList().size()==1);
			
		
	}

	/**
	 * Test selected Individual Firms
	 */
	@Test	
	public void testSelectedIndividualFirms()
	{
		printTitle(); 
		PeopleSearchDTO peopleSearchDTO =new PeopleSearchDTO();
		MockResourceRequest testRequest = new MockResourceRequest();
		testRequest.setParameter("selectedFirms", "47,31");
//		testController.setSelectedFirms(testRequest.getParameter("selectedFirms"), peopleSearchDTO, testRequest);
		assertTrue(peopleSearchDTO.getSelectedFirms().size()==2);
		assertTrue(peopleSearchDTO.getSelectedFirms().get(0).getCompany().equals("DLA Piper"));
		assertTrue(peopleSearchDTO.getSelectedFirms().get(1).getCompany().equals("Chadbourne Parke"));
	}


	 
	/**
	 * Test selected Rivaledge Firms
	 */
	@Test	
	public void testSelectedRivalEdgeFirms()
	{
		printTitle(); 
		PeopleSearchDTO peopleSearchDTO =new PeopleSearchDTO();
		MockResourceRequest testRequest = new MockResourceRequest();
		testRequest.setParameter("selectedFirms", ALMConstants.AMLAW_100);
//		testController.setSelectedFirms(testRequest.getParameter("selectedFirms"), peopleSearchDTO, testRequest);
		assertTrue(peopleSearchDTO.getSelectedFirms().size()>0);		
	}
	

	/**
	 * Test fetch search results functionality
	 */
	@Test	
	public void testApplySearchUrl()
	{
		printTitle(); 
		MockResourceRequest testRequest = new MockResourceRequest();
		MockResourceResponse testResponse = new MockResourceResponse();
		
		testRequest.setParameter("selectedPracticeArea", "All Practice Areas");
		testRequest.setParameter("selectedLocation", ALMConstants.ALL_LOCATIONS);
		testRequest.setParameter("selectedFirmSize", ALMConstants.ALL_FIRMS);
		testRequest.setParameter("selectedFirms", "31"); 
		testRequest.setParameter("Datetext", ALMConstants.LAST_90_DAYS);
		try  
		{
//			testController.applySearchUrl(testRequest, testResponse);
		} 
		catch(Exception ex)
		{
			//Do nothing
		}
		assertNotNull(testRequest.getAttribute("peopleResults"));
		assertNotNull(testRequest.getPortletSession().getAttribute("searchFormTo"));
		assertNotNull(testRequest.getPortletSession().getPortletContext().getRequestDispatcher("/WEB-INF/jsp/peoplesearchresults.jsp"));
		
	}
	
	/**
	 * Test fetch search results office drill down functionality functionality
	 */
	@Test	
	public void testOfficeDrillDown()
	{
		printTitle(); 
		MockResourceRequest testRequest = new MockResourceRequest();
		MockResourceResponse testResponse = new MockResourceResponse();
		
		testRequest.setParameter("selectedPracticeArea", "All Practice Areas");
		testRequest.setParameter("selectedLocation", ALMConstants.ALL_LOCATIONS);
		testRequest.setParameter("selectedFirmSize", ALMConstants.ALL_FIRMS);
		testRequest.setParameter("selectedFirms", "31"); 
		testRequest.setParameter("Datetext", ALMConstants.LAST_90_DAYS); 
		testRequest.setParameter("firmId", "31");
		try  
		{
//			testController.applySearchUrl(testRequest, testResponse);			
//			testController.officeDrillDown(testRequest, testResponse);
		} 
		catch(Exception ex)
		{
			//Do nothing
		}
		assertNotNull(testRequest.getAttribute("OfficeDrillDownData"));
		assertNotNull(testRequest.getAttribute("firmName"));
		assertNotNull(testRequest.getPortletSession().getPortletContext().getRequestDispatcher("/WEB-INF/jsp/peopleofficeresults.jsp"));
		
	}
	
	/**
	 * Test set Date range functionality
	 */
	@Test	
	public void testSetDateRange()
	{
		printTitle(); 
		Calendar calendar = Calendar.getInstance();
		Date currentDate = new Date();
		MockResourceRequest testRequest = new MockResourceRequest();
		MockResourceResponse testResponse = new MockResourceResponse();
		PeopleSearchDTO peopleSearchDTO =new PeopleSearchDTO();
		testRequest.setParameter("Datetext", ALMConstants.LAST_90_DAYS);
		
//		testController.setDateRange(testRequest, testResponse,peopleSearchDTO);
		calendar.add(Calendar.MONTH, -3);
		assertTrue(peopleSearchDTO.getToDate().toString().equals(currentDate.toString()));
		assertTrue(peopleSearchDTO.getFromDate().toString().equals(calendar.getTime().toString()));
		
		testRequest.setParameter("Datetext", ALMConstants.ANY);
//		testController.setDateRange(testRequest, testResponse,peopleSearchDTO);
		assertNull(peopleSearchDTO.getToDate());
		assertNull(peopleSearchDTO.getFromDate());
		
		
	}


	
	
	
}