<%@page import="com.alm.rivaledge.model.chart.BaseChartModelBean"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="java.util.Map"%>
<%@ taglib prefix="portlet" 		uri="http://java.sun.com/portlet_2_0"%> 
<%@ taglib prefix="c" 			 	uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"          uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form"            uri="http://www.springframework.org/tags/form"%>

<portlet:defineObjects/>

<%@ include file="./common.jsp" %>

<portlet:actionURL var="updateChartURL">
	<portlet:param name="action" value="updateChart"/>
</portlet:actionURL>

<portlet:actionURL var="removePortletURL">
	<portlet:param name="action" value="removePortleFromThePage"/>
</portlet:actionURL>


<script type="text/javascript">


(function () {
   
    
<%@ include file="./theme.jsp" %>

	<c:choose>
		<c:when test="${numAttorneysChartType eq 'stacked_horizontal_bar' }">
			var numAttorneys_typeOfChart = 'bar';	//This will be type of chart.
			var stackingType = 'normal';
		</c:when> 
		<c:when test="${numAttorneysChartType eq 'stacked_vertical_bar' }">
			var numAttorneys_typeOfChart = 'column';
			var stackingType = 'normal';
		</c:when>
		<c:when test="${numAttorneysChartType eq 'horizontal_bar' }">
			var numAttorneys_typeOfChart = 'bar';
			var stackingType = '';
		</c:when>
		<c:otherwise>
			var numAttorneys_typeOfChart = 'column';
			var stackingType = '';
		</c:otherwise>	
	</c:choose>

	var numAttorneys_titleOfChart 		= '${numAttorneysChartTitle}'; 		// This will be title of chart.
	var numAttorneysFirmNameList 		= [${numAttorneysFirmNameList}]; 	// This will be list of firms name.
	var numAttorneysPartnerCount 		= [${numAttorneysPartnerCount}]; 	 // This will be news count of individual firms.
	var numAttorneysAssociateCount 		= [${numAttorneysAssociateCount}]; 	// This will be publication count of individual firms.
	var numAttorneysOtherCounselCount 	= [${numAttorneysOtherCounselCount}]; // This will be twitter count of individual firms. 

	<c:choose>
	<c:when test="${(not empty numAttorneysCompareCount) and (numAttorneysCompareCount > 0)}">
		var numAttorneysCompareCount = ${numAttorneysCompareCount};
	</c:when> 	
	<c:otherwise>
		var numAttorneysCompareCount = 0;
	</c:otherwise>	
	</c:choose>

$(function()
{
	if(<%=!isHomePage%>)
	{
		if (numAttorneysFirmNameList == null || numAttorneysFirmNameList == '') 
		{
			$("#p_p_id<portlet:namespace/>").hide();
			$(".cph").show();
			return;
		}
	}
	else
	{
		if (numAttorneysFirmNameList == null || numAttorneysFirmNameList == '') 
		{
			$("#numberOfAttorneysChartDiv").removeClass("hideClass");
			$("#numberOfAttorneysChartContainer").addClass("hideClass");
				
			$("#firmStatHomeSearch_noOf_setting").hide();
			$("#No-Data-NoOfAttorney").addClass("No-Data-Charts");
			$("#firmStatHomeSearch_noOf").show();
		}
		else
		{
			$("#No-Data-NoOfAttorney").removeClass("No-Data-Charts");
			$("#numberOfAttorneysChartDiv").addClass("hideClass");
		}
	}
	
	
	$(".AnalyzeResultHeader").show();
	
	// When user clicks on analysis tab active-menu css class is removed from details tab.
	// And same class added to analysis tab.
	$("#firm-statistics-details").removeClass("active-menu");
	$("#firm-statistics-analysis").addClass("active-menu");
	
	$("#numberOfAttorneysApply, .closewhite").click(function(){
		$("#numberOfAttorneysViewSetting").hide();
	});
	
	$(".highcharts-button").click(function(){
		$("div#noAttorneyPopupDiv").hide();
	});
	$("div#noAttorneyPopupDiv").mouseover(function(){
		$("div#noAttorneyPopupDiv").show();
	});
	
	$("#numberOfAttorneysCancel").click(function(){
		$("#numberOfAttorneysViewSetting").hide();
	});
	
	$("div#noAttorneyPopupDiv").mouseover(function(){
		$("div#noAttorneyPopupDiv").show();
	});
	
	$("#numberOfAttorneysResetAll").click(function(){
		$("input[value=stacked_vertical_bar]").prop("checked", true);
		$("input[value=top_15]").prop("checked", true);
		$("#searchResultsFirmList > option").attr("selected",false);
		$("#watchList > option").attr("selected",false);
		$("#firmList > option").attr("selected",false);		
		$("input[value='My Firm']").prop("checked", false);
		$("input[value='RivalEdge Average']").prop("checked", true);
		$("input[value='Watchlist Average']").prop("checked", false);
		$("input[value='Average']").prop("checked", true);
		
	});

	$(".noOfAttorneySelectedFirm").click(function(){
		if($(".noOfAttorneySelectedFirm option:selected")){
			$("input[value=firm]").prop("checked", true);
		}
	});
	$("#firmList").change(function(){
		$("input[value='RivalEdge Average']").prop("checked", true);
	});
	$("#watchList").click(function(){
		if($("#watchList option:selected")){
			$("input[value='Watchlist Average']").prop("checked", true);
		}
	});
	
	
	$('#noOfAttorneyPrintCharts').click(function() {
        var chart = $('#numberOfAttorneysChartContainer').highcharts();
        chart.print();
    });
	$('#noOfAttorneyExportJPG').click(function() {
        var chart = $('#numberOfAttorneysChartContainer').highcharts();
        chart.exportChart({type: 'image/jpeg'});
    });
	$('#noOfAttorneyExportPNG').click(function() {
        var chart = $('#numberOfAttorneysChartContainer').highcharts();
        chart.exportChart({type: 'image/png'});
    });
	
	var numAttorneysPartnerCountList = [];
	var numAttorneysAssociateCountList = [];
	var numAttorneysOtherCounselCountList = [];
	for(var index = 0; index < numAttorneysFirmNameList.length; index++)
	{
		numAttorneysPartnerCountList.push(numAttorneysPartnerCount[index]);
		numAttorneysAssociateCountList.push(numAttorneysAssociateCount[index]);
		numAttorneysOtherCounselCountList.push(numAttorneysOtherCounselCount[index]);
	}
	var splitLineNumAttorneysCompareCount = parseFloat(numAttorneysCompareCount) - parseFloat(0.5);
	
	var labelXPosition = 0;
	var labelYPosition = 0;
	var labelRotation = 0;
	var numberOfAttorneyPointWidth = 30;
	if(numAttorneys_typeOfChart == 'bar')
	{
		labelXPosition = -5;
		labelYPosition = 0;
		labelRotation = 0;
		numberOfAttorneyPointWidth = 20;
	}
	else
	{
		labelXPosition = 5;
		labelYPosition = 10;
		labelRotation = -45;
	}
	
	var totalList = [];
	for(var index = 0; index < numAttorneysFirmNameList.length; index++)
	{
		totalList.push(numAttorneysPartnerCountList[index] + numAttorneysAssociateCountList[index] + numAttorneysOtherCounselCountList[index]);
	}
	
	
	$('#numberOfAttorneysChartContainer').highcharts(
	{
        chart: 
        {
            type: numAttorneys_typeOfChart
        },
        title: 
        {
            text: numAttorneys_titleOfChart
        },
        xAxis: 
        {
            categories: numAttorneysFirmNameList,
            labels: 
            {
                rotation: labelRotation,
				y: labelYPosition,
				x: labelXPosition,
				align: 'right',
				formatter: function()
				{
					var firmName = this.value;
					if(firmName.length > 10)
					{
						firmName = firmName.substring(0, 10) + "...";
					}
					return firmName;
				}
			},
			 plotLines: [{
	                color: '#000000',
	                width: 2,
	                dashStyle: 'Solid',
	                value: parseFloat(splitLineNumAttorneysCompareCount)
	            }]
        },
        yAxis: 
        {
        	gridLineWidth: 1,
			gridLineColor: '#cecece',
			minorGridLineColor: '#cecece',
			lineWidth: 1,
            stackLabels: 
            {
                enabled: false,
                style: 
                {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: 
        {
        	enabled: true
    	},
    	tooltip : {
			useHTML : true,
			positioner : function(boxWidth, boxHeight,
					point) {
					$("#numberOfAttorneysChartContainer .highcharts-tooltip span").css("height", "125px").css("width", "145px");
					var xPosition = point.plotX - 10;
					var yPosition = point.plotY - boxHeight + 40;
					
					if(numAttorneys_typeOfChart == 'column')
					{
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(175))
						{
							xPosition = point.plotX-75;
						}
						if((parseInt(point.plotX) - parseInt(boxWidth)) < parseInt(-200))
						{
							xPosition = point.plotX + 50;
						}
						if((parseInt(point.plotY) - parseInt(boxHeight)) < -75)
						{
							yPosition = point.plotY+50;
						}
					}
					else
					{
						xPosition = point.plotX + 100;
						yPosition = point.plotY - boxHeight + 100;
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(65))
						{
							xPosition = point.plotX-80;
						}
						
						if((parseInt(boxHeight) - parseInt(point.plotY)) > parseInt(100))
						{
							yPosition = point.plotY - boxHeight + 150;
						}
					}
					
				return {
					x : xPosition,
					y : yPosition
				};
			},
			formatter: function()
			{
				var pointerPosition = 0;
				for(var index = 0; index < numAttorneysFirmNameList.length; index++)
				{
					if(this.key == numAttorneysFirmNameList[index])
					{
						pointerPosition = index;
					}
				}
				var total = this.total;
				if(this.total == undefined)
				{
					total = parseInt(numAttorneysPartnerCount[pointerPosition]) + parseInt(numAttorneysAssociateCount[pointerPosition]) + parseInt(numAttorneysOtherCounselCount[pointerPosition]);
				}
				
				var tooltipOption = '';
				tooltipOption += '<table width="100%"><tr><td colspan="2" style="width: 100%"><span style="white-space: normal; font-weight: bold; color: #000;">' + this.key + '</span></td></tr><tr><td></td><td style="color:#000;">No.</td></tr><tr><td style="color: #000; padding-left: 10px; margin-top: 15px;">All Attorneys</td><td style="border: 1px solid; background-color: #000; color: #fff; margin-top: 15px;">' + total + '</td></tr><tr><td style="color: #15375c; padding-left: 10px; margin-top: 15px;">Partners</td><td style="border: 1px solid; background-color: #15375c; color: #fff; margin-top: 15px;">' + numAttorneysPartnerCount[pointerPosition] + '</td></tr><tr><td style="color: #ba323e; padding-left: 10px; margin-top: 15px;">Associates</td><td style="border: 1px solid; background-color: #ba323e; color: #fff; margin-top: 15px;">' + numAttorneysAssociateCount[pointerPosition] + '</td></tr><tr><td style="color: #de7c35; padding-left: 10px; margin-top: 15px;">Other Counsel</td><td style="border: 1px solid; background-color: #de7c35; color: #fff; margin-top: 15px;">' + numAttorneysOtherCounselCount[pointerPosition] + '</td></tr></table>';
				
				if(pointerPosition >= numAttorneysCompareCount)
				{
					var url = "firm-statistics-details?drilldownFirmName=" + encodeURIComponent(this.key) + searchCriteria;
					tooltipOption += '<div><span class="clickToViewDetails"><a href="'+url+'" style="color:#000;">Click to View Details</a></span></div>';
				}


				return tooltipOption;
			}
		},
        plotOptions: 
        {
            series: 
            {
            	stacking: 'normal',
            	dataLabels: 
            	{
                    enabled: false,
                    color: '#FFFFFF'
                },
				pointWidth: 15,
            }
        },
        series: [
        {
            name: 'Partners',
            color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#506a85'],
					[1, '#15375c']
				]
			},
            data: numAttorneysPartnerCountList
        }, 
        {
            name: 'Associates',
            color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#cb676f'],
					[1, '#ba323e']
				]
			},
            data: numAttorneysAssociateCountList
        }, 
        {
            name: 'Other Counsel',
            color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#e59c67'],
					[1, '#de7c35']
				]
			},
            data: numAttorneysOtherCounselCountList
        }],
		navigation: {
            buttonOptions: {
                enabled: false
            }
        }
    });
    
    if(stackingType == '')
	{
		
		var chart = $("#numberOfAttorneysChartContainer").highcharts();
		
		chart.series[chart.series.length-1].remove();
		chart.series[chart.series.length-1].remove();
		chart.series[chart.series.length-1].remove();
		
		chart.addSeries({
			showInLegend: true,
			name: 'Total',
			data: totalList,
			color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#506a85'],
					[1, '#15375c']
				]
			}
		});
		
	}
    
    $(".settingsgry, .highcharts-button").mouseover(function(){
		$("#noAttorneyPopupDiv").hide();
	});
	
});


$(document).ready(function()
{
	
	 $("#firmStatHomeSearch_noOf").click(function()
			    {
			    	Liferay.Portlet.showPopup(
			    		{
			    			uri : '${chartSearchPortletURL}', // defined in common.jsp
			    			title: "Search Criteria"
			    		});
			    });	
   
});


}());


function removeThisChartNumberOfAttorney(){
	
	var removePortletAction = '<%= removePortletURL.toString()%>';
	var r=confirm("Are you sure you want to delete this chart?");
	if (r == true)
  	{
		Liferay.Portlet.showBusyIcon("#bodyId", "Loading...");
		$("#numberOfAttorneysChartModelBean").attr('action', removePortletAction);
		$("#numberOfAttorneysChartModelBean").submit();
  	}
	else
	{
	  return false;
	}
}
</script>





 
<div class="newspublicationPage marbtm4 numberOfAttorneyChartDiv">
		<div class="colMin flLeft leftDynamicDiv">
			<div class="topHeader ForChartsTopHeader">
			<span title="Remove this chart" onclick="removeThisChartNumberOfAttorney();" style="float: right; font-weight: bold; color: rgb(255, 255, 255); cursor: pointer; font-family: verdana; margin: 2px 5px; padding: 3px 8px;">X</span>
			</div>
			<div id="numberOfAttorneysChartDiv" class="hideClass">0 Results, Please try a different search</div>
			<form:form  commandName="numberOfAttorneysChartModelBean" method="post" action="${updateChartURL}" id="numberOfAttorneysChartModelBean">
				<div class="flRight charts" id="No-Data-NoOfAttorney">
					<ul class="reset listView">
					<c:if test="<%=isHomePage%>" >
						<li id="firmStatHomeSearch_noOf" style="overflow:hidden;"><a href="javascript:void(0);" class="filter-icon" >&nbsp;</a></li>
					</c:if>
						<li id="firmStatHomeSearch_noOf_setting"><a href="#numberOfAttorneysViewSetting"
							class="btn icon settingsgry rightViewSetting login-window chartViewSetting"
							onclick="return false;">&nbsp;</a></li>
						<li>
							<a href="javascript:void(0);" id="noOfAttorneyPrintCharts" onclick="return false;" class="printChartClass"></a>
						</li>
						<li>
							<a href="javascript:void(0);" onclick="return false;" class="exportChartClass"></a>
                        <div class="actionSec">
                        <h5>Actions</h5>
                        <ul class="reset">
                            <li class="exportChartImage"><span id="noOfAttorneyExportJPG">Export as JPG</span></li>
                            <li class="exportChartImage"><span id="noOfAttorneyExportPNG">Export as PNG</span></li>
                        </ul>
                        <div class="clear">&nbsp;</div>
                        </div>
						</li>
					</ul>
							<%-- View Settings Start--%>
							<div style="display: none" class="viewBox popusdiv ClickPopup" id="numberOfAttorneysViewSetting"style="width: 540px;">
							
                                <div class="popHeader"> <a href="javascript:void(0);" class="btn icon closewhite closeOne flRight" style="margin-top: 2px">Close</a> SETTINGS: ${numAttorneysChartTitle}
                                  <div class="clear">&nbsp;</div>
                                </div>
                                <div class="section-one" style="width: 175px;">
                                  <h6>Chart Type</h6>
                                    <ul class="reset list4">
                                         <li>
                                          <form:radiobutton path="chartType" class="graphType" value="<%= BaseChartModelBean.ChartType.STACKED_HORIZONTAL_BAR.getValue() %>" />
                                          <span class="btn icon barcharthori">Stacked&nbsp;Horizontal&nbsp;Bar</span>
                                        </li>
                                        <li>
                                        <form:radiobutton path="chartType" class="graphType" value="<%=BaseChartModelBean.ChartType.STACKED_VERTICAL_BAR.getValue()%>" />
                                        <span class="btn icon stackedbarchart">Stacked&nbsp;Vertical&nbsp;Bar</span>
                                       </li>
                                        <li>
                                        <form:radiobutton path="chartType" class="graphType" value="<%=BaseChartModelBean.ChartType.VERTICAL_BAR.getValue()%>" />
                                        <span class="btn icon barchartvert">Vertical&nbsp;Bar</span>
                                       </li>
                                        <li>
                                        <form:radiobutton path="chartType" class="graphType" value="<%= BaseChartModelBean.ChartType.HORIZONTAL_BAR.getValue() %>" />
                                        <span class="btn icon barcharthori">Horizontal Bar</span>
                                       </li>
                                    </ul>
                                    <div class="clear">&nbsp;</div>
                                </div>
                                <div class="section-two"style="width: 180px;">
                                    <h6>Firm Data (Limit of 15)</h6>
                                    <ul class="reset list4">
                                      <li style="width: 125px">
                                        <form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.TOP_15.getValue() %>" />
                                        <span class="">Top 15 by No of Attorneys</span></li>
                                      <li>
                                         <form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.BOTTOM_15.getValue() %>" />
                                        <span class="">Bottom 15 by No of Attorneys</span></li>
                                      <li>
                                         <form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.FIRM.getValue() %>" />
                                        <span class="">Selected Firms:</span>
                                         <form:select path="searchResultsFirmList" multiple="true" size="4" style="width:150px;" class="noOfAttorneySelectedFirm">
                                            <form:options items="${allSearchResultsFirmList}" itemLabel="company" itemValue="companyId"/>
                                         </form:select>
                                        </li>
                                    </ul>
                                    <div class="clear">&nbsp;</div>
                                </div>
                                <div class="section-three" style="width:180px;">
                                  <div class="martp2">
                                    <h6>Comparison Data</h6>
                                    <div class="marbtm2 martp1">
                                    <!-- As per client requirement this options are hide. -->
									<p class="marbtm1" style="display: none;">
                                    <c:choose>
                                        <c:when test="${doIHaveFirm}">
                                            <form:checkbox path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.MY_FIRM.getValue() %>"/>
                                            <span>My Firm</span>
                                        </c:when>
                                        <c:otherwise>
                                            <form:checkbox path="comparisonDataTypeList" disabled="true" value="<%= BaseChartModelBean.ComparisonDataType.MY_FIRM.getValue() %>"/>
                                            <span title="You are not associated with any firm.">My Firm</span>
                                        </c:otherwise>
                                    </c:choose>
                                    </p>
                                    <p class="marbtm1">
                                        <form:checkbox path="comparisonDataTypeList" value="<%=BaseChartModelBean.ComparisonDataType.RIVAL_EDGE.getValue()%>"/>
                                         <form:select path="firmList" multiple="false">
                                            <form:options items="${allRankingList}"/>
                                         </form:select>
                                    </p>
                                    <p class="marbtm1-last">
                                    <c:choose>
                                        <c:when test="${not empty allWatchLists}">
                                            <form:checkbox path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.WATCHLIST_AVG.getValue() %>" />
                                            <span>Watchlist</span>
                                            <form:select path="watchList" multiple="true" size="4">
                                                <form:options items="${allWatchLists}" itemValue="groupId" itemLabel="groupName"/>
                                            </form:select>
                                        </c:when>
                                        <c:otherwise>
                                            <form:checkbox path="comparisonDataTypeList" disabled="true" value="<%= BaseChartModelBean.ComparisonDataType.WATCHLIST_AVG.getValue() %>" />
                                                <span title="You do not have any watchlists set up.">Watchlist</span>
                                        </c:otherwise>
                                    </c:choose>
                                        
                                    </p>
                                    <p class="marbtm1-last">
                                       <form:checkbox path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.AVERAGE.getValue() %>" />
                                        Average of Firms in Search </p>
                                </div>
                                  </div>
                                  <div class="clear">&nbsp;</div>
                                </div>
                                <div class="clear">&nbsp;</div>
                                <hr>
                                <div class="btmdiv" style="margin:0; border:none;">
                                <input type="button" value="Reset All" class="buttonTwo flLeft settingReset"   id="numberOfAttorneysResetAll"/>
                                <input type="button" class="buttonTwo flRight rightReset" value="Cancel" id="numberOfAttorneysCancel">
                                <input type="buttton" value="Apply" class="buttonOne flRight" style="margin: 0 5px 0 0;" id="numberOfAttorneysApply" onclick="applyChartSettings('#numberOfAttorneysChartModelBean');"/>
                                <div class="clear">&nbsp;</div>
                                </div>
                            
                          </div>
<%--View Settings End --%>
                </div>
				</form:form>
			<div id="numberOfAttorneysChartContainer" class="charts-spacing" style="position: relative;">
		</div>
	
	</div>
</div>