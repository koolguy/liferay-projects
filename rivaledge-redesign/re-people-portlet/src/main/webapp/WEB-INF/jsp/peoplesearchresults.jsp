<%@page import="com.alm.rivaledge.controller.firmstatistics.results.FirmStatisticsResultsController"%>
<%@page import="com.alm.rivaledge.transferobject.PeopleResult"%>
<%@page import="com.alm.rivaledge.transferobject.PeopleResultDTO"%>
<%@page import="com.alm.rivaledge.util.ALMConstants"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="javax.portlet.PortletURL"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<portlet:defineObjects/>

<portlet:resourceURL id="drillDown" var="drillDownURL"/> 

<%@ include file="calculation.jsp"%>

<%						
	PortletURL printPublicationsURL = renderResponse.createRenderURL();
	printPublicationsURL.setWindowState(LiferayWindowState.POP_UP);
	printPublicationsURL.setParameter("displayPrintPage", "true");
%>
<script>

// export 

function exportFile(fileType)
	{
		$('#fileType').val(fileType);
		
		if($("#printDrillDownFirmId").length != 0) // Element with that id exist so User is viewing drill down results and wants to export
		{
			$("#drillDownFirmId").val($("#printDrillDownFirmId").val());
			$("#drillDownType").val($("#printDrillDownType").val());
			
		}
		
		var r=confirm("Click OK to export or Cancel");
		
		if (r == true)
	  	{
	  		$('#exportForm').submit();
	  	}
		else
		{
		  return false;
		}
	}
	
function printPopup()
{
	
	if($("#printDrillDownFirmId").length == 0) // Element with that id doesn't exist so User is viewing normal results
	{
		var popupWindow = window.open(
				'<%= printPublicationsURL.toString() %>',
				'popUpWindow','height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes');
	
	}
	else //User is viewing the drilldown results, now make an ajax request to print the results
	{
		ajaxDrillDown("", $("#printDrillDownFirmId").val(), $("#printDrillDownType").val(), true);
	}
	
}

/**
 * This generic method is used for displaying and printing of drilldown results for ofices and locations.
 */
function ajaxDrillDown(drillText, firmId, drillDownType, displayPrintPage)
{
	
	if(displayPrintPage) //print 
	{
		var popupWindow = window.open('${drillDownURL}&firmId='+firmId+'&drillDownType='+drillDownType+'&displayPrintPage='+displayPrintPage+'',
				'popUpWindow','height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes');
		
		return;
	}
	
	else
	{
		
		Liferay.Portlet.showBusyIcon('#bodyId', 'Loading...');
	//Ajax call to fetch and display drill down data for selected Firm
	 $.ajax({
		    url:"${drillDownURL}",
		    method: "GET",
		    data: 
	    	{
		    	"firmId":firmId,
		    	"drillDownType" : drillDownType,
		    	"displayPrintPage" : displayPrintPage
	    	},
		    success: function(data)
		        {

				    	Liferay.Portlet.hideBusyIcon('#bodyId');
		    			$("#drilldata").html(data);		
				    	//Add dynamic Link of actual result table so that user can navigate back to reults screen from Office/Location drill down table
				    	$("#tableHeading").html("<a href='javascript:void(0)' onclick='enableMasterTable()' style='text-decoration:underline;'>DETAILED RESULTS</a>&nbsp;>"+drillText);
		        },
		        error: function(jqXHR, textStatus, errorThrown)
		        {
		        	Liferay.Portlet.hideBusyIcon('#bodyId');
		        	//alert("error:" + textStatus + " - exception:" + errorThrown);
		        }
		    }); 

	}
	
}

$(document).ready(function()
{
	$("[id^='officeExpand_']").click(function() 
	{	
	
		var firmName = $('td:first', $(this).parents('tr')).text();	
		var firmId=$(this).val();	
		ajaxDrillDown(firmName + "(Locations)", firmId, "<%=FirmStatisticsResultsController.DRILLDOWN_OFFICE%>", false);
	
	});

	$("[id^='practiceExpand_']").click(function() {	
		
		var firmName = $('td:first', $(this).parents('tr')).text();	
		var firmId=$(this).val();	
		ajaxDrillDown(firmName + "(Practices)", firmId, "<%=FirmStatisticsResultsController.DRILLDOWN_PRACTICE%>", false);
		
	});

});

	function enableMasterTable()
	{
		
		$("#mainResults, #groupedData").show(); 
		$("#pagination").show(); 
		$("#drilldata").html("");
		$("#tableHeading").html("DETAILED RESULTS");
	
		$("#mainResultsSpan").show();
		$("#drillDownResultsSpan").hide();
	}

</script>

 <div id="content"> 
      <div id="tab2">
        <h4 class="heading" id="tableHeading" style="text-transform:capitalize;">DETAILED RESULTS</h4>
        <div class="marbtm2 martp3">
        <%-- Display selected search Criteria string --%>
       <%@ include file="searchCriteriaDisplay.jspf"%>
       
			<div class="flRight">
				<ul class="menuRight flRight">
					<li><a class="btn icon alert" href="#">Create Alert</a></li>
					<li><a class="btn icon print" href="#" onClick="printPopup();">Print</a></li>
					<li><a id="exports" class="btn icon export" href="#">Export</a>
						<div id="actionBox" class="actionSec" style="">
							<h5>Actions</h5>
							<ul class="reset">
								<li><span>E</span><a href="#"
									onclick="exportFile('xls')">Export to Excel</a></li>
								<li><span>E</span><a href="#"
									onclick="exportFile('csv')">Export to CSV</a></li>
							</ul>
							<div class="clear">&nbsp;</div>
						</div></li>
					<li>
	        		<div class="viewSetting flRight rel" style="margin-top:0px">
					  		<a class="btn icon settings" href="javascript:void(0)" onclick="toggleViewSettingPeople(this);">
					  			<span>
					  				<span class="btn icon dropdownarrow" id="viewSettings">View Settings</span>
					  			</span>
					  		</a>
	      			 </div>
        		</li>
				</ul>
				<div class="clear">&nbsp;</div>
			</div>
		</div>
		<c:if test="${not isAdminUser}">
	    	<c:if test="${displayMessage}">
	  			<div style="color:#b63334;">
	  				Search Results are too large to display in their entirety. Only the first 5,000 results are displayed below. To view an entire search result, please narrow your search criteria above and rerun your search.
	  			</div>
	  		</c:if>
	    </c:if>
       <%@ include file="resultsTable.jspf" %>
       
       </div>
         <%--Pagination Starts --%>
		 <div class="PaginationScroll" id="pagination">
		      	  	
		      	  <ul>
					<%
					PeopleSearchModelBean psmb = 	(PeopleSearchModelBean) request.getAttribute("peopleSearchModelBean");
					int pageNumber 	= psmb.getGoToPage();
					int lastPage	= psmb.getLastPage();
						
						if (lastPage != 1)
						{
						%>
								Page:&nbsp;&nbsp;&nbsp;
						<%
								if (pageNumber > 5)
								{
						%>
									<li class="active"><a href="#" onclick="setPage(<%= 1 %>)"><%= 1 %></a>&nbsp;&nbsp;&nbsp;...&nbsp;&nbsp;&nbsp;</li>
						<%
								}
								for (int i = Math.max(1, pageNumber - 5); i <= Math.min(pageNumber + 5, lastPage); i++)
								{
									if (pageNumber == i)
									{
						%>
									<li><strong><%= i %>&nbsp;&nbsp;&nbsp;</strong> </li>
						<%
									}
									else
									{
						%>
										<li><a href="#" onclick="setPage(<%= i %>)"><%= i %></a>&nbsp;&nbsp;&nbsp;</li>
						<%				
									}
								}
								
								if ((lastPage - pageNumber) > 5)
								{
						%>
									...&nbsp;&nbsp;&nbsp;<li><a href="#" onclick="setPage(<%= lastPage %>)"><%= lastPage %></a></li>
						<%			
								}
						}
						else
						{
						%>
								Page:&nbsp;&nbsp;&nbsp;1
						<%				
						}
						%>
		      	  	
		     	</ul>	
		   		 
   			</div>
		<%--Pagination Ends --%>
   <%-- Placeholder for displaying results for drill down from offices and practices  --%>
        <div id="drilldata">
    
    	</div>
      

    </div>
    
    <div class="clear">&nbsp;</div>
    
    <form name="exportForm" method="post" id="exportForm" action="<portlet:resourceURL id="exportFile"/>">
   <input type="hidden" name="fileType" value="" id="fileType"/> 
    <%--These two hidden values are used for exporting the drill down results --%>
		<input type="hidden" name="drillDownFirmId" value="-1" id="drillDownFirmId"/>  <%-- use -1 to avoid NFE while casting to Integer--%>
		<input type="hidden" name="drillDownType" value="" id="drillDownType"/>
  </form>
  

<script src="<%=renderRequest.getContextPath()%>/js/s_code.js"></script>
 <script>

	// Omniture SiteCatalyst - START
	s.prop22 = "premium";
	s.pageName="rer:people-analysis-results";
	s.channel="rer:people-analysis";
	s.server="rer";
	s.prop1="people-analysis"
	s.prop2="people-analysis";
	s.prop3="people-analysis";
	s.prop4="people-analysis";
	s.prop21=s.eVar21='current.user@foo.bar';	
	s.events="event2";
	s.events="event27";
	
	s.t();
	// Omniture SiteCatalyst - END
</script> 
