<%@page import="com.alm.rivaledge.model.PeopleSearchModelBean"%>
<%@page import="com.alm.rivaledge.model.chart.BaseChartModelBean"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.alm.rivaledge.util.ALMConstants"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>

<%@ taglib prefix="liferay-portlet" uri="http://liferay.com/tld/portlet"%>
<%@ taglib prefix="portlet" 		uri="http://java.sun.com/portlet_2_0"%> 
<%@ taglib prefix="c" 			 	uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="liferay-ui"   	uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="liferay-util" 	uri="http://liferay.com/tld/util"%>
<%@ taglib prefix="spring"          uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form"            uri="http://www.springframework.org/tags/form"%>

<portlet:defineObjects/>

<%@ include file="./common.jsp" %>

<portlet:actionURL var="submitURL">
	<portlet:param name="action" value="changeSearchCriteria"/>
</portlet:actionURL>

<portlet:actionURL var="clickToViewURL">
	<portlet:param name="action" value="clickToView"/>
</portlet:actionURL>

<portlet:resourceURL var="persistSearchCriteriaForChartURL" id="persistSearchCriteriaForChart">
</portlet:resourceURL>

<%--
<c:if test="${isHomePage}">

	<script src="/re-theme/js/jquery-1.9.1.js"></script>
	<script src="/re-theme/js/jquery-ui.js"></script> 
	<script src="/re-theme/js/popup.js?v=${buildVersionId}"></script>
	<script src="/re-theme/js/s_code.js"></script> 
	<script src="/re-theme/js/webwidget_tab.js"></script> 

</c:if>
 --%>
<style>

#<portlet:namespace/>viewSettingsPopup {color:#888888; margin-top:10px; position:relative; z-index:999}
#<portlet:namespace/>viewSettingsPopup h6 {color:#222}
#<portlet:namespace/>viewSettingsPopup a {text-decoration:none; padding-bottom:15px}
#<portlet:namespace/>viewSettingsPopup span {padding:2px 0px 3px 0}
#<portlet:namespace/>viewSettingsPopup span span.btn.dropdownarrow.icon:before {float:right; padding-right:5px; background-position:-98px -75px; color:#888}
#<portlet:namespace/>viewSettingsPopup .viewBox {position:absolute; right:15px; top:20px; width:180px; background:#f0f0f0; border:1px solid #a4a1a1; display:none; padding:10px; -moz-box-shadow:0px 3px 3px #888; -webkit-box-shadow:0px 3px 3px #888; box-shadow:0px 3px 3px #888;}
#<portlet:namespace/>viewSettingsPopup .viewBox .btmdiv {border-top:1px solid #c0c0c0; padding-top:5px; margin-top:5px}
#<portlet:namespace/>viewSettingsPopup {position:absolute; right:5px; top:18px; width:680px; background:#fff; border:1px solid #a4a1a1; display:none; padding:10px; -moz-box-shadow:0px 3px 3px #888; -webkit-box-shadow:0px 3px 3px #888; box-shadow:0px 3px 3px #888; color:#222}
#<portlet:namespace/>viewSettingsPopup .btmdiv {border-top:1px solid #c0c0c0; padding-top:5px; margin-top:5px}

</style>

<%@ include file="hideOrDisplayColumnsJS.jspf" %>

<%-- People Search specific function definitions--%>
<script>

/* View Settings UI logic*/

/**
 * 
 */
function <portlet:namespace/>applyViewSettings() 
{
	$("#<portlet:namespace/>viewSettingsPopup").hide();
	$("#<portlet:namespace/>goToPage").val(1);
 	<portlet:namespace/>search();
}

/**
	* Runs only the jQuery Logic after the page loads
	* Hides or displays the columns based on the user view settings
	*/
	
function toggleViewSettingPeople(vsId)
{
	//get the position of the placeholder element
   	var vsPos   = $(vsId).offset();
   	var vsHeight = $(vsId).height();
   	var vsWidth = $(vsId).width();
    var popupWidth = $("#<portlet:namespace/>viewSettingsPopup").width();

    //show the menu directly over the placeholder
    $("#<portlet:namespace/>viewSettingsPopup").css({  position: "absolute", "left": (vsPos.left - popupWidth + vsWidth) + "px", "top":(vsPos.top + vsHeight)  + "px" });
    
	$("#<portlet:namespace/>viewSettingsPopup").toggle();
}

function <portlet:namespace/>cancelViewSettings() 
{
	$("#<portlet:namespace/>viewSettingsPopup").hide();
}
//reset view setting popup to default state
function <portlet:namespace/>resetViewSettings() 
{

	//uncheck all  check box  and radio button inside view setting div
	$('#<portlet:namespace/>viewSettingsPopup').find('input[type=checkbox]').removeAttr('checked');	
	$('#<portlet:namespace/>viewSettingsPopup').find('input[type=radio]:checked').removeAttr('checked');
	
	//reset 'Column Display Attorneys' section of view setting popup to default state
	$('#<portlet:namespace/>allGroupsAttorneys').prop("checked", true);
	$('#<portlet:namespace/>allOtherAttorney').find('input[type=checkbox]').prop("checked", true); 
	
	
	//reset 'Moves' section of view setting popup to default state
	$('#<portlet:namespace/>peopleMove').prop("checked", true);
	$('#<portlet:namespace/>added').prop("checked", true); 
	$('#<portlet:namespace/>removed').prop("checked", true);
	
	//reset 'Group By' section of view setting popup to default state
	$('#<portlet:namespace/>titleGroup').prop("checked", true);  
	
	
	//reset 'Display No. of Results' section of view setting popup to default state
	$('#<portlet:namespace/>hundredRecords').prop("checked", true);
	
	
	//reset 'Comparison Data' section of view setting popup to default state
	$('#<portlet:namespace/>myfirm').prop("checked", true);
	$('#<portlet:namespace/>rivalEdge').prop("checked", true);
	$('#<portlet:namespace/>watchlist').prop("checked", true);
	$('#<portlet:namespace/>average').prop("checked", true);			
	$('#<portlet:namespace/>rivaledgeSelect').val('<%=ALMConstants.AMLAW_100%>');

}


function <portlet:namespace/>changeOnMoves()
{
	var lengthofCheckBoxes = $("input[name='moves']:checked").length;
	if(lengthofCheckBoxes>0){		
	$('#<portlet:namespace/>peopleMove').prop("checked", true);
	} else{		
	$('#<portlet:namespace/>peopleMove').prop("checked", false);
	}
}

function <portlet:namespace/>changeOnDisplayColumns()
{
	var lengthofCheckBoxes = $("input[name='displayColumns']:checked").length;
	if(lengthofCheckBoxes>0){		
	$('#<portlet:namespace/>allGroupsAttorneys').prop("checked", true);
	} else{		
	$('#<portlet:namespace/>allGroupsAttorneys').prop("checked", false);
	}
}

function <portlet:namespace/>initializeSearchCriteria()
{
	hideOrDisplayTableColumns();// top priority to avoid flickering of table columns hiding/display, if any
	<portlet:namespace/>changeOnDisplayColumns();
	<portlet:namespace/>changeOnMoves();
	<portlet:namespace/>applyFirms();
	<portlet:namespace/>initializeDate();
	<portlet:namespace/>applyLocations();
	<portlet:namespace/>applyPracticeArea();
	<portlet:namespace/>applyFirmSize();
	<portlet:namespace/>refreshCounters();
	
}

/**
* Initializes the user selected Date from previous search and this is retained through out the session
*/
function <portlet:namespace/>initializeDate()
{
	var dateTextStr = '${peopleSearchModelBean.dateText}';
	
	if(dateTextStr.match('^Any') || dateTextStr.match('^Last'))
	{
		$("#<portlet:namespace/>datePeriodSelect option[value='"+dateTextStr+"']").attr('selected','selected'); 
		$('#<portlet:namespace/>period').prop("checked", true);
	}
	else{ // we have date range parse it and set the Date on datePicker
		$( "#<portlet:namespace/>from" ).datepicker( "setDate", dateTextStr.split("-")[0]);
		$( "#<portlet:namespace/>to" ).datepicker( "setDate", dateTextStr.split("-")[1]);
	}
	
	$("#<portlet:namespace/>dateText").val(dateTextStr);

}
	
function <portlet:namespace/>applyFirms()
{
	/* var firmValue;	
	if($("#allfirms").is(":checked"))
	{
		firmValue=$("#allfirms").val();
		$("#Firmstext").val(firmValue);
		$("#selectedFirms").val(firmValue);		
	} 
	else if($("#RivalEdgeList").is(":checked"))
	{
		var selectedFirms;
		firmValuetemp=$("#Select1").val();
		firmValue=$("#Select1 option:selected").map(function(){ return this.text }).get().join(";");
		selectedFirms=$('select#Select1').val();
		$("#Firmstext").val(firmValue);
		$("#selectedFirms").val(selectedFirms);		
	} 
	else if($("#allOtherFirm").is(":checked"))
	{ 
		var allVals = [];
   		var alltexts = [];
   		var allFirmsCounter = 0;
    		
	    $('#allOtherFirmDiv input[type=checkbox]:checked').each(function() 
	    {
	    	allVals.push($(this).val());
	       	alltexts.push($(this).attr('labelAttr'));
	       	allFirmsCounter++;
	    });
	    $("#individualFirmsCounter").html('<input type="checkbox" value="Abrams" id="firmCounter"> Firms ('+allFirmsCounter+') Selected');
	    $('#Select1 option:selected').removeAttr("selected"); 		
		$("#selectedFirms").val(allVals);
		$("#Firmstext").val(alltexts);
	} */
	

	var allSelectedValue = [];
	var allSelectedIds = [];
	
	 $("#<portlet:namespace/>Firmstext").val('');
	 $("#<portlet:namespace/>selectedFirms").val('');
	 
	var isFirmChecked = false;
	var allWatchListCounter = 0;
	
		
	//$('.individualfirmsWatchList input[type=checkbox]:checked').each(function() {
		$('#<portlet:namespace/>individualfirmsWatchListDIVPeopleFirmStats input[type=checkbox]:checked').each(function() {
		
		
		 allSelectedValue.push($(this).attr('labelAttr'));
		 allSelectedIds.push($(this).val());
		 $("#<portlet:namespace/>firm_watchlist").prop("checked", true);
		 $('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
		 
		 var watchListId = $('#defaultWatchListId').val();
			
			$('#<portlet:namespace/>individualfirmsWatchListDIVPeopleFirmStats input[type=checkbox]').each(function() {
				 if(this.value == watchListId)
					{
						$(this).prop("checked", true);
						$("#<portlet:namespace/>Firmstext").val($(this).attr('labelAttr'));
						$("#<portlet:namespace/>selectedFirms").val($(this).val());
						$("#<portlet:namespace/>firm_watchlist").prop("checked", true);
						$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
					}
			});
			
			
						allWatchListCounter++;						
						$("#homePageShowPeople .individualfirms-Watchlist-peopleFirmStats").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (' + allWatchListCounter +' Selected)');
						
						isFirmChecked = true;
						
						 $("#homePageShowPeople .rivaledgeListAMLAW_100_PeopleFirmStats").prop("checked", false);
						 $("#homePageShowPeople .rivaledgeListAMLAW_200_PeopleFirmStats").prop("checked", false);
						 $("#homePageShowPeople .rivaledgeListNLJ_250_PeopleFirmStats").prop("checked", false);

 			});
	
	$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
		
		 allSelectedValue.push($(this).attr('labelAttr'));
		 allSelectedIds.push($(this).val());
		 isFirmChecked = true;
		 $("#homePageShowPeople .rivaledgeListAMLAW_100_PeopleFirmStats").prop("checked", false);
		 $("#homePageShowPeople .rivaledgeListAMLAW_200_PeopleFirmStats").prop("checked", false);
		 $("#homePageShowPeople .rivaledgeListNLJ_250_PeopleFirmStats").prop("checked", false);

		
 	});
	
	if(allSelectedValue != ''  &&  allSelectedIds != '') 
		{
		
			$("#<portlet:namespace/>Firmstext").val(allSelectedValue.join(",")); //Use a comma separator for values which show up on UI
			$("#<portlet:namespace/>selectedFirms").val(allSelectedIds.join(";"));
			 isFirmChecked = true;
			
		}
	else if($("#homePageShowPeople .rivaledgeListAMLAW_100_PeopleFirmStats").is(":checked"))
	{
		
		$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.AMLAW_100 %>');
		$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.AMLAW_100 %>');
		$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
		$('#<portlet:namespace/>individualfirmsWatchListDIVPeopleFirmStats input[type=checkbox]:checked').removeAttr('checked');
		isFirmChecked = true;
		
	} 
	else if($("#homePageShowPeople .rivaledgeListAMLAW_200_PeopleFirmStats").is(":checked"))
	{
		
		$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.AMLAW_200 %>');
		$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.AMLAW_200 %>');
		$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
		$('#<portlet:namespace/>individualfirmsWatchListDIVPeopleFirmStats input[type=checkbox]:checked').removeAttr('checked');
		isFirmChecked = true;
		
	} 
	else if($("#homePageShowPeople .rivaledgeListNLJ_250_PeopleFirmStats").is(":checked"))
	{
		
		$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.NLJ_250 %>');
		$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.NLJ_250 %>');
		$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
		$('#<portlet:namespace/>individualfirmsWatchListDIVPeopleFirmStats input[type=checkbox]:checked').removeAttr('checked');
		isFirmChecked = true;
		
	} 
	else
	{
		$("#<portlet:namespace/>allfirms, #<portlet:namespace/>firm_watchlist").each(function(){
			
			 //$(".rivaledgeListAMLAW_100").prop("checked", false);
			 //$(".rivaledgeListAMLAW_200").prop("checked", false);
			 //$(".rivaledgeListNLJ_250").prop("checked", false);
			 //$('#individualfirms input[type=checkbox]:checked').removeAttr('checked'); 
			 //$("#firm_watchlist").prop("checked", false);
			
			
			if($("#<portlet:namespace/>allfirms").is(":checked"))
				{
				
					$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.ALL_FIRMS %>');
					$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.ALL_FIRMS %>');
					$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.ALL_FIRMS %>");
					isFirmChecked = true;
					
				} 
			 else
				{
				 
				 var watchListId = $('#defaultWatchListId').val();
					
					$('#<portlet:namespace/>individualfirmsWatchListDIVPeopleFirmStats input[type=checkbox]').each(function() {
						 if(this.value == watchListId)
							{
								$(this).prop("checked", true);
								$("#<portlet:namespace/>Firmstext").val($(this).attr('labelAttr'));
								$("#<portlet:namespace/>selectedFirms").val($(this).val());

								$("#<portlet:namespace/>firm_watchlist").prop("checked", true);
								$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
								isFirmChecked = true;
							}
					});
					
					<%-- if($(this).is(":checked")){
						var value =$(this).val();
					  	$("#Firmstext").val('AMLAW 100');
						$("#selectedFirms").val('AMLAW 100');
						$('#firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
						isFirmChecked = true;
						
					}  --%>
					
				}
		});
		
	}
	
	if(isFirmChecked == false)
		{
			$("#<portlet:namespace/>Firmstext").val('AmLaw 100');
			$("#<portlet:namespace/>selectedFirms").val('AmLaw 100');
			$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
			 $("#<portlet:namespace/>firm_watchlist").prop("checked", true);
			 $("#homePageShowPeople .rivaledgeListAMLAW_100_PeopleFirmStats").prop("checked", true);
			 
		}
}

function <portlet:namespace/>applyLocations()
{
	if($("#<portlet:namespace/>allLocations").is(":checked"))
	{
		$("#<portlet:namespace/>selectedLocation").val($("#<portlet:namespace/>allLocations").val());
	} 
	else
	{	 
		var allValsLocation = [];
		var allLocationCounter = 0;
		$('#<portlet:namespace/>allOtherLocationsDiv input[type=checkbox]:checked').each(function() 
		{ 
			allValsLocation.push($(this).val());
			allLocationCounter++;
		});
		$("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="Locations" id="<portlet:namespace/>locationSelection"> Locations ('+allLocationCounter+') Selected');

		if(allValsLocation.length > 0)
		{
			$("#<portlet:namespace/>allOtherLocations").prop("checked", true);
		    $("#<portlet:namespace/>selectedLocation").val(allValsLocation.join(";"));		
		}
	}
}


function <portlet:namespace/>applyPracticeArea()
{
	if($("#<portlet:namespace/>allPracticeAreaFirmStateId").is(":checked"))
	{
		$("#<portlet:namespace/>selectedPracticeArea").val($("#<portlet:namespace/>allPracticeAreaFirmStateId").val());			
	} 
	else
	{
		var allValsPracticeArea = [];
		var allPracticeCounter = 0;
		
		$('#<portlet:namespace/>practiceAreaDiv input[type=checkbox]:checked').each(function() 
		{ 
		 	allValsPracticeArea.push($(this).val());
		 	allPracticeCounter++;
		});
			 
		$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox"  value="Practice" id="<portlet:namespace/>practiceSelection"> PracticeAreas ('+allPracticeCounter+') Selected');
		
		if(allValsPracticeArea.length > 0)
		{
			$("#<portlet:namespace/>allPracticeAreaIndividual").prop("checked", true);
			$("#<portlet:namespace/>selectedPracticeArea").val(allValsPracticeArea.join(";"));		
		}
	}
}

function <portlet:namespace/>applyFirmSize()
{
	if($("#<portlet:namespace/>allFirmSize").is(":checked"))
	{
		$("#<portlet:namespace/>selectedFirmSize").val($("#<portlet:namespace/>allFirmSize").val());			
	} 
	else
	{
		var allValsFirmSize = [];
		var allFirmSizeCounter = 0;
			
		$('#<portlet:namespace/>allOtherFirmSizeDiv input[type=checkbox]:checked').each(function() 
		{ 
			allValsFirmSize.push($(this).val());
			allFirmSizeCounter++;
	    });
		 
		$("#<portlet:namespace/>firmSizeCounter").html('<input type="checkbox" value="firmSize" id="<portlet:namespace/>firmSizeSelection"> Firms Size ('+allFirmSizeCounter+') Selected');
		if(allValsFirmSize.length > 0)
		{ 
			$("#<portlet:namespace/>allOtherFirmSize").prop("checked", true);
	   		$("#<portlet:namespace/>selectedFirmSize").val(allValsFirmSize.join(";"));	
		}
	}
}

function <portlet:namespace/>applyDate()
{
	var checkDate=false;
	var dateValue;
	  
	if($("#<portlet:namespace/>dateRange").is(":checked"))
	{
		var fromDate = new Date($("#<portlet:namespace/>from").val());
	   	var toDate = new Date($("#<portlet:namespace/>to").val()); 
	   	if(!ValidateDate($("#<portlet:namespace/>from").val()))
	   	{
	    	checkDate=true;
	        $('#<portlet:namespace/>fromdateError').css('display','block');      
	   	}
	   	else
	   {
	    	checkDate=false;
	    	$('#<portlet:namespace/>fromdateError').css('display','none');
	   }  
	   if(!ValidateDate($("#<portlet:namespace/>to").val()))
	   {
	    	checkDate=true;
	    	$('#<portlet:namespace/>todateError').css('display','block');
	   }  
	   else
	   {
		if(!checkDate)
	    {
	     checkDate=false;
	     $('#<portlet:namespace/>todateError').css('display','none'); 
	    }
	   
	   }
	   if(!(toDate >= fromDate) && !checkDate)
	   {
	    $('#<portlet:namespace/>dateValidError').css('display','block');
	    checkDate=true;
	   } 
	   else
	   {
	    $('#<portlet:namespace/>dateValidError').css('display','none');
	   }
	  
	   if(checkDate)
	   {
	   return checkDate; 
	   }
	  
  }
  
  if($("#<portlet:namespace/>period").is(":checked"))
  {
   
   dateValue=$("#<portlet:namespace/>datePeriodSelect").val();
   
  } 
  else if($("#<portlet:namespace/>dateRange").is(":checked"))
  {
   dateValue=$("#<portlet:namespace/>from").val() + "-"+ $("#<portlet:namespace/>to").val();
  } 
  
  $("#<portlet:namespace/>dateText").val(dateValue);
}


function <portlet:namespace/>refreshCounters()
{
	
	var allFirmsCounter = 0;
	$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
	 allFirmsCounter++;
	});
	$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="Abrams" id="<portlet:namespace/>firmCounter"> Firms ('+allFirmsCounter+') Selected');
	
	var allLocationCounter = 0;
	 $('#<portlet:namespace/>allOtherLocationsDiv input[class=allLocationsCheckBox]:checked').each(function() {
		 allLocationCounter++;
	    });
	$("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="Locations" id="<portlet:namespace/>locationSelection"> Locations ('+allLocationCounter+') Selected');

	var allPracticeCounter = 0;
	$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:checked').each(function() {
		 allPracticeCounter++;
	    });
	$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox"  value="Practice" id="<portlet:namespace/>practiceSelection"> PracticeAreas ('+allPracticeCounter+') Selected');
		
	var allFirmSizeCounter = 0;
	$('#<portlet:namespace/>allOtherFirmSizeDiv input[type=checkbox]:checked').each(function() {
		allFirmSizeCounter++;
	    });
	$("#<portlet:namespace/>firmSizeCounter").html('<input type="checkbox" value="firmSize" id="<portlet:namespace/>firmSizeSelection"> Firms Size ('+allFirmSizeCounter+') Selected');
}

function <portlet:namespace/>clearDate()
{	
	$('#<portlet:namespace/>popupDate').find("option").attr("selected", false);
	$('#<portlet:namespace/>popupDate').find('input[type=text]').val('');
	$('#<portlet:namespace/>popupDate').find('input[type=radio]:checked').removeAttr('checked');				
	$("#<portlet:namespace/>dateText").val('<%=ALMConstants.LAST_WEEK%>');	
	$("#<portlet:namespace/>period").prop("checked", true); 
	$("#<portlet:namespace/>datePeriodSelect").val('<%=ALMConstants.LAST_WEEK%>');	
	$("#<portlet:namespace/>from").datepicker('enable');
	$("#<portlet:namespace/>to").datepicker('enable');
	<portlet:namespace/>applyDate();
}

function <portlet:namespace/>clearFirms()
{
	/* $('#popup').find("option").attr("selected", false);
	$('#popup').find('input[type=checkbox]:checked').removeAttr('checked');
	$('#popup').find('input[type=radio]:checked').removeAttr('checked');
	
	$("#individualFirmsCounter").html('<input type="checkbox" value="Abrams" id="firmCounter"> Firms (0) Selected');

	$("#RivalEdgeList").prop("checked", true); */
	<%-- $('#Select1').val('<%=ALMConstants.AMLAW_100%>'); --%>
	
	$('#<portlet:namespace/>popup').find("option").attr("selected", false);
	$('#<portlet:namespace/>popup').find('input[type=checkbox]:checked').removeAttr('checked');
	$('#<portlet:namespace/>popup').find('input[type=radio]:checked').removeAttr('checked');
	$("#<portlet:namespace/>allfirms").prop("checked", false);
	$("#homePageShowPeople .rivaledgeListAMLAW_100_PeopleFirmStats").prop("checked", false);
	$("#homePageShowPeople .rivaledgeListAMLAW_200_PeopleFirmStats").prop("checked", false);
	$("#homePageShowPeople .rivaledgeListNLJ_250_PeopleFirmStats").prop("checked", false);
	$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" id="<portlet:namespace/>firmCounter"> Firms (0) Selected');

	$("#<portlet:namespace/>firm_watchlist").prop("checked", true);
	
	 var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
		
		$('#<portlet:namespace/>individualfirmsWatchListDIVPeopleFirmStats input[type=checkbox]').each(function() {
			 if(this.value == watchListId)
				{
					$(this).prop("checked", true);
				}
		}); 
	
		<portlet:namespace/>applyFirms();
}

function <portlet:namespace/>clearLocations()
{
	$('#<portlet:namespace/>popupLocation').find('input[type=checkbox]:checked').removeAttr('checked');
	$('#<portlet:namespace/>popupLocation').find('input[type=radio]:checked').removeAttr('checked');
	$("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="Locations" id="<portlet:namespace/>locationSelection"> Locations (0) Selected');
	$('#<portlet:namespace/>allLocations').prop("checked", true);
	<portlet:namespace/>applyLocations();
}


function <portlet:namespace/>clearPracticeAreas()
{
	$('#<portlet:namespace/>popupPracticeArea').find('input[type=checkbox]:checked').removeAttr('checked');
	$('#<portlet:namespace/>popupPracticeArea').find('input[type=radio]:checked').removeAttr('checked');
	$('#<portlet:namespace/>allOtherPracticeArea input').find('input[type=checkbox]:checked').removeAttr('checked');
	$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox"  value="Practice" id="<portlet:namespace/>practiceSelection"> Practice Area (0) Selected');
	$('#<portlet:namespace/>allPracticeAreaFirmStateId').prop('checked', true);	
	<portlet:namespace/>applyPracticeArea();
}

function <portlet:namespace/>clearFirmSize()
{
	$('#<portlet:namespace/>popupFirmSize').find('input[type=checkbox]:checked').removeAttr('checked');
	$('#<portlet:namespace/>popupFirmSize').find('input[type=radio]:checked').removeAttr('checked');
	$("#<portlet:namespace/>firmSizeCounter").html('<input type="checkbox" value="Locations" id="<portlet:namespace/>locationSelection"> Locations (0) Selected');
	
	$('#<portlet:namespace/>allFirmSize').prop("checked", true);
	<portlet:namespace/>applyFirmSize();
}

function clearAll()
{
	<portlet:namespace/>clearFirms();
	<portlet:namespace/>clearLocations();
	<portlet:namespace/>clearPracticeAreas();
	<portlet:namespace/>clearFirmSize();
	<portlet:namespace/>clearDate();
}

function <portlet:namespace/>changeOnIndividualLocations()
{
	var allLocationsCounter = 0;
	 $('#<portlet:namespace/>allOtherLocationsDiv :checked').each(function() {
		 allLocationsCounter++;
	    });
			$("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="Locations" id="<portlet:namespace/>locationSelection"> Locations ('+allLocationsCounter+') Selected');
	 
		if(allLocationsCounter == 0)
		{
			$('#<portlet:namespace/>allOtherLocations').prop("checked", false);
			$('#<portlet:namespace/>allLocations').prop("checked", true);
			//$('#locationSelection').prop("checked", false);
		}
		else
		{
			$('#<portlet:namespace/>allOtherLocations').prop("checked", true);
			$('#<portlet:namespace/>allLocations').prop("checked", false);
			//$('#locationSelection').prop("checked", false);
		}
		<portlet:namespace/>applyLocations(); // apply them as the change

}

function <portlet:namespace/>allOtherFirmChange()
{
	var allFirmsCounter = 0;
	$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() 
	{
		allFirmsCounter++;
	});
	
	//$('#<portlet:namespace/>Select1 option:selected').removeAttr("selected");
	$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="Abrams"> Firms ('+allFirmsCounter+') Selected');
	
	if(allFirmsCounter > 0)
	{
		 $('#<portlet:namespace/>allOtherFirm').prop("checked", true);	
		 $("#homePageShowPeople .individualfirms-Watchlist-peopleFirmStats").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0 Selected)');
		 
	}
	else
	{
		$('#<portlet:namespace/>allOtherFirm').prop("checked", false);
	}
	

}

function <portlet:namespace/>changeOnIndividualPracticeAreas()
{
		var allPracticeCounter = 0;
	
		$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:checked').each(function() {
			 allPracticeCounter++;
	    });
		
		$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox"  value="Practice" id="<portlet:namespace/>practiceSelection"> PracticeAreas ('+allPracticeCounter+') Selected');
		
		if(allPracticeCounter == 0)
		{
			$('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", false);
			$('#<portlet:namespace/>allPracticeAreaFirmStateId').prop("checked", true);
		}
		else
		{
			$('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", true);
			$('#<portlet:namespace/>allPracticeAreaFirmStateId').prop("checked", false);
		}
		<portlet:namespace/>applyPracticeArea(); // apply them as the change
}


	function <portlet:namespace/>changeOnFirmSize()
	{
	
		var allFirmSizeCounter = 0;
		$('#<portlet:namespace/>allOtherFirmSizeDiv :checked').each(function() {
			 allFirmSizeCounter++;
		    });
		
		$("#<portlet:namespace/>firmSizeCounter").html('<input type="checkbox" value="firmSize" id="<portlet:namespace/>firmSizeSelection"> Firms Size ('+allFirmSizeCounter+') Selected');
			if(allFirmSizeCounter == 0)
			{
				$('#<portlet:namespace/>allOtherFirmSize').prop("checked", false);
				$('#<portlet:namespace/>allFirmSize').prop("checked", true);
			}
		else
			{
				$('#<portlet:namespace/>allOtherFirmSize').prop("checked", true);
				$('#<portlet:namespace/>allFirmSize').prop("checked", false);
			}
			
			<portlet:namespace/>applyFirmSize(); // apply them as the change
	}

function <portlet:namespace/>setPage(goToPage)
{
	// Sets the page number to the one selected by the user
	// and fires a submit to refresh with the new page
	// Does NOT tinker with the sort settings
	$("#<portlet:namespace/>goToPage").val(goToPage);
	<portlet:namespace/>search();
}

function <portlet:namespace/>sortAsc(sortColumn)
{
	// Changes the sort order to the new column selected by
	// the user along with the sort direction (ascending)
	// Also RESETS the page to 1
	$("#<portlet:namespace/>sortColumn").val(sortColumn);
	$("#<portlet:namespace/>sortOrder").val("asc");
	$("#<portlet:namespace/>goToPage").val(1);
	<portlet:namespace/>search();
}

function <portlet:namespace/>sortDesc(sortColumn)
{
	// Changes the sort order to the new column selected by
	// the user along with the sort direction (descending)
	// Also RESETS the page to 1
	$("#<portlet:namespace/>sortColumn").val(sortColumn);
	$("#<portlet:namespace/>sortOrder").val("desc");
	$("#<portlet:namespace/>goToPage").val(1);
	<portlet:namespace/>search();
}

function <portlet:namespace/>search()
{
	var selFirms=$("#<portlet:namespace/>Firmstext").val();
	
	//if user keep Firms text box as blank then considering firms filter as  All Firms
	if(!selFirms || 0 === selFirms.length){
		$('#<portlet:namespace/>popup').find("option").attr("selected", false);
		$('#<portlet:namespace/>popup').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>popup').find('input[type=radio]:checked').removeAttr('checked');
		
		$('#<portlet:namespace/>allfirms').prop("checked", true);
	}
	
	
	var isC2VPage = ${isC2VPage};  // its always gauranteed it either evaluates to true or false, but not empty
	var isHomePage = ${isHomePage};
	
	
	if(isC2VPage)
	{
		
		var actionURL = "${clickToViewURL}" +  "&drilldownFirmName=blah"; // we care the existence of param not its value;
		$('#peopleSearchModelBean').attr("action", actionURL);
	}
	
	if(isHomePage)
	{
		<portlet:namespace/>ajaxPersist(); // ajaxSubmit for Charts on HomePage
	}
	else
	{
		Liferay.Portlet.showBusyIcon("#bodyId","Loading..."); // show Busy Icon
		
		$('#peopleSearchModelBean').submit(); // normal form Submit 
	}
	
}


/**
* Persist the search Criteria for Chart on Home Page
*/
function <portlet:namespace/>ajaxPersist()
{
	window.parent.Liferay.Portlet.showBusyIcon("#bodyId","Loading...");
	 $.ajax({
		    url: "${persistSearchCriteriaForChartURL}",
		    method: "POST",
		    traditional: true,
		    data: $('#peopleSearchModelBean').serializeObject(),
		    success: function(data){
		    	
		    	window.parent.Liferay.Portlet.hideBusyIcon("#chartSearchPortletPopupId"); // hide popup loading icon
		    	window.parent.location.href = "home"; // refresh home page
		    	window.parent.Liferay.Portlet.showBusyIcon("#bodyId","Loading..."); // show the loading icon
		    	window.parent.AUI().DialogManager.closeByChild('#chartSearchPortletPopupId'); // close the popup
		    	
		    //	Liferay.Portlet.refreshPortlet('#p_p_id_${chartPortletId}_'); 
		        
		    },
		    error: function(jqXHR, textStatus, errorThrown) {
		    	window.parent.Liferay.Portlet.hideBusyIcon("#chartSearchPortletPopupId");
		         	 alert("error:" + textStatus + " - exception:" + errorThrown);
		   		}
		    }); 
}


	// function for validaing the date
	function ValidateDate(txtDate)
	{
	    var currVal = txtDate;
	    if(currVal == '')
	        return false;
	    
	    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
	    var dtArray = currVal.match(rxDatePattern); // is format OK?
	    
	    if (dtArray == null) 
	        return false;
	    
	    //Checks for mm/dd/yyyy format.
	    dtMonth = dtArray[1];
	    dtDay= dtArray[3];
	    dtYear = dtArray[5];        
	    
	    if (dtMonth < 1 || dtMonth > 12) 
	        return false;
	    else if (dtDay < 1 || dtDay> 31) 
	        return false;
	    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
	        return false;
	    else if (dtMonth == 2) 
	    {
	        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
	        if (dtDay> 29 || (dtDay ==29 && !isleap)) 
	                return false;
	    }
	    return true;
	}
	
</script>



<script>

$(document).ready(function(){
	
	//prepend namspaces to all the Ids (basically Spring Form elements) 
	$("#peopleSearchModelBean input, #peopleSearchModelBean select, #peopleSearchModelBean span").each(function(){
		
		var idVal = $(this).attr("id");
		 $(this).attr("id","<portlet:namespace/>" + idVal );
	});
	
	//Adding validation for date
	$('#<portlet:namespace/>fromdateError').hide();
	$('#<portlet:namespace/>todateError').hide();
	$('#<portlet:namespace/>dateValidError').hide();
	
	$("#<portlet:namespace/>popupDate").hide(); 

	$( "#<portlet:namespace/>from" ).datepicker({
		showOn: "button",
		buttonImage: "/re-people-portlet/images/calendar.gif",
		buttonImageOnly: true,
		defaultDate: "+1w",
		changeMonth: true,
		changeYear: true,
		onClose: function( selectedDate ) {
				$( "#<portlet:namespace/>to" ).datepicker( "option", "minDate", selectedDate );
			}
	});

	$( "#<portlet:namespace/>to" ).datepicker({
		showOn: "button",
		buttonImage: "/re-people-portlet/images/calendar.gif",
		buttonImageOnly: true,
		defaultDate: "+1w",
		changeMonth: true,
		changeYear: true,
		onClose: function( selectedDate ) {
				$( "#<portlet:namespace/>from" ).datepicker( "option", "maxDate", selectedDate );
			}
	});

	//when button for firms popup is clicked it would close all other popup on the screen 
	 $('#<portlet:namespace/>hide, #<portlet:namespace/>popup').click(function(e){		
		 $("#<portlet:namespace/>popupDate").hide();
		 $("#<portlet:namespace/>popupPracticeArea").hide();
		 $("#<portlet:namespace/>popupFirmSize").hide();
		 $("#<portlet:namespace/>popupLocation").hide();
		 
		var allFirmsCounter = 0;
		 
		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:not(:checked)').each(function() {
			allFirmsCounter++;
		});

		if(allFirmsCounter != 0) 
		{
			$("#firmCounter").prop("checked", false);
		} 
		else
		{
			$("#firmCounter").prop("checked", true);
		}
	     e.stopPropagation();	   
	});  

	//when button for date popup is clicked it would close all other popup on the screen 
	$('#<portlet:namespace/>datenone, #<portlet:namespace/>popupDate').click(function(e){
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupPracticeArea").hide();
		 $("#<portlet:namespace/>popupFirmSize").hide();
		 $("#<portlet:namespace/>popupLocation").hide();
	     e.stopPropagation();   
	});

	//when button for Practice area popup is clicked it would close all other popup on the screen 
	$('#<portlet:namespace/>practiceAreaId, #<portlet:namespace/>popupPracticeArea').click(function(e){
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupDate").hide();
		 $("#<portlet:namespace/>popupFirmSize").hide();
		 $("#<portlet:namespace/>popupLocation").hide();
		 
		var allPracticeCounter = 0;
		$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:not(:checked)').each(function() {
				allPracticeCounter++;
		});

		if(allPracticeCounter != 0) 
		{
			$("#<portlet:namespace/>practiceSelection").prop("checked", false);
		} 
		else
		{
			$("#<portlet:namespace/>practiceSelection").prop("checked", true);
		}
		
	     e.stopPropagation();   
	});
	
	$('#ui-datepicker-div').click(function(e){
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupPracticeArea").hide();
		 $("#<portlet:namespace/>popupFirmSize").hide();
		 $("#<portlet:namespace/>popupLocation").hide();
	   e.stopPropagation();   
	});
	
	//when button for firms Size popup is clicked it would close all other popup on the screen 
	$('#<portlet:namespace/>firmSizeId, #<portlet:namespace/>popupFirmSize').click(function(e){
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupDate").hide();
		 $("#<portlet:namespace/>popupPracticeArea").hide();
		 $("#<portlet:namespace/>popupLocation").hide();
		 
		 var allFirmSizeCounter = 0;
		 
		 $('#<portlet:namespace/>allOtherFirmSizeDiv input[type=checkbox]:not(:checked)').each(function() {
				allFirmSizeCounter++;
		 });

		if(allFirmSizeCounter != 0) 
		{
			$("#<portlet:namespace/>firmSizeSelection").prop("checked", false);
		} 
		else
		{
			$("#<portlet:namespace/>firmSizeSelection").prop("checked", true);
		}
		
	     e.stopPropagation();   
	});

	//when button for Location popup is clicked it would close all other popup on the screen 
	$('#<portlet:namespace/>locationId, #<portlet:namespace/>popupLocation').click(function(e){
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupDate").hide();
		 $("#<portlet:namespace/>popupPracticeArea").hide();
		 $("#<portlet:namespace/>popupFirmSize").hide();
		
		 var allLocationsCounter = 0;
		 $('#<portlet:namespace/>allOtherLocationsDiv input[type=checkbox]:not(:checked)').each(function() {
			 allLocationsCounter++;
		 });
		 
		 if(allLocationsCounter != 0)
		{
		 	$('#<portlet:namespace/>locationSelection').prop("checked", false);
		}
		else
		{
			$("#<portlet:namespace/>locationSelection").prop("checked", true);
		}
	     e.stopPropagation();   
	});

	$(document).click(function(){
	    $("#<portlet:namespace/>popup").hide(); 
	    $("#<portlet:namespace/>popupDate").hide();
	    $("#<portlet:namespace/>popupPracticeArea").hide();
	    $("#<portlet:namespace/>popupFirmSize").hide();
	    $("#<portlet:namespace/>popupLocation").hide();		
	});

	//set default date values
	var date = new Date();
	 currentDate = (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear();
	date.setDate(date.getDate() - 7);
	 lastWeekDate = (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear();

	 $('#<portlet:namespace/>btnAdd').click(function(){
		$('#homePageShowPeople .filtersPage').hide();
		$('#<portlet:namespace/>additional').show();
	})
	$('#<portlet:namespace/>btnSave').click(function(){
		$('#homePageShowPeople .filtersPage').show();
		$('#<portlet:namespace/>additional').hide();
	});

	
	$('#<portlet:namespace/>hide').click(
	    function () {
	        $("#<portlet:namespace/>popup").stop().slideToggle(500);    
	});
	
	$('#<portlet:namespace/>locationId').click(
		    function () {
		        $("#<portlet:namespace/>popupLocation").stop().slideToggle(500);    
		});

	$('#<portlet:namespace/>datenone').click(
		function () {
		    $("#<portlet:namespace/>popupDate").stop().slideToggle(500);    
	}); 

	$('#<portlet:namespace/>practiceAreaId').click(
		function () {
		    $("#<portlet:namespace/>popupPracticeArea").stop().slideToggle(500);    
	}); 
	
	$('#<portlet:namespace/>firmSizeId').click(
			function () {
			    $("#<portlet:namespace/>popupFirmSize").stop().slideToggle(500);    
		}); 
	

	//will run the search when user clicks on Apply button on People search  screen
	$("#<portlet:namespace/>applySearch").click(function() 
	{
		$("#<portlet:namespace/>goToPage").val(1);
		<portlet:namespace/>search();
	});
	
	//UI Logic for Firms
	$("input[id='<portlet:namespace/>allfirms']").change(function(){
		var checked = $("#<portlet:namespace/>allfirms").is(":checked");
		if(checked){		 
		 $('#<portlet:namespace/>RivalEdgeList').prop("checked", false);
		 $('#<portlet:namespace/>popup option:selected').removeAttr("selected");
		 $('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
		 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="Abrams" id="firmCounter"> Firms (0) Selected');
		}
	});

	$("input[id='<portlet:namespace/>RivalEdgeList']").change(function(){
		var checked = $("#<portlet:namespace/>RivalEdgeList").is(":checked");
		if(checked){   
		 $('#<portlet:namespace/>allfirms').prop("checked", false);
		 $('#<portlet:namespace/>popup option:nth(0)').attr("selected", "selected");	 
		 $('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
		 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="Abrams" id="firmCounter"> Firms (0) Selected');
		}
	});
	
	
	$("#<portlet:namespace/>Select1").change(function(){		
		var count = $("#<portlet:namespace/>Select1 :selected").length;
		if(count > 0){	
			 $('#<portlet:namespace/>allfirms').prop("checked", false);
			 $('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
			 $('#<portlet:namespace/>RivalEdgeList').prop("checked", true);
			 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="Abrams" id="firmCounter"> Firms (0) Selected');
		}
		
	}); 

	$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]').change(function(){
			var allFirmsCounter = 0;
			 $('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
				 allFirmsCounter++;
			    });
				$('#<portlet:namespace/>Select1 option:selected').removeAttr("selected");
				$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="Abrams" id="firmCounter"> Firms ('+allFirmsCounter+') Selected');
				
				if(allFirmsCounter > 0)
				{
					 $('#<portlet:namespace/>allOtherFirm').prop("checked", true);
					 $('#<portlet:namespace/>individualfirmsWatchListDIVPeopleFirmStats').find('input[type=checkbox]:checked').removeAttr('checked');
				}
				else
				{
					$('#<portlet:namespace/>allOtherFirm').prop("checked", false);
					 $('#<portlet:namespace/>firm_watchlist').prop("checked", true);
					 $('#<portlet:namespace/>allOtherFirm').prop("checked", false);
					 $('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
					var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
					
					$('#<portlet:namespace/>individualfirmsWatchListDIVPeopleFirmStats input[type=checkbox]').each(function() {
						 if(this.value == watchListId)
							{
								$(this).prop("checked", true);
							}
					});
				}
			
	});

	$('#<portlet:namespace/>clearButton').bind('click', function (event) {
		<portlet:namespace/>clearFirms();
		
	});
	
	//UI Logic for Date
	
	$("#<portlet:namespace/>datePeriodSelect").change(function() {
		 $("#<portlet:namespace/>period").prop("checked", true);
		 $("#<portlet:namespace/>dateRange").removeAttr("checked");
		 <portlet:namespace/>applyDate(); // apply them as the change
	});

	$("#<portlet:namespace/>from, #<portlet:namespace/>to").change(function() {
		 $("#<portlet:namespace/>dateRange").prop("checked", true);
		 $("#<portlet:namespace/>period").removeAttr("checked");
		 <portlet:namespace/>applyDate(); // apply them as the change
	});
	
	$('#<portlet:namespace/>clearButtonDate').bind('click', function (event) {
		<portlet:namespace/>clearDate();
		
	}); 
	
	
	//UI Logic for Locations
	$("#<portlet:namespace/>allLocations").change(function(){
		var checked = $(this).is(":checked");
		if(checked){  		
		 $('#<portlet:namespace/>allOtherLocationsDiv').find('input[type=checkbox]:checked').removeAttr('checked');	
		 $('#<portlet:namespace/>allOtherLocations').removeAttr('checked');	
		 $("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="Locations" id="<portlet:namespace/>locationSelection"> Locations (0) Selected');
		}
		<portlet:namespace/>applyLocations(); // apply them as the change
	});
	
	$("#<portlet:namespace/>allOtherLocations, #<portlet:namespace/>allOtherLocationsDiv input[type=checkbox]").click(function(){
	
		<portlet:namespace/>changeOnIndividualLocations();
	});
	
	 $('#<portlet:namespace/>clearButtonLocations').bind('click', function (event) {
		<portlet:namespace/>clearLocations();
	});

	//UI Logic for Practice Area
	
	$("#<portlet:namespace/>allPracticeAreaFirmStateId").change(function(){
		var checked = $(this).is(":checked");
		if(checked){  		
		 $('#<portlet:namespace/>allOtherPracticeArea').find('input[type=checkbox]:checked').removeAttr('checked');	
		 $('#<portlet:namespace/>allPracticeAreaIndividual').removeAttr('checked');	
		 $("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox"  value="Practice" id="<portlet:namespace/>practiceSelection"> PracticeArea (0) Selected');
		}
		<portlet:namespace/>applyPracticeArea(); // apply on change
	});
	
	$("#<portlet:namespace/>allPracticeAreaIndividual, #<portlet:namespace/>practiceAreaDiv input[type=checkbox]").click(function(){
		<portlet:namespace/>changeOnIndividualPracticeAreas();
	});
	
	$('#<portlet:namespace/>clearPracticeArea').bind('click', function (event) {
		<portlet:namespace/>clearPracticeAreas();
	});

	
	//UI Logic for FirmSize
	$("#<portlet:namespace/>allFirmSize").change(function(){
		var checked = $("#<portlet:namespace/>allFirmSize").is(":checked");
		if(checked){  		
		 $('#<portlet:namespace/>allOtherFirmSizeDiv').find('input[type=checkbox]:checked').removeAttr('checked');
		 $('#<portlet:namespace/>allOtherFirmSize').removeAttr('checked');
		 $("#<portlet:namespace/>firmSizeCounter").html('<input type="checkbox" value="firmSize" id="firmSizeSelection"> Firms Size (0) Selected');
		}
		<portlet:namespace/>applyFirmSize();// apply on change
	});
	
	
	$("#<portlet:namespace/>allOtherFirmSize, #<portlet:namespace/>allOtherFirmSizeDiv input[type=checkbox]").change(function(){
		<portlet:namespace/>changeOnFirmSize();
	});
	
	
	$('#<portlet:namespace/>clearButtonFirmSize').bind('click', function (event) {
		<portlet:namespace/>clearFirmSize();
	});

	$('#<portlet:namespace/>resetAll').bind('click', function (event) {
		clearAll();
		
	});

	$('#<portlet:namespace/>ApplyFirm').bind('click', function (event) {
			<portlet:namespace/>applyFirms();
		$("#<portlet:namespace/>popup").toggle();
	});

	$('#<portlet:namespace/>ApplyDate').bind('click', function (event) {
		
		var checkDate = <portlet:namespace/>applyDate();
		if(!checkDate)
			{
				 $("#<portlet:namespace/>popupDate").toggle();
			}
	}); 

	//this method get invoked when user clicks on Apply button from Practice Area popup and set selected practice area values to Practice area text box
	$('#<portlet:namespace/>ApplyPracticeArea').bind('click', function (event) {
		<portlet:namespace/>applyPracticeArea();
		 $("#<portlet:namespace/>popupPracticeArea").toggle();
	
	});
	
	//this method get invoked when user clicks on Apply button from Firm Size popup and set selected firm size values to Firm Size text box
	$('#<portlet:namespace/>ApplyFirmSize').bind('click', function (event) {		
		<portlet:namespace/>applyFirmSize();
		 $("#<portlet:namespace/>popupFirmSize").toggle();	
	});
	
	//this method get invoked when user clicks on Apply button from Location popup and set selected locations to Location text box
	$('#<portlet:namespace/>ApplyLocations').bind('click', function (event) {		
		<portlet:namespace/>applyLocations();
		$("#<portlet:namespace/>popupLocation").toggle();	
	});
	
	
	//Select All firms by selecting one check box
	$('#<portlet:namespace/>individualFirmsCounter').click(
		function () {
			  	var checked = $("#<portlet:namespace/>individualFirmsCounter #firmCounter").is(":checked");
			  	var watchListId = $('#defaultWatchListId').val();
				var allFirmsCounter = 0;
				var isFirmChecked = false;
				
				if(checked)
				{
					 $('#<portlet:namespace/>allOtherFirmDiv #<portlet:namespace/>allFirmsCheckBoxCounter').prop('checked', true);
					 $('#homePageShowPeople .rivaledgeListAMLAW_100_PeopleFirmStats').prop("checked", false);
					 $('#homePageShowPeople .rivaledgeListAMLAW_200_PeopleFirmStats').prop("checked", false);
					 $('#homePageShowPeople .rivaledgeListNLJ_250_PeopleFirmStats').prop("checked", false);
					 $('#<portlet:namespace/>firm_watchlist').prop("checked", false);
					 $('#<portlet:namespace/>individualfirmsWatchListPeopleFirmStats input[type=checkbox]:checked').removeAttr('checked');
					 $('#<portlet:namespace/>individualfirmsWatchListDIVPeopleFirmStats input[type=checkbox]:checked').removeAttr('checked');
					 
				}
				else
				{
					$('#<portlet:namespace/>allOtherFirmDiv #<portlet:namespace/>allFirmsCheckBoxCounter').removeAttr('checked');
					
					
					$('#individualfirmsWatchListDIVPeopleFirmStats input[type=checkbox]').each(function() {
						 if(this.value == watchListId)
							{
								$(this).prop("checked", true);
								$("#Firmstext").val($(this).attr('labelAttr'));
								$("#selectedFirms").val($(this).val());

								$("#firm_watchlist").prop("checked", true);
								$('#firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
								isFirmChecked = true;
							}
					});
					
					if(isFirmChecked == false)
					{
						$("#Firmstext").val('AMLAW 100');
						$("#selectedFirms").val('AMLAW 100');
						$('#firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
						 $("#firm_watchlist").prop("checked", true);
						 $("#homePageShowPeople .rivaledgeListAMLAW_100_PeopleFirmStats").prop("checked", true);
						 
					}
					
				}
				
				 $('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
						allFirmsCounter++;
					});

				if(allFirmsCounter != 0) 
				{
					 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="Abrams" id="firmCounter"> Firms ('+allFirmsCounter+') Selected');
					 $("#<portlet:namespace/>individualFirmsCounter #firmCounter").prop("checked", true);
					 $("#<portlet:namespace/>allOtherFirm").prop("checked", true);
				}
				else
				{
					 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="Abrams" id="firmCounter"> Firms ('+allFirmsCounter+') Selected');
					 $("#<portlet:namespace/>individualFirmsCounter #firmCounter").prop("checked", false);
					 $("#<portlet:namespace/>allOtherFirm").prop("checked", false);
					 $("#homePageShowPeople .rivaledgeListAMLAW_100_PeopleFirmStats").prop("checked", false);
					 $("#<portlet:namespace/>firm_watchlist").prop("checked", true);
					 var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
						
						$('#<portlet:namespace/>individualfirmsWatchListDIVPeopleFirmStats input[type=checkbox]').each(function() {
							 if(this.value == watchListId)
								{
									$(this).prop("checked", true);
								}
						});
				}
				
				<portlet:namespace/>applyFirms();

		});
	
	//Select All Locations by selecting one check box
	$('#<portlet:namespace/>locationsCounter').click(
			function () {
			  
			  	var checked = $("#<portlet:namespace/>locationsCounter #locationSelection").is(":checked");
				var allLocationsCounter = 0;
				
				if(checked)
				{
					 $('#<portlet:namespace/>allOtherLocationsDiv .allLocationsCheckBox').prop('checked', true);
				}
				else
				{
					$('#<portlet:namespace/>allOtherLocationsDiv .allLocationsCheckBox').removeAttr('checked');
				}
				
				 $('#<portlet:namespace/>allOtherLocationsDiv input[type=checkbox]:checked').each(function() {
						allLocationsCounter++;
					});

				if(allLocationsCounter != 0) 
				{
					 $("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="Locations" id="locationSelection"> Locations ('+allLocationsCounter+') Selected');
					 $("#<portlet:namespace/>locationsCounter #locationSelection").prop("checked", true);
					 $("#<portlet:namespace/>allOtherLocations").prop("checked", true);
					 $("#<portlet:namespace/>allLocations").prop("checked", false);
				}
				else
				{
					 $("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="Locations" id="locationSelection"> Locations ('+allLocationsCounter+') Selected');
					 $("#<portlet:namespace/>locationsCounter #locationSelection").prop("checked", false);
					 $("#<portlet:namespace/>allOtherLocations").prop("checked", false);
					 $("#<portlet:namespace/>allLocations").prop("checked", true);
				}
				<portlet:namespace/>applyLocations();

		});
	
	//Select All Practice Areas by selecting one check box
	$('#<portlet:namespace/>practiceAreaCounter').click(
			function () {
			  
			  	var checked = $("#<portlet:namespace/>practiceAreaCounter #<portlet:namespace/>practiceSelection").is(":checked");
				var allPracticeCounter = 0;
				if(checked)
				{
					 $('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]').prop('checked', true);
				}
				else
				{
					$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]').removeAttr('checked');
				}
				
				$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:checked').each(function() {
						allPracticeCounter++;
				});

				if(allPracticeCounter != 0) 
				{
					 $("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox"  value="Practice" id="<portlet:namespace/>practiceSelection"> PracticeAreas ('+allPracticeCounter+') Selected');
					 $("#<portlet:namespace/>practiceAreaCounter #<portlet:namespace/>practiceSelection").prop("checked", true);
					 $("#<portlet:namespace/>allPracticeAreaIndividual").prop("checked", true);
					 $("#<portlet:namespace/>allPracticeAreaFirmStateId").prop("checked", false);
				}
				else
				{
					 $("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox"  value="Practice" id="<portlet:namespace/>practiceSelection"> PracticeAreas ('+allPracticeCounter+') Selected');
					 $("#<portlet:namespace/>practiceAreaCounter #<portlet:namespace/>practiceSelection").prop("checked", false);
					 $("#<portlet:namespace/>allPracticeAreaIndividual").prop("checked", false);
					 $("#<portlet:namespace/>allPracticeAreaFirmStateId").prop("checked", true);
				}
				
				<portlet:namespace/>applyPracticeArea();

		});
	
	//Select All Firm Size by selecting one check box	
	$('#<portlet:namespace/>firmSizeCounter').click(
			function () {
			  
			  	var checked = $("#<portlet:namespace/>firmSizeCounter input[type=checkbox]").is(":checked");
				var allFirmSizeCounter = 0;
				if(checked)
				{
					 $('#<portlet:namespace/>allOtherFirmSizeDiv input[type=checkbox]').prop('checked', true);
				}
				else
				{
					$('#<portlet:namespace/>allOtherFirmSizeDiv input[type=checkbox]').prop("checked", false);
				}
				
				 $('#<portlet:namespace/>allOtherFirmSizeDiv input[type=checkbox]:checked').each(function() {
						allFirmSizeCounter++;
					});

				if(allFirmSizeCounter != 0) 
				{
					 $("#<portlet:namespace/>firmSizeCounter").html('<input type="checkbox" value="firmSize" id="firmSizeSelection">  Firms Size ('+allFirmSizeCounter+') Selected');
					 $("#<portlet:namespace/>firmSizeCounter input[type=checkbox]").prop("checked", true);
					 $("#<portlet:namespace/>allOtherFirmSize").prop("checked", true);
					 $("#<portlet:namespace/>allFirmSize").prop("checked", false);
				}
				else
				{
					 $("#<portlet:namespace/>firmSizeCounter").html('<input type="checkbox" value="firmSize" id="firmSizeSelection">  Firms Size ('+allFirmSizeCounter+') Selected');
					 $("#<portlet:namespace/>firmSizeCounter input[type=checkbox]").prop("checked", false);
					 $("#<portlet:namespace/>allOtherFirmSize").prop("checked", false);
					 $("#<portlet:namespace/>allFirmSize").prop("checked", true);
				}
				
				<portlet:namespace/>applyFirmSize(); // apply on change
		});
	
	$('#homePageShowPeople .rivaledgeListAMLAW_100_PeopleFirmStats').bind('click', function (event) {
		
		 $('#homePageShowPeople .rivaledgeListAMLAW_100_PeopleFirmStats').prop("checked", true);
		 $('#homePageShowPeople .rivaledgeListAMLAW_200_PeopleFirmStats').prop("checked", false);
		 $('#homePageShowPeople .rivaledgeListNLJ_250_PeopleFirmStats').prop("checked", false);
		 $('#<portlet:namespace/>firm_watchlist').prop("checked", true);
		 $('#<portlet:namespace/>individualfirmsWatchListDIVPeopleFirmStats input[type=checkbox]:checked').removeAttr('checked');
	 	 $("#homePageShowPeople .individualfirms-Watchlist-peopleFirmStats").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0 Selected)');
	 	
	 	$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="firmCounter"/> Firms (0) Selected');
		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
		
		$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.AMLAW_100 %>');
		$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.AMLAW_100 %>');
		$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
		<portlet:namespace/>applyFirms(); // apply on change
	});	
	
	$('#homePageShowPeople .rivaledgeListAMLAW_200_PeopleFirmStats').bind('click', function (event) {
		
		 $('#homePageShowPeople .rivaledgeListAMLAW_100_PeopleFirmStats').prop("checked", false);
		 $('#homePageShowPeople .rivaledgeListAMLAW_200_PeopleFirmStats').prop("checked", true);
		 $('#homePageShowPeople .rivaledgeListNLJ_250_PeopleFirmStats').prop("checked", false);
		 $('#<portlet:namespace/>firm_watchlist').prop("checked", true);
		 $('#<portlet:namespace/>individualfirmsWatchListDIVPeopleFirmStats input[type=checkbox]:checked').removeAttr('checked');
	 	 $("#homePageShowPeople .individualfirms-Watchlist-peopleFirmStats").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0 Selected)');
	 	
	 	$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="firmCounter"/> Firms (0) Selected');
		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
		
		$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.AMLAW_200 %>');
		$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.AMLAW_200 %>');
		$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
		<portlet:namespace/>applyFirms(); // apply on change
	});
	
	$('#homePageShowPeople .rivaledgeListNLJ_250_PeopleFirmStats').bind('click', function (event) {
		
		 $('#homePageShowPeople .rivaledgeListAMLAW_100_PeopleFirmStats').prop("checked", false);
		 $('#homePageShowPeople .rivaledgeListAMLAW_200_PeopleFirmStats').prop("checked", false);
		 $('#homePageShowPeople .rivaledgeListNLJ_250_PeopleFirmStats').prop("checked", true);
		 $('#<portlet:namespace/>firm_watchlist').prop("checked", true);
		 $('#<portlet:namespace/>individualfirmsWatchListDIVPeopleFirmStats input[type=checkbox]:checked').removeAttr('checked');
	 	 $("#homePageShowPeople .individualfirms-Watchlist-peopleFirmStats").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0 Selected)');
	 	
	 	$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="firmCounter"/> Firms (0) Selected');
		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
		
		$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.NLJ_250 %>');
		$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.NLJ_250 %>');
		$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
		<portlet:namespace/>applyFirms(); // apply on change
	});
	
	 $('#homePageShowPeople .allFirmsCheckBoxWatchListPeopleFirmStats').click(function() {
			
		 var allWatchListCounter = 0;
			$('#homePageShowPeople .rivaledgeListAMLAW_100_PeopleFirmStats').prop("checked", false);
			$('#homePageShowPeople .rivaledgeListAMLAW_200_PeopleFirmStats').prop("checked", false);
			$('#homePageShowPeople .rivaledgeListNLJ_250_PeopleFirmStats').prop("checked", false);
			$('#<portlet:namespace/>firm_watchlist').prop("checked", true);
			$('#<portlet:namespace/>allfirms').prop("checked", false);
			$('#<portlet:namespace/>allOtherFirmDiv').prop("checked", false);
			 
			$('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
			$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="Abrams" id="firmCounter"> Firms (0 Selected)');
			 
			$('#<portlet:namespace/>individualfirmsWatchListDIVPeopleFirmStats input[type=checkbox]:checked').each(function() {
			 	allWatchListCounter++;
		    });
			$("#homePageShowPeople .individualfirms-Watchlist-peopleFirmStats").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists ('+allWatchListCounter +' Selected)');
			 
			 $('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
			 <portlet:namespace/>applyFirms(); // apply on change
			});
		  
	 $("#<portlet:namespace/>allfirms").change(function(){
			$("#<portlet:namespace/>allfirms").prop("checked", true);
			$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.ALL_FIRMS %>');
			$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.ALL_FIRMS %>');
			$("#<portlet:namespace/>allfirms").val('<%=ALMConstants.ALL_FIRMS %>');
			
			$("#homePageShowPeople .rivaledgeListAMLAW_100_PeopleFirmStats").prop("checked", false);
			$("#homePageShowPeople .rivaledgeListAMLAW_200_PeopleFirmStats").prop("checked", false);
			$("#homePageShowPeople .rivaledgeListNLJ_250_PeopleFirmStats").prop("checked", false);
			$("#<portlet:namespace/>firm_watchlist").prop("checked", false);
			$('#<portlet:namespace/>individualfirmsWatchListDIVPeopleFirmStats').find('input[type=checkbox]:checked').removeAttr('checked');
			$('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
			$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="Abrams" id="firmCounter"> Firms (0) Selected');
			<portlet:namespace/>applyFirms(); // apply on change
		});

	$("input[class='allFirmsCheckBox']").change(function(){
		
		<portlet:namespace/>changeOnIndividualFirms();
	});
	
	function <portlet:namespace/>changeOnIndividualFirms()
	{
		var allFirmsCounter = 0;
		 $('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
			 allFirmsCounter++;
		    });
			
			$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="Abrams" id="firmCounter"> Firms ('+allFirmsCounter+') Selected');
			if(allFirmsCounter == 0)
			{
				$('#<portlet:namespace/>firm_watchlist').prop("checked", false);
				$('#<portlet:namespace/>allOtherFirm').prop("checked", false);
				$('#<portlet:namespace/>allfirms').prop("checked", false);
				
				$('#<portlet:namespace/>individualfirmsWatchListPeopleFirmStats input[type=checkbox]:checked').removeAttr('checked');
				$('#<portlet:namespace/>individualFirmsCounter input[type=checkbox]:checked').removeAttr('checked');
				

				var watchListId = $('#defaultWatchListId').val();
				
				$('#<portlet:namespace/>individualfirmsWatchListDIVPeopleFirmStats input[type=checkbox]').each(function() {
					 if(this.value == watchListId)
						{
							$(this).prop("checked", true);
						}
				});
			}
		else
			{
				$('#<portlet:namespace/>allOtherFirm').prop("checked", true);
				$('#<portlet:namespace/>firm_watchlist').prop("checked", false);
				$('#<portlet:namespace/>allfirms').prop("checked", false);
				
				$('#<portlet:namespace/>individualfirmsWatchListPeopleFirmStats input[type=checkbox]:checked').removeAttr('checked');
				
				$("#homePageShowPeople .individualfirms-Watchlist-peopleFirmStats").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0) Selected');
				
				
				$('#<portlet:namespace/>individualfirmsWatchListDIVPeopleFirmStats input[type=checkbox]:checked').removeAttr('checked');
				
			}
			
			<portlet:namespace/>applyFirms(); // apply on change

	}
	
		 $("#<portlet:namespace/>additionalFilter").click(function () {
		  var link = $(this);	
		  //Show and hide additional filter Div
		  $('#<portlet:namespace/>additionalFilterDiv').slideToggle('slow', function() {
		            if ($(this).is(":visible")) {
		                 link.text('Hide Filter');                
		            } else {
		                 link.text('Additional Filter');                
		            }        
		        });
		});
 

		//It will check if any option(Partners, Associates, Other Counsel, Admin, Other) is selected on view setting popup
		//then check the option for All Groups otherwise uncheck it.
		$("input[name='displayColumns']").change(function(){
			<portlet:namespace/>changeOnDisplayColumns();
		
		});
		
			$("input[id='<portlet:namespace/>allGroupsAttorneys']").change(function(){
			var checked = $("#<portlet:namespace/>allGroupsAttorneys").is(":checked");
			if(checked){		 
			 $('#<portlet:namespace/>allOtherAttorney').find('input[type=checkbox]').prop("checked", true);		 
			} else {
			$('#<portlet:namespace/>allOtherAttorney').find('input[type=checkbox]:checked').prop("checked", false);	
			}
		});
		
			$("input[name='moves']").change(function(){
			<portlet:namespace/>changeOnMoves();
		
		});
		
			$("input[id='<portlet:namespace/>peopleMove']").change(function(){
			var checked = $("#<portlet:namespace/>peopleMove").is(":checked");
			if(checked){		 
				$('#<portlet:namespace/>added').prop("checked", true);
				$('#<portlet:namespace/>removed').prop("checked", true);
			} else {
				$('#<portlet:namespace/>added').prop("checked", false);
				$('#<portlet:namespace/>removed').prop("checked", false);
			}
		});
		
			//Spring will add <input> tags with names prefixed with "_" for type "radio" and "checkbox".
			//On form POST submit they all are carried to server which is unneccessary
			//hence removing all such input box to make page and jQuery selections less heavy :) on document ready.
			
			//NOTE : If any inputs added with name prefixed with "_" purposefully will also get removed. So be cautious
		$("#peopleSearchModelBean input[name^=_]").remove();

		//autoselect previously selected search critria
		<portlet:namespace/>initializeSearchCriteria();
		
});

</script>


<!-- AutoComplete JS Code Start -->
<script>
$.widget("custom.catcomplete", $.ui.autocomplete, {
	_renderMenu: function( ul, items ) {
		var that = this,
		currentCategory = "";
		$.each( items, function( index, item ) {
			if ( item.category != currentCategory ) {
			ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
			currentCategory = item.category;
			}
	  that._renderItemData( ul, item );
	});
}
});

$(document).ready(function() {
	
var data = ${firmJson};
var practiceData = ${practiceJson};
var firmSizeData = ${allFirmSizeJson};


var availableLocation = new Array();

		<c:forEach items="${allOtherLocations}" var="loc">
			availableLocation.push("${loc}");
		</c:forEach>	   

function split( val ) {
	return val.split( /,\s*/ );
}

function <portlet:namespace/>splitValuesApartFromFirms( val ) {
	return val.split( /;\s*/ );
}

function extractLast( term ) {
	return split( term ).pop();
}

function <portlet:namespace/>extractLastApartFromFirms( term ) {
	return <portlet:namespace/>splitValuesApartFromFirms( term ).pop();
}

//Location Autocomplete code - START

$("#<portlet:namespace/>selectedLocation")
// don't navigate away from the field on tab when selecting an item
.bind( "keydown", function( event ) {
  if ( event.keyCode === $.ui.keyCode.TAB &&
      $( this ).data( "ui-autocomplete" ).menu.active ) {
    event.preventDefault();
  }
})
.autocomplete({
  minLength: 0,
  source: function( request, response ) {
    // delegate back to autocomplete, but extract the last term
    response( $.ui.autocomplete.filter(
    		availableLocation, <portlet:namespace/>extractLastApartFromFirms( request.term ) ) );
  },
  focus: function() {
    // prevent value inserted on focus
    return false;
  },
  select: function( event, ui ) {
    var terms = <portlet:namespace/>splitValuesApartFromFirms( this.value );
    // remove the current input
    terms.pop();
    // add the selected item
    terms.push( ui.item.value );
    // add placeholder to get the comma-and-space at the end
    terms.push("");   
    
    $('#<portlet:namespace/>allOtherLocationsDiv input[type=checkbox]').each(function()
    		{ 
				if($(this).val() == (ui.item.value) )
				{
					$(this).prop("checked", true);
					<portlet:namespace/>changeOnIndividualLocations();
					return false;
				}
			});
    this.value = terms.join(";");
    return false;
  }
});

//Location Autocomplete code - END

//Firm Size Autocomplete code - START

$("#<portlet:namespace/>selectedFirmSize")
// don't navigate away from the field on tab when selecting an item
.bind( "keydown", function( event ) {
  if ( event.keyCode === $.ui.keyCode.TAB &&
      $( this ).data( "ui-autocomplete" ).menu.active ) {
    event.preventDefault();
  }
})
.autocomplete({
  minLength: 0,
  source: function( request, response ) {
    // delegate back to autocomplete, but extract the last term
    response( $.ui.autocomplete.filter(
    		firmSizeData, <portlet:namespace/>extractLastApartFromFirms( request.term ) ) );
  },
  focus: function() {
    // prevent value inserted on focus
    return false;
  },
  select: function( event, ui ) {
    var terms = <portlet:namespace/>splitValuesApartFromFirms( this.value );
    // remove the current input
    terms.pop();
    // add the selected item
    terms.push( ui.item.value );
    // add placeholder to get the comma-and-space at the end
    terms.push("");
    
   
    $('#<portlet:namespace/>allOtherFirmSizeDiv input[type=checkbox]').each(function()
	   		{ 
				if($(this).val() == (ui.item.value) )
				{
					$(this).prop("checked", true);
					<portlet:namespace/>changeOnFirmSize();
					return false;
				}
			});
    this.value = terms.join(";");
    return false;
  }
});

//Firm Size Autocomplete code - END



// Practice Area Autocomplete code - START
$( "#<portlet:namespace/>selectedPracticeArea" )
	//don't navigate away from the field on tab when selecting an item
	.bind( "keydown", function( event ) {
		if ( event.keyCode === $.ui.keyCode.TAB &&
		$( this ).data( "ui-autocomplete" ).menu.active ) {
		event.preventDefault();
		}
	})
	.autocomplete({
	minLength: 3,
	source: function( request, response ) {
		//delegate back to autocomplete, but extract the last term
		response( $.ui.autocomplete.filter(
			practiceData, <portlet:namespace/>extractLastApartFromFirms( request.term ) ) );
	},
	focus: function() {
		//prevent value inserted on focus
		return false;
	},
	select: function( event, ui ) {
		var terms = <portlet:namespace/>splitValuesApartFromFirms( this.value );
		//remove the current input
		terms.pop();
		//add the selected item
		terms.push( ui.item.value );
		//add placeholder to get the comma-and-space at the end
		terms.push( "" );		
		
		$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]').each(function()
		   		{ 
					if($(this).val() == (ui.item.value) )
					{
						$(this).prop("checked", true);
						<portlet:namespace/>changeOnIndividualPracticeAreas();
						return false;
					}
				});
		this.value = terms.join( ";" );		
		return false;
	}
});

//Practice Area Autocomplete code - END

$( "#<portlet:namespace/>Firmstext" ).catcomplete({
	minLength: 3,
	source: function( request, response ) {
		// delegate back to autocomplete, but extract the last term
		response( $.ui.autocomplete.filter(
		data, extractLast( request.term ) ) );
	},
	focus: function() {
		// prevent value inserted on focus
		return false;
	},	
	change: function(event, ui) {	
		
		
		$('#<portlet:namespace/>popup').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>popup').find('input[type=radio]:checked').removeAttr('checked');
		
		var selectedValues = this.value.split(',');
		var output = '';
		var currentvalue="";		
		$.each(selectedValues, function(key, line) {
			line=line.trim();		
			if(line!=""){				
				currentvalue=$("input[labelattr='"+line+"'].allFirmsCheckBox").val();
				if(typeof(currentvalue)!="undefined"){
					output= output + currentvalue+ ",";
					$("input[labelattr='"+line+"'].allFirmsCheckBox").prop("checked", true);
					<portlet:namespace/>allOtherFirmChange();
				}	
							
			}			
		});		
		output = output.substring(0, output.length - 1);
		$('#<portlet:namespace/>selectedFirms').val(output);		
    },
	select: function( event, ui ) {
		var terms = split( this.value );		
		var termsValue = split( this.id );
		if( ui.item.value.indexOf("AmLaw 100") !== -1){
			return false;
		}
		
		// remove the current input
		terms.pop();
		termsValue.pop();
		// add the selected item
		
		terms.push( ui.item.value );
		termsValue.push( ui.item.id );
		// add placeholder to get the comma-and-space at the end
		terms.push( "" );
		termsValue.push( "" );
		this.value = terms.join( ", " );	
		$('#<portlet:namespace/>selectedFirms').val($('#<portlet:namespace/>selectedFirms').val().replace("AmLaw 100","") + "," + ui.item.id);		
		$('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]').each(function()
   		{ 
			if($(this).val() == (ui.item.id) )
			{
				$(this).prop("checked", true);
				<portlet:namespace/>allOtherFirmChange();
				return false;
			}
		});
		
		return false;
	}
});

});
</script>
<!-- AutoComplete JS Code End -->



<div id="homePageShowPeople" >
	
	<div class="breadcrumbs"><span>Firm Statistics</span></div>
	<form:form  commandName="peopleSearchModelBean" method="post" action="${submitURL}" id="peopleSearchModelBean" autocomplete="off" >
		<form:hidden path="goToPage"  	id="goToPage" />
	  	<form:hidden path="sortColumn" 	id="sortColumn" />
	  	<form:hidden path="sortOrder"  	id="sortOrder" />
        <form:hidden path="portletId"	id="portletId"/>
        <form:hidden path="addToSession"	id="addToSession"/>
	  	<div id="dropdown"> <!-- dropdown_title_start -->
	    	<ul id="droplist">    
	       
	        <li>
		        <label>Firm(s)</label>
		        <div class="srchBox">
		         <!-- <input type="text" name="Firmstext" id="Firmstext"  style="text-overflow:ellipsis;" class="input" /> -->
	       <c:choose>
              <c:when test="${empty allWatchListsDefaultList or allWatchListsDefaultList == ''}">
          			<input type="text" name="Firmstext" id="Firmstext"  value="<%=ALMConstants.AMLAW_100 %>" style="text-overflow:ellipsis;" class="input" autocomplete="off"/>
          	  </c:when>
          	  <c:otherwise>
          			<input type="text" name="Firmstext" id="Firmstext"  value="${allWatchListsDefaultList.groupName}" style="text-overflow:ellipsis;" class="input" autocomplete="off"/>
          	  </c:otherwise>
          	</c:choose>
		          <input type="button" name="search" value="" class="srchBack" id="testDiv" />
		          <div class="clear">&nbsp;</div>
		        </div>
		        <input type="button" name="search" id="hide"  value="" class="typeSel" />
		       
		        <div class="rel">
				<div  id="<portlet:namespace/>popup" class="firmPage">
				<p><form:radiobutton path="firmType" id="allfirms" value="<%=ALMConstants.ALL_FIRMS %>" />&nbsp;<%=ALMConstants.ALL_FIRMS %><!-- <input type="radio" name="Firms" value="All Firms"  id="allfirms" />All Firms --></p>
					<%-- <p><form:radiobutton path="firmType" id="RivalEdgeList"   value="<%=ALMConstants.RIVALEDGE_LIST %>" />&nbsp;<%=ALMConstants.RIVALEDGE_LIST %></p>
			
			<form:select path="firmList" id="Select1" multiple="false" size="4" cssStyle="width:220px">
				<form:options items="${allRankingList}" />
			</form:select> --%>
			
			 <p> <form:radiobutton path="firmType" id="firm_watchlist" value=""/>Select Watchlist/RivalEdge List</p>
             	<div class="individualfirms-Watchlist-peopleFirmStats" >
                	<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0 Selected)<br />
               </div>
 				 <div  class="individualfirmsWatchListPeopleFirmStats" id="<portlet:namespace/>individualfirmsWatchListPeopleFirmStats"> 
 				 
 				 	<form:checkbox  class="rivaledgeListAMLAW_100_PeopleFirmStats" path="firmList"  value="<%=ALMConstants.AMLAW_100 %>" /><%=ALMConstants.AMLAW_100 %><br>
					<form:checkbox  class="rivaledgeListAMLAW_200_PeopleFirmStats" path="firmList"  value="<%=ALMConstants.AMLAW_200 %>" /><%=ALMConstants.AMLAW_200 %><br>
					<form:checkbox  class="rivaledgeListNLJ_250_PeopleFirmStats" path="firmList"  value="<%=ALMConstants.NLJ_250 %>" /><%=ALMConstants.NLJ_250 %><br>
  
 				 	<div  class="individualfirmsWatchListDIV" id="<portlet:namespace/>individualfirmsWatchListDIVPeopleFirmStats"> 
		                <c:forEach var="watchlist"  items="${allWatchListsResult}">     
	    	  				 <form:checkbox  class="allFirmsCheckBoxWatchListPeopleFirmStats" path="firmListWatchList"  value="${watchlist.groupId}" labelAttr="${watchlist.groupName}" />${watchlist.groupName}<br>
	  				 	</c:forEach>
					</div>
			 	</div> 
			
			<br>
					
			<p><form:radiobutton path="firmType" value="<%=ALMConstants.INDIVIDUAL_LIST %>" id="allOtherFirm"/>&nbsp;<%=ALMConstants.INDIVIDUAL_LIST %></p>
			 <div class="individualfirms-first" id="<portlet:namespace/>individualFirmsCounter"> 
				<input type="checkbox" value="Abrams" id="firmCounter">
                 Firms (0 Selected)<br />
              </div>
			<!-- <div style="margin-left:15px;background-color:#FFFFFF;" id="allOtherFirmDiv"> -->	
			<div id="<portlet:namespace/>allOtherFirmDiv" style="background-color:#FFFFFF;" class="Select-Individual-Firms">
			<c:forEach var="firm"  items="${allFirms}">    
				<form:checkbox  class="allFirmsCheckBox" id="allFirmsCheckBoxCounter" path="firmList" value="${firm.companyId}" labelAttr="${firm.company}"/>${firm.company}<br>
			 </c:forEach>
				</div>
					<div class="popupsubmit">	
							<input type="button" class="buttonOne" value="Apply" id="ApplyFirm">
							<input type="button" class="buttonTwo" value="Clear All" id="clearButton">
						</div>
					
					</div>
				
				</div>
			<c:if test="${not empty allWatchListsDefaultList}">
	    		<input type="hidden" name="defaultWatchListId" id="defaultWatchListId" value="${allWatchListsDefaultList.groupId}" />
	   		</c:if>
	 		</li>
	 					
	 		<li>
	        <label>Location(s)</label>
						  <div class="srchBox">
							<%-- <input type="text"  value="<%=ALMConstants.ALL_LOCATIONS %>" class="input" name="selectedLocation" id="selectedLocation"  /> --%>
							<input type="text" class="input" name="selectedLocation" id="selectedLocation"  style="text-overflow:ellipsis;" autocomplete="off"/> 
							<input type="button" name="search" value="" class="srchBack" />
							<div class="clear">&nbsp;</div>
						  </div>
						 <input type="button" name="search" value=""  id="locationId" class="typeSel" />
						<div class="rel">	 
							<div id="<portlet:namespace/>popupLocation" class="firmPage">
								<p><form:radiobutton path="locations"  id="allLocations" value="<%=ALMConstants.ALL_LOCATIONS%>" /> <%=ALMConstants.ALL_LOCATIONS %></p>	
								<p><input type="radio"  id="allOtherLocations" value="Select Individual Locations" /> Select Individual Locations</p>
								<div class="individualfirms-first" id="<portlet:namespace/>locationsCounter"> 
									<input type="checkbox" value="Abrams"> Locations (0) Selected<br> </div>
								<div style="background-color:#FFFFFF;" id="<portlet:namespace/>allOtherLocationsDiv" class="Select-Individual-Firms">		
								<c:forEach items="${allOtherLocations}" var="loc">
									<form:checkbox path="locations" class="allLocationsCheckBox" value="${loc}"/>&nbsp;${loc}<br>
								</c:forEach>	   
								</div>
								<div class="popupsubmit">				
								<input type="button" class="buttonOne" value="Apply" id="ApplyLocations">
								<input type="button" class="buttonTwo" value="Clear All" id="clearButtonLocations">
								</div>
							</div>
						</div>
				 </li>
				 
	               <li>
						  <label>Practice Area(s)</label>
							  <div class="srchBox">
								<input type="text"  class="input" name="selectedPracticeArea" id="selectedPracticeArea" style="text-overflow:ellipsis;" autocomplete="off"/>
								<input type="button" name="search" value="" class="srchBack" />
								<div class="clear">&nbsp;</div>
							  </div>
							 <input type="button" name="search" value=""  id="practiceAreaId" class="typeSel" />
							<div class="rel">
								  <div  id="<portlet:namespace/>popupPracticeArea" class="firmPage">					
										
									<p><form:radiobutton path="practiceArea" class="allPracticeAreaFirmState" id="allPracticeAreaFirmStateId" value="<%=ALMConstants.ALL_PRACTICE_AREAS %>" />&nbsp;<%=ALMConstants.ALL_PRACTICE_AREAS%></p>
							   		<p> <input  type="radio"  name="allPracticeArea"  id="allPracticeAreaIndividual" value="Select Individual Practice Areas"> Select Individual Practice Areas</p>
										<div class="individualfirms-first" id="<portlet:namespace/>practiceAreaCounter"> <input type="checkbox"  value="Practice" id="practiceSelection"> Practice Areas (0 Selected)<br> </div>
									  <div style="background-color:#FFFFFF;" id="<portlet:namespace/>practiceAreaDiv" class="Select-Individual-Firms">										 
											 <div style="background-color:#FFFFFF;" id="<portlet:namespace/>allOtherPracticeArea">
											    <c:forEach var="practice" items="${allPracticeArea}">								
													<form:checkbox path="practiceArea" value="${practice.practiceArea}"/> ${practice.practiceArea}<br>
												</c:forEach>
											 </div>	
										</div>
									<div class="popupsubmit">
									 <input type="button" class="buttonOne" value="Apply" id="ApplyPracticeArea" />
									 <input type="button" class="buttonTwo" value="Clear All" id="clearPracticeArea" />
									</div>	
									
										
							</div>
							</div>
					</li>
			
		        <li>
		        <label>Date(s)</label>
		        <div class="srchBox">
		          <%--<input name="Datetext" id="datetext" value=""  readonly="readonly" class="input"  style="width:140px"/>   --%>
		          <form:input path="dateText" id="dateText" readonly="true" cssClass="input" cssStyle="width:140px" autocomplete="off"/>
		          <div class="clear">&nbsp;</div>
		        </div>
		        <input type="button" name="search" id="datenone" value="" class="drpDwn" />
		        <div class="rel">
		          <div id="<portlet:namespace/>popupDate" class="datePage">
		            <input type="radio" name="date" id="period"  />&nbsp;Period<br>
		            <select id="datePeriodSelect" class="marlt4">
		              <option value="Any">Any</option>
		              <option value="Last Week">Last Week</option>
		              <option value="Last 30 Days">Last 30 Days</option>
		              <option value="Last 90 Days">Last 90 Days</option>
		              <option value="Last 6 Months">Last 6 Months</option>
		              <option value="Last Year">Last Year</option>
		            </select>
		            <br>
		            <input type="radio" name="date"  id="dateRange"  checked="checked"/>
		            &nbsp;Date Range<br>
		            <div class="flLeft marlt4">
		              <label for="from">From</label>
		              <input type="text" id="from" name="from"  />
		            </div>
		            <div class="flLeft marlt3">
		              <label for="to">To</label>
		              <input type="text" id="to" name="to" />
		            </div>
		            <div class="clear"></div>
		            <span class="error" id="fromdateError"> Invalid  From Date</span> <span class="error" id="todateError"> Invalid  To Date</span> <span class="error" id="dateValidError"> To date should be more than from date</span>
		            <div class="popupsubmit Popup-Calendar">
		              <input type="button" class="buttonOne"  value="Apply" id="ApplyDate">
		              <input type="button" class="buttonTwo"  value="Clear All" id="clearButtonDate">
		            </div>
		          </div>
		        </div>
		      </li>
	     	
	     	<!-- Moved Firm Size from additional filter to first row of search -->
	      <li>
				  <label>Firm Size</label>
				  <div class="srchBox">
					<input type="text" class="input" name="selectedFirmSize" id="selectedFirmSize"  style="text-overflow:ellipsis;" autocomplete="off"/>
					<input type="button" name="search" value="" class="srchBack" />
					<div class="clear">&nbsp;</div>
				  </div>
				 <input type="button" name="search" value=""  id="firmSizeId" class="drpDwn" />
			<div class="rel">	 
				<div  id="<portlet:namespace/>popupFirmSize" class="firmPage">
					<p><form:radiobutton path="firmSizeList" value="<%=ALMConstants.ALL_FIRM_SIZES%>"  id="allFirmSize" />&nbsp;<%=ALMConstants.ALL_FIRM_SIZES %></p>	
					
					<p><input type="radio" name="firmSize" value="Select Firm Sizes" id="allOtherFirmSize">&nbsp;Select Firm Sizes</p>
					 <div class="individualfirms-first" id="<portlet:namespace/>firmSizeCounter">
                		<input type="checkbox" value="Abrams" /> Firms Size (0 Selected)<br />
              		</div>
					<div id="<portlet:namespace/>allOtherFirmSizeDiv" class="Select-Individual-Firms" style="border: 1px solid #ccc;">			      
		      		 
		      		  <c:forEach var="firmSizeVal" items="${allFirmSize}">								
							<form:checkbox path="firmSizeList" value="${firmSizeVal}"/> ${firmSizeVal}<br>
					  </c:forEach>
					</div>	
					<div class="popupsubmit">				
						<input type="button" class="buttonOne" value="Apply" id="ApplyFirmSize">
						<input type="button" class="buttonTwo" value="Clear All" id="clearButtonFirmSize">
					</div>
				</div>			  
			</div>
		</li>
	        <li class="Submit-Filter-Criteria">
	        <label>&nbsp;</label>
	        <input type="button" value="Apply" class="buttonOne" style="background:url(<%=renderRequest.getContextPath()%>/images/btn1.png) 0 0 repeat-x; border:1px solid #bf8d1f; -webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px; font:normal 12px Arial, Helvetica, sans-serif; color:#fff; padding:5px 10px; cursor:pointer; text-shadow: 0px 0px #FFF;" id="applySearch"/>
	      </li>
	      <li>
	        <label>&nbsp;</label>
	        <input type="button" value="Reset All" style="background:url(<%=renderRequest.getContextPath()%>/images/btn2.png) 0 0 repeat-x; border:1px solid #565656; -webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px; font:normal 12px Arial, Helvetica, sans-serif; color:#fff; padding:5px 10px; cursor:pointer; text-shadow: 0px 0px #FFF;" class="buttonTwo" id="resetAll" />
	      </li>
		   <li>
	        <label>&nbsp;</label>
	        <input type="hidden"  name="selectedFirms" id="selectedFirms"  title="" customAttr="" />
	      </li>
	      </ul>
	      <div class="clear">&nbsp;</div>
	    </div>
	       
	       <%--Remove this Div when Addition Filter div is enabled, because this div will add additional space which is not desired when the Additional filter is restored back --%>
	       <div style="height: 30px;">
	       </div>
	      
	    <%-- View Settings  Start--%>
	       <div style="display: none; position:absolute; margin:0; padding:0; background:#F0F0F0; border:none; width:673px !important;" class="viewBox popusdiv ClickPopup newspublicationPage charts" id="<portlet:namespace/>viewSettingsPopup">
						<div class="popHeader">
							<a href="javascript:void(0);" class="btn icon closewhite closeOne flRight" style="margin-top: 2px" onClick="<portlet:namespace/>cancelViewSettings();">&nbsp;Close</a>
								SETTINGS: Firm Statistics
						</div>
	                    <div class="Auto-Styling">
                            <h5>Column Display</h5>
                            <div class="clear"></div>
                            <div style="width:150px; float:left;">
                              <h6>Attorneys</h6>
                                <ul class="reset list4">
                                <li><input type="checkbox" class="mart1" name="allGroupsAttorneys" id="allGroupsAttorneys"  checked="checked"/><span>All Groups</span>
                                  <ul class="reset list5 marlt3" id="<portlet:namespace/>allOtherAttorney">
                                      <c:forEach var="dis_column" items="${allDisplayColumns}">
                                      <li>
                                      <%--
                                      <form:checkbox cssClass="mart1" path="displayColumns" value="${dis_column.value}"  id="${dis_column.value}"/> ${dis_column.displayValue}<br>
                                       --%>
                                       <form:checkbox cssClass="mart1" path="displayColumns" value="${dis_column.value}" /> ${dis_column.displayValue}<br>
                                      <li>
                                      </c:forEach>
                                  </ul>
                                </li>
                                </ul>
                          
                                <div class="clear">&nbsp;</div>
                            </div>
                            <div style="width:200px; float:left;">
                                <h6>Moves</h6>
                                <ul class="reset list4">
                                    <li><input type="checkbox" class="mart1" id="peopleMove"  checked="checked" /><span>People Moves</span>
                                        <ul class="reset list5 marlt3">
                                                <li>
                                                <form:checkbox path="moves" cssClass="mart1 flLeft" cssStyle="margin-top:3px" id="added"  value="<%=ALMConstants.ADDITIONS %>" />
                                                <span class="btn icon addpositive flLeft">Added to Directory</span>
                                                </li>
                                                <li>
                                                <form:checkbox path="moves" id="removed"  cssClass="mart1 flLeft" cssStyle="margin-top:3px" value="<%=ALMConstants.REMOVALS %>"/>
                                                <span class="btn icon subtractneg flLeft">Removed from Directory</span>
                                                </li>
                                        </ul>
                                    </li>
                                </ul>
                                <h6>Group By</h6>
                                <%--orderBy Values remain negative here as GroupBy is handled in code --%>
                                <%--TODO check with Gautam on how it exactly works --%>
                                <ul class="reset list4">
                                        <li><form:radiobutton id="titleGroup" path="orderBy" value="<%=PeopleSearchModelBean.GROUPBY_TITLE %>"/><span>Title</span></li>
                                        <li><form:radiobutton id="headCountGroup"  path="orderBy" value="<%=PeopleSearchModelBean.GROUPBY_HEAD_COUNT %>"/><span>Headcount and Changes</span></li>
                                </ul>
                                <div class="clear">&nbsp;</div>
                            </div>
                        </div>
                        <div class="FS-Vertical-Border"></div>
						<div class="Auto-Styling">
                            <h5>Comparison Data</h5>
                            <div class="clear"></div>
                            <div class="section-one">
                                <ul class="reset list4">
                                        <li>
                                        
                                        <c:choose>
                                    <c:when test="${doIHaveFirm}">
                                        <form:checkbox cssClass="mart1" path="comparisonDataTypeList" id="myfirm"
                                            value="<%=BaseChartModelBean.ComparisonDataType.MY_FIRM.getValue()%>" />
                                        <span>My Firm</span>
                                    </c:when>
                                    <c:otherwise>
                                        <form:checkbox cssClass="mart1" path="comparisonDataTypeList" id="myfirm" disabled="true"
                                            value="<%=BaseChartModelBean.ComparisonDataType.MY_FIRM.getValue()%>" />
                                        <span title="You are not associated with any firm.">My Firm</span>
                                    </c:otherwise>
                                </c:choose>
                                        
                                        </li>
                                        <li>
                                        
                                        <form:checkbox cssClass="mart1" path="comparisonDataTypeList" 
                                    value="<%=BaseChartModelBean.ComparisonDataType.RIVAL_EDGE.getValue()%>"  id="rivalEdge"/>
                                <form:select cssStyle="width:120px" path="firmList" multiple="false" id="rivaledgeSelect">
                                    <form:options items="${allRankingList}" />
                                </form:select>
                                        
                                       
                                        </li>
                                        <li>
                                        
                                        <c:choose>
                                    <c:when test="${not empty allWatchLists}">
                                        <form:checkbox cssClass="mart1" path="comparisonDataTypeList" id="watchlist" 
                                            value="<%=BaseChartModelBean.ComparisonDataType.WATCHLIST_AVG.getValue()%>" />
                                        <span class="mart1">Watchlist</span>
                                        <form:select  path="watchList" cssStyle="width:110px" multiple="false">
                                            <form:options items="${allWatchLists}" itemValue="groupId"	itemLabel="groupName" />
                                        </form:select>
                                    </c:when>
                                    <c:otherwise>
                                        <form:checkbox cssClass="mart1" path="comparisonDataTypeList" id="watchlist" disabled="true"
                                            value="<%=BaseChartModelBean.ComparisonDataType.WATCHLIST_AVG.getValue()%>" />
                                        <span title="You do not have any watchlists set up.">Watchlist</span>
                                    </c:otherwise>
                                </c:choose>
                                        
                                       
                                        </li>
                                        <li>
                                        
                                        <form:checkbox cssClass="mart1" path="comparisonDataTypeList" id="average"	value="<%=BaseChartModelBean.ComparisonDataType.AVERAGE.getValue()%>" />
                                <span>Average of Firms in Search</span>
                                
                                        </li>
                                </ul>
                                <div class="clear">&nbsp;</div>
                            </div>
	                    <div style="width:132px; float:left; clear:both;">
	                    	<h6>Display No. of Results</h6>
							<ul class="reset list4">
									<li><form:radiobutton path="searchResultsPerPage"  value="25"  id="twentyFiveRecords"/><span>25</span></li>
	                                <li><form:radiobutton path="searchResultsPerPage"  value="50"  id="fiftyRecords"/><span>50</span></li>
	                                <li><form:radiobutton path="searchResultsPerPage"  value="100" id="hundredRecords"/><span>100</span></li>
	                                <li><form:radiobutton path="searchResultsPerPage"  value="500" id="fiveHundredRecords"/><span>500</span></li>
							</ul>
	                        <div class="clear">&nbsp;</div>
	                    </div>
                        </div>
						<div class="clear">&nbsp;</div>
						<hr>
                        <div class="btmdiv" style="margin:0; border:none;">
							<input type="button" value="Reset All" class="buttonTwo flLeft settingReset" id="resetAllViewSetting" onclick="<portlet:namespace/>resetViewSettings();" />
							<input type="button" class="buttonTwo flRight rightReset" value="Cancel" onclick="<portlet:namespace/>cancelViewSettings();" >
							<input type="button" value="Apply" class="buttonOne flRight" style="margin: 0 5px 0 0;" id="applyViewSetting" onclick="<portlet:namespace/>applyViewSettings();" />
							<div class="clear">&nbsp;</div>
						</div>
					</div>
	    
	    <%--View Settings End --%>
	</form:form>
</div>
  
<script>
	// Omniture SiteCatalyst - START
	s.prop22 = "premium";
	s.pageName="rer:people-analysis-search";
	s.channel="rer:people-analysis";
	s.server="rer";
	s.prop1="people-analysis"
	s.prop2="people-analysis";
	s.prop3="people-analysis";
	s.prop4="people-analysis";
	s.prop21=s.eVar21='current.user@foo.bar';	
	s.events="event2";
	s.events="event27";
	
	s.t();
	// Omniture SiteCatalyst - END
</script> 
  
      