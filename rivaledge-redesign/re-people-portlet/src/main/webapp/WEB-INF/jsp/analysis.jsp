<%@page import="com.alm.rivaledge.transferobject.FirmResultDTO"%>
<%@page import="java.util.Set"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<portlet:defineObjects/>

<portlet:resourceURL var="applySearchUrl" id="applySearchUrl" />
<!-- 
<script src="<%=resourceRequest.getContextPath()%>/js/s_code.js"></script>
 -->
<script type="text/javascript">
$(function() {	

	
	// Omniture SiteCatalyst - START
	s.prop22 = "premium";
	s.pageName="rer:people-analysis";
	s.channel="rer:people-analysis";
	s.server="rer";
	s.prop1="people-analysis"
	s.prop2="people-analysis";
	s.prop3="people-analysis";
	s.prop4="people-analysis";
	s.prop21=s.eVar21='current.user@foo.bar';	
	s.events="event2";
	s.events="event27";
	s.t();
	// Omniture SiteCatalyst - END	
	
	var firmNameList = []; // List of selected firms names.
	var plusParnterList = []; // List of plus partner count.
	var minusParnterList = []; // List of minus partner count.
	var plusAssociateList = []; // List of plus associate count.
	var minusAssociateList = []; // List of minus associate count.
	var plusOthersList = []; // List of plus Other Counsel count.
	var minusOthersList = []; // List of minus Other Counsel count.
	
	// Get value of search result from session.
	var jsonData = '${peopleSearchResultsJSON}';
	
	$("#jsonResult").val(jsonData);
	// Parse json result into to javascript variable. 
	var jsonResult = $.parseJSON($("#jsonResult").val());

	// Put all the count into list.
	for(var index=0; index<jsonResult.length; index++){
		firmNameList.push(jsonResult[index].firmName);
		plusParnterList.push(jsonResult[index].partnerPlus);
		minusParnterList.push(jsonResult[index].partnerMinus);
		plusAssociateList.push(jsonResult[index].associatePlus);
		minusAssociateList.push(jsonResult[index].associateMinus);
		plusOthersList.push(jsonResult[index].otherConselPlus);
		minusOthersList.push(jsonResult[index].otherConselMinus);
	}
	
	// Call stack graph function on load.
	displayNetStackGraph(firmNameList, plusParnterList, minusParnterList, plusAssociateList, minusAssociateList, plusOthersList, minusOthersList, 'column', 50, "leftChart");
	// Call percentage stack graph function on load. 
	displayNetStackGraphPerc(firmNameList, plusParnterList, minusParnterList, plusAssociateList, minusAssociateList, plusOthersList, minusOthersList, 'column', 50, "rightChart");
	
	/* On click of apply button of right side chart view and setting option popup.
	 * It will check for type of chart and call perticular function.
	 *
	 */
	$("#rightChartSettingApply").click(function(){
		$("#login-box1").hide();
		if($('input[name=graphType2]:radio:checked').val() == 'stackVertical')
		{
			// If Vertical Stack graph is selected.
			displayNetStackGraphPerc(firmNameList, plusParnterList, minusParnterList, plusAssociateList, minusAssociateList, plusOthersList, minusOthersList, 'column', 50, "rightChart");
		} else if($('input[name=graphType2]:radio:checked').val() == 'stackHorizontal')
		{
			// If Horizontal Stack graph is selected.
			displayNetStackGraphPerc(firmNameList, plusParnterList, minusParnterList, plusAssociateList, minusAssociateList, plusOthersList, minusOthersList, 'bar', 0, "rightChart");
		} else if($('input[name=graphType2]:radio:checked').val() == 'verticalBar')
		{
			// If Vertical Bar (column) graph is selected.
			displayBarGraphPerc(firmNameList, plusParnterList, minusParnterList, plusAssociateList, minusAssociateList, plusOthersList, minusOthersList, 'column', 50, "rightChart");
		} else if($('input[name=graphType2]:radio:checked').val() == 'horizontalBar')
		{
			// If Horizontal Bar (column) graph is selected.
			displayBarGraphPerc(firmNameList, plusParnterList, minusParnterList, plusAssociateList, minusAssociateList, plusOthersList, minusOthersList, 'bar', 50, "rightChart");
		} else 
		{
			// If no graph is selected then it will display default vertical stack graph.
			displayNetStackGraphPerc(firmNameList, plusParnterList, minusParnterList, plusAssociateList, minusAssociateList, plusOthersList, minusOthersList, 'column', 50, "rightChart");
		}
	});
	
	/* On click of apply button of left side chart view and setting option popup.
	 * It will check for type of chart and call perticular function.
	 *
	 */
	$("#leftChartSettingApply").click(function(){
				
		if($('input[name=graphType]:radio:checked').val() == 'stackVertical')
		{
			// If Vertical Stack graph is selected.
			displayNetStackGraph(firmNameList, plusParnterList, minusParnterList, plusAssociateList, minusAssociateList, plusOthersList, minusOthersList, 'column', 50, "leftChart");
		} else if($('input[name=graphType]:radio:checked').val() == 'stackHorizontal')
		{
			// If Horizontal Stack graph is selected.
			displayNetStackGraph(firmNameList, plusParnterList, minusParnterList, plusAssociateList, minusAssociateList, plusOthersList, minusOthersList, 'bar', 0, "leftChart");
		} else if($('input[name=graphType]:radio:checked').val() == 'verticalBar')
		{
			// If Vertical Bar (column) graph is selected.
			displayBarGraph(firmNameList, plusParnterList, minusParnterList, plusAssociateList, minusAssociateList, plusOthersList, minusOthersList, 'column', 50, "leftChart");
		} else if($('input[name=graphType]:radio:checked').val() == 'horizontalBar')
		{
			// If Horizontal Bar (column) graph is selected.
			displayBarGraph(firmNameList, plusParnterList, minusParnterList, plusAssociateList, minusAssociateList, plusOthersList, minusOthersList, 'bar', 0, "leftChart");
		} else 
		{
			// If no graph is selected then it will display default vertical stack graph.
			displayNetStackGraph(firmNameList, plusParnterList, minusParnterList, plusAssociateList, minusAssociateList, plusOthersList, minusOthersList, 'column', 50, "leftChart");
		}
	});
	

	// HTML code from Rahul N.
	$('#btnAdd').click(function(){
		$('.filtersPage').hide();
		$('#additional').show();
	})
	$('#btnSave').click(function(){
		$('.filtersPage').show();
		$('#additional').hide();
	});
	
	$('#hide').click(
    function () {
        //show its submenu
        $("#firmPage").stop().slideToggle(500);    
    });
	
	$('#datenone').click(
    function () {
        //show its submenu
        $("#datePage").stop().slideToggle(500);    
    });
	
	
	
	
	//Hide-Show Add Analysis Pop-up
	$('.mybutton').hover(function(){
		$('.selectchartadd').show();
	}, function(){
		$('.selectchartadd').hide();
	});
	$('.selectchartadd').hover(function(){
		$('.selectchartadd').show();
	}, function(){
		$('.selectchartadd').hide();
	});

	
	
	// HTML code from Rahul N. ends here
	
	
	
	$(".netChangeOfAttorney").click(function(){
		if($(".leftDetails").css('display') == 'none'){
			$(".leftDetails").show();
			if($(".rightDetails").css('display') == 'none'){
				$(".leftDetails").css('float', 'left');
			} else {
				$(".leftDetails").css('float', 'right');
			}
		} else {
			$(".leftDetails").hide();
			$(".rightDetails").css('float', 'left');
		}
	});


	$(".percChangeOfAttorney").click(function(){
		if($(".rightDetails").css('display') == 'none'){
			$(".rightDetails").show();
			if($(".leftDetails").css('display') == 'none'){
				$(".rightDetails").css('float', 'left');
			} else {
				$(".rightDetails").css('float', 'right');
			}
		} else {
			$(".rightDetails").hide();
			$(".leftDetails").css('float', 'left');
		}
	});
	
	
	$("#login-box1").hide();
	$(".rightViewSetting").mouseover(function(){
		$("#login-box1").show();
	});
	
	// If user select 2 column radio button then display in two columns else in single column.
	$("#viewSettingColumnApply").click(function(){

		if($('input[name=columnType]:radio:checked').val() == 'one_column'){
			$('.rightDetails').css('float', 'none');
			$(".leftDetails").css('float', 'none');
			$(".dropdownarrow").css('float', 'none');
		} else {
			$('.rightDetails').css('float', 'right');
			$(".leftDetails").css('float', 'left');
			$(".dropdownarrow").css('float', 'right');
		}
	});
	
	// When user clicks reset all button default graph will be shown. 
	$(".settingReset").click(function(){
		$('input[name=graphType]:radio').attr('checked', false);
		$('input[name=sorting]:radio').attr('checked', false);
		$('.myFirm').attr('checked', true);
		$(".amLaw100AVg").attr('checked', true);
		
		displayStackGraph(firmNameList, dataTypeList, 'column', 50, "leftChart");
		
	});
	
	$("div#hoverOverTable").hide();
	$(".rightDetails").hide();
	
	// Add firms name in popup dropdown option of view and settings.
	var selectedFirms = $("<select multiple='multiple' class='popupSelectedFirms'/>");
	for(var index=0; index<firmNameList.length; index++){
		$('<option/>', {value: firmNameList[index], text: firmNameList[index]}).appendTo(selectedFirms);
	}
	$("#selected_firms").html(selectedFirms);
	var selectedFirms = $("<select multiple='multiple' class='popupSelectedFirms2'/>");
	for(var index=0; index<firmNameList.length; index++){
		$('<option/>', {value: firmNameList[index], text: firmNameList[index]}).appendTo(selectedFirms);
	}
	$("#selected_firms2").html(selectedFirms);
	
	
	$(".printLeftGraph").click(function(){
		Popup("left");
	});
	$(".printRightGraph").click(function(){
		Popup("right");
	});
	
	/*
	 * If user selects selected firms radio button then 
	 * enable dropdown list otherwise disable.
	 *
	 */
	$(".sorting").click(function(){
		if($('input[name=sorting]:radio:checked').val() == 'selected_firm'){
			$(".popupSelectedFirms").removeAttr("disabled");
		} else {			
			$(".popupSelectedFirms").attr("disabled", "disabled");
			$(".popupSelectedFirms > option:selected").each(function(){
				$(this).removeAttr("selected");
			});
		}
	});
	/*
	 * If user selects selected firms radio button then 
	 * enable dropdown list otherwise disable.
	 *
	 */
	$(".sorting2").click(function(){
		if($('input[name=sorting2]:radio:checked').val() == 'selected_firm_2'){
			$(".popupSelectedFirms2").removeAttr("disabled");
		} else {			
			$(".popupSelectedFirms2").attr("disabled", "disabled");
			$(".popupSelectedFirms2 > option:selected").each(function(){
				$(this).removeAttr("selected");
			});
		}
	})
	$(".popupSelectedFirms").attr("disabled", "disabled");
	$(".popupSelectedFirms2").attr("disabled", "disabled");
	$(document).click(function() {
		$("div#hoverOverTable").hide();
		$("div#percentageHoverOver").hide();
	});
	
});

// Print element option.
function PrintElem(elem)
{
    Popup($(elem).html());
}

// New popup will be shown for display print option.
function Popup(position) 
{
    var mywindow = window.open('', 'mydiv', 'height=400,width=600');
    mywindow.document.write('<html><head><title>my div</title>');
    mywindow.document.write('</head><body >');
    if(position == 'left'){
    	mywindow.document.write($("#leftChart").html());
    } else {
    	mywindow.document.write($("#rightChart").html());
    }
    
    mywindow.document.write('</body></html>');
    mywindow.print();
    mywindow.close();  

    return true;
}

/*
 * Display stack graph for net value.
 */
function displayNetStackGraph(firmNameList, plusParnterList, minusParnterList, plusAssociateList, minusAssociateList, plusOthersList, minusOthersList, type, y, chartPosition){
	
	
	
	var partnerList = [];
	var associateList = [];
	var otherList = [];
	var totalCount = [];
	
	var firmNamesList = [];
	var checkboxCount = 0;
	
	/*
	 * If any checkbox on view and setting popup is checked then value will be shown in graph otherwise not.
	 *
	 */
	
	if($(".myFirm").is(":checked")){
		firmNamesList.push("My Firm");
		partnerList.push(10);
		associateList.push(10);
		otherList.push(10);
		totalCount.push(30);
		checkboxCount++;
	} 
	
	if($(".amLaw100").is(":checked")){
		firmNamesList.push("AmLaw100");
		partnerList.push(10);
		associateList.push(10);
		otherList.push(10);
		totalCount.push(30);
		checkboxCount++;
	}
	
	if($(".watchListAvg").is(":checked")){
		firmNamesList.push("WathList Average");
		partnerList.push(10);
		associateList.push(10);
		otherList.push(10);
		totalCount.push(30);
		checkboxCount++;
	}
	
	if($(".avgFirmSearch").is(":checked")){
		firmNamesList.push("Average Firm Search");
		partnerList.push(10);
		associateList.push(10);
		otherList.push(10);
		totalCount.push(30);
		checkboxCount++;
	}
	
	var selectedFirmList = [];
	var indexList = [];

	// Push selected firms name in list.
	$(".popupSelectedFirms > option:selected").each(function(){
		selectedFirmList.push($(this).text());
	});
	
	// Find out position of selected firms name index from original list and put in index list.
	if(selectedFirmList.length > 0){
		for(var index=0; index<firmNameList.length; index++){
			if(firmNameList.indexOf(selectedFirmList[index]) >= 0){
				indexList.push(firmNameList.indexOf(selectedFirmList[index]));
			}
		}
	}
	
	var limit = 0;
	if(plusParnterList.length < 10){
		limit = plusParnterList.length;
	} else {
		limit = 10;
	}
	// If user has selected firms then graph will be generated only for selected firms otherwise for all.
	if(indexList.length > 0){
		for(var index=0; index<indexList.length; index++){
			firmNamesList.push(firmNameList[indexList[index]]);
			partnerList.push(parseInt(plusParnterList[indexList[index]])-parseInt(minusParnterList[indexList[index]]));
			associateList.push(parseInt(plusAssociateList[indexList[index]])-parseInt(minusAssociateList[indexList[index]]));
			otherList.push(parseInt(plusOthersList[indexList[index]])-parseInt(minusOthersList[indexList[index]]));
			totalCount.push(parseInt(plusParnterList[indexList[index]])-parseInt(minusParnterList[indexList[index]])+parseInt(plusAssociateList[indexList[index]])-parseInt(minusAssociateList[indexList[index]])+parseInt(plusOthersList[indexList[index]])-parseInt(minusOthersList[indexList[index]]));
		}
	} else {
		for(var index=0; index<limit; index++){
			
			firmNamesList.push(firmNameList[index]);
			partnerList.push(parseInt(plusParnterList[index])-parseInt(minusParnterList[index]));
			associateList.push(parseInt(plusAssociateList[index])-parseInt(minusAssociateList[index]));
			otherList.push(parseInt(plusOthersList[index])-parseInt(minusOthersList[index]));
			totalCount.push(parseInt(plusParnterList[index])-parseInt(minusParnterList[index])+parseInt(plusAssociateList[index])-parseInt(minusAssociateList[index])+parseInt(plusOthersList[index])-parseInt(minusOthersList[index]));
		}
	}
	
	// Whether user select top 10 or bottom 10 sort data in descending or ascending order.
	if($('input[name=sorting]:radio:checked').val() == 'top_ten'){
		sorting(firmNameList, partnerList, associateList, otherList, totalCount,  partnerList.length, 0);
	}

	if($('input[name=sorting]:radio:checked').val() == 'bottom_ten'){
		sorting(firmNameList, partnerList, associateList, otherList, totalCount, partnerList.length, 1);
	}
	
	// Sorting data based on total count.
	function sorting(firmNameList, partnerList, associateList, otherList, countList, n, m){
		
		for(var x=2; x<n; x++){
			var index_of_min = x;
			for(var y=x; y<n; y++){
				if(parseInt(m)==0){
					if(countList[index_of_min]<countList[y]){
				  		index_of_min = y;
					}
				} else {
					if(countList[index_of_min]>countList[y]){
				  		index_of_min = y;
					}
				}				
			}
			var temp = countList[x];
			countList[x] = countList[index_of_min];
			countList[index_of_min] = temp;
			
			var temp1 = firmNamesList[x];
			firmNamesList[x] = firmNamesList[index_of_min];
			firmNamesList[index_of_min] = temp1;

			var temp = partnerList[x];
			partnerList[x] = partnerList[index_of_min];
			partnerList[index_of_min] = temp;
			
			var temp1 = associateList[x];
			associateList[x] = associateList[index_of_min];
			associateList[index_of_min] = temp1;
			
			var temp1 = otherList[x];
			otherList[x] = otherList[index_of_min];
			otherList[index_of_min] = temp1;
		}
	}
	
	var selectedFirmsResult = "";
	for(var index=0; index<firmNamesList.length; index++){
		selectedFirmsResult += firmNameList[index] + ",";
	}
	$("#selectedFirmsResult").text(selectedFirmsResult);
	
	// Find and initialise position of chart.
	if(chartPosition == "leftChart"){
		chartPosition = '#leftChart';
	} else {
		chartPosition = '#rightChart';
	}
	
	
	// Generate graph.
	$(chartPosition).highcharts({
        chart: {
            type: type
        },
        title: {
            text: 'NET CHANGE OF ATTORNEYS'
        },
        xAxis: {
            categories: firmNamesList,
            labels: {
                rotation: -45,
                y: y,
			},
			plotLines: [{
                color: '#000000',
                width: 2,
                dashStyle: 'Solid',
                value: parseFloat(checkboxCount-0.5)
            }]
        },
        yAxis: {
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
        	enabled: true
    	},
    	tooltip: {
            formatter: function() {
            	displayPopUpNetStackGraph(firmNameList, plusParnterList, minusParnterList, plusAssociateList, minusAssociateList, plusOthersList, minusOthersList, this.x, this.y);
                return false;
            }
        },
        plotOptions: {
            series: {
            	stacking: 'normal',
            	dataLabels: {
                    enabled: true,
                    color: '#FFFFFF'
                },
                point: {
                    events: {
                        mouseOver: function(e) {
							
                        	var xPos = e.target.plotX + 40;
                        	var yPos = e.target.plotY + 330;
                        	
                       	 	$("div#hoverOverTable").css('top', yPos).css('left', xPos).css('z-index', 99).css('width', '315px');
                            $("div#hoverOverTable").show();
                        }
                    }
                    
                }
            }
        },
        series: [{
            name: 'Partner',
            color: '#446e49',
            data: partnerList
        }, {
            name: 'Associate',
            color: '#0c3362',
            data: associateList
        }, {
            name: 'Other Counsel',
            color: '#b89024',
            data: otherList
        }]
    });
}

/*
 * displayPopuNetStackGraph function will be call when user mouse over stack value.
 * This function will generate popup which will show factor of values for that firm.
 */
function displayPopUpNetStackGraph(firmNameList, plusParnterList, minusParnterList, plusAssociateList, minusAssociateList, plusOthersList, minusOthersList, x, y){
	var partnerList = [];
	var associateList = [];
	var otherList = [];
	
	
	
	for(var index=0; index<plusParnterList.length; index++){
		partnerList.push(parseInt(plusParnterList[index])-parseInt(minusParnterList[index]));
		associateList.push(parseInt(plusAssociateList[index])-parseInt(minusAssociateList[index]));
		otherList.push(parseInt(plusOthersList[index])-parseInt(minusOthersList[index]));
	}
	
	
	var table = "<b>"+x+"-Change of Attorneys</b><br><table>";
	for(var index=0; index<firmNameList.length; index++){
		if(x == firmNameList[index]){
			
			var addedAttorney = parseInt(plusParnterList[index])+parseInt(plusAssociateList[index])+parseInt(plusOthersList[index]);
			var removedAttorney = parseInt(minusParnterList[index]) + parseInt(minusAssociateList[index]) + parseInt(minusOthersList[index]);
			var netAllAttorney = addedAttorney - removedAttorney;
			table += '<div class="table"><div class="row"><span class="column1">&nbsp;</span><span class="column2">Net</span><span class="column3">Added</span><span class="column4">Removed</span></div><div class="row  back-color-row2"><span class="column1">All Attorney</span><span class="column2">'+netAllAttorney+'</span><span class="column3">'+addedAttorney+'</span><span class="column4">'+removedAttorney+'</span></div><div class="row   back-color-row3"><span class="column1">Partners</span><span class="column2">'+(parseInt(plusParnterList[index])-parseInt(minusParnterList[index]))+'</span><span class="column3">'+parseInt(plusParnterList[index])+'</span><span class="column4">'+parseInt(minusParnterList[index])+'</span></div><div class="row   back-color-row4"><span class="column1">Associates</span><span class="column2">'+(parseInt(plusAssociateList[index])-parseInt(minusAssociateList[index]))+'</span><span class="column3">'+parseInt(plusAssociateList[index])+'</span><span class="column4">'+parseInt(minusAssociateList[index])+'</span></div><div class="row   back-color-row5"><span class="column1">Other Counsel</span><span class="column2">'+(parseInt(plusOthersList[index])-parseInt(minusOthersList[index]))+'</span><span class="column3">'+parseInt(plusOthersList[index])+'</span><span class="column4">'+parseInt(minusOthersList[index])+'</span></div></div>'
		}
	}
	table += "</table>";
	$("#hoverOver").html(table);
	
}

/*
 * displayBarGraph function will generate bar (column) graph.
 */
function displayBarGraph(firmNameList, plusParnterList, minusParnterList, plusAssociateList, minusAssociateList, plusOthersList, minusOthersList, type, y, chartPosition){
	
	var partnerList = [];
	var associateList = [];
	var otherList = [];
	var totalCount = [];
	var firmNamesList = [];
	var checkboxCount = 0;
	
	/*
	 * If any checkbox on view and setting popup is checked then value will be shown in graph otherwise not.
	 *
	 */
	
	if($(".myFirm").is(":checked")){
		firmNamesList.push("My Firm");
		partnerList.push(10);
		associateList.push(10);
		otherList.push(10);
		totalCount.push(30);
		checkboxCount++;
	}
	
	if($(".amLaw100").is(":checked")){
		firmNamesList.push("AmLaw100");
		partnerList.push(10);
		associateList.push(10);
		otherList.push(10);
		totalCount.push(30);
		checkboxCount++;
	}
	
	if($(".watchListAvg").is(":checked")){
		firmNamesList.push("WathList Average");
		partnerList.push(10);
		associateList.push(10);
		otherList.push(10);
		totalCount.push(30);
		checkboxCount++;
	}
	
	if($(".avgFirmSearch").is(":checked")){
		firmNamesList.push("Average Firm Search");
		partnerList.push(10);
		associateList.push(10);
		otherList.push(10);
		totalCount.push(30);
		checkboxCount++;
	}
	
	var selectedFirmList = [];
	var indexList = [];

	// Push selected firms name in list.
	$(".popupSelectedFirms > option:selected").each(function(){
		selectedFirmList.push($(this).text());
	});
	
	// Find out position of selected firms name index from original list and put in index list.
	if(selectedFirmList.length > 0){
		for(var index=0; index<firmNameList.length; index++){
			if(firmNameList.indexOf(selectedFirmList[index]) >= 0){
				indexList.push(firmNameList.indexOf(selectedFirmList[index]));
			}
		}
	}
	
	var limit = 0;
	if(plusParnterList.length < 10){
		limit = plusParnterList.length;
	} else {
		limit = 10;
	}
	
	// If user has selected firms then graph will be generated only for selected firms otherwise for all.
	if(indexList.length > 0){
		for(var index=0; index<indexList.length; index++){
			firmNamesList.push(firmNameList[indexList[index]]);
			partnerList.push(parseInt(plusParnterList[indexList[index]])-parseInt(minusParnterList[indexList[index]]));
			associateList.push(parseInt(plusAssociateList[indexList[index]])-parseInt(minusAssociateList[indexList[index]]));
			otherList.push(parseInt(plusOthersList[indexList[index]])-parseInt(minusOthersList[indexList[index]]));
			totalCount.push(parseInt(plusParnterList[indexList[index]])-parseInt(minusParnterList[indexList[index]])+parseInt(plusAssociateList[indexList[index]])-parseInt(minusAssociateList[indexList[index]])+parseInt(plusOthersList[indexList[index]])-parseInt(minusOthersList[indexList[index]]));
		}
	} else {
		for(var index=0; index<limit; index++){
			firmNamesList.push(firmNameList[index]);
			partnerList.push(parseInt(plusParnterList[index])-parseInt(minusParnterList[index]));
			associateList.push(parseInt(plusAssociateList[index])-parseInt(minusAssociateList[index]));
			otherList.push(parseInt(plusOthersList[index])-parseInt(minusOthersList[index]));
			totalCount.push(parseInt(plusParnterList[index])-parseInt(minusParnterList[index])+parseInt(plusAssociateList[index])-parseInt(minusAssociateList[index])+parseInt(plusOthersList[index])-parseInt(minusOthersList[index]));
		}
	}
	
	// Whether user select top 10 or bottom 10 sort data in descending or ascending order.
	if($('input[name=sorting]:radio:checked').val() == 'top_ten'){
		sorting(firmNameList, partnerList, associateList, otherList, totalCount,  partnerList.length, 0);
	}

	if($('input[name=sorting]:radio:checked').val() == 'bottom_ten'){
		sorting(firmNameList, partnerList, associateList, otherList, totalCount, partnerList.length, 1);
	}
	
	// Sorting data based on total count.
	function sorting(firmNamesList, partnerList, associateList, otherList, countList, n, m){
		
		for(var x=2; x<n; x++){
			var index_of_min = x;
			for(var y=x; y<n; y++){
				if(parseInt(m)==0){
					if(countList[index_of_min]<countList[y]){
				  		index_of_min = y;
					}
				} else {
					if(countList[index_of_min]>countList[y]){
				  		index_of_min = y;
					}
				}				
			}
			var temp = countList[x];
			countList[x] = countList[index_of_min];
			countList[index_of_min] = temp;
			
			var temp1 = firmNamesList[x];
			firmNamesList[x] = firmNamesList[index_of_min];
			firmNamesList[index_of_min] = temp1;

			var temp = partnerList[x];
			partnerList[x] = partnerList[index_of_min];
			partnerList[index_of_min] = temp;
			
			var temp1 = associateList[x];
			associateList[x] = associateList[index_of_min];
			associateList[index_of_min] = temp1;
			
			var temp1 = otherList[x];
			otherList[x] = otherList[index_of_min];
			otherList[index_of_min] = temp1;
		}
	}
	
	// Find and initialise position of chart.
	if(chartPosition == "leftChart"){
		chartPosition = '#leftChart';
	} else {
		chartPosition = '#rightChart';
	}
	
	
	// Generate graph.
	$(chartPosition).highcharts({
        chart: {
            type: type
        },
        title: {
            text: 'NET CHANGE OF ATTORNEYS'
        },
        xAxis: {
            categories: firmNamesList,
            labels: {
                rotation: -45,
                y: y,
			},
			plotLines: [{
                color: '#000000',
                width: 2,
                dashStyle: 'Solid',
                value: parseFloat(checkboxCount-0.5)
            }]
        },
        yAxis: {
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
        	enabled: true
    	},
        tooltip: {
            formatter: function() {
                return '<b>'+ this.x +'</b><br/>'+
                    this.series.name +': '+ this.y +'<br/>'+
                    'Total: '+ this.point.stackTotal;
            }
        },
        plotOptions: {
        	series: {
                dataLabels: {
                    enabled: true,
                    color: '#000000'
                }
            }
        },
        series: [{
            name: 'Net Changes',
            color: '#084B8A',
            data: totalCount
        }]
    });
}



function displayNetStackGraphPerc(firmNameList, plusParnterList, minusParnterList, plusAssociateList, minusAssociateList, plusOthersList, minusOthersList, type, y, chartPosition){
	
	var totalEmployeeList = [];
	
	var jsonResult = $.parseJSON($("#jsonResult").val());
	for(var index=0; index<jsonResult.length; index++){
		totalEmployeeList.push(jsonResult[index].totalEmployeeCount);
	}
	var partnerList = [];
	var associateList = [];
	var otherList = [];
	var totalCount = [];
	var firmNamesList = [];
	var checkboxCount = 0;

	if($(".myFirmRight").is(":checked")){
		firmNamesList.push("My Firm");
		partnerList.push(10);
		associateList.push(10);
		otherList.push(10);
		totalCount.push(30);
		checkboxCount++;
	}
	
	if($(".amLaw100Right").is(":checked")){
		firmNamesList.push("AmLaw100");
		partnerList.push(10);
		associateList.push(10);
		otherList.push(10);
		totalCount.push(30);
		checkboxCount++;
	}
	
	if($(".watchListAvgRight").is(":checked")){
		firmNamesList.push("WathList Average");
		partnerList.push(10);
		associateList.push(10);
		otherList.push(10);
		totalCount.push(30);
		checkboxCount++;
	}
	
	if($(".avgFirmSearchRight").is(":checked")){
		firmNamesList.push("Average Firm Search");
		partnerList.push(10);
		associateList.push(10);
		otherList.push(10);
		totalCount.push(30);
		checkboxCount++;
	}
	
	var perPartnerList = [];
	var perAssociateList = [];
	var perOtherList = [];
	
	
	var selectedFirmList = [];
	var indexList = [];
	$(".popupSelectedFirms2 > option:selected").each(function(){
		selectedFirmList.push($(this).text());
	});
	
	if(selectedFirmList.length > 0){
		for(var index=0; index<firmNameList.length; index++){
			if(firmNameList.indexOf(selectedFirmList[index]) >= 0){
				indexList.push(firmNameList.indexOf(selectedFirmList[index]));
			}
		}
	}
	
	var limit = 0;
	if(plusParnterList.length < 10){
		limit = plusParnterList.length;
	} else {
		limit = 10;
	}
	
	if(indexList.length > 0){
		for(var index=0; index<indexList.length; index++){
			firmNamesList.push(firmNameList[indexList[index]]);
			partnerList.push(parseFloat(((parseInt(plusParnterList[indexList[index]])-parseInt(minusParnterList[indexList[index]]))/totalEmployeeList[indexList[index]]*100).toFixed(2)));
			associateList.push(parseFloat(((parseInt(plusAssociateList[indexList[index]])-parseInt(minusAssociateList[indexList[index]]))/totalEmployeeList[indexList[index]]*100).toFixed(2)));
			otherList.push(parseFloat(((parseInt(plusOthersList[indexList[index]])-parseInt(minusOthersList[indexList[index]]))/totalEmployeeList[indexList[index]]*100).toFixed(2)));
			totalCount.push(parseInt(plusParnterList[indexList[index]])-parseInt(minusParnterList[indexList[index]])+parseInt(plusAssociateList[indexList[index]])-parseInt(minusAssociateList[indexList[index]])+parseInt(plusOthersList[indexList[index]])-parseInt(minusOthersList[indexList[index]]));
		}
	} else {
		for(var index=0; index<limit; index++){
			firmNamesList.push(firmNameList[index]);
			partnerList.push(parseFloat(((parseInt(plusParnterList[index])-parseInt(minusParnterList[index]))/totalEmployeeList[index]*100).toFixed(2)));
			associateList.push(parseFloat(((parseInt(plusAssociateList[index])-parseInt(minusAssociateList[index]))/totalEmployeeList[index]*100).toFixed(2)));
			otherList.push(parseFloat(((parseInt(plusOthersList[index])-parseInt(minusOthersList[index]))/totalEmployeeList[index]*100).toFixed(2)));
			totalCount.push(parseInt(plusParnterList[index])-parseInt(minusParnterList[index])+parseInt(plusAssociateList[index])-parseInt(minusAssociateList[index])+parseInt(plusOthersList[index])-parseInt(minusOthersList[index]));
		}
	}
	
		
	if($('input[name=sorting2]:radio:checked').val() == 'top_ten_2'){
		sorting(firmNamesList, partnerList, associateList, otherList, totalCount,  partnerList.length, 0);
	}

	if($('input[name=sorting2]:radio:checked').val() == 'bottom_ten_2'){
		sorting(firmNamesList, partnerList, associateList, otherList, totalCount, partnerList.length, 1);
	}

	function sorting(firmNameList, partnerList, associateList, otherList, countList, n, m){
		
		for(var x=2; x<n; x++){
			var index_of_min = x;
			for(var y=x; y<n; y++){
				if(parseInt(m)==0){
					if(countList[index_of_min]<countList[y]){
				  		index_of_min = y;
					}
				} else {
					if(countList[index_of_min]>countList[y]){
				  		index_of_min = y;
					}
				}				
			}
			var temp = countList[x];
			countList[x] = countList[index_of_min];
			countList[index_of_min] = temp;
			
			var temp1 = firmNameList[x];
			firmNameList[x] = firmNameList[index_of_min];
			firmNameList[index_of_min] = temp1;

			var temp = partnerList[x];
			partnerList[x] = partnerList[index_of_min];
			partnerList[index_of_min] = temp;
			
			var temp1 = associateList[x];
			associateList[x] = associateList[index_of_min];
			associateList[index_of_min] = temp1;
			
			var temp1 = otherList[x];
			otherList[x] = otherList[index_of_min];
			otherList[index_of_min] = temp1;
		}
	}
	
	if(chartPosition == "leftChart"){
		chartPosition = '#leftChart';
	} else {
		chartPosition = '#rightChart';
	}
	
	$(chartPosition).highcharts({
        chart: {
            type: type
        },
        title: {
            text: 'PERCENTAGE CHANGE OF ATTORNEYS'
        },
        xAxis: {
            categories: firmNamesList,
            labels: {
                rotation: -45,
                y: y,
			},
			plotLines: [{
                color: '#000000',
                width: 2,
                dashStyle: 'Solid',
                value: parseFloat(checkboxCount-0.5)
            }]
        },
        yAxis: {
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
        	enabled: true
    	},
    	tooltip: {
            formatter: function() {
            	displayPopUpNetStackGraphPerc(firmNameList, plusParnterList, minusParnterList, plusAssociateList, minusAssociateList, plusOthersList, minusOthersList, this.x, this.y);
                return false;
            }
        },
        plotOptions: {
            series: {
            	stacking: 'normal',
            	dataLabels: {
                    enabled: true,
                    color: '#FFFFFF'
                },
                point: {
                    events: {
                        mouseOver: function(e) {
                        	var xPos = e.target.plotX + 640;
                        	var yPos = e.target.plotY + 330;
                       	 	$("div#percentageHoverOver").css('top', yPos).css('left', xPos).css('z-index', 999).css('width', '210px');;
                            $("div#percentageHoverOver").show();
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Partner',
            color: '#446e49',
            data: partnerList
        }, {
            name: 'Associate',
            color: '#0c3362',
            data: associateList
        }, {
            name: 'Other Counsel',
            color: '#b89024',
            data: otherList
        }]
    });
}

function displayPopUpNetStackGraphPerc(firmNameList, plusParnterList, minusParnterList, plusAssociateList, minusAssociateList, plusOthersList, minusOthersList, x, y){
	
	var totalEmployeeList = [];
	
	var jsonResult = $.parseJSON($("#jsonResult").val());
	for(var index=0; index<jsonResult.length; index++){
		totalEmployeeList.push(jsonResult[index].totalEmployeeCount);
	}
	var partnerList = [];
	var associateList = [];
	var otherList = [];
	
	partnerList.push(10);
	partnerList.push(10);
	
	associateList.push(10);
	associateList.push(10);
	
	otherList.push(10);
	otherList.push(10);
	
	
	for(var index=0; index<plusParnterList.length; index++){
		partnerList.push(parseInt(plusParnterList[index])-parseInt(minusParnterList[index]));
		associateList.push(parseInt(plusAssociateList[index])-parseInt(minusAssociateList[index]));
		otherList.push(parseInt(plusOthersList[index])-parseInt(minusOthersList[index]));
	}
	
	
	var table = "<b>"+x+"-Change of Attorneys</b><br><table>";
	for(var index=0; index<firmNameList.length; index++){
		if(x == firmNameList[index]){
			
			var addedAttorney = parseInt(plusParnterList[index])+parseInt(plusAssociateList[index])+parseInt(plusOthersList[index]);
			var removedAttorney = parseInt(minusParnterList[index]) + parseInt(minusAssociateList[index]) + parseInt(minusOthersList[index]);
			var netAllAttorney = addedAttorney - removedAttorney;
			table += '<div class="table"><div class="row"><span class="column1">&nbsp;</span><span class="column2">Net</span></div><div class="row  back-color-row2"><span class="column1">All Attorney</span><span class="column2">'+parseFloat((netAllAttorney/totalEmployeeList[index]*100).toFixed(2))+'</span></div><div class="row   back-color-row3"><span class="column1">Partners</span><span class="column2">'+parseFloat(((parseInt(plusParnterList[index])-parseInt(minusParnterList[index]))/totalEmployeeList[index]*100).toFixed(2))+'</span></div><div class="row   back-color-row4"><span class="column1">Associates</span><span class="column2">'+parseFloat(((parseInt(plusAssociateList[index])-parseInt(minusAssociateList[index]))/totalEmployeeList[index]*100).toFixed(2))+'</span></div><div class="row   back-color-row5"><span class="column1">Other Counsel</span><span class="column2">'+parseFloat(((parseInt(plusOthersList[index])-parseInt(minusOthersList[index]))/totalEmployeeList[index]*100).toFixed(2))+'</span></div></div>'
		}
	}
	table += "</table>";
	$("#perHoverOver").html(table);
	
}

function displayBarGraphPerc(firmNameList, plusParnterList, minusParnterList, plusAssociateList, minusAssociateList, plusOthersList, minusOthersList, type, y, chartPosition){
	
	
	var totalEmployeeList = [];
	
	var jsonResult = $.parseJSON($("#jsonResult").val());
	for(var index=0; index<jsonResult.length; index++){
		totalEmployeeList.push(jsonResult[index].totalEmployeeCount);
	}
	
	var partnerList = [];
	var associateList = [];
	var otherList = [];
	var totalCount = [];
	var firmNamesList = [];
	var checkboxCount = 0;
	
	if($(".myFirm").is(":checked")){
		firmNamesList.push("My Firm");
		partnerList.push(10);
		associateList.push(10);
		otherList.push(10);
		totalCount.push(30);
		checkboxCount++;
	}
	
	if($(".amLaw100").is(":checked")){
		firmNamesList.push("AmLaw100");
		partnerList.push(10);
		associateList.push(10);
		otherList.push(10);
		totalCount.push(30);
		checkboxCount++;
	}
	
	if($(".watchListAvg").is(":checked")){
		firmNamesList.push("WathList Average");
		partnerList.push(10);
		associateList.push(10);
		otherList.push(10);
		totalCount.push(30);
		checkboxCount++;
	}
	
	if($(".avgFirmSearch").is(":checked")){
		firmNamesList.push("Average Firm Search");
		partnerList.push(10);
		associateList.push(10);
		otherList.push(10);
		totalCount.push(30);
		checkboxCount++;
	}
	
	var perPartnerList = [];
	var perAssociateList = [];
	var perOtherList = [];
	
	
	
	var selectedFirmList = [];
	var indexList = [];
	$(".popupSelectedFirms2 > option:selected").each(function(){
		selectedFirmList.push($(this).text());
	});
	
	if(selectedFirmList.length > 0){
		for(var index=0; index<firmNameList.length; index++){
			if(firmNameList.indexOf(selectedFirmList[index]) >= 0){
				indexList.push(firmNameList.indexOf(selectedFirmList[index]));
			}
		}
	}
	
	var limit = 0;
	if(plusParnterList.length < 10){
		limit = plusParnterList.length;
	} else {
		limit = 10;
	}
	
	if(indexList.length > 0){
		for(var index=0; index<indexList.length; index++){
			firmNamesList.push(firmNameList[indexList[index]]);
			partnerList.push(parseFloat(((parseInt(plusParnterList[indexList[index]])-parseInt(minusParnterList[indexList[index]]))/totalEmployeeList[indexList[index]]*100).toFixed(2)));
			associateList.push(parseFloat(((parseInt(plusAssociateList[indexList[index]])-parseInt(minusAssociateList[indexList[index]]))/totalEmployeeList[indexList[index]]*100).toFixed(2)));
			otherList.push(parseFloat(((parseInt(plusOthersList[indexList[index]])-parseInt(minusOthersList[indexList[index]]))/totalEmployeeList[indexList[index]]*100).toFixed(2)));
			totalCount.push(parseFloat(((parseInt(plusParnterList[indexList[index]])-parseInt(minusParnterList[indexList[index]])
							+parseInt(plusAssociateList[indexList[index]])-parseInt(minusAssociateList[indexList[index]])
							+parseInt(plusOthersList[indexList[index]])-parseInt(minusOthersList[indexList[index]]))
							/totalEmployeeList[indexList[index]]*100).toFixed(2)));
		}
	} else {
		for(var index=0; index<limit; index++){
			firmNamesList.push(firmNameList[index]);
			partnerList.push(parseFloat(((parseInt(plusParnterList[index])-parseInt(minusParnterList[index]))/totalEmployeeList[index]*100).toFixed(2)));
			associateList.push(parseFloat(((parseInt(plusAssociateList[index])-parseInt(minusAssociateList[index]))/totalEmployeeList[index]*100).toFixed(2)));
			otherList.push(parseFloat(((parseInt(plusOthersList[index])-parseInt(minusOthersList[index]))/totalEmployeeList[index]*100).toFixed(2)));
			totalCount.push(parseFloat(((parseInt(plusParnterList[index])-parseInt(minusParnterList[index])
							+parseInt(plusAssociateList[index])-parseInt(minusAssociateList[index])
							+parseInt(plusOthersList[index])-parseInt(minusOthersList[index]))
							/totalEmployeeList[index]*100).toFixed(2)));
		}
	}
	
	
	
	if($('input[name=sorting2]:radio:checked').val() == 'top_ten_2'){
		sorting(firmNameList, partnerList, associateList, otherList, totalCount,  partnerList.length, 0);
	}

	if($('input[name=sorting2]:radio:checked').val() == 'bottom_ten_2'){
		sorting(firmNameList, partnerList, associateList, otherList, totalCount, partnerList.length, 1);
	}
	
	function sorting(firmNameList, partnerList, associateList, otherList, countList, n, m){
		
		for(var x=2; x<n; x++){
			var index_of_min = x;
			for(var y=x; y<n; y++){
				if(parseInt(m)==0){
					if(countList[index_of_min]<countList[y]){
				  		index_of_min = y;
					}
				} else {
					if(countList[index_of_min]>countList[y]){
				  		index_of_min = y;
					}
				}				
			}
			var temp = countList[x];
			countList[x] = countList[index_of_min];
			countList[index_of_min] = temp;
			
			var temp1 = firmNameList[x];
			firmNameList[x] = firmNameList[index_of_min];
			firmNameList[index_of_min] = temp1;

			var temp = partnerList[x];
			partnerList[x] = partnerList[index_of_min];
			partnerList[index_of_min] = temp;
			
			var temp1 = associateList[x];
			associateList[x] = associateList[index_of_min];
			associateList[index_of_min] = temp1;
			
			var temp1 = otherList[x];
			otherList[x] = otherList[index_of_min];
			otherList[index_of_min] = temp1;
		}
	}
	if(chartPosition == "leftChart"){
		chartPosition = '#leftChart';
	} else {
		chartPosition = '#rightChart';
	}
	
	$(chartPosition).highcharts({
        chart: {
            type: type
        },
        title: {
            text: 'PERCENTAGE CHANGE OF ATTORNEYS'
        },
        xAxis: {
            categories: firmNamesList,
            labels: {
                rotation: -45,
                y: y,
			},
			plotLines: [{
                color: '#000000',
                width: 2,
                dashStyle: 'Solid',
                value: parseFloat(checkboxCount-0.5)
            }]
        },
        yAxis: {
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
        	enabled: true
    	},
        tooltip: {
            formatter: function() {
                return '<b>'+ this.x +'</b><br/>'+
                    this.series.name +': '+ this.y +'<br/>'+
                    'Total: '+ this.point.stackTotal;
            }
        },
        plotOptions: {
        	series: {
                dataLabels: {
                    enabled: true,
                    color: '#000000'
                }
            }
        },
        series: [{
            name: 'Net Changes',
            color: '#084B8A',
            data: totalCount
        }]
    });
}

</script>
<div id="tab3" style="position: relative">
	<h4 class="heading">Analyze Results</h4>
	<div class="marbtm2 martp3">
		<div class="flLeft mart2" style="position: relative; z-index: 999">
			<button class="buttonTwo mybutton" onclick="return false;">
				<span class="btn icon drpdwngry"><span
					class="btn icon addgry">Add Analysis</span></span>
			</button>
			<div class="selectchartadd hover" style="display: none;">
				<h6>Select a chart to add to your analysis</h6>
				<ul class="reset list3">
					<li>
						<div class="imgdiv netChangeOfAttorney"></div>
						<div class="disc netChangeOfAttorney">
							<h4>Net Change of Attorney</h4>
							<p>Description of the chart Description of the chart</p>
						</div>
					</li>
					<li>
						<div class="imgdiv percChangeOfAttorney"></div>
						<div class="disc percChangeOfAttorney">
							<h4>PERCENTAGE CHANGE OF ATTORNEYS</h4>
							<p>Description of the chart Description of the chart</p>
						</div>
					</li>
				</ul>
				<div class="clear">&nbsp;</div>
			</div>
		</div>
		<div class="flLeft resultsec martp1">
				<strong>${peopleSearchModelBean.currentPageSize} Results</strong> <span>(
					<span id="selectedFirmsResult"></span>, ${peopleSearchModelBean.selectedPracticeArea}, ${peopleSearchModelBean.dateText}
					 )
				</span>
			</div>
		<div class="viewSetting flRight">
			<a class="btn icon settings" href="#"><span><span
					class="btn icon dropdownarrow">View Settings</span></span></a>
			<div class="viewBox">
				<h6>Layout</h6>
				<form action="#">
					<ul class="reset list2">
						<li><input type="radio" name="columnType" value="one_column" class="column">
							<span class="btn icon onecolumn">1 Column</span></li>
						<li><input type="radio" name="columnType" value="two_column" class="column">
							<span class="btn icon twocolumn">2 Column</span></li>
					</ul>
				</form>
				<div class="clear">&nbsp;</div>
				<div class="btmdiv">
					<input type="button" value="Reset All" class="buttonTwo flLeft settingReset">
					<input type="button" value="Apply" class="buttonOne flRight" id="viewSettingColumnApply">
					<div class="clear">&nbsp;</div>
				</div>
			</div>
		</div>
		<div class="clear">&nbsp;</div>
	</div>

	<!-- News Stories by Firm - Top 5 Firms -->

	<div class="newspublicationPage marbtm4">
		<div class="colMin flLeft leftDetails">
			<div class="topHeader">
				<div class="flRight">
					<ul class="reset listView">
						<li><a href="#" class="btn icon settingsgry">&nbsp;</a>
							<div class="viewBox">
								<h6>Chart Type</h6>
								<form action="#">
									<ul class="reset list4">
									
										<li><input type="radio" name="graphType"
											value="stackVertical" class="graphType"> <span
											class="btn icon stackedbarchart">Stacked Vertical Bar</span></li>
										<li><input type="radio" name="graphType"
											value="stackHorizontal" class="graphType"> <span
											class="btn icon barcharthori">Stacked Horizontal Bar</span></li>
										<li><input type="radio" name="graphType"
											value="verticalBar" class="graphType"> <span
											class="btn icon barchartvert">Vertical Bar</span></li>
										<li style="width: 125px"><input type="radio"
											name="graphType" value="horizontalBar" class="graphType">
											<span class="btn icon barcharthori">Horizontal Bar</span></li>
									</ul>
									<div class="clear">&nbsp;</div>
								</form>

								<form action="#">
									<h6>Firm Data (Limit of 10)</h6>
									<ul class="reset list4">
										<li style="width: 125px"><input type="radio"
											name="sorting" value="top_ten" class="sorting"> <span
											class="">Top 10</span></li>
										<li><input type="radio" name="sorting" value="bottom_ten"
											class="sorting"> <span class="">Bottom 10</span></li>
											
										<li>
											<input type="radio" name="sorting" value="selected_firm" class="sorting">
											<span class="">Select Firms:</span>
											<span id="selected_firms"></span>
											<span id="selFirm"></span>
										</li>
											
											
									</ul>
									<div class="clear">&nbsp;</div>
								</form>

								<div class="martp2">
									<h6>Comparison Data</h6>
									<div class="marbtm2 martp1">
										<p class="marbtm1">
											<input type="checkbox" class="myFirm"> My Firm
										</p>
										<p class="marbtm1">
											<input type="checkbox" class="amLaw100" checked="checked"> <select>
												<option>AmLaw100</option>
											</select>
										</p>
										<p><input type="checkbox" class="watchListAvg">
											Watchlist Average <select>
												<option>Firmwatchlist Name</option>
											</select>
										</p>
										<p class="marbtm1">
											<input type="checkbox" class="avgFirmSearch" checked="checked"> Average of Firms in search
										</p>
									</div>
								</div>
								<div class="clear">&nbsp;</div>
								<div class="btmdiv">
									<input type="button" value="Reset All" class="buttonTwo flLeft">
									<input type="button" value="Apply" class="buttonOne flRight"
										id="leftChartSettingApply">
									<div class="clear">&nbsp;</div>
								</div>
							</div></li>
						<li><a href="#" class="btn icon printgry printLeftGraph">&nbsp;</a></li>
						<li><a id="exports" href="#" class="btn icon exportgry">&nbsp;</a>
							<div id="actionBox" class="actionSec" style="">
								<h5>Actions</h5>
								<ul class="reset">
									<li><span>E</span><a href="#">Export to Excel</a></li>
									<li><span>E</span><a href="#">Export as JPEG Image</a></li>
									<li><span>E</span><a href="#">Export as PNG Image</a></li>
								</ul>
								<div class="clear">&nbsp;</div>
							</div></li>
					</ul>
				</div>
				Net Change of Attorneys
				<div class="clear">&nbsp;</div>
			</div>
			<div class="netypepage">
				<h6 class="rightAlign">Net Change of Attorneys</h6>
				<div class="padtp3 marbtm4">

					<div id="leftChart"
						style="overflow: hidden; width: 467px; height: 302px;"></div>
					
					
				</div>
			</div>
		</div>
		<div class="colMin flLeft rightDetails">
			<div class="topHeader">
				<div class="flRight">
					<ul class="reset listView">
						<li><a href="#" class="btn icon settingsgry">&nbsp;</a>
							<div class="viewBox">
								<h6>Chart Type</h6>
								<form action="#">
									<ul class="reset list4">
									
										<li><input type="radio" name="graphType2"
											value="stackVertical" class="graphType"> <span
											class="btn icon stackedbarchart">Stacked Vertical Bar</span></li>
										<li><input type="radio" name="graphType2"
											value="stackHorizontal" class="graphType"> <span
											class="btn icon barcharthori">Stacked Horizontal Bar</span></li>
										<li><input type="radio" name="graphType2"
											value="verticalBar" class="graphType"> <span
											class="btn icon barchartvert">Vertical Bar</span></li>
										<li style="width: 125px"><input type="radio"
											name="graphType2" value="horizontalBar" class="graphType">
											<span class="btn icon barcharthori">Horizontal Bar</span></li>
									</ul>
									<div class="clear">&nbsp;</div>
								</form>

								<form action="#">
									<h6>Firm Data (Limit of 10)</h6>
									<ul class="reset list4">
										<li style="width: 125px"><input type="radio"
											name="sorting2" value="top_ten_2" class="sorting2"> <span
											class="">Top 10</span></li>
										<li><input type="radio" name="sorting2" value="bottom_ten_2"
											class="sorting2"> <span class="">Bottom 10</span></li>

										<li><input type="radio" name="sorting2"
											value="selected_firm_2" class="sorting2"> <span class="">Select
												Firms:</span> <span id="selected_firms2"></span> <span id="selFirm"></span>
										</li>
											
											
									</ul>
									<div class="clear">&nbsp;</div>
								</form>

								<div class="martp2">
									<h6>Comparison Data</h6>
									<div class="marbtm2 martp1">
										<p class="marbtm1">
											<input type="checkbox" class="myFirmRight"> My Firm
										</p>
										<p class="marbtm1">
											<input type="checkbox" class="amLaw100Right" checked="checked"> <select>
												<option>AmLaw100</option>
											</select>
										</p>
										<p><input type="checkbox" class="watchListAvgRight">
											Watchlist Average <select>
												<option>Firmwatchlist Name</option>
											</select>
										</p>
										<p class="marbtm1">
											<input type="checkbox" class="avgFirmSearchRight" checked="checked"> Average of Firms in search
										</p>
									</div>
								</div>
								<div class="clear">&nbsp;</div>
								<div class="btmdiv">
									<input type="button" value="Reset All" class="buttonTwo flLeft">
									<input type="button" value="Apply" class="buttonOne flRight"
										id="rightChartSettingApply">
									<div class="clear">&nbsp;</div>
								</div>
							</div></li>
						<li><a href="#" class="btn icon printgry printRightGraph">&nbsp;</a></li>
						<li><a id="exports" href="#" class="btn icon exportgry">&nbsp;</a>
							<div id="actionBox" class="actionSec" style="">
								<h5>Actions</h5>
								<ul class="reset">
									<li><span>E</span><a href="#">Export to Excel</a></li>
									<li><span>E</span><a href="#">Export as JPEG Image</a></li>
									<li><span>E</span><a href="#">Export as PNG Image</a></li>
								</ul>
								<div class="clear">&nbsp;</div>
							</div></li>
					</ul>
				</div>
				Percentage Change of Attorneys
				<div class="clear">&nbsp;</div>
			</div>
			<div class="netypepage">
				<h6 class="rightAlign">Percentage Change of Attorneys</h6>
				<div class="padtp3 marbtm4">

					<div id="rightChart"
						style="overflow: hidden; width: 467px; height: 302px;"></div>
					
					
				</div>
			</div>
		</div>
		<div class="clear">&nbsp;</div>
	</div>
</div>
<input type="hidden" id="hiddenField">

<div id="hoverOverTable">
<div id="hoverOver"></div>
<span>Click to view details</span>
</div>
<div id="percentageHoverOver">
<div id="perHoverOver"></div>
</div>
<input type="hidden" name="jsonResult" id="jsonResult" value="${jsonResult}">
