<%@page import="java.util.Map.Entry"%>
<%@page import="com.alm.rivaledge.util.WebUtil"%>
<%@page import="java.text.DecimalFormat"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.alm.rivaledge.transferobject.PeopleResult"%>
<%@page import="com.alm.rivaledge.transferobject.PeopleResultDTO"%>
<%@page import="com.alm.rivaledge.model.PeopleSearchModelBean"%>
<%@page import="java.util.*"%>
    <%!
    protected Log		_log	= LogFactoryUtil.getLog("_calculation.jsp");

    /**
    * Returns the css Class Name (@[0] index) and formatted percentage value(@[1] index) for the given record
    */
	protected String[]  calculatePercentageData(PeopleResult ppl, TreeMap<Double, String> cssClassMap)
	{
		double percent = WebUtil.calculatePercentage(ppl);
		
		String[] returnMe = new String[]
				{ 
				getCssClassName(percent, cssClassMap),
				getFormattedPercentageValue(percent)
				};
		
		return returnMe;
	}
    
    /**
    * Returns the css class name corresponding to the percentage value
    */
    
    private String getCssClassName(double percentage, TreeMap<Double, String> cssClassMap)
    {
    	
    	/*
    	*Adjust the value to fall within the range [-25, 25]
    	*/
    	
		//_log.info("cssClassMap = " + cssClassMap);
		//_log.info("format("+percentage+") = " + percentageFormat.format(percentage));
		//_log.info("lastKey = " + cssClassMap.lastKey() + " and firstKey "+ cssClassMap.firstKey() );
		
		if(percentage > cssClassMap.lastKey())
		{
			percentage = cssClassMap.lastKey();
			
		}
    	else if(percentage < cssClassMap.firstKey())
		{
			percentage = cssClassMap.firstKey();
			
		}  
		
		
		String category = null;
		
		if(percentage >= 0d)
		{
			category = cssClassMap.ceilingEntry(percentage).getValue(); // get near top value
		}
		else 
		{
			category = cssClassMap.floorEntry(percentage).getValue(); // get near bottom value
		}
		
		return category;
		
    }
    
    DecimalFormat percentageFormat = new DecimalFormat("###.#"); // For bug 445
    /*
    *Formats the percentage Value
    * 23 = +23%
    * -5 = -5%
    */
    private String getFormattedPercentageValue(double percentage)
    {
    	//_log.info("format("+percentage+") = " + percentageFormat.format(percentage));
    	
    	StringBuffer formattedPercent = new StringBuffer();
    	
    	if(percentage > 0d)
		{
    		formattedPercent.append("+");
		}
		
    	return formattedPercent.append("<b>").append(percentageFormat.format(percentage)).append("%").append("</b>").toString();
    }
    %>