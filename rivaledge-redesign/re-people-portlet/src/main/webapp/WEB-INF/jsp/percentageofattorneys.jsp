<%@page import="com.alm.rivaledge.model.chart.BaseChartModelBean"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="java.util.Map"%>
<%@ taglib prefix="portlet" 		uri="http://java.sun.com/portlet_2_0"%> 
<%@ taglib prefix="c" 			 	uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"          uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form"            uri="http://www.springframework.org/tags/form"%>

<portlet:defineObjects/>

<%@ include file="./common.jsp" %>

<portlet:actionURL var="updateChartURL">
	<portlet:param name="action" value="updateChart"/>
</portlet:actionURL>

<portlet:actionURL var="removePortletURL">
	<portlet:param name="action" value="removePortleFromThePage"/>
</portlet:actionURL>

<script type="text/javascript">

var <portlet:namespace/>thisPortletId = '<%= themeDisplay.getPortletDisplay().getId() %>';

(function () {
<%@ include file="./theme.jsp" %>

	<c:choose>
		<c:when test="${percentAttorneysChartType eq 'stacked_horizontal_bar' }">
			var percentAttorneys_typeOfChart = 'bar';	//This will be type of chart.
		</c:when> 	
		<c:otherwise>
			var percentAttorneys_typeOfChart = 'column'; //This will be type of chart.
		</c:otherwise>	
	</c:choose>
	var percentAttorneys_titleOfChart 		= '${percentAttorneysChartTitle}'; 		// This will be title of chart.
	var percentAttorneysFirmNameList 		= [${percentAttorneysFirmNameList}]; 	// This will be list of firms name.
	var percentAttorneysPartnerCount 		= [${percentAttorneysPartnerCount}]; 	// This will be news count of individual firms.
	var percentAttorneysAssociateCount 		= [${percentAttorneysAssociateCount}]; 	// This will be publication count of individual firms.
	var percentAttorneysOtherCounselCount 	= [${percentAttorneysOtherCounselCount}]; // This will be twitter count of individual firms.
	var percentAttorneysPartnerCountList = [];
	var percentAttorneysAssociateCountList = [];
	var percentAttorneysOtherCounselCountList = [];
	var numAttorneysPartnerCountForPercent 		= [${numAttorneysPartnerCountForPercent}]; 	 // This will be news count of individual firms.
	var numAttorneysAssociateCountForPercent 		= [${numAttorneysAssociateCountForPercent}]; 	// This will be publication count of individual firms.
	var numAttorneysOtherCounselCountForPercent 	= [${numAttorneysOtherCounselCountForPercent}]; // This will be twitter count of individual firms
	
	<c:choose>
		<c:when test="${(not empty percentAttorneysCompareCount) and (percentAttorneysCompareCount > 0)}">
			var percentAttorneysCompareCount = ${percentAttorneysCompareCount};
		</c:when> 	
		<c:otherwise>
			var percentAttorneysCompareCount = 0;
		</c:otherwise>	
	</c:choose>

$(function()
{
	
	$(".AnalyzeResultHeader").show();
	
	// When user clicks on analysis tab active-menu css class is removed from details tab.
	// And same class added to analysis tab.
	$("#firm-statistics-details").removeClass("active-menu");
	$("#firm-statistics-analysis").addClass("active-menu");
	
	if(<%=!isHomePage%>)
	{
		if (percentAttorneysFirmNameList == null || percentAttorneysFirmNameList == '') 
		{
			$("#percentOfAttorneysChartDiv").removeClass("hideClass");
			$("#percentOfAttorneysChartContainer").addClass("hideClass");
			$("#percentOfAttorneysChartDiv").parent().find(".charts").hide(); // hide the settings icon
			
			$("#p_p_id<portlet:namespace/>").hide();
			$(".cph").show();
			return;
		}
	}
	else
	{
		if (percentAttorneysFirmNameList == null || percentAttorneysFirmNameList == '')
		{
			$("#percentOfAttorneysChartDiv").removeClass("hideClass");
			$("#percentOfAttorneysChartContainer").addClass("hideClass");
				
			$("#firmStatHomeSearch_perc_settings").hide();
			$("#No-Data-PercAttorney").addClass("No-Data-Charts");
		}
		else
		{
			$("#No-Data-PercAttorney").removeClass("No-Data-Charts");
			$("#percentOfAttorneysChartDiv").addClass("hideClass");
		}	
	}
	$("#percentAttorneysApply, .closewhite").click(function(){
		$("#percentAttorneysViewSetting").hide();
	});
	
	$(".highcharts-button").click(function(){
		$("div#percentOfAttorneysChartDiv").hide();
	});
	
	$(document).click(function(){
		$("div#percentAttorneyPopupDiv").hide();
	});
	
	$("#percentAttorneysCancel").click(function(){
		$("#percentAttorneysViewSetting").hide();
	});
	
	$("div#percentAttorneyPopupDiv").mouseover(function(){
		$("div#percentAttorneyPopupDiv").show();
	});
	
	
	$("#percentAttorneyResetAll").click(function(){
		$("input[id='percentOfAttorneyHorizontalBar']").prop("checked", false);
		$("input[value=stacked_vertical_bar]").prop("checked", true);
		$("input[value=top_15]").prop("checked", true);
		$("#searchResultsFirmList > option").attr("selected",false);
		$("#watchList > option").attr("selected",false);
		$("#firmList > option").attr("selected",false);		
		$("input[value=Average]").prop("checked", true);
		$("input[value='Watchlist Average']").prop("checked", false);		
		$("input[value='RivalEdge Average']").prop("checked", true);		
		$("#comparisonDataTypeList3").prop("checked", false);
		$("#comparisonDataTypeList1").prop("checked", false);
	});
	
	$(".percentOfAttorneySearchResultFirm").click(function(){
		if($(".percentOfAttorneySearchResultFirm option:selected")){
			$("input[value=firm]").prop("checked", true);
		}
	});
	$(".percentAttorneyFirmList").change(function(){
		$("input[value='RivalEdge Average']").prop("checked", true);
	});
	$(".percentAttorneyWatchList").click(function(){
		if($("#watchList option:selected")){
			$("input[value='Watchlist Average']").prop("checked", true);
		}
	});
	
	
	$('#percentOfAttorneyPrintCharts').click(function() {
        var chart = $('#percentOfAttorneysChartContainer').highcharts();
        chart.print();
    });
	$('#percentOfAttorneyExportJPG').click(function() {
        var chart = $('#percentOfAttorneysChartContainer').highcharts();
        chart.exportChart({type: 'image/jpeg'});
    });
	$('#percentOfAttorneyExportPNG').click(function() {
        var chart = $('#percentOfAttorneysChartContainer').highcharts();
        chart.exportChart({type: 'image/png'});
    });
	
	for(var index = 0; index < percentAttorneysFirmNameList.length; index++)
	{
		percentAttorneysPartnerCountList.push(percentAttorneysPartnerCount[index]);
		percentAttorneysAssociateCountList.push(percentAttorneysAssociateCount[index]);
		percentAttorneysOtherCounselCountList.push(percentAttorneysOtherCounselCount[index]);
	}
	var splitLinePercentAttorneysCompareCount = parseFloat(percentAttorneysCompareCount) - parseFloat(0.5);
	
	var labelXPosition = 0;
	var labelYPosition = 0;
	var labelRotation = 0;
	var percentOfAttorneyPointWidth = 30;
	if(percentAttorneys_typeOfChart == 'bar')
	{
		labelXPosition = -5;
		labelYPosition = 0;
		labelRotation = 0;
		percentOfAttorneyPointWidth = 20;
	}
	else
	{
		labelXPosition = 5;
		labelYPosition = 10;
		labelRotation = -45;
	}
	
	$('#percentOfAttorneysChartContainer').highcharts(
	{
        chart: 
        {
            type: percentAttorneys_typeOfChart
        },
        title: 
        {
            text: percentAttorneys_titleOfChart
        },
        xAxis: 
        {
            categories: percentAttorneysFirmNameList,
            labels: 
            {
                rotation: labelRotation,
				y: labelYPosition,
				x: labelXPosition,
				align: 'right',
				formatter: function()
				{
					var firmName = this.value;
					if(firmName.length > 10)
					{
						firmName = firmName.substring(0, 10) + "...";
					}
					return firmName;
				}
			},
		 plotLines: [{
                color: '#000000',
                width: 2,
                dashStyle: 'Solid',
                value: parseFloat(splitLinePercentAttorneysCompareCount)
            }]
        },
        yAxis: 
        {
        	gridLineWidth: 1,
			gridLineColor: '#cecece',
			minorGridLineColor: '#cecece',
			lineWidth: 1,
            stackLabels: 
            {
                enabled: false,
                style: 
                {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: 
        {
        	enabled: true
    	},
    	tooltip : {
			useHTML : true,
			positioner : function(boxWidth, boxHeight,
					point) {
					$("#percentOfAttorneysChartContainer .highcharts-tooltip span").css("height", "125px").css("width", "150px");
					var xPosition = point.plotX - 10;
					var yPosition = point.plotY - boxHeight + 40;
					
					if(percentAttorneys_typeOfChart == 'column')
					{
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(75))
						{
							xPosition = point.plotX-75;
						}
						if((parseInt(point.plotX) - parseInt(boxWidth)) < parseInt(-200))
						{
							xPosition = point.plotX + 50;
						}
						if((parseInt(point.plotY) - parseInt(boxHeight)) < 0)
						{
							yPosition = point.plotY + 60;
						}
					}
					else
					{
						xPosition = point.plotX + 100;
						yPosition = point.plotY - boxHeight + 100;
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(65))
						{
							xPosition = point.plotX-80;
						}
						
						if((parseInt(boxHeight) - parseInt(point.plotY)) > parseInt(100))
						{
							yPosition = point.plotY - boxHeight + 150;
						}
					}
					
				return {
					x : xPosition,
					y : yPosition
				};
			},
			formatter: function()
			{
				
				var pointerPosition = 0;
				for(var index = 0; index < percentAttorneysFirmNameList.length; index++)
				{
					if(this.key == percentAttorneysFirmNameList[index])
					{
						pointerPosition = index;
					}
				}
				
				var total = this.total;
				if(this.total == undefined)
				{
					total = parseInt(percentAttorneysPartnerCountList[pointerPosition]) + parseInt(percentAttorneysAssociateCountList[pointerPosition]) + parseInt(percentAttorneysOtherCounselCountList[pointerPosition]);
				}
				var noPartnerOfAttorney = numAttorneysPartnerCountForPercent[pointerPosition];
				var noAssociateOfAttorney = numAttorneysAssociateCountForPercent[pointerPosition];
				var noOtherCounselOfAttoney = numAttorneysOtherCounselCountForPercent[pointerPosition];
				var attorneyTotal = parseInt(noPartnerOfAttorney)+parseInt(noAssociateOfAttorney)+parseInt(noOtherCounselOfAttoney);
				var tooltipOption = '';
				tooltipOption += '<table width="100%"><tr><td colspan="3" style="width: 100%"><span style="white-space: normal; font-weight: bold; color: #000;">' + this.key + '</span></td></tr><tr><td></td><td style="color:#000;">%</td><td style="color:#000;">No.</td></tr><tr><td style="color: #000; padding-left: 10px; margin-top: 15px;">All Attorneys</td><td style="border: 1px solid; background-color: #000; color: #fff; margin-top: 15px;">100%</td><td style="border: 1px solid; background-color: #000; color: #fff; margin-top: 15px;">' + attorneyTotal + '</td></tr><tr><td style="color: #15375c; padding-left: 10px; margin-top: 15px;">Partners</td><td style="border: 1px solid; background-color: #15375c; color: #fff; margin-top: 15px;">' + percentAttorneysPartnerCountList[pointerPosition] + '%</td><td style="border: 1px solid; background-color: #15375c; color: #fff; margin-top: 15px;">' + noPartnerOfAttorney + '</td></tr><tr><td style="color: #ba323e; padding-left: 10px; margin-top: 15px;">Associates</td><td style="border: 1px solid; background-color: #ba323e; color: #fff; margin-top: 15px;">' + percentAttorneysAssociateCountList[pointerPosition] + '%</td><td style="border: 1px solid; background-color: #ba323e; color: #fff; margin-top: 15px;">' + noAssociateOfAttorney + '</td></tr><tr><td style="color: #de7c35; padding-left: 10px; margin-top: 15px;">Other Counsel</td><td style="border: 1px solid; background-color: #de7c35; color: #fff; margin-top: 15px;">' + percentAttorneysOtherCounselCountList[pointerPosition] + '%</td><td style="border: 1px solid; background-color: #de7c35; color: #fff; margin-top: 15px;">' + noOtherCounselOfAttoney + '</td></tr></table>';
				
				if(pointerPosition >= percentAttorneysCompareCount)
				{
					var url = "firm-statistics-details?drilldownFirmName=" + encodeURIComponent(this.key) + searchCriteria;
					tooltipOption += '<div><span class="clickToViewDetails"><a href="'+url+'" style="color:#000;">Click to View Details</a></span></div>';
				}					

				return tooltipOption;
			}
		},
        plotOptions: 
        {
            series: 
            {
            	stacking: 'normal',
            	dataLabels: 
            	{
                    enabled: false,
                    color: '#FFFFFF'
                },
                pointWidth: 15,
            }
        },
        series: [
        {
            name: 'Partners',
            color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#506a85'],
					[1, '#15375c']
				]
			},
            data: percentAttorneysPartnerCountList
        }, 
        {
            name: 'Associates',
            color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#cb676f'],
					[1, '#ba323e']
				]
			},
            data: percentAttorneysAssociateCountList
        }, 
        {
            name: 'Other Counsel',
            color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#e59c67'],
					[1, '#de7c35']
				]
			},
            data: percentAttorneysOtherCounselCountList
        }],
		navigation: {
            buttonOptions: {
                enabled: false
            }
        }
    });
    
    $(".settingsgry, .highcharts-button").mouseover(function(){
		$("#percentAttorneyPopupDiv").hide();
	});
		
});

$(document).ready(function(){
	$("#firmStatHomeSearch_perc").click(function(){
		Liferay.Portlet.showPopup({
			uri : '${chartSearchPortletURL}', // defined in common.jsp
			title: "Search Criteria"
		});
	});	
});

}());
</script>

<script type="text/javascript">

function removeThisChartPercentageOfAttorney(){
	
	var removePortletAction = '<%= removePortletURL.toString()%>';
	var r=confirm("Are you sure you want to delete this chart?");
	if (r == true)
  	{
		Liferay.Portlet.showBusyIcon("#bodyId", "Loading...");
		$("#percentageAttorneysChartModelBean").attr('action', removePortletAction);
		$("#percentageAttorneysChartModelBean").submit();
  	}
	else
	{
	  return false;
	}
}

</script> 
    	

<div class="newspublicationPage marbtm4 percentAttorneyChartDiv">
		<div class="colMin flLeft leftDynamicDiv">
			<div class="topHeader ForChartsTopHeader">
				<a title="Remove this chart" onclick="removeThisChartPercentageOfAttorney();" href="javascript:void(0);" style="float: right; font-weight: bold; color: rgb(255, 255, 255); cursor: pointer; font-family: verdana; margin: 2px 5px; padding: 3px 8px;">X</a>
			</div>
			<div id="percentOfAttorneysChartDiv" class="hideClass">0 Results, Please try a different search</div>
				<div class="flRight charts" id="No-Data-PercAttorney">
				<form:form  commandName="percentageAttorneysChartModelBean" method="post" action="${updateChartURL}" id="percentageAttorneysChartModelBean">
					<ul class="reset listView">
					<c:if test="<%=isHomePage%>" >
						<li id="firmStatHomeSearch_perc" style="overflow:hidden;"><a href="javascript:void(0);" class="filter-icon" >&nbsp;</a></li>
					</c:if>	
						<li id="firmStatHomeSearch_perc_settings"><a href="#percentAttorneysViewSetting"
							class="btn icon settingsgry rightViewSetting login-window chartViewSetting"
							onclick="return false;">&nbsp;</a></li>
						<li>
							<a href="javascript:void(0);" id="percentOfAttorneyPrintCharts" onclick="return false;" class="printChartClass"></a>
						</li>
						<li>
							<a href="javascript:void(0);" onclick="return false;" class="exportChartClass"></a>
                        <div class="actionSec">
                        <h5>Actions</h5>
                        <ul class="reset">
                            <li class="exportChartImage"><span id="percentOfAttorneyExportJPG">Export as JPG</span></li>
                            <li class="exportChartImage"><span id="percentOfAttorneyExportPNG">Export as PNG</span></li>
                        </ul>
                        <div class="clear">&nbsp;</div>
                        </div>
						</li>
					</ul>
<%-- View Settings Start--%>
					<div style="display: none" class="viewBox popusdiv ClickPopup" id="percentAttorneysViewSetting" style="width: 540px;">
                            <div class="popHeader"> <a href="javascript:void(0);" class="btn icon closewhite closeOne flRight" style="margin-top: 2px">Close</a> SETTINGS: ${percentAttorneysChartTitle}
                              <div class="clear">&nbsp;</div>
                            </div>
                            <div class="section-one" style="width: 175px;">
                              <h6>Chart Type</h6>
                                <ul class="reset list4">
                                  <li>
                                  <form:radiobutton path="chartType" class="graphType" value="<%= BaseChartModelBean.ChartType.STACKED_HORIZONTAL_BAR.getValue() %>" />
                                    <span class="btn icon barcharthori">Stacked Horizontal Bar</span></li>
                                  <li>
                                    <form:radiobutton path="chartType" class="graphType" value="<%=BaseChartModelBean.ChartType.STACKED_VERTICAL_BAR.getValue()%>" />
                                    <span class="btn icon stackedbarchart">Stacked Vertical Bar</span></li>
                                </ul>
                                <div class="clear">&nbsp;</div>
                            </div>
                            <div class="section-two" style="width: 180px;">
                                <h6>Firm Data (Limit of 15)</h6>
                                <ul class="reset list4">
                                  <li>
                                    <form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.TOP_15.getValue() %>" />
                                    <span class="">Top 15 by % of Partners</span></li>
                                  <li>
                                     <form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.BOTTOM_15.getValue() %>" />
                                    <span class="">Bottom 15 by % of Partners</span></li>
                                  <li>
                                    <form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.FIRM.getValue() %>" />
                                    <span class="">Selected Firms:</span>
                                     <form:select path="searchResultsFirmList" multiple="true" size="4" style="width:150px;" class="percentOfAttorneySearchResultFirm">
                                        <form:options items="${allSearchResultsFirmList}" itemLabel="company" itemValue="companyId"/>
                                     </form:select>
                                    </li>
                                </ul>
                                <div class="clear">&nbsp;</div>
                            </div>
                            <div class="section-three" style="width:180px;">
                              <div class="martp2">
                                <h6>Comparison Data</h6>
                                <div class="marbtm2 martp1">
								<!-- As per client requirement this options are hide. -->
								<p class="marbtm1" style="display: none;">
								<c:choose>
									<c:when test="${doIHaveFirm}">
										<form:checkbox path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.MY_FIRM.getValue() %>"/>
										<span>My Firm</span>
									</c:when>
									<c:otherwise>
										<form:checkbox path="comparisonDataTypeList" disabled="true" value="<%= BaseChartModelBean.ComparisonDataType.MY_FIRM.getValue() %>"/>
										<span title="You are not associated with any firm.">My Firm</span>
									</c:otherwise>
								</c:choose>
								</p>
								<p class="marbtm1">
									<form:checkbox path="comparisonDataTypeList" value="<%=BaseChartModelBean.ComparisonDataType.RIVAL_EDGE.getValue()%>"/>
                                     <form:select path="firmList" multiple="false" class="percentAttorneyFirmList">
										<form:options items="${allRankingList}"/>
									 </form:select>
								</p>
								<p class="marbtm1-last">
								<c:choose>
									<c:when test="${not empty allWatchLists}">
										<form:checkbox path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.WATCHLIST_AVG.getValue() %>" />
	                                 	<span>Watchlist</span>
	                               		<form:select path="watchList" multiple="true" size="4" class="percentAttorneyWatchList">
	                           		 		<form:options items="${allWatchLists}" itemValue="groupId" itemLabel="groupName"/>
										</form:select>
									</c:when>
									<c:otherwise>
										<form:checkbox path="comparisonDataTypeList" disabled="true" value="<%= BaseChartModelBean.ComparisonDataType.WATCHLIST_AVG.getValue() %>" />
	                                 		<span title="You do not have any watchlists set up.">Watchlist</span>
									</c:otherwise>
								</c:choose>
                                 	
                                </p>
                                <p class="marbtm1-last">
                                   <form:checkbox path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.AVERAGE.getValue() %>" />
                                    Average of Firms in Search </p>
							</div>
                              </div>
                              <div class="clear">&nbsp;</div>
                              </div>
                            <div class="clear">&nbsp;</div>
                            <hr>
                            <div class="btmdiv" style="margin:0; border:none;">
                                <input type="button" value="Reset All" class="buttonTwo flLeft settingReset" id="percentAttorneyResetAll"/>
                                <input type="button" class="buttonTwo flRight rightReset" value="Cancel" id="percentAttorneysCancel" >
                                <input type="button" value="Apply" class="buttonOne flRight" style="margin: 0 5px 0 0;"  id="percentAttorneysApply" onclick="applyChartSettings('#percentageAttorneysChartModelBean');"/>
                            <div class="clear">&nbsp;</div>
                            </div>
                         </div>
<%--View Settings End --%>
				</form:form>
            </div>	
			
			<div id="percentOfAttorneysChartContainer" class="charts-spacing" style="position: relative;">
		</div>
	</div>
</div>