<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>

<portlet:defineObjects />

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ALM Rival Edge</title>
<link rel="shortcut icon" type="<%= request.getContextPath() %>/images/x-icon"	href="images/favicon.ico" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui.css" />
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/main.css" />
 
<script src="/re-theme/js/jquery-1.9.1.js"></script>
<script src="/re-theme/js/jquery-ui.js"></script> 
<script src="/re-theme/js/s_code.js"></script>

<link rel="stylesheet" href="/re-theme/css/main.css?browserId=other&minifierType=css&languageId=en_US&t=1381131532000" />

</head>

<body style="background: #fff">
	<div class="printSec">
		<div class="topBar">
			<ul class="tplist reset flRight">
				<li><a href="#" onClick="window.print()">Print</a></li>
				<li><a href="#" onClick="window.close()">Close</a></li>
			</ul>
			<div class="clear">&nbsp;</div>
		</div>
		<div>
			<div class="dateSec flRight">Date Printed : ${printedDateStr} at ${formattedTime}</div>
			<h1 id="logo">
				<a href="#"><img
					src="<%= request.getContextPath() %>/images/logo.png"	width="154" height="27" alt="logo" /></a>
			</h1>
			<div class="clear">&nbsp;</div>
		</div>
		<h4>Firm Statistics</h4>
		<!-- Display selected search Criteria string -->
		
		<%@ include file="searchCriteriaDisplay.jspf" %>
		
		<%@ include file="drillDownResults.jsp" %>
	
	
	</div>

</body>
</html>