<%@page import="com.alm.rivaledge.model.chart.BaseChartModelBean"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="java.util.Map"%>
<%@ taglib prefix="portlet" 		uri="http://java.sun.com/portlet_2_0"%> 
<%@ taglib prefix="c" 			 	uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"          uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form"            uri="http://www.springframework.org/tags/form"%>

<portlet:defineObjects/>

<%@ include file="./common.jsp" %>

<portlet:actionURL var="updateChartURL">
	<portlet:param name="action" value="updateChart"/>
</portlet:actionURL>

<portlet:actionURL var="removePortletURL">
	<portlet:param name="action" value="removePortleFromThePage"/>
</portlet:actionURL>


<script type="text/javascript">

var <portlet:namespace/>thisPortletId = '<%= themeDisplay.getPortletDisplay().getId() %>';

function removeThisChartNumberOfPractices(){
	
	var removePortletAction = '<%= removePortletURL.toString()%>';
	var r=confirm("Are you sure you want to delete this chart?");
	if (r == true)
  	{
		Liferay.Portlet.showBusyIcon("#bodyId", "Loading...");
		$("#numberOfPracticesChartModelBean").attr('action', removePortletAction);
		$("#numberOfPracticesChartModelBean").submit();
  	}
	else
	{
	  return false;
	}
}

<%@ include file="./theme.jsp" %>

	<c:choose>
		<c:when test="${numOfPracticeChartType eq 'stacked_horizontal_bar' }">
			var numPractices_typeOfChart = 'bar';	//This will be type of chart.
		</c:when> 
		<c:when test="${numOfPracticeChartType eq 'stacked_vertical_bar' }">
			var numPractices_typeOfChart = 'column';	
		</c:when>
		<c:when test="${numOfPracticeChartType eq 'horizontal_bar' }">
			var numPractices_typeOfChart = 'bar';		
		</c:when>
		<c:otherwise>
			var numPractices_typeOfChart = 'column';		 
		</c:otherwise>	
	</c:choose>

	var numPractice_titleOfChart 		= '${numOfPracticeChartTitle}'; 		// This will be title of chart.
	var numPracticesFirmNameList 		= [${numAttorneysFirmNameList}]; 	// This will be list of firms name.
	var practicesCount 					= [${practicesCount}]; 	 // This will be news count of individual firms.
	var practicesToolTipTitle 			= [${practicesToolTipTitle}]; 
	var practicesToolTipCount 			= [${practicesToolTipCount}]; 
	<c:choose>
	<c:when test="${(not empty numAttorneysCompareCount) and (numAttorneysCompareCount > 0)}">
		var numAttorneysCompareCount = ${numAttorneysCompareCount};
	</c:when> 	
	<c:otherwise>
		var numAttorneysCompareCount = 0;
	</c:otherwise>	
	</c:choose>

$(function()
{	


	
	// When user clicks on analysis tab active-menu css class is removed from details tab.
	// And same class added to analysis tab.
	$("#firm-statistics-details").removeClass("active-menu");
	$("#firm-statistics-analysis").addClass("active-menu");
	
	if(<%=!isHomePage%>)
	{
		if (numPracticesFirmNameList == null || numPracticesFirmNameList == '') 
		{
			$("#numberOfPracticesChartDiv").removeClass("hideClass");
			$("#numberOfPracticesChartContainer").addClass("hideClass");
			
			$("#p_p_id<portlet:namespace/>").hide();
			$(".cph").show();
			return;
		}
	}
	else
	{
		if (numPracticesFirmNameList == null || numPracticesFirmNameList == '')
		{
			$("#numberOfPracticesChartDiv").removeClass("hideClass");
			$("#numberOfPracticesChartContainer").addClass("hideClass");
				
			$("#firmStatHomeSearch_noofPractice_settings").hide();
			$("#No-Data-NoOfPractice").addClass("No-Data-Charts");
		}
		else
		{
			$("#No-Data-NoOfPractice").removeClass("No-Data-Charts");
			$("#numberOfPracticesChartDiv").addClass("hideClass");
		}
	}
	
	$(".closewhite, #numOfPracticeCancel").click(function(){
		$("#numberOfPracticesViewSetting").hide();
	});
	
	$("#numberOfAttorneysApply").click(function(){
		$("#numberOfPracticesViewSetting").hide();
	});
	
	$("#numOfPracticeResetAll").click(function(){
		$('#numberOfPracticesViewSetting').find("option").attr("selected", false);
	    $('#numberOfPracticesViewSetting').find('input[type=checkbox]:checked').removeAttr('checked');
	    $('#numberOfPracticesViewSetting').find('input[type=radio]:checked').removeAttr('checked');
	    
	    $( "input[value='<%=BaseChartModelBean.ChartType.STACKED_VERTICAL_BAR.getValue()%>']" ).prop("checked", true);
	    $( "input[value='<%=BaseChartModelBean.FirmDataType.TOP_15.getValue() %>']" ).prop("checked", true);
	    $( "input[value='<%=BaseChartModelBean.ComparisonDataType.RIVAL_EDGE.getValue()%>']" ).prop("checked", true);
	    $( "input[value='<%= BaseChartModelBean.ComparisonDataType.AVERAGE.getValue() %>']" ).prop("checked", true);
	    $('#firmList').val("AmLaw 100");
	});
	
	$(".noOfPracticeSearchResultFirm").click(function(){
		if($(".noOfPracticeSearchResultFirm option:selected")){
			$("input[value=firm]").prop("checked", true);
		}
	});
	$(".noOfPracticeFirmList").change(function(){
		$("input[value='RivalEdge Average']").prop("checked", true);
	});
	$(".noOfPracticeWatchList").click(function(){
		if($(".noOfPracticeWatchList option:selected")){
			$("input[value='Watchlist Average']").prop("checked", true);
		}
	});
	
	
	$('#noOfPracticePrintCharts').click(function() {
        var chart = $('#numberOfPracticesChartContainer').highcharts();
        chart.print();
    });
	$('#noOfPracticeExportJPG').click(function() {
        var chart = $('#numberOfPracticesChartContainer').highcharts();
        chart.exportChart({type: 'image/jpeg'});
    });
	$('#noOfPracticeExportPNG').click(function() {
        var chart = $('#numberOfPracticesChartContainer').highcharts();
        chart.exportChart({type: 'image/png'});
    });
	
	var numAttorneysPracticeCountList = [];
	for(var index = 0; index < numPracticesFirmNameList.length; index++)
	{
		 numAttorneysPracticeCountList.push(practicesCount[index]);
	}  
	var splitLineNumAttorneysCompareCount = parseFloat(numAttorneysCompareCount) - parseFloat(0.5);
	
	var labelXPosition = 0;
	var labelYPosition = 0;
	var labelRotation = 0;
	
	if(numPractices_typeOfChart == 'bar')
	{
		labelXPosition = -5;
		labelYPosition = 0;
		labelRotation = 0;
	}
	else
	{
		labelXPosition = 5;
		labelYPosition = 10;
		labelRotation = -45;
	}
	
	$('#numberOfPracticesChartContainer').highcharts(
	{
        chart: 
        {
            type: numPractices_typeOfChart
        },
        title: 
        {
            text: numPractice_titleOfChart
        },
        xAxis: 
        {
            categories: numPracticesFirmNameList,
            labels: 
            {
                rotation: labelRotation,
				y: labelYPosition,
				x: labelXPosition,
				align: 'right',
				formatter: function()
				{
					var firmName = this.value;
					if(firmName.length > 10)
					{
						firmName = firmName.substring(0, 10) + "...";
					}
					return firmName;
				}
			},
			 plotLines: [{
	                color: '#000000',
	                width: 2,
	                dashStyle: 'Solid',
	                value: parseFloat(splitLineNumAttorneysCompareCount)
	            }]
        },
        yAxis: 
        {
        	gridLineWidth: 1,
			gridLineColor: '#cecece',
			minorGridLineColor: '#cecece',
			lineWidth: 1,
            stackLabels: 
            {
                enabled: false,
                style: 
                {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: 
        {
        	enabled: true
    	},
    	tooltip : {
    		useHTML : true,
			positioner : function(boxWidth, boxHeight,
					point) {
					//$("#numberOfPracticesChartContainer .highcharts-tooltip span").css("height", "155px").css("width", "225px");
					var xPosition = point.plotX - 50;
					var yPosition = point.plotY - boxHeight + 40;
					
					if(numPractices_typeOfChart == 'column')
					{
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(75))
						{
							xPosition = point.plotX-170;
						}
						if((parseInt(point.plotX) - parseInt(boxWidth)) < parseInt(-150))
						{
							xPosition = point.plotX + 50;
						}
						if((parseInt(point.plotY) - parseInt(boxHeight)) < 0)
						{
							yPosition = point.plotY+50;
						}
					}
					else
					{
						xPosition = point.plotX + 100;
						yPosition = point.plotY - boxHeight + 100;
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(65))
						{
							xPosition = point.plotX-80;
						}
						
						if((parseInt(boxHeight) - parseInt(point.plotY)) > parseInt(100))
						{
							yPosition = point.plotY - boxHeight + 150;
						}
					}
					
				return {
					x : xPosition,
					y : yPosition
				};
			},
			formatter: function()
			{
				
				//var url = "firm-statistics-details?drilldownPracticeArea=" + encodeURIComponent(this.key) + searchCriteria;
				var pointerPosition = 0;
				for(var index = 0; index < numPracticesFirmNameList.length; index++)
				{
					if(this.key == numPracticesFirmNameList[index])
					{
						pointerPosition = index;
					}
				}
				var tooltipOption = '';
				tooltipOption += '<span class="Tooltip-Heading">' + this.key + '</span><br/><b> Total Practices - ' + this.total + '</b><br/><b> Top Practices </b><br/>';
				
				for(var index=0; index<practicesToolTipTitle[pointerPosition].length; index++)
				{
					tooltipOption += '<b><span title="'+ practicesToolTipTitle[pointerPosition][index] +'">' + (practicesToolTipTitle[pointerPosition][index].length > 15 ? practicesToolTipTitle[pointerPosition][index].substring(0, 15) + '...' : practicesToolTipTitle[pointerPosition][index]) + '</span></b> - ' + practicesToolTipCount[pointerPosition][index] + ' Attorneys<br/>';
				}
				
				if(pointerPosition >= numAttorneysCompareCount)
				{
					var url = encodeURI("firm-statistics-details?drilldownFirmName='"+this.key+"'") + searchCriteria;
					tooltipOption += '<div><span class="clickToViewDetails"><a href="'+url+'" style="color:#000;">Click to View Details</a></span></div>';
				}
					
									
				return tooltipOption;
			}
		},
        plotOptions: 
        {
            series: 
            {
            	pointWidth: 15,
            	stacking: 'normal',
            	dataLabels: 
            	{
                    enabled: false,
                    color: '#FFFFFF'
                },
                point: 
                {
                    events: 
                    {
                        mouseOver: function(e) 
                        {}
                    }
                    
                }
            }
        },
        series: [
        {
            name: 'Practices',
            color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#506a85'],
					[1, '#15375c']
				]
			},
            data: numAttorneysPracticeCountList
        }],
		navigation: {
            buttonOptions: {
                enabled: false
            }
        }
    });
    
});

$(document).ready(function()
{
	 $("#firmStatHomeSearch_noofPractice").click(function()
			    {
			    	Liferay.Portlet.showPopup(
			    		{
			    			uri : '${chartSearchPortletURL}', // defined in common.jsp
			    			title: "Search Criteria"
			    		});
			    });	
	 
});

</script>
 
<div class="newspublicationPage marbtm4">
		<div class="colMin flLeft leftDynamicDiv">
			<div class="topHeader ForChartsTopHeader">
				<span title="Remove this chart" onclick="removeThisChartNumberOfPractices();" style="float: right; font-weight: bold; color: rgb(255, 255, 255); cursor: pointer; font-family: verdana; margin: 2px 5px; padding: 3px 8px;">X</span>
			</div>
			<div id="numberOfPracticesChartDiv" class="hideClass">0 Results, Please try a different search</div>
				<div class="flRight charts" id="No-Data-NoOfPractice">
					<ul class="reset listView">
						<c:if test="<%=isHomePage%>" >
							<li id="firmStatHomeSearch_noofPractice" style="overflow:hidden;"><a href="javascript:void(0);" class="filter-icon" >&nbsp;</a></li>
						</c:if>
						<li id="firmStatHomeSearch_noofPractice_settings"><a href="#numberOfPracticesViewSetting"
							class="btn icon settingsgry rightViewSetting login-window chartViewSetting"
							onclick="return false;">&nbsp;</a></li>
						<li>
							<a href="javascript:void(0);" id="noOfPracticePrintCharts" onclick="return false;" class="printChartClass"></a>
						</li>
						<li>
							<a href="javascript:void(0);" onclick="return false;" class="exportChartClass"></a>
                        <div class="actionSec">
                        <h5>Actions</h5>
                        <ul class="reset">
                            <li class="exportChartImage"><span id="noOfPracticeExportJPG">Export as JPG</span></li>
                            <li class="exportChartImage"><span id="noOfPracticeExportPNG">Export as PNG</span></li>
                        </ul>
                        <div class="clear">&nbsp;</div>
                        </div>
						</li>
					</ul>
							<div style="display: none" class="viewBox popusdiv ClickPopup" id="numberOfPracticesViewSetting">
							<form:form  commandName="numberOfPracticesChartModelBean" method="post" action="${updateChartURL}" id="numberOfPracticesChartModelBean">
                            <div class="popHeader"> <a href="javascript:void(0);" class="btn icon closewhite closeOne flRight" style="margin-top: 2px">Close</a> SETTINGS: NUMBER OF PRACTICE AREA BY FIRM CHART
                              <div class="clear">&nbsp;</div>
                            </div>
                            <div class="section-one" style="width: 175px;">
                              <h6>Chart Type</h6>
                                <ul class="reset list4">
                                     <li>
                                      <form:radiobutton path="chartType" class="graphType" value="<%= BaseChartModelBean.ChartType.STACKED_HORIZONTAL_BAR.getValue() %>" />
                                      <span class="btn icon barcharthori">Stacked&nbsp;Horizontal&nbsp;Bar</span>
                                    </li>
                                    <li>
                                    <form:radiobutton path="chartType" class="graphType" value="<%=BaseChartModelBean.ChartType.STACKED_VERTICAL_BAR.getValue()%>" />
                                    <span class="btn icon stackedbarchart">Stacked&nbsp;Vertical&nbsp;Bar</span>
                                   </li>
                                    <li>
                                    <form:radiobutton path="chartType" class="graphType" value="<%=BaseChartModelBean.ChartType.VERTICAL_BAR.getValue()%>" />
                                    <span class="btn icon barchartvert">Vertical&nbsp;Bar</span>
                                   </li>
                                    <li>
                                    <form:radiobutton path="chartType" class="graphType" value="<%= BaseChartModelBean.ChartType.HORIZONTAL_BAR.getValue() %>" />
                                    <span class="btn icon barcharthori">Horizontal Bar</span>
                                   </li>
                                </ul>
                                <div class="clear">&nbsp;</div>
                            </div>
                            <div class="section-two">
                                <h6>Firm Data (Limit of 15)</h6>
                                <ul class="reset list4">
                                  <li>
                                    <form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.TOP_15.getValue() %>" />
                                    <span class="">Top 15 by No of Attorneys</span></li>
                                  <li>
                                     <form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.BOTTOM_15.getValue() %>" />
                                    <span class="">Bottom 15 by No of Attorneys</span></li>
                                  <li>
                                     <form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.FIRM.getValue() %>" />
                                    <span class="">Selected Firms:</span>
                                     <form:select path="searchResultsFirmList" multiple="true" size="4" style="width:150px;" class="noOfPracticeSearchResultFirm">
										<form:options items="${allSearchResultsFirmList}" itemLabel="company" itemValue="companyId"/>
									 </form:select>
                                    </li>
                                </ul>
                                <div class="clear">&nbsp;</div>
                            </div>
                            <div class="section-three"  style="width:210px;">
                              <div class="martp2">
                                <h6>Comparison Data</h6>
                                <div class="marbtm2 martp1">
                                  	<!-- As per client requirement this options are hide. -->
									<p class="marbtm1" style="display: none;">                                  
                                  	<c:choose>
									<c:when test="${doIHaveFirm}">
										<form:checkbox path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.MY_FIRM.getValue() %>"/>
										<span>My Firm</span>
									</c:when>
									<c:otherwise>
										<form:checkbox path="comparisonDataTypeList" disabled="true" value="<%= BaseChartModelBean.ComparisonDataType.MY_FIRM.getValue() %>"/>
										<span title="You are not associated with any firm.">My Firm</span>
									</c:otherwise>
								   </c:choose>                                    
                                  </p>
                                  <p class="marbtm1">
                                   <form:checkbox path="comparisonDataTypeList" value="<%=BaseChartModelBean.ComparisonDataType.RIVAL_EDGE.getValue()%>" />
                                     <form:select path="firmList" multiple="false" class="noOfPracticeFirmList">
										<form:options items="${allRankingList}" />
									 </form:select>
                                   </p>
                                  <p class="marbtm1">
                                   <c:choose>
									<c:when test="${not empty allWatchLists}">
										<form:checkbox path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.WATCHLIST_AVG.getValue() %>" />
	                                 	<span>Watchlist</span>
	                               		<form:select path="watchList" multiple="true" size="4" class="noOfPracticeWatchList">
	                           		 		<form:options items="${allWatchLists}" itemValue="groupId" itemLabel="groupName"/>
										</form:select>
									</c:when>
									<c:otherwise>
										<form:checkbox path="comparisonDataTypeList" disabled="true" value="<%= BaseChartModelBean.ComparisonDataType.WATCHLIST_AVG.getValue() %>" />
	                                 		<span title="You do not have any watchlists set up.">Watchlist</span>
									</c:otherwise>
								</c:choose>
                                  </p>
                                   <p class="marbtm1-last">
                                   <form:checkbox path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.AVERAGE.getValue() %>" />
                                    Average of Firms in Search </p>
                                </div>
                              </div>
                              <div class="clear">&nbsp;</div>
                            </div>
                            <div class="clear">&nbsp;</div>
                            <hr>
                            <div class="btmdiv">
                                <input value="Reset All" class="buttonTwo flLeft settingReset" type="button" id="numOfPracticeResetAll"/>
                                <input type="button" class="buttonTwo flRight rightReset" value="Cancel" id="numOfPracticeCancel" >
                                <input value="Apply" class="buttonOne flRight" id="numberOfAttorneysApply" type="button" style="margin: 0 5px 0 0;" onclick="applyChartSettings('#numberOfPracticesChartModelBean');">
                                <div class="clear">&nbsp;</div>
                              </div>
                            </form:form>
                          </div>
                    </div>				
		     
			<div id="numberOfPracticesChartContainer" class="charts-spacing">
		</div>
	
	</div>
</div>