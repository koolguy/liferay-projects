<%@page import="com.alm.rivaledge.transferobject.PeopleResult"%>
<%@page import="com.alm.rivaledge.transferobject.PeopleResultDTO"%>
<%@page import="com.alm.rivaledge.model.PeopleSearchModelBean"%>
<%@page import="com.alm.rivaledge.util.ALMConstants"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.alm.rivaledge.model.chart.BaseChartModelBean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="liferay-portlet" uri="http://liferay.com/tld/portlet"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="liferay-util" uri="http://liferay.com/tld/util"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<portlet:defineObjects/>

<%@ include file="./common.jsp" %>



<portlet:actionURL var="netChangeAttorneyURL">
	<portlet:param name="action" value="netChangeAttorneysChartCriteriaChange"/>
</portlet:actionURL>

<portlet:actionURL var="removePortletURL">
	<portlet:param name="action" value="removePortleFromThePage"/>
</portlet:actionURL>


<script type="text/javascript">


function removeThisChartNetChangeAttorneyChart(){
	
	var removePortletAction = '<%= removePortletURL.toString()%>';
	//alert('INSIDE removeThisChart() method '+removePortletAction);
	var r=confirm("Are you sure you want to delete this chart?");
	if (r == true)
  	{
		Liferay.Portlet.showBusyIcon("#bodyId", "Loading...");
		$("#netAttorneyChangeModelBean").attr('action', removePortletAction);
		$("#netAttorneyChangeModelBean").submit();
  	}
	else
	{
	  return false;
	}
}

(function () {
	
<%@ include file="./theme.jsp" %>

<c:choose>
	<c:when test="${chartType eq 'stacked_horizontal_bar' }">
		var typeOfChart = 'bar';	//This will be type of chart.
		var stackingType = 'normal';
	</c:when> 
	<c:when test="${chartType eq 'stacked_vertical_bar' }">
		var typeOfChart = 'column';	
		var stackingType = 'normal';
	</c:when>
	<c:when test="${chartType eq 'horizontal_bar' }">
		var typeOfChart = 'bar';
		var stackingType = '';
	</c:when>
	<c:otherwise>
		var typeOfChart = 'column';
		var stackingType = '';
	</c:otherwise>	
</c:choose>

var sbFirmNames = [${sbFirmNames}];
var sbPartnersNet = [${sbPartnersNet}];
var sbAssociatesNet = [${sbAssociatesNet}];
var sbCounselsNet = [${sbCounselsNet}];
var netChangeSplit  	= ${comparisonCounter};		 	// This will be split line count.
$(function () {
Highcharts.setOptions({
  		colors: ['#2f6d9f', '#c1942c', '#35744a', '#0489B1', '#5F4C0B', '#B40404', '#380B61', '#DF7401', '#0A2229']
	});


	$("#netChangeAttorneyResetAll").click(function(){
		$('.graphType').attr('checked', false);
		$('.defaultChartType').prop('checked', true);
		$('.defaultLimitType').prop('checked', true);
		$("input[value=top_15]").prop("checked", true);
		$("#searchResultsNetChangeFirmList option:selected").prop("selected", false);
		$("#firmList > option").attr("selected",false);
		$('.percentChangeAverageValue').prop('checked', true);
		$('.percentChangeRankingCheck').prop('checked', true);
		$("#netChangeRankingList").val($("#netChangeRankingList option:first").val());
	});
	
	$("#searchResultsNetChangeFirmList").click(function(){
		if($("#searchResultsNetChangeFirmList option:selected")){
			$("input[value=firm]").prop("checked", true);
		}
	});
	$(".netChangeRankingList").click(function(){
		$("input[value='RivalEdge Average']").prop("checked", true);
	});
	$("#watchList").click(function(){
		if($("#watchList option:selected")){
			$("input[value='Watchlist Average']").prop("checked", true);
		}
	});
	
	$(".rightReset").click(function(){
		$("#netChangeAttorneyViewSettings").hide();
	});
	
	if(<%=!isHomePage%>)
	{
		if (sbFirmNames == null || sbFirmNames == '') 
		{
			$("#netChangeAttonrneyDiv").removeClass("hideClass");
			$("#netChangeAttonrneyChart").addClass("hideClass");
			$("#netAttorneyChangeModelBean").parent().find(".charts").hide(); // hide the settings icon
			
			$("#p_p_id<portlet:namespace/>").hide();
			$(".cph").show();
			return;
		}
	}
	else
	{
		if (sbFirmNames == null || sbFirmNames == '') 
		{
			$("#netChangeAttonrneyDiv").removeClass("hideClass");
			$("#netChangeAttonrneyChart").addClass("hideClass");
				
			$("#firmStatHomeSearch_netChange_settings").hide();
			$("#No-Data-NetChange").addClass("No-Data-Charts");
		}
		else
		{
			$("#No-Data-NetChange").removeClass("No-Data-Charts");
			$("#netChangeAttonrneyDiv").addClass("hideClass");	
		}
	}
	
	$(".AnalyzeResultHeader").show();
	
	// When user clicks on analysis tab active-menu css class is removed from details tab.
	// And same class added to analysis tab.
	$("#firms-details").removeClass("active-menu");
	$("#firms-analysis").addClass("active-menu");

	$("#netChangeAttorneyApply, .closewhite").click(function(){
		$("#netChangeAttorneyViewSettings").hide();
	});
	
	$('#netChangeAttorneyPrintCharts').click(function() {
        var chart = $('#netChangeAttonrneyChart').highcharts();
        chart.print();
    });
	$('#netChangeAttorneyExportCharts').click(function() {
        var chart = $('#netChangeAttonrneyChart').highcharts();
        chart.exportChart({type: 'image/jpeg'});
    });
	//var splitLineAmountNewsPubsCount = parseFloat(amountNewsPubsSplit) - parseFloat(0.5);
	
	var labelXPosition = 0;
	var labelYPosition = 0;
	var labelRotation = 0;
	if(typeOfChart == 'bar')
	{
		labelXPosition = -5;
		labelYPosition = 0;
		labelRotation = 0;
	}
	else
	{
		labelXPosition = 5;
		labelYPosition = 10;
		labelRotation = -45;
	}
	var associateAdd = [${sbAssociatesAdd}];
	var otherCounselAdd = [${sbCounselsAdd}];
	var partnerAdd = [${sbPartnersAdd}];
	
	var associateRemoved = [${sbAssociatesRemoved}];
	var otherConselRemoved = [${sbCounselsRemoved}];
	var partnerRemoved = [${sbPartnersRemoved}];
	
	var allNet = [${sbAllNet}];
	var allAdded = [${sbAllAdded}];
	var allRemoved = [${sbAllRemoved}];
	
	var splitLinenetChangeCount = parseFloat(netChangeSplit) - parseFloat(0.5);
	
   $('#netChangeAttonrneyChart').highcharts({
        chart: {
            type: typeOfChart
        },
        title: {
            text: '${chartTitle}'
        },
        xAxis: {
            categories: sbFirmNames,
				labels : {
				rotation: labelRotation,
				y: labelYPosition,
				x: labelXPosition,
				align: 'right',
					formatter : function() {
						var firmName = this.value;
						if (firmName.length > 15) {
							firmName = firmName
									.substring(0, 15)
									+ "...";
						}
						return firmName;
					}
				},
				plotLines : [ {
					color : '#000000',
					width : 2,
					dashStyle : 'Solid',
					value : parseFloat(splitLinenetChangeCount)
				} ]
        },
        yAxis : {
			gridLineWidth: 1,
			gridLineColor: '#cecece',
			minorGridLineColor: '#cecece',
			lineWidth: 1,
			title: {
				text: '',
				style: {
                    color: '#000'
                }
			},
			stackLabels : {
				enabled : false,
				style : {
					fontWeight : 'bold',
					color : (Highcharts.theme && Highcharts.theme.textColor)
							|| 'gray'
				}
			}
		},
		tooltip : {
			hideDelay: 1000,
			useHTML : true,
			positioner : function(boxWidth, boxHeight,
					point) {
					$("#netChangeAttonrneyChart .highcharts-tooltip span").css("width", "220px").css("word-wrap", "normal");
					var xPosition = point.plotX - 75;
					var yPosition = point.plotY - boxHeight + 30;
					
					if(typeOfChart == 'column')
					{
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(100))
						{
							xPosition = point.plotX - 175;
						}
						
						if(parseInt(point.plotY) < parseInt(boxHeight))
						{
							yPosition = point.plotY + 45;
						}
						if((parseInt(point.plotX) - parseInt(boxWidth)) < parseInt(-150))
						{
							xPosition = point.plotX + 35;
						}
					}
					else
					{
						xPosition = point.plotX + boxWidth + 200;
						yPosition = point.plotY - boxHeight + 100;
						
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(100))
						{
							xPosition = point.plotX-100;
						}
						
						if((parseInt(boxHeight) - parseInt(point.plotY)) > parseInt(100))
						{
							yPosition = point.plotY - boxHeight + 150;
						}
					}
					
				return {
					x : xPosition,
					y : yPosition
				};
			},
			formatter: function()
			{
				var pointerPosition = 0;
				for(var index = 0; index < sbFirmNames.length; index++)
				{
					if(this.key == sbFirmNames[index])
					{
						pointerPosition = index;
					}
				}
				
				var tooltipOption = '';
				tooltipOption += '<b>' + this.key + '</b><table><tr><td></td><td>Net</td><td>Added</td><td>Removed</td></tr><tr><td style="color: '+this.series.color+'; padding-left: 10px; margin-top: 15px;">All Attorneys</td><td style="border: 1px solid; background-color: #000; color: #fff; margin-top: 15px;">' + allNet[pointerPosition] + '</td><td style="border: 1px solid; background-color: #000; color: #fff; margin-top: 15px;">' + parseInt(allAdded[pointerPosition]) + '</td><td style="border: 1px solid; background-color: #000; color: #fff; margin-top: 15px;">' +  parseInt(allRemoved[pointerPosition]) + '</td></tr><tr><td style="color: #15375c; padding-left: 10px; margin-top: 15px;">Partners</td><td style="border: 1px solid; background-color: #15375c; color: #fff; margin-top: 15px;">' + sbPartnersNet[pointerPosition] + '</td><td style="border: 1px solid; background-color: #15375c; color: #fff; margin-top: 15px;">' + partnerAdd[pointerPosition] + '</td><td style="border: 1px solid; background-color: #15375c; color: #fff; margin-top: 15px;">' + partnerRemoved[pointerPosition] + '</td></tr><tr><td style="color: #ba323e; padding-left: 10px; margin-top: 15px;">Associates</td><td style="border: 1px solid; background-color: #ba323e; color: #fff; margin-top: 15px;">' + sbAssociatesNet[pointerPosition] + '</td><td style="border: 1px solid; background-color: #ba323e; color: #fff; margin-top: 15px;">' + associateAdd[pointerPosition] + '</td><td style="border: 1px solid; background-color: #ba323e; color: #fff; margin-top: 15px;">' + associateRemoved[pointerPosition] + '</td></tr><tr><td style="color: #de7c35; padding-left: 10px; margin-top: 15px;">Other Counsel</td><td style="border: 1px solid; background-color: #de7c35; color: #fff; margin-top: 15px;">' + sbCounselsNet[pointerPosition] + '</td><td style="border: 1px solid; background-color: #de7c35; color: #fff; margin-top: 15px;">' + otherCounselAdd[pointerPosition] + '</td><td style="border: 1px solid; background-color: #de7c35; color: #fff; margin-top: 15px;">' + otherConselRemoved[pointerPosition] + '</td></tr></table>';
				
				if(pointerPosition >= netChangeSplit)
				{
					var url = "firm-statistics-details?drilldownFirmName=" + encodeURIComponent(this.key) + searchCriteria;
					tooltipOption += '<div><span style="text-align:center;color:#000;"><a href="'+url+'" style="color:#000;">Click to View Details</a></span></div>';
				}
				
				return tooltipOption;
			}
		},
        plotOptions: {
                series: {
                	pointWidth: 15,
                    stacking: stackingType,
                    dataLabels: {
                        enabled: false,
                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'
                    }
                }
            },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Partners',
            color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#506a85'],
					[1, '#15375c']
				]
			},
            data: sbPartnersNet
        }, {
            name: 'Associates',
            color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#cb676f'],
					[1, '#ba323e']
				]
			},
            data: sbAssociatesNet
        }, {
            name: 'Other Counsel',
            color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#e59c67'],
					[1, '#de7c35']
				]
			},
            data: sbCounselsNet
        }],
		navigation: {
            buttonOptions: {
                enabled: false
            }
        }
    });
	
	var totalList = [];
	for(var index = 0; index < sbFirmNames.length; index++)
	{
		totalList.push(sbPartnersNet[index] + sbCounselsNet[index] + sbAssociatesNet[index]);
	}
	
	if(stackingType == '')
	{
		
		var chart = $("#netChangeAttonrneyChart").highcharts();
		
		chart.series[chart.series.length-1].remove();
		chart.series[chart.series.length-1].remove();
		chart.series[chart.series.length-1].remove();
		
		chart.addSeries({
			showInLegend: true,
			name: 'Total',
			data: totalList,
			color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#506a85'],
					[1, '#15375c']
				]
			}
		});
		
	}	
	
});

$(document).ready(function()
	{
			 $("#firmStatHomeSearch_netChange").click(function()
					    {
					    	Liferay.Portlet.showPopup(
					    		{
					    			uri : '${chartSearchPortletURL}', // defined in common.jsp
					    			title: "Search Criteria"
					    		});
					    });	
		   
	});
	
}());
		
</script>

<div class="newspublicationPage marbtm4">
	<div class="colMin flLeft leftDynamicDiv">
		<div class="topHeader ForChartsTopHeader">
			<span title="Remove this chart" onclick="removeThisChartNetChangeAttorneyChart();" style="float: right; font-weight: bold; color: rgb(255, 255, 255); cursor: pointer; font-family: verdana; margin: 2px 5px; padding: 3px 8px;">X</span>
		</div>
		<div id="netChangeAttonrneyDiv" class="hideClass">0 Results, Please try a different search</div>
		<form:form commandName="netAttorneyChangeModelBean" method="post" action="${netChangeAttorneyURL}" id="netAttorneyChangeModelBean">
			<div class="flRight charts" id="No-Data-NetChange">
				<ul class="reset listView">
					<c:if test="<%=isHomePage%>" >
						<li id="firmStatHomeSearch_netChange" style="overflow:hidden;"><a href="javascript:void(0);" class="filter-icon" >&nbsp;</a></li>
					</c:if>
					<li id="firmStatHomeSearch_netChange_settings"><a href="#netChangeAttorneyViewSettings"
						class="btn icon settingsgry rightViewSetting login-window chartViewSetting"
						onclick="return false;">&nbsp;</a></li>
					<li>
						<a href="javascript:void(0);" id="netChangeAttorneyPrintCharts" onclick="return false;" class="printChartClass"></a>
					</li>
					<li>
						<a href="javascript:void(0);" id="netChangeAttorneyExportCharts" onclick="return false;" class="exportChartClass"></a>
					</li>
				</ul>
				<div style="display: none; width:530px !important;" class="viewBox popusdiv ClickPopup" id="netChangeAttorneyViewSettings">
					<div class="popHeader">
						<a href="javascript:void(0);" class="btn icon closewhite closeOne flRight" style="margin-top: 2px">&nbsp;Close</a>
							SETTINGS: ${chartTitle}
						<div class="clear">&nbsp;</div>
					</div>
					<div class="section-four">
                      <h6>Chart Type</h6>
                        <ul class="reset list4">
                             <li>
                            <form:radiobutton path="chartType" class="graphType defaultChartType" value="<%=BaseChartModelBean.ChartType.STACKED_VERTICAL_BAR.getValue()%>" />
                            <span class="btn icon stackedbarchart">Stacked&nbsp;Vertical&nbsp;Bar</span>
                           	</li>
                            <li>
                              <form:radiobutton path="chartType" class="graphType" value="<%= BaseChartModelBean.ChartType.STACKED_HORIZONTAL_BAR.getValue() %>" />
                              <span class="btn icon barcharthori">Stacked&nbsp;Horizontal&nbsp;Bar</span>
                            </li>
                            <li>
                            <form:radiobutton path="chartType" class="graphType" value="<%=BaseChartModelBean.ChartType.VERTICAL_BAR.getValue()%>" />
                            <span class="btn icon barchartvert">Vertical&nbsp;Bar</span>
                           </li>
                            <li>
                            <form:radiobutton path="chartType" class="graphType" value="<%= BaseChartModelBean.ChartType.HORIZONTAL_BAR.getValue() %>" />
                            <span class="btn icon barcharthori">Horizontal&nbsp;Bar</span>
                           </li>
                        </ul>
                        <div class="clear">&nbsp;</div>
                    </div>
					<div class="section-two" style="width:170px;">
						<form action="#">
							<h6>Firm Data (Limit of 15)</h6>
							<ul class="reset list4">
								<li>
									<form:radiobutton path="limitType" class="defaultLimitType" value="<%= BaseChartModelBean.FirmDataType.TOP_15.getValue() %>"/>
									<span class="">Top 15 By Net Change</span></li>
								<li>
									<form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.BOTTOM_15.getValue() %>"/>
									<span class="">Bottom 15 By Net Change</span></li>
								<li>
									<form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.FIRM.getValue() %>"/>
									<span class="">Select Firms:</span> 
									<form:select path="searchResultsFirmList" id="searchResultsNetChangeFirmList" multiple="true" size="4" style="width:150px; margin:10px 0 0 0;">
										<form:options items="${peopleSearchResults}" itemLabel="companyName" itemValue="companyId"/>
									 </form:select>
								</li>
							</ul>
							<div class="clear">&nbsp;</div>
						</form>
					</div>
					<div class="section-three" style="width:180px;">
						<div class="martp2">
							<h6>Comparison Data</h6>
							<div class="marbtm2 martp1">
								<p class="marbtm1" style="display: none;">
								<c:choose>
									<c:when test="${doIHaveFirm}">
										<form:checkbox path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.MY_FIRM.getValue() %>"/>
										<span>My Firm</span>
									</c:when>
									<c:otherwise>
										<form:checkbox path="comparisonDataTypeList" disabled="true" value="<%= BaseChartModelBean.ComparisonDataType.MY_FIRM.getValue() %>"/>
										<span title="You are not associated with any firm.">My Firm</span>
									</c:otherwise>
								</c:choose>
								</p>
								<p class="marbtm1">
									<form:checkbox class="percentChangeRankingCheck" path="comparisonDataTypeList" value="<%=BaseChartModelBean.ComparisonDataType.RIVAL_EDGE.getValue()%>"/>
                                     <form:select class="netChangeRankingList" path="firmList" multiple="false">
										<form:options items="${allRankingList}"/>
									 </form:select>
								</p>
								<p class="marbtm1-last">
								<c:choose>
									<c:when test="${not empty allWatchLists}">
										<form:checkbox path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.WATCHLIST_AVG.getValue() %>" />
	                                 	<span>Watchlist</span>
	                               		<form:select path="watchList" multiple="true" size="4">
	                           		 		<form:options items="${allWatchLists}" itemValue="groupId" itemLabel="groupName"/>
										</form:select>
									</c:when>
									<c:otherwise>
										<form:checkbox  path="comparisonDataTypeList" disabled="true" value="<%= BaseChartModelBean.ComparisonDataType.WATCHLIST_AVG.getValue() %>" />
	                                 		<span title="You do not have any watchlists set up.">Watchlist</span>
									</c:otherwise>
								</c:choose>
                                 	
                                </p>
                                <p class="marbtm1-last">
                                   <form:checkbox class="percentChangeAverageValue" path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.AVERAGE.getValue() %>" />
                                    Average of Firms in Search </p>
							</div>
						</div>
						<div class="clear">&nbsp;</div>
					</div>
					<div class="clear">&nbsp;</div>
					<hr>
					<div class="btmdiv">
						<input type="button" value="Reset All" class="buttonTwo flLeft settingReset"  id="netChangeAttorneyResetAll">
						<input type="button" class="buttonTwo flRight rightReset" value="Cancel">
						<input type="button" value="Apply" class="buttonOne flRight" id="netChangeAttorneyApply" style="margin: 0 5px 0 0;" onclick="applyChartSettings('#netAttorneyChangeModelBean');">
						<div class="clear">&nbsp;</div>
					</div>
				</div>
			</div>
		</form:form>
		
		<div id="netChangeAttonrneyChart" class="charts-spacing"></div>
	</div>

</div>

