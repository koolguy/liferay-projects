<%@page import="com.alm.rivaledge.model.chart.BaseChartModelBean"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="java.util.Map"%>
<%@ taglib prefix="portlet" 		uri="http://java.sun.com/portlet_2_0"%> 
<%@ taglib prefix="c" 			 	uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring"          uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form"            uri="http://www.springframework.org/tags/form"%>

<portlet:defineObjects/>

<%@ include file="./common.jsp" %>

<portlet:actionURL var="updateChartURL">
	<portlet:param name="action" value="updateChart"/>
</portlet:actionURL>

<portlet:actionURL var="removePortletURL">
	<portlet:param name="action" value="removePortleFromThePage"/>
</portlet:actionURL>


<script type="text/javascript">

var <portlet:namespace/>thisPortletId = '<%= themeDisplay.getPortletDisplay().getId() %>';

function removeThisChartNoAttorneyByPractice(){
	
	var removePortletAction = '<%= removePortletURL.toString()%>';
	var r=confirm("Are you sure you want to delete this chart?");
	if (r == true)
  	{
		Liferay.Portlet.showBusyIcon("#bodyId", "Loading...");
		$("#numberOfAttorneysByPracticeChartModelBean").attr('action', removePortletAction);
		$("#numberOfAttorneysByPracticeChartModelBean").submit();
  	}
	else
	{
	  return false;
	}
}

<%@ include file="./theme.jsp" %>

	<c:choose>
		<c:when test="${numAttorneysByPracticeChartType eq 'stacked_horizontal_bar' }">
			var numAttorneysByPractice_typeOfChart = 'bar';	//This will be type of chart.
			var stackingType = 'normal';
		</c:when> 
		<c:when test="${numAttorneysByPracticeChartType eq 'stacked_vertical_bar' }">
			var numAttorneysByPractice_typeOfChart = 'column';
			var stackingType = 'normal';
		</c:when>
		<c:when test="${numAttorneysByPracticeChartType eq 'horizontal_bar' }">
			var numAttorneysByPractice_typeOfChart = 'bar';
			var stackingType = '';
		</c:when>
		<c:otherwise>
			var numAttorneysByPractice_typeOfChart = 'column';
			var stackingType = '';		 
		</c:otherwise>	
	</c:choose>

	var numAttorneysByPractice_titleOfChart 		= '${numAttorneysByPracticeChartTitle}'; 		// This will be title of chart.
	var numAttorneysByPracticeNameList 		= [${numAttorneysByPracticeNameList}]; 	// This will be list of firms name.
	var numAttorneysByPracticePartnerCount 		= [${numAttorneysByPracticePartnerCount}]; 	 // This will be news count of individual firms.
	var numAttorneysByPracticeAssociateCount 		= [${numAttorneysByPracticeAssociateCount}]; 	// This will be publication count of individual firms.
	var numAttorneysByPracticeOtherCounselCount 	= [${numAttorneysByPracticeOtherCounselCount}]; // This will be twitter count of individual firms. 


$(function()
{
	if(<%=!isHomePage%>)
	{
		if (numAttorneysByPracticeNameList == null || numAttorneysByPracticeNameList == '') 
		{
			$("#numberOfAttorneysByPracticeChartDiv").removeClass("hideClass");
			$("#numberOfAttorneysByPracticeChartContainer").addClass("hideClass");
			//$("#numberOfAttorneysByPracticeChartDiv").parent().find(".charts").hide(); // hide the settings icon
			
			$("#p_p_id<portlet:namespace/>").hide();
			$(".cph").show();
			return;
		}
	}
	else
	{
		if (numAttorneysByPracticeNameList == null || numAttorneysByPracticeNameList == '')
		{
			$("#numberOfAttorneysByPracticeChartDiv").removeClass("hideClass");
			$("#numberOfAttorneysByPracticeChartContainer").addClass("hideClass");
				
			$("#firmStatHomeSearch_noofAttorney_settings").hide();
			$("#No-Data-NoOfAttPrac").addClass("No-Data-Charts");
		}
		else
		{
			$("#No-Data-NoOfAttPrac").removeClass("No-Data-Charts");
			$("#numberOfAttorneysByPracticeChartDiv").addClass("hideClass");
		}
	}
	
	$(".AnalyzeResultHeader").show();
	
	// When user clicks on analysis tab active-menu css class is removed from details tab.
	// And same class added to analysis tab.
	$("#firm-statistics-details").removeClass("active-menu");
	$("#firm-statistics-analysis").addClass("active-menu");
	
	$("#numberOfAttorneysByPracticeApply, .closewhite").click(function(){
		$("#numberOfAttorneysByPracticeViewSetting").hide();
	});
	
	$("#noOfAttorneyPracticeResetAll").click(function(){
		$("input[value=stacked_vertical_bar]").prop("checked", true);
		$("input[value=top_15]").prop("checked", true);
		$("#searchResultsFirmList > option").attr("selected",false);
		$("#practiceAreaList > option").attr("selected",false);
	});
	
	$('#noOfAttorneyPracticePrintCharts').click(function() {
        var chart = $('#numberOfAttorneysByPracticeChartContainer').highcharts();
        chart.print();
    });
	$('#noOfAttorneyPracticeExportJPG').click(function() {
        var chart = $('#numberOfAttorneysByPracticeChartContainer').highcharts();
        chart.exportChart({type: 'image/jpeg'});
    });
	$('#noOfAttorneyPracticeExportPNG').click(function() {
        var chart = $('#numberOfAttorneysByPracticeChartContainer').highcharts();
        chart.exportChart({type: 'image/png'});
    });
	
	var numAttorneysByPracticePartnerCountList = [];
	var numAttorneysByPracticeAssociateCountList = [];
	var numAttorneysByPracticeOtherCounselCountList = [];
	for(var index = 0; index < numAttorneysByPracticeNameList.length; index++)
	{
		numAttorneysByPracticePartnerCountList.push(numAttorneysByPracticePartnerCount[index]);
		numAttorneysByPracticeAssociateCountList.push(numAttorneysByPracticeAssociateCount[index]);
		numAttorneysByPracticeOtherCounselCountList.push(numAttorneysByPracticeOtherCounselCount[index]);
	}
	
	var labelXPosition = 0;
	var labelYPosition = 0;
	var labelRotation = 0;
	
	if(numAttorneysByPractice_typeOfChart == 'bar')
	{
		labelXPosition = -5;
		labelYPosition = 0;
		labelRotation = 0;
	}
	else
	{
		labelXPosition = 5;
		labelYPosition = 10;
		labelRotation = -45;
	}
	
	
	$('#numberOfAttorneysByPracticeChartContainer').highcharts(
	{
        chart: 
        {
            type: numAttorneysByPractice_typeOfChart
        },
        title: 
        {
            text: numAttorneysByPractice_titleOfChart
        },
        subtitle:
		{
			text: '${noOfAttorneySelectedFirmName}'
		},
        xAxis: 
        {
            categories: numAttorneysByPracticeNameList,
            labels: 
            {
                rotation: labelRotation,
				y: labelYPosition,
				x: labelXPosition,
				align: 'right',
				formatter: function()
				{
					var firmName = this.value;
					if(firmName.length > 10)
					{
						firmName = firmName.substring(0, 10) + "...";
					}
					return firmName;
				}
			}
        },
        yAxis: 
        {
        	gridLineWidth: 1,
			gridLineColor: '#cecece',
			minorGridLineColor: '#cecece',
			lineWidth: 1,
            stackLabels: 
            {
                enabled: false,
                style: 
                {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: 
        {
        	enabled: true
    	},
    	tooltip : {
			useHTML : true,
			positioner : function(boxWidth, boxHeight,
					point) {
					
					$("#numberOfAttorneysByPracticeChartContainer .highcharts-tooltip span").css("height", "125px").css("width", "175px").css("white-space", "normal");
					var xPosition = point.plotX - 20;
					var yPosition = point.plotY - boxHeight + 40;
					
					if(numAttorneysByPractice_typeOfChart == 'column')
					{
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(75))
						{
							xPosition = point.plotX-115;
						}
						
						if((parseInt(point.plotY) - parseInt(boxHeight)) < -75)
						{
							yPosition = point.plotY+50;
						}
					}
					else
					{
						xPosition = point.plotX + 100;
						yPosition = point.plotY - boxHeight + 100;
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(65))
						{
							xPosition = point.plotX-80;
						}
						
						if((parseInt(boxHeight) - parseInt(point.plotY)) > parseInt(100))
						{
							yPosition = point.plotY - boxHeight + 150;
						}
					}
					
				return {
					x : xPosition,
					y : yPosition
				};
			},
			formatter: function()
			{
				
				var pointerPosition = 0;
				for(var index = 0; index < numAttorneysByPracticeNameList.length; index++)
				{
					if(this.key == numAttorneysByPracticeNameList[index])
					{
						pointerPosition = index;
					}
				}
				
				var url = "firm-statistics-details?drilldownFirmName=" + encodeURIComponent('${numAttorneysByPracticeFirmName}') + "&drilldownPracticeArea=" + encodeURIComponent(this.key) + searchCriteria;
				var tooltipOption = '';
				var totalAmount = parseInt(numAttorneysByPracticePartnerCount[pointerPosition]) + parseInt(numAttorneysByPracticeAssociateCount[pointerPosition]) + parseInt(numAttorneysByPracticeOtherCounselCount[pointerPosition]);
				tooltipOption += this.key + '<table><tr><td style="color: '+this.series.color+'; padding-left: 10px; margin-top: 15px;">All Attorneys</td><td style="border: 1px solid; background-color: #000; color: #fff; margin-top: 15px;">' + totalAmount + '</td></tr><tr><td style="color: #15375c; padding-left: 10px; margin-top: 15px;">Partners</td><td style="border: 1px solid; background-color: #15375c; color: #fff; margin-top: 15px;">' + numAttorneysByPracticePartnerCount[pointerPosition] + '</td></tr><tr><td style="color: #ba323e; padding-left: 10px; margin-top: 15px;">Associates</td><td style="border: 1px solid; background-color: #ba323e; color: #fff; margin-top: 15px;">' + numAttorneysByPracticeAssociateCount[pointerPosition] + '</td></tr><tr><td style="color: #de7c35; padding-left: 10px; margin-top: 15px;">Other Counsel</td><td style="border: 1px solid; background-color: #de7c35; color: #fff; margin-top: 15px;">' + numAttorneysByPracticeOtherCounselCount[pointerPosition] + '</td></tr></tr><tr><td colspan="2" class="clickToViewDetails"><a href="'+url+'">Click to View Details</a><td></tr></table>';
				
				return tooltipOption;
			}
		},
        plotOptions: 
        {
            series: 
            {
            	pointWidth: 15,
            	stacking: stackingType,
            	dataLabels : {
					enabled : false,
					color : '#FFFFFF',
					formatter : function() {
						if (parseInt(this.y) > 0) {
							return this.y;
						}
					}
				},
                point: 
                {
                    events: 
                    {
                        mouseOver: function(e) 
                        {}
                    }
                    
                }
            }
        },
        series: [
        {
            name: 'Partners',
            color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#506a85'],
					[1, '#15375c']
				]
			},
            data: numAttorneysByPracticePartnerCountList
        }, 
        {
            name: 'Associates',
            color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#cb676f'],
					[1, '#ba323e']
				]
			},
            data: numAttorneysByPracticeAssociateCountList
        }, 
        {
            name: 'Other Counsel',
            color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#e59c67'],
					[1, '#de7c35']
				]
			},
            data: numAttorneysByPracticeOtherCounselCountList
        }],
		navigation: {
            buttonOptions: {
                enabled: false
            }
        }
    });
    
    var totalList = [];
	for(var index = 0; index < numAttorneysByPracticePartnerCountList.length; index++)
	{
		totalList.push(numAttorneysByPracticePartnerCountList[index] + numAttorneysByPracticeAssociateCountList[index] + numAttorneysByPracticeOtherCounselCountList[index]);
	}
	
	if(stackingType == '')
	{
		
		var chart = $("#numberOfAttorneysByPracticeChartContainer").highcharts();
		
		chart.series[chart.series.length-1].remove();
		chart.series[chart.series.length-1].remove();
		chart.series[chart.series.length-1].remove();
		
		chart.addSeries({
			showInLegend: true,
			name: 'Total',
			data: totalList,
			color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#506a85'],
					[1, '#15375c']
				]
			}
		});
		
	}
});

$(document).ready(function()
{
	 $("#firmStatHomeSearch_noofAttorney").click(function()
			    {
			    	Liferay.Portlet.showPopup(
			    		{
			    			uri : '${chartSearchPortletURL}', // defined in common.jsp
			    			title: "Search Criteria"
			    		});
			    });	
});

</script>
 
<div class="newspublicationPage marbtm4">
		<div class="colMin flLeft leftDynamicDiv">
			<div class="topHeader ForChartsTopHeader">
				<span title="Remove this chart" onclick="removeThisChartNoAttorneyByPractice();" style="float: right; font-weight: bold; color: rgb(255, 255, 255); cursor: pointer; font-family: verdana; margin: 2px 5px; padding: 3px 8px;">X</span>
			</div>
			<div id="numberOfAttorneysByPracticeChartDiv" class="hideClass">0 Results, Please try a different search</div>	
				<div class="flRight charts" id="No-Data-NoOfAttPrac">
					<ul class="reset listView">
						<c:if test="<%=isHomePage%>" >
							<li id="firmStatHomeSearch_noofAttorney" style="overflow:hidden;"><a href="javascript:void(0);" class="filter-icon" >&nbsp;</a></li>
						</c:if>		
						<li id="firmStatHomeSearch_noofAttorney_settings"><a href="#numberOfAttorneysByPracticeViewSetting"
							class="btn icon settingsgry rightViewSetting login-window chartViewSetting"
							onclick="return false;">&nbsp;</a></li>
						<li>
							<a href="javascript:void(0);" id="noOfAttorneyPracticePrintCharts" onclick="return false;" class="printChartClass"></a>
						</li>
						<li>
							<a href="javascript:void(0);" onclick="return false;" class="exportChartClass"></a>
                        <div class="actionSec">
                        <h5>Actions</h5>
                        <ul class="reset">
                            <li class="exportChartImage"><span id="noOfAttorneyPracticeExportJPG">Export as JPG</span></li>
                            <li class="exportChartImage"><span id="noOfAttorneyPracticeExportPNG">Export as PNG</span></li>
                        </ul>
                        <div class="clear">&nbsp;</div>
                        </div>
						</li>
					</ul>
							<div style="display: none" class="viewBox popusdiv ClickPopup" id="numberOfAttorneysByPracticeViewSetting">
							<form:form  commandName="numberOfAttorneysByPracticeChartModelBean" method="post" action="${updateChartURL}" id="numberOfAttorneysByPracticeChartModelBean">
                            <div class="popHeader"> <a href="javascript:void(0);" class="btn icon closewhite closeOne flRight" style="margin-top: 2px" onClick="">Close</a> SETTINGS: NUMBER OF ATTORNEYS BY PRACTICE
                              <div class="clear">&nbsp;</div>
                            </div>
                            <div class="section-one" style="width: 165px;">
                              <h6>Chart Type</h6>
                                <ul class="reset list4">
                                     <li style="width: 125px">
                                      <form:radiobutton path="chartType" class="graphType" value="<%= BaseChartModelBean.ChartType.STACKED_HORIZONTAL_BAR.getValue() %>" />
                                      <span class="btn icon barcharthori">Stacked&nbsp;Horizontal&nbsp;Bar</span>
                                    </li>
                                    <li>
                                    <form:radiobutton path="chartType" class="graphType" value="<%=BaseChartModelBean.ChartType.STACKED_VERTICAL_BAR.getValue()%>" />
                                    <span class="btn icon stackedbarchart">Stacked&nbsp;Vertical&nbsp;Bar</span>
                                   </li>
                                    <li>
                                    <form:radiobutton path="chartType" class="graphType" value="<%=BaseChartModelBean.ChartType.VERTICAL_BAR.getValue()%>" />
                                    <span class="btn icon barchartvert">Vertical&nbsp;Bar</span>
                                   </li>
                                    <li>
                                    <form:radiobutton path="chartType" class="graphType" value="<%= BaseChartModelBean.ChartType.HORIZONTAL_BAR.getValue() %>" />
                                    <span class="btn icon barcharthori">Horizontal Bar</span>
                                   </li>
                                </ul>
                                <div class="clear">&nbsp;</div>
                            </div>
                            <div class="section-two">
                                <h6>Firm Data (Limit of 15)</h6>
                                <ul class="reset list4">
                                  <li style="width: 125px">
                                    <form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.TOP_15.getValue() %>" />
                                    <span class="">Top 15 by No of Attorneys</span></li>
                                  <li>
                                     <form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.BOTTOM_15.getValue() %>" />
                                    <span class="">Bottom 15 by No of Attorneys</span></li>
                                 <li>
                                     <form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.PRACTICE.getValue() %>" />
                                    <span class="">Selected Practices:</span>
                                     <form:select path="practiceAreaList" multiple="true" size="4" style="width:150px;">
										<form:options items="${allSearchResultsPracticeList}"/>
									 </form:select>
                                    </li>
                                </ul>
                                <div class="clear">&nbsp;</div>
                               
                            </div>
                            
                            <div class="section-three" style="width: 165px;">
                              <h6>Firms</h6>
                                <ul class="reset list4">
                                     <li>
                                     <%-- <form:radiobutton path="searchResultsFirmList" value="<%= BaseChartModelBean.FirmDataType.FIRM.getValue() %>" /> --%>
                                    <span class="">Selected Firms:</span>
                                     <form:select path="searchResultsFirmList" multiple="false"  style="width:150px;">
										<form:options items="${allSearchResultsFirmList}" itemLabel="company" itemValue="companyId"/> <%--Only one can be selected --%>
									 </form:select>
                                    </li>
                                </ul>
                                <div class="clear">&nbsp;</div>
                               
                            </div>
                             <div class="clear">&nbsp;</div>
                            <hr/>
                              <div class="btmdiv">
	                                <input value="Reset All" class="buttonTwo flLeft settingReset" type="button" id="noOfAttorneyPracticeResetAll">
                                	<input type="button" class="buttonTwo flRight rightReset" value="Cancel" id="noOfAttorneyPracticeCancel" >
	                                <input value="Apply" class="buttonOne flRight" id="numberOfAttorneysByPracticeApply" type="button" style="margin: 0 5px 0 0;" onclick="applyChartSettings('#numberOfAttorneysByPracticeChartModelBean');">
                                <div class="clear">&nbsp;</div>
                              </div>
                            </form:form>
                          </div>
                    </div>				
			<div id="numberOfAttorneysByPracticeChartContainer" class="charts-spacing">
		</div>
	
	</div>
</div>