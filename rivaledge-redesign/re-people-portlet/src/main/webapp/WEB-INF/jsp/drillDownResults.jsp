<%-- This page is used in common to display and print the drill down results--%>

<%@page import="java.util.Map"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<portlet:defineObjects/>

<%-- We need to calculate the percentages in the search results --%>

<%@ include file="calculation.jsp" %>
<%
  TreeMap<Double, String> cssClassMap = (TreeMap<Double, String>)request.getAttribute("cssClassMap");
%>

<%--These two hidden values are used while printing the drill down results (Practices or Locations)--%>

<input type="hidden" id="printDrillDownFirmId" value="${drillDownFirmId}"/>
<input type="hidden" id="printDrillDownType" value="${drillDownType}"/>


 <!-- Title grouping results Start -->
<c:if test="${peopleSearchModelBean.orderBy  == -1}"> <%--Group By Title --%>
    
 <div id="drillDownMainResults">
 
 <table border="0" cellspacing="0" cellpadding="0" class="tble3">
          <colgroup>
          <col width="12.5%"/>
          <col width="5%" id="firmHeadcount_subheader_col" />
          <col width="3%" id="plus_firmHeadcount_subheader_col" />
          <col width="3%" id="minus_firmHeadcount_subheader_col" />
          <col width="5%" id="partners_subheader_col" />
          <col width="3%" id="plus_partners_subheader_col" />
          <col width="3%" id="minus_partners_subheader_col" />
          <col width="5%" id="associates_subheader_col" />
          <col width="3%" id="plus_associates_subheader_col" />
          <col width="3%" id="minus_associates_subheader_col" />
          <col width="5%" id="otherCounsel_subheader_col" />
          <col width="3%" id="plus_otherCounsel_subheader_col" />
          <col width="3%" id="minus_otherCounsel_subheader_col" />
          <col width="5%" id="admin_subheader_col" />
          <col width="3%" id="plus_admin_subheader_col" />
          <col width="3%" id="minus_admin_subheader_col" />
          <col width="5%" id="other_subheader_col" />
          <col width="3%" id="plus_other_subheader_col" />
          <col width="3%" id="minus_other_subheader_col" />
          <col width="5%" />
          </colgroup>
          <tr class="tpbar">
            <th align="left">${title}</th>
            <th	id="firmHeadcount_header" colspan="3">Firm Headcount</th>
            <th id="partners_header" colspan="3">Partners</th>
            <th id="associates_header" colspan="3">Associates</th>
            <th id="otherCounsel_header" colspan="3">Other Counsel</th>
            <th id="admin_header" colspan="3">Admin</th>
            <th id="other_header" colspan="3">Other</th>
            <th>% Chg. (Attorneys)</th>
          </tr>
          <tr>
            <th align="left">&nbsp;</th>
            <th>Total</th>
      		<th	id="plus_firmHeadcount_subheader"><span class="btn icon addgry">&nbsp;</span></th>
            <th	id="minus_firmHeadcount_subheader"><span class="btn icon negry">&nbsp;</span></th>
            <th id="partners_subheader">Total</th>
            <th	id="plus_partners_subheader"><span class="btn icon addgry">&nbsp;</span></th>
            <th	id="minus_partners_subheader"><span class="btn icon negry">&nbsp;</span></th>
            <th id="associates_subheader">Total</th>
            <th	id="plus_associates_subheader"><span class="btn icon addgry">&nbsp;</span></th>
            <th	id="minus_associates_subheader"><span class="btn icon negry">&nbsp;</span></th>
            <th id="otherCounsel_subheader">Total</th>
            <th	id="plus_otherCounsel_subheader"><span class="btn icon addgry">&nbsp;</span></th>
            <th id="minus_otherCounsel_subheader"><span class="btn icon negry">&nbsp;</span></th>
            <th	id="admin_subheader">Total</th>
            <th	id="plus_admin_subheader"><span class="btn icon addgry">&nbsp;</span></th>
            <th	id="minus_admin_subheader"><span class="btn icon negry">&nbsp;</span></th>
            <th	id="other_subheader">Total</th>
            <th id="plus_other_subheader"><span class="btn icon addgry">&nbsp;</span></th>
            <th id="minus_other_subheader"><span class="btn icon negry">&nbsp;</span></th>
            <th>%</th>
          </tr>
        <c:forEach var="drillDownMap" items="${drillDownData}">
	          <tr class="odd">
	            <td> <div style="text-align:left;margin-left:10px">${drillDownMap.key}</div></td>
	            <td	id="firmHeadcount_">${drillDownMap.value.headCount}</td>
	            <td id="plus_firmHeadcount_"><div class="blue">${drillDownMap.value.headCountPlus}</div></td>
	            <td id="minus_firmHeadcount_"><div class="red">${drillDownMap.value.headCountMinus}</div></td>
	            <td id="partners_">${drillDownMap.value.partnerCount}</td>
	            <td id="plus_partners_"><div class="blue">${drillDownMap.value.partnerCountPlus}</div></td>
	            <td id="minus_partners_"><div class="red">${drillDownMap.value.partnerCountMinus}</div></td>
	            <td id="associates_">${drillDownMap.value.associateCount}</td>
	            <td id="plus_associates_"><div class="blue">${drillDownMap.value.associateCountPlus}</div></td>
	            <td id="minus_associates_"><div class="red">${drillDownMap.value.associateCountMinus}</div></td>
	            <td id="otherCounsel_">${drillDownMap.value.otherCounselCount}</td>
	            <td id="plus_otherCounsel_"><div class="blue">${drillDownMap.value.otherCounselCountPlus}</div></td>
	            <td id="minus_otherCounsel_"><div class="red">${drillDownMap.value.otherCounselCountMinus}</div></td>
	            <td id="admin_">${drillDownMap.value.adminCount}</td>
	            <td id="plus_admin_"><div class="blue">${drillDownMap.value.adminCountPlus}</div></td>
	            <td id="minus_admin_"><div class="red">${drillDownMap.value.adminCountMinus}</div></td>
	            <td id="other_">${drillDownMap.value.otherCount}</td>
	            <td id="plus_other_"><div class="blue">${drillDownMap.value.otherCountPlus}</div></td>
	            <td id="minus_other_"><div class="red">${drillDownMap.value.otherCountMinus}</div></td>
			    <%--Send the corresponding PeopleResult Object --%>
			    <%
			      String data[] =  calculatePercentageData(((Map.Entry<String, PeopleResult>)pageContext.getAttribute("drillDownMap")).getValue(), cssClassMap); 
	            %>
	            <td class="<%=data[0]%>">
	            <%=data[1]%>
	            </td>
	          </tr>
          </c:forEach>
 		</table>
	 </div>
  </c:if>
 <!-- Title grouping results End -->

 <!-- Head count grouping results Start -->
 <c:if test="${peopleSearchModelBean.orderBy  == -2}"> <%--Group By Head count --%>
  <div id="drillDownGroupedData" >
       
       		<table border="0" cellspacing="0" class="tble3" cellpadding="0">
		      <colgroup>
				 <col width="6%" />
				  <%--
				  <col width="3%" id="offices_subheader_col" />
		          <col width="3%" id="practices_subheader_col" /> 
		          --%>
		          <col width="3%" id="plus_firmHeadcount_subheader_col" />
		          <col width="3%" id="minus_firmHeadcount_subheader_col" />
		          <col width="3%" id="plus_partners_subheader_col" />
		          <col width="3%" id="minus_partners_subheader_col" />
		          <col width="3%" id="plus_associates_subheader_col" />
		          <col width="3%" id="minus_associates_subheader_col" />
		          <col width="3%" id="plus_otherCounsel_subheader_col" />
		          <col width="3%" id="minus_otherCounsel_subheader_col" />
		          <col width="3%" id="plus_admin_subheader_col" />
		          <col width="3%" id="minus_admin_subheader_col" />
		          <col width="3%" id="plus_other_subheader_col" />
		          <col width="3%" id="minus_other_subheader_col" />
				  <col width="3%" />
				
			  </colgroup>
		          <tr class="tpbar">
		            <th colspan="1" rowspan="2" class="backbrdnone">&nbsp;</th>
		            <th colspan="4" id="headcount">Headcount</th>
		            <th colspan="9" id="changes_grouped">Changes</th>
		          </tr>
		          <tr>
		            <th rowspan="2" id="firmHeadcount_subheader_grouped">Total</th>
		            <th rowspan="2" id="partners_subheader_grouped">Partner</th>
		            <th rowspan="2" id="associates_subheader_grouped">Associate</th> 
		            <th rowspan="2" id="otherCounsel_subheader_grouped">Counsel</th>
		            <th colspan="2" id="firmHeadcount_subheader_total_grouped">Total</th>
		            <th colspan="2" id="partners_subheader_total_grouped" >Partner</th>
		            <th colspan="2" id="associates_subheader_total_grouped">Associate</th>
		            <th colspan="2" id="otherCounsel_subheader_total_grouped">Counsel</th>
		            <th rowspan="2">% Chg. (Attorneys)</th>
		          </tr>
		          <tr>
		            <th >${title}</th>
		           
		            <th id="plus_firmHeadcount_subheader_total_grouped"><span class="btn icon addgry">&nbsp;</span></th>
		            <th id="minus_firmHeadcount_subheader_total_grouped"><span class="btn icon negry">&nbsp;</span></th>
		            <th id="plus_partners_subheader_total_grouped"><span class="btn icon addgry">&nbsp;</span></th>
		            <th id="minus_partners_subheader_total_grouped"><span class="btn icon negry">&nbsp;</span></th>
		            <th id="plus_associates_subheader_total_grouped"><span class="btn icon addgry">&nbsp;</span></th>
		            <th id="minus_associates_subheader_total_grouped"><span class="btn icon negry">&nbsp;</span></th>
		            <th id="plus_otherCounsel_subheader_total_grouped"><span class="btn icon addgry">&nbsp;</span></th>
		            <th id="minus_otherCounsel_subheader_total_grouped"><span class="btn icon negry">&nbsp;</span></th>
		          </tr>
		         
						   <c:forEach var="drillDownMap" items="${drillDownData}">
						           	<tr class="odd">
							            <td> <div style="text-align:left;margin-left:10px">${drillDownMap.key}</div></td>
							           <%--
							            
							            <td><button id="officeExpand_${results.companyId}_grouped" value="${results.companyId}"> ${results.officesCount}</button></td>
							            <td><button id="practiceExpand_${results.companyId}_grouped" value="${results.companyId}"> ${fn:length(results.practicesMap)}</button></td>
							            
							            --%>
							            <td id="firmHeadcount_grouped_">${drillDownMap.value.headCount}</td>
							            <td id="partners_grouped_">${drillDownMap.value.partnerCount}</td>
							            <td id="associates_grouped_">${drillDownMap.value.associateCount}</td>
							            <td id="otherCounsel_grouped_">${drillDownMap.value.otherCounselCount}</td>
							            <td id="plus_firmHeadcount_grouped_"><div class="blue">${drillDownMap.value.headCountPlus}</div></td>
							            <td id="minus_firmHeadcount_grouped_"><div class="red">${drillDownMap.value.headCountMinus}</div></td>
							            <td id="plus_partners_grouped_"><div class="blue">${drillDownMap.value.partnerCountPlus}</div></td>
							            <td id="minus_partners_grouped_"><div class="red">${drillDownMap.value.partnerCountMinus}</div></td>
							            <td id="plus_associates_grouped_"><div class="blue">${drillDownMap.value.associateCountPlus}</div></td>
							            <td id="minus_associates_grouped_"><div class="red">${drillDownMap.value.associateCountMinus}</div></td>
							            <td id="plus_otherCounsel_grouped_"><div class="blue">${drillDownMap.value.otherCounselCountPlus}</div></td>
							            <td id="minus_otherCounsel_grouped_"><div class="red">${drillDownMap.value.otherCounselCountMinus}</div></td>
									    <%
			    						  String data[] =  calculatePercentageData(((Map.Entry<String, PeopleResult>)pageContext.getAttribute("drillDownMap")).getValue(), cssClassMap); 
	          							%>
							            <td class="<%=data[0]%>">
							            <%=data[1]%>
							            </td>

						          </tr>
						           </c:forEach> 
          </table>
       </div>
 </c:if>

        
 <!-- Head count grouping results End -->

 <%-- Include the js file we have to hide and display the columns accordingly--%>
	<%@ include file="hideOrDisplayColumnsJS.jspf" %>
		
 <script>
	 hideOrDisplayTableColumns();
	 
	 $("#mainResultsSpan").hide();
	 $("#mainResults, #groupedData").hide(); 
	 $("#pagination").hide(); 
	
	 $("#drillDownResultsSpan").html("${resultSize} Results"); // the the no of results in search criteria string
	 $("#drillDownResultsSpan").show();
 </script>
 
 <script> 
 
	//Omniture SiteCatalyst - START
	s.prop22 = "premium";
	s.pageName="rer:people-analysis-${drillDownType}-results";
	s.channel="rer:people-analysis";
	s.server="rer";
	s.prop1="people-analysis"
	s.prop2="people-analysis";
	s.prop3="people-analysis";
	s.prop4="People-Analysis";
	s.prop21=s.eVar21='current.user@foo.bar';	
	s.events="event2";
	s.events="event27";
	s.t();
	// Omniture SiteCatalyst - END	
	
</script>

