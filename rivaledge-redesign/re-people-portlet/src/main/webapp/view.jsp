
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>

<portlet:defineObjects />

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ALM - RivalEdge - PeopleSearch</title>
<link href="/css/reset.css" rel="stylesheet" type="text/css" />
<link href="/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript"  src="/js/jquery-1.9.1.min.js"/>
<script type="text/javascript">
// Live Feed Page
  $(document).ready(function() {
	$('#btnAdd').click(function(){
		$('.filtersPage').hide()
		$('#additional').show()
	})
	$('#btnSave').click(function(){
		$('.filtersPage').show()
		$('#additional').hide()
	});
});
</script>
</head>

<body>
<div id="page_wrapper">
  <div id="page_Header">
    <div style="float:left; width:184px;"><img src="/images/logo.gif" alt="" width="184" height="49"/></div>
    <div id="navcontainer">
      <ul id="navlist">
        <li><a href="#">Doob-Doob Crocodile <img src="/images/arrow.gif" alt="" width="8" height="7" border="0" style="margin:0px 0px 0px 5px;" /></a></li>
        <li><a href="#">Manage Watchlists (00) </a></li>
        <li><a href="#">Manage Alerts (00)</a></li>
        <li><a href="#">Help</a></li>
      </ul>
    </div>
    <div style="clear:both;">
      <div id="menu">
        <ul id="menulist">
          <li><a href="#">HOME </a></li>
          <li style="background:url(images/files_menu.gif) top left no-repeat; height:46px; width:81px;"><a href="#">FIRMS</a></li>
          <li></li>
          <li><a href="#">PEOPLE<img src="/images/arrow.gif" alt="" width="8" height="7" border="0" style="margin:0px 0px 0px 8px;" /></a></li>
          <li></li>
          <li><a href="#">CLIENTS</a></li>
          <li><a href="#">EVENTS</a></li>
        </ul>
      </div>
      <div style="float:right;"><img src="/images/search_box.gif" alt="" width="368" height="47" /></div>
    </div>
  </div>
  <!--header div end--->
  <div class="title" style="clear:both">
    <h1>Firms > News and Publications</h1>
  </div>
  <div id="dropdown"><!-- dropdown_title_start -->
    <ul id="droplist">
      <li>
        <label>&nbsp;</label>
        <input type="radio" />
      </li>
      <li>
        <label>My Default View</label>
        <div class="srchBox">
          <input type="text" name="search" value="" class="input" />
          <input type="submit" name="search" value="" class="srchBack" />
          <div class="clear">&nbsp;</div>
        </div>
        <input type="submit" name="search" value="" class="drpDwn" />
      </li>
      <li>
        <label>&nbsp;</label>
        <input type="radio" />
      </li>
      <li>
        <label>Firm(s)</label>
        <div class="srchBox">
          <input type="text" name="search" value="" class="input" />
          <input type="submit" name="search" value="" class="srchBack" />
          <div class="clear">&nbsp;</div>
        </div>
        <input type="submit" name="search" value="" class="typeSel" />
      </li>
      <li>
        <label>Practice Area</label>
        <div class="srchBox">
          <input type="text" name="search" value="" class="input" />
          <input type="submit" name="search" value="" class="srchBack" />
          <div class="clear">&nbsp;</div>
        </div>
        <input type="submit" name="search" value="" class="typeSel" />
      </li>
      <li>
        <label>Dates</label>
        <div class="srchBox">
          <input type="text" name="search" value="" class="input" />
          <input type="submit" name="search" value="" class="srchBack" />
          <div class="clear">&nbsp;</div>
        </div>
        <input type="submit" name="search" value="" class="drpDwn" />
      </li>
      <li>
        <label>Keywords</label>
        <div class="srchBox">
          <input type="text" name="search" value="" class="input" />
          <input type="submit" name="search" value="" class="srchBack" />
          <div class="clear">&nbsp;</div>
        </div>
        <input type="submit" name="search" value="" class="typeSel" />
      </li>
      <li>
        <label>&nbsp;</label>
        <input type="submit" value="Apply" class="buttonOne" />
      </li>
      <li>
        <label>&nbsp;</label>
        <input type="submit" value="Reset All" class="buttonTwo" />
      </li>
      <li>
        <label>&nbsp;</label>
        <input type="submit" value="Save View" class="buttonTwo" />
      </li>
    </ul>
    <div class="clear">&nbsp;</div>
  </div>
  <div class="filtersPage">
    <div class="barSec"><a href="#" class="dwnBx" id="btnAdd">Additional Filters</a></div>
  </div>
  <div class="filtersPage" id="additional" style="display:none">
    <div id="dropdown"><!-- dropdown_title_start -->
      <ul id="droplist">
        <li>
          <label>Practice Area(s) for Publications </label>
          <div class="srchBox">
            <input type="text" name="search" value="" class="input" />
            <input type="submit" name="search" value="" class="srchBack" />
            <div class="clear">&nbsp;</div>
          </div>
          <input type="submit" name="search" value="" class="drpDwn" />
        </li>
        <li>
          <label>Types of Twitter Posts</label>
          <div class="srchBox">
            <input type="text" name="search" value="" class="input" />
            <input type="submit" name="search" value="" class="srchBack" />
            <div class="clear">&nbsp;</div>
          </div>
          <input type="submit" name="search" value="" class="typeSel" />
        </li>
      </ul>
      <div class="clear">&nbsp;</div>
    </div>
    <div class="barSec"><a href="#" class="upBx" id="btnSave">Hide Filter</a></div>
  </div>
  <div>
    <ul class="menuRight flRight">
      <li><a class="alert" href="#">Create Alert</a></li>
      <li><a class="print" href="#">Print</a></li>
      <li><a class="export" href="#">Export</a></li>
      <li><a class="help" href="#">Help</a></li>
    </ul>
    <div class="clear">&nbsp;</div>
  </div>
  <div class="detailsPage">
  	<h4 class="heading">Results Details</h4>
  	<div>
    	<a class="viewSetting flRight" href="#"><span><span>View Settings</span></span></a>
        <div class="clear">&nbsp;</div>
    </div>
    <table width="100%" border="0" class="tblOne" cellspacing="0" cellpadding="0">
    	<col width="35" />
        <col width="45" />
        <col width="650" />
        <thead>
      <tr>
        <th align="center"><input type="checkbox" /></th>
        <th align="center">Type</th>
        <th>Description</th>
        <th align="center">Date</th>
        <th>Firm</th>
        <th>Related Practice</th>
        <th>&nbsp;</th>
      </tr>
      </thead>
      <tr class="odd">
        <td align="center"><input type="checkbox" /></td>
        <td align="center"><img src="/images/news_icon.gif" width="16" height="15" alt="icon" /></td>
        <td><h6><a href="#">A&L Goodbody welcome new Aviation Partner, Marie O'Brien</a></h6>
          <p>A&L Goodbody are delighted to announce the appointment of new Aviation Partner Marie O'Brien - Company Site</p></td>
        <td align="center">03/07/2013</td>
        <td>A&L Goodbody</td>
        <td>&nbsp;</td>
        <td><img src="/images/row_arrow.gif" width="11" height="11" alt="icon" /> </td>
      </tr>
      <tr class="even">
        <td align="center"><input type="checkbox" /></td>
        <td align="center"><img src="/images/news_icon.gif" width="16" height="15" alt="icon" /></td>
        <td><h6><a href="#">A&amp;L Goodbody welcome new Aviation Partner, Marie O'Brien</a></h6>
          <p>A&amp;L Goodbody are delighted to announce the appointment of new Aviation Partner Marie O'Brien - Company Site</p></td>
        <td align="center">03/07/2013</td>
        <td>A&amp;L Goodbody</td>
        <td>&nbsp;</td>
        <td><img src="/images/row_arrow.gif" width="11" height="11" alt="icon" /> </td>
      </tr>
      <tr class="odd">
        <td align="center"><input type="checkbox" /></td>
        <td align="center"><img src="/images/news_icon.gif" width="16" height="15" alt="icon" /></td>
        <td><h6><a href="#">A&amp;L Goodbody welcome new Aviation Partner, Marie O'Brien</a></h6>
          <p>A&amp;L Goodbody are delighted to announce the appointment of new Aviation Partner Marie O'Brien - Company Site</p></td>
        <td align="center">03/07/2013</td>
        <td>A&amp;L Goodbody</td>
        <td>&nbsp;</td>
        <td><img src="/images/row_arrow.gif" width="11" height="11" alt="icon" /> </td>
      </tr>
      <tr class="even">
        <td align="center"><input type="checkbox" /></td>
        <td align="center"><img src="/images/publish_icon.gif" width="14" height="16" alt="icon" /></td>
        <td><h6><a href="#">Obama Administration Moves Forward With Export Control Reform</a></h6>
          <p>On March 7 and 8, 2013, President Obama took two important steps to further U.S. export control reform. First, the President notified Congress of the proposed transfer of certain items relating to aircraft and gas turbine engines from the jurisdiction of the State Department's International Traffic in Arms Regulations (ITAR), which govern the export of military products and technology, to the Commerce Department's Export Administration Regulations (EAR), which governs the export of commercial products... - Company Site</p></td>
        <td align="center">03/07/2013</td>
        <td>A&amp;L Goodbody</td>
        <td>&nbsp;</td>
        <td><img src="/images/row_arrow.gif" width="11" height="11" alt="icon"/> </td>
      </tr>
    </table>
  </div>
</div>
<!----middle_end---> 

<!------->
<div id="page_footer"> </div>
</body>
</html>