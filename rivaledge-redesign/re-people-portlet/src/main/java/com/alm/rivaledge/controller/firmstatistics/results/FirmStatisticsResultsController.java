package com.alm.rivaledge.controller.firmstatistics.results;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.Format;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFHyperlink;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.alm.rivaledge.controller.AbstractFirmStatisticsController;
import com.alm.rivaledge.model.ClickToView;
import com.alm.rivaledge.model.PeopleSearchModelBean;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.transferobject.PeopleResult;
import com.alm.rivaledge.transferobject.PeopleResultDTO;
import com.alm.rivaledge.transferobject.PeopleSearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.FastDateFormatFactoryUtil;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.servlet.ServletResponseUtil;

@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "peopleSearchModelBean", "peopleSearchDTO", "peopleSearchResults" })
public class FirmStatisticsResultsController extends AbstractFirmStatisticsController
{

	private Log _log = LogFactoryUtil.getLog(FirmStatisticsResultsController.class.getName());

	protected DecimalFormat percentageFormat = new DecimalFormat("###.#");

	public static final String DRILLDOWN_OFFICE 	= "office";
	public static final String DRILLDOWN_PRACTICE 	= "practice";

	@RenderMapping
	public String firmStatisticsResultsView(@ModelAttribute("peopleSearchModelBean") PeopleSearchModelBean peopleSearchModelBean,
											@ModelAttribute("peopleSearchDTO") PeopleSearchDTO peopleSearchDTO, 
											ModelMap model, 
											RenderRequest request,
											RenderResponse response) throws Exception
	{
		ClickToView<PeopleSearchModelBean, PeopleSearchDTO> c2v = new ClickToView<PeopleSearchModelBean, PeopleSearchDTO>();
		c2v.setSearchModelBean(peopleSearchModelBean);

		c2v.setSearchDTO(peopleSearchDTO);
		honorClick2View(request, model, c2v); // peopleSearchModelBean is affected only when you are directed here from C2V
		
		peopleSearchDTO = c2v.getSearchDTO();
		peopleSearchModelBean = c2v.getSearchModelBean();
		
		return renderResultsPage(peopleSearchModelBean, peopleSearchDTO, model, request, response);
	}
	
	@RenderMapping(params = "renderPage=clickToView")
	protected String clickToView(@RequestParam("peopleSearchModelBeanJson") String peopleSearchModelBeanJson, 
												ModelMap model, 
												 RenderRequest request,
												 RenderResponse response) throws Exception
	{
		PeopleSearchModelBean psmb = WebUtil.getObject(peopleSearchModelBeanJson, PeopleSearchModelBean.class);
		psmb.setAddToSession(false);
		
		model.addAttribute("peopleSearchModelBean", psmb);
		
		PeopleSearchDTO eventSearchDTO = getDTOFromModelBean(request, model, psmb);
		
		return renderResultsPage(psmb, eventSearchDTO, model, request, response);
	}
	
	protected String renderResultsPage( PeopleSearchModelBean peopleSearchModelBean, PeopleSearchDTO peopleSearchDTO, ModelMap model, RenderRequest request, RenderResponse response) throws UnsupportedEncodingException
	{
		List<PeopleResultDTO> peopleSearchResults = new ArrayList<PeopleResultDTO>();
		User 				  currentUser 		  = WebUtil.getCurrentUser(request, userService);
		
		boolean displayMessage = false;
		

		// Call the service implementation to get the search results count
		//		int resultSize = peopleService.getPeopleSearchCount(peopleSearchDTO, currentUser);

		//		boolean someFirmSizeSelected = peopleSearchModelBean.getFirmSizeList() != null
		//				&& !peopleSearchModelBean.getFirmSizeList().contains(ALMConstants.ALL_FIRM_SIZES);

		// We have results that match this search criteria
		// Return the first page of results
		peopleSearchResults = peopleService.peopleSearch(peopleSearchDTO, currentUser);

		int resultSize = peopleSearchResults.size();

		int lastPage = 0;
		if (resultSize > 0)
		{
			if (resultSize > peopleSearchDTO.getResultPerPage())
			{
				lastPage = (int) Math.ceil(new Double(resultSize) / peopleSearchDTO.getResultPerPage());
			}
			else
			{
				lastPage = 1;
			}
			if(resultSize >= 5000){
				
				displayMessage = true;
				model.addAttribute("displayMessage", displayMessage);
			}
		}
		// Put the total number of pages into the model
		peopleSearchModelBean.setLastPage(lastPage);

		// Put the total number of results into the model
		peopleSearchModelBean.setTotalResultCount(resultSize);

		// Put the current page size (this is not the page number but the number
		// of records on this page)
		// into the model
		if (peopleSearchResults != null && !peopleSearchResults.isEmpty())
		{
			peopleSearchModelBean.setCurrentPageSize(peopleSearchResults.size());
		}
		else
		{
			peopleSearchModelBean.setCurrentPageSize(0);
		}

		//model.addAttribute("peopleSearchResultsJSON", getJson(peopleSearchResults));

		// Put the results into the model

		int comparisonResultCount = 0;
		// add comparison results only on page one
		if (peopleSearchModelBean.getGoToPage() == 1)
		{
			Object[] returnResults = getComparisonDataResults(peopleSearchResults);
			comparisonResultCount = (Integer) returnResults[0];
			peopleSearchResults = (List<PeopleResultDTO>) returnResults[1];
		}

		model.addAttribute("comparisonResultCount", comparisonResultCount);
		model.addAttribute("peopleSearchResults", peopleSearchResults); // this will go into session, later on used for drill down data
		
		// Sort the results by name
		TreeSet<PeopleResultDTO> sortedResults = new TreeSet<PeopleResultDTO>(peopleSearchResults);
		
		model.addAttribute("fsResultsList", new ArrayList<PeopleResultDTO>(sortedResults));  // this is used for rendering the results on resultsTable.jspf

		// Put the firmSearchDTO in model, later this is used in
		// printPublication
		//	model.addAttribute("peopleSearchDTO", peopleSearchDTO);

		peopleSearchModelBean.setPrinting(false); // This is used while rendering the search criteria String on Search Results and Printing

		List<PeopleResult> pplResult = new ArrayList<PeopleResult>();

		for (PeopleResultDTO ppl : peopleSearchResults)
		{
			pplResult.add(ppl.getCompanyPeople());
		}

		populateCSSRangeMapForAttorneyChange(model, pplResult);
		
		model.addAttribute("peopleSearchModelBean", peopleSearchModelBean);
		model.addAttribute("peopleSearchDTO", peopleSearchDTO);

		return ("peoplesearchresults");
	}
	
	/**
	 * Populates a cssMap for different ranges of Attorney % chg
	 * 
	 * @param model
	 * @param resultsList
	 */
	public void populateCSSRangeMapForAttorneyChange(ModelMap model, Collection<PeopleResult> resultsList)
	{

		TreeMap<Double, String> cssClassMap = new TreeMap<Double, String>();

		if (resultsList != null && !resultsList.isEmpty())
		{

			double[] percentageArray = new double[resultsList.size()];

			int count = 0;

			for (PeopleResult ppl : resultsList)
			{
				percentageArray[count++] = WebUtil.calculatePercentage(ppl);
			}

			//	model.addAttribute("percentageList", percentageArray);

			//The magic starts here
			double[] holder = ArrayUtils.clone(percentageArray); // clone it, we don't want to work on original

			Arrays.sort(holder); // sort, so we can get Min and Max values

			double minRange = holder[0];
			double maxRange = holder[holder.length - 1];

			double minDelta = minRange / ALMConstants.PERCENTILE_DELTA;

			double maxDelta = maxRange / ALMConstants.PERCENTILE_DELTA;

			double min = 0;
			double max = 0;

			for (int i = 0; i < ALMConstants.PERCENTILE_DELTA; i++)
			{

				min = min + minDelta;
				max = max + maxDelta;

				cssClassMap.put(min, "minus-" + (i + 1));
				cssClassMap.put(max, "plus-" + (i + 1));
			}

		}
		cssClassMap.put(0d, "zero"); // Always add this at last, as we dint handle 0 yet

		model.addAttribute("cssClassMap", cssClassMap);
	}

	/**
	 * Searches the results List and segregate the comparison results and normal
	 * results.<br>
	 * Returns 1. Comparison results count (@[0] index) and 2. The Results List
	 * containing
	 * the comparison results(if any) (@[1] index)
	 * followed by search results
	 * 
	 * @param model
	 * @param peopleSearchModelBean
	 * @param peopleSearchDTO
	 * @param peopleSearchResults
	 */
	private Object[] getComparisonDataResults(List<PeopleResultDTO> results)
	{
		List<PeopleResultDTO> comparisonResult = new ArrayList<PeopleResultDTO>();

		/*
		 *  Add them to new List as comparison results are removed from the original list in the below loop
		 * 
		 */
		ArrayList<PeopleResultDTO> peopleSearchResults = new ArrayList<PeopleResultDTO>(results);

		for (Iterator<PeopleResultDTO> iterator = peopleSearchResults.listIterator(); iterator.hasNext();)
		{
			PeopleResultDTO peopleResultDTO = iterator.next();
			if (StringUtils.isNotBlank(peopleResultDTO.getCompanyName())
					&& (peopleResultDTO.getCompanyName().equalsIgnoreCase(ALMConstants.AMLAW_100))
					|| peopleResultDTO.getCompanyName().equalsIgnoreCase(ALMConstants.AMLAW_200)
					|| peopleResultDTO.getCompanyName().equalsIgnoreCase(ALMConstants.NLJ_250)
					|| peopleResultDTO.getCompanyName().startsWith("WatchList")
					|| peopleResultDTO.getCompanyName().startsWith("MyFirm"))
			{

				comparisonResult.add(peopleResultDTO); // add it to new list
				iterator.remove(); // remove from existing
			}
		}
		int comparisonResultCount = comparisonResult.size();
		comparisonResult.addAll(peopleSearchResults); // add remaining search results to end of comparison List 
		return new Object[] { comparisonResultCount, comparisonResult };

	}

	
	@SuppressWarnings("unchecked")
	@ResourceMapping(value = "drillDown")
	public String displayOrPrintDrillDownData(@RequestParam("firmId") Integer firmId,
			@RequestParam("drillDownType") String drillDownType, @RequestParam("displayPrintPage") Boolean displayPrintPage,
			ModelMap model, ResourceRequest request, ResourceResponse response) throws PortletException, IOException
	{
		try{
			
		// get the firmId for which user wants to drill down locations
		if (firmId == null)
		{
			firmId = 0;
		}

		List<PeopleResultDTO> peopleResults = (List<PeopleResultDTO>) model.get("peopleSearchResults");
		Map<String, PeopleResult> resultsMap = getDrillDownResults(peopleResults, firmId, drillDownType);

		if (StringUtils.equalsIgnoreCase(drillDownType, DRILLDOWN_OFFICE))
		{
			model.addAttribute("title", "Location");
		}
		else
		//drillDown = practice
		{
			model.addAttribute("title", "Practice");
		}
		model.addAttribute("drillDownType", drillDownType);
		model.addAttribute("drillDownData", resultsMap);
		model.addAttribute("resultSize", resultsMap.size());
		model.addAttribute("drillDownFirmId", firmId);

		populateCSSRangeMapForAttorneyChange(model, resultsMap.values()); // for percentage and color codes

		}
		catch(Exception e)
		{
			response.setProperty(ResourceResponse.HTTP_STATUS_CODE, "500");
		}
		if (displayPrintPage)
		{
			WebUtil.populatePrintTimings(model);
			return "printDrillDownResults";
		}
		else
		{
			return "drillDownResults";
		}
	}

	private Map<String, PeopleResult> getDrillDownResults(List<PeopleResultDTO> peopleResults, Integer firmId,
			String drillDownType)
	{
		Map<String, PeopleResult> resultMap = null; //don't fear NPE, null will do, as we are sure we will get a result below
		for (PeopleResultDTO peopleResultDTO : peopleResults)
		{
			if (peopleResultDTO.getCompanyId().equals(firmId))
			{
				if (StringUtils.equalsIgnoreCase(drillDownType, DRILLDOWN_OFFICE))
				{
					resultMap = peopleResultDTO.getOfficeLocationsMap();
					break;
				}
				else
				//drillDown = practice
				{
					resultMap = peopleResultDTO.getPracticesMap();
					break;
				}
			}
		}
		return resultMap;
	}

	@RenderMapping(params = "displayPrintPage=true")
	protected String printPublications(@ModelAttribute("peopleSearchModelBean") PeopleSearchModelBean peopleSearchModelBean,
			@ModelAttribute("peopleSearchDTO") PeopleSearchDTO peopleSearchDTO, ModelMap model, RenderRequest request,
			RenderResponse response) throws Exception
	{
		List<PeopleResultDTO> printResutsList = getSearchResultsForPrintAndExport(peopleSearchDTO, request);

		Object[] returnResults = getComparisonDataResults(printResutsList);

		model.addAttribute("comparisonResultCount", returnResults[0]);
		model.addAttribute("peopleSearchResults", returnResults[1]); // this will go into session, later on used for drill down data
		model.addAttribute("fsResultsList", returnResults[1]);  // this is used for rendering the results on resultsTable.jspf
		WebUtil.populatePrintTimings(model);
		peopleSearchModelBean.setPrinting(true);
		peopleSearchModelBean.setTotalResultCount(printResutsList.size());
		peopleSearchModelBean.setCurrentPageSize(printResutsList.size());
		
		List<PeopleResult> pplResult = new ArrayList<PeopleResult>();

		for (PeopleResultDTO ppl : printResutsList)
		{
			pplResult.add(ppl.getCompanyPeople());
		}

		populateCSSRangeMapForAttorneyChange(model, pplResult);

		return "printPeopleResults";
	}

	/**
	 * Entry point when the User clicks on Export.
	 * Responsible for exporting xls/csv file for normal results or drilldown
	 * results
	 */
	@ResourceMapping("exportFile")
	public void exportFile(@RequestParam String fileType, @RequestParam("drillDownFirmId") Integer firmId,
			@RequestParam("drillDownType") String drillDownType,
			@ModelAttribute("peopleSearchModelBean") PeopleSearchModelBean psmb,
			@ModelAttribute("peopleSearchDTO") PeopleSearchDTO peopleSearchDTO,
			@ModelAttribute("peopleSearchResults") List<PeopleResultDTO> peopleResults, ResourceRequest request,
			ResourceResponse response)
	{

		int[] removeIndices = null;
		List<List<String>> gridData = null;

		if (firmId != null && firmId != -1)
		{
			Map<String, PeopleResult> resultsMap = getDrillDownResults(peopleResults, firmId, drillDownType);

			if (StringUtils.equals(DRILLDOWN_OFFICE, drillDownType))
			{
				gridData = getDataGridForDrillDown(psmb, resultsMap, "Locations");
			}
			else
			{
				gridData = getDataGridForDrillDown(psmb, resultsMap, "Practices");
			}
			psmb.setDrilling(true); // set the expectation that we are in drilling phase
		}
		else
		{

			List<PeopleResultDTO> peopleResultsDTOList = getSearchResultsForPrintAndExport(peopleSearchDTO, request);
			gridData = getDataGrid(psmb, peopleResultsDTOList);
		}

		removeIndices = getColumnIndicesToHide(psmb);
		List<String[]> grid = removeColumnsIfRequired(gridData, removeIndices);

		psmb.setDrilling(false); // reset it back

		HttpServletRequest servletRequest = PortalUtil.getHttpServletRequest(request);
		HttpServletResponse servletResponse = PortalUtil.getHttpServletResponse(response);

		exportFileSeriously(fileType, grid, servletRequest, servletResponse);
	}

	/**
	 * Creates and Exports the file to Client taking into account the type and
	 * data grid
	 */

	private void exportFileSeriously(String fileType, List<String[]> grid, HttpServletRequest servletRequest,
			HttpServletResponse servletResponse)
	{
		String fileName = getFileName();

		if (fileType != null && fileType.equalsIgnoreCase(ALMConstants.CSV_EXT))
		{

			/* Code for export publications to csv file starts */

			String csv = toCSV(grid);

			fileName = fileName + StringPool.PERIOD + ALMConstants.CSV_EXT;
			byte[] bytes = csv.getBytes();

			try
			{
				ServletResponseUtil.sendFile(servletRequest, servletResponse, fileName, bytes, ContentTypes.TEXT_CSV_UTF8);
			}
			catch (IOException e)
			{
				_log.error("GOT ERROR WHILE SENDING SERVLET RESPONSE " + e.getMessage());
			}

			/* Code for export publications to csv file ends */

		}
		else
		{

			HSSFWorkbook workbook = new HSSFWorkbook();

			toExcel(workbook, grid); // add the grid to workbook

			/* Code for export publications to excel file starts */

			FileInputStream fileInputStreamReader = null;
			FileOutputStream fileOutPutStreamReader = null;
			File xlsFile = null;

			try
			{
				xlsFile = FileUtil.createTempFile("xls");
				try
				{
					xlsFile.createNewFile();
				}
				catch (IOException e)
				{
					_log.error("ERROR WHILE CREATING NEW FILE " + e.getMessage());
				}
				if (xlsFile.exists())
				{
					fileInputStreamReader = new FileInputStream(xlsFile);
				}

			}
			catch (FileNotFoundException e1)
			{
				_log.info("Error while creating and reading file " + e1.getMessage());
			}

			fileName = fileName + StringPool.PERIOD + ALMConstants.XLS_EXT;

			try
			{
				fileInputStreamReader.close();

				fileOutPutStreamReader = new FileOutputStream(xlsFile);

				workbook.write(fileOutPutStreamReader);

				fileOutPutStreamReader.close();

				byte[] bytes = read(xlsFile);

				servletResponse.setContentType(ALMConstants.CONTENT_TYPE_XLS);

				ServletResponseUtil.sendFile(servletRequest, servletResponse, fileName, bytes, ALMConstants.CONTENT_TYPE_XLS);
			}
			catch (FileNotFoundException e)
			{
				_log.error("FILE NOT FOUND EXCEPTION " + e.getMessage());
			}
			catch (IOException e)
			{
				_log.error("IO EXCEPTION XLS " + e.getMessage());
			}
			finally
			{

				try
				{
					fileInputStreamReader.close();
					fileOutPutStreamReader.close();
				}
				catch (IOException e)
				{

					_log.error("ERROR WHILE CLOSING INPUT OUTPUT STREAM READER " + e.getMessage());
				}
			}
			/* Code for export publications to excel file ends */
		}

	}

	private String toCSV(List<String[]> grid)
	{
		StringBuilder builder = new StringBuilder(5000);

		for (String[] row : grid)
		{
			for (String col : row)
			{
				builder.append(StringEscapeUtils.escapeCsv(col) + StringPool.COMMA);

			}
			builder.append(StringPool.NEW_LINE);
		}
		return builder.toString();
	}

	private void toExcel(HSSFWorkbook workbook, List<String[]> grid)
	{
		HSSFSheet sheet = workbook.createSheet(ALMConstants.WORKBOOK_NAME);

		int key = 0;
		for (String[] gridRow : grid)
		{

			HSSFRow row = sheet.createRow((key));

			Object[] objArr = gridRow;
			int cellnum = 0;
			for (Object obj : objArr)
			{
				HSSFCell cell = row.createCell(cellnum);
				if (obj instanceof Date)
				{
					cell.setCellValue((Date) obj);
				}
				else if (obj instanceof Boolean)
				{
					cell.setCellValue((Boolean) obj);
				}
				else if (obj instanceof String)
				{
					String str = (String) obj;
					if (str.startsWith(ALMConstants.HTTP))
					{
						HSSFHyperlink hyperLink = new HSSFHyperlink(HSSFHyperlink.LINK_URL);
						hyperLink.setAddress(str);
						cell.setHyperlink(hyperLink);
						HSSFRichTextString textString = new HSSFRichTextString(str);
						HSSFCellStyle cellStyle = workbook.createCellStyle();
						HSSFFont hssFont = workbook.createFont();
						hssFont.setUnderline(HSSFFont.U_SINGLE);
						hssFont.setColor(HSSFColor.BLUE.index);
						cellStyle.setFont(hssFont);
						cell.setCellStyle(cellStyle);
						cell.setCellValue(textString);
					}
					else
					{
						HSSFRichTextString textString = new HSSFRichTextString(str);
						if ((key == 0) || (key == 1))
						{
							HSSFCellStyle cellStyle = workbook.createCellStyle();
							HSSFFont hssFont = workbook.createFont();
							hssFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
							cellStyle.setFont(hssFont);
							cell.setCellStyle(cellStyle);
						}
						cell.setCellValue(textString);
					}

				}
				else if (obj instanceof Double)
				{
					cell.setCellValue((Double) obj);
				}
				cellnum++;
			}

			key++;

		}

	}

	/**
	 * @param gridData
	 * @param psmb
	 */
	private List<String[]> removeColumnsIfRequired(List<List<String>> gridData, int[] removeIndices)
	{

		List<String[]> grid = new ArrayList<String[]>(gridData.size());
		for (List<String> row : gridData)
		{
			String[] gridRow = ArrayUtils.removeAll(row.toArray(new String[] {}), removeIndices);
			grid.add(gridRow);
		}

		return grid;
	}

	/**
	 * There are 23 cols in gridData (1 index col + 22 data cols)
	 * 
	 * @param psmb
	 * @return
	 */
	private int[] getColumnIndicesToHide(PeopleSearchModelBean psmb)
	{
		TreeSet<Integer> cols = new TreeSet<Integer>();

		//calculate the indices to hide
		if (!psmb.isAddedChecked() && !psmb.isRemovedChecked())
		{
			// remove all + and - indices
			cols.addAll(SetUtil.fromArray(new Integer[] { 5, 6, 8, 9, 11, 12, 14, 15, 17, 18, 20, 21 }));
		}

		if (!psmb.isAddedChecked() || !psmb.isRemovedChecked())
		{
			if (!psmb.isAddedChecked())
			{
				cols.addAll(SetUtil.fromArray(new Integer[] { 5, 8, 11, 14, 17, 20 })); // remove all +  indices
			}

			if (!psmb.isRemovedChecked())
			{
				cols.addAll(SetUtil.fromArray(new Integer[] { 6, 9, 12, 15, 18, 21 })); // remove all - indices
			}
		}

		if (!psmb.isPartnersChecked()) // remove Partners
		{
			cols.addAll(SetUtil.fromArray(new Integer[] { 7, 8, 9 }));
		}

		if (!psmb.isAssociatesChecked())
		{
			cols.addAll(SetUtil.fromArray(new Integer[] { 10, 11, 12 }));
		}
		if (!psmb.isOtherCounselChecked())
		{
			cols.addAll(SetUtil.fromArray(new Integer[] { 13, 14, 15 }));
		}
		if (!psmb.isAdminChecked())
		{
			cols.addAll(SetUtil.fromArray(new Integer[] { 16, 17, 18 }));
		}
		if (!psmb.isOtherChecked())
		{
			cols.addAll(SetUtil.fromArray(new Integer[] { 19, 20, 21 }));
		}

		if (psmb.isDrilling())
		{
			cols.add(2); // we don't need these two cols while drilling the results
			cols.add(3);
		}
		return ArrayUtil.toArray(cols.toArray(new Integer[] {}));
	}

	private List<PeopleResultDTO> getSearchResultsForPrintAndExport(PeopleSearchDTO peopleSearchDTO, PortletRequest request)
	{

		PeopleSearchDTO psDTO = peopleSearchDTO;
		try
		{
			psDTO = (PeopleSearchDTO) peopleSearchDTO.clone();
		}
		catch (CloneNotSupportedException e)
		{
			_log.error("Cannot clone PeopleSearchDTO. Hence, using the original => " + e.getMessage());
		}

		User currentUser = WebUtil.getCurrentUser(request, userService);

		psDTO.setPageNumber(0);
		psDTO.setResultPerPage(WebUtil.getResultSizeLimitForCharts(currentUser));

		List<PeopleResultDTO> peopleSearchResults = peopleService.peopleSearch(psDTO, currentUser);

		return peopleSearchResults;
	}

	private String getFileName()
	{

		String fileName = "People Firm Statistics -";

		Format dateFormatDate = FastDateFormatFactoryUtil.getSimpleDateFormat("MM.dd.yy", Locale.US);

		fileName = fileName + dateFormatDate.format(CalendarFactoryUtil.getCalendar(Locale.US).getTime());

		return fileName;
	}

	/*
	 * Returns Data grid for Normal Search results
	 * */

	private List<List<String>> getDataGrid(PeopleSearchModelBean psmb, List<PeopleResultDTO> peopleResultsDTOList)
	{

		List<List<String>> dataGrid = new ArrayList<List<String>>(peopleResultsDTOList.size() + 2); //+2 for header

		List<String> header = new ArrayList<String>();

		if (psmb.getOrderBy() == PeopleSearchModelBean.GROUPBY_TITLE)
		{

			/* Following code set header in csv file */
			header.add(ALMConstants.TITLE_NO);
			header.add(ALMConstants.FIRM);
			header.add(ALMConstants.OFFICES);
			header.add(ALMConstants.PRACTICES);

			header.add(ALMConstants.FIRM_HEADCOUNT);
			header.add(""); // for +
			header.add(""); // for -

			header.add(ALMConstants.PARTNERS);
			header.add("");
			header.add("");

			header.add(ALMConstants.ASSOCIATE);
			header.add("");
			header.add("");

			header.add(ALMConstants.OTHER_COUNSEL);
			header.add("");
			header.add("");

			header.add(ALMConstants.ADMINISTRATIVE);
			header.add("");
			header.add("");

			header.add(ALMConstants.OTHER);
			header.add("");
			header.add("");

			header.add(ALMConstants.NET_CHANGE);

			dataGrid.add(header);

			/* Following code populated list values for second row */
			header = new ArrayList<String>();

			header.add("");
			header.add("");
			header.add("");
			header.add("");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("");

			dataGrid.add(header);

			int keyCVS = 0;

			for (PeopleResultDTO peopleResultDTO : peopleResultsDTOList)
			{
				List<String> record = new ArrayList<String>();
				PeopleResult peopleResult = peopleResultDTO.getCompanyPeople();
				keyCVS++;

				record.add(StringPool.BLANK + String.valueOf(keyCVS));
				record.add(StringPool.BLANK + peopleResultDTO.getCompanyName());
				record.add(StringPool.BLANK + peopleResultDTO.getOfficeLocationsMap().size());
				record.add(StringPool.BLANK + peopleResultDTO.getPracticesMap().size());
				record.add(StringPool.BLANK + peopleResult.getHeadCount());
				record.add(StringPool.BLANK + peopleResult.getHeadCountPlus());
				record.add(StringPool.BLANK + peopleResult.getHeadCountMinus());
				record.add(StringPool.BLANK + peopleResult.getPartnerCount());
				record.add(StringPool.BLANK + peopleResult.getPartnerCountPlus());
				record.add(StringPool.BLANK + peopleResult.getPartnerCountMinus());
				record.add(StringPool.BLANK + peopleResult.getAssociateCount());
				record.add(StringPool.BLANK + peopleResult.getAssociateCountPlus());
				record.add(StringPool.BLANK + peopleResult.getAssociateCountMinus());
				record.add(StringPool.BLANK + peopleResult.getOtherCounselCount());
				record.add(StringPool.BLANK + peopleResult.getOtherCounselCountPlus());
				record.add(StringPool.BLANK + peopleResult.getOtherCounselCountMinus());
				record.add(StringPool.BLANK + peopleResult.getAdminCount());
				record.add(StringPool.BLANK + peopleResult.getAdminCountPlus());
				record.add(StringPool.BLANK + peopleResult.getAdminCountMinus());
				record.add(StringPool.BLANK + peopleResult.getOtherCount());
				record.add(StringPool.BLANK + peopleResult.getOtherCountPlus());
				record.add(StringPool.BLANK + peopleResult.getOtherCountMinus());
				record.add(StringPool.BLANK + percentageFormat.format(WebUtil.calculatePercentage(peopleResult)));

				dataGrid.add(record);
			}
		}
		else if (psmb.getOrderBy() == PeopleSearchModelBean.GROUPBY_HEAD_COUNT)
		{
			//TODO the logic has to be changed as per the Head Count grouping

			/* Following code set header in csv file */
			header.add(ALMConstants.TITLE_NO);
			header.add(ALMConstants.FIRM);
			header.add(ALMConstants.OFFICES);
			header.add(ALMConstants.PRACTICES);

			header.add(ALMConstants.FIRM_HEADCOUNT);
			header.add(""); // for +
			header.add(""); // for -

			header.add(ALMConstants.PARTNERS);
			header.add("");
			header.add("");

			header.add(ALMConstants.ASSOCIATE);
			header.add("");
			header.add("");

			header.add(ALMConstants.OTHER_COUNSEL);
			header.add("");
			header.add("");

			header.add(ALMConstants.ADMINISTRATIVE);
			header.add("");
			header.add("");

			header.add(ALMConstants.OTHER);
			header.add("");
			header.add("");

			header.add(ALMConstants.NET_CHANGE);

			dataGrid.add(header);

			/* Following code populated list values for second row */
			header = new ArrayList<String>();

			header.add("");
			header.add("");
			header.add("");
			header.add("");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("");

			dataGrid.add(header);

			int keyCVS = 0;

			for (PeopleResultDTO peopleResultDTO : peopleResultsDTOList)
			{
				List<String> record = new ArrayList<String>();
				PeopleResult peopleResult = peopleResultDTO.getCompanyPeople();
				keyCVS++;

				record.add(StringPool.BLANK + String.valueOf(keyCVS));
				record.add(StringPool.BLANK + peopleResultDTO.getCompanyName());
				record.add(StringPool.BLANK + peopleResultDTO.getOfficeLocationsMap().size());
				record.add(StringPool.BLANK + peopleResultDTO.getPracticesMap().size());
				record.add(StringPool.BLANK + peopleResult.getHeadCount());
				record.add(StringPool.BLANK + peopleResult.getHeadCountPlus());
				record.add(StringPool.BLANK + peopleResult.getHeadCountMinus());
				record.add(StringPool.BLANK + peopleResult.getPartnerCount());
				record.add(StringPool.BLANK + peopleResult.getPartnerCountPlus());
				record.add(StringPool.BLANK + peopleResult.getPartnerCountMinus());
				record.add(StringPool.BLANK + peopleResult.getAssociateCount());
				record.add(StringPool.BLANK + peopleResult.getAssociateCountPlus());
				record.add(StringPool.BLANK + peopleResult.getAssociateCountMinus());
				record.add(StringPool.BLANK + peopleResult.getOtherCounselCount());
				record.add(StringPool.BLANK + peopleResult.getOtherCounselCountPlus());
				record.add(StringPool.BLANK + peopleResult.getOtherCounselCountMinus());
				record.add(StringPool.BLANK + peopleResult.getAdminCount());
				record.add(StringPool.BLANK + peopleResult.getAdminCountPlus());
				record.add(StringPool.BLANK + peopleResult.getAdminCountMinus());
				record.add(StringPool.BLANK + peopleResult.getOtherCount());
				record.add(StringPool.BLANK + peopleResult.getOtherCountPlus());
				record.add(StringPool.BLANK + peopleResult.getOtherCountMinus());
				record.add(StringPool.BLANK + percentageFormat.format(WebUtil.calculatePercentage(peopleResult)));

				dataGrid.add(record);
			}

		}
		return dataGrid;
	}

	/*
	 * Returns Data grid for Drill Down Results
	 * */

	private List<List<String>> getDataGridForDrillDown(PeopleSearchModelBean psmb, Map<String, PeopleResult> peopleResultMap,
			String drillDownTitle)
	{

		List<List<String>> dataGrid = new ArrayList<List<String>>(peopleResultMap.size() + 2); //+2 for header

		List<String> header = new ArrayList<String>();

		if (psmb.getOrderBy() == PeopleSearchModelBean.GROUPBY_TITLE)
		{

			/* Following code set header in csv file */
			header.add(ALMConstants.TITLE_NO);
			header.add(drillDownTitle);
			header.add("");
			header.add("");

			header.add(ALMConstants.FIRM_HEADCOUNT);
			header.add(""); // for +
			header.add(""); // for -

			header.add(ALMConstants.PARTNERS);
			header.add("");
			header.add("");

			header.add(ALMConstants.ASSOCIATE);
			header.add("");
			header.add("");

			header.add(ALMConstants.OTHER_COUNSEL);
			header.add("");
			header.add("");

			header.add(ALMConstants.ADMINISTRATIVE);
			header.add("");
			header.add("");

			header.add(ALMConstants.OTHER);
			header.add("");
			header.add("");

			header.add(ALMConstants.NET_CHANGE);

			dataGrid.add(header);

			/* Following code populated list values for second row */
			header = new ArrayList<String>();

			header.add("");
			header.add("");
			header.add("");
			header.add("");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("");

			dataGrid.add(header);

			int keyCVS = 0;

			for (Map.Entry<String, PeopleResult> peopleResultEntry : peopleResultMap.entrySet())
			{
				PeopleResult peopleResult = peopleResultEntry.getValue();
				List<String> record = new ArrayList<String>();
				keyCVS++;

				record.add(StringPool.BLANK + String.valueOf(keyCVS));
				record.add(StringPool.BLANK + peopleResultEntry.getKey());
				record.add(StringPool.BLANK);
				record.add(StringPool.BLANK);
				record.add(StringPool.BLANK + peopleResult.getHeadCount());
				record.add(StringPool.BLANK + peopleResult.getHeadCountPlus());
				record.add(StringPool.BLANK + peopleResult.getHeadCountMinus());
				record.add(StringPool.BLANK + peopleResult.getPartnerCount());
				record.add(StringPool.BLANK + peopleResult.getPartnerCountPlus());
				record.add(StringPool.BLANK + peopleResult.getPartnerCountMinus());
				record.add(StringPool.BLANK + peopleResult.getAssociateCount());
				record.add(StringPool.BLANK + peopleResult.getAssociateCountPlus());
				record.add(StringPool.BLANK + peopleResult.getAssociateCountMinus());
				record.add(StringPool.BLANK + peopleResult.getOtherCounselCount());
				record.add(StringPool.BLANK + peopleResult.getOtherCounselCountPlus());
				record.add(StringPool.BLANK + peopleResult.getOtherCounselCountMinus());
				record.add(StringPool.BLANK + peopleResult.getAdminCount());
				record.add(StringPool.BLANK + peopleResult.getAdminCountPlus());
				record.add(StringPool.BLANK + peopleResult.getAdminCountMinus());
				record.add(StringPool.BLANK + peopleResult.getOtherCount());
				record.add(StringPool.BLANK + peopleResult.getOtherCountPlus());
				record.add(StringPool.BLANK + peopleResult.getOtherCountMinus());
				record.add(StringPool.BLANK + percentageFormat.format(WebUtil.calculatePercentage(peopleResult)));

				dataGrid.add(record);
			}
		}

		else if (psmb.getOrderBy() == PeopleSearchModelBean.GROUPBY_HEAD_COUNT)
		{
			 //TODO the logic has to be changed as per the Head Count grouping
			/* Following code set header in csv file */
			header.add(ALMConstants.TITLE_NO);
			header.add(drillDownTitle);
			header.add("");
			header.add("");

			header.add(ALMConstants.FIRM_HEADCOUNT);
			header.add(""); // for +
			header.add(""); // for -

			header.add(ALMConstants.PARTNERS);
			header.add("");
			header.add("");

			header.add(ALMConstants.ASSOCIATE);
			header.add("");
			header.add("");

			header.add(ALMConstants.OTHER_COUNSEL);
			header.add("");
			header.add("");

			header.add(ALMConstants.ADMINISTRATIVE);
			header.add("");
			header.add("");

			header.add(ALMConstants.OTHER);
			header.add("");
			header.add("");

			header.add(ALMConstants.NET_CHANGE);

			dataGrid.add(header);

			/* Following code populated list values for second row */
			header = new ArrayList<String>();

			header.add("");
			header.add("");
			header.add("");
			header.add("");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("Total");
			header.add("+");
			header.add("-");

			header.add("");

			dataGrid.add(header);

			int keyCVS = 0;

			for (Map.Entry<String, PeopleResult> peopleResultEntry : peopleResultMap.entrySet())
			{
				PeopleResult peopleResult = peopleResultEntry.getValue();
				List<String> record = new ArrayList<String>();
				keyCVS++;

				record.add(StringPool.BLANK + String.valueOf(keyCVS));
				record.add(StringPool.BLANK + peopleResultEntry.getKey());
				record.add(StringPool.BLANK);
				record.add(StringPool.BLANK);
				record.add(StringPool.BLANK + peopleResult.getHeadCount());
				record.add(StringPool.BLANK + peopleResult.getHeadCountPlus());
				record.add(StringPool.BLANK + peopleResult.getHeadCountMinus());
				record.add(StringPool.BLANK + peopleResult.getPartnerCount());
				record.add(StringPool.BLANK + peopleResult.getPartnerCountPlus());
				record.add(StringPool.BLANK + peopleResult.getPartnerCountMinus());
				record.add(StringPool.BLANK + peopleResult.getAssociateCount());
				record.add(StringPool.BLANK + peopleResult.getAssociateCountPlus());
				record.add(StringPool.BLANK + peopleResult.getAssociateCountMinus());
				record.add(StringPool.BLANK + peopleResult.getOtherCounselCount());
				record.add(StringPool.BLANK + peopleResult.getOtherCounselCountPlus());
				record.add(StringPool.BLANK + peopleResult.getOtherCounselCountMinus());
				record.add(StringPool.BLANK + peopleResult.getAdminCount());
				record.add(StringPool.BLANK + peopleResult.getAdminCountPlus());
				record.add(StringPool.BLANK + peopleResult.getAdminCountMinus());
				record.add(StringPool.BLANK + peopleResult.getOtherCount());
				record.add(StringPool.BLANK + peopleResult.getOtherCountPlus());
				record.add(StringPool.BLANK + peopleResult.getOtherCountMinus());
				record.add(StringPool.BLANK + percentageFormat.format(WebUtil.calculatePercentage(peopleResult)));

				dataGrid.add(record);
			}

		}
		return dataGrid;
	}

	private byte[] read(File file)
	{

		byte[] buffer = new byte[(int) file.length()];
		InputStream ios = null;
		try
		{
			try
			{
				ios = new FileInputStream(file);
			}
			catch (FileNotFoundException e)
			{

				_log.error("FILE NOT FOUND EXCEPTION " + e.getMessage());
			}
			try
			{
				if (ios.read(buffer) == -1)
				{
				}
			}
			catch (IOException e)
			{

				_log.error("ERROR WHILE READING FILE " + e.getMessage());
			}
		}
		finally
		{
			try
			{
				if (ios != null)
					ios.close();
			}
			catch (IOException e)
			{
			}
		}

		return buffer;
	}

}
