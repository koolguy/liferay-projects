package com.alm.rivaledge.comparator;

import java.util.Comparator;

import com.alm.rivaledge.transferobject.PeopleResult;
import com.alm.rivaledge.util.WebUtil;

/**
 * Comparator to compare the PeopleResult based on the head count
 * @author fl889
 * @since 07-Nov-2013
 * @version 1.0
 */
public class AttorneyComparator implements Comparator<PeopleResult>
{
	boolean desc = true; // sort in desc order by default
	
	
	public AttorneyComparator(boolean desc)
	{
		this.desc = desc;
	}
	
	@Override
	public int compare(PeopleResult o1, PeopleResult o2)
	{
		return WebUtil.compare(o1.getHeadCount(), o2.getHeadCount(), desc); 
	}
}
