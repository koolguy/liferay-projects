package com.alm.rivaledge.controller.firmstatistics.search;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.alm.rivaledge.controller.AbstractFirmStatisticsController;
import com.alm.rivaledge.model.ClickToView;
import com.alm.rivaledge.model.PeopleSearchModelBean;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.Practice;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.service.PeopleService;
import com.alm.rivaledge.service.WatchlistService;
import com.alm.rivaledge.transferobject.PeopleSearchDTO;
import com.alm.rivaledge.transferobject.UserGroupDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.FirmJsonUtility;
import com.alm.rivaledge.util.PracticeJsonUtility;
import com.alm.rivaledge.util.WebUtil;
import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;

@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "peopleSearchModelBean", "peopleSearchDTO", "peopleSearchResults" })
public class FirmStatisticsSearchController extends AbstractFirmStatisticsController
{
	protected Log			_log	= LogFactoryUtil.getLog(FirmStatisticsSearchController.class.getName());

	@Autowired
	private PeopleService	peopleService;

	@Autowired
	private FirmService		firmService;
	
	@Autowired
	private WatchlistService watchlistService;

	@RenderMapping
	protected ModelAndView firmStatisticsSearchForm(@ModelAttribute("peopleSearchModelBean") PeopleSearchModelBean peopleSearchModelBean,
			ModelMap model, RenderRequest request, RenderResponse response) throws Exception
	{
		ClickToView<PeopleSearchModelBean, PeopleSearchDTO> c2v = new ClickToView<PeopleSearchModelBean, PeopleSearchDTO>();
		c2v.setSearchModelBean(peopleSearchModelBean);

		honorClick2View(request, model, c2v); // peopleSearchModelBean is affected only when you are directed here from C2V
		
		return renderSearchPage(peopleSearchModelBean, model, request, response);
				
	}

	
	private ModelAndView renderSearchPage(PeopleSearchModelBean peopleSearchModelBean, ModelMap model, RenderRequest request,
			RenderResponse response)
	{

		Gson gson = new Gson();
		FirmJsonUtility firmJsonUtility = null;
		PracticeJsonUtility practiceJsonUtility = null;
		List<UserGroupDTO> allWatchLists = null;
		List<FirmJsonUtility> firmJsonUtilityList = new ArrayList<FirmJsonUtility>();
		List<PracticeJsonUtility> practiceJsonUtilityList = new ArrayList<PracticeJsonUtility>();
		
		//Getting logged in user
		User currentUser = WebUtil.getCurrentUser(request, userService);
		
		if(currentUser != null)
		{
			allWatchLists = (List<UserGroupDTO>) watchlistService.getAllFirmWatchList(currentUser.getId());
		}	
		
		// fetching all firms information from DB to show under firms pop up
		List<Firm> firmsSet = firmService.getAllFirms();

		// fetching all Practice area information from DB to show under firms
		// pop up
		List<Practice> practiceList = firmService.getAllPractices();

		// creating firm json list for firm Auto completer
		// converting List<Firm> to List<FirmJsonUtility> as FirmJsonUtility
		// utility class have attributes(id,label,category)
		// which is required for Firms Autocompleter/Type ahead
		for (Firm firm : firmsSet)
		{
			firmJsonUtility = new FirmJsonUtility();
			firmJsonUtility.setId(firm.getCompanyId().toString());
			firmJsonUtility.setLabel(firm.getCompany());
			firmJsonUtility.setCategory("Single Firms");
			firmJsonUtilityList.add(firmJsonUtility);
		}

		// creating Practice area json list for Practice area Auto completer
		// converting List<Practice> to List<PracticeJsonUtility> as
		// PracticeJsonUtility utility class have attribute(label)
		// which is required for Firms Autocompleter/Type ahead
		for (Practice practice : practiceList)
		{
			practiceJsonUtility = new PracticeJsonUtility();
			practiceJsonUtility.setLabel(practice.getPracticeArea());
			practiceJsonUtilityList.add(practiceJsonUtility);
		}	
		
		
		// converting all firms to JSON string
		String jsonAllFirms = gson.toJson(firmService.getRivalEdgeList(firmJsonUtilityList));

		// converting all Practices to JSON string
		String jsonAllPractics = gson.toJson(practiceJsonUtilityList);
		model.addAttribute("firmJson", jsonAllFirms);
		model.addAttribute("practiceJson", jsonAllPractics);
		model.addAttribute("allFirms", firmsSet);
		model.addAttribute("allPracticeArea", practiceList);
		model.addAttribute("allWatchListsResult", allWatchLists);
		model.addAttribute("allOtherLocations", ALMConstants.LOCATIONS);
		model.addAttribute("allFirmSize", ALMConstants.FIRM_SIZE);
		model.addAttribute("allFirmSizeJson", gson.toJson(ALMConstants.FIRM_SIZE));
		model.addAttribute("allRankingList", ALMConstants.RIVALEDGE_RANKING_LIST);
		
		List<UserGroup> allWatchListsDefaultList = null;
		if(currentUser != null)
		{
			allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
			if(allWatchListsDefaultList.size() == 0)
			{
				model.addAttribute("allWatchListsDefaultList", "");
			}
			else
			{
				model.addAttribute("allWatchListsDefaultList", allWatchListsDefaultList.get(0));
			}
		}

		//model data for comparison options
		model.put("doIHaveFirm", WebUtil.doesUserBelongToAnyFirm(currentUser));
		model.put("allWatchLists", WebUtil.getUserWatchLists(currentUser, watchlistService));
		model.addAttribute("allDisplayColumns", ALMConstants.FIRM_STATS_DISPLAY_COLUMNS.values());
		
		return new ModelAndView("peoplesearch", model);
	
	}
	
	
	@ActionMapping(params = "action=changeSearchCriteria")
	public void changeSearchCriteria(@ModelAttribute("peopleSearchModelBean") PeopleSearchModelBean peopleSearchModelBean, 
									 ModelMap model, 
									 ActionRequest request,
									 ActionResponse response) throws PortletException, IOException, JSONException
	{
		// Declare the search DTO that will hold the user selection criteria
		PeopleSearchDTO peopleSearchDTO = getDTOFromModelBean(request, model, peopleSearchModelBean);
		User			currentUser		= WebUtil.getCurrentUser(request, userService);
		
		// The user changed the search criteria, so we will have to persist
		// the changed criteria into the database.
		// However, if this is the Home page, then we will persist these as
		// the search criteria for the specific chart instead, and not
		// generically as the module search criteria.
		String currentPortletId 	= peopleSearchModelBean.getPortletId();
		String 	currentPage			= WebUtil.getCurrentPage(request);
		boolean	isHomePage			= ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		boolean clickToViewPage = WebUtil.isClickToViewPage(request);
		if ((currentUser != null) && (currentPortletId != null) && (currentPortletId.length() > 0) && !clickToViewPage)
		{
			if (!isHomePage)
			{
				currentPage = "FIRM_STATISTICS";
				currentPortletId = "FIRM_STATISTICS";
			}
			userService.saveUserPreferences(currentUser.getId(), 
											currentPortletId, 
											WebUtil.getJson(peopleSearchModelBean), 
											null, 
											currentPage);
		}

		// Put it back in the model
		model.addAttribute("peopleSearchDTO", peopleSearchDTO);
	}
	
	
	@ActionMapping(params = "action=clickToView")
	protected void clickToView(@ModelAttribute("peopleSearchModelBean_c2v") PeopleSearchModelBean peopleSearchModelBean_c2v, 
												 ModelMap model, 
												 ActionRequest request,
												 ActionResponse response) throws Exception
	{
		String peopleSearchModelBeanJson = WebUtil.getJson(peopleSearchModelBean_c2v);
		response.setRenderParameter("peopleSearchModelBeanJson", peopleSearchModelBeanJson);
		response.setRenderParameter("renderPage", "clickToView");
	}
	
	@RenderMapping(params = "renderPage=clickToView")
	protected ModelAndView clickToView(@RequestParam("peopleSearchModelBeanJson") String peopleSearchModelBeanJson, 
												 ModelMap model, 
												 RenderRequest request,
												 RenderResponse response) throws Exception
	{
		PeopleSearchModelBean psmb = WebUtil.getObject(peopleSearchModelBeanJson, PeopleSearchModelBean.class);
		psmb.setAddToSession(false);
		model.addAttribute("peopleSearchModelBean", psmb);
		return renderSearchPage(psmb, model, request, response);
	}
	
	
	@RenderMapping(params = "renderPage=searchCriteriaForChart")
	protected ModelAndView renderSearchCriteriaForChart(@RequestParam("chartPortletId") String chartPortletId, 
												 ModelMap model, 
												 RenderRequest request,
												 RenderResponse response) throws Exception
	{
		User currentUser = (model.get("currentUser") != null) ? ((User) model.get("currentUser")) :	WebUtil.getCurrentUser(request, userService);

		PeopleSearchModelBean psmb = getUserPreference(currentUser.getId(), chartPortletId, ALMConstants.HOME_PAGE);
		
		if(psmb == null)
		{
			psmb = getDefaultPeopleSearchModelBean(currentUser);
		}
		psmb.setAddToSession(false);
		model.addAttribute("peopleSearchModelBean", psmb);
		model.addAttribute("chartPortletId", chartPortletId);
		return renderSearchPage(psmb, model, request, response);
	}
	
	
	/**
	 *  Persists the Search Criteria for a given ChartId
	 * @param peopleSearchModelBean use a different model Name ("peopleSearchModelBean_chart") rather than the one which goes into session
	 * Because Spring retrieves the model from Session first then updates them with form values submitted by client, we need a new Object rather one from session
	 * @param chartPortletId
	 * @param model
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@ResourceMapping("persistSearchCriteriaForChart")
	protected void persistSearchCriteriaForChartOnHomePage(@ModelAttribute("peopleSearchModelBean_chart") PeopleSearchModelBean peopleSearchModelBean, 
												@RequestParam("chartPortletId") String chartPortletId,
												ModelMap model, 
												ResourceRequest request,
												ResourceResponse response) throws Exception
	{
		User currentUser = (model.get("currentUser") != null) ? ((User) model.get("currentUser")) :	WebUtil.getCurrentUser(request, userService);

		userService.saveUserPreferences(currentUser.getId(), chartPortletId, WebUtil.getJson(peopleSearchModelBean), null, ALMConstants.HOME_PAGE);

		
	}
	
}
