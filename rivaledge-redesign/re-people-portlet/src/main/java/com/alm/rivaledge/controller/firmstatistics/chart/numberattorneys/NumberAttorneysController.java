package com.alm.rivaledge.controller.firmstatistics.chart.numberattorneys;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.alm.rivaledge.controller.AbstractFirmStatisticsController;
import com.alm.rivaledge.model.PeopleSearchModelBean;
import com.alm.rivaledge.model.chart.StandardChartModelBean;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.transferobject.PeopleResult;
import com.alm.rivaledge.transferobject.PeopleResultDTO;
import com.alm.rivaledge.transferobject.PeopleSearchDTO;
import com.alm.rivaledge.transferobject.UserGroupDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.util.StringPool;

@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "peopleSearchModelBean", "numberOfAttorneysChartModelBean"})
public class NumberAttorneysController extends AbstractFirmStatisticsController
{
	@ModelAttribute
	public void populateChartModelBean(ModelMap modelMap, PortletRequest request)
	{
		StandardChartModelBean 	nasmb 				= null;
		String					currentPortletId	= WebUtil.getCurrentPortletId(request);
		String					currentPage	 	 	= WebUtil.getCurrentPage(request);
		boolean					isHomePage		 	= ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		User 					currentUser 		= (modelMap.get("currentUser") != null) ? ((User) modelMap.get("currentUser")) : WebUtil.getCurrentUser(request, userService); 
		
		if (!isHomePage)
		{
			currentPage	= "FIRM_STATISTICS";
		}
		nasmb = getUserChartPreference(currentUser.getId(), currentPortletId, currentPage);
		
		if (nasmb == null)
		{
			nasmb = new StandardChartModelBean();
			nasmb.init(); //do an explicit init for default values
		}
		modelMap.put("numberOfAttorneysChartModelBean", nasmb);
	}
	
	@RenderMapping
	public ModelAndView chartRenderView(@ModelAttribute("peopleSearchModelBean") PeopleSearchModelBean peopleSearchModelBean,		
										@ModelAttribute("numberOfAttorneysChartModelBean") StandardChartModelBean nacmb, 
										ModelMap model,
										RenderRequest request, 
										RenderResponse response) throws Exception
	{
		
		PeopleSearchDTO peopleSearchDTO = getDTOFromModelBean(request, model, peopleSearchModelBean);
		// clone the DTO as the changes to the DTO should not reflect elsewhere
		PeopleSearchDTO chartPeopleSearchDTO = (PeopleSearchDTO) peopleSearchDTO.clone();

		// Get the current user from the render request
		User currentUser = WebUtil.getCurrentUser(request, userService);

		// Set the page and result size limits
		chartPeopleSearchDTO.setMaxResultSizeCap(1000);
		chartPeopleSearchDTO.setResultPerPage(1000);

		List<PeopleResultDTO> peopleSearchResults = new ArrayList<PeopleResultDTO>();

		// Call the service implementation to get the search results count
		int resultSize = peopleService.getPeopleSearchCount(peopleSearchDTO, currentUser);

		if (resultSize > 0)
		{
			// We have results that match this search criteria
			// Return the first page of results
			peopleSearchResults = peopleService.peopleSearchWithoutDrilldown(peopleSearchDTO, currentUser);

			List<PeopleResultDTO> refinedResults = refineSearchResultsByLimit(nacmb, new ArrayList<PeopleResultDTO>(peopleSearchResults));

			ChartData chartData = new ChartData();
			chartData.type = CHART_NO_OF_ATTORNEYS;
			
			prepareComparisonData(currentUser, nacmb, chartPeopleSearchDTO, refinedResults, chartData);
			// Set the chart type
			model.put("numAttorneysChartType",  nacmb.getChartType());

			// Set the chart title
			model.put("numAttorneysChartTitle", "NUMBER OF ATTORNEYS BY TYPE");

			// Generate the data
			// Generate the series data
			for (PeopleResultDTO prDTO : refinedResults)
			{
				PeopleResult pr =  prDTO.getCompanyPeople();
				
				if (chartData.sbFirm.length() > 0)
				{
					chartData.sbFirm.append(", ");
					chartData.sbPartners.append(", ");
					chartData.sbAssociates.append(", ");
					chartData.sbOtherCounsel.append(", ");
					chartData.sbTotals.append(", ");
				}
				chartData.sbFirm.append("\"" + prDTO.getCompanyName() + "\"");
				chartData.sbPartners.append(pr.getPartnerCount());
				chartData.sbAssociates.append(pr.getAssociateCount());
				chartData.sbOtherCounsel.append(pr.getOtherCounselCount());
				chartData.sbTotals.append(pr.getHeadCount());
				
			}
			
			List<Firm> allSearchResultsFirmList = new ArrayList<Firm>();
			
			for (PeopleResultDTO prDTO : peopleSearchResults)
			{
				/* why do we need to fetch from Service the whole object when we are interested only in 
				id and name which we already have
				*/
				//Firm firm = firmService.getFirmById(prDTO.getCompanyId()); 
				
				Firm firm = new Firm();
				firm.setCompanyId(prDTO.getCompanyId());
				firm.setCompany(prDTO.getCompanyName());
				
				allSearchResultsFirmList.add(firm);
			}
			
			List<UserGroupDTO> allWatchLists = new ArrayList<UserGroupDTO>();

			if (watchlistService != null && currentUser != null)
			{
				allWatchLists = (List<UserGroupDTO>) watchlistService.getAllFirmWatchList(currentUser.getId());
			}
			
			model.put("allRankingList", ALMConstants.RIVALEDGE_RANKING_LIST);
			model.put("allWatchLists", allWatchLists);
			model.put("allSearchResultsFirmList", allSearchResultsFirmList);
			
			model.put("numAttorneysPartnerCount", chartData.sbPartners.toString());
			model.put("numAttorneysAssociateCount", chartData.sbAssociates.toString());
			model.put("numAttorneysOtherCounselCount", chartData.sbOtherCounsel.toString());
			model.put("numAttorneysTotalAssociateCount", chartData.sbTotals.toString());
			model.put("numAttorneysFirmNameList", chartData.sbFirm.toString());
			model.put("numAttorneysCompareCount", chartData.compareCount);
		}

		return new ModelAndView("numberofattorneys");
	}
	
	@ActionMapping(params = "action=updateChart")
	public void updateChart(@ModelAttribute("numberOfAttorneysChartModelBean") StandardChartModelBean nacmb,
							ActionRequest request, 
							ActionResponse response)
	{
		// Persist the changes to the chart settings
		// If this is the home page, save them there, else
		// save with the generic NEWS_PUBS tag.
		String 	currentPortletId 	= WebUtil.getCurrentPortletId(request);
		String 	currentPage			= WebUtil.getCurrentPage(request);
		boolean	isHomePage			= ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		User	currentUser 		= WebUtil.getCurrentUser(request, userService);
	
		if (!isHomePage)
		{
			currentPage = "FIRM_STATISTICS";
		}
		userService.saveUserPreferences(currentUser.getId(), 
										currentPortletId, 
										null, 
										WebUtil.getJson(nacmb), 
										currentPage);
	}
	
	/*
	* Following method remove rendering portlet from the page. And redirects to same page.
	*
	*/

	@ActionMapping(params = "action=removePortleFromThePage")
	public void removePortletFromPage(ActionRequest request, ActionResponse response) throws Exception
	{
		WebUtil.removePortlet(request);
	}
}
