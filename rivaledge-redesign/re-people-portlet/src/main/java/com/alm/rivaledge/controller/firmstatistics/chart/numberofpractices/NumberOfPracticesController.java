package com.alm.rivaledge.controller.firmstatistics.chart.numberofpractices;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.alm.rivaledge.comparator.AttorneyComparator;
import com.alm.rivaledge.controller.AbstractFirmStatisticsController;
import com.alm.rivaledge.model.PeopleSearchModelBean;
import com.alm.rivaledge.model.chart.StandardChartModelBean;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.transferobject.PeopleResult;
import com.alm.rivaledge.transferobject.PeopleResultDTO;
import com.alm.rivaledge.transferobject.PeopleSearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.util.StringPool;

/**
 * This controller will render People - Firms Stats - Number of Practices by
 * Firm Chart
 * 
 * @author FL605
 * @since 7.1
 */
@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "peopleSearchModelBean", "numberOfPracticesChartModelBean" })
public class NumberOfPracticesController extends AbstractFirmStatisticsController
{
	@ModelAttribute
	public void populateChartModelBean(ModelMap modelMap, PortletRequest request)
	{
		StandardChartModelBean 	nasmb 				= null;
		
		String					currentPortletId	= WebUtil.getCurrentPortletId(request);
		String					currentPage	 	 	= WebUtil.getCurrentPage(request);
		boolean					isHomePage		 	= ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		User 					currentUser 		= (modelMap.get("currentUser") != null) ? ((User) modelMap.get("currentUser")) : WebUtil.getCurrentUser(request, userService); 
		
		if (!isHomePage)
		{
			currentPage	= "FIRM_STATISTICS";
		}
		
		nasmb = getUserChartPreference(currentUser.getId(), currentPortletId, currentPage);
		
		if (nasmb == null)
		{
			// Create and initialize the chartModelBean
			nasmb = new StandardChartModelBean();
			nasmb.init(); //do an explicit init for default values
		}
		modelMap.put("numberOfPracticesChartModelBean", nasmb);
	}

	@RenderMapping
	public ModelAndView chartRenderView(@ModelAttribute("peopleSearchModelBean") PeopleSearchModelBean peopleSearchModelBean,
										@ModelAttribute("numberOfPracticesChartModelBean") StandardChartModelBean nacmb, 
										ModelMap model,
										RenderRequest request, 
										RenderResponse response) throws Exception
	{

		PeopleSearchDTO peopleSearchDTO = getDTOFromModelBean(request, model, peopleSearchModelBean);
		// clone the DTO as the changes to the DTO should not reflect elsewhere
		PeopleSearchDTO chartPeopleSearchDTO = (PeopleSearchDTO) peopleSearchDTO.clone();

		// Get the current user from the render request
		User currentUser = WebUtil.getCurrentUser(request, userService);

		// Set the page and result size limits
		chartPeopleSearchDTO.setMaxResultSizeCap(1000);
		chartPeopleSearchDTO.setResultPerPage(1000);

		List<PeopleResultDTO> peopleSearchResults = new ArrayList<PeopleResultDTO>();

		// Call the service implementation to get the search results count
		int resultSize = peopleService.getPeopleSearchCount(peopleSearchDTO, currentUser);

		if (resultSize > 0)
		{
			// We have results that match this search criteria
			// Return the first page of results
			peopleSearchResults = peopleService.peopleSearch(peopleSearchDTO, currentUser);

			List<PeopleResultDTO> refinedResults = refineSearchResultsByForPracticeArea(nacmb, new ArrayList<PeopleResultDTO>(
					peopleSearchResults));

			ChartData chartData = new ChartData();
			chartData.type = CHART_NO_OF_PRACTICES;

			prepareComparisonData(currentUser, nacmb, chartPeopleSearchDTO, refinedResults, chartData);
			// Set the chart type
			model.put("numOfPracticeChartType", nacmb.getChartType());

			// Set the chart title
			model.put("numOfPracticeChartTitle", "NUMBER OF PRACTICE AREAS BY FIRM");

			Comparator<PeopleResult> comparator = new AttorneyComparator(true);
				
			// Generate the data
			// Generate the series data
			for (PeopleResultDTO prDTO : refinedResults)
			{
				if (chartData.sbFirm.length() > 0)
				{
					chartData.sbFirm.append(", ");
					chartData.sbPracticeCount.append(", ");
					chartData.sbToolTipTitle.append(", ");
					chartData.sbToolTipCount.append(", ");
				}

				chartData.sbFirm.append("\"" + prDTO.getCompanyName() + "\"");
				chartData.sbPracticeCount.append(prDTO.getPracticesMap().size() > 0 ? prDTO.getPracticesMap().size() : 0);
				
				Map<String, PeopleResult> practiceMap = prDTO.getPracticesMap();
				
				List<PeopleResult> pplResultForToolTip = new ArrayList<PeopleResult>(practiceMap.values());
				
				//Sort the results
				Collections.sort(pplResultForToolTip, comparator);
				
				pplResultForToolTip = WebUtil.top(pplResultForToolTip, ALMConstants.NO_OF_PRACTICES_IN_TOOLTIP);
				
				chartData.sbToolTipTitle.append("[");
				chartData.sbToolTipCount.append("[");
				
				for(PeopleResult ppl : pplResultForToolTip)
				{
					if (!chartData.sbToolTipTitle.toString().endsWith("["))
					{
						chartData.sbToolTipTitle.append(", ");
						chartData.sbToolTipCount.append(", ");
					}
					chartData.sbToolTipTitle.append("\"").append(WebUtil.cleanString(ppl.getSearchTitle())).append("\"");
					chartData.sbToolTipCount.append("\"").append(ppl.getHeadCount()).append("\"");
				}
				
				chartData.sbToolTipTitle.append("]");
				chartData.sbToolTipCount.append("]");
				

			}

			List<Firm> allSearchResultsFirmList = new ArrayList<Firm>();

			for (PeopleResultDTO prDTO : peopleSearchResults)
			{
				/* why do we need to fetch from Service the whole object when we are interested only in 
				id and name which we already have
				*/
				//Firm firm = firmService.getFirmById(prDTO.getCompanyId()); 

				Firm firm = new Firm();
				firm.setCompanyId(prDTO.getCompanyId());
				firm.setCompany(prDTO.getCompanyName());

				allSearchResultsFirmList.add(firm);
			}

			model.put("doIHaveFirm", WebUtil.doesUserBelongToAnyFirm(currentUser));
			model.put("allWatchLists", WebUtil.getUserWatchLists(currentUser, watchlistService));
			model.put("allRankingList", ALMConstants.RIVALEDGE_RANKING_LIST);
			model.put("allSearchResultsFirmList", allSearchResultsFirmList);

			model.put("numAttorneysFirmNameList", chartData.sbFirm.toString());
			model.put("practicesCount", chartData.sbPracticeCount);
			model.put("practicesToolTipTitle", chartData.sbToolTipTitle);
			model.put("practicesToolTipCount", chartData.sbToolTipCount);
			model.put("numAttorneysCompareCount", chartData.compareCount);
		}
		
		return new ModelAndView("numberofpractices");
	}

	@ActionMapping(params = "action=updateChart")
	public void updateChart(@ModelAttribute("numberOfPracticesChartModelBean") StandardChartModelBean nacmb,
							ActionRequest request, 
							ActionResponse response)
	{
		String 	currentPortletId 	= WebUtil.getCurrentPortletId(request);
		String 	currentPage			= WebUtil.getCurrentPage(request);
		boolean	isHomePage		 	= ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		User	currentUser 		= WebUtil.getCurrentUser(request, userService);
		if (!isHomePage)
		{
			currentPage = "FIRM_STATISTICS";
		}
		userService.saveUserPreferences(currentUser.getId(), 
										currentPortletId, 
										null, 
										WebUtil.getJson(nacmb), 
										currentPage);
	}

	/*
	* Following method remove rendering portlet from the page. And redirects to same page.
	*
	*/

	@ActionMapping(params = "action=removePortleFromThePage")
	public void removePortletFromPage(ActionRequest request, ActionResponse response) throws Exception
	{
		WebUtil.removePortlet(request);
	}
	
	
	/*class AttorneyCountComparator implements Comparator<PeopleResult>
	{

		@Override
		public int compare(PeopleResult o1, PeopleResult o2)
		{
			
			return 0;
		}
		
	}*/

}
