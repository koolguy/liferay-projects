package com.alm.rivaledge.controller.firmstatistics.chart.numberattorneysbypractice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.alm.rivaledge.controller.AbstractFirmStatisticsController;
import com.alm.rivaledge.model.PeopleSearchModelBean;
import com.alm.rivaledge.model.chart.BaseChartModelBean.FirmDataType;
import com.alm.rivaledge.model.chart.StandardChartModelBean;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.transferobject.PeopleResult;
import com.alm.rivaledge.transferobject.PeopleResultDTO;
import com.alm.rivaledge.transferobject.PeopleSearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.util.StringPool;

@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "peopleSearchModelBean", "numberOfAttorneysByPracticeChartModelBean" })
public class NumberAttorneysByPracticeController extends AbstractFirmStatisticsController
{

	@ModelAttribute
	public void populateChartModelBean(ModelMap modelMap, PortletRequest request)
	{
		StandardChartModelBean 	nasmb 				= null;
	
		String					currentPortletId	= WebUtil.getCurrentPortletId(request);
		String					currentPage	 	 	= WebUtil.getCurrentPage(request);
		boolean					isHomePage		 	= ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		User 					currentUser 		= (modelMap.get("currentUser") != null) ? ((User) modelMap.get("currentUser")) : WebUtil.getCurrentUser(request, userService); 
		
		if (!isHomePage)
		{
			currentPage	= "FIRM_STATISTICS";
		}
		nasmb = getUserChartPreference(currentUser.getId(), currentPortletId, currentPage);
		
		if (nasmb == null)
		{
			// Create and initialize the chartModelBean
			nasmb = new StandardChartModelBean();
			nasmb.init(); //do an explicit init for default values
		}
			modelMap.addAttribute("numberOfAttorneysByPracticeChartModelBean", nasmb);
	}

	@RenderMapping
	public ModelAndView chartRenderView(@ModelAttribute("peopleSearchModelBean") PeopleSearchModelBean peopleSearchModelBean,
										@ModelAttribute("numberOfAttorneysByPracticeChartModelBean") StandardChartModelBean cmb,
										ModelMap model,
										RenderRequest request,
										RenderResponse response) throws Exception
	{
		PeopleSearchDTO peopleSearchDTO = getDTOFromModelBean(request, model, peopleSearchModelBean);
		// clone the DTO as the changes to the DTO should not reflect elsewhere
		PeopleSearchDTO chartPeopleSearchDTO = (PeopleSearchDTO) peopleSearchDTO.clone();

		// Get the current user from the render request
		User currentUser = WebUtil.getCurrentUser(request, userService);

		// Set the page and result size limits
		chartPeopleSearchDTO.setMaxResultSizeCap(100000);
		chartPeopleSearchDTO.setResultPerPage(100000);

		List<PeopleResultDTO> peopleSearchResults = new ArrayList<PeopleResultDTO>();

		// Call the service implementation to get the search results count
		int resultSize = peopleService.getPeopleSearchCount(peopleSearchDTO, currentUser);

		if (resultSize > 0)
		{
			// We have results that match this search criteria
			// Return the first page of results
			peopleSearchResults = peopleService.peopleSearch(peopleSearchDTO, currentUser);

			//populate model for Firms dropdown
			List<Firm> allSearchResultsFirmList = new ArrayList<Firm>();

			for (PeopleResultDTO prDTO : peopleSearchResults)
			{
				Firm firm = new Firm();
				firm.setCompanyId(prDTO.getCompanyId());
				firm.setCompany(prDTO.getCompanyName());

				allSearchResultsFirmList.add(firm);

			}

			
			
			//set the default firm for which you have to display the results
			boolean setDefaultFirm = (cmb.getSearchResultsFirmList() == null ||  cmb.getSearchResultsFirmList().isEmpty());
			
			if(!setDefaultFirm) // This is not the first run and User has a selected value in drop down
			{
				Firm firm = new Firm();
				 //company Name not required as Firm takes only companyId into account while calculating hashCode and equals method
				firm.setCompanyId(Integer.valueOf(cmb.getSearchResultsFirmList().get(0)));
				
				if(!allSearchResultsFirmList.contains(firm)) 
				   // User has changed the search Criteria and the previously selected firm is now not in the search results
					//Hence, set the default Firm
				{
					
					setDefaultFirm = true;
				}
			}
			
			if(setDefaultFirm)
			{
				List<String> firmList = new ArrayList<String>();
				firmList.add("" + allSearchResultsFirmList.get(0).getCompanyId()); // make this as default selection
				cmb.setSearchResultsFirmList(firmList);
			}
			
			String limitType = cmb.getLimitType(); //get the existing limitType, no matter what it is 

			cmb.setLimitType(FirmDataType.FIRM.getValue()); //set it to Firm as we are interested only in selected firm results
			
			// This is 1st level refinement (Results pertaining to Selected firm)
			//I guess there will be only One record for a single firm, If multiple, pickup the first
			//TODO Are we sure we don't run into AIOBE?
			PeopleResultDTO peopleResultDTO = super.refineSearchResultsByLimit(cmb, peopleSearchResults).get(0); 

			cmb.setLimitType(limitType); // set the limitType back

			List<PeopleResult> refinedResults = refineResultsByPracticeArea(cmb, peopleResultDTO);

			// prepare model for practiceList
			List<String> allSearchResultsPracticeList = new ArrayList<String>(peopleResultDTO.getPracticesMap().keySet());

			ChartData chartData = new ChartData();
			chartData.type = CHART_NO_OF_ATTORNEYS_BY_PRACTICE;

			//comparison data not required
			//prepareComparisonData(currentUser, cmb, chartPeopleSearchDTO, refinedResults, chartData);
			// Set the chart type
			model.put("numAttorneysByPracticeChartType", cmb.getChartType());

			// Set the chart title
			model.put("numAttorneysByPracticeChartTitle", "NUMBER OF ATTORNEYS BY PRACTICE");
			model.put("noOfAttorneySelectedFirmName", peopleResultDTO.getCompanyName());

			// Generate the data
			// Generate the series data
			for (PeopleResult prDTO : refinedResults)
			{
				if (chartData.sbFirm.length() > 0)
				{
					chartData.sbFirm.append(", ");
					chartData.sbPartners.append(", ");
					chartData.sbAssociates.append(", ");
					chartData.sbOtherCounsel.append(", ");
					chartData.sbTotals.append(", ");
				}
				chartData.sbFirm.append("\"" + prDTO.getSearchTitle() + "\"");
				chartData.sbPartners.append(prDTO.getPartnerCount());
				chartData.sbAssociates.append(prDTO.getAssociateCount());
				chartData.sbOtherCounsel.append(prDTO.getOtherCounselCount());
				chartData.sbTotals.append(prDTO.getHeadCount());
				
			}

			//TODO replace it with practices of the selected firm
			model.put("allSearchResultsFirmList", allSearchResultsFirmList);
			model.put("allSearchResultsPracticeList", allSearchResultsPracticeList);
			model.put("numAttorneysByPracticePartnerCount", chartData.sbPartners.toString());
			model.put("numAttorneysByPracticeAssociateCount", chartData.sbAssociates.toString());
			model.put("numAttorneysByPracticeOtherCounselCount", chartData.sbOtherCounsel.toString());
			model.put("numAttorneysByPracticeTotalHeadCount", chartData.sbTotals.toString());
			model.put("numAttorneysByPracticeNameList", chartData.sbFirm.toString());
			model.put("numAttorneysByPracticeFirmName", peopleResultDTO.getCompanyName());
		}

		return new ModelAndView("numberofattorneysbypractice");
	}

	protected List<PeopleResult> refineResultsByPracticeArea(StandardChartModelBean cmb, PeopleResultDTO result)
	{
		Map<String, PeopleResult> practiceMap = result.getPracticesMap();
		List<PeopleResult> practiceAreaResults = new ArrayList<PeopleResult>();
		// User selected individual firms
		if ((FirmDataType.PRACTICE.getValue().equals(cmb.getLimitType())) && (cmb.getPracticeAreaList() != null)
				&& (!cmb.getPracticeAreaList().isEmpty()))
		{
			for (String selectedPracticeArea : cmb.getPracticeAreaList()) // run through the selected Practice Areas
			{
				for (String practiceArea : practiceMap.keySet())
				{
					if (StringUtils.equalsIgnoreCase(selectedPracticeArea, practiceArea)) //got a match
					{
						practiceAreaResults.add(practiceMap.get(practiceArea)); //add it to return results
						break; // go to next selected firm
					}
				}
			}

		}
		else
		{
			practiceAreaResults.addAll(practiceMap.values());
			// user select either top or bottom results
			// Need a Comparator for descending order 
			Comparator<PeopleResult> comparator = new Comparator<PeopleResult>()
			{
				@Override
				public int compare(PeopleResult o1, PeopleResult o2)
				{
					return WebUtil.compare(o1.getHeadCount(), o2.getHeadCount(), true); // sort in desc order
				}
			};

			//Sort the results
			Collections.sort(practiceAreaResults, comparator);

			if (FirmDataType.TOP_15.getValue().equals(cmb.getLimitType())) // Top selected
			{
				return WebUtil.top(practiceAreaResults, 15); // send top 15
			}
			else if (FirmDataType.BOTTOM_15.getValue().equals(cmb.getLimitType()))
			{
				return WebUtil.bottom(practiceAreaResults, 15); // send bottom 15
			}
			
			if (FirmDataType.TOP_10.getValue().equals(cmb.getLimitType())) // Top selected
			{
				return WebUtil.top(practiceAreaResults, 10); // send top 10
			}
			else if (FirmDataType.BOTTOM_10.getValue().equals(cmb.getLimitType()))
			{
				return WebUtil.bottom(practiceAreaResults, 10); // send bottom 10
			}

		}

		return practiceAreaResults;
	}

	@ActionMapping(params = "action=updateChart")
	public void updateChart(@ModelAttribute("numberOfAttorneysByPracticeChartModelBean") StandardChartModelBean cmb,
							ActionRequest request, 
							ActionResponse response)
	{
		String 	currentPortletId 	= WebUtil.getCurrentPortletId(request);
		String 	currentPage			= WebUtil.getCurrentPage(request);
		boolean	isHomePage			= ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		User	currentUser 		= WebUtil.getCurrentUser(request, userService);
		
		if (!isHomePage)
		{
			currentPage = "FIRM_STATISTICS";
		}
		userService.saveUserPreferences(currentUser.getId(), 
										currentPortletId, 
										null, 
										WebUtil.getJson(cmb), 
										currentPage);
	}

	/*
	* Following method remove rendering portlet from the page. And redirects to same page.
	*
	*/

	@ActionMapping(params = "action=removePortleFromThePage")
	public void removePortletFromPage(ActionRequest request, ActionResponse response) throws Exception
	{
		WebUtil.removePortlet(request);
	}
}
