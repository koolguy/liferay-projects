package com.alm.rivaledge.controller.firmstatistics.chart.netchangeattorneys;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.alm.rivaledge.controller.AbstractFirmStatisticsController;
import com.alm.rivaledge.model.PeopleSearchModelBean;
import com.alm.rivaledge.model.chart.BaseChartModelBean.ComparisonDataType;
import com.alm.rivaledge.model.chart.BaseChartModelBean.FirmDataType;
import com.alm.rivaledge.model.chart.StandardChartModelBean;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.transferobject.PeopleResult;
import com.alm.rivaledge.transferobject.PeopleResultDTO;
import com.alm.rivaledge.transferobject.PeopleSearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.util.StringPool;

@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "netAttorneyChangeModelBean", "peopleSearchModelBean" })
public class NetChangeAttorneysController extends AbstractFirmStatisticsController
{
	@ModelAttribute
	public void populateAmountNewsPubsChartModelBean(ModelMap modelMap, PortletRequest request)
	{
		StandardChartModelBean 	netAttorneyChangeModelBean 	= null;
		
		String					currentPortletId	= WebUtil.getCurrentPortletId(request);
		String					currentPage	 	 	= WebUtil.getCurrentPage(request);
		boolean					isHomePage		 	= ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		User 					currentUser 		= (modelMap.get("currentUser") != null) ? ((User) modelMap.get("currentUser")) : WebUtil.getCurrentUser(request, userService); 
		
		if (!isHomePage)
		{
			currentPage	= "FIRM_STATISTICS";
		}
		netAttorneyChangeModelBean = getUserChartPreference(currentUser.getId(), currentPortletId, currentPage);
		
		if (netAttorneyChangeModelBean == null)
		{
			netAttorneyChangeModelBean = new StandardChartModelBean();
			netAttorneyChangeModelBean.init();
		}
		modelMap.put("netAttorneyChangeModelBean", netAttorneyChangeModelBean);
	}

	@RenderMapping
	public String firmStatisticsResultsView(@ModelAttribute("netAttorneyChangeModelBean") StandardChartModelBean netAttorneyChangeModelBean,
											@ModelAttribute("peopleSearchModelBean") PeopleSearchModelBean peopleSearchModelBean,								
											ModelMap model, 
											RenderRequest request, 
											RenderResponse response) throws Exception
	{
		
		PeopleSearchDTO peopleSearchDTO = getDTOFromModelBean(request, model, peopleSearchModelBean);
		List<PeopleResultDTO> peopleSearchResults = new ArrayList<PeopleResultDTO>();

		// cloning search dto so that the original one is not modified.
		PeopleSearchDTO peopleSearchChartDTO = (PeopleSearchDTO) peopleSearchDTO.clone();

		User currentUser = WebUtil.getCurrentUser(request, userService);

		// Call the service implementation to get the search results count
		int resultSize = peopleService.getPeopleSearchCount(peopleSearchChartDTO, currentUser);

		if (resultSize > 0)
		{
			// We have results that match this search criteria
			// Return the first page of results
			peopleSearchResults = peopleService.peopleSearchWithoutDrilldown(peopleSearchChartDTO, null);
		}
		model.addAttribute("peopleSearchResults", peopleSearchResults);

		// model.addAttribute("peopleSearchResultsJSON",
		// getJson(peopleSearchResults));

		List<PeopleResultDTO> comparisonResult = new ArrayList<PeopleResultDTO>();

		List<Firm> selectedFirmsList = null;// new ArrayList<Firm>();
		List<String> firmNameList = netAttorneyChangeModelBean.getSearchResultsFirmList();

		// Selecting only top fifteen
		if (FirmDataType.TOP_10.getValue().equals(netAttorneyChangeModelBean.getLimitType()))
		{
			comparisonResult = sortByComparator(peopleSearchResults, 10, true);
		}
		else if (FirmDataType.BOTTOM_10.getValue().equals(netAttorneyChangeModelBean.getLimitType()))
		{
			comparisonResult = sortByComparator(peopleSearchResults, 10, false);
		}
		if (FirmDataType.TOP_15.getValue().equals(netAttorneyChangeModelBean.getLimitType()))
		{
			comparisonResult = sortByComparator(peopleSearchResults, 15, true);
		}
		else if (FirmDataType.BOTTOM_15.getValue().equals(netAttorneyChangeModelBean.getLimitType()))
		{
			comparisonResult = sortByComparator(peopleSearchResults, 15, false);
		}
		else
		{
			if (firmNameList != null)
			{
				selectedFirmsList = new ArrayList<Firm>();
				for (String selectedFirm : firmNameList)
				{
					if (selectedFirmsList.size() > ALMConstants.DEFAULT_NO_OF_COLUMNS_CHART)
					{
						break;
					}
					selectedFirmsList.add(firmService.getFirmById(Integer.parseInt(selectedFirm)));
				}
			}
			else
			{
				comparisonResult = sortByComparator(peopleSearchResults, ALMConstants.DEFAULT_NO_OF_COLUMNS_CHART, true);
			}
		}

		if (selectedFirmsList != null && selectedFirmsList.size() > 0)
		{
			peopleSearchChartDTO.setSelectedFirms(selectedFirmsList);
		}
		//
		// Put the results into the model

		addComparisonDataToResults(model, netAttorneyChangeModelBean, peopleSearchChartDTO, currentUser, comparisonResult, peopleSearchResults);
		model.put("allRankingList", ALMConstants.RIVALEDGE_RANKING_LIST);

		// Put the firmSearchDTO in model, later this is used in
		// printPublication
		// model.addAttribute("peopleSearchDTO", peopleSearchDTO);

		return ("netchangeattorneychart");
	}

	private List<PeopleResultDTO> sortByComparator(List<PeopleResultDTO> peopleList, int maxSize, final boolean isDescending)
	{
		
		if (peopleList != null && peopleList.size() > 0)
		{
			List<PeopleResultDTO> sortList = new ArrayList<PeopleResultDTO>(peopleList);
			Collections.sort(sortList, new Comparator<PeopleResultDTO>()
			{
				@Override
				public int compare(PeopleResultDTO thisEntry, PeopleResultDTO thatEntry)
				{
					Integer netChangeThat = 0;
					if (thatEntry.getCompanyPeople() != null)
					{
						netChangeThat = thatEntry.getCompanyPeople().getAssociateCountPlus() - thatEntry.getCompanyPeople().getAssociateCountMinus()
								+ thatEntry.getCompanyPeople().getOtherCounselCountPlus() - thatEntry.getCompanyPeople().getOtherCounselCountMinus()
								+ thatEntry.getCompanyPeople().getPartnerCountPlus() - thatEntry.getCompanyPeople().getPartnerCountMinus();
					}
					Integer netChangeThis = 0;
					if (thisEntry.getCompanyPeople() != null)
					{
						netChangeThis = thisEntry.getCompanyPeople().getAssociateCountPlus() - thisEntry.getCompanyPeople().getAssociateCountMinus()
								+ thisEntry.getCompanyPeople().getOtherCounselCountPlus() - thisEntry.getCompanyPeople().getOtherCounselCountMinus()
								+ thisEntry.getCompanyPeople().getPartnerCountPlus() - thisEntry.getCompanyPeople().getPartnerCountMinus();

					}

					if (isDescending)
					{
						return (netChangeThat.compareTo(netChangeThis));
					}
					else
					{
						return (netChangeThis.compareTo(netChangeThat));
					}
				}
			});
		}


		return WebUtil.top(peopleList, maxSize);
	}

	/**
	 * Searches the results List and segregate the comparison results and normal
	 * results.<br>
	 * The Results List added to model contains the comparison results(if any)
	 * followed by search results along with the comparison results count
	 * 
	 * @param model
	 * @param peopleSearchModelBean
	 * @param peopleSearchDTO
	 * @param peopleSearchResults
	 */
	/**
	 * @param model
	 * @param netAttorneyChangeModelBean
	 * @param peopleSearchDTO
	 * @param currentUser
	 * @param comparisonResults
	 */
	private void addComparisonDataToResults(ModelMap model, 
											StandardChartModelBean netAttorneyChangeModelBean, 
											PeopleSearchDTO peopleSearchDTO, 
											User currentUser,
											List<PeopleResultDTO> comparisonResults, 
											List<PeopleResultDTO> peopleSearchResults)
	{
		List<PeopleResultDTO> comparisonResult = new ArrayList<PeopleResultDTO>();

		if (comparisonResults.size() > 0)
		{
			// do nothing here since we already have the data required
		}
		else
		{
			// Call the service implementation to get the search results count
			int resultSize = peopleService.getPeopleSearchCount(peopleSearchDTO, currentUser);

			if (resultSize > 0)
			{
				// We have results that match this search criteria
				// Return the first page of results
				comparisonResults = peopleService.peopleSearchWithoutDrilldown(peopleSearchDTO, currentUser);
			}
		}

		// Set the comparisonCounter
		int comparisonCounter = 0;

		// Set the chart type
		model.put("chartType", netAttorneyChangeModelBean.getChartType());

		// Set the chart title
		model.put("chartTitle", "NET CHANGE OF ATTORNEYS");

		List<PeopleResultDTO> myFirmResults = new ArrayList<PeopleResultDTO>();
		List<PeopleResultDTO> myWatchListResults = new ArrayList<PeopleResultDTO>();
		List<PeopleResultDTO> myRivalAdgeResults = new ArrayList<PeopleResultDTO>();

		StringBuilder sbFirmNames = new StringBuilder();

		StringBuffer sbAssociatesAdd = new StringBuffer();
		StringBuffer sbCounselsAdd = new StringBuffer();
		StringBuffer sbPartnersAdd = new StringBuffer();

		StringBuffer sbAssociatesRemoved = new StringBuffer();
		StringBuffer sbCounselsRemoved = new StringBuffer();
		StringBuffer sbPartnersRemoved = new StringBuffer();

		StringBuffer sbAssociatesNet = new StringBuffer();
		StringBuffer sbCounselsNet = new StringBuffer();
		StringBuffer sbPartnersNet = new StringBuffer();

		StringBuffer sbAllNet = new StringBuffer();
		StringBuffer sbAllRemoved = new StringBuffer();
		StringBuffer sbAllAdded = new StringBuffer();

		// 1. My Firm
		// Does this user have a My Firm? Every user is supposed to have one,
		// but we can't take it for granted.
		if ((netAttorneyChangeModelBean.getComparisonDataTypeList() != null) && (!netAttorneyChangeModelBean.getComparisonDataTypeList().isEmpty())
				&& (netAttorneyChangeModelBean.getComparisonDataTypeList().contains(ComparisonDataType.MY_FIRM.getValue())) && (currentUser != null)
				&& (currentUser.getCompany() != null))
		{
			// We have to include comparison data for My Firm
			List<Firm> myFirmList = new ArrayList<Firm>(1);
			myFirmList.add(firmService.getFirmById(Integer.parseInt(currentUser.getCompany())));

			// Clone the searchDTO so that we don't overwrite the session
			// settings
			PeopleSearchDTO myPeopleSearchDTO = null;
			try
			{
				myPeopleSearchDTO = (PeopleSearchDTO) peopleSearchDTO.clone();
			}
			catch (CloneNotSupportedException e)
			{
				e.printStackTrace();
			}

			// Set the user's firm (my firm) as the sole firm in the firm list
			myPeopleSearchDTO.setSelectedFirms(myFirmList);

			myFirmResults = peopleService.peopleSearchWithoutDrilldown(myPeopleSearchDTO, currentUser);
			
			if ((myFirmResults != null) && (!myFirmResults.isEmpty()))
			{
				comparisonResult.add(myFirmResults.get(0));
			}

			// Increment the comparison counter
			comparisonCounter++;
		}

		// 2. Does the user have any watch lists set up?
		if ((netAttorneyChangeModelBean.getComparisonDataTypeList() != null) 
				&& (!netAttorneyChangeModelBean.getComparisonDataTypeList().isEmpty())
				&& (netAttorneyChangeModelBean.getComparisonDataTypeList().contains(ComparisonDataType.WATCHLIST_AVG.getValue())) 
				&& (currentUser != null)
				&& (netAttorneyChangeModelBean.getWatchList() != null) 
				&& (!netAttorneyChangeModelBean.getWatchList().isEmpty()))
		{
			// User has one or more watch lists
			// Iterate over the watch lists and run through each
			// Clone an instance of the firmSearchDTO
			PeopleSearchDTO myWatchListSearchDTO = null;
			try
			{
				myWatchListSearchDTO = (PeopleSearchDTO) peopleSearchDTO.clone();
			}
			catch (CloneNotSupportedException e)
			{
				e.printStackTrace();
			}

			// Now run through the list
			for (String watchListId : netAttorneyChangeModelBean.getWatchList())
			{
				// Get this watchlist
				UserGroup myWatchlist = watchlistService.getWatchList(Integer.parseInt(watchListId), currentUser.getId());

				// Get the list of firms in this watch list
				List<Firm> myWatchlistFirms = watchlistService.findAllFirmsInOneWatchlist(myWatchlist.getGroupId(), currentUser.getId());

				// Set the watch list firms in the search criteria
				myWatchListSearchDTO.setSelectedFirms(myWatchlistFirms);
				myWatchListResults = peopleService.peopleSearchWithoutDrilldown(myWatchListSearchDTO, null);
				double partnersAdded = 0;
				double counselsAdded = 0;
				double associatesAdded = 0;
				double partnersMinus = 0;
				double counselsMinus = 0;
				double associatesMinus = 0;
				double partnersNet = 0;
				double counselsNet = 0;
				double associatesNet = 0;
				double allNet = 0;
				double allAdded = 0;
				double allMinus = 0;
				double counter = 0;
				for (PeopleResultDTO prDTO : myWatchListResults)
				{
					if (prDTO.getCompanyPeople() != null)
					{
						PeopleResult pr = prDTO.getCompanyPeople();
						associatesAdded += pr.getAssociateCountPlus();
						associatesMinus += pr.getAssociateCountMinus();

						partnersAdded += pr.getPartnerCountPlus();
						partnersMinus += pr.getPartnerCountMinus();

						counselsAdded += pr.getOtherCounselCountPlus();
						counselsMinus += pr.getOtherCounselCountMinus();

						counter++;
					}
				}
				if (counter > 0)
				{
					partnersNet = (float) (partnersAdded - partnersMinus) / counter;
					counselsNet = (float) (counselsAdded - counselsMinus) / counter;
					associatesNet = (float) (associatesAdded - associatesMinus) / counter;

					allAdded = (float) (partnersAdded + counselsAdded + associatesAdded) / counter;
					allMinus = (float) (partnersMinus + counselsMinus + associatesMinus) / counter;
					allNet = (float) (partnersNet + counselsNet + associatesNet);

					associatesAdded = (float) associatesAdded / counter;
					associatesMinus = (float) associatesMinus / counter;

					partnersAdded = (float) partnersAdded / counter;
					partnersMinus = (float) partnersMinus / counter;

					counselsAdded = (float) counselsAdded / counter;
					counselsMinus = (float) counselsMinus / counter;

					partnersAdded = Math.round(partnersAdded);
					partnersMinus = Math.round(partnersMinus);
					counselsAdded = Math.round(counselsAdded);
					counselsMinus = Math.round(counselsMinus);
					associatesAdded = Math.round(associatesAdded);
					associatesMinus = Math.round(associatesMinus);

					partnersNet = Math.round(partnersNet);
					counselsNet = Math.round(counselsNet);
					associatesNet = Math.round(associatesNet);
					allAdded = Math.round(allAdded);
					allMinus = Math.round(allMinus);
					allNet = Math.round(allNet);
				}
				if (sbAssociatesAdd.length() > 0)
				{
					sbAssociatesAdd.append(",");
				}
				sbAssociatesAdd.append(associatesAdded);
				if (sbAssociatesRemoved.length() > 0)
				{
					sbAssociatesRemoved.append(",");
				}
				sbAssociatesRemoved.append(associatesMinus);
				if (sbAssociatesNet.length() > 0)
				{
					sbAssociatesNet.append(",");
				}
				sbAssociatesNet.append(associatesNet);
				if (sbCounselsAdd.length() > 0)
				{
					sbCounselsAdd.append(",");
				}
				sbCounselsAdd.append(counselsAdded);
				if (sbCounselsRemoved.length() > 0)
				{
					sbCounselsRemoved.append(",");
				}
				sbCounselsRemoved.append(counselsMinus);
				if (sbCounselsNet.length() > 0)
				{
					sbCounselsNet.append(",");
				}
				sbCounselsNet.append(counselsNet);
				if (sbPartnersAdd.length() > 0)
				{
					sbPartnersAdd.append(",");
				}
				sbPartnersAdd.append(partnersAdded);
				if (sbPartnersRemoved.length() > 0)
				{
					sbPartnersRemoved.append(",");
				}
				sbPartnersRemoved.append(partnersMinus);
				if (sbPartnersNet.length() > 0)
				{
					sbPartnersNet.append(",");
				}
				sbPartnersNet.append(partnersNet);
				if (sbAllAdded.length() > 0)
				{
					sbAllAdded.append(",");
				}
				sbAllAdded.append(allAdded);

				if (sbAllRemoved.length() > 0)
				{
					sbAllRemoved.append(",");
				}
				sbAllRemoved.append(allMinus);

				if (sbAllNet.length() > 0)
				{
					sbAllNet.append(",");
				}
				sbAllNet.append(allNet);

				if (sbFirmNames.length() > 0)
				{
					sbFirmNames.append(",");
				}
				sbFirmNames.append("\"" + myWatchlist.getGroupName() + "\"");
			}
			// Increment the comparison counter
			comparisonCounter++;
		}

		// 3. Does the user have any RivalEdge lists set up?
		if ((netAttorneyChangeModelBean.getComparisonDataTypeList() != null) 
				&& (!netAttorneyChangeModelBean.getComparisonDataTypeList().isEmpty())
				&& (netAttorneyChangeModelBean.getComparisonDataTypeList().contains(ComparisonDataType.RIVAL_EDGE.getValue()))
				&& (netAttorneyChangeModelBean.getFirmList() != null) 
				&& (!netAttorneyChangeModelBean.getFirmList().isEmpty()))
		{

			// User has selected a Rival Edge list for which data is needed
			for (String thisRE : netAttorneyChangeModelBean.getFirmList())
			{
				// Get the list of firms in this list entry - could be AmLaw100,
				// NLJ250, etc.
				List<Firm> reListFirms = firmService.getRanking(thisRE);
				// Clone an instance of the firmSearchDTO
				PeopleSearchDTO rankingSearchDTO = null;
				try
				{
					rankingSearchDTO = (PeopleSearchDTO) peopleSearchDTO.clone();
				}
				catch (CloneNotSupportedException e)
				{
					e.printStackTrace();
				}

				// Set the firms in this ranking list as the search firms
				rankingSearchDTO.setSelectedFirms(reListFirms);
				myRivalAdgeResults = peopleService.peopleSearchWithoutDrilldown(rankingSearchDTO, null);

				double partnersAdded = 0;
				double counselsAdded = 0;
				double associatesAdded = 0;
				double partnersMinus = 0;
				double counselsMinus = 0;
				double associatesMinus = 0;
				double partnersNet = 0;
				double counselsNet = 0;
				double associatesNet = 0;
				double allNet = 0;
				double allAdded = 0;
				double allMinus = 0;
				int counter = 0;
				for (PeopleResultDTO prDTO : myRivalAdgeResults)
				{
					if (prDTO.getCompanyPeople() != null)
					{
						PeopleResult pr = prDTO.getCompanyPeople();
						associatesAdded += pr.getAssociateCountPlus();
						associatesMinus += pr.getAssociateCountMinus();

						partnersAdded += pr.getPartnerCountPlus();
						partnersMinus += pr.getPartnerCountMinus();

						counselsAdded += pr.getOtherCounselCountPlus();
						counselsMinus += pr.getOtherCounselCountMinus();

						counter++;
					}
				}
				if (counter > 0)
				{
					partnersNet = (float) (partnersAdded - partnersMinus) / counter;
					counselsNet = (float) (counselsAdded - counselsMinus) / counter;
					associatesNet = (float) (associatesAdded - associatesMinus) / counter;

					allAdded = (float) (partnersAdded + counselsAdded + associatesAdded) / counter;
					allMinus = (float) (partnersMinus + counselsMinus + associatesMinus) / counter;
					allNet = (float) (partnersNet + counselsNet + associatesNet);

					associatesAdded = (float) associatesAdded / counter;
					associatesMinus = (float) associatesMinus / counter;

					partnersAdded = (float) partnersAdded / counter;
					partnersMinus = (float) partnersMinus / counter;

					counselsAdded = (float) counselsAdded / counter;
					counselsMinus = (float) counselsMinus / counter;

					partnersAdded = Math.round(partnersAdded);
					partnersMinus = Math.round(partnersMinus);
					counselsAdded = Math.round(counselsAdded);
					counselsMinus = Math.round(counselsMinus);
					associatesAdded = Math.round(associatesAdded);
					associatesMinus = Math.round(associatesMinus);

					partnersNet = Math.round(partnersNet);
					counselsNet = Math.round(counselsNet);
					associatesNet = Math.round(associatesNet);
					allAdded = Math.round(allAdded);
					allMinus = Math.round(allMinus);
					allNet = Math.round(allNet);
				}
				if (sbAssociatesAdd.length() > 0)
				{
					sbAssociatesAdd.append(",");
				}
				sbAssociatesAdd.append(associatesAdded);
				if (sbAssociatesRemoved.length() > 0)
				{
					sbAssociatesRemoved.append(",");
				}
				sbAssociatesRemoved.append(associatesMinus);
				if (sbAssociatesNet.length() > 0)
				{
					sbAssociatesNet.append(",");
				}
				sbAssociatesNet.append(associatesNet);
				if (sbCounselsAdd.length() > 0)
				{
					sbCounselsAdd.append(",");
				}
				sbCounselsAdd.append(counselsAdded);
				if (sbCounselsRemoved.length() > 0)
				{
					sbCounselsRemoved.append(",");
				}
				sbCounselsRemoved.append(counselsMinus);
				if (sbCounselsNet.length() > 0)
				{
					sbCounselsNet.append(",");
				}
				sbCounselsNet.append(counselsNet);
				if (sbPartnersAdd.length() > 0)
				{
					sbPartnersAdd.append(",");
				}
				sbPartnersAdd.append(partnersAdded);
				if (sbPartnersRemoved.length() > 0)
				{
					sbPartnersRemoved.append(",");
				}
				sbPartnersRemoved.append(partnersMinus);
				if (sbPartnersNet.length() > 0)
				{
					sbPartnersNet.append(",");
				}
				sbPartnersNet.append(partnersNet);
				if (sbAllAdded.length() > 0)
				{
					sbAllAdded.append(",");
				}
				sbAllAdded.append(allAdded);

				if (sbAllRemoved.length() > 0)
				{
					sbAllRemoved.append(",");
				}
				sbAllRemoved.append(allMinus);

				if (sbAllNet.length() > 0)
				{
					sbAllNet.append(",");
				}
				sbAllNet.append(allNet);

				if (sbFirmNames.length() > 0)
				{
					sbFirmNames.append(",");
				}
				sbFirmNames.append("\"" + thisRE + "\"");
			}
			// Increment the comparison counter
			comparisonCounter++;
		}
		// 4. Getting the average of search results.
		if ((netAttorneyChangeModelBean.getComparisonDataTypeList() != null) 
				&& (!netAttorneyChangeModelBean.getComparisonDataTypeList().isEmpty())
				&& (netAttorneyChangeModelBean.getComparisonDataTypeList().contains(ComparisonDataType.AVERAGE.getValue()))
				&& (netAttorneyChangeModelBean.getFirmList() != null) 
				&& (!netAttorneyChangeModelBean.getFirmList().isEmpty()))
		{

			double partnersAdded = 0;
			double counselsAdded = 0;
			double associatesAdded = 0;
			double partnersMinus = 0;
			double counselsMinus = 0;
			double associatesMinus = 0;
			double partnersNet = 0;
			double counselsNet = 0;
			double associatesNet = 0;
			double allNet = 0;
			double allAdded = 0;
			double allMinus = 0;
			double counter = 0;
			for (PeopleResultDTO prDTO : peopleSearchResults)
			{
				if (prDTO.getCompanyPeople() != null)
				{
					PeopleResult pr = prDTO.getCompanyPeople();
					associatesAdded += pr.getAssociateCountPlus();
					associatesMinus += pr.getAssociateCountMinus();

					partnersAdded += pr.getPartnerCountPlus();
					partnersMinus += pr.getPartnerCountMinus();

					counselsAdded += pr.getOtherCounselCountPlus();
					counselsMinus += pr.getOtherCounselCountMinus();

					counter++;
				}
			}

			if (counter > 0)
			{
				partnersNet = (float) (partnersAdded - partnersMinus) / counter;
				counselsNet = (float) (counselsAdded - counselsMinus) / counter;
				associatesNet = (float) (associatesAdded - associatesMinus) / counter;

				allAdded = (float) (partnersAdded + counselsAdded + associatesAdded) / counter;
				allMinus = (float) (partnersMinus + counselsMinus + associatesMinus) / counter;
				allNet = (float) (partnersNet + counselsNet + associatesNet);

				associatesAdded = (float) associatesAdded / counter;
				associatesMinus = (float) associatesMinus / counter;

				partnersAdded = (float) partnersAdded / counter;
				partnersMinus = (float) partnersMinus / counter;

				counselsAdded = (float) counselsAdded / counter;
				counselsMinus = (float) counselsMinus / counter;

				partnersAdded = Math.round(partnersAdded);
				partnersMinus = Math.round(partnersMinus);
				counselsAdded = Math.round(counselsAdded);
				counselsMinus = Math.round(counselsMinus);
				associatesAdded = Math.round(associatesAdded);
				associatesMinus = Math.round(associatesMinus);

				partnersNet = Math.round(partnersNet);
				counselsNet = Math.round(counselsNet);
				associatesNet = Math.round(associatesNet);
				allAdded = Math.round(allAdded);
				allMinus = Math.round(allMinus);
				allNet = Math.round(allNet);
				
			}
			if (sbAssociatesAdd.length() > 0)
			{
				sbAssociatesAdd.append(",");
			}
			sbAssociatesAdd.append(associatesAdded);
			if (sbAssociatesRemoved.length() > 0)
			{
				sbAssociatesRemoved.append(",");
			}
			sbAssociatesRemoved.append(associatesMinus);
			if (sbAssociatesNet.length() > 0)
			{
				sbAssociatesNet.append(",");
			}
			sbAssociatesNet.append(associatesNet);
			if (sbCounselsAdd.length() > 0)
			{
				sbCounselsAdd.append(",");
			}
			sbCounselsAdd.append(counselsAdded);
			if (sbCounselsRemoved.length() > 0)
			{
				sbCounselsRemoved.append(",");
			}
			sbCounselsRemoved.append(counselsMinus);
			if (sbCounselsNet.length() > 0)
			{
				sbCounselsNet.append(",");
			}
			sbCounselsNet.append(counselsNet);
			if (sbPartnersAdd.length() > 0)
			{
				sbPartnersAdd.append(",");
			}
			sbPartnersAdd.append(partnersAdded);
			if (sbPartnersRemoved.length() > 0)
			{
				sbPartnersRemoved.append(",");
			}
			sbPartnersRemoved.append(partnersMinus);
			if (sbPartnersNet.length() > 0)
			{
				sbPartnersNet.append(",");
			}
			sbPartnersNet.append(partnersNet);
			if (sbAllAdded.length() > 0)
			{
				sbAllAdded.append(",");
			}
			sbAllAdded.append(allAdded);

			if (sbAllRemoved.length() > 0)
			{
				sbAllRemoved.append(",");
			}
			sbAllRemoved.append(allMinus);

			if (sbAllNet.length() > 0)
			{
				sbAllNet.append(",");
			}
			sbAllNet.append(allNet);

			if (sbFirmNames.length() > 0)
			{
				sbFirmNames.append(",");
			}
			sbFirmNames.append("\"Average\"");
			// Increment the comparison counter
			comparisonCounter++;
		}

		comparisonResult.addAll(comparisonResults); // add remaining search
													// results to end of
													// comparison List

		for (PeopleResultDTO prDTO : comparisonResult)
		{
			if (prDTO.getCompanyPeople() != null)
			{
				if (sbAssociatesAdd.length() > 0)
				{
					sbAssociatesAdd.append(",");
				}
				sbAssociatesAdd.append(prDTO.getCompanyPeople().getAssociateCountPlus());
				if (sbAssociatesRemoved.length() > 0)
				{
					sbAssociatesRemoved.append(",");
				}
				sbAssociatesRemoved.append(prDTO.getCompanyPeople().getAssociateCountMinus());
				if (sbAssociatesNet.length() > 0)
				{
					sbAssociatesNet.append(",");
				}
				sbAssociatesNet.append(prDTO.getCompanyPeople().getAssociateCountPlus() - prDTO.getCompanyPeople().getAssociateCountMinus());
				if (sbCounselsAdd.length() > 0)
				{
					sbCounselsAdd.append(",");
				}
				sbCounselsAdd.append(prDTO.getCompanyPeople().getOtherCounselCountPlus());
				if (sbCounselsRemoved.length() > 0)
				{
					sbCounselsRemoved.append(",");
				}
				sbCounselsRemoved.append(prDTO.getCompanyPeople().getOtherCounselCountMinus());
				if (sbCounselsNet.length() > 0)
				{
					sbCounselsNet.append(",");
				}
				sbCounselsNet.append(prDTO.getCompanyPeople().getOtherCounselCountPlus() - prDTO.getCompanyPeople().getOtherCounselCountMinus());
				if (sbPartnersAdd.length() > 0)
				{
					sbPartnersAdd.append(",");
				}
				sbPartnersAdd.append(prDTO.getCompanyPeople().getPartnerCountPlus());
				if (sbPartnersRemoved.length() > 0)
				{
					sbPartnersRemoved.append(",");
				}
				sbPartnersRemoved.append(prDTO.getCompanyPeople().getPartnerCountMinus());
				if (sbPartnersNet.length() > 0)
				{
					sbPartnersNet.append(",");
				}
				sbPartnersNet.append(prDTO.getCompanyPeople().getPartnerCountPlus() - prDTO.getCompanyPeople().getPartnerCountMinus());
				if (sbAllAdded.length() > 0)
				{
					sbAllAdded.append(",");
				}
				sbAllAdded.append(prDTO.getCompanyPeople().getAssociateCountPlus() + prDTO.getCompanyPeople().getOtherCounselCountPlus()
						+ prDTO.getCompanyPeople().getPartnerCountPlus());

				if (sbAllRemoved.length() > 0)
				{
					sbAllRemoved.append(",");
				}
				sbAllRemoved.append(-prDTO.getCompanyPeople().getAssociateCountMinus() - prDTO.getCompanyPeople().getOtherCounselCountMinus()
						- prDTO.getCompanyPeople().getPartnerCountMinus());

				if (sbAllNet.length() > 0)
				{
					sbAllNet.append(",");
				}
				sbAllNet.append(prDTO.getCompanyPeople().getAssociateCountPlus() - prDTO.getCompanyPeople().getAssociateCountMinus()
						+ prDTO.getCompanyPeople().getOtherCounselCountPlus() - prDTO.getCompanyPeople().getOtherCounselCountMinus()
						+ prDTO.getCompanyPeople().getPartnerCountPlus() - prDTO.getCompanyPeople().getPartnerCountMinus());
			}
		}

		for (PeopleResultDTO prDTO : comparisonResult)
		{
			if (sbFirmNames.length() > 0)
			{
				sbFirmNames.append(",");
			}
			sbFirmNames.append("\"" + prDTO.getCompanyName() + "\"");
		}

		model.addAttribute("sbFirmNames", sbFirmNames);

		model.put("comparisonCounter", "\"" + comparisonCounter + "\"");

		model.put("sbAssociatesAdd", sbAssociatesAdd.toString());
		model.put("sbCounselsAdd", sbCounselsAdd.toString());
		model.put("sbPartnersAdd", sbPartnersAdd.toString());

		model.put("sbAssociatesRemoved", sbAssociatesRemoved.toString());
		model.put("sbCounselsRemoved", sbCounselsRemoved.toString());
		model.put("sbPartnersRemoved", sbPartnersRemoved.toString());

		model.put("sbAssociatesNet", sbAssociatesNet.toString());
		model.put("sbCounselsNet", sbCounselsNet.toString());
		model.put("sbPartnersNet", sbPartnersNet.toString());

		model.put("sbAllNet", sbAllNet.toString());
		model.put("sbAllRemoved", sbAllRemoved.toString());
		model.put("sbAllAdded", sbAllAdded.toString());
	}

	@ActionMapping(params = "action=netChangeAttorneysChartCriteriaChange")
	public void newsAndPublicationsSubmitForm(@ModelAttribute("netAttorneyChangeModelBean") StandardChartModelBean netAttorneyChangeModelBean, 
											  ActionRequest request,
											  ActionResponse response) throws Exception
	{
		String 	currentPortletId 	= WebUtil.getCurrentPortletId(request);
		String 	currentPage			= WebUtil.getCurrentPage(request);
		boolean	isHomePage			= ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		User	currentUser 		= WebUtil.getCurrentUser(request, userService);
		if (!isHomePage)
		{
			currentPage = "FIRM_STATISTICS";
		}
		userService.saveUserPreferences(currentUser.getId(), 
										currentPortletId, 
										null, 
										WebUtil.getJson(netAttorneyChangeModelBean), 
										currentPage);
	}
	
	/*
	* Following method remove rendering portlet from the page. And redirects to same page.
	*
	*/

	@ActionMapping(params = "action=removePortleFromThePage")
	public void removePortletFromPage(ActionRequest request, ActionResponse response) throws Exception
	{
		WebUtil.removePortlet(request);
	}
}
