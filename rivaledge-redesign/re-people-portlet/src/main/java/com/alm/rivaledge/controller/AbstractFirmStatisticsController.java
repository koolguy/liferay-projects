package com.alm.rivaledge.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.alm.rivaledge.model.ClickToView;
import com.alm.rivaledge.model.PeopleSearchModelBean;
import com.alm.rivaledge.model.chart.BaseChartModelBean.ComparisonDataType;
import com.alm.rivaledge.model.chart.BaseChartModelBean.FirmDataType;
import com.alm.rivaledge.model.chart.StandardChartModelBean;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserPreferences;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.service.PeopleService;
import com.alm.rivaledge.service.UserService;
import com.alm.rivaledge.service.WatchlistService;
import com.alm.rivaledge.transferobject.PeopleResult;
import com.alm.rivaledge.transferobject.PeopleResultDTO;
import com.alm.rivaledge.transferobject.PeopleSearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;

/**
 * People controller responsible for all operation on People search form and
 * results screen
 * 
 * @author FL605
 * @since Sprint 2
 */
public abstract class AbstractFirmStatisticsController
{
	
	private Log _log = LogFactoryUtil.getLog(AbstractFirmStatisticsController.class.getName());
	
	@Autowired
	protected PeopleService peopleService;

	@Autowired
	protected FirmService firmService;

	@Autowired
	protected WatchlistService watchlistService;

	@Autowired
	protected UserService userService;

	public static final int CHART_PERCENTAGE_ATTORNEYS = 1;

	public static final int CHART_NO_OF_ATTORNEYS = 2;

	public static final int CHART_NO_OF_PRACTICES = 3;

	public static final int CHART_NO_OF_ATTORNEYS_BY_PRACTICE = 4;

	/**
	 * Populates the Model which holds the Search criteria in search form,
	 * initializing to default values
	 * 
	 * @param model
	 */

	@ModelAttribute
	public void populatePeopleSearchModelBean(ModelMap model, PortletRequest request)
	{
		String					currentPortletId		 = WebUtil.getCurrentPortletId(request);
		String					currentPage				 = WebUtil.getCurrentPage(request);
		User 					currentUser 			 = (model.get("currentUser") != null) ? ((User) model.get("currentUser")) : WebUtil.getCurrentUser(request, userService); 
		boolean					isHomePage				 = ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));

		boolean 				isC2VPage				 = 	WebUtil.isClickToViewPage(request);
		model.addAttribute("isC2VPage", isC2VPage);
		
		boolean					isAdminUser				 = WebUtil.isAdminUser(currentUser);
																
		PeopleSearchModelBean 	peopleSearchModelBean	 = (PeopleSearchModelBean) model.get("peopleSearchModelBean"); //get the bean, may be null don't panic, we'll handle it!!
		if (currentUser != null)
		{
			model.put("currentUser", currentUser);
			model.put("isAdminUser", isAdminUser);
			
		}
		
		/** You are on home page, so get the ModelBean from DB without any hesitation, be it for 
		* 1. Search Portlet popup.
		* 2. Chart 
		*/
		if(isHomePage)
		{
			peopleSearchModelBean = getUserPreference(currentUser.getId(), currentPortletId, currentPage);
		}
		
		//Neither on home Page nor you have ready to use bean in Session
		if (!isHomePage && model.get("peopleSearchModelBean") == null) // you are on search page, directly from navigation, for the first Time!!
		{
				currentPortletId 	= "FIRM_STATISTICS";
				currentPage			= "FIRM_STATISTICS";
				peopleSearchModelBean = getUserPreference(currentUser.getId(), currentPortletId, currentPage); // get the search criteria from DB
		}
			
		//Still we don't have a bean? Create it!! 
		if (peopleSearchModelBean == null) // user didn't have an entry in DB yet
		{
			peopleSearchModelBean = getDefaultPeopleSearchModelBean(currentUser); // create the default search criteria
			
		}
		
		//TODO don't know the reason, what is this doing here!, but I'm playing safe as I don't know its effect if I comment it out
		//will comment this line and test when I have ample time :D :D :D
		peopleSearchModelBean.setPortletId(currentPortletId);
		
		// Either Home Page or "Click to View" Page in both the cases the bean should die after the page is rendered and shudn't make any love with session
		if(isHomePage || isC2VPage)
		{
			peopleSearchModelBean.setAddToSession(false);
		}
		
		// reflect the changes to bean by adding it to model
		model.addAttribute("peopleSearchModelBean", peopleSearchModelBean);

		/**
		 * TODO
		 *  Actually I'm thinking of removing the DTO and searchresults from session 
		 *  Reasons
		 *  1. DTO is created from bean most of the times in all controllers
		 *  2. What's the need to cache the search results when we fetch them all d time for all requests apart from drill down
		 *   
		 */
		//normal play, I don't have to tell you about this stuff at this point
		PeopleSearchDTO peopleSearchDTO = getDTOFromModelBean(request, model, peopleSearchModelBean);
		model.addAttribute("peopleSearchDTO", peopleSearchDTO);

		if (model.get("peopleSearchResults") == null)
		{
			model.addAttribute("peopleSearchResults", new ArrayList<PeopleResultDTO>());
		}
	}
	
	protected PeopleSearchModelBean getDefaultPeopleSearchModelBean(User currentUser)
	{
		List<UserGroup> 		allWatchListsDefaultList = null;
		
		PeopleSearchModelBean peopleSearchModelBean = new PeopleSearchModelBean();
		peopleSearchModelBean.init(); //do an explicit init for default values

		if (currentUser != null)
		{
			allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
		}

		List<String> firmList = new ArrayList<String>();
		List<Integer> firmListWatchList = new ArrayList<Integer>();

		if (allWatchListsDefaultList == null || allWatchListsDefaultList.isEmpty())
		{
			//FirmType
			peopleSearchModelBean.setFirmType(ALMConstants.RIVALEDGE_LIST);
			firmList.add(ALMConstants.AMLAW_100);
		}
		else
		{
			peopleSearchModelBean.setFirmType(ALMConstants.WATCH_LIST);
			//firmListWatchList.add("" + allWatchListsDefaultList.get(0).getGroupId());
			firmListWatchList.add(allWatchListsDefaultList.get(0).getGroupId());
		}
		peopleSearchModelBean.setFirmList(firmList);
		peopleSearchModelBean.setFirmListWatchList(firmListWatchList);
		return peopleSearchModelBean;
	}


	
	
	/**
	 * Based on the User Action, where the request is coming from, This will alter the searchBean, the searchDTO if required 
	 * and put them back to C2V.
	 * @param request
	 * @param model
	 * @param c2v
	 * @throws UnsupportedEncodingException
	 */
	protected void honorClick2View(PortletRequest request, ModelMap model, ClickToView<PeopleSearchModelBean, PeopleSearchDTO> c2v) throws UnsupportedEncodingException
	{
		HttpServletRequest originalRequest = WebUtil.getHttpServletRequest(request);
		
		PeopleSearchModelBean peopleSearchModelBean = null;
		
		if(!WebUtil.isClickToViewPage(request))
		{
			return; // its not C2V, then what the hell will I do here I'm returning, bye!!
		}
		
		// I'm here because I have to respect the C2V request of the user
		String searchCriteria = originalRequest.getParameter("searchCriteria");
		
		if (StringUtils.isNotBlank(searchCriteria))
		{
			//Niranjan : Its observed that the parameter is already decoded by the time we read it via originalRequest.getParameter();
			// so doing a decode again is replacing all the "+" with " " (space) and introducing new bugs (case : firmSize "1500+" decoded to "1500 ")
			// I'm not sure if this is because of any server specific settings or from underlying implementation.
			//I've decided to follow this blindly as it works for me!!
			String searchJSON = searchCriteria; // URLDecoder.decode(searchCriteria, "UTF-8");
			peopleSearchModelBean = WebUtil.getObject(searchJSON, PeopleSearchModelBean.class); // get the SearchBean
		}
		else
		{
			peopleSearchModelBean =  WebUtil.cloneBean(c2v.getSearchModelBean(),  PeopleSearchModelBean.class); // clone the bean, because we don't want to alter the original search criteria
		}
		
		String dateText = originalRequest.getParameter("drilldownDate");
		if (StringUtils.isNotBlank(dateText)) //do we have dateText in the request?
		{
			dateText = URLDecoder.decode(dateText, "UTF-8");
			
			SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
			try {
				Date d = new SimpleDateFormat("MM-dd-yy").parse(dateText);
				dateText = formatter.format(d);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				_log.error("Exception: Not a proper date "+dateText, e);
			}
			
			dateText = dateText.replaceAll("-", "/");
			
			dateText = dateText + "-" + dateText;
			peopleSearchModelBean.setDateText(dateText);
			
		}
		
		
		String firmName = originalRequest.getParameter("drilldownFirmName");

		if (StringUtils.isNotBlank(firmName)) //do we have firmName in the request?
		{
			String drilldownFirm = URLDecoder.decode(firmName, "UTF-8");
			List<Firm> matchingFirms = firmService.getFirmByName(drilldownFirm);
			
			if((matchingFirms != null) && (!matchingFirms.isEmpty()))
			{
				Firm firm = matchingFirms.get(0);
				
				peopleSearchModelBean.setFirmType(ALMConstants.INDIVIDUAL_FIRMS); // set it to Individual as we know there is single firm

				List<String> selectedFirmsList = new ArrayList<String>(1);
				selectedFirmsList.add(""+firm.getCompanyId()); // add Firm Id as String
				
				peopleSearchModelBean.setFirmList(selectedFirmsList); // actual Ids to get DTO
				peopleSearchModelBean.setSelectedFirms(firm.getCompany()); // this will go into SearchCriteria String
				
			}
		}
		
		String practiceArea = originalRequest.getParameter("drilldownPracticeArea");

		if (StringUtils.isNotBlank(practiceArea))
		{
			String searchPractice = URLDecoder.decode(practiceArea, "UTF-8");
			peopleSearchModelBean.setSelectedPracticeArea(searchPractice);
			List<String> selectedPractices = new ArrayList<String>(1);
			selectedPractices.add(searchPractice);
			peopleSearchModelBean.setPracticeArea(selectedPractices);
		}
		
		peopleSearchModelBean.setGoToPage(1);
		peopleSearchModelBean.setAddToSession(false); // don't add this to session. But, only to model
		
		model.addAttribute("peopleSearchModelBean", peopleSearchModelBean); 
		PeopleSearchDTO peopleSearchDTO = getDTOFromModelBean(request, model, peopleSearchModelBean);
		c2v.setSearchModelBean(peopleSearchModelBean); // set the changed bean back to C2V
		c2v.setSearchDTO(peopleSearchDTO); // set the changed DTO back to C2V
	}

	
	protected PeopleSearchModelBean getUserPreference(Integer userId, String portletId, String currentPage)
	{
		UserPreferences uP = userService.getUserPreferenceByUserIdAndPortletId(userId, portletId, currentPage);
		if ((uP != null) && (uP.getSearchCriteriaJSON() != null) && (uP.getSearchCriteriaJSON().trim().length() > 0))
		{
			return (WebUtil.getObject(uP.getSearchCriteriaJSON(), PeopleSearchModelBean.class));
		}
		return (null);
	}
	
	protected StandardChartModelBean getUserChartPreference(Integer userId, String portletId, String currentPage)
	{
		UserPreferences uP = userService.getUserPreferenceByUserIdAndPortletId(userId, portletId, currentPage);
		if ((uP != null) && (uP.getChartCriteriaJSON() != null) && (uP.getChartCriteriaJSON().trim().length() > 0))
		{
			return (WebUtil.getObject(uP.getChartCriteriaJSON(), StandardChartModelBean.class));
		}
		return (null);
	}

	
	
	/**
	 * Creates a PeopleSearchDTO from PeopleSearchModelBean
	 * 
	 * @param request
	 * @param model
	 * @param peopleSearchModelBean
	 * @return
	 */
	public PeopleSearchDTO getDTOFromModelBean(PortletRequest request, ModelMap model, PeopleSearchModelBean peopleSearchModelBean)
	{

		PeopleSearchDTO peopleSearchDTO = new PeopleSearchDTO();

		// Get the page number from the model bean
		int pageNumber = peopleSearchModelBean.getGoToPage();
		int orderBy = peopleSearchModelBean.getOrderBy();
		int searchResultsPerPage = peopleSearchModelBean.getSearchResultsPerPage();

		peopleSearchDTO.setOrderBy(orderBy);
		peopleSearchDTO.setResultPerPage(searchResultsPerPage);

		// Plug the page number into the search DTO
		//This sets the starting limit for the paginated search results in the sql query
		peopleSearchDTO.setPageNumber(pageNumber < 0 ? 0 : (pageNumber - 1) * searchResultsPerPage);

		// Set the sort column and order into the search DTO
		peopleSearchDTO.setSortColumn(peopleSearchModelBean.getSortColumn());
		peopleSearchDTO.setSortOrder(peopleSearchModelBean.getSortOrder());

		// Get the selected firms, if any, and set them into the search DTO
		setSelectedFirms(model, peopleSearchDTO, peopleSearchModelBean, request);

		// Set the date range criteria
		// There will always be a date range criteria, as the form view
		// does not allow this to be null
		setDateRange(model, peopleSearchDTO, peopleSearchModelBean);

		// Set the remaining search criteria, such as practice areas, etc.
		// See the setOtherSearchParameter method for more details
		setOtherSearchParameter(model, peopleSearchDTO, peopleSearchModelBean);

		setViewSettings(request, model, peopleSearchDTO, peopleSearchModelBean);
		return peopleSearchDTO;
	}

	/**
	 * Sets appropriate values on searchDTO from ModelBean for Comparison Data
	 * 
	 * @param request
	 * @param model
	 * @param peopleSearchDTO
	 * @param peopleSearchModelBean
	 */
	private void setViewSettings(PortletRequest request, ModelMap model, PeopleSearchDTO peopleSearchDTO,
			PeopleSearchModelBean peopleSearchModelBean)
	{
		//comparison data should show up only on first Page
		if (peopleSearchModelBean.getGoToPage() != 1)
		{
			return;
		}

		List<String> comparisonDataTypeList = peopleSearchModelBean.getComparisonDataTypeList();

		//1. My Firm Selected
		if (comparisonDataTypeList != null && comparisonDataTypeList.contains(ComparisonDataType.MY_FIRM.getValue()))
		{
			User currentUser = WebUtil.getCurrentUser(request, userService);
			List<Firm> myFirmList = WebUtil.getCurrentUserFirm(currentUser, firmService);
			if (!myFirmList.isEmpty())
			{
				peopleSearchDTO.setMyFirm(myFirmList.get(0));
			}
		}

		//2. RivalEdge List selected
		if (comparisonDataTypeList != null && comparisonDataTypeList.contains(ComparisonDataType.RIVAL_EDGE.getValue()))
		{
			List<String> rivalEdgeList = peopleSearchModelBean.getRivalEdgeList();
			if (rivalEdgeList != null && !rivalEdgeList.isEmpty())
			{

				if (rivalEdgeList.contains(ALMConstants.AMLAW_100))
				{
					peopleSearchDTO.setIsAmLaw100Selected(true);
				}

				if (rivalEdgeList.contains(ALMConstants.AMLAW_200))
				{
					peopleSearchDTO.setIsAmLaw200Selected(true);
				}

				if (rivalEdgeList.contains(ALMConstants.NLJ_250))
				{
					peopleSearchDTO.setIsNLJ250Selected(true);
				}

			}
		}

		// 3. WatchList selected
		if (comparisonDataTypeList != null && comparisonDataTypeList.contains(ComparisonDataType.WATCHLIST_AVG.getValue()))
		{

			List<String> watchListIds = peopleSearchModelBean.getWatchList();
			if (watchListIds != null && !watchListIds.isEmpty())
			{
				List<Integer> selectedWatchListIds = new ArrayList<Integer>(watchListIds.size());

				for (String id : watchListIds)
				{
					selectedWatchListIds.add(Integer.parseInt(id));
				}
				peopleSearchDTO.setSelectedWatchlists(selectedWatchListIds);
			}

		}
		//4. Average of SearchResults is done after fetching the search Results
		/*if (comparisonDataTypeList != null && comparisonDataTypeList.contains(ComparisonDataType.AVERAGE.getValue()))
		{
			
		}*/
	}

	/***
	 * Transforms , and ; separated values at 0-index to List values <br>
	 * We need values in List rather than CSV, so that they can bind to
	 * <code>Spring Form<code> 
	 * later on from session
	 * 
	 * @param peopleSearchModelBean
	 */
	public void transformBean(PeopleSearchModelBean peopleSearchModelBean)
	{

		List<String> firmList = peopleSearchModelBean.getFirmList();

		if (firmList != null && !firmList.isEmpty())
		{
			String firm = firmList.get(0);
			peopleSearchModelBean.setFirmList(ListUtil.fromArray(StringUtil.split(firm, ","))); //firmList is , separated
		}

		List<String> contentTypeList = peopleSearchModelBean.getLocations();

		if (contentTypeList != null && !contentTypeList.isEmpty())
		{
			String contentType = contentTypeList.get(0);
			peopleSearchModelBean.setLocations(ListUtil.fromArray(StringUtil.split(contentType, ";")));
		}

		List<String> practiceAreaList = peopleSearchModelBean.getPracticeArea();

		if (practiceAreaList != null && !practiceAreaList.isEmpty())
		{
			String practiceArea = practiceAreaList.get(0);
			peopleSearchModelBean.setPracticeArea(ListUtil.fromArray(StringUtil.split(practiceArea, ";")));
		}

		List<String> firmSizeList = peopleSearchModelBean.getFirmSizeList();

		if (firmSizeList != null && !firmSizeList.isEmpty())
		{
			String firmSize = firmSizeList.get(0);
			peopleSearchModelBean.setFirmSizeList(ListUtil.fromArray(StringUtil.split(firmSize, ";")));
		}
	}

	/**
	 * Set date range in FirmSearchDTO reference based on user input on Firm
	 * search Screen
	 * 
	 * @param request
	 * @param response
	 * @param peopleSearchDTO
	 */
	protected void setDateRange(ModelMap model, PeopleSearchDTO peopleSearchDTO, PeopleSearchModelBean peopleSearchModelBean)
	{
		String dateText = peopleSearchModelBean.getDateText();

		Calendar calendar = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		peopleSearchDTO.setToDate(date);
		if (dateText != null && !dateText.contains("-"))
		{

			if (dateText.equals(ALMConstants.ANY))
			{
				peopleSearchDTO.setFromDate(null);
				peopleSearchDTO.setToDate(null);
			}
			else if (dateText.equals(ALMConstants.LAST_WEEK))
			{
				calendar.add(Calendar.WEEK_OF_MONTH, -1);
				peopleSearchDTO.setFromDate(calendar.getTime());
			}
			else if (dateText.equals(ALMConstants.LAST_30_DAYS))
			{
				calendar.add(Calendar.MONTH, -1);
				peopleSearchDTO.setFromDate(calendar.getTime());
			}
			else if (dateText.equals(ALMConstants.LAST_90_DAYS))
			{
				calendar.add(Calendar.MONTH, -3);
				peopleSearchDTO.setFromDate(calendar.getTime());
			}
			else if (dateText.equals(ALMConstants.LAST_6_MONTHS))
			{
				calendar.add(Calendar.MONTH, -6);
				peopleSearchDTO.setFromDate(calendar.getTime());
			}
			else
			{
				calendar.add(Calendar.YEAR, -1);
				peopleSearchDTO.setFromDate(calendar.getTime());
			}

		}
		else
		{
			String[] dateRange = dateText.split("-");
			try
			{
				peopleSearchDTO.setFromDate(dateFormat.parse(dateRange[0]));
				peopleSearchDTO.setToDate(dateFormat.parse(dateRange[1]));
			}
			catch (ParseException e)
			{
				e.printStackTrace();
			}

		}

	}

	/**
	 * Set Practice Area, Firm Size, Location e in PeopleSearchDTO reference
	 * based on user input on People search Screen
	 * 
	 * @param model
	 * @param peopleSearchDTO
	 * @param peopleSearchModelBean
	 */

	protected void setOtherSearchParameter(ModelMap model, PeopleSearchDTO peopleSearchDTO,
			PeopleSearchModelBean peopleSearchModelBean)
	{
		if (peopleSearchModelBean.getLocations() != null
				&& !peopleSearchModelBean.getLocations().contains(ALMConstants.ALL_LOCATIONS))
		{
			peopleSearchDTO.setLocations(peopleSearchModelBean.getLocations());
		}
		if (peopleSearchModelBean.getPracticeArea() != null
				&& !peopleSearchModelBean.getPracticeArea().contains(ALMConstants.ALL_PRACTICE_AREAS))
		{
			peopleSearchDTO.setPracticeArea(peopleSearchModelBean.getPracticeArea());
		}
		if (peopleSearchModelBean.getFirmSizeList() != null
				&& !peopleSearchModelBean.getFirmSizeList().contains(ALMConstants.ALL_FIRM_SIZES))
		{
			peopleSearchDTO.setFirmSizeList(peopleSearchModelBean.getFirmSizeList());
		}

		peopleSearchModelBean.setSelectedLocations(peopleSearchModelBean.getLocations().toString().replace("[", "").replace("]", ""));
		peopleSearchModelBean.setSelectedPracticeArea(peopleSearchModelBean.getPracticeArea().toString().replace("[", "").replace("]", ""));
		peopleSearchModelBean.setSelectedFirmSize(peopleSearchModelBean.getFirmSizeList().toString().replace("[", "").replace("]", ""));
		

	}

	/**
	 * Set Firms information in PeopleSearchDTO reference based on user input on
	 * People search Screen
	 * 
	 * @param model
	 * @param peopleSearchDTO
	 */
	protected void setSelectedFirms(ModelMap model, PeopleSearchDTO peopleSearchDTO, PeopleSearchModelBean peopleSearchModelBean,
			PortletRequest request)
	{

		String firmType = peopleSearchModelBean.getFirmType();
		List<Firm> firmsList = null;
		List<Firm> firmListWatchList = new ArrayList<Firm>();
		List<String> firmsListForResults = new ArrayList<String>();

		if (ALMConstants.ALL_FIRMS.equals(firmType))
		{
			firmsList = new ArrayList<Firm>();
			firmsList.addAll(firmService.getAllFirms());
			peopleSearchDTO.setSelectedFirms(firmsList);
			firmsListForResults.add(firmType);
			peopleSearchModelBean.setFirmListWatchList(null);
			peopleSearchModelBean.setFirmList(null);
		}
		else if (ALMConstants.WATCH_LIST.equals(firmType))
		{
			firmsList = new ArrayList<Firm>();
			User currentUser = WebUtil.getCurrentUser(request, userService);
			String 	watchListsName = request.getParameter("Firmstext");
			
			List<Firm> firmsWatchList = null;
			if (currentUser != null)
			{
				firmsWatchList = watchlistService.findAllFirmsInManyWatchlists(peopleSearchModelBean.getFirmListWatchList(),
						currentUser.getId());
			}

			firmListWatchList.addAll(firmsWatchList);
			firmsList.addAll(firmsWatchList);
			/*
			for (Firm firmValue : firmsWatchList)
			{
				if (firmValue != null)
				{
					firmsListForResults.add(firmValue.getCompany());
				}
			}*/

			if (firmsWatchList.size() == 0)
			{
				List<Firm> notAvailableFirms = new ArrayList<Firm>();
				Firm tempFirm = new Firm();
				tempFirm.setCompanyId(0);
				notAvailableFirms.add(tempFirm);
				peopleSearchDTO.setSelectedFirms(notAvailableFirms);
			}
			else
			{
				peopleSearchDTO.setSelectedFirms(firmsWatchList);
			}
			peopleSearchModelBean.setFirmList(null);

			if(watchListsName == null)
			{
				List<UserGroup> allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
				if(allWatchListsDefaultList != null)
				{
					firmsListForResults.add(allWatchListsDefaultList.get(0).getGroupName());
				}
			}
			else
			{
				firmsListForResults.add(watchListsName);
			}
		}
		else if (ALMConstants.RIVALEDGE_LIST.equals(firmType))
		{
			firmsList = new ArrayList<Firm>();
			firmsList.addAll(firmService.getRanking(peopleSearchModelBean.getFirmList().get(0)));
			firmsListForResults.add(peopleSearchModelBean.getFirmList().get(0));
			peopleSearchModelBean.setFirmListWatchList(null);
			peopleSearchDTO.setSelectedFirms(firmsList);
		}
		else
		{ // User selected individual values
			firmsList = new ArrayList<Firm>();
			for (String firmId : peopleSearchModelBean.getFirmList())
			{
				try
				{
					Firm firmObject = firmService.getFirmById(Integer.parseInt(firmId));
					firmsList.add(firmObject);
					firmsListForResults.add(firmObject.getCompany());
				}
				catch (NumberFormatException nfEx)
				{
					// Do nothing.
				}
			}

			peopleSearchDTO.setSelectedFirms(firmsList);
			peopleSearchModelBean.setFirmListWatchList(null);
		}

		peopleSearchModelBean.setSelectedFirms(firmsListForResults.toString().replace("[", "").replace("]", ""));
	}

	/*
	 * Function takes peopleSearchResults object puts each count in jsonObject.
	 * And jsonObject added to jsonArray and jsonArray is returned.
	 */
	public JSONArray getJson(List<PeopleResultDTO> peopleSearchResults) throws JSONException
	{
		JSONArray jsonArray = new JSONArray();

		/*for(PeopleResultDTO peopleResult: peopleSearchResults){
			_log.debug(peopleResult.getPeopleResults().get(0));
		}*/

		for (PeopleResultDTO peopleResultDTO : peopleSearchResults)
		{
			JSONObject jsonObject = new JSONObject(); // JsonObject initialized.
			// All count variables initialized.
			int partnerPlus = 0;
			int partnerMinus = 0;
			int partnerCount = 0;
			int associatePlus = 0;
			int associateMinus = 0;
			int associateCount = 0;
			int otherPlus = 0;
			int otherMinus = 0;
			int otherCount = 0;
			int headCount = 0;
			for (PeopleResult peopleResult : peopleResultDTO.getPeopleResults())
			{
				// Count incremented for each firm individualy.
				partnerPlus += peopleResult.getPartnerCountPlus();
				partnerMinus += peopleResult.getPartnerCountMinus();
				partnerCount += peopleResult.getPartnerCount();

				associatePlus += peopleResult.getAssociateCountPlus();
				associateMinus += peopleResult.getAssociateCountMinus();
				associateCount += peopleResult.getAssociateCount();

				otherPlus += peopleResult.getOtherCounselCountPlus();
				otherMinus += peopleResult.getOtherCounselCountMinus();
				otherCount += peopleResult.getOtherCounselCount();

				headCount += peopleResult.getHeadCount();
			}
			// Values are put into jsonObject.
			jsonObject.put("firmName", peopleResultDTO.getCompanyName());

			jsonObject.put("partnerCount", partnerCount);
			jsonObject.put("partnerPlus", partnerPlus);
			jsonObject.put("partnerMinus", partnerMinus);

			jsonObject.put("associateCount", associateCount);
			jsonObject.put("associatePlus", associatePlus);
			jsonObject.put("associateMinus", associateMinus);

			jsonObject.put("otherConselCount", otherCount);
			jsonObject.put("otherConselPlus", otherPlus);
			jsonObject.put("otherConselMinus", otherMinus);

			jsonObject.put("totalEmployeeCount", headCount);

			// JsonObject put into jsonArray.
			jsonArray.put(jsonObject);
		}
		// JsonArray is returned.
		return jsonArray;
	}

	/**
	 * Chart related Methods
	 */

	/**
	 * Class written to move javascript array data across multiple methods.
	 * Its really handy
	 * 
	 * @author Niranjan
	 * @since 23-Sep-2013
	 * @version 1.0
	 */
	protected class ChartData
	{
		public ChartData()
		{
		}

		public int type; // this is not Highchart type, It is the type of ALM Chart like No of Attorneys, Percent Attorneys
		public int compareCount = 0;
		public StringBuffer sbFirm = new StringBuffer();
		public StringBuffer sbPartners = new StringBuffer();
		public StringBuffer sbAssociates = new StringBuffer();
		public StringBuffer sbOtherCounsel = new StringBuffer();
		public StringBuffer sbTotals = new StringBuffer();
		public StringBuffer sbPracticeCount = new StringBuffer();
		public StringBuffer sbToolTipTitle = new StringBuffer();
		public StringBuffer sbToolTipCount = new StringBuffer();
	}

	/**
	 * Runs the business logic and prepares the data for each user selected
	 * comparison option
	 * 
	 * @param currentUser
	 * @param pacmb
	 * @param chartSearchDTO
	 * @param searchResults
	 * @param chartData
	 * @throws CloneNotSupportedException
	 */
	protected void prepareComparisonData(User currentUser, StandardChartModelBean pacmb, PeopleSearchDTO chartSearchDTO,
			List<PeopleResultDTO> searchResults, ChartData chartData) throws CloneNotSupportedException
	{
		//User is interest in his own Firm
		if (pacmb.getComparisonDataTypeList() != null
				&& pacmb.getComparisonDataTypeList().contains(ComparisonDataType.MY_FIRM.getValue()) && currentUser != null)
		{
			PeopleSearchDTO dto = (PeopleSearchDTO) chartSearchDTO.clone();
			//get the currentUser Firm
			Firm firm = firmService.getFirmById(Integer.parseInt(currentUser.getCompany()));

			List<Firm> firmList = new ArrayList<Firm>();
			firmList.add(firm);
			dto.setSelectedFirms(firmList);
			int resultSize = peopleService.getPeopleSearchCount(dto, currentUser);

			List<PeopleResultDTO> results = new ArrayList<PeopleResultDTO>();
			if (resultSize > 0)
			{
				if (chartData.type == CHART_NO_OF_PRACTICES)
				{
					results = peopleService.peopleSearch(dto, currentUser);
				}
				else
				{
					results = peopleService.peopleSearchWithoutDrilldown(dto, currentUser);//peopleService.peopleSearch(dto, currentUser);
				}
				prepareComparisonChartData(results, chartData, firmList.size(), false);
				chartData.sbFirm.append("\"" + firm.getCompany() + "\"");
			}

		}

		// User selected One of the AMLAW 100, AMLAW 200 or NLJ 250
		if (pacmb.getComparisonDataTypeList() != null
				&& pacmb.getComparisonDataTypeList().contains(ComparisonDataType.RIVAL_EDGE.getValue()))
		{
			PeopleSearchDTO dto = (PeopleSearchDTO) chartSearchDTO.clone();
			//get the currentUser Firm
			List<Firm> firmList = new ArrayList<Firm>();
			firmList.addAll(firmService.getRanking(pacmb.getFirmList().get(0)));
			dto.setSelectedFirms(firmList);
			int resultSize = peopleService.getPeopleSearchCount(dto, currentUser);

			List<PeopleResultDTO> results = new ArrayList<PeopleResultDTO>();
			if (resultSize > 0)
			{
				dto.setPageNumber(0);
				dto.setResultPerPage(100000);
				if (chartData.type == CHART_NO_OF_PRACTICES)
				{
					results = peopleService.peopleSearch(dto, currentUser);
				}
				else
				{
					results = peopleService.peopleSearchWithoutDrilldown(dto, currentUser);//peopleService.peopleSearch(dto, currentUser);
				}
				prepareComparisonChartData(results, chartData, firmList.size(), false);
				chartData.sbFirm.append("\"" + pacmb.getFirmList().get(0) + "\"");
			}

		}
		// User selected WatchList
		if ((pacmb.getComparisonDataTypeList() != null)
				&& (pacmb.getComparisonDataTypeList().contains(ComparisonDataType.WATCHLIST_AVG.getValue()))
				&& (pacmb.getWatchList() != null) 
				&& (!pacmb.getWatchList().isEmpty()))
		{
			PeopleSearchDTO dto = (PeopleSearchDTO) chartSearchDTO.clone();
			for (String watchListId : pacmb.getWatchList())
			{

				List<Firm> firmList = new ArrayList<Firm>();
				firmList.addAll(watchlistService.findAllFirmsInOneWatchlist(Integer.parseInt(watchListId), currentUser.getId()));
				dto.setSelectedFirms(firmList);
				UserGroup watchList = watchlistService.getWatchList(Integer.parseInt(watchListId), currentUser.getId());
				int resultSize = peopleService.getPeopleSearchCount(dto, currentUser);

				List<PeopleResultDTO> results = new ArrayList<PeopleResultDTO>();
				if (resultSize > 0)
				{
					dto.setPageNumber(0);
					dto.setResultPerPage(100000);
					if (chartData.type == CHART_NO_OF_PRACTICES)
					{
						results = peopleService.peopleSearch(dto, currentUser);
					}
					else
					{
						results = peopleService.peopleSearchWithoutDrilldown(dto, currentUser);//peopleService.peopleSearch(dto, currentUser);
					}
					prepareComparisonChartData(results, chartData, firmList.size(), false);

					if (watchList != null)
					{
						chartData.sbFirm.append("\"" + watchList.getGroupName() + "\"");
					}
					else
					{// We may not enter into this condition at all as we are always sure there exist a watchlist for a given id
						//its just to be on safer side
						chartData.sbFirm.append("\" Watch List " + watchListId + "\""); // render the id on UI
					}
				}
			}

		}

		if (pacmb.getComparisonDataTypeList() != null
				&& pacmb.getComparisonDataTypeList().contains(ComparisonDataType.AVERAGE.getValue()))
		{
			prepareComparisonChartData(searchResults, chartData, 1, true);//dummy size 
			chartData.sbFirm.append("\"Average\""); // hard coded for the time being, TODO  get from the Bean if required
		}
	}

	/**
	 * Returns the limited search results based on user selection
	 * 
	 * @param pacmb
	 * @param genericPeopleSearchResults
	 * @return
	 */
	protected List<PeopleResultDTO> refineSearchResultsByLimit(StandardChartModelBean pacmb,
			List<PeopleResultDTO> genericPeopleSearchResults)
	{

		List<PeopleResultDTO> returnMe = new ArrayList<PeopleResultDTO>();

		// User selected individual firms
		if ((FirmDataType.FIRM.getValue().equals(pacmb.getLimitType())) && (pacmb.getSearchResultsFirmList() != null)
				&& (!pacmb.getSearchResultsFirmList().isEmpty()))
		{
			for (String firmId : pacmb.getSearchResultsFirmList()) // run through the selected firmIds
			{
				//Firm firm = firmService.getFirmById(Integer.parseInt(firmId)); // fetch the firm

				for (PeopleResultDTO resultDTO : genericPeopleSearchResults) //search the firm in searchResults
				{
					if (resultDTO.getCompanyId().equals(Integer.parseInt(firmId))) //got a match
					{
						returnMe.add(resultDTO); //add it to return results
						break; // go to next selected firm
					}
				}
			}

		}
		else
		{ // user select either top or bottom results
			// Need a Comparator for descending order 
			Comparator<PeopleResultDTO> comparator = new Comparator<PeopleResultDTO>()
			{
				@Override
				public int compare(PeopleResultDTO o1, PeopleResultDTO o2)
				{
					//return WebUtil.compare(o1.getTotalAttorneyCount(), o2.getTotalAttorneyCount(), true); // sort in desc order
					return WebUtil.compare(o1.getCompanyPeople().getHeadCount(), o2.getCompanyPeople().getHeadCount(), true); // sort in desc order
				}
			};

			//Sort the results, in my opinion TreeSet is not required, as we have to deal with ArrayList hereafter
			Collections.sort(genericPeopleSearchResults, comparator);

			if (FirmDataType.TOP_15.getValue().equals(pacmb.getLimitType())) // Top selected
			{
				return WebUtil.top(genericPeopleSearchResults, 15); // send top 15
			}
			else if (FirmDataType.BOTTOM_15.getValue().equals(pacmb.getLimitType()))
			{
				return WebUtil.bottom(genericPeopleSearchResults, 15); // send bottom 15
			}
			if (FirmDataType.TOP_10.getValue().equals(pacmb.getLimitType())) // Top selected
			{
				return WebUtil.top(genericPeopleSearchResults, 10); // send top 10
			}
			else if (FirmDataType.BOTTOM_10.getValue().equals(pacmb.getLimitType()))
			{
				return WebUtil.bottom(genericPeopleSearchResults, 10); // send bottom 10
			}

		}

		return returnMe;
	}

	/**
	 * Pushes the data for comparison graphs in the respective buffers
	 * 
	 * @param results
	 * @param chartData data to stuff in
	 * @param firmCountForAvgCalc average the results by this value
	 */
	protected void prepareComparisonChartData(List<PeopleResultDTO> results, ChartData chartData, int firmCountForAvgCalc,
			boolean avgComparison)
	{

		//to escape divide by zero error
		if (firmCountForAvgCalc <= 0)
		{
			firmCountForAvgCalc = 1;
		}

		chartData.compareCount++;  // increment the comparison data count

		//if type ==3 then chart type is Number of Practices by Firm 
		//so we only need practice count
		if (chartData.type == CHART_NO_OF_PRACTICES)
		{
			int practiceCount = 0;

			Map<String, Integer> practiceMap = getCountMapFromSearchResults(results, chartData).get("PracticeCount");

			for (Integer value : practiceMap.values())
			{
				practiceCount += value;
			}
			if (avgComparison) // Comparison is on Average of search results
			{
				firmCountForAvgCalc = results.size(); // average of all search results rather than grouped data
			}

			practiceCount = (int) Math.round(((double) practiceCount) / (double) firmCountForAvgCalc);

			if (chartData.sbFirm.length() > 0)
			{
				chartData.sbFirm.append(", ");
				chartData.sbPracticeCount.append(", ");
				chartData.sbToolTipTitle.append(", ");
				chartData.sbToolTipCount.append(", ");
			}

			chartData.sbPracticeCount.append(practiceCount);

			
			/**
			 * Logic stands this way
			* Loop through all the firms then all the practices
			* Irrespective of firms count the headcount of all the practices across all the firms
			*/
			Map<String, Integer> comparisonPracticeMap = new HashMap<String, Integer>(); // hold <practiceName,headCount> of all firms

			for (PeopleResultDTO prDTO : results)
			{
				Map<String, PeopleResult> practiceMap2 = prDTO.getPracticesMap();

				for (String practiceName : practiceMap2.keySet())
				{

					int headCount = 0;

					if (comparisonPracticeMap.containsKey(practiceName))
					{
						headCount += practiceMap2.get(practiceName).getHeadCount();
					}

					comparisonPracticeMap.put(practiceName, headCount);
				}

			}
			
			//get top practices with head count
			Map<String, Integer> sortedMap = WebUtil.sortMapByIntegerValue(comparisonPracticeMap, ALMConstants.NO_OF_PRACTICES_IN_TOOLTIP, true);
			
			chartData.sbToolTipTitle.append("[");
			chartData.sbToolTipCount.append("[");
			
			for(Entry<String, Integer> practiceEntry : sortedMap.entrySet())
			{
				if (!chartData.sbToolTipTitle.toString().endsWith("["))
				{
					chartData.sbToolTipTitle.append(", ");
					chartData.sbToolTipCount.append(", ");
				}
				chartData.sbToolTipTitle.append("\"").append(WebUtil.cleanString(practiceEntry.getKey())).append("\"");
				chartData.sbToolTipCount.append("\"").append((int) Math.round(((double) practiceEntry.getValue()) / (double) firmCountForAvgCalc)).append("\"");
			}
			
			chartData.sbToolTipTitle.append("]");
			chartData.sbToolTipCount.append("]");
			
		}
		else if (chartData.type == CHART_NO_OF_ATTORNEYS || chartData.type == CHART_PERCENTAGE_ATTORNEYS)
		{

			int attorneyCount = 0;
			int partnerCount = 0;
			int associateCount = 0;
			int otherCounselCount = 0;

			Map<String, Map<String, Integer>> allCountMap = getCountMapFromSearchResults(results, chartData);
			Map<String, Integer> attorneyMap = allCountMap.get("AttorneyCount");
			Map<String, Integer> partnerMap = allCountMap.get("PartnerCount");
			Map<String, Integer> associateMap = allCountMap.get("AssociateCount");
			Map<String, Integer> otherCounselMap = allCountMap.get("OtherCounselCount");

			for (Integer value : attorneyMap.values())
			{
				attorneyCount += value;
			}
			for (Integer value : partnerMap.values())
			{
				partnerCount += value;
			}
			for (Integer value : associateMap.values())
			{
				associateCount += value;
			}
			for (Integer value : otherCounselMap.values())
			{
				otherCounselCount += value;
			}

			if (avgComparison) // Comparison is on Average of search results
			{
				firmCountForAvgCalc = results.size(); // average of all search results rather than grouped data
			}

			attorneyCount = (int) Math.round(((double) attorneyCount) / (double) firmCountForAvgCalc);
			partnerCount = (int) Math.round(((double) partnerCount) / (double) firmCountForAvgCalc);
			associateCount = (int) Math.round(((double) associateCount) / (double) firmCountForAvgCalc);
			otherCounselCount = (int) Math.round(((double) otherCounselCount) / (double) firmCountForAvgCalc);

			if (chartData.sbFirm.length() > 0)
			{
				chartData.sbFirm.append(", ");
				chartData.sbTotals.append(", ");
				chartData.sbPartners.append(", ");
				chartData.sbAssociates.append(", ");
				chartData.sbOtherCounsel.append(", ");
			}

			if (attorneyCount == 0)
			{
				chartData.sbPartners.append(0);
				chartData.sbAssociates.append(0);
				chartData.sbOtherCounsel.append(0);
				chartData.sbTotals.append(0);
				chartData.sbTotals.append(0);
			}
			else
			{
				if (chartData.type == CHART_NO_OF_ATTORNEYS)
				{
					chartData.sbPartners.append(partnerCount);
					chartData.sbAssociates.append(associateCount);
					chartData.sbOtherCounsel.append(otherCounselCount);
					chartData.sbTotals.append(attorneyCount);
				}
				else
				// this is Percentage attorneys
				{
					int[] percentages = WebUtil.getPercentageRatio(attorneyCount, false, partnerCount, associateCount,
							otherCounselCount);
					chartData.sbPartners.append(percentages[0]);
					chartData.sbAssociates.append(percentages[1]);
					chartData.sbOtherCounsel.append(percentages[2]);
					chartData.sbTotals.append(attorneyCount);
				}
			}
		}

	}

	/**
	 * This method create separate map for Attorneycount, PartnerCount,
	 * associateCount, otherCounselCount, PracticeMap
	 * 
	 * @param peopleSearchResults
	 * @return
	 */
	private Map<String, Map<String, Integer>> getCountMapFromSearchResults(List<PeopleResultDTO> peopleSearchResults,
			ChartData chartData)
	{
		Map<String, Integer> attorneyCount = new HashMap<String, Integer>();
		Map<String, Integer> partnerCount = new HashMap<String, Integer>();
		Map<String, Integer> associateCount = new HashMap<String, Integer>();
		Map<String, Integer> otherCounselCount = new HashMap<String, Integer>();
		Map<String, Map<String, Integer>> allCountMap = new HashMap<String, Map<String, Integer>>();

		if (chartData.type == CHART_NO_OF_ATTORNEYS || chartData.type == CHART_PERCENTAGE_ATTORNEYS)

		{
			Integer attorneyCurrentCount = 0;
			Integer partnerCurrentCount = 0;
			Integer associateCurrentCount = 0;
			Integer otherCounselassociateCurrentCount = 0;

			// Calculate the data series for  All Count
			for (PeopleResultDTO frDTO : peopleSearchResults)
			{
				PeopleResult pr = frDTO.getCompanyPeople();
				if (StringUtils.isNotBlank(frDTO.getCompanyName()))
				{
					String key = frDTO.getCompanyName();
					
					/*if (attorneyCount.containsKey(key))
					{
						attorneyCurrentCount = attorneyCount.get(key) + frDTO.getTotalAttorneyCount();
						partnerCurrentCount = partnerCount.get(key) + frDTO.getPartnerCount();
						associateCurrentCount = associateCount.get(key) + frDTO.getAssociateCount();
						otherCounselassociateCurrentCount = otherCounselCount.get(key) + frDTO.getOtherCounselCount();

					}
					else
					{
						attorneyCurrentCount = frDTO.getTotalAttorneyCount();
						partnerCurrentCount = frDTO.getPartnerCount();
						associateCurrentCount = frDTO.getAssociateCount();
						otherCounselassociateCurrentCount = frDTO.getOtherCounselCount();

					}*/
					// fix 
					if (attorneyCount.containsKey(key))
					{
						attorneyCurrentCount = attorneyCount.get(key) + pr.getHeadCount();
						partnerCurrentCount = partnerCount.get(key) + pr.getPartnerCount();
						associateCurrentCount = associateCount.get(key) + pr.getAssociateCount();
						otherCounselassociateCurrentCount = otherCounselCount.get(key) + pr.getOtherCounselCount();

					}
					else
					{
						attorneyCurrentCount = pr.getHeadCount();
						partnerCurrentCount = pr.getPartnerCount();
						associateCurrentCount = pr.getAssociateCount();
						otherCounselassociateCurrentCount = pr.getOtherCounselCount();

					}

					attorneyCount.put(key, attorneyCurrentCount);
					partnerCount.put(key, partnerCurrentCount);
					associateCount.put(key, associateCurrentCount);
					otherCounselCount.put(key, otherCounselassociateCurrentCount);
				}
			}

			allCountMap.put("AttorneyCount", attorneyCount);
			allCountMap.put("PartnerCount", partnerCount);
			allCountMap.put("AssociateCount", associateCount);
			allCountMap.put("OtherCounselCount", otherCounselCount);
		}
		else if (chartData.type == CHART_NO_OF_PRACTICES)
		{
			Map<String, Integer> practiceMap = new HashMap<String, Integer>();
			Integer practiceCount = 0;
			// Calculate the data series for  Practice Count only as chart type is Number of Practices by Firm 
			for (PeopleResultDTO frDTO : peopleSearchResults)
			{
				if (StringUtils.isNotBlank(frDTO.getCompanyName()))
				{
					String key = frDTO.getCompanyName();
					if (practiceMap.containsKey(key))
					{
						practiceCount += frDTO.getPracticesMap().size();
					}
					else
					{
						practiceCount = frDTO.getPracticesMap().size();
					}

					practiceMap.put(key, practiceCount);
				}
			}

			allCountMap.put("PracticeCount", practiceMap);

		}

		return allCountMap;

	}

	/**
	 * Returns the limited search results based on user selection
	 * 
	 * @param pacmb
	 * @param genericPeopleSearchResults
	 * @return
	 */
	protected List<PeopleResultDTO> refineSearchResultsByForPracticeArea(StandardChartModelBean pacmb,
			List<PeopleResultDTO> genericPeopleSearchResults)
	{

		List<PeopleResultDTO> returnMe = new ArrayList<PeopleResultDTO>();

		// User selected individual firms
		if ((FirmDataType.FIRM.getValue().equals(pacmb.getLimitType())) && (pacmb.getSearchResultsFirmList() != null)
				&& (!pacmb.getSearchResultsFirmList().isEmpty()))
		{
			for (String firmId : pacmb.getSearchResultsFirmList()) // run through the selected firmIds
			{
				//	Firm firm = firmService.getFirmById(Integer.parseInt(firmId)); // fetch the firm

				for (PeopleResultDTO resultDTO : genericPeopleSearchResults) //search the firm in searchResults
				{
					if (resultDTO.getCompanyId().equals(Integer.parseInt(firmId))) //got a match
					{
						returnMe.add(resultDTO); //add it to return results
						break; // go to next selected firm
					}
				}
			}

		}
		else
		{ // user select either top or bottom results
			// Need a Comparator for descending order 
			Comparator<PeopleResultDTO> comparator = new Comparator<PeopleResultDTO>()
			{
				@Override
				public int compare(PeopleResultDTO o1, PeopleResultDTO o2)
				{
					return WebUtil.compare(o1.getPracticesMap().size(), o2.getPracticesMap().size(), true); // sort in desc order
				}
			};

			//Sort the results, in my opinion TreeSet is not required, as we have to deal with ArrayList hereafter
			Collections.sort(genericPeopleSearchResults, comparator);

			if (FirmDataType.TOP_15.getValue().equals(pacmb.getLimitType())) // Top selected
			{
				return WebUtil.top(genericPeopleSearchResults, 15); // send top 15
			}
			else if (FirmDataType.BOTTOM_15.getValue().equals(pacmb.getLimitType()))
			{
				return WebUtil.bottom(genericPeopleSearchResults, 15); // send bottom 15
			}
			if (FirmDataType.TOP_10.getValue().equals(pacmb.getLimitType())) // Top selected
			{
				return WebUtil.top(genericPeopleSearchResults, 10); // send top 10
			}
			else if (FirmDataType.BOTTOM_10.getValue().equals(pacmb.getLimitType()))
			{
				return WebUtil.bottom(genericPeopleSearchResults, 10); // send bottom 10
			}

		}

		return returnMe;
	}

}
