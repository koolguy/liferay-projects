package com.alm.rivaledge.controller.firmstatistics.chart.percentattorneys;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.alm.rivaledge.controller.AbstractFirmStatisticsController;
import com.alm.rivaledge.model.PeopleSearchModelBean;
import com.alm.rivaledge.model.chart.StandardChartModelBean;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.transferobject.PeopleResult;
import com.alm.rivaledge.transferobject.PeopleResultDTO;
import com.alm.rivaledge.transferobject.PeopleSearchDTO;
import com.alm.rivaledge.transferobject.UserGroupDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.util.StringPool;

@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "peopleSearchModelBean", "percentageAttorneysChartModelBean" })
public class PercentageAttorneysController extends AbstractFirmStatisticsController
{
	@ModelAttribute
	public void populateChartModelBean(ModelMap modelMap, PortletRequest request)
	{
		StandardChartModelBean 	pasmb 				= null;
		
		String					currentPortletId	= WebUtil.getCurrentPortletId(request);
		String					currentPage	 	 	= WebUtil.getCurrentPage(request);
		boolean					isHomePage		 	= ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		User 					currentUser 		= (modelMap.get("currentUser") != null) ? ((User) modelMap.get("currentUser")) : WebUtil.getCurrentUser(request, userService); 
		
		if (!isHomePage)
		{
			currentPage	= "FIRM_STATISTICS";
		}
		pasmb = getUserChartPreference(currentUser.getId(), currentPortletId, currentPage);
		
		if (pasmb == null)
		{
			// Create and initialize the chartModelBean
			pasmb = new StandardChartModelBean();
			pasmb.init(); // do an explicit init for default values
		}
		modelMap.put("percentageAttorneysChartModelBean", pasmb);
	}

	@RenderMapping
	public ModelAndView chartRenderView(@ModelAttribute("peopleSearchModelBean") PeopleSearchModelBean peopleSearchModelBean,
										@ModelAttribute("percentageAttorneysChartModelBean") StandardChartModelBean pacmb, 
										ModelMap model, 
										RenderRequest request, 
										RenderResponse response) throws Exception
	{
		
		PeopleSearchDTO peopleSearchDTO = getDTOFromModelBean(request, model, peopleSearchModelBean);
		// clone the DTO as the changes to the DTO should not reflect elsewhere
		PeopleSearchDTO chartPeopleSearchDTO = (PeopleSearchDTO) peopleSearchDTO.clone();

		// Get the current user from the render request
		User currentUser = WebUtil.getCurrentUser(request, userService);

		List<PeopleResultDTO> peopleSearchResults = new ArrayList<PeopleResultDTO>();

		// Call the service implementation to get the search results count
		int resultSize = peopleService.getPeopleSearchCount(chartPeopleSearchDTO, currentUser);

		if (resultSize > 0)
		{
			// We have results that match this search criteria
			// Return the first page of results
			peopleSearchResults = peopleService.peopleSearchWithoutDrilldown(chartPeopleSearchDTO, currentUser);

			List<PeopleResultDTO> refinedResults = refineSearchResultsByLimit(pacmb, new ArrayList<PeopleResultDTO>(peopleSearchResults));

			ChartData chartData = new ChartData();
			chartData.type = CHART_PERCENTAGE_ATTORNEYS;

			prepareComparisonData(currentUser, pacmb, chartPeopleSearchDTO, refinedResults, chartData);
			// Set the chart type
			model.put("percentAttorneysChartType", pacmb.getChartType());

			// Set the chart title
			model.put("percentAttorneysChartTitle", "PERCENTAGE OF ATTORNEYS BY TYPE");
			// Generate the data
			// Generate the series data
			for (PeopleResultDTO prDTO : refinedResults)
			{
				PeopleResult pr = prDTO.getCompanyPeople();
				
				if (chartData.sbFirm.length() > 0)
				{
					chartData.sbFirm.append(", ");
					chartData.sbPartners.append(", ");
					chartData.sbAssociates.append(", ");
					chartData.sbOtherCounsel.append(", ");
					chartData.sbTotals.append(", ");
				}
				chartData.sbFirm.append("\"" + prDTO.getCompanyName() + "\"");
				if (pr.getHeadCount() == 0)
				{
					chartData.sbPartners.append(0);
					chartData.sbAssociates.append(0);
					chartData.sbOtherCounsel.append(0);
					chartData.sbTotals.append(0);
				}
				else
				{
					int[] percentages = WebUtil.getPercentageRatio(pr.getHeadCount(), 
																   false, 
																   pr.getPartnerCount(), 
																   pr.getAssociateCount(),
																   pr.getOtherCounselCount());
					chartData.sbPartners.append(percentages[0]);
					chartData.sbAssociates.append(percentages[1]);
					chartData.sbOtherCounsel.append(percentages[2]);
					chartData.sbTotals.append(pr.getHeadCount());
				}
			}
			

			List<Firm> allSearchResultsFirmList = new ArrayList<Firm>();

			for (PeopleResultDTO prDTO : peopleSearchResults)
			{
				/*
				 * why do we need to fetch from Service the whole object when we
				 * are interested only in id and name which we already have
				 */
				// Firm firm = firmService.getFirmById(prDTO.getCompanyId());

				Firm firm = new Firm();
				firm.setCompanyId(prDTO.getCompanyId());
				firm.setCompany(prDTO.getCompanyName());

				allSearchResultsFirmList.add(firm);
			}

			List<UserGroupDTO> allWatchLists = new ArrayList<UserGroupDTO>();

			if (watchlistService != null && currentUser != null)
			{
				allWatchLists = (List<UserGroupDTO>) watchlistService.getAllFirmWatchList(currentUser.getId());
			}
			model.put("allRankingList", ALMConstants.RIVALEDGE_RANKING_LIST);
			model.put("allWatchLists", allWatchLists);
			model.put("allSearchResultsFirmList", allSearchResultsFirmList);
			model.put("percentAttorneysPartnerCount", chartData.sbPartners.toString());
			model.put("percentAttorneysAssociateCount", chartData.sbAssociates.toString());
			model.put("percentAttorneysOtherCounselCount", chartData.sbOtherCounsel.toString());
			model.put("percentAttorneysTotalAssociateCount", chartData.sbTotals.toString());
			model.put("percentAttorneysFirmNameList", chartData.sbFirm.toString());
			model.put("percentAttorneysCompareCount", chartData.compareCount);
			
			chartData = new ChartData();
			chartData.type = CHART_NO_OF_ATTORNEYS;
			prepareComparisonData(currentUser, pacmb, chartPeopleSearchDTO, refinedResults, chartData);
			for (PeopleResultDTO prDTO : refinedResults)
			{
				PeopleResult pr = prDTO.getCompanyPeople();
				
				if (chartData.sbFirm.length() > 0)
				{
					chartData.sbFirm.append(", ");
					chartData.sbPartners.append(", ");
					chartData.sbAssociates.append(", ");
					chartData.sbOtherCounsel.append(", ");
					chartData.sbTotals.append(", ");
				}
				/*chartData.sbFirm.append("\"" + prDTO.getCompanyName() + "\"");
				chartData.sbPartners.append(prDTO.getPartnerCount());
				chartData.sbAssociates.append(prDTO.getAssociateCount());
				chartData.sbOtherCounsel.append(prDTO.getOtherCounselCount());
				chartData.sbTotals.append(prDTO.getTotalAttorneyCount());*/
				
				//fix
				chartData.sbFirm.append("\"" + prDTO.getCompanyName() + "\"");
				chartData.sbPartners.append(pr.getPartnerCount());
				chartData.sbAssociates.append(pr.getAssociateCount());
				chartData.sbOtherCounsel.append(pr.getOtherCounselCount());
				chartData.sbTotals.append(pr.getHeadCount());
				
			}
			
			model.put("numAttorneysPartnerCountForPercent", chartData.sbPartners.toString());
			model.put("numAttorneysAssociateCountForPercent", chartData.sbAssociates.toString());
			model.put("numAttorneysOtherCounselCountForPercent", chartData.sbOtherCounsel.toString());
			model.put("numAttorneysTotalAssociateCountForPercent", chartData.sbTotals.toString());
		}
		return new ModelAndView("percentageofattorneys");
	}

	@ActionMapping(params = "action=updateChart")
	public void updateChart(@ModelAttribute("percentageAttorneysChartModelBean") StandardChartModelBean pacmb, 
							ActionRequest request, 
							ActionResponse response)
	{
		// Persist the changes to the chart settings
		// If this is the home page, save them there, else
		// save with the generic NEWS_PUBS tag.
		String 	currentPortletId 	= WebUtil.getCurrentPortletId(request);
		String 	currentPage			= WebUtil.getCurrentPage(request);
		boolean	isHomePage			= ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		User	currentUser 		= WebUtil.getCurrentUser(request, userService);
		if (!isHomePage)
		{
			currentPage = "FIRM_STATISTICS";
		}
		userService.saveUserPreferences(currentUser.getId(), 
										currentPortletId, 
										null, 
										WebUtil.getJson(pacmb), 
										currentPage);
	}
	
	/*
	* Following method remove rendering portlet from the page. And redirects to same page.
	*
	*/

	@ActionMapping(params = "action=removePortleFromThePage")
	public void removePortletFromPage(ActionRequest request, ActionResponse response) throws Exception
	{
		WebUtil.removePortlet(request);
	}
}
