package org.springframework.web.portlet.custom;

import javax.portlet.MimeResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletRequestDispatcher;

import org.springframework.web.portlet.DispatcherPortlet;

/**
 * Custom DispatcherPortlet implementation for resolving a bug in
 * {@link DispatcherPortlet}<br>
 * Bug desc : dispatcher.forward(request, response) wont work in RESOURCE_PHASE<br>
 * <a href=
 * "https://jira.springsource.org/browse/SPR-10406?page=com.atlassian.jira.plugin.system.issuetabpanels:all-tabpanel"
 * >JIRA Reference </a><br>
 * 
 * @author Niranjan
 * @since 02-Sep-2013
 * @version 1.0
 */
public class ALMDispatcherPortlet extends DispatcherPortlet
{

	protected void doDispatch(PortletRequestDispatcher dispatcher, PortletRequest request, MimeResponse response)
			throws Exception
	{

		//Original code 
		/*
		 if (PortletRequest.RESOURCE_PHASE.equals(request.getAttribute(PortletRequest.LIFECYCLE_PHASE))) {
			dispatcher.forward(request, response);
		}
		else {
			dispatcher.include(request, response);
		}
		*/

		dispatcher.include(request, response);
	}
}
