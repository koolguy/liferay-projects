package com.alm.rivaledge.controller.placeholder;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

/**
 * Placeholder controller. Shows the drill down data upon a "Click to View Details" call.
 * 
 * @author FL867
 * @version 1.0
 * @since 
 */
@Controller
@RequestMapping(value = "VIEW")
public class PlaceholderController
{
	@RenderMapping
	public ModelAndView renderClickToViewDetails(@RequestParam String chartType, 
												 @RequestParam Integer firmId, 
												 ModelMap model, 
												 RenderRequest request, 
												 RenderResponse response) throws Exception
	{
		return (new ModelAndView("placeholder"));
	}
}
