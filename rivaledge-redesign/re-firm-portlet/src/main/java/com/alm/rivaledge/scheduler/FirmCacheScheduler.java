package com.alm.rivaledge.scheduler;

import java.io.IOException;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import com.alm.rivaledge.service.CacheService;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;

@Component
public class FirmCacheScheduler implements MessageListener, ApplicationContextAware
{
	@Autowired
	private CacheService cacheService; 
	
	@Override
	public void receive(Message arg0)
	{
		System.out.println("\n\n *** Cron Job fired for Firm News and Pubs Cache...");
//		try
//		{
//			cacheService.firmNewsAndPublicationsJob();
//		}
//		catch (IOException ioEx)
//		{
//			ioEx.printStackTrace();
//		}
	}

	@Override
	public void setApplicationContext(ApplicationContext arg0) throws BeansException
	{
		// TODO Auto-generated method stub
		
	}
}
