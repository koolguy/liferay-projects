package com.alm.rivaledge.controller.search;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.alm.rivaledge.controller.AbstractFirmController;
import com.alm.rivaledge.model.ClickToView;
import com.alm.rivaledge.model.FirmSearchModelBean;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.Keyword;
import com.alm.rivaledge.persistence.domain.lawma0_data.Practice;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.transferobject.FirmSearchDTO;
import com.alm.rivaledge.transferobject.UserGroupDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.ContentTypeJsonUtility;
import com.alm.rivaledge.util.FirmJsonUtility;
import com.alm.rivaledge.util.PracticeJsonUtility;
import com.alm.rivaledge.util.WebUtil;
import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;

/**
 * Firm controller responsible for all operation on Firm search form and results
 * screen
 * 
 * @author FL605
 * @since Sprint 1
 */
@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "firmSearchModelBean", "firmSearchDTO", "searchResults" })
public class NewsPubsSearchController extends AbstractFirmController
{
	protected Log		_log	= LogFactoryUtil.getLog(NewsPubsSearchController.class.getName());

	private static String[] twitterPostTypeData = { ALMConstants.ALL, 
													ALMConstants.TWITTER_POST_TYPE_BY_LAW_FIRMS,
													ALMConstants.TWITTER_POST_TYPE_ABOUT_LAW_FIRMS,
													ALMConstants.TWITTER_POST_TYPE_BY_OTHERS
													}; 
	
	@Autowired
	private FirmService	firmService;

	@RenderMapping
	protected ModelAndView newsAndPublicationSearchForm(ModelMap model,
			@ModelAttribute("firmSearchModelBean") FirmSearchModelBean firmSearchModelBean,
			RenderRequest request, RenderResponse response) throws Exception
	{
		ClickToView<FirmSearchModelBean, FirmSearchDTO> c2v = new ClickToView<FirmSearchModelBean, FirmSearchDTO>();
		c2v.setSearchModelBean(firmSearchModelBean);

		honorClick2View(request, model, c2v); // firmSearchModelBean is affected only when you are directed here from C2V
		
		return renderSearchPage(firmSearchModelBean, model, request, response);
		
	}
	
	
	private ModelAndView renderSearchPage(FirmSearchModelBean firmSearchModelBean, ModelMap model, RenderRequest request,
			RenderResponse response)
	{
		Gson 						gson 								= new Gson();
		FirmJsonUtility 			firmJsonUtility 					= null;
		PracticeJsonUtility 		practiceJsonUtility 				= null;
		List<UserGroup> 			allWatchListsAutoComplete 			= null;
		List<FirmJsonUtility> 		firmJsonUtilityList 				= new ArrayList<FirmJsonUtility>();
		List<PracticeJsonUtility> 	practiceJsonUtilityList 			= new ArrayList<PracticeJsonUtility>();
		List<UserGroupDTO> 			allWatchLists 						= null;
		List<Firm>	 				firmsSet 							= firmService.getAllFirms();
		List<Practice> 				practiceList 						= firmService.getAllPractices();
		User 						currentUser							= WebUtil.getCurrentUser(request, userService);
		
		// Find the last search criteria run by this particular user
		
		if (currentUser != null)
		{
			allWatchLists = (List<UserGroupDTO>) watchlistService.getAllFirmWatchList(currentUser.getId());
			allWatchListsAutoComplete = (List<UserGroup>) watchlistService.getAllWatchListForAutoComplete(currentUser.getId());
			
			for (UserGroup userGroup : allWatchListsAutoComplete)
			{
				firmJsonUtility = new FirmJsonUtility();
				firmJsonUtility.setId(String.valueOf(userGroup.getGroupId()));
				firmJsonUtility.setLabel(userGroup.getGroupName());
				firmJsonUtility.setCategory("WatchLists");
				firmJsonUtilityList.add(firmJsonUtility);
			}
		}

		for (Firm firm : firmsSet)
		{
			firmJsonUtility = new FirmJsonUtility();
			firmJsonUtility.setId(firm.getCompanyId().toString());
			firmJsonUtility.setLabel(firm.getCompany());
			firmJsonUtility.setCategory("Single Firms");
			firmJsonUtilityList.add(firmJsonUtility);
		}

		for (Practice practice : practiceList)
		{
			practiceJsonUtility = new PracticeJsonUtility();
			practiceJsonUtility.setLabel(practice.getPracticeArea());
			practiceJsonUtilityList.add(practiceJsonUtility);
		}

		// converting all firms to JSON string
		//String jsonAllFirms = gson.toJson(firmService.getRivalEdgeList(firmJsonUtilityList));
		String jsonAllFirms = gson.toJson(firmJsonUtilityList);
		// converting all Practices to JSON string
		String jsonAllPractices = gson.toJson(practiceJsonUtilityList);

		// converting all Keyword to JSON string
		String jsonAllKeyWords = (currentUser != null) ? gson.toJson(firmService.findKeywords(currentUser.getId())) : "[]"; // send an empty array to avoid javascript error
																														//var keywords = ;	

		//for Auto complete
		List<ContentTypeJsonUtility> contentTypeJsonUtilityList = new ArrayList<ContentTypeJsonUtility>();
		contentTypeJsonUtilityList.add(new ContentTypeJsonUtility(ALMConstants.ALL_Types, ALMConstants.ALL_Types));
		contentTypeJsonUtilityList.add(new ContentTypeJsonUtility("News","News"));
		contentTypeJsonUtilityList.add(new ContentTypeJsonUtility("Pubs", "Publications"));
		contentTypeJsonUtilityList.add(new ContentTypeJsonUtility("Twitter", "Twitter"));
		
		// populate Models
		model.addAttribute("twitterPostTypeData",gson.toJson(twitterPostTypeData) );
		model.addAttribute("contentTypeData",  gson.toJson(contentTypeJsonUtilityList));
		model.addAttribute("firmJson", jsonAllFirms);
		model.addAttribute("practiceJson", jsonAllPractices);
		model.addAttribute("keywordJson", jsonAllKeyWords);
		model.addAttribute("allFirms", firmsSet);
		model.addAttribute("allPracticeArea", practiceList);
		model.addAttribute("allWatchLists", allWatchLists);
		model.addAttribute("allRankingList", ALMConstants.RIVALEDGE_RANKING_LIST);

		List<UserGroup> allWatchListsDefaultList = null;
		if (currentUser != null)
		{
			allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
			if (allWatchListsDefaultList.size() == 0)
			{
				model.addAttribute("allWatchListsDefaultList", "");
			}
			else
			{
				model.addAttribute("allWatchListsDefaultList", allWatchListsDefaultList.get(0));
			}
		}
		
		return new ModelAndView("firmsearch", model);
	}

	@ActionMapping(params = "action=changeSearchCriteria")
	protected void newsAndPublicationsSubmitForm(@ModelAttribute("firmSearchModelBean") FirmSearchModelBean firmSearchModelBean, 
												 ModelMap model, 
												 ActionRequest request,
												 ActionResponse response) throws Exception
	{
		FirmSearchDTO 	firmSearchDTO 	= getDTOFromModelBean(model, firmSearchModelBean, request);
		User			currentUser		= WebUtil.getCurrentUser(request, userService); 
		
		// Checking whether keyword already exist in database or not
		// if it doesn't exist in database then inserting new record in
		// tbl_rer_kwsearch_log table
		// for input keyword string
		if (firmSearchDTO.getKeyword() != null && firmService.countByKeyword(firmSearchDTO.getKeyword()).intValue() == 0)
		{
			Keyword keyword = new Keyword();
			keyword.setUserId(currentUser.getId());
			keyword.setKeyword(firmSearchDTO.getKeyword());
			firmService.addkeyword(keyword);
		}

		// The user changed the search criteria, so we will have to persist
		// the changed criteria into the database.
		// However, if this is the Home page, then we will persist these as
		// the search criteria for the specific chart instead, and not
		// generically as the module search criteria.
		String currentPortletId 	= firmSearchModelBean.getPortletId();
		String 	currentPage			= WebUtil.getCurrentPage(request);
		boolean	isHomePage			= ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		boolean clickToViewPage = WebUtil.isClickToViewPage(request);
		if ((currentUser != null) && (currentPortletId != null) && (currentPortletId.length() > 0) && !clickToViewPage)
		{
			if (!isHomePage)
			{
				currentPage = "NEWS_PUBS";
				currentPortletId = "NEWS_PUBS";
			}
			
			userService.saveUserPreferences(currentUser.getId(), 
											currentPortletId, 
											WebUtil.getJson(firmSearchModelBean), 
											null, 
											currentPage);
		}
		
	
		// set the new SearchDTO back into the model, as user changed the search
		// criteria
		model.addAttribute("firmSearchDTO", firmSearchDTO);
	}
	
	
	@ActionMapping(params = "action=clickToView")
	protected void clickToView(@ModelAttribute("firmSearchModelBean_c2v") FirmSearchModelBean firmSearchModelBean_c2v, 
												 ModelMap model, 
												 ActionRequest request,
												 ActionResponse response) throws Exception
	{
		String firmSearchModelBeanJson = WebUtil.getJson(firmSearchModelBean_c2v);
		response.setRenderParameter("firmSearchModelBeanJson", firmSearchModelBeanJson);
		response.setRenderParameter("renderPage", "clickToView");
	}
	
	//@RenderMapping(params = "renderPage=clickToView")
	@RequestMapping(params = "renderPage=clickToView")
	protected ModelAndView clickToView(@RequestParam("firmSearchModelBeanJson") String firmSearchModelBeanJson, 
												 ModelMap model, 
												 RenderRequest request,
												 RenderResponse response) throws Exception
	{
		FirmSearchModelBean fsmb = WebUtil.getObject(firmSearchModelBeanJson, FirmSearchModelBean.class);
		fsmb.setAddToSession(false);
		model.addAttribute("firmSearchModelBean", fsmb);
		return renderSearchPage(fsmb, model, request, response);
	}
	
	@RenderMapping(params = "renderPage=searchCriteriaForChart")
	protected ModelAndView renderSearchCriteriaForChart(@RequestParam("chartPortletId") String chartPortletId, 
												 ModelMap model, 
												 RenderRequest request,
												 RenderResponse response) throws Exception
	{
		User currentUser = (model.get("currentUser") != null) ? ((User) model.get("currentUser")) :	WebUtil.getCurrentUser(request, userService);

		FirmSearchModelBean fsmb = getUserPreference(currentUser.getId(), chartPortletId, ALMConstants.HOME_PAGE);
		if(fsmb == null)
		{
			fsmb = getDefaultFirmSearchModelBean(currentUser);
		}
		fsmb.setAddToSession(false);
		model.addAttribute("firmSearchModelBean", fsmb);
		model.addAttribute("chartPortletId", chartPortletId);
		return renderSearchPage(fsmb, model, request, response);
	}
	
	
	/**
	 *  Persists the Search Criteria for a given ChartId
	 * @param firmSearchModelBean use a different model Name ("firmSearchModelBean_chart") rather than the one which goes into session
	 * Because Spring retrieves the model from Session first then updates them with form values submitted by client, we need a new Object rather one from session
	 * @param chartPortletId
	 * @param model
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@ResourceMapping("persistSearchCriteriaForChart")
	protected void persistSearchCriteriaForChartOnHomePage(@ModelAttribute("firmSearchModelBean_chart") FirmSearchModelBean firmSearchModelBean, 
												@RequestParam("chartPortletId") String chartPortletId,
												ModelMap model, 
												ResourceRequest request,
												ResourceResponse response) throws Exception
	{
		User currentUser = (model.get("currentUser") != null) ? ((User) model.get("currentUser")) :	WebUtil.getCurrentUser(request, userService);

		userService.saveUserPreferences(currentUser.getId(), chartPortletId, WebUtil.getJson(firmSearchModelBean), null, ALMConstants.HOME_PAGE);

		
	}
	
}
