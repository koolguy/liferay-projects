package com.alm.rivaledge.controller.charts.percentnewspubs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.alm.rivaledge.controller.AbstractFirmController;
import com.alm.rivaledge.dto.NewsPubsDTO;
import com.alm.rivaledge.model.BaseSearchModelBean;
import com.alm.rivaledge.model.FirmSearchModelBean;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.service.UserService;
import com.alm.rivaledge.transferobject.FirmResultDTO;
import com.alm.rivaledge.transferobject.FirmSearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;

/**
 * Controller that renders a pie chart containing the percentage view of the
 * News and Pubs
 * 
 * @author FL867
 * @since Sprint 6
 * @version 1.0
 */
@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({"firmSearchModelBean"})
public class PercentageNewsPubsController extends AbstractFirmController
{
	public static boolean ASC = true;
    public static boolean DESC = false;
	
	@Autowired
	private FirmService firmService;
	
	@Autowired
	private UserService userService;
	
	@RenderMapping
	public ModelAndView defaultChartView(@ModelAttribute("firmSearchModelBean") FirmSearchModelBean firmSearchModelBean, 
										 ModelMap model,
										 RenderRequest request, 
										 RenderResponse response) throws Exception
	{
		FirmSearchModelBean firmSearchModelBeanForPiChart = (FirmSearchModelBean)firmSearchModelBean.clone();
		
		firmSearchModelBeanForPiChart.setOrderBy(-1);
		
		FirmSearchDTO firmSearchDTO = getDTOFromModelBean(model, firmSearchModelBeanForPiChart, request);
		//clone the DTO as the changes to the DTO should not reflect elsewhere
		FirmSearchDTO  		chartFirmSearchDTO 	= (FirmSearchDTO) firmSearchDTO.clone(); 
		
		// Check if there is a current search criteria in session.
		// There should always be one in session; else we should redirect
		// the user to the Details tab

		// Get the current user from the render request
		User currentUser = WebUtil.getCurrentUser(request, userService);
		
		// Call the service implementation to get the search results count
		List<FirmResultDTO> searchResults = new ArrayList<FirmResultDTO>(); //null is not allowed in session using spring
		
		int resultSize = firmService.getFirmSearchCount(chartFirmSearchDTO, currentUser);
		
		if (resultSize > 0)
		{
			// We have results that match this search criteria
			// Return the first page of results
			// TODO Add the current user as a parameter to the firmNewsPubsSearch method.
			searchResults = firmService.firmNewsPubsSearch(chartFirmSearchDTO);
			
			// Set the chart type
			model.addAttribute("percentNewsPubsChartType", "pie");
			
			// Set the chart title
			model.addAttribute("percentNewsPubsChartTitle", "PERCENT OF NEWS AND PUBLICATIONS");
			
			Map<Integer, NewsPubsDTO> chartMap = new HashMap<Integer, NewsPubsDTO>();
			
			// Calculate the data series
			for (FirmResultDTO frDTO : searchResults)
			{
				NewsPubsDTO thisDTO = null;
				if (chartMap.containsKey(frDTO.getFirmId()))
				{
					// Get the DTO
					thisDTO = chartMap.get(frDTO.getFirmId());
				}
				else
				{
					// New company
					// We will initialize it in the map
					thisDTO = new NewsPubsDTO();
					thisDTO.setFirmName("\"" + frDTO.getFirmName() + "\"");
				}

				// We will increment the totals
				if (frDTO.getDataType().equals(ALMConstants.CONTENT_NEWS))
				{
					thisDTO.setNewsCount(thisDTO.getNewsCount() + 1);
				}
				else if ("Pubs".equals(frDTO.getDataType()))
				{
					thisDTO.setPubsCount(thisDTO.getPubsCount() + 1);
				}
				else if (frDTO.getDataType().equals(ALMConstants.CONTENT_TWITTER))
				{
					thisDTO.setTweetsCount(thisDTO.getTweetsCount() + 1);
				}
				
				// Put this back in the map
				chartMap.put(frDTO.getFirmId(), thisDTO);
			}

			
		//  Creating another sorted map variable.
		Map<Integer, NewsPubsDTO> sortedMap = new HashMap<Integer, NewsPubsDTO>();
		sortedMap = sortByComparator(chartMap, DESC); // call sortByComparator method to sort data.
			
			// Generate the series data
			StringBuffer sbFirm   = new StringBuffer();
			StringBuffer sbNews   = new StringBuffer();
			StringBuffer sbPubs   = new StringBuffer();
			StringBuffer sbTweets = new StringBuffer();
			StringBuffer sbTotals = new StringBuffer();
			//int 		 count	  = 0;					// Increase count till 15.
			for (Integer firmId : sortedMap.keySet())
			{
				NewsPubsDTO thisDTO = sortedMap.get(firmId);
				if (sbFirm.length() > 0)
				{
					sbFirm.append(", ");
					sbNews.append(", ");
					sbPubs.append(", ");
					sbTweets.append(", ");
					sbTotals.append(", ");
				}
				sbFirm.append(thisDTO.getFirmName());
				sbNews.append(thisDTO.getNewsCount());
				sbPubs.append(thisDTO.getPubsCount());
				sbTweets.append(thisDTO.getTweetsCount());
				sbTotals.append(thisDTO.getNewsCount() + thisDTO.getPubsCount() + thisDTO.getTweetsCount());
				/*count++;
				if(count >=15)
				{
					break; // If count reaches to 15 break for loop and pass only 15 details.
				}*/
			}
			
			model.addAttribute("percentNewsPubsNewsCountByFirmList", sbNews.toString());
			model.addAttribute("percentNewsPubsPubsCountByFirmList", sbPubs.toString());
			model.addAttribute("percentNewsPubsTwitterCountByFirmList", sbTweets.toString());
			model.addAttribute("percentNewsPubsTotalCountByFirmList", sbTotals.toString());
			model.addAttribute("percentNewsPubsFirmsNameList", sbFirm.toString());
			
			
			// Put the x and y-Axis content - this will be dynamic
			// as Pie charts don't need this
//			chartModel.put("xAxis", "");
//			chartModel.put("yAxis", "");
		}

		return new ModelAndView("percentnewspubs");
	}
	
	// Sort method to sort map based on total count.
	private static Map<Integer, NewsPubsDTO> sortByComparator(Map<Integer, NewsPubsDTO> unsortMap, final boolean order)
	{

		List<Entry<Integer, NewsPubsDTO>> list = new LinkedList<Entry<Integer, NewsPubsDTO>>(unsortMap.entrySet());

	    // Sorting the list based on values
	    Collections.sort(list, new Comparator<Entry<Integer, NewsPubsDTO>>()
	    {
	    	public int compare(Entry<Integer, NewsPubsDTO> o1,Entry<Integer, NewsPubsDTO> o2)
	    	{
	    		int first = o1.getValue().getNewsCount() + o1.getValue().getPubsCount() + o1.getValue().getTweetsCount();
	            int second = o2.getValue().getNewsCount() + o2.getValue().getPubsCount() + o2.getValue().getTweetsCount();
	            	
	            if (order)
	            {
	            	return Integer.valueOf(first).compareTo(Integer.valueOf(second));
	            }
	            else
	            {
	               	return Integer.valueOf(second).compareTo(Integer.valueOf(first));

	            }
	    	}
	    });

	    //Maintaining insertion order with the help of LinkedList
	    Map<Integer, NewsPubsDTO> sortedMap = new LinkedHashMap<Integer, NewsPubsDTO>();
	    for (Entry<Integer, NewsPubsDTO> entry : list)
	    {
	    	sortedMap.put(entry.getKey(), entry.getValue());
	    }

	    return sortedMap;
	}

	@ActionMapping
	protected void newsAndPublicationsSubmitForm(@ModelAttribute("firmSearchModelBean") FirmSearchModelBean firmSearchModelBean,
												 ModelMap model, 
												 ActionRequest request, 
												 ActionResponse response) throws Exception
	{
		// Persist the changes to the chart settings
		// If this is the home page, save them there, else
		// save with the generic NEWS_PUBS tag.
		String 	currentPortletId 	= WebUtil.getCurrentPortletId(request);
		String 	currentPage			= WebUtil.getCurrentPage(request);
		boolean	isHomePage			= ((currentPage.equals("home")) || (currentPage.equals("")));
		User	currentUser 		= WebUtil.getCurrentUser(request, userService);
		if (!isHomePage)
		{
			currentPage = "NEWS_PUBS";
		}
		userService.saveUserPreferences(currentUser.getId(), 
										currentPortletId, 
										"", 
										"", 
										currentPage);
	}
	
	/*
	* Following method remove rendering portlet from the page. And redirects to same page.
	*
	*/

	@ActionMapping(params = "action=removePortleFromThePage")
	public void removePortletFromPage(ActionRequest request, ActionResponse response) throws Exception
	{
		WebUtil.removePortlet(request);
	}
}
