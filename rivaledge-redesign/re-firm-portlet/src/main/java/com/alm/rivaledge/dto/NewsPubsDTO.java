package com.alm.rivaledge.dto;

/**
 * POJO that represents the data for the charts in the Firm News and Pubs module
 * and spits out their series representations.
 * 
 * @author FL867
 * @version 1.0
 * @since Sprint 6
 */
public class NewsPubsDTO implements Comparable<NewsPubsDTO>
{
	private String	firmName;
	private Integer	newsCount;
	private Integer	pubsCount;
	private Integer	tweetsCount;

	public NewsPubsDTO()
	{
		super();
		this.newsCount 		= 0;
		this.pubsCount 		= 0;
		this.tweetsCount 	= 0;
	}
	
	public Integer getTotals()
	{
		return (getNewsCount() + getPubsCount() + getTweetsCount());
	}

	public String getFirmName()
	{
		return firmName;
	}

	public void setFirmName(String firmName)
	{
		this.firmName = firmName;
	}

	public Integer getNewsCount()
	{
		return newsCount;
	}

	public void setNewsCount(Integer newsCount)
	{
		this.newsCount = newsCount;
	}

	public Integer getPubsCount()
	{
		return pubsCount;
	}

	public void setPubsCount(Integer pubsCount)
	{
		this.pubsCount = pubsCount;
	}

	public Integer getTweetsCount()
	{
		return tweetsCount;
	}

	public void setTweetsCount(Integer tweetsCount)
	{
		this.tweetsCount = tweetsCount;
	}

	public void addNews()
	{
		this.newsCount += 1;
	}
	
	public void addPub()
	{
		this.pubsCount += 1;
	}
	
	public void addTweet()
	{
		this.tweetsCount += 1;
	}

	@Override
	public int compareTo(NewsPubsDTO that)
	{
		return (this.getTotals().compareTo(that.getTotals()));
	}
}
