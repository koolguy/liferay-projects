package com.alm.rivaledge.controller.charts.amountnewspubs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.alm.rivaledge.controller.AbstractFirmController;
import com.alm.rivaledge.dto.NewsPubsDTO;
import com.alm.rivaledge.model.FirmSearchModelBean;
import com.alm.rivaledge.model.chart.BaseChartModelBean.ComparisonDataType;
import com.alm.rivaledge.model.chart.BaseChartModelBean.FirmDataType;
import com.alm.rivaledge.model.chart.StandardChartModelBean;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.service.UserService;
import com.alm.rivaledge.service.WatchlistService;
import com.alm.rivaledge.transferobject.FirmResultDTO;
import com.alm.rivaledge.transferobject.FirmSearchDTO;
import com.alm.rivaledge.transferobject.UserGroupDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.util.StringPool;

@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({"firmSearchModelBean", "amountNewsPubsChartModelBean"})
public class AmountNewsPubsController extends AbstractFirmController
{
	public static boolean ASC  = true;
    public static boolean DESC = false;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private WatchlistService watchlistService;
	
	@ModelAttribute
	public void populateAmountNewsPubsChartModelBean(ModelMap modelMap, PortletRequest request)
	{
		StandardChartModelBean 	amountNewsPubsChartModelBean = null;
		String					currentPortletId			 = WebUtil.getCurrentPortletId(request);
		String					currentPage				 	 = WebUtil.getCurrentPage(request);
		boolean					isHomePage				 	 = ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		User 					currentUser 			 	 = (modelMap.get("currentUser") != null) 
																	? ((User) modelMap.get("currentUser")) 
																	: WebUtil.getCurrentUser(request, userService); 
		if (!isHomePage)
		{
			currentPage			= "NEWS_PUBS";
		}
		amountNewsPubsChartModelBean = getUserChartPreference(currentUser.getId(), currentPortletId, currentPage);
		
		if (amountNewsPubsChartModelBean != null)
		{
			modelMap.put("amountNewsPubsChartModelBean", amountNewsPubsChartModelBean);
		}
		else 
		{
			// Create and initialize the chartModelBean
			amountNewsPubsChartModelBean = new StandardChartModelBean();
			amountNewsPubsChartModelBean.init();
			
			// Bind this bean to the session
			modelMap.put("amountNewsPubsChartModelBean", amountNewsPubsChartModelBean);
		}
	}
	
	@RenderMapping
	public ModelAndView defaultChartView(@ModelAttribute("firmSearchModelBean") FirmSearchModelBean firmSearchModelBean, 
										 @ModelAttribute("amountNewsPubsChartModelBean") StandardChartModelBean amountNewsPubsChartModelBean, 
										 ModelMap model, 
										 RenderRequest request, 
										 RenderResponse response) throws Exception
	{
		FirmSearchDTO firmSearchDTO = getDTOFromModelBean(model, firmSearchModelBean, request);
		
		//clone the DTO as the changes to the DTO should not reflect elsewhere
		FirmSearchDTO  		chartFirmSearchDTO 	= (FirmSearchDTO) firmSearchDTO.clone(); 
		
		User currentUser = WebUtil.getCurrentUser(request, userService);	
		
		List<UserGroupDTO> 	allWatchLists 			 = new ArrayList<UserGroupDTO>();
		List<Firm> 			allSearchResultsFirmList = new ArrayList<Firm>();
		
		if (currentUser != null)
		{
			allWatchLists = (List<UserGroupDTO>) watchlistService.getAllFirmWatchList(currentUser.getId());
		}
		
		// Call the service implementation to get the search results count
		List<FirmResultDTO> searchResults = new ArrayList<FirmResultDTO>(); //null is not allowed in session using spring
		
		int resultSize = (chartFirmSearchDTO != null) ? firmService.getFirmSearchCount(chartFirmSearchDTO, currentUser) : 0;
		
		if (resultSize > 0)
		{
			// We have results that match this search criteria
			// Return all results
			chartFirmSearchDTO.setPageNumber(0);
			chartFirmSearchDTO.setResultPerPage(1000);
			searchResults = firmService.firmNewsPubsSearch(chartFirmSearchDTO);
			
			// Set the chart type
			model.put("chartType", amountNewsPubsChartModelBean.getChartType());
			
			// Set the chart title
			model.put("chartTitle", "AMOUNT OF NEWS AND PUBLICATIONS BY FIRM");
			
			// Now the fun begins
			// There's going to be a big song and dance about fetching the comparison data
			// and refining the search results to suit the user preferences.
			// Pay attention to the comments, because this is fairly tricky
			//
			// Comparison data is the stuff that shows to the left of the 
			// columns in the bar charts. It contains the values against which
			// the user compares the rest of the chart data.
			//
			// The following comparison data points are available for this chart:
			// 1. My Firm (the firm that the currently-logged-in user belongs to)
			// 2. My Watchlists (One or more watch lists from the current user's watch lists)
			// 3. RivalEdge lists (AmLaw 100, AmLaw200, NLJ250)
			// 4. Average (average of the results returned by the search)
			// These values are all available in the modelBean, so we can use them directly.
			
			// Set the comparisonCounter
			int comparisonCounter = 0;
			
			// Define the string buffers that will hold the series data
			StringBuffer sbFirm   = new StringBuffer();
			StringBuffer sbNews   = new StringBuffer();
			StringBuffer sbPubs   = new StringBuffer();
			StringBuffer sbTweets = new StringBuffer();
			
			// 1. My Firm
			// Does this user have a My Firm? Every user is supposed to have one,
			// but we can't take it for granted.
			if ((amountNewsPubsChartModelBean.getComparisonDataTypeList() != null)
					&& (!amountNewsPubsChartModelBean.getComparisonDataTypeList().isEmpty())
					&& (amountNewsPubsChartModelBean.getComparisonDataTypeList().contains(ComparisonDataType.MY_FIRM.getValue()))
					&& (currentUser != null)
					&& (currentUser.getCompany() != null))
			{
				// We have to include comparison data for My Firm
				List<Firm> myFirmList = new ArrayList<Firm>(1);
				myFirmList.add(firmService.getFirmById(Integer.parseInt(currentUser.getCompany())));
				
				// Clone the searchDTO so that we don't overwrite the session settings
				FirmSearchDTO myFirmSearchDTO = (FirmSearchDTO) firmSearchDTO.clone();
				
				// Set the user's firm (my firm) as the sole firm in the firm list
				myFirmSearchDTO.setSelectedFirms(myFirmList);
				
				// Run the search, keeping all other search criteria as is
				NewsPubsDTO myFirmDTO = calculateNewsPubsTweets(myFirmSearchDTO);
				
				// Stuff this DTO into the string buffers
				if (sbFirm.length() > 0)
				{
					sbFirm.append(", ");
					sbNews.append(", ");
					sbPubs.append(", ");
					sbTweets.append(", ");
				}
				sbFirm.append("\"My Firm\"");
				sbNews.append(myFirmDTO.getNewsCount());
				sbPubs.append(myFirmDTO.getPubsCount());
				sbTweets.append(myFirmDTO.getTweetsCount());

				// Increment the comparison counter
				comparisonCounter++;
			}
			
			// 2. Does the user have any watch lists set up?
			if ((amountNewsPubsChartModelBean.getComparisonDataTypeList() != null) 
					&& (!amountNewsPubsChartModelBean.getComparisonDataTypeList().isEmpty())
					&& (amountNewsPubsChartModelBean.getComparisonDataTypeList().contains(ComparisonDataType.WATCHLIST_AVG.getValue()))
					&& (currentUser != null)
					&& (amountNewsPubsChartModelBean.getWatchList() != null)
					&& (!amountNewsPubsChartModelBean.getWatchList().isEmpty()))
			{
				// User has one or more watch lists
				// Iterate over the watch lists and run through each
				// Clone an instance of the firmSearchDTO
				FirmSearchDTO myWatchListSearchDTO = (FirmSearchDTO) firmSearchDTO.clone();				
				myWatchListSearchDTO.setPageNumber(0);
				myWatchListSearchDTO.setResultPerPage(100000);

				// Now run through the list
				for (String watchListId : amountNewsPubsChartModelBean.getWatchList())
				{
					// Get this watchlist
					UserGroup myWatchlist = watchlistService.getWatchList(Integer.parseInt(watchListId), currentUser.getId());
					
					// Get the list of firms in this watch list
					List<Firm> myWatchlistFirms = watchlistService.findAllFirmsInOneWatchlist(myWatchlist.getGroupId(),currentUser.getId());
					
					// Set the watch list firms in the search criteria
					myWatchListSearchDTO.setSelectedFirms(myWatchlistFirms);
					
					// Run the search, keeping all other search criteria as is
					NewsPubsDTO myWatchlistDTO = calculateNewsPubsTweets(myWatchListSearchDTO);
					
					// Stuff this DTO into the string buffers
					if (sbFirm.length() > 0)
					{
						sbFirm.append(", ");
						sbNews.append(", ");
						sbPubs.append(", ");
						sbTweets.append(", ");
					}
					
					// Plug this watch list data into the string buffers
					sbFirm.append("\"" + myWatchlist.getGroupName() + "\"");
					Double thisAvg = (new Double(myWatchlistDTO.getNewsCount()) / new Double(myWatchlistFirms.size()));
					sbNews.append(Math.round(thisAvg));
					thisAvg = (new Double(myWatchlistDTO.getPubsCount()) / new Double(myWatchlistFirms.size()));
					sbPubs.append(Math.round(thisAvg));
					thisAvg = (new Double(myWatchlistDTO.getTweetsCount()) / new Double(myWatchlistFirms.size()));
					sbTweets.append(Math.round(thisAvg));

					// Increment the comparison counter
					comparisonCounter++;
				}
			}
			
			// 3. Does the user have any RivalEdge lists set up?
			if ((amountNewsPubsChartModelBean.getComparisonDataTypeList() != null) 
					&& (!amountNewsPubsChartModelBean.getComparisonDataTypeList().isEmpty())
					&& (amountNewsPubsChartModelBean.getComparisonDataTypeList().contains(ComparisonDataType.RIVAL_EDGE.getValue()))
					&& (amountNewsPubsChartModelBean.getFirmList() != null)
					&& (!amountNewsPubsChartModelBean.getFirmList().isEmpty()))
			{
				// User has selected a Rival Edge list for which data is needed
				for (String thisRE : amountNewsPubsChartModelBean.getFirmList())
				{
					// Get the list of firms in this list entry - could be AmLaw100, NLJ250, etc.
					List<Firm> reListFirms = firmService.getRanking(thisRE);
					
					// Clone an instance of the firmSearchDTO
					FirmSearchDTO rankingSearchDTO = (FirmSearchDTO) firmSearchDTO.clone();
					rankingSearchDTO.setPageNumber(0);
					rankingSearchDTO.setResultPerPage(100000);
					
					// Set the firms in this ranking list as the search firms
					rankingSearchDTO.setSelectedFirms(reListFirms);
					
					// Run the search, keeping all other criteria as is
					NewsPubsDTO rankingDTO = calculateNewsPubsTweets(rankingSearchDTO);
					
					// Stuff this DTO into the string buffers
					if (sbFirm.length() > 0)
					{
						sbFirm.append(", ");
						sbNews.append(", ");
						sbPubs.append(", ");
						sbTweets.append(", ");
					}
					sbFirm.append("\"" + thisRE + "\"");
					Double thisAvg = (new Double(rankingDTO.getNewsCount()) / new Double(reListFirms.size()));
					sbNews.append(Math.round(thisAvg));
					thisAvg = (new Double(rankingDTO.getPubsCount()) / new Double(reListFirms.size()));
					sbPubs.append(Math.round(thisAvg));
					thisAvg = (new Double(rankingDTO.getTweetsCount()) / new Double(reListFirms.size()));
					sbTweets.append(Math.round(thisAvg));

					// Increment the comparison counter
					comparisonCounter++;
				}
			}
			
			// 4. Averages
			//    For this we will use the chartModelBean
			//    (a) We have to run the search on the chartModelBean
			//    (b) We sort the results, and then narrow down by user preference
			//        (Top10, Bottom10, User-Selected Firms)
			//    (c) Calculate the average of the narrowed-down results
			//    (d) Stuff the average into the string buffers
			
			// (a) Run the search on the search criteria
			Map<Integer, NewsPubsDTO> chartMap = new HashMap<Integer, NewsPubsDTO>();
			
			// Calculate the data series
			for (FirmResultDTO frDTO : searchResults)
			{
				NewsPubsDTO thisDTO = null;
				if (chartMap.containsKey(frDTO.getFirmId()))
				{
					// Get the DTO
					thisDTO = chartMap.get(frDTO.getFirmId());
				}
				else
				{
					// New company
					// We will initialize it in the map
					thisDTO = new NewsPubsDTO();
					thisDTO.setFirmName("\"" + frDTO.getFirmName() + "\"");
					
					Firm tempFirm = new Firm();
					tempFirm.setCompanyId(frDTO.getFirmId());
					tempFirm.setCompany(frDTO.getFirmName());
					allSearchResultsFirmList.add(tempFirm);
				}

				// We will increment the totals
				if (frDTO.getDataType().equals(ALMConstants.CONTENT_NEWS))
				{
					thisDTO.addNews();
				}
				else if (frDTO.getDataType().equals("Pubs"))
				{
					thisDTO.addPub();
				}
				else if (frDTO.getDataType().equals(ALMConstants.CONTENT_TWITTER))
				{
					thisDTO.addTweet();
				}
				
				// Put this back in the map
				chartMap.put(frDTO.getFirmId(), thisDTO);
			}

			
			// (c) Calculate the averages now
			if ((amountNewsPubsChartModelBean.getComparisonDataTypeList() != null)
					&& (amountNewsPubsChartModelBean.getComparisonDataTypeList().contains(ComparisonDataType.AVERAGE.getValue())))
			{
				double newsAvg 	= 0.0d;
				double pubsAvg 	= 0.0d;
				double tweetsAvg = 0.0d;
				for (Integer thisKey : chartMap.keySet())
				{
					newsAvg   += (double) (chartMap.get(thisKey)).getNewsCount();
					pubsAvg   += (double) (chartMap.get(thisKey)).getPubsCount();
					tweetsAvg += (double) (chartMap.get(thisKey)).getTweetsCount();
					
					// Plug this firm into the chart options
					allSearchResultsFirmList.add(firmService.getFirmById(thisKey));
				}
				
				// (d) Stuff the values in the string buffers
				if (sbFirm.length() > 0)
				{
					sbFirm.append(", ");
					sbNews.append(", ");
					sbPubs.append(", ");
					sbTweets.append(", ");
				}
				sbFirm.append("\"Average\"");
				sbNews.append(Math.round(newsAvg / chartMap.size()));
				sbPubs.append(Math.round(pubsAvg / chartMap.size()));
				sbTweets.append(Math.round(tweetsAvg / chartMap.size()));
	
				// Increment the comparison counter
				comparisonCounter++;
			}
			
			// Finally we will process the actual chart results
			// If the user has opted to narrow down by selected 
			// firms then we only need those, else we will get all
			// of them.
			if ((FirmDataType.FIRM.getValue().equals(amountNewsPubsChartModelBean.getLimitType())) 
					&& (amountNewsPubsChartModelBean.getSearchResultsFirmList() != null) 
					&& (!amountNewsPubsChartModelBean.getSearchResultsFirmList().isEmpty()))
			{
				for (String firmId : amountNewsPubsChartModelBean.getSearchResultsFirmList())
				{
					if (sbFirm.length() > 0)
					{
						sbFirm.append(", ");
						sbNews.append(", ");
						sbPubs.append(", ");
						sbTweets.append(", ");
					}
					sbFirm.append(chartMap.get(Integer.parseInt(firmId)).getFirmName());
					sbNews.append(chartMap.get(Integer.parseInt(firmId)).getNewsCount());
					sbPubs.append(chartMap.get(Integer.parseInt(firmId)).getPubsCount());
					sbTweets.append(chartMap.get(Integer.parseInt(firmId)).getTweetsCount());
				}
			}
			else
			{
				// (b) Sort the results and narrow down
				Map<Integer, NewsPubsDTO> sortedMap = null;
				
				if (FirmDataType.BOTTOM_15.getValue().equals(amountNewsPubsChartModelBean.getLimitType()))
				{	
					sortedMap = sortByComparator(chartMap, 15, DESC);
				}
				else 
				{
					sortedMap = sortByComparator(chartMap, 15, ASC);
				}
				
				for (Integer thisKey : sortedMap.keySet())
				{
					if (sbFirm.length() > 0)
					{
						sbFirm.append(", ");
						sbNews.append(", ");
						sbPubs.append(", ");
						sbTweets.append(", ");
					}
					sbFirm.append(sortedMap.get(thisKey).getFirmName());
					sbNews.append(sortedMap.get(thisKey).getNewsCount());
					sbPubs.append(sortedMap.get(thisKey).getPubsCount());
					sbTweets.append(sortedMap.get(thisKey).getTweetsCount());
				}
			}

			model.put("newsCountByFirmList", sbNews.toString());
			model.put("pubsCountByFirmList", sbPubs.toString());
			model.put("twitterCountByFirmList", sbTweets.toString());
			model.put("firmsNameList", sbFirm.toString());
			model.put("comparisonCounter", comparisonCounter);
		}
		else
		{
			model.put("comparisonCounter", "0");
		}
		// Data for the chart options popup
		boolean doIHaveFirm = false;
		if(currentUser != null && currentUser.getCompany() != null)
		{
			doIHaveFirm = true;
		}
		model.put("doIHaveFirm", doIHaveFirm);
		model.put("allRankingList", ALMConstants.RIVALEDGE_RANKING_LIST);
		model.put("allWatchLists", allWatchLists);
		model.put("allSearchResultsFirmList", allSearchResultsFirmList);

		return new ModelAndView("amountnewspubs");
	}
	
	@ActionMapping(params = "action=amountOfNewsPubsChartCriteriaChange")
	public void newsAndPublicationsSubmitForm(@ModelAttribute("amountNewsPubsChartModelBean") StandardChartModelBean amountNewsPubsChartModelBean, 
											  ActionRequest request, 
											  ActionResponse response) throws Exception
	{
		// Persist the changes to the chart settings
		// If this is the home page, save them there, else
		// save with the generic NEWS_PUBS tag.
		String 	currentPortletId 	= WebUtil.getCurrentPortletId(request);
		String 	currentPage			= WebUtil.getCurrentPage(request);
		boolean	isHomePage			= ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		User	currentUser 		= WebUtil.getCurrentUser(request, userService);
		if (!isHomePage)
		{
			currentPage = "NEWS_PUBS";
		}
		userService.saveUserPreferences(currentUser.getId(), 
										currentPortletId, 
										null, 
										WebUtil.getJson(amountNewsPubsChartModelBean), 
										currentPage);
	}
	
	private NewsPubsDTO calculateNewsPubsTweets(FirmSearchDTO searchDTO)
	{
		NewsPubsDTO returnDTO = new NewsPubsDTO();

		searchDTO.setPageNumber(0);
		searchDTO.setResultPerPage(1000);
		List<FirmResultDTO> searchResults = firmService.firmNewsPubsSearch(searchDTO);
		
		for (FirmResultDTO frDTO : searchResults)
		{
			if (frDTO.getDataType().equals(ALMConstants.CONTENT_NEWS))
			{
				returnDTO.addNews();
			}
			else if (frDTO.getDataType().equals("Pubs"))
			{
				returnDTO.addPub();
			}
			else if (frDTO.getDataType().equals(ALMConstants.CONTENT_TWITTER))
			{
				returnDTO.addTweet();
			}
		}
		return (returnDTO);
	}

	private Map<Integer, NewsPubsDTO> sortByComparator(Map<Integer, NewsPubsDTO> rawMap, int maxSize, final boolean isDescending)
    {
		List<Entry<Integer, NewsPubsDTO>> rawList = new LinkedList<Entry<Integer, NewsPubsDTO>>(rawMap.entrySet());
		
		Collections.sort(rawList, new Comparator<Entry<Integer, NewsPubsDTO>>()
		{				
			@Override
			public int compare(Entry<Integer, NewsPubsDTO> thisEntry, Entry<Integer, NewsPubsDTO> thatEntry)
			{
				if (isDescending)
				{
					return (thatEntry.getValue().getTotals().compareTo(thisEntry.getValue().getTotals()));
				}
				else
				{
					return (thisEntry.getValue().getTotals().compareTo(thatEntry.getValue().getTotals()));
				}
			}
		});
		
		if (rawList.size() > maxSize)
		{
			rawList = rawList.subList(0, maxSize);
		}
		
		Map<Integer, NewsPubsDTO> sortedMap = new LinkedHashMap<Integer, NewsPubsDTO>(rawList.size());
		for (Entry<Integer, NewsPubsDTO> thisEntry : rawList)
		{
			sortedMap.put(thisEntry.getKey(), thisEntry.getValue());
		}

        return sortedMap;
    }
	
	/**
	* Following method remove rendering portlet from the page. And redirects to same page.
	*/
	@ActionMapping(params = "action=removePortleFromThePage")
	public void removePortletFromPage(ActionRequest request, ActionResponse response) throws Exception
	{
		WebUtil.removePortlet(request);
	}
}
