package com.alm.rivaledge.controller.charts.pubspracticearea;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.alm.rivaledge.controller.AbstractFirmController;
import com.alm.rivaledge.model.FirmSearchModelBean;
import com.alm.rivaledge.model.chart.BaseChartModelBean;
import com.alm.rivaledge.model.chart.BaseChartModelBean.FirmDataType;
import com.alm.rivaledge.model.chart.StandardChartModelBean;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.service.UserService;
import com.alm.rivaledge.transferobject.FirmResultDTO;
import com.alm.rivaledge.transferobject.FirmSearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;

/**
 * This controller for Firms - News & Pubs - Number of Pubs by Practice Area
 * 
 * @author FL605
 * 
 */
@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "firmSearchModelBean", "firmSearchChartModelBean"})
public class NumberOfPubsPracticeAreaController extends AbstractFirmController
{

	public static boolean	ASC		= true;
	public static boolean	DESC	= false;

	@Autowired
	private FirmService		firmService;

	@Autowired
	private UserService		userService;

	@ModelAttribute
	public void populateChartModelBean(ModelMap modelMap, PortletRequest request)
	{
		StandardChartModelBean 	firmSearchChartModelBean = null;
		String					currentPortletId			 = WebUtil.getCurrentPortletId(request);
		String					currentPage				 	 = WebUtil.getCurrentPage(request);
		boolean					isHomePage				 	 = ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		User 					currentUser 			 	 = (modelMap.get("currentUser") != null) 
																	? ((User) modelMap.get("currentUser")) 
																	: WebUtil.getCurrentUser(request, userService); 
		if (!isHomePage)
		{
			currentPortletId 	= "NEWS_PUBS";
			currentPage			= "NEWS_PUBS";
		}
		
		firmSearchChartModelBean = getUserChartPreference(currentUser.getId(), currentPortletId, currentPage);
		
		if (firmSearchChartModelBean != null)
		{
			modelMap.put("firmSearchChartModelBean", firmSearchChartModelBean);
		}

		if (modelMap.get("firmSearchChartModelBean") == null)
		{
			firmSearchChartModelBean = new StandardChartModelBean();
			firmSearchChartModelBean.init(); // do an explicit init for defualt
											// values
			firmSearchChartModelBean.setChartType(BaseChartModelBean.ChartType.VERTICAL_BAR.getValue());
			firmSearchChartModelBean.setLimitType(FirmDataType.PRACTICE.getValue());
			modelMap.addAttribute("firmSearchChartModelBean", firmSearchChartModelBean);
		}
		
	}
	
	/**
	 * This method will render Firms - News & Pubs - Number of Pubs by Practice
	 * Area chart
	 * 
	 * @param firmSearchDTO
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RenderMapping
	public String defaultChartView(@ModelAttribute("firmSearchChartModelBean") StandardChartModelBean firmSearchChartModelBean, 
								   @ModelAttribute("firmSearchModelBean") FirmSearchModelBean firmSearchModelBean, 
								   ModelMap model,  
								   RenderRequest request, 
								   RenderResponse response) throws Exception
	{
		String selectedFirmName = "";
		
		FirmSearchDTO firmSearchDTO = getDTOFromModelBean(model, firmSearchModelBean, request);
		// clone the DTO as the changes to the DTO should not reflect elsewhere
		FirmSearchDTO chartFirmSearchDTO = (FirmSearchDTO) firmSearchDTO.clone();

		// Get the current user from the render request
		User currentUser = WebUtil.getCurrentUser(request, userService);

		int resultSize = (chartFirmSearchDTO != null) ? firmService.getFirmSearchCount(chartFirmSearchDTO, currentUser) : 0;
		
		List<String> practiceAreaListAll = new ArrayList<String>();
		
		List<String> practiceAreaListSelected = new ArrayList<String>();
		
		List<Firm> firmListAll = new ArrayList<Firm>();
		
		List<FirmResultDTO> searchResultsOriginal = new ArrayList<FirmResultDTO>();
		
		if (resultSize > 0)
		{
			chartFirmSearchDTO.setMaxResultSizeCap(5000);
			chartFirmSearchDTO.setResultPerPage(5000);
			
			searchResultsOriginal = firmService.firmNewsPubsSearch(chartFirmSearchDTO);
			
			if (searchResultsOriginal != null && searchResultsOriginal.size() > 0)
			{
				for (FirmResultDTO firmResultDTO : searchResultsOriginal)
				{
					
					List<String> practiceList = new ArrayList<String>();
					if (!firmResultDTO.getPracticeArea().contains(";"))
					{
						practiceList.add(firmResultDTO.getPracticeArea());
					}
					else
					{
						practiceList.addAll(ListUtil.fromArray(StringUtil.split(firmResultDTO.getPracticeArea(), ";")));
					}
					
					for (String practiceArea : practiceList)
					{
						practiceArea = practiceArea.trim();
						if ("Practice Area Not Available".equals(practiceArea) || "".equals(practiceArea) || practiceArea.length()==0)
						{
							practiceArea = "Not Available";
						}
						if (!practiceAreaListAll.contains(practiceArea)){
							practiceAreaListAll.add(practiceArea);
						}
					}
					
					Integer firmID = firmResultDTO.getFirmId();
					if (firmID != 0)
					{
						Firm firm = new Firm();
						firm.setCompany(firmResultDTO.getFirmName());
						firm.setCompanyId(firmID);
						if (!firmListAll.contains(firm))
						{
							firmListAll.add(firm);
						}
					}
				}
			}
		}
		
		List<FirmResultDTO> searchResults = new ArrayList<FirmResultDTO>();
		
		boolean isCriterionChanged = false;
		
		for (String practiceArea : practiceAreaListAll)
		{
			if ("Practice Area Not Available".equals(practiceArea) || "".equals(practiceArea) || practiceArea.length() == 0)
			{
				practiceArea = "Not Available";
				practiceAreaListSelected.add(practiceArea.trim());
			}
			else
			{
				practiceAreaListSelected.add(practiceArea.trim());
			}
		}
		// List of firms to perform search for chart data
		List<Firm> firmList = new ArrayList<Firm>();
		// See if selected firm was changed. 
		if ((firmSearchChartModelBean != null) && (firmSearchChartModelBean.getSearchResultsFirmList() != null)
				&& (firmSearchChartModelBean.getSearchResultsFirmList().size() > 0))
		{
			if (firmListAll != null && firmListAll.size() > 0)
			{
				Firm firmTemp = firmService.getFirmById(Integer.parseInt(firmSearchChartModelBean.getSearchResultsFirmList().get(0)));
				if (!firmListAll.contains(firmTemp))
				{
					Firm f = new Firm();
					f.setCompany(firmListAll.get(0).getCompany());
					f.setCompanyId(firmListAll.get(0).getCompanyId());
					firmList.add(f);
				}
				else
				{
					firmList.add(firmTemp);
				}
				selectedFirmName = firmList.get(0).getCompany();
			}
		}
		else
		{
			if (searchResultsOriginal != null && searchResultsOriginal.size() > 0)
			{
				for (FirmResultDTO firmResultDTO : searchResultsOriginal)
				{
					Firm f = new Firm();
					f.setCompany(firmResultDTO.getFirmName());
					f.setCompanyId(firmResultDTO.getFirmId());
					Integer firmID = firmResultDTO.getFirmId();
					if (firmID != 0)
					{
						firmList.add(f);
						selectedFirmName = f.getCompany() ;// firmService.getFirmById(firmID).getCompany();
						break;
					}
				}
			}
		}
		
		boolean isPracticeSelected = false;
		
		if (firmList!= null && firmList.size() > 0)
		{
			chartFirmSearchDTO.setSelectedFirms(firmList);
			isCriterionChanged = true;
		}
		
		if (FirmDataType.TOP_15.getValue().equals(firmSearchChartModelBean.getLimitType())) // Top selected
		{
			// nothing to change
			// first get all the results then sort and get top 5
		}
		else if (FirmDataType.BOTTOM_15.getValue().equals(firmSearchChartModelBean.getLimitType()))
		{
			// nothing to change
			// first get all the results then sort and get bottom 5
		}
		else if (FirmDataType.PRACTICE.getValue().equals(firmSearchChartModelBean.getLimitType()))
		{
			if (firmSearchChartModelBean!=null && firmSearchChartModelBean.getPracticeAreaList() != null 
					&& firmSearchChartModelBean.getPracticeAreaList().size() > 0)
			{
				firmSearchChartModelBean.setLimitType(FirmDataType.PRACTICE.getValue());
				isCriterionChanged  = true;
				isPracticeSelected  = true;
				chartFirmSearchDTO.setPracticeArea(firmSearchChartModelBean.getPracticeAreaList());
				practiceAreaListSelected = firmSearchChartModelBean.getPracticeAreaList();
			}
		}
		
		if (isCriterionChanged)
		{
			resultSize = (chartFirmSearchDTO != null) ? firmService.getFirmSearchCount(chartFirmSearchDTO, currentUser) : 0;
		}

		if (resultSize > 0)
		{
			if (isCriterionChanged)
			{
				// if changed then use the changed dto
				searchResults = firmService.firmNewsPubsSearch(chartFirmSearchDTO);
			}else{
				// if nothing was changed, use the previous results as they are
				searchResults = searchResultsOriginal;
			}

			Map<String, Integer> chartMap = new LinkedHashMap<String, Integer>();

			// creating a list of all practice area from search results
			for (FirmResultDTO frDTO : searchResults)
			{
				List<String> practiceList = new ArrayList<String>();
				if (!frDTO.getPracticeArea().contains(";"))
				{
					practiceList.add(frDTO.getPracticeArea());
				}
				else
				{
					// multiple practice area is separated by ";" adding all
					// individuals to practiceList
					practiceList.addAll(ListUtil.fromArray(StringUtil.split(frDTO.getPracticeArea(), ";")));
				}
				
				for (String practiceArea : practiceList)
				{
					if ("Practice Area Not Available".equals(practiceArea) || "".equals(practiceArea) || practiceArea.length()==0)
					{
						practiceArea = "Not Available";
					}
					if (chartMap.containsKey(practiceArea))
					{
						chartMap.put(practiceArea, chartMap.get(practiceArea) + 1);
					}
					else
					{
						chartMap.put(practiceArea, 1);
					}
				}
			}

			// Sorting map
			Map<String, Integer> sortedChartMap = chartMap; //WebUtil.sortMapByIntegerValue(chartMap, 15, ASC);
			
			sortedChartMap = WebUtil.sortMapByIntegerValue(chartMap, chartMap.size(), ASC);
			if (FirmDataType.TOP_15.getValue().equals(firmSearchChartModelBean.getLimitType())) // Top selected
			{
				sortedChartMap = WebUtil.sortMapByIntegerValue(chartMap, 15, ASC);
			}
			else if (FirmDataType.BOTTOM_15.getValue().equals(firmSearchChartModelBean.getLimitType()))
			{
				sortedChartMap = WebUtil.sortMapByIntegerValue(chartMap, 15, DESC);
			}
			else if (!isPracticeSelected)
			{
				sortedChartMap = WebUtil.sortMapByIntegerValue(chartMap, 15, ASC);
			}
			StringBuffer sbPractice = new StringBuffer();
			StringBuffer sbPrcaticeCount = new StringBuffer();
			StringBuffer topPubsByPraticeStringList = new StringBuffer();
			List<String> publicationType = new ArrayList<String>();
			publicationType.add("Pubs");
			int allListCount = 0;
			for (String thisKey : sortedChartMap.keySet())
			{
				allListCount++;
				if (practiceAreaListSelected.contains(thisKey))
				{
					String thisKeyForTopFive = StringPool.BLANK;
					List<String> topFivePracticeAreaList = new ArrayList<String>();
					if(thisKey != null && !("Not Available".equalsIgnoreCase(thisKey))){
						thisKeyForTopFive = thisKey;
						topFivePracticeAreaList.add(thisKeyForTopFive);
					}
					
					FirmSearchDTO firmSearchDTOToFindTopFivePubs = new FirmSearchDTO();
					firmSearchDTOToFindTopFivePubs.setContentType(publicationType);
					firmSearchDTOToFindTopFivePubs.setPracticeArea(topFivePracticeAreaList);
					
					List<FirmResultDTO> topFivePublicationByPracticeList = firmService.findFiveLatestPublicationsByPractice(firmSearchDTOToFindTopFivePubs);  
					StringBuffer topFivePubsList = new StringBuffer();
					topFivePubsList.append("[");
					if(topFivePublicationByPracticeList != null && topFivePublicationByPracticeList.size() > 0){
						int topFivePubsCount = 0;
						for(FirmResultDTO firmResultDTO : topFivePublicationByPracticeList){
							topFivePubsList.append("\"");
							topFivePubsList.append(WebUtil.cleanString(firmResultDTO.getEntryTitle()));
							topFivePubsList.append("\"");
							topFivePubsCount++;
							if(topFivePubsCount == topFivePublicationByPracticeList.size()){
								topFivePubsList.append("");
							}else{
								topFivePubsList.append(",");
							}
						}
					}
					topFivePubsList.append("]");
					topPubsByPraticeStringList.append(topFivePubsList);
					
					if (sbPractice.length() > 0)
					{
						sbPractice.append(", ");
						sbPrcaticeCount.append(", ");
					}
					// would store practice area's corresponding to Firm
					sbPractice.append("\"");
					sbPractice.append(thisKey);
					sbPractice.append("\"");
					// would store count of each practice area corresponding to Firm
					sbPrcaticeCount.append(sortedChartMap.get(thisKey));
				}
				if(allListCount == sortedChartMap.size()){
					topPubsByPraticeStringList.append("");
				}else{
					topPubsByPraticeStringList.append(",");
				}
			}
			//topPubsByPraticeStringList.append("]");
			// Set the chart type
			model.put("NumberOfPubsPracticeAreaChartType", firmSearchChartModelBean.getChartType());
			model.put("numberOfPubsPracticeAreaList", sbPractice.toString());
			model.put("numberOfPubsPracticeAreaCount", sbPrcaticeCount.toString());
			model.put("topPubsByPraticeStringList", topPubsByPraticeStringList.toString());
			model.put("selectedFirmName", selectedFirmName);
			model.put("NumberOfPubsPracticeAreaChartTitleSettings", "NUMBER OF PUBLICATIONS BY PRACTICE");
			model.put("NumberOfPubsPracticeAreaChartTitle", "NUMBER OF PUBLICATIONS BY PRACTICE");
			model.put("selectedFirmName", selectedFirmName);
		}
		model.put("firmListAll", firmListAll);
		model.put("practiceAreaListAll", practiceAreaListAll);
		return "numberofpubspracticearea";
	}

	@ActionMapping
	protected void numberOfPubsPracticeAreaSubmitForm(@ModelAttribute("firmSearchChartModelBean") StandardChartModelBean firmSearchChartModelBean, 
													  ModelMap model, 
													  ActionRequest request,
													  ActionResponse response) throws Exception
	{
		// Persist the changes to the chart settings
		// If this is the home page, save them there, else
		// save with the generic NEWS_PUBS tag.
		String 	currentPortletId 	= WebUtil.getCurrentPortletId(request);
		String 	currentPage			= WebUtil.getCurrentPage(request);
		boolean	isHomePage			= ((currentPage.equals("home")) || (currentPage.equals("")));
		User	currentUser 		= WebUtil.getCurrentUser(request, userService);
		if (!isHomePage)
		{
			currentPage = "NEWS_PUBS";
		}
		userService.saveUserPreferences(currentUser.getId(), 
										currentPortletId, 
										"", 
										WebUtil.getJson(firmSearchChartModelBean), 
										currentPage);
	}
	
	/*
	* Following method remove rendering portlet from the page. And redirects to same page.
	*
	*/

	@ActionMapping(params = "action=removePortleFromThePage")
	public void removePortletFromPage(ActionRequest request, ActionResponse response) throws Exception
	{
		WebUtil.removePortlet(request);
	}
}
