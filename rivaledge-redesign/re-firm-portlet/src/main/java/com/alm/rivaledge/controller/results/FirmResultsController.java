package com.alm.rivaledge.controller.results;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.Format;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFHyperlink;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.alm.rivaledge.controller.AbstractFirmController;
import com.alm.rivaledge.model.ClickToView;
import com.alm.rivaledge.model.FirmSearchModelBean;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.transferobject.FirmResultDTO;
import com.alm.rivaledge.transferobject.FirmSearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.FastDateFormatFactoryUtil;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.servlet.ServletResponseUtil;

/**
 * Firm controller responsible for all operation on Firm search form and results
 * screen
 * 
 * @author FL605
 * @since Sprint 1
 */
@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "firmSearchModelBean", "firmSearchDTO", "searchResults" })
public class FirmResultsController extends AbstractFirmController
{
	protected Log		_log	= LogFactoryUtil.getLog(FirmResultsController.class.getName());

	@RenderMapping
	public String newsPubsResultsView(
			//@RequestParam(value = "drilldownFirmName", required = false) String firmName,
			//@RequestParam(value = "searchCriteria",  required = false) String searchCriteria,
			@ModelAttribute("firmSearchModelBean") FirmSearchModelBean firmSearchModelBean,
			@ModelAttribute("firmSearchDTO") FirmSearchDTO firmSearchDTO,
			ModelMap model, RenderRequest request, RenderResponse response) throws Exception
	{
		
		ClickToView<FirmSearchModelBean, FirmSearchDTO> c2v = new ClickToView<FirmSearchModelBean, FirmSearchDTO>();
		c2v.setSearchModelBean(firmSearchModelBean);
		c2v.setSearchDTO(firmSearchDTO);
		
		honorClick2View(request, model, c2v);
	
		// DTO is changed as result of searchModelBean
			firmSearchDTO = c2v.getSearchDTO();
			firmSearchModelBean = c2v.getSearchModelBean();

		return renderResultsPage(firmSearchModelBean, firmSearchDTO, model, request, response);
		
	}	

	
	@RenderMapping(params = "renderPage=clickToView")
	protected String clickToView(@RequestParam("firmSearchModelBeanJson") String firmSearchModelBeanJson, 
												 ModelMap model, 
												 RenderRequest request,
												 RenderResponse response) throws Exception
	{
		FirmSearchModelBean fsmb = WebUtil.getObject(firmSearchModelBeanJson, FirmSearchModelBean.class);
		fsmb.setAddToSession(false);
		
		model.addAttribute("firmSearchModelBean", fsmb);
		
		FirmSearchDTO firmSearchDTO = getDTOFromModelBean(model, fsmb, request);
		
		return renderResultsPage(fsmb, firmSearchDTO, model, request, response);
	}
	
	private String renderResultsPage(FirmSearchModelBean firmSearchModelBean, FirmSearchDTO firmSearchDTO, ModelMap model, RenderRequest request,
			RenderResponse response)
	{
		User currentUser = WebUtil.getCurrentUser(request, userService);
		
		boolean displayMessage = false;
		
		//null is not allowed in Spring session
		List<FirmResultDTO> searchResults = new ArrayList<FirmResultDTO>();
		
		// Call the service implementation to get the search results count
		int resultSize = (firmSearchDTO != null) ? firmService.getFirmSearchCount(firmSearchDTO, currentUser) : 0;
		
		if (resultSize > 0)
		{
			// We have results that match this search criteria
			// Return the first page of results
			searchResults = firmService.firmNewsPubsSearch(firmSearchDTO);
			
			if(resultSize >= 5000){
				
				displayMessage = true;
				model.addAttribute("displayMessage", displayMessage);
			}
		}
		
		int lastPage = 0;
		if (resultSize > 0)
		{
			if (resultSize > firmSearchDTO.getResultPerPage())
			{
				lastPage = (int) Math.ceil(new Double(resultSize) / firmSearchDTO.getResultPerPage());
			}
			else
			{
				lastPage = 1;
			}
		}

		// Put the total number of pages into the model
		firmSearchModelBean.setLastPage(lastPage);

		// Put the total number of results into the model
		firmSearchModelBean.setTotalResultCount(resultSize);

		// Put the current page size (this is not the page number but the
		// number of records on this page)
		// into the model
		if (searchResults != null && !searchResults.isEmpty())
		{
			firmSearchModelBean.setCurrentPageSize(searchResults.size());
		}
		else
		{
			firmSearchModelBean.setCurrentPageSize(0);
		}

		model.addAttribute("searchResults", searchResults);
		return "firmsearchresults";
	}

	@ActionMapping
	protected void newsAndPublicationsSubmitForm(@ModelAttribute("firmSearchModelBean") FirmSearchModelBean firmSearchModelBean, 
												 ModelMap model, 
												 ActionRequest request,
												 ActionResponse response) throws Exception
	{
	}

	@RenderMapping(params = "displayPrintPage=true")
	protected String printPublications(@ModelAttribute("firmSearchModelBean") FirmSearchModelBean firmSearchModelBean,
									   @ModelAttribute("firmSearchDTO") FirmSearchDTO firmSearchDTO,
									   ModelMap model, 
									   RenderRequest request, 
									   RenderResponse response) throws Exception
	{
		List<FirmResultDTO> searchResults = getSearchResultsForPrintAndExport(firmSearchDTO, request);
		firmSearchModelBean.setTotalResultCount(searchResults.size());
		model.addAttribute("firmResultsDTOList", searchResults);
		WebUtil.populatePrintTimings(model);
		return "printPublications";
	}
	
	/**
	 * Runs the search Query and returns the list without any pagination limit
	 * @param fsDTO
	 * @param request
	 * @return
	 */
	private List<FirmResultDTO> getSearchResultsForPrintAndExport(FirmSearchDTO fsDTO, PortletRequest request)
	{

		FirmSearchDTO serviceFirmSearchDTO = fsDTO;
		try
		{
			serviceFirmSearchDTO = (FirmSearchDTO) fsDTO.clone();
		}
		catch (CloneNotSupportedException e)
		{
			_log.error("Cannot clone FirmSearchDTO. Hence, using the original => " + e.getMessage());
		}

		User currentUser = WebUtil.getCurrentUser(request, userService);

		serviceFirmSearchDTO.setPageNumber(0);
		serviceFirmSearchDTO.setResultPerPage(WebUtil.getResultSizeLimitForCharts(currentUser));

		List<FirmResultDTO> results =   firmService.firmNewsPubsSearch(serviceFirmSearchDTO);

		return results;
	}

	@ResourceMapping("exportFile")
	public void exportFile(@RequestParam String fileType,
			@ModelAttribute("firmSearchDTO") FirmSearchDTO firmSearchDTO,
			ModelMap map, ResourceRequest request, ResourceResponse response)
	{
		/* Please assign list of FirmResultDTO to firmResultsDTOList */
		List<FirmResultDTO> firmResultsDTOList =  getSearchResultsForPrintAndExport(firmSearchDTO, request);

		String fileName = getFileName();

		/* Code for export publications to csv file starts */

		if (fileType != null && fileType.equalsIgnoreCase(ALMConstants.CSV_EXT))
		{

			StringBuilder userInfo = new StringBuilder();

			/* Following code set header in mvc file */

			userInfo = getUserInfo(firmResultsDTOList);

			String csv = userInfo.toString();

			fileName = fileName + StringPool.PERIOD + ALMConstants.CSV_EXT;
			byte[] bytes = csv.getBytes();

			HttpServletRequest servletRequest = PortalUtil.getHttpServletRequest(request);
			HttpServletResponse servletResponse = PortalUtil.getHttpServletResponse(response);

			try
			{
				ServletResponseUtil.sendFile(servletRequest, servletResponse, fileName, bytes, ContentTypes.TEXT_CSV_UTF8);
			}
			catch (IOException e)
			{
				_log.error("GOT ERROR WHILE SENDING SERVLET RESPONSE " + e.getMessage());
			}

			/* Code for export publications to csv file ends */

		}
		else
		{

			/* Code for export publications to excel file starts */

			FileInputStream fileInputStreamReader = null;
			FileOutputStream fileOutPutStreamReader = null;
			File xlsFile = null;

			try
			{
				xlsFile = FileUtil.createTempFile("xls");
				try
				{
					xlsFile.createNewFile();
				}
				catch (IOException e)
				{
					_log.error("ERROR WHILE CREATING NEW FILE " + e.getMessage());
				}
				if (xlsFile.exists())
				{
					fileInputStreamReader = new FileInputStream(xlsFile);
				}

			}
			catch (FileNotFoundException e1)
			{
				_log.info("Error while creating and reading file " + e1.getMessage());
			}

			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet(ALMConstants.WORKBOOK_NAME);

			sheet = getSheet(sheet, firmResultsDTOList, workbook);

			HttpServletRequest servletRequest = PortalUtil.getHttpServletRequest(request);
			HttpServletResponse servletResponse = PortalUtil.getHttpServletResponse(response);

			// byte[] bytes = workbook.getBytes();

			fileName = fileName + StringPool.PERIOD + ALMConstants.XLS_EXT;

			try
			{
				fileInputStreamReader.close();

				fileOutPutStreamReader = new FileOutputStream(xlsFile);

				workbook.write(fileOutPutStreamReader);

				fileOutPutStreamReader.close();

				byte[] bytes = read(xlsFile);

				/*
				 * response.setContentType(ALMConstants.CONTENT_TYPE_XLS);
				 * 
				 * response.getPortletOutputStream().write(bytes);
				 */

				servletResponse.setContentType(ALMConstants.CONTENT_TYPE_XLS);

				ServletResponseUtil.sendFile(servletRequest, servletResponse, fileName, bytes, ALMConstants.CONTENT_TYPE_XLS);
			}
			catch (FileNotFoundException e)
			{
				_log.error("FILE NOT FOUND EXCEPTION " + e.getMessage());
			}
			catch (IOException e)
			{
				_log.error("IO EXCEPTION XLS " + e.getMessage());
			}
			finally
			{

				try
				{
					fileInputStreamReader.close();
					fileOutPutStreamReader.close();
				}
				catch (IOException e)
				{

					_log.error("ERROR WHILE CLOSING INPUT OUTPUT STREAM READER " + e.getMessage());
				}
			}
			/* Code for export publications to excel file ends */
		}

	}

	private String getFileName()
	{

		String fileName = ALMConstants.PUBLICATION_DASH;

		Format dateFormatDate = FastDateFormatFactoryUtil.getSimpleDateFormat("MM.dd.yy", Locale.US);

		fileName = fileName + dateFormatDate.format(CalendarFactoryUtil.getCalendar(Locale.US).getTime());

		return fileName;
	}

	private byte[] read(File file)
	{

		byte[] buffer = new byte[(int) file.length()];
		InputStream ios = null;
		try
		{
			try
			{
				ios = new FileInputStream(file);
			}
			catch (FileNotFoundException e)
			{

				_log.error("FILE NOT FOUND EXCEPTION " + e.getMessage());
			}
			try
			{
				if (ios.read(buffer) == -1)
				{
				}
			}
			catch (IOException e)
			{

				_log.error("ERROR WHILE READING FILE " + e.getMessage());
			}
		}
		finally
		{
			try
			{
				if (ios != null)
					ios.close();
			}
			catch (IOException e)
			{
			}
		}

		return buffer;
	}

	private StringBuilder getUserInfo(List<FirmResultDTO> firmResultsDTOList)
	{

		StringBuilder userInfo = new StringBuilder();

		/* Following code set header in mvc file */

		userInfo.append(ALMConstants.TITLE_NO + StringPool.COMMA);
		userInfo.append(ALMConstants.TYPE + StringPool.COMMA);
		userInfo.append(ALMConstants.TITLE_DATE + StringPool.COMMA);
		userInfo.append(ALMConstants.TITLE_FIRM_NAME + StringPool.COMMA);
		userInfo.append(ALMConstants.TITLE_TITLE + StringPool.COMMA);
		userInfo.append(ALMConstants.TITLE_SOURCE + StringPool.COMMA);
		userInfo.append(ALMConstants.TITLE_DESCRIPTION + StringPool.COMMA);
		userInfo.append(ALMConstants.TITLE_RELATED_PRACTICE + StringPool.COMMA);
		userInfo.append(ALMConstants.TITLE_LINK + StringPool.NEW_LINE);

		/* Following code populated list values from second row */

		int keyCVS = 0;
		for (FirmResultDTO firmResultDTO : firmResultsDTOList)
		{
			keyCVS++;
			userInfo.append(String.valueOf(keyCVS) + StringPool.COMMA);
			userInfo.append(firmResultDTO.getDataType() + StringPool.COMMA);
			
			String formattedDate = "";
			
			if(firmResultDTO.getEntryDate() != null)
			{
				 formattedDate = ALMConstants.SEARCH_DATE_FORMAT.format(firmResultDTO.getEntryDate());
			}
			userInfo.append(formattedDate + StringPool.COMMA);
			userInfo.append(firmResultDTO.getFirmName() + StringPool.COMMA);
			userInfo.append(firmResultDTO.getEntryTitle() + StringPool.COMMA);
			userInfo.append(firmResultDTO.getDataSourceURL() + StringPool.COMMA);
			userInfo.append(firmResultDTO.getEntryDescription() + StringPool.COMMA);
			userInfo.append(firmResultDTO.getPracticeArea() + StringPool.COMMA);
			userInfo.append(firmResultDTO.getEntryURL() + StringPool.NEW_LINE);

		}

		return userInfo;
	}

	private HSSFSheet getSheet(HSSFSheet sheet, List<FirmResultDTO> firmResultsDTOList, HSSFWorkbook workbook)
	{

		int key = 0;

		Map<String, Object[]> data = new HashMap<String, Object[]>();

		/* Following code set header in excel file */

		data.put(String.valueOf(key), new Object[] { ALMConstants.TITLE_NO, ALMConstants.TYPE, ALMConstants.TITLE_DATE, ALMConstants.TITLE_FIRM_NAME, ALMConstants.TITLE_TITLE,
				ALMConstants.TITLE_SOURCE, ALMConstants.TITLE_DESCRIPTION, ALMConstants.TITLE_RELATED_PRACTICE, ALMConstants.TITLE_LINK });

		if (firmResultsDTOList != null && firmResultsDTOList.size() > 0)
		{
			for (FirmResultDTO firmResultDTO : firmResultsDTOList)
			{
				key++;
				String formattedDate = StringPool.BLANK;
				String dataSourceURL = StringPool.BLANK;
				String dataType = firmResultDTO.getDataType();
				dataSourceURL = firmResultDTO.getDataSourceURL();
				if(firmResultDTO.getEntryDate() != null)
				{
					formattedDate = ALMConstants.SEARCH_DATE_FORMAT.format(firmResultDTO.getEntryDate());
				}
				
				if( (dataSourceURL.equalsIgnoreCase(StringPool.BLANK) || dataSourceURL == null)){
					if(dataType.equalsIgnoreCase("pubs")){
						dataSourceURL = firmResultDTO.getFirmURL();
					}
				}
				
				if((dataSourceURL != null) && (!dataSourceURL.equalsIgnoreCase(StringPool.BLANK)) && (!dataSourceURL.startsWith(ALMConstants.HTTP))){
					dataSourceURL = ALMConstants.HTTP + dataSourceURL;
				}
				
				data.put(
						String.valueOf(key),
						new Object[] { String.valueOf(key), firmResultDTO.getDataType(), formattedDate, firmResultDTO.getFirmName(), firmResultDTO.getEntryTitle(),
								dataSourceURL, firmResultDTO.getEntryDescription(), firmResultDTO.getPracticeArea(), firmResultDTO.getEntryURL() });
				
			}
		}

		
		HSSFCellStyle hyperlinkCellStyle = workbook.createCellStyle();
		HSSFFont hyperlinkFont = workbook.createFont();
		hyperlinkFont.setUnderline(HSSFFont.U_SINGLE);
		hyperlinkFont.setColor(HSSFColor.BLUE.index);
		hyperlinkCellStyle.setFont(hyperlinkFont);
		
		HSSFCellStyle boldCellStyle = workbook.createCellStyle();
		HSSFFont boldCellFont = workbook.createFont();
		boldCellFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		boldCellStyle.setFont(boldCellFont);
		
		int rownum = 0;
		for (key = 0; key < data.size(); key++)
		{
			rownum = key;
			HSSFRow row = sheet.createRow((rownum));
			Object[] objArr = data.get(String.valueOf(key));
			int cellnum = 0;
			for (Object obj : objArr)
			{
				HSSFCell cell = row.createCell(cellnum);
				if (obj instanceof Date)
				{
					cell.setCellValue((Date) obj);
				}
				else if (obj instanceof Boolean)
				{
					cell.setCellValue((Boolean) obj);
				}
				else if (obj instanceof String)
				{
					String str = (String) obj;
					
					if (str.startsWith(ALMConstants.HTTP) || str.startsWith(ALMConstants.HTTPS))
					{
						HSSFHyperlink hyperLink = new HSSFHyperlink(HSSFHyperlink.LINK_URL);
						hyperLink.setAddress(str);
						cell.setHyperlink(hyperLink);
						HSSFRichTextString textString = new HSSFRichTextString(str);
						cell.setCellStyle(hyperlinkCellStyle);
						cell.setCellValue(textString);
					}
					else
					{
						HSSFRichTextString textString = new HSSFRichTextString(str);
						if (key == 0)
						{
							cell.setCellStyle(boldCellStyle);
						}
						cell.setCellValue(textString);
					}

				}
				else if (obj instanceof Double)
				{
					cell.setCellValue((Double) obj);
				}
				cellnum++;
			}
		}

		return sheet;
	}

}