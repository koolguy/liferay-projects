package com.alm.rivaledge.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.alm.rivaledge.model.ClickToView;
import com.alm.rivaledge.model.FirmSearchModelBean;
import com.alm.rivaledge.model.chart.StandardChartModelBean;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserPreferences;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.service.UserService;
import com.alm.rivaledge.service.WatchlistService;
import com.alm.rivaledge.transferobject.FirmResultDTO;
import com.alm.rivaledge.transferobject.FirmSearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;

/**
 * Firm controller responsible for all operation on Firm search form and results
 * screen
 * 
 * @author FL605
 * @since Sprint 1
 */
public abstract class AbstractFirmController
{
	protected Log _log = LogFactoryUtil.getLog(AbstractFirmController.class.getName());
	
	@Autowired
	protected FirmService firmService;

	@Autowired
	protected UserService userService;
	
	@Autowired
	protected WatchlistService watchlistService;

	/**
	 * Populates the Model which holds the Search criteria in search form initializing to default values
	 * 
	 * @param model
	 */
	@ModelAttribute
	public void populateFirmSearchModelBean(ModelMap model, PortletRequest request)
	{
		String				currentPortletId		 = WebUtil.getCurrentPortletId(request);
		String				currentPage				 = WebUtil.getCurrentPage(request);
		boolean				isHomePage				 = ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		boolean 			isC2VPage				 = 	WebUtil.isClickToViewPage(request);
		model.addAttribute("isC2VPage", isC2VPage);
		
		
		
		User 				currentUser 			 = (model.get("currentUser") != null) 
															? ((User) model.get("currentUser")) 
															: WebUtil.getCurrentUser(request, userService); 
															
		boolean				isAdminUser				 = WebUtil.isAdminUser(currentUser);									
		
		FirmSearchModelBean firmSearchModelBean		 = (FirmSearchModelBean) model.get("firmSearchModelBean");
		
		if (currentUser != null)
		{
			model.put("currentUser", currentUser);
			model.put("isAdminUser", isAdminUser);
		}	
			// There are three handling options here.
			// 1. If this is the Home page, get the current portlet id,
			//    which can be either that of the search portlet or a 
			//    chart portlet.
			//
			// 2. If this is the details-drilldown page, then we let the 
			//    details come from session, and let the results controller
			//    replace the search criteria with the request parameter.
			//
			// 3. If neither condition, then it must be the standard search
			//    and results page, so we will load the search criteria from 
			//    the database.
		
			if(isHomePage)
			{
				firmSearchModelBean = getUserPreference(currentUser.getId(), currentPortletId, currentPage);
			}
			
			if (!isHomePage && model.get("firmSearchModelBean") == null)
			{
				currentPortletId 	= "NEWS_PUBS";
				currentPage			= "NEWS_PUBS";
				firmSearchModelBean = getUserPreference(currentUser.getId(), currentPortletId, currentPage);
			}
			
			if (firmSearchModelBean == null)
			{
				firmSearchModelBean = getDefaultFirmSearchModelBean(currentUser);
				
			}

			firmSearchModelBean.setPortletId(currentPortletId);
			
			if(isHomePage || isC2VPage)
			{
				firmSearchModelBean.setAddToSession(false);
			}
			
			model.addAttribute("firmSearchModelBean", firmSearchModelBean);
		

		//transformBean(firmSearchModelBean);
			FirmSearchDTO firmSearchDTO = getDTOFromModelBean(model, firmSearchModelBean,request);
			model.addAttribute("firmSearchDTO", firmSearchDTO);
				
			if (model.get("searchResults") == null)
			{
				model.addAttribute("searchResults", new ArrayList<FirmResultDTO>());
			}
		
	}

	/***
	 * Returns the new searchModelBean with default search criteria and default watchlist for the user
	 * @param currentUser
	 * @return
	 */
	protected FirmSearchModelBean getDefaultFirmSearchModelBean(User currentUser)
	{
		List<UserGroup> 	allWatchListsDefaultList = null;
		
		FirmSearchModelBean firmSearchModelBean = new FirmSearchModelBean();
		firmSearchModelBean.init(); //do an explicit init for default values

		if (currentUser != null)
		{
			allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
		}
		
		List<String> firmList = new ArrayList<String>();
		List<Integer> firmListWatchList = new ArrayList<Integer>();
		
		if (allWatchListsDefaultList == null || allWatchListsDefaultList.isEmpty())
		{
			//FirmType
			firmSearchModelBean.setFirmType(ALMConstants.RIVALEDGE_LIST);
			firmList.add(ALMConstants.AMLAW_100);
		} 
		else
		{
			firmSearchModelBean.setFirmType(ALMConstants.WATCH_LIST);
			//firmListWatchList.add("" + allWatchListsDefaultList.get(0).getGroupId());
			firmListWatchList.add(allWatchListsDefaultList.get(0).getGroupId());
		}
		firmSearchModelBean.setFirmList(firmList);
		firmSearchModelBean.setFirmListWatchList(firmListWatchList);
		return firmSearchModelBean;
		
	}

	protected FirmSearchModelBean getUserPreference(Integer userId, String portletId, String currentPage)
	{
		UserPreferences uP = userService.getUserPreferenceByUserIdAndPortletId(userId, portletId, currentPage);
		if ((uP != null) && (uP.getSearchCriteriaJSON() != null) && (uP.getSearchCriteriaJSON().trim().length() > 0))
		{
			return (WebUtil.getObject(uP.getSearchCriteriaJSON(), FirmSearchModelBean.class));
		}
		return (null);
	}
	
	protected StandardChartModelBean getUserChartPreference(Integer userId, String portletId, String currentPage)
	{
		UserPreferences uP = userService.getUserPreferenceByUserIdAndPortletId(userId, portletId, currentPage);
		if ((uP != null) && (uP.getChartCriteriaJSON() != null) && (uP.getChartCriteriaJSON().trim().length() > 0))
		{
			return (WebUtil.getObject(uP.getChartCriteriaJSON(), StandardChartModelBean.class));
		}
		return (null);
	}

	/**
	 * Creates a FirmSearchDTO from  FirmSearchModelBean
	 * @param model
	 * @param firmSearchModelBean
	 * @return
	 */
	protected FirmSearchDTO getDTOFromModelBean(ModelMap model, FirmSearchModelBean firmSearchModelBean,PortletRequest request)
	{

		FirmSearchDTO firmSearchDTO = new FirmSearchDTO();

		// Get the page number from the model bean
		int pageNumber = firmSearchModelBean.getGoToPage();
		int orderBy = firmSearchModelBean.getOrderBy();
		int searchResultsPerPage = firmSearchModelBean.getSearchResultsPerPage();

		firmSearchDTO.setOrderBy(orderBy);
		firmSearchDTO.setResultPerPage(searchResultsPerPage);

		// Plug the page number into the search DTO
		//This sets the starting limit for the paginated search results in the sql query
		firmSearchDTO.setPageNumber(pageNumber < 0 ? 0 : (pageNumber - 1) * searchResultsPerPage);

		// Set the sort column and order into the search DTO
		firmSearchDTO.setSortColumn(firmSearchModelBean.getSortColumn());
		firmSearchDTO.setSortOrder(firmSearchModelBean.getSortOrder());

		// Get the selected firms, if any, and set them into the search DTO
		setSelectedFirms(model, firmSearchDTO, firmSearchModelBean,request);

		// Set the date range criteria
		// There will always be a date range criteria, as the form view
		// does not allow this to be null
		setDateRange(model, firmSearchDTO, firmSearchModelBean);

		// Set the remaining search criteria, such as practice areas, etc.
		// See the setOtherSearchParameter method for more details
		setOtherSearchParameter(model, firmSearchDTO, firmSearchModelBean);
		return firmSearchDTO;

	}
	
	
	/**
	 * Based on the User Action, where the request is coming from, This will alter the searchBean, the searchDTO if required 
	 * and put them back to C2V.
	 * @param request
	 * @param model
	 * @param c2v
	 * @throws UnsupportedEncodingException
	 */
	protected void honorClick2View(PortletRequest request, ModelMap model, ClickToView<FirmSearchModelBean, FirmSearchDTO> c2v) throws UnsupportedEncodingException
	{
		
		
		if(!WebUtil.isClickToViewPage(request))
		{
			return; // its not C2V, then what the hell will I do here I'm returning, bye!!
		}
		

		HttpServletRequest originalRequest = WebUtil.getHttpServletRequest(request);
		
		String searchCriteria = originalRequest.getParameter("searchCriteria");
		FirmSearchModelBean firmSearchModelBean = null;
		
		//String searchCriteria = originalRequest.getParameter("searchCriteria");
		if (StringUtils.isNotBlank(searchCriteria))
		{
			String searchJSON = URLDecoder.decode(searchCriteria, "UTF-8");
			firmSearchModelBean = WebUtil.getObject(searchJSON, FirmSearchModelBean.class); // get the SearchBean
		}
		else
		{
			firmSearchModelBean =  WebUtil.cloneBean(c2v.getSearchModelBean(),  FirmSearchModelBean.class); // clone the bean, because we don't want to alter the original search criteria
		}
		
		String practiceArea = originalRequest.getParameter("drilldownPracticeAreaName");
		
		if (StringUtils.isNotBlank(practiceArea))
		{
			String searchPractice = URLDecoder.decode(practiceArea, "UTF-8");
			firmSearchModelBean.setSelectedPracticeArea(searchPractice);
			List<String> selectedPractices = new ArrayList<String>(1);
			selectedPractices.add(searchPractice);
			firmSearchModelBean.setPracticeArea(selectedPractices);
		}
		
		String firmName = originalRequest.getParameter("drilldownFirmName");
		
		if (StringUtils.isNotBlank(firmName))
		{
			String drilldownFirm = URLDecoder.decode(firmName, "UTF-8");
			List<Firm> matchingFirms = firmService.getFirmByName(drilldownFirm);
			if ((matchingFirms != null) && (!matchingFirms.isEmpty()))
			{
				Firm firm = matchingFirms.get(0);

				firmSearchModelBean.setFirmType(ALMConstants.INDIVIDUAL_FIRMS); // set it to Individual as we know there is single firm

				List<String> selectedFirmsList = new ArrayList<String>(1);
				selectedFirmsList.add("" + firm.getCompanyId()); // add Firm Id as String

				firmSearchModelBean.setFirmList(selectedFirmsList); // actual Ids to get DTO
				firmSearchModelBean.setSelectedFirms(firm.getCompany()); // this will go into SearchCriteria String
			}
		}
		
		firmSearchModelBean.setGoToPage(1);
		firmSearchModelBean.setAddToSession(false); // don't add this to session. But, only to model
		model.addAttribute("firmSearchModelBean", firmSearchModelBean); 
		
		FirmSearchDTO firmSearchDTO = getDTOFromModelBean(model, firmSearchModelBean, request);
		c2v.setSearchModelBean(firmSearchModelBean); // set the changed bean back to C2V
		c2v.setSearchDTO(firmSearchDTO); // set the changed DTO back to C2V
			
	}

	/***
	 * Transforms , and ; separated values at 0-index to List values <br>
	 * We need values in List rather than CSV, so that they can bind to <code>Spring Form<code> 
	 * later on from session
	 * 
	 * @param firmSearchModelBean
	 */
	protected void transformBean(FirmSearchModelBean firmSearchModelBean)
	{

		List<String> firmList = firmSearchModelBean.getFirmList();

		if (firmList != null && !firmList.isEmpty())
		{
			String firm = firmList.get(0);
			firmSearchModelBean.setFirmList(ListUtil.fromArray(StringUtil.split(firm, ","))); //firmList is , separated
		}

		List<String> contentTypeList = firmSearchModelBean.getContentType();

		if (contentTypeList != null && !contentTypeList.isEmpty())
		{
			String contentType = contentTypeList.get(0);
			firmSearchModelBean.setContentType(ListUtil.fromArray(StringUtil.split(contentType, ";")));
		}

		List<String> practiceAreaList = firmSearchModelBean.getPracticeArea();

		if (practiceAreaList != null && !practiceAreaList.isEmpty())
		{
			String practiceArea = practiceAreaList.get(0);
			firmSearchModelBean.setPracticeArea(ListUtil.fromArray(StringUtil.split(practiceArea, ";")));
		}

		List<String> twitterTypeList = firmSearchModelBean.getTwitterType();

		if (twitterTypeList != null && !twitterTypeList.isEmpty())
		{
			String twitterType = twitterTypeList.get(0);
			firmSearchModelBean.setTwitterType(ListUtil.fromArray(StringUtil.split(twitterType, ";")));
		}

	}

	/**
	 * Set date range in FirmSearchDTO reference based on user input on Firm
	 * search Screen
	 * 
	 * @param request
	 * @param response
	 * @param firmSearchDTO
	 */
	protected void setDateRange(ModelMap model, FirmSearchDTO firmSearchDTO, FirmSearchModelBean firmSearchModelBean)
	{
		String dateText = firmSearchModelBean.getDateText();

		Calendar calendar = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		firmSearchDTO.setToDate(date);
		if (dateText != null && !dateText.contains("-"))
		{

			if (dateText.equals(ALMConstants.ANY))
			{
				firmSearchDTO.setFromDate(null);
				firmSearchDTO.setToDate(null);
			}
			else if (dateText.equals(ALMConstants.LAST_WEEK))
			{
				calendar.add(Calendar.WEEK_OF_MONTH, -1);
				firmSearchDTO.setFromDate(calendar.getTime());
			}
			else if (dateText.equals(ALMConstants.LAST_30_DAYS))
			{
				calendar.add(Calendar.MONTH, -1);
				firmSearchDTO.setFromDate(calendar.getTime());
			}
			else if (dateText.equals(ALMConstants.LAST_90_DAYS))
			{
				calendar.add(Calendar.MONTH, -3);
				firmSearchDTO.setFromDate(calendar.getTime());
			}
			else if (dateText.equals(ALMConstants.LAST_6_MONTHS))
			{
				calendar.add(Calendar.MONTH, -6);
				firmSearchDTO.setFromDate(calendar.getTime());
			}
			else
			{
				calendar.add(Calendar.YEAR, -1);
				firmSearchDTO.setFromDate(calendar.getTime());
			}

		}
		else
		{
			String[] dateRange = dateText.split("-");
			try
			{
				firmSearchDTO.setFromDate(dateFormat.parse(dateRange[0]));
				firmSearchDTO.setToDate(dateFormat.parse(dateRange[1]));
			}
			catch (ParseException e)
			{
				e.printStackTrace();
			}

		}

	}

	/**
	 * Set Practice Area, contentType, keywords and selectedTwitterPostType in
	 * FirmSearchDTO reference based on user input on Firm search Screen
	 * 
	 * @param request
	 * @param response
	 * @param firmSearchDTO
	 */
	protected void setOtherSearchParameter(ModelMap model, FirmSearchDTO firmSearchDTO, FirmSearchModelBean firmSearchModelBean)
	{
		if (firmSearchModelBean.getContentType() != null
				&& !firmSearchModelBean.getContentType().contains(ALMConstants.ALL_Types))
		{
			firmSearchDTO.setContentType(firmSearchModelBean.getContentType());
		}
		if (firmSearchModelBean.getTwitterType() != null && !firmSearchModelBean.getTwitterType().contains(ALMConstants.ALL))
		{
			firmSearchDTO.setTwitterPostType(firmSearchModelBean.getTwitterType());
		}
		if (firmSearchModelBean.getPracticeArea() != null
				&& !firmSearchModelBean.getPracticeArea().contains(ALMConstants.ALL_PRACTICE_AREAS))
		{
			firmSearchDTO.setPracticeArea(firmSearchModelBean.getPracticeArea());
		}
		firmSearchModelBean.setSelectedContentType(firmSearchModelBean.getContentType().toString().replace("[", "").replace("]", ""));
		firmSearchModelBean.setSelectedTwitterPostType(firmSearchModelBean.getTwitterType().toString().replace("[", "").replace("]", ""));
		firmSearchModelBean.setSelectedPracticeArea(firmSearchModelBean.getPracticeArea().toString().replace("[", "").replace("]", ""));
		firmSearchDTO.setKeyword(firmSearchModelBean.getKeywords());
	}

	/**
	 * Set Firms information in FirmSearchDTO reference based on user input on
	 * Firm search Screen
	 * 
	 * @param selectedFirms
	 * @param searchFormTo
	 * @param request
	 */
	protected void setSelectedFirms(ModelMap model, FirmSearchDTO searchFormTo, FirmSearchModelBean firmSearchModelBean,PortletRequest request)
	{

		String firmType = firmSearchModelBean.getFirmType();
		List<Firm> firmsList = new ArrayList<Firm>();
//		List<Firm> firmListWatchList = new ArrayList<Firm>();
		List<String> firmsListForResults = new ArrayList<String>();

		if (ALMConstants.ALL_FIRMS.equals(firmType))
		{
			//searchFormTo.setSelectedFirms(null);

			firmsList.addAll(firmService.getAllFirms());
			searchFormTo.setSelectedFirms(firmsList);
			firmsListForResults.add(firmType);
			firmSearchModelBean.setFirmListWatchList(null);
			firmSearchModelBean.setFirmList(null);
		}
		else if (ALMConstants.WATCH_LIST.equals(firmType))
		{
			User 		currentUser 	= WebUtil.getCurrentUser(request, userService);
			List<UserGroup> allWatchListsDefaultList = null;
			String 		watchListsName = request.getParameter("Firmstext");
			List<Firm> 	firmsWatchList 	= null;
			if (currentUser != null)
			{
				firmsWatchList = watchlistService.findAllFirmsInManyWatchlists(firmSearchModelBean.getFirmListWatchList(), currentUser.getId());
			}
			
			//firmListWatchList.addAll(firmsWatchList);
			//firmsList.addAll(firmsWatchList);

/*			for (Firm firmValue : firmsWatchList)
			{
				if (firmValue != null)
				{
					firmsListForResults.add(firmValue.getCompany());
				}
			}*/
			
			if (firmsWatchList.size() == 0)
			{
				List<Firm> notAvailableFirms = new ArrayList<Firm>();
				Firm tempFirm = new Firm();
				tempFirm.setCompanyId(-1);
				notAvailableFirms.add(tempFirm);
				searchFormTo.setSelectedFirms(notAvailableFirms);
			}
			else
			{
				searchFormTo.setSelectedFirms(firmsWatchList);
			}
			
			if (firmsWatchList.size() == 0)
			{
				List<Firm> 	notAvailableFirms 	= new ArrayList<Firm>();
				Firm 		tempFirm 			= new Firm();
				tempFirm.setCompanyId(0);
				notAvailableFirms.add(tempFirm);
				searchFormTo.setSelectedFirms(notAvailableFirms);
				//firmsListForResults.add("No Firms");
			}
			else
			{
				searchFormTo.setSelectedFirms(firmsWatchList);
			}
			firmSearchModelBean.setFirmList(null);
			if(watchListsName == null)
			{
				allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
				if(allWatchListsDefaultList != null)
				{
					firmsListForResults.add(allWatchListsDefaultList.get(0).getGroupName());
				}
			}
			else
			{
				firmsListForResults.add(watchListsName);
			}
		}	
		else if (ALMConstants.RIVALEDGE_LIST.equals(firmType))
		{
			firmsList.addAll(firmService.getRanking(firmSearchModelBean.getFirmList().get(0)));
			firmsListForResults.add(firmSearchModelBean.getFirmList().get(0));
			firmSearchModelBean.setFirmListWatchList(null);
			searchFormTo.setSelectedFirms(firmsList);
		}
		else
		{
			for (String firmId : firmSearchModelBean.getFirmList())
			{
				Firm firmObject = firmService.getFirmById(Integer.parseInt(firmId));
				firmsList.add(firmObject);
				firmsListForResults.add(firmObject.getCompany());
			}
			
			searchFormTo.setSelectedFirms(firmsList);
			firmSearchModelBean.setFirmListWatchList(null);
		}
		//searchFormTo.setSelectedFirms(firmsList);

		firmSearchModelBean.setSelectedFirms(firmsListForResults.toString().replace("[", "").replace("]", ""));
	}
}
