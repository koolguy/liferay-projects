<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@page import="java.util.List"%>
<%@page import="com.alm.rivaledge.transferobject.FirmSearchDTO"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.alm.rivaledge.util.ALMConstants"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>

<%@include file="./common.jsp" %>

<link rel="stylesheet" href="<%=renderRequest.getContextPath()%>/css/main.css" />

<%--
<c:if test="${isHomePage}">

	<script src="/re-theme/js/jquery-1.9.1.js"></script>
	<script src="/re-theme/js/jquery-ui.js"></script> 
	<script src="/re-theme/js/popup.js?v=${buildVersionId}"></script>
	<script src="/re-theme/js/s_code.js"></script> 
	<script src="/re-theme/js/webwidget_tab.js"></script> 

</c:if>
 --%>

<portlet:actionURL var="submitURL">
	<portlet:param name="action" value="changeSearchCriteria"/>
</portlet:actionURL>

<portlet:actionURL var="clickToViewURL">
	<portlet:param name="action" value="clickToView"/>
</portlet:actionURL>

<portlet:resourceURL var="persistSearchCriteriaForChartURL" id="persistSearchCriteriaForChart">
</portlet:resourceURL>






<!-- functions specific to apply buttons -->
<script type="text/javascript">

	//initializeSearchCriteria();
	/**
	*Autoselect previously selected search critria
	*/
	function <portlet:namespace/>initializeSearchCriteria()
	{
		<portlet:namespace/>applyFirms();
		<portlet:namespace/>initializeDate();
		<portlet:namespace/>applyContentType();
		<portlet:namespace/>applyTwitterType();
		<portlet:namespace/>applyPracticeArea();
		<portlet:namespace/>contentTypeSelection();
		
	}
	/**
	 * Initialized the user selected Date from previous search and this is retained through out the session
	 */
	function <portlet:namespace/>initializeDate()
	{
	
	var dateTextStr = '${firmSearchModelBean.dateText}';
	
	if(dateTextStr.match('^Any') || dateTextStr.match('^Last'))
		{
			$("#<portlet:namespace/>datePeriodSelect option[value='"+dateTextStr+"']").attr('selected','selected'); 
			$('#<portlet:namespace/>period').prop("checked", true);
		}
	else{ // we have date range parse it and set the Date on datePicker
			$( "#<portlet:namespace/>from" ).datepicker( "setDate", dateTextStr.split("-")[0]);
			$( "#<portlet:namespace/>to" ).datepicker( "setDate", dateTextStr.split("-")[1]);
		}
	
		$("#<portlet:namespace/>dateText").val(dateTextStr);
	
	}
	
	function <portlet:namespace/>applyFirms()
	{		
		
		
		var checkFlag = $("#<portlet:namespace/>allfirms").is(":checked");
		var valueofchecked =$("input[name='firmType']:checked").val();
		if(valueofchecked=="All Firms"){
			checkFlag=true;
			 $('#<portlet:namespace/>allfirms').prop("checked", true);
		}
		var allSelectedValue = [];
		var allSelectedIds = [];
		
		$("#<portlet:namespace/>Firmstext").val('');
		$("#<portlet:namespace/>selectedFirms").val('');
		 
		var isFirmChecked = false;
		var allWatchListCounter = 0;
			
			$('#<portlet:namespace/>individualfirmsWatchListDIVFirm input[type=checkbox]:checked').each(function() {
			
			 allSelectedValue.push($(this).attr('labelAttr'));
			 allSelectedIds.push($(this).val());
			 $("#<portlet:namespace/>firm_watchlist").prop("checked", true);
			 $('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
	
							isFirmChecked = true;
							
							 $("#homePageShowFirms .rivaledgeListAMLAW_100_Firm").prop("checked", false);
							 $("#homePageShowFirms .rivaledgeListAMLAW_200_Firm").prop("checked", false);
							 $("#homePageShowFirms .rivaledgeListNLJ_250_Firm").prop("checked", false);

	 			});
		
		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
		
			 allSelectedValue.push($(this).attr('mylabel'));
			 allSelectedIds.push($(this).val());
			 isFirmChecked = true;
			 $("#homePageShowFirms .rivaledgeListAMLAW_100_Firm").prop("checked", false);
			 $("#homePageShowFirms .rivaledgeListAMLAW_200_Firm").prop("checked", false);
			 $("#homePageShowFirms .rivaledgeListNLJ_250_Firm").prop("checked", false);
	 	});
		
		if(allSelectedValue != ''  &&  allSelectedIds != '') 
			{
			
				$("#<portlet:namespace/>Firmstext").val(allSelectedValue.join(",")); //Use a comma separator for values which show up on UI
				$("#<portlet:namespace/>selectedFirms").val(allSelectedIds.join(";"));
				 isFirmChecked = true;
				
			}
		else if($("#homePageShowFirms .rivaledgeListAMLAW_100_Firm").is(":checked"))
		{
			
			$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.AMLAW_100 %>');
			$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.AMLAW_100 %>');
			$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
			$('#<portlet:namespace/>individualfirmsWatchListDIVFirm input[type=checkbox]:checked').removeAttr('checked');
			isFirmChecked = true;
			
		} 
		else if($("#homePageShowFirms .rivaledgeListAMLAW_200_Firm").is(":checked"))
		{
			
			$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.AMLAW_200 %>');
			$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.AMLAW_200 %>');
			$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
			$('#<portlet:namespace/>individualfirmsWatchListDIVFirm input[type=checkbox]:checked').removeAttr('checked');
			isFirmChecked = true;
			
		} 
		else if($("#homePageShowFirms .rivaledgeListNLJ_250_Firm").is(":checked"))
		{
			
			$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.NLJ_250 %>');
			$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.NLJ_250 %>');
			$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
			$('#<portlet:namespace/>individualfirmsWatchListDIVFirm input[type=checkbox]:checked').removeAttr('checked');
			isFirmChecked = true;
			
		} 
		else
		{
			$("#<portlet:namespace/>allfirms, #<portlet:namespace/>firm_watchlist").each(function(){
				
				
				if($("#<portlet:namespace/>allfirms").is(":checked"))
					{
					
						$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.ALL_FIRMS %>');
						$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.ALL_FIRMS %>');
						$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.ALL_FIRMS %>");
						isFirmChecked = true;
						
					} 
				 else
					{
					 
					 var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
						
						$('#<portlet:namespace/>individualfirmsWatchListDIVFirm input[type=checkbox]').each(function() {
							 if(this.value == watchListId)
								{
									$(this).prop("checked", true);
									$("#<portlet:namespace/>Firmstext").val($(this).attr('labelAttr'));
									$("#<portlet:namespace/>selectedFirms").val($(this).val());

									$("#<portlet:namespace/>firm_watchlist").prop("checked", true);
									$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
									isFirmChecked = true;
								}
						});		
					
						
					}
			});
			
		}
		
		if(isFirmChecked == false)
			{
				$("#<portlet:namespace/>Firmstext").val('AmLaw 100');
				$("#<portlet:namespace/>selectedFirms").val('AmLaw 100');
				$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
				 $("#<portlet:namespace/>firm_watchlist").prop("checked", true);
				 $("#homePageShowFirms .rivaledgeListAMLAW_100_Firm").prop("checked", true);
				 
			}
		
		 <portlet:namespace/>changeOnFirms(); 
	}
	
	

	function <portlet:namespace/>applyContentType()
	{
		var allValsContentType = [];
	    $('#<portlet:namespace/>contentTypeSelectDiv :checked').each(function() {
	    	allValsContentType.push($(this).val());
	    });
	    $("#<portlet:namespace/>contentType").val(allValsContentType.join(";"));
	}
	
	function <portlet:namespace/>applyTwitterType()
	{
		var allValsTwitterPostType = [];
	    $('#<portlet:namespace/>twitterPostTypeDiv :checked').each(function() {
	    	allValsTwitterPostType.push($(this).val());
	    });
	    $("#<portlet:namespace/>selectedTwitterPostType").val(allValsTwitterPostType.join(";"));
	}
	
	function <portlet:namespace/>applyPracticeArea()
	{
		var allValsPracticeArea = [];
		 $('input[name=practiceArea]:checked').each(function() {
			 allValsPracticeArea.push($(this).val());
		   });
		$("#<portlet:namespace/>selectedPracticeArea").val(allValsPracticeArea.join(";"));
		<portlet:namespace/>changeOnPracticeArea();
	}
	
	function <portlet:namespace/>applyDate()
	 {
	  var  checkDate=false;
	  var dateValue;
	  
	  if($("#<portlet:namespace/>dateRange").is(":checked"))
	   {
	   
	   var fromDate = new Date($("#<portlet:namespace/>from").val());
	   var toDate = new Date($("#<portlet:namespace/>to").val()); 
	   if(!<portlet:namespace/>ValidateDate($("#<portlet:namespace/>from").val()))
	   {
	     checkDate=true;
	        $('#<portlet:namespace/>fromdateError').css('display','block');      
	   }
	   else
	   {
	    checkDate=false;
	    $('#<portlet:namespace/>fromdateError').css('display','none');
	   }  
	   if(!<portlet:namespace/>ValidateDate($("#<portlet:namespace/>to").val()))
	    {
	    checkDate=true;
	    $('#<portlet:namespace/>todateError').css('display','block');
	    
	    }  
	   else
	   {
	    if(!checkDate)
	    {
	     checkDate=false;
	     $('#<portlet:namespace/>todateError').css('display','none'); 
	    }
	   
	   }
	   if(!(toDate >= fromDate) && !checkDate)
	   {
	    $('#<portlet:namespace/>dateValidError').css('display','block');
	    checkDate=true;
	   } 
	   else
	   {
	    $('#<portlet:namespace/>dateValidError').css('display','none');
	   }
	  
	   if(checkDate)
	   {
	   return checkDate; 
	   }
	  }
	  
	  if($("#<portlet:namespace/>period").is(":checked"))
	  {
	   
	   dateValue=$("#<portlet:namespace/>datePeriodSelect").val();
	   
	  } 
	  else if($("#<portlet:namespace/>dateRange").is(":checked"))
	  {
	   dateValue=$("#<portlet:namespace/>from").val() + "-"+ $("#<portlet:namespace/>to").val();
	  } 
	  
	  $("#<portlet:namespace/>dateText").val(dateValue);
	 }
	
	
	function <portlet:namespace/>changeOnFirms()
	{
		var allFirmsCounter = 0;
		var allFirmsCounterNoChecked = 0;
		$('#<portlet:namespace/>allOtherFirmDiv :checked').each(function() {
			 allFirmsCounter++;
		 });
			$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:not(:checked)').each(function() {
			 allFirmsCounterNoChecked++;
		});
		
		if(allFirmsCounterNoChecked == 0){
			$(".individualfirms-first").html('<input type="checkbox" value="selectAllFirmsNewPubs" checked="checked" id="<portlet:namespace/>selectAllFirmsNewPubs" /> Firms ('+allFirmsCounter+') Selected');		
		}else{
			$(".individualfirms-first").html('<input type="checkbox" value="selectAllFirmsNewPubs" id="<portlet:namespace/>selectAllFirmsNewPubs" /> Firms ('+allFirmsCounter+') Selected');
		}
			
			if(allFirmsCounter > 0)
			{
				 $('#<portlet:namespace/>allOtherFirm').prop("checked", true);
				 $('#<portlet:namespace/>allfirms').prop("checked", false);
				 $('#<portlet:namespace/>firm_watchlist').prop("checked", false);
			}			
			else if(allFirmsCounter == 0)
			{
				$('#homePageShowFirms .allFirmsCheckBoxFirmState input[type=checkbox]:checked').removeAttr('checked');
				$('#<portlet:namespace/>allOtherFirm').prop("checked", false);
				var firmTextVal = $('#<portlet:namespace/>Firmstext').val();
				if(firmTextVal == 'All Firms'){
				}else{
					$('#<portlet:namespace/>firm_watchlist').prop("checked", true);
				}
			}
	}
	
	function <portlet:namespace/>changeOnPracticeArea()
	{

		var allPracticeAreaCounter = 0;
		var allPracticeAreaCounterNotChecked = 0;
		$('#<portlet:namespace/>allOtherPracticeArea :checked').each(function() {
		 allPracticeAreaCounter++;
	    });
		
		$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:not(:checked)').each(function() {
			allPracticeAreaCounterNotChecked++;
		});
		
		if(allPracticeAreaCounterNotChecked == 0){
			$("#<portlet:namespace/>individualPracticeAreaCounter").html('<input type="checkbox" checked="checked" value="selectAllPracticeAreasNewPubs" id="<portlet:namespace/>selectAllPracticeAreasNewPubs" /> PracticeArea ('+allPracticeAreaCounter+') Selected');		
		}else{
			$("#<portlet:namespace/>individualPracticeAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreasNewPubs" id="<portlet:namespace/>selectAllPracticeAreasNewPubs" /> PracticeArea ('+allPracticeAreaCounter+') Selected');
		}
		if(allPracticeAreaCounter == 0)
		{
			$('#<portlet:namespace/>individualPracticeAreaCounter').prop("checked", false);
			$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:checked').removeAttr('checked');
			
			$('#<portlet:namespace/>allPracticeAreaIndividualNewsPubs').prop("checked", false);
			$('#<portlet:namespace/>allPracticeArea').prop("checked", true);
		}
		else
		{
			$('#<portlet:namespace/>allPracticeAreaIndividualNewsPubs').prop("checked", true);
		 	$('#<portlet:namespace/>allPracticeArea').prop("checked", false);
		}
		
	}
	
	/*
	 * Invoked when user runs Autocomplete (Type Ahead) on Search box of Firm
	 */
	function <portlet:namespace/>autoSelectFirms(autoSelectedValue)
		{
		
		$('#<portlet:namespace/>popup option:selected').removeAttr("selected"); //fix for NumberFormatexception
		$('#<portlet:namespace/>allOtherFirm').prop("checked", true);
		$('#<portlet:namespace/>allOtherFirmDiv').find('input[value='+autoSelectedValue+']').prop("checked", true);
		
		<portlet:namespace/>applyFirms();
		//Put the selected values in hidden fields
		/* 
		var allVals = [];
	     $('#allOtherFirmDiv :checked').each(function() {
	       allVals.push($(this).val());
	     });
			$("#selectedFirms").val(allVals); */
		}
	
	
	/*
	 * Invoked when user runs Autocomplete (Type Ahead) on Search box of PracticeAreas
	 */
	function <portlet:namespace/>autoSelectPracticeAreas(autoSelectedValue)
	{
		
		$('#<portlet:namespace/>allPracticeArea').prop("checked", false);
		$('#<portlet:namespace/>allOtherPracticeArea').find('input[value="'+autoSelectedValue+'"]').prop("checked", true);
		<portlet:namespace/>applyPracticeArea();
	}
	/*
	 * Invoked when user runs Autocomplete (Type Ahead) on Search box of Content Type
	 */
	function <portlet:namespace/>autoSelectContentType(autoSelectedValue)
	{
		
		if(autoSelectedValue.match('^All Types'))
		{
			$('#<portlet:namespace/>popupContentType').find('input[type=checkbox]:checked').prop("checked", false);
		}
		else
		{
			$('#<portlet:namespace/>popupContentType').find('input[value="All Types"]').prop("checked", false);
		}
	
		$('#<portlet:namespace/>popupContentType').find('input[value="'+autoSelectedValue+'"]').prop("checked", true);
		
		<portlet:namespace/>applyContentType();
	}
	
	/*
	 * Invoked when user runs Autocomplete (Type Ahead) on Search box of Twitter Post Type
	 */
	function <portlet:namespace/>autoSelectTwitterPostType(autoSelectedValue)
	{
		
		if(autoSelectedValue.match('^All'))
		{
			$('#<portlet:namespace/>popupTwitterPostType').find('input[type=checkbox]:checked').prop("checked", false);
		}
		else
		{
			$('#<portlet:namespace/>popupTwitterPostType').find('input[value="All"]').prop("checked", false);
		}
		$('#<portlet:namespace/>popupTwitterPostType').find('input[value="'+autoSelectedValue+'"]').prop("checked", true);
		<portlet:namespace/>applyTwitterType();
	}
	
	//reset Practice Area field value to default
	function <portlet:namespace/>resetPracticeArea()
	{
		//reset practice Area
		$('#<portlet:namespace/>popupPracticeArea').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>allPracticeArea').prop('checked', true);	
		$('#<portlet:namespace/>selectedPracticeArea').val('<%= ALMConstants.ALL_PRACTICE_AREAS %>');
		$("#<portlet:namespace/>individualPracticeAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreasNewPubs" id="<portlet:namespace/>selectAllPracticeAreasNewPubs" /> PracticeArea (0) Selected');
	}
	
	//reset Type of twitter post field value to default
	function  <portlet:namespace/>resetTypeOfTwitterPost()
	{			
		$('#<portlet:namespace/>popupTwitterPostType').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>alltwitterPostType').prop('checked', true);
		$('#<portlet:namespace/>selectedTwitterPostType').val('<%= ALMConstants.ALL %>');
	}
	
	//This would disable additional filter option based on user selection in contect type popup
	//if News is selected : both Practice area and Types of Twitter Posts would be  grreyed out and inactive
	//if Twitter is selected:  Practice Area would be grayed out and inactive
	//if Publications is selected: Types of Twitter Posts would be grayed out and inactive
	function <portlet:namespace/>contentTypeSelection()
	{
		var contentTypeCounter = 0;
		var allValsContentType = [];
				 $('#<portlet:namespace/>allOtherContentType :checked').each(function() {
					 allValsContentType.push($(this).val());
					 contentTypeCounter++;
				    });		 
					
					
						if(contentTypeCounter > 0)
						{
							$('#<portlet:namespace/>allContentType').prop("checked", false);
							if(jQuery.inArray("News", allValsContentType) > -1)
							{
								//content Type is News so both Practice Area and Type of Twitter post would be disabled
								$("#<portlet:namespace/>practiceAreaListItem").attr("disabled", true);
								$("#<portlet:namespace/>practiceAreaId").attr("disabled", true);
								$("#<portlet:namespace/>selectedPracticeArea").attr("disabled", true);
								
								
								$("#<portlet:namespace/>typeOfTwitterPostListItem").attr("disabled", true);
								$("#<portlet:namespace/>selectedTwitterPostType").attr("disabled", true);
								$("#<portlet:namespace/>twitterPostTypeId").attr("disabled", true);

								$('#<portlet:namespace/>practiceAreaListItem').addClass("ui-state-disabled");
								$('#<portlet:namespace/>typeOfTwitterPostListItem').addClass("ui-state-disabled");	
								
								<portlet:namespace/>resetTypeOfTwitterPost();
								<portlet:namespace/>resetPracticeArea();
								
							} else
							{
								$("#<portlet:namespace/>practiceAreaListItem :input").removeAttr("disabled");
								$("#<portlet:namespace/>typeOfTwitterPostListItem :input").removeAttr("disabled");
								
								$('#<portlet:namespace/>practiceAreaListItem').removeClass("ui-state-disabled");
								$('#<portlet:namespace/>typeOfTwitterPostListItem').removeClass("ui-state-disabled");
								
							}
							
							if(jQuery.inArray("Pubs", allValsContentType) > -1)
							{
								$('#<portlet:namespace/>practiceAreaListItem :input').removeAttr("disabled");
								$('#<portlet:namespace/>practiceAreaListItem').removeClass("ui-state-disabled");
								
								if(jQuery.inArray("Twitter", allValsContentType) == -1)
								{
									$("#<portlet:namespace/>typeOfTwitterPostListItem").attr("disabled", true);
									$("#<portlet:namespace/>selectedTwitterPostType").attr("disabled", true);
									$("#<portlet:namespace/>twitterPostTypeId").attr("disabled", true);
									$('#<portlet:namespace/>typeOfTwitterPostListItem').addClass("ui-state-disabled");
									<portlet:namespace/>resetTypeOfTwitterPost();
								}
								
								
							} 
							
							if(jQuery.inArray("Twitter", allValsContentType) > -1)
							{
								$('#<portlet:namespace/>typeOfTwitterPostListItem :input').removeAttr("disabled");
								$('#<portlet:namespace/>typeOfTwitterPostListItem').removeClass("ui-state-disabled");
								
								if(jQuery.inArray("Pubs", allValsContentType) == -1)
								{
									$("#<portlet:namespace/>practiceAreaListItem").attr("disabled", true);
									$("#<portlet:namespace/>practiceAreaId").attr("disabled", true);
									$("#<portlet:namespace/>selectedPracticeArea").attr("disabled", true);									
									$('#<portlet:namespace/>practiceAreaListItem').addClass("ui-state-disabled");
									<portlet:namespace/>resetPracticeArea();
								}
								
								
							} 
						}	
	}
	
	function setPageFirms(goToPage)
	{
		// Sets the page number to the one selected by the user
		// and fires an AJAX submit to refresh with the new page
		// Does NOT tinker with the sort settings
		$("#<portlet:namespace/>goToPage").val(goToPage);
		<portlet:namespace/>search();
	}
	
	function sortResultsFirms(sortColumn)
	{
		// Changes the sort order to the new column selected by
		// the user along with the sort direction (ascending)
		// Also RESETS the page to 1
		
		
		var lastSortColumn  =  $("#<portlet:namespace/>sortColumn").val();
		
		if(lastSortColumn==sortColumn)
		{
			var lastSortOrder= $("#<portlet:namespace/>sortOrder").val();
			if(lastSortOrder=="asc")
			{
				$("#<portlet:namespace/>sortOrder").val("desc");
			}
			else if(lastSortOrder=="desc")
			{
				$("#<portlet:namespace/>sortOrder").val("asc");
			}
			else
			{
				$("#<portlet:namespace/>sortOrder").val("asc");
			}
		}
		else
		{
			$("#<portlet:namespace/>sortOrder").val("asc");
			
		}
		
		$("#<portlet:namespace/>sortColumn").val(sortColumn);
		$("#<portlet:namespace/>goToPage").val(1);
		<portlet:namespace/>search();
	}
	
	
	function <portlet:namespace/>search()
	{
		var isC2VPage = ${isC2VPage};  // its always gauranteed it either evaluates to true or false, but not empty
		var isHomePage = ${isHomePage};
		
		if(isC2VPage)
		{
			
			var actionURL = "${clickToViewURL}" +  "&drilldownFirmName=blah"; // we care the existence of param not its value;
			$('#firmSearchModelBean').attr("action", actionURL);
			//$('#firmSearchModelBean').attr("name", "firmSearchModelBean_c2v");
		}
		
		if(isHomePage)
		{
			<portlet:namespace/>ajaxPersist(); // ajaxSubmit for Charts on HomePage
		}
		else
		{
			Liferay.Portlet.showBusyIcon("#bodyId","Loading..."); // show Busy Icon
			
			$('#firmSearchModelBean').submit(); // normal form Submit 
		}
	}
	
	
	/**
	* Persist the search Criteria for Chart on Home Page
	*/
	function <portlet:namespace/>ajaxPersist()
	{
		window.parent.Liferay.Portlet.showBusyIcon("#bodyId","Loading...");
		 $.ajax({
			    url: "${persistSearchCriteriaForChartURL}",
			    method: "POST",
			    traditional: true,
			    data: $('#firmSearchModelBean').serializeObject(),
			    success: function(data){
			    	
			    	window.parent.Liferay.Portlet.hideBusyIcon("#chartSearchPortletPopupId"); // hide popup loading icon
			    	window.parent.location.href = "home"; // refresh home page
			    	window.parent.Liferay.Portlet.showBusyIcon("#bodyId","Loading..."); // show the loading icon
			    	window.parent.AUI().DialogManager.closeByChild('#chartSearchPortletPopupId'); // close the popup
			   		
			    	
			   	
			    	//	Liferay.Portlet.refreshPortlet('#p_p_id_${chartPortletId}_'); 
			        
			    },
			    error: function(jqXHR, textStatus, errorThrown) {
			    	window.parent.Liferay.Portlet.hideBusyIcon("#chartSearchPortletPopupId");
			         	 alert("error:" + textStatus + " - exception:" + errorThrown);
			   		}
			    }); 
	}
	
	
</script>

<script>

function toggleViewSettingsNewsPubs(vsId)
{
	
	//get the position of the placeholder element
   var vsPos   = $(vsId).offset();
   var vsHeight = $(vsId).height();
   var vsWidth = $(vsId).width();
   var popupWidth = $("#<portlet:namespace/>view_settings").width();
   
    //show the menu directly over the placeholder
   $("#<portlet:namespace/>view_settings").css({  position: "absolute", "left": (vsPos.left - popupWidth + vsWidth) + "px", "top":(vsPos.top + vsHeight)  + "px" });
   $("#<portlet:namespace/>view_settings").toggle();
}

function <portlet:namespace/>cancelViewSettings()
{
	 $("#<portlet:namespace/>view_settings").hide();
}
function <portlet:namespace/>applyViewSettings()
{
	$("#<portlet:namespace/>view_settings").toggle();
	<portlet:namespace/>search();
}

function <portlet:namespace/>resetViewSettings()
{
	$('input:radio[name=orderBy]:nth(0)').prop('checked',true);  //news_pub_id, the 1st radiobutton
	$('input:radio[name=searchResultsPerPage]:nth(2)').prop('checked',true);  //100, the 3rd radiobutton
	//$("#view_settings").toggle(); // dont close the popup, give a chance to end user to know what the default values are let him click apply
}


$.widget( "custom.catcomplete", $.ui.autocomplete, {
	_renderMenu: function( ul, items ) {
		var that = this,
		currentCategory = "";
		$.each( items, function( index, item ) {
			if ( item.category != currentCategory ) {
			ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
			currentCategory = item.category;
			}
	  that._renderItemData( ul, item );
	});
}
});

function <portlet:namespace/>ValidateDate(txtDate)
{
    var currVal = txtDate;
    if(currVal == '')
        return false;
    
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?
    
    if (dtArray == null) 
        return false;
    
    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[1];
    dtDay= dtArray[3];
    dtYear = dtArray[5];        
    
    if (dtMonth < 1 || dtMonth > 12) 
        return false;
    else if (dtDay < 1 || dtDay> 31) 
        return false;
    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
        return false;
    else if (dtMonth == 2) 
    {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay> 29 || (dtDay ==29 && !isleap)) 
                return false;
    }
    return true;
}

/* $(function() 
{}); */
</script>

<script>

$(document).ready(function(){	

	
	$("#firmsPrintChart").click(function(){
		var printChartList = [];
		if(amountNewsPubsChart != null){
			printChartList.push(amountNewsPubsChart);
		}
		if(numberOfPubsPracticeAreaChart != null){
			printChartList.push(numberOfPubsPracticeAreaChart);
		}
		if(percentNewsPubs != null){
			printChartList.push(percentNewsPubs);
		}
	});
	
	$("#firmSearchModelBean input, #firmSearchModelBean select, #firmSearchModelBean span").each(function(){
		
		var idVal = $(this).attr("id");
		 $(this).attr("id","<portlet:namespace/>" + idVal );
	});

	$("input[class='dummyclass']").change(function(){
		
		var checkedContentTypeCount = 0;
		
		$('#<portlet:namespace/>contentTypeSelectDiv input[type=checkbox]:checked').each(function() {
			checkedContentTypeCount++;
		 });
		
		if(checkedContentTypeCount == 0){
			$("#<portlet:namespace/>allContentType").prop("checked", true);
			$("#<portlet:namespace/>contentType").val('All Types');
		}
	});
	
	var data = ${firmJson};
	var practiceData = ${practiceJson};
    var keywords = ${keywordJson};
    var contentTypeData = ${contentTypeData};
    var twitterPostTypeData = ${twitterPostTypeData}; 
	
	function split( val ) 
	{
		return val.split( /,\s*/ );
	}

	function extractLast( term ) 
	{
		return split( term ).pop();
	}

	//Auto Complete for keywords
	$( "#<portlet:namespace/>keywords" ).autocomplete({
		  source: function( request, response ) {
		    var matches = $.map( keywords, function(tag) {
		      if ( tag.toUpperCase().indexOf(request.term.toUpperCase()) === 0 ) {
		        return tag;
		      }
		    });
		    response(matches);
		  }
	});
	
	
	//Auto Complete for Content Type
	$( "#<portlet:namespace/>contentType" )
	//don't navigate away from the field on tab when selecting an item
	.bind( "keydown", function( event ) 
	{
		if ( event.keyCode === $.ui.keyCode.TAB && $( this ).data( "ui-autocomplete" ).menu.active ) 
		{
			event.preventDefault();
		}
	})
	.autocomplete({minLength: 3, source: function( request, response ) 
	{
		//delegate back to autocomplete, but extract the last term
		response( $.ui.autocomplete.filter(contentTypeData, extractLast( request.term )));
	}, 
	focus: function() 
	{
		//prevent value inserted on focus
		return false;
	},
	select: function( event, ui ) 
	{
		var terms = split( this.value );
		//remove the current input
		terms.pop();
		//add the selected item
		terms.push( ui.item.value );
		// auto select the corresponding checkbox
		<portlet:namespace/>autoSelectContentType(ui.item.id);
		//add placeholder to get the comma-and-space at the end
		terms.push( "" );
		this.value = terms.join( ", " );
		return false;
	}
});
	
	//Auto Complete for Twitter Type
	$( "#<portlet:namespace/>selectedTwitterPostType")
	//don't navigate away from the field on tab when selecting an item
	.bind( "keydown", function( event ) 
	{
		if ( event.keyCode === $.ui.keyCode.TAB && $( this ).data( "ui-autocomplete" ).menu.active ) 
		{
			event.preventDefault();
		}
	})
	.autocomplete({minLength: 3, source: function( request, response ) 
	{
		//delegate back to autocomplete, but extract the last term
		response( $.ui.autocomplete.filter(twitterPostTypeData, extractLast( request.term )));
	}, 
	focus: function() 
	{
		//prevent value inserted on focus
		return false;
	},
	select: function( event, ui ) 
	{
		var terms = split( this.value );
		//remove the current input
		terms.pop();
		//add the selected item
		terms.push( ui.item.value );
		// auto select the corresponding checkbox
		<portlet:namespace/>autoSelectTwitterPostType(ui.item.value);
		//add placeholder to get the comma-and-space at the end
		terms.push( "" );
		this.value = terms.join( ", " );
		return false;
	}
});
	
	//Auto Complete for Practice Area	
	$( "#<portlet:namespace/>selectedPracticeArea" )
		//don't navigate away from the field on tab when selecting an item
		.bind( "keydown", function( event ) 
		{
			if ( event.keyCode === $.ui.keyCode.TAB && $( this ).data( "ui-autocomplete" ).menu.active ) 
			{
				event.preventDefault();
			}
		})
		.autocomplete({minLength: 3, source: function( request, response ) 
		{
			//delegate back to autocomplete, but extract the last term
			response( $.ui.autocomplete.filter(practiceData, extractLast( request.term )));
		}, 
		focus: function() 
		{
			//prevent value inserted on focus
			return false;
		},
		select: function( event, ui ) 
		{
			var terms = split( this.value );
			//remove the current input
			terms.pop();
			//add the selected item
			terms.push( ui.item.value );
			// auto select the corresponding checkbox
			<portlet:namespace/>autoSelectPracticeAreas(ui.item.value);
			//add placeholder to get the comma-and-space at the end
			terms.push( "" );
			this.value = terms.join( ", " );
			return false;
		}
	});


$( "#<portlet:namespace/>Firmstext" ).catcomplete({
	minLength: 3,
	source: function( request, response ) {
		// delegate back to autocomplete, but extract the last term
		response( $.ui.autocomplete.filter(
		data, extractLast( request.term ) ) );
	},
	focus: function() {
		// prevent value inserted on focus
		return false;
	},	
		change: function(event, ui) {
			
		$('#<portlet:namespace/>popup').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>popup').find('input[type=radio]:checked').removeAttr('checked');
		
		var selectedValues = this.value.split(',');
		var output = '';
		var currentvalue="";		
		$.each(selectedValues, function(key, line) {
			line=line.trim();		
			if(line!=""){				
				currentvalue=$("input[mylabel='"+line+"'].allFirmsCheckBoxFirmState").val();
				if(typeof(currentvalue)!="undefined"){
					output= output + currentvalue+ ",";
					$("input[mylabel='"+line+"'].allFirmsCheckBoxFirmState").prop("checked", true);
					<portlet:namespace/>changeOnFirms();
				}	
							
			}			
		});		
		output = output.substring(0, output.length - 1);
		$('#<portlet:namespace/>selectedFirms').val(output);		
    },
	select: function( event, ui ) {
		
		var terms = split( this.value );
		var termsValue = split( this.id );
		if( ui.item.value.indexOf("AmLaw 100") !== -1){
			return false;
		}
		
		// remove the current input
		terms.pop();
		termsValue.pop();
		// add the selected item
		
		terms.push( ui.item.value );
		termsValue.push( ui.item.id );
		
		
		<portlet:namespace/>autoSelectFirms(ui.item.id);
		$('#<portlet:namespace/>selectedFirms').val($('#<portlet:namespace/>selectedFirms').val().replace("AmLaw 100","") + "," + ui.item.id);

		// add placeholder to get the comma-and-space at the end
		terms.push( "" );
		termsValue.push( "" );
		this.value = terms.join( ", " );
		
		
		// auto select the corresponding checkbox
		
		/*var n=$('#selectedFirms').val().indexOf(",");
		if(n==0){
			var selectedFirmsString=$('#selectedFirms').val();
			$('#selectedFirms').val(selectedFirmsString.substring(1,selectedFirmsString.length));
		}*/
	
		return false;
	}
});
	
	$('#<portlet:namespace/>fromdateError').hide();
	$('#<portlet:namespace/>todateError').hide();
	$('#<portlet:namespace/>dateValidError').hide();
	
	$("#<portlet:namespace/>popupDate").hide(); 

	
	$( "#<portlet:namespace/>from" ).datepicker({
		changeMonth: true,
		changeYear: true,
		showOn: "button",
		buttonImage: "/re-firm-portlet/images/calendar.gif",
		buttonImageOnly: true,
		defaultDate: "+1w",
		onClose: function( selectedDate ) {
			$( "#<portlet:namespace/>to" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	
	$( "#<portlet:namespace/>to" ).datepicker({
		changeMonth: true,
		changeYear: true,
		showOn: "button",
		buttonImage: "/re-firm-portlet/images/calendar.gif",
		buttonImageOnly: true,
		defaultDate: "+1w",
		onClose: function( selectedDate ) {
			$( "#<portlet:namespace/>from" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
	

/* $("input[class='allFirmsCheckBoxFirmState']").click(function(){
	
	<portlet:namespace/>changeOnFirms();
			
}); */

$("#<portlet:namespace/>allOtherPracticeArea input[class='allPracticeAreaCheckBoxCounter']").click(function(){
	<portlet:namespace/>changeOnPracticeArea();
			
});

$('#<portlet:namespace/>hide, #<portlet:namespace/>popup').click(function(e){
	 $("#<portlet:namespace/>popupContentType").hide();
	 $("#<portlet:namespace/>popupDate").hide();
	 $("#<portlet:namespace/>popupPracticeArea").hide();
	 $("#<portlet:namespace/>popupTwitterPostType").hide();
    e.stopPropagation();
   
});  

$('#<portlet:namespace/>contentTypeId, #<portlet:namespace/>popupContentType').click(function(e){
	 $("#<portlet:namespace/>popup").hide(); 
	 $("#<portlet:namespace/>popupDate").hide();
	 $("#<portlet:namespace/>popupPracticeArea").hide();
	 $("#<portlet:namespace/>popupTwitterPostType").hide();
    e.stopPropagation();   
});


$('#<portlet:namespace/>datenone, #<portlet:namespace/>popupDate').click(function(e){
	 $("#<portlet:namespace/>popup").hide(); 
	 $("#<portlet:namespace/>popupContentType").hide();
	 $("#<portlet:namespace/>popupPracticeArea").hide();
	 $("#<portlet:namespace/>popupTwitterPostType").hide();
   e.stopPropagation();   
});  

$('#ui-datepicker-div').click(function(e){
	 $("#<portlet:namespace/>popup").hide(); 
	 $("#<portlet:namespace/>popupContentType").hide();
	 $("#<portlet:namespace/>popupPracticeArea").hide();
	 $("#<portlet:namespace/>popupTwitterPostType").hide();
   e.stopPropagation();   
});

$('#<portlet:namespace/>practiceAreaId, #<portlet:namespace/>popupPracticeArea').click(function(e){
	 $("#<portlet:namespace/>popup").hide(); 
	 $("#<portlet:namespace/>popupContentType").hide();
	 $("#<portlet:namespace/>popupDate").hide();
	 $("#<portlet:namespace/>popupTwitterPostType").hide();
  	 e.stopPropagation();   
});


$('#<portlet:namespace/>twitterPostTypeId, #<portlet:namespace/>popupTwitterPostType').click(function(e){
	  $("#<portlet:namespace/>popup").hide(); 
	  $("#<portlet:namespace/>popupContentType").hide();
	  $("#<portlet:namespace/>popupDate").hide();
	  $("#<portlet:namespace/>popupPracticeArea").hide();	
	 e.stopPropagation();   
});


$(document).click(function(){
	
	var firmstextVar = $("#<portlet:namespace/>Firmstext").val();
	
	if(firmstextVar == '' || firmstextVar == 'undefined' || firmstextVar == null){
		<portlet:namespace/>applyFirms();
	}
	
	var contentTypeVar = $("#<portlet:namespace/>contentType").val();
	if(contentTypeVar == '' || contentTypeVar == 'undefined' || contentTypeVar == null){
		$("#<portlet:namespace/>allContentType").prop("checked", true);
		$("#<portlet:namespace/>contentType").val('All Types');
	}
	
	var selectedPracticeAreaVar = $("#<portlet:namespace/>selectedPracticeArea").val();
	if(selectedPracticeAreaVar == '' || selectedPracticeAreaVar == 'undefined' || selectedPracticeAreaVar == null){
		<portlet:namespace/>applyPracticeArea();
	}
	
	var selectedTwitterPostTypeVar = $("#<portlet:namespace/>selectedTwitterPostType").val();
	if(selectedTwitterPostTypeVar == '' || selectedTwitterPostTypeVar == 'undefined' || selectedTwitterPostTypeVar == null){
		$("#<portlet:namespace/>selectedTwitterPostType").val('All');
		$("#<portlet:namespace/>alltwitterPostType").prop("checked", true);
		$("#<portlet:namespace/>allOthertwitterPostType").find('input[type=checkbox]').prop("checked", false);
		
	}
	
    $("#<portlet:namespace/>popup").hide(); 
    $("#<portlet:namespace/>popupContentType").hide();
    $("#<portlet:namespace/>popupDate").hide();
    $("#<portlet:namespace/>popupPracticeArea").hide();
    $("#<portlet:namespace/>popupTwitterPostType").hide();
	
});
 
$('#homePageShowFirms .individualfirms-Watchlist-Firm').click(function() {
	
	var checked = $("#<portlet:namespace/>selectAllWatchList").is(":checked");
	var allWatchListCounter = 0;
	var watchlistCounter = 0;
	var isFlag = true;
	
	if(checked){ 
		 $('#<portlet:namespace/>individualfirmsWatchListDIVFirm .allFirmsCheckBoxWatchListFirm').prop('checked', true);
		 $('#<portlet:namespace/>selectAllWatchList').prop("checked", true);
		 $('#<portlet:namespace/>firm_watchlist').prop("checked", true);
		 $('#<portlet:namespace/>allfirms').prop("checked", false);
		 $('#<portlet:namespace/>allOtherFirm').prop("checked", false);
		 $('#homePageShowFirms .rivaledgeListAMLAW_100_Firm').prop("checked", false);
		 $('#homePageShowFirms .rivaledgeListAMLAW_200_Firm').prop("checked", false);
		 $('#homePageShowFirms .rivaledgeListNLJ_250_Firm').prop("checked", false);
		 $('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
		 $('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
		 $('#<portlet:namespace/>selectAllFirmsNewPubs').prop("checked", false);
	}else{
		 $('#<portlet:namespace/>individualfirmsWatchListDIVFirm input[type=checkbox]:checked').removeAttr('checked');
		 $('#<portlet:namespace/>selectAllWatchList').prop("checked", false);
		 
		 var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
			
			$('#<portlet:namespace/>individualfirmsWatchListDIVFirm input[type=checkbox]').each(function() {
				 if(this.value == watchListId)
					{
						$(this).prop("checked", true);
						isFlag = false;
					}
			});
			$('#<portlet:namespace/>firm_watchlist').prop("checked", true);
			if(isFlag == true)
			{
				$("#homePageShowFirms .rivaledgeListAMLAW_100_Firm").prop("checked", true);
			}
	}
	
	$('#<portlet:namespace/>individualfirmsWatchListDIVFirm input[type=checkbox]:checked').each(function() {
		 allWatchListCounter++;
	});
	$('#<portlet:namespace/>individualfirmsWatchListDIVFirm input[type=checkbox]:not(:checked)').each(function() {
		watchlistCounter++;
	});
	if(watchlistCounter == 0)
	{
		$("#homePageShowFirms .individualfirms-Watchlist-Firm").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" checked="true"/> Watchlists ('+allWatchListCounter+') Selected');
	}
	else
	{
		$("#homePageShowFirms .individualfirms-Watchlist-Firm").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList"/> Watchlists ('+allWatchListCounter+') Selected');
	}
	<portlet:namespace/>applyFirms();
});

//Function for showing selected Watchlist and their counter for firms drop down
$("input[class='allFirmsCheckBoxWatchListFirm']").change(function(){
				
	 var allWatchListCounter = 0;
	 var watchlistCounter = 0;
	 $("#homePageShowFirms .rivaledgeListAMLAW_100_Firm").prop("checked", false);
	 $('#homePageShowFirms .rivaledgeListAMLAW_200_Firm').prop("checked", false);
	 $('#homePageShowFirms .rivaledgeListNLJ_250_Firm').prop("checked", false);
	 
	 $('#<portlet:namespace/>individualfirmsWatchListDIVFirm input[type=checkbox]:checked').each(function() {
		 allWatchListCounter++;
	 });
		
	 $('#<portlet:namespace/>individualfirmsWatchListDIVFirm input[type=checkbox]:not(:checked)').each(function() 
		{
			watchlistCounter++;
		});
			 
	 if(watchlistCounter == 0)
	 {
		$("#homePageShowFirms .individualfirms-Watchlist-Firm").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" checked="true"/> Watchlists ('+allWatchListCounter+') Selected');
	 }
	 else
	 {
		$("#homePageShowFirms .individualfirms-Watchlist-Firm").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList"/> Watchlists ('+allWatchListCounter+') Selected');
	 }
	 if(allWatchListCounter == 0)
	 {
		 $('#homePageShowFirms .rivaledgeListAMLAW_100_Firm').prop("checked", true);
	 }
	 <portlet:namespace/>applyFirms();
});


// Get Current Date and previous week Date
	var date = new Date();
 	currentDate = (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear();
	date.setDate(date.getDate() - 7);
 	lastWeekDate = (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear();

$('#<portlet:namespace/>btnAdd').click(function(){
	$('#homePageShowFirms .filtersPage').hide();
	$('#<portlet:namespace/>additional').show()
})
$('#<portlet:namespace/>btnSave').click(function(){
	$('#homePageShowFirms .filtersPage').show();
	$('#<portlet:namespace/>additional').hide()
});


$('#<portlet:namespace/>hide').click(
    function () {
    	//show its submenu     
        $("#<portlet:namespace/>popup").stop().slideToggle(500);    
    });

	$('#<portlet:namespace/>contentTypeId').click(
    function () {
        //show its submenu        
        $("#<portlet:namespace/>popupContentType").stop().slideToggle(500);    
    });

	$('#<portlet:namespace/>datenone').click(
function () {
    //show its submenu
    $("#<portlet:namespace/>popupDate").stop().slideToggle(500);    
}); 

$('#<portlet:namespace/>practiceAreaId').click(
function () {
    //show its submenu
    $("#<portlet:namespace/>popupPracticeArea").stop().slideToggle(500);    
});  

$('#<portlet:namespace/>twitterPostTypeId').click(
		function () {
		    //show its submenu
		    $("#<portlet:namespace/>popupTwitterPostType").stop().slideToggle(500);    
	});

/** Firms Selection logic*/ 
//This gets trigger when user clicks on All firms radio button from firms popup and 
//would uncheck all other  option apart from All firms radio button in the popup.
$("input[id='<portlet:namespace/>allfirms']").change(function(){
	var checked = $("#<portlet:namespace/>allfirms").is(":checked");
	if(checked){		 
	 $('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
	 $('#<portlet:namespace/>individualfirmsWatchListFirm').find('input[type=checkbox]:checked').removeAttr('checked');
	 $('#<portlet:namespace/>individualfirmsWatchListDIVFirm').find('input[type=checkbox]:checked').removeAttr('checked');
	 
	 $("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.ALL_FIRMS %>');
	 $("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.ALL_FIRMS %>');

	}
	
	$("#homePageShowFirms .individualfirms-first").html('<input type="checkbox" value="selectAllFirmsNewPubs" id="<portlet:namespace/>selectAllFirmsNewPubs" /> Firms (0) Selected');
});

/* $("input[id='<portlet:namespace/>RivalEdgeList']").change(function(){
	var checked = $("#<portlet:namespace/>RivalEdgeList").is(":checked");
	if(checked){   
	$("#<portlet:namespace/>Select1").removeAttr('disabled');		 
	 $('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
	 <portlet:namespace/>applyFirms();
	}else{
	 $("#<portlet:namespace/>Select1").attr("disabled", true);
	}
});
 */

/* $("#<portlet:namespace/>Select1").change(function(){		
	var count = $("#<portlet:namespace/>Select1 :selected").length;
	if(count>0){				 
	 $('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
	 $('#<portlet:namespace/>RivalEdgeList').prop("checked", true);
	 <portlet:namespace/>applyFirms();
	}
});  */

$("input[id='<portlet:namespace/>allOtherFirm']").change(function(){
	var checked = $("#<portlet:namespace/>allOtherFirm").is(":checked");

	if(checked){	 
	   $('#<portlet:namespace/>popup option:selected').removeAttr("selected");
	   
	}else{
	 $("#<portlet:namespace/>allOtherFirmDiv input").attr("disabled", true);
	}
	/*
	$('#<portlet:namespace/>ApplyFirm').attr("disabled", true);
	$('#<portlet:namespace/>ApplyFirm').addClass("ui-state-disabled");
	*/
});

/* $("input[class='allFirmsCheckBoxFirmState']").change(function(){
	
	var lengthofCheckBoxes = $("input[class='allFirmsCheckBoxFirmState']:checked").length;
	//alert('INSIDE <portlet:namespace/>allFirmsCheckBoxCounter input[type=checkbox] change function lengthofCheckBoxes is '+lengthofCheckBoxes);
	if(lengthofCheckBoxes>0){
	$('#<portlet:namespace/>ApplyFirm').removeAttr("disabled");
	$('#<portlet:namespace/>ApplyFirm').removeClass("ui-state-disabled");
	$('#<portlet:namespace/>allOtherFirm').prop("checked", true);
	//$('#<portlet:namespace/>Select1').find("option").attr("selected", false);
	<portlet:namespace/>applyFirms();
	} /*else{
	$('#<portlet:namespace/>ApplyFirm').attr("disabled", true);
	$('#<portlet:namespace/>ApplyFirm').addClass("ui-state-disabled");
	}*/

/*}); */


/** ContentType Selection logic*/ 
$("input[id='<portlet:namespace/>allContentType']").change(function(){
	 var checked = $("#<portlet:namespace/>allContentType").is(":checked");
	 if(checked){   
	  $('#<portlet:namespace/>allOtherContentType').find('input[type=checkbox]:checked').removeAttr('checked');
	  $('#<portlet:namespace/>practiceAreaListItem :input').removeAttr("disabled");
	  $('#<portlet:namespace/>typeOfTwitterPostListItem :input').removeAttr("disabled");
	  
	  $('#<portlet:namespace/>practiceAreaListItem').removeClass("ui-state-disabled");
	  $('#<portlet:namespace/>typeOfTwitterPostListItem').removeClass("ui-state-disabled");
	  
	  
	 } 
	});
	
$('#homePageShowFirms .rivaledgeListAMLAW_100_Firm').bind('click', function (event) {
	 $('#homePageShowFirms .rivaledgeListAMLAW_100_Firm').prop("checked", true);
	 $('#homePageShowFirms .rivaledgeListAMLAW_200_Firm').prop("checked", false);
	 $('#homePageShowFirms .rivaledgeListNLJ_250_Firm').prop("checked", false);
	 $('#<portlet:namespace/>firm_watchlist').prop("checked", true);
	 $('#<portlet:namespace/>individualfirmsWatchListDIVFirm input[type=checkbox]:checked').removeAttr('checked');
	 $("#homePageShowFirms .individualfirms-Watchlist-Firm").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0 Selected)');
	
	$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirmsNewPubs" id="<portlet:namespace/>selectAllFirmsNewPubs" /> Firms (0) Selected');
	$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
	
	$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.AMLAW_100 %>');
	$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.AMLAW_100 %>');
	$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
});	

$('#homePageShowFirms .rivaledgeListAMLAW_200_Firm').bind('click', function (event) {
	 $('#homePageShowFirms .rivaledgeListAMLAW_100_Firm').prop("checked", false);
	 $('#homePageShowFirms .rivaledgeListAMLAW_200_Firm').prop("checked", true);
	 $('#homePageShowFirms .rivaledgeListNLJ_250_Firm').prop("checked", false);
	 $('#<portlet:namespace/>firm_watchlist').prop("checked", true);
	 $('#<portlet:namespace/>individualfirmsWatchListDIVFirm input[type=checkbox]:checked').removeAttr('checked');
	 $("#homePageShowFirms .individualfirms-Watchlist-Firm").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0 Selected)');
	
	$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirmsNewPubs" id="<portlet:namespace/>selectAllFirmsNewPubs" /> Firms (0) Selected');
	$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
	
	$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.AMLAW_200 %>');
	$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.AMLAW_200 %>');
	$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
});	

$('#homePageShowFirms .rivaledgeListNLJ_250_Firm').bind('click', function (event) {
	 $('#homePageShowFirms .rivaledgeListAMLAW_100_Firm').prop("checked", false);
	 $('#homePageShowFirms .rivaledgeListAMLAW_200_Firm').prop("checked", false);
	 $('#homePageShowFirms .rivaledgeListNLJ_250_Firm').prop("checked", true);
	 $('#<portlet:namespace/>firm_watchlist').prop("checked", true);
	 $('#<portlet:namespace/>individualfirmsWatchListDIVFirm input[type=checkbox]:checked').removeAttr('checked');
	 $("#homePageShowFirms .individualfirms-Watchlist-Firm").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0 Selected)');
	
	$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirmsNewPubs" id="<portlet:namespace/>selectAllFirmsNewPubs" /> Firms (0) Selected');
	$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
	
	$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.NLJ_250 %>');
	$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.NLJ_250 %>');
	$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
});	

$('#homePageShowFirms .allFirmsCheckBoxWatchListFirm').click(function() {
	
	 $('#<portlet:namespace/>firm_watchlist').prop("checked", true);
	 $('#<portlet:namespace/>allfirms').prop("checked", false);
	 $('#<portlet:namespace/>allOtherFirm').prop("checked", false);
	 
	 $('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
	 
	 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirmsNewPubs" id="<portlet:namespace/>selectAllFirmsNewPubs" /> Firms (0 Selected)');
	 
	 $('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
	 $('#homePageShowFirms .rivaledgeListAMLAW_100_Firm').prop("checked", false);
	 $('#homePageShowFirms .rivaledgeListAMLAW_200_Firm').prop("checked", false);
	 $('#homePageShowFirms .rivaledgeListNLJ_250_Firm').prop("checked", false);
	 
	});

$('#<portlet:namespace/>individualFirmsCounter').click(function() {
	 
	var checked = $("#<portlet:namespace/>selectAllFirmsNewPubs").is(":checked");
	var allFirmsListCounter = 0;
	
	if(checked)
	{   
		 $('#<portlet:namespace/>allOtherFirmDiv .allFirmsCheckBoxFirmState').prop('checked', true);
	}
	else
	{
		$('#<portlet:namespace/>allOtherFirmDiv .allFirmsCheckBoxFirmState').prop('checked', false);
	}
	
	 $('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
		 allFirmsListCounter++;
	 });
	 
	 if(allFirmsListCounter == 0)
		 {
			 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirmsNewPubs" id="<portlet:namespace/>selectAllFirmsNewPubs"/> Firms ('+allFirmsListCounter+') Selected');
			 $('#<portlet:namespace/>allOtherFirm').prop("checked", false);
			 $('#<portlet:namespace/>selectAllFirmsNewPubs').prop("checked", false);
			 
				var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
				
				$('#<portlet:namespace/>individualfirmsWatchListDIVFirm input[type=checkbox]').each(function() {
					 if(this.value == watchListId)
						{
							$(this).prop("checked", true);
						}
				});
				$('#<portlet:namespace/>firm_watchlist').prop("checked", true);
		 }
	 else
		 {
			 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirmsNewPubs" id="<portlet:namespace/>selectAllFirmsNewPubs"/> Firms ('+allFirmsListCounter+') Selected');
			 $('#<portlet:namespace/>allOtherFirm').prop("checked", true);
			 $('#<portlet:namespace/>selectAllFirmsNewPubs').prop("checked", true);
			 $('#<portlet:namespace/>individualfirmsWatchListDIVFirm input[type=checkbox]:checked').removeAttr('checked');
			 $('#<portlet:namespace/>firm_watchlist').prop("checked", false);
			 $("#homePageShowFirms .rivaledgeListAMLAW_100_Firm").prop("checked", false);
			 $("#homePageShowFirms .rivaledgeListAMLAW_200_Firm").prop("checked", false);
			 $("#homePageShowFirms .rivaledgeListNLJ_250_Firm").prop("checked", false);
		 }
	 <portlet:namespace/>applyFirms();
});

$('#<portlet:namespace/>allPracticeAreaIndividualNewsPubs').click(function() {
	$('#<portlet:namespace/>allPracticeArea').prop("checked", false);
}); 

$('#<portlet:namespace/>individualPracticeAreaCounter').click(function() {
	 
	var checked = $("#<portlet:namespace/>selectAllPracticeAreasNewPubs").is(":checked");
	var allPracticeAreasListCounter = 0;
	
	if(checked)
	{   
		 $('#<portlet:namespace/>allOtherPracticeArea .allPracticeAreaCheckBoxCounter').prop('checked', true);
		 //$('#practiceArea1').prop('checked', false);
	}
	else
	{
		$('#<portlet:namespace/>allOtherPracticeArea .allPracticeAreaCheckBoxCounter').prop('checked', false);
	}
	
	 $('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:checked').each(function(){
		 allPracticeAreasListCounter++;
	 });
	 
	 if(allPracticeAreasListCounter == 0)
		 {
			 $("#<portlet:namespace/>individualPracticeAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreasNewPubs" id="<portlet:namespace/>selectAllPracticeAreasNewPubs" /> Practice Areas ('+allPracticeAreasListCounter+') Selected');
			 $('#<portlet:namespace/>allOtherPracticeArea').prop("checked", false);
			 $('#<portlet:namespace/>selectAllPracticeAreasNewPubs').prop("checked", false);
			 //$('.allPracticeArea').prop("checked", true);
			 $('#<portlet:namespace/>selectedPracticeArea').val('<%=ALMConstants.ALL_PRACTICE_AREAS %>');
			 $('#<portlet:namespace/>allPracticeAreaIndividualNewsPubs').prop("checked", false);
			  $('#<portlet:namespace/>allPracticeArea').prop("checked", true);

		 }
	 else
		 {
			 $("#<portlet:namespace/>individualPracticeAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreasNewPubs" id="<portlet:namespace/>selectAllPracticeAreasNewPubs" /> Practice Areas  ('+allPracticeAreasListCounter+') Selected');
			 $('#<portlet:namespace/>allOtherPracticeArea').prop("checked", true);
			 $('#<portlet:namespace/>selectAllPracticeAreasNewPubs').prop("checked", true);
			  $('#<portlet:namespace/>allPracticeAreaIndividualNewsPubs').prop("checked", true);
			  $('#<portlet:namespace/>allPracticeArea').prop("checked", false);
			  
		 }
	 <portlet:namespace/>applyPracticeArea();
}); 

$("input[class='allFirmsCheckBoxFirmState']").change(function(){
	
	<portlet:namespace/>changeOnIndividualFirms();
	<portlet:namespace/>applyFirms();
});

	function <portlet:namespace/>changeOnIndividualFirms()
{
	var allFirmsCounter = 0;
	 $('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
		 allFirmsCounter++;
	    });
		
		$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirmsNewPubs" id="<portlet:namespace/>selectAllFirmsNewPubs" /> Firms ('+allFirmsCounter+') Selected');
		if(allFirmsCounter == 0)
		{
			$('#<portlet:namespace/>firm_watchlist').prop("checked", true);
			$('#<portlet:namespace/>allOtherFirm').prop("checked", false);
			$('#<portlet:namespace/>allfirms').prop("checked", false);
			
			$('#<portlet:namespace/>individualfirmsWatchListFirm input[type=checkbox]:checked').removeAttr('checked');
			$('#<portlet:namespace/>individualFirmsCounter input[type=checkbox]:checked').removeAttr('checked');

			var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
			
			$('#<portlet:namespace/>individualfirmsWatchListDIVFirm input[type=checkbox]').each(function() {
				 if(this.value == watchListId)
					{
						$(this).prop("checked", true);
					}
			});
		}
	else
		{
			$('#<portlet:namespace/>allOtherFirm').prop("checked", true);
			$('#<portlet:namespace/>firm_watchlist').prop("checked", false);
			$('#<portlet:namespace/>allfirms').prop("checked", false);
			
			$('#<portlet:namespace/>individualfirmsWatchListFirm input[type=checkbox]:checked').removeAttr('checked');
			$("#homePageShowFirms .individualfirms-Watchlist-Firm").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0) Selected');
			$('#<portlet:namespace/>individualfirmsWatchListDIVFirm input[type=checkbox]:checked').removeAttr('checked');
			
		}
}
	
$(".dummyclass").click(function(){			
		<portlet:namespace/>contentTypeSelection();	
	});
	
	
$("input[name='contentType']").click(function(){
	 var checked = $("#<portlet:namespace/>allOtherContentType").find('input[type=checkbox]:checked').is(":checked");
	 if(checked){   
	  $("input[id='<portlet:namespace/>allContentType']").removeAttr("checked");
	  <portlet:namespace/>applyContentType();
	 }
	});

/** Twitter Type Selection logic */ 
$("input[id='<portlet:namespace/>alltwitterPostType']").change(function(){
	var checked = $(this).is(":checked");
	if(checked){   
		$('#<portlet:namespace/>allOthertwitterPostType').find('input[type=checkbox]').removeAttr('checked');
		<portlet:namespace/>applyTwitterType();
	} 
	
});

$('#<portlet:namespace/>allOthertwitterPostType').find('input[type=checkbox]').click(function(){
	 var checked = $(this).is(":checked");
	 if(checked){   
	  $("input[id='<portlet:namespace/>alltwitterPostType']").removeAttr("checked");
	  <portlet:namespace/>applyTwitterType();
	 }
	});

/** Practice Area Selection logic */ 
$("input[id='<portlet:namespace/>allPracticeArea']").change(function(){
	var checked = $(this).is(":checked");
	if(checked){   
		$('#<portlet:namespace/>allOtherPracticeArea').find('input[type=checkbox]:checked').removeAttr('checked');
		<portlet:namespace/>applyPracticeArea();
	} 
	
	$('#<portlet:namespace/>individualPracticeAreaCounter').prop("checked", false);
	$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:checked').removeAttr('checked');
	$("#<portlet:namespace/>individualPracticeAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreasNewPubs" id="<portlet:namespace/>selectAllPracticeAreasNewPubs" /> PracticeArea (0) Selected');
});

$('#<portlet:namespace/>allOtherPracticeArea').find('input[type=checkbox]').click(function(){
	 var checked = $(this).is(":checked");
	 if(checked){   
	  $("input[id='<portlet:namespace/>allPracticeArea']").removeAttr("checked");
	  <portlet:namespace/>applyPracticeArea();
	 }
	});

/* Date period selection logic*/


$("#<portlet:namespace/>datePeriodSelect").change(function() {
	 $("#<portlet:namespace/>period").prop("checked", true);
	 $("#<portlet:namespace/>dateRange").removeAttr("checked");
	 <portlet:namespace/>applyDate();
});

$("#<portlet:namespace/>from, #<portlet:namespace/>to").change(function() {
	 $("#<portlet:namespace/>dateRange").prop("checked", true);
	 $("#<portlet:namespace/>period").removeAttr("checked");
	 <portlet:namespace/>applyDate();
});

$("#<portlet:namespace/>applySearch").click(function() {
	$("#<portlet:namespace/>goToPage").val(1);
	 <portlet:namespace/>search();
});

$('#<portlet:namespace/>resetAll').bind('click', function (event) {
	$('#<portlet:namespace/>firmSearchModelBean').find("option").attr("selected", false);
	$('#<portlet:namespace/>firmSearchModelBean').find('input[type=checkbox]:checked').removeAttr('checked');
	$('#<portlet:namespace/>firmSearchModelBean').find('input[type=radio]:checked').removeAttr('checked');
	
	//Firms reset
	$('#<portlet:namespace/>Firmstext').val('<%=ALMConstants.AMLAW_100%>');
	$("#<portlet:namespace/>RivalEdgeList").prop("checked", true);
	$('#<portlet:namespace/>Select1').val('<%=ALMConstants.AMLAW_100%>');
	
	$("#<portlet:namespace/>allfirms").prop("checked", false);
	$("#homePageShowFirms .rivaledgeListAMLAW_100_Firm").prop("checked", false);
	$("#homePageShowFirms .rivaledgeListAMLAW_200_Firm").prop("checked", false);
	$("#homePageShowFirms .rivaledgeListNLJ_250_Firm").prop("checked", false);
	
	$('#<portlet:namespace/>popup').find("option").attr("selected", false);
	$('#<portlet:namespace/>popup').find('input[type=checkbox]:checked').removeAttr('checked');
	$('#<portlet:namespace/>popup').find('input[type=radio]:checked').removeAttr('checked');
	$("#<portlet:namespace/>RivalEdgeList").prop("checked", true);
	$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.AMLAW_100%>');
	$('#<portlet:namespace/>Select1').val('<%=ALMConstants.AMLAW_100%>');
	$("#homePageShowFirms .individualfirms-first").html('<input type="checkbox" value="selectAllFirmsNewPubs" id="<portlet:namespace/>selectAllFirmsNewPubs" /> Firms (0) Selected');
	clearFirms();
	
	$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirmsNewPubs" id="<portlet:namespace/>selectAllFirmsNewPubs" /> Firms (0) Selected');

	$("#<portlet:namespace/>firm_watchlist").prop("checked", true);
	
	<portlet:namespace/>applyFirms();
	
	//contentType reset
	$('#<portlet:namespace/>contentType').val('<%=ALMConstants.ALL_Types %>');
	$("#<portlet:namespace/>allContentType").prop("checked", true);
	
	//Date reset
	$("#<portlet:namespace/>dateText").val('<%=ALMConstants.LAST_WEEK%>');	
	$("#<portlet:namespace/>period").prop("checked", true); 
	$("#<portlet:namespace/>datePeriodSelect").val('<%=ALMConstants.LAST_WEEK%>');	
	
	//keyword reset
	$( "#<portlet:namespace/>keywords" ).val("");
	
	
	//Practice Area reset
	$('#<portlet:namespace/>selectedPracticeArea').val('All Practice Areas');
	$("#<portlet:namespace/>allPracticeArea").prop("checked", true);
	$('#<portlet:namespace/>practiceAreaListItem :input').removeAttr("disabled");
	$('#<portlet:namespace/>practiceAreaListItem').removeClass("ui-state-disabled");
	
	$('#<portlet:namespace/>popupPracticeArea').find('input[type=checkbox]:checked').removeAttr('checked');
	$('#<portlet:namespace/>allPracticeArea').prop('checked', true);	
	$('#<portlet:namespace/>allPracticeAreaIndividualNewsPubs').prop('checked', false);	
	$('#<portlet:namespace/>selectedPracticeArea').val('<%= ALMConstants.ALL_PRACTICE_AREAS %>');
	$("#<portlet:namespace/>individualPracticeAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreasNewPubs" id="<portlet:namespace/>selectAllPracticeAreasNewPubs" /> PracticeArea (0) Selected');
	
	//TwitterPostType reset
	$('#<portlet:namespace/>selectedTwitterPostType').val('<%=ALMConstants.ALL%>');
	$("#<portlet:namespace/>alltwitterPostType").prop("checked", true);
	$('#<portlet:namespace/>typeOfTwitterPostListItem :input').removeAttr("disabled");
	$('#<portlet:namespace/>typeOfTwitterPostListItem').removeClass("ui-state-disabled");
	
	
});


			//Firm clear button event: Will clear all selected values in firms popup and reset to default value
			$('#<portlet:namespace/>clearButton').bind('click', function (event) {
				$('#<portlet:namespace/>popup').find("option").attr("selected", false);
				$('#<portlet:namespace/>popup').find('input[type=checkbox]:checked').removeAttr('checked');
				$('#<portlet:namespace/>popup').find('input[type=radio]:checked').removeAttr('checked');
				$("#<portlet:namespace/>RivalEdgeList").prop("checked", true);
				$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.AMLAW_100%>');
				$('#<portlet:namespace/>Select1').val('<%=ALMConstants.AMLAW_100%>');
				$("#homePageShowFirms .individualfirms-first").html('<input type="checkbox" value="selectAllFirmsNewPubs" id="<portlet:namespace/>selectAllFirmsNewPubs" /> Firms (0) Selected');
				clearFirms();
			});
			
			function clearFirms()
			{
				/* $('#popup').find("option").attr("selected", false);
				$('#popup').find('input[type=checkbox]:checked').removeAttr('checked');
				$('#popup').find('input[type=radio]:checked').removeAttr('checked');
				
				$("#individualFirmsCounter").html('<input type="checkbox" value="Abrams" id="firmCounter"> Firms (0) Selected');

				$("#RivalEdgeList").prop("checked", true); */
				<%-- $('#Select1').val('<%=ALMConstants.AMLAW_100%>'); --%>
				
				$('#<portlet:namespace/>popup').find("option").attr("selected", false);
				$('#<portlet:namespace/>popup').find('input[type=checkbox]:checked').removeAttr('checked');
				$('#<portlet:namespace/>popup').find('input[type=radio]:checked').removeAttr('checked');
				$("#<portlet:namespace/>allfirms").prop("checked", false);
				$("#homePageShowFirms .rivaledgeListAMLAW_100_Firm").prop("checked", false);
				$("#homePageShowFirms .rivaledgeListAMLAW_200_Firm").prop("checked", false);
				$("#homePageShowFirms .rivaledgeListNLJ_250_Firm").prop("checked", false);
				$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirmsNewPubs" id="<portlet:namespace/>selectAllFirmsNewPubs" /> Firms (0) Selected');

				$("#<portlet:namespace/>firm_watchlist").prop("checked", true);
				
				<portlet:namespace/>applyFirms();
			}
			$('#<portlet:namespace/>clearButtonContentType').bind('click', function (event) {
				$('#<portlet:namespace/>popupContentType').find('input[type=checkbox]:checked').removeAttr('checked');
				$('#<portlet:namespace/>allContentType').prop('checked', true);
				$('#<portlet:namespace/>contentType').val('<%=ALMConstants.ALL_Types %>');
				
				
			});
			
			$('#<portlet:namespace/>clearButtonDate').bind('click', function (event) {
				$('#<portlet:namespace/>popupDate').find("option").attr("selected", false);
				$('#<portlet:namespace/>popupDate').find('input[type=text]').val('');
				$('#<portlet:namespace/>popupDate').find('input[type=radio]:checked').removeAttr('checked');				
				$("#<portlet:namespace/>dateText").val('<%=ALMConstants.LAST_WEEK%>');	
				$("#<portlet:namespace/>period").prop("checked", true); 
				$("#<portlet:namespace/>datePeriodSelect").val('<%=ALMConstants.LAST_WEEK%>');	
				$("#<portlet:namespace/>from").datepicker('enable');
				$("#<portlet:namespace/>to").datepicker('enable');
			}); 
			
			$('#<portlet:namespace/>clearTwitterPostType').bind('click', function (event) {
				$('#<portlet:namespace/>popupTwitterPostType').find('input[type=checkbox]:checked').removeAttr('checked');
				$('#<portlet:namespace/>alltwitterPostType').prop('checked', true);
				$('#<portlet:namespace/>selectedTwitterPostType').val('<%= ALMConstants.ALL %>');
				//$("#allOthertwitterPostType input").attr("disabled", true);	
			});
			
			$('#<portlet:namespace/>clearPracticeArea').bind('click', function (event) {
				$('#<portlet:namespace/>popupPracticeArea').find('input[type=checkbox]:checked').removeAttr('checked');
				$('#<portlet:namespace/>allPracticeArea').prop('checked', true);	
				$('#<portlet:namespace/>allPracticeAreaIndividualNewsPubs').prop('checked', false);	
				$('#<portlet:namespace/>selectedPracticeArea').val('<%= ALMConstants.ALL_PRACTICE_AREAS %>');
				$("#<portlet:namespace/>individualPracticeAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreasNewPubs" id="<portlet:namespace/>selectAllPracticeAreasNewPubs" /> PracticeArea (0) Selected');
				
			});


			$('#<portlet:namespace/>ApplyFirm').bind('click', function (event) {
					<portlet:namespace/>applyFirms();
					$("#<portlet:namespace/>popup").toggle();
			
			});
			

			$('#<portlet:namespace/>ApplyDate').bind('click', function (event) {
				
				var checkDate = <portlet:namespace/>applyDate();
				if(!checkDate)
					{
						 $("#<portlet:namespace/>popupDate").toggle();
					}
			
				});
				
				
			$('#<portlet:namespace/>ApplyFirmContentType').bind('click', function (event) {
					<portlet:namespace/>applyContentType();
				 $("#<portlet:namespace/>popupContentType").toggle();
			
			});
			
			
			$('#<portlet:namespace/>ApplyTwitterPostType').bind('click', function (event) {
			     <portlet:namespace/>applyTwitterType();
				 $("#<portlet:namespace/>popupTwitterPostType").toggle();
			
			});
			
			$('#<portlet:namespace/>ApplyPracticeArea').bind('click', function (event) {
				<portlet:namespace/>applyPracticeArea();
				 $("#<portlet:namespace/>popupPracticeArea").toggle();
			
			});

		 $("#additionalFilter").click(function () {
		  var link = $(this);
		
		  $('#additionalFilterDiv').slideToggle('slow', function() {
		            if ($(this).is(":visible")) {
		                 link.text('Hide Filter');                
		            } else {
		                 link.text('Additional Filter');                
		            }        
		        });
		});
		 
	//Spring will add <input> tags with names starting with "_" for type "radio" and "checkbox".
	//On form POST submit they all are carried to server which is unneccessary
	//hence removing all such input box to make page and jQuery selections less heavy :) on document ready.
	
	//NOTE : If any inputs added with name prefixed with "_" purposefully will also get removed. So be cautious
	$("#firmSearchModelBean input[name^=_]").remove();
	
	// autoselect previously selected search critria
  	<portlet:namespace/>initializeSearchCriteria() ;
 	//fire a search for the default search criteria
 	//search();


});
</script>



<div id="homePageShowFirms" >
<form:form commandName="firmSearchModelBean" method="post" action="${submitURL}" id="firmSearchModelBean"  autocomplete="off">
 	<form:hidden path="goToPage"  		id="goToPage"/>
 	<form:hidden path="sortColumn" 		id="sortColumn"/>
 	<form:hidden path="sortOrder"  		id="sortOrder"/>
 	<form:hidden path="portletId"		id="portletId"/>
 	<form:hidden path="addToSession"	id="addToSession"/>
<div class="breadcrumbs"><span>News and Publications</span></div>

<div id="dropdown">
<ul id="droplist">
 
 <li>
        <label>Firm(s)</label>
        <div class="srchBox">
        <%--  TODO retain the Firmstext value--%>
          <!-- <input type="text" name="Firmstext" id="Firmstext"  style="text-overflow:ellipsis;" class="input" /> -->
          
          <c:choose>
              <c:when test="${empty allWatchListsDefaultList or allWatchListsDefaultList == ''}">
          			<input type="text" name="Firmstext" id="Firmstext"  value="<%=ALMConstants.AMLAW_100 %>" style="text-overflow:ellipsis;" class="input" autocomplete="off"/>
          	  </c:when>
          	  <c:otherwise>
          			<input type="text" name="Firmstext" id="Firmstext"  value="${allWatchListsDefaultList.groupName}" style="text-overflow:ellipsis;" class="input"  autocomplete="off"/>
          	  </c:otherwise>
          </c:choose>
          
          <input type="button" name="search" value="" class="srchBack" id="testDiv" />
          <div class="clear">&nbsp;</div>
        </div>
        <input type="button" name="search" id="hide"  value="" class="typeSel" />
       
        <div class="rel">
		 <div  id="<portlet:namespace/>popup" class="firmPage">
			<p><form:radiobutton path="firmType" id="allfirms" value="<%=ALMConstants.ALL_FIRMS %>" />&nbsp;<%=ALMConstants.ALL_FIRMS %><!-- <input type="radio" name="Firms" value="All Firms"  id="allfirms" />All Firms --></p>
			<%-- <p><form:radiobutton path="firmType" id="RivalEdgeList"   value="<%=ALMConstants.RIVALEDGE_LIST %>" />&nbsp;<%=ALMConstants.RIVALEDGE_LIST %></p>
			
			<form:select path="firmList" id="Select1" multiple="false" size="4" cssStyle="width:220px">
				<form:options items="${allRankingList}" />
			</form:select> --%>
			
			<p> <form:radiobutton path="firmType" id="firm_watchlist" value=""/>Select Watchlist/RivalEdge List</p>
             	<div class="individualfirms-Watchlist-Firm" >
                	<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0 Selected)<br />
               </div>
 				 <div  class="individualfirmsWatchListFirm" id="<portlet:namespace/>individualfirmsWatchListFirm"> 
 				 
 				 	<form:checkbox  class="rivaledgeListAMLAW_100_Firm" path="firmList"  value="<%=ALMConstants.AMLAW_100 %>" /><%=ALMConstants.AMLAW_100 %><br>
					<form:checkbox  class="rivaledgeListAMLAW_200_Firm" path="firmList"  value="<%=ALMConstants.AMLAW_200 %>" /><%=ALMConstants.AMLAW_200 %><br>
					<form:checkbox  class="rivaledgeListNLJ_250_Firm" path="firmList"  value="<%=ALMConstants.NLJ_250 %>" /><%=ALMConstants.NLJ_250 %><br>
  
 				 	<div  class="individualfirmsWatchListDIV" id="<portlet:namespace/>individualfirmsWatchListDIVFirm"> 
		                <c:forEach var="watchlist"  items="${allWatchLists}">     
	    	  				 <form:checkbox  class="allFirmsCheckBoxWatchListFirm" path="firmListWatchList"  value="${watchlist.groupId}" labelAttr="${watchlist.groupName}" />${watchlist.groupName}<br>
	  				 	</c:forEach>
					</div>
			 	</div> 
			
			
			<p><form:radiobutton path="firmType" value="<%=ALMConstants.INDIVIDUAL_LIST %>" id="allOtherFirm"/>&nbsp;<%=ALMConstants.INDIVIDUAL_LIST %></p>
			 <div class="individualfirms-first" id="<portlet:namespace/>individualFirmsCounter"> 
				<input type="checkbox" value="selectAllFirmsNewPubs" id="<portlet:namespace/>selectAllFirmsNewPubs" /> Firms (0 Selected)<br />
               <!--  <input type="checkbox" value="Abrams" />Firms (0 Selected)<br /> -->
              </div>
			<!-- <div style="margin-left:15px;background-color:#FFFFFF;" id="allOtherFirmDiv"> -->	
			<div id="<portlet:namespace/>allOtherFirmDiv" class="Select-Individual-Firms">
			<c:forEach var="firm"  items="${allFirms}">    
				<form:checkbox  class="allFirmsCheckBoxFirmState"  path="firmList" value="${firm.companyId}" mylabel="${firm.company}"/>${firm.company}<br>
			 </c:forEach>
			
			<%--		     
			       <c:forEach var="firm"  items="${firmModel['allFirms']}">      
      				 <input type="checkbox"  id="allFirmsCheckBoxFirmState" value="<c:out value="${firm.companyId}"/>" labelAttr="${firm.company}" ><c:out value="${firm.company}"/><br>
  			 </c:forEach>	
				 --%>
			</div>
			<div class="popupsubmit">
			<input type="button" class="buttonOne" value="Apply" id="ApplyFirm">
			<input type="button" class="buttonTwo" value="Clear All" id="clearButton">
			</div>
		
		</div>
	   </div>
	   
	   <c:if test="${not empty allWatchListsDefaultList}">
	    		<input type="hidden" name="defaultWatchListId" id="defaultWatchListId" value="${allWatchListsDefaultList.groupId}" />
	   	</c:if>
 </li>
  <li>
        <label>Content Types</label>
        <div class="srchBox">
          <input type="text" name="content"  id="contentType" class="input"   autocomplete="off"/>         
          <div class="clear">&nbsp;</div>
        </div>
        <input type="button" name="search" id="contentTypeId" class="drpDwn"  />
        <div class="rel">
		<div  id="<portlet:namespace/>popupContentType" class="firmPage" style="width:165px !important;">
			<div id="<portlet:namespace/>contentTypeSelectDiv">
			<form:checkbox path="contentType"  id="allContentType" class="dummyclass" value="<%=ALMConstants.ALL_Types %>" />&nbsp;<%=ALMConstants.ALL_Types %>  <br>
			   <%--
			    <input  type="checkbox"  name="allContentType"  id="allContentType" 		value="<%=ALMConstants.ALL_Types %>"  checked="checked"><%=ALMConstants.ALL_Types %><br>
			     --%>
			    <div id="<portlet:namespace/>allOtherContentType">
			      <form:checkbox path="contentType" value="News" cssClass="dummyclass"/>&nbsp;News<br>
			      <form:checkbox path="contentType" value="Pubs" cssClass="dummyclass"/>&nbsp;Publications<br>
			      <form:checkbox path="contentType" value="Twitter" cssClass="dummyclass" />&nbsp;Twitter<br>
			      <%--
					<input  type="checkbox"  name="contentType"  		value="News" >News<br>
					<input  type="checkbox"  name="contentType"  		value="Publications" >Publications<br>
					<input  type="checkbox"  name="contentType" 		value="Twitter">Twitter<br>
					 --%>
				</div>
			</div>
		<div class="popupsubmit">
			<input type="button" class="buttonOne"  value="Apply" id="ApplyFirmContentType">
			<input type="button" class="buttonTwo" value="Clear All" id="clearButtonContentType">
		</div>
		</div>
		</div>
 </li>
  <li>
        <label>Dates</label>
        <div class="srchBox">
           <form:input path="dateText" id="dateText" readonly="true" cssClass="input" cssStyle="width:140px"  autocomplete="off"/>
        <%--
          <input name="dateText" id="dateText" value=""  readonly="readonly" class="input"  style="width:140px"/>          
           --%> <div class="clear">&nbsp;</div>
        </div>
        <input type="button" name="search" id="datenone" value="" class="drpDwn" />
        <div class="rel">
        <div id="<portlet:namespace/>popupDate" class="datePage">
        	<input type="radio" name="date" id="period"  />&nbsp;Period<br>
			<select id="datePeriodSelect" class="marlt4">
				<option value="Any">Any</option>
				<option value="Last Week">Last Week</option>
				<option value="Last 30 Days">Last 30 Days</option>
				<option value="Last 90 Days">Last 90 Days</option>
				<option value="Last 6 Months">Last 6 Months</option>
				<option value="Last Year">Last Year</option>  
			</select>
			<br>
			<input type="radio" name="date"  id="dateRange"  checked="checked"/>&nbsp;Date Range<br>

			<div class="flLeft marlt4"><label for="from">From</label>
			<input type="text" id="from" name="from"  /></div>
			<div class="flLeft marlt3"><label for="to">To</label>
			<input type="text" id="to" name="to" /></div>
			<div class="clear"></div>
			<span class="error" id="fromdateError"> Invalid  From Date</span>
			<span class="error" id="todateError"> Invalid  To Date</span>
			<span class="error" id="dateValidError"> To date should be more than from date</span>
			<div class="popupsubmit Popup-Calendar">
			<input type="button" class="buttonOne" value="Apply" id="ApplyDate">
			<input type="button" class="buttonTwo" value="Clear All" id="clearButtonDate">
			</div>
        </div>
        </div>
     </li>
 
		<li>
        <label>Keywords</label>
        <div class="srchBox">
          <%--
          <input type="text" name="search" value="" class="input" id="keywords"  name="keywords" />
          --%>
          <form:input path="keywords" id="keywords" cssClass="input"  autocomplete="off"/>
           <input type="button" name="search" value="" class="srchBack" />
          <div class="clear">&nbsp;</div>
        </div>
       
      </li>
	  <li class="Submit-Filter-Criteria">
        <label>&nbsp;</label>
        <input type="button" value="Apply" class="buttonOne" style="background:url(<%=renderRequest.getContextPath()%>/images/btn1.png) 0 0 repeat-x; border:1px solid #bf8d1f; -webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px; font:normal 12px Arial, Helvetica, sans-serif; color:#fff; padding:5px 10px; cursor:pointer; text-shadow: 0px 0px #FFF;" id="applySearch"/>
      </li>
      <li>
        <label>&nbsp;</label>
        <input type="button" value="Reset All" style="background:url(<%=renderRequest.getContextPath()%>/images/btn2.png) 0 0 repeat-x; border:1px solid #565656; -webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px; font:normal 12px Arial, Helvetica, sans-serif; color:#fff; padding:5px 10px; cursor:pointer; text-shadow: 0px 0px #FFF;" class="buttonTwo" id="resetAll" />
      </li>
	   <li>
        <label>&nbsp;</label>
        <input type="hidden"  name="selectedFirms" id="selectedFirms"  value="<%=ALMConstants.AMLAW_100%>" title="" customAttr="" />
      </li>
      </ul>
 <div class="clear">&nbsp;</div>
		  </div>
		  <div class="filtersPage">
			<div class="barSec" style="background:url(<%=renderRequest.getContextPath()%>/images/additinalBack.png) center 0 no-repeat; width:100%; height:24px; margin:10px 0"><a href="javascript:void(0);" class="dwnBx" id="<portlet:namespace/>btnAdd">Additional Filter
            <img src="/re-theme/images/add-fltr-dwn-arw.png" class="add-fltr-dwn-arw">
            </a></div>
           </div>
		  <div class="filtersPage" id="<portlet:namespace/>additional" style="display:none">
			<div id="dropdown"><!-- dropdown_title_start -->
			  <ul id="droplist">
				<li id="<portlet:namespace/>practiceAreaListItem">
				  <label>Practice Area(s) for Publications </label>
				  <div class="srchBox">
					<p><input type="text"  value="All Practice Areas" class="input" name="selectedPracticeArea" id="selectedPracticeArea"  autocomplete="off"/>
					<input type="button" name="search" value="" class="srchBack" /></p>
					<div class="clear">&nbsp;</div>
				  </div>
				 <input type="button" name="search" value=""  id="practiceAreaId" class="drpDwn" />
			<div class="rel">
				  <div style="width:225px" id="<portlet:namespace/>popupPracticeArea" class="firmPage">	
				  
				  <form:radiobutton path="practiceArea"  id="allPracticeArea" value="<%=ALMConstants.ALL_PRACTICE_AREAS %>" />&nbsp;<%=ALMConstants.ALL_PRACTICE_AREAS %><br>
				  <p> <input  type="radio"  name="allPracticeArea"  id="allPracticeAreaIndividualNewsPubs" value="Select Individual Practice Areas"> Select Individual Practice Areas</p>
				  <div class="individualPracticeAreaCounterNewsPubs" id="<portlet:namespace/>individualPracticeAreaCounter"> <input type="checkbox" value="selectAllPracticeAreasNewPubs" id="selectAllPracticeAreasNewPubs"> Practice Areas (0 Selected)<br> </div>
				  				
						<div id="<portlet:namespace/>practiceAreaDiv" class="Select-Individual-Firms">
							
						   	 <%--
						   		<input  type="checkbox"  name="allPracticeArea"  id="allPracticeArea" 	value="All Practice Areas"  checked="checked">All Practice Areas<br>
						     --%> 
						    <div id="<portlet:namespace/>allOtherPracticeArea">
						    <c:forEach var="practice"  items="${allPracticeArea}">								
								<form:checkbox path="practiceArea" class="allPracticeAreaCheckBoxCounter"  value="${practice.practiceArea}"/>&nbsp;${practice.practiceArea}<br>
							</c:forEach>
						 	 <%--
						    <form:checkboxes delimiter="<br/>" path="practiceArea" items="${firmModel['allPracticeArea']}" itemValue="practiceArea" itemLabel="practiceArea"/>
						     --%>
						    <%--
						    <c:forEach var="practice"  items="${firmModel['allPracticeArea']}">								
								<input  type="checkbox"  name="twitterPostType"  		value="${practice.practiceArea}"    >${practice.practiceArea}<br>
							</c:forEach>
							--%>								
						 </div>	
						 </div>
						
						<div class="clear"></div>
					<div class="popupsubmit">
					 <input type="button" class="buttonOne" value="Apply" id="ApplyPracticeArea" />
					 <input type="button" class="buttonTwo" value="Clear All" id="clearPracticeArea" />
					 </div>
					</div>
				</div>	
				</li>
				<li id="<portlet:namespace/>typeOfTwitterPostListItem">
				  <label>Types of Twitter Posts</label>
				  <div class="srchBox">
					<input type="text"  value="<%=ALMConstants.ALL%>"  class="input" name="selectedTwitterPostType" id="selectedTwitterPostType"  autocomplete="off"/>
					
					<div class="clear">&nbsp;</div>
				  </div>
				    <input type="button" name="search" value=""  id="twitterPostTypeId" class="drpDwn" />
				  <div class="rel">
				  <div style="width:165px !important;" id="<portlet:namespace/>popupTwitterPostType" class="firmPage">						
						<div id="<portlet:namespace/>twitterPostTypeDiv">
						<form:checkbox path="twitterType"  id="alltwitterPostType" value="<%=ALMConstants.ALL %>" />&nbsp;<%=ALMConstants.ALL %><br/>
						  <%--  <input  type="checkbox"  name="alltwitterPostType"  id="alltwitterPostType" 		value="<%=ALMConstants.ALL%>"  checked="checked"><%=ALMConstants.ALL%><br>
						     --%>
						    <div id="<portlet:namespace/>allOthertwitterPostType">
						    	 <form:checkbox path="twitterType"  value="<%=ALMConstants.TWITTER_POST_TYPE_BY_LAW_FIRMS %>" />&nbsp;<%=ALMConstants.TWITTER_POST_TYPE_BY_LAW_FIRMS %><br>
						    	 <form:checkbox path="twitterType"  value="<%=ALMConstants.TWITTER_POST_TYPE_ABOUT_LAW_FIRMS %>"  />&nbsp;<%=ALMConstants.TWITTER_POST_TYPE_ABOUT_LAW_FIRMS %><br>
						    	 <form:checkbox path="twitterType"  value="<%=ALMConstants.TWITTER_POST_TYPE_BY_OTHERS %>"  />&nbsp;<%=ALMConstants.TWITTER_POST_TYPE_BY_OTHERS %><br>
						    	 <%--
										<input  type="checkbox"  name="twitterPostType"  		value="<%=ALMConstants.TWITTER_POST_TYPE_BY_LAW_FIRMS%>" ><%=ALMConstants.TWITTER_POST_TYPE_BY_LAW_FIRMS%>"<br>
										<input  type="checkbox"  name="twitterPostType"  		value="<%=ALMConstants.TWITTER_POST_TYPE_ABOUT_LAW_FIRMS%>" ><%=ALMConstants.TWITTER_POST_TYPE_ABOUT_LAW_FIRMS%><br>
										<input  type="checkbox"  name="twitterPostType" 		value="<%=ALMConstants.TWITTER_POST_TYPE_BY_OTHERS%>"><%=ALMConstants.TWITTER_POST_TYPE_BY_OTHERS%><br>
								 --%>
							</div>
						</div>	
						<div class="clear"></div>
					  <div class="popupsubmit">
					 <input type="button" class="buttonOne" value="Apply" id="ApplyTwitterPostType">
					 <input type="button" class="buttonTwo" value="Clear All" id="clearTwitterPostType">
					 </div>
					</div>
					</div>
				  
				</li>
			  </ul>
			  <div class="clear">&nbsp;</div>
			</div>
           <div class="barSec" style="background:url(<%=renderRequest.getContextPath()%>/images/additinalBack.png) center 0 no-repeat; width:100%; height:24px; margin:10px 0"><a href="javascript:void(0);" class="upBx" id="<portlet:namespace/>btnSave">Hide Filter
            <img src="/re-theme/images/add-fltr-up-arw.png" class="add-fltr-up-arw">
            </a></div>
  </div>
  <div style="display: none; position:absolute; background:#F0F0F0; width:310px !important" class="viewBox popusdiv ClickPopup newspublicationPage charts" id="<portlet:namespace/>view_settings">
					<div class="popHeader">
						<a href="javascript:void(0);" class="btn icon closewhite closeOne flRight" style="margin-top: 2px" onclick="<portlet:namespace/>cancelViewSettings();">&nbsp;Close</a>
							SETTINGS: News and Publications
					</div>
					<div class="section-two" style="width:120px;">
                      <h6>Group Results By</h6>
                        <ul class="reset list4">
                             <li><form:radiobutton path="orderBy" value="-1"/><span>No Grouping</span></li>
                             <li><form:radiobutton path="orderBy" value="2" /><span>Firm</span></li>
                             <li><form:radiobutton path="orderBy" value="3"/><span>Content Type</span></li>
                        </ul>
                        <div class="clear">&nbsp;</div>
                    </div>
					<div class="section-two" style="width:140px;">
							<h6>Display No of Results</h6>
							<ul class="reset list4">
								<li><form:radiobutton path="searchResultsPerPage"  value="25"  /><span>25</span></li>
                                <li><form:radiobutton path="searchResultsPerPage"  value="50"  /><span>50</span></li>
                                <li><form:radiobutton path="searchResultsPerPage"  value="100" /><span>100</span></li>
                                <li><form:radiobutton path="searchResultsPerPage"  value="500" /><span>500</span></li>
							</ul>
							<div class="clear">&nbsp;</div>
					</div>
					<div class="clear">&nbsp;</div>
					<hr>
					<div class="btmdiv">
						<input type="button" value="Reset All" class="buttonTwo flLeft settingReset" onclick="<portlet:namespace/>resetViewSettings();">
						<input type="button" class="buttonTwo flRight rightReset" value="Cancel" onclick="<portlet:namespace/>cancelViewSettings();">
						<input type="button" value="Apply" class="buttonOne flRight" style="margin: 0 5px 0 0;" onclick="<portlet:namespace/>applyViewSettings();">
						<div class="clear">&nbsp;</div>
					</div>
				</div>

</form:form>
</div>

<script>
//Omniture SiteCatalyst - START
s.prop22 = "premium";
s.pageName="rer:firm-search";
s.channel="rer:firm-search";
s.server="rer";
s.prop1="Firm-Search"
s.prop2="Firm-Search";
s.prop3="Firm-Search";
s.prop4="Firm-Search";
s.prop21=s.eVar21='current.user@foo.bar';	
s.events="event2";
s.events="event27";
s.t();
// Omniture SiteCatalyst - END
</script>