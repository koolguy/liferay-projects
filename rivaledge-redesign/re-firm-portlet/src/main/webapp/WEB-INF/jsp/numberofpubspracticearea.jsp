<%@page import="com.alm.rivaledge.util.WebUtil"%>
<%@page import="com.alm.rivaledge.model.chart.BaseChartModelBean"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="java.util.Map"%>
<%@ taglib prefix="portlet" 		uri="http://java.sun.com/portlet_2_0"%> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" 			 	uri="http://java.sun.com/jsp/jstl/core" %>
<portlet:defineObjects/>

<%@ include file="./common.jsp" %>

<portlet:actionURL var="updateChartURL">
	<portlet:param name="action" value="updateChart"/>
</portlet:actionURL>

<portlet:actionURL var="removePortletURL">
	<portlet:param name="action" value="removePortleFromThePage"/>
</portlet:actionURL>

<style>
#firmSearchChartViewSettings .section-one{
	width: 160px;
}
#firmSearchChartViewSettings .section-two{
	width: 200px;
}
</style>
<script type="text/javascript">


	
function removeThisChartPracticeArea(){
	
	var removePortletAction = '<%= removePortletURL.toString()%>';
	var r=confirm("Are you sure you want to delete this chart?");
	if (r == true)
  	{
		Liferay.Portlet.showBusyIcon("#bodyId", "Loading...");
		$("#firmSearchChartModelBean").attr('action', removePortletAction);
		$("#firmSearchChartModelBean").submit();
  	}
	else
	{
	  return false;
	}
}

(function () {
	
	
var typeOfChartNPP = '';
var stackingTypeNPP = '';
<c:choose>
<c:when test="${NumberOfPubsPracticeAreaChartType eq 'vertical_bar' }">
	typeOfChartNPP = 'column';
	stackingTypeNPP = '';
</c:when>
<c:when test="${NumberOfPubsPracticeAreaChartType eq 'horizontal_bar' }">
	typeOfChartNPP = 'bar';
	stackingTypeNPP = '';
</c:when>
</c:choose>

<%@ include file="./theme.jsp" %>

	var numberOfPubsChartType 							= '${NumberOfPubsPracticeAreaChartType}'; 			 //This will be type of chart.
	var numberOfPubsChartTitle 							= "${NumberOfPubsPracticeAreaChartTitle}"; 			 // This will be title of chart.
	var numberOfPubsPracticeAreaList 					= [${numberOfPubsPracticeAreaList}]; 		 // This will be list of firms name.
	var numberOfPubsPracticeAreaCount 					= [${numberOfPubsPracticeAreaCount}]; 	 // This will be news count of individual firms.
	var numberOfPubsPracticePractices					= [${topPubsByPraticeStringList}];

$(function()
{

	
	$(".cancelNPP").click(function(){
		$("#firmSearchChartViewSettings").hide();
	});
	$(".newspublicationPage .closewhite").click(function(){
		//$("#login-box2").hide();
		$("#firmSearchChartViewSettings").hide();
	});
	// When user clicks reset all button default graph will be shown. 
	$("#noPubsByPracticeReset").click(function(){
		$('.defaultChartType').prop('checked', true);
		$("#firmSearchChartFirmList").val($("#firmSearchChartFirmList option:first").val());
		$('.defaultLimitType').prop('checked', true);
		$('.percentChangeAverageValueAPG').prop('checked', true);
		$('.percentChangeRankingCheckAPG').prop('checked', true);
		$("#percentChangeRankingListAPG").val($("#percentChangeRankingListAPG option:first").val());
		$("#searchResultsPercentChangeFirmList > option").attr("selected",false);
	});
	
	$("#searchResultsPercentChangeFirmList").click(function(){
		if($("#searchResultsPercentChangeFirmList option:selected")){
			$("input[value=practice]").prop("checked", true);
		}
	});
	
	// When user clicks on analysis tab active-menu css class is removed from details tab.
	// And same class added to analysis tab.
	$("#firms-details").removeClass("active-menu");
	$("#firms-analysis").addClass("active-menu");
	
	$("#amountNewsPubsByPracticeViewSettings").hide();
	
	$(".AnalyzeResultHeader").show();
		
	$("#noOfPubsPracticeAreaApply, .closewhite").click(function(){
		$("#amountNewsPubsByPracticeViewSettings").hide();
	});
	
	$("div#noPubsPopupDiv").mouseover(function(){
		$("div#noPubsPopupDiv").show();
	}).mouseout(function(){
		$("div#noPubsPopupDiv").hide();
	});
	
	if(<%=!isHomePage%>)
	{
		if(numberOfPubsPracticeAreaCount[0] == null)
		{
			$("#noDataNumberofpubspracticeareaDiv").removeClass("hideClass");	
			$("#noDataNumberofpubspracticeareaDiv").parent().find(".charts").hide(); // hide the settings icon
	
			//All the above code is useless if this is the final approach
			$("#p_p_id<portlet:namespace/>").hide();
			$(".cph").show();
			return;
		}
	}
	else
	{
		if(numberOfPubsPracticeAreaCount[0] == null)
		{
			$("#noDataNumberofpubspracticeareaDiv").removeClass("hideClass");
			$("#amountOfPubsPracticeAreaChartContainer").addClass("hideClass");
			
			$("#No-Data-noofPubsPractice").addClass("No-Data-Charts");
			$("#noofPubs_practice_settings").hide();
		}
		else
		{
			$("#No-Data-noofPubsPractice").removeClass("No-Data-Charts");
			$("#noDataNumberofpubspracticeareaDiv").addClass("hideClass");
		}
	}
	
	$("#noOfPubsPracticeAreaCancel").click(function(){
		$("#amountNewsPubsByPracticeViewSettings").hide();
	});
	
	$('#noOfPubsPracticePrintCharts').click(function() {
        var chart = $('#amountOfPubsPracticeAreaChartContainer').highcharts();
        chart.print();
    });
	$('#noOfPubsPracticeExportJPG').click(function() {
        var chart = $('#amountOfPubsPracticeAreaChartContainer').highcharts();
        chart.exportChart({type: 'image/jpeg'});
    });
	$('#noOfPubsPracticeExportPNG').click(function() {
        var chart = $('#amountOfPubsPracticeAreaChartContainer').highcharts();
        chart.exportChart({type: 'image/png'});
    });
	
	var labelXPosition = 0;
	var labelYPosition = 0;
	var labelRotation = 0;
	var dataLabelColor = '#FFFFFF';
	
	if(typeOfChartNPP == 'bar')
	{
		labelXPosition = -5;
		labelYPosition = 0;
		labelRotation = 0;
	}
	else
	{
		labelXPosition = 5;
		labelYPosition = 10;
		labelRotation = -45;
	}
	
	
	$('#amountOfPubsPracticeAreaChartContainer').highcharts(
	{
        chart: 
        {
            type: typeOfChartNPP
        },
        title: 
        {
            text: numberOfPubsChartTitle
        },
        subtitle:
		{
			text: '${selectedFirmName}'
		},
        xAxis: 
        {
            categories: numberOfPubsPracticeAreaList,
            labels: 
            {
                rotation: labelRotation,
				y: labelYPosition,
				x: labelXPosition,
				align: 'right',
				formatter: function()
				{
					var practiceArea = this.value;
					if(practiceArea.length > 15)
					{
						practiceArea = practiceArea.substring(0, 15) + "...";
					}
					return practiceArea;
				}
			}
        },
        yAxis: 
        {
			alternateGridColor: null,
			minorTickInterval: null,
			gridLineWidth: 1,
			gridLineColor: '#cecece',
			minorGridLineColor: '#cecece',
			lineWidth: 1,
			tickWidth: 0,
            stackLabels: 
            {
                enabled: false,
                style: 
                {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: 
        {
        	enabled: true
    	},
    	tooltip : {
			useHTML : true,
			positioner : function(boxWidth, boxHeight,
					point) {
					var xPosition = point.plotX - 0;
					var yPosition = point.plotY - boxHeight + 40;
					
					if(typeOfChartNPP == 'column')
					{
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(150))
						{
							xPosition = point.plotX-60;
						}
						
						if(parseInt(point.plotY) < parseInt(boxHeight))
						{
							yPosition = point.plotY+90;
						}
					}
					else
					{
						xPosition = point.plotX + 140;
						yPosition = point.plotY - boxHeight + 140;
						
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(40))
						{
							xPosition = point.plotX-50;
						}
						
						if((parseInt(boxHeight) - parseInt(point.plotY)) > parseInt(100))
						{
							yPosition = point.plotY - boxHeight + 200;
						}
					}
					
				return {
					x : xPosition,
					y : yPosition
				};
			},
			formatter: function()
			{
				var pointerPosition = 0;
				var selectedFirmName = '${selectedFirmName}';
				for(var index=0; index < numberOfPubsPracticeAreaList.length; index++){
					if(this.key == numberOfPubsPracticeAreaList[index]){
						pointerPosition = index;
					}
				}
				
				if(numberOfPubsPracticePractices[pointerPosition] != undefined){
					var tooltipOption = '';
					tooltipOption += '<div class="Tooltip-Heading">'+selectedFirmName+'</div><div class="Tooltip-Recent">Recent Publications</div>'
				
					for(var index=0; index < numberOfPubsPracticePractices[pointerPosition].length; index++){
						tooltipOption += '<div class="Tooltip-Rows"  title="'+ numberOfPubsPracticePractices[pointerPosition][index] +'">' + (numberOfPubsPracticePractices[pointerPosition][index].length > 15 ? numberOfPubsPracticePractices[pointerPosition][index].substring(0, 15) + '...' : numberOfPubsPracticePractices[pointerPosition][index]) +'</div>';
					}
					
					var url = "firms-details?drilldownPracticeAreaName=" + encodeURIComponent(this.key) + "&drilldownFirmName=" + encodeURIComponent(selectedFirmName) + searchCriteria ;
					tooltipOption += '<div><span class="clickToViewDetails"><a href="'+url+'">Click to View Details</a></span></div>';
					return tooltipOption;
				} else {
					return false;
				}
				
			}
		},
        plotOptions: 
        {
            series: 
            {
            	pointWidth: 15,
            	stacking: stackingTypeNPP,
            	dataLabels: 
            	{
                    enabled: false,
                    color: '#FFFFFF',
                    formatter: function()
					{
						if(parseInt(this.y) > 0)
						{
							return this.y;
						}
                    }
                },
                point: 
                {
                    events: 
                    {
                        mouseOver: function(e) 
                        {}
                    }
                    
                }
            }
        },
        series: [
        {
            name: 'Pubs Practice Area',
            color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#506a85'],
					[1, '#15375c']
				]
			},
            data: numberOfPubsPracticeAreaCount
        }],
		navigation: {
            buttonOptions: {
                enabled: false
            }
        }
    });
	
	$(document).click(function() {
		$("div#noPubsPopupDiv").hide();
	});
});

$(document).ready(function()
{	
    $("#noofPubs_practice").click(function()
    {
    	Liferay.Portlet.showPopup(
        		{
        			uri : '${chartSearchPortletURL}', // defined in common.jsp
        			title: "Search Criteria"
        		});
	});
	
});

}());

</script>

<div class="newspublicationPage">
	<div class="colMin flLeft leftDynamicDiv">
		<div class="topHeader ForChartsTopHeader">
			<a title="Remove this chart" onclick="removeThisChartPracticeArea();" href="javascript:void(0);" style="float: right; font-weight: bold; color: rgb(255, 255, 255); cursor: pointer; font-family: verdana; margin: 2px 5px; padding: 3px 8px;">X</a>
		</div>
		<div id="noDataNumberofpubspracticeareaDiv" class="hideClass">0 Results, Please try a different search</div>
			<form:form commandName="firmSearchChartModelBean" method="post" action="${updateChartURL}" id="firmSearchChartModelBean">
				<div class="flRight charts" id="No-Data-noofPubsPractice">
					<ul class="reset listView">
						<c:if test="<%=isHomePage%>" >
							<li id="noofPubs_practice" style="overflow:hidden;"><a href="javascript:void(0);" class="filter-icon" >&nbsp;</a></li>
						</c:if>
						<li id="noofPubs_practice_settings"><a href="#firmSearchChartViewSettings"
							class="btn icon settingsgry rightViewSetting login-window chartViewSetting"
							onclick="return false;">&nbsp;</a></li>
						<li>
							<a href="javascript:void(0);" id="noOfPubsPracticePrintCharts" onclick="return false;" class="printChartClass"></a>
						</li>
						<li>
							<a href="javascript:void(0);" id="noOfPubsPracticeExportCharts" onclick="return false;" class="exportChartClass"></a>
                        <div class="actionSec">
                        <h5>Actions</h5>
                        <ul class="reset">
                            <li class="exportChartImage"><span id="noOfPubsPracticeExportJPG">Export as JPG</span></li>
                            <li class="exportChartImage"><span id="noOfPubsPracticeExportPNG">Export as PNG</span></li>
                        </ul>
                        <div class="clear">&nbsp;</div>
                        </div>
						</li>
					</ul>
					<div style="display: none" class="viewBox popusdiv ClickPopup" id="firmSearchChartViewSettings">
						<div class="popHeader">
							<a href="javascript:void(0);" class="btn icon closewhite closeOne flRight" style="margin-top: 2px">&nbsp;Close</a>
								SETTINGS: ${NumberOfPubsPracticeAreaChartTitleSettings}
							<div class="clear">&nbsp;</div>
						</div>
						<div class="section-one">
	                      <h6>Chart Type</h6>
	                        <ul class="reset list4">
								<li>
	                            <form:radiobutton path="chartType" class="graphType defaultChartType" value="<%=BaseChartModelBean.ChartType.VERTICAL_BAR.getValue()%>" />
	                            <span class="btn icon barchartvert">Vertical&nbsp;Bar</span>
	                           </li>
	                            <li>
	                            <form:radiobutton path="chartType" class="graphType" value="<%= BaseChartModelBean.ChartType.HORIZONTAL_BAR.getValue() %>" />
	                            <span class="btn icon barcharthori">Horizontal&nbsp;Bar</span>
	                           </li>
	                        </ul>
	                        <div class="clear">&nbsp;</div>
	                    </div>
						<div class="section-two">
							<form action="#">
								<h6>Practice Data (Limit of 15)</h6>
								<ul class="reset list4">
									<li>
										<form:radiobutton path="limitType" class="defaultLimitType" value="<%= BaseChartModelBean.FirmDataType.TOP_15.getValue() %>"/>
										<span class="">Top 15 By Net Change</span></li>
									<li>
										<form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.BOTTOM_15.getValue() %>"/>
										<span class="">Bottom 15 By Net Change</span></li>
									<li>
										<form:radiobutton path="limitType"  value="<%= BaseChartModelBean.FirmDataType.PRACTICE.getValue() %>"/>
										<span class="">Selected Practices:</span> 
										<form:select path="practiceAreaList" id="searchResultsPercentChangeFirmList" multiple="true" size="4" style="width:150px; margin:10px 0 0 0;">
											<form:options items="${practiceAreaListAll}"/>
										 </form:select>
									</li>
								</ul>
								<div class="clear">&nbsp;</div>
							</form>
						</div>
						<div class="section-three">
							<div class="martp2">
								<h6>Firm Data (Limit of 1)</h6>
								<ul class="reset list4">
									<li>
											<span class="">Selected Firms:</span> 
											<form:select path="searchResultsFirmList" id="firmSearchChartFirmList" multiple="false" size="1" style="width:150px; margin:10px 0 0 0;">
												<form:options items="${firmListAll}" itemLabel="company" itemValue="companyId"/>
											 </form:select>
										</li>
								</ul>
							</div>
							<div class="clear">&nbsp;</div>
						</div>
						<div class="clear">&nbsp;</div>
						<hr>
						<div class="btmdiv">
							<input type="button" value="Reset All" class="buttonTwo flLeft settingReset" id="noPubsByPracticeReset">
							<input type="button" class="buttonTwo flRight rightReset cancelNPP" value="Cancel">
							<input type="button" value="Apply" class="buttonOne flRight" id="NPPApply" style="margin: 0 5px 0 0;" onclick="applyChartSettings('#firmSearchChartModelBean');">
							<div class="clear">&nbsp;</div>
						</div>
					</div>
				</div>
			</form:form>
			
			<div id="noPubsPopupDiv" style="z-index: 999; width: 200px; position: absolute; background:#ffffff; padding: 10px 10px 10px 10px; border: 1px solid #1A1A1A; display: none;">
				<div id="noPubsPopupData" class="Popup-Tooltip"></div>
			</div>
		
			<div id="amountOfPubsPracticeAreaChartContainer" class="charts-spacing"></div>
	</div>
</div>