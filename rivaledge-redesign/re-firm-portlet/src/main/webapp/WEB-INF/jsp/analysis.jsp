<%@page import="com.alm.rivaledge.transferobject.FirmResultDTO"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<portlet:defineObjects/>


<portlet:resourceURL var="applySearchUrl" id="applySearchUrl" />

<script>
$(function() {

	var firmNameList = [];// List of selected firms name
	var dataTypeList = []; // List of content type of firms
	
	<%--
<%
	// Get result data from session of searched firms
	List<FirmResultDTO> firmResultsDTOList = (List<FirmResultDTO>)request.getSession().getAttribute("results");
	
	if(firmResultsDTOList != null && !firmResultsDTOList.isEmpty())
	{
		for(FirmResultDTO firmResultDTO :  firmResultsDTOList)
		{
%>
		// Put firms name and it's content type in list.
		firmNameList.push("<%=firmResultDTO.getFirmName()%>");
		dataTypeList.push("<%=firmResultDTO.getDataType()%>");
<%
		}
	}
	
%>
--%>

	<c:if test="${fn:length(searchResults) gt 0}">
		 <c:forEach items="${searchResults}" var="results"  varStatus="loopStatus">
		// Put firms name and it's content type in list.
			firmNameList.push("${results.firmName}");
			dataTypeList.push("${results.dataType}");
		 </c:forEach>
	</c:if>
	
	if(firmNameList.length > 0){
		$(".mart2").show();
		$(".colSetting").show();
		$(".newspublicationPage").show();
		$("#emptyDataMsg").hide();
		// Call stack graph function.
		displayStackGraph(firmNameList, dataTypeList, 'column', 50, "leftChart");
		// Call pie chart graph function.
		displayPieChart(firmNameList, dataTypeList, '', "rightChart");
	} else {
		$(".mart2").hide();
		$(".colSetting").hide();
		$("#emptyDataMsg").text("0 Results, Please try a different search");
		$("#totalListCount").text(0);
		$(".newspublicationPage").hide();
	}
	
	// On click of right side apply button of view setting popup depend on selected option respective function will be call.
	$("#rightChartSettingApply").click(function(){
		$("#login-box1").hide();
		if($('input[name=graphType2]:radio:checked').val() == 'stackVertical')
		{
			displayStackGraph(firmNameList, dataTypeList, 'column', 50, "rightChart");
		} else if($('input[name=graphType2]:radio:checked').val() == 'stackHorizontal')
		{
			displayStackGraph(firmNameList, dataTypeList, 'bar', 0, "rightChart");
		} else if($('input[name=graphType2]:radio:checked').val() == 'verticalBar')
		{
			displayBarGraph(firmNameList, dataTypeList, 'column', 50, "rightChart");
		} else if($('input[name=graphType2]:radio:checked').val() == 'HorizontalBar')
		{
			displayBarGraph(firmNameList, dataTypeList, 'bar', 0, "rightChart");
		} else if($('input[name=graphType2]:radio:checked').val() == 'pieChart')
		{
			displayPieChart(firmNameList, dataTypeList, '', "rightChart");
		} else 
		{
			displayStackGraph(firmNameList, dataTypeList, 'column', 50, "rightChart");
		}
		if($('input[name=columnType]:radio:checked').val() == 'one_column'){
			$('.dynamicDiv').css('float', 'none').css('margin-left', '25%');
			$(".leftDynamicDiv").css('float', 'none').css('margin-left', '25%');
			$(".dropdownarrow").css('float', 'none');
		} else {
			$('.dynamicDiv').css('float', 'right').css('margin-left', '0');
			$(".leftDynamicDiv").css('float', 'left').css('margin-left', '0');
			$(".dropdownarrow").css('float', 'right');
		}
		$(".colSetting").css("float", "right");
	});
	
	// On click of left side apply button of view setting popup depend on selected option respective function will be call.
	$("#leftChartSettingApply").click(function(){
				
		if($('input[name=graphType]:radio:checked').val() == 'stackVertical')
		{
			displayStackGraph(firmNameList, dataTypeList, 'column', 50, "leftChart");
		} else if($('input[name=graphType]:radio:checked').val() == 'stackHorizontal')
		{
			displayStackGraph(firmNameList, dataTypeList, 'bar', 0, "leftChart");
		} else if($('input[name=graphType]:radio:checked').val() == 'verticalBar')
		{
			displayBarGraph(firmNameList, dataTypeList, 'column', 50, "leftChart");
		} else if($('input[name=graphType]:radio:checked').val() == 'HorizontalBar')
		{
			displayBarGraph(firmNameList, dataTypeList, 'bar', 0, "leftChart");
		} else if($('input[name=graphType]:radio:checked').val() == 'pieChart')
		{
			displayPieChart(firmNameList, dataTypeList, '', "leftChart");
		} else 
		{
			displayStackGraph(firmNameList, dataTypeList, 'column', 50, "leftChart");
		}
		if($('input[name=columnType]:radio:checked').val() == 'one_column'){
			$('.dynamicDiv').css('float', 'none').css('margin-left', '25%');
			$(".leftDynamicDiv").css('float', 'none').css('margin-left', '25%');
			$(".dropdownarrow").css('float', 'none');
		} else {
			$('.dynamicDiv').css('float', 'right').css('margin-left', '0');
			$(".leftDynamicDiv").css('float', 'left').css('margin-left', '0');
			$(".dropdownarrow").css('float', 'right');
		}
		$(".colSetting").css("float", "right");
	});
		

	// HTML code from Rahul N.
	$('#btnAdd').click(function(){
		$('.filtersPage').hide();
		$('#additional').show();
	})
	$('#btnSave').click(function(){
		$('.filtersPage').show();
		$('#additional').hide();
	});
	
	$('#hide').click(
    function () {
        //show its submenu
        $("#firmPage").stop().slideToggle(500);    
    });
	
	$('#datenone').click(
    function () {
        //show its submenu
        $("#datePage").stop().slideToggle(500);    
    });
	
	
	
	
	//Hide-Show Add Analysis Pop-up
	$('.mybutton').hover(function(){
		$('.selectchartadd').show();
	}, function(){
		$('.selectchartadd').hide();
	});
	$('.selectchartadd').hover(function(){
		$('.selectchartadd').show();
	}, function(){
		$('.selectchartadd').hide();
	});
	
	$(".percNewsNpubs").click(function(){
		if($(".dynamicDiv").css('display') == 'none'){
			$(".dynamicDiv").show();
		} else {
			$(".dynamicDiv").hide();
		}
		if($('input[name=columnType]:radio:checked').val() == 'one_column'){
			$('.dynamicDiv').css('float', 'none').css('margin-left', '25%');
			$(".leftDynamicDiv").css('float', 'none').css('margin-left', '25%');
			$(".dropdownarrow").css('float', 'none');
		} else {
			$('.dynamicDiv').css('float', 'right').css('margin-left', '0');
			$(".leftDynamicDiv").css('float', 'left').css('margin-left', '0');
			$(".dropdownarrow").css('float', 'right');
		}
		$(".colSetting").css("float", "right");
	});
	
	
	$(".amntNewsNpubs").click(function(){
		if($(".leftDynamicDiv").css('display') == 'none'){
			$(".leftDynamicDiv").show();
		} else {
			$(".leftDynamicDiv").hide();
		}
		if($('input[name=columnType]:radio:checked').val() == 'one_column'){
			$('.dynamicDiv').css('float', 'none').css('margin-left', '25%');
			$(".leftDynamicDiv").css('float', 'none').css('margin-left', '25%');
			$(".dropdownarrow").css('float', 'none');
		} else {
			if($(".leftDynamicDiv").css('display') == 'none')
			{
				$('.dynamicDiv').css('float', 'left').css('margin-left', '0');
			}
			else
			{
				$('.dynamicDiv').css('float', 'right').css('margin-left', '0');
			}
			$(".leftDynamicDiv").css('float', 'left').css('margin-left', '0');
			$(".dropdownarrow").css('float', 'right');
		}
		$(".colSetting").css("float", "right");
	});


	
	
	$("#login-box2").hide();
	
	
	$('.rightViewSetting').click(function() {
		$('#login-box2').toggleClass('hover').show();
	}).hover(function() {
		$('#login-box2').fadeIn(100);
	}, function() {
		$('#login-box2').fadeOut(100);
	});
	
	$('#login-box2').click(function() {
		$('#login-box2').toggleClass('hover').show();
	}).hover(function() {
		$('#login-box2').fadeIn(100);
	}, function() {
		$('#login-box2').fadeOut(100);
	});
	
	
	
	// When user select 1 or 2 column option and click on apply button layout will change accordingly.
	$("#viewSettingColumnApply").click(function(){

		if($('input[name=columnType]:radio:checked').val() == 'one_column'){
			$('.dynamicDiv').css('float', 'none').css('margin-left', '25%');
			$(".leftDynamicDiv").css('float', 'none').css('margin-left', '25%');
			$(".dropdownarrow").css('float', 'none');
		} else {
			$('.dynamicDiv').css('float', 'right').css('margin-left', '0');
			$(".leftDynamicDiv").css('float', 'left').css('margin-left', '0');
			$(".dropdownarrow").css('float', 'right');
		}
		$(".colSetting").css("float", "right");
	});
	
	$(".clickToViewDetails").click(function(){
		$("#hiddenField").val(true);
		$.ajax({
			url:"<%=applySearchUrl.toString()%>",
			method: "GET",
			data: {
				"selectedFirms":$("#selectedFirms").val(),
				"Datetext":$("#datetext").val(),
				"contentType":$("#contentType").val(),
				"keywords":$("#keywords").val(),
				"selectedTwitterPostType":$("#selectedTwitterPostType").val(),
				"selectedPracticeArea":$("#selectedPracticeArea").val(),
				"reloadData":true
				},
			success: function(data)
			{
				history.back();
			},
			error: function(jqXHR, textStatus, errorThrown) {
				alert("error:" + textStatus + " - exception:" + errorThrown);
			}
		});
		
	});
	
	
	// Reset all previously selection option when user click on Reset All.
	$(".settingReset").click(function(){
		$("#leftGraphDefaultChecked").prop('checked', true);
		$("#topTen").prop('checked', true);
		
		$('.myFirm').attr('checked', true);
		$(".amLaw100AVg").attr('checked', true);
		$(".popupSelectedFirms > option:selected").each(function(){
			$(this).attr('selected',false);
		});
		displayStackGraph(firmNameList, dataTypeList, 'column', 50, "leftChart");
		
	});
	$(".layoutSettReset").click(function(){
		// Reset radio button
		$("#twoColumn").prop('checked', true);
	});
	
	$('input[value=two_column]:radio').attr('checked', true);
	
	// Default graph will be shown when user click on Reset All on right side popup.
	$(".rightReset").click(function(){
		$('input[id=rightGraphDefaultChecked]:radio').prop('checked', true);
		displayPieChart(firmNameList, dataTypeList, '', "rightChart");
	});
	
	var filterList = RemoveDuplicateValue(firmNameList);
	// Creating dropdown of firms list in popup.
	var selectFirms = "<select multiple='multiple' class='popupSelectedFirms'>";
	for(var index=0; index<filterList.length; index++)
	{
		selectFirms += "<option value='"+filterList[index]+"'>"+filterList[index]+"</option>";
	}
	selectFirms += "</select>";
	$("#selected_firms").html(selectFirms);
	$(".popupSelectedFirms").prop("disabled", true);
	
	$(".sorting").click(function(){
		if($('input[name=sorting]:radio:checked').val() == 'selected_firm')
		{
			$(".popupSelectedFirms").prop("disabled", false);
		}
		else
		{
			$(".popupSelectedFirms").prop("disabled", true);
		}
	});
	
	$(".typeSel").click(function(){
		$("div#individualFirmDetailsDiv").hide();
		$("div#popupPieChartDiv").hide();
	});
	 
	$(".drpDwn").click(function(){
		$("div#individualFirmDetailsDiv").hide();
		$("div#popupPieChartDiv").hide();
	});
	$(".viewSetting").click(function(){
		$(".columnSet").show();
	}).hover(function() {
		$('.columnSet').fadeIn(100);
	}, function() {
		$('.columnSet').fadeOut(100);
		var leftMargin = $(".leftDynamicDiv").css('margin-left');
		
		if(leftMargin == '300px' && $('input[name=columnType]:radio:checked').val() == 'two_column'){
			$("#oneColumn").prop("checked", true);
		} else if(leftMargin == '0px' && $('input[name=columnType]:radio:checked').val() == 'one_column'){
			$("#twoColumn").prop("checked", true);
		}
	});
	
});

/*
 * It will remove duplicate value from list.
 */
function RemoveDuplicateValue(arrayval) {
	var _array1 = new Array();
    for (i = 0; i < arrayval.length; i++) {
    	if (!CheckDuplicateStatus(_array1, arrayval[i])) {
        	_array1.length += 1;
            _array1[_array1.length - 1] = arrayval[i];
		}
	}
    return _array1;
}

/*
 * Check for duplicate value in list.
 * if yes return true else false.
 */
function CheckDuplicateStatus(arrayval, e) {
	for (j = 0; j < arrayval.length; j++) if (arrayval[j] == e) return true;
    return false;
}


/*
 * displayStackGraph function will generate stack graph using available firms and data type.
 */
function displayStackGraph(firmNameList, dataTypeList, type, y, chartPosition){
	// Initialize count variables and list.
	var newsCount = 0;
	var twitterCount = 0;
	var pubCount = 0;
	var newCountList = [];
	var twitterCountList = [];
	var pubCountList = [];
	var filterList = [];
	var totalCount = [];
	
	// Duplicate firm names will be remove and put in filter list.
	filterList = RemoveDuplicateValue(firmNameList);
	$("#totalListCount").text(dataTypeList.length);
	
	var filterList2 = [];
	
	if($(".myFirm").is(":checked")){
		filterList2.push("My Firm");
		newCountList.push(5);
		twitterCountList.push(5);
		pubCountList.push(5);
		totalCount.push(15);
	} else {
		filterList2.push("");
		newCountList.push(0);
		twitterCountList.push(0);
		pubCountList.push(0);
		totalCount.push(0);
	}
	
	if($(".amLaw100AVg").is(":checked") || $(".watchListAvg").is(":checked"))
	{	
		if($(".amLaw100AVg").is(":checked"))
		{
			filterList2.push("AmLaw100 Average");
		} else if($(".watchListAvg").is(":checked"))
		{
			filterList2.push("Watch List Average");
		}		
		newCountList.push(5);
		twitterCountList.push(5);
		pubCountList.push(5);
		totalCount.push(15);
	} 
	else 
	{
		filterList2.push("");
		newCountList.push(0);
		twitterCountList.push(0);
		pubCountList.push(0);
		totalCount.push(0);
	}
		
	
	for(var i=0; i<filterList.length; i++){
		filterList2.push(filterList[i]);
	}
	
	
	var firmNamesList = [];
	var indexList = [];
	
	// Check for selected firms from popup and put in list.
	$(".popupSelectedFirms > option:selected").each(function(){
		firmNamesList.push($(this).text());
	});
	
	
	// Count news, publication, twitter count.
	for(var i=0; i<filterList.length; i++){
		newsCount = 0;
		twitterCount = 0;
		pubCount = 0;
		for(var j=0; j<firmNameList.length; j++){
			if(firmNameList[j] == filterList[i]){
				if(dataTypeList[j]=='News'){
					newsCount++;
				} else if(dataTypeList[j] == 'Twitter'){
					twitterCount++;
				} else {
					pubCount++;
				}
			}
		}
		newCountList.push(newsCount);
		twitterCountList.push(twitterCount);
		pubCountList.push(pubCount);
		totalCount.push(parseInt(newsCount)+parseInt(twitterCount)+parseInt(pubCount));
	}
		
	sorting(filterList2, totalCount, totalCount.length, 0);
	
	if($('input[name=sorting]:radio:checked').val() == 'top_ten' && chartPosition == "leftChart"){
		sorting(filterList2, totalCount, totalCount.length, 0);
	}

	if($('input[name=sorting]:radio:checked').val() == 'bottom_ten' && chartPosition == "leftChart"){
		sorting(filterList2, totalCount, totalCount.length, 1);
	}
	
	function sorting(filterList2, countList, n, m){
		for(var x=2; x<n; x++){
			var index_of_min = x;
			for(var y=x; y<n; y++){
				if(parseInt(m)==0){
					if(countList[index_of_min]<countList[y]){
				  		index_of_min = y;
					}
				} else {
					if(countList[index_of_min]>countList[y]){
				  		index_of_min = y;
					}
				}				
			}
			var temp = countList[x];
			countList[x] = countList[index_of_min];
			countList[index_of_min] = temp;
			
			var temp1 = filterList2[x];
			filterList2[x] = filterList2[index_of_min];
			filterList2[index_of_min] = temp1;
			
			
			var temp1 = newCountList[x];
			newCountList[x] = newCountList[index_of_min];
			newCountList[index_of_min] = temp1;
			
			
			var temp1 = twitterCountList[x];
			twitterCountList[x] = twitterCountList[index_of_min];
			twitterCountList[index_of_min] = temp1;
			
			
			var temp1 = pubCountList[x];
			pubCountList[x] = pubCountList[index_of_min];
			pubCountList[index_of_min] = temp1;
			
			
		}
	}
	// Find out index of selected firm from original list.
	if(firmNamesList.length > 0 && chartPosition == "leftChart"){
		for(var index=0; index<filterList2.length; index++){
			if(filterList2.indexOf(firmNamesList[index]) > 0){
				indexList.push(filterList2.indexOf(firmNamesList[index]));
			}
		}
	}
	
	var topFilterList = [];
	var topNewsList = [];
	var topPubList = [];
	var topTwitterList = [];
	var limit = 0;
	if(filterList2.length < 10){
		limit = filterList2.length;
	} else {
		limit = 12;
	}
	
	if(indexList.length > 0){
		topNewsList.push(50);
		topNewsList.push(50);
		
		topPubList.push(50);
		topPubList.push(50);
		
		topTwitterList.push(50);
		topTwitterList.push(50);
		
		totalCount.push(150);
		totalCount.push(150);
		
		topFilterList.push("WatchList 1");
		topFilterList.push("WatchList 2");
		
		for(var index=0; index< indexList.length; index++){
			topFilterList.push(filterList2[indexList[index]]);
			topNewsList.push(newCountList[indexList[index]]);
			topPubList.push(pubCountList[indexList[index]]);
			topTwitterList.push(twitterCountList[indexList[index]]);
		}
	} else {
		for(var index=0; index<limit; index++){
			
			topFilterList.push(filterList2[index]);
			topNewsList.push(newCountList[index]);
			topPubList.push(pubCountList[index]);
			topTwitterList.push(twitterCountList[index]);
			
		}
	}
	
	
	
	if(chartPosition == "leftChart"){
		chartPosition = '#leftChart';
	} else {
		chartPosition = '#rightChart';
	}
	
	
	$(chartPosition).highcharts({
        chart: {
            type: type
        },
        title: {
            text: 'AMOUNT OF NEWS AND PUBLICATIONS BY FIRM'
        },
        xAxis: {
            categories: topFilterList,
            labels: {
                rotation: -45,
                y: 30,
				x: -15,
                formatter: function(){
                	var firmName = this.value;
                	if(this.value.length > 10)
                		firmName = $.trim(this.value).substring(0, 10)+ "...";
                	return firmName;
                }
			},
			plotLines: [{
                color: '#000000',
                width: 2,
                dashStyle: 'Solid',
                value: parseFloat(1.5)
            }]
        },
        yAxis: {
            min: 0,
            
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
        	enabled: true
    	},
        tooltip: {
            formatter: function() {
            	displayStackPopup(topFilterList, newCountList, twitterCountList, pubCountList, this.x, this.y);
                return false;
            }
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                dataLabels: {
                    enabled: true,
                    color: '#000000',
                    formatter: function(){
                    	if(this.y > 0){
                    		return this.y;
                    	}
                    }
                },
                point: {
                    events: {
                        mouseOver: function(e) {
                        	var xPos = e.target.plotX + 40;
                        	var yPos = e.target.plotY + 237;
                        	
                       	 	$("div#individualFirmDetailsDiv").css('top', yPos).css('left', xPos);
                            $("div#individualFirmDetailsDiv").show();
							/*Code written by Anurag starts*/
							$(document).click(function() {
								$("div#individualFirmDetailsDiv").hide();
							});
							/*Code written by Anurag ends*/
                        }
                    }
                    
                }
            }
        },
        series: [{
            name: 'News',
            color: '#446e49',
            data: topNewsList
        }, {
            name: 'Twitter',
            color: '#0c3362',
            data: topTwitterList
        }, {
            name: 'Publication',
            color: '#b89024',
            data: topPubList
        }]
    });
}

/*
 * displayStackPopup function will be call when user mouseover stack column,
 * It will show details value of selected stack column
 */
function displayStackPopup(filterList, newCountList, twitterCountList, pubCountList, x, y){
	$("#individualFirmDetails").show();
	for(var index=0; index<filterList.length; index++){
		if(filterList[index] == x){
			$("#hoverFirmName").text(x);
			$("#totalColor").text(parseInt(newCountList[index])+parseInt(twitterCountList[index])+parseInt(pubCountList[index]));
			$("#newsColor").text(parseInt(newCountList[index]));
			$("#twitterColor").text(parseInt(twitterCountList[index]));
			$("#publicationColor").text(parseInt(pubCountList[index]));
		}
	}	
}

/*
 * displayBarGraph function will generate 'bar' or 'column' graph using available firms name and datatype.
 */
function displayBarGraph(firmNameList, dataTypeList, type, y, chartPosition){
	var newsCount =0;
	var twitterCount =0;
	var pubCount =0;
	var newCountList = [];
	var twitterCountList = [];
	var pubCountList = [];
	var filterList = [];
	var totalCount = [];
	
	// Duplicate firm names will be remove and put in filter list.
	filterList = RemoveDuplicateValue(firmNameList);
	
	
	
	
	var filterList2 = [];
	
	if($(".myFirm").is(":checked")){
		filterList2.push("My Firm");
		newCountList.push(50);
		twitterCountList.push(50);
		pubCountList.push(50);
		totalCount.push(150);
	} else {
		filterList2.push("");
		newCountList.push(0);
		twitterCountList.push(0);
		pubCountList.push(0);
		totalCount.push(0);
	}
	
	if($(".amLaw100AVg").is(":checked") || $(".watchListAvg").is(":checked"))
	{	
		if($(".amLaw100AVg").is(":checked"))
		{
			filterList2.push("AmLaw100 Average");
		} else if($(".watchListAvg").is(":checked"))
		{
			filterList2.push("Watch List Average");
		}		
		newCountList.push(50);
		twitterCountList.push(50);
		pubCountList.push(50);
		totalCount.push(150);
	} 
	else 
	{
		filterList2.push("");
		newCountList.push(0);
		twitterCountList.push(0);
		pubCountList.push(0);
		totalCount.push(0);
	}
	
	for(var i=0; i<filterList.length; i++){
		filterList2.push(filterList[i]);
	}
	
	
	var firmNamesList = [];
	var indexList = [];
	
	// Check for selected firms from popup and put in list.
	$(".popupSelectedFirms > option:selected").each(function(){
		firmNamesList.push($(this).text());
	});
	
	// Find out index of selected firm from original list.
	if(firmNamesList.length > 0 && chartPosition == "leftChart"){
		for(var index=0; index<filterList2.length; index++){
			if(filterList2.indexOf(firmNamesList[index]) > 0){
				indexList.push(filterList2.indexOf(firmNamesList[index]));
			}
		}
	}
	
	// Count news, publication, twitter count.
	for(var i=0; i<filterList.length; i++){
		newsCount = 0;
		twitterCount = 0;
		pubCount = 0;
		for(var j=0; j<firmNameList.length; j++){
			if(firmNameList[j] == filterList[i]){
				if(dataTypeList[j]=='News'){
					newsCount++;
				} else if(dataTypeList[j] == 'Twitter'){
					twitterCount++;
				} else {
					pubCount++;
				}
			}
		}
		newCountList.push(newsCount);
		twitterCountList.push(twitterCount);
		pubCountList.push(pubCount);
		totalCount.push(parseInt(newsCount)+parseInt(twitterCount)+parseInt(pubCount));
	}
	
	// Sort data based on selected option.
	sorting(filterList2, totalCount, totalCount.length, 0);
	 
	if($('input[name=sorting]:radio:checked').val() == 'top_ten' && chartPosition == "leftChart")
	{
		sorting(filterList2, totalCount, totalCount.length, 0);
	}
		
	if($('input[name=sorting]:radio:checked').val() == 'bottom_ten' && chartPosition == "leftChart")
	{
		sorting(filterList2, totalCount, totalCount.length, 1);
	}
	
	function sorting(filterList2, countList, n, m){
		for(var x=2; x<n; x++){
			var index_of_min = x;
			for(var y=x; y<n; y++){
				if(parseInt(m)==0){
					if(countList[index_of_min]<countList[y]){
				  		index_of_min = y;
					}
				} else {
					if(countList[index_of_min]>countList[y]){
				  		index_of_min = y;
					}
				}				
			}
			var temp = countList[x];
			countList[x] = countList[index_of_min];
			countList[index_of_min] = temp;
			
			var temp1 = filterList2[x];
			filterList2[x] = filterList2[index_of_min];
			filterList2[index_of_min] = temp1;
			
			
			var temp1 = newCountList[x];
			newCountList[x] = newCountList[index_of_min];
			newCountList[index_of_min] = temp1;
			
			
			var temp1 = twitterCountList[x];
			twitterCountList[x] = twitterCountList[index_of_min];
			twitterCountList[index_of_min] = temp1;
			
			
			var temp1 = pubCountList[x];
			pubCountList[x] = pubCountList[index_of_min];
			pubCountList[index_of_min] = temp1;			
		}
	}
	
	
	var topFilterList = [];
	var topNewsList = [];
	var topPubList = [];
	var topTwitterList = [];
	var limit = 0;
	if(filterList2.length < 10){
		limit = filterList2.length;
	} else {
		limit = 12;
	}
	
	if(indexList.length > 0){
		topNewsList.push(50);
		topNewsList.push(50);
		
		topPubList.push(50);
		topPubList.push(50);
		
		topTwitterList.push(50);
		topTwitterList.push(50);
		
		totalCount.push(150);
		totalCount.push(150);
		
		topFilterList.push("WatchList 1");
		topFilterList.push("WatchList 2");
		
		for(var index=0; index< indexList.length; index++){
			topFilterList.push(filterList2[indexList[index]]);
			topNewsList.push(newCountList[indexList[index]]);
			topPubList.push(pubCountList[indexList[index]]);
			topTwitterList.push(twitterCountList[indexList[index]]);
		}
	} else {
		for(var index=0; index<limit; index++){
			
			topFilterList.push(filterList2[index]);
			topNewsList.push(newCountList[index]);
			topPubList.push(pubCountList[index]);
			topTwitterList.push(twitterCountList[index]);
			
		}
	}
	
	if(chartPosition == "leftChart"){
		chartPosition = '#leftChart';
	} else {
		chartPosition = '#rightChart';
	}
	
	$(chartPosition).highcharts({
        chart: {
            type: type
        },
        title: {
            text: 'AMOUNT OF NEWS AND PUBLICATIONS BY FIRM'
        },
        xAxis: {
            categories: topFilterList,
            labels: {
                rotation: -45,
                y: 30,
				x: -15,
			},
			plotLines: [{
                color: '#000000',
                width: 2,
                dashStyle: 'Solid',
                value: parseFloat(1.5)
            }]
        },
        yAxis: {
            min: 0,
            
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
        	enabled: true
    	},
        tooltip: {
        	formatter: function() {
            	displayStackPopup(topFilterList, newCountList, twitterCountList, pubCountList, this.x, this.y);
                return false;
            }
        },
        plotOptions: {
        	column: {
                
                dataLabels: {
                    enabled: true,
                    color: '#000000',
                    formatter: function(){
                    	if(this.y > 0){
                    		return this.y;
                    	}
                    }
                },
                point: {
                    events: {
                        mouseOver: function(e) {
                        	var xPos = e.target.plotX + 40;
                        	var yPos = e.target.plotY + 237;
                        	
                       	 	$("div#individualFirmDetailsDiv").css('top', yPos).css('left', xPos);
                            $("div#individualFirmDetailsDiv").show();
							/*Code written by Anurag starts*/
							$(document).click(function() {
								$("div#individualFirmDetailsDiv").hide();
							});
							/*Code written by Anurag ends*/
                        }
                    }
                    
                }
            }
        },
        series: [{
            name: 'News',
            color: '#446e49',
            data: topNewsList
        }, {
            name: 'Twitter',
            color: '#0c3362',
            data: topTwitterList
        }, {
            name: 'Publication',
            color: '#b89024',
            data: topPubList
        }]
    });
}

/*
 * displayPieChart function will generate pie chart.
 */
function displayPieChart(firmNameList, dataTypeList, type, chartPosition){
	var newsCount =0;
	var twitterCount =0;
	var pubCount =0;
	var newCountList = [];
	var twitterCountList = [];
	var pubCountList = [];
	var filterList = [];
	var totalCount = [];
		
	
	filterList = RemoveDuplicateValue(firmNameList);
	

	 for(var i=0; i<filterList.length; i++){
		newsCount = 0;
		twitterCount = 0;
		pubCount = 0;
		for(var j=0; j<firmNameList.length; j++){
			if(firmNameList[j] == filterList[i]){
				if(dataTypeList[j]=='News'){
					newsCount++;
				} else if(dataTypeList[j] == 'Twitter'){
					twitterCount++;
				} else {
					pubCount++;
				}
			}
		}
		newCountList.push(newsCount);
		twitterCountList.push(twitterCount);
		pubCountList.push(pubCount);
		totalCount.push(parseInt(newsCount)+parseInt(twitterCount)+parseInt(pubCount));
	}
	
	var filterList2 = [];
	
	for(var i=0; i<filterList.length; i++){
		filterList2.push(filterList[i]);
	}
	
	// Sort data based on selected option.
	sorting(filterList2, totalCount, totalCount.length, 0);
	 
	if($('input[name=sorting]:radio:checked').val() == 'top_ten' && chartPosition == "leftChart")
	{
		sorting(filterList2, totalCount, totalCount.length, 0);
	}
		
	if($('input[name=sorting]:radio:checked').val() == 'bottom_ten' && chartPosition == "leftChart")
	{
		sorting(filterList2, totalCount, totalCount.length, 1);
	}
	
	function sorting(filterList2, countList, n, m){
		for(var x=2; x<n; x++){
			var index_of_min = x;
			for(var y=x; y<n; y++){
				if(parseInt(m)==0){
					if(countList[index_of_min]<countList[y]){
				  		index_of_min = y;
					}
				} else {
					if(countList[index_of_min]>countList[y]){
				  		index_of_min = y;
					}
				}				
			}
			var temp = countList[x];
			countList[x] = countList[index_of_min];
			countList[index_of_min] = temp;
			
			var temp1 = filterList2[x];
			filterList2[x] = filterList2[index_of_min];
			filterList2[index_of_min] = temp1;
			
			
			var temp1 = newCountList[x];
			newCountList[x] = newCountList[index_of_min];
			newCountList[index_of_min] = temp1;
			
			
			var temp1 = twitterCountList[x];
			twitterCountList[x] = twitterCountList[index_of_min];
			twitterCountList[index_of_min] = temp1;
			
			
			var temp1 = pubCountList[x];
			pubCountList[x] = pubCountList[index_of_min];
			pubCountList[index_of_min] = temp1;
		}
	}
	
	var topFilterList = [];
	var topNewsList = [];
	var topPubList = [];
	var topTwitterList = [];
	var limit = 0;
	if(filterList2.length < 10){
		limit = filterList2.length;
	} else {
		limit = 10;
	}
	for(var index=0; index<limit; index++){
		topFilterList.push(filterList2[index]);
		topNewsList.push(newCountList[index]);
		topPubList.push(pubCountList[index]);
		topTwitterList.push(twitterCountList[index]);
	}
	
	var pieChartData = [];
	
	for(var index=0; index<limit; index++){
		var singlePieChartData = [];
		singlePieChartData.push(topFilterList[index]);
		singlePieChartData.push(parseInt(topNewsList[index])+parseInt(topTwitterList[index])+parseInt(topPubList[index]));
		
		pieChartData.push(singlePieChartData);
	}
	if(chartPosition == "leftChart"){
		chartPosition = '#leftChart';
	} else {
		chartPosition = '#rightChart';
	}
	
	 $(chartPosition).highcharts({
         chart: {
             plotBackgroundColor: null,
             plotBorderWidth: null,
             plotShadow: false
         },
         title: {
             text: 'PERCENTAGE OF NEWS AND PUBLICATIONS'
         },
         tooltip: {
     	    pointFormat: '{series.name}: <b>{point.percentage}%</b>',
         	percentageDecimals: 1,
         	formatter: function(e){
         		var xPosition = e.chart.pointer.chartPosition.left + 0;
           	 	$("div#popupPieChartDiv").css('top', '-125px').css('left', xPosition).css('z-index', '99999').css('width', '250px');
         		popupPieChart(firmNameList, dataTypeList, this.key);
         		return false;
         	}
         },
         plotOptions: {
             pie: {
                 allowPointSelect: true,
                 cursor: 'pointer',
                 dataLabels: {
                	 distance: -30,
                     enabled: true,
                     color: '#ffffff',
                     formatter: function(){
                         return parseFloat(this.percentage).toFixed(0)+"%";
                     }
                 },
                 showInLegend: true,
                 point: {
                     events: {
                         mouseOver: function(e) {
                        	 
                             $("#popupPieChart").show();
							 
							 /*Code written by Anurag starts*/
							 $("#popupPieChartDiv").show();
							 $(document).click(function() {
							$("div#popupPieChartDiv").hide();
							/*Code written by Anurag ends*/
});
                         }
                     }
                     
                 }
             }
         },
         legend: {
             enabled: true,
             layout: 'vertical',
             align: 'right',
             width: 200,
             verticalAlign: 'middle',
             useHTML: true,
             labelFormatter: function() {
                 return '<div style="text-align: left; width:130px;">' + this.name + '</div>';
             }
         },
         series: [{
             type: 'pie',
             name: 'PERCENTAGE OF NEWS AND PUBLICATIONS',
             data: pieChartData
         }]
     });
}

/*
 * popupPieChart function will generate pie chart in popup when user mouseover perticular section of base pie chart.
 */
function popupPieChart(firmNameList, dataTypeList, firmName){
	var newsCount =0;
	var twitterCount =0;
	var pubCount =0;
	var newCountList = [];
	var twitterCountList = [];
	var pubCountList = [];
	var filterList = [];
	
	filterList = RemoveDuplicateValue(firmNameList);

	 for(var i=0; i<filterList.length; i++){
		newsCount = 0;
		twitterCount = 0;
		pubCount = 0;
		for(var j=0; j<firmNameList.length; j++){
			if(firmNameList[j] == filterList[i]){
				if(dataTypeList[j]=='News'){
					newsCount++;
				} else if(dataTypeList[j] == 'Twitter'){
					twitterCount++;
				} else {
					pubCount++;
				}
			}
		}
		newCountList.push(newsCount);
		twitterCountList.push(twitterCount);
		pubCountList.push(pubCount);
	}
	
	var firmNamePosition = 0;
	for(var index=0; index<filterList.length; index++){
		if(filterList[index] == firmName){
			firmNamePosition = index;
		}
	}

	var overall = parseInt(newCountList[firmNamePosition])+parseInt(pubCountList[firmNamePosition])+parseInt(twitterCountList[firmNamePosition]);
	var popupDetails = "<table><tr><th colspan='4' style='text-align: center;'>"+firmName+"</th></tr><tr><th style='padding:2px'>Overall</th><th style='padding:2px'>News</th><th style='padding:2px'>Publications</th><th style='padding:2px'>Twitter</th></tr><tr style='line-height:35px;'><td style='background:#cccccc; text-align:center; border:1px solid #333'>"+overall+"</td><td style='text-align:center; border:1px solid #333'>"+newCountList[firmNamePosition]+"</td><td style='text-align:center; border:1px solid #333'>"+pubCountList[firmNamePosition]+"</td><td style='text-align:center; border:1px solid #333'>"+twitterCountList[firmNamePosition]+"</td></tr></table>";
	$("#popupDetails").html(popupDetails);
	
	 $('#popupPieChart').highcharts({
         chart: {
             plotBackgroundColor: null,
             plotBorderWidth: null,
             plotShadow: false,
             width: 250,
             height: 250
         },
         title: {
             text: ''
         },
         tooltip: {
     	    pointFormat: '{series.name}: <b>{parseFloat(point.percentage).toFixed(0)}%</b>',
         	percentageDecimals: 1
         },
         plotOptions: {
             pie: {
                 allowPointSelect: true,
                 cursor: 'pointer',
                 dataLabels: {
                	 distance: -30,
                     enabled: true,
                     color: '#ffffff',
                     formatter: function(){
                         return parseFloat(this.percentage).toFixed(0)+"%";
                     }
                 },
                 showInLegend: true
             }
         },
         series: [{
             type: 'pie',
             name: 'PERCENTAGE OF NEWS AND PUBLICATIONS',
             data: [
                    ['News', newCountList[firmNamePosition]],
                    ['Twitter', twitterCountList[firmNamePosition]],
                    ['Publication', pubCountList[firmNamePosition]]
                   ]
         }],
         exporting: {
             enabled: false
         }
     });
}


</script>

<div id="tab3" style="position: relative">
	<h4 class="heading">Analyze Results</h4>
	<div class="marbtm2 martp3">
		<div class="flLeft mart2" style="position: relative;">
			<button class="buttonTwo mybutton" onclick="return false;">
				<span class="btn icon drpdwngry"><span
					class="btn icon addgry">Add Analysis</span></span>
			</button>
			<div class="selectchartadd hover" style="display: none; z-index: 999;">
				<h6>Select a chart to add to your analysis</h6>
				<ul class="reset list3">
					<li>
						<div class="imgdiv amntNewsNpubs"><img alt="" src="<%=request.getContextPath()%>/images/netchart.gif" height="100%" width="100%"></div>
						<div class="disc amntNewsNpubs">
							<h4>Amount Of News And Publications By Firm</h4>
							<p>Description of the chart Description of the chart</p>
						</div>
					</li>
					<li>
						<div class="imgdiv percNewsNpubs"><img alt="" src="<%=request.getContextPath()%>/images/percent-image.gif" height="100%" width="100%"></div>
						<div class="disc percNewsNpubs">
							<h4>Percentage Of News And Publications</h4>
							<p>Description of the chart Description of the chart</p>
						</div>
					</li>
				</ul>
				<div class="clear">&nbsp;</div>
			</div>
		</div>
	<%-- 	<div class="flLeft resultsec martp1">
		<% if(firmResultsDTOList!= null && firmResultsDTOList.size() > 0) {%>
			<strong><%= firmResultsDTOList.size() %> Results</strong> <span>(All Firms, Select
				Practice Areas, Last 30 days)</span>
				<% } %>
		</div> --%>
		<div class="flLeft resultsec"><strong><span id="totalListCount"></span> Results</strong> 
  		 <%-- old code
  		 
  		 <span>(
          		<c:choose>
	      				 <c:when test="${fn:length(selectedFirms) gt 100}">
	      				 	${fn:substring(selectedFirms, 0, 99)}..,&nbsp;
	      				 </c:when>
	      				 <c:otherwise>
	      				 	${selectedFirms},&nbsp;
	      				 </c:otherwise>		        		  
				</c:choose>				
	      		${contentType},&nbsp;	      				
				${Datetext},&nbsp;  
				<c:choose>
	      				 <c:when test="${fn:length(keywords) gt 100}">
	      				 	${fn:substring(keywords, 0, 99)}..,&nbsp;
	      				 </c:when>
	      				  <c:otherwise>
	      				 	<c:if test="${not empty keywords}">
	      				 		${keywords}, 
	      				 	</c:if>
	      				 </c:otherwise>		        		  
				</c:choose>
				<c:choose>
	      				 <c:when test="${fn:length(selectedPracticeArea) gt 100}">
	      				 	${fn:substring(selectedPracticeArea, 0, 99)}..,&nbsp;
	      				 </c:when>
	      				 <c:otherwise>
	      				 	${selectedPracticeArea},&nbsp;
	      				 </c:otherwise>		        		  
				</c:choose>      				 
	      				${selectedTwitterPostType}&nbsp;	      		
          )</span>
  		--%>
  		
  		<span>(
          		<c:choose>
	      				 <c:when test="${fn:length(firmSearchModelBean.selectedFirms) gt 100}">
	      				 	${fn:substring(firmSearchModelBean.selectedFirms, 0, 99)}..,&nbsp;
	      				 </c:when>
	      				 <c:otherwise>
	      				 	${firmSearchModelBean.selectedFirms},&nbsp;
	      				 </c:otherwise>		        		  
				</c:choose>				
	      		${firmSearchModelBean.selectedContentType},&nbsp;	      				
				 ${firmSearchModelBean.dateText},&nbsp;  
				<c:choose>
	      				 <c:when test="${fn:length(firmSearchModelBean.keywords) gt 100}">
	      				 	${fn:substring(firmSearchModelBean.keywords, 0, 99)}..,&nbsp;
	      				 </c:when>
	      				  <c:otherwise>
	      				 	<c:if test="${not empty firmSearchModelBean.keywords}">
	      				 		${firmSearchModelBean.keywords}, 
	      				 	</c:if>
	      				 </c:otherwise>		        		  
				</c:choose>
				<c:choose>
	      				 <c:when test="${fn:length(firmSearchModelBean.selectedPracticeArea) gt 100}">
	      				 	${fn:substring(firmSearchModelBean.selectedPracticeArea, 0, 99)}..,&nbsp;
	      				 </c:when>
	      				 <c:otherwise>
	      				 	${firmSearchModelBean.selectedPracticeArea},&nbsp;
	      				 </c:otherwise>		        		  
				</c:choose>      				 
	      				${firmSearchModelBean.selectedTwitterPostType}&nbsp;	      		
          )</span>
          
          
  		</div>
  		<br>
  		<div id="emptyDataMsg"></div>
  		
		<div class="viewSetting flRight  colSetting">
			<a class="btn icon settings" href="#"><span><span
					class="btn icon dropdownarrow">View Settings</span></span></a>
			<div class="viewBox columnSet">
				<h6>Layout</h6>
				<form action="#">
					<ul class="reset list2">
						<li><input type="radio" name="columnType" value="one_column" class="column" id="oneColumn">
							<span class="btn icon onecolumn">1 Column</span></li>
						<li><input type="radio" name="columnType" value="two_column" class="column" id="twoColumn">
							<span class="btn icon twocolumn">2 Column</span></li>
					</ul>
				</form>
				<div class="clear">&nbsp;</div>
				<div class="btmdiv">
					<input type="button" value="Reset All" class="buttonTwo flLeft layoutSettReset">
					<input type="button" value="Apply" class="buttonOne flRight" id="viewSettingColumnApply">
					<div class="clear">&nbsp;</div>
				</div>
			</div>
		</div>
		<div class="clear">&nbsp;</div>
	</div>

	<!-- News Stories by Firm - Top 5 Firms -->

	<div class="newspublicationPage marbtm4">
		<div class="colMin flLeft leftDynamicDiv">
			<div class="topHeader">
				<div class="flRight">
					<ul class="reset listView">
						<li><a href="#" class="btn icon settingsgry">&nbsp;</a>
							<div class="viewBox popusdiv">
                            <div class="popHeader"> <a href="#" class="btn icon closewhite closeOne flRight" style="margin-top: 2px">Close</a> SETTINGS: AMOUNT OF NEWS AND PUBLICATIONS BY FIRM
                              <div class="clear">&nbsp;</div>
                            </div>
                            <div class="section-one">
                              <h6>Chart Type</h6>
                              <form action="#">
                                <ul class="reset list4">
                                  <li style="width: 125px">
                                    <input name="graphType" value="pieChart" class="graphType" type="radio">
                                    <span class="btn icon piechart">Pie</span></li>
                                  <li>
                                    <input name="graphType" value="verticalBar" class="graphType" type="radio">
                                    <span class="btn icon barchartvert">Vertical Bar</span></li>
                                  <li>
                                    <input name="graphType" value="HorizontalBar" class="graphType" type="radio">
                                    <span class="btn icon barcharthori">Horizontal Bar</span></li>
                                  <li>
                                    <input name="graphType" value="stackVertical" class="graphType" checked="checked" id="leftGraphDefaultChecked" type="radio">
                                    <span class="btn icon stackedbarchart">Stacked Bar</span></li>
                                </ul>
                                <div class="clear">&nbsp;</div>
                              </form>
                            </div>
                            <div class="section-two">
                              <form action="#">
                                <h6>Firm Data (Limit of 10)</h6>
                                <ul class="reset list4">
                                  <li style="width: 125px">
                                    <input name="sorting" value="top_ten" class="sorting" type="radio" checked="checked" id="topTen">
                                    <span class="">Top 10</span></li>
                                  <li>
                                    <input name="sorting" value="bottom_ten" class="sorting" type="radio">
                                    <span class="">Bottom 10</span></li>
                                  <li>
                                    <input name="sorting" value="selected_firm" class="sorting" type="radio">
                                    <span class="">Selected Firms:</span> <span id="selected_firms">
                                    </span> <span id="selFirm"></span> </li>
                                </ul>
                                <div class="clear">&nbsp;</div>
                              </form>
                            </div>
                            <div class="section-three">
                              <div class="martp2">
                                <h6>Comparison Data</h6>
                                <div class="marbtm2 martp1">
                                  <p class="marbtm1">
                                    <input class="myFirm" checked="checked" type="checkbox">
                                    My Firm </p>
                                  <p class="marbtm1">
                                    <input class="amLaw100AVg" checked="checked" type="checkbox">
                                    AmLaw 100 Average </p>
                                  <p class="marbtm1-last">
                                    <input class="watchListAvg" type="checkbox">
                                    Watchlist Average
                                    <select>
                                      <option selected="selected">Firmwatchlist Name</option>
                                    </select>
                                  </p>
                                </div>
                              </div>
                              <div class="clear">&nbsp;</div>
                              <div class="btmdiv">
                                <input value="Reset All" class="buttonTwo flLeft settingReset" type="button">
                                <input value="Apply" class="buttonOne flRight" id="leftChartSettingApply" type="button">
                                <div class="clear">&nbsp;</div>
                              </div>
                            </div>
                          </div>
                        </li>
                        <li><a href="#" class="btn icon printgry">&nbsp;</a></li>
                        <li><a id="exports" href="#" class="btn icon exportgry">&nbsp;</a>
                          <div id="actionBox" class="actionSec" style="">
                            <h5>Actions</h5>
                            <ul class="reset">
                              <li><span>E</span><a href="#">Export as JPEG Image</a></li>
                              <li><span>E</span><a href="#">Export as PNG Image</a></li>
                            </ul>
                            <div class="clear">&nbsp;</div>
                          </div>
                        </li>
                      </ul>
                    </div>				News Stories by Firm - Top 5 Firms
				<div class="clear">&nbsp;</div>
			</div>
			<div class="netypepage">
				<h6 class="rightAlign">Top Five - News and Publications</h6>
				<div class="padtp3 marbtm4">
					<div id="popupPieFirmName"></div>
					<div id="leftChart"
						style="overflow: hidden; width: 467px; height: 302px;"></div>
					
					<div id="popupPieChartDiv" style="background-color:#ffffff;"><!--<a class="btn icon closewhite closeOne flRight close" href="#">Close</a>-->
						<div id="popupDetails" style="margin-top: 0px;"></div>
						
						<div id="popupPieChart"
							style="overflow: hidden; display: none;"></div>
						<span><a href="#">Click to View Details</a></span>
					</div>
				</div>
			</div>
		</div>
		<div class="colMin flRight dynamicDiv" style="position:relative;">
			<div class="topHeader">
				<div class="flRight">
					<ul class="reset listView">
						<li><a href="#login-box2" class="btn icon settingsgry rightViewSetting login-window">&nbsp;</a>
						
						
						</li>
						<li><a href="#" class="btn icon printgry">&nbsp;</a></li>
						<li><a id="exports" href="#" class="btn icon exportgry">&nbsp;</a>
							<div id="actionBox" class="actionSec" style="">
								<h5>Actions</h5>
								<ul class="reset">
									<li><span>E</span><a href="#">Export as JPEG Image</a></li>
									<li><span>E</span><a href="#">Export as PNG Image</a></li>
								</ul>
								<div class="clear">&nbsp;</div>
							</div></li>
					</ul>
					
					<div id="login-box2" class="popusdiv login-popupTwo">
								<div class="popHeader">
									<a href="#" class="btn icon closewhite closeOne flRight" style="margin-top: 2px">Close</a> Settings: % of News and
									Publications
									<div class="clear">&nbsp;</div>
								</div>
								<div class="popMiddle">
									<h6>Chart Type</h6>
									<form action="#">
										<ul class="reset list2">

											<li style="width: 125px"><input type="radio"
												name="graphType2" value="pieChart" class="graphType" checked="checked" id="rightGraphDefaultChecked">
												<span class="btn icon piechart">Pie</span></li>
											<li><input type="radio" name="graphType2"
												value="verticalBar" class="graphType"><span
												class="btn icon barchartvert">Vertical Bar</span></li>
											<li><input type="radio" name="graphType2"
												value="HorizontalBar" class="graphType"><span
												class="btn icon barcharthori">Horizontal Bar</span></li>
										</ul>
									</form>
									<div class="clear">&nbsp;</div>
								</div>
								
								<div class="popFooter">
									<!-- <div class="flLeft">
										<input type="button" value="Reset All" class="buttonTwo">
									</div> -->
									<div class="btmdiv">
									<input type="button" value="Reset All" class="buttonTwo flLeft rightReset">
									<input type="button" value="Apply" class="buttonOne flRight"
										id="rightChartSettingApply">
									<div class="clear">&nbsp;</div>
								</div>
								</div>
							</div>
					
				</div>
				News Stories by Firm - Top 5 Firms
				<div class="clear">&nbsp;</div>
			</div>
			<div class="netypepage">
				<h6 class="rightAlign">Top Five - News and Publications</h6>
				<div class="padtp3 marbtm4">
					<div id="rightChart"
						style="overflow: hidden; width: 467px; height: 302px;" class="rightChartClass"></div>
				</div>
			</div>
		</div>
		<div class="clear">&nbsp;</div>
	</div>
</div>
<input type="hidden" id="hiddenField">
<div id="individualFirmDetailsDiv" class="popuponhover" style="display: none;">
<table id="individualFirmDetails" >
	<tr><td colspan="2"><b><span id="hoverFirmName" class="popuptitle"></span></b></td></tr>
	<tr><td class="statistics" style="width: 40px; height: 35px; padding-top: 10px; color: white; background-color: gray;" ><span id="totalColor"></span></td><td><span class="statistics-type" id="totalValue">Total</span></td></tr>
	<tr><td class="statistics" style="width: 40px; height: 35px; padding-top: 10px; color: white; background-color: green;"><span id="newsColor"></span></td><td><span class="statistics-type" id="newsValue">News</span></td></tr>
	<tr><td class="statistics" style="width: 40px; height: 35px; padding-top: 10px; color: white; background-color: #FACC2E;"><span id="publicationColor"></span></td><td><span class="statistics-type" id="pubValue">Publications</span></td></tr>
	<tr><td class="statistics" style="width: 40px; height: 35px; padding-top: 10px; color: white; background-color: blue;"><span id="twitterColor"></span></td><td><span class="statistics-type" id="twitterValue">Twitter</span></td></tr>
</table>
<span><a href="#">Click to View Details</a></span>

</div>

<script src="<%=resourceRequest.getContextPath()%>/js/s_code.js"></script>
<script type="text/javascript">

//Omniture SiteCatalyst - START
s.prop22 = "premium";
s.pageName="rer:firm-search-analysis";
s.channel="rer:firm-search";
s.server="rer";
s.prop1="Firm-Search"
s.prop2="Firm-Search";
s.prop3="Firm-Search";
s.prop4="Firm-Search";
s.prop21=s.eVar21='current.user@foo.bar';	
s.events="event2";
s.events="event27";
s.t();
// Omniture SiteCatalyst - END
</script>

