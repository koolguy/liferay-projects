<%@page import="com.liferay.portal.model.Portlet"%>
<%@page import="com.liferay.portal.service.PortletLocalServiceUtil"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%> 

<portlet:defineObjects/>

<%
	ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
	String 		 url 		  = themeDisplay.getPortalURL() + themeDisplay.getPathMain() + "/firms-details";
	
	List allPortlets = PortletLocalServiceUtil.getPortlets();
	
	if ((allPortlets != null) && (!allPortlets.isEmpty()))
	{
		for (int i = 0; i < allPortlets.size(); i++)
		{
			Portlet thisPortlet = (Portlet) allPortlets.get(i);
			if ((thisPortlet.getDisplayName() != null) && (thisPortlet.getDisplayName().startsWith("RivalEdge - Firm - News and Pubs")))
			{
%>
	<h3><%= thisPortlet.getDisplayName() %></h3>
<%
			}
		}
	}
	
	System.out.println("\n\n*** url = " + url + "\n");
%>
