<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.alm.rivaledge.model.FirmSearchModelBean"%>
<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"      uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %> 

<portlet:defineObjects/>

<%						
	PortletURL printPublicationsURL = renderResponse.createRenderURL();
	printPublicationsURL.setWindowState(LiferayWindowState.POP_UP);
	printPublicationsURL.setParameter("displayPrintPage", "true");						
%>

<script type="text/javascript">
	function setFileType(fileType)
	{
		document.getElementById('fileType').value = fileType;
		var r=confirm("Click OK to export or Cancel");
		if (r == true)
	  	{
	  		document.getElementById('exportForm').submit();
	  	}
		else
		{
		  return false;
		}
	}
	
	function openPopUp()
	{
		popupWindow = window.open(
						'<%= printPublicationsURL.toString() %>',
						'popUpWindow','height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes');
	}
	
	
	
</script>


 <div class="detailsPage">
  	<h4 class="heading">Detailed Results</h4>
  	<div>
  		<div class="flRight">   	  
      		<ul class="menuRight flRight">
        		<li><a class="btn icon alert" href="#">Create Alert</a></li>
         		<li><a class="btn icon print" href="#" onClick="openPopUp();" >Print</a></li>
        		<li><a id="exports" class="btn icon export" href="#">Export</a>
	          		<div id="actionBox" class="actionSec" style="">
	            		<h5>Actions</h5>
	            		<ul class="reset">
	              			<li><span>E</span><a href="#"  onclick="setFileType('xls')">Export to Excel</a></li>
	              			<li><span>E</span><a href="#"  onclick="setFileType('csv')">Export to CSV</a></li>
	            		</ul>
	            		<div class="clear">&nbsp;</div>
	          		</div>
        		</li>
        		<li>
	        		<div class="viewSetting flRight rel" style="margin-top:0px">
					  		<a class="btn icon settings" href="javascript:void(0)" onclick="toggleViewSettingsNewsPubs(this);">
					  			<span>
					  				<span class="btn icon dropdownarrow">View Settings</span>
					  			</span>
					  		</a>
	      			 </div>
        		</li>
			</ul>
      		<div class="clear">&nbsp;</div>
		</div>
       
  		<div class="flLeft resultsec"><strong>${firmSearchModelBean.currentPageSize}&nbsp;of&nbsp;${firmSearchModelBean.totalResultCount} Results</strong> 
  		 <span>(
          		<c:choose>
	      				 <c:when test="${fn:length(firmSearchModelBean.selectedFirms) gt 100}">
	      				 	${fn:substring(firmSearchModelBean.selectedFirms, 0, 99)}..,&nbsp;
	      				 </c:when>
	      				 <c:otherwise>
	      				 	${firmSearchModelBean.selectedFirms},&nbsp;
	      				 </c:otherwise>		        		  
				</c:choose>				
	      		${firmSearchModelBean.selectedContentType},&nbsp;	      				
				 ${firmSearchModelBean.dateText},&nbsp;  
				<c:choose>
	      				 <c:when test="${fn:length(firmSearchModelBean.keywords) gt 100}">
	      				 	${fn:substring(firmSearchModelBean.keywords, 0, 99)}..,&nbsp;
	      				 </c:when>
	      				  <c:otherwise>
	      				 	<c:if test="${not empty firmSearchModelBean.keywords}">
	      				 		${firmSearchModelBean.keywords}, 
	      				 	</c:if>
	      				 </c:otherwise>		        		  
				</c:choose>
				<c:choose>
	      				 <c:when test="${fn:length(firmSearchModelBean.selectedPracticeArea) gt 100}">
	      				 	${fn:substring(firmSearchModelBean.selectedPracticeArea, 0, 99)}..,&nbsp;
	      				 </c:when>
	      				 <c:otherwise>
	      				 	${firmSearchModelBean.selectedPracticeArea},&nbsp;
	      				 </c:otherwise>		        		  
				</c:choose>      				 
	      				${firmSearchModelBean.selectedTwitterPostType}&nbsp;	      		
          )</span>
  		
  		</div>
  		
  		<!-- New code -->
  		
  		<!-- old code 
    	<a class="viewSetting flRight" style="position:relative" href="javascript:void(0)"	onClick="toggleViewSetting(this)">
			<span><span>ViewSettings</span></span>
		</a>
		-->
		 <div class="clear">&nbsp;</div>
    </div>
	<c:if test="${not isAdminUser}">
    	<c:if test="${displayMessage}">
  			<div style="color:#b63334;">
  				Search Results are too large to display in their entirety. Only the first 5,000 results are displayed below. To view an entire search result, please narrow your search criteria above and rerun your search.
  			</div>
  		</c:if>
    </c:if>
     <table class="tbleTwo" width="100%" border="0" cellspacing="0" cellpadding="0">
    	<colgroup>
            	<!--<col width="50"/>--> <!-- commented checkbox to fix bug RER 305 -->
                <col width="36"/>
                <col width="90"/>
                <col width="100"/>
                <col width="130"/>
                <col width="600"/>
                <col width="150"/>        
                
          </colgroup>
       <thead>
            <tr>
              <!--  <th></th> -->
              <th><a href="javascript:void(0);" onclick="sortResultsFirms(3)">Type</a></th>
              <th><a href="javascript:void(0);" onclick="sortResultsFirms(10)">Date</a></th>
              <th><a href="javascript:void(0);" onclick="sortResultsFirms(2)">Firm</a></th>
              <th><a href="javascript:void(0);" onclick="sortResultsFirms(4)">Source</a></th>
              <th><a href="javascript:void(0);" onclick="sortResultsFirms(6)">Description</a></th>
              <th><a href="javascript:void(0);" onclick="sortResultsFirms(9)">Related Practice</a></th>
            </tr>
          </thead>
       </table>
          
          
         
          <table class="tbleTwo" width="100%" border="0" cellspacing="0" cellpadding="0">
          
          <colgroup>
            	<!--<col width="50"/>--> <!-- commented checkbox to fix bug RER 305 -->
                <col width="36"/>
                <col width="90"/>
                <col width="100"/>
                <col width="130"/>
                <col width="600"/> <!-- changed from 550 to 600, and commented checkbox column -->
                <col width="150"/>
          </colgroup>
           <tbody>
      		<c:choose>
      			<c:when test="${fn:length(searchResults) gt 0}">
		      			 <c:forEach items="${searchResults}" var="results"  varStatus="loopStatus">
					      	 <%-- orderBy values 2 = firm_name
					      	 					 3 = data_type	
				<!-- debugging purpose-->
				<script> console.log("${loopStatus.index}. loopStatus.first > ${loopStatus.first}      orderBy -> ${firmSearchModelBean.orderBy}  condition -> ${(loopStatus.first) && (firmSearchModelBean.orderBy == 'data_type')}");</script>
				 --%> 
				 <%--The below choose cond'n  is responsible for grouping the results in Table in UI--%>
				<c:choose>
						<%-- For first row in Table --%>
   						 <c:when test="${(loopStatus.first) && (firmSearchModelBean.orderBy == 2)}">
     						    <tr class="oddone">
	      		 					<td colspan="7"><div class="padtp1 padlt1 padbtm1"><strong>${results.firmName}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(loopStatus.first) && (firmSearchModelBean.orderBy == 3)}">
     						    <tr class="oddone">
	      		 					<td colspan="7"><div class="padtp1 padlt1 padbtm1"><strong>${results.dataType}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 <%-- For subsequent rows in Table only when there is change in the firmName or dataType in 2 consecutive records --%>
   						 <c:when test="${(loopStatus.index > 0) && (firmSearchModelBean.orderBy == 2) && (searchResults[loopStatus.index - 1].firmName != searchResults[loopStatus.index].firmName)}">
     						    <tr class="oddone">
	      		 					<td colspan="7"><div class="padtp1 padlt1 padbtm1"><strong>${results.firmName}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(loopStatus.index > 0) && (firmSearchModelBean.orderBy == 3) && (searchResults[loopStatus.index - 1].dataType != searchResults[loopStatus.index].dataType)}">
     						    <tr class="oddone">
	      		 					<td colspan="7"><div class="padtp1 padlt1 padbtm1"><strong>${results.dataType}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 
				</c:choose>
					      	 
					      	 
					      	 <tr class="${loopStatus.index % 2 == 0 ? 'odd' : 'even'} ">
						     <!--    <td><input type="checkbox"/></td>--> <!-- commented checkbox to fix bug RER 305 -->
								<td>
						        	<c:choose>
						         		<c:when test="${results.dataType == 'Pubs' }">
						         			<img alt="Publication" src="<%=renderRequest.getContextPath()%>/images/publish.png" title="Publication">
						        		</c:when>
						        		<c:when test="${results.dataType == 'News' }">
						        			<img alt="News" src="<%=renderRequest.getContextPath()%>/images/news.png" title="News">
						        		</c:when>
						        		<c:when test="${results.dataType == 'Twitter' }">
						        			<img alt="Twitter" src="<%=renderRequest.getContextPath()%>/images/twitter.png" title="Twitter">
						        		</c:when>
						         	</c:choose>						        	
						        </td>
						        <c:set var="setFirmURL" value="${results.firmURL}"/>
						        <td><fmt:formatDate pattern="MM/dd/yyyy" value="${results.entryDate}"/></td>
						        <td>
						        	<c:choose>
									  <c:when test="${fn:startsWith(setFirmURL, 'http://')}">
									   <a href="${results.firmURL}" target="_blank"><span style="text-decoration:underline">${results.firmName}</span></a>
									  </c:when>
									  <c:otherwise>
									  	<a href="http://www.${results.firmURL}" target="_blank"><span style="text-decoration:underline">${results.firmName}</span></a>	
									  </c:otherwise>
									</c:choose> 
						        </td>
						        <td>
						        	<c:set var="dataSourceURL" value="${results.dataSourceURL}"/>
						         	<c:choose>
									  <c:when test="${empty results.dataSourceURL}">
									  	<c:choose>
										  <c:when test="${fn:startsWith(setFirmURL, 'http://')}">
										  	<c:choose>
										  		<c:when test="${((results.dataType == 'Pubs') || (results.dataType == 'News')) && (results.dataSource == 'CompanySite')}">
										  			<a href="${results.firmURL}" target="_blank">${results.dataSource}</a>
										  		</c:when>
										  		<c:otherwise>
										  			${results.dataSource}
										  		</c:otherwise>		
										  	</c:choose>
										  </c:when>
										  <c:otherwise>
										  	<c:choose>
										  		<c:when test="${((results.dataType == 'Pubs') || (results.dataType == 'News')) && (results.dataSource == 'CompanySite')}">
										  			<a href="http://www.${results.firmURL}" target="_blank">${results.dataSource}</a>	
										  		</c:when>
										  		<c:otherwise>
										  			${results.dataSource}
										  		</c:otherwise>		
										  	</c:choose>
										  </c:otherwise>
										</c:choose> 
									  </c:when>
									  <c:when test="${fn:startsWith(dataSourceURL, 'http://')}">
									  		<a href="${results.dataSourceURL}" target="_blank">${results.dataSource}</a>
									  </c:when>				 
									  <c:otherwise>
									    	<a href="http://${results.dataSourceURL}" target="_blank">${results.dataSource}</a>
									  </c:otherwise>
									</c:choose>
								</td>
						        <td>
						        	<c:choose> <%--Bug 354 fix --%>
									  <c:when test="${empty results.entryURL}">
									  <span style="color:#222">${results.entryTitle}</span>
									  </c:when>				 
									  <c:otherwise>
									  <a href="${results.entryURL}" target="_blank"> <span style="text-decoration:underline">${results.entryTitle}</span></a>
									  </c:otherwise>
									</c:choose>
						       		
						       		<br>  
					      			
					      			<c:choose>
						      			<c:when test="${results.dataType != 'Twitter'}">
							      			<c:choose>
							      				 <c:when test="${fn:length(results.entryDescription) gt 254}">
							      				 	${fn:substring(results.entryDescription, 0, 251)}...
							      				 </c:when>
							      				 <c:otherwise>
							      				 	${results.entryDescription} 
							      				 </c:otherwise>		        		  
							        		</c:choose>
						        		</c:when>
						        		<c:otherwise>
						        			&nbsp;
						        		</c:otherwise>
					        		</c:choose>
						       </td>   
						        <td>${results.practiceArea}</td>
					      	</tr>      
				      	</c:forEach>
				      	</table>
				      
				      	
		      	  	<div class="PaginationScroll">
		      	  	
		      	  <ul>
		      	  <%-- Move the below code block to any jspf, to improve reusablily of pagination--%>
				  <%-- pagination starts --%>
					<%
						FirmSearchModelBean fsm = 	((FirmSearchModelBean) request.getAttribute("firmSearchModelBean"));
						int pageNumber 	= fsm.getGoToPage();
						int lastPage	= fsm.getLastPage();
						if (lastPage != 1)
						{
						%>
								Page:&nbsp;&nbsp;&nbsp;
						<%
								if (pageNumber > 5)
								{
						%>
									<li class="active"><a href="javascript:void(0);" onclick="setPageFirms(<%= 1 %>)"><%= 1 %></a>&nbsp;&nbsp;&nbsp;...&nbsp;&nbsp;&nbsp;</li>
						<%
								}
								for (int i = Math.max(1, pageNumber - 5); i <= Math.min(pageNumber + 5, lastPage); i++)
								{
									if (pageNumber == i)
									{
						%>
									<li><strong><%= i %>&nbsp;&nbsp;&nbsp;</strong> </li>
						<%
									}
									else
									{
						%>
										<li><a href="javascript:void(0);" onclick="setPageFirms(<%= i %>)"><%= i %></a>&nbsp;&nbsp;&nbsp;</li>
						<%				
									}
								}
								
								if ((lastPage - pageNumber) > 5)
								{
						%>
									...&nbsp;&nbsp;&nbsp;<li><a href="javascript:void(0);" onclick="setPageFirms(<%= lastPage %>)"><%= lastPage %></a></li>
						<%			
								}
						}
						else
						{
						%>
								Page:&nbsp;&nbsp;&nbsp;1
						<%				
						}
						%>
		      	  	<!-- pagination ends -->
		     	</ul>	
		   		 
   </div>
   
      		</c:when>
      		<c:otherwise>
	      		<tr class="odd">
	      			<td colspan="8" align="center">0 Results, Please try a different search</td>
	      		</tr>     		
      		</c:otherwise>
		     
      </c:choose>
   
	</table> 
  </div>
  
<form name="exportForm" method="post" id="exportForm" action="<portlet:resourceURL id="exportFile"/>">
	<input type="hidden" name="fileType" value="" id="fileType"/>
</form>
  
  <script src="<%=renderRequest.getContextPath()%>/js/s_code.js"></script>

	<script type="text/javascript">
	 
	 // Omniture SiteCatalyst - START
	 s.prop22 = "premium";
	 s.pageName="rer:firm-search-results";
	 s.channel="rer:firm-search";
	 s.server="rer";
	 s.prop1="Firm-Search"
	 s.prop2="Firm-Search";
	 s.prop3="Firm-Search";
	 s.prop4="Firm-Search";
	 s.prop21=s.eVar21='current.user@foo.bar'; 
	 s.events="event2";
	 s.events="event27";
	 s.t();
	 // Omniture SiteCatalyst - END
	 
	 $(function(){
		// When user clicks on details tab active-menu css class is removed from analysis tab.
		// And same class added to details tab.
		// By default active-menu class will be there for details tab.
		$("#firms-details").addClass("active-menu");
		$("#firms-analysis").removeClass("active-menu");
	});	
	</script>