<%@page import="java.util.Map"%>
<%@ taglib prefix="portlet" 		uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" 			 	uri="http://java.sun.com/jsp/jstl/core" %>

<portlet:defineObjects/>

<%@ include file="./common.jsp" %>

<script type="text/javascript">

<%@ include file="./theme.jsp" %>

<%
	Map<String, String> percentNewsPubsChartModel = (Map<String, String>) request.getAttribute("percentNewsPubsModel");
%>
	var percentNewsPubsTypeOfChart 				= '<%= percentNewsPubsChartModel.get("percentNewsPubsChartType") %>'; 			 	//This will be type of chart.
	var percentNewsPubsTitleOfChart 			= '<%= percentNewsPubsChartModel.get("percentNewsPubsChartTitle") %>'; 			 	// This will be title of chart.
	var percentNewsPubsFirmsNameList 			= [<%= percentNewsPubsChartModel.get("percentNewsPubsFirmsNameList") %>]; 		 	// This will be list of firms name.
	var percentNewsPubsNewsCountByFirmList 		= [<%= percentNewsPubsChartModel.get("percentNewsPubsNewsCountByFirmList") %>]; 	// This will be news count of individual firms.
	var percentNewsPubsPubsCountByFirmList 		= [<%= percentNewsPubsChartModel.get("percentNewsPubsPubsCountByFirmList") %>]; 	// This will be publication count of individual firms.
	var percentNewsPubsTwitterCountByFirmList 	= [<%= percentNewsPubsChartModel.get("percentNewsPubsTwitterCountByFirmList") %>]; 	// This will be twitter count of individual firms. 
	var percentNewsPubsTotalCountByFirmList 	= [<%= percentNewsPubsChartModel.get("percentNewsPubsTotalCountByFirmList") %>]; 	// This will be total count of individual firms. 

$(function()
{
	if(<%=!isHomePage%>)
	{
		if (percentNewsPubsFirmsNameList == null || percentNewsPubsFirmsNameList == '')
		{
			$("#noDataPercentnewspubsDiv").removeClass("hideClass");
			$("#percentageNewsPubsChartContainer").addClass("hideClass");
			$("#noDataPercentnewspubsDiv").parent().find(".charts").hide(); // hide the settings icon
			
			//All the above code is useless if this is the final approach
			$("#p_p_id<portlet:namespace/>").hide();
			$(".cph").show();
			return;
		}
	}
	else
	{
		if (percentNewsPubsFirmsNameList == null || percentNewsPubsFirmsNameList == '') 
		{	
			$("#noDataPercentnewspubsDiv").removeClass("hideClass");
			$("#percentageNewsPubsChartContainer").addClass("hideClass");
			
			$("#No-Data-Percent").addClass("No-Data-Charts");
			$("#newspubls_perc_settings").hide();
		}
		else
		{
			$("#No-Data-Percent").removeClass("No-Data-Charts");
			$("#noDataPercentnewspubsDiv").addClass("hideClass");
		}
	}

	Highcharts.setOptions({
  		colors: ['#2f6d9f', '#c1942c', '#35744a', '#0489B1', '#5F4C0B', '#B40404', '#380B61', '#DF7401', '#0A2229']
	});

	$(".AnalyzeResultHeader").show();
	
	$("#rightChartSettingApply, .closewhite").click(function(){
		$("#login-box2").hide();
	});
	
	$(".highcharts-button").click(function(){
		$("div#popupPieChartDiv").hide();
		$("#login-box2").hide();
	});
	
	$("#rightChartSettingApply").click(function(){
		$("#login-box2").hide();
		if($('input[name=graphType2]:radio:checked').val() == 'verticalBar')
		{
			displayBarGraph('column');
		} else if($('input[name=graphType2]:radio:checked').val() == 'HorizontalBar')
		{
			displayBarGraph('bar');
		}else if($('input[name=graphType2]:radio:checked').val() == 'pieChart')
		{
			displayPieChart();
		}
	});
	$(".rightReset").click(function(){
		$("#login-box2").hide();
		$("#rightGraphDefaultChecked").prop('checked', true);
		displayPieChart();
	});
	
	// When user clicks on analysis tab active-menu css class is removed from details tab.
	// And same class added to analysis tab.
	$("#firms-details").removeClass("active-menu");
	$("#firms-analysis").addClass("active-menu");
	
	$("div#popupPieChartDiv").mouseover(function(){
		$("div#popupPieChartDiv").show();
	});
	
	displayPieChart();
});
function displayPieChart()
{
	var percentNewsPubsPieChartSeries = []; // This will be series of data.
	 
	for(var index=0; index < percentNewsPubsFirmsNameList.length; index++)
	{
		// This will be pie chart data.
	 	// First attribute firm name and second will be total count.
		var percentNewsPubsPieChartData = [];
		percentNewsPubsPieChartData.push(percentNewsPubsFirmsNameList[index]);
		percentNewsPubsPieChartData.push(percentNewsPubsTotalCountByFirmList[index]);
		percentNewsPubsPieChartSeries.push(percentNewsPubsPieChartData);
	}
	
	// Generate graph.
	$('#percentageNewsPubsChartContainer').highcharts(
	{
        chart:
		{
			height: 500,
			width: 575
        },
        title:
		{
            text: percentNewsPubsTitleOfChart
        },
        tooltip:
		{
    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
			formatter: function(e){
           	 	$("#login-box2").hide();
         		popupPieChart(this.key);
         		return false;
         	}
        },
        plotOptions:
		{
            pie:
			{
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                	 distance: -30,
                     enabled: true,
                     color: '#ffffff',
                     formatter: function(){
                         return parseFloat(this.percentage).toFixed(0)+"%";
                     }
                 },
				showInLegend: true,
				point: {
					events: {
                       mouseOver: function(e) { 
                            $("#popupPieChart").show();
							$("#popupPieChartDiv").show();
							$(document).click(function() {
								$("div#popupPieChartDiv").hide();
							});
						},
						mouseOut: function(e){
							$("div#popupPieChartDiv").hide();
						},
						legendItemClick: function () {
							return false; // <== returning false will cancel the default action
						}
                     }
                 }
			}
        },
		legend: {
            enabled: true,
			borderColor: '#fff',
			itemStyle: {
                paddingBottom: '7px'
            },
            layout: 'vertical',
            align: 'right',
            width: 200,
            verticalAlign: 'middle',
            useHTML: true,
            labelFormatter: function() {
                return '<div style="text-align: left; width:130px;">' + this.name + '</div>';
            }
         },
        series: [
		{
            type: 'pie',
            name: percentNewsPubsTitleOfChart,
            data: percentNewsPubsPieChartSeries
        }]
    });
}
function popupPieChart(key){

	$("#percentageNewsPubsChartContainer").mousemove(function(event){
		
		var parentOffset = $(this).parent().offset();
        var relativeXPosition = (event.pageX - parentOffset.left);
        var relativeYPosition = (event.pageY - parentOffset.top);
		
		$("#popupPieChartDiv").css("top", relativeYPosition-290).css("left", relativeXPosition-100)
	});
	
	var firmIndex = 0;
	for(var index=0; index < percentNewsPubsFirmsNameList.length; index++)
	{
		if(key == percentNewsPubsFirmsNameList[index])
		{
			firmIndex = index;
		}
	}

	var popupDetails = "<table width='100%'><tr><th colspan='4' style='font-size:12px; color:#000'>"+key+"</th></tr><tr><th style='padding:2px;color:#777; font-size:12px;'>Overall</th><th style='padding:2px; color:#777; font-size:12px;'>News</th><th style='padding:2px;color:#777; font-size:12px;'>Pubs</th><th style='padding:2px; color:#777; font-size:12px;'>Tweets</th></tr><tr style='line-height:15px;'><td style='background:#777777; text-align:center; border:1px solid #333; color:#777; color: #FFFFFF; font-size: 14px; font-weight: bold;'>"+percentNewsPubsTotalCountByFirmList[firmIndex]+"</td><td style='text-align:center; border:1px solid #333; color:#2f6d9f; font-size: 14px; font-weight: bold;'>"+percentNewsPubsNewsCountByFirmList[firmIndex]+"</td><td style='text-align:center; border:1px solid #333; color:#2f6d9f; font-size: 14px; font-weight: bold;'>"+percentNewsPubsPubsCountByFirmList[firmIndex]+"</td><td style='text-align:center; border:1px solid #333; color:#2f6d9f;font-size: 14px; font-weight: bold;'>"+percentNewsPubsTwitterCountByFirmList[firmIndex]+"</td></tr></table>";
	$("#popupDetails").html(popupDetails);
	
	 $('#popupPieChart').highcharts({
         chart: {
             plotBackgroundColor: null,
             plotBorderWidth: null,
             plotShadow: false,
             width: 200,
             height: 200
         },
         title: {
             text: ''
         },
         tooltip: {
     	    pointFormat: '{series.name}: <b>{parseFloat(point.percentage).toFixed(0)}%</b>',
         	percentageDecimals: 1
         },
         plotOptions: {
             pie: {
                 allowPointSelect: true,
                 cursor: 'pointer',
                 dataLabels: {
                	 distance: -30,
                     enabled: true,
                     color: '#ffffff',
                     formatter: function(){
                         return parseFloat(this.percentage).toFixed(0)+"%";
                     }
                 },
                 showInLegend: true
             }
         },
         series: [{
             type: 'pie',
             name: 'PERCENTAGE OF NEWS AND PUBLICATIONS',
             data: [
                    ['News', percentNewsPubsNewsCountByFirmList[firmIndex]],
                    ['Pubs', percentNewsPubsPubsCountByFirmList[firmIndex]],
                    ['Tweets', percentNewsPubsTwitterCountByFirmList[firmIndex]]
                   ]
         }],
         exporting: {
             enabled: false
         }
     });
}
function displayBarGraph(type)
{
	var labelXPosition = 0;
	var labelYPosition = 0;
	var labelRotation = 0;
	if(type == 'bar')
	{
		labelXPosition = 0;
		labelYPosition = 0;
		labelRotation = 0;
	}
	else
	{
		labelXPosition = -15;
		labelYPosition = 30;
		labelRotation = -45;
	}
	$('#percentageNewsPubsChartContainer').highcharts({
        chart: {
            type: type,
            height: 500,
			width: 575
        },
        title:
		{
            text: percentNewsPubsTitleOfChart
        },
        xAxis: {
            categories: percentNewsPubsFirmsNameList,
            labels: 
            {
                rotation: labelRotation,
                y: labelYPosition,
				x: labelXPosition,
				formatter: function()
				{
					var firmName = this.value;
					if(firmName.length > 10)
					{
						firmName = firmName.substring(0, 10) + "...";
					}
					return firmName;
				}
			}
        },
        yAxis: {
            min: 0,
            
            stackLabels: {
                enabled: true,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
        	enabled: true
    	},
        tooltip: {
        	formatter: function() {
				return this.y;
			}
        },
        plotOptions: {
        	column: {
                
                dataLabels: {
                    enabled: true,
                    color: '#000000',
                    formatter: function(){
                    	if(this.y > 0){
                    		return this.y;
                    	}
                    }
                },
                point: {
                    events: {
                        mouseOver: function(e) {}
                    }
                    
                }
            }
        },
        series: [
        {
            name: 'News',
            color: '#446e49',
            data: percentNewsPubsNewsCountByFirmList
        }, 
        {
            name: 'Pubs',
            color: '#0c3362',
            data: percentNewsPubsPubsCountByFirmList
        }, 
        {
            name: 'Tweets',
            color: '#b89024',
            data: percentNewsPubsTwitterCountByFirmList
        }]
    });
}

<script type="text/javascript">

$(document).ready(function()
{	
    $("#newspubls_perc").click(function()
    {
    	//Toggle the value of the search portlet 
    	//hidden field portletId to be the portlet id
    	//of this chart.
    	$("#_refirmsearchportlet_WAR_refirmportlet_portletId").val(<portlet:namespace/>thisPortletId);
    	
		if ($("#homePageShowPeople").hasClass('ui-dialog-content')) 
		{
			$("#homePageShowPeople" ).dialog( "close" );
		}
		if ($("#homePageShowAttorney").hasClass('ui-dialog-content'))
		{
			$("#homePageShowAttorney" ).dialog( "close" );
		}
		if ($("#homePageShowEvent").hasClass('ui-dialog-content'))
		{
			$( "#homePageShowEvent" ).dialog( "close" );
		}
		$( "#homePageShowFirms" ).dialog( "open" );
	});
	
	if(<%=isHomePage%>)
	{
		$( "#homePageShowFirms" ).dialog({
	      autoOpen: false,
	      show: 'fade',
	      hide: 'fade',
	      title:'Filter Criteria',
	      position: 'center',
	      modal: true,
	      width: 1100,
	      closeOnEscape: true,
	      height: 350,
	      minHeight: 350,
	      closeText: 'x CLOSE',
	      dialogClass: "myClass",
	      resizable: true,
	      draggable:false
	    });
    }	
});

</script> 


</script>

<div class="newspublicationPage">
	<div class="colMin flLeft leftDynamicDiv">
		<div class="topHeader ForChartsTopHeader">
		<div id="noDataPercentnewspubsDiv" class="hideClass">0 Results, Please try a different search</div>
		<div class="flRight charts" id="No-Data-Percent">
			<ul class="reset listView">
				<c:if test="<%=isHomePage%>" >
					<li id="newspubls_perc" style="overflow:hidden;"><a href="#" class="filter-icon" >&nbsp;</a></li>
				</c:if>
				<li id="newspubls_perc_settings"><a href="#login-box2" class="btn icon settingsgry rightViewSetting login-window chartViewSetting" onclick="return false;">&nbsp;</a></li>
			</ul>
			<div id="login-box2" class="popusdiv login-popupTwo onclickpopup" style="width:400px !important;">
				<div class="popHeader">
					<a href="#" class="btn icon closewhite closeOne flRight" style="margin-top: 2px">&nbsp;Close</a>
					Settings: Percentage of News and Publications
					<div class="clear">&nbsp;</div>
				</div>
				<div class="section-two">
					<h6>Chart Type</h6>
						<ul class="reset list4">
							<li>
								<input type="radio" name="graphType2" value="pieChart" class="graphType" checked="checked" id="rightGraphDefaultChecked">
								<span class="btn icon piechart">Pie</span>
							</li>
							<li>
								<input type="radio" name="graphType2" value="verticalBar" class="graphType">
								<span class="btn icon barchartvert">Vertical Bar</span>
							</li>
							<li>
								<input type="radio" name="graphType2" value="HorizontalBar" class="graphType">
								<span class="btn icon barcharthori">Horizontal Bar</span>
							</li>
						</ul>
					<div class="clear">&nbsp;</div>
				</div>
				<div class="clear"></div>				
				<hr>
				<div class="btmdiv">
						<input type="button" value="Reset All" class="buttonTwo flLeft rightReset">
						<input type="button" class="buttonTwo flRight rightReset" value="Cancel">
						<input type="button" value="Apply" class="buttonOne flRight" id="rightChartSettingApply" style="margin: 0 5px 0 0;">
						<div class="clear">&nbsp;</div>
				</div>
			</div>
					
		</div>
		<div id="popupPieChartDiv" style="background-color:#ffffff;width:200px;height:265px;">
			<div id="popupDetails" style="margin-top: 0px;"></div>				
			<div id="popupPieChart"	style="overflow: hidden; display: none;"></div>
			<span style="margin-left: 50px;"><a style="color:#777; font-size:12px;"href="firms-details" target="_blank">Click to View Details</a></span>
		</div>
		<div class="clear">&nbsp;</div>
	</div>
	<div id="percentageNewsPubsChartContainer" class="charts-spacing"></div>
</div>
</div>
