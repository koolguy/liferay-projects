<%@page import="com.alm.rivaledge.model.FirmSearchModelBean"%>
<%@page import="com.alm.rivaledge.util.WebUtil"%>
<%@page import="com.alm.rivaledge.model.chart.BaseChartModelBean"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="java.util.Map"%>
	
<%@ include file="./common.jsp" %>

<portlet:actionURL var="amountOfNewsPubsURL">
	<portlet:param name="action" value="amountOfNewsPubsChartCriteriaChange"/>
</portlet:actionURL>

<portlet:actionURL var="removePortletURL">
	<portlet:param name="action" value="removePortleFromThePage"/>
</portlet:actionURL>

<script type="text/javascript">
(function () {
   

<%@ include file="./theme.jsp" %>

<c:choose>
<c:when test="${chartType eq 'stacked_horizontal_bar' }">
	var typeOfChart = 'bar';	//This will be type of chart.
	var stackingType = 'normal';
</c:when> 
<c:when test="${chartType eq 'stacked_vertical_bar' }">
	var typeOfChart = 'column';	
	var stackingType = 'normal';
</c:when>
<c:when test="${chartType eq 'horizontal_bar' }">
	var typeOfChart = 'bar';
	var stackingType = '';
</c:when>
<c:otherwise>
	var typeOfChart = 'column';
	var stackingType = '';
</c:otherwise>	
</c:choose>
	var currentMousePos = { x: -1, y: -1 };
	var titleOfChart 			= '${chartTitle}'; 			 	// This will be title of chart.
	var firmsNameList 			= [${firmsNameList}]; 		 	// This will be list of firms name.
	var newsCountByFirmList 	= [${newsCountByFirmList}]; 	// This will be news count of individual firms.
	var pubsCountByFirmList 	= [${pubsCountByFirmList}]; 	// This will be publication count of individual firms.
	var twitterCountByFirmList 	= [${twitterCountByFirmList}]; 	// This will be twitter count of individual firms. 
	
	<c:choose>
	<c:when test="${(not empty comparisonCounter) and (comparisonCounter > 0)}">
		var amountNewsPubsSplit = ${comparisonCounter};
	</c:when> 	
	<c:otherwise>
		var amountNewsPubsSplit = 0;
	</c:otherwise>
	</c:choose>

$(function() {
	
	if(<%=!isHomePage%>)
	{
		if (firmsNameList == null || firmsNameList == '') 
		{
			$("#noDataamountnewspubsDiv").removeClass("hideClass");
			$("#amountOfNewsPubsChartContainer").addClass("hideClass");
			$("#amountNewsPubsChartModelBean").parent().find(".charts").hide(); // hide the settings icon
			
			//All the above code is useless if this is the final approach
			$("#p_p_id<portlet:namespace/>").hide();
			$(".cph").show();
			return;
		}
		else
		{
			$("#noDataamountnewspubsDiv").addClass("hideClass");
		}
	}
	else
	{
		if (firmsNameList == null || firmsNameList == '') 
		{	
			$("#noDataamountnewspubsDiv").removeClass("hideClass");
			$("#amountOfNewsPubsChartContainer").addClass("hideClass");
			
			$("#No-Data-Amount").addClass("No-Data-Charts");
			$("#newspubls_amount_setting").hide();
		}
		else
		{
			$("#No-Data-Amount").removeClass("No-Data-Charts");
			$("#noDataamountnewspubsDiv").addClass("hideClass");
		}
	}	
	
	
	$(".AnalyzeResultHeader").show();
	
	// When user clicks on analysis tab active-menu css class is removed from details tab.
	// And same class added to analysis tab.
	$("#firms-details").removeClass("active-menu");
	$("#firms-analysis").addClass("active-menu");

	$("#amountOfNewsPubsApply, .closewhite").click(function(){
		$("#amountNewsPubsViewSettings").hide();
	});


	$(document).mousemove(function(event) {
        currentMousePos.x = event.pageX;
        currentMousePos.y = event.pageY;
    });
	
	$(".highcharts-tooltip span").mouseover(function(){
		$(".highcharts-tooltip span").show();
	}).mouseout(function(){
		$(".highcharts-tooltip span").hide();
	});
	
	$(".highcharts-button").click(function(){
		
		$("div#popupDiv").hide();
		$("#login-box2").hide();
	});
	$("div#popupDiv").mouseover(function(){
		$("div#popupDiv").show();
	});
	
	$("#amountOfNewsPubsCancel").click(function(){
		$("#amountNewsPubsViewSettings").hide();
	});
	
	$("#amntNewsPubsResetAll").click(function(){
		$("input[value=stacked_vertical_bar]").prop("checked", true);
		$("input[value=top_15]").prop("checked", true);
		$("#searchResultsFirmList > option").attr("selected",false);
		$("#watchList > option").attr("selected",false);
		$("input[value=Average]").prop("checked", true);
		$("input[value='RivalEdge Average']").prop("checked", true);
		$("#comparisonDataTypeList3").prop("checked", false);
		$("#comparisonDataTypeList1").prop("checked", false);
	});
	
	$("#searchResultsFirmList").click(function(){
		if($("#searchResultsFirmList option:selected")){
			$("input[value=firm]").prop("checked", true);
		}
	});
	$("#firmList").change(function(){
		$("input[value='RivalEdge Average']").prop("checked", true);
	});
	$("#watchList").click(function(){
		if($("#watchList option:selected")){
			$("input[value='Watchlist Average']").prop("checked", true);
		}
	});
	
	
	$('#amntNewsPubsPrintCharts').click(function() {
        var chart = $('#amountOfNewsPubsChartContainer').highcharts();
        chart.print();
    });
	$('#amntNewsPubsExportJPG').click(function() {
        var chart = $('#amountOfNewsPubsChartContainer').highcharts();
        chart.exportChart({type: 'image/jpeg'});
    });
	$('#amntNewsPubsExportPNG').click(function() {
        var chart = $('#amountOfNewsPubsChartContainer').highcharts();
        chart.exportChart({type: 'image/png'});
    });
	
	var splitLineAmountNewsPubsCount = parseFloat(amountNewsPubsSplit) - parseFloat(0.5);
	
	var labelXPosition = 0;
	var labelYPosition = 0;
	var labelRotation = 0;
	var dataLabelColor = '#FFFFFF';
	var amntNewsPubsChartHeight = 575;
	var amntNewsPubsChartWidth = 1000;
	
	if(stackingType == '')
	{
		dataLabelColor = '#000';
	}
	if(typeOfChart == 'bar')
	{
		labelXPosition = -5;
		labelYPosition = 0;
		labelRotation = 0;
		amntNewsPubsChartHeight = 1000;
		amntNewsPubsChartWidth = 575;
	}
	else
	{
		labelXPosition = 5;
		labelYPosition = 10;
		labelRotation = -45;
	}
	var pointWidth = 30;
		
	var totalList = [];
	for(var index = 0; index < pubsCountByFirmList.length; index++)
	{
		totalList.push(newsCountByFirmList[index] + pubsCountByFirmList[index] + twitterCountByFirmList[index]);
	}
	
	$('#amountOfNewsPubsChartContainer').highcharts(
	{
		chart : {
			type : typeOfChart
		},
		title : {
			text : titleOfChart
		},
		xAxis : {
			categories : firmsNameList,
			labels : {
				rotation: labelRotation,
				y: labelYPosition,
				x: labelXPosition,
				align: 'right',
				formatter : function() {
					var firmName = this.value;
					if (firmName.length > 15) {
						firmName = firmName
								.substring(0, 15)
								+ "...";
					}
					return firmName;
				}
			},
			plotLines : [ {
				color : '#000000',
				width : 2,
				dashStyle : 'Solid',
				value : parseFloat(splitLineAmountNewsPubsCount)
			} ]
		},
		yAxis : {
			gridLineWidth: 1,
			gridLineColor: '#cecece',
			minorGridLineColor: '#cecece',
			lineWidth: 1,
			stackLabels : {
				enabled : false,
				style : {
					fontWeight : 'bold',
					color : (Highcharts.theme && Highcharts.theme.textColor)
							|| 'gray'
				}
			}
		},
		legend : {
			enabled : true
		},
		tooltip : {
			useHTML : true,
			positioner : function(boxWidth, boxHeight,
					point) {
					$("#amountOfNewsPubsChartContainer .highcharts-tooltip span").css("height", "170px").css("width", "150px");
					var xPosition = point.plotX - 20;
					var yPosition = point.plotY - boxHeight + 38;
					
					if(typeOfChart == 'column')
					{
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(175))
						{
							xPosition = point.plotX-80;
						}
						
						if((parseInt(point.plotY) - parseInt(boxHeight)) < -20)
						{
							yPosition = point.plotY+42;
						}
					}
					else
					{
						xPosition = point.plotX + 130;
						yPosition = point.plotY - boxHeight + 150;
						
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(50))
						{
							xPosition = point.plotX-50;
						}
						
						if((parseInt(boxHeight) - parseInt(point.plotY)) > parseInt(100))
						{
							yPosition = point.plotY - boxHeight + 240;
						}
					}
					
				return {
					x : xPosition,
					y : yPosition
				};
			},
			formatter: function()
			{
				var pointerPosition = 0;
				for(var index = 0; index < firmsNameList.length; index++)
				{
					if(this.key == firmsNameList[index])
					{
						pointerPosition = index;
					}
				}
				var total = this.total;
				if(this.total == undefined)
				{
					total = parseInt(newsCountByFirmList[pointerPosition]) + parseInt(pubsCountByFirmList[pointerPosition]) + parseInt(twitterCountByFirmList[pointerPosition]);
				}
				
				var tooltipOption = '';
				tooltipOption +=  '<table style="height: 130px; width: 100%;" id="amntNewsPubsTooltipData"><tr><td colspan="2" style="width: 100%"><span style="white-space: normal;">' + this.key + '</span></td></tr><tr><td style="border: 1px solid; background-color: #000; color: #fff; margin-top: 15px; text-align: center; width:40%;">' + total + '</td><td style="color: #000; padding-left: 10px; margin-top: 15px;">Total</td></tr><tr><td style="border: 1px solid; background-color: #15375c; color: #fff; margin-top: 15px; text-align: center; width:40%;">' + newsCountByFirmList[pointerPosition] + '</td><td style="color: #15375c; padding-left: 10px; margin-top: 15px;">News</td></tr><tr><td style="border: 1px solid; background-color: #ba323e; color: #fff; margin-top: 15px; text-align: center; width:40%;">' + pubsCountByFirmList[pointerPosition] + '</td><td style="color: #ba323e; padding-left: 10px; margin-top: 15px;">Pubs</td></tr><tr><td style="border: 1px solid; background-color: #de7c35; color: #fff; margin-top: 15px; text-align: center; width:40%;">' + twitterCountByFirmList[pointerPosition] + '</td><td style="color: #de7c35; padding-left: 10px; margin-top: 15px;">Tweets</td></tr></tr></table>';
				
				
				if (pointerPosition >= amountNewsPubsSplit)
				{
				
					var url = "firms-details?drilldownFirmName=" + encodeURIComponent(this.key) + searchCriteria;
					
					tooltipOption += '<div><span class="clickToViewDetails"><a href="'+url+'">Click to View Details</a></span></div>';
				}
				
				
				return tooltipOption;
			}
		},
		plotOptions : {
			series : {
				pointWidth: 15,
				stacking : stackingType,
				dataLabels : {
					enabled : false,
					color : dataLabelColor,
					formatter : function() {
						if (parseInt(this.y) > 0) {
							return this.y;
						}
					}
				},
				point : {
					events : {
						mouseOver : function(e) {
							
							$(document).click(function() {
								$("div#popupDiv").hide();
							});
						}
					}

				}
			}
		},
		series : [ {
			name : 'News',
			color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#506a85'],
					[1, '#15375c']
				]
			},
			data : newsCountByFirmList
		}, {
			name : 'Pubs',			
			color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#cb676f'],
					[1, '#ba323e']
				]
			},
			data : pubsCountByFirmList
		}, {
			name : 'Tweets',			
			color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#e59c67'],
					[1, '#de7c35']
				]
			},
			data : twitterCountByFirmList
		}],
		navigation: {
            buttonOptions: {
                enabled: false
            }
        }
	});
	$(".settingsgry, .highcharts-button").mouseover(function(){
		$("#popupDiv").hide();
	});
		
	if(stackingType == '')
	{
		
		var chart = $("#amountOfNewsPubsChartContainer").highcharts();
		
		chart.series[chart.series.length-1].remove();
		chart.series[chart.series.length-1].remove();
		chart.series[chart.series.length-1].remove();
		
		chart.addSeries({
			showInLegend: true,
			name: 'Total',
			data: totalList,
			color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#506a85'],
					[1, '#15375c']
				]
			}
		});
		
	}	
	
});


$(document).ready(function()
		{	
		    $("#newspubls_amount").click(function()
		    {
		    	Liferay.Portlet.showPopup(
		    		{
		    			uri : '${chartSearchPortletURL}', // defined in common.jsp
		    			title: "Search Criteria"
		    		});
		    });	
		 });
		 
		 

}());


		
				

function removeThisChartNewsAndPubs()
{	
	var removePortletAction = '<%= removePortletURL.toString()%>';
	var r=confirm("Are you sure you want to delete this chart?");
	if (r == true)
  	{
		Liferay.Portlet.showBusyIcon("#bodyId", "Loading...");
		$("#amountNewsPubsChartModelBean").attr('action', removePortletAction);
		$("#amountNewsPubsChartModelBean").submit();
  	}
	else
	{
	  return false;
	}
}


</script>

<div class="newspublicationPage marbtm4 amntNewsPubsChartDiv">
	<div class="colMin flLeft leftDynamicDiv">
		<div class="topHeader ForChartsTopHeader">
		<a title="Remove this chart" onclick="removeThisChartNewsAndPubs();" href="javascript:void(0);" style="float: right; font-weight: bold; color: rgb(255, 255, 255); cursor: pointer; font-family: verdana; margin: 2px 5px; padding: 3px 8px;">X</a>
		</div>
		<div id="popupDiv" style="z-index: 999; width: 180px; position: absolute; background:#ffffff; padding: 10px 10px 10px 10px; border: 1px solid #1A1A1A; display: none;">
			<div id="popupData"></div>
		</div>
		<div id="noDataamountnewspubsDiv" class="hideClass" >0 Results, Please try a different search </div>
		<form:form commandName="amountNewsPubsChartModelBean" method="post" action="${amountOfNewsPubsURL}" id="amountNewsPubsChartModelBean" class="flRight">
			<div class="flRight charts" id="No-Data-Amount">
				<ul class="reset listView">
					<c:if test="<%=isHomePage%>" >
						<li id="newspubls_amount" style="overflow:hidden;"><a href="javascript:void(0);" class="filter-icon" >&nbsp;</a></li>
					</c:if>
					<li id="newspubls_amount_setting"><a href="#amountNewsPubsViewSettings"
						class="btn icon settingsgry rightViewSetting login-window chartViewSetting"
						onclick="return false;">&nbsp;</a></li>
					<li>
						<a href="javascript:void(0);" id="amntNewsPubsPrintCharts" onclick="return false;" class="printChartClass"></a>
					</li>
					<li>
						<a href="javascript:void(0);" onclick="return false;" class="exportChartClass"></a>
                        <div class="actionSec">
                        <h5>Actions</h5>
                        <ul class="reset">
                            <li class="exportChartImage"><span id="amntNewsPubsExportJPG">Export as JPG</span></li>
                            <li class="exportChartImage"><span id="amntNewsPubsExportPNG">Export as PNG</span></li>
                        </ul>
                        <div class="clear">&nbsp;</div>
                        </div>
					</li>
				</ul>
				<div style="display: none" class="viewBox popusdiv ClickPopup" id="amountNewsPubsViewSettings">
					<div class="popHeader">
						<a href="javascript:void(0);" class="btn icon closewhite closeOne flRight" style="margin-top: 2px">&nbsp;Close</a>
							SETTINGS: ${chartTitle}
						<div class="clear">&nbsp;</div>
					</div>
					<div class="section-one">
                      <h6>Chart Type</h6>
                        <ul class="reset list4">
                            <li>
                            <form:radiobutton path="chartType" class="graphType" value="<%=BaseChartModelBean.ChartType.STACKED_VERTICAL_BAR.getValue()%>" id="stacked_vertical_graph_id"/>
                            <span class="btn icon stackedbarchart">Stacked&nbsp;Vertical&nbsp;Bar</span>
                           	</li>
                            <li>
                              <form:radiobutton path="chartType" class="graphType" value="<%= BaseChartModelBean.ChartType.STACKED_HORIZONTAL_BAR.getValue() %>" />
                              <span class="StackHorizontalBar">Stacked&nbsp;Horizontal&nbsp;Bar</span>
                            </li>
                            <li>
                            <form:radiobutton path="chartType" class="graphType" value="<%=BaseChartModelBean.ChartType.VERTICAL_BAR.getValue()%>" />
                            <span class="btn icon barchartvert">Vertical&nbsp;Bar</span>
                           </li>
                            <li>
                            <form:radiobutton path="chartType" class="graphType" value="<%= BaseChartModelBean.ChartType.HORIZONTAL_BAR.getValue() %>" />
                            <span class="btn icon barcharthori">Horizontal&nbsp;Bar</span>
                           </li>
                        </ul>
                        <div class="clear">&nbsp;</div>
                    </div>
					<div class="section-two">
						<form action="#">
							<h6>Firm Data (Limit of 15)</h6>
							<ul class="reset list4">
								<li>
									<form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.TOP_15.getValue() %>"/>
									<span class="">Top 15</span></li>
								<li>
									<form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.BOTTOM_15.getValue() %>"/>
									<span class="">Bottom 15</span></li>
								<li>
									<form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.FIRM.getValue() %>"/>
									<span class="">Selected Firms:</span> 
									<form:select path="searchResultsFirmList" multiple="true" size="4" style="width:150px; margin:10px 0 0 0;">
										<form:options items="${allSearchResultsFirmList}" itemLabel="company" itemValue="companyId"/>
									 </form:select>
								</li>
							</ul>
							<div class="clear">&nbsp;</div>
						</form>
					</div>
					<div class="section-three">
						<div class="martp2">
							<h6>Comparison Data</h6>
							<div class="marbtm2 martp1">
								<!-- As per client requirement this options are hide. -->
								<p class="marbtm1" style="display: none;">
								<c:choose>
									<c:when test="${doIHaveFirm}">
										<form:checkbox path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.MY_FIRM.getValue() %>"/>
										<span>My Firm</span>
									</c:when>
									<c:otherwise>
										<form:checkbox path="comparisonDataTypeList" disabled="true" value="<%= BaseChartModelBean.ComparisonDataType.MY_FIRM.getValue() %>"/>
										<span title="You are not associated with any firm.">My Firm</span>
									</c:otherwise>
								</c:choose>
								</p>
								<p class="marbtm1">
									<form:checkbox path="comparisonDataTypeList" value="<%=BaseChartModelBean.ComparisonDataType.RIVAL_EDGE.getValue()%>"/>
                                     <form:select path="firmList" multiple="false">
										<form:options items="${allRankingList}"/>
									 </form:select>
								</p>
								<p class="marbtm1-last">
								<c:choose>
									<c:when test="${not empty allWatchLists}">
										<form:checkbox path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.WATCHLIST_AVG.getValue() %>" />
	                                 	<span>Watchlist</span>
	                               		<form:select path="watchList" multiple="true" size="4">
	                           		 		<form:options items="${allWatchLists}" itemValue="groupId" itemLabel="groupName"/>
										</form:select>
									</c:when>
									<c:otherwise>
										<form:checkbox path="comparisonDataTypeList" disabled="true" value="<%= BaseChartModelBean.ComparisonDataType.WATCHLIST_AVG.getValue() %>" />
	                                 		<span title="You do not have any watchlists set up.">Watchlist</span>
									</c:otherwise>
								</c:choose>
                                 	
                                </p>
                                <p class="marbtm1-last">
                                   <form:checkbox path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.AVERAGE.getValue() %>" />
                                    Average of Firms in Search </p>
							</div>
						</div>
						<div class="clear">&nbsp;</div>
					</div>
					<div class="clear">&nbsp;</div>
					<hr>
					<div class="btmdiv">
						<input type="button" value="Reset All" class="buttonTwo flLeft settingReset" id="amntNewsPubsResetAll">
						<input type="button" class="buttonTwo flRight rightReset" value="Cancel" id="amountOfNewsPubsCancel">
						<input type="button" value="Apply" class="buttonOne flRight" id="amountOfNewsPubsApply" style="margin: 0 5px 0 0;" onclick="applyChartSettings('#amountNewsPubsChartModelBean');">
						<div class="clear">&nbsp;</div>
					</div>
				</div>
			</div>
		</form:form>
		<!-- </div> -->
		<div id="amountOfNewsPubsChartContainer" class="charts-spacing" style="position: relative;"></div>
	</div>
	<div class="clear">&nbsp;</div>
</div>
