<%@page import="com.alm.rivaledge.model.FirmSearchModelBean"%>
<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"      uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %>

<div class="flLeft resultsec"><strong>${firmSearchModelBean.currentPageSize}&nbsp;of&nbsp;${firmSearchModelBean.totalResultCount} Results</strong> 
	<span>(
    	<c:choose>
   			<c:when test="${fn:length(firmSearchModelBean.selectedFirms) gt 100}">
   				${fn:substring(firmSearchModelBean.selectedFirms, 0, 99)}..,&nbsp;
   			</c:when>
   			<c:otherwise>
   				${firmSearchModelBean.selectedFirms},&nbsp;
   			</c:otherwise>		        		  
		</c:choose>				
   		${firmSearchModelBean.selectedContentType},&nbsp;	      				
 		${firmSearchModelBean.dateText},&nbsp;  
		<c:choose>
   			<c:when test="${fn:length(firmSearchModelBean.keywords) gt 100}">
   				${fn:substring(firmSearchModelBean.keywords, 0, 99)}..,&nbsp;
   			</c:when>
   			<c:otherwise>
   				<c:if test="${not empty firmSearchModelBean.keywords}">
   					${firmSearchModelBean.keywords}, 
   				 </c:if>
   			</c:otherwise>		        		  
		</c:choose>
		<c:choose>
   			<c:when test="${fn:length(firmSearchModelBean.selectedPracticeArea) gt 100}">
   				${fn:substring(firmSearchModelBean.selectedPracticeArea, 0, 99)}..,&nbsp;
   			</c:when>
   			<c:otherwise>
   				${firmSearchModelBean.selectedPracticeArea},&nbsp;
   			</c:otherwise>		        		  
		</c:choose>      				 
   			${firmSearchModelBean.selectedTwitterPostType}&nbsp;	      		
      	)</span>
  		
</div>