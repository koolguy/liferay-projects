<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"      uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %> 

<portlet:defineObjects/>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>ALM Rival Edge</title>
<link href="<%= renderRequest.getContextPath() %>/css/main.css" rel="stylesheet" type="text/css"/>
<link rel="shortcut icon" type="<%= renderRequest.getContextPath() %>/images/x-icon" href="images/favicon.ico"/>

</head>
<body style="background:#fff">
<div class="printSec">
  <div class="topBar">
    <ul class="tplist reset flRight">
      <li><a href="#" onClick="window.print()">Print</a></li>
      <li><a href="#" onClick="window.close()">Close</a></li>
    </ul>
    <div class="clear">&nbsp;</div>
  </div>
  <div>
    <div class="dateSec flRight">Date Printed : ${printedDateStr} at ${formattedTime}</div>
    <h1 id="logo"><a href="#"><img src="<%= renderRequest.getContextPath() %>/images/logo1.png" width="154" height="27" alt="logo"/></a></h1>
    <div class="clear">&nbsp;</div>
  </div>
  <h4>Firm News and Publications</h4>
  
<div class="flLeft resultsec"><strong>${firmSearchModelBean.totalResultCount}&nbsp;of&nbsp;${firmSearchModelBean.totalResultCount} Results</strong> 
  		 <span>(
          		<c:choose>
	      				 <c:when test="${fn:length(firmSearchModelBean.selectedFirms) gt 100}">
	      				 	${fn:substring(firmSearchModelBean.selectedFirms, 0, 99)}..,&nbsp;
	      				 </c:when>
	      				 <c:otherwise>
	      				 	${firmSearchModelBean.selectedFirms},&nbsp;
	      				 </c:otherwise>		        		  
				</c:choose>				
	      		${firmSearchModelBean.selectedContentType},&nbsp;	      				
				 ${firmSearchModelBean.dateText},&nbsp;  
				<c:choose>
	      				 <c:when test="${fn:length(firmSearchModelBean.keywords) gt 100}">
	      				 	${fn:substring(firmSearchModelBean.keywords, 0, 99)}..,&nbsp;
	      				 </c:when>
	      				  <c:otherwise>
	      				 	<c:if test="${not empty firmSearchModelBean.keywords}">
	      				 		${firmSearchModelBean.keywords}, 
	      				 	</c:if>
	      				 </c:otherwise>		        		  
				</c:choose>
				<c:choose>
	      				 <c:when test="${fn:length(firmSearchModelBean.selectedPracticeArea) gt 100}">
	      				 	${fn:substring(firmSearchModelBean.selectedPracticeArea, 0, 99)}..,&nbsp;
	      				 </c:when>
	      				 <c:otherwise>
	      				 	${firmSearchModelBean.selectedPracticeArea},&nbsp;
	      				 </c:otherwise>		        		  
				</c:choose>      				 
	      				${firmSearchModelBean.selectedTwitterPostType}&nbsp;	      		
          )</span>
  		
  		</div>
  		
  <div class="clear">&nbsp;</div>
   
   
         <table class="tbleTwo" width="100%" border="0" cellspacing="0" cellpadding="0">
          
          <colgroup>
            	<!--<col width="50"/>--> <!-- commented checkbox to fix bug RER 305 -->
                <col width="36"/>
                <col width="90"/>
                <col width="100"/>
                <col width="130"/>
                <col width="600"/> <!-- changed from 550 to 600, and commented checkbox column -->
                <col width="150"/>
          </colgroup>
           <thead>
            <tr>
              <th>Type</th>
              <th>Date</th>
              <th>Firm</th>
              <th>Source</th>
              <th>Description</th>
              <th>Related Practice</th>
            </tr>
          </thead>
           <tbody>
      		
      			<c:if test="${fn:length(firmResultsDTOList) gt 0}">
		      			 <c:forEach items="${firmResultsDTOList}" var="results"  varStatus="loopStatus">
					      	 <%-- orderBy values 2 = firm_name
					      	 					 3 = data_type	
				<!-- debugging purpose-->
				<script> console.log("${loopStatus.index}. loopStatus.first > ${loopStatus.first}      orderBy -> ${firmSearchModelBean.orderBy}  condition -> ${(loopStatus.first) && (firmSearchModelBean.orderBy == 'data_type')}");</script>
				 --%> 
				 <%--The below choose cond'n  is responsible for grouping the results in Table in UI--%>
				<c:choose>
						<%-- For first row in Table --%>
   						 <c:when test="${(loopStatus.first) && (firmSearchModelBean.orderBy == 2)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.firmName}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(loopStatus.first) && (firmSearchModelBean.orderBy == 3)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.dataType}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 <%-- For subsequent rows in Table only when there is change in the firmName or dataType in 2 consecutive records --%>
   						 <c:when test="${(loopStatus.index > 0) && (firmSearchModelBean.orderBy == 2) && (firmResultsDTOList[loopStatus.index - 1].firmName != firmResultsDTOList[loopStatus.index].firmName)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.firmName}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(loopStatus.index > 0) && (firmSearchModelBean.orderBy == 3) && (firmResultsDTOList[loopStatus.index - 1].dataType != firmResultsDTOList[loopStatus.index].dataType)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.dataType}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 
				</c:choose>
					      	 
					      	 
			      	 <tr class="${loopStatus.index % 2 == 0 ? 'odd' : 'even'}">
						<td>
				        	${results.dataType}				        	
				        </td>
				        <td><fmt:formatDate pattern="MM/dd/yyyy" value="${results.entryDate}"/></td>
				        <td> <a href="http://www.${results.firmURL}" target="_blank"  ><span style="text-decoration:underline">${results.firmName}</span></a></td>
				        <td>
				         	<c:choose>
							  <c:when test="${empty results.dataSourceURL}">
							   ${results.dataSource}
							  </c:when>				 
							  <c:otherwise>
							    <a href="${results.dataSourceURL}" target="_blank">${results.dataSource}</a>
							  </c:otherwise>
							</c:choose>
						</td>
				        <td><a href="${results.entryURL}" target="_blank"> <span style="text-decoration:underline">${results.entryTitle}</span></a> <br/>
			      			<c:choose>
				      			<c:when test="${results.dataType != 'Twitter'}">  
					      			<c:choose>
					      				 <c:when test="${fn:length(results.entryDescription) gt 254}">
					      				 	${fn:substring(results.entryDescription, 0, 251)}...
					      				 </c:when>
					      				 <c:otherwise>
					      				 	${results.entryDescription} 
					      				 </c:otherwise>		        		  
					        		</c:choose>
				        		</c:when>
					        		<c:otherwise>
					        			&nbsp;
					        		</c:otherwise>
			        		</c:choose>
				       </td>   
				        <td>${results.practiceArea}</td>
			      	</tr>  
					      	    
		      	</c:forEach>
		      	</c:if>
		      	</tbody>
		      	
		  </table>
		      
       
</div>
</body>
<script src="<%=renderRequest.getContextPath()%>/js/s_code.js"></script>
<script type="text/javascript">
	// Omniture SiteCatalyst - START
	s.prop22 = "premium";
	s.pageName="rer:firm-search-Print-Publications";
	s.channel="rer:firm-search";
	s.server="rer";
	s.prop1="Firm-Search"
	s.prop2="Firm-Search";
	s.prop3="Firm-Search";
	s.prop4="Firm-Search";
	s.prop21=s.eVar21='current.user@foo.bar';	
	s.events="event2";
	s.events="event27";
	s.t();
	// Omniture SiteCatalyst - END
</script>
</html>
