<%@page import="com.alm.rivaledge.util.WebUtil"%>
<%@page import="java.util.Map"%>
<%@ taglib prefix="portlet" 		uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" 			 	uri="http://java.sun.com/jsp/jstl/core" %>

<portlet:defineObjects/>

<%@ include file="./common.jsp" %>

<portlet:actionURL var="removePortletURL">
	<portlet:param name="action" value="removePortleFromThePage"/>
</portlet:actionURL>

<script type="text/javascript">

function removeThisChartPercentNewsAndPubs()
{	
	var r=confirm("Are you sure you want to delete this chart?");
	if (r == true)
  	{
		Liferay.Portlet.showBusyIcon("#bodyId", "Loading...");
		$("#removeChartPercentNewsPubs").submit();
  	}
	else
	{
	  return false;
	}
}

(function () {
	
<%@ include file="./theme.jsp" %>

	var percentNewsPubsTypeOfChart 				= '${percentNewsPubsChartType}'; 			 	//This will be type of chart.
	var percentNewsPubsTitleOfChart 			= '${percentNewsPubsChartTitle}'; 			 	// This will be title of chart.
	var percentNewsPubsFirmsNameList 			= [${percentNewsPubsFirmsNameList}]; 		 	// This will be list of firms name.
	var percentNewsPubsNewsCountByFirmList 		= [${percentNewsPubsNewsCountByFirmList}]; 	// This will be news count of individual firms.
	var percentNewsPubsPubsCountByFirmList 		= [${percentNewsPubsPubsCountByFirmList}]; 	// This will be publication count of individual firms.
	var percentNewsPubsTwitterCountByFirmList 	= [${percentNewsPubsTwitterCountByFirmList}]; 	// This will be twitter count of individual firms. 
	var percentNewsPubsTotalCountByFirmList 	= [${percentNewsPubsTotalCountByFirmList}]; 	// This will be total count of individual firms. 

$(function()
{
	if(<%=!isHomePage%>)
	{
		if (percentNewsPubsFirmsNameList == null || percentNewsPubsFirmsNameList == '')
		{
			$("#noDataPercentnewspubsDiv").removeClass("hideClass");
			$("#percentageNewsPubsChartContainer").addClass("hideClass");
			$("#noDataPercentnewspubsDiv").parent().find(".charts").hide(); // hide the settings icon
	
			//All the above code is useless if this is the final approach
			$("#p_p_id<portlet:namespace/>").hide();
			$(".cph").show();
			return;
		}
	}
	else
	{
		if (percentNewsPubsFirmsNameList == null || percentNewsPubsFirmsNameList == '')
		{
			$("#noDataPercentnewspubsDiv").removeClass("hideClass");
			$("#percentageNewsPubsChartContainer").addClass("hideClass");
			
			$("#No-Data-PercNews").addClass("No-Data-Charts");
			$("#perc_newsPubs_settings").hide();
		}
		else
		{
			$("#No-Data-PercNews").removeClass("No-Data-Charts");
			$("#noDataPercentnewspubsDiv").addClass("hideClass");
		}	
	}

	Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function(color) {
	    return {
	        radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
	        stops: [
	            [0, color],
	            [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
	        ]
	    };
	});

	$(".AnalyzeResultHeader").show();
	
	$("#rightChartSettingApply, .closewhite").click(function(){
		$("#login-box2").hide();
	});
	
	$(".highcharts-button").click(function(){
		$("div#popupPieChartDiv").hide();
		$("#login-box2").hide();
	});
	
	$("#rightChartSettingApply").click(function(){
		$("#login-box2").hide();
		if($('input[name=graphType2]:radio:checked').val() == 'verticalBar')
		{
			displayBarGraph('column');
		} else if($('input[name=graphType2]:radio:checked').val() == 'HorizontalBar')
		{
			displayBarGraph('bar');
		}else if($('input[name=graphType2]:radio:checked').val() == 'pieChart')
		{
			displayPieChart();
		}
	});
	$("#percentNewsPubsResetAll").click(function(){
		$("#rightGraphDefaultChecked").prop('checked', true);
	});
	
	$("#percentNewsPubsSettingCancel").click(function(){
		$("#login-box2").hide();
	});
	
	$('#percentNewsPubsPrintCharts').click(function() {
        var chart = $('#percentageNewsPubsChartContainer').highcharts();
        chart.print();
    });
	$('#percentNewsPubsExportJPG').click(function() {
        var chart = $('#percentageNewsPubsChartContainer').highcharts();
        chart.exportChart({type: 'image/jpeg'});
    });
	$('#percentNewsPubsExportPNG').click(function() {
        var chart = $('#percentageNewsPubsChartContainer').highcharts();
        chart.exportChart({type: 'image/png'});
    });
	
	// When user clicks on analysis tab active-menu css class is removed from details tab.
	// And same class added to analysis tab.
	$("#firms-details").removeClass("active-menu");
	$("#firms-analysis").addClass("active-menu");
	
	$("div#popupPieChartDiv").mouseover(function(){
		$("div#popupPieChartDiv").show();
	}).mouseout(function(){
		$("div#popupPieChartDiv").hide();
	});
	$(".highcharts-series-group").mouseout(function(){
		$("div#popupPieChartDiv").hide();
	});
	
	$("div#popupPieChartDiv").mouseover(function(){
		$("div#popupPieChartDiv").show();
	});
	$(document).click(function(e){
		$("#percentagePopupDiv").hide();
	});
	
	$("#percentagePopupDiv").mouseover(function(){
		$("#percentagePopupDiv").show();
	});
	
	displayPieChart();
});
function displayPieChart()
{
	var percentNewsPubsPieChartSeries = []; // This will be series of data.
	 
	for(var index=0; index < percentNewsPubsFirmsNameList.length; index++)
	{
		// This will be pie chart data.
	 	// First attribute firm name and second will be total count.
		var percentNewsPubsPieChartData = [];
		percentNewsPubsPieChartData.push(percentNewsPubsFirmsNameList[index]);
		percentNewsPubsPieChartData.push(percentNewsPubsTotalCountByFirmList[index]);
		percentNewsPubsPieChartSeries.push(percentNewsPubsPieChartData);
	}
	var showLegend = false;
	if(percentNewsPubsFirmsNameList.length <= 15){
		showLegend = true;
	}
	
	// Generate graph.
	$('#percentageNewsPubsChartContainer').highcharts(
	{
        chart:
		{
			
        },
        title:
		{
            text: percentNewsPubsTitleOfChart
        },
        tooltip:
		{
    	    pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>',
			formatter: function(e){
           	 	$("#login-box2").hide();
         		popupPieChart(this.key);
         		return false;
         	}
        },
        plotOptions:
		{
            pie:
			{
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                	 distance: -30,
                     enabled: true,
                     color: '#ffffff',
                     formatter: function(){
						if(this.percentage > 0)
						{
							return parseFloat(this.percentage).toFixed(0)+"%";
						}
                     }
                 },
				showInLegend: true,
				point: {
					events: {
                       mouseOver: function(e) {
                       		$("#popupDiv").hide();
                            $("#popupPieChart").show();
							$("#popupPieChartDiv").show();
							$(document).click(function() {
								$("div#popupPieChartDiv").hide();
							});
						},
						mouseOut: function(e){
							$("div#popupPieChartDiv").hide();
						},
						legendItemClick: function () {
							return false; // <== returning false will cancel the default action
						}
                     }
                 }
			}
        },
		legend: {
            enabled: showLegend,
			borderColor: '#fff',
			itemStyle: {
                paddingBottom: '7px'
            },
            layout: 'vertical',
            align: 'right',
            width: 200,
            verticalAlign: 'middle',
            useHTML: true,
            labelFormatter: function() {
                return '<div style="text-align: left; width:130px;">' + this.name + '</div>';
            }
         },
        series: [
		{
            type: 'pie',
            name: percentNewsPubsTitleOfChart,
            data: percentNewsPubsPieChartSeries
        }],
		navigation: {
            buttonOptions: {
                enabled: false
            }
        }
    });
}
function popupPieChart(key){
	$("#percentageNewsPubsChartContainer").mousemove(function(event){
		
		var parentOffset = $(this).parent().offset();
        var relativeXPosition = (event.pageX);
        var relativeYPosition = (event.pageY);
		$("#popupPieChartDiv").css("top", relativeYPosition-300).css("left", relativeXPosition-100)
	});
	
	var firmIndex = 0;
	for(var index=0; index < percentNewsPubsFirmsNameList.length; index++)
	{
		if(key == percentNewsPubsFirmsNameList[index])
		{
			firmIndex = index;
		}
	}

	var popupDetails = "<table width='100%'><tr><th colspan='4' style='font-size:12px; color:#000'>"+key+"</th></tr><tr><th style='padding:2px;color:#777; font-size:12px;'>Overall</th><th style='padding:2px; color:#777; font-size:12px;'>News</th><th style='padding:2px;color:#777; font-size:12px;'>Pubs</th><th style='padding:2px; color:#777; font-size:12px;'>Tweets</th></tr><tr style='line-height:15px;'><td style='background:#777777; text-align:center; border:1px solid #333; color:#777; color: #FFFFFF; font-size: 14px; font-weight: bold;'>"+percentNewsPubsTotalCountByFirmList[firmIndex]+"</td><td style='text-align:center; border:1px solid #333; color:#2f6d9f; font-size: 14px; font-weight: bold;'>"+percentNewsPubsNewsCountByFirmList[firmIndex]+"</td><td style='text-align:center; border:1px solid #333; color:#2f6d9f; font-size: 14px; font-weight: bold;'>"+percentNewsPubsPubsCountByFirmList[firmIndex]+"</td><td style='text-align:center; border:1px solid #333; color:#2f6d9f;font-size: 14px; font-weight: bold;'>"+percentNewsPubsTwitterCountByFirmList[firmIndex]+"</td></tr></table>";

	var encodeUrl = "firms-details?drilldownFirmName=" + encodeURIComponent(key) + searchCriteria;

	$("#percentNewsPubsViewDetails").attr("href", encodeUrl);
	
	$("#popupDetails").html(popupDetails);
	
	 $('#popupPieChart').highcharts({
         chart: {
             plotBackgroundColor: null,
             plotBorderWidth: null,
             plotShadow: false,
             width: 200,
             height: 200
         },
         title: {
             text: ''
         },
         tooltip: {
     	    pointFormat: '{series.name}: <b>{parseFloat(point.percentage).toFixed(0)}%</b>',
         	percentageDecimals: 1
         },
         plotOptions: {
             pie: {
                 allowPointSelect: true,
                 cursor: 'pointer',
                 dataLabels: {
                	 distance: -30,
                     enabled: true,
                     color: '#ffffff',
                     formatter: function(){
                        if(this.percentage > 0)
						{
							return parseFloat(this.percentage).toFixed(0)+"%";
						}
                     }
                 },
                 showInLegend: true,
                 point: {
					events: {
						legendItemClick: function () {
							return false; // <== returning false will cancel the default action
						}
                     }
                 }
             }
         },
         series: [{
             type: 'pie',
             name: 'PERCENTAGE OF NEWS AND PUBLICATIONS',
             data: [
                    ['News', percentNewsPubsNewsCountByFirmList[firmIndex]],
                    ['Pubs', percentNewsPubsPubsCountByFirmList[firmIndex]],
                    ['Tweets', percentNewsPubsTwitterCountByFirmList[firmIndex]]
                   ]
         }],
         exporting: {
             enabled: false
         }
     });
}
function displayBarGraph(type)
{
	var labelXPosition = 0;
	var labelYPosition = 0;
	var labelRotation = 0;
	if(type == 'bar')
	{
		labelXPosition = -5;
		labelYPosition = 0;
		labelRotation = 0;
	}
	else
	{
		labelXPosition = 5;
		labelYPosition = 10;
		labelRotation = -45;
	}
	
	var totalList = [];
	for(var index = 0; index < percentNewsPubsFirmsNameList.length; index++)
	{
		totalList.push(percentNewsPubsNewsCountByFirmList[index] + percentNewsPubsPubsCountByFirmList[index] + percentNewsPubsTwitterCountByFirmList[index]);
		
	}

	
	$('#percentageNewsPubsChartContainer').highcharts({
        chart: {
            type: type
        },
        title:
		{
            text: percentNewsPubsTitleOfChart
        },
        xAxis: {
            categories: percentNewsPubsFirmsNameList,
            labels: 
            {
                rotation: labelRotation,
                y: labelYPosition,
				x: labelXPosition,
				align: 'right',
				formatter: function()
				{
					var firmName = this.value;
					if(firmName.length > 10)
					{
						firmName = firmName.substring(0, 10) + "...";
					}
					return firmName;
				}
			}
        },
        yAxis: {
            min: 0,
            
            stackLabels: {
                enabled: false,
                style: {
                    fontWeight: 'bold',
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                }
            }
        },
        legend: {
        	enabled: true
    	},
        tooltip : {
				useHTML : true,
				positioner : function(boxWidth, boxHeight,
						point) {
						
						var xPosition = point.plotX - 20;
						var yPosition = point.plotY - boxHeight + 30;
						
						if(type == 'column')
						{
							if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(650))
							{
								xPosition = point.plotX-125;
							}
							
							if(parseInt(point.plotY) < parseInt(boxHeight))
							{
								yPosition = point.plotY+90;
							}
						}
						else
						{
							xPosition = point.plotX + 120;
							yPosition = point.plotY - boxHeight + 100;
							
							if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(100))
							{
								xPosition = point.plotX;
							}
							
							if((parseInt(boxHeight) - parseInt(point.plotY)) > parseInt(100))
							{
								yPosition = point.plotY - boxHeight + 150;
							}
						}
						
					return {
						x : xPosition,
						y : yPosition
					};
				},
				formatter: function()
				{
					var pointerPosition = 0;
					for(var index = 0; index < percentNewsPubsFirmsNameList.length; index++)
					{
						if(this.key == percentNewsPubsFirmsNameList[index])
						{
							pointerPosition = index;
						}
					}
					var total = this.total;
					if(this.total == undefined)
					{
						total = parseInt(percentNewsPubsNewsCountByFirmList[pointerPosition]) + parseInt(percentNewsPubsPubsCountByFirmList[pointerPosition]) + parseInt(percentNewsPubsTwitterCountByFirmList[pointerPosition]);
					}
					var tooltipOption = '';
					var url = "firms-details?drilldownFirmName=" + encodeURIComponent(this.key) + searchCriteria;
					tooltipOption +=  '<table width="100%"><tr><td colspan="2" style="width: 100%"><span style="white-space: normal; font-weight: bold; color: #000;">' + this.key + ' - Total News and Pubs</span></td></tr><tr></tr><tr style="width: 50%;"><td style="color: #000; padding-left: 10px; margin-top: 15px; text-align: center; width: 25%;">Total</td><td style="border: 1px solid; background-color: #000; color: #fff; margin-top: 15px; text-align: center; width: 25%;">' + total + '</td></tr><tr style="width: 50%;"><td style="color: #446e49; padding-left: 10px; margin-top: 15px; text-align: center; width: 25%;">News</td><td style="border: 1px solid; background-color: #446e49; color: #fff; margin-top: 15px; text-align: center; width: 25%;">' + percentNewsPubsNewsCountByFirmList[pointerPosition] + '</td></tr><tr style="width: 50%;"><td style="color: #0c3362; padding-left: 10px; margin-top: 15px; text-align: center; width: 25%;">Pubs</td><td style="border: 1px solid; background-color: #0c3362; color: #fff; margin-top: 15px; text-align: center; width: 25%;">' + percentNewsPubsPubsCountByFirmList[pointerPosition] + '</td></tr><tr style="width: 50%;"><td style="color: #b89024; padding-left: 10px; margin-top: 15px; text-align: center; width: 25%;">Tweets</td><td style="border: 1px solid; background-color: #b89024; color: #fff; margin-top: 15px; text-align: center; width: 25%;">' + percentNewsPubsTwitterCountByFirmList[pointerPosition] + '</td></tr></tr><tr><td colspan="2" style="text-align: center;"><a href="'+url+'">Click to View Details</a></td></tr></table>';
					return tooltipOption;
				}
			},
        plotOptions: {
        	column: {
                
                dataLabels: {
                    enabled: false,
                    color: '#000000',
                    formatter: function(){
                    	if(this.y > 0){
                    		return this.y;
                    	}
                    }
                }
            },
			bar: {
				dataLabels: {
					enabled: false
				},
			},
			series: {
				pointWidth: type == 'bar' ? 20:30,
			}
        },
        series: [
        {
            name: 'Total',
            color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#506a85'],
					[1, '#15375c']
				]
			},
            data: totalList
        }],
		navigation: {
            buttonOptions: {
                enabled: false
            }
        }
    });
}

$(document).ready(function(){	
	$("#perc_newsPubs").click(function(){
		Liferay.Portlet.showPopup({
			uri : '${chartSearchPortletURL}', // defined in common.jsp
       		title: "Search Criteria"
		});
	});	
});

}());

</script>

<div class="newspublicationPage percentNewsPubsChartDiv">
	<div class="colMin flLeft leftDynamicDiv">
		<div class="topHeader ForChartsTopHeader">
					<span title="Remove this chart" onclick="removeThisChartPercentNewsAndPubs();" style="float: right; font-weight: bold; color: rgb(255, 255, 255); cursor: pointer; font-family: verdana; margin: 2px 5px; padding: 3px 8px;">X</span>
		</div>
		<div id="noDataPercentnewspubsDiv" class="hideClass">0 Results, Please try a different search</div>
		<div class="flRight charts" id="No-Data-PercNews">
			<ul class="reset listView">
				<c:if test="<%=isHomePage%>" >
					<li id="perc_newsPubs" style="overflow:hidden;"><a href="javascript:void(0);" class="filter-icon" >&nbsp;</a></li>
				</c:if>
				<li id="perc_newsPubs_settings"><a href="#login-box2" class="btn icon settingsgry rightViewSetting login-window chartViewSetting" onclick="return false;">&nbsp;</a></li>
				<li>
					<a href="javascript:void(0);" id="percentNewsPubsPrintCharts" onclick="return false;" class="printChartClass"></a>
				</li>
				<li>
					<a href="javascript:void(0);" id="percentNewsPubsExportCharts" onclick="return false;" class="exportChartClass"></a>
                        <div class="actionSec">
                        <h5>Actions</h5>
                        <ul class="reset">
                            <li class="exportChartImage"><span id="percentNewsPubsExportJPG">Export as JPG</span></li>
                            <li class="exportChartImage"><span id="percentNewsPubsExportPNG">Export as PNG</span></li>
                        </ul>
                        <div class="clear">&nbsp;</div>
                        </div>
				</li>
			</ul>
			<div id="login-box2" class="popusdiv login-popupTwo onclickpopup" style="width:400px !important;">
				<div class="popHeader">
					<a href="javascript:void(0);" class="btn icon closewhite closeOne flRight" style="margin-top: 2px">&nbsp;Close</a>
					Settings: Percentage of News and Publications
					<div class="clear">&nbsp;</div>
				</div>
				<div class="section-two">
					<h6>Chart Type</h6>
						<ul class="reset list4">
							<li>
								<input type="radio" name="graphType2" value="pieChart" class="graphType" checked="checked" id="rightGraphDefaultChecked">
								<span class="btn icon piechart">Pie</span>
							</li>
							<!-- As per client requirement this options are hide. -->
							<li style="display: none;">
								<input type="radio" name="graphType2" value="verticalBar" class="graphType">
								<span class="btn icon barchartvert">Vertical Bar</span>
							</li>
							<li style="display: none;">
								<input type="radio" name="graphType2" value="HorizontalBar" class="graphType">
								<span class="btn icon barcharthori">Horizontal Bar</span>
							</li>
						</ul>
					<div class="clear">&nbsp;</div>
				</div>
				<div class="clear"></div>				
				<hr>
				<div class="btmdiv">
						<input type="button" value="Reset All" class="buttonTwo flLeft rightReset" id="percentNewsPubsResetAll">
						<input type="button" class="buttonTwo flRight rightReset" value="Cancel" id="percentNewsPubsSettingCancel">
						<input type="button" value="Apply" class="buttonOne flRight" id="rightChartSettingApply" style="margin: 0 5px 0 0;">
						<div class="clear">&nbsp;</div>
				</div>
			</div>
					
		</div>
		<div id="popupPieChartDiv" style="background-color:#ffffff;width:200px; box-shadow:1px 4px 8px #444;">
			<div id="popupDetails" style="margin-top: 0px;"></div>				
			<div id="popupPieChart"	style="overflow: hidden; display: none;"></div>
			<span style="margin-left: 50px;"><a style="color:#000000; font-size:12px;"href="firms-details" id="percentNewsPubsViewDetails">Click to View Details</a></span>
		</div>
	
	<div id="percentageNewsPubsChartContainer" class="charts-spacing"></div>
	<form method="post" action="${removePortletURL}" id="removeChartPercentNewsPubs">
	</form>
</div>
</div>
