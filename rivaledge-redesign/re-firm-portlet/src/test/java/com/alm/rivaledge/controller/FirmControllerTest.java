package com.alm.rivaledge.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.portlet.MockRenderRequest;
import org.springframework.mock.web.portlet.MockRenderResponse;
import org.springframework.mock.web.portlet.MockResourceRequest;
import org.springframework.mock.web.portlet.MockResourceResponse;
import org.springframework.ui.ModelMap;
import org.springframework.web.portlet.ModelAndView;

import com.alm.rivaledge.BaseALMTest;
import com.alm.rivaledge.controller.results.FirmResultsController;
import com.alm.rivaledge.model.FirmSearchModelBean;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.transferobject.FirmSearchDTO;
import com.alm.rivaledge.util.ALMConstants;

public class FirmControllerTest extends BaseALMTest
{
	@Autowired
	private FirmResultsController testController;
	
	/**
	 * Test that the print preview returns the correct view.
	 */
	@Test
	public void testPrintPreview()
	{
		printTitle();
		assertNotNull(testController);
		
		MockRenderRequest testRequest = new MockRenderRequest();
		MockRenderResponse testResponse = new MockRenderResponse();
		String viewName = null;
		testRequest.setParameter("displayPrintPage", "yes");
		try
		{
//			viewName = testController.printPublications(testRequest, testResponse);
		}
		catch (Exception ex)
		{
			// Do nothing
		}
		assertEquals("printPublications", viewName);
	}

	/**
	 * Test that the form view returns everything needed for the search.
	 */
	@Test
	@SuppressWarnings("unchecked")
	public void testDefaultFormView()
	{
		printTitle();
		assertNotNull(testController);
		
		MockRenderRequest testRequest = new MockRenderRequest();
		MockRenderResponse testResponse = new MockRenderResponse();
		String viewName = null;
		ModelMap modelMap = new ModelMap();
		try
		{
//			viewName = testController.newsAndPublicationSearchForm(modelMap, testRequest, testResponse);
		}
		catch (Exception ex)
		{
			// Do nothing
		}
		if (viewName == null)
		{
			System.out.println("\n\n*** viewName = NULL");
		}
		assertEquals("firmsearch", viewName);

		assertTrue(modelMap.keySet().contains("allFirms"));
		assertTrue(modelMap.keySet().contains("allPracticeArea"));
		
		List<Firm> testFirms = (List<Firm>) modelMap.get("allFirms");
		assertNotNull(testFirms);
	}
	
	
	/**
	 * Test set Date range functionality
	 */
	@Test	
	public void testSetDateRange()
	{
		printTitle(); 
		Calendar calendar = Calendar.getInstance();
		Date currentDate = new Date();
		
		ModelMap map = new ModelMap();
		FirmSearchDTO firmSearchDTO =new FirmSearchDTO();
		FirmSearchModelBean firmSearchModelBean = new FirmSearchModelBean();
		
		firmSearchModelBean.setDateText(ALMConstants.LAST_90_DAYS);
		
//		testController.setDateRange(map, firmSearchDTO, firmSearchModelBean);
		calendar.add(Calendar.MONTH, -3);
		assertTrue(firmSearchDTO.getToDate().toString().equals(currentDate.toString()));
		assertTrue(firmSearchDTO.getFromDate().toString().equals(calendar.getTime().toString()));
		firmSearchModelBean.setDateText(ALMConstants.ANY);

//		testController.setDateRange(map, firmSearchDTO, firmSearchModelBean);
		assertNull(firmSearchDTO.getToDate());
		assertNull(firmSearchDTO.getFromDate());
		
		
	}
	
	/**
	 * Test selected Individual Firms
	 */
	@Test	
	public void testSelectedIndividualFirms()
	{
		printTitle(); 
		ModelMap map = new ModelMap();
		FirmSearchDTO firmSearchDTO =new FirmSearchDTO();
		FirmSearchModelBean firmSearchModelBean = new FirmSearchModelBean();
		
		firmSearchModelBean.setFirmType(ALMConstants.INDIVIDUAL_LIST);
		firmSearchModelBean.setFirmList(Arrays.asList(new String[]{"47","31"}));

//		testController.setSelectedFirms(map, firmSearchDTO, firmSearchModelBean);
		assertTrue(firmSearchDTO.getSelectedFirms().size()==2);
		assertTrue(firmSearchDTO.getSelectedFirms().get(0).getCompany().equals("DLA Piper"));
		assertTrue(firmSearchDTO.getSelectedFirms().get(1).getCompany().equals("Chadbourne Parke"));
	}
	
	/**
	 * Test selected Rivaledge Firms
	 */
	@Test	
	public void testSelectedRivalEdgeFirms()
	{
		printTitle(); 
		
		ModelMap map = new ModelMap();
		FirmSearchDTO firmSearchDTO =new FirmSearchDTO();
		FirmSearchModelBean firmSearchModelBean = new FirmSearchModelBean();
		
		firmSearchModelBean.setFirmType(ALMConstants.RIVALEDGE_LIST);
		firmSearchModelBean.setFirmList(Arrays.asList(new String[]{ALMConstants.AMLAW_100}));

//		testController.setSelectedFirms(map, firmSearchDTO, firmSearchModelBean);
		
		assertTrue(firmSearchDTO.getSelectedFirms().size()>0);		
	}
	
	/**
	 * Test default values of Firm search Form
	 */
	@Test	
	public void testSetOtherSearchParameterWithDefaultValues()
	{
		printTitle();
		ModelMap map = new ModelMap();
		FirmSearchDTO firmSearchDTO =new FirmSearchDTO();
		FirmSearchModelBean firmSearchModelBean = new FirmSearchModelBean();
		
		firmSearchModelBean.setPracticeArea(Arrays.asList(new String[]{ALMConstants.ALL_PRACTICE_AREAS}));
		firmSearchModelBean.setContentType(Arrays.asList(new String[]{ALMConstants.ALL_Types}));
		firmSearchModelBean.setTwitterType(Arrays.asList(new String[]{ALMConstants.ALL}));
		
		try
		{
//		testController.setOtherSearchParameter(map,firmSearchDTO, firmSearchModelBean);
		} 
		catch (Exception ex)
		{
			// Do nothing
		}
		assertNull(firmSearchDTO.getPracticeArea());
		assertNull(firmSearchDTO.getContentType());
		assertNull(firmSearchDTO.getTwitterPostType());
			
		
	}
	
	
	/**
	 * Test fetch search results functionality
	 */
	@Test	
	public void testApplySearchUrl()
	{
		printTitle(); 
		MockResourceRequest testRequest = new MockResourceRequest();
		MockResourceResponse testResponse = new MockResourceResponse();
		ModelMap map = new ModelMap();
		FirmSearchModelBean fsmb = new FirmSearchModelBean();
		fsmb.setFirmType(ALMConstants.INDIVIDUAL_LIST);
		fsmb.setFirmList(Arrays.asList(new String[]{"31"}));
		fsmb.setPracticeArea(Arrays.asList(new String[]{"Admiralty, Aviation and Transportation;Aerospace and Defense;Antitrust and Trade Regulation"}));
		fsmb.setContentType(Arrays.asList(new String[]{ALMConstants.ALL_Types}));
		fsmb.setTwitterType(Arrays.asList(new String[]{"Posts BY Law Firms;Posts ABOUT Law Firms"}));
		fsmb.setDateText(ALMConstants.LAST_YEAR);
		fsmb.setOrderBy(1);
		fsmb.setSortColumn(10);
		fsmb.setSortOrder("desc");
		fsmb.setGoToPage(1);
		fsmb.setSearchResultsPerPage(100);
		
		ModelAndView mav = null;
		/*
		testRequest.setParameter("selectedFirms", "31"); 
		testRequest.setParameter("selectedPracticeArea", "Admiralty, Aviation and Transportation;Aerospace and Defense;Antitrust and Trade Regulation");
		testRequest.setParameter("contentType", ALMConstants.ALL_Types);
		testRequest.setParameter("selectedTwitterPostType", "Posts BY Law Firms;Posts ABOUT Law Firms");
		testRequest.setParameter("datetext", ALMConstants.LAST_YEAR);*/
		try  
		{
//			mav = testController.applySearchUrl(fsmb, map,testRequest, testResponse);
		} 
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		assertEquals("firmsearchresults", mav.getViewName());
		
		assertNotNull(mav.getModelMap().get("searchResults"));
		assertNotNull(mav.getModelMap().get("firmSearchDTO"));
		
	}
}
