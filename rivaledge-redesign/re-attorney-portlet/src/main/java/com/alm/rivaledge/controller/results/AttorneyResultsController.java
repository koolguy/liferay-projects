package com.alm.rivaledge.controller.results;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.text.Format;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFHyperlink;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.alm.rivaledge.controller.AbstractAttorneyController;
import com.alm.rivaledge.model.AttorneySearchModelBean;
import com.alm.rivaledge.model.ClickToView;
import com.alm.rivaledge.model.FirmSearchModelBean;
import com.alm.rivaledge.model.PeopleSearchModelBean;
import com.alm.rivaledge.persistence.dao.CacheDAO;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.service.AttorneyService;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.transferobject.AttorneyResultDTO;
import com.alm.rivaledge.transferobject.AttorneySearchDTO;
import com.alm.rivaledge.transferobject.FirmSearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.ALMConstants.ATTORNEY_DISPLAY_COLUMNS;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.FastDateFormatFactoryUtil;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.servlet.ServletResponseUtil;

/**
 * Event controller responsible for all operation on Event search form and results
 * screen
 * 
 * @author FL728
 * 
 */
@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "attorneySearchModelBean", "attorneySearchDTO", "attorneySearchResults" })
public class AttorneyResultsController extends AbstractAttorneyController
{
	private static final String HEADER = "HEADER";
	private static final String GROUPED_TITLE_HEADER = "GROUPED_TITLE_HEADER";
	private static final String DATA = "DATA";
	

	protected Log _log = LogFactoryUtil.getLog(AttorneyResultsController.class.getName());
	
	@Autowired
	private AttorneyService attorneyService;
	
	@Autowired
	private FirmService 	firmService;
	
	@Autowired
	private CacheDAO		cacheDAO;

	@RenderMapping
	public String defaultDetailsView(@ModelAttribute("attorneySearchModelBean") AttorneySearchModelBean attorneySearchModelBean,
									 @ModelAttribute("attorneySearchDTO") AttorneySearchDTO attorneySearchDTO,
									 ModelMap model,  
									 RenderRequest request, 
									 RenderResponse response) throws Exception
	{
		
		ClickToView<AttorneySearchModelBean, AttorneySearchDTO> c2v = new ClickToView<AttorneySearchModelBean, AttorneySearchDTO>();
		c2v.setSearchModelBean(attorneySearchModelBean);
		c2v.setSearchDTO(attorneySearchDTO);
		
		boolean honored = honorClick2View(request, model, c2v);
	
		//RER-422
		if (!honored && !attorneySearchModelBean.isShowResult())
		{
			return "blankattorneyresult";
		}

		
		// DTO is changed as result of searchModelBean
		attorneySearchDTO = c2v.getSearchDTO();
		attorneySearchModelBean = c2v.getSearchModelBean();

		return renderResultsPage(attorneySearchModelBean, attorneySearchDTO, model, request, response);
	}
	
	

	
	@RenderMapping(params = "renderPage=clickToView")
	protected String clickToView(@RequestParam("attorneySearchModelBeanJson") String attorneySearchModelBeanJson, 
												 ModelMap model, 
												 RenderRequest request,
												 RenderResponse response) throws Exception
	{
		AttorneySearchModelBean asmb = WebUtil.getObject(attorneySearchModelBeanJson, AttorneySearchModelBean.class);
		asmb.setAddToSession(false);
		model.addAttribute("attorneySearchModelBean", asmb);
		
		
		AttorneySearchDTO attorneySearchDTO = getDTOFromModelBean(model, asmb, request);
		
		return renderResultsPage(asmb, attorneySearchDTO, model, request, response);
	}
	
	private String renderResultsPage(AttorneySearchModelBean attorneySearchModelBean, AttorneySearchDTO attorneySearchDTO, ModelMap model, RenderRequest request,
			RenderResponse response) throws CloneNotSupportedException
	{
		User 				currentUser 	= (model.get("currentUser") != null) ? ((User) model.get("currentUser")) : WebUtil.getCurrentUser(request, userService);

		boolean displayMessage = false;
		//prepare a search DTO that will hold the user selection criteria
		
		//attorneySearchDTO = getDTOFromModelBean(model, attorneySearchModelBean,request);
		
		List<AttorneyResultDTO> attorneySearchResults = new ArrayList<AttorneyResultDTO>(); //null is not allowed in session using spring
		
		//Dozer is doing a deep copy which unnecessarily copies Firm Objects in SearchDTO. FTB, We are fine with a shallow copy (using clone)
		//WebUtil.copyProperties(attorneySearchDTO, serviceAttorneySearchDTO);
		AttorneySearchDTO serviceAttorneySearchDTO = (AttorneySearchDTO) attorneySearchDTO.clone(); 
		
		int resultSize = attorneyService.getAttorneyDataCount(serviceAttorneySearchDTO, currentUser);
		_log.info("\n\n*** getAttorneyDataCount resultSize = " + resultSize);
		
		if (resultSize > 0)
		{
			// We have results that match this search criteria
			// Return the first page of results
			attorneySearchResults = attorneyService.getAttorneyData(serviceAttorneySearchDTO);
			
			if(resultSize >= 5000){
				
				displayMessage = true;
				model.addAttribute("displayMessage", displayMessage);
			}
		}
		
		int lastPage = 0;
		if (resultSize > 0)
		{
			if (resultSize > attorneySearchDTO.getResultPerPage())
			{
				lastPage = (int) Math.ceil(new Double(resultSize) / attorneySearchDTO.getResultPerPage());
			}
			else
			{
				lastPage = 1;
			}
		}
		
		// Put the total number of pages into the model
		attorneySearchModelBean.setLastPage(lastPage);
		// Put the total number of results into the model
		attorneySearchModelBean.setTotalResultCount(resultSize);
		
		// Put the current page size (this is not the page number but the number of records on this page)
		// into the model
		if (attorneySearchResults != null && !attorneySearchResults.isEmpty())
		{
			attorneySearchModelBean.setCurrentPageSize(attorneySearchResults.size());
		}
		else
		{
			attorneySearchModelBean.setCurrentPageSize(0);
		}
		
		//Calculate the result page start range and end range
		String[] resultRanges = WebUtil.getResultRanges(attorneySearchDTO.getResultPerPage(), attorneySearchModelBean.getGoToPage(), resultSize);
		String startRange = resultRanges[0];
		String endRange = resultRanges[1];
		
		//Set values in modal bean
		attorneySearchModelBean.setResultStartRange(startRange);
		attorneySearchModelBean.setResultEndRange(endRange);
		
		// Put the results into the model
		//PortletSession session = request.getPortletSession();
		//session.setAttribute("attorneySearchResults", attorneySearchResults, PortletSession.APPLICATION_SCOPE);
		model.addAttribute("attorneySearchResults", attorneySearchResults);
		
		//model.addAttribute("attorneySearchResults", attorneySearchResults);
				
		//Put the attorneySearchDTO in model, later this is used in printPublication 
		//model.addAttribute("attorneySearchDTO", attorneySearchDTO);

		return "attorneysearchresults";
	}
	/**
	 * Following method renders printExport jsp. This jsp displays attorney
	 * print preview page
	 * 
	 * @param request
	 * @param response
	 */
	
	@RenderMapping(params = "displayPrintPage=true")
	protected String displayPrintPage(@ModelAttribute("attorneySearchModelBean") AttorneySearchModelBean asmb,
			ModelMap model, RenderRequest request, RenderResponse response) throws Exception
	{
		AttorneySearchDTO attorneySearchDTO = getDTOFromModelBean(model, asmb,request);
		List<AttorneyResultDTO> attorneyPrintSearchResults = getSearchResultsForPrintAndExport(attorneySearchDTO, request);

		WebUtil.populatePrintTimings(model);
		model.addAttribute("attorneyPrintSearchResults", attorneyPrintSearchResults);
		return "printAttorney";
	}
	
	
	private List<AttorneyResultDTO> getSearchResultsForPrintAndExport(AttorneySearchDTO asDTO, PortletRequest request)
	{

		AttorneySearchDTO serviceAttorneySearchDTO = asDTO;
		try
		{
			serviceAttorneySearchDTO = (AttorneySearchDTO) asDTO.clone();
		}
		catch (CloneNotSupportedException e)
		{
			_log.error("Cannot clone AttorneySearchDTO. Hence, using the original => " + e.getMessage());
		}

		User currentUser = WebUtil.getCurrentUser(request, userService);

		serviceAttorneySearchDTO.setPageNumber(0);
		serviceAttorneySearchDTO.setResultPerPage(WebUtil.getResultSizeLimitForCharts(currentUser));

		List<AttorneyResultDTO> printResults =  attorneyService.getAttorneyData(serviceAttorneySearchDTO);

		return printResults;
	}

	
	@ResourceMapping("exportFile")
	public void exportFile(@RequestParam String fileType,
			@ModelAttribute("attorneySearchModelBean") AttorneySearchModelBean asmb,
			@ModelAttribute("attorneySearchDTO") AttorneySearchDTO asDTO,
			ModelMap model, ResourceRequest request, ResourceResponse response)
	{
		//fileType = ParamUtil.getString(request, "fileType", ALMConstants.CSV_EXT);
		
		_log.debug("INSIDE EXPORT TO EXCEL");
		
		List<AttorneyResultDTO> attorneyResultsDTOList = getSearchResultsForPrintAndExport(asDTO, request);
		
		if (attorneyResultsDTOList != null && attorneyResultsDTOList.size() > 0)
		{
			_log.debug("INSIDE EXPORT TO EXCEL attorneyResultsDTOList size is " + attorneyResultsDTOList.size());
		}
		
		
	
		//_log.debug(":::ResultController::selectedColumns::"+selectedColumns);
		
		String fileName = getFileName();

		/* Code for export publications to csv file starts */

		if (fileType != null && fileType.equalsIgnoreCase(ALMConstants.CSV_EXT))
		{
			StringBuilder userInfo = new StringBuilder();

			/* Following code set header in mvc file */
			userInfo = getUserInfo(attorneyResultsDTOList, asmb);

			String csv = userInfo.toString();

			fileName = fileName + StringPool.PERIOD + ALMConstants.CSV_EXT;
			byte[] bytes = csv.getBytes();

			HttpServletRequest servletRequest = PortalUtil.getHttpServletRequest(request);
			HttpServletResponse servletResponse = PortalUtil.getHttpServletResponse(response);

			try
			{
				ServletResponseUtil.sendFile(servletRequest, servletResponse, fileName, bytes, ContentTypes.TEXT_CSV_UTF8);
			}
			catch (IOException e)
			{
				_log.error("GOT ERROR WHILE SENDING SERVLET RESPONSE " + e.getMessage());
			}

			/* Code for export publications to csv file ends */
		}
		else
		{
			/* Code for export publications to excel file starts */
			FileInputStream fileInputStreamReader = null;
			FileOutputStream fileOutPutStreamReader = null;
			File xlsFile = null;

			try
			{
				xlsFile = FileUtil.createTempFile("xls");
				try
				{
					xlsFile.createNewFile();
				}
				catch (IOException e)
				{
					_log.error("ERROR WHILE CREATING NEW FILE " + e.getMessage());
				}
				if (xlsFile.exists())
				{
					fileInputStreamReader = new FileInputStream(xlsFile);
				}

			}
			catch (FileNotFoundException e1)
			{
				_log.info("Error while creating and reading file " + e1.getMessage());
			}

			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet(ALMConstants.WORKBOOK_NAME);

			sheet = getSheet(sheet, attorneyResultsDTOList, workbook, asmb);

			HttpServletRequest servletRequest = PortalUtil.getHttpServletRequest(request);
			HttpServletResponse servletResponse = PortalUtil.getHttpServletResponse(response);

			fileName = fileName + StringPool.PERIOD + ALMConstants.XLS_EXT;

			try
			{
				fileInputStreamReader.close();

				fileOutPutStreamReader = new FileOutputStream(xlsFile);

				workbook.write(fileOutPutStreamReader);

				fileOutPutStreamReader.close();

				byte[] bytes = read(xlsFile);

				servletResponse.setContentType(ALMConstants.CONTENT_TYPE_XLS);

				ServletResponseUtil.sendFile(servletRequest, servletResponse, fileName, bytes, ALMConstants.CONTENT_TYPE_XLS);
			}
			catch (FileNotFoundException e)
			{
				_log.error("FILE NOT FOUND EXCEPTION " + e.getMessage());
			}
			catch (IOException e)
			{
				_log.error("IO EXCEPTION XLS " + e.getMessage());
			}
			finally
			{

				try
				{
					fileInputStreamReader.close();
					fileOutPutStreamReader.close();
				}
				catch (IOException e)
				{

					_log.error("ERROR WHILE CLOSING INPUT OUTPUT STREAM READER " + e.getMessage());
				}
			}
			/* Code for export publications to excel file ends */
		}

	}
	
	private String getFileName()
	{

		String fileName = ALMConstants.ATTORNEY_FILE_NAME;

		Format dateFormatDate = FastDateFormatFactoryUtil.getSimpleDateFormat("MM.dd.yy", Locale.US);

		fileName = fileName + dateFormatDate.format(CalendarFactoryUtil.getCalendar(Locale.US).getTime());

		return fileName;
	}
	
	private StringBuilder getUserInfo(List<AttorneyResultDTO> attorneyResultsDTOList, AttorneySearchModelBean asmb)
	{
		StringBuilder userInfo = new StringBuilder();

		/* Following code set header in mvc file */
		userInfo.append(ALMConstants.TITLE_NO + StringPool.COMMA);
		userInfo.append(ALMConstants.TITLE_ATTORNEY);
		
		userInfo = setCSVFileValues(userInfo, asmb, null);
		
		/* Following code populated list values from second row */
		int keyCVS = 0;
		for (AttorneyResultDTO attorneyResultDTO : attorneyResultsDTOList)
		{
			keyCVS++;
			userInfo.append(String.valueOf(keyCVS) + StringPool.COMMA);
			
			String attorneyName = attorneyResultDTO.getAttorneyName();
			if (Validator.isNotNull(attorneyName))
			{
				attorneyName = attorneyName.replaceAll(StringPool.COMMA, StringPool.SEMICOLON);
			}
			userInfo.append(attorneyName);
			
			userInfo = setCSVFileValues(userInfo, asmb, attorneyResultDTO);
			
		}
		return userInfo;
	}

	
	/**
	 * following method writes all the final firm results set to sheet.
	 * 
	 * @param sheet
	 * @param attorneyResultsDTOList
	 * @param workbook
	 */
	private HSSFSheet getSheet(HSSFSheet sheet, List<AttorneyResultDTO> attorneyResultsDTOList, HSSFWorkbook workbook, AttorneySearchModelBean asmb)
	{

		Map<String, Object[]> data = setXLSFileData(asmb, attorneyResultsDTOList);
		int rownum = 0;
		for (String key : data.keySet())
		{
			HSSFRow row = sheet.createRow((rownum++));
			Object[] objArr = data.get(key);
			int cellnum = 0;
			for (Object obj : objArr)
			{
				HSSFCell cell = row.createCell(cellnum);
				if (obj instanceof Date)
				{
					cell.setCellValue((Date) obj);
				}
				else if (obj instanceof Boolean)
				{
					cell.setCellValue((Boolean) obj);
				}
				else if (obj instanceof String)
				{
					String str = (String) obj;
					if (str.startsWith(ALMConstants.HTTP))
					{
						HSSFHyperlink hyperLink = new HSSFHyperlink(HSSFHyperlink.LINK_URL);
						hyperLink.setAddress(str);
						cell.setHyperlink(hyperLink);
						HSSFRichTextString textString = new HSSFRichTextString(str);
						HSSFCellStyle cellStyle = workbook.createCellStyle();
						HSSFFont hssFont = workbook.createFont();
						hssFont.setUnderline(HSSFFont.U_SINGLE);
						hssFont.setColor(HSSFColor.BLUE.index);
						cellStyle.setFont(hssFont);
						cell.setCellStyle(cellStyle);
						cell.setCellValue(textString);
					}
					else
					{
						HSSFRichTextString textString = new HSSFRichTextString(str);
						if (key.startsWith(HEADER) || key.startsWith(GROUPED_TITLE_HEADER))
						{
							HSSFCellStyle cellStyle = workbook.createCellStyle();
							HSSFFont hssFont = workbook.createFont();
							hssFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
							cellStyle.setFont(hssFont);
							cell.setCellStyle(cellStyle);
						}
						cell.setCellValue(textString);
					}

				}
				else if (obj instanceof Double)
				{
					cell.setCellValue((Double) obj);
				}
				cellnum++;
			}
		}

		return sheet;
	}
	
	private byte[] read(File file)
	{

		byte[] buffer = new byte[(int) file.length()];
		InputStream ios = null;
		try
		{
			try
			{
				ios = new FileInputStream(file);
			}
			catch (FileNotFoundException e)
			{

				_log.error("FILE NOT FOUND EXCEPTION " + e.getMessage());
			}
			try
			{
				if (ios.read(buffer) == -1)
				{
				}
			}
			catch (IOException e)
			{

				_log.error("ERROR WHILE READING FILE " + e.getMessage());
			}
		}
		finally
		{
			try
			{
				if (ios != null)
					ios.close();
			}
			catch (IOException e)
			{
			}
		}

		return buffer;
	}
	
	private StringBuilder setCSVFileValues(StringBuilder userInfo,AttorneySearchModelBean asmb, AttorneyResultDTO attorneyResultDTO)
	{
		List<String> selectedColumns = asmb.getDisplayColumns();
		if (selectedColumns != null && selectedColumns.size() > 0 )
		{
			for( String column : selectedColumns )
			{
				//_log.debug(":::ResultController::column::"+column);
				if (column.equalsIgnoreCase(ATTORNEY_DISPLAY_COLUMNS.firm.name()))
				{
					if (attorneyResultDTO == null )
					{
						userInfo.append(StringPool.COMMA + ALMConstants.TITLE_FIRM_NAME);
					}
					else if ( attorneyResultDTO != null)
					{
						String attorneyFirmName = attorneyResultDTO.getFirmName();
						if ( Validator.isNotNull(attorneyFirmName))
						{
							attorneyFirmName = StringEscapeUtils.escapeCsv(attorneyFirmName);
						}
						userInfo.append(StringPool.COMMA + attorneyFirmName);
					}
					continue;
				}
				if (column.equalsIgnoreCase(ATTORNEY_DISPLAY_COLUMNS.title.name()))
				{
					if (attorneyResultDTO == null )
					{	
						userInfo.append(StringPool.COMMA + ALMConstants.TITLE_TITLE);
					}
					else if (attorneyResultDTO != null )
					{
						String attorneyTitle = attorneyResultDTO.getTitle();
						if (Validator.isNotNull(attorneyTitle))
						{
							attorneyTitle = StringEscapeUtils.escapeCsv(attorneyTitle);
						}
						userInfo.append( StringPool.COMMA + attorneyTitle );
					}
					continue;
				}
				if (column.equalsIgnoreCase(ATTORNEY_DISPLAY_COLUMNS.location.name()))
				{
					if (attorneyResultDTO == null )
					{
						userInfo.append(StringPool.COMMA + ALMConstants.ATTORNEY_LOCATION);
					}
					else if (attorneyResultDTO != null )
					{
						String attorneyLocation = attorneyResultDTO.getLocation();
						if (Validator.isNotNull(attorneyLocation))
						{
							attorneyLocation = StringEscapeUtils.escapeCsv(attorneyLocation);
						}
						userInfo.append(StringPool.COMMA + attorneyLocation);
					}
					continue;
				}
				if (column.equalsIgnoreCase(ATTORNEY_DISPLAY_COLUMNS.practice.name()))
				{
					if (attorneyResultDTO == null )
					{	
						userInfo.append(StringPool.COMMA + ALMConstants.ATTORNEY_PRACTICES);
					}
					else if (attorneyResultDTO != null )
					{
						String attorneyPractices = attorneyResultDTO.getPractices();
						if (Validator.isNotNull(attorneyPractices))
						{
							attorneyPractices = StringEscapeUtils.escapeCsv(attorneyPractices);
						}
						userInfo.append(StringPool.COMMA + attorneyPractices);
					}
					continue;
				}
				if (column.equalsIgnoreCase(ATTORNEY_DISPLAY_COLUMNS.education.name()))
				{
					if (attorneyResultDTO == null )
					{	
						userInfo.append(StringPool.COMMA + ALMConstants.ATTORNEY_EDUCATION);
					}
					else if (attorneyResultDTO != null )
					{
						String attorneyEducation = attorneyResultDTO.getEducation();
						if (Validator.isNotNull(attorneyEducation))
						{
							attorneyEducation = StringEscapeUtils.escapeCsv(attorneyEducation);
						}
						userInfo.append(StringPool.COMMA + attorneyEducation);
					}
					continue;
				}
				if (column.equalsIgnoreCase(ATTORNEY_DISPLAY_COLUMNS.admission.name()))
				{
					if (attorneyResultDTO == null )
					{	
						userInfo.append(StringPool.COMMA + ALMConstants.ATTORNEY_ADMISSION);
					}
					else if (attorneyResultDTO != null )
					{
						String attorneyAdmission = attorneyResultDTO.getAdmission();
						if (Validator.isNotNull(attorneyAdmission))
						{
							attorneyAdmission = StringEscapeUtils.escapeCsv(attorneyAdmission);
						}
						userInfo.append(StringPool.COMMA + attorneyAdmission);
					}
					continue;
				}
				if (column.equalsIgnoreCase(ATTORNEY_DISPLAY_COLUMNS.bio_text.name()))
				{
					if (attorneyResultDTO == null )
					{
						userInfo.append(StringPool.COMMA + ALMConstants.ATTORNEY_BIO_TEXT);
					}
					else if (attorneyResultDTO != null )
					{
						String attorneyKeywords = attorneyResultDTO.getKeywords();
						if (Validator.isNotNull(attorneyKeywords))
						{
							attorneyKeywords = StringEscapeUtils.escapeCsv(attorneyKeywords);
						}
						userInfo.append(StringPool.COMMA + attorneyKeywords);
					}
					continue;
				}
				if (column.equalsIgnoreCase(ATTORNEY_DISPLAY_COLUMNS.contact_info.name()))
				{
					if (attorneyResultDTO == null )
					{
						userInfo.append(StringPool.COMMA + ALMConstants.ATTORNEY_CONTACT_INFO);
					}
					else if (attorneyResultDTO != null )
					{
						String attorneyContactInfo = attorneyResultDTO.getContactInfo();
						if (Validator.isNotNull(attorneyContactInfo))
						{
							attorneyContactInfo = StringEscapeUtils.escapeCsv(attorneyContactInfo);
						}
						userInfo.append(StringPool.COMMA + attorneyContactInfo);
					}
					continue;
				}
			}
		}
		userInfo.append(StringPool.NEW_LINE);
		
		return userInfo;
	}
	
	private Map<String, Object[]> setXLSFileData(AttorneySearchModelBean asmb, List<AttorneyResultDTO> attorneyResultsDTOList)
	{

		List<String> selectedColumns = asmb.getDisplayColumns();
		
		Map<String, Object[]> data = new LinkedHashMap<String, Object[]>(attorneyResultsDTOList.size() + 20); // we need ordered keyset. And +20 is for buffer for grouped header title

		boolean header = false;

		for (int i = -1; i < attorneyResultsDTOList.size(); i++)
		{
			
			if (i == -1)
			{
				header = true;
			}
			else
			{
				header = false;
			}

			AttorneyResultDTO attorneyResultDTO = null;
			
			if(!header)
			{
				attorneyResultDTO = attorneyResultsDTOList.get(i);
			}

			
			String groupingTitle = null;
			
			if(( i == 0 || (i > 0 && attorneyResultDTO.getFirmName().equals(attorneyResultsDTOList.get(i - 1).getFirmName())))  && asmb.getOrderBy() == 5 )
			{
				groupingTitle = attorneyResultDTO.getFirmName();
			}
			else if(( i == 0 || (i > 0 && attorneyResultDTO.getTitle().equals(attorneyResultsDTOList.get(i - 1).getTitle())))  && asmb.getOrderBy() == 7 )
			{
				groupingTitle = attorneyResultDTO.getTitle();
			}
			else if(( i == 0 || (i > 0 && attorneyResultDTO.getLocation().equals(attorneyResultsDTOList.get(i - 1).getLocation())))  && asmb.getOrderBy() == 8 )
			{
				groupingTitle = attorneyResultDTO.getLocation();
			}
			else if(( i == 0 || (i > 0 && attorneyResultDTO.getPractices().equals(attorneyResultsDTOList.get(i - 1).getPractices())))  && asmb.getOrderBy() == 9 )
			{
				groupingTitle = attorneyResultDTO.getPractices();
			}
			
			if(groupingTitle != null)
			{
				Object[] record = new Object[selectedColumns.size() + 2];
				record[0] = groupingTitle;
				data.put(GROUPED_TITLE_HEADER + i, record); // + i is to maintain the uniqueness of keys
			}
			
			int columnIndex = 2;

			Object[] record = new Object[selectedColumns.size() + 2];

			if (header)
			{
				record[0] = ALMConstants.TITLE_NO;
				record[1] = ALMConstants.TITLE_ATTORNEY;
			}
			else
			{
				record[0] = String.valueOf( i + 1);
				record[1] = attorneyResultDTO.getAttorneyName();
			}

			if (selectedColumns != null && selectedColumns.size() > 0)
			{

				for (String column : selectedColumns)
				{
					if (column.equalsIgnoreCase(ATTORNEY_DISPLAY_COLUMNS.firm.name()))
					{
						if (header)
						{
							record[columnIndex] = ALMConstants.TITLE_FIRM_NAME;
						}
						else
						{
							record[columnIndex] = attorneyResultDTO.getFirmName();
						}
						columnIndex++;
						continue;
					}
					if (column.equalsIgnoreCase(ATTORNEY_DISPLAY_COLUMNS.title.name()))
					{
						if (header)
						{
							record[columnIndex] = ALMConstants.TITLE_TITLE;
						}
						else
						{
							record[columnIndex] = attorneyResultDTO.getTitle();
						}
						columnIndex++;
						continue;
					}
					if (column.equalsIgnoreCase(ATTORNEY_DISPLAY_COLUMNS.location.name()))
					{
						if (header)
						{
							record[columnIndex] = ALMConstants.ATTORNEY_LOCATION;
						}
						else
						{
							record[columnIndex] = attorneyResultDTO.getLocation();
						}
						columnIndex++;
						continue;
					}
					if (column.equalsIgnoreCase(ATTORNEY_DISPLAY_COLUMNS.practice.name()))
					{
						if (header)
						{
							record[columnIndex] = ALMConstants.ATTORNEY_PRACTICES;
						}
						else
						{
							record[columnIndex] = attorneyResultDTO.getPractices();
						}
						columnIndex++;
						continue;
					}
					if (column.equalsIgnoreCase(ATTORNEY_DISPLAY_COLUMNS.education.name()))
					{
						if (header)
						{
							record[columnIndex] = ALMConstants.ATTORNEY_EDUCATION;
						}
						else
						{
							record[columnIndex] = attorneyResultDTO.getEducation();
						}
						columnIndex++;
						continue;
					}
					if (column.equalsIgnoreCase(ATTORNEY_DISPLAY_COLUMNS.admission.name()))
					{
						if (header)
						{
							record[columnIndex] = ALMConstants.ATTORNEY_ADMISSION;
						}
						else
						{
							record[columnIndex] = attorneyResultDTO.getAdmission();
						}
						columnIndex++;
						continue;
					}
					if (column.equalsIgnoreCase(ATTORNEY_DISPLAY_COLUMNS.bio_text.name()))
					{
						if (header)
						{
							record[columnIndex] = ALMConstants.ATTORNEY_BIO_TEXT;
						}
						else
						{
							record[columnIndex] = attorneyResultDTO.getKeywords();
						}
						columnIndex++;
						continue;
					}
					if (column.equalsIgnoreCase(ATTORNEY_DISPLAY_COLUMNS.contact_info.name()))
					{
						if (header)
						{
							record[columnIndex] = ALMConstants.ATTORNEY_CONTACT_INFO;
						}
						else
						{
							record[columnIndex] = attorneyResultDTO.getContactInfo();
						}
						columnIndex++;
						continue;
					}
				}
			}

			if (header)
			{
				data.put(HEADER + i, record);  // + i is to maintain the uniqueness of keys, we are already sure there can be only one header, still a fall back approach
			}
			else
			{
				data.put(DATA + i, record);  // + i is to maintain the uniqueness of keys
			}
		}
		return data;
	}
}