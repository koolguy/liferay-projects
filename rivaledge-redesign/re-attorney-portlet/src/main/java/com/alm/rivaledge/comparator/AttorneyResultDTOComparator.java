package com.alm.rivaledge.comparator;

import java.util.Comparator;

import com.alm.rivaledge.transferobject.AttorneyResultDTO;

/**
 * This class is responsible for sorting Results based on user selection on view Setting popup
 * @author FL605
 * @since Sprint 3
 */
public class AttorneyResultDTOComparator implements Comparator<AttorneyResultDTO> {
	
	
	private String sortBy="Firm";
	

	/**
	 * 
	 * @return parameter on which search results are sorted
	 */
	public String getSortBy() 
	{
		return sortBy;
	}

	public void setSortBy(String sortBy) 
	{
		this.sortBy = sortBy;
	}


		/** 
		 * Sort Attorney search results in ascending order based on user selection in view setting popup
		 * @author FL605
		 * @since Sprint 3
		 */
		public int compare(AttorneyResultDTO ob1, AttorneyResultDTO ob2){
	    	
	    	if(sortBy.equalsIgnoreCase("Firm")){
	    		String ob1FirmName = ((AttorneyResultDTO)ob1).getFirmName();        
	  	        String ob2FirmName = ((AttorneyResultDTO)ob2).getFirmName();          
	  	        //ascending order firms
	  		    return ob1FirmName.compareTo(ob2FirmName);
	    		
	    	} else if(sortBy.equalsIgnoreCase("Title")){
	    		String ob1Title = ((AttorneyResultDTO)ob1).getTitle();        
	  	        String ob2Title = ((AttorneyResultDTO)ob2).getTitle();          
	  	        //ascending order Title
	  		    return ob1Title.compareTo(ob2Title);
	    		
	    	} else if(sortBy.equalsIgnoreCase("Location")){
	    		String ob1Location = ((AttorneyResultDTO)ob1).getLocation();        
	  	        String ob2Location = ((AttorneyResultDTO)ob2).getLocation();          
	  	        //ascending order Location
	  		    return ob1Location.compareTo(ob2Location);
	    		
	    	}  else if(sortBy.equalsIgnoreCase("Practice")){
	    		String ob1Location = ((AttorneyResultDTO)ob1).getPractices();        
	  	        String ob2Location = ((AttorneyResultDTO)ob2).getPractices();          
	  	        //ascending order Practices
	  		    return ob1Location.compareTo(ob2Location);
	    		
	    	}  	else{
	    		String ob1FirmName = ((AttorneyResultDTO)ob1).getFirmName();        
	  	        String ob2FirmName = ((AttorneyResultDTO)ob2).getFirmName();          
	  	        //ascending order firms
	  		    return ob1FirmName.compareTo(ob2FirmName);
	    	}
	      
	 
	    }
	   


}
