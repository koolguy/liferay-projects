package com.alm.rivaledge.util;

/**
 * Just for Attorney Percent Graduation chart controller
 */
public class ChartData {
	public ChartData()
	{
	}
	private int type; // this is not Highchart type, It is the type of ALM Chart like No of Attorneys, Percent Attorneys
	private int compareCount = 0;
	private String sbFirm 			= "";
	private String sbPartners 		= "";
	private String sbAssociates		= "";
	private String sbOtherCounsel 	= "";
	private String sbTotals			= "";
	private String sbPracticeCount 	= "";
	
	// Just for Attorney Percent Graduation chart controller
	private String sbTotalValue = "";
	
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getCompareCount() {
		return compareCount;
	}
	public void setCompareCount(int compareCount) {
		this.compareCount = compareCount;
	}
	public String getSbFirm() {
		return sbFirm;
	}
	public void setSbFirm(String sbFirm) {
		this.sbFirm = sbFirm;
	}
	public String getSbPartners() {
		return sbPartners;
	}
	public void setSbPartners(String sbPartners) {
		this.sbPartners = sbPartners;
	}
	public String getSbAssociates() {
		return sbAssociates;
	}
	public void setSbAssociates(String sbAssociates) {
		this.sbAssociates = sbAssociates;
	}
	public String getSbOtherCounsel() {
		return sbOtherCounsel;
	}
	public void setSbOtherCounsel(String sbOtherCounsel) {
		this.sbOtherCounsel = sbOtherCounsel;
	}
	public String getSbTotals() {
		return sbTotals;
	}
	public void setSbTotals(String sbTotals) {
		this.sbTotals = sbTotals;
	}
	public String getSbPracticeCount() {
		return sbPracticeCount;
	}
	public void setSbPracticeCount(String sbPracticeCount) {
		this.sbPracticeCount = sbPracticeCount;
	}
	public String getSbTotalValue() {
		return sbTotalValue;
	}
	public void setSbTotalValue(String sbTotalValue) {
		this.sbTotalValue = sbTotalValue;
	}
	
	public void printData(){
		System.out.println("Printing Series : "+ this.getSbFirm());
		System.out.println("sbPartners : "+ this.getSbPartners());
		System.out.println("sbAssociates : "+ this.getSbAssociates());
		System.out.println("sbOtherCounsel : "+ this.getSbOtherCounsel());
		System.out.println("sbTotals : "+ this.getSbTotals());
		System.out.println("sbTotalValue : "+ this.getSbTotalValue());
	}
	
	
}
