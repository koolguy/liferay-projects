package com.alm.rivaledge.controller.charts.percentgraduationyear;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.alm.rivaledge.controller.AbstractAttorneyController;
import com.alm.rivaledge.controller.charts.graduationyear.AttorneyGraduationChartController;
import com.alm.rivaledge.model.AttorneySearchModelBean;
import com.alm.rivaledge.model.chart.BaseChartModelBean;
import com.alm.rivaledge.model.chart.BaseChartModelBean.ComparisonDataType;
import com.alm.rivaledge.model.chart.BaseChartModelBean.FirmDataType;
import com.alm.rivaledge.model.chart.StandardChartModelBean;
import com.alm.rivaledge.persistence.dao.CacheDAO;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.service.AttorneyService;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.service.WatchlistService;
import com.alm.rivaledge.transferobject.AttorneyGraduationResultDTO;
import com.alm.rivaledge.transferobject.AttorneyResultDTO;
import com.alm.rivaledge.transferobject.AttorneySearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.ChartData;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;

@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "attorneysByPercentGradYearChartModelBean", "attorneySearchModelBean" })
public class AttorneyPercentGraduationChartController extends AbstractAttorneyController
{
	protected Log				_log	= LogFactoryUtil.getLog(AttorneyGraduationChartController.class.getName());
	@Autowired
	private AttorneyService		attorneyService;

	@Autowired
	private CacheDAO			cacheDAO;

	@Autowired
	private FirmService			firmService;
  
	@Autowired
	protected WatchlistService	watchlistService;

	@ModelAttribute
	public void populateChartModelBean(ModelMap model, PortletRequest request)
	{
		StandardChartModelBean 	attorneysByPercentGradYearChartModelBean = null;
		String					currentPortletId			 = WebUtil.getCurrentPortletId(request);
		String					currentPage				 	 = WebUtil.getCurrentPage(request);
		boolean					isHomePage				 	 = ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		User 					currentUser 			 	 = (model.get("currentUser") != null) ? ((User) model.get("currentUser")) : WebUtil.getCurrentUser(request, userService); 

		if (!isHomePage)
		{
			currentPage			= "ATTORNEY_SEARCH";
		}
		
		attorneysByPercentGradYearChartModelBean = getUserChartPreference(currentUser.getId(), currentPortletId, currentPage);
		
		if (attorneysByPercentGradYearChartModelBean == null)
		{
			attorneysByPercentGradYearChartModelBean = new StandardChartModelBean();
			attorneysByPercentGradYearChartModelBean.init(); // do an explicit init for default values
			attorneysByPercentGradYearChartModelBean.setChartType(BaseChartModelBean.ChartType.LINE.getValue());
		}

		model.addAttribute("attorneysByPercentGradYearChartModelBean", attorneysByPercentGradYearChartModelBean);
	}

	public List<Firm> sortByAverage(List<AttorneyGraduationResultDTO> results, List<Firm> firmList, int maxSize, final boolean isDescending)
	{
		// to hold firmid and average value

		Map<Integer, Integer> resultMap = new HashMap<Integer, Integer>();

		for (Firm firm : firmList)
		{
			int average = 0;
			int totalExp = 0;
			int totalAttorney = 0;
			int maxExperience = 0;

			// calculate average of each firm, put it into the result map with
			// firmId
			for (AttorneyGraduationResultDTO aDTO : results)
			{
				if (firm.getCompanyId().equals(aDTO.getFirmId()))
				{
					try
					{
						totalAttorney += aDTO.getAttorneyCount();
						if (aDTO.getExperience() != null)
						{
							int tempExp = aDTO.getExperience();
							if (maxExperience < tempExp && tempExp < 200)
							{
								maxExperience = tempExp;
							}
							totalExp += (tempExp * aDTO.getAttorneyCount());
						}
					}
					catch (NullPointerException npe)
					{
						System.out.println("Exception: Null Pointer Exception " + npe.getMessage());
						npe.printStackTrace();
					}
				}
			}
			if (totalAttorney != 0)
			{
				average = Math.round(totalExp / totalAttorney);
			}
			resultMap.put(firm.getCompanyId(), average);
		}

		List<Entry<Integer, Integer>> rawList = new LinkedList<Entry<Integer, Integer>>(resultMap.entrySet());
		Collections.sort(rawList, new Comparator<Entry<Integer, Integer>>()
		{
			public int compare(Entry<Integer, Integer> o1, Entry<Integer, Integer> o2)
			{
				if (isDescending)
				{

					return (o2.getValue().compareTo(o1.getValue()));
				}
				else
				{
					return (o1.getValue().compareTo(o2.getValue()));
				}
			}
		});

		if (rawList.size() > maxSize)
		{
			rawList = rawList.subList(0, maxSize);
		}
		List<Firm> sortedFirmList = new ArrayList<Firm>();
		for (Entry<Integer, Integer> entry : rawList)
		{
			sortedFirmList.add(firmService.getFirmById(entry.getKey()));
		}

		return sortedFirmList;
	}

	@RenderMapping
	public String defaultChartView(@ModelAttribute("attorneysByPercentGradYearChartModelBean") StandardChartModelBean attorneysByPercentGradYearChartModelBean,
			 @ModelAttribute("attorneySearchModelBean") AttorneySearchModelBean attorneySearchModelBean,
			 ModelMap model, RenderRequest request, RenderResponse response) throws Exception
	{
		AttorneySearchDTO attorneySearchDTO = getDTOFromModelBean(model, attorneySearchModelBean, request);
		
		List<Firm> firmList = new ArrayList<Firm>();

		AttorneySearchDTO serviceAttorneySearchDTO = (AttorneySearchDTO) attorneySearchDTO.clone();

		List<AttorneyResultDTO> attorneySearchResults = new ArrayList<AttorneyResultDTO>();

		// Get the current user from the render request
		User currentUser = WebUtil.getCurrentUser(request, userService);

		attorneySearchResults = attorneyService.findAttorneyDataForcharts(serviceAttorneySearchDTO, currentUser);

		List<Firm> firmListAll = new ArrayList<Firm>();

		if (attorneySearchResults != null && attorneySearchResults.size() > 0)
		{
			for (AttorneyResultDTO attorneyResultDTO : attorneySearchResults)
			{
				Integer firmID = attorneyResultDTO.getFirmId();
				if (firmID != 0)
				{
					firmListAll.add(firmService.getFirmById(firmID));
				}
			}
		}

		if ((attorneysByPercentGradYearChartModelBean != null) && (attorneysByPercentGradYearChartModelBean.getSearchResultsFirmList() != null) && (attorneysByPercentGradYearChartModelBean.getSearchResultsFirmList().size() > 0))
		{
			List<String> firmIDs = attorneysByPercentGradYearChartModelBean.getSearchResultsFirmList();
			int countFirmsTemp = 0;
			for (String firmId : firmIDs)
			{
				if (countFirmsTemp > 5)
				{
					break;
				}
				Firm firmInBean = firmService.getFirmById(Integer.parseInt(firmId));
				if (firmListAll.contains(firmInBean))
				{
					firmList.add(firmInBean);
					countFirmsTemp++;
				}
			}
			if (firmList.size() == 0)
			{
			    if(firmListAll.size()>0){
				firmList.add(firmListAll.get(0));
			    }
			}
		}
		else
		{
			if (attorneySearchResults != null && attorneySearchResults.size() > 0)
			{
				for (AttorneyResultDTO attorneyResultDTO : attorneySearchResults)
				{
					Integer firmID = attorneyResultDTO.getFirmId();
					if (firmID != 0)
					{
						firmList.add(firmService.getFirmById(firmID));
						break;
					}
				}
			}
		}

		serviceAttorneySearchDTO.setSelectedFirms(firmList);

		List<AttorneyGraduationResultDTO> attorneySearchChartResultData = attorneyService.findAttorneyDataForGradcharts(serviceAttorneySearchDTO);

		List<ChartData> chartSeries = new ArrayList<ChartData>();
		// Set the comparisonCounter
//		int comparisonCounter = 0;

		// 1. My Firm
		// Does this user have a My Firm? Every user is supposed to have one,
		// but we can't take it for granted.
		if ((attorneysByPercentGradYearChartModelBean.getComparisonDataTypeList() != null) && (!attorneysByPercentGradYearChartModelBean.getComparisonDataTypeList().isEmpty())
				&& (attorneysByPercentGradYearChartModelBean.getComparisonDataTypeList().contains(ComparisonDataType.MY_FIRM.getValue())) && (currentUser != null)
				&& (currentUser.getCompany() != null))
		{
			// We have to include comparison data for My Firm
			List<Firm> myFirmList = new ArrayList<Firm>(1);
			myFirmList.add(firmService.getFirmById(Integer.parseInt(currentUser.getCompany())));

			// Clone the searchDTO so that we don't overwrite the session
			// settings
			AttorneySearchDTO myAttorneySearchDTO = null;
			try
			{
				myAttorneySearchDTO = (AttorneySearchDTO) attorneySearchDTO.clone();
			}
			catch (CloneNotSupportedException e)
			{
				e.printStackTrace();
			}

			// Set the user's firm (my firm) as the sole firm in the firm list
			myAttorneySearchDTO.setSelectedFirms(myFirmList);

			List<AttorneyGraduationResultDTO> myFirmAGCResultData = attorneyService.findAttorneyDataForGradcharts(myAttorneySearchDTO);

			StringBuffer sbAssociates = new StringBuffer();
			StringBuffer sbCounsels = new StringBuffer();
			StringBuffer sbPartners = new StringBuffer();
			StringBuffer sbAll = new StringBuffer(); // percent
			StringBuffer sbAllValues = new StringBuffer(); // all count

//			double average = 0;
			int totalExp = 0;
			int totalAttorney = 0;
			int maxExperience = 0;
			for (AttorneyGraduationResultDTO aDTO : myFirmAGCResultData)
			{
				try
				{
					totalAttorney += aDTO.getAttorneyCount();
					if (aDTO.getExperience() != null)
					{
						int tempExp = aDTO.getExperience();
						if (maxExperience < tempExp && tempExp < 200)
						{
							maxExperience = tempExp;
						}
						totalExp += (tempExp * aDTO.getAttorneyCount());
					}
				}
				catch (NullPointerException npe)
				{
					System.out.println("Exception: Null Pointer Exception " + npe.getMessage());
					npe.printStackTrace();
				}
			}
			int[] expAssociate = new int[maxExperience + 2];
			int[] expCounsel = new int[maxExperience + 2];
			int[] expPartner = new int[maxExperience + 2];
			double[] expAll = new double[maxExperience + 2];

			for (AttorneyGraduationResultDTO aDTO : myFirmAGCResultData)
			{
				if (ALMConstants.ASSOCIATE.equalsIgnoreCase(aDTO.getTitle()))
				{
					if (aDTO.getExperience() != null && aDTO.getExperience() >= 0 && aDTO.getExperience() < 200)
					{
						expAssociate[aDTO.getExperience() + 1] = aDTO.getAttorneyCount();
					}
					else
					{
						expAssociate[0] = aDTO.getAttorneyCount();
					}
				}
			}
			for (AttorneyGraduationResultDTO aDTO : myFirmAGCResultData)
			{
				if (ALMConstants.OTHER_COUNSEL.equalsIgnoreCase(aDTO.getTitle()))
				{
					if (aDTO.getExperience() != null && aDTO.getExperience() >= 0 && aDTO.getExperience() < 200)
					{
						expCounsel[aDTO.getExperience() + 1] = aDTO.getAttorneyCount();
					}
					else
					{
						expCounsel[0] = aDTO.getAttorneyCount();
					}
				}
			}
			for (AttorneyGraduationResultDTO aDTO : myFirmAGCResultData)
			{
				if (ALMConstants.PARTNERS.equalsIgnoreCase(aDTO.getTitle()))
				{
					if (aDTO.getExperience() != null && aDTO.getExperience() >= 0 && aDTO.getExperience() < 200)
					{
						expPartner[aDTO.getExperience() + 1] = aDTO.getAttorneyCount();
					}
					else
					{
						expPartner[0] = aDTO.getAttorneyCount();
					}
				}
			}

			for (int i = 0; i <= maxExperience; i++)
			{
				if (sbAssociates.length() > 0)
				{
					sbAssociates.append(",");
				}
				sbAssociates.append(expAssociate[i]);
				if (sbCounsels.length() > 0)
				{
					sbCounsels.append(",");
				}
				sbCounsels.append(expCounsel[i]);
				if (sbPartners.length() > 0)
				{
					sbPartners.append(",");
				}
				sbPartners.append(expPartner[i]);
				expAll[i] = expAssociate[i] + expCounsel[i] + expPartner[i];
				if (sbAll.length() > 0)
				{
					sbAll.append(",");
					sbAllValues.append(",");
				}
				double temp = 0;
				if (totalExp > 0)
				{
					// temp = ( ( expAll[i] * i ) * 100 ) / totalExp;
					temp = expAll[i] * 100 / totalAttorney;
				}
				sbAll.append(temp);
				sbAllValues.append(expAll[i]);
			}

			ChartData chartData = new ChartData();
			chartData.setSbAssociates(sbAssociates.toString());
			chartData.setSbFirm("My Firm");
			chartData.setSbOtherCounsel(sbCounsels.toString());
			chartData.setSbPartners(sbPartners.toString());
			chartData.setSbTotals(sbAll.toString());
			chartData.setSbTotalValue(sbAllValues.toString());
			chartSeries.add(chartData);

			// adding results to model
			model.put("sbAssociatesMyFirm", sbAssociates.toString());
			model.put("sbCounselsMyFirm", sbCounsels.toString());
			model.put("sbPartnersMyFirm", sbPartners.toString());
			model.put("sbAllMyFirm", sbAll.toString());

			// Increment the comparison counter
//			comparisonCounter++;
		}
		// 2. Does the user have any watch lists set up?
		if ((attorneysByPercentGradYearChartModelBean.getComparisonDataTypeList() != null) && (!attorneysByPercentGradYearChartModelBean.getComparisonDataTypeList().isEmpty())
				&& (attorneysByPercentGradYearChartModelBean.getComparisonDataTypeList().contains(ComparisonDataType.WATCHLIST_AVG.getValue())) && (currentUser != null)
				&& (attorneysByPercentGradYearChartModelBean.getWatchList() != null) && (!attorneysByPercentGradYearChartModelBean.getWatchList().isEmpty()))
		{
			// implementation for watchlist
			// Clone the searchDTO so that we don't overwrite the session
			// settings
			AttorneySearchDTO watchListSearchDTO = null;
			try
			{
				watchListSearchDTO = (AttorneySearchDTO) attorneySearchDTO.clone();
			}
			catch (CloneNotSupportedException e)
			{
				e.printStackTrace();
			}
			// Get the list of firms in this list entry - could be AmLaw100,
			// NLJ250, etc.
			List<Firm> watchListFirms = new ArrayList<Firm>();
			// Now run through the list

			String watchListName = "";

			for (String watchListId : attorneysByPercentGradYearChartModelBean.getWatchList())
			{
				// Get this watchlist
				UserGroup myWatchlist = watchlistService.getWatchList(Integer.parseInt(watchListId), currentUser.getId());
				watchListName = myWatchlist.getGroupName();
				// Get the list of firms in this watch list
				List<Firm> myWatchlistFirms = watchlistService.findAllFirmsInOneWatchlist(myWatchlist.getGroupId(), Integer.parseInt(currentUser.getUserid()));
				watchListFirms.addAll(myWatchlistFirms);
			}
			// Set the firms in this ranking list as the search firms
			watchListSearchDTO.setSelectedFirms(watchListFirms);
			List<AttorneyGraduationResultDTO> watchListAGCResultData = attorneyService.findAttorneyDataForGradcharts(watchListSearchDTO);

			StringBuffer sbAssociates = new StringBuffer();
			StringBuffer sbCounsels = new StringBuffer();
			StringBuffer sbPartners = new StringBuffer();
			StringBuffer sbAll = new StringBuffer();
			StringBuffer sbAllValues = new StringBuffer(); // all count

//			double average = 0;
			int totalExp = 0;
			int totalAttorney = 0;
			int maxExperience = 0;
			for (AttorneyGraduationResultDTO aDTO : watchListAGCResultData)
			{
				try
				{
					totalAttorney += aDTO.getAttorneyCount();
					if (aDTO.getExperience() != null)
					{
						int tempExp = aDTO.getExperience();
						if (maxExperience < tempExp && tempExp < 200)
						{
							maxExperience = tempExp;
						}
						totalExp += (tempExp * aDTO.getAttorneyCount());
					}
				}
				catch (NullPointerException npe)
				{
					System.out.println("Exception: Null Pointer Exception " + npe.getMessage());
					npe.printStackTrace();
				}
			}
			int[] expAssociate = new int[maxExperience + 2];
			int[] expCounsel = new int[maxExperience + 2];
			int[] expPartner = new int[maxExperience + 2];
			double[] expAll = new double[maxExperience + 2];

			for (AttorneyGraduationResultDTO aDTO : watchListAGCResultData)
			{
				if (ALMConstants.ASSOCIATE.equalsIgnoreCase(aDTO.getTitle()))
				{
					if (aDTO.getExperience() != null && aDTO.getExperience() >= 0 && aDTO.getExperience() < 200)
					{
						expAssociate[aDTO.getExperience() + 1] = aDTO.getAttorneyCount();
					}
					else
					{
						expAssociate[0] = aDTO.getAttorneyCount();
					}
				}
			}
			for (AttorneyGraduationResultDTO aDTO : watchListAGCResultData)
			{
				if (ALMConstants.OTHER_COUNSEL.equalsIgnoreCase(aDTO.getTitle()))
				{
					if (aDTO.getExperience() != null && aDTO.getExperience() >= 0 && aDTO.getExperience() < 200)
					{
						expCounsel[aDTO.getExperience() + 1] = aDTO.getAttorneyCount();
					}
					else
					{
						expCounsel[0] = aDTO.getAttorneyCount();
					}
				}
			}
			for (AttorneyGraduationResultDTO aDTO : watchListAGCResultData)
			{
				if (ALMConstants.PARTNERS.equalsIgnoreCase(aDTO.getTitle()))
				{
					if (aDTO.getExperience() != null && aDTO.getExperience() >= 0 && aDTO.getExperience() < 200)
					{
						expPartner[aDTO.getExperience() + 1] = aDTO.getAttorneyCount();
					}
					else
					{
						expPartner[0] = aDTO.getAttorneyCount();
					}
				}
			}

			for (int i = 0; i <= maxExperience; i++)
			{
				if (sbAssociates.length() > 0)
				{
					sbAssociates.append(",");
				}
				sbAssociates.append(expAssociate[i]);
				if (sbCounsels.length() > 0)
				{
					sbCounsels.append(",");
				}
				sbCounsels.append(expCounsel[i]);
				if (sbPartners.length() > 0)
				{
					sbPartners.append(",");
				}
				sbPartners.append(expPartner[i]);
				expAll[i] = expAssociate[i] + expCounsel[i] + expPartner[i];
				if (sbAll.length() > 0)
				{
					sbAll.append(",");
					sbAllValues.append(",");
				}
				double temp = 0;
				if (totalExp > 0)
				{
					// temp = ( ( expAll[i] * i ) * 100 ) / totalExp;
					temp = expAll[i] * 100 / totalAttorney;
				}
				sbAll.append(temp);
				sbAllValues.append(expAll[i]);
			}

			// adding results to model
			model.put("sbAssociatesWatchList", sbAssociates.toString());
			model.put("sbCounselsWatchList", sbCounsels.toString());
			model.put("sbPartnersWatchList", sbPartners.toString());
			model.put("sbAllWatchList", sbAll.toString());

			ChartData chartData = new ChartData();
			chartData.setSbAssociates(sbAssociates.toString());
			chartData.setSbFirm(watchListName);
			chartData.setSbOtherCounsel(sbCounsels.toString());
			chartData.setSbPartners(sbPartners.toString());
			chartData.setSbTotals(sbAll.toString());
			chartData.setSbTotalValue(sbAllValues.toString());
			chartSeries.add(chartData);

		}

		// 3. Does the user have any RivalEdge lists set up?
		if ((attorneysByPercentGradYearChartModelBean.getComparisonDataTypeList() != null) && (!attorneysByPercentGradYearChartModelBean.getComparisonDataTypeList().isEmpty())
				&& (attorneysByPercentGradYearChartModelBean.getComparisonDataTypeList().contains(ComparisonDataType.RIVAL_EDGE.getValue())) && (attorneysByPercentGradYearChartModelBean.getFirmList() != null)
				&& (!attorneysByPercentGradYearChartModelBean.getFirmList().isEmpty()))
		{
			// User has selected a Rival Edge list for which data is needed
			for (String thisRE : attorneysByPercentGradYearChartModelBean.getFirmList())
			{

				// Get the list of firms in this list entry - could be AmLaw100,
				// NLJ250, etc.
				List<Firm> reListFirms = firmService.getRanking(thisRE);

				// Clone the searchDTO so that we don't overwrite the session
				// settings
				AttorneySearchDTO rankingSearchDTO = null;
				try
				{
					rankingSearchDTO = (AttorneySearchDTO) attorneySearchDTO.clone();
				}
				catch (CloneNotSupportedException e)
				{
					e.printStackTrace();
				}

				// Set the firms in this ranking list as the search firms
				rankingSearchDTO.setSelectedFirms(reListFirms);

				List<AttorneyGraduationResultDTO> rankingAGCResultData = attorneyService.findAttorneyDataForGradcharts(rankingSearchDTO);

				StringBuffer sbAssociates = new StringBuffer();
				StringBuffer sbCounsels = new StringBuffer();
				StringBuffer sbPartners = new StringBuffer();
				StringBuffer sbAll = new StringBuffer();
				StringBuffer sbAllValues = new StringBuffer(); // all count

//				double average = 0;
				int totalExp = 0;
				int totalAttorney = 0;
				int maxExperience = 0;
				for (AttorneyGraduationResultDTO aDTO : rankingAGCResultData)
				{
					try
					{
						totalAttorney += aDTO.getAttorneyCount();
						if (aDTO.getExperience() != null)
						{
							int tempExp = aDTO.getExperience();
							if (maxExperience < tempExp && tempExp < 200)
							{
								maxExperience = tempExp;
							}
							totalExp += (tempExp * aDTO.getAttorneyCount());
						}
					}
					catch (NullPointerException npe)
					{
						System.out.println("Exception: Null Pointer Exception " + npe.getMessage());
						npe.printStackTrace();
					}
				}

				int[] expAssociate = new int[maxExperience + 2];
				int[] expCounsel = new int[maxExperience + 2];
				int[] expPartner = new int[maxExperience + 2];
				double[] expAll = new double[maxExperience + 2];

				for (AttorneyGraduationResultDTO aDTO : rankingAGCResultData)
				{
					if (ALMConstants.ASSOCIATE.equalsIgnoreCase(aDTO.getTitle()))
					{
						if (aDTO.getExperience() != null && aDTO.getExperience() >= 0 && aDTO.getExperience() < 200)
						{
							expAssociate[aDTO.getExperience() + 1] = aDTO.getAttorneyCount();
						}
						else
						{
							expAssociate[0] = aDTO.getAttorneyCount();
						}
					}
				}
				for (AttorneyGraduationResultDTO aDTO : rankingAGCResultData)
				{
					if (ALMConstants.OTHER_COUNSEL.equalsIgnoreCase(aDTO.getTitle()))
					{
						if (aDTO.getExperience() != null && aDTO.getExperience() >= 0 && aDTO.getExperience() < 200)
						{
							expCounsel[aDTO.getExperience() + 1] = aDTO.getAttorneyCount();
						}
						else
						{
							expCounsel[0] = aDTO.getAttorneyCount();
						}
					}
				}
				for (AttorneyGraduationResultDTO aDTO : rankingAGCResultData)
				{
					if (ALMConstants.PARTNERS.equalsIgnoreCase(aDTO.getTitle()))
					{
						if (aDTO.getExperience() != null && aDTO.getExperience() >= 0 && aDTO.getExperience() < 200)
						{
							expPartner[aDTO.getExperience() + 1] = aDTO.getAttorneyCount();
						}
						else
						{
							expPartner[0] = aDTO.getAttorneyCount();
						}
					}
				}

				for (int i = 0; i <= maxExperience; i++)
				{
					if (sbAssociates.length() > 0)
					{
						sbAssociates.append(",");
					}
					sbAssociates.append(expAssociate[i]);
					if (sbCounsels.length() > 0)
					{
						sbCounsels.append(",");
					}
					sbCounsels.append(expCounsel[i]);
					if (sbPartners.length() > 0)
					{
						sbPartners.append(",");
					}
					sbPartners.append(expPartner[i]);
					expAll[i] = expAssociate[i] + expCounsel[i] + expPartner[i];
					if (sbAll.length() > 0)
					{
						sbAll.append(",");
						sbAllValues.append(",");
					}
					double temp = 0;
					if (totalExp > 0)
					{
						// temp = ( ( expAll[i] * i ) * 100 ) / totalExp;
						temp = expAll[i] * 100 / totalAttorney;
					}
					sbAll.append(temp);
					sbAllValues.append(expAll[i]);
				}

				// adding results to model
				model.put("sbAssociatesRivalEdge", sbAssociates.toString());
				model.put("sbCounselsRivalEdge", sbCounsels.toString());
				model.put("sbPartnersRivalEdge", sbPartners.toString());
				model.put("sbAllRivalEdge", sbAll.toString());

				ChartData chartData = new ChartData();
				chartData.setSbAssociates(sbAssociates.toString());
				chartData.setSbFirm(thisRE);
				chartData.setSbOtherCounsel(sbCounsels.toString());
				chartData.setSbPartners(sbPartners.toString());
				chartData.setSbTotals(sbAll.toString());
				chartData.setSbTotalValue(sbAllValues.toString());
				chartSeries.add(chartData);

			}
			// Increment the comparison counter
//			comparisonCounter++;
		}

		List<AttorneyGraduationResultDTO> averageAGCResultData = new ArrayList<AttorneyGraduationResultDTO>();

		// 4. Getting the average of search results.
		if ((attorneysByPercentGradYearChartModelBean.getComparisonDataTypeList() != null) && (!attorneysByPercentGradYearChartModelBean.getComparisonDataTypeList().isEmpty())
				&& (attorneysByPercentGradYearChartModelBean.getComparisonDataTypeList().contains(ComparisonDataType.AVERAGE.getValue())) && (attorneysByPercentGradYearChartModelBean.getFirmList() != null)
				&& (!attorneysByPercentGradYearChartModelBean.getFirmList().isEmpty()))
		{
			// Clone the searchDTO so that we don't overwrite the session
			// settings
			AttorneySearchDTO averageSearchDTO = null;
			try
			{
				averageSearchDTO = (AttorneySearchDTO) attorneySearchDTO.clone();
			}
			catch (CloneNotSupportedException e)
			{
				e.printStackTrace();
			}

			List<Firm> firmListAverage = new ArrayList<Firm>();

			if (attorneySearchResults != null && attorneySearchResults.size() > 0)
			{
				for (AttorneyResultDTO attorneyResultDTO : attorneySearchResults)
				{
					Integer firmID = attorneyResultDTO.getFirmId();
					if (firmID != 0)
					{
						firmListAverage.add(firmService.getFirmById(firmID));
					}
				}
			}

			// Set the firms in this ranking list as the search firms
			averageSearchDTO.setSelectedFirms(firmListAverage);

			averageAGCResultData = attorneyService.findAttorneyDataForGradcharts(averageSearchDTO);
			StringBuffer sbAssociates = new StringBuffer();
			StringBuffer sbCounsels = new StringBuffer();
			StringBuffer sbPartners = new StringBuffer();
			StringBuffer sbAll = new StringBuffer();
			StringBuffer sbAllValues = new StringBuffer(); // all count

//			double average = 0;
			int totalExp = 0;
			int totalAttorney = 0;
			int maxExperience = 0;
			for (AttorneyGraduationResultDTO aDTO : averageAGCResultData)
			{
				try
				{
					totalAttorney += aDTO.getAttorneyCount();
					if (aDTO.getExperience() != null)
					{
						int tempExp = aDTO.getExperience();
						if (maxExperience < tempExp && tempExp < 200)
						{
							maxExperience = tempExp;
						}
						totalExp += (tempExp * aDTO.getAttorneyCount());
					}
				}
				catch (NullPointerException npe)
				{
					System.out.println("Exception: Null Pointer Exception " + npe.getMessage());
					npe.printStackTrace();
				}
			}
			int[] expAssociate = new int[maxExperience + 2];
			int[] expCounsel = new int[maxExperience + 2];
			int[] expPartner = new int[maxExperience + 2];
			double[] expAll = new double[maxExperience + 2];

			for (AttorneyGraduationResultDTO aDTO : averageAGCResultData)
			{
				if (ALMConstants.ASSOCIATE.equalsIgnoreCase(aDTO.getTitle()))
				{
					if (aDTO.getExperience() != null && aDTO.getExperience() >= 0 && aDTO.getExperience() < 200)
					{
						expAssociate[aDTO.getExperience() + 1] = aDTO.getAttorneyCount();
					}
					else
					{
						expAssociate[0] = aDTO.getAttorneyCount();
					}
				}
			}
			for (AttorneyGraduationResultDTO aDTO : averageAGCResultData)
			{
				if (ALMConstants.OTHER_COUNSEL.equalsIgnoreCase(aDTO.getTitle()))
				{
					if (aDTO.getExperience() != null && aDTO.getExperience() >= 0 && aDTO.getExperience() < 200)
					{
						expCounsel[aDTO.getExperience() + 1] = aDTO.getAttorneyCount();
					}
					else
					{
						expCounsel[0] = aDTO.getAttorneyCount();
					}
				}
			}
			for (AttorneyGraduationResultDTO aDTO : averageAGCResultData)
			{
				if (ALMConstants.PARTNERS.equalsIgnoreCase(aDTO.getTitle()))
				{
					if (aDTO.getExperience() != null && aDTO.getExperience() >= 0 && aDTO.getExperience() < 200)
					{
						expPartner[aDTO.getExperience() + 1] = aDTO.getAttorneyCount();
					}
					else
					{
						expPartner[0] = aDTO.getAttorneyCount();
					}
				}
			}

			for (int i = 0; i <= maxExperience; i++)
			{
				if (sbAssociates.length() > 0)
				{
					sbAssociates.append(",");
				}
				sbAssociates.append(expAssociate[i]);
				if (sbCounsels.length() > 0)
				{
					sbCounsels.append(",");
				}
				sbCounsels.append(expCounsel[i]);
				if (sbPartners.length() > 0)
				{
					sbPartners.append(",");
				}
				sbPartners.append(expPartner[i]);
				expAll[i] = expAssociate[i] + expCounsel[i] + expPartner[i];
				if (sbAll.length() > 0)
				{
					sbAll.append(",");
					sbAllValues.append(",");
				}
				double temp = 0;
				if (totalExp > 0)
				{
					// temp = ( ( expAll[i] * i ) * 100 ) / totalExp;
					temp = expAll[i] * 100 / totalAttorney;
				}
				sbAll.append(temp);
				sbAllValues.append(expAll[i]);
			}

			// adding results to model
			model.put("sbAssociatesAverage", sbAssociates.toString());
			model.put("sbCounselsAverage", sbCounsels.toString());
			model.put("sbPartnersAverage", sbPartners.toString());
			model.put("sbAllAverage", sbAll.toString());

			ChartData chartData = new ChartData();
			chartData.setSbAssociates(sbAssociates.toString());
			chartData.setSbFirm("Average");
			chartData.setSbOtherCounsel(sbCounsels.toString());
			chartData.setSbPartners(sbPartners.toString());
			chartData.setSbTotals(sbAll.toString());
			chartData.setSbTotalValue(sbAllValues.toString());
			chartSeries.add(chartData);

			// Increment the comparison counter
//			comparisonCounter++;
		}
		// Selecting only top five
		// we are sending averageAGCResultData because we need to sort all the
		// firms according to
		// average graduation year and then top or bottom 5 should be used.
		if (FirmDataType.TOP_5.getValue().equals(attorneysByPercentGradYearChartModelBean.getLimitType()))
		{
			// Clone the searchDTO so that we don't overwrite the session
			// settings
			AttorneySearchDTO topSearchDTOAll = null;
			try
			{
				topSearchDTOAll = (AttorneySearchDTO) attorneySearchDTO.clone();
			}
			catch (CloneNotSupportedException e)
			{
				e.printStackTrace();
			}

			// Set the firms in this ranking list as the search firms
			topSearchDTOAll.setSelectedFirms(firmListAll);
			List<AttorneyGraduationResultDTO> topAGCResultData = new ArrayList<AttorneyGraduationResultDTO>();
			topAGCResultData = attorneyService.findAttorneyDataForGradcharts(topSearchDTOAll);

			List<Firm> firmListTop = sortByAverage(topAGCResultData, firmListAll, 5, true);
			// Clone the searchDTO so that we don't overwrite the session
			// settings
			AttorneySearchDTO topSearchDTO = null;
			try
			{
				topSearchDTO = (AttorneySearchDTO) attorneySearchDTO.clone();
			}
			catch (CloneNotSupportedException e)
			{
				e.printStackTrace();
			}

			topSearchDTO.setSelectedFirms(firmListTop);
			attorneySearchChartResultData = attorneyService.findAttorneyDataForGradcharts(topSearchDTO);
			// comparisonResult = sortByComparator(peopleSearchResults, 10,
			// true);
			// now the firm list should be changed as well
			firmList = firmListTop;
		}
		else if (FirmDataType.BOTTOM_5.getValue().equals(attorneysByPercentGradYearChartModelBean.getLimitType()))
		{
			// Clone the searchDTO so that we don't overwrite the session
			// settings
			AttorneySearchDTO bottomSearchDTOAll = null;
			try
			{
				bottomSearchDTOAll = (AttorneySearchDTO) attorneySearchDTO.clone();
			}
			catch (CloneNotSupportedException e)
			{
				e.printStackTrace();
			}

			// Set the firms in this ranking list as the search firms
			bottomSearchDTOAll.setSelectedFirms(firmListAll);
			List<AttorneyGraduationResultDTO> bottomAGCResultData = new ArrayList<AttorneyGraduationResultDTO>();
			bottomAGCResultData = attorneyService.findAttorneyDataForGradcharts(bottomSearchDTOAll);

			List<Firm> firmListBottom = sortByAverage(bottomAGCResultData, firmListAll, 5, false);
			// Clone the searchDTO so that we don't overwrite the session
			// settings
			AttorneySearchDTO bottomSearchDTO = null;
			try
			{
				bottomSearchDTO = (AttorneySearchDTO) attorneySearchDTO.clone();
			}
			catch (CloneNotSupportedException e)
			{
				e.printStackTrace();
			}

			bottomSearchDTO.setSelectedFirms(firmListBottom);
			attorneySearchChartResultData = attorneyService.findAttorneyDataForGradcharts(bottomSearchDTO);
			// comparisonResult = sortByComparator(peopleSearchResults, 10,
			// true);
			// now the firm list should be changed as well
			firmList = firmListBottom;
		}

		ChartData[] charts = new ChartData[firmList.size()];
		int counterCharts = 0;
		for (Firm firm : firmList)
		{

			// original search results
			StringBuffer sbAssociates = new StringBuffer();
			StringBuffer sbCounsels = new StringBuffer();
			StringBuffer sbPartners = new StringBuffer();
			StringBuffer sbAll = new StringBuffer();
			StringBuffer sbAllValues = new StringBuffer(); // all count

//			double average = 0;
			int totalExp = 0;
			int totalAttorney = 0;
			int maxExperience = 0;
			for (AttorneyGraduationResultDTO aDTO : attorneySearchChartResultData)
			{
				if (aDTO.getFirmId().equals(firm.getCompanyId()))
				{
					try
					{
						totalAttorney += aDTO.getAttorneyCount();
						if (aDTO.getExperience() != null)
						{
							int tempExp = aDTO.getExperience();
							if (maxExperience < tempExp && tempExp < 200)
							{
								maxExperience = tempExp;
							}
							totalExp += (tempExp * aDTO.getAttorneyCount());
						}
					}
					catch (NullPointerException npe)
					{
						System.out.println("Exception: Null Pointer Exception " + npe.getMessage());
						npe.printStackTrace();
					}
				}
			}
//			if (totalAttorney > 0)
//			{
//				average = totalExp / totalAttorney;
//			}
			int[] expAssociate = new int[maxExperience + 2];
			int[] expCounsel = new int[maxExperience + 2];
			int[] expPartner = new int[maxExperience + 2];
			double[] expAll = new double[maxExperience + 2];

			for (AttorneyGraduationResultDTO aDTO : attorneySearchChartResultData)
			{
				if (aDTO.getFirmId().equals(firm.getCompanyId()))
				{
					if (ALMConstants.ASSOCIATE.equalsIgnoreCase(aDTO.getTitle()))
					{
						if (aDTO.getExperience() != null && aDTO.getExperience() >= 0 && aDTO.getExperience() < 200)
						{
							expAssociate[aDTO.getExperience() + 1] = aDTO.getAttorneyCount();
							expAll[aDTO.getExperience() + 1] += aDTO.getAttorneyCount();
						}
						else
						{
							expAssociate[0] = aDTO.getAttorneyCount();
							expAll[0] += aDTO.getAttorneyCount();
						}
					}
				}
			}
			for (AttorneyGraduationResultDTO aDTO : attorneySearchChartResultData)
			{
				if (aDTO.getFirmId().equals(firm.getCompanyId()))
				{
					if (ALMConstants.OTHER_COUNSEL.equalsIgnoreCase(aDTO.getTitle()))
					{
						if (aDTO.getExperience() != null && aDTO.getExperience() >= 0 && aDTO.getExperience() < 200)
						{
							expCounsel[aDTO.getExperience() + 1] = aDTO.getAttorneyCount();
							expAll[aDTO.getExperience() + 1] += aDTO.getAttorneyCount();
						}
						else
						{
							expCounsel[0] = aDTO.getAttorneyCount();
							expAll[0] += aDTO.getAttorneyCount();
						}
					}
				}
			}
			for (AttorneyGraduationResultDTO aDTO : attorneySearchChartResultData)
			{
				if (aDTO.getFirmId().equals(firm.getCompanyId()))
				{
					if (ALMConstants.PARTNERS.equalsIgnoreCase(aDTO.getTitle()))
					{
						if (aDTO.getExperience() != null && aDTO.getExperience() >= 0 && aDTO.getExperience() < 200)
						{
							expPartner[aDTO.getExperience() + 1] = aDTO.getAttorneyCount();
							expAll[aDTO.getExperience() + 1] += aDTO.getAttorneyCount();
						}
						else
						{
							expPartner[0] = aDTO.getAttorneyCount();
							expAll[0] += aDTO.getAttorneyCount();
						}
					}
				}
			}
			for (int i = 0; i <= maxExperience; i++)
			{
				if (sbAssociates.length() > 0)
				{
					sbAssociates.append(",");
				}
				sbAssociates.append(expAssociate[i]);
				if (sbCounsels.length() > 0)
				{
					sbCounsels.append(",");
				}
				sbCounsels.append(expCounsel[i]);
				if (sbPartners.length() > 0)
				{
					sbPartners.append(",");
				}
				sbPartners.append(expPartner[i]);
//				int tempExp = expAssociate[i] + expCounsel[i] + expPartner[i];
				// expAll[i] = expAssociate[i] + expCounsel[i] + expPartner[i];
				if (sbAll.length() > 0)
				{
					sbAll.append(",");
					sbAllValues.append(",");
				}
				double temp = 0;
				if (totalExp > 0)
				{
					// temp = ( ( expAll[i] * i ) * 100 ) / totalExp;
					temp = expAll[i] * 100 / totalAttorney;
				}
				sbAll.append(temp);
				sbAllValues.append(expAll[i]);
			}

			charts[counterCharts] = new ChartData();
			charts[counterCharts].setSbAssociates(sbAssociates.toString());
			charts[counterCharts].setSbFirm(firm.getCompany());
			charts[counterCharts].setSbOtherCounsel(sbCounsels.toString());
			charts[counterCharts].setSbPartners(sbPartners.toString());
			charts[counterCharts].setSbTotals(sbAll.toString());
			charts[counterCharts].setSbTotalValue(sbAllValues.toString());
			chartSeries.add(charts[counterCharts]);

			counterCharts++;
		}

		// Put the results into the model
		// Set the chart type

		// trying to print all series
		for (ChartData c : chartSeries)
		{
			c.printData();
		}

		model.put("allRankingList", ALMConstants.RIVALEDGE_RANKING_LIST);
		model.put("chartType", attorneysByPercentGradYearChartModelBean.getChartType());
		model.put("attorneyGraduationChartTitle", "% OF ATTORNEYS BY YEAR");
		model.put("attorneyGraduationChartYAxisTitle", "% of Attorneys");
		model.put("attorneyGraduationChartXAxisTitle", "Years Since Law School Graduation");
		model.addAttribute("attorneySearchResults", attorneySearchResults);
		model.addAttribute("attorneySearchChartResultData", attorneySearchChartResultData);
		// model.put("selectedFirmName", selectedFirmName);
		model.addAttribute("seriesData", chartSeries);

		// Added extra attribute to check whether we have data to display for
		// chart
		// if attorneySearchResults is not having any records, user would see
		// message Available stating "0 Results, Please try a different search"
		if (attorneySearchResults == null || attorneySearchResults.size() == 0)
		{
			model.addAttribute("searchResultFlag", ALMConstants.NO_RESULT_MESSAGE);
		}

		return "attorneysbypercentgradyear";

	}

	@ActionMapping(params = "action=updateChart")
	public void updateChart(@ModelAttribute("attorneysByPercentGradYearChartModelBean") StandardChartModelBean attorneysByPercentGradYearChartModelBean, 
							ActionRequest request,
							ActionResponse response)
	{
		// Persist the changes to the chart settings
		// If this is the home page, save them there, else
		// save with the generic NEWS_PUBS tag.
		String 	currentPortletId 	= WebUtil.getCurrentPortletId(request);
		String 	currentPage			= WebUtil.getCurrentPage(request);
		boolean	isHomePage			= ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		User	currentUser 		= WebUtil.getCurrentUser(request, userService);
		if (!isHomePage)
		{
			currentPage = "ATTORNEY_SEARCH";
		}
		userService.saveUserPreferences(currentUser.getId(), 
										currentPortletId, 
										null, 
										WebUtil.getJson(attorneysByPercentGradYearChartModelBean), 
										currentPage);
	}

	/**
	 * Following method remove rendering portlet from the page. And redirects to
	 * same page.
	 * 
	 */
	@ActionMapping(params = "action=removePortleFromThePage")
	public void removePortletFromPage(ActionRequest request, ActionResponse response) throws Exception
	{
		WebUtil.removePortlet(request);
	}
}
