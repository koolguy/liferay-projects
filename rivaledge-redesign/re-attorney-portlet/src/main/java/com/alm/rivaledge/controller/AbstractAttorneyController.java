package com.alm.rivaledge.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.alm.rivaledge.model.AttorneySearchModelBean;
import com.alm.rivaledge.model.ClickToView;
import com.alm.rivaledge.model.PeopleSearchModelBean;
import com.alm.rivaledge.model.chart.StandardChartModelBean;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserPreferences;
import com.alm.rivaledge.service.AttorneyService;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.service.UserService;
import com.alm.rivaledge.service.WatchlistService;
import com.alm.rivaledge.transferobject.AttorneyResultDTO;
import com.alm.rivaledge.transferobject.AttorneySearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;

/**
 * Attorney controller responsible for all operation on Attorney search form and
 * results screen
 * 
 * @author FL605
 * @since Sprint 3
 * 
 */
public class AbstractAttorneyController
{
	@Autowired
	private FirmService		firmService;

	@Autowired
	private AttorneyService	attorneyService;

	@Autowired
	protected UserService userService;
	
	@Autowired
	protected WatchlistService watchlistService;

	/**
	 * Populates the Model which holds the Search criteria in search form initializing to default values
	 * 
	 * @param model
	 */
	@ModelAttribute
	public void populateAttorneySearchModelBean(ModelMap model, PortletRequest request)
	{
		
		String				currentPortletId		 = WebUtil.getCurrentPortletId(request);
		String				currentPage				 = WebUtil.getCurrentPage(request);
		boolean				isHomePage				 = ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		boolean 			isC2VPage				 = 	WebUtil.isClickToViewPage(request);
		model.addAttribute("isC2VPage", isC2VPage);
		
		
		User 				currentUser 			 = (model.get("currentUser") != null) ? ((User) model.get("currentUser")) : WebUtil.getCurrentUser(request, userService); 
		boolean				isAdminUser				 = WebUtil.isAdminUser(currentUser);									

		AttorneySearchModelBean attorneySearchModelBean		 =  (AttorneySearchModelBean) model.get("attorneySearchModelBean");
		
		if (currentUser != null)
		{
			model.put("currentUser", currentUser);
			model.put("isAdminUser", isAdminUser);
		}
		
		if (isHomePage)
		{
			attorneySearchModelBean = getUserPreference(currentUser.getId(), currentPortletId, currentPage);
		}
		
		if (!isHomePage && model.get("attorneySearchModelBean") == null)
		{
				currentPortletId 	= "ATTORNEY_SEARCH";
				currentPage			= "ATTORNEY_SEARCH";
				attorneySearchModelBean = getUserPreference(currentUser.getId(), currentPortletId, currentPage);
		}
		
		if (attorneySearchModelBean == null)
		{
			attorneySearchModelBean = 	getDefaultAttorneySearchModelBean(currentUser);
		}
		
		attorneySearchModelBean.setPortletId(currentPortletId);
		
		if(isHomePage || isC2VPage)
		{
			attorneySearchModelBean.setAddToSession(false);
		}
		
		model.addAttribute("attorneySearchModelBean", attorneySearchModelBean);

		AttorneySearchDTO attorneySearchDTO = getDTOFromModelBean(model, attorneySearchModelBean,request);
		model.addAttribute("attorneySearchDTO",  attorneySearchDTO);
		
		if (model.get("attorneySearchResults") == null)
		{
			model.addAttribute("attorneySearchResults", new ArrayList<AttorneyResultDTO>());
		}
		
	}
	
	/**
	 * Returns the new searchModelBean with default search criteria and default watchlist for the user
	 * @param currentUser
	 * @return
	 */
	protected AttorneySearchModelBean getDefaultAttorneySearchModelBean(User currentUser)
	{
		List<UserGroup> 		allWatchListsDefaultList = null;
		AttorneySearchModelBean attorneySearchModelBean = new AttorneySearchModelBean();
		attorneySearchModelBean.init(); //do an explicit init for default values
		
		if (currentUser != null)
		{
			allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
		}
		
		List<String> firmList = new ArrayList<String>();
		List<Integer> firmListWatchList = new ArrayList<Integer>();
		
		if (allWatchListsDefaultList == null || allWatchListsDefaultList.isEmpty())
		{
			//FirmType
			attorneySearchModelBean.setFirmType(ALMConstants.RIVALEDGE_LIST);
			firmList.add(ALMConstants.AMLAW_100);
		} 
		else
		{
			attorneySearchModelBean.setFirmType(ALMConstants.WATCH_LIST);
			firmListWatchList.add(allWatchListsDefaultList.get(0).getGroupId());
		}
		attorneySearchModelBean.setFirmList(firmList);
		attorneySearchModelBean.setFirmListWatchList(firmListWatchList);
		
		return attorneySearchModelBean;
		
	}

	/**
	 * Based on the User Action, where the request is coming from, This will alter the searchBean, the searchDTO if required 
	 * and put them back to C2V.
	 * @param request
	 * @param model
	 * @param c2v
	 * @return true if honored, false otherwise
	 * @throws UnsupportedEncodingException 
	 */
	protected boolean honorClick2View(RenderRequest request, ModelMap model,
			ClickToView<AttorneySearchModelBean, AttorneySearchDTO> c2v) throws UnsupportedEncodingException {
		if(!WebUtil.isClickToViewPage(request))
		{
			return false;  // its not C2V, then what the hell will I do here I'm returning, bye!!
		}
		
	
		
		HttpServletRequest originalRequest = WebUtil.getHttpServletRequest(request);
		String 				firmName 		= originalRequest.getParameter("drilldownFirmName"); 
		
		AttorneySearchModelBean attorneySearchModelBean = null;
		AttorneySearchDTO attorneySearchDTO = null;
		
		if (StringUtils.isNotBlank(firmName))
			{
				attorneySearchModelBean =  WebUtil.cloneBean(c2v.getSearchModelBean(),  AttorneySearchModelBean.class); // clone the bean, because we don't want to alter the original search criteria
				
				String drilldownFirm = URLDecoder.decode(firmName, "UTF-8");
				List<Firm> matchingFirms = firmService.getFirmByName(drilldownFirm);
				if ((matchingFirms != null) && (!matchingFirms.isEmpty()))
				{
					Firm firm = matchingFirms.get(0);
	
					attorneySearchModelBean.setFirmType(ALMConstants.INDIVIDUAL_FIRMS); // set it to Individual as we know there is single firm
	
					List<String> selectedFirmsList = new ArrayList<String>(1);
					selectedFirmsList.add("" + firm.getCompanyId()); // add Firm Id as String
	
					attorneySearchModelBean.setFirmList(selectedFirmsList); // actual Ids to get DTO
					attorneySearchModelBean.setSelectedFirms(firm.getCompany()); // this will go into SearchCriteria String
					
					attorneySearchDTO = getDTOFromModelBean(model, attorneySearchModelBean, request);
				}
			
			}
			else
			{
				firmName = originalRequest.getParameter("firmStatsDrilldown");
				if (StringUtils.isNotBlank(firmName))
				{
					String 		drilldownFirm 	= URLDecoder.decode(firmName, "UTF-8");
					List<Firm> 	matchingFirms 	= firmService.getFirmByName(drilldownFirm);
					if ((matchingFirms != null) && (!matchingFirms.isEmpty()))
					{
						String					searchCriteria 	= originalRequest.getParameter("drilldownSearch");
						String					searchJSON		= URLDecoder.decode(searchCriteria, "UTF-8");
						PeopleSearchModelBean 	searchBean		= WebUtil.getObject(searchJSON, PeopleSearchModelBean.class);

						// Create a new search DTO
						
						
						// Create and initialize a new searchBean
						attorneySearchModelBean = new AttorneySearchModelBean();
						attorneySearchModelBean.init();
						
						attorneySearchDTO = new AttorneySearchDTO();
						// Set the selected firms
						attorneySearchDTO.setSelectedFirms(matchingFirms);
						attorneySearchModelBean.setSelectedFirms(drilldownFirm);
						
						// Set the location(s)
						if ((searchBean.getLocations() != null) && (!searchBean.getLocations().contains(ALMConstants.ALL_LOCATIONS)))
						{
							attorneySearchDTO.setLocations(searchBean.getLocations());
							attorneySearchModelBean.setSelectedLocation(searchBean.getSelectedLocations());
						}
						
						// Set the practice(s)
						if ((searchBean.getPracticeArea() != null) && (!searchBean.getPracticeArea().contains(ALMConstants.ALL_PRACTICE_AREAS)))
						{
							attorneySearchDTO.setPracticeArea(searchBean.getPracticeArea());
							attorneySearchModelBean.setSelectedPracticeArea(searchBean.getSelectedPracticeArea());
						}
						
						// Set the title
						String selectedTitle = originalRequest.getParameter("title");
						if ((selectedTitle != null) && (selectedTitle.trim().length() > 0))
						{
							List<String> selectedTitles = new ArrayList<String>(1);
							selectedTitles.add(selectedTitle);
							attorneySearchDTO.setTitles(selectedTitles);
							attorneySearchModelBean.setSelectedTitles(selectedTitle);
						}
						
						
					}				
				}
			}
		
		
		attorneySearchModelBean.setGoToPage(1);
		attorneySearchModelBean.setAddToSession(false); // don't add this to session. But, only to model
		
		
		model.addAttribute("attorneySearchModelBean", attorneySearchModelBean);
		model.addAttribute("attorneySearchDTO", attorneySearchDTO);

		c2v.setSearchModelBean(attorneySearchModelBean); // set the changed bean back to C2V
		c2v.setSearchDTO(attorneySearchDTO); // set the changed DTO back to C2V
		
		
		
		return true;
		
	}
	
	protected AttorneySearchModelBean getUserPreference(Integer userId, String portletId, String currentPage)
	{
		UserPreferences uP = userService.getUserPreferenceByUserIdAndPortletId(userId, portletId, currentPage);
		if ((uP != null) && (uP.getSearchCriteriaJSON() != null) && (uP.getSearchCriteriaJSON().trim().length() > 0))
		{
			return (WebUtil.getObject(uP.getSearchCriteriaJSON(), AttorneySearchModelBean.class));
		}
		return (null);
	}
	
	protected StandardChartModelBean getUserChartPreference(Integer userId, String portletId, String currentPage)
	{
		UserPreferences uP = userService.getUserPreferenceByUserIdAndPortletId(userId, portletId, currentPage);
		if ((uP != null) && (uP.getChartCriteriaJSON() != null) && (uP.getChartCriteriaJSON().trim().length() > 0))
		{
			return (WebUtil.getObject(uP.getChartCriteriaJSON(), StandardChartModelBean.class));
		}
		return (null);
	}

	
	/**
	 * Creates a AttorneySearchDTO from  AttorneySearchModelBean
	 * @param model
	 * @param attorneySearchModelBean
	 * @return
	 */
	protected AttorneySearchDTO getDTOFromModelBean(ModelMap model, AttorneySearchModelBean attorneySearchModelBean,PortletRequest request) {
		
		AttorneySearchDTO attorneySearchDTO = new AttorneySearchDTO();
		
		// Get the page number from the model bean
		int pageNumber = attorneySearchModelBean.getGoToPage();
		int orderBy = attorneySearchModelBean.getOrderBy();
		int searchResultsPerPage = attorneySearchModelBean.getSearchResultsPerPage();
		attorneySearchDTO.setOrderBy(orderBy);
		attorneySearchDTO.setResultPerPage(searchResultsPerPage);
		
		// Plug the page number into the search DTO
		//This sets the starting limit for the paginated search results in the sql query
		attorneySearchDTO.setPageNumber(pageNumber < 0 ? 0 : (pageNumber - 1) * searchResultsPerPage);
		
		// Set the sort column and order into the search DTO
		attorneySearchDTO.setSortColumn(attorneySearchModelBean.getSortColumn());
		attorneySearchDTO.setSortOrder(attorneySearchModelBean.getSortOrder());
		
		// Get the selected firms, if any, and set them into the search DTO
		setSelectedFirms(model, attorneySearchDTO, attorneySearchModelBean,request);
		
		// Set the remaining search criteria, such as practice areas, etc.
		// See the setOtherSearchParameter method for more details
		setOtherSearchParameter(model, attorneySearchDTO, attorneySearchModelBean);
		return attorneySearchDTO;

	}

	/**
	 * Set Practice Area, contentType, keywords and selectedTwitterPostType in
	 * AttorneySearchDTO reference based on user input on Firm search Screen
	 * 
	 * @param request
	 * @param response
	 * @param attorneySearchDTO
	 */
	protected void setOtherSearchParameter(ModelMap model, AttorneySearchDTO attorneySearchDTO, AttorneySearchModelBean attorneySearchModelBean)
	{
		if (attorneySearchModelBean.getPracticeArea() != null
				&& !attorneySearchModelBean.getPracticeArea().contains(ALMConstants.ALL_PRACTICE_AREAS))
		{
			attorneySearchDTO.setPracticeArea(attorneySearchModelBean.getPracticeArea());
		}
		
		if (attorneySearchModelBean.getSelectedLocation() !=null && !attorneySearchModelBean.getSelectedLocation().contains(ALMConstants.ALL_LOCATIONS))
		{
			attorneySearchDTO.setLocations(ListUtil.fromArray(StringUtil.split(attorneySearchModelBean.getSelectedLocation(), ",")));
		}
		if (attorneySearchModelBean.getTitle() !=null && (!attorneySearchModelBean.getTitle().isEmpty()) && !attorneySearchModelBean.getTitle().get(0).equals(ALMConstants.ALL))
		{
			attorneySearchDTO.setTitles(attorneySearchModelBean.getTitle());
		}
		attorneySearchModelBean.setSelectedPracticeArea(attorneySearchModelBean.getPracticeArea().toString().replace("[", "").replace("]", ""));
		attorneySearchModelBean.setSelectedTitles(attorneySearchModelBean.getTitle().toString().replace("[", "").replace("]", ""));
		
		String fromDateText = attorneySearchModelBean.getFromDate();
		String toDateText = attorneySearchModelBean.getToDate();
		if (ALMConstants.ANY.equals(fromDateText) && ALMConstants.ANY.equals(toDateText)) 
		{
			attorneySearchModelBean.setFromDate(null);
			attorneySearchModelBean.setToDate(null);
		}
		else
		{
			attorneySearchModelBean.setFromDate(attorneySearchModelBean.getFromDate());
			attorneySearchModelBean.setToDate(attorneySearchModelBean.getToDate());
		}
		if(attorneySearchModelBean.getName()!=null && !StringUtils.isEmpty(attorneySearchModelBean.getName()))
		{			
			attorneySearchDTO.setAttorneyName(ListUtil.fromArray(StringUtil.split(attorneySearchModelBean.getName(), " ")));
		}
		//attorneySearchDTO.setAttorneyName(StringUtils.defaultIfBlank(attorneySearchModelBean.getName(), null));
		attorneySearchDTO.setKeywordsInBioText(StringUtils.defaultIfBlank(attorneySearchModelBean.getKeywords(), null));
		attorneySearchDTO.setLawSchooltext(StringUtils.defaultIfBlank(attorneySearchModelBean.getLawSchools(), null));
		attorneySearchDTO.setAdmissions(StringUtils.defaultIfBlank(attorneySearchModelBean.getAdmissions(), null));
		attorneySearchDTO.setGraduationStartYear(StringUtils.defaultIfBlank(attorneySearchModelBean.getFromDate(), null));
		attorneySearchDTO.setGraduationEndYear(StringUtils.defaultIfBlank(attorneySearchModelBean.getToDate(), null));
		
	}

	
	/**
	 * Set Firms information in AttorneySearchDTO reference based on user input
	 * on Attorney search Screen
	 * 
	 * @param model
	 * @param attorneySearchDTO
	 */
	protected void setSelectedFirms(ModelMap model, AttorneySearchDTO attorneySearchDTO, AttorneySearchModelBean attorneySearchModelBean,PortletRequest request)
	{
		String firmType = attorneySearchModelBean.getFirmType();
		List<Firm> firmsList = new ArrayList<Firm>();
		List<Firm> firmListWatchList = new ArrayList<Firm>();
		List<String> firmsListForResults = new ArrayList<String>();
		
		if (ALMConstants.ALL_FIRMS.equals(firmType))
		{
			firmsListForResults.add(firmType);
			attorneySearchModelBean.setFirmListWatchList(null);
			
		}
		else if (ALMConstants.RIVALEDGE_LIST.equals(firmType))
		{
			firmsList = new ArrayList<Firm>();
			firmsList.addAll(firmService.getRanking(attorneySearchModelBean.getFirmList().get(0)));
			firmsListForResults.add(attorneySearchModelBean.getFirmList().get(0));
			attorneySearchModelBean.setFirmListWatchList(null);
			
			attorneySearchDTO.setSelectedFirms(firmsList);
		}
		else if (ALMConstants.WATCH_LIST.equals(firmType))
		{
			User currentUser = (model.get("currentUser") != null) ? ((User) model.get("currentUser")) : WebUtil.getCurrentUser(request, userService); 
			List<Firm> firmsWatchList = null;
			if (currentUser != null)
			{
				firmsWatchList = watchlistService.findAllFirmsInManyWatchlists(attorneySearchModelBean.getFirmListWatchList(), currentUser.getId());
			}
			
			firmListWatchList.addAll(firmsWatchList);
			firmsList.addAll(firmsWatchList);

			for (Firm firmValue : firmsWatchList)
			{
				if (firmValue != null)
				{
					firmsListForResults.add(firmValue.getCompany());
				}
			}
			
			if (firmsWatchList.size() == 0)
			{
				List<Firm> notAvailableFirms = new ArrayList<Firm>();
				Firm tempFirm = new Firm();
				tempFirm.setCompanyId(0);
				notAvailableFirms.add(tempFirm);
				attorneySearchDTO.setSelectedFirms(notAvailableFirms);
			}
			else
			{
				attorneySearchDTO.setSelectedFirms(firmsWatchList);
				attorneySearchModelBean.setFirmList(null);
				//attorneySearchModelBean.setFirmListWatchList(firmListWatchList);
			}
			//attorneySearchModelBean.setFirmListWatchList(firmListWatchList);
			//attorneySearchModelBean.setFirmList(null);
		}
		else
		{
			firmsList = new ArrayList<Firm>();
			if(attorneySearchModelBean.getFirmList()!=null && attorneySearchModelBean.getFirmList().size() >0)
			{
				for (String firmId : attorneySearchModelBean.getFirmList())
				{
					Firm firmObject = firmService.getFirmById(Integer.parseInt(firmId));
					firmsList.add(firmObject);
					firmsListForResults.add(firmObject.getCompany());
				}
				attorneySearchModelBean.setFirmListWatchList(null);
				attorneySearchDTO.setSelectedFirms(firmsList);
				
			}
			else
			{
				attorneySearchModelBean.setFirmListWatchList(null);
				attorneySearchDTO.setSelectedFirms(null);
			}
	
		}
		
		//attorneySearchDTO.setSelectedFirms(firmsList);
		attorneySearchModelBean.setSelectedFirms(firmsListForResults.toString().replace("[", "").replace("]", ""));
		
	}

	protected Log	_log	= LogFactoryUtil.getLog(AbstractAttorneyController.class.getName());
}
