package com.alm.rivaledge.controller.search;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.StringTokenizer;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.alm.rivaledge.controller.AbstractAttorneyController;
import com.alm.rivaledge.model.AttorneySearchModelBean;
import com.alm.rivaledge.model.ClickToView;
import com.alm.rivaledge.persistence.domain.lawma0_data.Admission;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.Keyword;
import com.alm.rivaledge.persistence.domain.lawma0_data.LawSchool;
import com.alm.rivaledge.persistence.domain.lawma0_data.Practice;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.service.AttorneyService;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.service.WatchlistService;
import com.alm.rivaledge.transferobject.AttorneySearchDTO;
import com.alm.rivaledge.transferobject.AutocompleteSearchDTO;
import com.alm.rivaledge.transferobject.LocationList;
import com.alm.rivaledge.transferobject.UserGroupDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.FirmJsonUtility;
import com.alm.rivaledge.util.PracticeJsonUtility;
import com.alm.rivaledge.util.WebUtil;
import com.google.gson.Gson;
import com.liferay.portal.kernel.cache.MultiVMPoolUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;




/**
 * Event controller responsible for all operation on Event search form and results
 * screen
 * 
 * @author FL728
 * 
 */
@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "attorneySearchModelBean", "attorneySearchDTO", "attorneySearchResults" })
public class AttorneySearchController extends AbstractAttorneyController
{
	protected Log _log = LogFactoryUtil.getLog(AttorneySearchController.class.getName());
	
	@Autowired
	private FirmService firmService;
	
	@Autowired
	private WatchlistService watchlistService;
	
	@Autowired
	private AttorneyService attorneyService;
	
	@RenderMapping
	protected ModelAndView attorneySearchForm(@ModelAttribute("attorneySearchModelBean") AttorneySearchModelBean attorneySearchModelBean,
			ModelMap model, RenderRequest request, RenderResponse response) throws Exception
	{
		ClickToView<AttorneySearchModelBean, AttorneySearchDTO> c2v = new ClickToView<AttorneySearchModelBean, AttorneySearchDTO>();
		c2v.setSearchModelBean(attorneySearchModelBean);

		honorClick2View(request, model, c2v); // attorneySearchModelBean is affected only when you are directed here from C2V
		
		return renderSearchPage(attorneySearchModelBean, model, request, response);
	}
	
	

	private ModelAndView renderSearchPage(AttorneySearchModelBean attorneySearchModelBean, ModelMap model, RenderRequest request,
			RenderResponse response)
	{
		
		Gson 		 gson 		= new Gson();
		String locationTreeJsonString="";
		List<String> fromYear 	= new ArrayList<String>();
		List<String> toYear 	= new ArrayList<String>();
		setYearRange(fromYear, toYear);
		
		FirmJsonUtility 			firmJsonUtility 		= null;
		PracticeJsonUtility 		practiceJsonUtility 	= null;
		List<UserGroupDTO> 			allWatchLists 			= null;
		List<FirmJsonUtility> 		firmJsonUtilityList 	= new ArrayList<FirmJsonUtility>();
		List<PracticeJsonUtility> 	practiceJsonUtilityList	= new ArrayList<PracticeJsonUtility>();
		List<Firm> 					firmsSet 				= firmService.getAllFirms();
		List<Practice> 				practiceList 			= firmService.getAllPractices();
		User 						currentUser 			= (model.get("currentUser") != null) ? ((User) model.get("currentUser")) : WebUtil.getCurrentUser(request, userService); 
		
		if (currentUser != null)
		{
			allWatchLists = (List<UserGroupDTO>) watchlistService.getAllFirmWatchList(currentUser.getId());
		}		
		
		// creating firm json list for firm Auto completer
		for (Firm firm : firmsSet)
		{
			firmJsonUtility = new FirmJsonUtility();
			firmJsonUtility.setId(firm.getCompanyId().toString());
			firmJsonUtility.setLabel(firm.getCompany());
			firmJsonUtility.setCategory("Single Firms");
			firmJsonUtilityList.add(firmJsonUtility);
		}

		// creating Practice area json list for Practice area Auto completer
		for (Practice practice : practiceList)
		{
			practiceJsonUtility = new PracticeJsonUtility();
			practiceJsonUtility.setLabel(practice.getPracticeArea());
			practiceJsonUtilityList.add(practiceJsonUtility);
		}
		
		// converting all firms to JSON string
		String jsonAllFirms = gson.toJson(firmService.getRivalEdgeList(firmJsonUtilityList));
		// converting all Practices to JSON string
		String jsonAllPractics = gson.toJson(practiceJsonUtilityList);
		
		List<UserGroup> allWatchListsDefaultList = null;
		if (currentUser != null)
		{
			allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
			if (allWatchListsDefaultList.size() == 0)
			{
				model.addAttribute("allWatchListsDefaultList", "");
			}
			else
			{
				model.addAttribute("allWatchListsDefaultList", allWatchListsDefaultList.get(0));
			}
		}		
		
		if(MultiVMPoolUtil.get("LocationCache", "LocationTree")==null)
		{
			//Fetching All location hierarchy information as its not available in cache
			List<LocationList>  locList= attorneyService.fetchLocationTree(attorneySearchModelBean.getSelectedLocation());	
			//converting all locations information to JSON String so that same can available as input for Dynatree.
			locationTreeJsonString= gson.toJson(locList);
			//Putting location json information to cache
			MultiVMPoolUtil.put("LocationCache", "LocationTree" ,locationTreeJsonString);
		}
		else
		{
			//getting Location tree JSON String from cache
			locationTreeJsonString=(String) MultiVMPoolUtil.get("LocationCache", "LocationTree");
		}
		List<String> locationList=ListUtil.fromArray(StringUtil.split(attorneySearchModelBean.getSelectedLocation(), ","));
		locationTreeJsonString=attorneyService.updateLocationTree(locationTreeJsonString, locationList);
		//populate Models
		model.addAttribute("firmJson", jsonAllFirms);
		model.addAttribute("LocationJsonTree", locationTreeJsonString);
		model.addAttribute("AllLocationsJson", gson.toJson(attorneyService.fetchCitiesForAutocomplete()));
		model.addAttribute("titlesJson", gson.toJson(ALMConstants.ALL_TITLES_LIST));
		model.addAttribute("allFirms", firmsSet);
		model.addAttribute("practiceJson", jsonAllPractics);
		model.addAttribute("fromYear", fromYear);
		model.addAttribute("toYear", toYear);
		model.addAttribute("allPracticeArea", practiceList);
		model.addAttribute("allWatchLists", allWatchLists);
		model.addAttribute("allOtherLocations", ALMConstants.LOCATIONS);
		model.addAttribute("allRankingList", ALMConstants.RIVALEDGE_RANKING_LIST);
		model.addAttribute("allDisplayColumns", ALMConstants.ATTORNEY_DISPLAY_COLUMNS.values());
		
		return new ModelAndView("attorneysearch", model);
	}
	
	@ActionMapping(params = "action=changeSearchCriteria")
	protected void attorneySubmitForm(@ModelAttribute("attorneySearchModelBean") AttorneySearchModelBean attorneySearchModelBean,
									  ModelMap model, 
									  ActionRequest request, 
									  ActionResponse response) throws Exception
	{
		AttorneySearchDTO attorneySearchDTO = getDTOFromModelBean(model, attorneySearchModelBean, request);
		User			  currentUser		= WebUtil.getCurrentUser(request, userService);	
		
		addingAutocompleteInformation(attorneySearchDTO, currentUser);

		// The user changed the search criteria, so we will have to persist
		// the changed criteria into the database.
		// However, if this is the Home page, then we will persist these as
		// the search criteria for the specific chart instead, and not
		// generically as the module search criteria.
		String currentPortletId 	= attorneySearchModelBean.getPortletId();
		String 	currentPage			= WebUtil.getCurrentPage(request);
		boolean	isHomePage			= ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		boolean clickToViewPage = WebUtil.isClickToViewPage(request);
		if ((currentUser != null) && (currentPortletId != null) && (currentPortletId.length() > 0) && !clickToViewPage)
		{
			if (!isHomePage)
			{
				currentPage = "ATTORNEY_SEARCH";
				currentPortletId = "ATTORNEY_SEARCH";
			}
			userService.saveUserPreferences(currentUser.getId(), 
											currentPortletId, 
											WebUtil.getJson(attorneySearchModelBean), 
											null, 
											currentPage);
		}
		
		//RER-422
		attorneySearchModelBean.setShowResult(Boolean.TRUE);
		
		//set the new SearchDTO back into the model, as user changed the search criteria
		model.addAttribute("attorneySearchDTO", attorneySearchDTO);
		
		//set attorneySearchModelBean into modal again - RER-422
		model.addAttribute("attorneySearchModelBean", attorneySearchModelBean);
	}

	/**
	 * This method checks whether user inputs String for keyword,admission and law school is already there
	 * in their respective table if not then persist the same into database. 
	 * 
	 * @param attorneySearchDTO
	 * @param currentUser
	 */
	private void addingAutocompleteInformation(AttorneySearchDTO attorneySearchDTO, User currentUser) 
	{
		
		// Checking whether keyword already exist in database or not
		// if it doesn't exist in database then inserting new record in
		// tbl_rer_kwsearch_log table
		// for input keyword string
		if (attorneySearchDTO.getKeywordsInBioText() != null && firmService.countByKeyword(attorneySearchDTO.getKeywordsInBioText()).intValue() == 0)
		{
			Keyword keyword = new Keyword();
			keyword.setUserId(currentUser!=null?currentUser.getId():0);
			keyword.setKeyword(attorneySearchDTO.getKeywordsInBioText());
			firmService.addkeyword(keyword);
		}
		
		// Checking whether input Law School already exist in database or not
		// if it doesn't exist in database then inserting new record in
	    // tbl_rer_lawschoolsearch_log table
		// for input Law School string so that next it would be available for user to select from Autocomplete
		if (attorneySearchDTO.getLawSchooltext() != null && attorneyService.countByLawSchool(attorneySearchDTO.getLawSchooltext()).intValue() == 0)
		{
			
			LawSchool lawSchool = new LawSchool();
			lawSchool.setUserId(currentUser!=null?currentUser.getId():0);
			lawSchool.setLawschool(attorneySearchDTO.getLawSchooltext());
			attorneyService.addLawSchool(lawSchool);
		}
		
		// Checking whether input Admission already exist in database or not
		// if it doesn't exist in database then inserting new record in
		// tbl_rer_lawschoolsearch_log table
		// or input Law School string so that next it would be available for user to select from Autocomplete
		if (attorneySearchDTO.getAdmissions() != null && attorneyService.countByAdmission(attorneySearchDTO.getAdmissions()).intValue() == 0)
		{
			Admission admission = new Admission();
			admission.setUserId(currentUser!=null?currentUser.getId():0);
			admission.setAdmission(attorneySearchDTO.getAdmissions());
			attorneyService.addAdmission(admission);
		}
	}
	
	@ResourceMapping("updateModelBean")
	public void updateModelBean(@ModelAttribute("attorneySearchModelBean") AttorneySearchModelBean attorneySearchModelBean, 
								ModelMap model, 
								ResourceRequest request, 
								ResourceResponse response)
	{
		//get the selected columns from form
		String inputData = request.getParameter("displayColumns");
		if ( inputData == null || inputData.equals(""))
		{	
			return;
		}
		StringTokenizer st = new StringTokenizer(inputData,";");
		List<String> displayColumnsList = new ArrayList<String>();
		while(st.hasMoreElements())
		{
			String element = (String)st.nextElement();
			displayColumnsList.add(element);
		}
		
		//set columns back to model bean
		attorneySearchModelBean.setDisplayColumns(displayColumnsList);
		
		//set model bean back to session to get those values back in print page
		model.put("attorneySearchModelBean", attorneySearchModelBean);
	}
	
	/**
	 * This method would returns all qualified keyword in Json format(Max 100) based on user input in Keyword input box and logged-in User.
	 * Ex: if User has entered String "data" so it would return all keywords which is having
	 * "data" String.
	 * @param model
	 * @param request
	 * @param response
	 * @throws IOException 
	 */
	@ResourceMapping("attorneyKeywords")
	public void keywordSearch(ModelMap model, ResourceRequest request, ResourceResponse response) throws IOException
	{
		String 					inputKeyword 			= request.getParameter("keyword");
		User 					currentUser 			= (model.get("currentUser") != null) ? ((User) model.get("currentUser")) : WebUtil.getCurrentUser(request, userService);
		AutocompleteSearchDTO 	autocompleteSearchDTO 	= null;		
		List<String> 			listOfKeywords 			= null;
		
	   if (inputKeyword != null && !StringUtils.isEmpty(inputKeyword) )
		{
		    autocompleteSearchDTO	= new AutocompleteSearchDTO();
		    autocompleteSearchDTO.setKeyword("%"+inputKeyword.trim()+"%");
		    autocompleteSearchDTO.setUserId(currentUser!=null?currentUser.getId():0);
			listOfKeywords 			= attorneyService.getKeywords(autocompleteSearchDTO);
		}
		
		// converting all Keywords to JSON string
		Gson gson = new Gson();
		String jsonAllKeywords= gson.toJson(listOfKeywords);
		response.getWriter().write(jsonAllKeywords);
	}
	
	/**
	 * This method would returns all qualified Law School in Json format(Max 100) based on user input in Law School input box and logged-in User.
	 * Ex: if User has entered String "data" so it would return all Law School which is having
	 * "data" String
	 * @param model
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@ResourceMapping("attorneyLawSchool")
	public void lawSchoolSearch(ModelMap model, ResourceRequest request, ResourceResponse response) throws IOException
	{
		String 					inputLawSchoolText 		= request.getParameter("lawSchoolText");
		User 					currentUser 			= (model.get("currentUser") != null) ? ((User) model.get("currentUser")) : WebUtil.getCurrentUser(request, userService);
		AutocompleteSearchDTO 	autocompleteSearchDTO 	= null;		
		List<String> 			listOflawSchools 		= null;
		
	   if (inputLawSchoolText != null && !StringUtils.isEmpty(inputLawSchoolText) )
		{
		    autocompleteSearchDTO	= new AutocompleteSearchDTO();
		    autocompleteSearchDTO.setKeyword("%"+inputLawSchoolText.trim()+"%");
		    autocompleteSearchDTO.setUserId(currentUser!=null?currentUser.getId():0);
		    listOflawSchools 			= attorneyService.getLawSchool(autocompleteSearchDTO);
		}
		
		// converting all Law School to JSON string
		Gson gson = new Gson();
		String jsonAllLawSchools = gson.toJson(listOflawSchools);
		response.getWriter().write(jsonAllLawSchools);
	}
	
	/**
	 * This method would returns all qualified Admissions in Json format(Max 100) based on user input in Admissions input box and logged-in User.
	 * Ex: if User has entered String "data" so it would return all Admissions which is having
	 * "data" String
	 * @param model
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	@ResourceMapping("attorneyAdmissions")
	public void admissionsSearch(ModelMap model, ResourceRequest request, ResourceResponse response) throws IOException
	{
		String 					inputAdmissions 		= request.getParameter("admissions");
		User 					currentUser 			= (model.get("currentUser") != null) ? ((User) model.get("currentUser")) : WebUtil.getCurrentUser(request, userService);
		AutocompleteSearchDTO 	autocompleteSearchDTO 	= null;		
		List<String> 			listOfAdmissions 		= null;
		
	   if (inputAdmissions != null && !StringUtils.isEmpty(inputAdmissions) )
		{
		    autocompleteSearchDTO	= new AutocompleteSearchDTO();
		    autocompleteSearchDTO.setKeyword("%"+inputAdmissions.trim()+"%");
		    autocompleteSearchDTO.setUserId(currentUser!=null?currentUser.getId():0);
			listOfAdmissions 			= attorneyService.getAdmissions(autocompleteSearchDTO);
		}
		
		// converting all Admissions to JSON string
		Gson gson = new Gson();
		String jsonAllAdmissions= gson.toJson(listOfAdmissions);
		response.getWriter().write(jsonAllAdmissions);
	}	

	@ActionMapping(params = "action=clickToView")
	protected void clickToView(@ModelAttribute("attorneySearchModelBean_c2v") AttorneySearchModelBean attorneySearchModelBean_c2v, 
												 ModelMap model, 
												 ActionRequest request,
												 ActionResponse response) throws Exception
	{
		String attorneySearchModelBeanJson = WebUtil.getJson(attorneySearchModelBean_c2v);
		response.setRenderParameter("attorneySearchModelBeanJson", attorneySearchModelBeanJson);
		response.setRenderParameter("renderPage", "clickToView");
	}
	
	@RenderMapping(params = "renderPage=clickToView")
	protected ModelAndView clickToView(@RequestParam("attorneySearchModelBeanJson") String attorneySearchModelBeanJson, 
												 ModelMap model, 
												 RenderRequest request,
												 RenderResponse response) throws Exception
	{
		AttorneySearchModelBean asmb = WebUtil.getObject(attorneySearchModelBeanJson, AttorneySearchModelBean.class);
		asmb.setAddToSession(false);
		model.addAttribute("attorneySearchModelBean", asmb);
		return renderSearchPage(asmb, model, request, response);
	}
	
	@RenderMapping(params = "renderPage=searchCriteriaForChart")
	protected ModelAndView renderSearchCriteriaForChart(@RequestParam("chartPortletId") String chartPortletId, 
												 ModelMap model, 
												 RenderRequest request,
												 RenderResponse response) throws Exception
	{
		User currentUser = (model.get("currentUser") != null) ? ((User) model.get("currentUser")) :	WebUtil.getCurrentUser(request, userService);

		AttorneySearchModelBean asmb = getUserPreference(currentUser.getId(), chartPortletId, ALMConstants.HOME_PAGE);
		if(asmb == null)
		{
			asmb = getDefaultAttorneySearchModelBean(currentUser);
		}
		asmb.setPortletId(chartPortletId);
		asmb.setAddToSession(false);
		model.addAttribute("attorneySearchModelBean", asmb);
		model.addAttribute("chartPortletId", chartPortletId);
		return renderSearchPage(asmb, model, request, response);
	}
	
	
	
	@ResourceMapping("persistSearchCriteriaForChart")
	protected void persistSearchCriteriaForChartOnHomePage(@ModelAttribute("attorneySearchModelBean") AttorneySearchModelBean attorneySearchModelBean, 
												@RequestParam("chartPortletId") String chartPortletId,
												ModelMap model, 
												ResourceRequest request,
												ResourceResponse response) throws Exception
	{
		User currentUser = (model.get("currentUser") != null) ? ((User) model.get("currentUser")) :	WebUtil.getCurrentUser(request, userService);

		userService.saveUserPreferences(currentUser.getId(), chartPortletId, WebUtil.getJson(attorneySearchModelBean), null, ALMConstants.HOME_PAGE);
		
	}
	

	/**
	 * This method is for populating Graduation start and end year on Attorney
	 * search Screen It would start from 1900 and will have values till current
	 * year
	 * 
	 * @param fromYear
	 * @param toYear
	 * 
	 */
	private void setYearRange(List<String> fromYear, List<String> toYear)
	{
		Calendar cal = Calendar.getInstance(); // This gets the current date and  time.
		
		int currentYear = cal.get(Calendar.YEAR);
		fromYear.add(ALMConstants.ANY);
		toYear.add(ALMConstants.ANY);
		for (Integer i = 1900; i <= currentYear; i++)
		{
			fromYear.add(i.toString());
		}
		for (Integer i = 1900; i <= 2050; i++)
		{
			toYear.add(i.toString());
		}
	}
}
