package com.alm.rivaledge.controller.charts.graduationyear;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.alm.rivaledge.controller.AbstractAttorneyController;
import com.alm.rivaledge.model.AttorneySearchModelBean;
import com.alm.rivaledge.model.chart.BaseChartModelBean.ComparisonDataType;
import com.alm.rivaledge.model.chart.StandardChartModelBean;
import com.alm.rivaledge.persistence.dao.CacheDAO;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.service.AttorneyService;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.transferobject.AttorneyGraduationResultDTO;
import com.alm.rivaledge.transferobject.AttorneyResultDTO;
import com.alm.rivaledge.transferobject.AttorneySearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.StringPool;

@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "attorneysByGradYearChartModelBean", "attorneySearchModelBean" })
public class AttorneyGraduationChartController extends AbstractAttorneyController
{

	protected Log			_log	= LogFactoryUtil.getLog(AttorneyGraduationChartController.class.getName());
	@Autowired
	private AttorneyService	attorneyService;

	@Autowired
	private CacheDAO		cacheDAO;

	@Autowired
	private FirmService		firmService;

	@ModelAttribute
	public void populateChartModelBean(ModelMap model, PortletRequest request)
	{
		StandardChartModelBean 	attorneysByGradYearChartModelBean = null;
		String					currentPortletId			 = WebUtil.getCurrentPortletId(request);
		String					currentPage				 	 = WebUtil.getCurrentPage(request);
		boolean					isHomePage				 	 = ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		User 					currentUser 			 	 = (model.get("currentUser") != null) ? ((User) model.get("currentUser")) : WebUtil.getCurrentUser(request, userService); 

		if (!isHomePage)
		{
			currentPage			= "ATTORNEY_SEARCH";
		}
	
		attorneysByGradYearChartModelBean = getUserChartPreference(currentUser.getId(), currentPortletId, currentPage);
		
		if (attorneysByGradYearChartModelBean == null)
		{
			attorneysByGradYearChartModelBean = new StandardChartModelBean();
			attorneysByGradYearChartModelBean.init(); // do an explicit init for default values
		}
		model.put("attorneysByGradYearChartModelBean", attorneysByGradYearChartModelBean);
	}

	@RenderMapping
	public String defaultChartView(@ModelAttribute("attorneysByGradYearChartModelBean") StandardChartModelBean attorneysByGradYearChartModelBean,
								   @ModelAttribute("attorneySearchModelBean") AttorneySearchModelBean attorneySearchModelBean,
								   ModelMap model, 
								   RenderRequest request,
								   RenderResponse response) throws Exception
	{
		
		AttorneySearchDTO attorneySearchDTO = getDTOFromModelBean(model, attorneySearchModelBean, request);
		AttorneySearchDTO serviceAttorneySearchDTO = (AttorneySearchDTO) attorneySearchDTO.clone();

		List<AttorneyResultDTO> attorneySearchResults = new ArrayList<AttorneyResultDTO>();

		// Get the current user from the render request
		User currentUser = WebUtil.getCurrentUser(request, userService);

		attorneySearchResults = attorneyService.findAttorneyDataForcharts(serviceAttorneySearchDTO, currentUser);

		//populate model for Firms dropdown
		List<Firm> allSearchResultsFirmList = new ArrayList<Firm>();

		for (AttorneyResultDTO arDTO : attorneySearchResults)
		{
			Firm firm = new Firm();
			firm.setCompanyId(arDTO.getFirmId());
			firm.setCompany(arDTO.getFirmName());

			allSearchResultsFirmList.add(firm);
		}
		
		String selectedFirmName = "";
		List<Firm> firmList = new ArrayList<Firm>();
		
		if ((attorneysByGradYearChartModelBean != null) && (attorneysByGradYearChartModelBean.getSearchResultsFirmList() != null)
				&& (attorneysByGradYearChartModelBean.getSearchResultsFirmList().size() > 0))
		{
			Firm firmTemp = new Firm();
			firmTemp = firmService.getFirmById(Integer.parseInt(attorneysByGradYearChartModelBean.getSearchResultsFirmList().get(0)));
			if (!allSearchResultsFirmList.contains(firmTemp)){
				firmList.add(allSearchResultsFirmList.get(0));
			}else{
				firmList.add(firmService.getFirmById(Integer.parseInt(attorneysByGradYearChartModelBean.getSearchResultsFirmList().get(0))));
			}
			if (firmList!= null && firmList.size() >0){
				selectedFirmName = firmList.get(0).getCompany();
			}
		}
		else
		{
			if (attorneySearchResults != null && attorneySearchResults.size() > 0)
			{
				for (AttorneyResultDTO attorneyResultDTO : attorneySearchResults)
				{
					Integer firmID = attorneyResultDTO.getFirmId();
					if (firmID != 0)
					{
						firmList.add(firmService.getFirmById(firmID));
						selectedFirmName = firmList.get(0).getCompany() ;// firmService.getFirmById(firmID).getCompany();
						break;
					}
				}
			}
		}

		serviceAttorneySearchDTO.setSelectedFirms(firmList);

		List<AttorneyGraduationResultDTO> attorneySearchChartResultData = attorneyService.findAttorneyDataForGradcharts(serviceAttorneySearchDTO);

		StringBuffer sbAssociates 	= new StringBuffer();
		StringBuffer sbCounsels 	= new StringBuffer();
		StringBuffer sbPartners 	= new StringBuffer();
		StringBuffer sbAll 			= new StringBuffer();

		double 	average 	  = 0;
		int 	totalExp 	  = 0;
		int 	totalAttorney = 0;
		int 	maxExperience = 0;
		for (AttorneyGraduationResultDTO aDTO : attorneySearchChartResultData)
		{
			try
			{
				totalAttorney += aDTO.getAttorneyCount();
				if (aDTO.getExperience() != null)
				{
					int tempExp = aDTO.getExperience();
					if (maxExperience < tempExp && tempExp < 200)
					{
						maxExperience = tempExp;
					}
					totalExp += ( tempExp * aDTO.getAttorneyCount() );
				}
			}
			catch (NullPointerException npe)
			{
				System.out.println("Exception: Null Pointer Exception " + npe.getMessage());
				npe.printStackTrace();
			}
		}
		if (totalAttorney > 0)
		{
			average = totalExp / totalAttorney;
		}
		int[] expAssociate = new int[maxExperience+2];
		int[] expCounsel = new int[maxExperience+2];
		int[] expPartner = new int[maxExperience+2];
		
		for (AttorneyGraduationResultDTO aDTO : attorneySearchChartResultData)
		{
			if (ALMConstants.ASSOCIATE.equalsIgnoreCase(aDTO.getTitle()))
			{
				if (aDTO.getExperience() != null && aDTO.getExperience() >= 0 && aDTO.getExperience() < 200)
				{
					expAssociate[aDTO.getExperience()+1] = aDTO.getAttorneyCount();
				}
				else
				{
					expAssociate[0] = aDTO.getAttorneyCount();
				}
			}
		}
		for (AttorneyGraduationResultDTO aDTO : attorneySearchChartResultData)
		{
			if (ALMConstants.OTHER_COUNSEL.equalsIgnoreCase(aDTO.getTitle()))
			{
				if (aDTO.getExperience() != null && aDTO.getExperience() >= 0 && aDTO.getExperience() < 200)
				{
					expCounsel[aDTO.getExperience()+1] = aDTO.getAttorneyCount();
				}
				else
				{
					expCounsel[0] = aDTO.getAttorneyCount();
				}
			}
		}
		for (AttorneyGraduationResultDTO aDTO : attorneySearchChartResultData)
		{
			if (ALMConstants.PARTNERS.equalsIgnoreCase(aDTO.getTitle()))
			{
				if (aDTO.getExperience() != null && aDTO.getExperience() >= 0 && aDTO.getExperience() < 200)
				{
					expPartner[aDTO.getExperience()+1] = aDTO.getAttorneyCount();
				}
				else
				{
					expPartner[0] = aDTO.getAttorneyCount();
				}
			}
		}
		
		for (int i = 0; i <= maxExperience +1 ; i++) {
			if (sbAssociates.length() > 0)
			{
				sbAssociates.append(",");
			}
			sbAssociates.append(expAssociate[i]);
			if (sbCounsels.length() > 0)
			{
				sbCounsels.append(",");
			}
			sbCounsels.append(expCounsel[i]);
			if (sbPartners.length() > 0)
			{
				sbPartners.append(",");
			}
			sbPartners.append(expPartner[i]);
			int tempExp = expAssociate[i] + expCounsel[i] + expPartner[i];
			if (sbAll.length()>0){
				sbAll.append(",");
			}
			sbAll.append(tempExp);
		}
		
		
		// 4. Getting the average of search results.
		if ((attorneysByGradYearChartModelBean.getComparisonDataTypeList() != null)
				&& (!attorneysByGradYearChartModelBean.getComparisonDataTypeList().isEmpty())
				&& (attorneysByGradYearChartModelBean.getComparisonDataTypeList().contains(ComparisonDataType.AVERAGE.getValue()))
				&& (attorneysByGradYearChartModelBean.getFirmList() != null)
				&& (!attorneysByGradYearChartModelBean.getFirmList().isEmpty()))
		{
			model.put("isAverageAsked", true);
		}else{
			model.put("isAverageAsked", false);
		}
		
		
		
		// Put the results into the model
		// Set the chart type
		model.put("chartType", attorneysByGradYearChartModelBean.getChartType());
		model.put("attorneyGraduationChartAverage", average);
		model.put("attorneyGraduationChartTitle", "NUMBER OF ATTORNEYS BY YEAR <br/>"+selectedFirmName);
		model.put("attorneyGraduationChartSettingsTitle", "NUMBER OF ATTORNEYS BY YEAR");
		model.put("attorneyGraduationChartYAxisTitle", "No. of Attorneys");
		model.put("attorneyGraduationChartXAxisTitle", "Years Since Law School Graduation");
		model.addAttribute("attorneySearchResults", attorneySearchResults);
		model.addAttribute("attorneySearchChartResultData", attorneySearchChartResultData);
		
		//Checking if there are search results or not for the search we ran 	
		if (attorneySearchChartResultData==null || attorneySearchChartResultData.size()==0)
		{
			model.put("sbAssociates", "");
			model.put("sbCounsels", "");
			model.put("sbPartners", "");
		}
		else
		{
			model.put("sbAssociates", sbAssociates.toString());
			model.put("sbCounsels", sbCounsels.toString());
			model.put("sbPartners", sbPartners.toString());
		}
		
		model.put("selectedFirmName", selectedFirmName);
//		model.put("selectedFirmId", firmList.get(0).getCompanyId());
		model.put("allSearchResultsFirmList", allSearchResultsFirmList);
		
		return "attorneysbygradyear";

	}

	@ActionMapping(params = "action=updateChart")
	public void updateChart(@ModelAttribute("attorneysByGradYearChartModelBean") StandardChartModelBean attorneySearchModelBean,
							ActionRequest request, 
							ActionResponse response)
	{
		// Persist the changes to the chart settings
		// If this is the home page, save them there, else
		// save with the generic NEWS_PUBS tag.
		String 	currentPortletId 	= WebUtil.getCurrentPortletId(request);
		String 	currentPage			= WebUtil.getCurrentPage(request);
		boolean	isHomePage			= ((currentPage.equals("home")) || (currentPage.equals("")));
		User	currentUser 		= WebUtil.getCurrentUser(request, userService);
		if (!isHomePage)
		{
			currentPage = "ATTORNEY_SEARCH";
		}
		userService.saveUserPreferences(currentUser.getId(), 
										currentPortletId, 
										null, 
										WebUtil.getJson(attorneySearchModelBean), 
										currentPage);
	}
	
	/*
	* Following method remove rendering portlet from the page. And redirects to same page.
	*
	*/
	@ActionMapping(params = "action=removePortleFromThePage")
	public void removePortletFromPage(ActionRequest request, ActionResponse response) throws Exception
	{
		WebUtil.removePortlet(request);
	}
}
