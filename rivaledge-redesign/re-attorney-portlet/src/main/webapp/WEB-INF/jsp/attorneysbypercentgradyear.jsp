<%@page import="com.alm.rivaledge.util.ALMConstants"%>
<%@page import="com.alm.rivaledge.model.chart.BaseChartModelBean"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.alm.rivaledge.model.AttorneySearchModelBean"%>
<%@page import="com.alm.rivaledge.util.ChartData"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="liferay-portlet" uri="http://liferay.com/tld/portlet"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="liferay-util" uri="http://liferay.com/tld/util"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<portlet:defineObjects />

<%@ include file="./common.jsp" %>

<portlet:actionURL var="removePortletURL">
	<portlet:param name="action" value="removePortleFromThePage"/>
</portlet:actionURL>

<style>

.percentGradAttorneyPage h6 {
	color: #222222;
    font: bold 12px Arial,Helvetica,sans-serif;
    margin: 5px 0 0 10px !important;
}

.percentGradAttorneyPage .section-one {
    float: left;
    width: 160px;
}
.percentGradAttorneyPage .section-two {
    float: left;
    width: 200px;
}
</style>
<script type="text/javascript">

var <portlet:namespace/>thisPortletId = '<%= themeDisplay.getPortletDisplay().getId() %>';

function removeThisChartAttorneysPercentGradYear(){
	
	var removePortletAction = '<%= removePortletURL.toString()%>';
	var r=confirm("Are you sure you want to delete this chart?");
	if (r == true)
  	{
		Liferay.Portlet.showBusyIcon("#bodyId", "Loading...");
		$("#attorneysByPercentGradYearChartModelBean").attr('action', removePortletAction);
		$("#attorneysByPercentGradYearChartModelBean").submit();
  	}
	else
	{
	  return false;
	}
}

<jsp:include page="theme.html"/>

<c:choose>
<c:when test="${chartType eq 'line' }">
	var typeOfChartA = 'line';	//This will be type of chart.
	var stackingTypeA = '';
</c:when> 
<c:otherwise>
	var typeOfChartA = 'line';
	var stackingTypeA = '';
</c:otherwise>	
</c:choose>


var series=new Array();
var seriesPartners=new Array();
var seriesAssociates=new Array();
var seriesCounsels=new Array();
var seriesTotals=new Array();
var seriesAllVal=new Array();
var seriesArray = new Array();
var checkData="${searchResultFlag}";


var seriesPartnersValues=new Array();
var seriesAssociatesValues=new Array();
var seriesCounselsValues=new Array();

<c:choose>
<c:when test="${fn:length(seriesData) gt 0}">
	<c:forEach  var="results" items="${seriesData}" varStatus="i">
	series[<c:out value="${i.count}."/>] = '${results.sbFirm}';
	seriesPartners[<c:out value="${i.count}."/>] = [${results.sbPartners}];
	seriesAssociates[<c:out value="${i.count}."/>] = [${results.sbAssociates}];
	seriesCounsels[<c:out value="${i.count}."/>] = [${results.sbOtherCounsel}];
	seriesTotals[<c:out value="${i.count}."/>] = [${results.sbTotals}];
	seriesAllVal[<c:out value="${i.count}."/>] = [${results.sbTotalValue}];
	</c:forEach>
</c:when>
</c:choose>

$(function () 
{
	if(<%=!isHomePage%>)
	{	
		if(checkData=="<%=ALMConstants.NO_RESULT_MESSAGE%>")
		{
			$("#p_p_id<portlet:namespace/>").hide();
			$(".cph").show();
			return;
		}
	}
	else
	{
		if(checkData=="<%=ALMConstants.NO_RESULT_MESSAGE%>")
		{
			$("#noDataPercentAttorneyGraduationDiv").removeClass("hideClass");
			$("#container-graduation-percent").addClass("hideClass");
			
			$("#No-Data-AttorneyPercGradyear").addClass("No-Data-Charts");
			$("#attorney_percGradYear_settings").hide();
		}
		else
		{
			$("#No-Data-AttorneyPercGradyear").removeClass("No-Data-Charts");
			$("#noDataPercentAttorneyGraduationDiv").addClass("hideClass");
		}
	}
	
	// When user clicks reset all button default graph will be shown. 
	$(".settingReset").click(function(){
		$('.graphType').attr('checked', false);
		$('.default').prop('checked', true);
		$("#searchResultsPercentChangeFirmList").val($("#searchResultsPercentChangeFirmList option:first").val());
		$('.defaultLimitType').prop('checked', true);
		$('.percentChangeAverageValueAPG').prop('checked', true);
		$('.percentChangeRankingCheckAPG').prop('checked', true);
		$("#percentChangeRankingListAPG").val($("#percentChangeRankingListAPG option:first").val());
	});
	$(".cancelAPG").click(function(){
		$("#percentChangeAttorneyViewSettings").hide();
	});
	Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function(color) {
	    return {
	        radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
	        stops: [
	            [0, color],
	            [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
	        ]
	    };
	});
	
	$("#searchResultsPercentChangeFirmList").click(function(){
		if($("#searchResultsPercentChangeFirmList option:selected")){
			$("input[value=firm]").prop("checked", true);
		}
	});
	$("#percentChangeRankingListAPG").change(function(){
		$("input[value='RivalEdge Average']").prop("checked", true);
	});
	$("#watchList").click(function(){
		if($("#watchList option:selected")){
			$("input[value='Watchlist Average']").prop("checked", true);
		}
	});

	$(".AnalyzeResultHeader").show();
	
	$(".percentGradAttorneyPage .closewhite").click(function(){
		//$("#login-box2").hide();
		$("#percentChangeAttorneyViewSettings").hide();
	});
	
	$(".highcharts-button").click(function(){
		$("div#popupPieChartDiv").hide();
		$("#login-box2").hide();
	});
	$("#attorneyGradYrCloseSetting").click(function(){
		$("#percentAttorneysGradViewSetting").hide();
	});
	
	$("#rightChartSettingApply").click(function(){
		$("#login-box2").hide();
		if($('input[name=graphType2]:radio:checked').val() == 'verticalBar')
		{
			displayBarGraph('column');
		} else if($('input[name=graphType2]:radio:checked').val() == 'HorizontalBar')
		{
			displayBarGraph('bar');
		}else if($('input[name=graphType2]:radio:checked').val() == 'pieChart')
		{
			displayPieChart();
		}
	});
	
	$('#attorneyPerGradYrPrintCharts').click(function() {
        var chart = $('#container-graduation-percent').highcharts();
        chart.print();
    });
	$('#attorneyPerGradYrExportJPG').click(function() {
        var chart = $('#container-graduation-percent').highcharts();
        chart.exportChart({type: 'image/jpeg'});
    });
	$('#attorneyPerGradYrExportPNG').click(function() {
        var chart = $('#container-graduation-percent').highcharts();
        chart.exportChart({type: 'image/png'});
    });
	
	var xAxisLabels = [];
	
	// When user clicks on analysis tab active-menu css class is removed from details tab.
	// And same class added to analysis tab.
		$("#attorney-details").removeClass("active-menu");
		$("#attorney-analysis").addClass("active-menu");
	
        $('#container-graduation-percent').highcharts({
            chart: {
                type: typeOfChartA,
				height: 500,
				width: 1000
            },
            title: {
                text: '${attorneyGraduationChartTitle}',
                style: {
					color: '#000'
				}
            },
		xAxis: {
			categories: xAxisLabels,
    		title: {
				text: '${attorneyGraduationChartXAxisTitle}',
				style: {
                    color: '#000'
                }
			},
			labels : {
				formatter: function()
				{
					
					if(this.value == 0)
					{
						return "Not Available"
					}
					else if(this.value == 1)
					{
						return this.value
					}
					else if(this.value%5 == 0)
					{
						return this.value;
					}
				}
			},
			plotLines : [ {
				color : '#000000',
				width : 2,
				dashStyle : 'Solid',
				value : parseFloat(0.5)
			}]
           },
		yAxis : {
			gridLineWidth: 1,
		    gridLineColor: '#cecece',
		    minorGridLineColor: '#cecece',
		    lineWidth: 1,
			min: 0,
			title: {
				text: '${attorneyGraduationChartYAxisTitle}',
				style: {
                    color: '#000'
                }
			},
			labels : {
				formatter: function()
				{
					return this.value + "%";
				}
			},
			stackLabels : {
				enabled : false,
				style : {
					fontWeight : 'bold',
					color : (Highcharts.theme && Highcharts.theme.textColor)
							|| 'gray'
				}
			}
		},
		legend : {
			enabled : true
		},
		tooltip : {
			hideDelay: 1000,
			useHTML : true,
			positioner : function(boxWidth, boxHeight,
					point) {
					
					var xPosition = point.plotX - 20;
					var yPosition = point.plotY - boxHeight + 20;
					
					if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(625))
					{
						xPosition = point.plotX-120;
					}
					
					if(parseInt(point.plotY) < parseInt(boxHeight))
					{
						yPosition = point.plotY+120;
					}
					
				return {
					x : xPosition,
					y : yPosition
				};
			},
			formatter: function()
			{
				
				var seriesId = seriesArray[this.series.name];
				var total = seriesTotals[seriesId][this.x];
				var tooltipOption = '<b>'+this.series.name+'<br/>' + this.x + ' Years Since Graduation </b>';
				tooltipOption +=  '<table><tr><td></td><td>%</td><td>No.</td></tr>'+
				'<tr><td style="color: '+this.series.color+'; padding-left: 10px; margin-top: 15px;">All Attorneys</td>'+
				'<td style="border: 1px solid; background-color: #000; color: #fff; margin-top: 15px;text-align: center;">';
				tooltipOption += parseFloat(Math.round(seriesTotals[seriesId][this.x])).toFixed(0) + "%"
				+'</td>'+
				'<td style="border: 1px solid; background-color: #000; color: #fff; margin-top: 15px;text-align: center;">' ;
				tooltipOption +=	seriesAllVal[seriesId][this.x] + '</td>'+
				'</tr><tr>'+
				'<td style="color: #15375c; padding-left: 10px; margin-top: 15px;">Partners</td>'+
				'<td style="border: 1px solid; background-color: #15375c; color: #fff; margin-top: 15px;text-align: center;">';
				var temp1 = seriesPartnersValues[seriesId];
				if(temp1>0){
					temp1 = Math.round(seriesPartners[seriesId][this.x] * 100 / seriesPartnersValues[seriesId] );
				}else{
					temp1 = 0;
				}
				tooltipOption += temp1 + '%</td>'+
				'<td style="border: 1px solid; background-color: #15375c; color: #fff; margin-top: 15px;text-align: center;">' ;
				tooltipOption += seriesPartners[seriesId][this.x]   + '</td>'+'</tr><tr>'+
				'<td style="color: #ba323e; padding-left: 10px; margin-top: 15px;">Associates</td>'+
				'<td style="border: 1px solid; background-color: #ba323e; color: #fff; margin-top: 15px;text-align: center;">';
				var temp2 = seriesAssociatesValues[seriesId];
				if(temp2>0){
					temp2 =Math.round(seriesAssociates[seriesId][this.x] * 100 /  seriesAssociatesValues[seriesId]  );
				}else{
					temp2 = 0;
				}
				tooltipOption +=   temp2 + '%</td>'+
				'<td style="border: 1px solid; background-color: #ba323e; color: #fff; margin-top: 15px;text-align: center;">' ;
				tooltipOption += seriesAssociates[seriesId][this.x]+ '</td>'+
				'</tr><tr>'+
				'<td style="color: #de7c35; padding-left: 10px; margin-top: 15px;">Other Counsel</td>'+
				'<td style="border: 1px solid; background-color: #de7c35; color: #fff; margin-top: 15px;text-align: center;">';
				var temp3 = seriesCounselsValues[seriesId];
				if(temp3>0){
					temp3 = Math.round(seriesCounsels[seriesId][this.x] * 100 /  seriesCounselsValues[seriesId]  );
				}else{
					temp3 = 0;
				}
				tooltipOption +=  temp3 + '%</td>'+
				'<td style="border: 1px solid; background-color: #de7c35; color: #fff; margin-top: 15px;text-align: center;">' ;
				tooltipOption += seriesCounsels[seriesId][this.x]+ '</td>'+
				'</tr>';
				
				var url = "attorney-details?drilldownYear=" + encodeURIComponent(this.key) + "&drilldownFirmName=" + encodeURIComponent(this.series.name);
				tooltipOption += '<tr><td style="text-align:center;color:#000;"><a href="'+url+'" style="color:#000;">Click to View Details</a></td></tr></table>';

				return tooltipOption;
			}
		},
		plotOptions : {
			column: {
				events: {
					legendItemClick: function () {
						return false; 
					}
				}
			},
			series : {
				stacking : stackingTypeA,
				dataLabels : {
					enabled : false,
					color : '#FFFFFF',
					formatter : function() {
						if (parseInt(this.y) > 0) {
							return this.y;
						}
					}
				},
				point : {
					events : {
						mouseOver : function(e) {
						}
					}

				}
			}
		},
		series: [],
		navigation: {
            buttonOptions: {
                enabled: false
            }
        }
	});
        
        
                
        var chart = $('#container-graduation-percent').highcharts();
        for(var i=1;i<series.length;i++){
        	seriesArray[series[i]] = i;
        	chart.addSeries({
      		  name: series[i],
      		  data: seriesTotals[i]
      		 });	
        }
        
        for (var i=1; i < series.length; i++){
    		
    		var t1 = 0;
    		var t2 = 0;
    		var t3 = 0;
    		for (var j=0; j < seriesPartners[i].length; j++){
    			t1 += seriesPartners[i][j];
    		}
    		for (var j=0; j < seriesAssociates[i].length; j++){
    			t2 += seriesAssociates[i][j];
    		}
    		for (var j=0; j < seriesCounsels[i].length; j++){
    			t3 += seriesCounsels[i][j];
    		}
    		seriesPartnersValues[i] = t1;
    		seriesAssociatesValues[i] = t2;
    		seriesCounselsValues[i] = t3;
    		
        }
});


$(document).ready(function()
	    {	
	        $("#attorney_percGradYear").click(function()
	        {
	        	Liferay.Portlet.showPopup(
	        		{
	        			uri : '${chartSearchPortletURL}', // defined in common.jsp
	        			title: "Search Criteria"
	        		});
	        });	
	     });


</script>

<portlet:actionURL var="updateChartURL">
	<portlet:param name="action" value="updateChart"/>
</portlet:actionURL>
<div class="percentGradAttorneyPage newspublicationPage">
	<div class="colMin flLeft leftDynamicDiv">
		<div class="topHeader ForChartsTopHeader" style="width:100%;">
			<span title="Remove this chart" onclick="removeThisChartAttorneysPercentGradYear();" style="float: right; font-weight: bold; color: rgb(255, 255, 255); cursor: pointer; font-family: verdana; margin: 2px 5px; padding: 3px 8px;">X</span>
		</div>
		<div id="noDataPercentAttorneyGraduationDiv" class="hideClass">0 Results, Please try a different search</div>
		
			<form:form commandName="attorneysByPercentGradYearChartModelBean" method="post" action="${updateChartURL}" id="attorneysByPercentGradYearChartModelBean">
			<div class="flRight charts">
				<ul class="reset listView" id="No-Data-AttorneyPercGradyear">
					<c:if test="<%=isHomePage%>" >
						<li id="attorney_percGradYear" style="overflow:hidden;"><a href="javascript:void(0);" class="filter-icon" >&nbsp;</a></li>
					</c:if>
					<li id="attorney_percGradYear_settings"><a href="#percentChangeAttorneyViewSettings"
						class="btn icon settingsgry rightViewSetting login-window chartViewSetting"
						onclick="return false;">&nbsp;</a></li>
					<li>
						<a href="javascript:void(0);" id="attorneyPerGradYrPrintCharts" onclick="return false;" class="printChartClass"></a>
					</li>
					<li>
						<a href="javascript:void(0);" onclick="return false;" class="exportChartClass"></a>
                        <div class="actionSec">
                        <h5>Actions</h5>
                        <ul class="reset">
                            <li class="exportChartImage"><span id="attorneyPerGradYrExportJPG">Export as JPG</span></li>
                            <li class="exportChartImage"><span id="attorneyPerGradYrExportPNG">Export as PNG</span></li>
                        </ul>
                        <div class="clear">&nbsp;</div>
                        </div>
					</li>
				</ul>
				<div style="display: none" class="viewBox popusdiv ClickPopup" id="percentChangeAttorneyViewSettings">
					<div class="popHeader">
						<a href="javascript:void(0);" class="btn icon closewhite closeOne flRight" style="margin-top: 2px">&nbsp;Close</a>
							SETTINGS: ${attorneyGraduationChartTitle}
						<div class="clear">&nbsp;</div>
					</div>
					<div class="section-one">
                      <h6>Chart Type</h6>
                        <ul class="reset list4">
                             <li>
                              <form:radiobutton path="chartType" class="graphType default" value="<%= BaseChartModelBean.ChartType.LINE.getValue() %>" />
                              <span class="btn icon linechartmultiple">Line</span>
                            </li>
                        </ul>
                        <div class="clear">&nbsp;</div>
                    </div>
					<div class="section-two">
						<form action="#">
							<h6>Firm Data (Limit of 5)</h6>
							<ul class="reset list4">
								<li>
									<form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.TOP_5.getValue() %>"/>
									<span class="">Top 5 By Net Change</span></li>
								<li>
									<form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.BOTTOM_5.getValue() %>"/>
									<span class="">Bottom 5 By Net Change</span></li>
								<li>
									<form:radiobutton path="limitType" class="defaultLimitType"  value="<%= BaseChartModelBean.FirmDataType.FIRM.getValue() %>"/>
									<span class="">Selected Firms:</span> 
									<form:select path="searchResultsFirmList" id="searchResultsPercentChangeFirmList" multiple="true" size="4" style="width:150px; margin:10px 0 0 0;">
										<form:options items="${attorneySearchResults}" itemLabel="firmName" itemValue="firmId"/>
									 </form:select>
								</li>
							</ul>
							<div class="clear">&nbsp;</div>
						</form>
					</div>
					<div class="section-three">
						<div class="martp2">
							<h6>Comparison Data</h6>
							<div class="marbtm2 martp1">
								<!-- As per client requirement this options are hide. -->
								<p class="marbtm1" style="display: none;">
								<c:choose>
									<c:when test="${doIHaveFirm}">
										<form:checkbox path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.MY_FIRM.getValue() %>"/>
										<span>My Firm</span>
									</c:when>
									<c:otherwise>
										<form:checkbox path="comparisonDataTypeList" disabled="true" value="<%= BaseChartModelBean.ComparisonDataType.MY_FIRM.getValue() %>"/>
										<span title="You are not associated with any firm.">My Firm</span>
									</c:otherwise>
								</c:choose>
								</p>
								<p class="marbtm1">
									<form:checkbox class="percentChangeRankingCheckAPG" path="comparisonDataTypeList" value="<%=BaseChartModelBean.ComparisonDataType.RIVAL_EDGE.getValue()%>"/>
                                     <form:select id="percentChangeRankingListAPG" path="firmList" multiple="false">
										<form:options items="${allRankingList}"/>
									 </form:select>
								</p>
								<p class="marbtm1-last">
								<c:choose>
									<c:when test="${not empty allWatchLists}">
										<form:checkbox path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.WATCHLIST_AVG.getValue() %>" />
	                                 	<span>Watchlist</span>
	                               		<form:select path="watchList" multiple="true" size="4">
	                           		 		<form:options items="${allWatchLists}" itemValue="groupId" itemLabel="groupName"/>
										</form:select>
									</c:when>
									<c:otherwise>
										<form:checkbox  path="comparisonDataTypeList" disabled="true" value="<%= BaseChartModelBean.ComparisonDataType.WATCHLIST_AVG.getValue() %>" />
	                                 		<span title="You do not have any watchlists set up.">Watchlist</span>
									</c:otherwise>
								</c:choose>
                                 	
                                </p>
                                <p class="marbtm1-last">
                                   <form:checkbox class="percentChangeAverageValueAPG" path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.AVERAGE.getValue() %>" />
                                    Average of Firms in Search </p>
							</div>
						</div>
						<div class="clear">&nbsp;</div>
					</div>
					<div class="clear">&nbsp;</div>
					<hr>
					<div class="btmdiv">
						<input type="button" value="Reset All" class="buttonTwo flLeft settingReset">
						<input type="button" class="buttonTwo flRight rightReset cancelAPG" value="Cancel">
						<input type="button" value="Apply" class="buttonOne flRight" id="percentChangeAttorneyApply" style="margin: 0 5px 0 0;" onclick="applyChartSettings('#attorneysByPercentGradYearChartModelBean');">
						<div class="clear">&nbsp;</div>
					</div>
				</div>
			</div>
		</form:form>
	
	<div id="container-graduation-percent" class="charts-spacing"></div>
</div>
</div>
