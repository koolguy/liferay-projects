<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"		uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %> 

<%@page import="com.alm.rivaledge.util.ALMConstants"%>

<portlet:defineObjects />

<head>
<title>ALM Rival Edge</title>
<link rel="shortcut icon" type="<%=renderRequest.getContextPath()%>/images/x-icon" href="images/favicon.ico">

<script src="/re-theme/js/jquery-1.9.1.js"></script>


</head>
<div class="printSec">
  <div class="topBar">
    <ul class="tplist flRight">
      <li><a href="#" onClick="window.print();">Print</a></li>
      <li><a href="#" onClick="window.close();">Close</a></li>
    </ul>
    <div class="clear">&nbsp;</div>
  </div>
  <div>
    <div class="dateSec flRight">Date Printed : ${printedDateStr} at ${formattedTime}</div>
   
    <h1 id="logo"><a href="#"><img src="<%=renderRequest.getContextPath()%>/images/logo1.png" width="154" height="27" alt="logo" /></a></h1>
    <div class="clear">&nbsp;</div>
  </div>
  <h4>People Attorney Search </h4>
  
  <!-- Adding search criteria in jsp -->
  
    <div class="flLeft resultsec"><strong>Showing ${attorneySearchModelBean.totalResultCount} of&nbsp;${attorneySearchModelBean.totalResultCount} Results</strong> 
  		 <span><em>(
          <c:choose>
			 <c:when test="${fn:length(attorneySearchModelBean.selectedFirms) gt 100}">
			 	${fn:substring(attorneySearchModelBean.selectedFirms, 0, 99)}..,
			 </c:when>
			 <c:otherwise>
			 	${attorneySearchModelBean.selectedFirms}, 
			 </c:otherwise>		        		  
		</c:choose>
		 <c:choose>
			 <c:when test="${fn:length(attorneySearchModelBean.selectedLocation) gt 100}">
			 	${fn:substring(attorneySearchModelBean.selectedLocation, 0, 99)}..,
			 </c:when>
			 <c:otherwise>
			 	${attorneySearchModelBean.selectedLocation}, 
			 </c:otherwise>		        		  
		</c:choose>
		 <c:choose>
			 <c:when test="${fn:length(attorneySearchModelBean.selectedPracticeArea) gt 100}">
			 	${fn:substring(attorneySearchModelBean.selectedPracticeArea, 0, 99)}..,
			 </c:when>
			 <c:otherwise>
			 	${attorneySearchModelBean.selectedPracticeArea}, 
			 </c:otherwise>		        		  
		</c:choose>
			${attorneySearchModelBean.selectedTitles}, 
			<c:choose>
	      		<c:when test="${fn:length(attorneySearchModelBean.fromDate) gt 0}">
	      			${attorneySearchModelBean.fromDate}
	      		</c:when>
	      		<c:otherwise>
	      			<%=ALMConstants.ANY%> 
	      		</c:otherwise>
			</c:choose>
			<c:choose>
	      		<c:when test="${fn:length(attorneySearchModelBean.toDate) gt 0}">
	      			${attorneySearchModelBean.toDate}
	      		</c:when>
	      		<c:otherwise>
	      			-<%=ALMConstants.ANY%> 
	      		</c:otherwise>
			</c:choose>
			<c:if test="${fn:length(attorneySearchModelBean.lawSchools) gt 0}">
				,${attorneySearchModelBean.lawSchools}
			</c:if> 
			<c:if test="${fn:length(attorneySearchModelBean.admissions) gt 0}">
				,${attorneySearchModelBean.admissions}
			</c:if>
		 	<c:if test="${fn:length(attorneySearchModelBean.name) gt 0}">
				,${attorneySearchModelBean.name}
			</c:if>
			<c:if test="${fn:length(attorneySearchModelBean.keywords) gt 0}">
				,${attorneySearchModelBean.keywords}
			</c:if>
          )</em>
          </span>
          </div>
  	<div class="clear">&nbsp;</div>
  	<table width="100%" border="0" cellspacing="0" cellpadding="10" class="tbleTwo" id="attorneySearchResultsTable">
    <tr>
	  	<th>Attorney</th>
      	<th id="firColumnHeading">Firm</th>
      	<th id="titColumnHeading">Title</th>
      	<th id="locColumnHeading">Location</th>
      	<th id="praColumnHeading">Practice(s)</th>
      	<th id="eduColumnHeading">Education</th>
      	<th id="AdmColumnHeading">Admissions</th>
      	<th id="bioColumnHeading">Bio Text</th>
      	<th id="conColumnHeading">Contact Info</th>
    </tr>
    
    <c:choose>
      		<c:when test="${fn:length(attorneyPrintSearchResults) gt 0}">
      			 <c:forEach items="${attorneyPrintSearchResults}" var="attorneyResultsDTO" varStatus="i">
      			 
      			 	<c:choose>
						<%-- For first row in Table --%>
   						 <c:when test="${(i.first) && (attorneySearchModelBean.orderBy == 5)}">
     						    <tr class="oddone">
	      		 					<td colspan="10"><div class="padtp1 padlt1 padbtm1"><strong>${attorneyResultsDTO.firmName}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(i.first) && (attorneySearchModelBean.orderBy == 7)}">
     						    <tr class="oddone">
	      		 					<td colspan="10"><div class="padtp1 padlt1 padbtm1"><strong>${attorneyResultsDTO.title}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 <c:when test="${(i.first) && (attorneySearchModelBean.orderBy == 8)}">
     						    <tr class="oddone">
	      		 					<td colspan="10"><div class="padtp1 padlt1 padbtm1"><strong>${attorneyResultsDTO.location}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(i.first) && (attorneySearchModelBean.orderBy == 9)}">
     						    <tr class="oddone">
	      		 					<td colspan="10"><div class="padtp1 padlt1 padbtm1"><strong>${attorneyResultsDTO.practices}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 <%-- For subsequent rows in Table only when there is change in 2 consecutive records as they are already sorted at DB level--%>
   						 <c:when test="${(i.index > 0) && (attorneySearchModelBean.orderBy == 5) && (attorneyPrintSearchResults[i.index - 1].firmName != attorneyPrintSearchResults[i.index].firmName)}">
     						    <tr class="oddone">
	      		 					<td colspan="10"><div class="padtp1 padlt1 padbtm1"><strong>${attorneyResultsDTO.firmName}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(i.index > 0) && (attorneySearchModelBean.orderBy == 7) && (attorneyPrintSearchResults[i.index - 1].title != attorneyPrintSearchResults[i.index].title)}">
     						    <tr class="oddone">
	      		 					<td colspan="10"><div class="padtp1 padlt1 padbtm1"><strong>${attorneyResultsDTO.practices}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 <c:when test="${(i.index > 0) && (attorneySearchModelBean.orderBy == 8) && (attorneyPrintSearchResults[i.index - 1].location != attorneyPrintSearchResults[i.index].location)}">
     						    <tr class="oddone">
	      		 					<td colspan="10"><div class="padtp1 padlt1 padbtm1"><strong>${attorneyResultsDTO.location}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(i.index > 0) && (attorneySearchModelBean.orderBy == 9) && (attorneyPrintSearchResults[i.index - 1].practices != attorneyPrintSearchResults[i.index].practices)}">
     						    <tr class="oddone">
	      		 					<td colspan="10"><div class="padtp1 padlt1 padbtm1"><strong>${attorneyResultsDTO.practices}</strong></div></td>
	      		 				</tr>
   						 </c:when>
   						 
				</c:choose>
      			 	
      			 	<tr>
				      <td>${attorneyResultsDTO.attorneyName}</td>
				      <td id="firColumn_${attorneyId}">${attorneyResultsDTO.firmName}</td>
				      <td id="titColumn_${attorneyId}">${attorneyResultsDTO.title}</td>
				      <td id="locColumn_${attorneyId}">${attorneyResultsDTO.location}</td>
				      <td id="praColumn_${attorneyId}">${attorneyResultsDTO.practices}</td>
				      <td id="eduColumn_${attorneyId}">${attorneyResultsDTO.education}</td>
				      <td id="AdmColumn_${attorneyId}">${attorneyResultsDTO.admission}</td>
				      <td id="bioColumn_${attorneyId}">${attorneyResultsDTO.keywords}</td>
				      <td id="conColumn_${attorneyId}">${attorneyResultsDTO.contactInfo}</td>
				    </tr>
      			 </c:forEach>
      		</c:when>
    </c:choose>   			 
  </table>
</div>

<script type="text/javascript">
	
	var cols = '${attorneySearchModelBean.displayColumns}';
	cols = cols.toString();
	//alert("cols::"+cols);
	
	function hideColumns()
	{
		if(cols.indexOf("firm") == -1)
		{
			$("[id*='firColumn']").addClass('hideClass');
		}
		if(cols.indexOf("title") == -1)
		{
			$("[id*='titColumn']").addClass('hideClass');
		}
		if(cols.indexOf("location") == -1)
		{
			$("[id*='locColumn']").addClass('hideClass');
		}
		if(cols.indexOf("practice") == -1)
		{
			$("[id*='praColumn']").addClass('hideClass');
		}
		if(cols.indexOf("education") == -1)
		{
			$("[id*='eduColumn']").addClass('hideClass');
		}
		if(cols.indexOf("admission") == -1)
		{
			$("[id*='AdmColumn']").addClass('hideClass');
		}
		if(cols.indexOf("bio_text") == -1)
		{
			$("[id*='bioColumn']").addClass('hideClass');
		}
		if(cols.indexOf("contact_info") == -1)
		{
			$("[id*='conColumn']").addClass('hideClass');
		}
		
	}
	
	$(document).ready(function()
	{
		hideColumns();
	});
	
</script>

<script type="text/javascript">
		//Omniture SiteCatalyst - START
		s.prop22 = "premium";
		s.pageName="rer:attorney-search-printAttorney";
		s.channel="rer:attorney-search";
		s.server="rer";
		s.prop1="attorney-search";
		s.prop2="attorney-search";
		s.prop3="attorney-search";
		s.prop4="attorney-search";
		s.prop21=s.eVar21='current.user@foo.bar';	
		s.events="event2";
		s.events="event27";
		s.t();
// Omniture SiteCatalyst - END

</script>

