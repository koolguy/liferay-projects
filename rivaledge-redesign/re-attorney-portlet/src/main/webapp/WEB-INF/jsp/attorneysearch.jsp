<%@page import="com.alm.rivaledge.transferobject.FirmSearchDTO"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.alm.rivaledge.util.ALMConstants"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>

<%@ taglib prefix="portlet" 		uri="http://java.sun.com/portlet_2_0"%> 
<%@ taglib prefix="liferay-util"    uri="http://liferay.com/tld/util"%>
<%@ taglib prefix="spring"          uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form"            uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="liferay-portlet" uri="http://liferay.com/tld/portlet"%>
<%@ taglib prefix="c" 			 	uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="liferay-ui"   	uri="http://liferay.com/tld/ui"%>

<portlet:defineObjects/>

<!-- Custom css for Dynatree -->
<style>

ul.dynatree-container li
{
clear:left;
}

span.dynatree-icon
{
display: none !important;
}


ul.dynatree-container
{
border: 1px solid #CCCCCC !important;
padding: 2px 0 5px !important;
height: 200px  !important;
overflow: scroll !important; 
}

</style>

<%@ include file="./common.jsp" %>

<script src="<%=renderRequest.getContextPath()%>/js/jquery-ui.custom.js" type="text/javascript"></script>
<script src="<%=renderRequest.getContextPath()%>/js/jquery.cookie.js" type="text/javascript"></script>
<script src="<%=renderRequest.getContextPath()%>/js/jquery.dynatree.js" type="text/javascript"></script>

<link href="<%=renderRequest.getContextPath()%>/css/ui.dynatree.css" rel="stylesheet" type="text/css">



<portlet:actionURL var="changeSearchCriteria">
	<portlet:param name="action" value="changeSearchCriteria"/>
</portlet:actionURL>

<portlet:actionURL var="clickToViewURL">
	<portlet:param name="action" value="clickToView"/>
</portlet:actionURL>

<portlet:resourceURL var="persistSearchCriteriaForChartURL" id="persistSearchCriteriaForChart">
</portlet:resourceURL>

<portlet:resourceURL var="attorneySearch" id="attorneySearch">
</portlet:resourceURL>

<portlet:resourceURL var="attorneyLawSchool" id="attorneyLawSchool">
</portlet:resourceURL>

<portlet:resourceURL var="attorneyAdmissions" id="attorneyAdmissions">
</portlet:resourceURL>

<portlet:resourceURL var="attorneyKeywords" id="attorneyKeywords">
</portlet:resourceURL>

<portlet:resourceURL var="updateModelBean" id="updateModelBean">
</portlet:resourceURL>

<portlet:resourceURL var="fetchLocations" id="fetchLocations">
</portlet:resourceURL>     

<%--
This script tag contains functions related to Attorney Search actions 
 It has only function definitions and no jQuery hooks or handlers.
Please make sure you have all the function definitions at single place and loaded first
--%>
<script type="text/javascript">
	
	/* View Settings UI logic*/
	
	var displayColumnsChanged = false;
	var groupByValuesChanged = false;
	var resultsPerPageValuesChanged = false;
	var locJson=${LocationJsonTree};
	
	$(document).ready(function(){
		
		$("#attorneySearchModelBean input, #attorneySearchModelBean select, #attorneySearchModelBean span").each(function(){
			
			var idVal = $(this).attr("id");
			 $(this).attr("id","<portlet:namespace/>" + idVal );
		});	 	 
		
		
		  
	//Initialize Location Tree	
 	$("#<portlet:namespace/>locationTree").dynatree({
			  persist: true,
			  checkbox: true,
			  selectMode: 3,
			  onSelect: function(select, node) {
			     
			        var selLength=  $("#<portlet:namespace/>locationTree").dynatree("getTree").getSelectedNodes().length;
			        if(selLength>0)
			        {
			        	$("#<portlet:namespace/>allOtherLocations").prop('checked',true);
			        }
			        else
			        {
			        	$("#<portlet:namespace/>allLocations").prop('checked',true);
			        }
			        // Get a list of all selected nodes, and convert to a key array:
			        var selKeys = $.map(node.tree.getSelectedNodes(), function(node){			        	
			        	if(node.data.children==null){
			        		return node.data.title;
			        		}	  	
			          
			        });
			        var selectedval= selKeys.join(", ");
			        $("#<portlet:namespace/>selectedLocation").val(selectedval);

			        // Get a list of all selected TOP nodes
			        var selRootNodes = node.tree.getSelectedNodes(true);
			        // ... and convert to a key array:
			        var selRootKeys = $.map(selRootNodes, function(node){
			          return node.data.key;
			        });
			        $("#echoSelectionRootKeys3").text(selRootKeys.join(", "));
			        $("#echoSelectionRoots3").text(selRootNodes.join(", "));
			      },
		      onPostInit: function(isReloading, isError) {
		         logMsg("onPostInit(%o, %o)", isReloading, isError);
		         // Re-fire onActivate, so the text is update
		         this.reactivate();
		      },
		      onActivate: function(node) {
		       // $("#echoActive").text(node.data.title);
		    	  var selNodes = node.tree.getSelectedNodes();
		    	 
		          // convert to title/key array
		          var selKeys = $.map(selNodes, function(node1){
		              //alert("Title: '" + node1.data.title + "'");
		          });
		      },
		      onDeactivate: function(node) {
		        //$("#echoActive").text("-");
		      },
		      onDblClick: function(node, event) {
		        logMsg("onDblClick(%o, %o)", node, event);
		        node.toggleExpand();
		      },
		      children:locJson
		      
		    });  		
		//$("#<portlet:namespace/>popupLocation").dynatree("getTree").selectKey("Aberdeen");
		//$("#<portlet:namespace/>popupLocation").dynatree("getTree").getNodeByKey("Agra").select();
		$("#<portlet:namespace/>locationTree").dynatree("getTree").reload();
		
		//Type ahead for Law School field
		$("#<portlet:namespace/>selectedLawSchool").autocomplete({
			  source: function(request, response){
			    $.post('<%=attorneyLawSchool.toString()%>', {lawSchoolText:$("#<portlet:namespace/>selectedLawSchool").val()}, function(data){     
			        response($.map(data, function(item){
			        return {
			            label: item
			        }
			        }))
			    }, "json");
			  },
			  minLength: 3,
			  dataType: "json",
			  cache: false,
			  focus: function(event, ui) {
			    return false;
			  },
			  select: function(event, ui) {
			    this.value = ui.item.label;
			    return false;
			  }
		});
		
		//Type ahead for Admissions field
		$("#<portlet:namespace/>selectedAdmissions").autocomplete({
			  source: function(request, response){
			    $.post('<%=attorneyAdmissions.toString()%>', {admissions:$("#<portlet:namespace/>selectedAdmissions").val()}, function(data){     
			        response($.map(data, function(item){
			        return {
			            label: item
			        }
			        }))
			    }, "json");
			  },
			  minLength: 3,
			  dataType: "json",
			  cache: false,
			  focus: function(event, ui) {
			    return false;
			  },
			  select: function(event, ui) {
			    this.value = ui.item.label;
			    return false;
			  }
		});
		
		
		//Type ahead for Keywords field
		$("#<portlet:namespace/>keywordsInBio").autocomplete({
			  source: function(request, response){
			    $.post('<%=attorneyKeywords.toString()%>', {keyword:$("#<portlet:namespace/>keywordsInBio").val()}, function(data){     
			        response($.map(data, function(item){
			        return {
			            label: item
			        }
			        }))
			    }, "json");
			  },
			  minLength: 3,
			  dataType: "json",
			  cache: false,
			  focus: function(event, ui) {
			    return false;
			  },
			  select: function(event, ui) {
			    this.value = ui.item.label;
			    return false;
			  }
		}); 
		
	});
	
	/*
	* User has changed the Display Columns selection
	* Push them to hidden field that will be send to server on ajax call
	*/
	function changeOnDisplayColumns()
	{
		displayColumnsChanged = true;
		var displayColumnsList = [];
		$("input[name=displayColumns]:checked").each(function(){
			displayColumnsList.push($(this).val());
		});
		
		$("#<portlet:namespace/>displayColumns_hidden").val(displayColumnsList.join(";"));
		//alert($("#displayColumns_hidden").val());
	}
	
	/* Tracks if User changed Group By values
	 *
	 */
	function changeOnGroupByValues()
	{
		groupByValuesChanged = true;
	}
	
	/* Tracks if User changed Results per page values
	 *
	 */
	function changeOnResultsPerPageValues()
	{
		resultsPerPageValuesChanged = true;
	}
	
	/*
	* Apply view settings
	*/
	function applyViewSettings() 
	{
	
		if(!groupByValuesChanged && !resultsPerPageValuesChanged) // don't hit server as user changed only displayColumns
		{
			toggleSearchTableColumns();
			
			//Ajax call to server for setting display columns to get those columns in print page
			$.ajax({
			    url:"<%=updateModelBean.toString()%>",
			    method: "POST",
			    data: 
			    {
			    "displayColumns":$("#<portlet:namespace/>displayColumns_hidden").val()
			    },
			    success: function(data)
		        {
		    	
		        },
			   	error: function(jqXHR, textStatus, errorThrown) 
			   	{
			    	alert("error:" + textStatus + " - exception:" + errorThrown);
				}
			});
		}
		else
		{
			$(".attorneysearch").hide();  
			search(); //user changed something else we need fresh data from server
		}
		
		displayColumnsChanged = false;
		groupByValuesChanged = false;		
		resultsPerPageValuesChanged = false;
		$(".attorneysearch").toggle();  
	}
	
	function toggleViewSettings(vsId)
	{
		
		//get the position of the placeholder element
		var vsPos   = $(vsId).offset();
	   	var vsHeight = $(vsId).height();
	   	var vsWidth = $(vsId).width();
	    //show the menu directly over the placeholder
	    var popupWidth = $(".attorneysearch").width();
	    $(".attorneysearch").css({  position: "absolute", "left": (vsPos.left - popupWidth + vsWidth) + "px", "top":(vsPos.top + vsHeight)  + "px" });
	    
	    $(".attorneysearch").toggle();
	}	
	
	function cancelViewSettings()
	{
		$(".attorneysearch").toggle(); 
	}
	
	//reset view setting popup to default state
	function resetViewSettings() {
		//clear all
		$('.attorneysearch').find('input[type=checkbox]:checked').removeAttr('checked');
		$('.attorneysearch').find('input[type=radio]:checked').removeAttr('checked');

		//select interested fields
		$('input:radio[name=orderBy]:nth(1)').prop('checked',true);  //Firm name, the 2nd radiobutton
		groupByValuesChanged = true;
		$('input:radio[name=searchResultsPerPage]:nth(2)').prop('checked',true);  //100, the 3rd radiobutton
		resultsPerPageValuesChanged = true;	
		for(var i = 0; i < 5 ; i++) //only first 5 columns shud be visible
		{
			$('input:checkbox[name=displayColumns]:nth('+ i +')').prop('checked',true);
		}
		changeOnDisplayColumns(); //notify that displayColumns are changed, to refresh the displayColumns_hidden field
		//$(".attorneysearch").stop().slideToggle(500); //Keep user to that popup view and let user click on Apply
	}
	
	
	/*
	* Hide or display columns based on user selection
	* This will be invoked as and when 1. The search results are rendered
	*								   2. User changes the display columns and apply on view settings popup
	*/
	function toggleSearchTableColumns()
	{
		var columnIndex = 1;  //Exclude first 2 columns 2 = Attorney name
		
		$("input[name=displayColumns]").each(function(){
			if($(this).is(":checked"))
				{
					$("#attorneySearchResultsTable th:nth-child(" + (columnIndex+1) + "), #attorneySearchResultsTable td:nth-child(" + (columnIndex+1) + ")").show();
				}
			else
				{
					$("#attorneySearchResultsTable th:nth-child(" + (columnIndex+1) + "), #attorneySearchResultsTable td:nth-child(" + (columnIndex+1) + ")").hide();
				}
			columnIndex++;
		});
	}
	
	
	/**
	*Autoselect previously selected search critria
	*/
	function <portlet:namespace/>initializeSearchCriteria()
	{
		<portlet:namespace/>applyFirms();
		//<portlet:namespace/>applyLocations();
		<portlet:namespace/>applyPracticeArea();
		<portlet:namespace/>applyTitles();
		<portlet:namespace/>refreshCounters();
	}
	
	//Apply Buttons functions
	function <portlet:namespace/>applyFirms()
	{
		//alert('INSIDE applyFirm() ');
		var checkFlag = $("#<portlet:namespace/>allfirms").is(":checked");
		var valueofchecked =$("input[name='firmType']:checked").val();
		if(valueofchecked=="<%=ALMConstants.ALL_FIRMS%>"){		
			checkFlag=true;
			 $('#<portlet:namespace/>allfirms').prop("checked", true);
		}
		
		var allSelectedValue = [];
		var allSelectedIds = [];
		
		 $("#<portlet:namespace/>Firmstext").val('');
		 $("#<portlet:namespace/>selectedFirms").val('');
		 
		var isFirmChecked = false;
		var allWatchListCounter = 0;
		var counter = 0;
		var allFirmsCounter = 0
	
		
			$('#homePageShowAttorney .individualfirmsWatchList input[type=checkbox]:checked').each(function() {
				
				 allSelectedValue.push($(this).attr('labelAttr'));
				 allSelectedIds.push($(this).val());
				 $("#<portlet:namespace/>firm_watchlist").prop("checked", true);
				 $('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
				 
				 var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
					
					$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]').each(function() {
						 if(this.value == watchListId)
							{
								$(this).prop("checked", true);
							}
					});
					 //$('#firm_watchlist').prop("checked", true);
				 
				 $('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').each(function() 
					{
							allWatchListCounter++;
					});
							
					$("#homePageShowAttorney .individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList"/> Watchlists (' + allWatchListCounter +' Selected)');
		 		});
		 
		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
			
			
			 allSelectedValue.push($(this).attr('labelAttr'));
			 allSelectedIds.push($(this).val());
			 $("#<portlet:namespace/>allOtherFirm").prop("checked", true);
			 $("#homePageShowAttorney .rivaledgeListAMLAW_100").prop("checked", false);
			 $("#homePageShowAttorney .rivaledgeListAMLAW_200").prop("checked", false);
			 $("#homePageShowAttorney .rivaledgeListNLJ_250").prop("checked", false);
	 	});
		
		if(allSelectedValue != ''  &&  allSelectedIds != '') 
			{
				$("#<portlet:namespace/>Firmstext").val(allSelectedValue.join(",")); //Use a comma separator for values which show up on UI
				$("#<portlet:namespace/>selectedFirms").val(allSelectedIds.join(";"));
			}
		else
		{
			//$("#allfirms, #firm_watchlist, #firm_individual").each(function(){
				$("#<portlet:namespace/>allfirms, .rivaledgeListAMLAW_100, .rivaledgeListAMLAW_200, .rivaledgeListNLJ_250").each(function(){
				
				if($("#<portlet:namespace/>allfirms").is(":checked"))
					{
					
						$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.ALL_FIRMS %>');
						$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.ALL_FIRMS %>');
						$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.ALL_FIRMS %>");
					} 
				else if($("#homePageShowAttorney .rivaledgeListAMLAW_100").is(":checked"))
				{
					
			
					$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.AMLAW_100 %>');
					$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.AMLAW_100 %>');
					$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
					$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
				} 
				else if($("#homePageShowAttorney .rivaledgeListAMLAW_200").is(":checked"))
				{
					
					$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.AMLAW_200 %>');
					$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.AMLAW_200 %>');
					$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
					$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
				} 
				else if($("#homePageShowAttorney .rivaledgeListNLJ_250").is(":checked"))
				{
					
					$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.NLJ_250 %>');
					$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.NLJ_250 %>');
					$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
					$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
				} 
				 else
					{
					
					 if($(this).is(":checked")){
							var value =$(this).val();
						  //$("#Firmstext").val(value);
						 	//$("#selectedFirms").val(value);
							$("#<portlet:namespace/>Firmstext").val('AMLAW 100');
							$("#<portlet:namespace/>selectedFirms").val('AMLAW 100');
							$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
						} 
					}
			
				
			});
		}
		
	}
	
	function <portlet:namespace/>applyLocations()
	{

		 if($("#<portlet:namespace/>allLocations").is(":checked"))
		 {
				$("#<portlet:namespace/>selectedLocation").val($("#<portlet:namespace/>allLocations").val());
		 } 
		
		 else
		 {
			 var allValsLocation = [];
			 var allLocationCounter = 0;
				$('#<portlet:namespace/>allOtherLocationsDiv input[type=checkbox]:checked').each(function() { 
					 allValsLocation.push($(this).val());
					 allLocationCounter++;
			    });
				$("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="<portlet:namespace/>selectAllLocations"/>  Locations ('+allLocationCounter+') Selected');

			if(allValsLocation.length> 0){
				$("#<portlet:namespace/>allOtherLocations").prop("checked", true);
			    $("#<portlet:namespace/>selectedLocation").val(allValsLocation.join(";"));
			    //alert($("#selectedLocation").val());
			}
			
		}
	}


	function <portlet:namespace/>applyPracticeArea()
	{
		
		if($("#homePageShowAttorney .allPracticeArea").is(":checked"))
		 {
				$("#<portlet:namespace/>selectedPracticeArea").val($("#homePageShowAttorney .allPracticeArea").val());			
		 } 
		 else
		 {
			var allValsPracticeArea = [];
			
			var allPracticeCounter = 0;
				
			
			$('#<portlet:namespace/>practiceAreaDiv input[type=checkbox]:checked').each(function() { 
				 	allValsPracticeArea.push($(this).val());
				 	allPracticeCounter++;
			 });
				 
			$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="<portlet:namespace/>selectAllPracticeAreas"/> PracticeAreas ('+allPracticeCounter+') Selected');
			
			if(allValsPracticeArea.length> 0)
			{
				$("#<portlet:namespace/>allPracticeAreaIndividual").prop("checked", true);
				$("#<portlet:namespace/>selectedPracticeArea").val(allValsPracticeArea.join(";"));		
			}
		}
	}
	
	
	function <portlet:namespace/>applyTitles()
	{
		
		var allValsTitles = [];
		 $('#<portlet:namespace/>titleSelectDiv :checked').each(function() {
			 allValsTitles.push($(this).val());
		    });
		 $("#<portlet:namespace/>selectedTitles").val(allValsTitles.join(";"));
		    
	}
	
	
	/* Clear Buttons functions*/
	
	function <portlet:namespace/>clearAll()
	{
		
		//alert('INSIDE CREAR ALL ');
		
		var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
		var isFlag = false;
		//Firms reset
		clearFirms();
		
		//Practice Area reset
		clearPracticeAreas();
		
		//Location reset
		clearLocations();
		//Titles reset
		
		clearTitles();
		
		//School graduation reset
		$('#<portlet:namespace/>graduationFrom').val('<%=ALMConstants.ANY%>'); 
		$('#<portlet:namespace/>graduationTo').val('<%=ALMConstants.ANY%>'); 
		
		//law school reset
		$('#<portlet:namespace/>selectedLawSchool').val(''); 

		//Admissions reset
		$('#<portlet:namespace/>selectedAdmissions').val('');
		
		//name reset
		$('#<portlet:namespace/>name').val('');
		
		//keyword reset
		$('#<portlet:namespace/>keywordsInBio').val(''); 
		
		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]').each(function() {
			 if(this.value == watchListId)
				{
					$(this).prop("checked", true);
					$('#<portlet:namespace/>firm_watchlist').prop("checked", true);
					$("#<portlet:namespace/>Firmstext").val($(this).attr('labelattr')); 
					$("#<portlet:namespace/>selectedFirms").val($(this).val());
					$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.WATCH_LIST %>');
					isFlag = true;
				}
		});
		
		if(isFlag == false)
		{
			$('#<portlet:namespace/>firm_watchlist').prop("checked", true);
			$('#homePageShowAttorney .rivaledgeListAMLAW_100').prop("checked", true);
			$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.AMLAW_100 %>');
			$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.AMLAW_100 %>');
			$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
		}
	}
	
function clearFirms()
{
	
	$('#<portlet:namespace/>popup').find("option").attr("selected", false);
	$('#<portlet:namespace/>popup').find('input[type=checkbox]:checked').removeAttr('checked');
	$('#<portlet:namespace/>popup').find('input[type=radio]:checked').removeAttr('checked');
	
	$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/>  Firms (0) Selected');

	$("#<portlet:namespace/>RivalEdgeList").prop("checked", true);
	$('#<portlet:namespace/>Select1').val('<%=ALMConstants.AMLAW_100%>');
	<portlet:namespace/>applyFirms();
}

function clearLocations()
{
	//$('#<portlet:namespace/>popupLocation').find('input[type=checkbox]:checked').removeAttr('checked');
	$('#<portlet:namespace/>popupLocation').find('input[type=radio]:checked').removeAttr('checked');
	//$("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="<portlet:namespace/>selectAllLocations"/>  Locations (0) Selected');
	$("#<portlet:namespace/>locationTree").dynatree("getRoot").visit(function(node){
			    node.expand(false);
			    node.select(false);
	});
	$('#<portlet:namespace/>allLocations').prop("checked", true);
	<portlet:namespace/>applyLocations();
}


function clearPracticeAreas()
{
	$('#<portlet:namespace/>popupPracticeArea').find('input[type=checkbox]:checked').removeAttr('checked');
	$('#<portlet:namespace/>popupPracticeArea').find('input[type=radio]:checked').removeAttr('checked');
	$('#<portlet:namespace/>allOtherPracticeArea input').find('input[type=checkbox]:checked').removeAttr('checked');
	$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="<portlet:namespace/>selectAllPracticeAreas"/> Practice Areas (0) Selected');
	$('#homePageShowAttorney .allPracticeArea').prop('checked', true);	
	<portlet:namespace/>applyPracticeArea();
}

function clearTitles()
{
	$('#<portlet:namespace/>titleSelectDiv').find('input[type=checkbox]:checked').removeAttr('checked');
	$('#<portlet:namespace/>allTitles').prop("checked", true);
	<portlet:namespace/>applyTitles();
}
	
/*Refresh counter values*/
	
function <portlet:namespace/>refreshCounters()
{
	var allFirmsCounter = 0;
	$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
	 allFirmsCounter++;
	});
	$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/>  Firms ('+allFirmsCounter+') Selected');
	
	var allLocationCounter = 0;
	 $('#<portlet:namespace/>allOtherLocationsDiv input[class=allLocationsCheckBox]:checked').each(function() {
		 allLocationCounter++;
	    });
	$("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="<portlet:namespace/>selectAllLocations"/>  Locations ('+allLocationCounter+') Selected');

	var allPracticeCounter = 0;
	$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:checked').each(function() {
		 allPracticeCounter++;
	    });
	$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="<portlet:namespace/>selectAllPracticeAreas"/> PracticeAreas ('+allPracticeCounter+') Selected');
}

	
	
	/* Change on individual checkboxes(select/deselct) functions*/
	
	function changeOnIndividualLocations()
	{
		var allLocationsCounter = 0;
		 $('#<portlet:namespace/>allOtherLocationsDiv :checked').each(function() {
			 allLocationsCounter++;
		    });
				$("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="<portlet:namespace/>selectAllLocations"/>  Locations ('+allLocationsCounter+') Selected');
		 
			if(allLocationsCounter == 0)
			{
				$('#<portlet:namespace/>allOtherLocations').prop("checked", false);
				$('#<portlet:namespace/>allLocations').prop("checked", true);
				
			}
			else
			{
				$('#<portlet:namespace/>allOtherLocations').prop("checked", true);
				$('#<portlet:namespace/>allLocations').prop("checked", false);
			}
					
	}

	function changeOnIndividualPracticeAreas()
	{
		
			var allPracticeCounter = 0;
		
			$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:checked').each(function() {
				 allPracticeCounter++;
		    });
			
			$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="<portlet:namespace/>selectAllPracticeAreas"/> PracticeAreas ('+allPracticeCounter+') Selected');
			
			if(allPracticeCounter == 0)
			{
				$('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", false);
				$('#homePageShowAttorney .allPracticeArea').prop("checked", true);
			}
			else
			{
				$('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", true);
				$('#homePageShowAttorney .allPracticeArea').prop("checked", false);
			}
	}

	
	
	/* Search Actions' functions*/
	
	function ajaxSubmit()
	{
		
		//$("#searchForm").mask("Waiting...");
		 $.ajax({
			    url: "${applySearchUrl}",
			    method: "GET",
			    data: {
			    	"firmType":$("input[name=firmType]:checked").val(),
		    		"firmList":$("#<portlet:namespace/>selectedFirms").val(),
			    	"locations":$("#<portlet:namespace/>selectedLocation").val(),
			    	"practiceArea":$("#<portlet:namespace/>selectedPracticeArea").val(),
			    	"title":$("#<portlet:namespace/>selectedTitles").val(),
			    	"fromDate":$("#<portlet:namespace/>graduationFrom").val(),
			    	"toDate":$("#<portlet:namespace/>graduationTo").val(),
			    	"lawSchools":$("#<portlet:namespace/>selectedLawSchool").val(),
			    	"admissions":$("#<portlet:namespace/>selectedAdmissions").val(),
			    	"name":$("#<portlet:namespace/>name").val(),
			    	"keywords":$("#<portlet:namespace/>keywordsInBio").val(),
			    	"displayColumns" : $("#<portlet:namespace/>displayColumns_hidden").val(),
			    	"goToPage":$("#<portlet:namespace/>goToPage").val(),
			    	"sortColumn":$("#<portlet:namespace/>sortColumn").val(),
				    "sortOrder":$("#<portlet:namespace/>sortOrder").val(),
			    	"orderBy" :$("input[name=orderBy]:checked").val(),
			    	"searchResultsPerPage" :$("input[name=searchResultsPerPage]:checked").val()
			    	},
			    success: function(data){
			    		
			    	$("#attorneyResultDiv").html(data);
			    	$('body').removeClass("modal");
			        },
			    error: function(jqXHR, textStatus, errorThrown) {
			           // alert("error:" + textStatus + " - exception:" + errorThrown);
			   		}
			    }); 
	}

	function search()
	{

		
		var isC2VPage = ${isC2VPage};  // its always gauranteed it either evaluates to true or false, but not empty
		var isHomePage = ${isHomePage};
		
		if(isC2VPage)
		{
			
			var actionURL = "${clickToViewURL}" +  "&drilldownFirmName=blah"; // we care the existence of param not its value;
			$('#attorneySearchModelBean').attr("action", actionURL);
			//$('#eventSearchModelBean').attr("name", "eventSearchModelBean_c2v");
		}
		
		if(isHomePage)
		{
			<portlet:namespace/>ajaxPersist(); // ajaxSubmit for Charts on HomePage
		}
		else
		{
			Liferay.Portlet.showBusyIcon("#bodyId","Loading..."); // show Busy Icon
			$('#attorneySearchModelBean').submit();
			
		}
	}
	
	/**
	* Persist the search Criteria for Chart on Home Page
	* All this stuff is done in an iframe
	*/
	function <portlet:namespace/>ajaxPersist()
	{
		window.parent.Liferay.Portlet.showBusyIcon("#chartSearchPortletPopupId","Loading Search Criteria...");
		 $.ajax({
			    url: "${persistSearchCriteriaForChartURL}",
			    method: "POST",
			    traditional: true,
			    data: $('#attorneySearchModelBean').serializeObject(),
			    success: function(data){
			    	
			    	window.parent.Liferay.Portlet.hideBusyIcon("#chartSearchPortletPopupId"); // hide popup loading icon
			    	window.parent.location.href = "home"; // refresh home page
			    	window.parent.Liferay.Portlet.showBusyIcon("#bodyId","Loading..."); // show the loading icon
			    	window.parent.AUI().DialogManager.closeByChild('#chartSearchPortletPopupId'); // close the popup
			    //	Liferay.Portlet.refreshPortlet('#p_p_id_${chartPortletId}_'); 
			        
			    },
			    error: function(jqXHR, textStatus, errorThrown) {
			    	window.parent.Liferay.Portlet.hideBusyIcon("#chartSearchPortletPopupId");
			         	 alert("error:" + textStatus + " - exception:" + errorThrown);
			   		}
			    }); 
	}
	
	
	
	function setPage(goToPage)
	{
		// Sets the page number to the one selected by the user
		// and fires an AJAX submit to refresh with the new page
		// Does NOT tinker with the sort settings
		$("#<portlet:namespace/>goToPage").val(goToPage);
		search();
	}
	
	function sortResults(sortColumn)
	{
		// Changes the sort order to the new column selected by
		// the user along with the sort direction (ascending)
		// Also RESETS the page to 1		
		var lastSortColumn  =  $("#<portlet:namespace/>sortColumn").val();
		
		if(lastSortColumn==sortColumn)
		{
			var lastSortOrder= $("#<portlet:namespace/>sortOrder").val();
			if(lastSortOrder=="asc")
			{
				$("#<portlet:namespace/>sortOrder").val("desc");
			}
			else if(lastSortOrder=="desc")
			{
				$("#<portlet:namespace/>sortOrder").val("asc");
			}
			else
			{
				$("#<portlet:namespace/>sortOrder").val("asc");
			}
		}
		else
		{
			$("#<portlet:namespace/>sortOrder").val("asc");
			
		}
		
		$("#<portlet:namespace/>sortColumn").val(sortColumn);
		$("#<portlet:namespace/>goToPage").val(1);
		search();
	}
	
	
	function sortDesc(sortColumn)
	{
		// Changes the sort order to the new column selected by
		// the user along with the sort direction (descending)
		// Also RESETS the page to 1
		$("#<portlet:namespace/>sortColumn").val(sortColumn);
		$("#<portlet:namespace/>sortOrder").val("desc");
		$("#<portlet:namespace/>goToPage").val(1);
		search();
	}
	
</script>

<script>


$.widget( "custom.catcomplete", $.ui.autocomplete, {
	_renderMenu: function( ul, items ) {
		var that = this,
		currentCategory = "";
		$.each( items, function( index, item ) {
			if ( item.category != currentCategory ) {
			ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
			currentCategory = item.category;
			}
	  that._renderItemData( ul, item );
	});
}
});

  $(document).ready(function() {
  
	var data = ${firmJson};
	var practiceData = ${practiceJson};
	var titlesJson = ${titlesJson};
	//dont initialize here 
	//$("#RivalEdgeList").prop("checked", true);
	//$("#graduationTo option[value='2013']").attr('selected', 'selected');	


	var availableLocation = new Array();

	<c:forEach items="${allOtherLocations}" var="loc">
		availableLocation.push("${loc}");
	</c:forEach>

function split( val ) {
	return val.split( /,\s*/ );
}

function splitValuesApartFromFirms( val ) {
	return val.split( /;\s*/ );
}

function extractLast( term ) {
	return split( term ).pop();
}

function extractLastApartFromFirms( term ) {
	return splitValuesApartFromFirms( term ).pop();
}
  

$("#<portlet:namespace/>selectedLocation").bind( "keydown", function( event ) {
// don't navigate away from the field on tab when selecting an item
  if ( event.keyCode === $.ui.keyCode.TAB &&
      $( this ).data( "ui-autocomplete" ).menu.active ) {
    		event.preventDefault();
  		}
	}).autocomplete({
  		minLength: 0,
  		source: function( request, response ) {
    	// delegate back to autocomplete, but extract the last term
    	response( $.ui.autocomplete.filter(
    		availableLocation, extractLastApartFromFirms( request.term ) ) );
  		},
  		focus: function() {
    	// prevent value inserted on focus
    	return false;
  		}, select: function( event, ui ) {
    		var terms = splitValuesApartFromFirms( this.value );
    		// remove the current input
    		terms.pop();
    		// add the selected item
    		terms.push( ui.item.value );
    		// add placeholder to get the comma-and-space at the end
    		terms.push("");
    		this.value = terms.join(";");
    		//alert(this.value);
    		
    		$('#<portlet:namespace/>allOtherLocationsDiv input[type=checkbox]').each(function()
    		{ 
				if($(this).val() == (ui.item.value) )
				{
					$(this).prop("checked", true);
					changeOnIndividualLocations();
					return false;
				}
			});
    		return false;
  		}
	});


$( "#<portlet:namespace/>selectedPracticeArea" )
	//don't navigate away from the field on tab when selecting an item
	.bind( "keydown", function( event ) {
		if ( event.keyCode === $.ui.keyCode.TAB &&
		$( this ).data( "ui-autocomplete" ).menu.active ) {
		event.preventDefault();
		}
	})
	.autocomplete({
	minLength: 3,
	source: function( request, response ) {
		//delegate back to autocomplete, but extract the last term
		response( $.ui.autocomplete.filter(
			practiceData, extractLastApartFromFirms( request.term ) ) );
	},
	focus: function() {
		//prevent value inserted on focus
		return false;
	},
	select: function( event, ui ) {
		var terms = splitValuesApartFromFirms( this.value );
		//remove the current input
		terms.pop();
		//add the selected item
		terms.push( ui.item.value );
		//add placeholder to get the comma-and-space at the end
		terms.push( "" );
		this.value = terms.join( ";" );
		
		$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]').each(function()
   		{ 
			if($(this).val() == (ui.item.value) )
			{
				$(this).prop("checked", true);
				changeOnIndividualPracticeAreas();
				return false;
			}
		});
		
		return false;
	}
});



  $( "#<portlet:namespace/>Firmstext" ).catcomplete({
	minLength: 3,
	source: function( request, response ) {
		// delegate back to autocomplete, but extract the last term
		response( $.ui.autocomplete.filter(
		data, extractLast( request.term ) ) );
	},
	focus: function() {
		// prevent value inserted on focus
		return false;
	},
	change: function(event, ui) {		
		
		
		$('#<portlet:namespace/>popup').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>popup').find('input[type=radio]:checked').removeAttr('checked');
		
		var selectedValues = this.value.split(',');
		var output = '';
		var currentvalue="";		
		$.each(selectedValues, function(key, line) {
			line=line.trim();		
			if(line!=""){				
				currentvalue=$("input[labelattr='"+line+"'].allFirmsCheckBox").val();
				if(typeof(currentvalue)!="undefined"){
					output= output + currentvalue+ ",";
					$("input[labelattr='"+line+"'].allFirmsCheckBox").prop("checked", true);
					allOtherFirmChange();
				}	
							
			}			
		});		
		output = output.substring(0, output.length - 1);
		$('#selectedFirms').val(output);		
    },
	select: function( event, ui ) {
		var terms = split( this.value );		
		var termsValue = split( this.id );
		if( ui.item.value.indexOf("AmLaw 100") !== -1){
			return false;
		}
		
		// remove the current input
		terms.pop();
		termsValue.pop();
		// add the selected item
		
		terms.push( ui.item.value );
		termsValue.push( ui.item.id );
		// add placeholder to get the comma-and-space at the end
		terms.push( "" );
		termsValue.push( "" );
		this.value = terms.join( ", " );	
		$('#<portlet:namespace/>selectedFirms').val($('#<portlet:namespace/>selectedFirms').val().replace("AmLaw 100","") + "," + ui.item.id);
		
		$('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]').each(function()
   		{ 
			if($(this).val() == (ui.item.id) )
			{
				$(this).prop("checked", true);
				allOtherFirmChange();
				return false;
			}
		});
		
		return false;
	}
});  
  
  //Autocomplete for Titles
  $( "#<portlet:namespace/>selectedTitles" )
	//don't navigate away from the field on tab when selecting an item
	.bind( "keydown", function( event ) {
		if ( event.keyCode === $.ui.keyCode.TAB &&
		$( this ).data( "ui-autocomplete" ).menu.active ) {
		event.preventDefault();
		}
	})
	.autocomplete({
	minLength: 3,
	source: function( request, response ) {
		//delegate back to autocomplete, but extract the last term
		response( $.ui.autocomplete.filter(
				titlesJson, extractLastApartFromFirms( request.term ) ) );
	},
	focus: function() {
		//prevent value inserted on focus
		return false;
	},
	select: function( event, ui ) {
		var terms = splitValuesApartFromFirms( this.value );
		//remove the current input
		terms.pop();
		//add the selected item
		terms.push( ui.item.value );
		//add placeholder to get the comma-and-space at the end
		terms.push( "" );
		this.value = terms.join( ";" );
		$("#<portlet:namespace/>allTitles").prop("checked", false);
		$('#<portlet:namespace/>allOtherTitlesDiv   input[type=checkbox]').each(function()
		{ 
			if($(this).val() == (ui.item.value) )
			{
				$(this).prop("checked", true);
				return false;
			}
		});
		
		return false;
	}
});

  
  
  
	 $('#<portlet:namespace/>hide, #<portlet:namespace/>popup').click(function(e){
		 $("#<portlet:namespace/>popupPracticeArea").hide();		
		 $("#<portlet:namespace/>popupLocation").hide();
		 $("#<portlet:namespace/>popupTitles").hide();	
	     e.stopPropagation();	   
	});  

	

	$('#<portlet:namespace/>practiceAreaId, #<portlet:namespace/>popupPracticeArea').click(function(e){
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupLocation").hide();
		 $("#<portlet:namespace/>popupTitles").hide();	
	     e.stopPropagation();   
	});
	


	$('#<portlet:namespace/>locationId, #<portlet:namespace/>popupLocation').click(function(e){
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupPracticeArea").hide();
		 $("#<portlet:namespace/>popupTitles").hide();	
	     e.stopPropagation();   
	});

	
	$('#<portlet:namespace/>titleDropdown, #<portlet:namespace/>popupTitles').click(function(e){
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupPracticeArea").hide();
		 $("#<portlet:namespace/>popupLocation").hide();
	     e.stopPropagation();   
	});
	$(document).click(function(){
	    $("#<portlet:namespace/>popup").hide(); 
	    $("#<portlet:namespace/>popupPracticeArea").hide();
	    $("#<portlet:namespace/>popupLocation").hide();		 
	    $("#<portlet:namespace/>popupTitles").hide();	
	});
  
  
  
	$('#<portlet:namespace/>btnAdd').click(function(){
		$('#homePageShowAttorney .filtersPage').hide()
		$('#<portlet:namespace/>additional').show()
	})
	$('#<portlet:namespace/>btnSave').click(function(){
		$('#homePageShowAttorney .filtersPage').show()
		$('#<portlet:namespace/>additional').hide()
	});
	
	$('#<portlet:namespace/>hide').click(
    function () {
        //show its submenu
        $("#<portlet:namespace/>popup").stop().slideToggle(500);    
    });   
    
    $('#<portlet:namespace/>titleDropdown').click(
    	    function () {
    	        //show its submenu
    	        $("#<portlet:namespace/>popupTitles").stop().slideToggle(500);    
    	    });
	
	$('#<portlet:namespace/>locationId').click(
		    function () {
		        $("#<portlet:namespace/>popupLocation").stop().slideToggle(500);    
	});
	
	$('#<portlet:namespace/>practiceAreaId').click(
		function () {
		    $("#<portlet:namespace/>popupPracticeArea").stop().slideToggle(500);    
	});
	
	
	$("#<portlet:namespace/>allLocations").change(function(){
		var checked = $(this).is(":checked");
		if(checked){  		
		 //$('#<portlet:namespace/>allOtherLocationsDiv').find('input[type=checkbox]:checked').removeAttr('checked');	
		// $('#<portlet:namespace/>allOtherLocations').removeAttr('checked');	
		// $("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="<portlet:namespace/>selectAllLocations"/>  Locations (0) Selected');
			$("#<portlet:namespace/>locationTree").dynatree("getRoot").visit(function(node){
			    node.expand(false);
			    node.select(false);
			});
		}
		<portlet:namespace/>applyLocations();
	});
	
	$("#<portlet:namespace/>allOtherLocations").change(function(){
		var selLength=  $("#<portlet:namespace/>locationTree").dynatree("getTree").getSelectedNodes().length;
        if(selLength>0)
        {
        	$("#<portlet:namespace/>allOtherLocations").prop('checked',true);
        }
        else
        {
        	$("#<portlet:namespace/>allLocations").prop('checked',true);
        }		
		<portlet:namespace/>applyLocations();
	});
	
	/* $("#<portlet:namespace/>allOtherLocations, #<portlet:namespace/>allOtherLocationsDiv input[type=checkbox]").click(function(){
	
		changeOnIndividualLocations();
	}); */
	

	//UI Logic for Practice Area
	
	$("#homePageShowAttorney .allPracticeArea").change(function(){
		var checked = $(this).is(":checked");
		if(checked){  		
			 $('#<portlet:namespace/>allOtherPracticeArea').find('input[type=checkbox]:checked').removeAttr('checked');	
			 $('#<portlet:namespace/>allPracticeAreaIndividual').removeAttr('checked');	
			 $("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="<portlet:namespace/>selectAllPracticeAreas"/> PracticeAreas (0) Selected');
		}
		<portlet:namespace/>applyPracticeArea();
	});
	
	$("#<portlet:namespace/>allPracticeAreaIndividual, #<portlet:namespace/>practiceAreaDiv input[type=checkbox]").click(function(){
		changeOnIndividualPracticeAreas();
	});
	
	
	//UI Logic for Titles
	
	$("input[id='<portlet:namespace/>allTitles']").change(function(){
		var checked = $("#<portlet:namespace/>allTitles").is(":checked");
		if(checked){  		
			 $('#<portlet:namespace/>allOtherTitlesDiv').find('input[type=checkbox]:checked').removeAttr('checked');
		}
		<portlet:namespace/>applyTitles();
	});
	
	$("input[class='allTitlesCheckBox']").change(function(){
		var lengthofCheckBoxes = $("input[class='allTitlesCheckBox']:checked").length;
		if(lengthofCheckBoxes>0){
			$('#<portlet:namespace/>allTitles').prop("checked", false);
		}
	
		<portlet:namespace/>applyTitles();
	});
	
	$('#<portlet:namespace/>allfirms').bind('click', function (event) {
		$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.ALL_FIRMS %>');
		$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.ALL_FIRMS %>');
		$('#<portlet:namespace/>firm_watchlist').val('<%=ALMConstants.ALL_FIRMS %>');
		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>allOtherFirm').prop("checked", false);
		$('#<portlet:namespace/>selectAllFirms').prop("checked", false);
		$('#<portlet:namespace/>firm_watchlist').prop("checked", false);
		
	});
	
	$('#homePageShowAttorney .rivaledgeListAMLAW_100').bind('click', function (event) {
		
		$('#homePageShowAttorney .rivaledgeListAMLAW_100').prop("checked", true);
		$('#homePageShowAttorney .rivaledgeListAMLAW_200').prop("checked", false);
		$('#homePageShowAttorney .rivaledgeListNLJ_250').prop("checked", false);
		$('#<portlet:namespace/>firm_watchlist').prop("checked", true);
		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
	 	$("#homePageShowAttorney .individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList"/> Watchlists (0 Selected)');
	 	
	 	$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms (0) Selected');
		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
		$('#<portlet:namespace/>Firmstext').val("<%=ALMConstants.AMLAW_100 %>");
	});
	
	$('#homePageShowAttorney .rivaledgeListAMLAW_200').bind('click', function (event) {
		
		$('#homePageShowAttorney .rivaledgeListAMLAW_100').prop("checked", false);
		$('#homePageShowAttorney .rivaledgeListAMLAW_200').prop("checked", true);
		$('#homePageShowAttorney .rivaledgeListNLJ_250').prop("checked", false);
		$('#<portlet:namespace/>firm_watchlist').prop("checked", true);
		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
	 	$("#homePageShowAttorney .individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList"/> Watchlists (0 Selected)');
	 	
	 	$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms (0) Selected');
		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
		$('#<portlet:namespace/>Firmstext').val("<%=ALMConstants.AMLAW_200 %>");
	});
	
	$('#homePageShowAttorney .rivaledgeListNLJ_250').bind('click', function (event) {
		
		$('#homePageShowAttorney .rivaledgeListAMLAW_100').prop("checked", false);
		$('#homePageShowAttorney .rivaledgeListAMLAW_200').prop("checked", false);
		$('#homePageShowAttorney .rivaledgeListNLJ_250').prop("checked", true);
		$('#<portlet:namespace/>firm_watchlist').prop("checked", true);
		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
	 	$("#homePageShowAttorney .individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList"/> Watchlists (0 Selected)');
	 	
	 	$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms (0) Selected');
		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
		$('#<portlet:namespace/>Firmstext').val("<%=ALMConstants.NLJ_250 %>");

	});
	
	$("input[class='allFirmsCheckBoxWatchListAttorneySearch']").change(function(){
	
		 $('#homePageShowAttorney .rivaledgeListAMLAW_200').prop("checked", false);
		 $('#homePageShowAttorney .rivaledgeListNLJ_250').prop("checked", false);
	});
	
	 $('#homePageShowAttorney .allFirmsCheckBoxWatchListAttorneySearch').click(function() {
	 
		 var allWatchListCounter = 0;
		 $('#<portlet:namespace/>firm_watchlist').prop("checked", true);
		 $('#<portlet:namespace/>allfirms').prop("checked", false);
		 
		 $('#<portlet:namespace/>allOtherFirm').prop("checked", false);
		 
		 $('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
		 
		
		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').each(function() 
		{
			allWatchListCounter++;
		});
		
		 $("#homePageShowAttorney .individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList"/> Watchlists (' + allWatchListCounter +' Selected)');
		 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms (0 Selected)');
		 $('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
		 $('#homePageShowAttorney .rivaledgeListAMLAW_100').prop("checked", false);
		 $('#homePageShowAttorney .rivaledgeListAMLAW_200').prop("checked", false);
		 $('#homePageShowAttorney .rivaledgeListNLJ_250').prop("checked", false);
		 
		 if(allWatchListCounter == 0)
		 {
			$('#homePageShowAttorney .rivaledgeListAMLAW_100').prop("checked", true);
		 }else{
			$('#homePageShowAttorney .rivaledgeListAMLAW_100').prop("checked", false);
		 }
		 
		 <portlet:namespace/>applyFirms();
		 
		});
	 
	 
	 $('#<portlet:namespace/>individualFirmsCounter').click(function() {
		 
			var checked = $("#<portlet:namespace/>selectAllFirms").is(":checked");
			var allFirmsListCounter = 0;
			if(checked)
			{   
				 $('#<portlet:namespace/>allOtherFirmDiv #<portlet:namespace/>allFirmsCheckBoxCounter').prop('checked', true);
				 $("#homePageShowAttorney .rivaledgeListAMLAW_100").prop("checked", false);
				 $("#homePageShowAttorney .rivaledgeListAMLAW_200").prop("checked", false);
				 $("#homePageShowAttorney .rivaledgeListNLJ_250").prop("checked", false);
			}
			else
			{
				$('#<portlet:namespace/>allOtherFirmDiv #<portlet:namespace/>allFirmsCheckBoxCounter').prop('checked', false);
			}
			
			 $('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
				 allFirmsListCounter++;
			 });
			 
			 if(allFirmsListCounter == 0)
				 {
					 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms ('+allFirmsListCounter+') Selected');
					 $("#homePageShowAttorney .individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList"/> Watchlists (1) Selected)');
					 $('#<portlet:namespace/>allfirms').prop("checked", false);
					 $('#<portlet:namespace/>allOtherFirm').prop("checked", false);
					 $('#<portlet:namespace/>selectAllFirms').prop("checked", false);
					 
						var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
						
						$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]').each(function() {
							 if(this.value == watchListId)
								{
									$(this).prop("checked", true);
								}
						});
						 $('#<portlet:namespace/>firm_watchlist').prop("checked", true);
				 }
			 else
				 {
					 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms ('+allFirmsListCounter+') Selected');
					 $("#homePageShowAttorney .individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList"/> Watchlists (0) Selected)');
					 $('#<portlet:namespace/>allOtherFirm').prop("checked", true);
					 $('#<portlet:namespace/>selectAllFirms').prop("checked", true);
					 $('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
					 $('#<portlet:namespace/>firm_watchlist').prop("checked", false);
					 $('#<portlet:namespace/>allfirms').prop("checked", false);
				 }
		});
			
	 $('#<portlet:namespace/>practiceAreaCounter').click(function() {
		 
			var checked = $("#<portlet:namespace/>selectAllPracticeAreas").is(":checked");
			var allPracticeAreasListCounter = 0;
			
			if(checked)
			{   
				 $('#<portlet:namespace/>allOtherPracticeArea #<portlet:namespace/>practiceId').prop('checked', true);
				 $('#practiceArea1').prop('checked', false);
			}
			else
			{
				$('#<portlet:namespace/>allOtherPracticeArea #<portlet:namespace/>practiceId').prop('checked', false);
			}
			
			 $('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:checked').each(function(){
				 allPracticeAreasListCounter++;
			 });
			 
			 if(allPracticeAreasListCounter == 0)
				 {
					 $("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="<portlet:namespace/>selectAllPracticeAreas"/> Practice Areas ('+allPracticeAreasListCounter+') Selected');
					 $('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", false);
					 $('#<portlet:namespace/>selectAllPracticeAreas').prop("checked", false);
					 $('#homePageShowAttorney .allPracticeArea').prop("checked", true);
					 $('#<portlet:namespace/>selectedPracticeArea').val('<%=ALMConstants.ALL_PRACTICE_AREAS %>');
				 }
			 else
				 {
					 $("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="<portlet:namespace/>selectAllPracticeAreas"/> Practice Areas  ('+allPracticeAreasListCounter+') Selected');
					 $('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", true);
					 $('#<portlet:namespace/>selectAllPracticeAreas').prop("checked", true);
				 }
			 
			 <portlet:namespace/>applyPracticeArea();
			 if(checked)
			 {
				 $("#<portlet:namespace/>selectAllPracticeAreas").prop('checked', true);
			 }
		}); 
	 
	 $('#<portlet:namespace/>locationsCounter').click(function() {
		 
			var checked = $("#<portlet:namespace/>selectAllLocations").is(":checked");
			var allLocationsListCounter = 0;
			if(checked)
			{   
				 $('#<portlet:namespace/>allOtherLocationsDiv .allLocationsCheckBox').prop('checked', true);
			}
			else
			{
				$('#<portlet:namespace/>allOtherLocationsDiv .allLocationsCheckBox').prop('checked', false);
			}
			
			 $('#<portlet:namespace/>allOtherLocationsDiv input[type=checkbox]:checked').each(function() {
				 allLocationsListCounter++;
			 });
			 
			 if(allLocationsListCounter == 0)
				 {
					 $("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="<portlet:namespace/>selectAllLocations"/> Locations ('+allLocationsListCounter+') Selected');
					 $('#<portlet:namespace/>allLocations').prop("checked", true);
					 $('#<portlet:namespace/>allOtherLocations').prop("checked", false);
					 $('#<portlet:namespace/>selectAllLocations').prop("checked", false);
					 $('#<portlet:namespace/>selectedLocation').val('<%=ALMConstants.ALL_LOCATIONS %>');
				 }
			 else
				 {
					 $("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="<portlet:namespace/>selectAllLocations"/> Locations ('+allLocationsListCounter+') Selected');
					 $('#<portlet:namespace/>allLocations').prop("checked", false);
					 $('#<portlet:namespace/>allOtherLocations').prop("checked", true);
					 $('#<portlet:namespace/>selectAllLocations').prop("checked", true);
				 }
			 <portlet:namespace/>applyLocations(); 
			 if(checked)
			 {
				 $("#<portlet:namespace/>selectAllLocations").prop('checked', true);
			 }
			 
		});
	 
	//Function for showing selected Location name
	 $("input[class='allLocationsCheckBox']").change(function(){
			changeOnIndividualLocations();
			<portlet:namespace/>applyLocations();
		});
	 
	//Function for showing selected PracticeArea name
	 $("input[id='<portlet:namespace/>practiceId']").change(function(){
	 	changeOnIndividualPracticeAreas();
	 	<portlet:namespace/>applyPracticeArea();
	 });
	
		//Function for showing selected PracticeArea name
	 $("input[id='<portlet:namespace/>allFirmsCheckBoxCounter']").change(function(){
		 

		 changeOnIndividualFirms();
		 
	 	 <portlet:namespace/>applyFirms();
	 	
	 });
		
	function changeOnIndividualPracticeAreas()
		{
			var allPracticeCounter = 0;
			 $('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:checked').each(function() {
				allPracticeCounter++;
				
			    });
				
				$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="<portlet:namespace/>selectAllPracticeAreas"/> PracticeAreas ('+allPracticeCounter+') Selected');
				
				if(allPracticeCounter == 0)
				{
					$('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", false);
					$('#homePageShowAttorney .allPracticeArea').prop("checked", true);
					$('#<portlet:namespace/>selectedPracticeArea').val('<%=ALMConstants.ALL_PRACTICE_AREAS %>');
				}
			else
				{
					$('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", true);
					$('#homePageShowAttorney .allPracticeArea').prop("checked", false);
				}
		}
	
	function changeOnIndividualFirms()
	{
		//alert('INSIDE changeOnIndividualFirms() method ');
		var allFirmsCounter = 0;
		 $('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
			 allFirmsCounter++;
		    });
			
			$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms ('+allFirmsCounter+') Selected');
			if(allFirmsCounter == 0)
			{
				$('#<portlet:namespace/>firm_watchlist').prop("checked", false);
				$('#<portlet:namespace/>allOtherFirm').prop("checked", false);
				$('#<portlet:namespace/>allfirms').prop("checked", false);
				
				$('#<portlet:namespace/>individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
				$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
				

				var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
				
				$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]').each(function() {
					 if(this.value == watchListId)
						{
							$(this).prop("checked", true);
						}
				});
			}
		else
			{
				$('#<portlet:namespace/>allOtherFirm').prop("checked", true);
				$('#<portlet:namespace/>firm_watchlist').prop("checked", false);
				$('#<portlet:namespace/>allfirms').prop("checked", false);
				
				$('#<portlet:namespace/>individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
				$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
			}
	}

//UI Logic for allFirms
	
	$("input[id='<portlet:namespace/>allfirms']").change(function(){
		allFirmChange();
	});

	function allFirmChange()
	{
		var checked = $("#<portlet:namespace/>allfirms").is(":checked");
		if(checked)
		{		 
			 $('#<portlet:namespace/>firm_watchlist').prop("checked", false);
			 $('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
			 $('#<portlet:namespace/>individualfirmsWatchList').find('input[type=checkbox]:checked').removeAttr('checked');
			 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="Abrams" id="<portlet:namespace/>firmCounter"> Firms (0) Selected');
		}
	}
	
	$("input[id='<portlet:namespace/>RivalEdgeList']").change(function(){
		rivalEdgeListChange();
	});
	
	function rivalEdgeListChange()
	{
		var checked = $("#<portlet:namespace/>RivalEdgeList").is(":checked");
		if(checked)
		{
			$('#<portlet:namespace/>allfirms').prop("checked", false);
			$('#<portlet:namespace/>popup option:nth(0)').attr("selected", "selected");
			$('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
			$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/>  Firms (0) Selected');
		}
	}
	
	$("#<portlet:namespace/>Select1").change(function(){
		individualSelectChange();
	}); 
	
	function individualSelectChange()
	{
		var count = $("#<portlet:namespace/>Select1 :selected").length;
		if(count> 0)
		{	
			$('#<portlet:namespace/>allfirms').prop("checked", false);
			$('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
			$('#<portlet:namespace/>RivalEdgeList').prop("checked", true);
			$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/>  Firms (0) Selected');
		}
	}
	
	$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]').change(function(){
		allOtherFirmChange();
	});

	function allOtherFirmChange()
	{
		var allFirmsCounter = 0;
		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() 
		{
			allFirmsCounter++;
		});
		$('#<portlet:namespace/>popup option:selected').removeAttr("selected");
		$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/>  Firms ('+allFirmsCounter+') Selected');
		
		if(allFirmsCounter> 0)
		{
			 $('#<portlet:namespace/>allOtherFirm').prop("checked", true);
			 $('#homePageShowAttorney .allFirmsCheckBoxWatchListAttorneySearch').prop("checked", false);
			 $("#homePageShowAttorney .individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList"/>  Watchlists (0) Selected');
			 
		}
		else
		{
			$('#<portlet:namespace/>allOtherFirm').prop("checked", false);
			$('#<portlet:namespace/>firm_watchlist').prop("checked", true);
			
			var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
			
			$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]').each(function() {
				 if(this.value == watchListId)
					{
						$(this).prop("checked", true);
					}
				 
				 $("#homePageShowAttorney .individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList"/>  Watchlists (1) Selected');
			});
		}
		
		 $('#homePageShowAttorney .rivaledgeListAMLAW_100').prop("checked", false);
		 $('#homePageShowAttorney .rivaledgeListAMLAW_200').prop("checked", false);
		 $('#homePageShowAttorney .rivaledgeListNLJ_250').prop("checked", false);
	}
	
	//DisplayColumns select ad deselect

	$("input[name=displayColumns]").click(function(){
		changeOnDisplayColumns();
	});

	$("input[name=orderBy]").click(function(){
		changeOnGroupByValues();	
	});

	$("input[name=searchResultsPerPage]").click(function(){
		changeOnResultsPerPageValues();	
	});
	
	$('#<portlet:namespace/>resetAll').bind('click', function (event) {
		<portlet:namespace/>clearAll();
	});
	
	
	//Firm clear button event: Will clear all slected values in firms popup and reset to default value
	$('#<portlet:namespace/>clearButton').bind('click', function (event) {
		clearFirms();
		var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
		
		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]').each(function() {
			 if(this.value == watchListId)
				{
					$(this).prop("checked", true);
				}
		});
		<portlet:namespace/>applyFirms();
	});

	$('#<portlet:namespace/>clearPracticeArea').bind('click', function (event) {
		clearPracticeAreas();
	});
	
	$('#<portlet:namespace/>clearButtonTitles').bind('click', function (event) {
		clearTitles();
	});
	
	$('#<portlet:namespace/>clearButtonLocations').bind('click', function (event) {
		clearLocations();
	});

	
	
	$('#<portlet:namespace/>ApplyFirm').bind('click', function (event) {
		<portlet:namespace/>applyFirms();
		$("#<portlet:namespace/>popup").toggle();
	
	});
	
	$('#<portlet:namespace/>ApplyPracticeArea').bind('click', function (event) {
		<portlet:namespace/>applyPracticeArea();
		$("#<portlet:namespace/>popupPracticeArea").toggle();
	
	});
	
	$('#<portlet:namespace/>ApplyTitles').bind('click', function (event) {
		<portlet:namespace/>applyTitles();
		$("#<portlet:namespace/>popupTitles").toggle();
	
	});
	
	
	$('#<portlet:namespace/>ApplyLocations').bind('click', function (event) {		
		<portlet:namespace/>applyLocations();
		$("#<portlet:namespace/>popupLocation").toggle();	
	});

	$("#<portlet:namespace/>applySearch").click(function() {
		
		var fromDate= $("#<portlet:namespace/>graduationFrom").val();
		var toDate= $("#<portlet:namespace/>graduationTo").val();

		if(fromDate !='<%=ALMConstants.ANY%>' && toDate !='<%=ALMConstants.ANY%>' &&  fromDate>toDate)
		{
			alert("Graduation End Year should be greater than Graduation Start Year ");
			return false;
		}
		$("#<portlet:namespace/>goToPage").val(1);
		//search();
	});
	

	//Spring will add <input> tags with names prefixed with "_" for type "radio" and "checkbox".
	//On form POST submit they all are carried to server which is unneccessary
	//hence removing all such input box to make page and jQuery selections less heavy :) on document ready.
	
	//NOTE : If any inputs added with name prefixed with "_" purposefully will also get removed. So be cautious
	$("#attorneySearchModelBean input[name^=_]").remove();
	
	//autoselect previously selected search critria
	<portlet:namespace/>initializeSearchCriteria();
	//fire a search for the default search criteria
	//search();
		
});

$(document).ready(function(){
	$(".menu").hover(
		function(){$(".sub").slideToggle(400);},
		function(){$(".sub").hide();}
	);
});

(function(){
      var del = 200;
      $('.icontent').hide().prev('a').hover(function(){
        $(this).next('.icontent').stop('fx', true).slideToggle(del);
      });
    })();
    
	
</script>

<div id="homePageShowAttorney">
<form:form  commandName="attorneySearchModelBean" method="post" action="${changeSearchCriteria}" id="attorneySearchModelBean" autocomplete="off">
 	<form:hidden path="goToPage"  	id="goToPage"/>
 	<form:hidden path="sortColumn" 	id="sortColumn"/>
 	<form:hidden path="sortOrder"  	id="sortOrder"/>
 	<form:hidden path="portletId"	id="portletId"/>
 	<form:hidden path="addToSession"	id="addToSession"/>
<div class="breadcrumbs"><span>ATTORNEY SEARCH</span></div>
<%--  <form name="searchForm" method="post" action="" id="searchForm"> --%>
	<div id="dropdown">
		<!-- dropdown_title_start -->
		<ul id="droplist">

			<li><label>Firm(s)</label>
				<div class="srchBox" style="width:182px">
					
					<%-- <input type="text" name="Firmstext" id="Firmstext" value="<%=ALMConstants.AMLAW_100 %>"
						style="text-overflow: ellipsis;width:139px" class="input"/> --%>
					
					     <c:choose>
			              <c:when test="${empty allWatchListsDefaultList or allWatchListsDefaultList == ''}">
			          			<input type="text" name="Firmstext" id="Firmstext"  value="<%=ALMConstants.AMLAW_100 %>" style="text-overflow:ellipsis;" class="input" autocomplete="off"/>
			          	  </c:when>
			          	  <c:otherwise>
			          			<input type="text" name="Firmstext" id="Firmstext"  value="${allWatchListsDefaultList.groupName}" style="text-overflow:ellipsis;" class="input" autocomplete="off"/>
			          	  </c:otherwise>
			          	</c:choose>
 
						  <!--  <input type="text" name="Firmstext" id="Firmstext"  style="text-overflow:ellipsis;" class="input"/> -->
						   <input type="button" name="search" value="" class="srchBack" id="testDiv"/>
						   <div class="clear">&nbsp;</div>
				</div> <input type="button" name="search" id="hide" value="" class="typeSel"/>


				<div class="rel">
					<div id="<portlet:namespace/>popup" class="firmPage">
						<p><form:radiobutton path="firmType" id="allfirms" value="<%=ALMConstants.ALL_FIRMS %>"/>&nbsp;<%=ALMConstants.ALL_FIRMS %></p>
						<%-- <p><form:radiobutton path="firmType" id="RivalEdgeList"   value="<%=ALMConstants.RIVALEDGE_LIST %>"/>&nbsp;<%=ALMConstants.RIVALEDGE_LIST %></p> --%>
						<p> <form:radiobutton path="firmType" id="firm_watchlist" value=""/>Select Watchlist/ RivalEdge List</p>
						<%-- <form:select path="firmList" id="Select1" multiple="false" size="4" cssStyle="width:220px">
							<form:options items="${allRankingList}"/>
						</form:select> --%>
						
						<div class="individualfirms-Watchlist">
                			<input type="checkbox" value="selectWatchList" id="selectAllWatchList"/> Watchlists (0 Selected)<br/>
               			</div>
               			<div  class="individualfirmsWatchList Select-Individual-Firms" id="<portlet:namespace/>individualfirmsWatchList"> 
						<form:checkbox  class="rivaledgeListAMLAW_100" path="firmList"  value="<%=ALMConstants.AMLAW_100 %>"/><%=ALMConstants.AMLAW_100 %><br>
						<form:checkbox  class="rivaledgeListAMLAW_200" path="firmList"  value="<%=ALMConstants.AMLAW_200 %>"/><%=ALMConstants.AMLAW_200 %><br>
						<form:checkbox  class="rivaledgeListNLJ_250" path="firmList"  value="<%=ALMConstants.NLJ_250 %>"/><%=ALMConstants.NLJ_250 %><br>
  
 				 		<div  class="individualfirmsWatchListDIV" id="<portlet:namespace/>individualfirmsWatchListDIV"> 
			                <c:forEach var="watchlist"  items="${allWatchLists}">     
		    	  				 <form:checkbox  class="allFirmsCheckBoxWatchListAttorneySearch" path="firmListWatchList"  value="${watchlist.groupId}" labelAttr="${watchlist.groupName}"/>${watchlist.groupName}<br>
		  				 	</c:forEach>
						</div>
						</div>
						<br>
						
						<p><form:radiobutton path="firmType" value="<%=ALMConstants.INDIVIDUAL_LIST %>" id="allOtherFirm"/>&nbsp;<%=ALMConstants.INDIVIDUAL_LIST %></p>
						
						<div class="individualfirms-first" id="<portlet:namespace/>individualFirmsCounter"> 
	               			 <input type="checkbox" value="selectAllFirms" id="selectAllFirms"/>Firms (0 Selected)<br/>
              			</div>
						
						<div	id="<portlet:namespace/>allOtherFirmDiv" class="Select-Individual-Firms">
							<c:forEach var="firm" items="${allFirms}">
								<form:checkbox  class="allFirmsCheckBox" id="allFirmsCheckBoxCounter" path="firmList" value="${firm.companyId}" labelAttr="${firm.company}"/>${firm.company}<br>
							</c:forEach>
						</div>
 						<div class="popupsubmit">
 						 <input type="button" class="buttonOne" value="Apply" id="ApplyFirm">
							<input type="button" class="buttonTwo" value="Clear All" id="clearButton">
						</div>

					</div>
				</div>
				 <c:if test="${not empty allWatchListsDefaultList}">
					<input type="hidden" name="defaultWatchListId" id="defaultWatchListId" value="${allWatchListsDefaultList.groupId}"/>
				 </c:if>
			</li>
				
			  <li>
        		<label>Location(s)</label>
					  <div class="srchBox">
						<%-- <input type="text"  value="<%=ALMConstants.ALL_LOCATIONS %>" class="input" name="selectedLocation" id="selectedLocation" /> --%>
						<form:input cssClass="input" path="selectedLocation" name="selectedLocation" id="selectedLocation" style="text-overflow:ellipsis;" /> 
						<input type="button" name="search" value="" class="srchBack"/>
						<div class="clear">&nbsp;</div>
					  </div>
					 <input type="button" name="search" value=""  id="locationId" class="typeSel"/>
					<div class="rel">	 
					<%-- 	<div  id="<portlet:namespace/>popupLocation" class="firmPage">
							<p><form:radiobutton path="locations"  id="allLocations" value="<%=ALMConstants.ALL_LOCATIONS%>"/> <%=ALMConstants.ALL_LOCATIONS %></p>	
							<p><input type="radio"  id="allOtherLocations" value="Select Individual Locations"/> Select Individual Locations</p>
							<div class="individualfirms-first" id="<portlet:namespace/>locationsCounter"> 
								<input type="checkbox" value="selectAllLocations" id="selectAllLocations"/>  Locations (0) Selected<br> </div>
							<div id="<portlet:namespace/>allOtherLocationsDiv" class="Select-Individual-Firms">		
							<c:forEach items="${allOtherLocations}" var="loc">
								<form:checkbox path="locations" class="allLocationsCheckBox" value="${loc}"/>${loc}<br>
							</c:forEach>	   
							</div>
							<div class="popupsubmit">				
							<input type="button" class="buttonOne" value="Apply" id="ApplyLocations">
							<input type="button" class="buttonTwo" value="Clear All" id="clearButtonLocations">
							</div>
						</div> --%>
						
						<div  id="<portlet:namespace/>popupLocation" class="firmPage"><!-- When using initAjax, it may be nice to put a throbber here, that spins until the initial content is loaded: -->
						<p><form:radiobutton path="locations"  id="allLocations" value="<%=ALMConstants.ALL_LOCATIONS%>"/> <%=ALMConstants.ALL_LOCATIONS %></p>
						<p><form:radiobutton path="locations"  id="allOtherLocations" value="Select Individual Locations"/> Select Individual Locations</p>
						<div  id="<portlet:namespace/>locationTree">
					<%-- 	<c:if test="${not empty locationJson}">	
							<ul>						
								<c:forEach  var="results" items="${locationJson}" varStatus="i">									
										<li id="id1">${results.title}
											<c:if test="${not empty results.children}">	
											<ul>	
												<c:forEach  var="country" items="${results.children}">
													<li>${country.title}
														<c:if test="${not empty country.children}">	
															<ul>
																<c:forEach  var="city" items="${country.children}">
																	<li>${city.title}
																</c:forEach>
															</ul>
														</c:if>	
												</c:forEach>
											</ul>
											</c:if>
									
								</c:forEach>
							</ul>
						</c:if> --%>
						
						</div>
						<div class="popupsubmit">				
							<input type="button" class="buttonOne" value="Apply" id="ApplyLocations">
							<input type="button" class="buttonTwo" value="Clear All" id="clearButtonLocations">
						</div>
						
					</div>
 				</li>
		
		   <li>
					  <label>Practice Area(s)</label>
						  <div class="srchBox">
							<input type="text"  class="input" name="selectedPracticeArea" id="selectedPracticeArea" style="text-overflow:ellipsis;" autocomplete="off"/>
							<input type="button" name="search" value="" class="srchBack"/>
							<div class="clear">&nbsp;</div>
						  </div>
						 <input type="button" name="search" value=""  id="practiceAreaId" class="typeSel"/>
						<div class="rel">
							  <div  id="<portlet:namespace/>popupPracticeArea" class="firmPage">					
									
								<p><form:radiobutton path="practiceArea" class="allPracticeArea" value="<%=ALMConstants.ALL_PRACTICE_AREAS %>"/><%=ALMConstants.ALL_PRACTICE_AREAS%></p>
						   		<p><form:radiobutton path="practiceArea"  id="allPracticeAreaIndividual" value="Select Individual Practice Areas" /> Select Individual Practice Areas</p>
									<div class="individualfirms-first" id="<portlet:namespace/>practiceAreaCounter"> <input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" /> Practice Areas (0 Selected)<br> </div>
								  <div id="<portlet:namespace/>practiceAreaDiv" class="Select-Individual-Firms">										 
										 <div id="<portlet:namespace/>allOtherPracticeArea">
										    <c:forEach var="practice" items="${allPracticeArea}">								
												<form:checkbox path="practiceArea" id="practiceId" value="${practice.practiceArea}"/> ${practice.practiceArea}<br>
											</c:forEach>
										 </div>	
									</div>
						<%-- <p>
							<form:checkbox path="firstListedPractice" id="firstListedPractice" value="<%=ALMConstants.FIRST_LISTED_PRACTICE %>"/>Search first listed practice only
						</p> --%>

						<div class="clear"></div>
								<div class="popupsubmit">
								 <input type="button" class="buttonOne" value="Apply" id="ApplyPracticeArea"/>
								 <input type="button" class="buttonTwo" value="Clear All" id="clearPracticeArea"/>
								</div>	
								
									
						</div>
						</div>
				</li>
				
				
				<li><label>Title(s)</label>
				<div class="srchBox">
					<input type="text" name="search" value="All" class="input" id="selectedTitles" style="text-overflow: ellipsis;width:110px" autocomplete="off"/> 
					<input type="button" name="search" value="" class="srchBack"/>
					<div class="clear">&nbsp;</div>
				</div> 
				<input type="button" name="search" value="" class="typeSel"	id="titleDropdown"/>

				<div class="rel">
					<div id="<portlet:namespace/>popupTitles" class="firmPage">
					<div id="<portlet:namespace/>titleSelectDiv">
						<div>
						<form:checkbox path="title"  id="allTitles" value="<%=ALMConstants.ALL%>"/>&nbsp;<%=ALMConstants.ALL%>  <br>
							
							<div id="<portlet:namespace/>allOtherTitlesDiv">
								<form:checkbox path="title" class="allTitlesCheckBox" 
								    value="<%=ALMConstants.PARTNERS%>"/>&nbsp;<%=ALMConstants.PARTNERS%><br>
								<form:checkbox path="title" class="allTitlesCheckBox"
									value="<%=ALMConstants.ASSOCIATE%>"/><%=ALMConstants.ASSOCIATE%><br>
								<form:checkbox path="title" class="allTitlesCheckBox"
									value="<%=ALMConstants.OTHER_COUNSEL%>"/><%=ALMConstants.OTHER_COUNSEL%><br>
								<form:checkbox path="title" class="allTitlesCheckBox"
									value="<%=ALMConstants.ADMINISTRATIVE%>"/><%=ALMConstants.ADMINISTRATIVE%><br>
								<form:checkbox path="title" class="allTitlesCheckBox"
									value="<%=ALMConstants.OTHER%>"/><%=ALMConstants.OTHER%><br>
							</div>
						</div>
						</div>
						<div class="popupsubmit">
						<input type="button" class="buttonOne" value="Apply" id="ApplyTitles">
						<input type="button" class="buttonTwo" value="Clear All" id="clearButtonTitles">
						
						</div>
					</div>
				</div></li>
				
				<li class="Submit-Filter-Criteria"><label>&nbsp;</label><input type="submit" value="Apply"	class="buttonOne" id="applySearch"/></li>
				<li><label>&nbsp;</label><input type="button" value="Reset All"	class="buttonTwo" id="resetAll"/></li>
			<li><input type="hidden" name="selectedFirms" id="selectedFirms" title="" customAttr=""/></li>

		</ul>
		<div class="clear">&nbsp;</div>
	</div>
	<div class="filtersPage">
		<div class="barSec">
			<a href="javascript:void(0);" class="dwnBx" id="<portlet:namespace/>btnAdd">Additional Filters</a>
		</div>
	</div>
	<div class="filtersPage" id="<portlet:namespace/>additional" style="display: none">
		<div id="dropdown">
			<!-- dropdown_title_start -->
			<ul id="droplist">
				<li>
					<div class="col2-1" style="width:212px">
						<label>Law School Graduation</label>
						<span class="flLeft marlt1 mart1" style="margin-top: 5px; color:#777777">From</span>
						<div class="flLeft selectBox" style="width: 74px">
							<form:select path="fromDate" id="graduationFrom">
								<form:options items="${fromYear}"/>
							</form:select>
						</div>
						<span class="flLeft marlt1 mart1"  style="margin-top: 5px; color:#777777">To</span>
						<div class="flLeft selectBox" style="width: 74px">
							<form:select path="toDate" id="graduationTo">
								<form:options items="${toYear}"/>
							</form:select>
						</div>
					</div>
				</li>
				<li><label>Law School</label>
					<div class="srchBox">
						 <form:input path="lawSchools" type="text" name="search" class="input"	id="selectedLawSchool" autocomplete="off"/>
							<input type="button" name="search" value="" class="srchBack"/>
						<div class="clear">&nbsp;</div>
					</div> 
					<!-- <input type="button" name="search" value="" class="typeSel"/> -->
					</li>

				<li><label>Admissions</label>
					<div class="srchBox">
						<form:input path="admissions" type="text" name="search" value="" class="input"
							id="selectedAdmissions" autocomplete="off"/>
							  <input type="button"	name="search" value="" class="srchBack"/> 
						<div class="clear">&nbsp;</div>
					</div>
					<!--  <input type="button" name="search" value="" class="typeSel"/> -->
					 </li>
				<li><label>Name</label>
					<div class="srchBox" style="width: 175px">
						<form:input path="name" type="text" name="search" class="input"
							style="width: 140px" id="name"/> <input type="button"
							name="search" value="" class="srchBack" autocomplete="off"/>
						<div class="clear">&nbsp;</div>
					</div></li>
					
					<li>
					<div class="col2-1">
						<label>Keywords in Bio</label>
						<div class="srchBox">
							<form:input path="keywords" type="text" name="search" class="input" id="keywordsInBio" autocomplete="off"
								/> <input type="button" name="search" value=""
								class="srchBack"/>
							<div class="clear">&nbsp;</div>
						</div>
					</div>
				</li>
			</ul>
			<div class="clear">&nbsp;</div>
		</div>
		<div class="barSec">
			<a href="javascript:void(0);" class="upBx" id="<portlet:namespace/>btnSave">Hide Filter</a>
		</div>
		
	</div>
	
	<div class="attorneysearch" id="viewSettingPopup" style="display:none">
              <div class="popHeader"><span id="viewSettingCloseSpan"><a style="margin-top:2px" class="btn icon closewhite closeOne flRight" href="javascript:void(0);"  id="viewSettingClose"  onclick="cancelViewSettings();">Close</a> </span>SETTINGS: ATTORNEY SEARCH
                <div class="clear">&nbsp;</div>
              </div>
              <div class="popMiddle">
                <div class="col3-4">
                  <h6>Column Display:</h6>
                  <div class="marlt3">
					<ul class="reset list5">
							<c:forEach var="dis_column" items="${allDisplayColumns}">								
								<li><form:checkbox path="displayColumns" value="${dis_column.value}"/> ${dis_column.displayValue}</li>
							</c:forEach>
							<!-- to hold the ; separated values to send to controller -->
							<input type="hidden" id="displayColumns_hidden"/>
													
					</ul>
				</div>
                </div>
                <div class="col3-4">
                  <h6>Group By:</h6>
				<div class="marlt3">
					<div id="groupByDiv">
						<ul class="reset list5">
						
						<li><form:radiobutton path="orderBy" value="-1"/>&nbsp;None </li> 
						<li><form:radiobutton path="orderBy" value="5"/>&nbsp;Firm </li>
						<li><form:radiobutton path="orderBy" value="7"/>&nbsp;Title</li> 
						<li><form:radiobutton path="orderBy" value="8"/>&nbsp;Location </li>
						<li><form:radiobutton path="orderBy" value="9"/>&nbsp;Practice </li>
						</ul>
					</div>
				</div>
			</div>
                <div class="col3-4">
                  <h6>Display No. of Results</h6>
				<div class="marlt3">
					<ul class="reset list5">
					<li><form:radiobutton path="searchResultsPerPage"  value="25" />&nbsp;25</li>
					<li><form:radiobutton path="searchResultsPerPage"  value="50" />&nbsp;50</li>
					<li><form:radiobutton path="searchResultsPerPage"  value="100"/>&nbsp;100</li>
					<li><form:radiobutton path="searchResultsPerPage"  value="500"/>&nbsp;500</li>
					</ul>
				</div>
			</div>
                <div class="clear">&nbsp;</div>
              </div>
              <div class="popFooter">
                <div class="flLeft">
                  <input type="button" class="buttonTwo" value="Reset All" id="resetAllViewSetting" onclick="resetViewSettings();">
                </div>
                <div class="flRight">
                	<input type="hidden"  value="" id="selectedColumns">
                	<input type="hidden"  value="" id="selectedGroupBy">
                  	<input type="button" class="buttonOne" value="Apply" id="ApplySetting" onclick="applyViewSettings();">
                  	<input type="button" class="buttonTwo" value="Cancel" id="CancelSetting" onclick="cancelViewSettings();">
                </div>
                <div class="clear">&nbsp;</div>
              </div>
	     </div>
	
</form:form>
</div>

<script type="text/javascript">
	// Omniture SiteCatalyst - START
	s.prop22 = "premium";
	s.pageName="rer:attorney-search";
	s.channel="rer:attorney-search";
	s.server="rer";
	s.prop1="attorney-search"
	s.prop2="attorney-search";
	s.prop3="attorney-search";
	s.prop4="attorney-search";
	s.prop21=s.eVar21='current.user@foo.bar';	
	s.events="event2";
	s.events="event27";
	s.t();
	// Omniture SiteCatalyst - END
</script>
