<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.alm.rivaledge.model.AttorneySearchModelBean"%>
<%@page import="com.alm.rivaledge.util.ALMConstants"%>

<%@ taglib prefix="c" 				uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" 				uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="portlet"			uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="fmt" 			uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="liferay-portlet" uri="http://liferay.com/tld/portlet"%>
<%@ taglib prefix="liferay-ui"   	uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="liferay-util" 	uri="http://liferay.com/tld/util"%>
<%@ taglib prefix="spring"          uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form"            uri="http://www.springframework.org/tags/form"%>

<portlet:defineObjects/>

<%						
	PortletURL printPublicationsURL = renderResponse.createRenderURL();
	printPublicationsURL.setWindowState(LiferayWindowState.POP_UP);
	printPublicationsURL.setParameter("displayPrintPage", "true");							
%>

<style>
	.hideClass 
	{
		display:none;
	}
</style>

<script type="text/javascript">
function setFileType(fileType)
{
	document.getElementById('fileType').value = fileType;
	var r=confirm("Click OK to export or Cancel");
	if (r == true)
  	{
  		document.getElementById('exportForm').submit();
  	}
	else
	{
	  return false;
	}
}	
	
function openPopUp()
{
	popupWindow = window.open(
					'<%= printPublicationsURL.toString() %>',
					'popUpWindow','height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes');
}

$(document).ready(function(){
	
	$("#attorney-analysis").removeClass("active-menu");
	$("#attorney-details").addClass("active-menu");
	
});


</script>

<div id="content"> 
      <div id="tab2">
      	<h4 class="heading">Detailed Results</h4>
        <div class="marbtm2 martp3">
       		<div>
        		<div class="flLeft resultsec"><strong>Showing ${attorneySearchModelBean.resultStartRange} to ${attorneySearchModelBean.resultEndRange}&nbsp;of&nbsp;${attorneySearchModelBean.totalResultCount} Results</strong> 
			  		 <span><em>(
			          <c:choose>
				      				 <c:when test="${fn:length(attorneySearchModelBean.selectedFirms) gt 100}">
				      				 	${fn:substring(attorneySearchModelBean.selectedFirms, 0, 99)}..,
				      				 </c:when>
				      				 <c:otherwise>
				      				 	${attorneySearchModelBean.selectedFirms}, 
				      				 </c:otherwise>		        		  
					</c:choose>
					 <c:choose>
				      				 <c:when test="${fn:length(attorneySearchModelBean.selectedLocation) gt 100}">
				      				 	${fn:substring(attorneySearchModelBean.selectedLocation, 0, 99)}..,
				      				 </c:when>
				      				 <c:otherwise>
				      				 	${attorneySearchModelBean.selectedLocation}, 
				      				 </c:otherwise>		        		  
					</c:choose>
					 <c:choose>
				      				 <c:when test="${fn:length(attorneySearchModelBean.selectedPracticeArea) gt 100}">
				      				 	${fn:substring(attorneySearchModelBean.selectedPracticeArea, 0, 99)}..,
				      				 </c:when>
				      				 <c:otherwise>
				      				 	${attorneySearchModelBean.selectedPracticeArea}, 
				      				 </c:otherwise>		        		  
					</c:choose>
						
						${attorneySearchModelBean.selectedTitles},
						<c:choose>
				      		<c:when test="${fn:length(attorneySearchModelBean.fromDate) gt 0}">
				      			${attorneySearchModelBean.fromDate}
				      		</c:when>
				      		<c:otherwise>
				      			<%=ALMConstants.ANY%> 
				      		</c:otherwise>
						</c:choose>
						<c:choose>
				      		<c:when test="${fn:length(attorneySearchModelBean.toDate) gt 0}">
				      			-${attorneySearchModelBean.toDate}
				      		</c:when>
				      		<c:otherwise>
				      			-<%=ALMConstants.ANY%> 
				      		</c:otherwise>
						</c:choose>
						<c:if test="${fn:length(attorneySearchModelBean.lawSchools) gt 0}">
							,${attorneySearchModelBean.lawSchools}
						</c:if> 
						<c:if test="${fn:length(attorneySearchModelBean.admissions) gt 0}">
							,${attorneySearchModelBean.admissions}
						</c:if>
					 	<c:if test="${fn:length(attorneySearchModelBean.name) gt 0}">
							,${attorneySearchModelBean.name}
						</c:if>
						<c:if test="${fn:length(attorneySearchModelBean.keywords) gt 0}">
							,${attorneySearchModelBean.keywords}
						</c:if>
						
			          )</em>
			          </span>
				</div>
        	</div>
            <div class="flRight">
            	<ul class="menuRight flRight">
					<li>
						<a class="btn icon alert" href="#">Create Alert</a>
					</li>
					<li>
						<a class="btn icon print" href="#" onClick="openPopUp();">Print</a>
					</li>
					<li>
						<a id="exports" class="btn icon export" href="#">Export</a>
						<div id="actionBox" class="actionSec" style="">
							<h5>Actions</h5>
							<ul class="reset">
								<li><span>E</span><a href="#"
									onclick="setFileType('xls')">Export to Excel</a></li>
								<li><span>E</span><a href="#"
									onclick="setFileType('csv')">Export to CSV</a></li>
							</ul>
							<div class="clear">&nbsp;</div>
						</div>
					</li>
					<li>
						<div class="viewSetting flRight rel" style="margin-top:0px;">
				        	<a class="btn icon settings" href="javascript:void(0)" onclick="toggleViewSettings(this);">
				          		<span><span class="btn icon dropdownarrow" id="viewSettingsAttorney" >View Settings</span></span>
				          	</a>
				        </div>
					</li>
				</ul>
            </div>      
		</div>
		<c:if test="${not isAdminUser}">
			<c:if test="${displayMessage}">
		  		<div style="color:#b63334;">
		  			Search Results are too large to display in their entirety. Only the first 5,000 results are displayed below. To view an entire search result, please narrow your search criteria above and rerun your search.
		  		</div>
		  	</c:if>
	  	</c:if>
	</div>	
        
        <div class="clear">&nbsp;</div>
          
         <div id="attorneyResultDiv"></div>
        
        <div id="tableDiv">
        <table width="100%" class="tble4" border="0" cellspacing="0" cellpadding="0" id="attorneySearchResultsTable">
         <%--  <colgroup>
          <col width="25" />
         <col id="attorney_col" width="130" />
          <col id="firm_col" width="130" />
          <col id="title_col" width="70" />
          <col id="location_col" width="100" />
          <col id="practices_col" width="150" />
          <col id="education_col" width="200" />
          <col id="admissions_col" width="70" /> 
          <col id="bioText_col" width="200" /> 
          <col id="contactInfo_col" width="150" /> 
          </colgroup> --%>
          
        	<!-- 
        		 
        		Attorney  Sorting Implementation : The number which is passed in sortResults() method depends upon index 
        										  of field in query.
        										  below are the indexes of field in Query for which we need to implement sorting
        										  
        										  Attorney		:column #2
        										  Firm			:column #5
        										  Title			:column #7
        										  Location		:column #8
        										  Practices		:column #9
        										  Education		:column #10
        										  Admissions	:column #11
        										  Bio Text		:column #12
        										  Contact Info	:column #14   
        										      
        		Note : Please make sure that whenever you are changing the order of fields in query,change these indxes as well  										         	
        	 -->
        
        
	          <tr class="tpbar">					
					<th id="attorney_header" align="left"><a href="javascript:void(0);" onclick="sortResults(2)">Attorney</a></th>
					<th id="firm_header" align="left"><a href="javascript:void(0);" onclick="sortResults(5)">Firm</a></th>
					<th id="title_header"><a href="javascript:void(0);" onclick="sortResults(7)">Title</a></th>
					<th id="location_header"><a href="javascript:void(0);" onclick="sortResults(8)">Location</a></th>
					<th id="practices_header"><a href="javascript:void(0);" onclick="sortResults(9)">Practices</a></th>
					<th id="education_header"><a href="javascript:void(0);" onclick="sortResults(10)">Education</a></th>
					<th id="admissions_header"><a href="javascript:void(0);" onclick="sortResults(11)">Admissions</a></th>
					<th id="bioText_header"><a href="javascript:void(0);" onclick="sortResults(12)">Bio Text</a></th>
					<th id="contactInfo_header"><a href="javascript:void(0);" onclick="sortResults(14)">Contact Info</a></th>
				</tr>  
	          <c:choose>
	          <c:when test="${fn:length(attorneySearchResults) gt 0}">
      		 	<c:forEach  var="results" items="${attorneySearchResults}" varStatus="i">
      		 	<%--Group By logic starts --%>
      		 	 <%-- orderBy values 	firmName;		//column #5
										title			//column #7
										location		//column #8
										practices		//column #9	
				 --%> 
				 <%--The below choose cond'n  is responsible for grouping the results in Table in UI--%>
				<c:choose>
						<%-- For first row in Table --%>
   						 <c:when test="${(i.first) && (attorneySearchModelBean.orderBy == 5)}">
     						    <tr class="oddone">
	      		 					<td colspan="10"><div class="padtp1 padlt1 padbtm1"><strong>${results.firmName}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(i.first) && (attorneySearchModelBean.orderBy == 7)}">
     						    <tr class="oddone">
	      		 					<td colspan="10"><div class="padtp1 padlt1 padbtm1"><strong>${results.title}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 <c:when test="${(i.first) && (attorneySearchModelBean.orderBy == 8)}">
     						    <tr class="oddone">
	      		 					<td colspan="10"><div class="padtp1 padlt1 padbtm1"><strong>${results.location}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(i.first) && (attorneySearchModelBean.orderBy == 9)}">
     						    <tr class="oddone">
	      		 					<td colspan="10"><div class="padtp1 padlt1 padbtm1"><strong>${results.practices}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 <%-- For subsequent rows in Table only when there is change in 2 consecutive records as they are already sorted at DB level--%>
   						 <c:when test="${(i.index > 0) && (attorneySearchModelBean.orderBy == 5) && (attorneySearchResults[i.index - 1].firmName != attorneySearchResults[i.index].firmName)}">
     						    <tr class="oddone">
	      		 					<td colspan="10"><div class="padtp1 padlt1 padbtm1"><strong>${results.firmName}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(i.index > 0) && (attorneySearchModelBean.orderBy == 7) && (attorneySearchResults[i.index - 1].title != attorneySearchResults[i.index].title)}">
     						    <tr class="oddone">
	      		 					<td colspan="10"><div class="padtp1 padlt1 padbtm1"><strong>${results.practices}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 <c:when test="${(i.index > 0) && (attorneySearchModelBean.orderBy == 8) && (attorneySearchResults[i.index - 1].location != attorneySearchResults[i.index].location)}">
     						    <tr class="oddone">
	      		 					<td colspan="10"><div class="padtp1 padlt1 padbtm1"><strong>${results.location}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(i.index > 0) && (attorneySearchModelBean.orderBy == 9) && (attorneySearchResults[i.index - 1].practices != attorneySearchResults[i.index].practices)}">
     						    <tr class="oddone">
	      		 					<td colspan="10"><div class="padtp1 padlt1 padbtm1"><strong>${results.practices}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 
				</c:choose>
				<%--Group By logic Ends --%>
      		 	
						<tr class="${i.index % 2 == 0 ? 'evenone' : 'oddone'} ">
							<!--  <td align="center"><input type="checkbox" /></td> --> <!-- code commented done by Sachin to fix the bug RER - 305 -->
							<td align="left">
							<a href="${results.attorneyLink}" target="_blank">${results.attorneyName}</a>
							</td>
							<td>
							<a href="${results.firmLink}" target="_blank">${results.firmName}</a>
							</td>
							<td align="center">${results.title}</td>
							<td align="center">${results.location}</td>
							<td>${results.practices}</td>
							<td>
								<c:choose>
				      				 <c:when test="${fn:length(results.education) gt 254}">
				      				 	${fn:substring(results.education, 0, 251)}...
				      				 </c:when>
				      				 <c:otherwise>
				      				 	${results.education} 
				      				 </c:otherwise>		        		  
						        </c:choose>
							</td>
							<td>${results.admission}</td>
							<td>
								<c:choose>
				      				 <c:when test="${fn:length(results.keywords) gt 254}">
				      				 	${fn:substring(results.keywords, 0, 251)}...
				      				 </c:when>
				      				 <c:otherwise>
				      				 	${results.keywords} 
				      				 </c:otherwise>		        		  
						        </c:choose>
							</td>
							<td>${results.contactInfo}</td>
						</tr>
				</c:forEach>
          </c:when>
	           <c:otherwise>
		      		 <tr class="${i.index % 2 == 0 ? 'evenone' : 'oddone'} ">
		      			<td colspan="10" align="center" style="text-align: center;">0 Results, Please try a different search</td>
		      		</tr>     		
	      		</c:otherwise>
      		</c:choose>
        </table>
        
        <div class="PaginationScroll">
		      	  	
		      	  <ul>
					<%
					AttorneySearchModelBean asmb = 	(AttorneySearchModelBean) request.getAttribute("attorneySearchModelBean");
					int pageNumber 	= asmb.getGoToPage();
					int lastPage	= asmb.getLastPage();
						
						//System.out.println("\n\n*** pageNumber = " + pageNumber + "; lastPage = " + lastPage);
						if (lastPage != 1)
						{
						%>
								Page:&nbsp;&nbsp;&nbsp;
						<%
								if (pageNumber > 5)
								{
						%>
									<li class="active"><a href="#" onclick="setPage(<%= 1 %>)"><%= 1 %></a>&nbsp;&nbsp;&nbsp;...&nbsp;&nbsp;&nbsp;</li>
						<%
								}
								for (int i = Math.max(1, pageNumber - 5); i <= Math.min(pageNumber + 5, lastPage); i++)
								{
									if (pageNumber == i)
									{
						%>
									<li><strong><%= i %>&nbsp;&nbsp;&nbsp;</strong> </li>
						<%
									}
									else
									{
						%>
										<li><a href="#" onclick="setPage(<%= i %>)"><%= i %></a>&nbsp;&nbsp;&nbsp;</li>
						<%				
									}
								}
								
								if ((lastPage - pageNumber) > 5)
								{
						%>
									...&nbsp;&nbsp;&nbsp;<li><a href="#" onclick="setPage(<%= lastPage %>)"><%= lastPage %></a></li>
						<%			
								}
						}
						else
						{
						%>
								Page:&nbsp;&nbsp;&nbsp;1
						<%				
						}
						%>
		      	  	
		     	</ul>	
		   		 
   			</div>
        </div>
      </div>
      <!--<div id="tab3"> Hello Analysis </div>--> 
    </div>
    <div class="clear">&nbsp;</div>
    
<form name="exportForm" method="post" id="exportForm" action="<portlet:resourceURL id="exportFile"/>">
	<input type="hidden" name="fileType" value="" id="fileType"/>
  </form>
    
    <script>
    //You have search results with all columns, but hide/display the columns according to the user preference in view settings 
    	toggleSearchTableColumns(); 
    </script>
  <script>
  //Omniture SiteCatalyst - START
  s.prop22 = "premium";
  s.pageName="rer:attorney-search-results";
  s.channel="rer:attorney-search";
  s.server="rer";
  s.prop1="attorney-search"
  s.prop2="attorney-search";
  s.prop3="attorney-search";
  s.prop4="attorney-search";
  s.prop21=s.eVar21='current.user@foo.bar';	
  s.events="event2";
  s.events="event27";
  s.t();
  // Omniture SiteCatalyst - END


  </script>