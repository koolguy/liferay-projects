<%@ taglib prefix="portlet"			uri="http://java.sun.com/portlet_2_0"%>

<portlet:defineObjects/>

<div id="content" style="text-align:center;"> 
    Please click the "Apply" button to view detailed attorney results.
</div>
<div class="clear">&nbsp;</div>
    
<script>
//Omniture SiteCatalyst - START
s.prop22 = "premium";
s.pageName="rer:attorney-search-results";
s.channel="rer:attorney-search";
s.server="rer";
s.prop1="attorney-search"
s.prop2="attorney-search";
s.prop3="attorney-search";
s.prop4="attorney-search";
s.prop21=s.eVar21='current.user@foo.bar';	
s.events="event2";
s.events="event27";
s.t();
// Omniture SiteCatalyst - END
</script>