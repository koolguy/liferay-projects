<%@page import="com.alm.rivaledge.model.chart.BaseChartModelBean"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.alm.rivaledge.model.AttorneySearchModelBean"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="liferay-portlet" uri="http://liferay.com/tld/portlet"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="liferay-util" uri="http://liferay.com/tld/util"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<portlet:defineObjects />

<%@ include file="./common.jsp" %>

<portlet:actionURL var="removePortletURL">
	<portlet:param name="action" value="removePortleFromThePage"/>
</portlet:actionURL>

<style>

.attorneyByGradYearPage h6 
{
	color: #222222;
    font: bold 12px Arial,Helvetica,sans-serif;
    margin: 5px 0 0 10px !important;
}

</style>

<script type="text/javascript">

var <portlet:namespace/>thisPortletId = '<%= themeDisplay.getPortletDisplay().getId() %>';

function removeThisChartAttorneysByGradYear()
{
	var removePortletAction = '<%= removePortletURL.toString()%>';
	var r=confirm("Are you sure you want to delete this chart?");
	if (r == true)
  	{
		Liferay.Portlet.showBusyIcon("#bodyId", "Loading...");
		$("#attorneysByGradYearChartModelBean").attr('action', removePortletAction);
		$("#attorneysByGradYearChartModelBean").submit();
  	}
	else
	{
	  return false;
	}
}

<jsp:include page="theme.html"/>

<c:choose>
<c:when test="${chartType eq 'stacked_horizontal_bar' }">
	var typeOfChartAG = 'bar';	//This will be type of chart.
	var stackingTypeAG = 'normal';
</c:when> 
<c:when test="${chartType eq 'stacked_vertical_bar' }">
	var typeOfChartAG = 'column';	
	var stackingTypeAG = 'normal';
</c:when>
<c:when test="${chartType eq 'horizontal_bar' }">
	var typeOfChartAG = 'bar';
	var stackingTypeAG = '';
</c:when>
<c:otherwise>
	var typeOfChartAG = 'column';
	var stackingTypeAG = '';
</c:otherwise>	
</c:choose>

var isAverageAsked = ${isAverageAsked};
var average = ${attorneyGraduationChartAverage};
var attorneyGradPartner = [${sbPartners}];
var attorneyGradAssociates = [${sbAssociates}];
var attorneyGradOtherCounsel = [${sbCounsels}];

var allValues = new Array();

$(function () 
{
	if(<%=!isHomePage%>)
	{
		if(attorneyGradPartner.length ==0 && attorneyGradAssociates.length==0 && attorneyGradOtherCounsel.length==0)
		{
			$("#p_p_id<portlet:namespace/>").hide();
			$(".cph").show();
			return;
		}
	}
	else
	{
		if(attorneyGradPartner.length ==0 && attorneyGradAssociates.length==0 && attorneyGradOtherCounsel.length==0)
		{
			$("#noDataPercentnewspubsDiv").removeClass("hideClass");
			$("#container-graduation").addClass("hideClass");
			
			$("#No-Data-AttorneyGradYear").addClass("No-Data-Charts");
			$("#attorney_gradYear_settings").hide();
		}
		else
		{
			$("#No-Data-AttorneyGradYear").removeClass("No-Data-Charts");
			$("#noDataPercentnewspubsDiv").addClass("hideClass");
		}
	}
	
	
	// When user clicks reset all button default graph will be shown. 
	$(".settingReset").click(function(){
		$('.graphType').attr('checked', false);
		$('.default').prop('checked', true);
		$("#searchedFirmList").val($("#searchedFirmList option:first").val());
	});
	$(".cancelAG").click(function(){
		$("#percentAttorneysViewSetting").hide();
	});
	Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function(color) {
	    return {
	        radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
	        stops: [
	            [0, color],
	            [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
	        ]
	    };
	});
	
	
	$("#searchedFirmList").click(function(){
		$("#searchedFirmList").change(function(){
			$("input[value=firm]").prop("checked", true);
		});
	});
	
	/* $("#searchResultsFirmList").click(function(){
		if($("#searchResultsFirmList option:selected")){
			$("input[value=firm]").prop("checked", true);
		}
	}); */

	$(".AnalyzeResultHeader").show();
	
	$("#rightChartSettingApply, .closewhite").click(function(){
		//$("#login-box2").hide();
	});
	
	$(".highcharts-button").click(function(){
		$("div#popupPieChartDiv").hide();
		//$("#login-box2").hide();
	});
	$("#attorneyGradYrCloseSetting").click(function(){
		$("#percentAttorneysViewSetting").hide();
	});
	
	$("#rightChartSettingApply").click(function(){
		//$("#login-box2").hide();
		if($('input[name=graphType2]:radio:checked').val() == 'verticalBar')
		{
			displayBarGraph('column');
		} else if($('input[name=graphType2]:radio:checked').val() == 'HorizontalBar')
		{
			displayBarGraph('bar');
		}else if($('input[name=graphType2]:radio:checked').val() == 'pieChart')
		{
			displayPieChart();
		}
	});
	
	$('#attorneyByGradYrPrintCharts').click(function() {
        var chart = $('#container-graduation').highcharts();
        chart.print();
    });
	$('#attorneyByGradYrExportJPG').click(function() {
        var chart = $('#container-graduation').highcharts();
        chart.exportChart({type: 'image/jpeg'});
    });
	$('#attorneyByGradYrExportPNG').click(function() {
        var chart = $('#container-graduation').highcharts();
        chart.exportChart({type: 'image/png'});
    });
	
	var xAxisLabels = [];
	for(var index = 0; index < attorneyGradPartner.length; index++)
	{
		xAxisLabels.push(index);
	}
	// When user clicks on analysis tab active-menu css class is removed from details tab.
	// And same class added to analysis tab.
	$("#attorney-details").removeClass("active-menu");
	$("#attorney-analysis").addClass("active-menu");
	
        $('#container-graduation').highcharts({
            chart: {
                type: typeOfChartAG,
				height: 500,
				width: 1000
            },
            title: {
                text: '${attorneyGraduationChartTitle}',
                style: {
					color: '#000'
				}
            },
		xAxis: {
			categories: xAxisLabels,
    		title: {
				text: '${attorneyGraduationChartXAxisTitle}',
				style: {
                    color: '#000'
                }
			},
			labels : {
				formatter: function()
				{
					
					if(this.value == 0)
					{
						return "Not Available"
					}
					else if(this.value == 1)
					{
						return this.value
					}
					else if(this.value%5 == 0)
					{
						return this.value;
					}
				}
			},
			plotLines : [ {
				color : '#000000',
				width : 2,
				dashStyle : 'Solid',
				value : parseFloat(0.5)
			}
			<c:choose>
		<c:when test="${isAverageAsked eq 'true' }">
			,{
	    		value: average,
	    		width: 2,
	    		color: 'red',
				id: 'plotline-average',
	    		dashStyle: 'solid',
	    		label: {
					useHTML: true,
					align: 'left',
	    			text: '<span class="plotLabel"><b>Avg. Years Since</b></span><b>Graduation: ' + average + '</b>',
					rotation: -360,
					x: 15,
					y: 27,
					style: {
                        border: '1px solid #000'
                    }
	    		}
	    	}
	    	</c:when> 
	    	</c:choose>
	    	]
           },
		yAxis : {
			gridLineWidth: 1,
		    gridLineColor: '#cecece',
		    minorGridLineColor: '#cecece',
		   	lineWidth: 1,
			title: {
				text: '${attorneyGraduationChartYAxisTitle}',
				style: {
                    color: '#000'
                }
			},
			stackLabels : {
				enabled : false,
				style : {
					fontWeight : 'bold',
					color : (Highcharts.theme && Highcharts.theme.textColor)
							|| 'gray'
				}
			}
		},
		legend : {
			enabled : true
		},
		tooltip : {
				hideDelay: 1000,
				useHTML : true,
				positioner : function(boxWidth, boxHeight,
						point) {
						
						var xPosition = point.plotX - 20;
						var yPosition = point.plotY - boxHeight + 20;
						
						if((parseInt(point.plotX) - parseInt(boxWidth)) > parseInt(625))
						{
							xPosition = point.plotX-150;
						}
						
						if(parseInt(point.plotY) < parseInt(boxHeight))
						{
							yPosition = point.plotY+120;
						}
						
					return {
						x : xPosition,
						y : yPosition
					};
				},
				formatter: function()
				{
					var pointerPosition = 0;
					var total = parseInt(attorneyGradPartner[this.x]) + parseInt(attorneyGradAssociates[this.x]) + parseInt(attorneyGradOtherCounsel[this.x]);
					var tooltipOption = '<b>${selectedFirmName}<br/>' + this.x + ' Years Since Graduation </b>';
					tooltipOption +=  '<table><tr><td></td><td>%</td><td>No.</td></tr><tr><td style="color: '+this.series.color+'; padding-left: 10px; margin-top: 15px;">All Attorneys</td><td style="border: 1px solid; background-color: #000; color: #fff; margin-top: 15px;text-align: center;">100%</td><td style="border: 1px solid; background-color: #000; color: #fff; margin-top: 15px;text-align: center;">' + total + '</td></tr><tr><td style="color: #15375c; padding-left: 10px; margin-top: 15px;">Partners</td><td style="border: 1px solid; background-color: #15375c; color: #fff; margin-top: 15px;text-align: center;">' + Math.round(attorneyGradPartner[this.x]/total*100) + '%</td><td style="border: 1px solid; background-color: #15375c; color: #fff; margin-top: 15px;text-align: center;">' + attorneyGradPartner[this.x] + '</td></tr><tr><td style="color: #ba323e; padding-left: 10px; margin-top: 15px;">Associates</td><td style="border: 1px solid; background-color: #ba323e; color: #fff; margin-top: 15px;text-align: center;">' + Math.round(attorneyGradAssociates[this.x]/total*100) + '%</td><td style="border: 1px solid; background-color: #ba323e; color: #fff; margin-top: 15px;text-align: center;">' + attorneyGradAssociates[this.x] + '</td></tr><tr><td style="color: #de7c35; padding-left: 10px; margin-top: 15px;">Other Counsel</td><td style="border: 1px solid; background-color: #de7c35; color: #fff; margin-top: 15px;text-align: center;">' + Math.round(attorneyGradOtherCounsel[this.x]/total*100) + '%</td><td style="border: 1px solid; background-color: #de7c35; color: #fff; margin-top: 15px;text-align: center;">' + attorneyGradOtherCounsel[this.x] + '</td></tr></table>';
					
					var url = "attorney-details?drilldownYear=" + encodeURIComponent(this.key) + "&drilldownFirmName=" + encodeURIComponent('${selectedFirmName}');
					tooltipOption += '<div><span style="text-align:center;color:#000;"><a href="'+url+'" style="color:#000;">Click to View Details</a></span></div>';

					return tooltipOption;
				}
			},
		plotOptions : {
			column: {
				events: {
					legendItemClick: function () {
						return false; 
					}
				}
			},
			series : {
				stacking : stackingTypeAG,
				dataLabels : {
					enabled : false,
					color : '#FFFFFF',
					formatter : function() {
						if (parseInt(this.y) > 0) {
							return this.y;
						}
					}
				},
				point : {
					events : {
						mouseOver : function(e) {
						}
					}

				}
			}
		},
		series: [],
		navigation: {
            buttonOptions: {
                enabled: false
            }
        }
	});
	
	 var chart = $('#container-graduation').highcharts();
	 
	 var maxExp = 0;
	 if(attorneyGradPartner.length > maxExp ){
		maxExp = attorneyGradPartner.length;
	 }
	 if(attorneyGradAssociates.length > maxExp ){
		maxExp = attorneyGradAssociates.length;
	 }
	 if(attorneyGradOtherCounsel.length > maxExp ){
		maxExp = attorneyGradOtherCounsel.length;
	 }
	 if(stackingTypeAG == "normal"){
			chart.addSeries({
				name: 'Partners',
               	data: attorneyGradPartner,
              	color: {
					linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
					stops: [
						[0, '#506a85'],
						[1, '#15375c']
					]
				},
               	pointWidth: 8
      		 });
			 chart.addSeries({
			   name: 'Associates',
               data: attorneyGradAssociates,
               color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#cb676f'],
					[1, '#ba323e']
				]
			},
               pointWidth: 8
      		 });
			 chart.addSeries({
			   name: 'Other Counsel',
               data: attorneyGradOtherCounsel,
               color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#e59c67'],
					[1, '#de7c35']
				]
			},
               pointWidth: 8
      		 });
	 }else{
	 
		 for(var i=0;i<maxExp;i++){
				var a = isNaN(attorneyGradPartner[i]) ? 0 : attorneyGradPartner[i];
				var b =isNaN(attorneyGradAssociates[i]) ? 0 : attorneyGradAssociates[i];
				var c =isNaN(attorneyGradOtherCounsel[i]) ? 0 : attorneyGradOtherCounsel[i];
				var d = a + b + c;
				allValues[i] = d;
			}
			chart.addSeries({
			   name: 'Attorneys',
               data: allValues,
               color: {
				linearGradient: { x1: 0, x2: 0, y1: 0, y1: 1 },
				stops: [
					[0, '#506a85'],
					[1, '#15375c']
				]
			},
               pointWidth: 8
      		 });
	 }
        
	
});


    $(document).ready(function()
    {	
        $("#attorney_gradYear").click(function()
        {
        	Liferay.Portlet.showPopup(
        		{
        			uri : '${chartSearchPortletURL}', // defined in common.jsp
        			title: "Search Criteria"
        		});
        });	
     });

</script>

<portlet:actionURL var="updateChartURL">
	<portlet:param name="action" value="updateChart"/>
</portlet:actionURL>
<div class="attorneyByGradYearPage newspublicationPage">
	<div class="colMin flLeft leftDynamicDiv WideChart">
		<div class="topHeader ForChartsTopHeader" style="width:100%;">
			<span title="Remove this chart" onclick="removeThisChartAttorneysByGradYear();" style="float: right; font-weight: bold; color: rgb(255, 255, 255); cursor: pointer; font-family: verdana; margin: 2px 5px; padding: 3px 8px;">X</span>
		</div>
		<div id="noDataPercentnewspubsDiv" class="hideClass">0 Results, Please try a different search</div>
				
				<form:form  commandName="attorneysByGradYearChartModelBean" method="post" action="${updateChartURL}" id="attorneysByGradYearChartModelBean">
					<div class="flRight charts" id="No-Data-AttorneyGradYear">
					<ul class="reset listView">
						<c:if test="<%=isHomePage%>" >
							<li id="attorney_gradYear" style="overflow:hidden;"><a href="javascript:void(0);" class="filter-icon" >&nbsp;</a></li>
						</c:if>
						<li id="attorney_gradYear_settings" ><a href="#percentAttorneysViewSetting" class="btn icon settingsgry rightViewSetting login-window chartViewSetting" onclick="return false;">&nbsp;</a></li>
						<li>
							<a href="javascript:void(0);" id="attorneyByGradYrPrintCharts" onclick="return false;" class="printChartClass"></a>
						</li>
						<li>
							<a href="javascript:void(0);" onclick="return false;" class="exportChartClass"></a>
                        <div class="actionSec">
                        <h5>Actions</h5>
                        <ul class="reset">
                            <li class="exportChartImage"><span id="attorneyByGradYrExportJPG">Export as JPG</span></li>
                            <li class="exportChartImage"><span id="attorneyByGradYrExportPNG">Export as PNG</span></li>
                        </ul>
                        <div class="clear">&nbsp;</div>
                        </div>
						</li>
					</ul>
							<div style="display: none; top:70px; right:45px;" class="viewBox popusdiv ClickPopup" id="percentAttorneysViewSetting">
                            <div class="popHeader"> <a href="javascript:void(0);" class="btn icon closewhite closeOne flRight" style="margin-top: 2px" id="attorneyGradYrCloseSetting">Close</a> 
                            SETTINGS: ${attorneyGraduationChartSettingsTitle}
                              <div class="clear">&nbsp;</div>
                            </div>
                            <div class="section-one" style="width: 200px;">
                              <h6>Chart Type</h6>
                                <ul class="reset list4" >
                                 <li>
                                    <form:radiobutton path="chartType" class="graphType default" value="<%=BaseChartModelBean.ChartType.STACKED_VERTICAL_BAR.getValue()%>" />
                                    <span class="btn icon stackedbarchart">Stacked Vertical Bar</span></li>
                                  <li >
                                  <form:radiobutton path="chartType" class="graphType" value="<%= BaseChartModelBean.ChartType.STACKED_HORIZONTAL_BAR.getValue() %>" />
                                    <span class="btn icon barcharthori">Stacked Horizontal Bar</span></li>
                                  <li>
                                    <form:radiobutton path="chartType" class="graphType" value="<%=BaseChartModelBean.ChartType.VERTICAL_BAR.getValue()%>" />
                                    <span class="btn icon barchartvert">Vertical Bar</span></li>
                                     <li>
                                    <form:radiobutton path="chartType" class="graphType" value="<%=BaseChartModelBean.ChartType.HORIZONTAL_BAR.getValue()%>" />
                                    <span class="btn icon barcharthori">Horizontal Bar</span></li>
                                </ul>
                                <div class="clear">&nbsp;</div>
                            </div>
                            <div class="section-two" style="width:200px;">
                                <h6>Firm Data (Limit of 1)</h6>
                                <ul class="reset list4">
                                  
                                  <li>
                                    <form:radiobutton path="limitType" value="<%= BaseChartModelBean.FirmDataType.FIRM.getValue() %>" />
                                    <span class="">Selected Firms:</span>
                                     <form:select id="searchedFirmList" path="searchResultsFirmList" multiple="false" size="1" style="width:150px;">
										<form:options items="${allSearchResultsFirmList}"  itemLabel="company" itemValue="companyId"/>
									 </form:select>
                                    </li>
                                    <li>
                                    <form:checkbox path="comparisonDataTypeList" value="<%= BaseChartModelBean.ComparisonDataType.AVERAGE.getValue() %>" />
                                    <span class="marbtm1-last">Average of Firms in Search </span>
                                    </li>
                                </ul>
                                <div class="clear">&nbsp;</div>
                            </div>
							<div class="clear">&nbsp;</div>
							<hr />
                            <div class="section-three"  style="width:100%;">
                              <div class="clear">&nbsp;</div>
                              <div class="btmdiv">
                                <input value="Reset All" class="buttonTwo flLeft settingReset" type="button">
                                <input type="button" class="buttonTwo flRight rightReset cancelAG" value="Cancel">
                                <input style="margin: 0 5px 0 0;" value="Apply" class="buttonOne flRight" id="percentAttorneysApply" type="button" onclick="applyChartSettings('#attorneysByGradYearChartModelBean');">
                                <div class="clear">&nbsp;</div>
                              </div>
                            </div>
                          </div>
                         </div>	 
                        </form:form>
	
			<div id="container-graduation" class="charts-spacing"></div>
	</div>
</div>
