package com.alm.rivaledge.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.portlet.MockRenderRequest;
import org.springframework.mock.web.portlet.MockRenderResponse;
import org.springframework.mock.web.portlet.MockResourceRequest;
import org.springframework.mock.web.portlet.MockResourceResponse;
import org.springframework.web.portlet.ModelAndView;

import com.alm.rivaledge.BaseALMTest;
import com.alm.rivaledge.controller.search.AttorneySearchController;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.transferobject.AttorneyResultDTO;
import com.alm.rivaledge.transferobject.AttorneySearchDTO;
import com.alm.rivaledge.util.ALMConstants;

/**
 * Unit test cases for Attorney Controller
 * 
 * @author FL605
 * @since Sprint 3
 * 
 */
public class AttorneyControllerTest extends BaseALMTest
{
	@Autowired
	private AttorneySearchController	testController;

	/**
	 * Test that the form view returns everything needed for the search.
	 */
	@Test
	@SuppressWarnings("unchecked")
	public void testDefaultFormView()
	{
		printTitle();
		assertNotNull(testController);
		MockRenderRequest testRequest = new MockRenderRequest();
		MockRenderResponse testResponse = new MockRenderResponse();
		ModelAndView testMAV = null;
		try
		{
//			testMAV = testController.attorneySearchForm(testRequest, testResponse);
		}
		catch (Exception ex)
		{
			// Do nothing
		}
		assertTrue(testMAV.getViewName().equals("attorneysearch"));
		assertTrue(testMAV.getModel().keySet().contains("attorneyModel"));

		Map<String, Collection<?>> testModel = (Map<String, Collection<?>>) testMAV.getModel().get("attorneyModel");
		assertTrue(testModel.keySet().contains("allFirms"));
		assertTrue(testModel.keySet().contains("allPracticeArea"));

		Collection<Firm> testFirms = (Collection<Firm>) testModel.get("allFirms");
		assertNotNull(testFirms);
	}

	/**
	 * Test default values of Attorney search Form
	 */
	@Test
	public void testSetOtherSearchParameterWithDefaultValues()
	{
		printTitle();
		AttorneySearchDTO attorneySearchDTO = new AttorneySearchDTO();
		MockResourceRequest testRequest = new MockResourceRequest();
		testRequest.setParameter("selectedPracticeArea", "All Practice Areas");
		testRequest.setParameter("selectedLocation", ALMConstants.ALL_LOCATIONS);
		testRequest.setParameter("selectedTitles", ALMConstants.ALL);
		testRequest.setParameter("selectedLawSchool", "All Schools");
		testRequest.setParameter("selectedAdmissions", "");
		try
		{
//			testController.setOtherSearchParameter(testRequest, attorneySearchDTO);
		}
		catch (Exception ex)
		{
			// Do nothing
		}
		assertNull(attorneySearchDTO.getPracticeArea());
		assertNull(attorneySearchDTO.getLocations());
		assertNull(attorneySearchDTO.getTitles());
		assertNull(attorneySearchDTO.getLawSchooltext());
		assertNull(attorneySearchDTO.getAdmissions());

	}

	/**
	 * Test Attorney search Form values apart form default
	 */
	@Test
	public void testSetOtherSearchParameterWithoutDefaultValues()
	{
		printTitle();
		AttorneySearchDTO attorneySearchDTO = new AttorneySearchDTO();
		MockResourceRequest testRequest = new MockResourceRequest();
		testRequest.setParameter("selectedPracticeArea", "Admiralty, Aviation and Transportation;Aerospace and Defense");
		testRequest.setParameter("selectedLocation", "Boston, MA;");
		testRequest.setParameter("selectedTitles", "Partner;Associate");
		testRequest.setParameter("name", "brown");
		try
		{
//			testController.setOtherSearchParameter(testRequest, attorneySearchDTO);
		}
		catch (Exception ex)
		{
			// Do nothing
		}
		assertTrue(attorneySearchDTO.getPracticeArea().size() == 2);
		assertTrue(attorneySearchDTO.getLocations().size() == 1);
		assertTrue(attorneySearchDTO.getTitles().size() == 2);
		assertTrue(attorneySearchDTO.getAttorneyName().equals("%brown%"));
	}

	/**
	 * Test selected Individual Firms
	 */
	@Test
	public void testSelectedIndividualFirms()
	{
		printTitle();
		AttorneySearchDTO attorneySearchDTO = new AttorneySearchDTO();
		MockResourceRequest testRequest = new MockResourceRequest();
		testRequest.setParameter("selectedFirms", "47,31");
//		testController.setSelectedFirms(testRequest.getParameter("selectedFirms"), attorneySearchDTO, testRequest);
		assertTrue(attorneySearchDTO.getSelectedFirms().size() == 2);
		assertTrue(attorneySearchDTO.getSelectedFirms().get(0).getCompany().equals("DLA Piper"));
		assertTrue(attorneySearchDTO.getSelectedFirms().get(1).getCompany().equals("Chadbourne Parke"));
	}

	/**
	 * Test selected Rivaledge Firms
	 */
	@Test
	public void testSelectedRivalEdgeFirms()
	{
		printTitle();
		AttorneySearchDTO attorneySearchDTO = new AttorneySearchDTO();
		MockResourceRequest testRequest = new MockResourceRequest();
		testRequest.setParameter("selectedFirms", ALMConstants.AMLAW_100);
//		testController.setSelectedFirms(testRequest.getParameter("selectedFirms"), attorneySearchDTO, testRequest);
		assertTrue(attorneySearchDTO.getSelectedFirms().size() > 0);
	}

	/**
	 * Test fetch search results functionality
	 */
	@Test
	public void testApplySearchUrl()
	{
		printTitle();
		MockResourceRequest testRequest = new MockResourceRequest();
		MockResourceResponse testResponse = new MockResourceResponse();

		testRequest.setParameter("selectedPracticeArea", "All Practice Areas");
		testRequest.setParameter("selectedLocation", ALMConstants.ALL_LOCATIONS);
		testRequest.setParameter("selectedTitles", ALMConstants.ALL);
		testRequest.setParameter("selectedLawSchool", "All Schools");
		testRequest.setParameter("selectedAdmissions", "");
		testRequest.setParameter("selectedFirms", ALMConstants.AMLAW_100);
		try
		{
//			testController.applySearchUrl(testRequest, testResponse);
		}
		catch (Exception ex)
		{
			// Do nothing
		}
		assertNotNull(testRequest.getAttribute("attorneyResults"));
		assertNotNull(testRequest.getAttribute("attorneyResultsSize"));
		assertNotNull(testRequest.getAttribute("attorneySearchDTO"));
		assertNotNull(testRequest.getPortletSession().getPortletContext().getRequestDispatcher("/WEB-INF/jsp/attorneysearchresults.jsp"));
	}

	/**
	 * Test Group By functionlity of Attorney controller
	 */
	@Test
	public void testApplyGroupBy()
	{
		printTitle();
		MockResourceRequest testRequest = new MockResourceRequest();
		MockResourceResponse testResponse = new MockResourceResponse();
		List<AttorneyResultDTO> listofAttorneyResultDTO = new ArrayList<AttorneyResultDTO>();
		AttorneyResultDTO attorneyResultDTO1 = new AttorneyResultDTO();
		AttorneyResultDTO attorneyResultDTO2 = new AttorneyResultDTO();
		attorneyResultDTO1.setFirmName("ABC");
		attorneyResultDTO2.setFirmName("BCD");
		listofAttorneyResultDTO.add(attorneyResultDTO1);
		listofAttorneyResultDTO.add(attorneyResultDTO2);
		testRequest.getPortletSession().setAttribute("results", listofAttorneyResultDTO);
		testRequest.setParameter("selectedColumnString", "firmsSetting,titleSetting");
		testRequest.setParameter("selectedsortByString", "Firm");
		try
		{
//			testController.applyGroupBy(testRequest, testResponse);
		}
		catch (Exception ex)
		{
			// Do nothing
		}

	}
	
	@Test
	public void testPrintPreview()
	{
		printTitle();
		assertNotNull(testController);
		
		MockRenderRequest testRequest = new MockRenderRequest();
		MockRenderResponse testResponse = new MockRenderResponse();
		ModelAndView testMAV = null;
		testRequest.setParameter("displayPrintPage", "yes");
		try
		{
//			testMAV = testController.displayPrintPage(testRequest, testResponse);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		assertTrue(testMAV.getViewName().equals("printAttorney"));
	}

}