package com.alm.rivaledge.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.portlet.MockRenderRequest;
import org.springframework.mock.web.portlet.MockRenderResponse;
import org.springframework.mock.web.portlet.MockResourceRequest;
import org.springframework.mock.web.portlet.MockResourceResponse;
import org.springframework.web.portlet.ModelAndView;

import com.alm.rivaledge.BaseALMTest;
import com.alm.watchlist.controller.WatchListController;

public class WatchListControllerTest extends BaseALMTest
{
	@Autowired
	private WatchListController watchListController;
	
	/**
	 * Test that the form view returns firms with number of Attorney ,practice and location count 
	 */
	@Test
	@SuppressWarnings("unchecked")
	public void testDefaultFormView()
	{
		printTitle();
		assertNotNull(watchListController);
		
		MockRenderRequest testRequest = new MockRenderRequest();
		MockRenderResponse testResponse = new MockRenderResponse();
		ModelAndView testMAV = null;
//		try
//		{
//			testMAV = watchListController.myWatchListForm(testRequest, testResponse);
//		}
//		catch (Exception ex)
//		{
//			// Do nothing
//		}
		assertTrue(testMAV.getViewName().equals("mywatchlist"));
		assertTrue(testMAV.getModel().keySet().contains("peopleModel"));

		Map<String, Collection<?>> testModel = (Map<String, Collection<?>>) testMAV.getModel().get("peopleModel");
		assertTrue(testModel.keySet().contains("allPracticeArea"));
		
	}
	
	/**
	 * Test method is used for saving watchlist.
	 */
	@Test
	@SuppressWarnings("unchecked")
	public void testSaveWatchlistURL()
	{
		printTitle();
		assertNotNull(watchListController);
		
		MockResourceRequest testRequest = new MockResourceRequest();
		MockResourceResponse testResponse = new MockResourceResponse();
		
		testRequest.setParameter("newWatchlistFirmName", "TestWatchList");
		
		try 
		{
			watchListController.saveWatchlistURL(testRequest, testResponse);
		} 
		catch(Exception ex)
		{
			// Do nothing
		}
	}
	
	/**
	 * Test method is used for modifying watchlist.
	 */
	@Test
	@SuppressWarnings("unchecked")
	public void testModifyWatchlistURL()
	{
		printTitle();
		assertNotNull(watchListController);
		
		MockResourceRequest testRequest = new MockResourceRequest();
		MockResourceResponse testResponse = new MockResourceResponse();
		
		testRequest.setParameter("oldModifyWatchlistFirmName", "TestWatchList");
		testRequest.setParameter("modifyWatchlistFirmName", "TestWatchList-updated");
		
		try 
		{
			watchListController.modifyWatchlistURL(testRequest, testResponse);
		} 
		catch(Exception ex)
		{
			// Do nothing
		}
		 
	}
	
}
