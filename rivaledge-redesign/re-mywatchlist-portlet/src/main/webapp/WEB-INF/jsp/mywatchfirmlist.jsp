<%@page import="javax.portlet.PortletURL"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<portlet:defineObjects/>
<portlet:resourceURL var="directWatchlistFirmURL" id="directWatchlistFirmURL" />
<portlet:resourceURL var="listAllFirmsURL" id="listAllFirmsURL" />
<portlet:resourceURL var="firmCounterURL" id="firmCounterURL" />

<script>
$(document).ready(function(){

$.expr[':'].containsexactly = function(obj, index, meta, stack) 
{  
    return $(obj).text() === meta[3];
}; 

	$(".firmlistEachCheckBox").click(function () {	
	
		var watchList = $(this).attr('firmname');
		var defaultWatchListFlag = $(this).attr('defaultWatchListFlag');
		
		 $('#popup_box_modified #modifyWatchlistFirmName').val(watchList);
	     $('#popup_box_modified #oldModifyWatchlistFirmName').attr("value", watchList);
	     $('.topHeader #firmHeading').val(watchList);
	     
	    $.ajax({
		    url:"<%=directWatchlistFirmURL.toString()%>",
		    method: "GET",
		    data: {"watchlistName":watchList},
		    success: function(data)
		        {
		    	$("#firmHeadingShow").html(data);
		    	$(".modifyPage .topHeader .showDeleteModify").attr("style", "display:block");
		    	$("#hideFirmsWatchList  a.firmlistEachCheckBox").css("background","");
		    	 $("#hideFirmsWatchList  a.firmlistEachCheckBox").removeClass("active");
		    	 if(defaultWatchListFlag=='Y'){
		    		 $("#defaultWatchlistLink").attr("style", "display:none");
		    		 $("#hideFirmsWatchList  a.firmlistEachCheckBox:containsexactly("+watchList+")").css('background','url("/re-theme/images/star-img.png") no-repeat right center #7F6D41');
		    	 } else{
		    		 $("#defaultWatchlistLink").attr("style", "display:block;background-position: 0 center;float: right; padding: 0 10px 0 20px; width: auto; font-size:11px");
		    		  $("#hideFirmsWatchList  a.firmlistEachCheckBox:containsexactly("+watchList+")").addClass("active");
		    	 }
		    	
				   $.ajax({
				    url:"<%=firmCounterURL.toString()%>",
				    method: "GET",
				    data: {"watchListName":watchList},
				    success: function(data)
				        {
				    	$("#firmCounter").html(data);
				    	 
				        },
				        error: function(jqXHR, textStatus, errorThrown) {
				            //alert("error:" + textStatus + " - exception:" + errorThrown);
				            }
				    });
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            //alert("error:" + textStatus + " - exception:" + errorThrown);
		            }
		    }); 
		
		 $.ajax({
		    url:"<%=listAllFirmsURL.toString()%>",
		    method: "GET",
		    data: {"watchlistName":watchList},
		    success: function(data)
		        {
		    	$("#selectedFirmDataDiv").html(data);
		    	 
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            //alert("error:" + textStatus + " - exception:" + errorThrown);
		            }
		    });
      
	});
	
	$('.active').click(function() {
		var hideText = $('#hideFirmsWatchListText').val();
		if(hideText == '' || hideText == undefined)
		{
			$('#hideFirmsWatchList').hide();
			$('#hideFirmsWatchListText').val('hide')
		}
		else
		{
			$('#hideFirmsWatchList').show();
			$('#hideFirmsWatchListText').val('')
		}

});

});
</script>

<ul id="leftMenu">
	<li><a class="active" href="#">Firms (${allWatchListsSize})</a>
	<div id="hideFirmsWatchList">
		<ul style="display: block">
			<c:forEach var="watchList" items="${allWatchLists}">
				<li><a href="#" class="firmlistEachCheckBox"
					firmname="${watchList.groupName}" defaultWatchListFlag="${watchList.flgDefault}">${watchList.groupName }</a></li>

			</c:forEach>
		</ul></div></li>
</ul>

<div id="watchlistCountDiv"  style="display: none;">Manage Watchlists (${allWatchListsSize})</div>


