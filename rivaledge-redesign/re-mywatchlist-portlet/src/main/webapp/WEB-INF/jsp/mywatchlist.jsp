<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.alm.rivaledge.util.ALMConstants"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>

<portlet:defineObjects/>

<portlet:resourceURL var="saveWatchlistURL" id="saveWatchlistURL" />
<portlet:resourceURL var="modifyWatchlistURL" id="modifyWatchlistURL" />
<portlet:resourceURL var="defaultWatchlistURL" id="defaultWatchlistURL" />
<portlet:resourceURL var="deleteWatchlistURL" id="deleteWatchlistURL" />
<portlet:resourceURL var="directWatchlistFirmURL" id="directWatchlistFirmURL" />
<portlet:resourceURL var="listAllFirmsURL" id="listAllFirmsURL" />
<portlet:resourceURL var="addFirmValuetoBucketURL" id="addFirmValuetoBucketURL" />
<portlet:resourceURL var="removeAddedFirmsURL" id="removeAddedFirmsURL" />
<portlet:resourceURL var="firmCounterURL" id="firmCounterURL" />
<portlet:resourceURL var="showWatchListMsgURL" id="showWatchListMsgURL" />
<portlet:resourceURL var="applySearchFilterURL" id="applySearchFilterURL" />
<portlet:resourceURL var="watchListCounterURL" id="watchListCounterURL" />



<portlet:resourceURL var="filterFirmsByAlphabeticURL" id="filterFirmsByAlphabeticURL" />


<link rel="stylesheet" href="<%=renderRequest.getContextPath()%>/css/jquery-ui.css" />
<!-- 
<script src="<%=renderRequest.getContextPath()%>/js/jquery-1.9.1.js"></script>
<script src="<%=renderRequest.getContextPath()%>/js/jquery-ui.js"></script> 
<script src="<%=renderRequest.getContextPath()%>/js/webwidget_tab.js"></script> 
 -->
<%						

String tabs1 = ParamUtil.getString(request, "tabs1", "Complete Firm List");
PortletURL portletURL = renderResponse.createRenderURL();
portletURL.setParameter("tabs1", tabs1);
String tabNames = "Complete Firm List,Firm Groups/Indices";
%>

<style>

.portlet-layout { background:none !important;}


.ui-autocomplete-category {
	font-weight: bold;
	padding: .2em .4em;
	margin: .8em 0 .2em;
	line-height: 1.5;
}

.ui-autocomplete {
	max-height: 200px;
	width:220px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
}

#allOtherFirmDiv , #practiceAreaDiv {
	/*background-color:#00FFFF;
	width:220px;
	height:150px;
	overflow:scroll;*/
}



</style>

<script language="javascript" type="text/javascript">
    $(function() {
        $(".webwidget_tab").webwidget_tab({
            head_text_color: '#888',
            head_current_text_color: '#222'
        });
    });
</script>

<script type="text/javascript">



function changeOnFirmSize()
{

var allFirmSizeCounter = 0;
$('#allOtherFirmSizeDiv :checked').each(function() {
	 allFirmSizeCounter++;
    });

$("#firmSizeCounter").html('<input type="checkbox" value="firmSize" id="firmSizeSelection"> Firms Size ('+allFirmSizeCounter+') Selected');
	if(allFirmSizeCounter == 0)
	{
		$('#allOtherFirmSize').prop("checked", false);
		$('#allFirmSize').prop("checked", true);
	}
else
	{
		$('#allOtherFirmSize').prop("checked", true);
		$('#allFirmSize').prop("checked", false);
	}
}

function clearFirmSize()
{
	$('#popupFirmSize').find('input[type=checkbox]:checked').removeAttr('checked');
	$('#popupFirmSize').find('input[type=radio]:checked').removeAttr('checked');
	$("#firmSizeCounter").html('<input type="checkbox" value="Locations" id="locationSelection"> Locations (0) Selected');
	
	$('#allFirmSize').prop("checked", true);
	applyFirmSize();
}

function applyFirmSize()
{
	if($("#allFirmSize").is(":checked"))
	{
		$("#selectedFirmSize").val($("#allFirmSize").val());			
	} 
	else
	{
		var allValsFirmSize = [];
		var allFirmSizeCounter = 0;
			
		$('#allOtherFirmSizeDiv input[type=checkbox]:checked').each(function() 
		{ 
			allValsFirmSize.push($(this).val());
			allFirmSizeCounter++;
	    });
		 
		$("#firmSizeCounter").html('<input type="checkbox" value="firmSize" id="firmSizeSelection"> Firms Size ('+allFirmSizeCounter+') Selected');
		if(allValsFirmSize.length > 0)
		{ 
			$("#allOtherFirmSize").prop("checked", true);
	   		$("#selectedFirmSize").val(allValsFirmSize.join(";"));	
		}
	}
}



  $(document).ready(function() {
	
	$('#hide').click(
    function () {
        //show its submenu
        $("#firmPage").stop().slideToggle(500);    
    });
	
});

$(document).ready(function () {
	
	$("#allFirmSize").prop("checked", true);
	
  $('#leftMenu > li > a').click(function(){
    if ($(this).attr('class') != 'active'){
      $('#leftMenu li ul').slideUp();
      $(this).next().slideToggle();
      $('#leftMenu li a').removeClass('active');
      $(this).addClass('active');
    }
  });
});
</script>


<script>
$.widget( "custom.catcomplete", $.ui.autocomplete, {
	_renderMenu: function( ul, items ) {
		var that = this,
		currentCategory = "";
		$.each( items, function( index, item ) {
			if ( item.category != currentCategory ) {
			ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
			currentCategory = item.category;
			}
	  that._renderItemData( ul, item );
	});
}
});

$.expr[':'].containsexactly = function(obj, index, meta, stack) 
{  
    return $(obj).text() === meta[3];
}; 

$.fn.textEquals = function(txt,trim) {
    var text = (trim) ? $.trim($(this).text()) : $(this).text();
    return text == txt;
}

$(function() {

var practiceData = ${practiceJson};
//setting all available locations
var availableLocation = [
                         "Boston, MA",
                         "Dallas, TX",
                         "Denver, CO",
                         "Houston, TX",
                         "Las Vegas, NV",
                         "Los Angeles, CA",
                         "Miami, FL",
                         "New Orleans, LA",
                         "New York, NY",
                         "Orlando, FL",
                         "Philadelphia, PA",
                         "Phoenix, AZ",
                         "Pittsburgh, PA",
                         "San Antonio, TX",
                         "San Francisco, CA",
                         "Seattle, WA",
                         "Tampa, FL",
                         "Washington, DC"                     
                       ];

function split( val ) {
	return val.split( /,\s*/ );
}

function extractLast( term ) {
	return split( term ).pop();
}


$("#selectedLocation")
// don't navigate away from the field on tab when selecting an item
.bind( "keydown", function( event ) {
  if ( event.keyCode === $.ui.keyCode.TAB &&
      $( this ).data( "ui-autocomplete" ).menu.active ) {
    event.preventDefault();
  }
})
.autocomplete({
  minLength: 0,
  source: function( request, response ) {
    // delegate back to autocomplete, but extract the last term
    response( $.ui.autocomplete.filter(
    		availableLocation, extractLast( request.term ) ) );
  },
  focus: function() {
    // prevent value inserted on focus
    return false;
  },
  select: function( event, ui ) {
    var terms = split( this.value );
    // remove the current input
    terms.pop();
    // add the selected item
    terms.push( ui.item.value );
    // add placeholder to get the comma-and-space at the end
    terms.push("");
    this.value = terms.join(";");
    return false;
  }
});


$( "#selectedPracticeArea" )
	//don't navigate away from the field on tab when selecting an item
	.bind( "keydown", function( event ) {
		if ( event.keyCode === $.ui.keyCode.TAB &&
		$( this ).data( "ui-autocomplete" ).menu.active ) {
		event.preventDefault();
		}
	})
	.autocomplete({
	minLength: 3,
	source: function( request, response ) {
		//delegate back to autocomplete, but extract the last term
		response( $.ui.autocomplete.filter(
			practiceData, extractLast( request.term ) ) );
	},
	focus: function() {
		//prevent value inserted on focus
		return false;
	},
	select: function( event, ui ) {
		var terms = split( this.value );
		//remove the current input
		terms.pop();
		//add the selected item
		terms.push( ui.item.value );
		//add placeholder to get the comma-and-space at the end
		terms.push( "" );
		this.value = terms.join( ";" );
		return false;
	}
});

});
</script>

<script>

$(document).ready(function(){

	 $('#hide, #popup').click(function(e){		
		 $("#popupPracticeArea").hide();
		 $("#popupFirmSize").hide();
		 $("#popupLocation").hide();
	     e.stopPropagation();	   
	});  
	 
	 $('#practiceAreaId, #popupPracticeArea').click(function(e){
		 $("#popup").hide(); 
		 $("#popupFirmSize").hide();
		 $("#popupLocation").hide();
	     e.stopPropagation();   
	});
	
	$('#firmSizeId, #popupFirmSize').click(function(e){
		 $("#popup").hide(); 
		 $("#popupPracticeArea").hide();
		 $("#popupLocation").hide();
	     e.stopPropagation();   
	});

	$('#locationId, #popupLocation').click(function(e){
		 $("#popup").hide(); 
		 $("#popupPracticeArea").hide();
		 $("#popupFirmSize").hide();
	     e.stopPropagation();   
	});

	$(document).click(function(){
	    $("#popup").hide(); 
	    $("#popupPracticeArea").hide();
	    $("#popupFirmSize").hide();
	    $("#popupLocation").hide();		
	});
	
	$('#selectedFirmSize').val('<%=ALMConstants.ALL_FIRMS %>');
	
	//Practice Area reset
	$('#selectedPracticeArea').val('All Practice Areas');
	$("#allPracticeArea").prop("checked", true);
	
	//Location reset
	$('#selectedLocation').val('<%=ALMConstants.ALL_LOCATIONS %>');
	$("#allLocations").prop("checked", true);
	
	//Firm Size's toggle off
	$('#firmSizeId').click(
			function () {
			    $("#popupFirmSize").stop().slideToggle(500);    
		}); 
	//Practice Area's toggle off
	$('#practiceAreaId').click(
			function () {
			    $("#popupPracticeArea").stop().slideToggle(500);    
		});
	//Location's toggle off
	$('#locationId').click(
		    function () {
		        $("#popupLocation").stop().slideToggle(500);    
		});
	
	//Function is used for checked all firms checkbox for adding firms into a watchlist
	$('#allFirmChecked').click(function() {
		var checked = $("#allFirmChecked").is(":checked");
		if(checked){   
			 $('#allFirmsData .watchlistAreaCheckBox').prop('checked', true);
		}else{
			 $('#allFirmsData').find('input[type=checkbox]:checked').removeAttr('checked');
		}
	});
	
	//Function is used for checked all firms checkbox for removing firms from a watchlist
	$('#selectedFirmChecked').click(function() {
		var checked = $("#selectedFirmChecked").is(":checked");
		if(checked){   
			 $('#selectedFirmDataDiv #firmBucketId').prop('checked', true);
		}else{
			 $('#selectedFirmDataDiv').find('input[type=checkbox]:checked').removeAttr('checked');
		}
	});
	
<%-- 	//moved
$('.close').click(function() {
		
		var removeFmId = $(this).attr('removefirmId');
		
		 
		 var choice=confirm("Are you sure you want to remove this firm ?");
		 
		 if (choice==true)
		         {
		           
		    	   $.ajax({
					    url:"<%=removeAddedOneFirmURL.toString()%>",
					    method: "GET",
					    data: {"watchListName":$('.topHeader #firmHeading').val(),"removeFmValues":removeFmId,},
					    //data: {"watchListName":$('.topHeader #firmHeading').val(),"removeFmValues":removeFmId,},
					    success: function(data)
					        {
					    	$("#selectedFirmDataDiv").html(data);
					    	
					    	 $.ajax({
								    url:"<%=firmCounterURL.toString()%>",
								    method: "GET",
								    data: {"watchListName":$('.topHeader #firmHeading').val()},
								    success: function(data)
								        {
								    	$("#firmCounter").html(data);
								        },
								        error: function(jqXHR, textStatus, errorThrown) {
								            alert("error:" + textStatus + " - exception:" + errorThrown);
								            }
								    });
					    	 
					        },
					        error: function(jqXHR, textStatus, errorThrown) {
					            alert("error:" + textStatus + " - exception:" + errorThrown);
					            }
					    });
		         }
		       else
		   		{
		    	   return;
		    	}
		    	
		    	//$(this).attr('removeFirmId').val('');
		 
	}); --%>
	
	// Function is used for removing added firms from watchlist
	$('#removeAddedFirms').click(function() {
		
		var val = [];
		    $('#selectedFirmDataDiv #firmBucketId:checked').each(function(i){
		      val[i] = $(this).val();
		    });
		    
		    $('#selectedFirmChecked').prop("checked", false);
		    
		    $('#firmAllValue').val(val);
		    var choice=confirm("Are you sure you want to remove these firms?");
		       
		       if (choice==true)
		         {
		    	   $.ajax({
					    url:"<%=removeAddedFirmsURL.toString()%>",
					    method: "GET",
					   	data: {"firmAddedValues":$("#firmAllValue").val(),"watchListName":$('.topHeader #firmHeading').val()},
					    success: function(data)
					        {
					    	$("#selectedFirmDataDiv").html(data);
					    	$('#selectedFirmDataDiv').find('input[type=checkbox]:checked').removeAttr('checked');
					    	
					    	 $.ajax({
								    url:"<%=firmCounterURL.toString()%>",
								    method: "GET",
								    data: {"watchListName":$('.topHeader #firmHeading').val()},
								    success: function(data)
								        {
								    	$("#firmCounter").html(data);
								    	 
								        },
								        error: function(jqXHR, textStatus, errorThrown) {
								            alert("error:" + textStatus + " - exception:" + errorThrown);
								            }
								    });
					    	 
					        },
					        error: function(jqXHR, textStatus, errorThrown) {
					            alert("error:" + textStatus + " - exception:" + errorThrown);
					            }
					    });
		         }
		       else
		   		{
		    	   return;
		    	}
	});
	
$("#applySearchFilter").click(function () {
		
	var selValue = $('.selectFirmsByFirstCharacter.active').text();
        $.ajax({
		    url:"<%=applySearchFilterURL.toString()%>",
		    method: "GET",
		    data: {"selectedFirmSize":$("#selectedFirmSize").val(),"selectedPracticeArea" : $("#selectedPracticeArea").val(),"selectedLocation" : $("#selectedLocation").val(),
		    	  "firmFirstCharacterRefresh":selValue},
		    success: function(data)
		        {
		    	$("#filteredData").html(data);
		    	$('a[firstChar='+selValue+']').addClass('active');	 
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           	 //alert("error:" + textStatus + " - exception:" + errorThrown);
		            }
		    }); 
});
	
	
	//Function for showing popup for adding watchlist 
	$("#addWatchList").click(function () {
        $('#login-box3').fadeIn("slow");
        $('#newWatchlistFirmName').val('');
    });
	
	//Function for a event when click on firms watchlist. It will show all firms added to Watchlist firm 
	$(".firmlistEachCheckBox").click(function () {
		
		 var watchList = $(this).attr('firmname');
		 var defaultWatchListFlag = $(this).attr('defaultWatchListFlag');
		 $('#login-box3-modified #modifyWatchlistFirmName').val(watchList);
	     $('#login-box3-modified #oldModifyWatchlistFirmName').attr("value", watchList);
	     $('.topHeader #firmHeading').val(watchList);
	     
        $.ajax({
		    url:"<%=directWatchlistFirmURL.toString()%>",
		    method: "GET",
		    data: {"watchlistName":watchList},
		    success: function(data)
		        {
		    	$("#firmHeadingShow").html(data);
		    	 $(".modifyPage .topHeader .showDeleteModify").attr("style", "display:block"); 
		    	 $("#hideFirmsWatchList  a.firmlistEachCheckBox").css("background","");
		    	 $("#hideFirmsWatchList  a.firmlistEachCheckBox").removeClass("active");
		    	 $('a[defaultwatchlistflag=Y]').css('background','url("/re-theme/images/star-img.png") no-repeat 95% center #4D4D4D');
		    	 
		    	 if(defaultWatchListFlag=='Y'){
		    		 $("#defaultWatchlistLink").attr("style", "display:none");
		    		 $("#hideFirmsWatchList  a.firmlistEachCheckBox:containsexactly("+watchList+")").css('background','url("/re-theme/images/star-img.png") no-repeat 95% center #969696');
		    	 } else{
		    		 $("#defaultWatchlistLink").attr("style", "display:block;background-position: 0 center;float: right; padding: 0 10px 0 20px; width: auto; font-size:11px");
		    		  $("#hideFirmsWatchList  a.firmlistEachCheckBox:containsexactly("+watchList+")").addClass("active");
		    	 }
		    	  
		    		
		    	 
		    	
		    	 
		    	  $.ajax({
		  		    url:"<%=firmCounterURL.toString()%>",
		  		    method: "GET",
		  		    data: {"watchListName":watchList},
		  		    success: function(data)
		  		        {
		  		    	$("#firmCounter").html(data);
		  		    	 
		  		        },
		  		        error: function(jqXHR, textStatus, errorThrown) {
		  		            //alert("error:" + textStatus + " - exception:" + errorThrown);
		  		            }
		  		    });
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            //alert("error:" + textStatus + " - exception:" + errorThrown);
		            }
		    }); 
		
        $.ajax({
		    url:"<%=listAllFirmsURL.toString()%>",
		    method: "GET",
		    data: {"watchlistName":watchList},
		    success: function(data)
		        {
		    	$("#selectedFirmDataDiv").html(data);
		    	 
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           // alert("error:" + textStatus + " - exception:" + errorThrown);
		            }
		    });
        
    });
	
	//Function for a event when click on NEW button for adding watchlist
	$(".newMod").click(function () {
        var firmHeadingValue = $('.topHeader #firmHeading').val();
        $('#login-box3-modified #modifyWatchlistFirmName').val(firmHeadingValue);
 	    $('#login-box3-modified #oldModifyWatchlistFirmName').attr("value", firmHeadingValue);
		$('#login-box3-modified').fadeIn("slow"); 
    });
	
	//Function for a event when click on Delete Watchlist button for deletion watchlist
	$(".deleteWatchList").click(function () {
       var delWatchList = $('.topHeader #firmHeading').val();
       
       var choice=confirm("Are you sure you want to delete this watchlist?");
       
       if (choice==true)
         {
    	   $('.topHeader #firmHeading').val('');
           $.ajax({
   		    url:"<%=deleteWatchlistURL.toString()%>",
   		    method: "GET",
   		    data: {"deleteWatchlistFirmName":delWatchList},
   		    success: function(data)
   		        {
   		    	$("#resultsDataDiv").html(data);
   		    	var countInfo=$("#watchlistCountDiv").html();
   		    	$("#manageWatchListLink").html(countInfo);
   		    	
   		    	if(countInfo[19] == 0)
   		    	{
   		    		$("#findSelectAddPage").css("display", "none");
   		    		$(".firmsWatchlist ").css("display", "none");
   		    		$(".topHeader").css("display", "none");
   		    		$("#resultsDataDiv").css("display", "none");
   		    		$(".modifyPage").css("display", "none"); 
   		    		
   		    		$("#showWatchListMsg").css("display", "block");
		    		$("#showWatchListMsgNext").css("display", "block");
		    		$(".Triangular-Edge").css("display", "block");
		    		
		    		
   		    	}
   		    	
   		    	$('#allFirmsData').find('input[type=checkbox]:checked').removeAttr('checked');
   		    	$('#newWatchlistFirmName').val('');
   		    	$("#manageWatchListLink").html(countInfo);

   		    	$('a[defaultwatchlistflag=Y]').css('background','url("/re-theme/images/star-img.png") no-repeat 95% center #969696');
   		    	$('a[defaultwatchlistflag=Y]').trigger('click');
   		    			    	  
   		    	//$('a[defaultwatchlistflag=Y]').css('background','url("/re-theme/images/star-img.png") no-repeat 95% center #969696');  
   		    	//$('a[defaultwatchlistflag=Y]').trigger('click');
   		    	 $.ajax({
							    url:"<%=watchListCounterURL.toString()%>",
							    method: "GET",
							    success: function(data)
							        {
							    		$("#watchListCounter").html(data);
							        },
							        error: function(jqXHR, textStatus, errorThrown) {
							            //alert("error:" + textStatus + " - exception:" + errorThrown);
							            }
							    }); 
   		    	 
   		    	 $.ajax({
   		 		    url:"<%=directWatchlistFirmURL.toString()%>",
   		 		    method: "GET",
   		 		    data: {"watchlistName":''},
   		 		    success: function(data)
   		 		        {
   		 		    
   		 		    	$("#firmHeadingShow").html(data);
   		 		    	//$(".rightPanelDiv").html(data);
   		 		    
   		 		    	$('.modifyPage .topHeader .showDeleteModify').attr("style", "display:none");
   		 		    	$('.topHeader #firmHeading').val('');
   				    	$('#firmHeadingShow .star').val('');
   		 		        },
   		 		        error: function(jqXHR, textStatus, errorThrown) {
   		 		           // alert("error:" + textStatus + " - exception:" + errorThrown);
   		 		            }
   		 		    }); 
   		         
   		          $.ajax({
   		 		    url:"<%=listAllFirmsURL.toString()%>",
   		 		    method: "GET",
   		 		    data: {"watchlistName":delWatchList},
   		 		    success: function(data)
   		 		        {
   		 		    	$("#selectedFirmDataDiv").html(data);
   		 		    	
   		 		        },
   		 		        error: function(jqXHR, textStatus, errorThrown) {
   		 		            //alert("error:" + textStatus + " - exception:" + errorThrown);
   		 		            }
   		 		    }); 
   		          
   		          $.ajax({
   	 		  		    url:"<%=firmCounterURL.toString()%>",
   	 		  		    method: "GET",
   	 		  		    data: {"watchListName":delWatchList},
   	 		  		    success: function(data)
   	 		  		        {
   	 		  		    	$("#firmCounter").html(data);
   	 		  		    	 
   	 		  		        },
   	 		  		        error: function(jqXHR, textStatus, errorThrown) {
   	 		  		            alert("error:" + textStatus + " - exception:" + errorThrown);
   	 		  		            }
   	 		  		    });
   		          
   		    	 $.ajax({
 				    url:"<%=showWatchListMsgURL.toString()%>",
 				    method: "GET",
 				    success: function(data)
 				        {
 				    		$("#showWatchListMsg").html(data);
 				        },
 				        error: function(jqXHR, textStatus, errorThrown) {
 				            alert("error:" + textStatus + " - exception:" + errorThrown);
 				            }
 				    }); 

   		    	 
   		        },
   		        error: function(jqXHR, textStatus, errorThrown) {
   		            alert("error:" + textStatus + " - exception:" + errorThrown);
   		            }
   		    }); 
         }
       else
       {
         return;
       } 
    });
	
	
	//Function for a event when click on Default Watchlist button for deletion watchlist
	$(".defaultWatchlist").click(function () {
       var delWatchList = $('.topHeader #firmHeading').val();
       
       var choice=confirm("Would you like to make this Firm Watch List your default for all searches?");
       
       if (choice==true)
         {
    	   $('.topHeader #firmHeading').val('');
           $.ajax({
   		    url:"<%=defaultWatchlistURL.toString()%>",
   		    method: "GET",
   		    data: {"defaultWatchlistFirmName":delWatchList},
   		    success: function(data)
   		        {
   		    		//alert(delWatchList);
   		    		$("#hideFirmsWatchList  .firmlistEachCheckBox").attr("defaultWatchListFlag", "N");
   		    		
   		    		$("#hideFirmsWatchList  a.firmlistEachCheckBox:containsexactly("+delWatchList+")").attr("defaultWatchListFlag", "Y");
   		    		$("#hideFirmsWatchList  a.firmlistEachCheckBox:containsexactly("+delWatchList+")").css('background','url("/re-theme/images/star-img.png") no-repeat 95% center #969696');
   		    		$("#defaultWatchlistLink").css("display", "none");
   		    		
   		        },
   		        error: function(jqXHR, textStatus, errorThrown) {
   		            alert("error:" + textStatus + " - exception:" + errorThrown);
   		            }
   		    }); 
         }
       else
       {
         return;
       } 
    });
	
	
	$("#addFirmDiv").click(function () {
		if($("#findSelectAddPage").css('display')=='none'){
			$("#findSelectAddPage").css("display", "block");
		} else {
			$("#findSelectAddPage").css("display", "none");
		}
    });
	
	//Function for a event when click on SAVE button in popup for adding watchlist
 	$("#saveWatchlistFirmName").click(function() {	
 		var newWatchListName = $("#newWatchlistFirmName").val();
 		
 		if(newWatchListName == '' || newWatchListName == undefined)
 			{
 				alert('Please enter watchlist name before save.')
 			}
 		else
 			{
			 	$.ajax({
				    url:"<%=saveWatchlistURL.toString()%>",
				    method: "GET",
				    data: {"newWatchlistFirmName":$("#newWatchlistFirmName").val()},
				    success: function(data)
				        {
				    	//$(".modifyPage").html(data);
				    	
				    	$("#resultsDataDiv").html(data);
				    	
	   		    		 $("#findSelectAddPage").css("display", "block");
	   		    		$(".firmsWatchlist ").css("display", "block");
	   		    		$(".topHeader").css("display", "block");
	   		    		$("#resultsDataDiv").css("display", "block");
	   		    		$(".modifyPage").css("display", "block");
	   		    		 
	   		    		$("#showWatchListMsg").css("display", "none");
				    	$("#showWatchListMsgNext").css("display", "none");
				    	$(".Triangular-Edge").css("display", "none"); 
				    	
				    	$('#allFirmsData').find('input[type=checkbox]:checked').removeAttr('checked');
				    	$('#newWatchlistFirmName').val('');
				    	var countInfo=$("#watchlistCountDiv").html();
		   		    	$("#manageWatchListLink").html(countInfo);
				    	
				    	$('#login-box3').fadeOut("slow");
				    	$("#rightPanelShowData").css("display", "block");
				    	
				    	$('a[defaultwatchlistflag=Y]').css('background','url("/re-theme/images/star-img.png") no-repeat 95% center #969696');
				    	$('a[defaultwatchlistflag=Y]').trigger('click');
				    	
				    	$.ajax({
							    url:"<%=watchListCounterURL.toString()%>",
							    method: "GET",
							    success: function(data)
							        {
							    		$("#watchListCounter").html(data);
							        },
							        error: function(jqXHR, textStatus, errorThrown) {
							            alert("error:" + textStatus + " - exception:" + errorThrown);
							            }
							    }); 
				    	 
				    	 $.ajax({
							    url:"<%=showWatchListMsgURL.toString()%>",
							    method: "GET",
							    success: function(data)
							        {
							    		$("#showWatchListMsg").html(data);
							        },
							        error: function(jqXHR, textStatus, errorThrown) {
							            alert("error:" + textStatus + " - exception:" + errorThrown);
							            }
							    }); 
				    	 
				    	 
				        },
				        error: function(jqXHR, textStatus, errorThrown) {
				            alert("error:" + textStatus + " - exception:" + errorThrown);
				            }
				}); 
 			}
	});
 
 	//Function for a event when click on Modify button in popup for modifying watchlist
 	$("#modifyWatchlistButton").click(function() {	
	 var firmModifiedValue = $("#modifyWatchlistFirmName").val();
	 $('.topHeader #firmHeading').val(firmModifiedValue);
		
		if(firmModifiedValue == '' || firmModifiedValue == undefined)
			{
				alert('You can not modify watchlist name with blank. Please enter watchlist name.')
			}
		else
			{
				 $.ajax({
					    url:"<%=modifyWatchlistURL.toString()%>",
					    method: "GET",
					    data: {"modifyWatchlistFirmName":$("#modifyWatchlistFirmName").val(),"oldModifyWatchlistFirmName":$("#login-box3-modified #oldModifyWatchlistFirmName").val()},
					    success: function(data)
					        {
					    	$("#resultsDataDiv").html(data);
					    	 $('#login-box3-modified').fadeOut("slow");
					    	
					        },
					        error: function(jqXHR, textStatus, errorThrown) {
					            alert("error:" + textStatus + " - exception:" + errorThrown);
					            }
					    }); 
				 
			     $.ajax({
					    url:"<%=directWatchlistFirmURL.toString()%>",
					    method: "GET",
					    data: {"watchlistName":firmModifiedValue},
					    success: function(data)
					        {
					    	$("#firmHeadingShow").html(data);
					    	$('.modifyPage .topHeader .showDeleteModify').attr("style", "display:block");
					        },
					        error: function(jqXHR, textStatus, errorThrown) {
					            alert("error:" + textStatus + " - exception:" + errorThrown);
					            }
					    }); 
			}
		
	});
 
	 //Function for a event when click on Cancel button in popup for adding watchlist
	 $("#cancelWatchlistFirmName").click(function() {		
		 $('#login-box3').fadeOut("slow");
	 });
	 
	//Function for a event when click on Cancel button in popup for modifying watchlist
	 $("#cancelWatchlistModifyFirm").click(function() {		
		 $('#login-box3-modified').fadeOut("slow");
	 });
 
 	//Function for adding firms into watchlist when click on Add Marked button
 	$("#addFirmValuetoBucket").click(function() {		
	 	var val = [];
	    $('#allFirmsData .watchlistAreaCheckBox:checked').each(function(i){
	      val[i] = $(this).val();
	    });
	    $('#firmAllValue').val(val);
	    var selectedWatchList=$('.topHeader #firmHeading').val();
	    if(selectedWatchList==null || selectedWatchList==""){	    	
	    	alert("Please select a watchlist");
	    	return;
	    }
	    

	    $.ajax({
		    url:"<%=addFirmValuetoBucketURL.toString()%>",
		    method: "GET",
		    data: {"firmAllValues":$("#firmAllValue").val(),"watchListName":$('.topHeader #firmHeading').val()},
		    success: function(data)
		        {
		    	//$("#selectedFirmDataDiv").append(data);
		    	$("#selectedFirmDataDiv").html(data);
		    	 $('#allFirmsData').find('input[type=checkbox]:checked').removeAttr('checked');
		    	 $.ajax({
		 		    url:"<%=firmCounterURL.toString()%>",
		 		    method: "GET",
		 		    data: {"watchListName":$('.topHeader #firmHeading').val()},
		 		    success: function(data)
		 		        {
		 		    	$("#firmCounter").html(data);
		 		    	 
		 		        },
		 		        error: function(jqXHR, textStatus, errorThrown) {
		 		            alert("error:" + textStatus + " - exception:" + errorThrown);
		 		            }
		 		    });
		    	 
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("error:" + textStatus + " - exception:" + errorThrown);
		            }
		    });
		});
 
 	//Function for adding firms into watchlist when click on Add to List button
 	$(".addToList").click(function() {
 		
 		var selectedWatchList=$('.topHeader #firmHeading').val();
	    if(selectedWatchList==null || selectedWatchList==""){	    	
	    	alert("Please select a watchlist");
	    	return;
	    }
	 $.ajax({
		    url:"<%=addFirmValuetoBucketURL.toString()%>",
		    method: "GET",
		    data: {"firmAllValues":$(this).attr('firmValue'),"firmCompName":$('.topHeader #firmHeading').val()},
		    success: function(data)
		        {
		    	//$("#selectedFirmDataDiv").append(data);
		    	$("#selectedFirmDataDiv").html(data);
		    	 $.ajax({
		 		    url:"<%=firmCounterURL.toString()%>",
		 		    method: "GET",
		 		    data: {"watchListName":$('.topHeader #firmHeading').val()},
		 		    success: function(data)
		 		        {
		 		    	$("#firmCounter").html(data);
		 		    	 
		 		        },
		 		        error: function(jqXHR, textStatus, errorThrown) {
		 		            alert("error:" + textStatus + " - exception:" + errorThrown);
		 		            }
		 		    });
		    	 
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("error:" + textStatus + " - exception:" + errorThrown);
		            }
		    });
	 
	});
 	
 	$(".selectFirmsByFirstCharacter").click(function() { 		
 		 var selectedFilter = $(this).attr('firstChar'); 
 		 $('div.resultBar mart2 marbtm1 *').removeClass('active');
 		 $('body').addClass("modal");
 		 $.ajax({
 			    url:"<%=filterFirmsByAlphabeticURL.toString()%>",
 			    method: "GET",
 			    //data: {"firmFirstCharacter":$("#allFirmsData #selectedFirstFirmCharacter").val()},
 			 	data: {"firmFirstCharacter":$(this).attr('firstChar')},
 			    success: function(data)
 			        {
 			    	 $("#filteredData").html(data);
 			    	$('body').removeClass("modal");
 			    	$('a[firstChar='+selectedFilter+']').addClass('active');
 			        },
 			        error: function(jqXHR, textStatus, errorThrown) {
 			            //alert("error:" + textStatus + " - exception:" + errorThrown);
 			            }
 			    });
	});
	
	$('.active').click(function() {
		var hideText = $('#hideFirmsWatchListText').val();
		if(hideText == '' || hideText == undefined)
		{
			$('#hideFirmsWatchList').hide();
			$('#hideFirmsWatchListText').val('hide')
		}
		else
		{
			$('#hideFirmsWatchList').show();
			$('#hideFirmsWatchListText').val('')
		}
	});
	
	$('.arw').click(function() {
		var hideText = $('#hideWatchListText').val();
		if(hideText == '' || hideText == undefined)
		{
			$('#hideFirmWatch').hide();
			$('#hideWatchListText').val('hide')
		}
		else
		{
			$('#hideFirmWatch').show();
			$('#hideWatchListText').val('')
		}
	});
	
	$('#clearPracticeArea').bind('click', function (event) {
		$('#popupPracticeArea').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#popupPracticeArea').find('input[type=radio]:checked').removeAttr('checked');
		$('#allOtherPracticeArea input').find('input[type=radio]:checked').removeAttr('checked');
		$('#allPracticeArea').prop('checked', true);		
	});
	
	$('#clearButtonFirmSize').bind('click', function (event) {
		clearFirmSize();
	});
	
	
	$('#clearButtonLocations').bind('click', function (event) {
		$('#popupLocation').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#popupLocation').find('input[type=radio]:checked').removeAttr('checked');
		$('#allLocations').prop('checked', true);
		$("#selectedLocation").val('<%=ALMConstants.ALL_LOCATIONS %>');
	});
	

	$("#allOtherFirmSize, #allOtherFirmSizeDiv input[type=checkbox]").change(function(){
			changeOnFirmSize();
		});
	
	//UI Logic for FirmSize
	$("#allFirmSize").change(function(){
		var checked = $("#allFirmSize").is(":checked");
		if(checked){  		
		 $('#allOtherFirmSizeDiv').find('input[type=checkbox]:checked').removeAttr('checked');
		 $('#allOtherFirmSize').removeAttr('checked');
		 $("#firmSizeCounter").html('<input type="checkbox" value="firmSize" id="firmSizeSelection"> Firms Size (0) Selected');
		}
	});
	
/* 	$("input[id='allFirmSizes']").change(function(){
		var checked = $("#allFirmSizes").is(":checked");
		if(checked){   
			$('#ApplyFirmSize').attr("disabled", true);
			$('#ApplyFirmSize').addClass("ui-state-disabled");
		}else{
			$('#ApplyFirmSize').removeAttr('disabled');
			$('#ApplyFirmSize').removeClass("ui-state-disabled");
		}
		
	}); */
	
	$("input[id='allLocations']").change(function(){
		var checked = $("#allLocations").is(":checked");
		if(checked){  		
		 $('#allOtherLocationsDiv').find('input[type=checkbox]:checked').removeAttr('checked');		
		 $('#ApplyLocations').removeAttr('disabled');
		 $('#ApplyLocations').removeClass("ui-state-disabled");
		}
	});
	
	$("input[id='allOtherLocation']").change(function(){
		var checked = $("#allOtherLocation").is(":checked");
		if(checked){   
			$('#ApplyLocations').attr("disabled", true);
			$('#ApplyLocations').addClass("ui-state-disabled");
		}else{
			$('#ApplyLocations').removeAttr('disabled');
			$('#ApplyLocations').removeClass("ui-state-disabled");
		}
		
	});
	
	$("input[id='allPracticeArea']").change(function(){
		var checked = $("#allPracticeArea").is(":checked");
		if(checked){  		
		 $('#allOtherPracticeArea').find('input[type=checkbox]:checked').removeAttr('checked');	
		 $('#ApplyPracticeArea').removeAttr("disabled");
		 $('#ApplyPracticeArea').removeClass("ui-state-disabled");
		}
	});
	
	
/* 	$("input[name='allFirmSizeCheckBox']").change(function(){
		var lengthofCheckBoxes = $("input[name='allFirmSizeCheckBox']:checked").length;
		if(lengthofCheckBoxes>0){
		$('#ApplyFirmSize').removeAttr("disabled");
		$('#ApplyFirmSize').removeClass("ui-state-disabled");
		$('#allFirmSizes').prop("checked", true);
		} else{
		$('#ApplyFirmSize').attr("disabled", true);
		$('#ApplyFirmSize').addClass("ui-state-disabled");
		}
	
	});
	 */
	$("input[name='allLocationsCheckBox']").change(function(){
		var lengthofCheckBoxes = $("input[name='allLocationsCheckBox']:checked").length;
		if(lengthofCheckBoxes>0){
		$('#ApplyLocations').removeAttr("disabled");
		$('#ApplyLocations').removeClass("ui-state-disabled");
		$('#allOtherLocation').prop("checked", true);
		} else{
		$('#ApplyLocations').attr("disabled", true);
		$('#ApplyLocations').addClass("ui-state-disabled");
		}
	
	}); 
	
	$("input[name='practiceAreaCheckBox']").change(function(){
		var lengthofCheckBoxes = $("input[name='practiceAreaCheckBox']:checked").length;
		if(lengthofCheckBoxes>0){
		$('#ApplyPracticeArea').removeAttr("disabled");
		$('#ApplyPracticeArea').removeClass("ui-state-disabled");
		$('#allPracticeArea').prop("checked", false);
		} else{
		$('#ApplyPracticeArea').attr("disabled", true);
		$('#ApplyPracticeArea').addClass("ui-state-disabled");
		}
	
	});
	
	$('#ApplyPracticeArea').bind('click', function (event) {
		
		 if($("#allPracticeArea").is(":checked")){
				$("#selectedPracticeArea").val($("#allPracticeArea").val());			
			} else{
					var allValsPracticeArea = [];
					 $('#practiceAreaDiv :checked').each(function() {
						 allValsPracticeArea.push($(this).val());
					    });
					 
					    $("#selectedPracticeArea").val(allValsPracticeArea.join(";"));		
				   }
			
		 $("#popupPracticeArea").toggle();
	
	});
	
	$('#ApplyLocations').bind('click', function (event) {		
		if($("#allLocations").is(":checked")){
			$("#selectedLocation").val('<%=ALMConstants.ALL_LOCATIONS %>');			
		} else{
				var allValsLocation = [];
				 $('#allOtherLocationsDiv :checked').each(function() {
					 allValsLocation.push($(this).val());
				    });
				    $("#selectedLocation").val(allValsLocation.join(";"));		
			   }
		
		 $("#popupLocation").toggle();	
	});
	
	$('#ApplyFirmSize').bind('click', function (event) {		
		 applyFirmSize();
		 $("#popupFirmSize").toggle();
	});
	
	$('#ApplySelectedFirm').bind('click', function (event) {
		
		var val = [];
	    $('#popupFirmSize  #allFirmSizeCheckBox:checked').each(function(i){
	      val[i] = $(this).val();
	    });
	
		$('#selectedFirmSize').val(val);
		$("#popupFirmSize").toggle();	
	});
	
	$('a[firstChar=A]').addClass('active');	 
	//$('a[defaultwatchlistflag=Y]').addClass('defaultWatchlistSelection'); 
	$('a[defaultwatchlistflag=Y]').css('background','url("/re-theme/images/star-img.png") no-repeat 95% center #969696');
	$('a[defaultwatchlistflag=Y]').trigger('click');

});

</script>
</head>

<body>
<div id="wrapper">
  <div id="maincontent"> 
    <div class="manageWatchSec" >
      <div class="breadcrumbs"><span>Manage Watchlists</span></div>
     <p class="Watchlist-Option-Selection">Select a list to modify or create a new list</p>
      <div class="leftMenuDiv flLeft mywatchTxt" >
 		<div><a class="newClick flRight login-window" href="#login-box3"><span class="add" id="addWatchList">New</span></a> <span id="watchListCounter" class="arw">My Watchlists (${allWatchListsSize})</span> 
         <div class="clear">&nbsp;</div>
		  <div id="login-box3" class="popusdiv login-popupThree">
            <div class="watchlistPage">
               <div class="popHeader"> New Watchlist Name
                <div class="clear">&nbsp;</div>
              </div>
               <div class="popMiddle">
                  Watch List Name
                  <input  type="text"  name="newWatchlistFirmName"  id="newWatchlistFirmName"value="" class="WatchListName" /> 
                  <div class="clear">&nbsp;</div>
                <div class="clear">&nbsp;</div>
              </div>
              <div class="clear"></div>
              <hr>
              <div class="btmdiv" style="margin:0; border:none;">
                <input type="button" name="cancelWatchlistFirmName" id="cancelWatchlistFirmName" value="Cancel" class="buttonTwo flRight" />
                <input type="button" name="saveWatchlistFirmName" id="saveWatchlistFirmName" value="Save"  class="buttonOne flRight" style="margin: 0 5px 0 0;" />
                <div class="clear">&nbsp;</div>
              </div>
            </div>
          </div>
		  
        </div>
        <div id="resultsDataDiv" class="Firms-Listing" style="display:${allWatchListsSize == 0 ? 'none':'block'}">
		  <ul id="leftMenu">
          <%-- <li><a class="active" href="#">Firms (${allWatchListsSize})</a> --%>
          <div id="hideFirmWatch">
          <li><a class="active" href="javascript:void(0);">Firms (${allWatchListsSize})</a>
          <div id="hideFirmsWatchList">
	            <ul style="display:block">
				<!-- <li><a class="active" href="#">Firms Watchlist </a> </li> -->
	           <c:forEach var="watchList"  items="${allWatchLists}">								
						 <li><a href="javascript:void(0)" class="firmlistEachCheckBox" firmname="${watchList.groupName}" defaultWatchListFlag="${watchList.flgDefault}">${watchList.groupName }</a></li>
				</c:forEach>
				</ul>
			</div>
			</li>
			</div>
			</ul>
          </div>
      
      <div class="messageboxSide Watchlist-Btm-Msg" id="showWatchListMsg" style="display:${allWatchListsSize == 0 ? 'block':'none'}">
         <p class="smlTxt">No Watchlists have been created.<br/>
       		Please add New Watchlist using above button.
         </p>
      </div>
      </div>
  
	  <div class="Triangular-Edge" style="display:${allWatchListsSize == 0 ? 'block':'none'}"></div>

	  <div class="messagebox No-Watchlist-Msg" id="showWatchListMsgNext" style="display:${allWatchListsSize == 0 ? 'block':'none'}">
      	<p class="smlTxt">No Watchlists have been created.<br/>
       		Please add a New Watchlist using '+New' button.
       	</p> 
      </div>
      
    <div class="firmsRightDiv flLeft"  id="rightPanelShowData" style="display:${rightPanelStatus}; margin:0 0 0 10px;">
	  <div class="modifyPage" > 
        <!-- <div class="modifyPage" > -->
          <div class="topHeader">
			<div id="firmHeadingShow" class="WatchlistFirmName">
	          <span>${watchlistName}</span> 
	          <input type="hidden" id="firmHeading" value="${watchlistName}" class="WatchListName" />
	          <div class="clear">&nbsp;</div>
	          </div>
          	<div class="showDeleteModify" id="WatchlistFirmName-Actions" style="display:none">
	          <a class="deleteWatchList" href="#">Delete Watchlist</a>
	          <a class="newMod" href="#">Modify Name</a>
	          <a class="defaultWatchlist star" href="#" id="defaultWatchlistLink">Make Default List</a>
	          </div>          
		 </div>
          <div class="firmsWatchlist flLeft" >
            <div id="firmCounter"> <h5>Firms in this Watchlist</h5> </div>
            <div class="marbtm2 martp2">
              <input type="button" value="Removed Marked" class="buttonTwo flLeft" id="removeAddedFirms"/>
              <input type="button" value="Add Firms  <<" class="buttonTwo flRight" id="addFirmDiv"/>
              <div class="clear">&nbsp;</div>
            </div>
            <table width="100%" border="0" cellspacing="0" class="tbleTwo" cellpadding="0">
              <tr>
                <th align="left"><input type="checkbox" id="selectedFirmChecked" />&nbsp; Firm Name</th>
                <th>&nbsp;</th>
              </tr>
              <tr class="even">
             	<td colspan="2">
	                <div id="selectedFirmDataDiv">
	           			<!-- <div> -->
	           			<c:forEach var="bucketFirm"  items="${bucketFirms}">
							 <input  type="checkbox"  id="firmBucketId" value="${bucketFirm.companyId}"> ${bucketFirm.company} 
							 <a href="#" class="close" removefirmId="${bucketFirm.companyId}">&nbsp;</a> 
						</c:forEach>
						<div class="clear"></div>
						<!-- </div> -->
	          		</div>
             	</td>
              </tr>
            </table>
          </div>
          <div class="findSelectAddPage flRight" id="findSelectAddPage">
            <h5>Find and Select Firms to Add Using:</h5>
            <div class="webwidget_tab" id="webwidget_tab">
 
              <div class="tabBody">
                <ul>
                  <li class="tabCot">
                    <div id="dropdown" class="marbtm4">
                      <ul id="droplist" style="width:100%">
              <%--    <li>
					  <label>Firm Size</label>
					   <div class="srchBox">
						<input type="text"  value="<%=ALMConstants.ALL_FIRMS %>" class="input" name="selectedFirmSize" id="selectedFirmSize"  />
						<!-- <input type="button" name="search" value="" class="srchBack" />  -->
						<div class="clear">&nbsp;</div>
					  </div>
 						<input type="button" name="search" value=""  id="firmSizeId" class="drpDwn" />
					<div class="rel">	 
						<div  id="popupFirmSize" class="firmPage">
							<input type="checkbox" name="allFirmSizeCheckBox"  value="<%=ALMConstants.ALL_FIRMS%>"  id="allFirmSizeCheckBox" /><%=ALMConstants.ALL_FIRMS %><br>								 
								  <c:forEach var="firmSizeVal" items="${allFirmSize}">								
									 <input type="checkbox"  name="allFirmSizeCheckBox"  id="allFirmSizeCheckBox" value="${firmSizeVal}">${firmSizeVal}<br>
					  			</c:forEach>					      
				      		 <div class="clear"></div>
				      		 <input type="button" value="Clear All" id="clearButtonFirm">
							<input type="button" value="Apply" id="ApplySelectedFirm">	
						</div>
						  
					</div>
				</li> --%>
				
				 <li>
				  <label>Firm Size</label>
				  <div class="srchBox">
					<input type="text" class="input" name="selectedFirmSize" id="selectedFirmSize"  style="text-overflow:ellipsis;"/>
					<input type="button" name="search" value="" class="srchBack" />
					<div class="clear">&nbsp;</div>
				  </div>
				 <input type="button" name="search" value=""  id="firmSizeId" class="drpDwn" />
			<div class="rel">	 
				<div  id="popupFirmSize" class="firmPage">
					<p><input type="radio" name="firmSize" value="<%=ALMConstants.ALL_FIRM_SIZES%>"  id="allFirmSize" />&nbsp;<%=ALMConstants.ALL_FIRM_SIZES %></p>	
					
					<p><input type="radio" name="firmSize" value="Select Firm Sizes" id="allOtherFirmSize">&nbsp;Select Firm Sizes</p>
					 <div class="individualfirms-first" id="firmSizeCounter">
                		<input type="checkbox" value="Abrams" /> Firms Size (0 Selected)<br />
              		</div>
					<div id="allOtherFirmSizeDiv" class="Select-Individual-Firms" style="border: 1px solid #ccc;">			      
		      		 
		      		  <c:forEach var="firmSizeVal" items="${allFirmSize}">								
							<input type="checkbox" name="firmSizeList" value="${firmSizeVal}"/> ${firmSizeVal}<br>
					  </c:forEach>
					</div>	
					<div class="popupsubmit">				
						<input type="button" class="buttonOne" value="Apply" id="ApplyFirmSize">
						<input type="button" class="buttonTwo" value="Clear All" id="clearButtonFirmSize">
					</div>
				</div>			  
			</div>
			</li>
                <li>
					  <label>Practice Area(s)</label>
						  <div class="srchBox">
							<input type="text"  value="All Practice Areas" class="input" name="selectedPracticeArea" id="selectedPracticeArea" />
							<input type="button" name="search" value="" class="srchBack" />
							<div class="clear">&nbsp;</div>
						  </div>
						 <input type="button" name="search" value=""  id="practiceAreaId" class="typeSel" />
						<div class="rel">
							  <div  id="popupPracticeArea" class="firmPage">					
									<div class="Select-Individual-Firms" id="practiceAreaDiv">
									    <input type="checkbox"  name="allPracticeArea"  id="allPracticeArea" value="All Practice Areas" checked="checked">All Practice Areas<br>
									    <div id="allOtherPracticeArea">
									    <c:forEach var="practice"  items="${peopleModel['allPracticeArea']}">								
											<input type="checkbox"  name="practiceAreaCheckBox" id="practiceAreaCheckBox" value="${practice.practiceArea}"    >${practice.practiceArea}<br>
										</c:forEach>
																			
									 </div>	
									 </div>
                                    <div class="popupsubmit">
                                        <input type="button" id="clearPracticeArea" value="Clear All" class="buttonOne">
                                        <input type="button" id="ApplyPracticeArea" value="Apply" class="buttonTwo">
                                    </div>
								</div>
						</div>
				</li>
                  <li>
					  <label>Location(s)</label>
					  <div class="srchBox">
						<input type="text"  value="<%=ALMConstants.ALL_LOCATIONS %>" class="input" name="selectedLocation" id="selectedLocation"  />
						<input type="button" name="search" value="" class="srchBack" />
						<div class="clear">&nbsp;</div>
					  </div>
					 <input type="button" name="search" value=""  id="locationId" class="typeSel" />
					<div class="rel">	 
						<div  id="popupLocation" class="firmPage">
							<p><input type="radio" name="location" value="<%=ALMConstants.ALL_LOCATIONS%>"  id="allLocations"  checked="checked"/><%=ALMConstants.ALL_LOCATIONS %></p>	
							
							<p><input type="radio" name="location" value="Select Individual Locations" id="allOtherLocation">Select Individual Locations</p>
							<div class="Select-Individual-Firms" id="allOtherLocationsDiv">			      
				      		  <input type="checkbox"  name="allLocationsCheckBox" id="allLocationsCheckBox" value="Boston, MA">Boston, MA<br>	  
				      		  <input type="checkbox"  name="allLocationsCheckBox" id="allLocationsCheckBox" value="Dallas, TX">Dallas, TX<br>	 
				      		  <input type="checkbox"  name="allLocationsCheckBox" id="allLocationsCheckBox" value="Denver, CO">Denver, CO<br>	
				      		  <input type="checkbox"  name="allLocationsCheckBox" id="allLocationsCheckBox" value="Houston, TX">Houston, TX<br>	
				      		  <input type="checkbox"  name="allLocationsCheckBox" id="allLocationsCheckBox" value="Las Vegas, NV">Las Vegas, NV<br>	
				      		  <input type="checkbox"  name="allLocationsCheckBox" id="allLocationsCheckBox" value="Los Angeles, CA">Los Angeles, CA<br>	
				      		  <input type="checkbox"  name="allLocationsCheckBox" id="allLocationsCheckBox" value="Miami, FL">Miami, FL<br>	
				      		  <input type="checkbox"  name="allLocationsCheckBox" id="allLocationsCheckBox" value="New Orleans, LA">New Orleans, LA<br>	
				      		  <input type="checkbox"  name="allLocationsCheckBox" id="allLocationsCheckBox" value="New York, NY">New York, NY<br>	
				      		  <input type="checkbox"  name="allLocationsCheckBox" id="allLocationsCheckBox" value="Orlando, FL">Orlando, FL<br>
				      		  <input type="checkbox"  name="allLocationsCheckBox" id="allLocationsCheckBox" value="Philadelphia, PA">Philadelphia, PA<br>
				      		  <input type="checkbox"  name="allLocationsCheckBox" id="allLocationsCheckBox" value="Phoenix, AZ">Phoenix, AZ<br>	
				      		  <input type="checkbox"  name="allLocationsCheckBox" id="allLocationsCheckBox" value="Pittsburgh, PA">Pittsburgh<br>	
				      		  <input type="checkbox"  name="allLocationsCheckBox" id="allLocationsCheckBox" value="San Antonio, TX">San Antonio, TX<br>	
				      		  <input type="checkbox"  name="allLocationsCheckBox" id="allLocationsCheckBox" value="San Francisco, CA">San Francisco, CA<br>	
				      		  <input type="checkbox"  name="allLocationsCheckBox" id="allLocationsCheckBox" value="Seattle, WA">Seattle, WA<br>
				      		  <input type="checkbox"  name="allLocationsCheckBox" id="allLocationsCheckBox" value="Tampa, FL">Tampa, FL<br>
				      		  <input type="checkbox"  name="allLocationsCheckBox" id="allLocationsCheckBox" value="Washington, DC">Washington, DC<br>      		  
							</div>				
                            <div class="popupsubmit">
                                <input type="button" id="clearButtonLocations" value="Clear All" class="buttonOne">
                                <input type="button" id="ApplyLocations" value="Apply" class="buttonTwo">
                            </div>
						</div>
					</div>
				</li>
				
                        <li>
                          <label>&nbsp;</label>
                          <input type="button" value="Apply" class="buttonApply buttonOne" id="applySearchFilter"/>
                        </li>
                      </ul>
                      <div class="clear">&nbsp;</div>
                    </div>
                   <!--  <div class="resultBar mart2 marbtm1"> -->
                   <div id="filteredData">
	                    <table width="100%" border="0" class="tbleTwo custom-table" cellspacing="0" cellpadding="0" id="allFirmsData">
	                    <div id="firmResultUpdate flLeft">
	                      <input type="button" value="Add Marked" class="buttonTwo flLeft" id="addFirmValuetoBucket"/>
	                      <span class="flLeft marlt2 martp1 Firm-Count"><strong>${watchListFirmCount} Results</strong></span>
	                      </div>
	                      <div id="firmsByAlphabetic flRight"  class="resultBar mart2 marbtm1">
	                      <ul class="reset pagiList flRight">
	                      		<li><a class="selectFirmsByFirstCharacter" firstChar="All" href="#">All</a></li>
	                      		<li><a class="selectFirmsByFirstCharacter" firstChar="A" href="#">A</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="B" href="#">B</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="C" href="#">C</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="D" href="#">D</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="E" href="#">E</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="F" href="#">F</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="G" href="#">G</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="H" href="#">H</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="I" href="#">I</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="J" href="#">J</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="K" href="#">K</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="L" href="#">L</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="M" href="#">M</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="N" href="#">N</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="O" href="#">O</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="P" href="#">P</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="Q" href="#">Q</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="R" href="#">R</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="S" href="#">S</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="T" href="#">T</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="U" href="#">U</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="V" href="#">V</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="W" href="#">W</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="X" href="#">X</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="Y" href="#">Y</a></li>
		                        <li><a class="selectFirmsByFirstCharacter" firstChar="Z" href="#">Z</a></li>
	                       </ul>
	                       </div>
	                       <input type="hidden" id="selectedFirstFirmCharacter" value=""/>
	                      <div class="clear">&nbsp;</div>
	                    <!-- </div> -->
	                    <!-- <table width="100%" border="0" class="tbleTwo" cellspacing="0" cellpadding="0" id="allFirmsData"> -->
	                      <colgroup>
	                      	<col width="40">
                            <col width="300">
                            <col width="150">
                            <col width="150">
                            <col width="100">
                            <col width="100">
                            <col width="100">
	                      </colgroup>
	                      <tr>
	                        <th align="center"><input type="checkbox" id="allFirmChecked"/></th>
	                        <th align="left">Firm Name</th>
	                        <th align="center">No. of Attys</th>
	                        <th align="center">AmLaw Rank</th>
	                        <th align="center">Location</th>
	                        <th align="center">Practice</th>
	                      </tr>
	                      
	                      <% int counter = 0; %>
	                       <c:forEach var="watchList"  items="${watchListFirmResult}">
		                       <%
		                       		if(counter % 2 == 0) {
		                       %>
		                        <tr class="even">							
										<td>	<input  type="checkbox"  class="watchlistAreaCheckBox" id="firmIdValue" name="firmId[]" value="${watchList.watchlistCandidateFirm.companyId}"> </td>
										<td>	${watchList.watchlistCandidateFirm.company} </td>
										<td>	${watchList.attorneyCount} </td>
										<td>	${watchList.watchlistCandidateFirm.amlawRank} </td>
										<td>	${watchList.locationCount} </td>
										<td>	${watchList.practiceCount} </td>
										<td> 	<input type="button" class="addToList" value="Add to List" firmValue="${watchList.watchlistCandidateFirm.companyId}" firmCompanyName="${watchList.watchlistCandidateFirm.company}"/>  </td>
								</tr>
								<% } else { %>
								<tr class="odd">							
										<td>	<input  type="checkbox"  class="watchlistAreaCheckBox" id="firmIdValue" name="firmId[]" value="${watchList.watchlistCandidateFirm.companyId}"> </td>
										<td>	${watchList.watchlistCandidateFirm.company} </td>
										<td>	${watchList.attorneyCount} </td>
										<td>	${watchList.watchlistCandidateFirm.amlawRank} </td>
										<td>	${watchList.locationCount} </td>
										<td>	${watchList.practiceCount} </td>
										<td> 	<input type="button" class="addToList" value="Add to List" firmValue="${watchList.watchlistCandidateFirm.companyId}" firmCompanyName="${watchList.watchlistCandidateFirm.company}"/>  </td>
								</tr>
								<% }
		                       		counter++;
		                       	%>
							</c:forEach>
							
	                    </table>
                    </div>
                  </li>
                  
                </ul>
               
              </div>
            </div>
          </div>
         
        </div>
     
     </div> <!-- show hide when firms not exist -->
    </div>
  </div>
</div>

<div id="login-box3-modified" class="popusdiv login-popupThree">    
            <div class="watchlistPage">
              <div class="popHeader">Modify Watchlist Name
                <div class="clear">&nbsp;</div>
              </div>
              <div class="popMiddle">
                  Watch List Name
                  <input  type="text"  name="modifyWatchlistFirmName"  id="modifyWatchlistFirmName" value=""/>
                  <input  type="hidden"  name="oldModifyWatchlistFirmName"  id="oldModifyWatchlistFirmName" value=""/> 
                  <div class="clear">&nbsp;</div>
                <div class="clear">&nbsp;</div>
              </div>
              <div class="clear"></div>
              <hr>
              <div class="btmdiv" style="margin:0; border:none;">
                <input  type="button"  name="cancelWatchlistModifyFirm" id="cancelWatchlistModifyFirm" value="Cancel"  class="buttonTwo flRight" />
                <input  type="button"  name="modifyWatchlistButton"  id="modifyWatchlistButton" value="Modify"  class="buttonOne flRight" style="margin: 0 5px 0 0;" />                <div class="clear">&nbsp;</div>
              </div>
            </div>
</div>

<input type="hidden" id="firmAllValue" name="firmAllValue"/>
<input type="hidden" id="hideFirmsWatchListText" name="hideFirmsWatchListText"/>
<input type="hidden" id="hideWatchListText" name="hideWatchListText"/>
  
  <!-- Showing Deatils and Analysis tab -->
<%-- 
<liferay-ui:tabs
   names="<%= tabNames %>"
   url="<%= portletURL.toString() %>"
>
	<liferay-ui:section>
		<div id="findSelectAddPage"></div>
	</liferay-ui:section>
	
	<liferay-ui:section>
		<liferay-util:include page="/WEB-INF/jsp/firmgroupindices.jsp"  servletContext="<%=this.getServletContext() %>"></liferay-util:include>
	</liferay-ui:section>
</liferay-ui:tabs>   --%>    
        