<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%> 
<%@page import="javax.portlet.PortletURL"%>

<portlet:defineObjects/>
<portlet:resourceURL var="removeAddedOneFirmURL" id="removeAddedOneFirmURL" />
<portlet:resourceURL var="firmCounterURL" id="firmCounterURL" />

 <script>
 $(document).ready(function(){

$('.close').click(function() {
		
		var removeFmId = $(this).attr('removefirmId');
		 var choice=confirm("Are you sure you want to remove this firm ?");
		 
		 if (choice==true)
		         {
		    	   $.ajax({
					    url:"<%=removeAddedOneFirmURL.toString()%>",
					    method: "GET",
					    //data: {"firmAddedValues":removeFmId,"watchListName":$('.topHeader #firmHeading').val()},
					    data: {"watchListName":$('.topHeader #firmHeading').val(),"removeFmValues":removeFmId,},
					    success: function(data)
					        {
					    	$("#selectedFirmDataDiv").html(data);
					    	
					    	 $.ajax({
								    url:"<%=firmCounterURL.toString()%>",
								    method: "GET",
								    data: {"watchListName":$('.topHeader #firmHeading').val()},
								    success: function(data)
								        {
								    	$("#firmCounter").html(data);
								        },
								        error: function(jqXHR, textStatus, errorThrown) {
								            alert("error:" + textStatus + " - exception:" + errorThrown);
								            }
								    });
					    	 
					        },
					        error: function(jqXHR, textStatus, errorThrown) {
					           // alert("error:" + textStatus + " - exception:" + errorThrown);
					            }
					    });
		         }
		       else
		   		{
		    	   return;
		    	}
		 
	});
	
}); 
	
</script>  
<c:forEach var="bucketFirm" items="${bucketFirms}">
	<br />
	<input type="checkbox" id="firmBucketId"
		value="${bucketFirm.companyId}"> ${bucketFirm.company}
	<a href="#" class="close" removefirmId="${bucketFirm.companyId}">&nbsp;</a>

</c:forEach>
<br/>
<font color=red> Selected firm is already in this Watchlist.</font>

