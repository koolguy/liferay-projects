<%@page import="javax.portlet.PortletURL"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<portlet:defineObjects/>
<portlet:resourceURL var="filterFirmsByAlphabeticURL" id="filterFirmsByAlphabeticURL" />
<portlet:resourceURL var="addFirmValuetoBucketURL" id="addFirmValuetoBucketURL" />
<portlet:resourceURL var="firmCounterURL" id="firmCounterURL" />

 <script>
$(document).ready(function(){


$(".selectFirmsByFirstCharacter").click(function() {


 		 $.ajax({
 			    url:"<%=filterFirmsByAlphabeticURL.toString()%>",
 			    method: "GET",
 			    data: {"firmFirstCharacter":$(this).attr('firstChar')},
 			    success: function(data)
 			        {
 			    	$("#allFirmsData").html(data);
 			    	 $("#firmResultUpdate").html(data);
 			        },
 			        error: function(jqXHR, textStatus, errorThrown) {
 			           // alert("error:" + textStatus + " - exception:" + errorThrown);
 			            }
 			    });
 		 
 		});
 		
 		
 	//Function for adding firms into watchlist when click on Add Marked button
 	$("#addFirmValuetoBucket").click(function() {		
	 	var val = [];
	    $('#allFirmsData .watchlistAreaCheckBox:checked').each(function(i){
	      val[i] = $(this).val();
	    });
	    $('#firmAllValue').val(val);

	    $.ajax({
		    url:"<%=addFirmValuetoBucketURL.toString()%>",
		    method: "GET",
		    data: {"firmAllValues":$("#firmAllValue").val(),"watchListName":$('.topHeader #firmHeading').val()},
		    success: function(data)
		        {
		    	//$("#selectedFirmDataDiv").append(data);
		    	$("#selectedFirmDataDiv").html(data);
		    	 $('#allFirmsData').find('input[type=checkbox]:checked').removeAttr('checked');
		    	 $.ajax({
		 		    url:"<%=firmCounterURL.toString()%>",
		 		    method: "GET",
		 		    data: {"watchListName":$('.topHeader #firmHeading').val()},
		 		    success: function(data)
		 		        {
		 		    	$("#firmCounter").html(data);
		 		    	 
		 		        },
		 		        error: function(jqXHR, textStatus, errorThrown) {
		 		           // alert("error:" + textStatus + " - exception:" + errorThrown);
		 		            }
		 		    });
		    	 
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            //alert("error:" + textStatus + " - exception:" + errorThrown);
		            }
		    });
		});
 
 	//Function for adding firms into watchlist when click on Add to List button
 	$(".addToList").click(function() {	
 	
 	var selectedWatchList=$('.topHeader #firmHeading').val();
	    if(selectedWatchList==null || selectedWatchList==""){	    	
	    	alert("Please select a watchlist");
	    	return;
	    }	
	 $.ajax({
		    url:"<%=addFirmValuetoBucketURL.toString()%>",
		    method: "GET",
		    data: {"firmAllValues":$(this).attr('firmValue'),"firmCompName":$('.topHeader #firmHeading').val()},
		    success: function(data)
		        {
		    	//$("#selectedFirmDataDiv").append(data);
		    	$("#selectedFirmDataDiv").html(data);
		    	 $.ajax({
		 		    url:"<%=firmCounterURL.toString()%>",
		 		    method: "GET",
		 		    data: {"watchListName":$('.topHeader #firmHeading').val()},
		 		    success: function(data)
		 		        {
		 		    	$("#firmCounter").html(data);
		 		    	 
		 		        },
		 		        error: function(jqXHR, textStatus, errorThrown) {
		 		            //alert("error:" + textStatus + " - exception:" + errorThrown);
		 		            }
		 		    });
		    	 
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           // alert("error:" + textStatus + " - exception:" + errorThrown);
		            }
		    });
	 
	});
 		
 	});	

</script> 

<table width="100%" border="0" class="tbleTwo" cellspacing="0" cellpadding="0" id="allFirmsData">
 <div id="firmResultUpdate">
 <input type="button" value="Add Marked" class="buttonTwo flLeft" id="addFirmValuetoBucket"/>
                      <span class="flLeft marlt2 martp1"><strong>${watchListFirmCount} Results</strong></span>
                      </div>
                      <div id="firmsByAlphabetic">
                     	<a class="selectFirmsByFirstCharacter" firstChar="A" href="#">A&nbsp;&nbsp;</a><a class="selectFirmsByFirstCharacter" firstChar="B" href="#">B&nbsp;&nbsp;</a>
                      	<a class="selectFirmsByFirstCharacter" firstChar="C" href="#">C&nbsp;&nbsp;</a><a class="selectFirmsByFirstCharacter" firstChar="D" href="#">D&nbsp;&nbsp;</a>
                      	<a class="selectFirmsByFirstCharacter" firstChar="E" href="#">E&nbsp;&nbsp;</a><a class="selectFirmsByFirstCharacter" firstChar="F" href="#">F&nbsp;&nbsp;</a>
                      	<a class="selectFirmsByFirstCharacter" firstChar="G" href="#">G&nbsp;&nbsp;</a><a class="selectFirmsByFirstCharacter" firstChar="H" href="#">H&nbsp;&nbsp;</a>
                      	<a class="selectFirmsByFirstCharacter" firstChar="I" href="#">I&nbsp;&nbsp;</a><a class="selectFirmsByFirstCharacter" firstChar="J" href="#">J&nbsp;&nbsp;</a>
                      	<a class="selectFirmsByFirstCharacter" firstChar="K" href="#">K&nbsp;&nbsp;</a><a class="selectFirmsByFirstCharacter" firstChar="L" href="#">L&nbsp;&nbsp;</a>
                      	<a class="selectFirmsByFirstCharacter" firstChar="M" href="#">M&nbsp;&nbsp;</a><br/>
                      	<a class="selectFirmsByFirstCharacter" firstChar="N" href="#">N&nbsp;&nbsp;</a><a class="selectFirmsByFirstCharacter" firstChar="O" href="#">O&nbsp;&nbsp;</a>
                      	<a class="selectFirmsByFirstCharacter" firstChar="P" href="#">P&nbsp;&nbsp;</a><a class="selectFirmsByFirstCharacter" firstChar="Q" href="#">Q&nbsp;&nbsp;</a>
                      	<a class="selectFirmsByFirstCharacter" firstChar="R" href="#">R&nbsp;&nbsp;</a><a class="selectFirmsByFirstCharacter" firstChar="S" href="#">S&nbsp;&nbsp;</a>
                      	<a class="selectFirmsByFirstCharacter" firstChar="T" href="#">T&nbsp;&nbsp;</a><a class="selectFirmsByFirstCharacter" firstChar="U" href="#">U&nbsp;&nbsp;</a>
                      	<a class="selectFirmsByFirstCharacter" firstChar="V" href="#">V&nbsp;&nbsp;</a><a class="selectFirmsByFirstCharacter" firstChar="W" href="#">W&nbsp;&nbsp;</a>
                      	<a class="selectFirmsByFirstCharacter" firstChar="X" href="#">X&nbsp;&nbsp;</a><a class="selectFirmsByFirstCharacter" firstChar="Y" href="#">Y&nbsp;&nbsp;</a>
                      	<a class="selectFirmsByFirstCharacter" firstChar="Z" href="#">Z&nbsp;&nbsp;</a>
                       </div>
                      <div class="clear">&nbsp;</div>
                      <col width="35" />
                      <col width="235" />
                      <tr>
                        <th align="center"><input type="checkbox" id="allFirmChecked"/></th>
                        <th align="left">Firm Name</th>
                        <th align="center">No. of Attys</th>
                        <th align="center">AmLaw Rank</th>
                        <th align="center">Location</th>
                        <th align="center">Practice</th>
                      </tr>
                      
                      <% int counter = 0; %>
                       <c:forEach var="watchList"  items="${watchListFirmResult}">
	                       <%
	                       		if(counter % 2 == 0) {
	                       %>
	                        <tr class="even">							
									<td>	<input  type="checkbox"  class="watchlistAreaCheckBox" id="firmIdValue" name="firmId[]" value="${watchList.watchlistCandidateFirm.companyId}"> </td>
									<td>	${watchList.watchlistCandidateFirm.company} </td>
									<td>	${watchList.attorneyCount} </td>
									<td>	${watchList.watchlistCandidateFirm.amlawRank} </td>
									<td>	${watchList.watchlistCandidateFirm.amlawRank} </td>
									<td>	${watchList.practiceCount} </td>
									<td> 	<input type="button" class="addToList" value="Add to List" firmValue="${watchList.watchlistCandidateFirm.companyId}" firmCompanyName="${watchList.watchlistCandidateFirm.company}"/>  </td>
							</tr>
							<% } else { %>
							<tr class="odd">							
									<td>	<input  type="checkbox"  class="watchlistAreaCheckBox" id="firmIdValue" name="firmId[]" value="${watchList.watchlistCandidateFirm.companyId}"> </td>
									<td>	${watchList.watchlistCandidateFirm.company} </td>
									<td>	${watchList.attorneyCount} </td>
									<td>	${watchList.watchlistCandidateFirm.amlawRank} </td>
									<td>	${watchList.watchlistCandidateFirm.amlawRank} </td>
									<td>	${watchList.practiceCount} </td>
									<td> 	<input type="button" class="addToList" value="Add to List" firmValue="${watchList.watchlistCandidateFirm.companyId}" firmCompanyName="${watchList.watchlistCandidateFirm.company}"/>  </td>
							</tr>
							<% }
	                       		counter++;
	                       	%>
						</c:forEach>
						
                    </table>
						


