package com.alm.watchlist.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.portlet.PortletException;
import javax.portlet.PortletRequestDispatcher;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.Practice;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroupCompany;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.service.UserService;
import com.alm.rivaledge.service.WatchlistService;
import com.alm.rivaledge.transferobject.UserGroupCompanyDTO;
import com.alm.rivaledge.transferobject.UserGroupDTO;
import com.alm.rivaledge.transferobject.WatchListFirmResultDTO;
import com.alm.rivaledge.transferobject.WatchlistCriteriaSearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.PracticeJsonUtility;
import com.alm.rivaledge.util.WebUtil;
import com.google.gson.Gson;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.util.PortalUtil;

/**
 * WatchListController is used to handle request for adding/updating/modifying
 * WatchList as well as manage firms for a watchlist
 * 
 * @author Rishikesh Mishra
 * 
 */
@Controller("watchListController")
@RequestMapping(value = "VIEW")
public class WatchListController
{

	@Autowired
	private FirmService			firmService;

	@Autowired
	private WatchlistService	watchlistService;

	@Autowired
	private UserService			userService;

	/**
	 * Render first page with Firms,Locations,practice areas and firm values
	 * with no. of Attorneys,AmLaw Rank,Location and practice area counter .
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RenderMapping
	public ModelAndView myWatchListForm(ModelMap peopleModel,RenderRequest request, RenderResponse response) throws SystemException, PortalException
	{
		Gson gson = new Gson();
		PracticeJsonUtility practiceJsonUtility = null;
		List<PracticeJsonUtility> practiceJsonUtilityList = new ArrayList<PracticeJsonUtility>();
		List<Practice> practiceList = null;
		List<WatchListFirmResultDTO> watchListFirmResultDTO = null;
		List<UserGroupDTO> allWatchLists = null;
		if (firmService != null)
		{
			// Getting all practice area
			practiceList = firmService.getAllPractices();
		}

		for (Practice practice : practiceList)
		{
			practiceJsonUtility = new PracticeJsonUtility();
			practiceJsonUtility.setLabel(practice.getPracticeArea());
			practiceJsonUtilityList.add(practiceJsonUtility);
		}

		String jsonAllPractics = gson.toJson(practiceJsonUtilityList);
		request.setAttribute("practiceJson", jsonAllPractics);
		
		peopleModel.put("allPracticeArea", practiceList);
		peopleModel.put("allFirmSize", ALMConstants.FIRM_SIZE);

		// Get the current logged-in user
		User currentUser = WebUtil.getCurrentUser(request, userService);

		// Do we have a not null service implementation?
		if ((watchlistService != null) && (currentUser != null))
		{
			// watchListFirmResultDTO contains all firms with Attorneys,Location
			// and practice area counter and AmLaw Rank start with 'A' alphabet-
			// default render of Firms
			watchListFirmResultDTO = watchlistService.getFirmsForWatchlist(currentUser, "A%");
			request.setAttribute("watchListFirmResult", watchListFirmResultDTO);
			request.setAttribute("watchListFirmCount", (watchListFirmResultDTO != null) ? watchListFirmResultDTO.size() : 0);
			// allWatchLists contains available firm's watch list
			allWatchLists = (List<UserGroupDTO>) watchlistService.getAllFirmWatchList(currentUser.getId());
		}

		if ((allWatchLists == null) || (allWatchLists.size() == 0))
		{
			request.setAttribute("rightPanelStatus", "none");
			request.setAttribute("allWatchListsSize", 0);
			request.setAttribute("showWatchListMsgToUser", "No Watchlists have been created. Please add a New Watchlist using   '+New' button.");
		}
		else
		{
			request.setAttribute("rightPanelStatus", "block");
			Collections.sort(allWatchLists);
			request.setAttribute("allWatchLists", allWatchLists);
			request.setAttribute("allWatchListsSize", allWatchLists.size());
		}
		return (new ModelAndView("mywatchlist", "peopleModel", peopleModel));
	}

	/**
	 * This method is used for saving a new watch list
	 * 
	 * @param request
	 * @param response
	 * @throws PortletException
	 * @throws IOException
	 */
	@ResourceMapping(value = "saveWatchlistURL")
	public void saveWatchlistURL(ResourceRequest request, ResourceResponse response) throws PortletException, IOException, SystemException, PortalException
	{
		List<UserGroupDTO> allWatchLists = null;
		UserGroupDTO userGroupDTO = new UserGroupDTO();
		Integer watchListId = null;
		String newWatchlistFirmName = request.getParameter("newWatchlistFirmName");
		User currentUser = WebUtil.getCurrentUser(request, userService);
		if (currentUser != null)
		{
			currentUser.setId(currentUser.getId());
			userGroupDTO.setId(currentUser.getId());
		}

		// Do we have a not null service implementation?
		if (watchlistService != null)
		{
			userGroupDTO.setGroupName(newWatchlistFirmName);
			watchListId = watchlistService.getWatchList(userGroupDTO);
			/*
			 * Below code block is checking whether Watchlist has been created
			 * or not ? if Watchlist is not exist then it allow to create else
			 * it render to error message page
			 */
			if (watchListId == null)
			{
				userGroupDTO.setGroupName(newWatchlistFirmName);
				userGroupDTO.setDateAdded(new Date());
				watchlistService.addMyWatchList(userGroupDTO);
			}
			else
			{
				// code block for rendering error page if watchlist exist
				PortletRequestDispatcher dispatcher = request.getPortletSession().getPortletContext().getRequestDispatcher("/WEB-INF/jsp/mywatchfirmlisterror.jsp");
				dispatcher.include(request, response);
			}
			allWatchLists = (List<UserGroupDTO>) watchlistService.getAllFirmWatchList(currentUser.getId());
		}

		if (allWatchLists.size() == 0)
		{
			request.setAttribute("rightPanelStatus", "none");
		}
		else
		{
			request.setAttribute("rightPanelStatus", "block");
			Collections.sort(allWatchLists);
			request.setAttribute("allWatchLists", allWatchLists);
			request.setAttribute("allWatchListsSize", allWatchLists.size());
		}
		PortletRequestDispatcher dispatcher = request.getPortletSession().getPortletContext().getRequestDispatcher("/WEB-INF/jsp/mywatchfirmlist.jsp");
		dispatcher.include(request, response);
	}

	/**
	 * This method is used for modifying available/created watchlist
	 * 
	 * @param request
	 * @param response
	 * @throws PortletException
	 * @throws IOException
	 */
	@ResourceMapping(value = "modifyWatchlistURL")
	public void modifyWatchlistURL(ResourceRequest request, ResourceResponse response) throws PortletException, IOException, SystemException, PortalException
	{
		List<UserGroupDTO> allWatchLists = null;
		UserGroupDTO userGroupDTO = new UserGroupDTO();
		String modifyWatchlistFirmName = request.getParameter("modifyWatchlistFirmName");
		String oldModifyWatchlistFirmName = request.getParameter("oldModifyWatchlistFirmName");
		User currentUser = WebUtil.getCurrentUser(request, userService);

		userGroupDTO.setGroupName(modifyWatchlistFirmName);
		userGroupDTO.setOldGroupName(oldModifyWatchlistFirmName);
		// Do we have a not null service implementation?
		if (watchlistService != null)
		{
			// update existing watchlist
			watchlistService.updateMyWatchList(userGroupDTO);
			allWatchLists = (List<UserGroupDTO>) watchlistService.getAllFirmWatchList(currentUser.getId());
		}
		Collections.sort(allWatchLists);
		request.setAttribute("allWatchLists", allWatchLists);
		request.setAttribute("allWatchListsSize", allWatchLists.size());
		request.setAttribute("watchlistName", modifyWatchlistFirmName);
		PortletRequestDispatcher dispatcher = request.getPortletSession().getPortletContext().getRequestDispatcher("/WEB-INF/jsp/mywatchfirmlist.jsp");
		dispatcher.include(request, response);
	}

	/**
	 * This method is used for deletion existing watchlist
	 * 
	 * @param request
	 * @param response
	 * @throws PortletException
	 * @throws IOException
	 */
	@ResourceMapping(value = "deleteWatchlistURL")
	public void deleteWatchlistURL(ResourceRequest request, ResourceResponse response) throws PortletException, IOException, SystemException, PortalException
	{
		List<UserGroupDTO> allWatchLists = null;
		UserGroupDTO userGroupDTO = new UserGroupDTO();
		UserGroupCompany userGroupCompany = new UserGroupCompany();
		UserGroupCompany userGroupCompanyTemp = new UserGroupCompany();
		Integer userGroupId = null;
		String deleteWatchlistFirmName = request.getParameter("deleteWatchlistFirmName");
		userGroupDTO.setGroupName(deleteWatchlistFirmName);
		User currentUser = WebUtil.getCurrentUser(request, userService);		
		if(currentUser!=null)
		{
			userGroupDTO.setId(currentUser.getId());
		}
		// Do we have a not null service implementation?
		if (watchlistService != null)
		{
			// checking whether watchlist is selected or not for deletion
			if (deleteWatchlistFirmName != null)
			{
				userGroupId = watchlistService.getWatchList(userGroupDTO);
				userGroupCompanyTemp.setGroupId(userGroupId);
				userGroupCompanyTemp.setId((int) currentUser.getId());
				List<UserGroupCompanyDTO> listOfFirmGroupId = watchlistService.getAddedFirms(userGroupCompanyTemp);
				// Below code block remove all firms attached with a WattchList
				for (UserGroupCompanyDTO userGroupCompanyId : listOfFirmGroupId)
				{
					Firm fm = firmService.getFirmById(userGroupCompanyId.getCompanyId());
					// userGroupCompany.setId(USER_ID);
					userGroupCompany.setId((int) currentUser.getId());

					userGroupCompany.setCompanyId(fm.getCompanyId());
					userGroupCompany.setGroupId(userGroupId);
					watchlistService.removeFirmsFromWatchList(userGroupCompany);
				}
				// delete selected watchlist
				watchlistService.deleteMyWatchList(userGroupDTO);
			}

			allWatchLists = (List<UserGroupDTO>) watchlistService.getAllFirmWatchList(currentUser.getId());

		}

		if (allWatchLists.size() == 0)
		{
			request.setAttribute("rightPanelStatus", "none");
			request.setAttribute("allWatchListsSize", allWatchLists.size());
		}
		else
		{
			request.setAttribute("rightPanelStatus", "block");
			Collections.sort(allWatchLists);
			request.setAttribute("allWatchLists", allWatchLists);
			request.setAttribute("allWatchListsSize", allWatchLists.size());
			request.setAttribute("firmsCount", "0");
		}
		PortletRequestDispatcher dispatcher = request.getPortletSession().getPortletContext().getRequestDispatcher("/WEB-INF/jsp/mywatchfirmlist.jsp");
		dispatcher.include(request, response);
	}

	/**
	 * This method is used for displaying watchlist in header tab where we can
	 * perform edit/delete action
	 * 
	 * @param request
	 * @param response
	 * @throws PortletException
	 * @throws IOException
	 */
	@ResourceMapping(value = "directWatchlistFirmURL")
	public void directWatchlistFirmURL(ResourceRequest request, ResourceResponse response) throws PortletException, IOException
	{
		String watchlistName = request.getParameter("watchlistName");
		request.setAttribute("watchlistName", watchlistName);
		PortletRequestDispatcher dispatcher = request.getPortletSession().getPortletContext().getRequestDispatcher("/WEB-INF/jsp/mywatchfirmheader.jsp");
		dispatcher.include(request, response);
	}

	/**
	 * This method is used for displaying all firms
	 * 
	 * @param request
	 * @param response
	 * @throws PortletException
	 * @throws IOException
	 */
	@ResourceMapping(value = "listAllFirmsURL")
	public void listAllFirmsURL(ResourceRequest request, ResourceResponse response) throws PortletException, IOException, SystemException, PortalException
	{
		List<UserGroupCompanyDTO> listOfFirmGroupId = null;
		UserGroupDTO userGroupDTOTemp = new UserGroupDTO();
		UserGroupCompany userGroupCompanyTemp = new UserGroupCompany();
		// Getting logged in user
		User loggedUser = WebUtil.getCurrentUser(request, userService);
		
		String watchlistName = request.getParameter("watchlistName");
		List<Firm> bucketFirms = new ArrayList<Firm>();
		// Do we have a not null service implementation?
		if (watchlistService != null)
		{
			userGroupDTOTemp.setGroupName(watchlistName);
			if (loggedUser != null)
			{
				userGroupDTOTemp.setId(loggedUser.getId());
				userGroupCompanyTemp.setId(loggedUser.getId());
			}
			Integer userGroupDTO = watchlistService.getWatchList(userGroupDTOTemp);
			userGroupCompanyTemp.setGroupId(userGroupDTO);

			// retrieve all added firms for a watchlist
			listOfFirmGroupId = watchlistService.getAddedFirms(userGroupCompanyTemp);
			for (UserGroupCompanyDTO userGroupCompanyId : listOfFirmGroupId)
			{
				Firm fm = firmService.getFirmById(userGroupCompanyId.getCompanyId());
				bucketFirms.add(fm);
			}
		}
		Collections.sort(bucketFirms);
		request.setAttribute("watchlistName", watchlistName);
		request.setAttribute("bucketFirms", bucketFirms);
		request.setAttribute("firmsCount", bucketFirms.size());

		PortletRequestDispatcher dispatcher = request.getPortletSession().getPortletContext().getRequestDispatcher("/WEB-INF/jsp/mywatchfirmheadernext.jsp");
		dispatcher.include(request, response);
	}

	/**
	 * This method is used for adding firms into selected watchlist
	 * 
	 * @param request
	 * @param response
	 * @throws PortletException
	 * @throws IOException
	 */
	@ResourceMapping(value = "addFirmValuetoBucketURL")
	public void addFirmValuetoBucketURL(ResourceRequest request, ResourceResponse response) throws PortletException, IOException, SystemException, PortalException
	{
		List<Firm> bucketFirms = new ArrayList<Firm>();
		List<Integer> bucketFirmsBase = new ArrayList<Integer>();
		List<Integer> bucketFirmsTemp = new ArrayList<Integer>();
		List<UserGroupCompanyDTO> listOfFirmGroupId = null;
		Boolean duplicateEntry = null;

		Integer userGroupDTO = null;
		UserGroupDTO userGroupDTOtemp = new UserGroupDTO();
		UserGroupCompany userGroupCompany = new UserGroupCompany();
		UserGroupCompany userGroupCompanyTemp = new UserGroupCompany();

		String firmIdValue = request.getParameter("firmAllValues");
		String watchListName = request.getParameter("watchListName");
		String firmCompName = request.getParameter("firmCompName");
		StringTokenizer st = new StringTokenizer(firmIdValue, ",");
		
		// Getting logged in user
		User loggedUser = WebUtil.getCurrentUser(request, userService);

		// Do we have a not null service implementation?
		if (watchlistService != null)
		{
			if (loggedUser != null)
			{
				userGroupDTOtemp.setId(loggedUser.getId());
				userGroupCompanyTemp.setId(loggedUser.getId());
			}
			if (firmCompName == null)
			{
				userGroupDTOtemp.setGroupName(watchListName);
				userGroupDTOtemp.setId(loggedUser.getId());
				userGroupDTO = watchlistService.getWatchList(userGroupDTOtemp);
			}
			else
			{
				userGroupDTOtemp.setGroupName(firmCompName);
				userGroupDTOtemp.setId(loggedUser.getId());
				userGroupDTO = watchlistService.getWatchList(userGroupDTOtemp);
			}

			userGroupCompanyTemp.setGroupId(userGroupDTO);

			// getAddedFirms from a watchlist after selected firm deletion
			listOfFirmGroupId = watchlistService.getAddedFirms(userGroupCompanyTemp);

			for (UserGroupCompanyDTO userGroupCompanyId : listOfFirmGroupId)
			{
				Firm fm = firmService.getFirmById(userGroupCompanyId.getCompanyId());
				bucketFirms.add(fm);
				bucketFirmsBase.add(fm.getCompanyId());
			}

			while (st.hasMoreElements())
			{
				Integer it = Integer.valueOf(st.nextElement().toString());
				bucketFirmsTemp.add(it);
			}
			boolean isFirmContain = false;
			for (Integer bucketFirm : bucketFirmsTemp)
			{
				if (bucketFirmsBase.contains(bucketFirm))
				{
					isFirmContain = true;
					break;
				}
			}
			if (isFirmContain == true)
			{
				List<Integer> firmsNeedToAddinWatchlist = new ArrayList<Integer>(bucketFirmsTemp);
				firmsNeedToAddinWatchlist.removeAll(bucketFirmsBase);
				for (Integer userGroupCompanyId : firmsNeedToAddinWatchlist)
				{
					Firm fm = firmService.getFirmById(userGroupCompanyId);
					userGroupCompany.setId(loggedUser.getId());
					userGroupCompany.setCompanyId(fm.getCompanyId());
					userGroupCompany.setDateAdded(new Date());
					userGroupCompany.setGroupId(userGroupDTO);
					// Adding firms into a watchlist
					watchlistService.saveFirmForWatchList(userGroupCompany);
				}
				duplicateEntry = Boolean.TRUE;
			}
			else
			{
				for (Integer userGroupCompanyId : bucketFirmsTemp)
				{

					Firm fm = firmService.getFirmById(userGroupCompanyId);
					userGroupCompany.setId(loggedUser.getId());
					userGroupCompany.setCompanyId(fm.getCompanyId());
					userGroupCompany.setDateAdded(new Date());
					userGroupCompany.setGroupId(userGroupDTO);
					// Adding firms into a watchlist
					watchlistService.saveFirmForWatchList(userGroupCompany);

				}
				duplicateEntry = Boolean.FALSE;
			}
		}

		bucketFirms = new ArrayList<Firm>();
		;
		listOfFirmGroupId = watchlistService.getAddedFirms(userGroupCompanyTemp);
		for (UserGroupCompanyDTO userGroupCompanyId : listOfFirmGroupId)
		{
			Firm fm = firmService.getFirmById(userGroupCompanyId.getCompanyId());
			bucketFirms.add(fm);
		}
		Collections.sort(bucketFirms);
		request.setAttribute("bucketFirms", bucketFirms);

		if (duplicateEntry == Boolean.TRUE)
		{
			PortletRequestDispatcher dispatcher = request.getPortletSession().getPortletContext().getRequestDispatcher("/WEB-INF/jsp/mywatchfirmbucketError.jsp");
			dispatcher.include(request, response);
		}
		else
		{
			PortletRequestDispatcher dispatcher = request.getPortletSession().getPortletContext().getRequestDispatcher("/WEB-INF/jsp/mywatchfirmbucket.jsp");
			dispatcher.include(request, response);
		}
	}

	/**
	 * This method is used for removing multiple added firms from a watchlist
	 * 
	 * @param request
	 * @param response
	 * @throws PortletException
	 * @throws IOException
	 */
	@ResourceMapping(value = "removeAddedFirmsURL")
	public void removeAddedFirmsURL(ResourceRequest request, ResourceResponse response) throws PortletException, IOException, SystemException, PortalException
	{
		Integer userGroupDTO = null;
		List<UserGroupCompanyDTO> listOfFirmGroupId = null;
		UserGroupCompany userGroupCompany = new UserGroupCompany();
		UserGroupCompany userGroupCompanyTemp = new UserGroupCompany();
		List<Firm> bucketFirms = new ArrayList<Firm>();
		UserGroupDTO userGroupDTOtemp = new UserGroupDTO();
		// Getting logged in user
		User loggedUser = WebUtil.getCurrentUser(request, userService);
		String firmIdValue = request.getParameter("firmAddedValues");
		String watchListName = request.getParameter("watchListName");
		StringTokenizer st = new StringTokenizer(firmIdValue, ",");

		// Do we have a not null service implementation?
		if (watchlistService != null)
		{

			if (watchListName != null)
			{
				userGroupDTOtemp.setGroupName(watchListName);
				userGroupDTOtemp.setId(loggedUser.getId());
				userGroupDTO = watchlistService.getWatchList(userGroupDTOtemp);
			}
			// Code block for removing selected firms from a watchlist
			while (st.hasMoreElements())
			{
				Integer it = Integer.valueOf(st.nextElement().toString());
				Firm fm = firmService.getFirmById(it);
				userGroupCompany.setId(loggedUser.getId());
				userGroupCompany.setCompanyId(fm.getCompanyId());
				userGroupCompany.setGroupId(userGroupDTO);
				watchlistService.removeFirmsFromWatchList(userGroupCompany);
			}

			userGroupCompanyTemp.setGroupId(userGroupDTO);
			userGroupCompanyTemp.setId(loggedUser.getId());
			// getAddedFirms from a watchlist after selected firm deletion
			listOfFirmGroupId = watchlistService.getAddedFirms(userGroupCompanyTemp);
		}

		for (UserGroupCompanyDTO userGroupCompanyId : listOfFirmGroupId)
		{
			Firm fm = firmService.getFirmById(userGroupCompanyId.getCompanyId());
			bucketFirms.add(fm);
		}
		Collections.sort(bucketFirms);
		request.setAttribute("bucketFirms", bucketFirms);
		request.setAttribute("firmsCount", bucketFirms.size());
		PortletRequestDispatcher dispatcher = request.getPortletSession().getPortletContext().getRequestDispatcher("/WEB-INF/jsp/mywatchfirmheadernext.jsp");
		dispatcher.include(request, response);
	}

	/**
	 * This method is used for removing single added firms from a watchlist
	 * 
	 * @param request
	 * @param response
	 * @throws PortletException
	 * @throws IOException
	 */
	@ResourceMapping(value = "removeAddedOneFirmURL")
	public void removeAddedOneFirmURL(ResourceRequest request, ResourceResponse response) throws PortletException, IOException, SystemException, PortalException
	{

		HttpServletRequest originalRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(request));
		Integer userGroupDTO = null;
		List<UserGroupCompanyDTO> listOfFirmGroupId = null;
		UserGroupCompany userGroupCompany = new UserGroupCompany();
		UserGroupCompany userGroupCompanyTemp = new UserGroupCompany();
		List<Firm> bucketFirms = new ArrayList<Firm>();
		UserGroupDTO userGroupDTOtemp = new UserGroupDTO();
		// Getting logged in user
		User loggedUser = WebUtil.getCurrentUser(request, userService);

		// temp fix to get value firm id which needs to be deleted
		String removeFmValues = originalRequest.getParameter("removeFmValues");
		String watchListName = request.getParameter("watchListName");
		StringTokenizer st = new StringTokenizer(removeFmValues, ",");

		// Do we have a not null service implementation?
		if (watchlistService != null)
		{

			if (watchListName != null)
			{
				userGroupDTOtemp.setGroupName(watchListName);
				userGroupDTOtemp.setId(loggedUser.getId());
				userGroupDTO = watchlistService.getWatchList(userGroupDTOtemp);
			}
			// Code block for removing selected firms from a watchlist
			while (st.hasMoreElements())
			{
				Integer it = Integer.valueOf(st.nextElement().toString());
				Firm fm = firmService.getFirmById(it);
				userGroupCompany.setId(loggedUser.getId());
				userGroupCompany.setCompanyId(fm.getCompanyId());
				userGroupCompany.setGroupId(userGroupDTO);
				watchlistService.removeFirmsFromWatchList(userGroupCompany);
			}
			userGroupCompanyTemp.setGroupId(userGroupDTO);
			userGroupCompanyTemp.setId(loggedUser.getId());
			// getAddedFirms from a watchlist after selected firm deletion
			listOfFirmGroupId = watchlistService.getAddedFirms(userGroupCompanyTemp);
		}

		for (UserGroupCompanyDTO userGroupCompanyId : listOfFirmGroupId)
		{
			Firm fm = firmService.getFirmById(userGroupCompanyId.getCompanyId());
			bucketFirms.add(fm);
		}
		Collections.sort(bucketFirms);
		request.setAttribute("bucketFirms", bucketFirms);
		request.setAttribute("firmsCount", bucketFirms.size());

		PortletRequestDispatcher dispatcher = request.getPortletSession().getPortletContext().getRequestDispatcher("/WEB-INF/jsp/mywatchfirmheadernext.jsp");
		dispatcher.include(request, response);
	}

	/**
	 * Count total number of added firms into a watchlist
	 * 
	 * @param request
	 * @param response
	 * @throws PortletException
	 * @throws IOException
	 */
	@ResourceMapping(value = "firmCounterURL")
	public void firmCounterURL(ResourceRequest request, ResourceResponse response) throws PortletException, IOException, SystemException, PortalException
	{
		Integer userGroupDTO = null;
		List<UserGroupCompanyDTO> listOfFirmGroupId = null;
		UserGroupDTO userGroupDTOtemp = new UserGroupDTO();
		UserGroupCompany userGroupCompanyTemp = new UserGroupCompany();
		// Getting logged in user
		User loggedUser = WebUtil.getCurrentUser(request, userService);
		String watchListName = request.getParameter("watchListName");

		// Do we have a not null service implementation?
		if (watchlistService != null)
		{
			if (watchListName != null)
			{
				userGroupDTOtemp.setGroupName(watchListName);
				userGroupDTOtemp.setId(loggedUser.getId());

				userGroupDTO = watchlistService.getWatchList(userGroupDTOtemp);
			}
			userGroupCompanyTemp.setGroupId(userGroupDTO);
			userGroupCompanyTemp.setId(loggedUser.getId());
			// get added firms for a watchlist
			listOfFirmGroupId = watchlistService.getAddedFirms(userGroupCompanyTemp);
		}
		request.setAttribute("firmsCount", listOfFirmGroupId.size());
		PortletRequestDispatcher dispatcher = request.getPortletSession().getPortletContext().getRequestDispatcher("/WEB-INF/jsp/firmcounter.jsp");
		dispatcher.include(request, response);

	}

	/**
	 * display message to User what need to do next if Watchlist is available or
	 * not
	 * 
	 * @param request
	 * @param response
	 * @throws PortletException
	 * @throws IOException
	 */

	@ResourceMapping(value = "showWatchListMsgURL")
	public void showWatchListMsgURL(ResourceRequest request, ResourceResponse response) throws PortletException, IOException, SystemException, PortalException
	{
		User currentUser = WebUtil.getCurrentUser(request, userService);

		// allWatchLists contains available firm's watch list
		List<UserGroupDTO> allWatchLists = (List<UserGroupDTO>) watchlistService.getAllFirmWatchList(currentUser.getId());

		if (allWatchLists.size() == 0)
		{
			request.setAttribute("showWatchListMsgToUser", "No Watchlists have been created.Please add New Watchlist using above button.");
		}
		else
		{
			request.setAttribute("showWatchListMsgToUser", "Select a list to modify or create a new list.");
		}

		PortletRequestDispatcher dispatcher = request.getPortletSession().getPortletContext().getRequestDispatcher("/WEB-INF/jsp/showheadingmessage.jsp");
		dispatcher.include(request, response);
	}

	@ResourceMapping(value = "filterFirmsByAlphabeticURL")
	public void filterFirmsByAlphabeticURL(@RequestParam(value = "firmFirstCharacter") String firmFirstChar, ResourceRequest request, ResourceResponse response)
			throws PortletException, IOException, SystemException, PortalException
	{

	/*	String firmFirstCharRefresh = request.getParameter("firmFirstCharacterRefresh");
		firmFirstChar = firmFirstCharRefresh == null ? firmFirstChar : firmFirstCharRefresh;*/
		WatchlistCriteriaSearchDTO  watchlistCriteriaSearchDTO =new WatchlistCriteriaSearchDTO();
		if (firmFirstChar != null && firmFirstChar.contains(ALMConstants.ALL))
		{
			firmFirstChar = null;
		}
		else
		{
			firmFirstChar = (firmFirstChar + "%");
		}

		List<WatchListFirmResultDTO> watchListFirmResultDTO = null;

		watchlistCriteriaSearchDTO.setFirmFilterChar(firmFirstChar);
		setFilters(request,watchlistCriteriaSearchDTO);
		if (watchlistService != null)
		{
			// watchListFirmResultDTO contains all firms with Attorneys,Location
			// and practice area counter and AmLaw Rank
			//watchListFirmResultDTO = watchlistService.getFirmsForWatchlist(currentUser, firmFirstChar);
			watchListFirmResultDTO = watchlistService.getFilteredFirms(watchlistCriteriaSearchDTO);
			request.setAttribute("watchListFirmResult", watchListFirmResultDTO);
			request.setAttribute("watchListFirmCount", watchListFirmResultDTO.size());
		}

		PortletRequestDispatcher dispatcher = request.getPortletSession().getPortletContext().getRequestDispatcher("/WEB-INF/jsp/refreshfirmslist.jsp");
		dispatcher.include(request, response);
	}

	
	/**
	 * This method filter the Firms based on below search criteria.
	 * 1.First Letter of Firm
	 * 2.Firm Size
	 * 3.Location
	 * 4.Practice Area
	 * @param request
	 * @param response
	 * @throws PortletException
	 * @throws IOException
	 * @throws SystemException
	 * @throws PortalException
	 */
	@ResourceMapping(value = "applySearchFilterURL")
	public void applySearchFilterURL(ResourceRequest request, ResourceResponse response) throws PortletException, IOException, SystemException, PortalException
	{		
		WatchlistCriteriaSearchDTO  watchlistCriteriaSearchDTO =new WatchlistCriteriaSearchDTO();
		List<WatchListFirmResultDTO> watchListFirmResultDTO = null;
		setSearchCriteria(request,watchlistCriteriaSearchDTO);	
		if (watchlistService != null)
		{
			// watchListFirmResultDTO contains all firms with Attorneys,Location
			// and practice area counter and AmLaw Rank
			watchListFirmResultDTO = watchlistService.getFilteredFirms(watchlistCriteriaSearchDTO);
			request.setAttribute("watchListFirmResult", watchListFirmResultDTO);
			request.setAttribute("watchListFirmCount", watchListFirmResultDTO.size());
		}
		PortletRequestDispatcher dispatcher = request.getPortletSession().getPortletContext().getRequestDispatcher("/WEB-INF/jsp/refreshfirmslist.jsp");
		dispatcher.include(request, response);
	}

	/**
	 *  This method would set filter parameters(Firm Size,Location,Practice Area,First letter of Firm)  to watchlistCriteriaSearchDTO to filter firms.
	 *  	 *   
	 *  
	 * @param request
	 * @param watchlistCriteriaSearchDTO
	 */
	private void setSearchCriteria(ResourceRequest request,WatchlistCriteriaSearchDTO watchlistCriteriaSearchDTO) 
	{
		String selectedFirmSize = request.getParameter("selectedFirmSize");
		String selectedPracticeArea = request.getParameter("selectedPracticeArea");
		String selectedLocation = request.getParameter("selectedLocation");
		String firmFirstCharRefresh = request.getParameter("firmFirstCharacterRefresh");
		
		if (selectedLocation != null && !selectedLocation.contains(ALMConstants.ALL_LOCATIONS))
		{
			watchlistCriteriaSearchDTO.setLocation(ListUtil.fromArray(StringUtil.split(selectedLocation, ";")));
		}
		if (selectedPracticeArea != null && !selectedPracticeArea.contains(ALMConstants.ALL_PRACTICE_AREAS))
		{
			watchlistCriteriaSearchDTO.setPracticeArea(ListUtil.fromArray(StringUtil.split(selectedPracticeArea, ";")));
		}
		if (selectedFirmSize != null && !selectedFirmSize.contains(ALMConstants.ALL_FIRM_SIZES))
		{
			watchlistCriteriaSearchDTO.setFirmSize(ListUtil.fromArray(StringUtil.split(selectedFirmSize, ";")));
		}
		
		if (firmFirstCharRefresh != null && firmFirstCharRefresh.contains(ALMConstants.ALL))
		{
			firmFirstCharRefresh = null;
		}
		else
		{
			firmFirstCharRefresh = (firmFirstCharRefresh + "%");
		}
		watchlistCriteriaSearchDTO.setFirmFilterChar(firmFirstCharRefresh);		
		
	}

	@ResourceMapping(value = "watchListCounterURL")
	public void watchListCounterURL(ResourceRequest request, ResourceResponse response) throws PortletException, IOException, SystemException, PortalException
	{
		List<UserGroupDTO> allWatchLists = null;

		User currentUser = WebUtil.getCurrentUser(request, userService);

		allWatchLists = (List<UserGroupDTO>) watchlistService.getAllFirmWatchList(currentUser.getId());

		request.setAttribute("allWatchListsSize", allWatchLists.size());

		PortletRequestDispatcher dispatcher = request.getPortletSession().getPortletContext().getRequestDispatcher("/WEB-INF/jsp/totalwatchlist.jsp");
		dispatcher.include(request, response);
	}

	/**
	 * This method set the default watchlist of the user
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws PortletException
	 * @throws IOException
	 * @throws SystemException
	 * @throws PortalException
	 */
	@ResourceMapping(value = "defaultWatchlistURL")
	public String setDefaultWatchList(ResourceRequest request, ResourceResponse response) throws PortletException, IOException, SystemException, PortalException
	{

		UserGroupDTO userGroupDTO = new UserGroupDTO();
		String defaultWatchlistFirmName = request.getParameter("defaultWatchlistFirmName");
		userGroupDTO.setGroupName(defaultWatchlistFirmName);
		
		User currentUser = WebUtil.getCurrentUser(request, userService);

		if (currentUser != null)
		{
			userGroupDTO.setId(currentUser.getId());
		}

		// Do we have a not null service implementation?
		if (watchlistService != null && defaultWatchlistFirmName != null)
		{
			// delete selected watchlist
			watchlistService.setDefaultWatchList(userGroupDTO);
		}
		return defaultWatchlistFirmName;

	}
	
	/**
	 * This method would set filter parameters(Firm Size,Location,Practice Area) to watchlistCriteriaSearchDTO to filter firms
	 * @param request
	 * @param watchlistCriteriaSearchDTO
	 */
	private void setFilters(ResourceRequest request,WatchlistCriteriaSearchDTO watchlistCriteriaSearchDTO)
	{
		String selectedFirmSize = request.getParameter("selectedFirmSize");
		String selectedPracticeArea = request.getParameter("selectedPracticeArea");
		String selectedLocation = request.getParameter("selectedLocation");
		
		if (selectedLocation != null && !selectedLocation.contains(ALMConstants.ALL_LOCATIONS))
		{
			watchlistCriteriaSearchDTO.setLocation(ListUtil.fromArray(StringUtil.split(selectedLocation, ";")));
		}
		if (selectedPracticeArea != null && !selectedPracticeArea.contains(ALMConstants.ALL_PRACTICE_AREAS))
		{
			watchlistCriteriaSearchDTO.setPracticeArea(ListUtil.fromArray(StringUtil.split(selectedPracticeArea, ";")));
		}
		if (selectedFirmSize != null && !selectedFirmSize.contains(ALMConstants.ALL_FIRM_SIZES))
		{
			watchlistCriteriaSearchDTO.setFirmSize(ListUtil.fromArray(StringUtil.split(selectedFirmSize, ";")));
		}
	}

	protected Log	_log	= LogFactoryUtil.getLog(WatchListController.class.getName());

}
