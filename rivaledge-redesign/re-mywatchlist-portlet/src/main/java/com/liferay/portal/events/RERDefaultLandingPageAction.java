package com.liferay.portal.events;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.alm.rivaledge.service.LoginHookUserService;
import com.alm.rivaledge.service.UserService;
import com.alm.rivaledge.util.SpringApplicationContext;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.lar.PortletDataHandlerKeys;
import com.liferay.portal.kernel.lar.UserIdStrategy;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.struts.LastPath;
import com.liferay.portal.kernel.util.PrefsPropsUtil;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.LayoutSet;
import com.liferay.portal.model.LayoutSetPrototype;
import com.liferay.portal.model.User;
import com.liferay.portal.security.auth.PrincipalThreadLocal;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.security.permission.PermissionCheckerFactoryUtil;
import com.liferay.portal.security.permission.PermissionThreadLocal;
import com.liferay.portal.service.LayoutLocalServiceUtil;
import com.liferay.portal.service.LayoutServiceUtil;
import com.liferay.portal.service.LayoutSetPrototypeLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;


public class RERDefaultLandingPageAction extends Action 
{
	//@Autowired
	//LoginHookUserService loginHookUserService;
	
	//@Autowired
	//UserService userService;
	
	public void run(HttpServletRequest request, HttpServletResponse response)
		throws ActionException {

		try {
			doRun(request, response);
		}
		catch (Exception e) {
			throw new ActionException(e);
		}
	}

	protected void doRun(
			HttpServletRequest request, HttpServletResponse response)
		throws Exception {

		long companyId = PortalUtil.getCompanyId(request);

		String path = PrefsPropsUtil.getString(
			companyId, PropsKeys.DEFAULT_LANDING_PAGE_PATH);

		if (_log.isInfoEnabled()) {
			_log.info(
				PropsKeys.DEFAULT_LANDING_PAGE_PATH + StringPool.EQUAL + path);
		}

		if (Validator.isNotNull(path)) {
			LastPath lastPath = new LastPath(
				StringPool.BLANK, path, new HashMap<String, String[]>());

			HttpSession session = request.getSession();

			session.setAttribute(WebKeys.LAST_PATH, lastPath);
		}
		
		//Redirect users to their private page - START
		try
		{
			System.out.println("\n");		
			//Get Liferay User
			HttpSession session = request.getSession();
			User user = UserLocalServiceUtil.getUser(PortalUtil.getUserId(request));
			System.out.println("::::in RERDefaultLandingPage:::::user::"+user.getUserId()+"-"+user.getFullName());
			
			//Set permissionChecker
			PrincipalThreadLocal.setName(user.getUserId());
			PermissionChecker permissionChecker = PermissionCheckerFactoryUtil.create(user, true);
			PermissionThreadLocal.setPermissionChecker(permissionChecker);
			
			//Check if user is not omni admin
			if(!permissionChecker.isOmniadmin())
			{
				HashMap<String, Object> userIds = new HashMap<String, Object>();
				
				//Get spring services
				UserService userService = (UserService)SpringApplicationContext.getBean("userService");
				LoginHookUserService loginHookUserService = (LoginHookUserService)SpringApplicationContext.getBean("loginHookUserService");
				
				//Get ALM User
				com.alm.rivaledge.persistence.domain.lawma0_data.User almUser = WebUtil.getCurrentUser(request, userService);
				if( almUser == null )
				{
					System.out.println("RERDefaultlandingpage::ALMUser is NULL:::");
					return;
				}
				else
				{
					//System.out.println("RERDefaultlandingpage::ALMUser is NOT NULL:::"+almUser.getEmail());
					userIds.put("ALMID", almUser.getId());
				}
				
				userIds.put("LRID", user.getUserId());
				//check if user is exist
				//If exist check if template is applied or not
				//If not then add entry into user mapping table and return false
				boolean templateApplied = loginHookUserService.isUserTemplateApplied(userIds);
				System.out.println(":::RERDefaultLandingPage:::templateApplied:::"+templateApplied);
				//If template is not applied, apply the template
				if(!templateApplied)
				{
					//Get the template ID
					List<LayoutSetPrototype> layoutlist = (List<LayoutSetPrototype>)LayoutSetPrototypeLocalServiceUtil.getLayoutSetPrototypes(-1, -1);
					System.out.println(":::RERDefaultLandingPage:::layoutlist:::"+layoutlist.size());
					if(layoutlist != null && layoutlist.size() > 0 )
					{
						String layoutName = "RER";
						long privateLayoutSetPrototypeId = 0;
						for( LayoutSetPrototype layout : layoutlist )
						{
							layoutName = layout.getName(PortalUtil.getLocale(request));
							//System.out.println("::: LayoutName::"+layoutName);
							if(layoutName.equalsIgnoreCase("RER"))
							{
								privateLayoutSetPrototypeId = layout.getLayoutSetPrototypeId();
								System.out.println("::: privateLayoutSetPrototypeId ::"+privateLayoutSetPrototypeId);
							}
						}
	
						//Apply default layout to user
						if (privateLayoutSetPrototypeId > 0) 
						{
							LayoutSetPrototype layoutSetPrototype =
								LayoutSetPrototypeLocalServiceUtil.getLayoutSetPrototype(
									privateLayoutSetPrototypeId);
	
							LayoutSet privateLayoutSet = user.getGroup().getPrivateLayoutSet();
	
							Map<String, String[]> parameterMap = getLayoutSetPrototypeParameters();
	
							File file = LayoutLocalServiceUtil.exportLayoutsAsFile(
									layoutSetPrototype.getLayoutSet().getGroupId(), layoutSetPrototype.getLayoutSet().isPrivateLayout(),
								null, parameterMap, null, null);
	
							try 
							{
								LayoutServiceUtil.importLayouts(
										privateLayoutSet.getGroupId(), privateLayoutSet.isPrivateLayout(),
									parameterMap, file);
							}
							finally 
							{
								file.delete();
							}
							
							//Update mapping to set template applied to TRUE
							loginHookUserService.updateUserMapping(almUser.getId());
						}
					}	
				}
				//Set the default landing page path
				LastPath lastPath = null;
				lastPath = new LastPath(request.getContextPath(),PropsUtil.get(PropsKeys.LAYOUT_FRIENDLY_URL_PRIVATE_USER_SERVLET_MAPPING)
					+ StringPool.FORWARD_SLASH + user.getScreenName() + StringPool.FORWARD_SLASH );
				
				System.out.println(":::RERDefaultLandingPage:::lastPath::"+lastPath);
				session.setAttribute(WebKeys.LAST_PATH, lastPath);
				
				response.sendRedirect(lastPath.getContextPath()+lastPath.getPath());
			}
		}
		catch(Exception e)
		{
			System.out.println("RERDefaultLandingPageAction::Error in setting template for user::"+e.getMessage());
			e.printStackTrace();
		}
		//Redirect users to their private page - END
		
		// The commented code shows how you can programmaticaly set the user's
		// landing page. You can modify this class to utilize a custom algorithm
		// for forwarding a user to his landing page. See the references to this
		// class in portal.properties.

		/*Map<String, String[]> params = new HashMap<String, String[]>();

		params.put("p_l_id", new String[] {"1806"});

		LastPath lastPath = new LastPath("/c", "/portal/layout", params);

		session.setAttribute(WebKeys.LAST_PATH, lastPath);*/
	}
	
	public static Map<String, String[]> getLayoutSetPrototypeParameters() {
		Map<String, String[]> parameterMap =
			new LinkedHashMap<String, String[]>();

		parameterMap.put(
			PortletDataHandlerKeys.CATEGORIES,
			new String[] {Boolean.TRUE.toString()});
		parameterMap.put(
			PortletDataHandlerKeys.DATA_STRATEGY,
			new String[] {PortletDataHandlerKeys.DATA_STRATEGY_MIRROR});
		parameterMap.put(
			PortletDataHandlerKeys.DELETE_MISSING_LAYOUTS,
			new String[] {Boolean.TRUE.toString()});
		parameterMap.put(
			PortletDataHandlerKeys.DELETE_PORTLET_DATA,
			new String[] {Boolean.FALSE.toString()});
		parameterMap.put(
			PortletDataHandlerKeys.PERMISSIONS,
			new String[] {Boolean.TRUE.toString()});
		parameterMap.put(
			PortletDataHandlerKeys.PORTLET_DATA,
			new String[] {Boolean.TRUE.toString()});
		parameterMap.put(
			PortletDataHandlerKeys.PORTLET_DATA_ALL,
			new String[] {Boolean.TRUE.toString()});
		parameterMap.put(
			PortletDataHandlerKeys.PORTLET_SETUP,
			new String[] {Boolean.TRUE.toString()});
		parameterMap.put(
			PortletDataHandlerKeys.PORTLET_USER_PREFERENCES,
			new String[] {Boolean.TRUE.toString()});
		parameterMap.put(
			PortletDataHandlerKeys.THEME,
			new String[] {Boolean.FALSE.toString()});
		parameterMap.put(
			PortletDataHandlerKeys.USER_ID_STRATEGY,
			new String[] {UserIdStrategy.CURRENT_USER_ID});
		parameterMap.put(
			PortletDataHandlerKeys.USER_PERMISSIONS,
			new String[] {Boolean.FALSE.toString()});

		return parameterMap;
	}
	
	private static Log _log = LogFactoryUtil.getLog(
		RERDefaultLandingPageAction.class);

}