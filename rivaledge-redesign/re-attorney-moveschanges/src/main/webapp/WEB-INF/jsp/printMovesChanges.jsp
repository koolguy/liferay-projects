<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c"       uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"      uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt"     uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0" %> 
    

<portlet:defineObjects/>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ALM Rival Edge</title>
<link rel="shortcut icon" type="<%= renderRequest.getContextPath() %>/images/x-icon" href="images/favicon.ico"/>
</head>
<body style="background:#fff">

<div class="printSec">
	  <div class="topBar">
		    <ul class="tplist reset flRight">
		      <li><a href="#" onClick="window.print()">Print</a></li>
		      <li><a href="#" onClick="window.close()">Close</a></li>
		    </ul>
		    <div class="clear">&nbsp;</div>
      </div>
      <div>
		  <div class="dateSec flRight">Date Printed : ${printedDateStr} at ${formattedTime}</div>
  	 </div>
  		 <h4>Attorney Moves And Changes</h4>
  	     <div class="flLeft resultsec martp1"> <strong>${attorneyMoveSearchModelBean.totalResultCount} of ${attorneyMoveSearchModelBean.totalResultCount} Results</strong>  
          <span><em>(
						 <c:choose>
							 <c:when test="${fn:length(attorneyMoveSearchModelBean.selectedFirms) gt 100}">
		      				 	${fn:substring(attorneyMoveSearchModelBean.selectedFirms, 0, 99)}..,
		      				 </c:when>
							<c:otherwise>
									 <c:if test="${not empty attorneyMoveSearchModelBean.selectedFirms}">
									 	${attorneyMoveSearchModelBean.selectedFirms}, 
									 </c:if>	      				 	
		      				</c:otherwise>
						</c:choose>  
						 <c:if test="${not empty attorneyMoveSearchModelBean.selectedChangeTypes}">			 
							  ${attorneyMoveSearchModelBean.selectedChangeTypes},
						</c:if> 				
						<c:choose>
							<c:when
								test="${fn:length(attorneyMoveSearchModelBean.selectedLocation) gt 100}">
	      				 	${fn:substring(attorneyMoveSearchModelBean.selectedLocation, 0, 99)}..,
	      				 </c:when>
							<c:otherwise>
	      				 	${attorneyMoveSearchModelBean.selectedLocation}, 
	      				 </c:otherwise>
						</c:choose> 
						<c:choose>
							<c:when
								test="${fn:length(attorneyMoveSearchModelBean.selectedPracticeArea) gt 100}">
	      				 	${fn:substring(attorneyMoveSearchModelBean.selectedPracticeArea, 0, 99)}..,
	      				 </c:when>
							<c:otherwise>
	      				 	${attorneyMoveSearchModelBean.selectedPracticeArea}, 
	      				 </c:otherwise>
						</c:choose> 
						<c:if test="${not empty attorneyMoveSearchModelBean.dateText}">			 
							  ${attorneyMoveSearchModelBean.dateText},
						</c:if>
						<c:if test="${not empty attorneyMoveSearchModelBean.titles}">
							${attorneyMoveSearchModelBean.selectedTitles},
						</c:if> 
						<c:if test="${not empty attorneyMoveSearchModelBean.name}">			 
							  ${attorneyMoveSearchModelBean.name},
						</c:if> 
						<c:if test="${not empty attorneyMoveSearchModelBean.keywords}">
							  ${attorneyMoveSearchModelBean.keywords}
						</c:if> )
				</em></span>
          
          </div>
  		<div class="clear">&nbsp;</div>
  		   <table width="100%" class="tble4" border="0" cellspacing="0" cellpadding="0">
          <colgroup>
          <col width="25" />
          <col width="110" />
          <col width="130" />
          <col width="150" />
          <col width="150" />
          <col width="150" />
          <col width="150" />
          <col width="150" />
          <col width="150" />
          </colgroup>
          <tr class="tpbar">
            <th></th>
            <th align="left">Date</th>
            <th align="left">Action</th>
            <th>Name</th>
            <th>Firm</th>
            <th>Title</th>
            <th>Location</th>
            <th>Practices</th>
            <th>From</th>
            <th>To</th>
          </tr>
         
	   <c:choose>
			<c:when test="${fn:length(printSearchResults) gt 0}">
				<c:forEach items="${printSearchResults}" var="results" varStatus="i">			
				
				
					<c:choose>
						<%-- For first row in Table --%>
   						 <c:when test="${(i.first) && (attorneyMoveSearchModelBean.orderBy == 6)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.firmName}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 
   						 <c:when test="${(i.first) && (attorneyMoveSearchModelBean.orderBy == 8)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.title}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 
   						  <c:when test="${(i.first) && (attorneyMoveSearchModelBean.orderBy == 9)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.location}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(i.first) && (attorneyMoveSearchModelBean.orderBy == 10)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.practices}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						
   						  
   						 <%-- For subsequent rows in Table only when there is change in the CompanName or practiceArea or location in 2 consecutive records --%>
   						 <c:when test="${(i.index > 0) && (attorneyMoveSearchModelBean.orderBy == 6) && (printSearchResults[i.index - 1].firmName != printSearchResults[i.index].firmName)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.firmName}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(i.index > 0) && (attorneyMoveSearchModelBean.orderBy == 10) && (printSearchResults[i.index - 1].practices != printSearchResults[i.index].practices)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.practices}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 <c:when test="${(i.index > 0) && (attorneyMoveSearchModelBean.orderBy == 9) && (printSearchResults[i.index - 1].location != printSearchResults[i.index].location)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.location}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(i.index > 0) && (attorneyMoveSearchModelBean.orderBy == 8) && (printSearchResults[i.index - 1].title != printSearchResults[i.index].title)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.title}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 
				</c:choose>
				
					<!-- <tr class="odd"> -->
				<tr class="${i.index % 2 == 0 ? 'odd' : 'even'} ">
		             <td align="center"><!-- <input type="checkbox" /> --></td>
		            <td align="left"><fmt:formatDate pattern="MM/dd/yyyy" value="${results.lastActionDate}"/></td>
		            <td>${results.lastAction}</td>
		            <td align="center"><a href="${results.attorneyLink}" target="_blank"  ><span style="text-decoration:underline">${results.attorneyName}</span></a></td>
		            <td align="center"><a href="${results.firmLink}" target="_blank"  ><span style="text-decoration:underline">${results.firmName}</span></a></td>
		            <td>${results.title}</td>
		            <td>${results.location}</td>
		            <td>${results.practices}</td>
		            <td>
			            <c:choose>
			            	<c:when test="${ not empty results.movedFromFirmLink}">
			            		<a href="${results.movedFromFirmLink}" target="_blank"  ><span style="text-decoration:underline">${results.movedFrom}</span></a>
			            	</c:when>
			            	<c:otherwise>
			            		<span>${results.movedFrom}</span>
			               </c:otherwise>			            
			            </c:choose>		            	
		            </td>
		            <td>
		            	<c:choose>
		            		<c:when test="${not empty results.movedToFirmLink}">
		            			<a href="${results.movedToFirmLink}" target="_blank"  ><span style="text-decoration:underline">${results.movedTo}</span></a>
		            		</c:when>
		            		<c:otherwise>
		            			<span>${results.movedTo}</span>
		            		</c:otherwise>
		            	</c:choose>		            	
		            </td>	           
		          </tr>
				</c:forEach>
   			
			</c:when>
			<c:otherwise>
				<tr class="odd">
					<td colspan="7" align="center">0 Results, Please try a	different search</td>
				</tr>
			</c:otherwise>

		</c:choose>
	
	</tbody>
	</table>
  		
</div>
</body>
</html>