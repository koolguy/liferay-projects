<%@page import="com.alm.rivaledge.model.AttorneyMoveSearchModelBean"%>
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib 	prefix="portlet" uri="http://java.sun.com/portlet_2_0" %>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="javax.portlet.PortletURL"%>
<portlet:defineObjects />
<%						
	PortletURL printPublicationsURL = renderResponse.createRenderURL();
	printPublicationsURL.setWindowState(LiferayWindowState.POP_UP);
	printPublicationsURL.setParameter("displayPrintPage", "true");						
	
	System.out.println("\n\n*** printURL = " + printPublicationsURL + "\n");
%>
 
 <script type="text/javascript">
function setFileType(fileType)
{
	document.getElementById('fileType').value = fileType;
	var r=confirm("Click OK to export or Cancel otherwise");
	if (r == true)
  	{
  		document.getElementById('exportForm').submit();
  	}
	else
	{
	  return false;
	}
}


function openPopUp()
{
popupWindow = window.open(
				'<%= printPublicationsURL.toString() %>',
				'popUpWindow','height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes');
}
</script>
 
 
 



 <div id="content"> 
      <div id="tab2">
        <h4 class="heading">Detailed Results</h4>
        <div class="marbtm2 martp3">         
          <div class="flLeft resultsec martp1"><strong>${attorneyMoveSearchModelBean.currentPageSize}&nbsp;of&nbsp;${attorneyMoveSearchModelBean.totalResultCount} Results</strong>  
          <span><em>(
						<c:choose>
							<c:when test="${fn:length(attorneyMoveSearchModelBean.selectedFirms) gt 100}">
	      				 	${fn:substring(attorneyMoveSearchModelBean.selectedFirms, 0, 99)}..,
	      				 </c:when>
							<c:otherwise>
								 <c:if test="${not empty attorneyMoveSearchModelBean.selectedFirms}">
								 	${attorneyMoveSearchModelBean.selectedFirms}, 
								 </c:if>	      				 	
	      				  </c:otherwise>
						</c:choose> 
						 <c:if test="${not empty attorneyMoveSearchModelBean.selectedChangeTypes}">			 
							  ${attorneyMoveSearchModelBean.selectedChangeTypes},
						</c:if> 				
						<c:choose>
							<c:when
								test="${fn:length(attorneyMoveSearchModelBean.selectedLocation) gt 100}">
	      				 	${fn:substring(attorneyMoveSearchModelBean.selectedLocation, 0, 99)}..,
	      				 </c:when>
							<c:otherwise>							
								<c:if test="${not empty attorneyMoveSearchModelBean.selectedLocation}">
								  ${attorneyMoveSearchModelBean.selectedLocation},
								</c:if>	      				 	 
	      				 </c:otherwise>
						</c:choose> 
						<c:choose>
							<c:when
								test="${fn:length(attorneyMoveSearchModelBean.selectedPracticeArea) gt 100}">
	      				 	${fn:substring(attorneyMoveSearchModelBean.selectedPracticeArea, 0, 99)}..,
	      				 </c:when>
							<c:otherwise>	      				 	  
	      				 	   <c:if test="${not empty attorneyMoveSearchModelBean.selectedPracticeArea}">
								  ${attorneyMoveSearchModelBean.selectedPracticeArea}, 
								</c:if>
	      				   </c:otherwise>
						</c:choose> 
						<c:if test="${not empty attorneyMoveSearchModelBean.dateText}">			 
							  ${attorneyMoveSearchModelBean.dateText},
						</c:if>
						<c:if test="${not empty attorneyMoveSearchModelBean.titles}">
							${attorneyMoveSearchModelBean.selectedTitles}							
						</c:if> 
						<c:if test="${not empty attorneyMoveSearchModelBean.name}">			 
							  ,${attorneyMoveSearchModelBean.name}
						</c:if> 
						)
				</em></span>
          
          </div>
          <div class="flRight">   	  
      		<ul class="menuRight flRight">
        		<li><a class="btn icon alert" href="#">Create Alert</a></li>
         		<li><a class="btn icon print" href="#" onClick="openPopUp();" >Print</a></li>
        		<li><a id="exports" class="btn icon export" href="#">Export</a>
	          		<div id="actionBox" class="actionSec" style="">
	            		<h5>Actions</h5>
	            		<ul class="reset">
	              			<li><span>E</span><a href="#"  onclick="setFileType('xls')">Export to Excel</a></li>
	              			<li><span>E</span><a href="#"  onclick="setFileType('csv')">Export to CSV</a></li>
	            		</ul>
	            		<div class="clear">&nbsp;</div>
	          		</div>
        		</li>
          <li>           	
           		<div class="viewSetting flRight rel" style="margin-top:0px">
					  		<a class="btn icon settings" href="javascript:void(0)" onclick="toggleViewSetting(this);">
					  			<span>
					  				<span class="btn icon dropdownarrow" id="viewSettingsAttorney">View Settings</span>
					  			</span>
					  		</a>
	      			 </div>
            
            </li>
            </ul>
            </div>
            
          </div>
          <div class="clear">&nbsp;</div>
          <c:if test="${not isAdminUser}">
	          <c:if test="${displayMessage}">
		  		<div style="color:#b63334;">
		  			Search Results are too large to display in their entirety. Only the first 5,000 results are displayed below. To view an entire search result, please narrow your search criteria above and rerun your search.
		  		</div>
	  		  </c:if>
  		  </c:if>
        </div>
        <table width="100%" class="tble4" border="0" cellspacing="0" cellpadding="0">
          <colgroup>
          <col width="25" />
          <col width="110" />
          <col width="130" />
          <col width="150" />
          <col width="150" />
          <col width="150" />
          <col width="150" />
          <col width="150" />
          <col width="150" />
          </colgroup>
          <tr class="tpbar">
          	<!--         		 
        		Attorney Moves  Sorting Implementation : The number which is passed in sortResults() method depends upon index 
        										  of field in query.
        										  below are the indexes of field in Query for which we need to implement sorting
        										  
        										  Date				:column #12
        										  Action			:column #11
        										  Name				:column #3
        										  Firm				:column #6
        										  Title				:column #8
        										  Location			:column #9
        										  Practices			:column #10
        										  From				:column #13
        										  To				:column #14   
        										      
        		Note : Please make sure that whenever you are changing the order of fields in query,change these Indexes as well  										         	
        	 -->
          
            <th></th>
            <th align="left"><a href="javascript:void(0);" onclick="sortResults(12)">Date</a></th>
            <th align="left"><a href="javascript:void(0);" onclick="sortResults(11)">Action</a></th>
            <th><a href="javascript:void(0);" onclick="sortResults(3)">Name</a></th>
            <th><a href="javascript:void(0);" onclick="sortResults(6)">Firm</a></th>
            <th><a href="javascript:void(0);" onclick="sortResults(8)">Title</a></th>
            <th><a href="javascript:void(0);" onclick="sortResults(9)">Location</a></th>
            <th><a href="javascript:void(0);" onclick="sortResults(10)">Practices</a></th>
            <th><a href="javascript:void(0);" onclick="sortResults(13)">From</a></th>
            <th><a href="javascript:void(0);" onclick="sortResults(14)">To</a></th>
          </tr>
          <!--<c:choose>
          	<c:when test="${fn:length(serachResults) gt 0}">
	          	<c:forEach var="results" items="${serachResults}" varStatus="loopStatus">   
		          	<tr class="${loopStatus.index % 2 == 0 ? 'evenone' : 'oddone'} ">
		            <td align="center"><input type="checkbox" /></td>
		            <td align="left"><fmt:formatDate pattern="MM/dd/yyyy" value="${results.lastActionDate}"/></td>
		            <td>${results.lastAction}</td>
		            <td align="center"><a href="${results.attorneyLink}" target="_blank"  ><span style="text-decoration:underline">${results.attorneyName}</span></a></td>
		            <td align="center"><a href="${results.firmLink}" target="_blank"  ><span style="text-decoration:underline">${results.firmName}</span></a></td>
		            <td>${results.title}</td>
		            <td>${results.location}</td>
		            <td>${results.practices}</td>
		            <td>From</td>
		            <td>To</td>		           
		          </tr>
	          </c:forEach> 
          	</c:when>  	
        
          <c:otherwise>
	      		<tr class="odd">
	      			<td colspan="8" align="center">0 Results, Please try a different search</td>
	      		</tr>     		
      		</c:otherwise>
        </c:choose> -->
        
        <c:choose>
			<c:when test="${fn:length(attorneyMoveSearchResults) gt 0}">
				<c:forEach items="${attorneyMoveSearchResults}" var="results" varStatus="i">
			
				 <%-- orderBy values 	2 = companyId
					      	 			6 = practiceArea
					      	 			7 = location	
				 --%> 
				 <%--The below choose cond'n  is responsible for grouping the results in Table in UI--%>
				<c:choose>
						<%-- For first row in Table --%>
   						 <c:when test="${(i.first) && (attorneyMoveSearchModelBean.orderBy == 6)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.firmName}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 
   						 <c:when test="${(i.first) && (attorneyMoveSearchModelBean.orderBy == 8)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.title}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 
   						  <c:when test="${(i.first) && (attorneyMoveSearchModelBean.orderBy == 9)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.location}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(i.first) && (attorneyMoveSearchModelBean.orderBy == 10)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.practices}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						
   						  
   						 <%-- For subsequent rows in Table only when there is change in the CompanName or practiceArea or location in 2 consecutive records --%>
   						 <c:when test="${(i.index > 0) && (attorneyMoveSearchModelBean.orderBy == 6) && (attorneyMoveSearchResults[i.index - 1].firmName != attorneyMoveSearchResults[i.index].firmName)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.firmName}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(i.index > 0) && (attorneyMoveSearchModelBean.orderBy == 10) && (attorneyMoveSearchResults[i.index - 1].practices != attorneyMoveSearchResults[i.index].practices)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.practices}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 <c:when test="${(i.index > 0) && (attorneyMoveSearchModelBean.orderBy == 9) && (attorneyMoveSearchResults[i.index - 1].location != attorneyMoveSearchResults[i.index].location)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.location}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						  <c:when test="${(i.index > 0) && (attorneyMoveSearchModelBean.orderBy == 8) && (attorneyMoveSearchResults[i.index - 1].title != attorneyMoveSearchResults[i.index].title)}">
     						    <tr class="oddone">
	      		 					<td colspan="6"><div class="padtp1 padlt1 padbtm1"><strong>${results.title}</strong></div></td>      		 		
	      		 				</tr>
   						 </c:when>
   						 
				</c:choose>
				
					<!-- <tr class="odd"> -->
				<tr class="${i.index % 2 == 0 ? 'odd' : 'even'} ">
		             <td align="center"><!-- <input type="checkbox" /> --></td>
		            <td align="left"><fmt:formatDate pattern="MM/dd/yyyy" value="${results.lastActionDate}"/></td>
		            <td>${results.lastAction}</td>
		            <td align="center"><a href="${results.attorneyLink}" target="_blank"  ><span style="text-decoration:underline">${results.attorneyName}</span></a></td>
		            <td align="center"><a href="${results.firmLink}" target="_blank"  ><span style="text-decoration:underline">${results.firmName}</span></a></td>
		            <td>${results.title}</td>
		            <td>${results.location}</td>
		            <td>${results.practices}</td>
		            <td>
			            <c:choose>
			            	<c:when test="${ not empty results.movedFromFirmLink}">
			            		<a href="${results.movedFromFirmLink}" target="_blank"  ><span style="text-decoration:underline">${results.movedFrom}</span></a>
			            	</c:when>
			            	<c:otherwise>
			            		<span>${results.movedFrom}</span>
			               </c:otherwise>			            
			            </c:choose>		            	
		            </td>
		            <td>
		            	<c:choose>
		            		<c:when test="${not empty results.movedToFirmLink}">
		            			<a href="${results.movedToFirmLink}" target="_blank"  ><span style="text-decoration:underline">${results.movedTo}</span></a>
		            		</c:when>
		            		<c:otherwise>
		            			<span>${results.movedTo}</span>
		            		</c:otherwise>
		            	</c:choose>		            	
		            </td>		           
		          </tr>
				</c:forEach>
   			
			</c:when>
			<c:otherwise>
				<tr class="odd">
					<td colspan="7" align="center" style="text-align: center;">0 Results, Please try a	different search</td>
				</tr>
			</c:otherwise>

		</c:choose>
        
        
        </table>
        <div class="PaginationScroll">
        	  <ul>
					<%
						AttorneyMoveSearchModelBean amsm = 	((AttorneyMoveSearchModelBean) request.getAttribute("attorneyMoveSearchModelBean"));
						int pageNumber 	= 0;
						int lastPage	= 0;
						if(amsm != null) {
						 pageNumber 	= amsm.getGoToPage();
						 lastPage	= amsm.getLastPage();
						}
						System.out.println("\n\n*** pageNumber = " + pageNumber + "; lastPage = " + lastPage);
						if (lastPage != 1)
						{
						%>
								Page:&nbsp;&nbsp;&nbsp;
						<%
								if (pageNumber > 5)
								{
						%>
									<li class="active"><a href="#" onclick="setPage(<%= 1 %>)"><%= 1 %></a>&nbsp;&nbsp;&nbsp;...&nbsp;&nbsp;&nbsp;</li>
						<%
								}
								for (int i = Math.max(1, pageNumber - 5); i <= Math.min(pageNumber + 5, lastPage); i++)
								{
									if (pageNumber == i)
									{
						%>
									<li><strong><%= i %>&nbsp;&nbsp;&nbsp;</strong> </li>
						<%
									}
									else
									{
						%>
										<li><a href="#" onclick="setPage(<%= i %>)"><%= i %></a>&nbsp;&nbsp;&nbsp;</li>
						<%				
									}
								}
								
								if ((lastPage - pageNumber) > 5)
								{
						%>
									...&nbsp;&nbsp;&nbsp;<li><a href="#" onclick="setPage(<%= lastPage %>)"><%= lastPage %></a></li>
						<%			
								}
						}
						else
						{
						%>
								Page:&nbsp;&nbsp;&nbsp;1
						<%				
						}
						%>
		      	  	
		     	</ul>
        </div>
      </div>
      <!--<div id="tab3"> Hello Analysis </div>--> 
    </div>
    
    <form name="exportForm" method="post" id="exportForm" action="<portlet:resourceURL id="exportFile"/>">
	<input type="hidden" name="fileType" value="" id="fileType"/>
  </form>

<script src="<%=renderRequest.getContextPath()%>/js/s_code.js"></script>
<script type="text/javascript">

//Omniture SiteCatalyst - START
s.prop22 = "premium";
s.pageName="rer:Attorny-Moves-changes-search-results";
s.channel="rer:Attorny-Moves-changes";
s.server="rer";
s.prop1="Attorny-Moves-changes"
s.prop2="Attorny-Moves-changes";
s.prop3="Attorny-Moves-changes";
s.prop4="Attorny-Moves-changes";
s.prop21=s.eVar21='current.user@foo.bar';	
s.events="event2";
s.events="event27";
s.t();
//Omniture SiteCatalyst - END

<!--

//-->
</script>
    