<%@ page 	import="com.alm.rivaledge.transferobject.AttorneyMoveChangesSearchDTO"%>
<%@ page 	import="javax.portlet.WindowState"%>
<%@ page 	import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ page 	import="javax.portlet.PortletURL"%>
<%@ page 	import="com.alm.rivaledge.util.ALMConstants"%>
<%@ page 	import="javax.portlet.PortletURL"%>
<%@ page 	import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@ taglib 	prefix="liferay-portlet"	uri="http://liferay.com/tld/portlet" %>
<%@ taglib 	prefix="portlet"			uri="http://java.sun.com/portlet_2_0" %>
<%@ taglib	prefix="c" 					uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib	prefix="liferay-ui" 		uri="http://liferay.com/tld/ui" %>
<%@ taglib  prefix="liferay-util" 		uri="http://liferay.com/tld/util"%>
<%@ taglib  prefix="liferay-theme" 		uri="http://liferay.com/tld/theme" %>
<%@ taglib  prefix="spring"             uri="http://www.springframework.org/tags"%>
<%@ taglib  prefix="form"               uri="http://www.springframework.org/tags/form"%>
<portlet:defineObjects />

<%@ include file="./common.jsp" %>
<link rel="stylesheet" href="<%=renderRequest.getContextPath()%>/css/jquery-ui.css" />

<portlet:actionURL var="submitURL">
	<portlet:param name="action" value="changeSearchCriteria"/>
</portlet:actionURL>

<script type="text/javascript">


	//Autoselect previously selected search critria
	function <portlet:namespace/>initializeSearchCriteria()
	{
		<portlet:namespace/>applyFirms();
		<portlet:namespace/>initializeDate();
		<portlet:namespace/>applyLocations();
		<portlet:namespace/>applyPracticeArea();
		<portlet:namespace/>applyChangeType();
		<portlet:namespace/>applyTitle();		
		<portlet:namespace/>refreshCounters();
	}
	
	//This would trigger when user change something in "Select Individual Firms" section of firms popup
	function <portlet:namespace/>allOtherFirmChange()
	{
		//holds the count of firms selected in "Select Individual Firms" section of firms popup
		var allFirmsCounter = 0;
		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() 
		{
			allFirmsCounter++;
		});
		$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="Abrams"> Firms ('+allFirmsCounter+') Selected');
		
		if(allFirmsCounter > 0)
		{
			 $('#<portlet:namespace/>allOtherFirm').prop("checked", true);
			 $("#homePageShowAttorneyMC .individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0 Selected)');
		}
		else
		{
			$('#<portlet:namespace/>allOtherFirm').prop("checked", false);
		}
	}
	
	//This would trigger when user clicks on "Apply" button on View setting popup
	function <portlet:namespace/>applyViewSettings()
	{		
		$("#<portlet:namespace/>viewSettingsPopup").hide(); 
		<portlet:namespace/>search();
	}
	
	//This would trigger when user clicks on "Apply" button on search form 
	function <portlet:namespace/>search()
	{
		Liferay.Portlet.showBusyIcon("#bodyId","Loading..."); // show Busy Icon
		$("#<portlet:namespace/>goToPage").val(1);
		$('#attorneyMoveSearchModelBean').submit();
	}

	//toggle view setting popup
	function toggleViewSetting(vsId)
	{
		 //get the position of the placeholder element
	   	var vsPos   = $(vsId).offset();
	   	var vsHeight = $(vsId).height();
	   	var vsWidth = $(vsId).width();
	    //show the menu directly over the placeholder
	    var popupWidth = $("#<portlet:namespace/>viewSettingsPopup").width();
	    $("#<portlet:namespace/>viewSettingsPopup").css({  position: "absolute", "left": (vsPos.left - popupWidth + vsWidth) + "px", "top":(vsPos.top + vsHeight)  + "px" });
	    
	   
		$("#<portlet:namespace/>viewSettingsPopup").toggle(); 
	}
	
	//Initialize Date
	function <portlet:namespace/>initializeDate()
	{
		var dateTextStr = '${attorneyMoveSearchModelBean.dateText}';
		if(dateTextStr.match('^Any') || dateTextStr.match('^Last'))
			{
				$("#<portlet:namespace/>datePeriodSelect option[value='"+dateTextStr+"']").attr('selected','selected'); 
				$('#<portlet:namespace/>period').prop("checked", true);
			}
		else{ // we have date range parse it and set the Date on datePicker
				$( "#<portlet:namespace/>from" ).datepicker( "setDate", dateTextStr.split("-")[0]);
				$( "#<portlet:namespace/>to" ).datepicker( "setDate", dateTextStr.split("-")[1]);
			}		
			$("#<portlet:namespace/>dateText").val(dateTextStr);
	}
	
	//This method would trigger when user clicks on "Clear All" button from firm popup
	//and would reset the firm value to default slection.
	function <portlet:namespace/>clearFirms()
	{		
		$('#<portlet:namespace/>popup').find("option").attr("selected", false);
		$('#<portlet:namespace/>popup').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>popup').find('input[type=radio]:checked').removeAttr('checked');
		
		$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/>  Firms (0) Selected');
	
		$("#<portlet:namespace/>firm_watchlist").prop("checked", true);
		
		var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
		
		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]').each(function() {
			 if(this.value == watchListId)
				{
					$(this).prop("checked", true);
				}
		});
		
		<portlet:namespace/>applyFirms();
	}
	
	//This method would trigger when user clicks on "Clear All" button from Date popup
	//and would reset the date value to default selection
	function <portlet:namespace/>clearDate()
	{	
		$('#<portlet:namespace/>popupDate').find("option").attr("selected", false);
		$('#<portlet:namespace/>popupDate').find('input[type=text]').val('');
		$('#<portlet:namespace/>popupDate').find('input[type=radio]:checked').removeAttr('checked');				
		$("#<portlet:namespace/>dateText").val('<%=ALMConstants.LAST_WEEK%>');	
		$("#<portlet:namespace/>period").prop("checked", true); 
		$("#<portlet:namespace/>datePeriodSelect").val('<%=ALMConstants.LAST_WEEK%>');	
		$("#<portlet:namespace/>from").datepicker('enable');
		$("#<portlet:namespace/>to").datepicker('enable');
		<portlet:namespace/>applyDate();
	}
	
	//This method would trigger when user clicks on "Clear All" button from Location popup
	//and would reset the Location value to default selection
	function <portlet:namespace/>clearLocations()
	{
		$('#<portlet:namespace/>popupLocation').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>popupLocation').find('input[type=radio]:checked').removeAttr('checked');
		$("#locationsCounter").html('<input type="checkbox" value="Abrams"> Locations (0) Selected');
		$('#<portlet:namespace/>allLocations').prop("checked", true);
		<portlet:namespace/>applyLocations();
	}

	//This method would trigger when user clicks on "Clear All" button from Practice Area popup
	//and would reset the Practice Area value to default selection
	function <portlet:namespace/>clearPracticeAreas()
	{
		$('#<portlet:namespace/>popupPracticeArea').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>popupPracticeArea').find('input[type=radio]:checked').removeAttr('checked');
		$('#<portlet:namespace/>allOtherPracticeArea input').find('input[type=checkbox]:checked').removeAttr('checked');
		$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="Abrams"> Practice Area (0) Selected');
		$('.allPracticeArea').prop('checked', true);	
		<portlet:namespace/>applyPracticeArea();
	}
	
	//This method would trigger when user clicks on "Clear All" button from Change Type popup
	//and would reset the Change Type value to default selection
	function <portlet:namespace/>clearChangeType()
	{
		$('#<portlet:namespace/>changeType').find('input[type=checkbox]:checked').removeAttr('checked');
		//$('#allChangeType').prop("checked", true);
		$('#<portlet:namespace/>AdditionsChangeType').prop("checked", true);
		$('#<portlet:namespace/>RemovalsChangeType').prop("checked", true);
		<portlet:namespace/>applyChangeType();
	}
	
	//This method would trigger when user clicks on "Clear All" button from Titles popup
	//and would reset the Titles value to default selection
	function <portlet:namespace/>clearTitles()
	{
		$('#<portlet:namespace/>titlesDiv').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>allTitles').prop("checked", true);
		<portlet:namespace/>applyTitle();
	}
	
	//This method would trigger when user clicks on "Reset All" button 
	//and would reset all the fields to  default selection
	function <portlet:namespace/>clearAll()
	{
		<portlet:namespace/>clearFirms();
		<portlet:namespace/>clearLocations();
		<portlet:namespace/>clearPracticeAreas();
		<portlet:namespace/>clearDate();
		<portlet:namespace/>clearChangeType();
		<portlet:namespace/>clearTitles();
		//initializeSearchCriteria();
	}
	
	//this method would validate date format which user has entered
	function <portlet:namespace/>ValidateDate(txtDate)
	{
	    var currVal = txtDate;
	    if(currVal == '')
	    return false;
	    
	    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
	    var dtArray = currVal.match(rxDatePattern); // is format OK?
	    
	    if (dtArray == null) 
	    return false;
	    
	    //Checks for mm/dd/yyyy format.
	    dtMonth = dtArray[1];
	    dtDay= dtArray[3];
	    dtYear = dtArray[5];        
	    
	    if (dtMonth < 1 || dtMonth > 12) 
	    return false;
	    else if (dtDay < 1 || dtDay> 31) 
	    return false;
	    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
	    return false;
	    else if (dtMonth == 2) 
	    {
	        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
	        if (dtDay> 29 || (dtDay ==29 && !isleap)) 
	        return false;
	    }
	    return true;
	}
	
	//This method would invoke when user clicks on Apply button from firms popup
	function <portlet:namespace/>applyFirms()
	{
		var checkFlag = $("#<portlet:namespace/>allfirms").is(":checked");
		var valueofchecked =$("input[name='firmType']:checked").val();
		if(valueofchecked=="All Firms")
		{
			checkFlag=true;
			$('#<portlet:namespace/>allfirms').prop("checked", true);
		}
		
		var allSelectedValue = [];
		var allSelectedIds = [];
		
		 $("#<portlet:namespace/>Firmstext").val('');
		 $("#<portlet:namespace/>selectedFirms").val('');
		 
		var allWatchListCounter = 0;			
		
			$('.individualfirmsWatchList input[type=checkbox]:checked').each(function() {
				 allWatchListCounter = 0;
				 allSelectedValue.push($(this).attr('labelAttr'));
				 allSelectedIds.push($(this).val());
				 $("#<portlet:namespace/>firm_watchlist").prop("checked", true);
				 $('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
				 
				 $('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').each(function() 
					{
							allWatchListCounter++;
					});
							
					$("#homePageShowAttorneyMC .individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (' + allWatchListCounter +' Selected)');
				 
		 	});
		
		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
			 allSelectedValue.push($(this).attr('labelAttr'));
			 allSelectedIds.push($(this).val());
			 $("#<portlet:namespace/>allOtherFirm").prop("checked", true);
			 
	 	});
		
		if(allSelectedValue != ''  &&  allSelectedIds != '') 
			{
			
				$("#<portlet:namespace/>Firmstext").val(allSelectedValue.join(",")); //Use a comma separator for values which show up on UI
				$("#<portlet:namespace/>selectedFirms").val(allSelectedIds.join(";"));
			}
		else
		{
			
				$("#<portlet:namespace/>allfirms, .rivaledgeListAMLAW_100, .rivaledgeListAMLAW_200, .rivaledgeListNLJ_250").each(function(){
				if(checkFlag)
					{
						$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.ALL_FIRMS %>');
						$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.ALL_FIRMS %>');
						
						 $('#<portlet:namespace/>firm_watchlist').prop("checked", false);
						 $('#<portlet:namespace/>allOtherFirm').prop("checked", false);
						 $('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
						 
						 $('#<portlet:namespace/>individualfirmsWatchList').find('input[type=checkbox]:checked').removeAttr('checked');
						 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="Abrams" id="<portlet:namespace/>firmCounter"> Firms (0) Selected');
						
					} 
				
				 else
					{
					 if($(this).is(":checked")){
							var value =$(this).val();
							$("#<portlet:namespace/>Firmstext").val(value);
							$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.AMLAW_100%>');
							$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
							} 
						}
				
					
				});
				
		}
		
	}
	
	//This method would invoke when user clicks on Apply button from Date popup
	//will validate date as well before apply
	function <portlet:namespace/>applyDate()
	{
		var checkDate=false;
		var dateValue;
		  
		if($("#<portlet:namespace/>dateRange").is(":checked"))
		{
			var fromDate = new Date($("#<portlet:namespace/>from").val());
		   	var toDate = new Date($("#<portlet:namespace/>to").val()); 
		   	if(!<portlet:namespace/>ValidateDate($("#<portlet:namespace/>from").val()))
		   	{
		    	checkDate=true;
		        $('#<portlet:namespace/>fromdateError').css('display','block');      
		   	}
		   	else
		   {
		    	checkDate=false;
		    	$('#<portlet:namespace/>fromdateError').css('display','none');
		   }  
		   if(!<portlet:namespace/>ValidateDate($("#<portlet:namespace/>to").val()))
		   {
		    	checkDate=true;
		    	$('#<portlet:namespace/>todateError').css('display','block');
		   }  
		   else
		   {
				if(!checkDate)
			    {
			     checkDate=false;
			     $('#<portlet:namespace/>todateError').css('display','none'); 
			    }		   
		   }
		   if(!(toDate >= fromDate) && !checkDate)
		   {
			    $('#<portlet:namespace/>dateValidError').css('display','block');
			    checkDate=true;
		   } 
		   else
		   {
		    	$('#<portlet:namespace/>dateValidError').css('display','none');
		   }
		  
		   if(checkDate)
		   {
		  	 return checkDate; 
		   }
		  
	  }
	  
	  if($("#<portlet:namespace/>period").is(":checked"))
	  {
	   
	   dateValue=$("#<portlet:namespace/>datePeriodSelect").val();
	   
	  } 
	  else if($("#<portlet:namespace/>dateRange").is(":checked"))
	  {
	   dateValue=$("#<portlet:namespace/>from").val() + "-"+ $("#<portlet:namespace/>to").val();
	  } 
	  
	  $("#<portlet:namespace/>dateText").val(dateValue);
	}
	
	
	//This method would invoke when user clicks on Apply button from Location popup
	function <portlet:namespace/>applyLocations()
	{
		var checked= false;		
		 if($("#<portlet:namespace/>allLocations").is(":checked"))
		 {
			 	//as "All Locations" option is  checked so checking radio button corresponding to it
				$("#<portlet:namespace/>selectedLocation").val($("#<portlet:namespace/>allLocations").val());
				checked = true;
		 } 		 
		if(!checked)
		 {
			//as "All locations" option is not checked so counting individual locations and storing the count in below vaiable
			 var allValsLocation = [];
			
				$('#<portlet:namespace/>allOtherLocationsDiv input[type=checkbox]:checked').each(function() { 
					 allValsLocation.push($(this).val());
			    });
				 
			if(allValsLocation.length > 0){
				$("#<portlet:namespace/>allOtherLocation").prop("checked", true);
				//putting ";" as separter
			    $("#<portlet:namespace/>selectedLocation").val(allValsLocation.join(";"));		
			}
		}
	}

	
	//This method would invoke when user clicks on Apply button from Practice Area popup
	function <portlet:namespace/>applyPracticeArea()
	{
		 if($("#homePageShowAttorneyMC .allPracticeArea").is(":checked"))
		 {
			 	//as "All Practice Areas" option is  checked so checking radio button corresponding to it
				$("#<portlet:namespace/>selectedPracticeArea").val($("#homePageShowAttorneyMC .allPracticeArea").val());			
		 } 
		 else
		 {		
			var allValsPracticeArea = [];
			//as "All Practice Areas" option is not checked so counting individual Practice area and storing the count in allPracticeCounter
			var allPracticeCounter = 0;
			
			$('#<portlet:namespace/>practiceAreaDiv input[type=checkbox]:checked').each(function() 
			{ 
			 	allValsPracticeArea.push($(this).val());
			 	allPracticeCounter++;
			});				 
			$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="Abrams"> PracticeAreas ('+allPracticeCounter+') Selected');			
			if(allValsPracticeArea.length > 0)
			{
				$("#<portlet:namespace/>allPracticeAreaIndividual").prop("checked", true);
				//putting ";" as separter
				$("#<portlet:namespace/>selectedPracticeArea").val(allValsPracticeArea.join(";"));		
			}
		}
	}
	
	//This method would invoke when user clicks on Apply button from Change Type popup
	function <portlet:namespace/>applyChangeType()
	{
		var allValsChangeType = [];
		 $('#<portlet:namespace/>changeType :checked').each(function() {
			 allValsChangeType.push($(this).val());
		    });
		 $("#<portlet:namespace/>selectedChangeType").val(allValsChangeType.join(";"));
	}
	
	//This method would invoke when user clicks on Apply button from Title popup
	function <portlet:namespace/>applyTitle()
	{
		var allValsTitles = [];
		 $('#<portlet:namespace/>popupTitles :checked').each(function() {
			 allValsTitles.push($(this).val());
		    });
		    $("#<portlet:namespace/>selectedTitles").val(allValsTitles.join(";"));
		
	
	}	

	//Refresh counter values for Firms,Locations and Practice Area popup
	function <portlet:namespace/>refreshCounters()
	{
		var allFirmsCounter = 0;
		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
		 allFirmsCounter++;
		});
		$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/>  Firms ('+allFirmsCounter+') Selected');
		
		var allLocationCounter = 0;
		 $('#<portlet:namespace/>allOtherLocationsDiv input[class=allLocationsCheckBox]:checked').each(function() {
			 allLocationCounter++;
		    });
		$("#locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="selectAllLocations" />  Locations ('+allLocationCounter+') Selected');

		var allPracticeCounter = 0;
		$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:checked').each(function() {
			 allPracticeCounter++;
		    });
		$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="<portlet:namespace/>selectAllPracticeAreas" /> PracticeAreas ('+allPracticeCounter+') Selected');
	}


	function <portlet:namespace/>changeOnIndividualLocations()
	{
		var allLocationsCounter = 0;
		 $('#<portlet:namespace/>allOtherLocationsDiv :checked').each(function() {
			 allLocationsCounter++;
		    });
				$("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="<portlet:namespace/>selectAllLocations" /> Locations ('+allLocationsCounter+') Selected');
		 
			if(allLocationsCounter == 0)
			{
				$('#<portlet:namespace/>allOtherLocations').prop("checked", false);
				$('#<portlet:namespace/>allLocations').prop("checked", true);
			}
			else
			{
				$('#<portlet:namespace/>allOtherLocations').prop("checked", true);
				$('#<portlet:namespace/>allLocations').prop("checked", false);
			}
			<portlet:namespace/>applyLocations(); // apply them as the change
	}


	function <portlet:namespace/>changeOnIndividualPracticeAreas()
	{
			var allPracticeCounter = 0;
		
			$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:checked').each(function() {
				 allPracticeCounter++;
		    });
			
			$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="Abrams"> PracticeAreas ('+allPracticeCounter+') Selected');
			
			if(allPracticeCounter == 0)
			{
				$('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", false);
				$('.allPracticeArea').prop("checked", true);
			}
			else
			{
				$('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", true);
				$('.allPracticeArea').prop("checked", false);
			}
	}
	
	
	
	function <portlet:namespace/>changeOnIndividualFirms()
	 {
	  
	  var allFirmsCounter = 0;
	   $('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
	    allFirmsCounter++;
	      });
	   
	   $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms ('+allFirmsCounter+') Selected');
	   if(allFirmsCounter == 0)
	   {
	    $('#<portlet:namespace/>firm_watchlist').prop("checked", false);
	    $('#<portlet:namespace/>allOtherFirm').prop("checked", false);
	    $('#<portlet:namespace/>allfirms').prop("checked", false);
	    
	    $('#<portlet:namespace/>individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
	    $('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
	    

	    var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
	    
	    $('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]').each(function() {
	      if(this.value == watchListId)
	      {
	       $(this).prop("checked", true);
	      }
	    });
	   }
	  else
	   {
	    $('#<portlet:namespace/>allOtherFirm').prop("checked", true);
	    $('#<portlet:namespace/>firm_watchlist').prop("checked", false);
	    $('#<portlet:namespace/>allfirms').prop("checked", false);
	    
	    $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0 Selected)');
	    $('#<portlet:namespace/>individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
	    $('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
	   }
	 }
	
$.widget( "custom.catcomplete", $.ui.autocomplete, {
	_renderMenu: function( ul, items ) {
		var that = this,
		currentCategory = "";
		$.each( items, function( index, item ) {
			if ( item.category != currentCategory ) {
			ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
			currentCategory = item.category;
			}
	  that._renderItemData( ul, item );
	});
}
});

$(document).ready(function(){
	
	$("#attorneyMoveSearchModelBean input, #attorneyMoveSearchModelBean select, #attorneyMoveSearchModelBean span").each(function(){
		var idVal = '<portlet:namespace/>' + $(this).attr("id");
		$(this).attr("id", idVal );
	});
	
	$(".menu").hover(
		function(){$(".sub").slideToggle(400);},
		function(){$(".sub").hide();}
	);
	
	//Spring will add <input> tags with names starting with "_" for type "radio" and "checkbox".
	//On form POST submit they all are carried to server which is unneccessary
	//hence removing all such input box to make page and jQuery selections less heavy :) on document ready.
	
	//NOTE : If any inputs added with name prefixed with "_" purposefully will also get removed. So be cautious
	$("#attorneyMoveSearchModelBean input[name^=_]").remove();
	
  	$('#<portlet:namespace/>fromdateError').hide();
	$('#<portlet:namespace/>todateError').hide();
	$('#<portlet:namespace/>dateValidError').hide();

	//initialize datepicker for Date from field
    $( "#<portlet:namespace/>from" ).datepicker({
		  changeMonth: true,
		  changeYear: true,
		  showOn: "button",
		  buttonImage: "<%=themeDisplay.getPathThemeImages()%>/calendar.gif",
		  buttonImageOnly: true,
		  defaultDate: "+1w",
		  onClose: function( selectedDate ) {
		  $( "#<portlet:namespace/>to" ).datepicker( "option", "minDate", selectedDate );
		  }
	  });
	
	//initialize datepicker for Date To field
	  $( "#<portlet:namespace/>to" ).datepicker({
		  changeMonth: true,
		  changeYear: true,
		  showOn: "button",
		  buttonImage: "<%=themeDisplay.getPathThemeImages()%>/calendar.gif", 
		  buttonImageOnly: true,
		  defaultDate: "+1w",
		  onClose: function( selectedDate ) {
		  $( "#<portlet:namespace/>from" ).datepicker( "option", "maxDate", selectedDate );
		  }
	  });
	
	var date = new Date();
	var currentDate = (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear();
	$( "#<portlet:namespace/>to" ).datepicker( "setDate", currentDate);
	date.setDate(date.getDate() - 7);
	var lastWeekDate = (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear();
	$( "#<portlet:namespace/>from" ).datepicker( "setDate", lastWeekDate);
	$("#<portlet:namespace/>dateText").val(lastWeekDate +"-"+currentDate);
	
  
	var data 			 = 	${firmJson};  		//firm Autocomplete data in Json format
	var practiceData	 = 	${practiceJson};  	//Practice Area Autocomplete data in Json format
	var changeTypeData	 =	${changeTypeJson};	//ChangeTye Autocomplete data in Json format
	var titlesData		 =	${titlesJson};		//Titles Autocomplete data in Json format
	

	var availableLocation = [
                         "Boston, MA",
                         "Dallas, TX",
                         "Denver, CO",
                         "Houston, TX",
                         "Las Vegas, NV",
                         "Los Angeles, CA",
                         "Miami, FL",
                         "New Orleans, LA",
                         "New York, NY",
                         "Orlando, FL",
                         "Philadelphia, PA",
                         "Phoenix, AZ",
                         "Pittsburgh, PA",
                         "San Antonio, TX",
                         "San Francisco, CA",
                         "Seattle, WA",
                         "Tampa, FL",
                         "Washington, DC"                     
                       ];
	
$("#graduationTo option[value='2013']").attr('selected', 'selected');	

//This method to spilt values in firms autocomplete by ","
function <portlet:namespace/>split( val ) {
	return val.split( /,\s*/ );
}


//This method to spilt values in all other fields apart from firms(Change Type, Locations, Practice Area, Title) autocomplete by ";"
function <portlet:namespace/>splitValuesApartFromFirms( val ) {
	return val.split( /;\s*/ );
}


//This method to extract the last value in Firm autocomplete
function <portlet:namespace/>extractLast( term ) {
	return <portlet:namespace/>split( term ).pop();
}

//This method to extract the last value in all other fields apart from firms(Change Type, Locations, Practice Area, Title) autocomplete
function <portlet:namespace/>extractLastApartFromFirms( term ) {
	return <portlet:namespace/>splitValuesApartFromFirms( term ).pop();
}
  
/* Location Autocomplete Code Start */

$("#<portlet:namespace/>selectedLocation")
// don't navigate away from the field on tab when selecting an item
.bind( "keydown", function( event ) {
  if ( event.keyCode === $.ui.keyCode.TAB &&
      $( this ).data( "ui-autocomplete" ).menu.active ) {
    event.preventDefault();
  }
})
.autocomplete({
  minLength: 0,
  source: function( request, response ) {
    // delegate back to autocomplete, but extract the last term
    response( $.ui.autocomplete.filter(
    		availableLocation, <portlet:namespace/>extractLastApartFromFirms( request.term ) ) );
  },
  focus: function() {
    // prevent value inserted on focus
    return false;
  },
  change: function(event, ui) {
	  
		//this block would execute when user finishes  entering location using location Autocomplete
		//initially clearling all the checkbox/radio button in location popup 
		$('#<portlet:namespace/>popupLocation').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>popupLocation').find('input[type=radio]:checked').removeAttr('checked');
		
		//spilting all the values
		var selectedValues = this.value.split(';');	
		//iterating over all the values which user has entered and checking corresponding checkbox 
		$.each(selectedValues, function(key, currentValue) {
			currentValue=currentValue.trim();			
			if(currentValue!=""){	
				$('#<portlet:namespace/>allOtherLocationsDiv input[value="'+currentValue+'"]').prop("checked", true);		
			}			
		});		
		<portlet:namespace/>changeOnIndividualLocations();	
  },
  select: function( event, ui ) {
    var terms = <portlet:namespace/>splitValuesApartFromFirms( this.value );
    // remove the current input
    terms.pop();
    // add the selected item
    terms.push( ui.item.value );
    // add placeholder to get the comma-and-space at the end
    terms.push("");    
    this.value = terms.join(";");
    return false;
  }
});

/* Location Autocomplete Code End */

 
 /* Change Type Autocomplete Code Start */

$("#<portlet:namespace/>selectedChangeType")
// don't navigate away from the field on tab when selecting an item
.bind( "keydown", function( event ) {
  if ( event.keyCode === $.ui.keyCode.TAB &&
      $( this ).data( "ui-autocomplete" ).menu.active ) {
    event.preventDefault();
  }
})
.autocomplete({
  minLength: 0,
  source: function( request, response ) {
    // delegate back to autocomplete, but extract the last term
    response( $.ui.autocomplete.filter(
    		changeTypeData, <portlet:namespace/>extractLastApartFromFirms( request.term ) ) );
  },
  focus: function() {
    // prevent value inserted on focus
    return false;
  },
  change: function(event, ui) {
	  
		//this block would execute when user finishes  entering Change Type using Change Type Autocomplete
		//initially clearling all the checkbox/radio button in Change Type popup 
		$('#<portlet:namespace/>popupChangeType').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>popupChangeType').find('input[type=radio]:checked').removeAttr('checked');
		
		//spilting all the values
		var selectedValues = this.value.split(';');	
		//iterating over all the values which user has entered and checking corresponding checkbox 
		$.each(selectedValues, function(key, currentValue) {
			currentValue=currentValue.trim();			
			if(currentValue!=""){	
				$('#<portlet:namespace/>allOtherChangeTypeDiv input[value="'+currentValue+'"]').prop("checked", true);		
			}			
		});		
		<portlet:namespace/>applyChangeType();
  },
  select: function( event, ui ) {
    var terms = <portlet:namespace/>splitValuesApartFromFirms( this.value );
    // remove the current input
    terms.pop();
    // add the selected item
    terms.push( ui.item.value );
    // add placeholder to get the comma-and-space at the end
    terms.push("");	
    this.value = terms.join(";");
    return false;
  }
});

/* Change Type Autocomplete Code End */


 /* Titles Autocomplete Code Start */

$("#<portlet:namespace/>selectedTitles")
// don't navigate away from the field on tab when selecting an item
.bind( "keydown", function( event ) {
  if ( event.keyCode === $.ui.keyCode.TAB &&
      $( this ).data( "ui-autocomplete" ).menu.active ) {
    event.preventDefault();
  }
})
.autocomplete({
  minLength: 0,
  source: function( request, response ) {
    // delegate back to autocomplete, but extract the last term
    response( $.ui.autocomplete.filter(
    		titlesData, <portlet:namespace/>extractLastApartFromFirms( request.term ) ) );
  },
  focus: function() {
    // prevent value inserted on focus
    return false;
  },
  change: function(event, ui) {
	  
		//this block would execute when user finishes  entering titles using title Autocomplete
		//initially clearling all the checkbox/radio button in title popup 
		$('#<portlet:namespace/>popupTitles').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>popupTitles').find('input[type=radio]:checked').removeAttr('checked');
		
		//spilting all the values
		var selectedValues = this.value.split(';');	
		//iterating over all the values which user has entered and checking corresponding checkbox 
		$.each(selectedValues, function(key, currentValue) {
			currentValue=currentValue.trim();			
			if(currentValue!=""){	
				$('#<portlet:namespace/>allOtherTitlesDiv input[value="'+currentValue+'"]').prop("checked", true);		
			}			
		});		
		<portlet:namespace/>applyTitle();	
    },
  select: function( event, ui ) {
    var terms = <portlet:namespace/>splitValuesApartFromFirms( this.value );
    // remove the current input
    terms.pop();
    // add the selected item
    terms.push( ui.item.value );
    // add placeholder to get the comma-and-space at the end
    terms.push(""); 
    this.value = terms.join(";");
    return false;
  }
});

/* Titles Autocomplete Code End */
 
/* Practice Area Autocomplete Code Start */

$( "#<portlet:namespace/>selectedPracticeArea" )
	//don't navigate away from the field on tab when selecting an item
	.bind( "keydown", function( event ) {
		if ( event.keyCode === $.ui.keyCode.TAB &&
		$( this ).data( "ui-autocomplete" ).menu.active ) {
		event.preventDefault();
		}
	})
	.autocomplete({
	minLength: 3,
	source: function( request, response ) {
		//delegate back to autocomplete, but extract the last term
		response( $.ui.autocomplete.filter(
			practiceData, <portlet:namespace/>extractLastApartFromFirms( request.term ) ) );
	},
	focus: function() {
		//prevent value inserted on focus
		return false;
	},
	change: function(event, ui) {		
		
		//this block would execute when user finishes  entering practice area using practice area Autocomplete
		//initially clearling all the checkbox/radio button in practice area popup 
		$('#<portlet:namespace/>popupPracticeArea').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>popupPracticeArea').find('input[type=radio]:checked').removeAttr('checked');
		
		//spilting all the values
		var selectedValues = this.value.split(';');	
		//iterating over all the values which user has entered and checking corresponding checkbox 
		$.each(selectedValues, function(key, line) {
			line=line.trim();		
			if(line!=""){				
				$("input[practiceLabel='"+line+"']").prop("checked", true);							
			}			
		});		
		<portlet:namespace/>changeOnIndividualPracticeAreas();		
    },
	select: function( event, ui ) {
		var terms = <portlet:namespace/>splitValuesApartFromFirms( this.value );
		//remove the current input
		terms.pop();
		//add the selected item
		terms.push( ui.item.value );
		//add placeholder to get the comma-and-space at the end
		terms.push( "" );		
		this.value = terms.join( ";" );
		return false;
	}
});

/* Practice Area Autocomplete Code End */

  
  /* Firm Autocomplete Code Start */
  
  $( "#<portlet:namespace/>Firmstext" ).catcomplete({
	minLength: 3,
	source: function( request, response ) {
		// delegate back to autocomplete, but extract the last term
		response( $.ui.autocomplete.filter(
		data, <portlet:namespace/>extractLast( request.term ) ) );
	},
	focus: function() {
		// prevent value inserted on focus
		return false;
	},
	change: function(event, ui) {		
		
		//this block would execute when user finishes  entering Firms using firm Autocomplete
		//initially clearling all the checkbox/radio button in firm popup 
		$('#<portlet:namespace/>popup').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>popup').find('input[type=radio]:checked').removeAttr('checked');
		
		//spilting all the values
		var selectedValues = this.value.split(',');		
		var output = '';
		var currentvalue="";
		//iterating over all the values which user has entered and checking corresponding checkbox in Firms popup
		$.each(selectedValues, function(key, line) {
			line=line.trim();		
			if(line!=""){
				//getting firm id based on firms name
				currentvalue=$("input[labelattr='"+line+"'].allFirmsCheckBox").val();
				if(typeof(currentvalue)!="undefined"){
					output= output + currentvalue+ ",";
					$("input[labelattr='"+line+"'].allFirmsCheckBox").prop("checked", true);
					<portlet:namespace/>allOtherFirmChange();
				}	
							
			}			
		});
		//removing last comma
		output = output.substring(0, output.length - 1);
		//setting all the firms ids in hidden field
		$('#<portlet:namespace/>selectedFirms').val(output);		
    },
	select: function( event, ui ) {
	
		var terms = <portlet:namespace/>split( this.value );		
		var termsValue = <portlet:namespace/>split( this.id );
		if( ui.item.value.indexOf("AmLaw 100") !== -1){
			return false;
		}
		
		// remove the current input
		terms.pop();
		termsValue.pop();
		// add the selected item
		
		terms.push( ui.item.value );
		termsValue.push( ui.item.id );
		// add placeholder to get the comma-and-space at the end
		terms.push( "" );
		termsValue.push( "" );
		this.value = terms.join( ", " );	
		$('#<portlet:namespace/>selectedFirms').val($('#<portlet:namespace/>selectedFirms').val().replace("AmLaw 100","") + "," + ui.item.id);		
		$('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]').each(function()
   		{ 
			if($(this).val() == (ui.item.id) )
			{
				$(this).prop("checked", true);
				<portlet:namespace/>allOtherFirmChange();
				return false;
			}
		});
		
		return false;
	}
}); 
 
  /* Firm Autocomplete Code End */ 
  
	
  $('.rivaledgeListAMLAW_100').bind('click', function (event) {
  		$('.rivaledgeListAMLAW_100').prop("checked", true);
  		$('.rivaledgeListAMLAW_200').prop("checked", false);
  		$('.rivaledgeListNLJ_250').prop("checked", false);
  		$('#<portlet:namespace/>firm_watchlist').prop("checked", true);
  		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
  	 	$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0 Selected)');
  	 	
  	 	$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms (0) Selected');
  		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
  		$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
  		$('#<portlet:namespace/>Firmstext').val("<%=ALMConstants.AMLAW_100 %>");
  	});
  	
  	$('.rivaledgeListAMLAW_200').bind('click', function (event) {
  		
  		$('.rivaledgeListAMLAW_100').prop("checked", false);
  		$('.rivaledgeListAMLAW_200').prop("checked", true);
  		$('.rivaledgeListNLJ_250').prop("checked", false);
  		$('#<portlet:namespace/>firm_watchlist').prop("checked", true);
  		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
  	 	$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0 Selected)');
  	 	
  	 	$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms (0) Selected');
  		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
  		$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
  		$('#<portlet:namespace/>Firmstext').val("<%=ALMConstants.AMLAW_200 %>");
  	});
  	
  	$('.rivaledgeListNLJ_250').bind('click', function (event) {
  		
  		$('.rivaledgeListAMLAW_100').prop("checked", false);
  		$('.rivaledgeListAMLAW_200').prop("checked", false);
  		$('.rivaledgeListNLJ_250').prop("checked", true);
  		$('#<portlet:namespace/>firm_watchlist').prop("checked", true);
  		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
  	 	$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0 Selected)');
  	 	
  	 	$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms (0) Selected');
  		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
  		$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
  		$('#<portlet:namespace/>Firmstext').val("<%=ALMConstants.NLJ_250 %>");

  	});
  	
  	$("input[class='allFirmsCheckBoxWatchList']").change(function(){
  		 $('.rivaledgeListAMLAW_100').prop("checked", false);
  		 $('.rivaledgeListAMLAW_200').prop("checked", false);
  		 $('.rivaledgeListNLJ_250').prop("checked", false);
  		<portlet:namespace/>applyFirms();
  	});
  	
  	 $('.allFirmsCheckBoxWatchList').click(function() {
  		 var allWatchListCounter = 0;
  		 $('#<portlet:namespace/>firm_watchlist').prop("checked", true);
  		 $('#<portlet:namespace/>allfirms').prop("checked", false);
  		 
  		 $('#<portlet:namespace/>allOtherFirm').prop("checked", false);
  		 
  		 $('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
  		 
  		
  		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').each(function() 
  		{
  			allWatchListCounter++;
  		});
  		
  		 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (' + allWatchListCounter +' Selected)');
  		 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms (0 Selected)');
  		 $('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
  		 $('.rivaledgeListAMLAW_100').prop("checked", false);
  		 $('.rivaledgeListAMLAW_200').prop("checked", false);
  		 $('.rivaledgeListNLJ_250').prop("checked", false);
  		 
  		});
  	 
  	  //Function for showing selected PracticeArea name
  	  $("input[id='<portlet:namespace/>allFirmsCheckBoxCounterAttorneyMovesAndChanges']").change(function(){		  
  	   
  		<portlet:namespace/>changeOnIndividualFirms();  	   
  	 	<portlet:namespace/>applyFirms();
  	   
  	  });
  	 
 	//Select All firms by selecting one check box
	 $('#<portlet:namespace/>individualFirmsCounter').click(function() {
	 
		var checked = $("#<portlet:namespace/>selectAllFirms").is(":checked");
		var allFirmsListCounter = 0;
		if(checked)
		{   
			 $('#<portlet:namespace/>allOtherFirmDiv #<portlet:namespace/>allFirmsCheckBoxCounterAttorneyMovesAndChanges').prop('checked', true);
		}
		else
		{
			$('#<portlet:namespace/>allOtherFirmDiv #<portlet:namespace/>allFirmsCheckBoxCounterAttorneyMovesAndChanges').prop('checked', false);
		}
		
		 $('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
			 allFirmsListCounter++;
		 });
		 
		 if(allFirmsListCounter == 0)
			 {
				 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms ('+allFirmsListCounter+') Selected');
				 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (1) Selected)');
				 $('#<portlet:namespace/>allfirms').prop("checked", false);
				 $('#<portlet:namespace/>allOtherFirm').prop("checked", false);
				 $('#<portlet:namespace/>selectAllFirms').prop("checked", false);
				 
					var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
					
					$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]').each(function() {
						 if(this.value == watchListId)
							{
								$(this).prop("checked", true);
							}
					});
					 $('#<portlet:namespace/>firm_watchlist').prop("checked", true);
			 }
		 else
			 {
				 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms ('+allFirmsListCounter+') Selected');
				 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0) Selected)');
				 $('#<portlet:namespace/>allOtherFirm').prop("checked", true);
				 $('#<portlet:namespace/>selectAllFirms').prop("checked", true);
				 $('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
				 $('#<portlet:namespace/>firm_watchlist').prop("checked", false);
				 $('#<portlet:namespace/>allfirms').prop("checked", false);
			 }
	});

  
  
	  $('#<portlet:namespace/>hide, #<portlet:namespace/>popup').click(function(e){

		 $("#<portlet:namespace/>popupPracticeArea").hide();	
		 $("#<portlet:namespace/>popupLocation").hide();
		 $("#<portlet:namespace/>popupTitles").hide();	
		 $("#<portlet:namespace/>popupDate").hide();
		 $("#<portlet:namespace/>popupChangeType").hide();
		 
			var allFirmsCounter = 0;
			 
			$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:not(:checked)').each(function() {
				allFirmsCounter++;
			});

			if(allFirmsCounter != 0) 
			{
				$("#<portlet:namespace/>firmCounter").prop("checked", false);
			} 
			else
			{
				$("#<portlet:namespace/>firmCounter").prop("checked", true);
			}
	     e.stopPropagation();	   
	});   


	$('#<portlet:namespace/>practiceAreaId, #<portlet:namespace/>popupPracticeArea').click(function(e){
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupLocation").hide();
		 $("#<portlet:namespace/>popupTitles").hide();
		 $("#<portlet:namespace/>popupDate").hide();
		 $("#<portlet:namespace/>popupChangeType").hide();
		 
		 var allPracticeCounter = 0;
			$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:not(:checked)').each(function() {
					allPracticeCounter++;
			});

			if(allPracticeCounter != 0) 
			{
				$("#practiceSelection").prop("checked", false);
			} 
			else
			{
				$("#practiceSelection").prop("checked", true);
			}
		 
	     e.stopPropagation();   
	});
	 
	
	$('#ui-datepicker-div').click(function(e){		
		$("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupPracticeArea").hide();
		 $("#<portlet:namespace/>popupLocation").hide();
		 $("#<portlet:namespace/>popupTitles").hide();	
		 $("#<portlet:namespace/>popupChangeType").hide();
	    e.stopPropagation();   
	});

	$('#<portlet:namespace/>datenone, #<portlet:namespace/>popupDate').click(function(e){
		
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupPracticeArea").hide();
		 $("#<portlet:namespace/>popupLocation").hide();
		 $("#<portlet:namespace/>popupTitles").hide();	
		 $("#<portlet:namespace/>popupChangeType").hide();
	  	 e.stopPropagation();   
	});

	$('#<portlet:namespace/>locationId, #<portlet:namespace/>popupLocation').click(function(e){
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupPracticeArea").hide();
		 $("#<portlet:namespace/>popupTitles").hide();	
		 $("#<portlet:namespace/>popupDate").hide();
		 $("#<portlet:namespace/>popupChangeType").hide();
		 
		 var allLocationsCounter = 0;
		 $('#<portlet:namespace/>allOtherLocationsDiv input[type=checkbox]:not(:checked)').each(function() {
			 allLocationsCounter++;
		 });
		 
		 if(allLocationsCounter != 0)
		{
		 	$('#locationSelection').prop("checked", false);
		}
		else
		{
			$("#locationSelection").prop("checked", true);
		}
	     e.stopPropagation();   
	});

	
	$('#<portlet:namespace/>titleDropdown, #<portlet:namespace/>popupTitles').click(function(e){
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupPracticeArea").hide();
		 $("#<portlet:namespace/>popupLocation").hide();
		 $("#<portlet:namespace/>popupDate").hide();
		 $("#<portlet:namespace/>popupChangeType").hide();
	     e.stopPropagation();   
	});
	
	$('#<portlet:namespace/>popupChangeType, #<portlet:namespace/>changeTypeDropdown').click(function(e){
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupPracticeArea").hide();
		 $("#<portlet:namespace/>popupLocation").hide();
		 $("#<portlet:namespace/>popupDate").hide();
		 $("#<portlet:namespace/>popupTitles").hide();	
	     e.stopPropagation();   
	});
	
	
	$(document).click(function(){
	    $("#<portlet:namespace/>popup").hide(); 
	    $("#<portlet:namespace/>popupPracticeArea").hide();
	    $("#<portlet:namespace/>popupLocation").hide();		 
	    $("#<portlet:namespace/>popupTitles").hide();
	    $("#<portlet:namespace/>popupDate").hide();
	    $("#<portlet:namespace/>popupChangeType").hide();
	});
  
	$('#<portlet:namespace/>btnAdd').click(function(){
		$('.filtersPage').hide();
		$('#<portlet:namespace/>additional').show();
	})
	$('#<portlet:namespace/>btnSave').click(function(){
		$('.filtersPage').show();
		$('#<portlet:namespace/>additional').hide();
	});
	
	$('#<portlet:namespace/>hide').click(
    	function () {
        //open firms submenu/popup
        $("#<portlet:namespace/>popup").stop().slideToggle(500);    
    });  
    
    
    
    $('#<portlet:namespace/>titleDropdown').click(
    	function () {
    	 //open Title submenu/popup
    	 $("#<portlet:namespace/>popupTitles").stop().slideToggle(500);    
   });
	
	$('#<portlet:namespace/>locationId').click(
		 function () {
		 //open location submenu/popup
		 $("#<portlet:namespace/>popupLocation").stop().slideToggle(500);    
	});
	
	$('#<portlet:namespace/>practiceAreaId').click(
		function () {
		//open practice area submenu/popup
		$("#<portlet:namespace/>popupPracticeArea").stop().slideToggle(500);    
	});
	
	$('#<portlet:namespace/>changeTypeDropdown').click(
		function () {
		//open practice area submenu/popup
		$("#<portlet:namespace/>popupChangeType").stop().slideToggle(500);    
	});
	
	$('#<portlet:namespace/>datenone').click(
		function () {
		    //show date submenu
		    $("#<portlet:namespace/>popupDate").stop().slideToggle(500);    
	}); 
	

	//UI Logic for Firms
	$("input[id='<portlet:namespace/>allfirms']").change(function(){
		var checked = $("#<portlet:namespace/>allfirms").is(":checked");
		if(checked){		 
		 $('#<portlet:namespace/>firm_watchlist').prop("checked", false);
		 $('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
		 $('#<portlet:namespace/>individualfirmsWatchList').find('input[type=checkbox]:checked').removeAttr('checked');
		 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="Abrams" id="<portlet:namespace/>firmCounter"> Firms (0) Selected');
		}
		<portlet:namespace/>applyFirms();
	});
	
	$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]').change(function(){
	
			var allFirmsCounter = 0;
			 $('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
				 allFirmsCounter++;
			    });
				
				$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="Abrams" id="<portlet:namespace/>firmCounter"> Firms ('+allFirmsCounter+') Selected');
				
				if(allFirmsCounter > 0)
				{
					 $('#<portlet:namespace/>allOtherFirm').prop("checked", true);
				}
				else{
					$('#<portlet:namespace/>allOtherFirm').prop("checked", false);
				}
			
	});

	$('#<portlet:namespace/>clearButton').bind('click', function (event) {
		<portlet:namespace/>clearFirms();
		//applyFirms();
	});
	
	
	
	$("input[id='<portlet:namespace/>allLocations']").change(function(){
		var checked = $("#<portlet:namespace/>allLocations").is(":checked");
		if(checked){  		
		 $('#<portlet:namespace/>allOtherLocationsDiv').find('input[type=checkbox]:checked').removeAttr('checked');		
		 $('#<portlet:namespace/>ApplyLocations').removeAttr('disabled');
		 $('#<portlet:namespace/>ApplyLocations').removeClass("ui-state-disabled");
		}
		<portlet:namespace/>applyLocations();
	});
	
	$("input[id='<portlet:namespace/>allPracticeArea']").change(function(){
		var checked = $("#<portlet:namespace/>allPracticeArea").is(":checked");
		if(checked){  		
		 $('#<portlet:namespace/>allOtherPracticeArea').find('input[type=checkbox]:checked').removeAttr('checked');	
		 $('#<portlet:namespace/>ApplyPracticeArea').removeAttr("disabled");
		 $('#<portlet:namespace/>ApplyPracticeArea').removeClass("ui-state-disabled");
		}
		<portlet:namespace/>applyPracticeArea();
	});
	
	$("input[id='<portlet:namespace/>allTitles']").change(function(){
		var checked = $("#<portlet:namespace/>allTitles").is(":checked");
		if(checked){  	
		 $('#<portlet:namespace/>allOtherTitlesDiv').find('input[type=checkbox]:checked').removeAttr('checked');
		 $('#<portlet:namespace/>ApplyTitles').removeAttr("disabled");
		 $('#<portlet:namespace/>ApplyTitles').removeClass("ui-state-disabled");
		}
		<portlet:namespace/>applyTitle();
	});
	
	
	$("#<portlet:namespace/>datePeriodSelect").change(function() {
		 $("#<portlet:namespace/>period").prop("checked", true);
		 $("#<portlet:namespace/>dateRange").removeAttr("checked");
		 <portlet:namespace/>applyDate(); // apply them as the change
	});
	
	
	$('#<portlet:namespace/>allOtherTitlesDiv input[type=checkbox]').change(function(){
		var allTitlesCounter = 0;
		 $('#<portlet:namespace/>allOtherTitlesDiv input[type=checkbox]:checked').each(function() {
			 allTitlesCounter++;
		    });				
			if(allTitlesCounter > 0)
			{
				 $('#<portlet:namespace/>allTitles').prop("checked", false);
			}
			
			<portlet:namespace/>applyTitle();
					
});

	
	$("input[id='<portlet:namespace/>allChangeType']").change(function(){
		var checked = $("#<portlet:namespace/>allChangeType").is(":checked");
		if(checked){	
		 $('#<portlet:namespace/>allOtherChangeTypeDiv').find('input[type=checkbox]:checked').removeAttr('checked');
		}
		<portlet:namespace/>applyChangeType();
	});
	
	$('#<portlet:namespace/>allOtherChangeTypeDiv input[type=checkbox]').change(function(){
		var allChangeTypeCounter = 0;
		 $('#<portlet:namespace/>allOtherChangeTypeDiv input[type=checkbox]:checked').each(function() {
			 allChangeTypeCounter++;
		    });				
			if(allChangeTypeCounter > 0)
			{
				 $('#<portlet:namespace/>allChangeType').prop("checked", false);
			}
			<portlet:namespace/>applyChangeType();
					
	});
	
	$("input[name='allLocationsCheckBox']").change(function(){
		var lengthofCheckBoxes = $("input[name='allLocationsCheckBox']:checked").length;
		if(lengthofCheckBoxes>0){
		$('#<portlet:namespace/>ApplyLocations').removeAttr("disabled");
		$('#<portlet:namespace/>ApplyLocations').removeClass("ui-state-disabled");
		$('#<portlet:namespace/>allOtherLocation').prop("checked", true);
		} 
		<portlet:namespace/>applyLocations();
	}); 
	
	$("input[name='practiceAreaCheckBox']").change(function(){
		var lengthofCheckBoxes = $("input[name='practiceAreaCheckBox']:checked").length;
		if(lengthofCheckBoxes>0){
		$('#<portlet:namespace/>ApplyPracticeArea').removeAttr("disabled");
		$('#<portlet:namespace/>ApplyPracticeArea').removeClass("ui-state-disabled");
		$('#<portlet:namespace/>allPracticeArea').prop("checked", false);
		} 
		<portlet:namespace/>applyPracticeArea();
	});
	
	$("input[name='allTitlesCheckBox']").change(function(){
		var lengthofCheckBoxes = $("input[name='allTitlesCheckBox']:checked").length;
		if(lengthofCheckBoxes>0){
		$('#<portlet:namespace/>ApplyTitles').removeAttr("disabled");
		$('#<portlet:namespace/>ApplyTitles').removeClass("ui-state-disabled");
		$('#<portlet:namespace/>allTitles').prop("checked", false);
		
		} else{
		$('#<portlet:namespace/>ApplyTitles').attr("disabled", true);
		$('#<portlet:namespace/>ApplyTitles').addClass("ui-state-disabled");
		}
		<portlet:namespace/>applyChangeType();
	});
	
	//UI Logic for Practice Area
	
	$(".allPracticeArea").change(function(){
		var checked = $(this).is(":checked");
		if(checked){  		
		 $('#<portlet:namespace/>allOtherPracticeArea').find('input[type=checkbox]:checked').removeAttr('checked');	
		 $('#<portlet:namespace/>allPracticeAreaIndividual').removeAttr('checked');	
		 $("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="Abrams"> PracticeArea (0) Selected');
		}
	});
	
	$("#<portlet:namespace/>allPracticeAreaIndividual, #<portlet:namespace/>practiceAreaDiv input[type=checkbox]").click(function(){
		<portlet:namespace/>changeOnIndividualPracticeAreas();
		<portlet:namespace/>applyPracticeArea();
	});
	
	
	
	$('#<portlet:namespace/>clearPracticeArea').bind('click', function (event) {
		<portlet:namespace/>clearPracticeAreas();
	});

	
	
	//Change Type popupup values selection  
	
	$("input[name='allChangeTypeCheckBoxMovesAndChanges']").change(function(){
		var lengthofCheckBoxes = $("input[name='allChangeTypeCheckBoxMovesAndChanges']:checked").length;
		if(lengthofCheckBoxes>0){
		$('#<portlet:namespace/>ApplyallChangeType').removeAttr("disabled");
		$('#<portlet:namespace/>ApplyallChangeType').removeClass("ui-state-disabled");
		$('#<portlet:namespace/>allChangeType').prop("checked", false);
		} else{
		$('#<portlet:namespace/>ApplyallChangeType').attr("disabled", true);
		$('#<portlet:namespace/>ApplyallChangeType').addClass("ui-state-disabled");
		}
		allChangeType
	});
	
	$('#<portlet:namespace/>resetAll').bind('click', function (event) {
		
		<portlet:namespace/>clearAll();
		
		//name reset
		$('#<portlet:namespace/>name').val('');
		
		//keyword reset
		$('#keywords').val(''); 
		
		//hiding date error divs
		$('#<portlet:namespace/>fromdateError').hide();
		$('#<portlet:namespace/>todateError').hide();
		$('#<portlet:namespace/>dateValidError').hide();
		
	});
	
	//Function for showing selected PracticeArea counter	
	$("input[id='<portlet:namespace/>practiceAreaCheckBox']").click(function(){
		
		var allPracticeAreaCounter = 0;
				 $('#<portlet:namespace/>allOtherPracticeArea :checked').each(function() {
					 allPracticeAreaCounter++;
				    });
					
					$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="Abrams"> PracticeArea ('+allPracticeAreaCounter+') Selected');
					
					if(allPracticeAreaCounter == 0)
					{
					$('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", false);
					$('#<portlet:namespace/>allPracticeArea').prop("checked", false);
					}
				else
					{
					$('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", true);
					$('#<portlet:namespace/>allPracticeArea').prop("checked", false);
					}
				
	});
	
	$("input[id='<portlet:namespace/>allPracticeArea']").click(function(){
 		$('#<portlet:namespace/>practiceAreaCheckBox input[type=checkbox]:checked').removeAttr('checked');
 		$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="Abrams"> Practice Area (0) Selected');
	});
	
	
	//Function for showing selected Locations counter	
	$("input[id='<portlet:namespace/>allLocationsCheckBox']").click(function(){
		
		var allLocationsCounter = 0;
				 $('#<portlet:namespace/>allOtherLocationsDiv :checked').each(function() {
					 allLocationsCounter++;
				    });
					
					$("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="Abrams" id="<portlet:namespace/>selectAllLocations"> Locations ('+allLocationsCounter+') Selected');
					
					if(allLocationsCounter == 0)
					{
					$('#<portlet:namespace/>allOtherLocation').prop("checked", false);
					$('#<portlet:namespace/>allLocations').prop("checked", false);
					}
				else
					{
					$('#<portlet:namespace/>allOtherLocation').prop("checked", true);
					$('#<portlet:namespace/>allLocations').prop("checked", false);
					}
					<portlet:namespace/>applyLocations();
	});
	
	$("input[id='<portlet:namespace/>allLocations']").click(function(){
 		$('#<portlet:namespace/>allLocationsCheckBox input[type=checkbox]:checked').removeAttr('checked');
 		$("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="Abrams" id="<portlet:namespace/>selectAllLocations"> Locations (0) Selected');
	});

	$("input[id='<portlet:namespace/>practiceAreaCheckBox']").click(function(){
		
		var allPracticeAreaCounter = 0;
				 $('#<portlet:namespace/>practiceAreaDiv :checked').each(function() {
					 allPracticeAreaCounter++;
				    });
					
					$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="Abrams"> PracticeArea ('+allPracticeAreaCounter+') Selected');
					
					if(allPracticeAreaCounter == 0)
					{
					$('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", false);
					$('#<portlet:namespace/>allPracticeArea').prop("checked", false);
					}
				else
					{
					$('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", true);
					$('#<portlet:namespace/>allPracticeArea').prop("checked", false);
					}
				
	});


$("input[id='<portlet:namespace/>allPracticeArea']").click(function(){
		$('#<portlet:namespace/>practiceAreaCheckBox input[type=checkbox]:checked').removeAttr('checked');
		$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="Abrams"> PracticeArea (0) Selected');
});


	
	
	$('#<portlet:namespace/>clearPracticeArea').bind('click', function (event) {		
		<portlet:namespace/>clearPracticeAreas();
	});
	
	$('#<portlet:namespace/>clearButtonTitles').bind('click', function (event) {
		$('#<portlet:namespace/>popupTitles').find('input[type=checkbox]:checked').removeAttr('checked');		
		$('#<portlet:namespace/>allTitles').prop('checked', true);		
	});
	
	$('#<portlet:namespace/>clearButtonLocations').bind('click', function (event) {		
		<portlet:namespace/>clearLocations();
		
	});
	
	$('#<portlet:namespace/>clearButtonDate').bind('click', function (event) {
		$('#<portlet:namespace/>popupDate').find("option").attr("selected", false);
		$('#<portlet:namespace/>popupDate').find('input[type=text]').val('');
		$('#<portlet:namespace/>popupDate').find('input[type=radio]:checked').removeAttr('checked');				
		$("#<portlet:namespace/>dateText").val('<%=ALMConstants.LAST_WEEK%>');	
		$("#<portlet:namespace/>period").prop("checked", true); 
		$("#<portlet:namespace/>datePeriodSelect").val('<%=ALMConstants.LAST_WEEK%>');	
		$("#<portlet:namespace/>from").datepicker('enable');
		$("#<portlet:namespace/>to").datepicker('enable');
		<portlet:namespace/>applyDate();
	}); 
	
	//Change Type clear button event:Will clear all selected values in and reset change type values to default
	$('#<portlet:namespace/>clearButtonallChangeType').bind('click', function (event) {
		<portlet:namespace/>clearChangeType();
	});

	
	$('#<portlet:namespace/>ApplyFirm').bind('click', function (event) {
		<portlet:namespace/>applyFirms();
		$("#<portlet:namespace/>popup").toggle();
	});
	
	$('#<portlet:namespace/>ApplyPracticeArea').bind('click', function (event) {
		<portlet:namespace/>applyPracticeArea();
		 $("#<portlet:namespace/>popupPracticeArea").toggle();
	
	});
	
	$('#<portlet:namespace/>ApplyTitles').bind('click', function (event) {
		<portlet:namespace/>applyTitle();
		 $("#<portlet:namespace/>popupTitles").toggle();
	});
	
	$('#<portlet:namespace/>ApplyLocations').bind('click', function (event) {		
		if($("#<portlet:namespace/>allLocations").is(":checked")){
			$("#<portlet:namespace/>selectedLocation").val('<%=ALMConstants.ALL_LOCATIONS%>');			
		} else{
				var allValsLocation = [];
				 $('#<portlet:namespace/>allOtherLocationsDiv :checked').each(function() {
					 allValsLocation.push($(this).val());
				    });
				    $("#<portlet:namespace/>selectedLocation").val(allValsLocation.join(";"));		
			   }
		
		 $("#<portlet:namespace/>popupLocation").toggle();	
	});
	
 	$('#<portlet:namespace/>ApplyDate').bind('click', function (event) {	
		
 		<portlet:namespace/>applyDate();
		 $("#<portlet:namespace/>popupDate").toggle();

	}); 
 	
	 $('#<portlet:namespace/>practiceAreaCounter, #practiceAreaCounter').click(function() {	 
	
		 
			var checked = $("#<portlet:namespace/>selectAllPracticeAreas").is(":checked");
			var allPracticeAreasListCounter = 0;
			if(checked)
			{   
				 $('#<portlet:namespace/>allOtherPracticeArea #<portlet:namespace/>practiceId').prop('checked', true);
				 $('#practiceArea1').prop('checked', false);
			}
			else
			{
				$('#<portlet:namespace/>allOtherPracticeArea #<portlet:namespace/>practiceId').prop('checked', false);
			}
			 $('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:checked').each(function(){
				 allPracticeAreasListCounter++;
			 });
			 
			 if(allPracticeAreasListCounter == 0)
				 {
					 $("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="<portlet:namespace/>selectAllPracticeAreas" /> Practice Areas ('+allPracticeAreasListCounter+') Selected');
					 $('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", false);
					 $('#<portlet:namespace/>selectAllPracticeAreas').prop("checked", false);
					 $('.allPracticeArea').prop("checked", true);
					 $('#<portlet:namespace/>selectedPracticeArea').val('<%=ALMConstants.ALL_PRACTICE_AREAS %>');
				 }
			 else
				 {
					 $("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="<portlet:namespace/>selectAllPracticeAreas" /> Practice Areas  ('+allPracticeAreasListCounter+') Selected');
					 $('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", true);
					 $('#<portlet:namespace/>selectAllPracticeAreas').prop("checked", true);
				 }
		}); 
	 
	 $('#<portlet:namespace/>locationsCounter').click(function() {
		 
			var checked = $("#<portlet:namespace/>selectAllLocations").is(":checked");
			var allLocationsListCounter = 0;
			if(checked)
			{   
				 $('#<portlet:namespace/>allOtherLocationsDiv .allLocationsCheckBox').prop('checked', true);
			}
			else
			{
				$('#<portlet:namespace/>allOtherLocationsDiv .allLocationsCheckBox').prop('checked', false);
			}
			
			 $('#<portlet:namespace/>allOtherLocationsDiv input[type=checkbox]:checked').each(function() {
				 allLocationsListCounter++;
			 });
			 if(allLocationsListCounter == 0)
				 {
					 $("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="<portlet:namespace/>selectAllLocations" /> Locations ('+allLocationsListCounter+') Selected');
					 $('#<portlet:namespace/>allLocations').prop("checked", true);
					 $('#<portlet:namespace/>allOtherLocations').prop("checked", false);
					 $('#<portlet:namespace/>selectAllLocations').prop("checked", false);
					 $('#<portlet:namespace/>selectedLocation').val('<%=ALMConstants.ALL_LOCATIONS %>');
				 }
			 else
				 {
					 $("#<portlet:namespace/>locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="<portlet:namespace/>selectAllLocations" /> Locations ('+allLocationsListCounter+') Selected');
					 $('#<portlet:namespace/>allLocations').prop("checked", false);
					 $('#<portlet:namespace/>allOtherLocations').prop("checked", true);
					 $('#<portlet:namespace/>selectAllLocations').prop("checked", true);
				 }
		});
 	
 	//Reset all the option of View setting popup
    $("#<portlet:namespace/>resetAllViewSetting").click(function() {		     
    		$('input:radio[name=orderBy]:nth(1)').prop('checked',true);  //firms/orgs, the 2nd radiobutton
           $('input:radio[name=searchResultsPerPage]:nth(2)').prop('checked',true);  //100, the 3rd radiobutton  	
     });
    
    //Will close view popup(This event would trigger when user click on Cancel )
     $("#<portlet:namespace/>CancelSetting").click(function() {
     	 $("#<portlet:namespace/>viewSettingsPopup").toggle(); 
     });  
     
     //Will close view popup
      $("#<portlet:namespace/>viewSettingClose").click(function() {
     	 $("#<portlet:namespace/>viewSettingsPopup").toggle(); 
     	
     }); 
     
   $("#attorney-moves-analysis").removeClass("active-menu");
  	$("#attorney-moves-details").addClass("active-menu");

	$('#<portlet:namespace/>ApplyallChangeType').bind('click', function (event) {
		  <portlet:namespace/>applyChangeType();		
		 $("#<portlet:namespace/>popupChangeType").toggle();
	
	});
	


	
/* 
	//this method get invoked when user clicks on Apply button from Practice Area popup and set selected practice area values to Practice area text box
	$('#ApplyPracticeArea').bind('click', function (event) {
		applyPracticeArea();
		 $("#popupPracticeArea").toggle();
	 */
	 
	// Function for Event search based on selected criteria like Firms,Practice Area,Location, Dates and Keywords
	 $("#<portlet:namespace/>applySearch").click(function() {	
		 <portlet:namespace/>search();
	}); 
	

		//Spring will add <input> tags with names prefixed with "_" for type "radio" and "checkbox".
		//On form POST submit they all are carried to server which is unneccessary
		//hence removing all such input box to make page and jQuery selections less heavy :) on document ready.
		
		//NOTE : If any inputs added with name prefixed with "_" purposefully will also get removed. So be cautious
	$("#attorneyMoveSearchModelBean input[name^=_]").remove();
		
	//autoselect previously selected search critria
	<portlet:namespace/>initializeSearchCriteria();
});

(function(){
      var del = 200;
      $('.icontent').hide().prev('a').hover(function(){
        $(this).next('.icontent').stop('fx', true).slideToggle(del);
      });
    })();
    
</script>
<script type="text/javascript">
	function setPage(goToPage)
	{
		// Sets the page number to the one selected by the user
		// and fires an AJAX submit to refresh with the new page
		// Does NOT tinker with the sort settings
		$("#<portlet:namespace/>goToPage").val(goToPage);
		$("#attorneyMoveSearchModelBean").submit();
	}
	
	function sortResults(sortColumn)
	{
		// Changes the sort order to the new column selected by
		// the user along with the sort direction (ascending)
		// Also RESETS the page to 1
		
		var lastSortColumn  =  $("#<portlet:namespace/>sortColumn").val();
		
		if(lastSortColumn==sortColumn)
		{
			var lastSortOrder= $("#<portlet:namespace/>sortOrder").val();
			if(lastSortOrder=="asc")
			{
				$("#<portlet:namespace/>sortOrder").val("desc");
			}
			else if(lastSortOrder=="desc")
			{
				$("#<portlet:namespace/>sortOrder").val("asc");
			}
			else
			{
				$("#<portlet:namespace/>sortOrder").val("asc");
			}
		}
		else
		{
			$("#<portlet:namespace/>sortOrder").val("asc");
			
		}
		
		$("#<portlet:namespace/>sortColumn").val(sortColumn);
		$("#<portlet:namespace/>goToPage").val(1);
		$("#attorneyMoveSearchModelBean").submit();
	}
	
	/* function sortDesc(sortColumn)
	{
		// Changes the sort order to the new column selected by
		// the user along with the sort direction (descending)
		// Also RESETS the page to 1
		$("#sortColumn").val(sortColumn);
		$("#sortOrder").val("desc");
		$("#goToPage").val(1);
		$("#attorneyMoveSearchModelBean").submit();
	} */
	
	if(<%=isHomePage%>)
	{
		
		$('#homePageShowAttorneyMC').hide();
	}
	else
	{
		$('#homePageShowAttorneyMC').show();
	}
	
</script>

<%
	String displayStr = "";
	if(isHomePage)
	{
		displayStr = "display:none";
	}
	
%>

<div id="homePageShowAttorneyMC" style="<%=displayStr%>" >
<div class="breadcrumbs"><span>Moves and Changes</span></div>

<form:form  commandName="attorneyMoveSearchModelBean" method="post" action="${submitURL}" id="attorneyMoveSearchModelBean" autocomplete="off">
 	<form:hidden path="goToPage"  id="goToPage" />
 	<form:hidden path="sortColumn" id="sortColumn" />
 	<form:hidden path="sortOrder"  id="sortOrder" /> 	
	
	<div id="dropdown">
		<!-- dropdown_title_start -->
		<ul id="droplist">			
 		<li><label>Firm(s)</label>
				<div class="srchBox" style="width:182px">
					
					     <c:choose>
			              <c:when test="${empty allWatchListsDefaultList}">
			          			<input type="text" name="Firmstext" id="Firmstext"  value="<%=ALMConstants.AMLAW_100 %>" style="text-overflow:ellipsis;" class="input" autocomplete="off"/>
			          	  </c:when>
			          	  <c:otherwise>
			          			<input type="text" name="Firmstext" id="Firmstext"  value="" style="text-overflow:ellipsis;" class="input" autocomplete="off"/>
			          	  </c:otherwise>
			          	</c:choose>
 
 
						  <!--  <input type="text" name="Firmstext" id="Firmstext"  style="text-overflow:ellipsis;" class="input" /> -->
						   <input type="button" name="search" value="" class="srchBack" id="testDiv" />
						   <div class="clear">&nbsp;</div>
				</div> <input type="button" name="search" id="hide" value="" class="typeSel" />


				<div class="rel">
					<div id="<portlet:namespace/>popup" class="firmPage">
						<p><form:radiobutton path="firmType" id="allfirms" value="<%=ALMConstants.ALL_FIRMS %>" />&nbsp;<%=ALMConstants.ALL_FIRMS %></p>
						<p> <form:radiobutton path="firmType" id="firm_watchlist" value=""/>Select Watchlist/ RivalEdge List</p>
					
						
						<div class="individualfirms-Watchlist" >
                			<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0 Selected)<br />
               			</div>
               			<div class="individualfirmsWatchList Select-Individual-Firms" id="<portlet:namespace/>individualfirmsWatchList"> 
						<form:checkbox  class="rivaledgeListAMLAW_100" path="firmList"  value="<%=ALMConstants.AMLAW_100 %>" /><%=ALMConstants.AMLAW_100 %><br>
						<form:checkbox  class="rivaledgeListAMLAW_200" path="firmList"  value="<%=ALMConstants.AMLAW_200 %>" /><%=ALMConstants.AMLAW_200 %><br>
						<form:checkbox  class="rivaledgeListNLJ_250" path="firmList"  value="<%=ALMConstants.NLJ_250 %>" /><%=ALMConstants.NLJ_250 %><br>
  
 				 		<div  class="individualfirmsWatchListDIV" id="<portlet:namespace/>individualfirmsWatchListDIV"> 
			                <c:forEach var="watchlist"  items="${allWatchLists}">     
		    	  				 <form:checkbox  class="allFirmsCheckBoxWatchList" path="firmListWatchList"  value="${watchlist.groupId}" labelAttr="${watchlist.groupName}" />${watchlist.groupName}<br>
		  				 	</c:forEach>
						</div>
						</div>
						<br>
						
						<p><form:radiobutton path="firmType" value="<%=ALMConstants.INDIVIDUAL_LIST %>" id="allOtherFirm"/>&nbsp;<%=ALMConstants.INDIVIDUAL_LIST %></p>
						
						<div class="individualfirms-first" id="<portlet:namespace/>individualFirmsCounter"> 
	               			 <input type="checkbox" value="selectAllFirms" id="selectAllFirms"/>Firms (0 Selected)<br />
              			</div>
						
						<div class="Select-Individual-Firms" id="<portlet:namespace/>allOtherFirmDiv">
							<c:forEach var="firm" items="${allFirms}">
								<form:checkbox  class="allFirmsCheckBox" id="allFirmsCheckBoxCounterAttorneyMovesAndChanges" path="firmList" value="${firm.companyId}" labelAttr="${firm.company}"/>${firm.company}<br>
							</c:forEach>
						</div>
 						<div class="popupsubmit">
 						 <input type="button" class="buttonOne" value="Apply" id="ApplyFirm">
							<input type="button" class="buttonTwo" value="Clear All" id="clearButton">
						</div>

					</div>
				</div>
				 <c:if test="${not empty allWatchListsDefaultList}">
					<input type="hidden" name="defaultWatchListId" id="defaultWatchListId" value="${allWatchListsDefaultList.groupId}" />
				</c:if>
			</li>
 		
			<li><label>Change Type</label>
				<div class="srchBox">
					<input type="text" name="search" value="" class="input" style="text-overflow: ellipsis;"
						id="selectedChangeType" autocomplete="off"/> <input type="button" name="search"
						value="" class="srchBack" />
					<div class="clear">&nbsp;</div>
				</div> <input type="button" name="search" value="" class="typeSel"
				id="changeTypeDropdown" />

				<div class="rel">
					<div id="<portlet:namespace/>popupChangeType" class="firmPage" style="width: 220px">
						<div id="<portlet:namespace/>changeType">
							<form:checkbox path="changeType"  name="allChangeType" id="allChangeType" value="<%=ALMConstants.ALL_MOVES_CHANGES%>" />&nbsp;<%=ALMConstants.ALL_MOVES_CHANGES%><br>
							<div id="<portlet:namespace/>allOtherChangeTypeDiv">
								<form:checkbox path="changeType" name="allChangeTypeCheckBoxMovesAndChanges" value="<%=ALMConstants.ADDITIONS%>" id="AdditionsChangeType" />&nbsp;Additions<br>
								<form:checkbox path="changeType" name="allChangeTypeCheckBoxMovesAndChanges" value="<%=ALMConstants.REMOVALS%>"  id="RemovalsChangeType"/>&nbsp;Removals<br>
								<form:checkbox path="changeType" name="allChangeTypeCheckBoxMovesAndChanges" value="<%=ALMConstants.UPDATES%>"/>&nbsp;Updates<br>
							</div>
						</div>
						<div class="popupsubmit">
						<input type="button" class="buttonOne"
							value="Apply" id="ApplyallChangeType">
						<input type="button" class="buttonTwo" value="Clear All"
							id="clearButtonallChangeType"> 
						</div>
					</div>
				</div></li>
			<li><label>Location(s)</label>
				<div class="srchBox">
					<input type="text" value="<%=ALMConstants.ALL_LOCATIONS%>"
						class="input" name="selectedLocation" id="selectedLocation" style="text-overflow: ellipsis;" autocomplete="off"/> <input
						type="button" name="search" value="" class="srchBack" />
					<div class="clear">&nbsp;</div>
				</div> <input type="button" name="search" value="" id="locationId"
				class="typeSel" />
				<div class="rel">
					<div id="<portlet:namespace/>popupLocation" class="firmPage" style="width: 250px">						
						<p><form:radiobutton path="locations"  id="allLocations" value="<%=ALMConstants.ALL_LOCATIONS%>"  /> <%=ALMConstants.ALL_LOCATIONS %></p>	
						<p><input type="radio"  id="allOtherLocation" value="Select Individual Locations" /> Select Individual Locations</p>
						
						<div class="individualfirms-first" id="<portlet:namespace/>locationsCounter"> 
								<input type="checkbox" value="selectAllLocations" id="selectAllLocations" />Locations (0) Selected<br> </div>
						<div class="Select-Individual-Firms" id="<portlet:namespace/>allOtherLocationsDiv">		
							<c:forEach items="${allOtherLocations}" var="location">
								<form:checkbox path="locations" name="allLocationsCheckBox" id="allLocationsCheckBox" class="allLocationsCheckBox" value="${location}"/>${location}<br>
							</c:forEach>	   
						</div>				
						<div class="popupsubmit">
						<input type="button" class="buttonOne" value="Apply" id="ApplyLocations">
						<input type="button" class="buttonTwo" value="Clear All" id="clearButtonLocations">
						</div>
					</div>
				</div></li>
			<li><label>Practice Area(s)</label>
				<div class="srchBox">
					<input type="text" value="All Practice Areas" class="input" style="text-overflow: ellipsis;"
						name="selectedPracticeArea" id="selectedPracticeArea" autocomplete="off"/> <input
						type="button" name="search" value="" class="srchBack" />
					<div class="clear">&nbsp;</div>
				</div> <input type="button" name="search" value="" id="practiceAreaId"
				class="drpDwn" />
				<div class="rel">
					<div id="<portlet:namespace/>popupPracticeArea" class="firmPage" style="width: 270px">
					
					<!-- <input type="radio" name="allPracticeArea"
							id="allPracticeArea" value="All Practice Areas"
							checked="checked">&nbsp;All Practice Areas<br> -->
					<p><form:radiobutton path="practiceArea" name="allPracticeArea" id="allPracticeArea" class="allPracticeArea" value="<%=ALMConstants.ALL_PRACTICE_AREAS %>" /><%=ALMConstants.ALL_PRACTICE_AREAS%></p>
					<p><input type="radio"  name="allPracticeArea" id="allPracticeAreaIndividual" value="Select Individual Practice Areas" />&nbsp;Select Individual Practice Areas</p>
					<div class="individualfirms-first" id="<portlet:namespace/>practiceAreaCounter"> <input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" /> Practice Areas (0 Selected)<br> </div>
																									
						
						 <div id="<portlet:namespace/>practiceAreaDiv">										 
								<div class="Select-Individual-Firms" id="<portlet:namespace/>allOtherPracticeArea">
									<c:forEach var="practice" items="${allPracticeArea}">								
										<form:checkbox path="practiceArea"   id="practiceId" value="${practice.practiceArea}" practiceLabel="${practice.practiceArea}"/> ${practice.practiceArea}<br>
									</c:forEach>
								</div>	
						 </div>
						 
						<div class="clear"></div> 
						<div class="popupsubmit">
						<input type="button" class="buttonOne" value="Apply" id="ApplyPracticeArea" />
						<input type="button" class="buttonTwo" value="Clear All" id="clearPracticeArea" />
						</div>
					</div>
				</div></li>

			<li><label>Date(s)</label>
				<div class="srchBox">
					 <!-- <input name="Datetext" id="datetext" value="" readonly="readonly"
						class="input" style="width: 140px" />  -->
					 <form:input path="dateText" id="dateText" readonly="true" value="" cssClass="input" cssStyle="width:140px" autocomplete="off"/>
					
					<div class="clear">&nbsp;</div>
				</div> <input type="button" name="search" id="datenone" value=""
				class="drpDwn" />
				<div class="rel">
					<div id="<portlet:namespace/>popupDate" class="datePage">
						<input type="radio" name="date" id="period" />&nbsp;Period<br> <select
							id="datePeriodSelect">
							<option value="Any">Any</option>
							<option value="Last Week">Last Week</option>
							<option value="Last 30 Days">Last 30 Days</option>
							<option value="Last 90 Days">Last 90 Days</option>
							<option value="Last 6 Months">Last 6 Months</option>
							<option value="Last Year">Last Year</option>
						</select> <br> <input type="radio" name="date" id="dateRange"
							checked="checked" />&nbsp;Date Range<br>

						<div class="flLeft">
							<label for="from">From</label> <input type="text" id="from"
								name="from" />
						</div>
						<div class="flLeft">
							<label for="to">To</label> <input type="text" id="to" name="to" />
						</div>
						<div class="clear"></div>
						<span class="error" id="fromdateError"> Invalid From Date</span> <span
							class="error" id="todateError"> Invalid To Date</span> <span
							class="error" id="dateValidError"> To date should be more
							than from date</span> 
							<div class="popupsubmit Popup-Calendar">
							<input type="button" value="Apply"  class="buttonOne"
							id="ApplyDate">
							<input type="button" value="Clear All" class="buttonTwo"
							id="clearButtonDate"> 
							</div>
					</div>
				</div></li>
			<li class="Submit-Filter-Criteria"><label>&nbsp;</label> <input type="button" value="Apply"
				class="buttonOne"
				style="background:url(<%=themeDisplay.getPathThemeImages()%>/btn1.png) 0 0 repeat-x; border:1px solid #bf8d1f; -webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px; font:normal 12px Arial, Helvetica, sans-serif; color:#fff; padding:5px 10px; cursor:pointer; text-shadow: 0px 0px #FFF;"
				id="applySearch" /></li>
			<li><label>&nbsp;</label> <input type="button" value="Reset All"
				style="background:url(<%=themeDisplay.getPathThemeImages()%>/btn2.png) 0 0 repeat-x; border:1px solid #565656; -webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px; font:normal 12px Arial, Helvetica, sans-serif; color:#fff; padding:5px 10px; cursor:pointer; text-shadow: 0px 0px #FFF;"
				class="buttonTwo" id="resetAll" /></li>
			<li><label>&nbsp;</label> 
			<%-- <input type="hidden" name="selectedFirms" id="selectedFirms" value="<%=ALMConstants.AMLAW_100%>" /> --%> 
			<input type="hidden"  name="selectedFirms" id="selectedFirms"  title="" customAttr="" /></li>

		</ul>
		<div class="clear">&nbsp;</div>
	</div>
	<div class="filtersPage">
		<div class="barSec">
			<a href="#" class="dwnBx" id="<portlet:namespace/>btnAdd">Additional Filters</a>
		</div>
	</div>
	<div class="filtersPage" id="<portlet:namespace/>additional" style="display: none">
		<div id="dropdown">
			<!-- dropdown_title_start -->
			<ul id="droplist">
				<li><label>Title(s)</label>
					<div class="srchBox">
						<input type="text" name="search" value="All" class="input" style="text-overflow: ellipsis;" id="selectedTitles" autocomplete="off"/>
							 <input type="button" name="search" value="" class="srchBack" />
						<div class="clear">&nbsp;</div>
					</div> <input type="button" name="search" value="" class="typeSel"
					id="titleDropdown" />

					<div class="rel">
						<div id="<portlet:namespace/>popupTitles" class="firmPage" style="width: 220px">

							<div id="<portlet:namespace/>titlesDiv">
								<form:checkbox  name="allTitles" path="titles"
									value="<%=ALMConstants.ALL_TITLES%>" id="allTitles" />&nbsp;<%=ALMConstants.ALL_TITLES%>
								<div id="<portlet:namespace/>allOtherTitlesDiv">
									<form:checkbox path="titles" name="allTitlesCheckBox" value="<%=ALMConstants.PARTNERS%>" />&nbsp;<%=ALMConstants.PARTNERS%><br>
									<form:checkbox path="titles" name="allTitlesCheckBox" value="<%=ALMConstants.ASSOCIATE%>" />&nbsp;<%=ALMConstants.ASSOCIATE%><br>
									<form:checkbox path="titles" name="allTitlesCheckBox" value="<%=ALMConstants.OTHER_COUNSEL%>" />&nbsp;<%=ALMConstants.OTHER_COUNSEL%><br>
									<form:checkbox path="titles" name="allTitlesCheckBox" value="<%=ALMConstants.ADMINISTRATIVE%>" />&nbsp;<%=ALMConstants.ADMINISTRATIVE%><br>
									<form:checkbox path="titles" name="allTitlesCheckBox" value="<%=ALMConstants.OTHER%>" />&nbsp;<%=ALMConstants.OTHER%><br>
								</div>
							</div>
							
							<div class="popupsubmit">
								<input type="button" class="buttonOne" value="Apply" id="ApplyTitles">
								<input type="button" class="buttonTwo" value="Clear All" id="clearButtonTitles">
							</div>

						</div>
					</div></li>

				<li><label>Name</label>
					<div class="srchBox" style="width: 175px">
						<!-- <input type="text" name="search" class="input"
							style="width: 140px" id="name" size="100" /> -->
						<form:input path="name" id="name" name="search" value="" cssClass="input" cssStyle="width:140px" size="100" autocomplete="off"/>
						<input
							type="button" name="search" value="" class="srchBack" />
						<div class="clear">&nbsp;</div>
					</div></li>
			<!-- Sachin: code commented to fix the bug 415:  Eliminate Keyword in Bio Search Field -->
			<!-- 	<li>
					<div class="col2-1">
						<label>Keywords in Bio</label>
						<div class="srchBox">
							<form:input path="keywords" id="keywords" name="keywordsearch" value="" cssClass="input" cssStyle="width:140px" size="100"/>
							<div class="clear">&nbsp;</div>
						</div>
					
					</div>
				</li>  -->
			</ul>
			<div class="clear">&nbsp;</div>
		</div>
		<div class="barSec">
			<a href="#" class="upBx" id="<portlet:namespace/>btnSave">Hide Filter</a>
		</div>
	</div>

<!-- 	<ul id="tabs">
		<li><a class="active" href="#" title="tab2"><span>Details</span></a></li>
		<li><a href="#" title="tab3"><span>Analysis</span></a></li>
	</ul> 
	<div id="resultsDataDiv"></div> -->
	
	<div style="display: none; position:absolute; margin:0; padding:0; background:#F0F0F0; border:none; width:400px !important;" class="viewBox popusdiv ClickPopup newspublicationPage charts" id="<portlet:namespace/>viewSettingsPopup">
              <div class="popHeader" style="font-weight:bold;"><a style="margin-top:2px" class="btn icon closewhite closeOne flRight" href="#" id="<portlet:namespace/>viewSettingClose">Close</a> SETTINGS: ATTORNEY MOVES AND CHANGES
                <div class="clear">&nbsp;</div>
              </div>
              <div class="popMiddle">           
                <div class="col3-4">
                  <h6>Group By:</h6>
                  <form action="#" class="marlt3">
                    <ul class="reset list5">                   
                        
						  
						  <li> <form:radiobutton path="orderBy" value="-1"/>None <br /> </li>
						  <li> <form:radiobutton path="orderBy" value="6" />Firm <br />  </li>
						  <li> <form:radiobutton path="orderBy" value="8"/>Title <br /> </li>
						  <li><form:radiobutton path="orderBy" value="9"/>Location <br /></li> 
						  <li> <form:radiobutton path="orderBy" value="10"/>Practice <br /> </li>
                    </ul>
                  </form>
                </div>
                <div class="col3-4">
                  <h6>Display No. of Results</h6>
                  <form:radiobutton path="searchResultsPerPage"  value="25"  />25<br />
			     <form:radiobutton path="searchResultsPerPage"  value="50"  />50<br />
			     <form:radiobutton path="searchResultsPerPage"  value="100" />100<br />
			     <form:radiobutton path="searchResultsPerPage"  value="500" />500<br />
                </div>
                <div class="clear">&nbsp;</div>
              </div>
              <div class="popFooter">
                <div class="flLeft">
                  <input type="button" class="buttonTwo" value="Reset All" id="resetAllViewSetting">
                </div>
                <div class="flRight">
                  <input type="button" class="buttonOne" value="Apply" id="ApplySetting" onclick="<portlet:namespace/>applyViewSettings();">
                  <input type="button" class="buttonTwo" value="Cancel" id="CancelSetting">
                </div>
                <div class="clear">&nbsp;</div>
              </div>
            </div>
</form:form>
</div>
<!-- 
<script type="text/javascript">

//Omniture SiteCatalyst - START
s.prop22 = "premium";
s.pageName="rer:Attorny-Moves-changes-search";
s.channel="rer:Attorny-Moves-changes";
s.server="rer";
s.prop1="Attorny-Moves-changes"
s.prop2="Attorny-Moves-changes";
s.prop3="Attorny-Moves-changes";
s.prop4="Attorny-Moves-changes";
s.prop21=s.eVar21='current.user@foo.bar';	
s.events="event2";
s.events="event27";
s.t();
//Omniture SiteCatalyst - END
</script>
 -->

