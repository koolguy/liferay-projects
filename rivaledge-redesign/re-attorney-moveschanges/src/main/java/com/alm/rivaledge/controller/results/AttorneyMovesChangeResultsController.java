package com.alm.rivaledge.controller.results;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFHyperlink;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.alm.rivaledge.controller.AbstractAttorneyMovesChangesController;
import com.alm.rivaledge.model.AttorneyMoveSearchModelBean;
import com.alm.rivaledge.model.PeopleSearchModelBean;
import com.alm.rivaledge.persistence.dao.CacheDAO;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.service.AttorneyService;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.service.WatchlistService;
import com.alm.rivaledge.transferobject.AttorneyMoveChangesResultDTO;
import com.alm.rivaledge.transferobject.AttorneyMoveChangesSearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.kernel.util.ContentTypes;
import com.liferay.portal.kernel.util.FastDateFormatFactoryUtil;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.util.PortalUtil;
import com.liferay.util.servlet.ServletResponseUtil;


@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "attorneyMoveSearchModelBean", "attorneyMoveChangesSearchDTO", "attorneyMoveChangesResult" })
public class AttorneyMovesChangeResultsController extends AbstractAttorneyMovesChangesController
{
	@Autowired
	private FirmService	firmService;

	@Autowired
	private WatchlistService watchlistService;

	@Autowired
	private AttorneyService	attorneyService;
	
	@Autowired
	private CacheDAO	cacheDAO;	// NEVER call a DAO directly from a Controller; replace with a service instance
	
	@RenderMapping
	public String attorneyMovesResultsView(@ModelAttribute("attorneyMoveSearchModelBean") AttorneyMoveSearchModelBean attorneyMoveSearchModelBean,
										   @ModelAttribute("attorneyMoveChangesSearchDTO") AttorneyMoveChangesSearchDTO attorneyMoveChangesSearchDTO,
										   ModelMap model,  
										   RenderRequest request, 
										   RenderResponse response) throws Exception
	{
		User currentUser = (model.get("currentUser") != null) ? ((User) model.get("currentUser")) : WebUtil.getCurrentUser(request, userService); 
		
		boolean displayMessage = false;
		
		HttpServletRequest 	originalRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(request));
		String 				firmName 		= originalRequest.getParameter("drilldownFirmName");
		if ((firmName != null) && (firmName.trim().length() > 0))
		{
			String 		drilldownFirm 	= URLDecoder.decode(firmName, "UTF-8");
			List<Firm> 	matchingFirms 	= firmService.getFirmByName(drilldownFirm);
			if ((matchingFirms != null) && (!matchingFirms.isEmpty()))
			{
				attorneyMoveChangesSearchDTO.setSelectedFirms(matchingFirms);
			}
		}
		else
		{
			firmName = originalRequest.getParameter("firmStatsDrilldown");
			if ((firmName != null) && (firmName.trim().length() > 0))
			{
				String 		drilldownFirm 	= URLDecoder.decode(firmName, "UTF-8");
				List<Firm> 	matchingFirms 	= firmService.getFirmByName(drilldownFirm);
				if ((matchingFirms != null) && (!matchingFirms.isEmpty()))
				{
					String					searchCriteria 	= originalRequest.getParameter("drilldownSearch");
					String					searchJSON		= URLDecoder.decode(searchCriteria, "UTF-8");
					PeopleSearchModelBean 	searchBean		= WebUtil.getObject(searchJSON, PeopleSearchModelBean.class);

					// Create a new search DTO
					attorneyMoveChangesSearchDTO = new AttorneyMoveChangesSearchDTO();
					
					// Create and initialise a new searchBean
					attorneyMoveSearchModelBean = new AttorneyMoveSearchModelBean();
					attorneyMoveSearchModelBean.init();
					
					// Set the selected firms
					attorneyMoveChangesSearchDTO.setSelectedFirms(matchingFirms);
					attorneyMoveSearchModelBean.setSelectedFirms(drilldownFirm);
					
					// Set the date criteria
					setDateRange(model, attorneyMoveChangesSearchDTO, searchBean.getDateText());
					attorneyMoveSearchModelBean.setDateText(searchBean.getDateText());
					
					// Set the location(s)
					if ((searchBean.getLocations() != null) && (!searchBean.getLocations().contains(ALMConstants.ALL_LOCATIONS)))
					{
						attorneyMoveChangesSearchDTO.setLocations(searchBean.getLocations());
						attorneyMoveSearchModelBean.setSelectedLocation(searchBean.getSelectedLocations());
					}
					
					// Set the practice(s)
					if ((searchBean.getPracticeArea() != null) && (!searchBean.getPracticeArea().contains(ALMConstants.ALL_PRACTICE_AREAS)))
					{
						attorneyMoveChangesSearchDTO.setPracticeArea(searchBean.getPracticeArea());
						attorneyMoveSearchModelBean.setSelectedPracticeArea(searchBean.getSelectedPracticeArea());
					}
					
					// Set the title
					String selectedTitle = originalRequest.getParameter("title");
					if ((selectedTitle != null) && (selectedTitle.trim().length() > 0))
					{
						List<String> selectedTitles = new ArrayList<String>(1);
						selectedTitles.add(selectedTitle);
						attorneyMoveChangesSearchDTO.setTitles(selectedTitles);
						attorneyMoveSearchModelBean.setSelectedTitles(selectedTitle);
					}
					
					// Set the change type
					String selectedMovement = originalRequest.getParameter("mode");
					if ((selectedMovement != null) && (selectedMovement.trim().length() > 0))
					{
						List<String> selectedChanges = new ArrayList<String>(1);
						selectedChanges.add(selectedMovement);
						attorneyMoveChangesSearchDTO.setChangeType(selectedChanges);
						attorneyMoveSearchModelBean.setSelectedChangeTypes(selectedMovement);
					}
					model.addAttribute("attorneyMoveSearchModelBean", attorneyMoveSearchModelBean);
					model.addAttribute("attorneyMoveChangesSearchDTO", attorneyMoveChangesSearchDTO);
				}				
			}
		}

		List<AttorneyMoveChangesResultDTO> attorneyMoveSearchResults = new ArrayList<AttorneyMoveChangesResultDTO>();
		
		int resultSize = attorneyService.getAttorneyMoveChangesCount(attorneyMoveChangesSearchDTO, currentUser);
		
		if (resultSize > 0)
		{
			attorneyMoveSearchResults = attorneyService.getAttorneyMovesChanges(attorneyMoveChangesSearchDTO, currentUser);
			
			if(resultSize >= 5000){
				
				displayMessage = true;
				model.addAttribute("displayMessage", displayMessage);
			}
		}
		
		int lastPage = 0;
		if (resultSize > 0)
		{
			if (resultSize > attorneyMoveChangesSearchDTO.getResultPerPage())
			{
				lastPage = (int) Math.ceil(new Double(resultSize) / attorneyMoveChangesSearchDTO.getResultPerPage());
			}
			else
			{
				lastPage = 1;
			}
		}
		// Put the total number of pages into the model
		attorneyMoveSearchModelBean.setLastPage(lastPage);
		
		// Put the total number of results into the model
		attorneyMoveSearchModelBean.setTotalResultCount(resultSize);

		// Put the current page size (this is not the page number but the number of records on this page)
		// into the model
		if (attorneyMoveSearchResults != null && !attorneyMoveSearchResults.isEmpty())
		{
			attorneyMoveSearchModelBean.setCurrentPageSize(attorneyMoveSearchResults.size());
		}
		else
		{
			attorneyMoveSearchModelBean.setCurrentPageSize(0);
		}
		// Put the results into the model
		model.addAttribute("attorneyMoveSearchResults", attorneyMoveSearchResults);
				
		//Put the firmSearchDTO in model, later this is used in printPublication 
		model.addAttribute("attorneyMoveChangesResult", attorneyMoveSearchResults);
	//	PortletSession session = request.getPortletSession();
	//	session.setAttribute("attorneyMoveSearchResults", attorneyMoveSearchResults, PortletSession.APPLICATION_SCOPE);
		
		//return the view
		return "attorneymovesresults";
	}
	
	@RenderMapping(params = "displayPrintPage=true")
	protected String printPublications(@ModelAttribute("attorneyMoveSearchModelBean") AttorneyMoveSearchModelBean attorneyMoveSearchModelBean,
									   @ModelAttribute("attorneyMoveChangesSearchDTO") AttorneyMoveChangesSearchDTO attorneyMoveChangesSearchDTO,
									   ModelMap map , RenderRequest request, RenderResponse response) throws Exception
	{
		
		List<AttorneyMoveChangesResultDTO> printMoveSearchResults =  getSearchResultsForPrintAndExport(attorneyMoveChangesSearchDTO, request);
		attorneyMoveSearchModelBean.setTotalResultCount(printMoveSearchResults!=null?printMoveSearchResults.size():0);
		map.addAttribute("printSearchResults" , printMoveSearchResults);
		WebUtil.populatePrintTimings(map);
		return "printMovesChanges";
	}


	@ResourceMapping("exportFile")
	public void exportFile(@RequestParam String fileType, 
			 			   @ModelAttribute("attorneyMoveChangesSearchDTO") AttorneyMoveChangesSearchDTO attorneyMoveChangesSearchDTO,ResourceRequest request, ResourceResponse response)
	{
		/* Please assign list of AttorneyMoveChangesSearchDTO to attorneyMoveChangesSearchDTOList */

		List<AttorneyMoveChangesResultDTO> movesChangesResultsDTOList = getSearchResultsForPrintAndExport(attorneyMoveChangesSearchDTO, request);

		String fileName = getFileName();

		/* Code for export publications to csv file starts */

		if (fileType != null && fileType.equalsIgnoreCase(ALMConstants.CSV_EXT))
		{

			StringBuilder userInfo = new StringBuilder();

			/* Following code set header in mvc file */

			userInfo = getUserInfo(movesChangesResultsDTOList);

			String csv = userInfo.toString();

			fileName = fileName + StringPool.PERIOD + ALMConstants.CSV_EXT;
			byte[] bytes = csv.getBytes();

			HttpServletRequest servletRequest = PortalUtil.getHttpServletRequest(request);
			HttpServletResponse servletResponse = PortalUtil.getHttpServletResponse(response);

			try
			{
				ServletResponseUtil.sendFile(servletRequest, servletResponse, fileName, bytes, ContentTypes.TEXT_CSV_UTF8);
			}
			catch (IOException e)
			{
				_log.error("GOT ERROR WHILE SENDING SERVLET RESPONSE " + e.getMessage());
			}

			/* Code for export publications to csv file ends */

		}
		else
		{

			/* Code for export publications to excel file starts */

			FileInputStream fileInputStreamReader = null;
			FileOutputStream fileOutPutStreamReader = null;
			File xlsFile = null;

			try
			{
				xlsFile = FileUtil.createTempFile("xls");
				try
				{
					xlsFile.createNewFile();
				}
				catch (IOException e)
				{
					_log.error("ERROR WHILE CREATING NEW FILE " + e.getMessage());
				}
				if (xlsFile.exists())
				{
					fileInputStreamReader = new FileInputStream(xlsFile);
				}

			}
			catch (FileNotFoundException e1)
			{
				_log.info("Error while creating and reading file " + e1.getMessage());
			}

			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet(ALMConstants.WORKBOOK_NAME);

			sheet = getSheet(sheet, movesChangesResultsDTOList, workbook);

			HttpServletRequest servletRequest = PortalUtil.getHttpServletRequest(request);
			HttpServletResponse servletResponse = PortalUtil.getHttpServletResponse(response);

			// byte[] bytes = workbook.getBytes();

			fileName = fileName + StringPool.PERIOD + ALMConstants.XLS_EXT;

			try
			{
				fileInputStreamReader.close();

				fileOutPutStreamReader = new FileOutputStream(xlsFile);

				workbook.write(fileOutPutStreamReader);

				fileOutPutStreamReader.close();

				byte[] bytes = read(xlsFile);

				/*
				 * response.setContentType(ALMConstants.CONTENT_TYPE_XLS);
				 * 
				 * response.getPortletOutputStream().write(bytes);
				 */

				servletResponse.setContentType(ALMConstants.CONTENT_TYPE_XLS);

				ServletResponseUtil.sendFile(servletRequest, servletResponse, fileName, bytes, ALMConstants.CONTENT_TYPE_XLS);
			}
			catch (FileNotFoundException e)
			{
				_log.error("FILE NOT FOUND EXCEPTION " + e.getMessage());
			}
			catch (IOException e)
			{
				_log.error("IO EXCEPTION XLS " + e.getMessage());
			}
			finally
			{
				try
				{
					fileInputStreamReader.close();
					fileOutPutStreamReader.close();
				}
				catch (IOException e)
				{
					_log.error("ERROR WHILE CLOSING INPUT OUTPUT STREAM READER " + e.getMessage());
				}
			}
			/* Code for export publications to excel file ends */
		}
	}
	
	private List<AttorneyMoveChangesResultDTO> getSearchResultsForPrintAndExport(AttorneyMoveChangesSearchDTO attorneyMoveChangesSearchDTO, PortletRequest request)
	{
		AttorneyMoveChangesSearchDTO amcDTO = attorneyMoveChangesSearchDTO;
		try
		{
			amcDTO = (AttorneyMoveChangesSearchDTO) attorneyMoveChangesSearchDTO.clone();
		}
		catch (CloneNotSupportedException e)
		{
			_log.error("Cannot clone PeopleSearchDTO. Hence, using the original => " + e.getMessage());
		}

		User currentUser = WebUtil.getCurrentUser(request, userService);

		amcDTO.setPageNumber(0);
		amcDTO.setResultPerPage(WebUtil.getResultSizeLimitForCharts(currentUser));

		List<AttorneyMoveChangesResultDTO> attorneySearchResults = attorneyService.getAttorneyMovesChanges(amcDTO, currentUser);

		return attorneySearchResults;
	}

	private String getFileName()
	{
		String fileName = ALMConstants.ATTORNEY_MOVES_FILE_NAME;

		Format dateFormatDate = FastDateFormatFactoryUtil.getSimpleDateFormat("MM.dd.yy", Locale.US);

		fileName = fileName + dateFormatDate.format(CalendarFactoryUtil.getCalendar(Locale.US).getTime());

		return fileName;
	}
	
	private StringBuilder getUserInfo(List<AttorneyMoveChangesResultDTO> movesChangesResultsDTOList)
	{
		SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
		StringBuilder userInfo = new StringBuilder();

		/* Following code set header in mvc file */
		userInfo.append(ALMConstants.TITLE_NO + StringPool.COMMA);
		userInfo.append(ALMConstants.TITLE_DATE + StringPool.COMMA);
		userInfo.append(ALMConstants.ACTION + StringPool.COMMA);
		userInfo.append(ALMConstants.NAME + StringPool.COMMA);
		userInfo.append(ALMConstants.FIRM + StringPool.COMMA);
		userInfo.append(ALMConstants.TITLE + StringPool.COMMA);
		userInfo.append(ALMConstants.ATTORNEY_LOCATION + StringPool.COMMA);
		userInfo.append(ALMConstants.PRACTICES + StringPool.COMMA);
		userInfo.append(ALMConstants.FROM + StringPool.COMMA);
		userInfo.append(ALMConstants.TO + StringPool.NEW_LINE);

		/* Following code populated list values from second row */
		
		int keyCVS = 0;
	
		for (AttorneyMoveChangesResultDTO movesChangesResultDTO : movesChangesResultsDTOList)
		{
			keyCVS++;
			
			String name = movesChangesResultDTO.getAttorneyName();
			String location = movesChangesResultDTO.getLocation();
			String practices = movesChangesResultDTO.getPractices();
			
			if (name != null)
			{
				name = name.replaceAll(StringPool.COMMA, StringPool.SEMICOLON);
			}
			if (location != null)
			{
				location = location.replaceAll(StringPool.COMMA, StringPool.SEMICOLON);
			}
			if (practices != null)
			{
				practices = practices.replaceAll(StringPool.COMMA, StringPool.SEMICOLON);
			}
			
			userInfo.append(String.valueOf(keyCVS) + StringPool.COMMA);
			userInfo.append(format.format(movesChangesResultDTO.getLastActionDate()) + StringPool.COMMA);
			userInfo.append(movesChangesResultDTO.getLastAction() + StringPool.COMMA);
			userInfo.append(name + StringPool.COMMA);
			userInfo.append(movesChangesResultDTO.getFirmName() + StringPool.COMMA);
			userInfo.append(movesChangesResultDTO.getTitle() + StringPool.COMMA);
			userInfo.append(location + StringPool.COMMA);
			userInfo.append(practices + StringPool.COMMA);
			userInfo.append(movesChangesResultDTO.getMovedFrom()  + StringPool.COMMA);
			userInfo.append(movesChangesResultDTO.getMovedTo() + StringPool.NEW_LINE);
		}

		return userInfo;
	}
	
	private HSSFSheet getSheet(HSSFSheet sheet, List<AttorneyMoveChangesResultDTO> movesChangesResultsDTOList, HSSFWorkbook workbook)
	{
		int key = 0;

		Map<String, Object[]> data = new HashMap<String, Object[]>();

		/* Following code set header in excel file */

		data.put(String.valueOf(key), new Object[] 
		{
			ALMConstants.TITLE_NO,
			ALMConstants.TITLE_DATE,
			ALMConstants.ACTION,
			ALMConstants.NAME,
			ALMConstants.FIRM, 
			ALMConstants.TITLE,
			ALMConstants.ATTORNEY_LOCATION ,
			ALMConstants.PRACTICES,
			ALMConstants.FROM ,
			ALMConstants.TO
		});

		if (movesChangesResultsDTOList != null && movesChangesResultsDTOList.size() > 0)
		{
			for (AttorneyMoveChangesResultDTO eventResultDTO : movesChangesResultsDTOList)
			{
				key++;
				
				SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
				
				data.put(
						String.valueOf(key),
						new Object[] { String.valueOf(key),
							format.format(eventResultDTO.getLastActionDate()),
							String.valueOf(eventResultDTO.getLastAction()),
							String.valueOf(eventResultDTO.getAttorneyName()),
							String.valueOf(eventResultDTO.getFirmName()),
							String.valueOf(eventResultDTO.getTitle()),
							String.valueOf(eventResultDTO.getLocation()),
							String.valueOf(eventResultDTO.getPractices()),
							String.valueOf(StringUtils.defaultIfBlank(eventResultDTO.getMovedFrom(), StringPool.BLANK)),
							String.valueOf(StringUtils.defaultIfBlank(eventResultDTO.getMovedTo(), StringPool.BLANK))
							});
			}
		}

		HSSFCellStyle hyperlinkCellStyle = workbook.createCellStyle();
		HSSFFont hyperlinkFont = workbook.createFont();
		hyperlinkFont.setUnderline(HSSFFont.U_SINGLE);
		hyperlinkFont.setColor(HSSFColor.BLUE.index);
		hyperlinkCellStyle.setFont(hyperlinkFont);
		
		HSSFCellStyle boldCellStyle = workbook.createCellStyle();
		HSSFFont boldCellFont = workbook.createFont();
		boldCellFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		boldCellStyle.setFont(boldCellFont);
		
		int rownum = 1;
		for (key = 0; key < data.size(); key++)
		{
			rownum = key;
			//HSSFRow headRow = sheet.createFreezePane(colSplit, rowSplit)
			HSSFRow row = sheet.createRow((rownum));
			
			Object[] objArr = data.get(String.valueOf(key));
			int cellnum = 0;
			for (Object obj : objArr)
			{
				HSSFCell cell = row.createCell(cellnum);
				if (obj instanceof Date)
				{
					cell.setCellValue((Date) obj);
				}
				else if (obj instanceof Boolean)
				{
					cell.setCellValue((Boolean) obj);
				}
				else if (obj instanceof String)
				{
					String str = (String) obj;
					if (str.startsWith(ALMConstants.HTTP))
					{
						HSSFHyperlink hyperLink = new HSSFHyperlink(HSSFHyperlink.LINK_URL);
						hyperLink.setAddress(str);
						cell.setHyperlink(hyperLink);
						HSSFRichTextString textString = new HSSFRichTextString(str);
						cell.setCellStyle(hyperlinkCellStyle);
						cell.setCellValue(textString);
					}
					else
					{
						HSSFRichTextString textString = new HSSFRichTextString(str);
						if (key == 0)
						{
							cell.setCellStyle(boldCellStyle);
						}
						cell.setCellValue(textString);
					}

				}
				else if (obj instanceof Double)
				{
					cell.setCellValue((Double) obj);
				}
				cellnum++;
			}
		}

		return sheet;
	}
	
	private byte[] read(File file)
	{

		byte[] buffer = new byte[(int) file.length()];
		InputStream ios = null;
		try
		{
			try
			{
				ios = new FileInputStream(file);
			}
			catch (FileNotFoundException e)
			{

				_log.error("FILE NOT FOUND EXCEPTION " + e.getMessage());
			}
			try
			{
				if (ios.read(buffer) == -1)
				{
				}
			}
			catch (IOException e)
			{

				_log.error("ERROR WHILE READING FILE " + e.getMessage());
			}
		}
		finally
		{
			try
			{
				if (ios != null)
					ios.close();
			}
			catch (IOException e)
			{
			}
		}

		return buffer;
	}


}
