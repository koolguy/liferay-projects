package com.alm.rivaledge.controller;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.alm.rivaledge.model.AttorneyMoveSearchModelBean;
import com.alm.rivaledge.model.chart.StandardChartModelBean;
import com.alm.rivaledge.persistence.dao.CacheDAO;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserPreferences;
import com.alm.rivaledge.service.AttorneyService;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.service.UserService;
import com.alm.rivaledge.service.WatchlistService;
import com.alm.rivaledge.transferobject.AttorneyMoveChangesResultDTO;
import com.alm.rivaledge.transferobject.AttorneyMoveChangesSearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.WebUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;

public class AbstractAttorneyMovesChangesController 
{
	protected Log _log = LogFactoryUtil.getLog(AbstractAttorneyMovesChangesController.class.getName());
	
	@Autowired
	protected FirmService	firmService;

	@Autowired
	protected WatchlistService watchlistService;

	@Autowired
	protected AttorneyService	attorneyService;
	
	@Autowired
	protected CacheDAO	cacheDAO;
	
	@Autowired
	protected UserService userService;
	
	/**
	 * Populates the Model which holds the Search criteria in search form, initializing to default values
	 * 
	 * @param model
	 */

	@ModelAttribute
	public void populateAttorneyMoveSearchModelBean(ModelMap model, PortletRequest request)
	{
		
		String				currentPortletId		 = WebUtil.getCurrentPortletId(request);
		String				currentPage				 = WebUtil.getCurrentPage(request);
		boolean				isHomePage				 = ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		boolean 			isC2VPage				 = 	WebUtil.isClickToViewPage(request);
		model.addAttribute("isC2VPage", isC2VPage);
		
		User 				currentUser 			 = (model.get("currentUser") != null) ? ((User) model.get("currentUser")) : WebUtil.getCurrentUser(request, userService); 
		boolean				isAdminUser				 = WebUtil.isAdminUser(currentUser);								

		AttorneyMoveSearchModelBean attorneyMoveSearchModelBean	=  (AttorneyMoveSearchModelBean) model.get("attorneyMoveSearchModelBean");;
																		
		if (currentUser != null)
		{
			model.put("currentUser", currentUser);
			model.put("isAdminUser", isAdminUser);
		}

		if (isHomePage)
		{
			attorneyMoveSearchModelBean = getUserPreference(currentUser.getId(), currentPortletId, currentPage);
		}
		
		if (!isHomePage && model.get("attorneyMoveSearchModelBean") == null)
		{
			currentPortletId 	= "ATTORNEY_MOVES";
			currentPage			= "ATTORNEY_MOVES";
			attorneyMoveSearchModelBean = getUserPreference(currentUser.getId(), currentPortletId, currentPage);
		}
		
		if (attorneyMoveSearchModelBean == null)
		{
			attorneyMoveSearchModelBean = getDefaultAttorneyMoveSearchModelBean(currentUser);
			
		}
		
		attorneyMoveSearchModelBean.setPortletId(currentPortletId);
	
		if(isHomePage || isC2VPage)
		{
			attorneyMoveSearchModelBean.setAddToSession(false);
		}
	
		model.addAttribute("attorneyMoveSearchModelBean", attorneyMoveSearchModelBean);

		AttorneyMoveChangesSearchDTO attorneyMoveChangesSearchDTO = getDTOFromModelBean(model, attorneyMoveSearchModelBean,request);
		model.addAttribute("attorneyMoveChangesSearchDTO", attorneyMoveChangesSearchDTO);
		
		if (model.get("attorneyMoveChangesResultDTO") == null)
		{
			model.addAttribute("attorneyMoveChangesResult", new ArrayList<AttorneyMoveChangesResultDTO>());
		}
	}

	protected AttorneyMoveSearchModelBean getDefaultAttorneyMoveSearchModelBean(User currentUser) {
		
		List<UserGroup> 			allWatchListsDefaultList 	= null;
		AttorneyMoveSearchModelBean attorneyMoveSearchModelBean = new AttorneyMoveSearchModelBean();
		attorneyMoveSearchModelBean.init(); //do an explicit init for default values	
		
		if (currentUser != null)
		{
			allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
		}
		
		List<String> firmList = new ArrayList<String>();
		List<Integer> firmListWatchList = new ArrayList<Integer>();
		
		if (allWatchListsDefaultList == null || allWatchListsDefaultList.isEmpty())
		{
			//FirmType
			attorneyMoveSearchModelBean.setFirmType(ALMConstants.RIVALEDGE_LIST);
			firmList.add(ALMConstants.AMLAW_100);
		} 
		else
		{
			attorneyMoveSearchModelBean.setFirmType(ALMConstants.WATCH_LIST);
			//firmListWatchList.add("" + allWatchListsDefaultList.get(0).getGroupId());
			firmListWatchList.add(allWatchListsDefaultList.get(0).getGroupId());
		}
		attorneyMoveSearchModelBean.setFirmList(firmList);
		attorneyMoveSearchModelBean.setFirmListWatchList(firmListWatchList);
		return attorneyMoveSearchModelBean;
	}

	protected AttorneyMoveSearchModelBean getUserPreference(Integer userId, String portletId, String currentPage)
	{
		UserPreferences uP = userService.getUserPreferenceByUserIdAndPortletId(userId, portletId, currentPage);
		if ((uP != null) && (uP.getSearchCriteriaJSON() != null) && (uP.getSearchCriteriaJSON().trim().length() > 0))
		{
			return (WebUtil.getObject(uP.getSearchCriteriaJSON(), AttorneyMoveSearchModelBean.class));
		}
		return (null);
	}
	
	protected StandardChartModelBean getUserChartPreference(Integer userId, String portletId, String currentPage)
	{
		UserPreferences uP = userService.getUserPreferenceByUserIdAndPortletId(userId, portletId, currentPage);
		if ((uP != null) && (uP.getChartCriteriaJSON() != null) && (uP.getChartCriteriaJSON().trim().length() > 0))
		{
			return (WebUtil.getObject(uP.getChartCriteriaJSON(), StandardChartModelBean.class));
		}
		return (null);
	}

	/**
	 * Creates a FirmSearchDTO from  FirmSearchModelBean
	 * @param model
	 * @param attorneyMoveSearchModelBean
	 * @return
	 */
	protected AttorneyMoveChangesSearchDTO getDTOFromModelBean(ModelMap model, AttorneyMoveSearchModelBean attorneyMoveSearchModelBean,PortletRequest request)
	{

		AttorneyMoveChangesSearchDTO attorneyMoveChangesSearchDTO = new AttorneyMoveChangesSearchDTO();

		// Get the page number from the model bean
		int pageNumber = attorneyMoveSearchModelBean.getGoToPage();
		int orderBy = attorneyMoveSearchModelBean.getOrderBy();
		int searchResultsPerPage = attorneyMoveSearchModelBean.getSearchResultsPerPage();

		attorneyMoveChangesSearchDTO.setOrderBy(orderBy);
		attorneyMoveChangesSearchDTO.setResultPerPage(searchResultsPerPage);

		// Plug the page number into the search DTO
		//This sets the starting limit for the paginated search results in the sql query
		attorneyMoveChangesSearchDTO.setPageNumber(pageNumber < 0 ? 0 : (pageNumber - 1) * searchResultsPerPage);

		// Set the sort column and order into the search DTO
		attorneyMoveChangesSearchDTO.setSortColumn(attorneyMoveSearchModelBean.getSortColumn());
		attorneyMoveChangesSearchDTO.setSortOrder(attorneyMoveSearchModelBean.getSortOrder());

		// Get the selected firms, if any, and set them into the search DTO
		setSelectedFirms(model, attorneyMoveChangesSearchDTO, attorneyMoveSearchModelBean,request);

		// Set the date range criteria
		// There will always be a date range criteria, as the form view
		// does not allow this to be null
		setDateRange(model, attorneyMoveChangesSearchDTO, attorneyMoveSearchModelBean);

		// Set the remaining search criteria, such as practice areas, etc.
		// See the setOtherSearchParameter method for more details
		setOtherSearchParameter(model, attorneyMoveChangesSearchDTO, attorneyMoveSearchModelBean);
		return attorneyMoveChangesSearchDTO;

	}
	
	/***
	 * Transforms , and ; separated values at 0-index to List values <br>
	 * We need values in List rather than CSV, so that they can bind to <code>Spring Form<code> 
	 * later on from session
	 * 
	 * @param attorneyMoveSearchModelBean
	 */
	protected void transformBean(AttorneyMoveSearchModelBean attorneyMoveSearchModelBean)
	{

		List<String> firmList = attorneyMoveSearchModelBean.getFirmList();
		if (firmList != null && !firmList.isEmpty())
		{
			String firm = firmList.get(0);
			attorneyMoveSearchModelBean.setFirmList(ListUtil.fromArray(StringUtil.split(firm, ","))); //firmList is , separated
		}
		

		List<String> changeTypeList = attorneyMoveSearchModelBean.getChangeType();
		if (changeTypeList != null && !changeTypeList.isEmpty())
		{
			String changeType = changeTypeList.get(0);
			attorneyMoveSearchModelBean.setChangeType(ListUtil.fromArray(StringUtil.split(changeType, ";")));
		}
		
		List<String> practiceAreaList = attorneyMoveSearchModelBean.getPracticeArea();
		if (practiceAreaList != null && !practiceAreaList.isEmpty())
		{
			String practiceArea = practiceAreaList.get(0);
			attorneyMoveSearchModelBean.setPracticeArea(ListUtil.fromArray(StringUtil.split(practiceArea, ";")));
		}
		
		List<String> locationsList = attorneyMoveSearchModelBean.getLocations();
		if (locationsList != null && !locationsList.isEmpty())
		{
			String location = locationsList.get(0);
			attorneyMoveSearchModelBean.setLocations(ListUtil.fromArray(StringUtil.split(location, ";")));
		}

		List<String> titlesList = attorneyMoveSearchModelBean.getTitles();
		if (titlesList != null && !titlesList.isEmpty())
		{
			String title = titlesList.get(0);
			attorneyMoveSearchModelBean.setTitles(ListUtil.fromArray(StringUtil.split(title, ";")));
		}
	}
	
	/**
	 * Set date range in AttorneyMoveChangesSearchDTO reference based on user input on Attorney moves and changes
	 * search Screen
	 * 
	 * @param request
	 * @param response
	 * @param searchFormTo
	 */
	protected void setDateRange(ModelMap model, AttorneyMoveChangesSearchDTO attorneyMovesChangesDTO, AttorneyMoveSearchModelBean attorneyMoveSearchModelBean)
	{
		String dateText = attorneyMoveSearchModelBean.getDateText();
		setDateRange(model, attorneyMovesChangesDTO, dateText);
	}
	
	protected void setDateRange(ModelMap model, AttorneyMoveChangesSearchDTO attorneyMovesChangesDTO, String dateText)
	{
		Calendar calendar = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date date = new Date();
		attorneyMovesChangesDTO.setToDate(date);
		if (dateText != null && !dateText.contains("-"))
		{

			if (dateText.equals(ALMConstants.ANY))
			{
				attorneyMovesChangesDTO.setFromDate(null);
				attorneyMovesChangesDTO.setToDate(null);
			}
			else if (dateText.equals(ALMConstants.LAST_WEEK))
			{
				calendar.add(Calendar.WEEK_OF_MONTH, -1);
				attorneyMovesChangesDTO.setFromDate(calendar.getTime());
			}
			else if (dateText.equals(ALMConstants.LAST_30_DAYS))
			{
				calendar.add(Calendar.MONTH, -1);
				attorneyMovesChangesDTO.setFromDate(calendar.getTime());
			}
			else if (dateText.equals(ALMConstants.LAST_90_DAYS))
			{
				calendar.add(Calendar.MONTH, -3);
				attorneyMovesChangesDTO.setFromDate(calendar.getTime());
			}
			else if (dateText.equals(ALMConstants.LAST_6_MONTHS))
			{
				calendar.add(Calendar.MONTH, -6);
				attorneyMovesChangesDTO.setFromDate(calendar.getTime());
			}
			else
			{
				calendar.add(Calendar.YEAR, -1);
				attorneyMovesChangesDTO.setFromDate(calendar.getTime());
			}
		}
		else
		{
			String[] dateRange = dateText.split("-");
			try
			{
				attorneyMovesChangesDTO.setFromDate(dateFormat.parse(dateRange[0]));
				attorneyMovesChangesDTO.setToDate(dateFormat.parse(dateRange[1]));
			}
			catch (ParseException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Set Practice Area, Locations, Titles, Change types, 'Keywords in Bio' and name  in AttorneyMoveChangesSearchDTO reference based on user input 
	 * on Attorney moves and changes search Screen
	 * @param request
	 * @param response
	 * @param attorneySearchDTO
	 */
	protected void setOtherSearchParameter(ModelMap model,  AttorneyMoveChangesSearchDTO attorneyMoveChangesSearchDTO,AttorneyMoveSearchModelBean attorneyMoveSearchModelBean)
	{
		
		if (attorneyMoveSearchModelBean.getLocations() != null
				&& !attorneyMoveSearchModelBean.getLocations().contains(ALMConstants.ALL_LOCATIONS))
		{
			attorneyMoveChangesSearchDTO.setLocations(attorneyMoveSearchModelBean.getLocations());
			attorneyMoveSearchModelBean.setSelectedLocation(attorneyMoveSearchModelBean.getLocations().toString().replace("[", "").replace("]", ""));
		}
		if (attorneyMoveSearchModelBean.getPracticeArea() != null
				&& !attorneyMoveSearchModelBean.getPracticeArea().contains(ALMConstants.ALL_PRACTICE_AREAS))
		{
			attorneyMoveChangesSearchDTO.setPracticeArea(attorneyMoveSearchModelBean.getPracticeArea());
			attorneyMoveSearchModelBean.setSelectedPracticeArea(attorneyMoveSearchModelBean.getPracticeArea().toString().replace("[", "").replace("]", ""));
		}
		
		if (attorneyMoveSearchModelBean.getChangeType() != null && !attorneyMoveSearchModelBean.getChangeType().contains(ALMConstants.ALL_MOVES_CHANGES))
		{
			attorneyMoveChangesSearchDTO.setChangeType(attorneyMoveSearchModelBean.getChangeType());
			attorneyMoveSearchModelBean.setSelectedChangeTypes(attorneyMoveSearchModelBean.getChangeType().toString().replace("[", "").replace("]", ""));	
		}
		
		if (attorneyMoveSearchModelBean.getTitles() != null && !attorneyMoveSearchModelBean.getTitles().contains(ALMConstants.ALL_TITLES))
		{
			attorneyMoveChangesSearchDTO.setTitles(attorneyMoveSearchModelBean.getTitles());
			attorneyMoveSearchModelBean.setSelectedTitles(attorneyMoveSearchModelBean.getTitles().toString().replace("[", "").replace("]", ""));	
		} 	
		
		
		if (attorneyMoveSearchModelBean.getKeywords() != null && !StringUtils.isEmpty(attorneyMoveSearchModelBean.getKeywords()))
		{
			attorneyMoveChangesSearchDTO.setKeywordsInBioText(attorneyMoveSearchModelBean.getKeywords());
			
		}
		if(attorneyMoveSearchModelBean.getName()!=null && !StringUtils.isEmpty(attorneyMoveSearchModelBean.getName()))
		{			
			attorneyMoveChangesSearchDTO.setAttorneyName(ListUtil.fromArray(StringUtil.split(attorneyMoveSearchModelBean.getName(), " ")));
		}
		
		attorneyMoveSearchModelBean.setSelectedChangeTypes(attorneyMoveSearchModelBean.getChangeType().toString().replace("[", "").replace("]", ""));
		attorneyMoveSearchModelBean.setSelectedLocation(attorneyMoveSearchModelBean.getLocations().toString().replace("[", "").replace("]", ""));
		attorneyMoveSearchModelBean.setSelectedPracticeArea(attorneyMoveSearchModelBean.getPracticeArea().toString().replace("[", "").replace("]", ""));
		attorneyMoveSearchModelBean.setSelectedTitles(attorneyMoveSearchModelBean.getTitles().toString().replace("[", "").replace("]", ""));
		
		
	}
	
	/**
	 * Set Firms information in AttorneyMoveChangesSearchDTO reference based on user input on Attorney Moves and Changes search Screen
	 * @param selectedFirms
	 * @param attorneySearchDTO
	 */
	protected void setSelectedFirms(ModelMap model,AttorneyMoveChangesSearchDTO attorneyMoveChangesSearchDTO, AttorneyMoveSearchModelBean attorneyMoveSearchModelBean,PortletRequest request)
	{
		String 			firmType 			= attorneyMoveSearchModelBean.getFirmType();
		List<Firm> 		firmsList 			= new ArrayList<Firm>();
		List<Firm> 		firmListWatchList 	= new ArrayList<Firm>();
		List<String>	firmsListForResults = new ArrayList<String>();
		User 			currentUser 		= (model.get("currentUser") != null) ? ((User) model.get("currentUser")) : WebUtil.getCurrentUser(request, userService); 
		
		if (firmType != null && !"".equals(firmType))
		{
			if (firmType.equals(ALMConstants.ALL_FIRMS))
			{
				//set the firms filter to null as user has selected 'All Firms'
				attorneyMoveChangesSearchDTO.setSelectedFirms(null);
				firmsListForResults.add(firmType);
				attorneyMoveSearchModelBean.setFirmListWatchList(null);
				attorneyMoveSearchModelBean.setFirmList(null);
			}
			
			
			else if (ALMConstants.WATCH_LIST.equals(firmType))
			{
				List<Firm> firmsWatchList = null;
				if (currentUser != null)
				{
					firmsWatchList = watchlistService.findAllFirmsInManyWatchlists(attorneyMoveSearchModelBean.getFirmListWatchList(), currentUser.getId());
				}
				
				firmListWatchList.addAll(firmsWatchList);
				firmsList.addAll(firmsWatchList);

				for (Firm firmValue : firmsWatchList)
				{
					if (firmValue != null)
					{
						firmsListForResults.add(firmValue.getCompany());
					}
				}
				
				if (firmsWatchList.size() == 0)
				{
					List<Firm> 	notAvailableFirms 	= new ArrayList<Firm>();
					Firm 		tempFirm 			= new Firm();
					tempFirm.setCompanyId(0);
					notAvailableFirms.add(tempFirm);
					attorneyMoveChangesSearchDTO.setSelectedFirms(notAvailableFirms);
				}
				else
				{
					attorneyMoveChangesSearchDTO.setSelectedFirms(firmsWatchList);
				}
				attorneyMoveSearchModelBean.setFirmList(null);
				
			}
			else if (ALMConstants.RIVALEDGE_LIST.equals(firmType))
			{
				firmsList.addAll(firmService.getRanking(attorneyMoveSearchModelBean.getFirmList().get(0)));
				attorneyMoveChangesSearchDTO.setSelectedFirms(firmsList);
				firmsListForResults.add(attorneyMoveSearchModelBean.getFirmList().get(0));
				attorneyMoveSearchModelBean.setFirmListWatchList(null);
			}
			else
			{
				for (String firmId : attorneyMoveSearchModelBean.getFirmList())
				{
					Firm firmObject = firmService.getFirmById(Integer.parseInt(firmId));
					firmsList.add(firmObject);
					firmsListForResults.add(firmObject.getCompany());
				}
				attorneyMoveChangesSearchDTO.setSelectedFirms(firmsList);
				attorneyMoveSearchModelBean.setFirmListWatchList(null);
			}
			
			//attorneyMoveChangesSearchDTO.setSelectedFirms(firmsList);
			attorneyMoveSearchModelBean.setSelectedFirms(firmsListForResults.toString().replace("[", "").replace("]", ""));
		}
	}
}