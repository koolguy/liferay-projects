package com.alm.rivaledge.controller.search;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.alm.rivaledge.controller.AbstractAttorneyMovesChangesController;
import com.alm.rivaledge.model.AttorneyMoveSearchModelBean;
import com.alm.rivaledge.model.PeopleSearchModelBean;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.Practice;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.service.AttorneyService;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.service.WatchlistService;
import com.alm.rivaledge.transferobject.AttorneyMoveChangesSearchDTO;
import com.alm.rivaledge.transferobject.UserGroupDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.FirmJsonUtility;
import com.alm.rivaledge.util.PracticeJsonUtility;
import com.alm.rivaledge.util.WebUtil;
import com.google.gson.Gson;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.util.PortalUtil;


@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "attorneyMoveSearchModelBean", "attorneyMoveChangesSearchDTO", "attorneyMoveChangesResult" })
public class AttorneyMovesChangesSearchController extends AbstractAttorneyMovesChangesController {
	
	@Autowired
	private FirmService	firmService;

	@Autowired
	private WatchlistService watchlistService;

	@Autowired
	private AttorneyService	attorneyService;
	
	/**
	 * Render Attorney Moves and Changes Search Form which enables the user to search about Attorney moves and changes across law firms
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	@RenderMapping
	protected String attorneyMovesAndChangesSearchForm(@ModelAttribute("attorneyMoveSearchModelBean") AttorneyMoveSearchModelBean attorneyMoveSearchModelBean, 
													   ModelMap model, 
													   RenderRequest request, 
													   RenderResponse response) throws Exception
	{
		User currentUser = (model.get("currentUser") != null) ? ((User) model.get("currentUser")) : WebUtil.getCurrentUser(request, userService); 
		
		HttpServletRequest 	originalRequest = PortalUtil.getOriginalServletRequest(PortalUtil.getHttpServletRequest(request));
		String 				firmName 		= originalRequest.getParameter("drilldownFirmName");
		if ((firmName != null) && (firmName.trim().length() > 0))
		{
			String 		drilldownFirm 	= URLDecoder.decode(firmName, "UTF-8");
			List<Firm> 	matchingFirms 	= firmService.getFirmByName(drilldownFirm);
			if ((matchingFirms != null) && (!matchingFirms.isEmpty()))
			{
				attorneyMoveSearchModelBean.setSelectedFirms(drilldownFirm);
				model.addAttribute("attorneyMoveSearchModelBean", attorneyMoveSearchModelBean);
			}
		}
		else
		{
			firmName = originalRequest.getParameter("firmStatsDrilldown");
			if ((firmName != null) && (firmName.trim().length() > 0))
			{
				String 		drilldownFirm 	= URLDecoder.decode(firmName, "UTF-8");
				List<Firm> 	matchingFirms 	= firmService.getFirmByName(drilldownFirm);
				if ((matchingFirms != null) && (!matchingFirms.isEmpty()))
				{
					String					searchCriteria 	= originalRequest.getParameter("drilldownSearch");
					String					searchJSON		= URLDecoder.decode(searchCriteria, "UTF-8");
					PeopleSearchModelBean 	searchBean		= WebUtil.getObject(searchJSON, PeopleSearchModelBean.class);

					// Create and initialise a new searchBean
					attorneyMoveSearchModelBean = new AttorneyMoveSearchModelBean();
					attorneyMoveSearchModelBean.init();
					
					// Set the selected firms
					attorneyMoveSearchModelBean.setSelectedFirms(drilldownFirm);
					
					// Set the date criteria
					attorneyMoveSearchModelBean.setDateText(searchBean.getDateText());
					
					// Set the location(s)
					if ((searchBean.getLocations() != null) && (!searchBean.getLocations().contains(ALMConstants.ALL_LOCATIONS)))
					{
						attorneyMoveSearchModelBean.setSelectedLocation(searchBean.getSelectedLocations());
					}
					
					// Set the practice(s)
					if ((searchBean.getPracticeArea() != null) && (!searchBean.getPracticeArea().contains(ALMConstants.ALL_PRACTICE_AREAS)))
					{
						attorneyMoveSearchModelBean.setSelectedPracticeArea(searchBean.getSelectedPracticeArea());
					}
					
					// Set the title
					String selectedTitle = originalRequest.getParameter("title");
					if ((selectedTitle != null) && (selectedTitle.trim().length() > 0))
					{
						attorneyMoveSearchModelBean.setSelectedTitles(selectedTitle);
					}
					
					// Set the change type
					String selectedMovement = originalRequest.getParameter("mode");
					if ((selectedMovement != null) && (selectedMovement.trim().length() > 0))
					{
						attorneyMoveSearchModelBean.setSelectedChangeTypes(selectedMovement);
					}
					model.addAttribute("attorneyMoveSearchModelBean", attorneyMoveSearchModelBean);
				}				
			}
		}

		//creating reference of Gson which we would help converting firms and practice
		//list to json string
		Gson gson = new Gson();
		FirmJsonUtility firmJsonUtility = null;
		PracticeJsonUtility practiceJsonUtility = null;
		List<FirmJsonUtility> firmJsonUtilityList = new ArrayList<FirmJsonUtility>();
		List<PracticeJsonUtility> practiceJsonUtilityList = new ArrayList<PracticeJsonUtility>();
		
		//fetching all firms information from DB to show under firms pop up
		List<Firm> firmsSet = firmService.getAllFirms();
		//fetching all Practice area information from DB to show under firms pop up
		List<Practice> practiceList = firmService.getAllPractices();
		List<UserGroupDTO> allWatchLists = null;

		if (currentUser != null)
		{
			allWatchLists = (List<UserGroupDTO>) watchlistService.getAllFirmWatchList(currentUser.getId());
		}
		//creating firm json list for firm Auto completer
		//converting List<Firm> to List<FirmJsonUtility> as FirmJsonUtility utility class have attributes(id,label,category)  
		//which is required for Firms Autocompleter/Type ahead
		for (Firm firm : firmsSet)
		{
			firmJsonUtility = new FirmJsonUtility();
			firmJsonUtility.setId(firm.getCompanyId().toString());
			firmJsonUtility.setLabel(firm.getCompany());
			firmJsonUtility.setCategory("Single Firms");
			firmJsonUtilityList.add(firmJsonUtility);
		}
		
		//creating Practice area json list for Practice area Auto completer
		//converting List<Practice> to List<PracticeJsonUtility> as PracticeJsonUtility utility class have attribute(label)
		//which is required for Firms Autocompleter/Type ahead
		for (Practice practice : practiceList)
		{
			practiceJsonUtility = new PracticeJsonUtility();
			practiceJsonUtility.setLabel(practice.getPracticeArea());
			practiceJsonUtilityList.add(practiceJsonUtility);
		}
		//converting all firms to JSON string
		String jsonAllFirms = gson.toJson(firmJsonUtilityList);
		
		//converting all Practices to JSON string
		String jsonAllPractics = gson.toJson(practiceJsonUtilityList);
		
		
		List<UserGroup> allWatchListsDefaultList = null;
		if (currentUser != null)
		{
			allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
			if (allWatchListsDefaultList.size() == 0)
			{
				model.addAttribute("allWatchListsDefaultList", "");
			}
			else
			{
				model.addAttribute("allWatchListsDefaultList", allWatchListsDefaultList.get(0));
			}
		}
		
		model.addAttribute("firmJson", jsonAllFirms);
		model.addAttribute("practiceJson", jsonAllPractics);
		model.addAttribute("changeTypeJson", gson.toJson(ALMConstants.CHANGE_TYPES)); // Change Type Json String
		model.addAttribute("titlesJson", gson.toJson(ALMConstants.ALL_TITLES_LIST));  // Title Json String
		model.addAttribute("allFirms", firmsSet);
		model.addAttribute("allPracticeArea", practiceList);
		model.addAttribute("allWatchLists", allWatchLists);
		model.addAttribute("allOtherLocations", ALMConstants.LOCATIONS);
		model.addAttribute("allRankingList", ALMConstants.RIVALEDGE_RANKING_LIST);		
		return "attorneymovesandchangessearch";
	}
	
	/**
	 * This methods gets invoked when user clicks on Apply button on Attorney moves and changes search form and set search criteria 
	 * to Attorney search DTO
	 * @param attorneyMoveSearchModelBean
	 * @param model
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@ActionMapping(params = "action=changeSearchCriteria")
	protected void attorneyMovesSubmitForm(@ModelAttribute("attorneyMoveSearchModelBean") AttorneyMoveSearchModelBean attorneyMoveSearchModelBean,
										   ModelMap model, 
										   ActionRequest request, 
										   ActionResponse response) throws Exception
	{
		AttorneyMoveChangesSearchDTO attorneyMoveChangesSearchDTO = getDTOFromModelBean(model, attorneyMoveSearchModelBean,request);
		User						 currentUser				  = WebUtil.getCurrentUser(request, userService);
		
		// The user changed the search criteria, so we will have to persist
		// the changed criteria into the database.
		// However, if this is the Home page, then we will persist these as
		// the search criteria for the specific chart instead, and not
		// generically as the module search criteria.
		String currentPortletId 	= attorneyMoveSearchModelBean.getPortletId();
		String 	currentPage			= WebUtil.getCurrentPage(request);
		boolean	isHomePage			= ((currentPage.equals(ALMConstants.HOME_PAGE)) || (currentPage.equals(StringPool.BLANK)));
		if ((currentUser != null) && (currentPortletId != null) && (currentPortletId.length() > 0))
		{
			if (!isHomePage)
			{
				currentPage = "ATTORNEY_MOVES";
				currentPortletId = "ATTORNEY_MOVES";
			}
			userService.saveUserPreferences(currentUser.getId(), 
											currentPortletId, 
											WebUtil.getJson(attorneyMoveSearchModelBean), 
											null, 
											currentPage);
		}

		//set the new SearchDTO back into the model, as user changed the search criteria
		model.addAttribute("attorneyMoveChangesSearchDTO", attorneyMoveChangesSearchDTO);
	}
}