package com.alm.rivaledge.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.portlet.MockRenderRequest;
import org.springframework.mock.web.portlet.MockRenderResponse;
import org.springframework.mock.web.portlet.MockResourceRequest;
import org.springframework.mock.web.portlet.MockResourceResponse;
import org.springframework.web.portlet.ModelAndView;

import com.alm.rivaledge.BaseALMTest;
import com.alm.rivaledge.controller.search.AttorneyMovesChangesSearchController;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.transferobject.AttorneyMoveChangesSearchDTO;
import com.alm.rivaledge.util.ALMConstants;

/**
 * Unit test cases for Attorney Moves and Changes Controller 
 * 
 * @author FL605
 * @since Sprint 4
 * 
 */
public class AttorneyMovesChangesControllerTest extends BaseALMTest
{
	@Autowired
	private AttorneyMovesChangesSearchController	testController;

	/**
	 * Test that the form view returns everything needed for the search.
	 */
	@Test
	@SuppressWarnings("unchecked")
	public void testDefaultFormView()
	{
		printTitle();
		assertNotNull(testController);
		MockRenderRequest testRequest = new MockRenderRequest();
		MockRenderResponse testResponse = new MockRenderResponse();
		ModelAndView testMAV = null;
		try
		{
//			testMAV = testController.attorneyMovesAndChangesSearchForm(testRequest, testResponse);
		}
		catch (Exception ex)
		{
			// Do nothing
		}
		assertTrue(testMAV.getViewName().equals("attorneymovesandchangessearch"));
		assertTrue(testMAV.getModel().keySet().contains("attorneymoveschangesModel"));

		Map<String, Collection<?>> testModel = (Map<String, Collection<?>>) testMAV.getModel().get("attorneymoveschangesModel");
		assertTrue(testModel.keySet().contains("allFirms"));
		assertTrue(testModel.keySet().contains("allPracticeArea"));

		Collection<Firm> testFirms = (Collection<Firm>) testModel.get("allFirms");
		assertNotNull(testFirms);
	}

	/**
	 * Test default values of Attorney Moves and changes search Form
	 */
	@Test
	public void testSetOtherSearchParameterWithDefaultValues()
	{
		printTitle();
		AttorneyMoveChangesSearchDTO attorneySearchDTO = new AttorneyMoveChangesSearchDTO();
		MockResourceRequest testRequest = new MockResourceRequest();
		testRequest.setParameter("selectedPracticeArea", "All Practice Areas");
		testRequest.setParameter("selectedLocation", ALMConstants.ALL_LOCATIONS);
		testRequest.setParameter("selectedTitles", ALMConstants.ALL);
		testRequest.setParameter("selectedChangeType", ALMConstants.ALL);
		try
		{
//			testController.setOtherSearchParameter(testRequest, attorneySearchDTO);
		}
		catch (Exception ex)
		{
			// Do nothing
		}
		assertNull(attorneySearchDTO.getPracticeArea());
		assertNull(attorneySearchDTO.getLocations());
		assertNull(attorneySearchDTO.getTitles());	
		assertNull(attorneySearchDTO.getChangeType());	

	}

	/**
	 * Test Attorney  Moves and changes search Form values apart form default
	 */
	@Test
	public void testSetOtherSearchParameterWithoutDefaultValues()
	{
		printTitle();
		AttorneyMoveChangesSearchDTO attorneySearchDTO = new AttorneyMoveChangesSearchDTO();
		MockResourceRequest testRequest = new MockResourceRequest();
		testRequest.setParameter("selectedPracticeArea", "Admiralty, Aviation and Transportation;Aerospace and Defense");
		testRequest.setParameter("selectedLocation", "Boston, MA;");
		testRequest.setParameter("selectedTitles", "Partner;Associate");
		testRequest.setParameter("name", "brown");
		testRequest.setParameter("selectedChangeType", "Additions;Removals");
		try
		{
//			testController.setOtherSearchParameter(testRequest, attorneySearchDTO);
		}
		catch (Exception ex)
		{
			// Do nothing
		}
		assertTrue(attorneySearchDTO.getPracticeArea().size() == 2);
		assertTrue(attorneySearchDTO.getLocations().size() == 1);
		assertTrue(attorneySearchDTO.getTitles().size() == 2);
		assertTrue(attorneySearchDTO.getAttorneyName().equals("brown"));
		assertTrue(attorneySearchDTO.getChangeType().size()==2);
	}

	/**
	 * Test selected Individual Firms
	 */
	@Test
	public void testSelectedIndividualFirms()
	{
		printTitle();
		AttorneyMoveChangesSearchDTO attorneySearchDTO = new AttorneyMoveChangesSearchDTO();
		MockResourceRequest testRequest = new MockResourceRequest();
		testRequest.setParameter("selectedFirms", "47,31");
//		testController.setSelectedFirms(testRequest.getParameter("selectedFirms"), attorneySearchDTO, testRequest);
		assertTrue(attorneySearchDTO.getSelectedFirms().size() == 2);
		assertTrue(attorneySearchDTO.getSelectedFirms().get(0).getCompany().equals("DLA Piper"));
		assertTrue(attorneySearchDTO.getSelectedFirms().get(1).getCompany().equals("Chadbourne Parke"));
	}

	/**
	 * Test selected Rivaledge Firms
	 */
	@Test
	public void testSelectedRivalEdgeFirms()
	{
		printTitle();
		AttorneyMoveChangesSearchDTO attorneySearchDTO = new AttorneyMoveChangesSearchDTO();
		MockResourceRequest testRequest = new MockResourceRequest();
		testRequest.setParameter("selectedFirms", ALMConstants.AMLAW_100);
//		testController.setSelectedFirms(testRequest.getParameter("selectedFirms"), attorneySearchDTO, testRequest);
		assertTrue(attorneySearchDTO.getSelectedFirms().size() > 0);
	}

	/**
	 * Test fetch search results functionality
	 */
	@Test
	public void testApplySearchUrl()
	{
		printTitle();
		MockResourceRequest testRequest = new MockResourceRequest();
		MockResourceResponse testResponse = new MockResourceResponse();

		testRequest.setParameter("selectedPracticeArea", "All Practice Areas");
		testRequest.setParameter("selectedLocation", ALMConstants.ALL_LOCATIONS);
		testRequest.setParameter("selectedTitles", ALMConstants.ALL);
		testRequest.setParameter("selectedFirms", ALMConstants.AMLAW_100);
		testRequest.setParameter("selectedChangeType", ALMConstants.ALL);
		try
		{
//			testController.applySearchUrl(testRequest, testResponse);
		}
		catch (Exception ex)
		{
			// Do nothing
		}		
		assertNotNull(testRequest.getPortletSession().getPortletContext().getRequestDispatcher("/WEB-INF/jsp/attorneymovesresults.jsp"));
	}

	
	/**
	 * Test set Date range functionality
	 */
	@Test	
	public void testSetDateRange()
	{
		printTitle(); 
		Calendar calendar = Calendar.getInstance();
		Date currentDate = new Date();
		MockResourceRequest testRequest = new MockResourceRequest();
		MockResourceResponse testResponse = new MockResourceResponse();
		AttorneyMoveChangesSearchDTO attorneySearchDTO = new AttorneyMoveChangesSearchDTO();
		testRequest.setParameter("Datetext", ALMConstants.LAST_90_DAYS);
		
//		testController.setDateRange(testRequest, testResponse,attorneySearchDTO);
		calendar.add(Calendar.MONTH, -3);
		assertTrue(attorneySearchDTO.getToDate().toString().equals(currentDate.toString()));
		assertTrue(attorneySearchDTO.getFromDate().toString().equals(calendar.getTime().toString()));
		
		testRequest.setParameter("Datetext", ALMConstants.ANY);
//		testController.setDateRange(testRequest, testResponse,attorneySearchDTO);
		assertNull(attorneySearchDTO.getToDate());
		assertNull(attorneySearchDTO.getFromDate());
		
		
	}
	

}