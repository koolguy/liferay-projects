package com.alm.rivaledge.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alm.rivaledge.BaseALMTest;
import com.alm.rivaledge.persistence.dao.CacheDAO;
import com.alm.rivaledge.persistence.dao.FirmDAO;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.transferobject.AttorneyResultDTO;
import com.alm.rivaledge.transferobject.AttorneySearchDTO;
import com.alm.rivaledge.transferobject.EventResultDTO;
import com.alm.rivaledge.transferobject.EventSearchDTO;
import com.alm.rivaledge.transferobject.PeopleResultDTO;
import com.alm.rivaledge.transferobject.PeopleSearchDTO;

public class CacheServiceTest extends BaseALMTest
{
	@Autowired
	private CacheService testService;
	
	@Autowired
	private CacheDAO testDAO;
	
	@Autowired
	private FirmDAO firmDAO;
	
	@Test
	public void testFirmNewsAndPublicationsJob()
	{
		printTitle();
		int testCount = 0;
		try
		{
			long startTime = Calendar.getInstance().getTimeInMillis(); 
			testCount = testService.firmNewsAndPublicationsJob();
			long endTime = Calendar.getInstance().getTimeInMillis();
			System.out.println(((endTime - startTime) / 60000) + " minutes run time");
		}
		catch (IOException ioEx)
		{
			ioEx.printStackTrace();
		}
		Assert.assertTrue(testCount >= 0);
	}
	
	@Test
	public void testPeopleDataJob()
	{
		printTitle();
		int testCount = 0;
		try
		{
			long startTime = Calendar.getInstance().getTimeInMillis(); 
			testCount  = testService.attorneySearchDataJob();
			testCount += testService.attorneyMovesAndChangesJob();
			testCount += testService.firmStatisticsJob();
			long endTime = Calendar.getInstance().getTimeInMillis();
			System.out.println(((endTime - startTime) / 60000) + " minutes run time");
		}
		catch (IOException ioEx)
		{
			ioEx.printStackTrace();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

		Assert.assertTrue(testCount >= 0);
	}
	
	@Test
	public void testEventsJob()
	{
		printTitle();
		int testCount = 0;
		try
		{
			long startTime = Calendar.getInstance().getTimeInMillis(); 
			testCount = testService.eventsJob();
			long endTime = Calendar.getInstance().getTimeInMillis();
			System.out.println(((endTime - startTime) / 60000) + " minutes run time");
		}
		catch (IOException ioEx)
		{
			ioEx.printStackTrace();
		}
		Assert.assertTrue(testCount >= 0);
	}
	
//	@Test
	public void testFirmStatisticsSearch()
	{
		printTitle();
		
		// No search criteria; all null or default
		PeopleSearchDTO 		testDTO 	= new PeopleSearchDTO();
		List<PeopleResultDTO> 	testResults	= null;
		
		// Set the selected firms
		List<Firm> testList = new ArrayList<Firm>();
		testList.addAll(firmDAO.findByName("Alston & Bird"));
//		testList.add(firmDAO.findByPK(3));
//		testList.add(firmDAO.findByPK(406));
//		testList.addAll(firmDAO.findAmLaw100());
//		testList.addAll(firmDAO.findAllFirms());
		testDTO.setSelectedFirms(testList);
		long startTime = Calendar.getInstance().getTimeInMillis();
		testResults = testDAO.findFirmStatistics(testDTO);
		long endTime = Calendar.getInstance().getTimeInMillis();
		Assert.assertNotNull(testResults);
		Assert.assertFalse(testResults.isEmpty());
		System.out.println("\n\n*** " + ((endTime - startTime) / 1000) + " seconds run time");
		System.out.println("*** Selected firms; testResults = " + testResults.size());
		
		for (PeopleResultDTO prDTO : testResults)
		{
			if (prDTO.getCompanyPeople() != null)
			{
				System.out.println(prDTO.getCompanyPeople().toString());
			}
		}

//		List<String> testPractices = new ArrayList<String>(1);
//		testPractices.add("Foobar");
//		testDTO.setPracticeArea(testPractices);
//
//		List<String> testLocations = new ArrayList<String>(1);
//		testLocations.add("North Korea");
//		testDTO.setLocations(testLocations);
//
//		// Set a date range
//		Calendar rightNow = Calendar.getInstance();
//		testDTO.setToDate(rightNow.getTime());
//		rightNow.add(Calendar.MONTH, -12); // 6 months range
//		testDTO.setFromDate(rightNow.getTime());
//		System.out.println(ALMConstants.SEARCH_DATE_FORMAT.format(testDTO.getFromDate()) + " to " + ALMConstants.SEARCH_DATE_FORMAT.format(testDTO.getToDate()));
//		testResults = testDAO.findFirmStatistics(testDTO);
//		Assert.assertNotNull(testResults);
//		Assert.assertFalse(testResults.isEmpty());
//		System.out.println("*** Selected firms; testResults = " + testResults.size());
//		
//		for (PeopleResultDTO prDTO : testResults)
//		{
//			if (prDTO.getCompanyPeople() != null)
//			{
//				System.out.println(prDTO.getCompanyPeople().toString());
//			}
//		}
		
		// Set firms and one location, no practice
//		List<String> testLocations = new ArrayList<String>(1);
//		testLocations.add("Newark, NJ");
//		testDTO.setLocations(testLocations);
//		testResults = testDAO.findFirmStatistics(testDTO);
//		Assert.assertNotNull(testResults);
//		Assert.assertFalse(testResults.isEmpty());
//		System.out.println("*** Selected firms + One Location; testResults = " + testResults.size() + "\n\n");
		
		// Set firms and one practice, no location
//		testDTO.setLocations(null);
//		List<String> testPractices = new ArrayList<String>(1);
//		testPractices.add("Litigation");
//		testDTO.setPracticeArea(testPractices);
//		testResults = testDAO.findFirmStatistics(testDTO);
//		Assert.assertNotNull(testResults);
//		Assert.assertFalse(testResults.isEmpty());
//		System.out.println("*** Selected firms + One Practice; testResults = " + testResults.size() + "\n\n");
//		for (PeopleResultDTO tDTO : testResults)
//		{
//			System.out.println(tDTO.toString());
//		}

		// Set firms, one location, and one practice
//		testDTO.setLocations(testLocations);
//		testResults = testDAO.findFirmStatistics(testDTO);
//		Assert.assertNotNull(testResults);
//		Assert.assertFalse(testResults.isEmpty());
//		System.out.println("\n\n*** Selected firms + One Location + One Practice; testResults = " + testResults.size());
//		for (PeopleResultDTO tDTO : testResults)
//		{
//			System.out.println(tDTO.toString());
//		}
	}
	
//	@Test
	public void testEventSearch()
	{
		printTitle();
		EventSearchDTO testDTO = new EventSearchDTO();
		List<EventResultDTO> testResults = testDAO.findEventData(testDTO);
		Assert.assertNotNull(testResults);
		Assert.assertFalse(testResults.isEmpty());
		
		List<Firm> testFirms = firmDAO.findByMatchingName("Wolf");
		testDTO.setSelectedFirmsOrganizations(testFirms);
		testResults = testDAO.findEventData(testDTO);
		Assert.assertNotNull(testResults);
		Assert.assertFalse(testResults.isEmpty());
		
		List<String> testPractices = new ArrayList<String>(1);
		testPractices.add("Litigation");
		testDTO.setPracticeArea(testPractices);
		testResults = testDAO.findEventData(testDTO);
		Assert.assertNotNull(testResults);
		Assert.assertFalse(testResults.isEmpty());
	}
	
//	@Test
	public void testAttorneySearch()
	{
		printTitle();

		// No criteria
		AttorneySearchDTO testDTO = new AttorneySearchDTO();
		List<AttorneyResultDTO> testResults = testDAO.findAttorneyData(testDTO);
		Assert.assertNotNull(testResults);
		Assert.assertFalse(testResults.isEmpty());
		System.out.println("*** No criteria: testResults = " + testResults.size() + "\n\n");
		
		// Set the selected firms
		testDTO.setSelectedFirms(firmDAO.findByMatchingName("Wolff"));
		testResults = testDAO.findAttorneyData(testDTO);
		Assert.assertNotNull(testResults);
		Assert.assertFalse(testResults.isEmpty());
		System.out.println("*** Selected firms; testResults = " + testResults.size() + "\n\n");
		
		// Set firms and one location, no practice
		List<String> testLocations = new ArrayList<String>(1);
		testLocations.add("Newark, NJ");
		testDTO.setLocations(testLocations);
		testResults = testDAO.findAttorneyData(testDTO);
		
		// Set firms and one practice, no location
		testDTO.setLocations(null);
		List<String> testPractices = new ArrayList<String>(1);
		testPractices.add("Litigation");
		testDTO.setPracticeArea(testPractices);
		testResults = testDAO.findAttorneyData(testDTO);
		Assert.assertNotNull(testResults);
		Assert.assertFalse(testResults.isEmpty());
		System.out.println("*** Selected firms + One Practice; testResults = " + testResults.size() + "\n\n");

		// Set firms, one location, and one practice
		testDTO.setLocations(testLocations);
		testResults = testDAO.findAttorneyData(testDTO);
		Assert.assertNotNull(testResults);
		Assert.assertFalse(testResults.isEmpty());
		System.out.println("\n\n*** Selected firms + One Location + One Practice; testResults = " + testResults.size());
	}
}