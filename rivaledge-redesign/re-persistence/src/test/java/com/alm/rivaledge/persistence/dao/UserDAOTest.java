package com.alm.rivaledge.persistence.dao;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alm.rivaledge.BaseALMTest;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserPreferences;

public class UserDAOTest extends BaseALMTest
{
	@Autowired
	private UserDAO testDao;
	
	@Test
	public void testGetUserPreferenceByUserAndPortlet()
	{
		printTitle();
		UserPreferences testPreferences = testDao.findUserPreferenceByUserAndPortlet(512, "", null);
		if (testPreferences == null)
		{
			System.out.println("\n*** testPreferences = NULL");
		}
	}
}
