package com.alm.rivaledge.persistence.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alm.rivaledge.BaseALMTest;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.Practice;

/**
 * Unit tests for the FirmDAO.
 * 
 * @author FL867
 * @since Sprint 1
 */
public class FirmDAOTest extends BaseALMTest
{
	@Autowired
	private FirmDAO	testDAO;

	/**
	 * Tests if a Firm can be retrieved with only its primary key.
	 */
	@Test
	public void testFindByPK()
	{
		printTitle();
		Firm testFirm = testDAO.findByPK(5279);
		assertNotNull(testFirm);
		assertTrue(testFirm.getCompany().equals("FEEDBURNER"));
		
		testFirm = testDAO.findByPK(5398);
		assertNotNull(testFirm);
		assertTrue(testFirm.getCompany().equals("Zytemp"));
	}

	/**
	 * Tests if a firm can be retrieved with only its name.
	 */
	@Test
	public void testFindByName()
	{
		printTitle();
		
		//Test that if we supply the full name, we get the record correctly.
		List<Firm> testFirms = testDAO.findByName("Chadbourne Parke");
		assertNotNull(testFirms);
		assertTrue(!testFirms.isEmpty());
		assertTrue((testFirms.iterator().next()).getCompanyId() == 31);
		
		//Reset the collection to null
		testFirms = null;
		assertNull(testFirms);
		
		//Test that if we supply a partial name, we do NOT get the record.
		testFirms = testDAO.findByName("bourne");
		assertNotNull(testFirms);
		assertTrue(testFirms.isEmpty());
	}

	/**
	 * Tests if a firm can be retrieved with only part of its name.
	 */
	@Test
	public void testFindByMatchingName()
	{
		printTitle();
		
		//Test that if we supply a partial name, we still get the partial matches.
		List<Firm> testFirms = testDAO.findByMatchingName("bourne");
		assertNotNull(testFirms);
		assertTrue(!testFirms.isEmpty());
	}

	/**
	 * Tests if all the firms in the database can be found
	 */
	@Test
	public void testFindAll()
	{
		printTitle();
		List<Firm> testFirms = testDAO.findAllFirms();
		assertNotNull(testFirms);
		assertTrue(!testFirms.isEmpty());
	}
	
	/**
	 * Tests if the master list of practices can be loaded from the database.
	 */
	@Test
	public void testFindPractices()
	{
		printTitle();
		List<Practice> testPractices = testDAO.findPractices();
		assertNotNull(testPractices);
		assertTrue(!testPractices.isEmpty());
	}
	
	/**
	 * Tests if the practices can be searched by keyword.
	 */
	@Test
	public void testFindMatchingPractices()
	{
		printTitle();
		List<Practice> testPractices = testDAO.findMatchingPractices("en");
		assertNotNull(testPractices);
		assertTrue(!testPractices.isEmpty());
		assertTrue(testPractices.size() == 8);
	}
}
