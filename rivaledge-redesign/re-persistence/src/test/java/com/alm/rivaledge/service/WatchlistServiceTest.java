package com.alm.rivaledge.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alm.rivaledge.BaseALMTest;
import com.alm.rivaledge.persistence.dao.UserDAO;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroupCompany;
import com.alm.rivaledge.transferobject.UserGroupCompanyDTO;
import com.alm.rivaledge.transferobject.UserGroupDTO;
import com.alm.rivaledge.transferobject.WatchListFirmResultDTO;


/**
 * All the unit tests for the watch list service. Will test all the business
 * logic related to watch lists.
 * 
 * @author FL867
 * @since Sprint 3
 */
public class WatchlistServiceTest extends BaseALMTest
{
	@Autowired
	private WatchlistService testService;
	
	@Autowired
	private UserDAO testUserDAO;
	
	/**
	 * Test method is used for returning all the candidate firms that can be added to this user's watch list.
	 */
	@Test
	public void testGetFirmsForWatchlist()
	{
		printTitle();
		
		// Do we have a non-null service implementation?
		assertNotNull(testService);
		
		List<WatchListFirmResultDTO> testResults = null;
		
		
		// Does this return null if I don't pass in a valid user?
//		testResults = testService.getFirmsForWatchlist(null);
		assertNull(testResults);
		
		// Get a user instance from the database
		User testUser = testUserDAO.findByPrimaryKey(405);
		assertNotNull(testUser);
		
		// Get the user watchlists for this user
		Calendar startTime = Calendar.getInstance();
//		testResults = testService.getFirmsForWatchlist(testUser);
		Calendar endTime = Calendar.getInstance();
		System.out.println("*** 1st Run: " + (endTime.getTimeInMillis() - startTime.getTimeInMillis()) + " milliseconds");
		assertNotNull(testResults);
		
		// Test again to see if the cache kicks in
		startTime = Calendar.getInstance();
//		testResults = testService.getFirmsForWatchlist(testUser);
		endTime = Calendar.getInstance();
		System.out.println("*** 2nd Run: " + (endTime.getTimeInMillis() - startTime.getTimeInMillis()) + " milliseconds");
		assertNotNull(testResults);
	}
	
	/**
	 * Test method is used for adding new Watchlist
	 */
	@Test
	public void testAddMyWatchList()
	{
		printTitle();
		
		// Do we have a non-null service implementation?
		assertNotNull(testService);
		
		// userGroupDTO for creating a watchlist
		UserGroupDTO userGroupDTO = new UserGroupDTO();
		userGroupDTO.setId(10168);
		userGroupDTO.setGroupName("TestWatchList");
		userGroupDTO.setDateAdded(new Date());
		// adding a watchlist into a database
		testService.addMyWatchList(userGroupDTO);
		//checking groupId is assigned or not to new watchlist
		assertNotNull(userGroupDTO.getGroupId());
	}
	
	/**
	 * Test method is used for updating existing watchlist
	 */
	@Test
	public void testUpdateMyWatchList()
	{
		printTitle();
		
		// Do we have a non-null service implementation?
		assertNotNull(testService);
		
		// userGroupDTO for creating a watchlist
		UserGroupDTO userGroupDTO = new UserGroupDTO();
		userGroupDTO.setId(10168);
		userGroupDTO.setOldGroupName("TestWatchList");
		userGroupDTO.setGroupName("TestWatchList-Updated");
		userGroupDTO.setDateAdded(new Date());
		// updating existing watchlist into a database
		testService.updateMyWatchList(userGroupDTO);
	
		Integer groupId = null;
		UserGroupDTO userGroupDTOtemp = new UserGroupDTO();
		userGroupDTOtemp.setId(10168);
		userGroupDTOtemp.setGroupName("TestWatchList-Updated");
		//checking groupId is assigned or not for updated watchlist
		groupId = testService.getWatchList(userGroupDTOtemp);
		assertNotNull(groupId);
	}
	
	/**
	 * Test method is used for deletion of existing watchlist
	 */
	@Test
	public void testDeleteMyWatchList()
	{
		printTitle();
		
		// Do we have a non-null service implementation?
		assertNotNull(testService);
		// userGroupDTO for creating a watchlist
		UserGroupDTO userGroupDTO = new UserGroupDTO();
		userGroupDTO.setId(10168);
		userGroupDTO.setGroupName("TestWatchList-Updated");
		userGroupDTO.setDateAdded(new Date());
		// deletion existing watchlist into a database
		testService.deleteMyWatchList(userGroupDTO);
		Integer groupId = null;
		UserGroupDTO userGroupDTOtemp = new UserGroupDTO();
		userGroupDTOtemp.setId(10168);
		userGroupDTOtemp.setGroupName("TestWatchList-Updated");
		//checking groupId is assigned or not for updated watchlist
		groupId = testService.getWatchList(userGroupDTOtemp);
		//checking groupId should be null because watchlist has been deleted
		//groupId = testService.getWatchList("TestWatchList-Updated");
		assertNull(groupId);
		
	}
	
	/**
	 * Test method is used for getting all firm watchlists for a user
	 */
	@Test
	public void testGetAllFirmWatchList()
	{
		printTitle();
		
		// Do we have a non-null service implementation?
		assertNotNull(testService);
		/*UserGroupDTO userGroupDTOtemp = new UserGroupDTO();
		userGroupDTOtemp.setId(10168);
		userGroupDTOtemp.setGroupName("TestWatchList-Updated");*/
		
		//fetching firm's watchlist
		List<UserGroupDTO> userGroupsList  = testService.getAllFirmWatchList(10168);
		assertNotNull(userGroupsList);
	}
	
	/**
	 * Test method is used for returning watch list's id based on watchlistname
	 */
	@Test
	public void testGetWatchList()
	{
		printTitle();
		
		// Do we have a non-null service implementation?
		assertNotNull(testService);
		
		Integer groupId = null;
		UserGroupDTO userGroupDTOtemp = new UserGroupDTO();
		userGroupDTOtemp.setId(10168);
		userGroupDTOtemp.setGroupName("GetWatchList");
		
		//fetching a watchlist and we should get groupId if watchlist exist otherwise groupId should be null
		groupId = testService.getWatchList(userGroupDTOtemp);
		assertNull(groupId);
	}
	
	/**
	 * Test method is used for saving firm into a watchlist
	 */
	@Test
	public void testSaveFirmForWatchList()
	{
		printTitle();
		
		// Do we have a non-null service implementation?
		assertNotNull(testService);
		
		// userGroupDTO for creating a watchlist
		UserGroupDTO userGroupDTO = new UserGroupDTO();
		Integer groupId = null;
		userGroupDTO.setId(10168);
		userGroupDTO.setGroupName("CheckWatchList11");
		userGroupDTO.setDateAdded(new Date());
		//saving watchlist
		testService.addMyWatchList(userGroupDTO);
		UserGroupDTO userGroupDTOtemp = new UserGroupDTO();
		userGroupDTOtemp.setId(10168);
		userGroupDTOtemp.setGroupName("CheckWatchList11");
		//checking watchlist exist or not ?
		groupId = testService.getWatchList(userGroupDTO);
		assertNotNull(groupId);
		
		// adding a firm for created watchlist
		UserGroupCompany userGroupCompany = new UserGroupCompany();
	
		userGroupCompany.setId(10168);
		userGroupCompany.setCompanyId(636);
		userGroupCompany.setDateAdded(new Date());
		userGroupCompany.setGroupId(groupId);
		//saving firm into watchlist
		testService.saveFirmForWatchList(userGroupCompany);
		//userGroupCompanies should not be null because we have added firm into watchlist
		List<UserGroupCompanyDTO> userGroupCompanies = testService.getAddedFirms(userGroupCompany);
		assertNotNull(userGroupCompanies);
		
		//removing firm from watchlist
		testService.removeFirmsFromWatchList(userGroupCompany);
		List<UserGroupCompanyDTO> tempUserGroupCompanyies = testService.getAddedFirms(userGroupCompany);
		assertEquals(tempUserGroupCompanyies.size(),0);
		//removing watchlist
		testService.deleteMyWatchList(userGroupDTO);
		UserGroupDTO userGroupDTOnew = new UserGroupDTO();
		userGroupDTOnew.setId(10168);
		userGroupDTOnew.setGroupName("CheckWatchList11");

		groupId = testService.getWatchList(userGroupDTO);
		assertNull(groupId);
		
	}
	
	/**
	 * Test method is used for returning all Firms associated with a watchlist
	 */
	@Test
	public void testGetAddedFirms()
	{

		printTitle();
		
		// Do we have a non-null service implementation?
		assertNotNull(testService);
		// userGroupDTO for creating a watchlist
		UserGroupDTO userGroupDTO = new UserGroupDTO();
		Integer groupId = null;
		userGroupDTO.setId(10168);
		userGroupDTO.setGroupName("CheckWatchList22");
		userGroupDTO.setDateAdded(new Date());
		//saving watchlist
		testService.addMyWatchList(userGroupDTO);
		UserGroupDTO userGroupDTOnew = new UserGroupDTO();
		userGroupDTOnew.setId(10168);
		userGroupDTOnew.setGroupName("CheckWatchList22");

		//checking watchlist exist or not ?
		groupId = testService.getWatchList(userGroupDTOnew);
		assertNotNull(groupId);
		// adding a firm for created watchlist
		UserGroupCompany userGroupCompany = new UserGroupCompany();
		userGroupCompany.setId(10168);
		userGroupCompany.setCompanyId(636);
		userGroupCompany.setDateAdded(new Date());
		userGroupCompany.setGroupId(groupId);
		//saving firm into watchlist
		testService.saveFirmForWatchList(userGroupCompany);
		//userGroupCompanies should not be null because we have added firm into watchlist
		List<UserGroupCompanyDTO> userGroupCompanies = testService.getAddedFirms(userGroupCompany);
		assertNotNull(userGroupCompanies);
		//removing firm from watchlist
		testService.removeFirmsFromWatchList(userGroupCompany);
		List<UserGroupCompanyDTO> tempUserGroupCompanyies = testService.getAddedFirms(userGroupCompany);
		assertEquals(tempUserGroupCompanyies.size(),0);
		//removing watchlist
		testService.deleteMyWatchList(userGroupDTO);
		UserGroupDTO userGroupDTOtemp = new UserGroupDTO();
		userGroupDTOtemp.setId(10168);
		userGroupDTOtemp.setGroupName("CheckWatchList22");
		groupId = testService.getWatchList(userGroupDTOtemp);
		assertNull(groupId);
	}
	
	/**
	 * Test method is used for removing firms for a watchlist
	 */
	
	@Test
	public void testRemoveFirmsFromWatchList() 
	{
		
		printTitle();
		
		// Do we have a non-null service implementation?
		assertNotNull(testService);
		// userGroupDTO for creating a watchlist
		UserGroupDTO userGroupDTO = new UserGroupDTO();
		Integer groupId = null;
		userGroupDTO.setId(10168);
		userGroupDTO.setGroupName("CheckWatchList33");
		userGroupDTO.setDateAdded(new Date());
		//saving watchlist
		testService.addMyWatchList(userGroupDTO);
		// adding a firm for created watchlist
		UserGroupCompany userGroupCompany = new UserGroupCompany();
		
		userGroupCompany.setId(10168);
		userGroupCompany.setCompanyId(636);
		userGroupCompany.setDateAdded(new Date());
		
		groupId = testService.getWatchList(userGroupDTO);
		userGroupCompany.setGroupId(groupId);
		//removing firm from watchlist
		testService.removeFirmsFromWatchList(userGroupCompany);
		List<UserGroupCompanyDTO> tempUserGroupCompanyies = testService.getAddedFirms(userGroupCompany);
		assertEquals(tempUserGroupCompanyies.size(),0);
		//removing watchlist
		testService.deleteMyWatchList(userGroupDTO);
		groupId = testService.getWatchList(userGroupDTO);
		assertNull(groupId);
		
	}
	
}
