package com.alm.rivaledge.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.alm.rivaledge.BaseALMTest;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.Practice;
import com.alm.rivaledge.transferobject.FirmResultDTO;
import com.alm.rivaledge.transferobject.FirmSearchDTO;
import com.alm.rivaledge.util.ALMConstants;

/**
 * Unit tests for the FirmService.
 * 
 * @author FL867
 * @since Sprint 1
 */
public class FirmServiceTest extends BaseALMTest
{
	@Autowired
	@Qualifier("firmService")
	private FirmService	testService;

	/**
	 * Tests that we are able to fetch the AmLaw 100 and 200 and NLJ 250 rankings.
	 */
	@Test
	public void testGetRanking()
	{
		printTitle();
		
		// Test for AmLaw100
		List<Firm> testList = testService.getRanking(ALMConstants.AMLAW_100);
		assertNotNull(testList);
		assertTrue(!testList.isEmpty());
		for (Firm tF : testList)
		{
			assertTrue((tF.getAmlawRank() >= 1) && (tF.getAmlawRank() <= 100));
		}

		// Test for AmLaw200
		testList = testService.getRanking(ALMConstants.AMLAW_200);
		assertNotNull(testList);
		assertTrue(!testList.isEmpty());
		for (Firm tF : testList)
		{
			assertTrue((tF.getAmlawRank() >= 1) && (tF.getAmlawRank() <= 200));
		}

		// Test for NLJ250
		testList = testService.getRanking(ALMConstants.NLJ_250);
		assertNotNull(testList);
		assertTrue(!testList.isEmpty());
		for (Firm tF : testList)
		{
			assertTrue((tF.getNljRank() >= 1) && (tF.getNljRank() <= 250));
		}
	}
	
	/**
	 * Tests the keyword search for Practices
	 */
	@Test
	public void testGetMatchingPractices()
	{
		printTitle();
		
		//Try with two character keyword; we should get an empty list.
		List<Practice> testList = testService.getMatchingPractices("en");
		assertNotNull(testList);
		assertTrue(testList.isEmpty());
		
		//Try with a two character keyword padded with some spaces;
		//we should still get an empty set.
		testList = testService.getMatchingPractices("en   ");
		assertNotNull(testList);
		assertTrue(testList.isEmpty());
		
		//Now try with a three character keyword; we should get valid results
		testList = testService.getMatchingPractices("ent");
		assertNotNull(testList);
		assertTrue(!testList.isEmpty());
		
		//Finally try with a four character keyword; we should get a single search result
		testList = testService.getMatchingPractices("ente");
		assertNotNull(testList);
		assertTrue(!testList.isEmpty());
	}
	
	/**
	 * Test the firm search functionality
	 */
	@Test
	public void testFirmSearch()
	{
		printTitle();
		
		// Setup the SearchDTO
		FirmSearchDTO testDTO = new FirmSearchDTO();
		List<Firm> testFirms = new ArrayList<Firm>(1); 
		testFirms.add(testService.getFirmById(31));
				
		List<String> testPractices = new ArrayList<String>(3);
		testPractices.add("Corporate and Business");
		testPractices.add("Banking and Finance");
		testPractices.add("Labor and Employment");
		
		// Stuff everything into the DTO
		testDTO.setSelectedFirms(testFirms);
		testDTO.setPracticeArea(testPractices);
		
		// Get the result set
		List<FirmResultDTO> testResults = testService.firmNewsPubsSearch(testDTO);
		assertNotNull(testResults);
		assertTrue(!testResults.isEmpty());
		
		// Default search
		// Firms = ALL (default)
		// Practice Areas = ALL (default)
		// Dates = One month range - 01/DEc/2012 to 31/Dec/2012
		
		try
		{
			testDTO.setFromDate(ALMConstants.SEARCH_DATE_FORMAT.parse("2012-12-01"));
			testDTO.setToDate(ALMConstants.SEARCH_DATE_FORMAT.parse("2012-12-31"));
		}
		catch (ParseException pEx)
		{
			// Do nothing.
		}
		
		testDTO.setPracticeArea(testPractices);
		testResults = testService.firmNewsPubsSearch(testDTO);
		assertNotNull(testResults);
		assertTrue(!testResults.isEmpty());
		assertTrue(testResults.size() == 2);
		
		testFirms = testService.getRanking(ALMConstants.AMLAW_100);
		testDTO = new FirmSearchDTO();
		testDTO.setSelectedFirms(testFirms);
		try
		{
			testDTO.setFromDate(ALMConstants.SEARCH_DATE_FORMAT.parse("2012-01-01"));
			testDTO.setToDate(ALMConstants.SEARCH_DATE_FORMAT.parse("2012-12-31"));
		}
		catch (ParseException pEx)
		{
			// Do nothing.
		}
		testDTO.setPracticeArea(testPractices);
		testResults = testService.firmNewsPubsSearch(testDTO);
		assertNotNull(testResults);
		assertTrue(!testResults.isEmpty());
	}
	
	@Test
	public void testFirmNewsPubsSearch()
	{
		FirmSearchDTO testDTO = new FirmSearchDTO();
		List<Firm> testFirms = new ArrayList<Firm>(2);
		testFirms.add(testService.getFirmById(7387));
		testFirms.add(testService.getFirmById(635));
		
		testDTO.setSelectedFirms(testFirms);
		testDTO.setPageNumber(1);
		testDTO.setMaxResultSizeCap(1000);
		
		// Default search
		int totalRecords = testService.getFirmSearchCount(testDTO, null);
		assertTrue(totalRecords > 100);
		
		List<FirmResultDTO> testResults = testService.firmNewsPubsSearch(testDTO);
		assertNotNull(testResults);
		assertTrue(testResults.size() == 100);
		
		// These 100 records must all be in descending order of date
		Date thisDate = null;
		for (FirmResultDTO tR : testResults)
		{
			if (thisDate != null)
			{
				assertTrue((thisDate.compareTo(tR.getEntryDate())) >= 0);
			}
			thisDate = tR.getEntryDate();
		}
		
		// Now we will flip the sort order to ascending date
		testDTO.setSortOrder("asc");
		
		testResults = testService.firmNewsPubsSearch(testDTO);
		assertNotNull(testResults);
		assertTrue(testResults.size() == 100);
		thisDate = null;
		for (FirmResultDTO tR : testResults)
		{
			if (thisDate != null)
			{
				assertTrue((thisDate.compareTo(tR.getEntryDate())) <= 0);
			}
			thisDate = tR.getEntryDate();
		}
		
		// We will set the page number to 2
		testDTO.setPageNumber(2);
		testResults = testService.firmNewsPubsSearch(testDTO);
		assertNotNull(testResults);
		assertTrue(testResults.size() == 100);
	}
}

