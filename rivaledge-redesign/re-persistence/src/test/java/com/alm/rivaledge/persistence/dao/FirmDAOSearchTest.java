package com.alm.rivaledge.persistence.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alm.rivaledge.BaseALMTest;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.transferobject.FirmResultDTO;
import com.alm.rivaledge.transferobject.FirmSearchDTO;
import com.alm.rivaledge.util.ALMConstants;

/**
 * Just the unit tests for the Firm news/publications search
 * 
 * @author FL867
 * @since Sprint 1
 * 
 */
public class FirmDAOSearchTest extends BaseALMTest
{
	@Autowired
	private FirmDAO	firmDAO;
	
	@Autowired
	private CacheDAO testDAO;
	
	/**
	 * Search ALL publications/news/Tweets for a specific company;
	 * no other criteria.
	 */
	@Test
	public void testSearchForOneCompany()
	{
		printTitle();
		
		// Setup the SearchDTO
		FirmSearchDTO testDTO = new FirmSearchDTO();
		List<Firm> testFirms = firmDAO.findByName("Chadbourne Parke");
		assertNotNull(testFirms);
		assertTrue(!testFirms.isEmpty());
		assertTrue(testFirms.size() == 1);
		
		// Stuff everything into the DTO
		testDTO.setSelectedFirms(testFirms);
		
		// Get the result set
		List<FirmResultDTO> testResults = testDAO.findFirmNewsPubs(testDTO);
		assertNotNull(testResults);
		assertTrue(!testResults.isEmpty());
		for (FirmResultDTO testData : testResults)
		{
			assertTrue(testData.getFirmName().equals("Chadbourne Parke"));
		}
	}

	/**
	 * Search ALL publications/news/Tweets for a set of firms;
	 * no other criteria.
	 */
	@Test
	public void testSearchForManyCompanies()
	{
		printTitle();
		
		// Setup the SearchDTO
		FirmSearchDTO testDTO = new FirmSearchDTO();
		List<Firm> testFirms = firmDAO.findByMatchingName("Miller");
		assertNotNull(testFirms);
		assertTrue(!testFirms.isEmpty());
		
		// Stuff everything into the DTO
		testDTO.setSelectedFirms(testFirms);
		
		// Get the result set
		List<FirmResultDTO> testResults = testDAO.findFirmNewsPubs(testDTO);
		assertNotNull(testResults);
		assertTrue(!testResults.isEmpty());
	}

	/**
	 * Search ALL publications/news/Tweets for a specific company;
	 * with one Practice Area specified.
	 */
	@Test
	public void testSearchForOneCompanyAndOnePracticeArea()
	{
		printTitle();
		
		// Setup the SearchDTO
		FirmSearchDTO testDTO = new FirmSearchDTO();
		List<Firm> testFirms = firmDAO.findByName("Chadbourne Parke");
		assertNotNull(testFirms);
		assertTrue(!testFirms.isEmpty());
		assertTrue(testFirms.size() == 1);
				
		List<String> testPractices = new ArrayList<String>(1);
		testPractices.add("%Corporate and Business%");
		
		// Stuff everything into the DTO
		testDTO.setSelectedFirms(testFirms);
		testDTO.setPracticeArea(testPractices);
		
		// Get the result set
		List<FirmResultDTO> testResults = testDAO.findFirmNewsPubs(testDTO);
		assertNotNull(testResults);
		assertTrue(!testResults.isEmpty());
	}

	/**
	 * Search ALL publications/news/Tweets for a specific company;
	 * with more than one Practice Area specified.
	 */
	@Test
	public void testSearchForOneCompanyAndManyPracticeAreas()
	{
		printTitle();
		
		// Setup the SearchDTO
		FirmSearchDTO testDTO = new FirmSearchDTO();
		List<Firm> testFirms = firmDAO.findByName("Chadbourne Parke");
		assertNotNull(testFirms);
		assertTrue(!testFirms.isEmpty());
		assertTrue(testFirms.size() == 1);
				
		List<String> testPractices = new ArrayList<String>(3);
		testPractices.add("%Corporate and Business%");
		testPractices.add("%Banking and Finance%");
		testPractices.add("%Labor and Employment%");
		
		// Stuff everything into the DTO
		testDTO.setSelectedFirms(testFirms);
		testDTO.setPracticeArea(testPractices);
		
		// Get the result set
		List<FirmResultDTO> testResults = testDAO.findFirmNewsPubs(testDTO);
		assertNotNull(testResults);
		assertTrue(!testResults.isEmpty());
	}

	/**
	 * Search ALL publications/news/Tweets for many companies;
	 * with more than one Practice Area specified.
	 */
	@Test
	public void testSearchForManyCompaniesAndManyPracticeAreas()
	{
		printTitle();
		
		// Setup the SearchDTO
		FirmSearchDTO testDTO = new FirmSearchDTO();
		List<Firm> testFirms = firmDAO.findByMatchingName("Miller");
		assertNotNull(testFirms);
		assertTrue(!testFirms.isEmpty());
				
		List<String> testPractices = new ArrayList<String>(3);
		testPractices.add("%Corporate and Business%");
		testPractices.add("%Banking and Finance%");
		testPractices.add("%Labor and Employment%");
		
		// Stuff everything into the DTO
		testDTO.setSelectedFirms(testFirms);
		testDTO.setPracticeArea(testPractices);
		
		// Get the result set
		List<FirmResultDTO> testResults = testDAO.findFirmNewsPubs(testDTO);
		assertNotNull(testResults);
		assertTrue(!testResults.isEmpty());
	}

	/**
	 * Search ALL publications/news/Tweets for a specific company
	 * with a specified date range.
	 */
	@Test
	public void testSearchForOneCompanyByDateRange()
	{
		printTitle();
		
		// Setup the SearchDTO
		FirmSearchDTO testDTO = new FirmSearchDTO();
		List<Firm> testFirms = firmDAO.findByName("Chadbourne Parke");
		assertNotNull(testFirms);
		assertTrue(!testFirms.isEmpty());
		assertTrue(testFirms.size() == 1);
		
		// Stuff everything into the DTO
		testDTO.setSelectedFirms(testFirms);
		try
		{
			testDTO.setFromDate(ALMConstants.SEARCH_DATE_FORMAT.parse("2010-01-01"));
			testDTO.setToDate(ALMConstants.SEARCH_DATE_FORMAT.parse("2011-12-31"));
		}
		catch (ParseException pEx)
		{
			// Do nothing.
		}
		
		// Get the result set
		List<FirmResultDTO> testResults = testDAO.findFirmNewsPubs(testDTO);
		assertNotNull(testResults);
		assertTrue(!testResults.isEmpty());
	}
}