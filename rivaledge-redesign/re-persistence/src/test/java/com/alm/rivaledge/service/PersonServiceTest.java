package com.alm.rivaledge.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alm.rivaledge.BaseALMTest;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.transferobject.PeopleResult;
import com.alm.rivaledge.transferobject.PeopleResultDTO;
import com.alm.rivaledge.transferobject.PeopleSearchDTO;

public class PersonServiceTest extends BaseALMTest
{
	@Autowired
	private PeopleService testService;
	
	@Autowired
	private FirmService firmService;
	
	@Test
	public void testSearchWithOneCompany()
	{
		printTitle();
		PeopleSearchDTO psDTO = new PeopleSearchDTO();
		List<Firm> testFirms = new ArrayList<Firm>(1);
		Firm testFirm = new Firm();
		testFirm.setCompanyId(8);
		testFirms.add(testFirm);
		psDTO.setSelectedFirms(testFirms);
		
		List<PeopleResultDTO> testResults = testService.peopleSearch(psDTO, null);
		
		if (testResults != null)
		{
			System.out.println("\n\n*** testResults = " + testResults.size());
			if (testResults.get(0).getPeopleResults() != null)
			{
				for (PeopleResult pR : testResults.get(0).getPeopleResults())
				{
					System.out.println("*** " + pR.toString());
				}
			}
		}
		else
		{
			System.out.println("\n\n*** testResults = NULL");
		}
	}

	@Test
	public void testSearchWithManyCompanies()
	{
		printTitle();
		PeopleSearchDTO psDTO = new PeopleSearchDTO();
		List<Firm> testFirms = new ArrayList<Firm>(2);
		
		Firm testFirm = new Firm();
		testFirm.setCompanyId(8);
		testFirms.add(testFirm);
		
		testFirm = new Firm();
		testFirm.setCompanyId(406);
		testFirms.add(testFirm);
		
		testFirm = new Firm();
		testFirm.setCompanyId(3);
		testFirms.add(testFirm);
		
		testFirm = new Firm();
		testFirm.setCompanyId(47);
		testFirms.add(testFirm);
		
		psDTO.setSelectedFirms(testFirms);
		
		List<PeopleResultDTO> testResults = testService.peopleSearch(psDTO, null);
		
		if (testResults != null)
		{
			System.out.println("\n\n*** testResults = " + testResults.size());
			if (testResults.get(0).getPeopleResults() != null)
			{
				for (PeopleResultDTO prDTO : testResults)
				{
					for (PeopleResult pR : prDTO.getPeopleResults())
					{
						System.out.println("*** " + pR.toString());
					}				
				}
			}
		}
		else
		{
			System.out.println("\n\n*** testResults = NULL");
		}
	}
}
