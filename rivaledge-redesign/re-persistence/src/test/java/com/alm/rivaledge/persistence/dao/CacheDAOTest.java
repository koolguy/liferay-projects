package com.alm.rivaledge.persistence.dao;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alm.rivaledge.BaseALMTest;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.transferobject.PeopleResultDTO;
import com.alm.rivaledge.transferobject.PeopleSearchDTO;

public class CacheDAOTest extends BaseALMTest
{
	@Autowired 
	private CacheDAO testDAO;
	
	@Autowired
	private FirmDAO firmDAO;
	
	@Test
	public void testGetBasicHeadcounts()
	{
		printTitle();
		
		PeopleSearchDTO 		testDTO 	= new PeopleSearchDTO();
		List<PeopleResultDTO> 	testResults	= null;
		
		// Set the selected firms
		List<Firm> testList = new ArrayList<Firm>();
//		testList.add(firmDAO.findByPK(3));
//		testList.add(firmDAO.findByPK(12));
//		testList.addAll(firmDAO.findAmLaw100());
		testList.addAll(firmDAO.findAllFirms());
		testDTO.setSelectedFirms(testList);
		
		String firmSize = "1500+";
		List<String> stringList = new ArrayList<String>(2);
		stringList.add(firmSize);
		firmSize = "400-500";
		stringList.add(firmSize);
		testDTO.setFirmSizeList(stringList);
		
		long startTime = Calendar.getInstance().getTimeInMillis();
		testResults = testDAO.findFirmStatisticsVeryFast(testDTO);
		long endTime = Calendar.getInstance().getTimeInMillis();
		Assert.assertNotNull(testResults);
		Assert.assertFalse(testResults.isEmpty());
		System.out.println("\n\n*** " + ((endTime - startTime) / 1000) + " seconds run time");
		System.out.println("*** Selected firms; testResults = " + testResults.size());
		
		for (PeopleResultDTO prDTO : testResults)
		{
			if (prDTO.getCompanyPeople() != null)
			{
				System.out.println(prDTO.getCompanyPeople().toString());
			}
		}
	}
}
