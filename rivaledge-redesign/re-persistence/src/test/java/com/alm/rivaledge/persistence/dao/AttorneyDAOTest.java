package com.alm.rivaledge.persistence.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alm.rivaledge.BaseALMTest;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.transferobject.AttorneyResultDTO;
import com.alm.rivaledge.transferobject.AttorneySearchDTO;

/**
 * Unit tests for the Firm Attorney Search results
 * 
 * @author FL605
 * @since Sprint 3
 * 
 */
public class AttorneyDAOTest extends BaseALMTest
{
	@Autowired
	private CacheDAO	testDAO;

	@Autowired
	private FirmDAO		firmDAO;

	/**
	 * Unit test case for Attorney search results.
	 */
	@Test
	public void testFindAttorneyData()
	{
		printTitle();
		AttorneySearchDTO testDTO = new AttorneySearchDTO();

		List<Firm> testFirms = new ArrayList<Firm>(1);
		testFirms.add(firmDAO.findByPK(31));

		List<String> testLocations = new ArrayList<String>(3);
		testLocations.add("London, United Kingdom");
		testLocations.add("New York, NY");
		testLocations.add("Houston, TX");

		// Stuff everything into the DTO
		testDTO.setSelectedFirms(testFirms);
		testDTO.setLocations(testLocations);

		List<AttorneyResultDTO> testResults = testDAO.findAttorneyData(testDTO);
		assertNotNull(testResults);
		assertTrue(!testResults.isEmpty());

		List<String> attorneyNamelist= new ArrayList<String>(1);
		attorneyNamelist.add("Krivoy, Clara");
		testDTO.setAttorneyName(attorneyNamelist);
		testDTO.setLocations(testLocations);
		testResults = testDAO.findAttorneyData(testDTO);
		assertNotNull(testResults);
		assertTrue(!testResults.isEmpty());

		List<String> testTitles = new ArrayList<String>(1);
		testTitles.add("Partner");
		testDTO.setTitles(testTitles);
		testDTO.setLocations(testLocations);
		testResults = testDAO.findAttorneyData(testDTO);
		
		assertNotNull(testResults);
		assertTrue(!testResults.isEmpty());
	}
}
