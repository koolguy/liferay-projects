package com.alm.rivaledge.persistence.dao;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alm.rivaledge.BaseALMTest;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.Person;
import com.alm.rivaledge.transferobject.PeopleSearchDTO;

public class PersonDAOTest extends BaseALMTest
{
	@Autowired
	private PersonDAO testDAO;
	
	@Autowired
	private FirmDAO firmDAO;
	
	/**
	 * Given a person id, tests that the owning firm can be found too.
	 */
	@Test
	public void testChildrenSelect()
	{
		Person testPerson = testDAO.findByPK(421708);
		assertNotNull(testPerson);
		assertTrue(testPerson.getStdName().equals("Wagner, Jeffrey M"));
		assertTrue(testPerson.getLawFirm().getCompany().equals("Kaye Scholer"));
		assertNotNull(testPerson.getPersonChanges());
//		assertFalse(testPerson.getPersonChanges().isEmpty());
	}
	
	/**
	 * Find all person movement (changes) for a single firm
	 */
	@Test
	public void testFindPersonData()
	{
		List<Firm> testFirms = firmDAO.findByName("DLA Piper");
		assertNotNull(testFirms);
		assertTrue(testFirms.size() == 1);
		PeopleSearchDTO testDTO = new PeopleSearchDTO();
		testDTO.setSelectedFirms(testFirms);
		List<Person> testResults = testDAO.findPersonData(testDTO, 0);
		assertNotNull(testResults);
		assertFalse(testResults.isEmpty());
		
		List<String> testPractices = new ArrayList<String>(3);
		testPractices.add("Corporate and Business");
		testPractices.add("Banking and Finance");
		testPractices.add("Labor and Employment");
		testDTO.setPracticeArea(testPractices);
		
		testResults = testDAO.findPersonData(testDTO, 0);
		assertNotNull(testResults);
		assertFalse(testResults.isEmpty());
		
		List<String> testLocations = new ArrayList<String>(3);
		testLocations.add("Boston, MA");
		testLocations.add("New York, NY");
		testLocations.add("Washington, DC)");
		
		testResults = testDAO.findPersonData(testDTO, 0);
		assertNotNull(testResults);
		assertFalse(testResults.isEmpty());
	}
}
