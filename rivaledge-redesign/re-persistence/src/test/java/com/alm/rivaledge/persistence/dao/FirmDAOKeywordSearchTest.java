package com.alm.rivaledge.persistence.dao;

import static org.junit.Assert.assertNotNull;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alm.rivaledge.BaseALMTest;
import com.alm.rivaledge.transferobject.FirmResultDTO;
import com.alm.rivaledge.transferobject.FirmSearchDTO;

/**
 * Specific set of unit tests that only deal with the different 
 * keyword searches that are possible for the Firm news and publications
 * search.
 * @author FL867
 * @since Sprint 2
 */
public class FirmDAOKeywordSearchTest extends BaseALMTest
{
	@Autowired
	private FirmDAO firmDAO;
	
	@Autowired
	private CacheDAO testDAO;
	
	@Test
	public void fooTest()
	{
		printTitle();
		FirmSearchDTO testDTO = new FirmSearchDTO();
		testDTO.setSelectedFirms(firmDAO.findByMatchingName("Ada"));
		List<String> testTypes = new ArrayList<String>();
		testTypes.add("News");
		testTypes.add("Twitter");
		testDTO.setContentType(testTypes);
		testTypes = new ArrayList<String>();
		testTypes.add("%M&A%");
		testTypes.add("%Litigation%");
		testDTO.setPracticeArea(testTypes);
		
		testDTO.setKeyword("%announce%");
		
		List<FirmResultDTO> testResults = testDAO.findFirmNewsPubs(testDTO);
		assertNotNull(testResults);
		System.out.println("*** size = " + testResults.size());
		Assert.assertFalse(testResults.isEmpty());
		Assert.assertTrue(testResults.size() <= 100);
	}
}
