package com.alm.rivaledge.persistence.dao;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.alm.rivaledge.BaseALMTest;
import com.alm.rivaledge.persistence.domain.lawma0_data.Person;
import com.alm.rivaledge.transferobject.PeopleSearchDTO;

/**
 * Unit tests for the PersonDAO.
 * 
 * @author FL867
 * @since Sprint 2
 */
public class PersonDAOSearchTest extends BaseALMTest
{
	@Autowired
	private PersonDAO	testDao;

	@Autowired
	private FirmDAO		firmDao;

	/**
	 * Search for all persons for a single firm, no other criteria.
	 */
	@Test
	public void testSearchForOneCompany()
	{
		printTitle();
		PeopleSearchDTO testDTO = new PeopleSearchDTO();
		testDTO.setSelectedFirms(firmDao.findByName("Chadbourne Parke"));

		List<Person> testResults = testDao.findPersonData(testDTO, 0);
		assertNotNull(testResults);
		int testId = testDTO.getSelectedFirms().iterator().next().getCompanyId();
		for (Person testPerson : testResults)
		{
			assertTrue(testPerson.getLawFirm().getCompanyId() == testId);
		}
	}

	/**
	 * Search for all persons for two firms, no other criteria
	 */
	@Test
	public void testSearchForTwoCompanies()
	{
		printTitle();
		PeopleSearchDTO testDTO = new PeopleSearchDTO();
		testDTO.setSelectedFirms(firmDao.findByMatchingName("Chad"));

		List<Person> testResults = testDao.findPersonData(testDTO, 0);
		assertNotNull(testResults);
		int testId1 = 31;
		int testId2 = 6612;
		for (Person testPerson : testResults)
		{
			assertTrue((testPerson.getLawFirm().getCompanyId() == testId1) || (testPerson.getLawFirm().getCompanyId() == testId2));
		}
	}

	/**
	 * Search for one company and many practices
	 */
	@Test
	public void testSearchForOneCompanyAndManyPractices()
	{
		printTitle();
		PeopleSearchDTO testDTO = new PeopleSearchDTO();
		testDTO.setSelectedFirms(firmDao.findByName("Chadbourne Parke"));

		List<String> testLocations = new ArrayList<String>(3);
		testLocations.add("Corporate and Business");
		testLocations.add("M&A");
		testLocations.add("Litigation");
		testDTO.setPracticeArea(testLocations);
		List<Person> testResults = testDao.findPersonData(testDTO, 0);
		assertNotNull(testResults);
		int testId = testDTO.getSelectedFirms().iterator().next().getCompanyId();
		for (Person testPerson : testResults)
		{
			assertTrue(testPerson.getLawFirm().getCompanyId() == testId);
//			assertTrue((testPerson.getPrimaryStdPractice().equals("Corporate and Business")) 
//					|| (testPerson.getPrimaryStdPractice().equals("M&A"))
//					|| (testPerson.getPrimaryStdPractice().equals("Litigation")));
		}
	}
	
	/**
	 * Search for one company and many locations
	 */
	@Test
	public void testSearchForOneCompanyAndManyLocations()
	{
		printTitle();
		PeopleSearchDTO testDTO = new PeopleSearchDTO();
		testDTO.setSelectedFirms(firmDao.findByName("Chadbourne Parke"));

		List<String> testLocations = new ArrayList<String>(3);
		testLocations.add("New York, NY");
		testLocations.add("Washington, DC");
		testLocations.add("London, United Kingdom");
		testDTO.setPracticeArea(testLocations);
		List<Person> testResults = testDao.findPersonData(testDTO, 0);
		assertNotNull(testResults);
		int testId = testDTO.getSelectedFirms().iterator().next().getCompanyId();
		for (Person testPerson : testResults)
		{
			assertTrue(testPerson.getLawFirm().getCompanyId() == testId);
			assertTrue((testPerson.getStdLoc().equals("New York, NY")) 
					|| (testPerson.getStdLoc().equals("Washington, DC"))
					|| (testPerson.getStdLoc().equals("London, United Kingdom")));
		}
	}
}
