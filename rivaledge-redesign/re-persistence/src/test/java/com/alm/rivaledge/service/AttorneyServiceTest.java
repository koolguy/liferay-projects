package com.alm.rivaledge.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.alm.rivaledge.BaseALMTest;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.transferobject.AttorneyResultDTO;
import com.alm.rivaledge.transferobject.AttorneySearchDTO;

/**
 * Unit test cases for Attorney Service
 * @author FL605
 * @since Sprint 3
 */
public class AttorneyServiceTest extends BaseALMTest {
	
	@Autowired
	@Qualifier("attorneyervice")
	private AttorneyService	testService;
	
	@Autowired
	@Qualifier("firmService")
	private FirmService	testServiceFirm;
	
	
	/**
	 * Tests the Attorney search Functionality
	 */
	@Test
	public void testAttorneySearch()
	{
		printTitle();
		AttorneySearchDTO  attorneySearchDTO= new AttorneySearchDTO();
		
		List<Firm> testFirms = new ArrayList<Firm>(1); 
		testFirms.add(testServiceFirm.getFirmById(31));
		
		List<String> testLocations = new ArrayList<String>(3);
		testLocations.add("London, United Kingdom");
		testLocations.add("New York, NY");
		testLocations.add("Houston, TX");
		
		// Stuff everything into the DTO
		attorneySearchDTO.setSelectedFirms(testFirms);
		attorneySearchDTO.setLocations(testLocations);
		
		List<AttorneyResultDTO> testResults = testService.getAttorneyData(attorneySearchDTO);
		assertNotNull(testResults);
		assertTrue(!testResults.isEmpty()); 
		
		List<String> attorneyNamelist= new ArrayList<String>(1);
		attorneyNamelist.add("Krivoy, Clara");
		attorneySearchDTO.setAttorneyName(attorneyNamelist);
		testResults=testService.getAttorneyData(attorneySearchDTO);
		assertNotNull(testResults);
		assertTrue(!testResults.isEmpty()); 
		
		List<String> testTitles = new ArrayList<String>(1); 
		testTitles.add("Partner");
		attorneySearchDTO.setTitles(testTitles);
		assertNotNull(testResults);
		assertTrue(!testResults.isEmpty()); 
		
		
		
	}

}
