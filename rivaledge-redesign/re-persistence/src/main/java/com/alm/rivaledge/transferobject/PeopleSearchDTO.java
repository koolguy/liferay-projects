package com.alm.rivaledge.transferobject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;

/**
 * Search POJO that will be used to transfer user selections in the web form
 * down to the service layer, which will then translate them into appropriate
 * DAO calls.
 * 
 * @author FL867
 * @since Sprint 2
 * 
 */
public class PeopleSearchDTO extends BaseSearchDTO implements Serializable
{
	private static final long	serialVersionUID	= 8161198648878053472L;

	private List<Firm>			selectedFirms;
	private Firm				myFirm;
	private Boolean				isAmLaw100Selected;
	private Boolean				isAmLaw200Selected;
	private Boolean				isNLJ250Selected;
	private List<Integer>		selectedWatchlists;
	private Date				toDate;
	private Date				fromDate;
	private List<String>		practiceArea;
	private List<String>		locations;
	private List<String>		firmSizeList;
	private List<String>		quasimodoList;
	private Integer				groupingId;
	private String				dynamicSearchColumn;
	
	private List<String> firmSizeQueryClauses;
	
	public PeopleSearchDTO()
	{
		super();
		setPageNumber(0);
		setSortColumn(3); //sorting based on firms
		setSortOrder("asc"); 
		setOrderBy(-1);  //no grouping
	}
	
	/**
	 * @return List of Firm(Companies) object which user has selected on People
	 *         search Form
	 */
	
	
	public List<Firm> getSelectedFirms()
	{
		return selectedFirms;
	}

	public void setSelectedFirms(List<Firm> selectedFirms)
	{
		this.selectedFirms = selectedFirms;
	}

	public Firm getMyFirm()
	{
		return myFirm;
	}

	public void setMyFirm(Firm myFirm)
	{
		this.myFirm = myFirm;
	}

	public Boolean getIsAmLaw100Selected()
	{
		return isAmLaw100Selected;
	}

	public void setIsAmLaw100Selected(Boolean isAmLaw100Selected)
	{
		this.isAmLaw100Selected = isAmLaw100Selected;
	}

	public Boolean getIsAmLaw200Selected()
	{
		return isAmLaw200Selected;
	}

	public void setIsAmLaw200Selected(Boolean isAmLaw200Selected)
	{
		this.isAmLaw200Selected = isAmLaw200Selected;
	}

	public Boolean getIsNLJ250Selected()
	{
		return isNLJ250Selected;
	}

	public void setIsNLJ250Selected(Boolean isNLJ250Selected)
	{
		this.isNLJ250Selected = isNLJ250Selected;
	}

	public Date getToDate()
	{
		return toDate;
	}

	public void setToDate(Date toDate)
	{
		this.toDate = toDate;
	}

	public Date getFromDate()
	{
		return fromDate;
	}

	public void setFromDate(Date fromDate)
	{
		this.fromDate = fromDate;
	}

	/**
	 * @return List of Practice Area which user has selected on People search
	 *         Form
	 */
	public List<String> getPracticeArea()
	{
		return practiceArea;
	}

	public void setPracticeArea(List<String> practiceArea)
	{
		this.practiceArea = practiceArea;
	}

	/**
	 * @return List of Locations which user has selected on People search Form
	 */
	public List<String> getLocations()
	{
		return locations;
	}

	public void setLocations(List<String> locations)
	{
		this.locations = locations;
	}

	/**
	 * @return List of Firm Size which user has selected on People search Form
	 */
	public List<String> getFirmSizeList()
	{
		return firmSizeList;
	}

	public void setFirmSizeList(List<String> firmSizeList)
	{
		this.firmSizeList = firmSizeList;
		
		prepareFirmSizeQueryClause();
	}

//	public Boolean getIsAmLaw100Selected()
//	{
//		return isAmLaw100Selected;
//	}
//
//	public void setIsAmLaw100Selected(Boolean isAmLaw100Selected)
//	{
//		this.isAmLaw100Selected = isAmLaw100Selected;
//	}
//
//	public Boolean getIsAmLaw200Selected()
//	{
//		return isAmLaw200Selected;
//	}
//
//	public void setIsAmLaw200Selected(Boolean isAmLaw200Selected)
//	{
//		this.isAmLaw200Selected = isAmLaw200Selected;
//	}
//
//	public Boolean getIsNLJ250Selected()
//	{
//		return isNLJ250Selected;
//	}
//
//	public void setIsNLJ250Selected(Boolean isNLJ250Selected)
//	{
//		this.isNLJ250Selected = isNLJ250Selected;
//	}

	/**
	 * Prepares the Greater than and BETWEEN clauses <br>
	 * This will be going into the where clause of the query used while selecting Attorneys matching the search criteria in Firm stats 
	 */
	private void prepareFirmSizeQueryClause()
	{

		if (firmSizeList == null || firmSizeList.isEmpty()) // All Firm Sizes selected
		{
			return;
		}

		// You are here because user want to refine the search results by some ranges of firm size 
		firmSizeQueryClauses = new ArrayList<String>(); 

		for (String firmSizeRange : firmSizeList)
		{
			if (firmSizeRange.equals("1500+"))
			{
				firmSizeQueryClauses.add(" > 1500"); // no upper limit
			}
			else
			{
				String[] range = firmSizeRange.split("-");
				firmSizeQueryClauses.add("BETWEEN " + range[0] + " AND " + range[1]); // we have lower and upper limit
			}
		}

	}

	public List<Integer> getSelectedWatchlists()
	{
		return selectedWatchlists;
	}

	public void setSelectedWatchlists(List<Integer> selectedWatchlists)
	{
		this.selectedWatchlists = selectedWatchlists;
	}

	public List<String> getQuasimodoList()
	{
		if ((getPracticeArea() != null) && (!getPracticeArea().isEmpty()) 
				&& (getLocations() != null) && (!getLocations().isEmpty()))
		{
			quasimodoList = new ArrayList<String>();
			quasimodoList.addAll(practiceArea);
			quasimodoList.addAll(locations);
		}
		else if ((getPracticeArea() != null) && (!getPracticeArea().isEmpty()))
		{
			quasimodoList = new ArrayList<String>();
			quasimodoList.addAll(practiceArea);
		}
		else if ((getLocations() != null) && (!getLocations().isEmpty()))
		{
			quasimodoList = new ArrayList<String>();
			quasimodoList.addAll(locations);
		}
		else 
		{
			quasimodoList = null;
		}
		return (quasimodoList);
	}

	public void setQuasimodoList(List<String> quasimodoList)
	{
		this.quasimodoList = quasimodoList;
	}

	public Integer getGroupingId()
	{
		return groupingId;
	}

	public void setGroupingId(Integer groupingId)
	{
		this.groupingId = groupingId;
	}

	public String getDynamicSearchColumn()
	{
		return dynamicSearchColumn;
	}

	public void setDynamicSearchColumn(String dynamicSearchColumn)
	{
		this.dynamicSearchColumn = dynamicSearchColumn;
	}

	public List<String> getFirmSizeQueryClauses()
	{
		return firmSizeQueryClauses;
	}

	/*public void setFirmSizeQueryClauses(List<String> firmSizeQueryClauses)
	{
		this.firmSizeQueryClauses = firmSizeQueryClauses;
	}*/
}
