package com.alm.rivaledge.util;


/**
 * This class will store firms information in a format 
 * which is required for Practice Area auto completer/ type ahead
 * 
 * @author FL605
 * @since Sprint 1
 */
public class PracticeJsonUtility
{

    private String label;

    public PracticeJsonUtility()
    {
    }

    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }
}
