package com.alm.rivaledge.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.alm.rivaledge.persistence.domain.lawma0_data.Admission;
import com.alm.rivaledge.persistence.domain.lawma0_data.City;
import com.alm.rivaledge.persistence.domain.lawma0_data.LawSchool;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.transferobject.AttorneyGraduationResultDTO;
import com.alm.rivaledge.transferobject.AttorneyMoveChangesResultDTO;
import com.alm.rivaledge.transferobject.AttorneyMoveChangesSearchDTO;
import com.alm.rivaledge.transferobject.AttorneyResultDTO;
import com.alm.rivaledge.transferobject.AttorneySearchDTO;
import com.alm.rivaledge.transferobject.AutocompleteSearchDTO;
import com.alm.rivaledge.transferobject.LocationList;


/**
 * Handles all business logic for the Person entity in ALM Rival Edge.
 * 
 * @author FL605
 * @since Sprint 3
 */
@Component
public interface AttorneyService
{

	/**
	 * Search for all the people who meet the criteria set forth in the Attorney
	 * search DTO.
	 * 
	 * @param attorneySearchDTO
	 * @return List<AttorneyResultDTO>
	 */
	public List<AttorneyResultDTO> getAttorneyData(AttorneySearchDTO attorneySearchDTO);

	public Integer getAttorneyDataCount(AttorneySearchDTO asDTO, User currentUser);

	/**
	 * This method fetches the list of Keywords to populate values in keyword Auto complete based on user input
	 * in Keywords in Bio filter
	 * 
	 * @param autocompleteSearchDTO
	 * @return List<String>
	 */
	public List<String> getKeywords(AutocompleteSearchDTO autocompleteSearchDTO);
	
	/**
	 * This method fetches the list of Law School  to populate values in Law School Auto complete based on user input
	 * @param autocompleteSearchDTO
	 * @return
	 */
	public List<String> getLawSchool(AutocompleteSearchDTO autocompleteSearchDTO);
	
	/**
	 * This method fetches the list of Admission to populate values in Admission Auto complete based on user input
	 * @param autocompleteSearchDTO
	 * @return
	 */
	public List<String> getAdmissions(AutocompleteSearchDTO autocompleteSearchDTO);

	/**
	 * Get the count of all attorney moves and changes that match the selected
	 * criteria
	 * 
	 * @param attorneyMoveChangesSearchDTO
	 * @param currentUser
	 * @return
	 */
	public int getAttorneyMoveChangesCount(AttorneyMoveChangesSearchDTO attorneyMoveChangesSearchDTO, User currentUser);

	public List<AttorneyMoveChangesResultDTO> getAttorneyMovesChanges(AttorneyMoveChangesSearchDTO attorneyMoveChangesSearchDTO, User currentUser);
	
	public List<AttorneyResultDTO> findAttorneyDataForcharts(AttorneySearchDTO attorneySearchDTO, User currentUser);
	
	public List<AttorneyGraduationResultDTO> findAttorneyDataForGradcharts(AttorneySearchDTO attorneySearchDTO);
	
	/**
	 * This method is to check whether we need to store Law School information in database or not
	 * if count is 0 Law School will be  persisted in DB(It means input Law School String not there in Auto Complete,Persisting the same would be available 
	 * next time in Auto complete when user tries to enter the same String )
	 * if count is more than 0 then  we would not store Law School information in database
	 * 	 
	 * Note : It checks the count based on userId
	 * @param lawschool
	 * @return
	 */
	public Integer countByLawSchool(String lawschool);
	
	/**
	 * This method is to check whether we need to store Admission information in database or not
	 * if count is 0 Admission will be  persisted in DB(It means input Admission String not there in Auto Complete,Persisting the same would be available 
	 * next time in Auto complete when user tries to enter the same String )
	 * if count is more than 0 then  we would not store Admission information in database	
	 * 
	 * Note : It checks the count based on userId
	 * @param admission
	 * @return
	 */
	public Integer countByAdmission(String admission);
	
	/**
	 * This method add the Law School input String to database so that it would be available next time in Auto complete
	 * 
	 * @param lawSchool
	 */
	public void addLawSchool(LawSchool lawSchool);
	
	/**
	 * This method add the Admission input String to database so that it would be available next time in Auto complete
	 * @param admission
	 */
	public void addAdmission(Admission admission);
	
	/**
	 *  Location list Which will contain Location tree details
		Every Location list object will have its title and children information
	 * @return
	 */
	public List<LocationList> fetchLocationTree(String locationList);
	
	/**
	 * This method will update Location tree json string based on previously selected  location.
	 * 
	 * 
	 * @param locationTreeJson
	 * @param selectedLocation
	 * @return
	 */
	public String updateLocationTree(String locationTreeJson, List<String> selectedLocation);
	
	
	/**
	 * @return
	 */
	public List<String> fetchCitiesForAutocomplete();

}
