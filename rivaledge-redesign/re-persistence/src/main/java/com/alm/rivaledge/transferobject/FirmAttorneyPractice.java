package com.alm.rivaledge.transferobject;

import java.io.Serializable;

public class FirmAttorneyPractice implements Serializable
{
	private static final long	serialVersionUID	= -2204131261175969505L;
	private Integer				id;
	private Integer				firmId;
	private String				firmName;
	private String				firmLink;
	private String				title;
	private String				practices;
	private Integer				attorneyId;

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Integer getFirmId()
	{
		return firmId;
	}

	public void setFirmId(Integer firmId)
	{
		this.firmId = firmId;
	}

	public String getFirmName()
	{
		return firmName;
	}

	public void setFirmName(String firmName)
	{
		this.firmName = firmName;
	}

	public String getFirmLink()
	{
		return firmLink;
	}

	public void setFirmLink(String firmLink)
	{
		this.firmLink = firmLink;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getPractices()
	{
		return practices;
	}

	public void setPractices(String practices)
	{
		this.practices = practices;
	}

	public Integer getAttorneyId()
	{
		return attorneyId;
	}

	public void setAttorneyId(Integer attorneyId)
	{
		this.attorneyId = attorneyId;
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append((firmId != null) ? firmId : "\\N");
		builder.append("||");
		builder.append((firmName != null) ? firmName : "\\N");
		builder.append("||");
		builder.append((firmLink != null) ? firmLink : "\\N");
		builder.append("||");
		builder.append((title != null) ? title : "\\N");
		builder.append("||");
		builder.append((practices != null) ? practices : "\\N");
		builder.append("||");
		builder.append((attorneyId != null) ? attorneyId : "\\N");
		builder.append("$$");
		return builder.toString();
	}
}
