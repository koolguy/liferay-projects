package com.alm.rivaledge.persistence.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alm.rivaledge.persistence.dao.lawma0_data.FirmDataMapper;
import com.alm.rivaledge.persistence.dao.lawma0_data.FirmLocationMapper;
import com.alm.rivaledge.persistence.dao.lawma0_data.FirmMapper;
import com.alm.rivaledge.persistence.dao.lawma0_data.FirmPracticeMapper;
import com.alm.rivaledge.persistence.dao.lawma0_data.PracticeMapper;
import com.alm.rivaledge.persistence.dao.lawma0_data.UserGroupCompanyMapper;
import com.alm.rivaledge.persistence.dao.lawma0_data.UserGroupMapper;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.FirmExample;
import com.alm.rivaledge.persistence.domain.lawma0_data.FirmLocation;
import com.alm.rivaledge.persistence.domain.lawma0_data.FirmLocationExample;
import com.alm.rivaledge.persistence.domain.lawma0_data.FirmPractice;
import com.alm.rivaledge.persistence.domain.lawma0_data.FirmPracticeExample;
import com.alm.rivaledge.persistence.domain.lawma0_data.Practice;
import com.alm.rivaledge.persistence.domain.lawma0_data.PracticeExample;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroupCompany;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroupCompanyExample;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroupExample;
import com.alm.rivaledge.transferobject.UserGroupCompanyDTO;
import com.alm.rivaledge.transferobject.UserGroupDTO;
import com.alm.rivaledge.transferobject.WatchListFirmResultDTO;
import com.alm.rivaledge.transferobject.WatchlistCriteriaSearchDTO;

/**
 * Data Access Object that handles all persistence for the Firm domain object.
 * 
 * @author FL867
 * @since Sprint 1
 */
@Repository("firmDAO")
public class FirmDAO
{
	@Autowired
	private FirmMapper		firmMapper;

	@Autowired
	private PracticeMapper	practiceMapper;

	@Autowired
	private FirmDataMapper	firmDataMapper;
	
	@Autowired
	private FirmLocationMapper firmLocationMapper;
	
	@Autowired
	private FirmPracticeMapper firmPracticeMapper;
	
	@Autowired
	private UserGroupMapper userGroupMapper;

	@Autowired
	private UserGroupCompanyMapper userGroupCompanyMapper;

	/**
	 * Returns the Firm corresponding to this primary key.
	 * 
	 * @param pk
	 *            Primary key
	 * @return Firm
	 */
	public Firm findByPK(Integer pk)
	{
		// Execute the query and return the result set
		return (firmMapper.selectByPrimaryKey(pk));
	}

	/**
	 * Returns a collection of all the Firms that match the specified name. Note
	 * that this method will return only EXACT matches. Setting "DLA" as the
	 * search parameter will NOT return the firm "DLA Piper".
	 * 
	 * @param firmName
	 *            Name of the firm to search by
	 * @return List<Firm>
	 */
	public List<Firm> findByName(String firmName)
	{
		// Instantiate the FirmExample
		FirmExample fEx = new FirmExample();

		// Set the criteria
		fEx.or().andCompanyEqualTo(firmName).andAreaIdEqualTo(1).andCompanyTypeEqualTo("C");

		// Return the result set
		return (firmMapper.selectByExample(fEx));
	}

	/**
	 * Returns a collection of all the Firms that match the specified name. Note
	 * that this method will return all Firms that even partially match. Setting
	 * "Man" as the search parameter will return both "Mannat Phelps" and
	 * "Manning Fulton".
	 * 
	 * @param partialFirmName
	 *            Name of the firm to search by
	 * @return List<Firm>
	 */
	public List<Firm> findByMatchingName(String partialFirmName)
	{
		// Instantiate the FirmExample
		FirmExample fEx = new FirmExample();

		// Set the criteria
		fEx.or().andCompanyLike("%" + partialFirmName + "%").andAreaIdEqualTo(1).andCompanyTypeEqualTo("C");
		
		// Execute and return the result set
		return (firmMapper.selectByExample(fEx));
	}

	/**
	 * Returns a set of all the firms in the database.
	 * 
	 * @return List<Firm>
	 */
	public List<Firm> findAllFirms()
	{
		// Instantiate the FirmExample
		FirmExample fEx = new FirmExample();
		fEx.or().andAreaIdEqualTo(1).andCompanyTypeEqualTo("C");
		fEx.setOrderByClause("company");

		// Get the result set
		return (firmMapper.selectByExample(fEx));
	}

	/**
	 * Returns a list of all the firms that comprise the AmLaw100. Note that
	 * unlike many of the other findBy methods in the FirmDAO this method
	 * returns a list and not a set. This is because the sorting order for this
	 * collection is determined by the AmLaw100 ranking and not by the natural
	 * sorting order implemented in the Firm domain object's compareTo().
	 * 
	 * @return List<Firm>
	 */
	public List<Firm> findAmLaw100()
	{
		// Instantiate the FirmExample
		FirmExample fEx = new FirmExample();

		// Set the criteria and sort order of the result set
		fEx.or().andAmlawRankBetween(1, 100).andAreaIdEqualTo(1).andCompanyTypeEqualTo("C");
		fEx.setOrderByClause("amlaw_rank asc");

		// Get the result set and return it
		return (firmMapper.selectByExample(fEx));
	}

	/**
	 * Returns a list of all the firms that comprise the AmLaw200. Note that
	 * unlike many of the other findBy methods in the FirmDAO this method
	 * returns a list and not a set. This is because the sorting order for this
	 * collection is determined by the AmLaw200 ranking and not by the natural
	 * sorting order implemented in the Firm domain object's compareTo().
	 * 
	 * @return List<Firm>
	 */
	public List<Firm> findAmLaw200()
	{
		// Instantiate the FirmExample
		FirmExample fEx = new FirmExample();

		// Set the criteria and sort order of the result set
		fEx.or().andAmlawRankBetween(1, 200).andAreaIdEqualTo(1).andCompanyTypeEqualTo("C");
		fEx.setOrderByClause("amlaw_rank asc");

		// Get the result set and return it
		return (firmMapper.selectByExample(fEx));
	}

	/**
	 *
	 * Returns a list of all the firms that comprise the NLJ350. Note that
	 * unlike many of the other findBy methods in the FirmDAO this method
	 * returns a list and not a set. This is because the sorting order for this
	 * collection is determined by the NLJ350 ranking and not by the natural
	 * sorting order implemented in the Firm domain object's compareTo().
	 * 
	 * Note: This method will now return firms that comprise the NLJ350 ranking rather than NLJ250
	 * 
	 * @return List<Firm>
	 */
	public List<Firm> findNLJ250()
	{
		// Instantiate the FirmExample
		FirmExample fEx = new FirmExample();

		// Set the criteria and sort order of the result set
		fEx.or().andNljRankBetween(1, 350).andAreaIdEqualTo(1).andCompanyTypeEqualTo("C");
		fEx.setOrderByClause("nlj_rank asc");

		// Get the result set and return it
		return (firmMapper.selectByExample(fEx));
	}

	/**
	 * Returns a set of all the practices that Firms engage in.
	 * 
	 * @return Set<Firm>
	 */
	public List<Practice> findPractices()
	{
		// Instantiate the PracticeExample
		PracticeExample pEx = new PracticeExample();

		// Set the criteria and be sure to apply DISTINCT
		pEx.setDistinct(true);
		pEx.or().andPracticeAreaIsNotNull();
		pEx.setOrderByClause("practice_area");

		// Get the result set and cast to Set
		return (practiceMapper.selectByExample(pEx));
	}

	/**
	 * Returns a list of practices that match the given keyword. This will
	 * return even partial matches.
	 * 
	 * @param String
	 *            keyword to search by.
	 * @return List<Practice>
	 */
	public List<Practice> findMatchingPractices(String practiceKeyword)
	{
		// Instantiate the FirmExample
		PracticeExample pEx = new PracticeExample();

		// Set the criteria
		pEx.or().andPracticeAreaLike("%" + practiceKeyword + "%");
		pEx.setDistinct(true);
		pEx.setOrderByClause("practice_area");

		// Get the result set
		return (practiceMapper.selectByExample(pEx));
	}

	public Set<FirmLocation> findAllLocationsInTheUSA()
	{
		// Instantiate the FirmLocationExample
		FirmLocationExample flEx = new FirmLocationExample();

		// Set the criteria
		flEx.or().andCountryCodeEqualTo(1);
		flEx.setOrderByClause("std_loc");

		return (null);
	}

	/**
	 * Given a specific user, finds the last 100 keywords that this user
	 * searched for.
	 * 
	 * @param currentUser
	 * @return List<String>
	 */
	public List<String> findUserKeywords(User currentUser)
	{
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Given a firm id, find all the locations where this firm has offices
	 * 
	 * @param firmId
	 * @return List<FirmLocation>
	 */
	public List<FirmLocation> findLocationsByFirm(Integer firmId)
	{
		// Instantiate the FirmLocationExample
		FirmLocationExample flEx = new FirmLocationExample();

		// Set the criteria
		flEx.or().andCompanyIdEqualTo(firmId);
		flEx.setOrderByClause("std_loc");
		flEx.setDistinct(true);

		// Execute and return
		return (firmLocationMapper.selectByExample(flEx));
	}
	
	/**
	 * Given a firm id, find all the practices this firm handles
	 * @param firmId
	 * @return
	 */
	public List<FirmPractice> findPracticesByFirm(Integer firmId)
	{
		// Instantiate the FirmPracticeExample
		FirmPracticeExample fpEx = new FirmPracticeExample();
		
		// Set the criteria
		fpEx.or().andCompanyIdEqualTo(firmId);
		fpEx.setOrderByClause("std_practice");
		fpEx.setDistinct(true);
		
		// Execute and return
		return (firmPracticeMapper.selectByExample(fpEx));
	}
	
	
	/**
	 * Would return all filtered firms information based on user search criteria 
	 * @param watchlistCriteriaSearchDTO
	 * @return
	 */
	public List<WatchListFirmResultDTO> findWatchlistFirms(WatchlistCriteriaSearchDTO watchlistCriteriaSearchDTO)
	{
		//WatchlistCriteriaSearchDTO watchlistCriteriaSearchDTO=new WatchlistCriteriaSearchDTO();
		//watchlistCriteriaSearchDTO.setFirmFilterChar(firmFirstChar);	
		return (firmMapper.selectWatchlistFirms(massageWatchlistCriteriaSearchDTO(watchlistCriteriaSearchDTO)));
	}
	
	/**
	 * This method is used for adding new Watchlist
	 * @param ugDTO usergroup DTO object that contain watchlist's property value
	 */
	/**
	 * @param ugDTO
	 */
	public void addMyWatchList(UserGroupDTO ugDTO)
	{
		
		UserGroupExample example =new UserGroupExample();
		example.createCriteria().andIdEqualTo(ugDTO.getId());
		int count=userGroupMapper.countByExample(example);		
		if(count==0)
		{
			ugDTO.setFlgDefault("Y");
		}else
		{
			ugDTO.setFlgDefault("N");
		}
		userGroupMapper.addMyWatchList(ugDTO);
	}
	
	/**
	 * This method is used for updating existing watchlist
	 * @param ugDTO usergroup DTO object that contain watchlist's updated property value
	 */
	public void updateMyWatchList(UserGroupDTO ugDTO)
	{
		userGroupMapper.updateMyWatchList(ugDTO);
	}
	
	/**
	 * This method is used for deletion of existing watchlist
	 * @param ugDTO usergroup DTO object that contain watchlist value which will be delete
	 */
	public void deleteMyWatchList(UserGroupDTO ugDTO)
	{
		userGroupMapper.deleteMyWatchList(ugDTO);
	}
	
	/**
	 * Get all firm watchlists for a user
	 * @return firm list 
	 */
	
	public List<UserGroupDTO> getAllFirmWatchList(Integer userId) 
	{
		return userGroupMapper.getAllFirmWatchList(userId);
	}
	
	/**
	 * Save firm for a watchlist
	 * @param userGroupCompany used to store firm value
	 * @return 
	 */
	public int saveFirmForWatchList(UserGroupCompany userGroupCompany) 
	{
		return userGroupCompanyMapper.insert(userGroupCompany);
	}
	
	/**
	 * Get groupId for a watchlist
	 * @param userGroupDTO store watchList name and logged user id
	 * @return
	 */
	public Integer getWatchList(UserGroupDTO userGroupDTO) 
	{
		return userGroupMapper.getWatchList(userGroupDTO);
	}
	
	/**
	 * Return all Firms associated with a watchlist
	 * @param userGroupCompany to check associated firms with watchlist
	 * @return
	 */
	public List<UserGroupCompanyDTO> getAddedFirms(UserGroupCompany userGroupCompany) 
	{
		return userGroupCompanyMapper.getAddedFirms(userGroupCompany);
	}
	
	/**
	 * remove firms for a watchlist
	 * @param userGroupCompany used to store firm value
	 * @return 
	 */
	public void removeFirmsFromWatchList(UserGroupCompany userGroupCompany) 
	{
		userGroupCompanyMapper.removeFirmsFromWatchList(userGroupCompany);
	}
	
	/**
	 * This method is used to get all organization list
	 * @return
	 */
	public List<Firm> getOrganizationsList()
	{
		return firmMapper.selectOrganizationsList();
	}
	
	/**
	 * This method is used for getting firm watchlists sorted by Date added
	 * @return UserGroup 
	 */
	public List<UserGroup> getWatchListSortedByDate(Integer userId)
	{
		return userGroupMapper.getWatchListSortedByDate(userId);
	}
	
	public List<Firm> findFirmsByWatchList(List<Integer> watchListIds,Integer userId)
	{
		// Set up the return list
		List<Firm> returnList = new ArrayList<Firm>();
		
		// Set up the usergroupcompany mapping
		UserGroupCompanyExample ugcEx = new UserGroupCompanyExample();
		ugcEx.setDistinct(true);
		ugcEx.setOrderByClause("company_id asc");
		
		// Set the criteria
		ugcEx.or().andGroupIdIn(watchListIds).andIdEqualTo(userId);;
		
		//Execute the query
		List<UserGroupCompany> ugcList = userGroupCompanyMapper.selectByExample(ugcEx);
		
		if ((ugcList != null) && (!ugcList.isEmpty()))
		{
			for (UserGroupCompany thisUGC : ugcList)
			{
				returnList.add(findByPK(thisUGC.getCompanyId()));
			}
		}
		
		// Return the list
		return (returnList);
	}
	
	
	
	public UserGroup findWatchListById(Integer watchListId, Integer userId)
	{
		// Set up the usergroup mapping
		UserGroupExample ugEx = new UserGroupExample();
		
		// Set the criteria
		ugEx.createCriteria().andGroupIdEqualTo(watchListId).andIdEqualTo(userId);
		
		//Execute the query
		List<UserGroup> ugList = userGroupMapper.selectByExample(ugEx);
		UserGroup userGroup = null;
		
		if ((ugList != null) && (!ugList.isEmpty()))
		{
			userGroup = ugList.get(0);
		}
		return userGroup;
	}
	
	/** 
	 * Set default watchlist of User by setting flg_default to "Y" in user_groups table
	 * @param ugDTO
	 */
	public void setDefaultWatchList(UserGroupDTO ugDTO)
	{
		userGroupMapper.setDefaultWatchList(ugDTO);		
	}
	
	
	/**
	 * This method check whether watchlist is default or not before deleting it
	 * @param ugDTO
	 * @return
	 */
	public UserGroup checkDefaultWatchList(UserGroupDTO ugDTO)
	{
		return userGroupMapper.checkDefaultWatchList(ugDTO);	
	}
	
	/**
	 * This method gets invoked when user delete default watchlist
	 * After deletion of default watchlist this method set the default watchlist of the user( watchlist which is having  is lowest groupid)
	 * @param ugDTO
	 */
	public void deleteDefaultMyWatchList(UserGroupDTO ugDTO)
	{
		 userGroupMapper.deleteMyWatchList(ugDTO);		 
		 userGroupMapper.setDefaultMyWatchListAfterDelete(ugDTO);
	}	
	
	
	private WatchlistCriteriaSearchDTO massageWatchlistCriteriaSearchDTO(WatchlistCriteriaSearchDTO searchDTO)
	{
		WatchlistCriteriaSearchDTO watchlistCriteriaSearchDTO = null;
		try
		{
			watchlistCriteriaSearchDTO = (WatchlistCriteriaSearchDTO)searchDTO.clone();
		}
		catch (CloneNotSupportedException cnsEx)
		{
			watchlistCriteriaSearchDTO = searchDTO;
		}
	

		// Prefix practices with a %
		if ((watchlistCriteriaSearchDTO.getPracticeArea() != null) && (!watchlistCriteriaSearchDTO.getPracticeArea().isEmpty()))
		{
			List<String> wildcardedPracticeAreas = new ArrayList<String>(watchlistCriteriaSearchDTO.getPracticeArea().size());
			for (String practiceArea : watchlistCriteriaSearchDTO.getPracticeArea())
			{
				wildcardedPracticeAreas.add("%" + practiceArea + "%");
			}
			watchlistCriteriaSearchDTO.setPracticeArea(wildcardedPracticeAreas);
		}

		
		return (watchlistCriteriaSearchDTO);
	}
	
	/**
	 * This method is used for getting all watch lists for a user
	 * @param userid
	 * @return watchlists 
	 */
	public List<UserGroup> getAllWatchListForAutoComplete(Integer userId)
	{
		return userGroupMapper.getAllWatchListForAutoComplete(userId);
	}
	
	

	/**
	 * Returns a Firm Object that match the input String
	 * @param firmName
	 * @return
	 */
	public Firm findByFirmName(String firmName)
	{
		
		FirmExample fEx = new FirmExample();		
		fEx.or().andCompanyEqualTo(firmName).andAreaIdEqualTo(1).andCompanyTypeEqualTo("C");		
		return (firmMapper.selectFirmByName(fEx));
	}
}
