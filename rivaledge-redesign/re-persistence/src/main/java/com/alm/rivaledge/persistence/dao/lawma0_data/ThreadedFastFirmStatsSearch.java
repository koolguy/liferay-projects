package com.alm.rivaledge.persistence.dao.lawma0_data;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import com.alm.rivaledge.transferobject.PeopleResult;
import com.alm.rivaledge.transferobject.PeopleResultDTO;
import com.alm.rivaledge.transferobject.PeopleSearchDTO;

public class ThreadedFastFirmStatsSearch implements Callable<PeopleResultDTO>
{
	private CacheMapper 	cacheMapper;
	private PeopleSearchDTO psDTO;
	private PeopleResultDTO	resultDTO;
	private Boolean			isDrilldown;
	
	public ThreadedFastFirmStatsSearch(PeopleResultDTO prDTO, PeopleSearchDTO searchDTO, CacheMapper searchMapper, Boolean isDrill)
	{
		super();
		this.resultDTO	 = prDTO;
		this.psDTO  	 = searchDTO;
		this.cacheMapper = searchMapper;
		this.isDrilldown = isDrill;
	}

	@Override
	public PeopleResultDTO call() throws Exception
	{
		psDTO.setDynamicSearchColumn("firm_name");
		List<PeopleResult> movementResults = cacheMapper.findFirmStatisticsMovements(psDTO);
		if ((movementResults != null) && (!movementResults.isEmpty()))
		{
			PeopleResult companyPeople = resultDTO.getCompanyPeople();
			companyPeople.addMovementCounts(movementResults.get(0));
			resultDTO.setCompanyPeople(companyPeople);
			
			if (isDrilldown)
			{
				List<String> selectedLocations = psDTO.getLocations();
				List<String> selectedPractices = psDTO.getPracticeArea();
				
				if (selectedLocations != null)
				{
					psDTO.setLocations(null);
					psDTO.setPracticeArea(null);
					psDTO.setDynamicSearchColumn("location");
					for (String thisLocation : selectedLocations)
					{
						List<String> locationList = new ArrayList<String>(1);
						locationList.add(thisLocation);
						psDTO.setLocations(locationList);
						List<PeopleResult> locationResult = cacheMapper.findFirmStatisticsMovements(psDTO);
						if ((locationResult != null) && (!locationResult.isEmpty()))
						{
							resultDTO.addLocationMovements(locationResult.get(0));
						}
					}
				}
				if (selectedPractices != null)
				{
					psDTO.setLocations(null);
					psDTO.setPracticeArea(null);
					psDTO.setDynamicSearchColumn("practices");
					for (String thisPractice : selectedPractices)
					{
						List<String> practiceList = new ArrayList<String>(1);
						practiceList.add(thisPractice);
						psDTO.setPracticeArea(practiceList);
						List<PeopleResult> practiceResult = cacheMapper.findFirmStatisticsMovements(psDTO);
						if ((practiceResult != null) && (!practiceResult.isEmpty()))
						{
							resultDTO.addPracticeMovements(practiceResult.get(0));
						}
					}
				}
			}
		}
		return (resultDTO);
	}	
}
