package com.alm.rivaledge.transferobject;

import java.io.Serializable;

/**
 * The base class for all Search DTOs. It provides common functionality, such as
 * pagination support and sorting support.
 * 
 * @author FL867
 * @since Sprint 3
 * @version 1.0
 */
public abstract class BaseSearchDTO implements Serializable, Cloneable
{
	private static final long serialVersionUID = 2033150317220759133L;
	
	private int		pageNumber;
	private int		sortColumn;
	private String	sortOrder;
	private int		maxResultSizeCap;
	/**
	 * This variable holds the value of Group By criteria in View Settings.<br>
	 * orderBy value goes into the <code>order by</code> clause of the query
	 * value of -1 denotes no grouping
	 */
	private int orderBy;
	
	/**
	 * This sets the <code>limit</code> on the <code>ResulSet</code> of the query
	 */
	private int resultPerPage = 100; // the default limit is 100 
	
	public BaseSearchDTO()
	{
		super();
		setPageNumber(0);
		setSortColumn(2);
		setSortOrder("desc");
		setOrderBy(-1);  //default value will be set by respective subclasses
	}

	/**
	 * The pageNumber that is sought by the user. Refers to the specific page
	 * out of the total set of pages.
	 * 
	 * @return int
	 */
	public int getPageNumber()
	{
		return pageNumber;
	}

	/**
	 * Specifies the page number that is to be fetched.
	 * 
	 * @param pageNumber
	 */
	public void setPageNumber(int pageNumber)
	{
		/*if (pageNumber < 1)
		{
			pageNumber = 1;
		}
		this.pageNumber = (pageNumber - 1) * 100;*/
		//don't perform the above calculation in this method, as the pageSize is a value from user settings and no more a constant
		this.pageNumber = pageNumber;
	}

	/**
	 * Gets the column that the results should be sorted by. In a DTO with five
	 * columns the range of values for this field would be either 1, 2, 3, 4, or
	 * 5.
	 * 
	 * @return
	 */
	public int getSortColumn()
	{
		return sortColumn;
	}

	/**
	 * Sets the column that the results should be sorted by. In a DTO with five
	 * columns the range of acceptable values for this field would be either 1,
	 * 2, 3, 4, or 5.
	 * 
	 * @param sortColumn
	 */
	public void setSortColumn(int sortColumn)
	{
		this.sortColumn = sortColumn;
	}

	/**
	 * Determines whether this will be an ASCending or DESCending sort.
	 * 
	 * @return
	 */
	public String getSortOrder()
	{
		return sortOrder;
	}

	/**
	 * Set to ASC for ascending sort and to DESC for descending sort.
	 * 
	 * @param isDescendingOrder
	 */
	public void setSortOrder(String sortOrder)
	{
		this.sortOrder = sortOrder;
	}

	public String getSortCriteria()
	{
		return (sortColumn + " " + sortOrder);
	}

	public void setOrderBy(int orderBy)
	{
		this.orderBy = orderBy;
	}
	
	public int getOrderBy()
	{
		return orderBy;
	}

	public int getResultPerPage()
	{
		return resultPerPage;
	}

	public void setResultPerPage(int resultPerPage)
	{
		this.resultPerPage = resultPerPage;
	}
	
	public int getMaxResultSizeCap()
	{
		return maxResultSizeCap;
	}

	public void setMaxResultSizeCap(int maxResultSizeCap)
	{
		this.maxResultSizeCap = maxResultSizeCap;
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}
}
