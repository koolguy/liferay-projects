package com.alm.rivaledge.transferobject;

import java.io.Serializable;

/**
 * Search POJO that will  transfer user input(Keyword,lawSchool,
 * admissions) to service layer to populate Auto complete values.
 * @author FL605
 *
 */
public class AutocompleteSearchDTO  implements Serializable
{
	
	
	private static final long serialVersionUID = 6035623223553734391L;
	
	private 	String 	 keyword;
	private 	String 	 lawSchool;
	private  	String   admissions;
	private  	Integer  userId;
	
	public String getKeyword() 
	{
		return keyword;
	}
	public void setKeyword(String keyword)
	{
		this.keyword = keyword;
	}
	public String getLawSchool() 
	{
		return lawSchool;
	}
	public void setLawSchool(String lawSchool) 
	{
		this.lawSchool = lawSchool;
	}
	public String getAdmissions() 
	{
		return admissions;
	}
	public void setAdmissions(String admissions) 
	{
		this.admissions = admissions;
	}	
	public Integer getUserId() 
	{
		return userId;
	}
	public void setUserId(Integer userId) 
	{
		this.userId = userId;
	}
	
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((admissions == null) ? 0 : admissions.hashCode());
		result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
		result = prime * result
				+ ((lawSchool == null) ? 0 : lawSchool.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AutocompleteSearchDTO other = (AutocompleteSearchDTO) obj;
		if (admissions == null) 
		{
			if (other.admissions != null)
				return false;
		} else if (!admissions.equals(other.admissions))
			return false;
		if (keyword == null) 
		{
			if (other.keyword != null)
				return false;
		} else if (!keyword.equals(other.keyword))
			return false;
		if (lawSchool == null) 
		{
			if (other.lawSchool != null)
				return false;
		} else if (!lawSchool.equals(other.lawSchool))
			return false;
		return true;
	}
	
	
	@Override
	public String toString() 
	{
		return "AutocompleteSearchDTO [keyword=" + keyword + " ||  lawSchool="
				+ lawSchool + " ||  admissions=" + admissions + " ||  userId="
				+ userId + "]";
	}
	
	
}
