package com.alm.rivaledge.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alm.rivaledge.persistence.dao.FirmDAO;
import com.alm.rivaledge.persistence.dao.PersonDAO;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroupCompany;
import com.alm.rivaledge.service.WatchlistService;
import com.alm.rivaledge.transferobject.UserGroupCompanyDTO;
import com.alm.rivaledge.transferobject.UserGroupDTO;
import com.alm.rivaledge.transferobject.WatchListFirmResultDTO;
import com.alm.rivaledge.transferobject.WatchlistCriteriaSearchDTO;

@Service("watchlistService")
public class WatchlistServiceImpl implements WatchlistService
{
	@Autowired
	private FirmDAO firmDAO;
	
	@Autowired
	private PersonDAO personDAO;
	
	/**
	 * Returns all the candidate firms that can be added to this user's watch
	 * list.
	 * 
	 * @param currentUser
	 * @return
	 */
	@Override
	public List<WatchListFirmResultDTO> getFirmsForWatchlist(User currentUser, String firmFirstChar)
	{
		// We need a valid, non-null User before we can run anything.
		if (currentUser == null)
		{
			return (null);
		}
		WatchlistCriteriaSearchDTO  watchlistCriteriaSearchDTO =new WatchlistCriteriaSearchDTO();
		watchlistCriteriaSearchDTO.setFirmFilterChar(firmFirstChar);
		// We have a non-null user
		return (firmDAO.findWatchlistFirms(watchlistCriteriaSearchDTO));
	}
	
	/**
	 * This method is used for adding new Watchlist
	 * @param ugDTO usergroup DTO object that contain watchlist's property value
	 */
	public void addMyWatchList(UserGroupDTO ugDTO)
	{
		firmDAO.addMyWatchList(ugDTO);
	}
	
	/**
	 * This method is used for updating existing watchlist
	 * @param ugDTO usergroup DTO object that contain watchlist's updated property value
	 */
	public void updateMyWatchList(UserGroupDTO ugDTO)
	{
		firmDAO.updateMyWatchList(ugDTO);
	}
	
	/**
	 * This method is used for deletion of existing watchlist
	 * @param ugDTO usergroup DTO object that contain watchlist value which will be delete
	 */
	public void deleteMyWatchList(UserGroupDTO ugDTO){
	
		UserGroup userGroup = checkDefaultWatchList(ugDTO);
	
		if(userGroup.getFlgDefault()!=null && userGroup.getFlgDefault().equals("Y"))
		{	
			//Deleting default watchlist
			firmDAO.deleteDefaultMyWatchList(ugDTO);
		} 
		else
		{   
			firmDAO.deleteMyWatchList(ugDTO);
		}
		
		
	}
	
	/**
	 * This method is used for getting all firm watchlists for a user
	 * @return UserGroupDTO list 
	 */
	public List<UserGroupDTO> getAllFirmWatchList(Integer userId) 
	{
		return firmDAO.getAllFirmWatchList(userId);
	}
	
	/**
	 * This method is used for returning watch list's id based on watchlistname
	 * @param watchListFirmName
	 * @return get Watchlist id
	 */
	public Integer getWatchList(UserGroupDTO userGroupDTO) 
	{
		return firmDAO.getWatchList(userGroupDTO);
	}
	
	/**
	 * This method is used for saving firm into a watchlist
	 * @param userGroupCompany to check associated firms with watchlist
	 * @return List<UserGroupCompanyDTO>
	 */
	public List<UserGroupCompanyDTO> getAddedFirms(UserGroupCompany userGroupCompany) 
	{
		return firmDAO.getAddedFirms(userGroupCompany);
	}
	
	/**
	 * This method is used for returning all Firms associated with a watchlist
	 * @param userGroupCompany used to store firm value
	 * @return added firm id value
	 */
	public int saveFirmForWatchList(UserGroupCompany userGroupCompany) 
	{
		return firmDAO.saveFirmForWatchList(userGroupCompany);
	}
	
	/**
	 * This method is used for removing firms for a watchlist
	 * @param userGroupCompany used to store firm value associate with watchlist
	 * @return 
	 */
	public void removeFirmsFromWatchList(UserGroupCompany userGroupCompany)
	{
		firmDAO.removeFirmsFromWatchList(userGroupCompany);
	}

	@Override
	public List<Firm> findAllFirmsInOneWatchlist(Integer watchListId,Integer userId)
	{
		List<Integer> watchList = new ArrayList<Integer>(1);
		watchList.add(watchListId);
		return (findAllFirmsInManyWatchlists(watchList,userId));
	}

	@Override
	public List<Firm> findAllFirmsInManyWatchlists(List<Integer> allWatchlists,Integer userId)
	{
		return (firmDAO.findFirmsByWatchList(allWatchlists,userId));
	}
	
	public UserGroup getWatchList(Integer watchListId, Integer userId)
	{
		return (firmDAO.findWatchListById(watchListId, userId));
	}
	
	@Override
	public void setDefaultWatchList(UserGroupDTO ugDTO)
	{
		firmDAO.setDefaultWatchList(ugDTO);
	}
	
	
	/** This method check whether watchlist is default or not before deleting it
	 * @param ugDTO
	 * @return
	 */
	public UserGroup checkDefaultWatchList(UserGroupDTO ugDTO)
	{
		return firmDAO.checkDefaultWatchList(ugDTO);
	}

	
	@Override
	public List<WatchListFirmResultDTO>   getFilteredFirms(WatchlistCriteriaSearchDTO watchlistCriteriaSearchDTO)
	{
		
		List<WatchListFirmResultDTO> allSearchResults = firmDAO.findWatchlistFirms(watchlistCriteriaSearchDTO);
		// Now we will check for and apply the firm size filter
					if ((watchlistCriteriaSearchDTO.getFirmSize() != null) && (!watchlistCriteriaSearchDTO.getFirmSize().isEmpty()))
					{
						// Customer wants to filter by firm size
						// We will go at it in descending order
						List<WatchListFirmResultDTO> targetsForDeletion = new ArrayList<WatchListFirmResultDTO>();
						
						for (WatchListFirmResultDTO thisDTO : allSearchResults)
						{
							int firmSize = thisDTO.getAttorneyCount();
							
							boolean isValidFirmSize = false;
							
							for (String firmSizeRange : watchlistCriteriaSearchDTO.getFirmSize())
							{
								if (firmSizeRange.equals("1500+"))
								{
									if (firmSize >= 1500)
									{
										isValidFirmSize = true;
									}
								}
								if ((!isValidFirmSize) && (firmSizeRange.equals("1000-1499")))
								{
									if (firmSize >= 1000 && firmSize <= 1499)
									{
										isValidFirmSize = true;
									}
								}
								if ((!isValidFirmSize) && (firmSizeRange.equals("750-999")))
								{
									if (firmSize >= 750 && firmSize <= 999)
									{
										isValidFirmSize = true;
									}
								}
								if ((!isValidFirmSize) && (firmSizeRange.equals("500-749")))
								{
									if (firmSize >= 500 && firmSize <= 749)
									{
										isValidFirmSize = true;
									}
								}
								if ((!isValidFirmSize) && (firmSizeRange.equals("400-499")))
								{
									if (firmSize >= 400 && firmSize <= 499)
									{
										isValidFirmSize = true;
									}
								}
								if ((!isValidFirmSize) && (firmSizeRange.equals("300-399")))
								{
									if (firmSize >= 300 && firmSize <= 399)
									{
										isValidFirmSize = true;
									}
								}
								if ((!isValidFirmSize) && (firmSizeRange.equals("200-299")))
								{
									if (firmSize >= 200 && firmSize <= 299)
									{
										isValidFirmSize = true;
									}
								}
								if ((!isValidFirmSize) && (firmSizeRange.equals("100-199")))
								{
									if (firmSize >= 100 && firmSize <= 199)
									{
										isValidFirmSize = true;
									}
								}
								if ((!isValidFirmSize) && (firmSizeRange.equals("50-99")))
								{
									if (firmSize >= 50 && firmSize <= 99)
									{
										isValidFirmSize = true;
									}
								}
								if ((!isValidFirmSize) && (firmSizeRange.equals("25-49")))
								{
									if (firmSize >= 25 && firmSize <= 49)
									{
										isValidFirmSize = true;
									}
								}
								if ((!isValidFirmSize) && (firmSizeRange.equals("1-24")))
								{
									if (firmSize >= 1 && firmSize <= 24)
									{
										isValidFirmSize = true;
									}
								}
							}
							
							// If this is not within the range of firm sizes
							// specified by the user, it will have to be marked
							// for deletion from the preReturnMap.
							if (!isValidFirmSize)
							{
								targetsForDeletion.add(thisDTO);
							}
						}
						
						// Delete marked results
						for (WatchListFirmResultDTO targetDTO : targetsForDeletion)
						{
							allSearchResults.remove(targetDTO);
						}
						
						// We don't need targetsForDeletion any more
						targetsForDeletion = null;
					}
		
		return allSearchResults;
	}
	
	/**
	 * This method is used for getting all watch lists for a user
	 * @param userid
	 * @return watchlists 
	 */
	public List<UserGroup> getAllWatchListForAutoComplete(Integer userId)
	{
		return firmDAO.getAllWatchListForAutoComplete(userId);
	}
}