package com.alm.rivaledge.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alm.rivaledge.persistence.dao.CacheDAO;
import com.alm.rivaledge.persistence.dao.FirmDAO;
import com.alm.rivaledge.persistence.dao.PersonDAO;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.service.PeopleService;
import com.alm.rivaledge.transferobject.PeopleResultDTO;
import com.alm.rivaledge.transferobject.PeopleSearchDTO;
import com.alm.rivaledge.util.ALMConstants;

@Service("peopleService")
public class PeopleServiceImpl implements PeopleService
{
	@Autowired
	private CacheDAO	cacheDAO;

	@Autowired
	private PersonDAO	personDAO;

	@Autowired
	private FirmDAO		firmDAO;

	@Override
	public List<PeopleResultDTO> peopleSearch(PeopleSearchDTO psDTO, User currentUser)
	{
		long startTime = Calendar.getInstance().getTimeInMillis();
		// Before we get the result set from the database we would want to
		// apply the result set size restrictions.
		// RE Admins are allowed to get large result sets (10,000 rows max)
		// a search. All other users are limited to 1,000 rows per search.
		// So we will first check what type of user is invoking this search.
		int resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_SUBSCRIBER; // Default
		if ((currentUser != null) && (currentUser.getUsertype().equals(ALMConstants.RIVALEDGE_ADMIN)))
		{
			// This is an admin
			resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_RE_ADMIN;
		}
		psDTO.setMaxResultSizeCap(resultSetSizeCap);
		
		List<PeopleResultDTO> allSearchResults = cacheDAO.findFirmStatisticsVeryFast(psDTO);
		long endTime = Calendar.getInstance().getTimeInMillis();
		System.out.println("\n\n*** Firm Stats data fetch: " + ((endTime - startTime) / 1000) + " seconds run time");
		//return (narrowBySize(allSearchResults, psDTO));
		return allSearchResults;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int getPeopleSearchCount(PeopleSearchDTO psDTO, User currentUser)
	{
		// Before we get the result set count from the database we would want to
		// apply the result set size restrictions.
		// RE Admins are allowed to get large result sets (10,000 rows max)
		// a search. All other users are limited to 1,000 rows per search.
		// So we will first check what type of user is invoking this search.
		int resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_SUBSCRIBER; // Default
		if ((currentUser != null) && (currentUser.getUsertype().equals(ALMConstants.RIVALEDGE_ADMIN)))
		{
			// This is an admin
			resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_RE_ADMIN;
		}
		psDTO.setMaxResultSizeCap(resultSetSizeCap);

		// Get the result set count
		Integer resultCount = cacheDAO.findFirmStatisticsDataCount(psDTO); 
		
		// If it is greater than the cap then cut it down
		if (resultCount > resultSetSizeCap)
		{
			return (resultSetSizeCap);
		}
		return (resultCount);
	}

	@Override
	public List<PeopleResultDTO> peopleSearchWithoutDrilldown(PeopleSearchDTO psDTO, User currentUser)
	{
		int resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_SUBSCRIBER; // Default
		if ((currentUser != null) && (currentUser.getUsertype().equals(ALMConstants.RIVALEDGE_ADMIN)))
		{
			// This is an admin
			resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_RE_ADMIN;
		}
		psDTO.setMaxResultSizeCap(resultSetSizeCap);
		
		List<PeopleResultDTO> allSearchResults = cacheDAO.findFirmStatisticsVeryFastWithoutDrilldown(psDTO);
		
		//return (narrowBySize(allSearchResults, psDTO));
		return allSearchResults;
	}
	
	/**
	 * @deprecated The whole business logic is handled in query in favor of pagination
	 * @param allSearchResults
	 * @param psDTO
	 * @return
	 */
	private List<PeopleResultDTO> narrowBySize(List<PeopleResultDTO> allSearchResults, PeopleSearchDTO psDTO)
	{
		if ((allSearchResults != null) && (!allSearchResults.isEmpty()))
		{
			// Now we will check for and apply the firm size filter
			if ((psDTO.getFirmSizeList() != null) && (!psDTO.getFirmSizeList().isEmpty()))
			{
				// Customer wants to filter by firm size
				// We will go at it in descending order
				List<PeopleResultDTO> targetsForDeletion = new ArrayList<PeopleResultDTO>();
				
				for (PeopleResultDTO thisDTO : allSearchResults)
				{
					if ((thisDTO.getCompanyPeople() != null) && (thisDTO.getCompanyPeople().getHeadCount() != null))
					{
						int firmSize = thisDTO.getCompanyPeople().getHeadCount();
						
						boolean isValidFirmSize = false;

						for (String firmSizeRange : psDTO.getFirmSizeList())
						{
							if (firmSizeRange.equals("1500+"))
							{
								if (firmSize >= 1500)
								{
									isValidFirmSize = true;
								}
							}
							if ((!isValidFirmSize) && (firmSizeRange.equals("1000-1499")))
							{
								if (firmSize >= 1000 && firmSize <= 1499)
								{
									isValidFirmSize = true;
								}
							}
							if ((!isValidFirmSize) && (firmSizeRange.equals("750-999")))
							{
								if (firmSize >= 750 && firmSize <= 999)
								{
									isValidFirmSize = true;
								}
							}
							if ((!isValidFirmSize) && (firmSizeRange.equals("500-749")))
							{
								if (firmSize >= 500 && firmSize <= 749)
								{
									isValidFirmSize = true;
								}
							}
							if ((!isValidFirmSize) && (firmSizeRange.equals("400-499")))
							{
								if (firmSize >= 400 && firmSize <= 499)
								{
									isValidFirmSize = true;
								}
							}
							if ((!isValidFirmSize) && (firmSizeRange.equals("300-399")))
							{
								if (firmSize >= 300 && firmSize <= 399)
								{
									isValidFirmSize = true;
								}
							}
							if ((!isValidFirmSize) && (firmSizeRange.equals("200-299")))
							{
								if (firmSize >= 200 && firmSize <= 299)
								{
									isValidFirmSize = true;
								}
							}
							if ((!isValidFirmSize) && (firmSizeRange.equals("100-199")))
							{
								if (firmSize >= 100 && firmSize <= 199)
								{
									isValidFirmSize = true;
								}
							}
							if ((!isValidFirmSize) && (firmSizeRange.equals("50-99")))
							{
								if (firmSize >= 50 && firmSize <= 99)
								{
									isValidFirmSize = true;
								}
							}
							if ((!isValidFirmSize) && (firmSizeRange.equals("25-49")))
							{
								if (firmSize >= 25 && firmSize <= 49)
								{
									isValidFirmSize = true;
								}
							}
							if ((!isValidFirmSize) && (firmSizeRange.equals("1-24")))
							{
								if (firmSize >= 1 && firmSize <= 24)
								{
									isValidFirmSize = true;
								}
							}
						}
					
						// If this is not within the range of firm sizes
						// specified by the user, it will have to be marked
						// for deletion from the preReturnMap.
						if (!isValidFirmSize)
						{
							targetsForDeletion.add(thisDTO);
						}
					}
				}
				
				// Delete marked results
				for (PeopleResultDTO targetDTO : targetsForDeletion)
				{
					allSearchResults.remove(targetDTO);
				}
				
				// We don't need targetsForDeletion any more
				targetsForDeletion = null;
			}			
		}
		Set<PeopleResultDTO> sortedSearchResults = null;
		if (allSearchResults != null)
		{
			sortedSearchResults = new TreeSet<PeopleResultDTO>();
			for (PeopleResultDTO thisDTO : allSearchResults)
			{
				if (thisDTO != null)
				{
					sortedSearchResults.add(thisDTO);
				}
			}
		}
		else
		{
			sortedSearchResults = new TreeSet<PeopleResultDTO>();
		}
		return (new ArrayList<PeopleResultDTO>(sortedSearchResults));
	}
}