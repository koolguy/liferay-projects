package com.alm.rivaledge.persistence.dao.lawma0_data;

import com.alm.rivaledge.persistence.domain.lawma0_data.LoginHookUserMapping;

public interface LoginHookMapper 
{
	LoginHookUserMapping selectByPrimaryKey(Integer userIds);
	
	void addUserMapping(LoginHookUserMapping loginHookUserMapping);
	
	void updateUserMapping(LoginHookUserMapping loginHookUserMapping);
}
