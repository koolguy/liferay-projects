package com.alm.rivaledge.persistence.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alm.rivaledge.persistence.dao.lawma0_data.KeywordMapper;
import com.alm.rivaledge.persistence.domain.lawma0_data.Keyword;
import com.alm.rivaledge.transferobject.AutocompleteSearchDTO;

/**
 * Data Access Object that handles all persistence for the Keyword domain object.
 * @author FL605
 * @since Sprint 6
 *
 */
@Repository("keywordDAO")
public class KeywordDAO
{
	
	
	@Autowired
	private KeywordMapper keywordMapper;
	
	/**
	 * Add keyword information to database
	 * @param keyword
	 */
	public void addKeyword(Keyword keyword)
	{
		keywordMapper.addKeyword(keyword);
	}

	
	/**
	 * This method is to check whether we need to store keyword information in database or not
	 * if count is 0 keyword will be  persisted in DB
	 * if count is more than 0 then  we would not store keyword information in database	  
	 * @param keyword
	 * @return the count of records which is equal  to  input "keyword" String to method
	 */
	public Integer countByKeyword(String keyword)
	{
		return keywordMapper.countByKeyword(keyword);
	}
	
	/**
	 * This method will return list of keywords for logged in user
	 * @param userId
	 * @return
	 */
	public List<Keyword> findKeywords(Integer userId)
	{
		return keywordMapper.findKeywords(userId);
	}
	
	/**
	 * This method will return list of keywords for logged in user
	 * @param userId
	 * @return
	 */
	public List<Keyword> findKeywords(AutocompleteSearchDTO autocompleteSearchDTO)
	{
		return keywordMapper.findKeywordForAutocomplete(autocompleteSearchDTO);
	}
}
