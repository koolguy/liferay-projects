package com.alm.rivaledge.persistence.dao.lawma0_data;

public interface LMDMapper
{
	void bulkInsertLMDFirmNewsPubsCache(String loadFile);
	
	void dropLMDFirmNewsPubsCacheTable();
	
	void createLMDFirmNewsPubsCacheTable();
}
