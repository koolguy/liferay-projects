package com.alm.rivaledge.util;

/**
 * This class will store location information in a format 
 * which is required for location drop down
 * 
 * @author FL605
 * @since Sprint 1
 */
public class LocationJsonUtility
{
    private String id;
    private String label;

    public LocationJsonUtility()
    {
    	
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

}
