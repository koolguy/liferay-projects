package com.alm.rivaledge.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.transferobject.EventResultDTO;
import com.alm.rivaledge.transferobject.EventSearchDTO;

/**
 * Interface for all the business logic related to events. Most of them will be
 * about calling the cacheDAO to fetch data.
 * 
 * @author FL867
 * @version 1.0
 * @since Sprint 7
 */
@Component
public interface EventService
{
	/**
	 * Return all the events in the events_cache that match the selected
	 * criteria.
	 * 
	 * @param searchDTO
	 * @return
	 */
	public List<EventResultDTO> getEventData(EventSearchDTO searchDTO, User currentUser);

	/**
	 * Return a count of all the events in the events_cache that match the
	 * selected criteria.
	 * 
	 * @param searchDTO
	 * @return
	 */
	public Integer getEventDataCount(EventSearchDTO searchDTO, User currentUser);
}
