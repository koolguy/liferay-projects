package com.alm.rivaledge.service;

import java.util.HashMap;

import org.springframework.stereotype.Component;

@Component
public interface LoginHookUserService 
{
	/**
	 * Given a user id, check if the user exists in the RER mapping table
	 * and whether the user has had the template applied.
	 * 
	 * @param userId
	 * @return boolean
	 */
	public boolean isUserTemplateApplied(HashMap<String, Object> userIds);
	
	
	/**
	 * Update user mapping given ALMUser ID 
	 * 
	 * @param almUserId
	 */
	public void updateUserMapping(int almUserId);
}
