package com.alm.rivaledge.transferobject;

import java.io.Serializable;
import java.util.Date;

public class PeopleResult implements Serializable, Cloneable
{
	private static final long	serialVersionUID	= -3783536340365902175L;

	private Integer				resultId;
	private Integer				groupingId;
	private String				groupType;
	private String				searchTitle;
	private String 				searchLocations;
	private String 				searchPractices;
	private Integer				headCount;
	private Integer				headCountPlus;
	private Integer				headCountMinus;
	private Integer				partnerCount;
	private Integer				partnerCountPlus;
	private Integer				partnerCountMinus;
	private Integer				associateCount;
	private Integer				associateCountPlus;
	private Integer				associateCountMinus;
	private Integer				otherCounselCount;
	private Integer				otherCounselCountPlus;
	private Integer				otherCounselCountMinus;
	private Integer				adminCount;
	private Integer				adminCountPlus;
	private Integer				adminCountMinus;
	private Integer				otherCount;
	private Integer				otherCountPlus;
	private Integer				otherCountMinus;
	private Date				actionDate;

	public PeopleResult()
	{
		super();
		setHeadCount(0);
		setHeadCountPlus(0);
		setHeadCountMinus(0);
		setPartnerCount(0);
		setPartnerCountPlus(0);
		setPartnerCountMinus(0);
		setAssociateCount(0);
		setAssociateCountPlus(0);
		setAssociateCountMinus(0);
		setOtherCounselCount(0);
		setOtherCounselCountPlus(0);
		setOtherCounselCountMinus(0);
		setAdminCount(0);
		setAdminCountPlus(0);
		setAdminCountMinus(0);
		setOtherCount(0);
		setOtherCountPlus(0);
		setOtherCountMinus(0);
	}

	public void addHeadCounts(PeopleCacheResult pcResult)
	{
		if (pcResult.getStaffType().equals("Partner"))
		{
			partnerCount += 1;
			headCount += 1;
		}
		else if (pcResult.getStaffType().equals("Associate"))
		{
			associateCount += 1;
			headCount += 1;
		}
		else if (pcResult.getStaffType().equals("Other Counsel/Attorney"))
		{
			otherCounselCount += 1;
			headCount += 1;
		}
		else if (pcResult.getStaffType().equals("Administrative / Support Staff"))
		{
			adminCount += 1;
		}
		else if (pcResult.getStaffType().equals("Other"))
		{
			otherCount += 1;
		}
	}
	
	public void addMovementCounts(PeopleResult movementResult)
	{
		this.headCountPlus			+= movementResult.getHeadCountPlus();
		this.headCountMinus			+= movementResult.getHeadCountMinus();
		this.partnerCountPlus 		+= movementResult.getPartnerCountPlus();
		this.partnerCountMinus 		+= movementResult.getPartnerCountMinus();
		this.associateCountPlus		+= movementResult.getAssociateCountPlus();
		this.associateCountMinus	+= movementResult.getAssociateCountMinus();
		this.otherCounselCountPlus	+= movementResult.getOtherCounselCountPlus();
		this.otherCounselCountMinus	+= movementResult.getOtherCounselCountMinus();
		this.adminCountPlus			+= movementResult.getAdminCountPlus();
		this.adminCountMinus		+= movementResult.getAdminCountMinus();
		this.otherCountPlus			+= movementResult.getOtherCountPlus();
		this.otherCountMinus		+= movementResult.getOtherCountMinus();
	}
	
	public void addMovementCounts(PeopleCacheResult pcResult)
	{
		if (pcResult.getStaffType().equals("Partner"))
		{
			if (pcResult.getLastActionDate() != null)
			{
				if (pcResult.getLastAction().equals("added"))
				{
					partnerCountPlus += 1;
					headCountPlus += 1;
				}
				else if (pcResult.getLastAction().equals("removed"))
				{
					partnerCountMinus += 1;
					headCountMinus += 1;
				}
			}
		}
		else if (pcResult.getStaffType().equals("Associate"))
		{
			if (pcResult.getLastActionDate() != null)
			{
				if (pcResult.getLastAction().equals("added"))
				{
					associateCountPlus += 1;
					headCountPlus += 1;
				}
				else if (pcResult.getLastAction().equals("removed"))
				{
					associateCountMinus += 1;
					headCountMinus += 1;
				}
			}
		}
		else if (pcResult.getStaffType().equals("Other Counsel/Attorney"))
		{
			if (pcResult.getLastActionDate() != null)
			{
				if (pcResult.getLastAction().equals("added"))
				{
					otherCounselCountPlus += 1;
					headCountPlus += 1;
				}
				else if (pcResult.getLastAction().equals("removed"))
				{
					otherCounselCountMinus += 1;
					headCountMinus += 1;
				}
			}
		}
		else if (pcResult.getStaffType().equals("Administrative / Support Staff"))
		{
			if (pcResult.getLastActionDate() != null)
			{
				if (pcResult.getLastAction().equals("added"))
				{
					adminCountPlus += 1;
				}
				else if (pcResult.getLastAction().equals("removed"))
				{
					adminCountMinus += 1;
				}
			}
		}
		else if (pcResult.getStaffType().equals("Other"))
		{
			if (pcResult.getLastActionDate() != null)
			{
				if (pcResult.getLastAction().equals("added"))
				{
					otherCountPlus += 1;
				}
				else if (pcResult.getLastAction().equals("removed"))
				{
					otherCountMinus += 1;
				}
			}
		}
	}
	
	public Integer getResultId()
	{
		return resultId;
	}

	public void setResultId(Integer resultId)
	{
		this.resultId = resultId;
	}

	public Integer getGroupingId()
	{
		return groupingId;
	}

	public void setGroupingId(Integer groupingId)
	{
		this.groupingId = groupingId;
	}

	public String getGroupType()
	{
		return groupType;
	}

	public void setGroupType(String groupType)
	{
		this.groupType = groupType;
	}

	public String getSearchTitle()
	{
		return searchTitle;
	}

	public void setSearchTitle(String searchTitle)
	{
		this.searchTitle = searchTitle;
	}

	public String getSearchLocations()
	{
		return searchLocations;
	}

	public void setSearchLocations(String searchLocations)
	{
		this.searchLocations = searchLocations;
	}

	public String getSearchPractices()
	{
		return searchPractices;
	}

	public void setSearchPractices(String searchPractices)
	{
		this.searchPractices = searchPractices;
	}

	public Integer getHeadCount()
	{
		return headCount;
	}

	public void setHeadCount(Integer headCount)
	{
		this.headCount = headCount;
	}

	public Integer getHeadCountPlus()
	{
		return headCountPlus;
	}

	public void setHeadCountPlus(Integer headCountPlus)
	{
		this.headCountPlus = headCountPlus;
	}

	public Integer getHeadCountMinus()
	{
		return headCountMinus;
	}

	public void setHeadCountMinus(Integer headCountMinus)
	{
		this.headCountMinus = headCountMinus;
	}

	public Integer getPartnerCount()
	{
		return partnerCount;
	}

	public void setPartnerCount(Integer partnerCount)
	{
		this.partnerCount = partnerCount;
	}

	public Integer getPartnerCountPlus()
	{
		return partnerCountPlus;
	}

	public void setPartnerCountPlus(Integer partnerCountPlus)
	{
		this.partnerCountPlus = partnerCountPlus;
	}

	public Integer getPartnerCountMinus()
	{
		return partnerCountMinus;
	}

	public void setPartnerCountMinus(Integer partnerCountMinus)
	{
		this.partnerCountMinus = partnerCountMinus;
	}

	public Integer getAssociateCount()
	{
		return associateCount;
	}

	public void setAssociateCount(Integer associateCount)
	{
		this.associateCount = associateCount;
	}

	public Integer getAssociateCountPlus()
	{
		return associateCountPlus;
	}

	public void setAssociateCountPlus(Integer associateCountPlus)
	{
		this.associateCountPlus = associateCountPlus;
	}

	public Integer getAssociateCountMinus()
	{
		return associateCountMinus;
	}

	public void setAssociateCountMinus(Integer associateCountMinus)
	{
		this.associateCountMinus = associateCountMinus;
	}

	public Integer getOtherCounselCount()
	{
		return otherCounselCount;
	}

	public void setOtherCounselCount(Integer otherCounselCount)
	{
		this.otherCounselCount = otherCounselCount;
	}

	public Integer getOtherCounselCountPlus()
	{
		return otherCounselCountPlus;
	}

	public void setOtherCounselCountPlus(Integer otherCounselCountPlus)
	{
		this.otherCounselCountPlus = otherCounselCountPlus;
	}

	public Integer getOtherCounselCountMinus()
	{
		return otherCounselCountMinus;
	}

	public void setOtherCounselCountMinus(Integer otherCounselCountMinus)
	{
		this.otherCounselCountMinus = otherCounselCountMinus;
	}

	public Integer getAdminCount()
	{
		return adminCount;
	}

	public void setAdminCount(Integer adminCount)
	{
		this.adminCount = adminCount;
	}

	public Integer getAdminCountPlus()
	{
		return adminCountPlus;
	}

	public void setAdminCountPlus(Integer adminCountPlus)
	{
		this.adminCountPlus = adminCountPlus;
	}

	public Integer getAdminCountMinus()
	{
		return adminCountMinus;
	}

	public void setAdminCountMinus(Integer adminCountMinus)
	{
		this.adminCountMinus = adminCountMinus;
	}

	public Integer getOtherCount()
	{
		return otherCount;
	}

	public void setOtherCount(Integer otherCount)
	{
		this.otherCount = otherCount;
	}

	public Integer getOtherCountPlus()
	{
		return otherCountPlus;
	}

	public void setOtherCountPlus(Integer otherCountPlus)
	{
		this.otherCountPlus = otherCountPlus;
	}

	public Integer getOtherCountMinus()
	{
		return otherCountMinus;
	}

	public void setOtherCountMinus(Integer otherCountMinus)
	{
		this.otherCountMinus = otherCountMinus;
	}

	public Date getActionDate()
	{
		return actionDate;
	}

	public void setActionDate(Date actionDate)
	{
		this.actionDate = actionDate;
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append("PeopleResult [");
		if (resultId != null)
		{
			builder.append("resultId=");
			builder.append(resultId);
			builder.append(", ");
		}
		if (groupingId != null)
		{
			builder.append("groupingId=");
			builder.append(groupingId);
			builder.append(", ");
		}
		if (groupType != null)
		{
			builder.append("groupType=");
			builder.append(groupType);
			builder.append(", ");
		}
		if (searchTitle != null)
		{
			builder.append("searchTitle=");
			builder.append(searchTitle);
			builder.append(", ");
		}
		if (headCount != null)
		{
			builder.append("headCount=");
			builder.append(headCount);
			builder.append(", ");
		}
		if (headCountPlus != null)
		{
			builder.append("headCountPlus=");
			builder.append(headCountPlus);
			builder.append(", ");
		}
		if (headCountMinus != null)
		{
			builder.append("headCountMinus=");
			builder.append(headCountMinus);
			builder.append(", ");
		}
		if (partnerCount != null)
		{
			builder.append("partnerCount=");
			builder.append(partnerCount);
			builder.append(", ");
		}
		if (partnerCountPlus != null)
		{
			builder.append("partnerCountPlus=");
			builder.append(partnerCountPlus);
			builder.append(", ");
		}
		if (partnerCountMinus != null)
		{
			builder.append("partnerCountMinus=");
			builder.append(partnerCountMinus);
			builder.append(", ");
		}
		if (associateCount != null)
		{
			builder.append("associateCount=");
			builder.append(associateCount);
			builder.append(", ");
		}
		if (associateCountPlus != null)
		{
			builder.append("associateCountPlus=");
			builder.append(associateCountPlus);
			builder.append(", ");
		}
		if (associateCountMinus != null)
		{
			builder.append("associateCountMinus=");
			builder.append(associateCountMinus);
			builder.append(", ");
		}
		if (otherCounselCount != null)
		{
			builder.append("otherCounselCount=");
			builder.append(otherCounselCount);
			builder.append(", ");
		}
		if (otherCounselCountPlus != null)
		{
			builder.append("otherCounselCountPlus=");
			builder.append(otherCounselCountPlus);
			builder.append(", ");
		}
		if (otherCounselCountMinus != null)
		{
			builder.append("otherCounselCountMinus=");
			builder.append(otherCounselCountMinus);
			builder.append(", ");
		}
		if (adminCount != null)
		{
			builder.append("adminCount=");
			builder.append(adminCount);
			builder.append(", ");
		}
		if (adminCountPlus != null)
		{
			builder.append("adminCountPlus=");
			builder.append(adminCountPlus);
			builder.append(", ");
		}
		if (adminCountMinus != null)
		{
			builder.append("adminCountMinus=");
			builder.append(adminCountMinus);
			builder.append(", ");
		}
		if (otherCount != null)
		{
			builder.append("otherCount=");
			builder.append(otherCount);
			builder.append(", ");
		}
		if (otherCountPlus != null)
		{
			builder.append("otherCountPlus=");
			builder.append(otherCountPlus);
			builder.append(", ");
		}
		if (otherCountMinus != null)
		{
			builder.append("otherCountMinus=");
			builder.append(otherCountMinus);
			builder.append(", ");
		}
		if (actionDate != null)
		{
			builder.append("actionDate=");
			builder.append(actionDate);
		}
		builder.append("]");
		return builder.toString();
	}

	public PeopleResult mergeMovements(PeopleResult thatPR, String groupingType)
	{
		this.setGroupType(groupingType);
		
		// Merge the plus/minus counts from the argument into this entity
		this.setHeadCountPlus(thatPR.getHeadCountPlus());
		this.setHeadCountMinus(thatPR.getHeadCountMinus());
		this.setPartnerCountPlus(thatPR.getPartnerCountPlus());
		this.setPartnerCountMinus(thatPR.getPartnerCountMinus());
		this.setAssociateCountPlus(thatPR.getAssociateCountPlus());
		this.setAssociateCountMinus(thatPR.getAssociateCountMinus());
		this.setOtherCounselCountPlus(thatPR.getOtherCounselCountPlus());
		this.setOtherCounselCountMinus(thatPR.getOtherCounselCountMinus());
		this.setAdminCountPlus(thatPR.getAdminCountPlus());
		this.setAdminCountMinus(thatPR.getAdminCountMinus());
		this.setOtherCounselCountPlus(thatPR.getOtherCountPlus());
		this.setOtherCountMinus(thatPR.getOtherCountMinus());
		
		return (this);
	}

	@Override
	protected Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}
}
