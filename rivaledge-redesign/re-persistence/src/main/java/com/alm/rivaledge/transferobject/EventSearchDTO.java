package com.alm.rivaledge.transferobject;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;

/**
 * This class will have all the search criteria which user has entered on the
 * Event search form
 * 
 * @author FL613
 * @since Sprint 4
 */
public class EventSearchDTO extends BaseSearchDTO implements Serializable
{
	private static final long	serialVersionUID	= -3376557653172171644L;

	private List<Firm>			selectedFirmsOrganizations;
	private List<String>		practiceArea;
	private List<String>		locations;
	private String				practiceAreaString;
	private Date				toDate;
	private Date				fromDate;
	private String				keyword;
	private List<String>		type;

	public EventSearchDTO()
	{
		super();
		// Override the default sort column
		setSortColumn(3);
		setSortOrder("desc");
	}

	public List<Firm> getSelectedFirmsOrganizations()
	{
		return selectedFirmsOrganizations;
	}

	public void setSelectedFirmsOrganizations(List<Firm> selectedFirmsOrganizations)
	{
		this.selectedFirmsOrganizations = selectedFirmsOrganizations;
	}

	public List<String> getPracticeArea()
	{
		return practiceArea;
	}

	public void setPracticeArea(List<String> practiceArea)
	{
		this.practiceArea = practiceArea;
	}

	public List<String> getLocations()
	{
		return locations;
	}

	public void setLocations(List<String> locations)
	{
		this.locations = locations;
	}

	public String getPracticeAreaString()
	{
		return practiceAreaString;
	}

	public void setPracticeAreaString(String practiceAreaString)
	{
		this.practiceAreaString = practiceAreaString;
	}

	public Date getToDate()
	{
		return toDate;
	}

	public void setToDate(Date toDate)
	{
		this.toDate = toDate;
	}

	public Date getFromDate()
	{
		return fromDate;
	}

	public void setFromDate(Date fromDate)
	{
		this.fromDate = fromDate;
	}

	public String getKeyword()
	{
		return keyword;
	}

	public void setKeyword(String keyword)
	{
		this.keyword = keyword;
	}

	public List<String> getType()
	{
		return type;
	}

	public void setType(List<String> type)
	{
		this.type = type;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fromDate == null) ? 0 : fromDate.hashCode());
		result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
		result = prime * result + ((locations == null) ? 0 : locations.hashCode());
		result = prime * result + ((practiceArea == null) ? 0 : practiceArea.hashCode());
		result = prime * result + ((practiceAreaString == null) ? 0 : practiceAreaString.hashCode());
		result = prime * result + ((selectedFirmsOrganizations == null) ? 0 : selectedFirmsOrganizations.hashCode());
		result = prime * result + ((toDate == null) ? 0 : toDate.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EventSearchDTO other = (EventSearchDTO) obj;
		if (fromDate == null)
		{
			if (other.fromDate != null)
				return false;
		}
		else if (!fromDate.equals(other.fromDate))
			return false;
		if (keyword == null)
		{
			if (other.keyword != null)
				return false;
		}
		else if (!keyword.equals(other.keyword))
			return false;
		if (locations == null)
		{
			if (other.locations != null)
				return false;
		}
		else if (!locations.equals(other.locations))
			return false;
		if (practiceArea == null)
		{
			if (other.practiceArea != null)
				return false;
		}
		else if (!practiceArea.equals(other.practiceArea))
			return false;
		if (practiceAreaString == null)
		{
			if (other.practiceAreaString != null)
				return false;
		}
		else if (!practiceAreaString.equals(other.practiceAreaString))
			return false;
		if (selectedFirmsOrganizations == null)
		{
			if (other.selectedFirmsOrganizations != null)
				return false;
		}
		else if (!selectedFirmsOrganizations.equals(other.selectedFirmsOrganizations))
			return false;
		if (toDate == null)
		{
			if (other.toDate != null)
				return false;
		}
		else if (!toDate.equals(other.toDate))
			return false;
		if (type == null)
		{
			if (other.type != null)
				return false;
		}
		else if (!type.equals(other.type))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "EventsSearchDTO [selectedFirmsOrganizations=" + selectedFirmsOrganizations + ", practiceArea=" + practiceArea + ", locations=" + locations
				+ ", practiceAreaString=" + practiceAreaString + ", toDate=" + toDate + ", fromDate=" + fromDate + ", keyword=" + keyword + ", type=" + type + "]";
	}

}
