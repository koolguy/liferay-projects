package com.alm.rivaledge.transferobject;

import java.io.Serializable;
import java.util.Date;

import com.alm.rivaledge.util.ALMConstants;

public class AttorneyAction implements Serializable
{
	private static final long	serialVersionUID	= -444404812186210182L;

	private String				moveAction;
	private Date				actionDate;

	public String getFormattedDate()
	{
		if (getActionDate() != null)
		{
			try
			{
				return (ALMConstants.SEARCH_DATE_FORMAT.format(actionDate));
			}
			catch (Exception ex)
			{
				// Do nothing
			}
		}
		return ("");
	}
	
	public String getFormattedAction()
	{
		if ((moveAction != null) && (moveAction.trim().length() > 0))
		{
			return (moveAction.trim().toUpperCase());
		}
		return ("");
	}
	
	
	public String getMoveAction()
	{
		return moveAction;
	}

	public void setMoveAction(String moveAction)
	{
		this.moveAction = moveAction;
	}

	public Date getActionDate()
	{
		return actionDate;
	}

	public void setActionDate(Date actionDate)
	{
		this.actionDate = actionDate;
	}
}