package com.alm.rivaledge.util;


/**
 * This class will store Content Type information in a format 
 * which is required for Content Type auto completer/ type ahead
 * 
 * @author FL889
 * @since Sprint 6
 */
public class ContentTypeJsonUtility
{

    private String id;
    private String label;

    public ContentTypeJsonUtility()
    {
    	
    }
    

    public ContentTypeJsonUtility(String id, String label)
	{
		super();
		this.id = id;
		this.label = label;
	}


	public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

}
