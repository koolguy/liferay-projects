package com.alm.rivaledge.service.impl;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alm.rivaledge.persistence.dao.LoginHookUserMappingDAO;
import com.alm.rivaledge.persistence.domain.lawma0_data.LoginHookUserMapping;

import com.alm.rivaledge.service.LoginHookUserService;

@Service("loginHookUserService")
public class LoginHookUserServiceImpl implements LoginHookUserService 
{
	@Autowired
	private LoginHookUserMappingDAO loginHookDAO;
	
	
	/* (non-Javadoc)
	 * @see com.alm.rivaledge.service.LoginHookUserService#isUserTemplateApplied(java.util.HashMap)
	 */
	@Override
	public boolean isUserTemplateApplied(HashMap<String, Object> userIds)
	{
		if(userIds != null && userIds.size() >  0)
		{
			Integer almId = (Integer)userIds.get("ALMID");
			System.out.println("LoginHookUserService::almId::"+almId);
			Long lrId = (Long)userIds.get("LRID");
			System.out.println("LoginHookUserService::lrId::"+lrId);
			
			LoginHookUserMapping loginHookUserMapping = loginHookDAO.findByPK(almId);
			
			//If user is logged in for first time
			if( loginHookUserMapping == null)
			{
				//insert entry into DB
				loginHookUserMapping = new LoginHookUserMapping();
				loginHookUserMapping.setLiferayUserId(lrId.intValue());
				loginHookUserMapping.setLmdUserId(almId);
				loginHookUserMapping.setIsTemplateApplied(Boolean.FALSE);
				loginHookDAO.addUserMapping(loginHookUserMapping);
				System.out.println("LoginHookUserService::Mapping Added::"+almId+"::"+lrId);
				return false;
			}
			else
			{
				return loginHookUserMapping.getIsTemplateApplied();
			}
		}
		
		return false;
	}

	/* (non-Javadoc)
	 * @see com.alm.rivaledge.service.LoginHookUserService#updateUserMapping(int)
	 */
	@Override
	public void updateUserMapping(int almUserId)
	{
		
		if(almUserId >  0)
		{
			LoginHookUserMapping loginHookUserMapping = loginHookDAO.findByPK(almUserId);
			if( loginHookUserMapping == null )
			{
				System.out.println("LoginHookUserServiceImpl::ALM User NOT found::"+almUserId);
			}
			else
			{
				loginHookUserMapping.setIsTemplateApplied(Boolean.TRUE);
				loginHookDAO.updateUserMapping(loginHookUserMapping);
			}
			
		}
	}
	
	

}
