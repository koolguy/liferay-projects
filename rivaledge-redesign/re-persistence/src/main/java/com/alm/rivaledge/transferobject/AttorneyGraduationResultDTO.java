package com.alm.rivaledge.transferobject;

public class AttorneyGraduationResultDTO extends BaseSearchDTO {

	private static final long serialVersionUID = 6796679175080531389L;
	private Integer firmId;
	private String firmName; // column #5
	private String title; // column #7

	private Integer attorneyCount = 0;
	private Integer experience;

	public Integer getFirmId() {
		return firmId;
	}

	public void setFirmId(Integer firmId) {
		this.firmId = firmId;
	}

	public String getFirmName() {
		return firmName;
	}

	public void setFirmName(String firmName) {
		this.firmName = firmName;
	}

	public Integer getAttorneyCount() {
		return attorneyCount;
	}

	public void setAttorneyCount(Integer attorneyCount) {
		this.attorneyCount = attorneyCount;
	}

	public Integer getExperience() {
		return experience;
	}

	public void setExperience(Integer experience) {
		this.experience = experience;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Override
	public String toString() {
		String val = "Printing DTO :: Firm : " + this.getFirmId() + " - "
				+ this.getFirmName() + " Title: " + this.getTitle();
		val += " Attorneys : " + this.getAttorneyCount() + " Experience : "
				+ this.getExperience();
		return val;
	}
}
