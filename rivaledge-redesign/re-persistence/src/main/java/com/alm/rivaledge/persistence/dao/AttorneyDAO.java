package com.alm.rivaledge.persistence.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alm.rivaledge.persistence.dao.lawma0_data.AdmissionMapper;
import com.alm.rivaledge.persistence.dao.lawma0_data.LawSchoolMapper;
import com.alm.rivaledge.persistence.dao.lawma0_data.PersonMapper;
import com.alm.rivaledge.persistence.domain.lawma0_data.Admission;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.LawSchool;
import com.alm.rivaledge.persistence.domain.lawma0_data.Person;
import com.alm.rivaledge.transferobject.AttorneyResultDTO;
import com.alm.rivaledge.transferobject.AttorneySearchDTO;
import com.alm.rivaledge.transferobject.AutocompleteSearchDTO;


/**
 * 
 * Data Access Object that handles all persistence for the People/PeopleDetailText domain object.
 * @author FL605
 * @since Sprint 3
 *
 */
@Repository("attorneyDAO")
public class AttorneyDAO 
{
	
	
	@Autowired
	private PersonMapper 	personMapper;
	
	@Autowired
	private FirmDAO 		firmDAO;
	
	@Autowired
	private	LawSchoolMapper lawSchoolMapper;
	
	@Autowired
	private	AdmissionMapper admissionMapper;
	

	
	/**
	 * Fetch The search result based on the search criteria which user has selected on Attorney  search form screen
	 * @param attorneySearchDTO
	 * @return List<AttorneyResultDTO>
	 */
	public List<AttorneyResultDTO> findAttorneyData(AttorneySearchDTO  attorneySearchDTO)
	{	
		List<AttorneyResultDTO>  attorneyResultDTOList= new ArrayList<AttorneyResultDTO>();	
		
		//get the list of person from DB based on search criteria
		List<Person> lists=personMapper.getListOfPersons(attorneySearchDTO);
		
		
		//creating list of AttorneyResultDTO based on Peoples list
		for(Person person  : lists)
		{
			AttorneyResultDTO  attorneyResultDTO =new AttorneyResultDTO();
			StringBuilder sb=new StringBuilder();
			attorneyResultDTO.setAttorneyId(person.getPersonId());
			attorneyResultDTO.setAttorneyName(person.getStdName());
			attorneyResultDTO.setAttorneyLink(person.getLink());
			Firm firm=firmDAO.findByPK(person.getCompanyId());
			if(firm!=null){
				attorneyResultDTO.setFirmName(firm.getCompany());
				attorneyResultDTO.setFirmLink(firm.getHomeUrl());
			}			
			attorneyResultDTO.setTitle(person.getStdTitle());
			attorneyResultDTO.setLocation(person.getStdLoc());
			attorneyResultDTO.setPractices(person.getStdPractices());
			attorneyResultDTO.setEducation(person.getPersonDetails().getEdu());
			attorneyResultDTO.setAdmission(person.getPersonDetails().getAdm());
			attorneyResultDTO.setKeywords(person.getPersonDetails().getDetailText());			
			sb.append(person.getPhone());
			sb.append(",");			
			sb.append(person.getEmail());
			attorneyResultDTO.setContactInfo(sb.toString());
			attorneyResultDTOList.add(attorneyResultDTO);
		}
		
		return attorneyResultDTOList;
		
	}
	
	/**
	 * This method will return list of keywords for logged in user
	 * @param userId
	 * @return
	 */
	public List<LawSchool> findLawSchool(AutocompleteSearchDTO autocompleteSearchDTO)
	{
		return lawSchoolMapper.findLawSchoolForAutocomplete(autocompleteSearchDTO);
	}
	
	/**
	 * This method will return list of keywords for logged in user
	 * @param userId
	 * @return
	 */
	public List<Admission> findAdmissions(AutocompleteSearchDTO autocompleteSearchDTO)
	{
		return admissionMapper.findAdmissionsForAutocomplete(autocompleteSearchDTO);
	}
	
	
	/**
	 * This method is to check whether we need to store lawschool information in database or not
	 * if count is 0 lawschool will be  persisted in DB
	 * if count is more than 0 then  we would not store lawschool information in database	  
	 * @param keyword
	 * @return the count of records which is equal  to  input "lawschool" String to method
	 */
	public Integer countByLawSchool(String lawschool) 
	{
		return lawSchoolMapper.countByLawSchool(lawschool);
	}
	
	/**
	 * This method is to check whether we need to store admission information in database or not
	 * if count is 0 admission will be  persisted in DB
	 * if count is more than 0 then  we would not store admission information in database	  
	 * @param keyword
	 * @return the count of records which is equal  to  input "admission" String to method
	 */
	public Integer countByAdmission(String admission) 
	{
		return admissionMapper.countByAdmission(admission);
	}
	
	/**
	 * Add Law School information to database for Auto complete
	 * @param keyword
	 */
	public void addLawSchool(LawSchool lawSchool)
	{
		lawSchoolMapper.addLawSchool(lawSchool);
	}
	/**
	 * Add Admission information to database for Auto complete
	 * @param keyword
	 */
	public void addAdmission(Admission admission)
	{
		admissionMapper.addAdmission(admission);
	}
}
