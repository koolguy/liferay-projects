package com.alm.rivaledge.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alm.rivaledge.persistence.dao.AttorneyDAO;
import com.alm.rivaledge.persistence.dao.CacheDAO;
import com.alm.rivaledge.persistence.dao.CityDAO;
import com.alm.rivaledge.persistence.dao.KeywordDAO;
import com.alm.rivaledge.persistence.domain.lawma0_data.Admission;
import com.alm.rivaledge.persistence.domain.lawma0_data.City;
import com.alm.rivaledge.persistence.domain.lawma0_data.Keyword;
import com.alm.rivaledge.persistence.domain.lawma0_data.LawSchool;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.service.AttorneyService;
import com.alm.rivaledge.transferobject.AttorneyGraduationResultDTO;
import com.alm.rivaledge.transferobject.AttorneyMoveChangesResultDTO;
import com.alm.rivaledge.transferobject.AttorneyMoveChangesSearchDTO;
import com.alm.rivaledge.transferobject.AttorneyResultDTO;
import com.alm.rivaledge.transferobject.AttorneySearchDTO;
import com.alm.rivaledge.transferobject.AutocompleteSearchDTO;
import com.alm.rivaledge.transferobject.LocationList;
import com.alm.rivaledge.util.ALMConstants;



@Service("attorneyService")
public class AttorneyServiceImpl implements AttorneyService
{
	@Autowired
	private CacheDAO	cacheDAO;
	
	@Autowired
	private AttorneyDAO	attorneyDAO;
	
	@Autowired
	private KeywordDAO	keywordDAO;	 
	
	@Autowired
	private CityDAO	cityDAO;	

	

	@Override
	public List<AttorneyResultDTO> getAttorneyData(AttorneySearchDTO attorneySearchDTO)
	{
		return cacheDAO.findAttorneyData(attorneySearchDTO);
	}

	@Override
	public Integer getAttorneyDataCount(AttorneySearchDTO asDTO, User currentUser)
	{
		// Before we get the result set count from the database we would want to
		// apply the result set size restrictions.
		// RE Admins are allowed to get large result sets (10,000 rows max)
		// a search. All other users are limited to 1,000 rows per search.
		// So we will first check what type of user is invoking this search.
		int resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_SUBSCRIBER; // Default
		if ((currentUser != null) && (currentUser.getUsertype().equals(ALMConstants.RIVALEDGE_ADMIN)))
		{
			// This is an admin
			resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_RE_ADMIN;
		}
		asDTO.setMaxResultSizeCap(resultSetSizeCap);

		compactDTO(asDTO);
		// Get the result set count
		int resultCount = cacheDAO.findAttorneyDataCount(asDTO);

		// If it is greater than the cap then cut it down
		if (resultCount > resultSetSizeCap)
		{
			return (resultSetSizeCap);
		}
		return (resultCount);
	}

	/**
	 * nullifies all the String variables if required (null, empty, trailing and leading whitespaces)<br>
	 * Currently the mybatis query have only null check.<br>
	 * Rather than moving the empty string checking logic to Mapper xml its seems good idea to compact the Object values before handling it to DAO layer<br>
	 * @param asDTO
	 */
	private void compactDTO(AttorneySearchDTO asDTO)
	{
		//asDTO.setAttorneyName(.defaultIfBlank(asDTO.getAttorneyName(), null));
		asDTO.setAdmissions(StringUtils.defaultIfBlank(asDTO.getAdmissions(), null));
		asDTO.setLawSchooltext(StringUtils.defaultIfBlank(asDTO.getLawSchooltext(), null));
		asDTO.setUndergraduateSchoolText(StringUtils.defaultIfBlank(asDTO.getUndergraduateSchoolText(), null));
		asDTO.setKeywordsInBioText(StringUtils.defaultIfBlank(asDTO.getKeywordsInBioText(), null));
	}

	
/*	@Override
	public List<String> getAttorneyNames(String inputName)
	{
		List<AttorneyResultDTO> attorneyResult = attorneyDAO.getAttorneyNames(inputName);
		
		List<String> stringAttorney = new ArrayList<String>();
		
		if(attorneyResult!= null && attorneyResult.size() > 0)
		{
			for( AttorneyResultDTO attorneyResultDTO : attorneyResult )
			{
				stringAttorney.add(attorneyResultDTO.getAttorneyName());
			}
		}
		
		return stringAttorney;
	}*/

	@Override
	public int getAttorneyMoveChangesCount(AttorneyMoveChangesSearchDTO attorneyMoveChangesSearchDTO, User currentUser)
	{
		int resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_SUBSCRIBER; // Default
		int resultCount=0;
		if ((currentUser != null) && (currentUser.getUsertype().equals(ALMConstants.RIVALEDGE_ADMIN)))
		{
			// This is an admin
			resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_RE_ADMIN;
		}
		attorneyMoveChangesSearchDTO.setMaxResultSizeCap(resultSetSizeCap);
		//check whether  user has entered search filter based on Attorney name and its having more than one String  separated by space
		if(attorneyMoveChangesSearchDTO.getAttorneyName()!=null && attorneyMoveChangesSearchDTO.getAttorneyName().size()>1)
		{		
			
			List<AttorneyMoveChangesResultDTO> initialResult=cacheDAO.findAttorneyMovementsCount(attorneyMoveChangesSearchDTO);
			List<Integer> attorneyList= new ArrayList<Integer>();
			if(initialResult!=null && initialResult.size() > resultSetSizeCap)
			{
				initialResult=initialResult.subList(0, resultSetSizeCap);
			}			
			for(AttorneyMoveChangesResultDTO attorneyResultDTO : initialResult)
			{
				attorneyList.add(attorneyResultDTO.getAttorneyId());
			}
			attorneyMoveChangesSearchDTO.setAttorneyIdList(attorneyList);
			resultCount=initialResult.size() + cacheDAO.findAttorneyMovementsCountWithMutipleNameString(attorneyMoveChangesSearchDTO);
		}		
		else
		{
			//either user has not run a search on Attorney name or 
			//has entered only one string for Attorney name filter
			resultCount = cacheDAO.findAttorneyMovementsCount(attorneyMoveChangesSearchDTO).size();
		}

		
	
		// If it is greater than the cap then cut it down
		if (resultCount > resultSetSizeCap)
		{
			return (resultSetSizeCap);
		}
		return (resultCount);
	}

	@Override
	public List<AttorneyMoveChangesResultDTO> getAttorneyMovesChanges(AttorneyMoveChangesSearchDTO attorneyMoveChangesSearchDTO, User currentUser)
	{
		int resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_SUBSCRIBER; // Default
		if ((currentUser != null) && (currentUser.getUsertype().equals(ALMConstants.RIVALEDGE_ADMIN)))
		{
			// This is an admin
			resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_RE_ADMIN;
		}
		attorneyMoveChangesSearchDTO.setMaxResultSizeCap(resultSetSizeCap);
		
		 List<AttorneyMoveChangesResultDTO> finalResultsList= new ArrayList<AttorneyMoveChangesResultDTO>();
		//check whether  user has entered search filter based on Attorney name and its having more than one String  separated by space 
		if(attorneyMoveChangesSearchDTO.getAttorneyName()!=null && attorneyMoveChangesSearchDTO.getAttorneyName().size()>1)
		{
			finalResultsList = cacheDAO.findAttorneyMovementsData(attorneyMoveChangesSearchDTO);
			List<Integer> attorneyList= new ArrayList<Integer>();
			for(AttorneyMoveChangesResultDTO attorneyResultDTO:finalResultsList)
			{
				attorneyList.add(attorneyResultDTO.getAttorneyId());
			}
			attorneyMoveChangesSearchDTO.setAttorneyIdList(attorneyList);
			finalResultsList.addAll(cacheDAO.findAttorneyMovementsDataWithMutipleNameString(attorneyMoveChangesSearchDTO));
		}
		else
		{
			//either user has not run a search on Attorney name or 
			//has entered only one string for Attorney name filter
			finalResultsList = cacheDAO.findAttorneyMovementsData(attorneyMoveChangesSearchDTO);
		}
		
		if(finalResultsList!=null && finalResultsList.size() > resultSetSizeCap)
		{
			finalResultsList= finalResultsList.subList(0, resultSetSizeCap-1);
		}
		return finalResultsList;
		
		
	}
	
	@Override
	public List<AttorneyResultDTO> findAttorneyDataForcharts(AttorneySearchDTO attorneySearchDTO, User currentUser)
	{
		
		int resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_SUBSCRIBER; // Default
		if ((currentUser != null) && (currentUser.getUsertype().equals(ALMConstants.RIVALEDGE_ADMIN)))
		{
			// This is an admin
			resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_RE_ADMIN;
		}
		attorneySearchDTO.setMaxResultSizeCap(resultSetSizeCap);
		return cacheDAO.findAttorneyDataForcharts(attorneySearchDTO);
	}
	
	@Override
	public List<AttorneyGraduationResultDTO> findAttorneyDataForGradcharts(AttorneySearchDTO attorneySearchDTO)
	{	
		return cacheDAO.findAttorneyDataForGradcharts(attorneySearchDTO);
	}
	
	@Override
	public List<String> getKeywords(AutocompleteSearchDTO autocompleteSearchDTO)
	{	
		List<String> listOfKeywords =new ArrayList<String>();
		List<Keyword> keywords = keywordDAO.findKeywords(autocompleteSearchDTO);
		for(Keyword keyword: keywords)
		{
			listOfKeywords.add(keyword.getKeyword());			
		}
		return listOfKeywords;
	}	
	
	@Override
	public List<String> getLawSchool(AutocompleteSearchDTO autocompleteSearchDTO)
	{
		List<String> listOfLawSchool =new ArrayList<String>();
		List<LawSchool> lawSchools = attorneyDAO.findLawSchool(autocompleteSearchDTO);
		for(LawSchool lawSchool: lawSchools)
		{
			listOfLawSchool.add(lawSchool.getLawschool());			
		}
		return listOfLawSchool;
	}
	
	@Override
	public List<String> getAdmissions(AutocompleteSearchDTO autocompleteSearchDTO)
	{
		List<String> listOfAdmissions =new ArrayList<String>();
		List<Admission> admissions = attorneyDAO.findAdmissions(autocompleteSearchDTO);
		for(Admission admission: admissions)
		{
			listOfAdmissions.add(admission.getAdmission());			
		}
		return listOfAdmissions;
		
	}

	@Override
	public Integer countByLawSchool(String lawschool) 
	{
		return attorneyDAO.countByLawSchool(lawschool);
	}

	@Override
	public Integer countByAdmission(String admission) 
	{
		return attorneyDAO.countByAdmission(admission);
		
	}
	
	/**
	 * Add Law School information to database
	 * @param keyword
	 */
	public void addLawSchool(LawSchool lawSchool)
	{
		attorneyDAO.addLawSchool(lawSchool);
	}
	
	/**
	 * Add Law School information to database
	 * @param keyword
	 */
	public void addAdmission(Admission admission)
	{
		attorneyDAO.addAdmission(admission);
	}

	
	@Override
	public List<LocationList> fetchLocationTree(String locationList)
	{
		//Final Location list Which will contain Location tree details
		//Every Location list object will have its title and children information
		//It would three level tree: Level 1: Geographic Region, Level 2: Country, Level 3: City
		List<LocationList> finalLocationList=new ArrayList<LocationList>();
		
		
		List<City> temCountryList=new ArrayList<City>();
		List<City> tempCityList=new ArrayList<City>();
		if(cityDAO.selectGeoRegion()!=null && cityDAO.selectGeoRegion().size() >0)
		{
			for(City geoRegion : cityDAO.selectGeoRegion())
			{
				LocationList geoRegionChild= new LocationList();
				geoRegionChild.setTitle(geoRegion.getGeographicRegion());
				//fetching all countries based on geographic region
				temCountryList=cityDAO.selectCountry(geoRegion.getGeographicRegion());
				List<LocationList> countryList=new ArrayList<LocationList>();
				//iterating over countries to fetch  all cities
				for(City country : temCountryList)
				{
					LocationList countryItem= new LocationList();
					countryItem.setTitle(country.getCountry());
					//fetching all Cities based on country
					tempCityList=cityDAO.selectCity(country.getCountry());
					List<LocationList> cityList=new ArrayList<LocationList>();
					for(City city :  tempCityList)
					{
						LocationList cityItem= new LocationList();
						cityItem.setTitle(city.getCity());
						/*if(locationList!=null && locationList.contains(city.getCity()))
						{
							cityItem.setSelect(true);
						}*/
						//adding city information
						cityList.add(cityItem);					
					}
					//Adding Cities to Country
					countryItem.setChildren(cityList);
					countryList.add(countryItem);
				}
				//Adding countries to geographic region
				geoRegionChild.setChildren(countryList);
				//Adding All geographic region information final List
				finalLocationList.add(geoRegionChild);
			}
			
		}	
		//now we don't need this
		temCountryList=null;
		tempCityList=null;
		
		return finalLocationList;
		
	}
	
	@Override
	public String updateLocationTree(String locationTreeJson, List<String> selectedLocation)
	{
		if(selectedLocation !=null && selectedLocation.size()>0)
		{			
			for(String location: selectedLocation)
			{
				//Changing "select" flag to true for selected location.
				//that will result in by default selection of location in  tree
				locationTreeJson=locationTreeJson.replace("{\"select\":false,\"title\":\""+location.trim()+"\"}","{\"select\":true,\"title\":\""+location.trim()+"\"}");
			}
		}
		return locationTreeJson;
	}
	
	@Override
	public List<String> fetchCitiesForAutocomplete()
	{
		List<String> listOfCities =new ArrayList<String>();
		List<City> allCities=cityDAO.fetchCitiesForAutocomplete();
		for(City city : allCities)
		{
			listOfCities.add(city.getCity());			
		}		
		return listOfCities;
	}
	
	
}
