package com.alm.rivaledge.transferobject;

import java.io.Serializable;
import java.util.List;

import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;

public class GroupEventsResultDTO implements Serializable
{
	private static final long		serialVersionUID	= 7365000737693045696L;
	private Integer					groupEventsId;
	private UserGroup				userGroup;
	private List<EventResultDTO>	groupEvents;

	/**
	 * Primary key for this userGroup - events mapping
	 * @return
	 */
	public Integer getGroupEventsId()
	{
		return groupEventsId;
	}

	public void setGroupEventsId(Integer groupEventsId)
	{
		this.groupEventsId = groupEventsId;
	}

	/**
	 * The UserGroup (aka Watch list) that owns these events
	 * @return
	 */
	public UserGroup getUserGroup()
	{
		return userGroup;
	}

	public void setUserGroup(UserGroup userGroup)
	{
		this.userGroup = userGroup;
	}

	/**
	 * The events owned by this user group (aka watch list)
	 * @return
	 */
	public List<EventResultDTO> getGroupEvents()
	{
		return groupEvents;
	}

	public void setGroupEvents(List<EventResultDTO> groupEvents)
	{
		this.groupEvents = groupEvents;
	}
}
