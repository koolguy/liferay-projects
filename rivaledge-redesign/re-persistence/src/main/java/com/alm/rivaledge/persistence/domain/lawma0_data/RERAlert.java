package com.alm.rivaledge.persistence.domain.lawma0_data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

//import com.liferay.portal.kernel.util.ListUtil;
//import com.liferay.portal.kernel.util.StringUtil;

public class RERAlert {

	private Integer alertUserId;
	private Integer alertId;
	private Integer userId;
	private String searchJson;
	private String alertType;
	private String alertTypeId;
	private String alertName;
	private Integer alertFrequency;
	private List<String> alertEmails;
	private Date lastTriggered;
	private Date lastUpdated;
	private String lastTriggeredStr;
	private Date dateAdded;
	private Date mappingDateAdded;
	private Integer isActive;
	private String alertEmailsStr;
	
	public Integer getAlertUserId() {
		return alertUserId;
	}
	public void setAlertUserId(Integer alertUserId) {
		this.alertUserId = alertUserId;
	}
	public Integer getAlertId() {
		return alertId;
	}
	public void setAlertId(Integer alertId) {
		this.alertId = alertId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getSearchJson() {
		return searchJson;
	}
	public void setSearchJson(String searchJson) {
		this.searchJson = searchJson;
	}
	public String getAlertType() {
		return alertType;
	}
	public void setAlertType(String alertType) {
		this.alertType = alertType;
	}
	public String getAlertTypeId() {
		return alertTypeId;
	}
	public void setAlertTypeId(String alertTypeId) {
		this.alertTypeId = alertTypeId;
	}
	public String getAlertName() {
		return alertName;
	}
	public void setAlertName(String alertName) {
		this.alertName = alertName;
	}
	public Integer getAlertFrequency() {
		return alertFrequency;
	}
	public void setAlertFrequency(Integer alertFrequency) {
		this.alertFrequency = alertFrequency;
	}
	public List<String> getAlertEmails() {
		return alertEmails;
	}
	public void setAlertEmails(List<String> alertEmails) {
		this.alertEmails = alertEmails;
		String str = "";
		for(String email: this.alertEmails){
			if(str.length()>0){
				str += ";";
			}
			str += email;
		}
		this.alertEmailsStr = str;
	}
	public Date getLastTriggered() {
		return lastTriggered;
	}
	public void setLastTriggered(Date lastTriggered) {
		this.lastTriggered = lastTriggered;
		if(lastTriggered != null) {
			this.setLastTriggeredStr(this.lastTriggered.toString());
		}
	}
	public Date getLastUpdated() {
		return lastUpdated;
	}
	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
	public String getLastTriggeredStr() {
		return lastTriggeredStr;
	}
	public void setLastTriggeredStr(String lastTriggeredStr) {
		this.lastTriggeredStr = lastTriggeredStr;
	}
	public Date getDateAdded() {
		return dateAdded;
	}
	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}
	public Date getMappingDateAdded() {
		return mappingDateAdded;
	}
	public void setMappingDateAdded(Date mappingDateAdded) {
		this.mappingDateAdded = mappingDateAdded;
	}
	public Integer getIsActive() {
		return isActive;
	}
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
	public String getAlertEmailsStr() {
		return alertEmailsStr;
	}
	public void setAlertEmailsStr(String alertEmailsStr) {
		if(alertEmailsStr != null){
			this.alertEmails = new ArrayList<String>();
			StringTokenizer st = new StringTokenizer(alertEmailsStr, ";");
			while (st.hasMoreElements()) {
				String email = (String)st.nextElement();
				if( email.trim().length() > 0)  this.alertEmails.add(email);
			}
		}
		this.alertEmailsStr = alertEmailsStr;
	}
	
	
}
