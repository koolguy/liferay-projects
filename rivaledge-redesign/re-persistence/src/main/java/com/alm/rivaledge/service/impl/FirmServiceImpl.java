package com.alm.rivaledge.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alm.rivaledge.persistence.dao.CacheDAO;
import com.alm.rivaledge.persistence.dao.FirmDAO;
import com.alm.rivaledge.persistence.dao.KeywordDAO;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.FirmLocation;
import com.alm.rivaledge.persistence.domain.lawma0_data.Keyword;
import com.alm.rivaledge.persistence.domain.lawma0_data.Practice;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.transferobject.EventSearchDTO;
import com.alm.rivaledge.transferobject.FirmResultDTO;
import com.alm.rivaledge.transferobject.FirmSearchDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.FirmJsonUtility;

@Service("firmService")
public class FirmServiceImpl implements FirmService
{
	@Autowired
	private FirmDAO	firmDAO;
	
	@Autowired
	private CacheDAO cacheDAO;
	
	@Autowired
	private KeywordDAO	keywordDAO;

	@Override
	public List<Firm> getRanking(String rankingType)
	{
		if (rankingType.equals(ALMConstants.AMLAW_100))
		{
			return firmDAO.findAmLaw100();
		}
		else if (rankingType.equals(ALMConstants.AMLAW_200))
		{
			return firmDAO.findAmLaw200();
		}
		else if (rankingType.equals(ALMConstants.NLJ_250))
		{
			return firmDAO.findNLJ250();
		}
		return null;
	}

	@Override
	public List<Firm> getAllFirms()
	{
		return (firmDAO.findAllFirms());
	}

	@Override
	public List<Practice> getAllPractices()
	{
		return (firmDAO.findPractices());
	}

	@Override
	public List<Practice> getMatchingPractices(String practiceKeyword)
	{
		if ((practiceKeyword != null) && (practiceKeyword.trim().length() >= 3))
		{
			return (firmDAO.findMatchingPractices(practiceKeyword));
		}
		return (new ArrayList<Practice>());
	}

	@Override
	public List<FirmLocation> getAllLocations()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int getFirmSearchCount(FirmSearchDTO fsDTO, User currentUser)
	{
		// Before we get the result set count from the database we would want to
		// apply the result set size restrictions.
		// RE Admins are allowed to get large result sets (10,000 rows max)
		// a search. All other users are limited to 1,000 rows per search.
		// So we will first check what type of user is invoking this search.
		int resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_SUBSCRIBER; // Default
		if ((currentUser != null) && (currentUser.getUsertype().equals(ALMConstants.RIVALEDGE_ADMIN)))
		{
			// This is an admin
			resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_RE_ADMIN;
		}
		fsDTO.setMaxResultSizeCap(resultSetSizeCap);

		// Get the result set count
		int resultCount = cacheDAO.findFirmDataCount(fsDTO);
		
		// If it is greater than the cap then cut it down
		if (resultCount > resultSetSizeCap)
		{
			return (resultSetSizeCap);
		}
		return (resultCount);
	}
	
	public int getEventSearchCount(EventSearchDTO esDTO, User currentUser)
	{
		// Before we get the result set count from the database we would want to
				// apply the result set size restrictions.
				// RE Admins are allowed to get large result sets (10,000 rows max)
				// a search. All other users are limited to 1,000 rows per search.
				// So we will first check what type of user is invoking this search.
				int resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_SUBSCRIBER; // Default
				if ((currentUser != null) && (currentUser.getUsertype().equals(ALMConstants.RIVALEDGE_ADMIN)))
				{
					// This is an admin
					resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_RE_ADMIN;
				}
				esDTO.setMaxResultSizeCap(resultSetSizeCap);

				// Get the result set count
				int resultCount = cacheDAO.findEventDataCount(esDTO);
				
				// If it is greater than the cap then cut it down
				if (resultCount > resultSetSizeCap)
				{
					return (resultSetSizeCap);
				}
			return (resultCount);
	}
	
	public List<FirmResultDTO> firmNewsPubsSearch(FirmSearchDTO searchFormTo)
	{
		return (cacheDAO.findFirmNewsPubs(searchFormTo));
	}

	@Override
	public Firm getFirmById(Integer companyId)
	{
		return (firmDAO.findByPK(companyId));
	}

	@Override
	public List<FirmJsonUtility> getRivalEdgeList(List<FirmJsonUtility> firmJsonUtilityList)
	{
		FirmJsonUtility firmJsonUtility = null;
		for (String rivalEdge : ALMConstants.RIVALEDGE_RANKING_LIST)
		{
			firmJsonUtility = new FirmJsonUtility();
			firmJsonUtility.setId(rivalEdge);
			firmJsonUtility.setCategory(ALMConstants.RIVALEDGE_LIST);
			firmJsonUtility.setLabel(rivalEdge);
			firmJsonUtilityList.add(firmJsonUtility);
		}
		return firmJsonUtilityList;
	}

	@Override
	public List<String> getUserKeywords(User currentUser)
	{
		return (firmDAO.findUserKeywords(currentUser));
	}
	
	/**
	 * This method is used to get all organization list
	 * @return
	 */
	public List<Firm> getOrganizationsList()
	{
		return (firmDAO.getOrganizationsList());
	}
	
	/**
	 * This method is used for getting all firm watchlists sorted by Date added
	 * @return userGroup 
	 */
	public List<UserGroup> getWatchListSortedByDate(Integer userId)
	{
		return (firmDAO.getWatchListSortedByDate(userId));
	}
	
	/**
	 * Method returns list of top five publications by practice area 
	 * Specified in the FirmSearchDTO.
	 * 
	 * @param firmSearchDTO
	 * @return
	 */
	public List<FirmResultDTO> findFiveLatestPublicationsByPractice(FirmSearchDTO firmSearchDTO)
	{
		return (cacheDAO.findFiveLatestPublicationsByPractice(firmSearchDTO));
	}
	
	@Override
	public void addkeyword(Keyword keyword)
	{
		keywordDAO.addKeyword(keyword);
	}
	
	@Override
	public Integer countByKeyword(String keyword)
	{
		return keywordDAO.countByKeyword(keyword);
	}
	
	@Override
	public List<String> findKeywords(Integer userId)
	{
		List<Keyword> keywords= keywordDAO.findKeywords(userId);
		List<String> keywordsFinalList=new ArrayList<String>();
		for(Keyword keyword: keywords)
		{
			keywordsFinalList.add(keyword.getKeyword());
			
		}
		return keywordsFinalList;
	}

	@Override
	public List<Firm> getFirmByName(String drilldownFirm)
	{
		return (firmDAO.findByName(drilldownFirm));
	}
}
