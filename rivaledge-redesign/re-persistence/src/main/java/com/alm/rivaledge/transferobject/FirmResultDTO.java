package com.alm.rivaledge.transferobject;

import java.io.Serializable;
import java.util.Date;

import com.alm.rivaledge.util.ALMConstants;

/**
 * The Data Transfer Object (DTO) that will be returned by the FirmService when
 * a search for news/publications/Tweets is run.
 * 
 * @author FL867
 * @since Sprint 1
 *  
 *  Change Log :  Date           User             Comments 
 *                -----------   -------------     ---------------------------
 *				  Aug 19 2013    Niranjan 	      FirmResultDTO no more implements Comparable , 
 *												  as this will change the order of the results,
 *												  which are already ordered at query level using order by clause
 */
public class FirmResultDTO implements Serializable //, Comparable<FirmResultDTO> 
{
	private static final long	serialVersionUID	= 7391717225098415706L;

	private Integer				entryId;			// Column 1
	private String				firmName;			// Column 2
	private String				dataType;			// Column 3
	private String				dataSource;			// Column 4
	private String				dataSourceURL;		// Column 5
	private String				entryTitle;			// Column 6
	private String				entryURL;			// Column 7
	private String				entryDescription;	// Column 8
	private String				practiceArea;		// Column 9
	private Date				entryDate;			// Column 10
	private String				firmURL;			// Column 11
	private Integer				firmId;				// Column 12

	/**
	 * This is the unique id (primary key) for a specific event (news item,
	 * publication, or tweet).
	 * 
	 * @return Integer
	 */
	public Integer getEntryId()
	{
		return entryId;
	}

	/**
	 * Indicates the type of event, whether a news item, publication, or Tweet.
	 * 
	 * @return
	 */
	public String getDataType()
	{
		return dataType;
	}

	public void setDataType(String dataType)
	{
		this.dataType = dataType;
	}

	/**
	 * Returns the web URL to the source. For example, for a news item posted on
	 * a company web site, the source would be the domain name of the said
	 * company.
	 * 
	 * @return
	 */
	public String getDataSourceURL()
	{
		return dataSourceURL;
	}

	public void setDataSourceURL(String dataSourceURL)
	{
		this.dataSourceURL = dataSourceURL;
	}

	/**
	 * The summary text or title for the news item, tweet, or publication.
	 * 
	 * @return
	 */
	public String getEntryTitle()
	{
		return entryTitle;
	}

	public void setEntryTitle(String entryTitle)
	{
		this.entryTitle = entryTitle;
	}

	/**
	 * The web URL that links to this news item, publication, or tweet.
	 * 
	 * @return
	 */
	public String getEntryURL()
	{
		return entryURL;
	}

	public void setEntryURL(String entryURL)
	{
		this.entryURL = entryURL;
	}

	public void setEntryId(Integer entryId)
	{
		this.entryId = entryId;
	}

	/**
	 * Returns the name of the law firm that is the subject of this news item,
	 * publication, or tweet. Eg., FEEDBURNER, DLA Piper, etc.
	 * 
	 * @return
	 */
	public String getFirmName()
	{
		return firmName;
	}

	public void setFirmName(String firmName)
	{
		this.firmName = firmName;
	}

	/**
	 * Returns the source where this news item, publication, or tweet
	 * originated.
	 * 
	 * @return
	 */
	public String getDataSource()
	{
		return dataSource;
	}

	public void setDataSource(String dataSource)
	{
		this.dataSource = dataSource;
	}

	/**
	 * Returns a detailed description of this news item, publication, or tweet.
	 * 
	 * @return
	 */
	public String getEntryDescription()
	{
		return entryDescription;
	}

	public void setEntryDescription(String entryDescription)
	{
		this.entryDescription = entryDescription;
	}

	/**
	 * Returns one of the standard practice areas that are referenced by this
	 * news item, publication, or tweet.
	 * 
	 * @return
	 */
	public String getPracticeArea()
	{
		return practiceArea;
	}

	public void setPracticeArea(String practiceArea)
	{
		this.practiceArea = practiceArea;
	}

	/**
	 * Returns the date on which this news item, publication, or tweet was
	 * posted.
	 * 
	 * @return
	 */
	public Date getEntryDate()
	{
		return entryDate;
	}

	public void setEntryDate(Date entryDate)
	{
		this.entryDate = entryDate;
	}

	public String getFirmURL()
	{
		return firmURL;
	}

	public void setFirmURL(String firmURL)
	{
		this.firmURL = firmURL;
	}

	public Integer getFirmId()
	{
		return firmId;
	}

	public void setFirmId(Integer firmId)
	{
		this.firmId = firmId;
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append(firmName);
		builder.append("||");
		builder.append((dataType != null) ? dataType : "\\N");
		builder.append("||");
		builder.append((dataSource != null) ? dataSource : "\\N");
		builder.append("||");
		builder.append((dataSourceURL != null) ? dataSourceURL : "\\N");
		builder.append("||");
		builder.append((entryTitle != null) ? entryTitle : "\\N");
		builder.append("||");
		builder.append((entryURL != null) ? entryURL : "\\N");
		builder.append("||");
		builder.append((entryDescription != null) ? entryDescription : "\\N");
		builder.append("||");
		builder.append((practiceArea != null) ? practiceArea : "\\N");
		builder.append("||");
		builder.append((entryDate != null) ? ALMConstants.SEARCH_DATE_FORMAT.format(entryDate) : "\\N");
		builder.append("||");
		builder.append((firmURL != null) ? firmURL : "\\N");
		builder.append("||");
		builder.append(firmId);
		builder.append("$$");
		return builder.toString();
	}

	/*@Override
	public int compareTo(FirmResultDTO that)
	{
		return (this.getEntryId().compareTo(that.getEntryId()));
	}

	@Override
	public boolean equals(Object that)
	{
		if (that != null)
		{
			return (this.getEntryId().equals(((FirmResultDTO) that).getEntryId()));
		}
		return (true);
	}*/
}
