package com.alm.rivaledge.transferobject;

import java.io.Serializable;
import java.util.Date;

import com.alm.rivaledge.util.ALMConstants;

public class PeopleCacheResult implements Serializable
{
	private static final long	serialVersionUID	= -2379909656284708689L;
	private Integer				groupingId;
	private Integer				attorneyId;
	private Integer				firmId;
	private String				staffType;
	private String				groupingType;
	private String				countType;
	private String				locationOrPractice;
	private String				lastAction;
	private Date				lastActionDate;

	public PeopleCacheResult()
	{
		super();
	}

	public PeopleCacheResult(AttorneyMoveChangesResultDTO amcrDTO, String groupingType, String countType)
	{
		super();
		this.attorneyId = amcrDTO.getAttorneyId();
		this.firmId 	= amcrDTO.getFirmId();
		this.staffType 	= amcrDTO.getTitle();
		this.countType 	= countType;
		if ((groupingType != null) && (groupingType.equalsIgnoreCase(PeopleResultDTO.LOCATION)))
		{
			this.locationOrPractice = amcrDTO.getLocation();
		}
		else
		{
			this.locationOrPractice = amcrDTO.getPractices();
		}
		this.groupingType 	= groupingType;
		this.lastAction 	= amcrDTO.getLastAction();
		this.lastActionDate	= amcrDTO.getLastActionDate();
	}

	public Integer getGroupingId()
	{
		return groupingId;
	}

	public void setGroupingId(Integer groupingId)
	{
		this.groupingId = groupingId;
	}

	public Integer getAttorneyId()
	{
		return attorneyId;
	}

	public void setAttorneyId(Integer attorneyId)
	{
		this.attorneyId = attorneyId;
	}

	public Integer getFirmId()
	{
		return firmId;
	}

	public void setFirmId(Integer firmId)
	{
		this.firmId = firmId;
	}

	public String getStaffType()
	{
		return staffType;
	}

	public void setStaffType(String staffType)
	{
		this.staffType = staffType;
	}

	public String getGroupingType()
	{
		return groupingType;
	}

	public void setGroupingType(String groupingType)
	{
		this.groupingType = groupingType;
	}

	public String getCountType()
	{
		return countType;
	}

	public void setCountType(String countType)
	{
		this.countType = countType;
	}

	public String getLocationOrPractice()
	{
		if ((locationOrPractice == null) || (locationOrPractice.trim().length() == 0) || (locationOrPractice.equalsIgnoreCase("N/A")))
		{
			if ((groupingType != null) && (groupingType.equals(PeopleResultDTO.LOCATION)))
			{
				return ("Location Not Available");
			}
			else
			{
				return ("Practice Area Not Available");
			}
		}
		if (locationOrPractice.equalsIgnoreCase("Location not identified"))
		{
			return ("Location Not Available");
		}
		return (locationOrPractice);
	}

	public void setLocationOrPractice(String locationOrPractice)
	{
		this.locationOrPractice = locationOrPractice;
	}

	public String getLastAction()
	{
		return lastAction;
	}

	public void setLastAction(String lastAction)
	{
		this.lastAction = lastAction;
	}

	public Date getLastActionDate()
	{
		return lastActionDate;
	}

	public void setLastActionDate(Date lastActionDate)
	{
		this.lastActionDate = lastActionDate;
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append((attorneyId != null) ? attorneyId : "\\N");
		builder.append("||");
		builder.append((firmId != null) ? firmId : "\\N");
		builder.append("||");
		builder.append((staffType != null) ? staffType : "\\N");
		builder.append("||");
		builder.append((groupingType != null) ? groupingType : "\\N");
		builder.append("||");
		builder.append((countType != null) ? countType : "\\N");
		builder.append("||");
		builder.append((locationOrPractice != null) ? locationOrPractice : "\\N");
		builder.append("||");
		builder.append((lastAction != null) ? lastAction : "\\N");
		builder.append("||");
		builder.append((lastActionDate != null) ? ALMConstants.SEARCH_DATE_FORMAT.format(lastActionDate) : "\\N");
		builder.append("$$");
		return builder.toString();
	}
}