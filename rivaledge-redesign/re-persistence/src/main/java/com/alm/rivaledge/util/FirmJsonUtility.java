package com.alm.rivaledge.util;


/**
 * This class will store firms information in a format 
 * which is required for Firm auto completer/ type ahead
 * 
 * @author FL605
 * @since Sprint 1
 */
public class FirmJsonUtility
{

    private String id;
    private String label;
    private String category;

    public FirmJsonUtility()
    {
    	
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getLabel()
    {
        return label;
    }

    public void setLabel(String label)
    {
        this.label = label;
    }

    public String getCategory()
    {
        return category;
    }

    public void setCategory(String category)
    {
        this.category = category;
    }
}
