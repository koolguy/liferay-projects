package com.alm.rivaledge.transferobject;

import java.io.Serializable;

import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;

/**
 * This is the DTO that will be passed back to the service that renders firms
 * available for selection in a user watch list of firms.
 * 
 * @author FL867
 * @since Sprint 3
 */
public class WatchListFirmResultDTO implements Serializable
{
	private static final long	serialVersionUID	= -3671931604618992024L;

	private Firm				watchlistCandidateFirm;
	private Integer				attorneyCount;
	private Integer				practiceCount;
	private Integer				locationCount;

	/**
	 * This represents an individual Firm that can be added to a user watchlist.
	 * 
	 * @return Firm
	 */
	public Firm getWatchlistCandidateFirm()
	{
		return watchlistCandidateFirm;
	}

	public void setWatchlistCandidateFirm(Firm watchlistCandidateFirm)
	{
		this.watchlistCandidateFirm = watchlistCandidateFirm;
	}

	/**
	 * Returns the number of attorneys (partners, attorneys, and other counsel).
	 * This does not include the admin and other staff.
	 * 
	 * @return
	 */
	public Integer getAttorneyCount()
	{
		return attorneyCount;
	}

	public void setAttorneyCount(Integer attorneyCount)
	{
		this.attorneyCount = attorneyCount;
	}

	/**
	 * Returns the number of unique practices that this firm engages in.
	 * @return
	 */
	public Integer getPracticeCount()
	{
		return practiceCount;
	}

	public void setPracticeCount(Integer practiceCount)
	{
		this.practiceCount = practiceCount;
	}

	/**
	 * Returns the number of unique locations where this firm has offices.
	 * @return
	 */
	public Integer getLocationCount()
	{
		return locationCount;
	}

	public void setLocationCount(Integer locationCount)
	{
		this.locationCount = locationCount;
	}
}