package com.alm.rivaledge.transferobject;

import java.io.Serializable;
import java.util.List;

/**
 * This class will store all the search criteria which user has entered on the
 * Watch list screen
 * 
 * @author FL605
 * @since Sprint 6
 */
public class WatchlistCriteriaSearchDTO implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6009059137725195542L;
	
	private List<String> firmSize;
	private List<String> practiceArea;
	private List<String> location;
	private String 		 firmFilterChar;
	
	public WatchlistCriteriaSearchDTO() 
	{
		
	}
	
	public List<String> getFirmSize() 
	{
		return firmSize;
	}
	
	public void setFirmSize(List<String> firmSize) 
	{
		this.firmSize = firmSize;
	}
	
	public List<String> getPracticeArea() 
	{
		return practiceArea;
	}
	
	public void setPracticeArea(List<String> practiceArea) 
	{
		this.practiceArea = practiceArea;
	}
	
	public List<String> getLocation() 
	{
		return location;
	}
	
	public void setLocation(List<String> location) 
	{
		this.location = location;
	}

	public String getFirmFilterChar() 
	{
		return firmFilterChar;
	}

	public void setFirmFilterChar(String firmFilterChar) 
	{
		this.firmFilterChar = firmFilterChar;
	}

	
	@Override
	public String toString() {
		return "WatchlistCriteriaSearchDTO [firmSize=" + firmSize
				+ " ||  practiceArea=" + practiceArea + " ||  location="
				+ location + " ||  firmFilterChar=" + firmFilterChar + "]";
	}

	@Override
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}
	
	

}
