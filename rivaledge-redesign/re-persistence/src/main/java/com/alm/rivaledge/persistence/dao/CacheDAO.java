package com.alm.rivaledge.persistence.dao;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alm.rivaledge.persistence.dao.lawma0_data.CacheMapper;
import com.alm.rivaledge.persistence.dao.lawma0_data.ThreadedFastFirmStatsSearch;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.transferobject.AttorneyGraduationResultDTO;
import com.alm.rivaledge.transferobject.AttorneyMoveChangesResultDTO;
import com.alm.rivaledge.transferobject.AttorneyMoveChangesSearchDTO;
import com.alm.rivaledge.transferobject.AttorneyResultDTO;
import com.alm.rivaledge.transferobject.AttorneySearchDTO;
import com.alm.rivaledge.transferobject.EventResultDTO;
import com.alm.rivaledge.transferobject.EventSearchDTO;
import com.alm.rivaledge.transferobject.FirmResultDTO;
import com.alm.rivaledge.transferobject.FirmSearchDTO;
import com.alm.rivaledge.transferobject.PeopleResult;
import com.alm.rivaledge.transferobject.PeopleResultDTO;
import com.alm.rivaledge.transferobject.PeopleSearchDTO;
import com.alm.rivaledge.transferobject.PeopleSubResult;
import com.alm.rivaledge.util.ALMConstants;

/**
 * Data Access Object that handles all persistence for the Cache service.
 * 
 * @author FL867
 * @since Sprint 1
 */
@Repository("cacheDAO")
public class CacheDAO
{
	@Autowired
	private CacheMapper	cacheMapper;

	@Autowired
	private FirmDAO		firmDAO;
	
	private static List<String> MASTER_PRACTICES = null;
	
	/**
	 * Bulk insert into the RER cache table for Firm news and pubs
	 * 
	 * @param localInfile
	 */
	public void bulkInsertNewsPubsTweets()
	{
		// Bulk insert
		cacheMapper.bulkInsertFirmNewsCache();

		cacheMapper.bulkInsertFirmPubsCache();

		cacheMapper.bulkInsertFirmTweetsCache();
	}

	/**
	 * Find the list of firm news and pubs that match the search criteria
	 * specified in the FirmSearchDTO.
	 * 
	 * @param fsDTO
	 * @return
	 */
	public List<FirmResultDTO> findFirmNewsPubs(FirmSearchDTO searchDTO)
	{
		// Prefix and suffix the keywords with % so that it works in a LIKE find
		FirmSearchDTO fsDTO = null;
		try
		{
			fsDTO = (FirmSearchDTO) searchDTO.clone();
		}
		catch (CloneNotSupportedException cnsEx)
		{
			fsDTO = searchDTO;
		}

		if (fsDTO.getKeyword() != null)
		{
			if (fsDTO.getKeyword().length() == 0)
			{
				fsDTO.setKeyword(null);
			}
			else
			{
				fsDTO.setKeyword("%" + fsDTO.getKeyword() + "%");
			}
		}

		// Prefix and suffix each practice with a % for the same reason as above
		if ((fsDTO.getPracticeArea() != null) && (!fsDTO.getPracticeArea().isEmpty()))
		{
			List<String> wildcardPracticeAreaList = new ArrayList<String>(fsDTO.getPracticeArea().size());
			for (String pArea : fsDTO.getPracticeArea())
			{
				wildcardPracticeAreaList.add("%" + pArea + "%");
			}
			fsDTO.setPracticeArea(wildcardPracticeAreaList);
		}

		// Reset the selectedFirms to null if it is empty
		if ((fsDTO.getSelectedFirms() != null) && (fsDTO.getSelectedFirms().isEmpty()))
		{
			fsDTO.setSelectedFirms(null);
		}

		// Call the SQL
		return (cacheMapper.findFirmNewsPubs(fsDTO));
	}
	
	/**
	 * Method returns list of top five publications by practice area 
	 * Specified in the FirmSearchDTO.
	 * 
	 * @param firmSearchDTO
	 * @return
	 */
	public List<FirmResultDTO> findFiveLatestPublicationsByPractice(FirmSearchDTO firmSearchDTO)
	{
		// Prefix and suffix the keywords with % so that it works in a LIKE find
		FirmSearchDTO fsDTO = null;
		try
		{
			fsDTO = (FirmSearchDTO) firmSearchDTO.clone();
		}
		catch (CloneNotSupportedException cnsEx)
		{
			fsDTO = firmSearchDTO;
		}

		// Prefix and suffix each practice with a % for the same reason as above
		if ((fsDTO.getPracticeArea() != null) && (!fsDTO.getPracticeArea().isEmpty()))
		{
			List<String> wildcardPracticeAreaList = new ArrayList<String>(fsDTO.getPracticeArea().size());
			for (String pArea : fsDTO.getPracticeArea())
			{
				wildcardPracticeAreaList.add("%" + pArea + "%");
			}
			fsDTO.setPracticeArea(wildcardPracticeAreaList);
		}

		// Call the SQL
		return (cacheMapper.findFiveLatestPublicationsByPractice(fsDTO));
	}

	/**
	 * Find the count (not the actual results) of firm news and pubs that match
	 * the search criteria specified in the FirmSearchDTO.
	 * 
	 * @param fsDTO
	 * @return
	 */
	public int findFirmDataCount(FirmSearchDTO fsDTO)
	{
		// Prefix and suffix the keywords with % so that it works in a LIKE find
		if (fsDTO.getKeyword() != null)
		{
			if (fsDTO.getKeyword().length() == 0)
			{
				fsDTO.setKeyword(null);
			}
			else
			{
				fsDTO.setKeyword("%" + fsDTO.getKeyword() + "%");
			}
		}

		// Prefix and suffix each practice with a % for the same reason as above
		if ((fsDTO.getPracticeArea() != null) && (!fsDTO.getPracticeArea().isEmpty()))
		{
			List<String> wildcardPracticeAreaList = new ArrayList<String>(fsDTO.getPracticeArea().size());
			for (String pArea : fsDTO.getPracticeArea())
			{
				wildcardPracticeAreaList.add("%" + pArea + "%");
			}
			fsDTO.setPracticeArea(wildcardPracticeAreaList);
		}

		// Reset the selectedFirms to null if it is empty
		if ((fsDTO.getSelectedFirms() != null) && (fsDTO.getSelectedFirms().isEmpty()))
		{
			fsDTO.setSelectedFirms(null);
		}
		return (cacheMapper.findFirmDataCount(fsDTO));
	}

	/**
	 * Finds all the attorneys for a given law firm. (Partner, Associate, Other
	 * Counsel, Admin, and Other only)
	 * 
	 * @param companyId
	 * @return
	 */
	public List<AttorneyResultDTO> findAllAttorneysByLawFirm(Integer companyId)
	{
		return (cacheMapper.findAllAttorneysByCompany(companyId));
	}

	public Integer findAllAttorneysCountByLawFirm(Integer companyId)
	{
		return (cacheMapper.findAllAttorneysCountByCompany(companyId));
	}

	public void bulkInsertAttorneyData()
	{
		// Run the bulk insert
		cacheMapper.bulkInsertAttorneyCache();

		// Follow it up with the bulk update
		cacheMapper.bulkUpdateAttorneyCache();
	}

	public void recreateAttorneyDataCache()
	{
		// Drop the existing table
		cacheMapper.dropAttorneyDataCacheTable();

		// Create the new table
		cacheMapper.createAttorneyDataCacheTable();
	}

	/**
	 * Finds the all the attorneys who meet the search criteria specified in the
	 * search DTO.
	 * 
	 * @param attorneySearchDTO
	 * @return
	 */
	public List<AttorneyResultDTO> findAttorneyData(AttorneySearchDTO asDTO)
	{
		AttorneySearchDTO attorneySearchDTO = massageAttorneySearchDTO(asDTO);
		 List<AttorneyResultDTO> finalResultsList= new ArrayList<AttorneyResultDTO>();
		//check whether  user has entered search filter based on Attorney name and its having more than one String  separated by space 
		if(attorneySearchDTO.getAttorneyName()!=null && attorneySearchDTO.getAttorneyName().size()>1)
		{
			finalResultsList = cacheMapper.findAttorneyData(attorneySearchDTO);
			List<Integer> attorneyList= new ArrayList<Integer>();
			for(AttorneyResultDTO attorneyResultDTO:finalResultsList)
			{
				attorneyList.add(attorneyResultDTO.getAttorneyId());
			}
			attorneySearchDTO.setAttorneyIdList(attorneyList);
			finalResultsList.addAll(cacheMapper.findAttorneyDataWithMutipleNameString(attorneySearchDTO));
		}
		else
		{
			//either user has not run a search on Attorney name or 
			//has entered only one string for Attorney name filter
			finalResultsList = cacheMapper.findAttorneyData(attorneySearchDTO);
		}
		return finalResultsList;
	}

	public int findAttorneyDataCount(AttorneySearchDTO asDTO)
	{
		int totalResults=0;
		AttorneySearchDTO attorneySearchDTO =massageAttorneySearchDTO(asDTO);
		//check whether  user has entered search filter based on Attorney name and its having more than one String  separated by space 
		if(attorneySearchDTO.getAttorneyName()!=null && attorneySearchDTO.getAttorneyName().size()>1)
		{			
			List<AttorneyResultDTO> initialResult=cacheMapper.findAttorneyDataCount(attorneySearchDTO);
			List<Integer> attorneyList= new ArrayList<Integer>();
			if(initialResult!=null && initialResult.size() > ALMConstants.MAX_RESULT_SIZE_SUBSCRIBER)
			{
				initialResult=initialResult.subList(0, ALMConstants.MAX_RESULT_SIZE_SUBSCRIBER-1);
			}			
			for(AttorneyResultDTO attorneyResultDTO : initialResult)
			{
				attorneyList.add(attorneyResultDTO.getAttorneyId());
			}
			attorneySearchDTO.setAttorneyIdList(attorneyList);
			totalResults=initialResult.size() + cacheMapper.findAttorneyDataCountWithMutipleNameString(attorneySearchDTO);
		}
		else
		{
			//either user has not run a search on Attorney name or 
			//has entered only one string for Attorney name filter
			totalResults = cacheMapper.findAttorneyDataCount(attorneySearchDTO).size();
		}
		
		//cacheMapper.findAttorneyDataCount(massageAttorneySearchDTO(asDTO))+ 1;
		
		return totalResults;
	}

	public List<PeopleResultDTO> findFirmStatisticsWithoutDrilldown(PeopleSearchDTO psDTO)
	{
		List<PeopleResultDTO> returnList = new ArrayList<PeopleResultDTO>();
		PeopleSearchDTO 	  searchDTO  = null;
		try
		{
			searchDTO = (PeopleSearchDTO) psDTO.clone();
		}
		catch (CloneNotSupportedException cnsEx)
		{
			searchDTO = psDTO;
		}
		searchDTO.setOrderBy(1);
		searchDTO.setSortColumn(1);
		searchDTO.setPageNumber(0);
		
		searchDTO.setDynamicSearchColumn("firm_name");
		
		// Merge both maps to construct the final return list
		// currentMap 	= head counts for current attorneys
		// movementsMap = head counts for added/removed attorneys
		// movementsMap.keySet() will always be a subset of currentMap.keySet()
		Map<Integer, PeopleResult> currentMap 	= new HashMap<Integer, PeopleResult>();
		Map<Integer, PeopleResult> movementsMap = new HashMap<Integer, PeopleResult>();
		
		for (PeopleResult thisResult : cacheMapper.findFirmStatistics(searchDTO))
		{
			currentMap.put(thisResult.getGroupingId(), thisResult);
		}
		
		if (!currentMap.isEmpty())
		{
			for (PeopleResult thisResult : cacheMapper.findFirmStatisticsMovements(searchDTO))
			{
				movementsMap.put(thisResult.getGroupingId(), thisResult);
			}
			
			// Iterate over the currentMap, creating instances of 
			// PeopleResultDTO as we go along
			// We will also be doing the locations and practices
			for (Integer thisFirmKey : currentMap.keySet())
			{
				PeopleResultDTO newResult = new PeopleResultDTO();
				newResult.setCompanyId(thisFirmKey);
				newResult.setCompanyName(currentMap.get(thisFirmKey).getSearchTitle());
				newResult.setCompanyWebsite(currentMap.get(thisFirmKey).getGroupType());
				
				if (movementsMap.containsKey(thisFirmKey))
				{
					// Merge this record into the other one
					newResult.setCompanyPeople(currentMap.get(thisFirmKey).mergeMovements(movementsMap.get(thisFirmKey), PeopleResultDTO.FIRM));
				}
				else
				{
					PeopleResult summaryResult = currentMap.get(thisFirmKey); 
					summaryResult.setGroupType(PeopleResultDTO.FIRM);
					newResult.setCompanyPeople(summaryResult);
				}
				returnList.add(newResult);
			}
		}
		return (returnList);
	}
	
	public List<PeopleResultDTO> findFirmStatisticsVeryFast(PeopleSearchDTO searchDTO)
	{
		// Get the basic result list;
		// This will contain all the head counts, but will not
		// contain any movement counts
		List<PeopleResultDTO> resultList = getBasicHeadcounts(searchDTO); 
		List<PeopleResultDTO> returnList = new CopyOnWriteArrayList<PeopleResultDTO>(); 
		PeopleSearchDTO 	  psDTO  	 = null;
		for (int outerLoopCounter = 0; outerLoopCounter < resultList.size(); outerLoopCounter += 10)
		{
			List<Callable<PeopleResultDTO>> allTasks 	 = new ArrayList<Callable<PeopleResultDTO>>();
			ExecutorService 				taskExecutor = Executors.newCachedThreadPool();
			for (int innerLoopCounter = outerLoopCounter; innerLoopCounter < (outerLoopCounter + 10); innerLoopCounter++)
			{
				if (innerLoopCounter >= resultList.size())
				{
					break;
				}
				try
				{
					psDTO = (PeopleSearchDTO) searchDTO.clone();
				}
				catch (CloneNotSupportedException cnsEx)
				{
					psDTO = null;
				}
				
				if (psDTO != null)
				{
					// Set this single firm into the search criteria clone
					List<Firm> 	singleFirmList 	= new ArrayList<Firm>(1);
					Firm		thisFirm		= new Firm();
					thisFirm.setCompanyId(resultList.get(innerLoopCounter).getCompanyId());
					singleFirmList.add(thisFirm);
					psDTO.setSelectedFirms(singleFirmList);
					
					// Create a new instance of the threaded executor class
					ThreadedFastFirmStatsSearch newSearch = new ThreadedFastFirmStatsSearch(resultList.get(innerLoopCounter), psDTO, cacheMapper, true);
					allTasks.add(newSearch);
				}
			}
			List<Future<PeopleResultDTO>> taskResults = null;
			try
			{
				// Launch all threads simultaneously
				taskResults = taskExecutor.invokeAll(allTasks);
				for (Future<PeopleResultDTO> thisResult : taskResults)
				{
					while (!thisResult.isDone())
					{
						// Do nothing; just wait
					}
					
					PeopleResultDTO result = thisResult.get();
					if (result != null)
					{
						returnList.add(result);
					}
				}
				taskExecutor.shutdown();
				taskExecutor.awaitTermination(5, TimeUnit.MILLISECONDS);
			}
			catch (InterruptedException iEx)
			{
				iEx.printStackTrace();
			}
			catch (ExecutionException eEx)
			{
				eEx.printStackTrace();
			}
		}
		
		return (returnList);
	}
	
	public List<PeopleResultDTO> findFirmStatisticsVeryFastWithoutDrilldown(PeopleSearchDTO searchDTO)
	{
		// Get the basic result list;
		// This will contain all the head counts, but will not
		// contain any movement counts
		List<PeopleResultDTO> 	resultList 	 = new ArrayList<PeopleResultDTO>();
		List<PeopleResult> 		basicResults = cacheMapper.findBasicHeadCounts(searchDTO);
		if ((basicResults != null) && (!basicResults.isEmpty()))
		{
			for (PeopleResult thisResult : basicResults)
			{
				PeopleResultDTO newDTO = new PeopleResultDTO();
				newDTO.setCompanyId(thisResult.getGroupingId());
				newDTO.setCompanyName(thisResult.getSearchTitle());
				newDTO.setCompanyWebsite(thisResult.getGroupType());

				// This is the overall firm count
				newDTO.setCompanyPeople(thisResult);
				
				resultList.add(newDTO);
			}
		}
		
		List<PeopleResultDTO> returnList = new CopyOnWriteArrayList<PeopleResultDTO>(); 
		PeopleSearchDTO 	  psDTO  	 = null;
		for (int outerLoopCounter = 0; outerLoopCounter < resultList.size(); outerLoopCounter += 10)
		{
			List<Callable<PeopleResultDTO>> allTasks 	 = new ArrayList<Callable<PeopleResultDTO>>();
			ExecutorService 				taskExecutor = Executors.newCachedThreadPool();
			for (int innerLoopCounter = outerLoopCounter; innerLoopCounter < (outerLoopCounter + 10); innerLoopCounter++)
			{
				if (innerLoopCounter >= resultList.size())
				{
					break;
				}
				try
				{
					psDTO = (PeopleSearchDTO) searchDTO.clone();
				}
				catch (CloneNotSupportedException cnsEx)
				{
					psDTO = null;
				}
				
				if (psDTO != null)
				{
					// Set this single firm into the search criteria clone
					List<Firm> 	singleFirmList 	= new ArrayList<Firm>(1);
					Firm		thisFirm		= new Firm();
					thisFirm.setCompanyId(resultList.get(innerLoopCounter).getCompanyId());
					singleFirmList.add(thisFirm);
					psDTO.setSelectedFirms(singleFirmList);
					
					// Create a new instance of the threaded executor class
					ThreadedFastFirmStatsSearch newSearch = new ThreadedFastFirmStatsSearch(resultList.get(innerLoopCounter), psDTO, cacheMapper, false);
					allTasks.add(newSearch);
				}
			}
			List<Future<PeopleResultDTO>> taskResults = null;
			try
			{
				// Launch all threads simultaneously
				taskResults = taskExecutor.invokeAll(allTasks);
				for (Future<PeopleResultDTO> thisResult : taskResults)
				{
					while (!thisResult.isDone())
					{
						// Do nothing; just wait
					}
					
					PeopleResultDTO result = thisResult.get();
					if (result != null)
					{
						returnList.add(result);
					}
				}
				taskExecutor.shutdown();
				taskExecutor.awaitTermination(5, TimeUnit.MILLISECONDS);
			}
			catch (InterruptedException iEx)
			{
				iEx.printStackTrace();
			}
			catch (ExecutionException eEx)
			{
				eEx.printStackTrace();
			}
		}
		
		return (returnList);
	}

	
	private List<PeopleResultDTO> getBasicHeadcounts(PeopleSearchDTO searchDTO)
	{
		// Find head counts from tbl_rer_cache_attorney_data
		List<PeopleResult> 				basicResults 	= cacheMapper.findBasicHeadCounts(searchDTO);
		Map<Integer, PeopleResultDTO> 	returnMap 		= new HashMap<Integer, PeopleResultDTO>();
		if ((basicResults != null) && (!basicResults.isEmpty()))
		{
			for (PeopleResult thisResult : basicResults)
			{
				PeopleResultDTO newDTO = new PeopleResultDTO();
				newDTO.setCompanyId(thisResult.getGroupingId());
				newDTO.setCompanyName(thisResult.getSearchTitle());
				newDTO.setCompanyWebsite(thisResult.getGroupType());

				// This is the overall firm count
				newDTO.setCompanyPeople(thisResult);
				
				// Plug back into the map
				returnMap.put(thisResult.getGroupingId(), newDTO);
			}
		}
		
		// Find location and practice counts from tbl_rer_cache_headcounts
		basicResults = cacheMapper.findLocationPracticeHeadCounts(searchDTO);
		if ((basicResults != null) && (!basicResults.isEmpty()))
		{
			for (PeopleResult thisResult : basicResults)
			{
				PeopleResultDTO newDTO = returnMap.get(thisResult.getGroupingId()); 
				if (newDTO != null)
				{
					// This is either a location or a practice
					if (MASTER_PRACTICES == null)
					{
						MASTER_PRACTICES = cacheMapper.findMasterPractices();
					}
					if (MASTER_PRACTICES.contains(thisResult.getSearchPractices()))
					{
						thisResult.setSearchTitle(thisResult.getSearchPractices());
						newDTO.addPractice(thisResult);
					}
					else
					{
						thisResult.setSearchTitle(thisResult.getSearchLocations());
						newDTO.addLocation(thisResult);
					}
					
					// Plug back into the map
					returnMap.put(thisResult.getGroupingId(), newDTO);
				}
			}
		}
		
		return (new ArrayList<PeopleResultDTO>(returnMap.values()));
	}

	/**
	 * Find the PeopleResultsDTOs that match the PeopleSearchDTO
	 */
	public List<PeopleResultDTO> findFirmStatistics(PeopleSearchDTO searchDTO)
	{
		List<PeopleResultDTO> returnList = new CopyOnWriteArrayList<PeopleResultDTO>(); 
		PeopleSearchDTO 	  psDTO  	 = null;
		List<Firm> 			  allFirms 	 = null;
		if ((searchDTO.getSelectedFirms() == null) || (searchDTO.getSelectedFirms().isEmpty()))
		{
			// User has not selected any specific firm,
			// so the search has to be run against ALL firms.
			allFirms = firmDAO.findAllFirms();
		}
		else
		{
			allFirms = searchDTO.getSelectedFirms();
		}
		for (int outerLoopCounter = 0; outerLoopCounter < allFirms.size(); outerLoopCounter += 10)
		{
			List<Callable<PeopleResultDTO>> allTasks = new ArrayList<Callable<PeopleResultDTO>>();
			ExecutorService 				taskExecutor = Executors.newCachedThreadPool();
			for (int innerLoopCounter = outerLoopCounter; innerLoopCounter < (outerLoopCounter + 10); innerLoopCounter++)
			{
				if (innerLoopCounter >= allFirms.size())
				{
					break;
				}
				try
				{
					psDTO = (PeopleSearchDTO) searchDTO.clone();
				}
				catch (CloneNotSupportedException cnsEx)
				{
					psDTO = searchDTO;
				}
				
				// Set this single firm into the search criteria clone
				List<Firm> singleFirmList = new ArrayList<Firm>(1);
				singleFirmList.add(allFirms.get(innerLoopCounter));
				psDTO.setSelectedFirms(singleFirmList);
				
				// Create a new instance of the threaded executor class
				ThreadedFirmStatsSearch newSearch = new ThreadedFirmStatsSearch(psDTO, cacheMapper);
				allTasks.add(newSearch);
			}
			List<Future<PeopleResultDTO>> taskResults = null;
			try
			{
				// Launch all threads simultaneously
				taskResults = taskExecutor.invokeAll(allTasks);
				for (Future<PeopleResultDTO> thisResult : taskResults)
				{
					while (!thisResult.isDone())
					{
						// Do nothing Just wait
					}
					
					PeopleResultDTO result = thisResult.get();
					if(result != null)
					{
						returnList.add(result);
						
					}
				}
				taskExecutor.shutdown();
				taskExecutor.awaitTermination(5, TimeUnit.MILLISECONDS);
			}
			catch (InterruptedException iEx)
			{
				iEx.printStackTrace();
			}
			catch (ExecutionException eEx)
			{
				eEx.printStackTrace();
			}
		}
		return (returnList);
	}

	public Integer findFirmStatisticsDataCount(PeopleSearchDTO psDTO)
	{
		List<PeopleResult> allResults = (cacheMapper.findBasicHeadCounts(psDTO));
		if (allResults != null)
		{
			return (allResults.size());
		}
		return (0);
	}

	public void dropFirmNewsPubsCacheTable()
	{
		cacheMapper.dropFirmNewsPubsCacheTable();
	}

	public void createFirmNewsPubsCacheTable()
	{
		cacheMapper.createFirmNewsPubsCacheTable();
	}

	/**
	 * Given a firm id, returns a list of all the attorneys for that firm who
	 * have been cached in the attorney_data cache.
	 * 
	 * @return
	 */
	public List<AttorneyResultDTO> findAttorneysByFirm(Integer firmId)
	{
		return (cacheMapper.findAttorneysByFirmFromCache(firmId));
	}

	public void dropPeopleSearchCacheTables()
	{
		cacheMapper.dropFirmStatisticsHeadCountCacheTable();
		cacheMapper.dropFirmAttorneyPracticesTable();
//		cacheMapper.dropFirmStatisticsCacheDTOTable();
		// cacheMapper.dropFirmStatisticsCacheResultTable();
	}

	public void createPeopleSearchCacheTables()
	{
		cacheMapper.createFirmStatisticsHeadCountCacheTable();
		cacheMapper.createFirmAttorneyPracticesTable();
//		cacheMapper.createFirmStatisticsCacheDTOTable();
		// cacheMapper.createFirmStatisticsCacheResultTable();
	}

	/**
	 * Persist an instance of PeopleResultDTO to the cache table.
	 * 
	 * @param thisResult
	 */
	public Integer insertPeopleSearchResultDTO(PeopleResultDTO thisResult)
	{
		return (cacheMapper.insertPeopleResultDTO(thisResult));
	}

	public Integer findGroupIdByFirmId(Integer companyId)
	{
		return (cacheMapper.findGroupIdByFirmId(companyId));
	}

	/**
	 * Persist an instance of PeopleResult to the cache table.
	 * 
	 * @param thisResult
	 */
	public void insertPeopleResult(PeopleResult pResult)
	{
		cacheMapper.insertPeopleResult(pResult);
	}

	public List<Integer> findFirmIdsFromAttorneyData()
	{
		return (cacheMapper.findFirmIdsFromAttorneyDataCache());
	}

	public PeopleResultDTO findMovementsByFirm(Integer companyId)
	{
		return (cacheMapper.findPeopleResultDTOByFirm(companyId));
	}

	public Integer findGroupIdByCollection(String collectionName)
	{
		return (cacheMapper.findGroupIdByCollectionName(collectionName));
	}

	public List<PeopleResult> findPeopleResultByGroupId(Integer groupId)
	{
		return (cacheMapper.findPeopleResultByGroupId(groupId));
	}

	/**
	 * Drops the event cache table.
	 */
	public void dropEventsCacheTable()
	{
		cacheMapper.dropEventsCacheTable();
	}

	public List<EventResultDTO> findAllLawFirmEvents(int offset)
	{
		return (cacheMapper.findAllLawFirmEvents(offset));
	}

	public List<EventResultDTO> findAllOrganisationEvents(int offset)
	{
		return (cacheMapper.findAllOrganisationEvents(offset));
	}

	/**
	 * Creates the events cache table
	 */
	public void createEventsCacheTable()
	{
		cacheMapper.createEventsCacheTable();
	}

	public void bulkInsertEventCacheData()
	{
		cacheMapper.bulkInsertLawFirmEventsCache();

		cacheMapper.bulkInsertOrganisationsEventsCache();
	}

	/**
	 * Given a EventSearchDTO, returns all matching event results
	 */
	public List<EventResultDTO> findEventData(EventSearchDTO esDTO)
	{
		return (cacheMapper.findEventData(massageEventSearchDTO(esDTO)));
	}

	/**
	 * Given a EventSearchDTO, returns count of matching event results
	 * 
	 * @param esDTO
	 * @return
	 */
	public Integer findEventDataCount(EventSearchDTO esDTO)
	{
		return (cacheMapper.findEventDataCount(massageEventSearchDTO(esDTO)));
	}

	private EventSearchDTO massageEventSearchDTO(EventSearchDTO esDTO)
	{
		// Prefix and suffix the keywords with % so that it works in a LIKE find
		if (esDTO.getKeyword() != null)
		{
			if (esDTO.getKeyword().length() == 0)
			{
				esDTO.setKeyword(null);
			}
			else
			{
				esDTO.setKeyword("%" + esDTO.getKeyword() + "%");
			}
		}

		// Prefix and suffix each practice and location with a % for the same
		// reason as above
		if ((esDTO.getPracticeArea() != null) && (!esDTO.getPracticeArea().isEmpty()))
		{
			List<String> wildcardPracticeAreaList = new ArrayList<String>(esDTO.getPracticeArea().size());
			for (String pArea : esDTO.getPracticeArea())
			{
				wildcardPracticeAreaList.add("%" + pArea + "%");
			}
			esDTO.setPracticeArea(wildcardPracticeAreaList);
		}
		if ((esDTO.getLocations() != null) && (!esDTO.getLocations().isEmpty()))
		{
			List<String> wildcardLocationsList = new ArrayList<String>(esDTO.getLocations().size());
			for (String tLoc : esDTO.getLocations())
			{
				wildcardLocationsList.add("%" + tLoc + "%");
			}
			esDTO.setLocations(wildcardLocationsList);
		}

		// Reset the selectedFirms to null if it is empty
		if ((esDTO.getSelectedFirmsOrganizations() != null) && (esDTO.getSelectedFirmsOrganizations().isEmpty()))
		{
			esDTO.setSelectedFirmsOrganizations(null);
		}

		return (esDTO);
	}

	private AttorneySearchDTO massageAttorneySearchDTO(AttorneySearchDTO searchDTO)
	{
		AttorneySearchDTO attorneySearchDTO = null;
		try
		{
			attorneySearchDTO = (AttorneySearchDTO) searchDTO.clone();
		}
		catch (CloneNotSupportedException cnsEx)
		{
			attorneySearchDTO = searchDTO;
		}

		// Reset the selectedFirms to null if it is empty
		if ((attorneySearchDTO.getSelectedFirms() != null) && (attorneySearchDTO.getSelectedFirms().isEmpty()))
		{
			attorneySearchDTO.setSelectedFirms(null);
		}

		// Prefix practices with a %
		if ((attorneySearchDTO.getPracticeArea() != null) && (!attorneySearchDTO.getPracticeArea().isEmpty()))
		{
			List<String> wildcardedPracticeAreas = new ArrayList<String>(attorneySearchDTO.getPracticeArea().size());
			for (String practiceArea : attorneySearchDTO.getPracticeArea())
			{
				wildcardedPracticeAreas.add("%" + practiceArea + "%");
			}
			attorneySearchDTO.setPracticeArea(wildcardedPracticeAreas);
		}

		// Prefix attorney name with %
		if ((attorneySearchDTO.getAttorneyName() != null) && (attorneySearchDTO.getAttorneyName().size() > 0))
		{
			List<String> wildcardedAttorneyName = new ArrayList<String>(attorneySearchDTO.getAttorneyName().size());
			for (String attorneyName : attorneySearchDTO.getAttorneyName())
			{
				//Attorney name filter would only be applicable when user has entered string having more than three characters  
				if(attorneyName.length() >3)
				{
					wildcardedAttorneyName.add("%" + attorneyName + "%");
				}
				
			}
			attorneySearchDTO.setAttorneyName(wildcardedAttorneyName);
			
			
			//attorneySearchDTO.setAttorneyName("%" + attorneySearchDTO.getAttorneyName() + "%");
		}

		// Prefix law school text with %
		if ((attorneySearchDTO.getLawSchooltext() != null) && (attorneySearchDTO.getLawSchooltext().length() > 0))
		{
			attorneySearchDTO.setLawSchooltext("%" + attorneySearchDTO.getLawSchooltext() + "%");
		}

		// Prefix undergraduate school text with %
		if ((attorneySearchDTO.getUndergraduateSchoolText() != null) && (attorneySearchDTO.getUndergraduateSchoolText().length() > 0))
		{
			attorneySearchDTO.setUndergraduateSchoolText("%" + attorneySearchDTO.getUndergraduateSchoolText() + "%");
		}

		// Prefix keywords with %
		if ((attorneySearchDTO.getKeywordsInBioText() != null) && (attorneySearchDTO.getKeywordsInBioText().length() > 0))
		{
			attorneySearchDTO.setKeywordsInBioText("%" + attorneySearchDTO.getKeywordsInBioText() + "%");
		}

		// Prefix admissions with %
		if ((attorneySearchDTO.getAdmissions() != null) && (attorneySearchDTO.getAdmissions().length() > 0))
		{
			attorneySearchDTO.setAdmissions("%" + attorneySearchDTO.getAdmissions() + "%");
		}
		
		if(attorneySearchDTO.getGraduationStartYear() !=null && attorneySearchDTO.getGraduationStartYear().equals(ALMConstants.ANY))
		{
			attorneySearchDTO.setGraduationStartYear(null);
		}
		if(attorneySearchDTO.getGraduationEndYear() !=null && attorneySearchDTO.getGraduationEndYear().equals(ALMConstants.ANY))
		{
			attorneySearchDTO.setGraduationEndYear(null);
		}


		return (attorneySearchDTO);
	}

	private AttorneyMoveChangesSearchDTO massageAttorneyMoveChangesSearchDTO(AttorneyMoveChangesSearchDTO searchDTO)
	{
		AttorneyMoveChangesSearchDTO attorneyMovesDTO = null;
		try
		{
			attorneyMovesDTO = (AttorneyMoveChangesSearchDTO) searchDTO.clone();
		}
		catch (CloneNotSupportedException cnsEx)
		{
			attorneyMovesDTO = searchDTO;
		}

		// Reset the selectedFirms to null if it is empty
		if ((attorneyMovesDTO.getSelectedFirms() != null) && (attorneyMovesDTO.getSelectedFirms().isEmpty()))
		{
			attorneyMovesDTO.setSelectedFirms(null);
		}

		// Prefix practices with a %
		if ((attorneyMovesDTO.getPracticeArea() != null) && (!attorneyMovesDTO.getPracticeArea().isEmpty()))
		{
			List<String> wildcardedPracticeAreas = new ArrayList<String>(attorneyMovesDTO.getPracticeArea().size());
			for (String practiceArea : attorneyMovesDTO.getPracticeArea())
			{
				wildcardedPracticeAreas.add("%" + practiceArea + "%");
			}
			attorneyMovesDTO.setPracticeArea(wildcardedPracticeAreas);
		}

		if ((attorneyMovesDTO.getChangeType() != null) && (!attorneyMovesDTO.getChangeType().isEmpty()))
		{
			List<String> changeTypes = new ArrayList<String>(attorneyMovesDTO.getChangeType().size());
			for (String changeType : attorneyMovesDTO.getChangeType())
			{
				if (changeType != null && changeType.equals(ALMConstants.ADDITIONS))
				{
					changeTypes.add("added");
				}
				else if (changeType != null && changeType.equals(ALMConstants.REMOVALS))
				{
					changeTypes.add("removed");
				}
				else if (changeType != null && changeType.equals(ALMConstants.UPDATES))
				{
					changeTypes.add("updated");
				}
				else
				{
					changeTypes.add(changeType);
				}

			}
			attorneyMovesDTO.setChangeType(changeTypes);
		}

		// Prefix attorney name with %
		if ((attorneyMovesDTO.getAttorneyName() != null) && (attorneyMovesDTO.getAttorneyName().size() > 0))
		{
			List<String> wildcardedAttorneyName = new ArrayList<String>(attorneyMovesDTO.getAttorneyName().size());
			for (String attorneyName : attorneyMovesDTO.getAttorneyName())
			{
				//Attorney name filter would only be applicable when user has entered string having more than three characters  
				if(attorneyName.length() >3)
				{
					wildcardedAttorneyName.add("%" + attorneyName + "%");
				}
				
			}
			attorneyMovesDTO.setAttorneyName(wildcardedAttorneyName);
			
			
			//attorneySearchDTO.setAttorneyName("%" + attorneySearchDTO.getAttorneyName() + "%");
		}

		// Prefix keywords with %
		if ((attorneyMovesDTO.getKeywordsInBioText() != null) && (attorneyMovesDTO.getKeywordsInBioText().length() > 0))
		{
			attorneyMovesDTO.setKeywordsInBioText("%" + attorneyMovesDTO.getKeywordsInBioText() + "%");
		}
		return (attorneyMovesDTO);
	}

	// Find current firm stats by company, but not moves/changes
	public List<PeopleSubResult> findFirmStatisticsForCacheByCompanyId(Integer firmId)
	{
		return (cacheMapper.findFirmStatsForCacheByCompanyId(firmId));
	}

	public void recreateAttorneyMovesChangesCache()
	{
		// Drop the existing table
		cacheMapper.dropAttorneyMovesChangesCacheTable();

		// Create the new table
		cacheMapper.createAttorneyMovesChangesCacheTable();
	}

	public void bulkInsertAttorneyMovesChanges()
	{
		// Run the bulk insert
		cacheMapper.bulkInsertAttorneyMovesChangesCache();
	}

	public List<AttorneyMoveChangesResultDTO> findAttorneyMovementsCount(AttorneyMoveChangesSearchDTO searchDTO)
	{
		return cacheMapper.findAttorneyMovementsCount(massageAttorneyMoveChangesSearchDTO(searchDTO));
	}
	
	public int findAttorneyMovementsCountWithMutipleNameString(AttorneyMoveChangesSearchDTO searchDTO)
	{
		return cacheMapper.findAttorneyMovementsCountWithMutipleNameString(massageAttorneyMoveChangesSearchDTO(searchDTO));
	}

	public List<AttorneyMoveChangesResultDTO> findAttorneyMovementsData(AttorneyMoveChangesSearchDTO searchDTO)
	{
		List<AttorneyMoveChangesResultDTO> attorneyMovesResult=cacheMapper.findAttorneyMovementsData(massageAttorneyMoveChangesSearchDTO(searchDTO));
		Firm  firm =new Firm();
		for(AttorneyMoveChangesResultDTO attorneyMoveChangesResultDTO : attorneyMovesResult)
		{
			if(attorneyMoveChangesResultDTO.getMovedFrom()!=null && !StringUtils.isEmpty(attorneyMoveChangesResultDTO.getMovedFrom()))
			{
				firm=firmDAO.findByFirmName(attorneyMoveChangesResultDTO.getMovedFrom());
				if(firm!=null)
				{
					attorneyMoveChangesResultDTO.setMovedFromFirmLink(firm.getHomeUrl());
				}
			}
			if(attorneyMoveChangesResultDTO.getMovedTo()!=null && !StringUtils.isEmpty(attorneyMoveChangesResultDTO.getMovedTo()))
			{
				firm=firmDAO.findByFirmName(attorneyMoveChangesResultDTO.getMovedTo());
				if(firm!=null)
				{
					attorneyMoveChangesResultDTO.setMovedToFirmLink(firm.getHomeUrl());
				}
				
			}
		}
		return attorneyMovesResult;
	}
	
	public List<AttorneyMoveChangesResultDTO> findAttorneyMovementsDataWithMutipleNameString(AttorneyMoveChangesSearchDTO searchDTO)
	{
		List<AttorneyMoveChangesResultDTO> attorneyMovesResult=cacheMapper.findAttorneyMovementsDataWithMutipleNameString(massageAttorneyMoveChangesSearchDTO(searchDTO));
		Firm  firm =new Firm();
		for(AttorneyMoveChangesResultDTO attorneyMoveChangesResultDTO : attorneyMovesResult)
		{
			if(attorneyMoveChangesResultDTO.getMovedFrom()!=null && !StringUtils.isEmpty(attorneyMoveChangesResultDTO.getMovedFrom()))
			{
				firm=firmDAO.findByFirmName(attorneyMoveChangesResultDTO.getMovedFrom());
				if(firm!=null)
				{
					attorneyMoveChangesResultDTO.setMovedFromFirmLink(firm.getHomeUrl());
				}
			}
			if(attorneyMoveChangesResultDTO.getMovedTo()!=null && !StringUtils.isEmpty(attorneyMoveChangesResultDTO.getMovedTo()))
			{
				firm=firmDAO.findByFirmName(attorneyMoveChangesResultDTO.getMovedTo());
				if(firm!=null)
				{
					attorneyMoveChangesResultDTO.setMovedToFirmLink(firm.getHomeUrl());
				}
				
			}
		}
		return attorneyMovesResult;
	}

	public List<AttorneyResultDTO> findAttorneyDataForcharts(AttorneySearchDTO attorneySearchDTO)
	{
		return cacheMapper.findAttorneyDataForcharts(massageAttorneySearchDTO(attorneySearchDTO));
	}

	public List<AttorneyGraduationResultDTO> findAttorneyDataForGradcharts(AttorneySearchDTO attorneySearchDTO)
	{
		return cacheMapper.findAttorneyDataForGradcharts(massageAttorneySearchDTO(attorneySearchDTO));
	}

	public List<String> findMasterPractices()
	{
		return (cacheMapper.findMasterPractices());
	}

	public void bulkInsertFirmStatistics(File firmStatsCacheFile)
	{
		cacheMapper.bulkInsertFirmStatistics(firmStatsCacheFile.toString());
		cacheMapper.bulkInsertFirmStatisticsHeadCountCache();
		cacheMapper.bulkInsertFirmAttorneyPracticeCache();
	}

	public List<Integer> findAllFirmsWithAttorneysInCache()
	{
		return (cacheMapper.findAllAttorneyFirmsFromCache());
	}
}