package com.alm.rivaledge.persistence.domain.lawma0_data;

import java.io.Serializable;

/**
 * Represents a user's search preferences for each portlet as a JSON string.
 * 
 * @author FL867
 * @version 1.0
 * @since Sprint 9.2
 */
public class UserPreferences implements Serializable
{
	private static final long	serialVersionUID	= -8621701646982332470L;
	private Integer				mappingId;
	private Integer				userId;
	private String				portletId;
	private String				searchCriteriaJSON;
	private String				chartCriteriaJSON;
	private String				applicablePage;
	
	public boolean isValid()
	{
		if ((getUserId() != null) && (getPortletId() != null) && (getApplicablePage() != null))
		{
			return (true);
		}
		return (false);
	}

	public Integer getMappingId()
	{
		return mappingId;
	}

	public void setMappingId(Integer mappingId)
	{
		this.mappingId = mappingId;
	}

	public Integer getUserId()
	{
		return userId;
	}

	public void setUserId(Integer userId)
	{
		this.userId = userId;
	}

	public String getPortletId()
	{
		return portletId;
	}

	public void setPortletId(String portletId)
	{
		this.portletId = portletId;
	}

	public String getSearchCriteriaJSON()
	{
		return searchCriteriaJSON;
	}

	public void setSearchCriteriaJSON(String searchCriteriaJSON)
	{
		this.searchCriteriaJSON = searchCriteriaJSON;
	}

	public String getChartCriteriaJSON()
	{
		return chartCriteriaJSON;
	}

	public void setChartCriteriaJSON(String chartCriteriaJSON)
	{
		this.chartCriteriaJSON = chartCriteriaJSON;
	}

	public String getApplicablePage()
	{
		return applicablePage;
	}

	public void setApplicablePage(String applicablePage)
	{
		this.applicablePage = applicablePage;
	}
}
