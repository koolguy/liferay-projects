package com.alm.rivaledge.transferobject;

import java.io.Serializable;
import java.util.Date;

public class UserGroupDTO implements Serializable,Comparable<UserGroupDTO>
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private int groupId;
	private String groupName;
	private Date dateAdded;
	private String oldGroupName;
	private String flgDefault;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getGroupId() {
		return groupId;
	}
	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public Date getDateAdded() {
		return dateAdded;
	}
	public void setDateAdded(Date dateAdded) {
		this.dateAdded = dateAdded;
	}
	public String getOldGroupName() {
		return oldGroupName;
	}
	public void setOldGroupName(String oldGroupName) {
		this.oldGroupName = oldGroupName;
	}
	public String getFlgDefault() {
		return flgDefault;
	}
	public void setFlgDefault(String flgDefault) {
		this.flgDefault = flgDefault;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((dateAdded == null) ? 0 : dateAdded.hashCode());
		result = prime * result + groupId;
		result = prime * result
				+ ((groupName == null) ? 0 : groupName.hashCode());
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserGroupDTO other = (UserGroupDTO) obj;
		if (dateAdded == null) {
			if (other.dateAdded != null)
				return false;
		} else if (!dateAdded.equals(other.dateAdded))
			return false;
		if (groupId != other.groupId)
			return false;
		if (groupName == null) {
			if (other.groupName != null)
				return false;
		} else if (!groupName.equals(other.groupName))
			return false;
		if (id != other.id)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "UserGroupDTO [id=" + id + ", groupId=" + groupId
				+ ", groupName=" + groupName + ", dateAdded=" + dateAdded + "]";
	}
	
    /**
     * This method is used for sorting watchlist firms in alphabetical ascending order
     * 
     * @param userGroup
     */
	@Override
	public int compareTo(UserGroupDTO userGroupDTO) 
	{
		return this.getGroupName().compareTo(userGroupDTO.getGroupName());
	}
	
}
