package com.alm.rivaledge.persistence.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alm.rivaledge.persistence.dao.lawma0_data.LoginHookMapper;
import com.alm.rivaledge.persistence.domain.lawma0_data.LoginHookUserMapping;


@Repository("loginHookDAO")
public class LoginHookUserMappingDAO 
{
	@Autowired
	private LoginHookMapper loginHookMapper;
	
	/**
	 * Given a user id, returns the corresponding UserMapping entity.
	 * 
	 * @param userId
	 * @return
	 */
	public LoginHookUserMapping findByPK(Integer userIds)
	{
		return (loginHookMapper.selectByPrimaryKey(userIds));
	}
	
	/**
	 * Add a User Mapping
	 * 
	 * @param loginHookUserMapping
	 */
	public void addUserMapping(LoginHookUserMapping loginHookUserMapping)
	{
		loginHookMapper.addUserMapping(loginHookUserMapping);
	}
	
	/**
	 * Update a User Mapping
	 * 
	 * @param loginHookUserMapping
	 */
	public void updateUserMapping(LoginHookUserMapping loginHookUserMapping)
	{
		loginHookMapper.updateUserMapping(loginHookUserMapping);
	}
}
