package com.alm.rivaledge.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alm.rivaledge.persistence.dao.RERAlertDAO;
import com.alm.rivaledge.persistence.domain.lawma0_data.RERAlert;
import com.alm.rivaledge.persistence.domain.lawma0_data.RERAlertUserEmail;
import com.alm.rivaledge.service.RERAlertService;

@Service("reralertService")
public class RERAlertServiceImpl implements RERAlertService{
	
	@Autowired
	private RERAlertDAO rerAlertDao;
	
	// Insert data into alert_query table.
	public int insertAlertQuery(RERAlert rerAlert){
		int alertId = rerAlertDao.insertAlertQuery(rerAlert);
		return alertId;
	}
	
	// Insert data into alert_user mapping table.
	public int insertAlertUserMapping(RERAlert rerAlert){
		int alertUserId = rerAlertDao.insertAlertUserMapping(rerAlert);
		return alertUserId;
	}
	
	// find the alert from alert_query table
	public RERAlert findRERAlertByTYPE_QUERY(RERAlert rerAlert){
		return rerAlertDao.findRERAlertByTYPE_QUERY(rerAlert);
	}
	
	// find alert by alert id
	public RERAlert findRERAlertByID(Integer alertId){
		return rerAlertDao.findRERAlertByID(alertId);
	}
	
	// find all alerts by user id
	public List<RERAlert> findAllUserAlertsByUserId(Integer userId){
		return rerAlertDao.findAllUserAlertsByUserId(userId);
	}
	
	// count all alert from alert_query table by type and query
	public int getRERAlertCountByTYPE_QUERY(RERAlert rerAlert){
		return rerAlertDao.getRERAlertCountByTYPE_QUERY(rerAlert);
	}
	
	// update alert from in alert_user table
	public void updateAlertUser(RERAlert rerAlert){
		rerAlertDao.updateAlertUser(rerAlert);
	}

	// insert  user email id
	public void insertUserEmails(RERAlertUserEmail RERAlertUserEmailObj){
		rerAlertDao.insertUserEmails(RERAlertUserEmailObj);
	}
	
	// find all user email ids
	public List<RERAlertUserEmail> findAllUserEmailIds(Integer userId){
		return rerAlertDao.findAllUserEmailIds(userId);
	}

	// delete user email id 
	public void deleteUserEmails(Integer id){
		rerAlertDao.deleteUserEmails(id);
	}
	
	// delete alert_user by id 
	public void deleteAlertUserById(Integer id){
		rerAlertDao.deleteAlertUserById(id);
	}
	
	// update user email id
	public void updateAlertEmail(RERAlertUserEmail RERAlertUserEmailObj){
		rerAlertDao.updateAlertEmail(RERAlertUserEmailObj);
	}


}
