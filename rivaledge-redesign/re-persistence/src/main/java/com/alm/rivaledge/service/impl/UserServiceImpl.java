package com.alm.rivaledge.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alm.rivaledge.persistence.dao.UserDAO;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserPreferences;
import com.alm.rivaledge.service.UserService;

@Service("userService")
public class UserServiceImpl implements UserService
{
	@Autowired
	private UserDAO userDAO;
	
	@Override
	public User getUserById(Integer userId)
	{
		return (userDAO.findByPrimaryKey(userId));
	}

	@Override
	public List<UserGroup> getUserWatchlists(Integer userId)
	{
		return (userDAO.findUserWatchLists(userId));
	}

	@Override
	public User getUserByEmail(String currentUserEmailAddress)
	{
		List<User> matchingUsers = userDAO.findByEmail(currentUserEmailAddress);
		if ((matchingUsers != null) && (!matchingUsers.isEmpty()))
		{
			return (matchingUsers.get(0));
		}
		return (null);
	}

	@Override
	public UserPreferences getUserPreferenceByUserIdAndPortletId(Integer userId, String portletId, String currentPage)
	{
		return (userDAO.findUserPreferenceByUserAndPortlet(userId, portletId, currentPage));
	}

	@Override
	public void saveUserPreferences(Integer userId, String currentPortletId, String searchCriteria, String chartCriteria, String currentPage)
	{
		UserPreferences userPreferences = new UserPreferences();
		userPreferences.setUserId(userId);
		userPreferences.setPortletId(currentPortletId);
		userPreferences.setSearchCriteriaJSON(searchCriteria);
		userPreferences.setChartCriteriaJSON(chartCriteria);
		userPreferences.setApplicablePage(currentPage);
		userDAO.persistUserPreferences(userPreferences);
	}
}
