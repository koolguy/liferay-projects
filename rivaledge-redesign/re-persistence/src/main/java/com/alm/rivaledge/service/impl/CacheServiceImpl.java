package com.alm.rivaledge.service.impl;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.StringTokenizer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alm.rivaledge.persistence.dao.CacheDAO;
import com.alm.rivaledge.persistence.dao.FirmDAO;
import com.alm.rivaledge.persistence.dao.UserDAO;
import com.alm.rivaledge.service.CacheService;
import com.alm.rivaledge.transferobject.AttorneyResultDTO;
import com.alm.rivaledge.transferobject.FirmAttorneyPractice;

/**
 * 
 * @author FL867
 * 
 */
@Service("cacheService")
public class CacheServiceImpl implements CacheService
{
	@Autowired
	private CacheDAO	cacheDAO;

	@Autowired
	private FirmDAO		firmDAO;

	@Autowired
	private UserDAO		userDAO;

//	@Override
	public int firmNewsAndPublicationsJob() throws IOException
	{
		// Set up the tables
		// Drop the existing cache table
		cacheDAO.dropFirmNewsPubsCacheTable();

		// Create it afresh
		cacheDAO.createFirmNewsPubsCacheTable();

		cacheDAO.bulkInsertNewsPubsTweets();

		return (1);
	}

//	@Override
	public int attorneyMovesAndChangesJob() throws Exception
	{
		int returnCount = 0;
		
		// Drop and recreate the attorney moves & changes cache
		cacheDAO.recreateAttorneyMovesChangesCache();
		
		// Bulk insert
		cacheDAO.bulkInsertAttorneyMovesChanges();

		return (returnCount);
	}

//	@Override
	public int attorneySearchDataJob() throws IOException
	{
		int returnCount = 0;
		
		// Drop and recreate the attorney data cache
		cacheDAO.recreateAttorneyDataCache();
		
		cacheDAO.bulkInsertAttorneyData();

		return (returnCount);
	}

	@Override
	public int firmStatisticsJob() throws Exception
	{
		int returnCounter = 0;
		
		File firmStatsCacheFile = File.createTempFile("firmStatsCache", ".txt");
		FileWriter firmStatsWriter = new FileWriter(firmStatsCacheFile);
		
		List<String> masterPractices = cacheDAO.findMasterPractices();
		
		List<Integer> allFirms = cacheDAO.findAllFirmsWithAttorneysInCache();

		cacheDAO.dropPeopleSearchCacheTables();
		cacheDAO.createPeopleSearchCacheTables();
		
		for (Integer thisFirm : allFirms)
		{
			// Get all the attorneys in this firm
			List<AttorneyResultDTO> firmAttorneys = cacheDAO.findAllAttorneysByLawFirm(thisFirm);
			if ((firmAttorneys != null) && (!firmAttorneys.isEmpty()))
			{
				for (AttorneyResultDTO thisResult : firmAttorneys)
				{
					// Get the attorney's practices
					for (StringTokenizer stPractices = new StringTokenizer(thisResult.getPractices(), ";") ; stPractices.hasMoreTokens() ; )
					{
						String thisPractice = stPractices.nextToken();
						if ((thisPractice != null && thisPractice.equals("Practice Area Not Available")) || (masterPractices.contains(thisPractice)))
						{
							// Valid practice
							FirmAttorneyPractice newFAP = new FirmAttorneyPractice();
							newFAP.setFirmId(thisResult.getFirmId());
							newFAP.setFirmName(thisResult.getFirmName());
							newFAP.setFirmLink(thisResult.getFirmLink());
							newFAP.setTitle((thisResult.getTitle() != null && thisResult.getTitle().trim().length() > 0) ? thisResult.getTitle() : "Other");
							newFAP.setAttorneyId(thisResult.getAttorneyId());
							newFAP.setPractices(thisPractice);
							firmStatsWriter.write(newFAP.toString());
						}
					}
				}
			}
		}		
		firmStatsWriter.flush();
		firmStatsWriter.close();
		
		cacheDAO.bulkInsertFirmStatistics(firmStatsCacheFile);

//			// Now for the collections - AmLaw 100, 200, NLJ 250, watch lists,
//			// my firm, etc.
//			// AmLaw 100
//			List<Firm> amLaw100Firms = firmDAO.findAmLaw100();
//			if ((amLaw100Firms != null) && (!amLaw100Firms.isEmpty()))
//			{
//				returnCounter += processCollections("AmLaw 100", amLaw100Firms, listOfFirmIds);
//			}
//
//			// AmLaw 200
//			List<Firm> amLaw200Firms = firmDAO.findAmLaw200();
//			if ((amLaw200Firms != null) && (!amLaw200Firms.isEmpty()))
//			{
//				returnCounter += processCollections("AmLaw 200", amLaw200Firms, listOfFirmIds);
//			}
//
//			// NLJ 250
//			List<Firm> nlj250Firms = firmDAO.findNLJ250();
//			if ((nlj250Firms != null) && (!nlj250Firms.isEmpty()))
//			{
//				returnCounter += processCollections("NLJ 250", nlj250Firms, listOfFirmIds);
//			}
//
//			// Watch lists
//			List<UserGroup> allWatchlists = userDAO.findAllWatchlists();
//			if ((allWatchlists != null) && (!allWatchlists.isEmpty()))
//			{
//				for (UserGroup thisWatchlist : allWatchlists)
//				{
//					List<UserGroupCompany> watchlistFirms = userDAO.findWatchListFirms(thisWatchlist.getId());
//					if ((watchlistFirms != null) && (!watchlistFirms.isEmpty()))
//					{
//						List<Firm> firmsList = new ArrayList<Firm>(watchlistFirms.size());
//						for (UserGroupCompany thisUGC : watchlistFirms)
//						{
//							firmsList.add(firmDAO.findByPK(thisUGC.getCompanyId()));
//						}
//						// returnCounter +=
//						// processCollections(thisWatchlist.getGroupName(),
//						// firmsList, listOfFirmIds);
//					}
//				}
//			}
//		}
//		return (returnCounter);
//	}

//	private int processCollections(String collectionName, List<Firm> firmsInCollection, List<Integer> listOfFirmIds)
//	{
//		int returnCounter = 0;
//		PeopleResultDTO collectionResult = new PeopleResultDTO();
//		PeopleResult firmCount = new PeopleResult();
//		Map<String, PeopleResult> locationMap = new TreeMap<String, PeopleResult>();
//		Map<String, PeopleResult> practiceMap = new TreeMap<String, PeopleResult>();
//		PeopleResult locationCount = null;
//		PeopleResult practiceCount = null;
//
//		collectionResult.setCompanyName(collectionName);
//		firmCount.setGroupType("FIRM");
//		firmCount.setSearchTitle(collectionName);
//
//		for (Firm thisFirm : firmsInCollection)
//		{
//			if ((thisFirm != null) && (thisFirm.getCompanyId() != null) && (listOfFirmIds.contains(thisFirm.getCompanyId())))
//			{
//				// We have attorney data for this AmLaw 100 firm
//				PeopleResultDTO thisDTO = cacheDAO.findMovementsByFirm(thisFirm.getCompanyId());
//				List<PeopleResult> dtoResults = cacheDAO.findPeopleResultByGroupId(thisDTO.getGroupingId());
//				if ((dtoResults != null) && (!dtoResults.isEmpty()))
//				{
//					for (PeopleResult thisResult : dtoResults)
//					{
//						if (thisResult.getGroupType().equals("FIRM"))
//						{
//							firmCount.setHeadCount(firmCount.getHeadCount() + thisResult.getHeadCount());
//							firmCount.setHeadCountPlus(firmCount.getHeadCountPlus() + thisResult.getHeadCountPlus());
//							firmCount.setHeadCountMinus(firmCount.getHeadCountMinus() + thisResult.getHeadCountMinus());
//
//							firmCount.setPartnerCount(firmCount.getPartnerCount() + thisResult.getPartnerCount());
//							firmCount.setPartnerCountPlus(firmCount.getPartnerCountPlus() + thisResult.getPartnerCountPlus());
//							firmCount.setPartnerCountMinus(firmCount.getPartnerCountMinus() + thisResult.getPartnerCountMinus());
//
//							firmCount.setAssociateCount(firmCount.getAssociateCount() + thisResult.getAssociateCount());
//							firmCount.setAssociateCountPlus(firmCount.getAssociateCountPlus() + thisResult.getAssociateCountPlus());
//							firmCount.setAssociateCountMinus(firmCount.getAssociateCountMinus() + thisResult.getAssociateCountMinus());
//
//							firmCount.setOtherCounselCount(firmCount.getOtherCounselCount() + thisResult.getOtherCounselCount());
//							firmCount.setOtherCounselCountPlus(firmCount.getOtherCounselCountPlus() + thisResult.getOtherCounselCountPlus());
//							firmCount.setOtherCounselCountMinus(firmCount.getOtherCounselCountMinus() + thisResult.getOtherCounselCountMinus());
//
//							firmCount.setAdminCount(firmCount.getAdminCount() + thisResult.getAdminCount());
//							firmCount.setAdminCountPlus(firmCount.getAdminCountPlus() + thisResult.getAdminCountPlus());
//							firmCount.setAdminCountMinus(firmCount.getAdminCountMinus() + thisResult.getAdminCountMinus());
//
//							firmCount.setOtherCount(firmCount.getOtherCount() + thisResult.getOtherCount());
//							firmCount.setOtherCountPlus(firmCount.getOtherCountPlus() + thisResult.getOtherCountPlus());
//							firmCount.setOtherCountMinus(firmCount.getOtherCountMinus() + thisResult.getOtherCountMinus());
//						}
//						else if (thisResult.getGroupType().equals("LOCATION"))
//						{
//							if (locationMap.containsKey(thisResult.getSearchTitle()))
//							{
//								locationCount = locationMap.remove(thisResult.getSearchTitle());
//							}
//							else
//							{
//								locationCount = new PeopleResult();
//								locationCount.setGroupType("LOCATION");
//								locationCount.setSearchTitle(thisResult.getSearchTitle());
//							}
//							locationCount.setHeadCount(locationCount.getHeadCount() + thisResult.getHeadCount());
//							locationCount.setHeadCountPlus(locationCount.getHeadCountPlus() + thisResult.getHeadCountPlus());
//							locationCount.setHeadCountMinus(locationCount.getHeadCountMinus() + thisResult.getHeadCountMinus());
//
//							locationCount.setPartnerCount(locationCount.getPartnerCount() + thisResult.getPartnerCount());
//							locationCount.setPartnerCountPlus(locationCount.getPartnerCountPlus() + thisResult.getPartnerCountPlus());
//							locationCount.setPartnerCountMinus(locationCount.getPartnerCountMinus() + thisResult.getPartnerCountMinus());
//
//							locationCount.setAssociateCount(locationCount.getAssociateCount() + thisResult.getAssociateCount());
//							locationCount.setAssociateCountPlus(locationCount.getAssociateCountPlus() + thisResult.getAssociateCountPlus());
//							locationCount.setAssociateCountMinus(locationCount.getAssociateCountMinus() + thisResult.getAssociateCountMinus());
//
//							locationCount.setOtherCounselCount(locationCount.getOtherCounselCount() + thisResult.getOtherCounselCount());
//							locationCount.setOtherCounselCountPlus(locationCount.getOtherCounselCountPlus() + thisResult.getOtherCounselCountPlus());
//							locationCount.setOtherCounselCountMinus(locationCount.getOtherCounselCountMinus() + thisResult.getOtherCounselCountMinus());
//
//							locationCount.setAdminCount(locationCount.getAdminCount() + thisResult.getAdminCount());
//							locationCount.setAdminCountPlus(locationCount.getAdminCountPlus() + thisResult.getAdminCountPlus());
//							locationCount.setAdminCountMinus(locationCount.getAdminCountMinus() + thisResult.getAdminCountMinus());
//
//							locationCount.setOtherCount(locationCount.getOtherCount() + thisResult.getOtherCount());
//							locationCount.setOtherCountPlus(locationCount.getOtherCountPlus() + thisResult.getOtherCountPlus());
//							locationCount.setOtherCountMinus(locationCount.getOtherCountMinus() + thisResult.getOtherCountMinus());
//						}
//						else if (thisResult.getGroupType().equals("PRACTICE"))
//						{
//							if (practiceMap.containsKey(thisResult.getSearchTitle()))
//							{
//								practiceCount = practiceMap.remove(thisResult.getSearchTitle());
//							}
//							else
//							{
//								practiceCount = new PeopleResult();
//								practiceCount.setGroupType("PRACTICE");
//								practiceCount.setSearchTitle(thisResult.getSearchTitle());
//							}
//							practiceCount.setHeadCount(practiceCount.getHeadCount() + thisResult.getHeadCount());
//							practiceCount.setHeadCountPlus(practiceCount.getHeadCountPlus() + thisResult.getHeadCountPlus());
//							practiceCount.setHeadCountMinus(practiceCount.getHeadCountMinus() + thisResult.getHeadCountMinus());
//
//							practiceCount.setPartnerCount(practiceCount.getPartnerCount() + thisResult.getPartnerCount());
//							practiceCount.setPartnerCountPlus(practiceCount.getPartnerCountPlus() + thisResult.getPartnerCountPlus());
//							practiceCount.setPartnerCountMinus(practiceCount.getPartnerCountMinus() + thisResult.getPartnerCountMinus());
//
//							practiceCount.setAssociateCount(practiceCount.getAssociateCount() + thisResult.getAssociateCount());
//							practiceCount.setAssociateCountPlus(practiceCount.getAssociateCountPlus() + thisResult.getAssociateCountPlus());
//							practiceCount.setAssociateCountMinus(practiceCount.getAssociateCountMinus() + thisResult.getAssociateCountMinus());
//
//							practiceCount.setOtherCounselCount(practiceCount.getOtherCounselCount() + thisResult.getOtherCounselCount());
//							practiceCount.setOtherCounselCountPlus(practiceCount.getOtherCounselCountPlus() + thisResult.getOtherCounselCountPlus());
//							practiceCount.setOtherCounselCountMinus(practiceCount.getOtherCounselCountMinus() + thisResult.getOtherCounselCountMinus());
//
//							practiceCount.setAdminCount(practiceCount.getAdminCount() + thisResult.getAdminCount());
//							practiceCount.setAdminCountPlus(practiceCount.getAdminCountPlus() + thisResult.getAdminCountPlus());
//							practiceCount.setAdminCountMinus(practiceCount.getAdminCountMinus() + thisResult.getAdminCountMinus());
//
//							practiceCount.setOtherCount(practiceCount.getOtherCount() + thisResult.getOtherCount());
//							practiceCount.setOtherCountPlus(practiceCount.getOtherCountPlus() + thisResult.getOtherCountPlus());
//							practiceCount.setOtherCountMinus(practiceCount.getOtherCountMinus() + thisResult.getOtherCountMinus());
//						}
//						if (locationCount != null)
//						{
//							// locationCount.setActionDate(thisResult.getActionDate());
//							locationMap.put(thisResult.getSearchTitle(), locationCount);
//							locationCount = null;
//						}
//						if (practiceCount != null)
//						{
//							// practiceCount.setActionDate(thisResult.getActionDate());
//							practiceMap.put(thisResult.getSearchTitle(), practiceCount);
//							practiceCount = null;
//						}
//					}
//				}
//			}
//		}
//		// Persist the collection
//		cacheDAO.insertPeopleSearchResultDTO(collectionResult);
//		returnCounter++;
//		Integer groupId = cacheDAO.findGroupIdByCollection(collectionName);
//		firmCount.setGroupingId(groupId);
//		cacheDAO.insertPeopleResult(firmCount);
//		for (String locationKey : locationMap.keySet())
//		{
//			PeopleResult thisCount = locationMap.get(locationKey);
//			thisCount.setGroupingId(groupId);
//			cacheDAO.insertPeopleResult(thisCount);
//			returnCounter++;
//		}
//		for (String practiceKey : practiceMap.keySet())
//		{
//			PeopleResult thisCount = practiceMap.get(practiceKey);
//			thisCount.setGroupingId(groupId);
//			cacheDAO.insertPeopleResult(thisCount);
//			returnCounter++;
//		}
		return (returnCounter);
	}

//	@Override
	public int eventsJob() throws IOException
	{
		// Set up the tables
		// Drop the existing table
		cacheDAO.dropEventsCacheTable();

		// Create it afresh
		cacheDAO.createEventsCacheTable();

		// Bulk insert into the database
		cacheDAO.bulkInsertEventCacheData();

		return (1);
	}
}