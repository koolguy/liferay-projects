package com.alm.rivaledge.transferobject;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;

/**
 * This class will have all the search criteria which user has entered on the
 * Attorney Move Changes search form
 * 
 * @author FL605
 * @since Sprint 4
 */
public class AttorneyMoveChangesSearchDTO extends BaseSearchDTO implements Serializable 
{
	private static final long serialVersionUID = -818842073305492286L;
	
	private List<Firm>				selectedFirms;
	private List<String>		    changeType;
	private List<String> 			practiceArea;
	private List<String> 			locations;
	private Date 					toDate;
	private Date 					fromDate;
	private List<String> 			titles;
	private String 					keywordsInBioText;
	private List<String>		    attorneyName;
	private List<Integer>		    attorneyIdList;

	public AttorneyMoveChangesSearchDTO() 
	{
		super();
		// Override the default sort column
		setSortColumn(6);
	}

	public List<Firm> getSelectedFirms() 
	{
		return selectedFirms;
	}

	public void setSelectedFirms(List<Firm> selectedFirms) 
	{
		this.selectedFirms = selectedFirms;
	}

	public List<String> getPracticeArea() 
	{
		return practiceArea;
	}

	public void setPracticeArea(List<String> practiceArea) 
	{
		this.practiceArea = practiceArea;
	}

	public List<String> getLocations() 
	{
		return locations;
	}

	public void setLocations(List<String> locations) 
	{
		this.locations = locations;
	}

	public List<String> getTitles()
{
		return titles;
	}

	public void setTitles(List<String> titles) 
	{
		this.titles = titles;
	}
	

	public String getKeywordsInBioText() 
	{
		return keywordsInBioText;
	}

	public void setKeywordsInBioText(String keywordsInBioText)
	{
		this.keywordsInBioText = keywordsInBioText;
	}

	public List<String> getChangeType() 
	{
		return changeType;
	}

	public void setChangeType(List<String> changeType) 
	{
		this.changeType = changeType;
	}

	public Date getToDate() 
	{
		return toDate;
	}

	public void setToDate(Date toDate) 
	{
		this.toDate = toDate;
	}

	public Date getFromDate() 
	{
		return fromDate;
	}

	public void setFromDate(Date fromDate) 
	{
		this.fromDate = fromDate;
	}

	public List<String> getAttorneyName() 
	{
		return attorneyName;
	}

	public void setAttorneyName(List<String> attorneyName) 
	{
		this.attorneyName = attorneyName;
	}

	public List<Integer> getAttorneyIdList() 
	{
		return attorneyIdList;
	}

	public void setAttorneyIdList(List<Integer> attorneyIdList) 
	{
		this.attorneyIdList = attorneyIdList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((attorneyName == null) ? 0 : attorneyName.hashCode());
		result = prime * result
				+ ((changeType == null) ? 0 : changeType.hashCode());
		result = prime * result
				+ ((fromDate == null) ? 0 : fromDate.hashCode());
		result = prime
				* result
				+ ((keywordsInBioText == null) ? 0 : keywordsInBioText
						.hashCode());
		result = prime * result
				+ ((locations == null) ? 0 : locations.hashCode());
		result = prime * result
				+ ((practiceArea == null) ? 0 : practiceArea.hashCode());
		result = prime * result
				+ ((selectedFirms == null) ? 0 : selectedFirms.hashCode());
		result = prime * result + ((titles == null) ? 0 : titles.hashCode());
		result = prime * result + ((toDate == null) ? 0 : toDate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AttorneyMoveChangesSearchDTO other = (AttorneyMoveChangesSearchDTO) obj;
		if (attorneyName == null) 
		{
			if (other.attorneyName != null)
				return false;
		} else if (!attorneyName.equals(other.attorneyName))
			return false;
		if (changeType == null)
		{
			if (other.changeType != null)
				return false;
		} else if (!changeType.equals(other.changeType))
			return false;
		if (fromDate == null) 
		{
			if (other.fromDate != null)
				return false;
		} else if (!fromDate.equals(other.fromDate))
			return false;
		if (keywordsInBioText == null) 
		{
			if (other.keywordsInBioText != null)
				return false;
		} else if (!keywordsInBioText.equals(other.keywordsInBioText))
			return false;
		if (locations == null) 
		{
			if (other.locations != null)
				return false;
		} else if (!locations.equals(other.locations))
			return false;
		if (practiceArea == null) 
		{
			if (other.practiceArea != null)
				return false;
		} else if (!practiceArea.equals(other.practiceArea))
			return false;
		if (selectedFirms == null) 
		{
			if (other.selectedFirms != null)
				return false;
		} else if (!selectedFirms.equals(other.selectedFirms))
			return false;
		if (titles == null) 
		{
			if (other.titles != null)
				return false;
		} else if (!titles.equals(other.titles))
			return false;
		if (toDate == null)
		{
			if (other.toDate != null)
				return false;
		} else if (!toDate.equals(other.toDate))
			return false;
		return true;
	}

	@Override
	public String toString() 
	{
		StringBuilder builder = new StringBuilder();
		builder.append(selectedFirms);
		builder.append("||");
		builder.append(changeType);
		builder.append("||");
		builder.append(practiceArea);
		builder.append("||");
		builder.append(locations);
		builder.append("||");
		builder.append(toDate);
		builder.append("||");
		builder.append(fromDate);
		builder.append("||");
		builder.append(titles);
		builder.append("||");
		builder.append(attorneyName);
		builder.append("||");
		builder.append(keywordsInBioText);
		builder.append("||");		
		builder.append("$$");
		return builder.toString();
	}

}
