package com.alm.rivaledge.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroupCompany;
import com.alm.rivaledge.transferobject.UserGroupCompanyDTO;
import com.alm.rivaledge.transferobject.UserGroupDTO;
import com.alm.rivaledge.transferobject.WatchListFirmResultDTO;
import com.alm.rivaledge.transferobject.WatchlistCriteriaSearchDTO;

/**
 * This will handle all the business logic related to creating, editing, and
 * maintaining user watch lists.
 * 
 * @author FL867
 * @since Sprint 3
 */
@Component
public interface WatchlistService
{
	/**
	 * Returns all the candidate firms that can be added to this user's watch
	 * list.
	 * 
	 * @param currentUser
	 * @return
	 */
	public List<WatchListFirmResultDTO> getFirmsForWatchlist(User currentUser, String firmFirstChar);
	
	/**
	 * This method is used for adding new Watchlist
	 * @param ugDTO usergroup DTO object that contain watch list's property value
	 */
	public void addMyWatchList(UserGroupDTO ugDTO);
	
	/**
	 * This method is used for updating existing watch list
	 * @param ugDTO usergroup DTO object that contain watch list's updated property value
	 */
	public void updateMyWatchList(UserGroupDTO ugDTO);
	
	/**
	 * This method is used for deletion of existing watch list
	 * @param ugDTO usergroup DTO object that contain watch list value which will be delete
	 */
	public void deleteMyWatchList(UserGroupDTO ugDTO);
	
	/**
	 * This method is used for getting all firm watch lists for a user
	 * @return firm list 
	 */
	public List<UserGroupDTO> getAllFirmWatchList(Integer userId);
	
	/**
	 * This method is used for returning watch list's id based on watch listname
	 * @param watchListFirmName
	 * @return get Watchlist id
	 */
	public Integer getWatchList(UserGroupDTO userGroupDTO);
	
	/**
	 * This method is used for saving firm into a watch list
	 * @param userGroupCompany used to store firm value
	 * @return added firm id value
	 */
	public int saveFirmForWatchList(UserGroupCompany userGroupCompany);
	
	/**
	 * This method is used for returning all Firms associated with a watch list
	 * @param userGroupCompany to check associated firms with watch list
	 * @return List<UserGroupCompanyDTO>
	 */
	public List<UserGroupCompanyDTO> getAddedFirms(UserGroupCompany userGroupCompany);
	
	/**
	 * This method is used for removing firms for a watch list
	 * @param userGroupCompany used to store firm value associate with watch list
	 * @return 
	 */
	public void removeFirmsFromWatchList(UserGroupCompany userGroupCompany);
	
	/**
	 * Find all the firms in a given watch list.
	 * @param watchListId
	 * @return
	 */
	public List<Firm> findAllFirmsInOneWatchlist(Integer watchListId,Integer userId);
	
	public List<Firm> findAllFirmsInManyWatchlists(List<Integer> allWatchlists,Integer userId);
	
	public UserGroup getWatchList(Integer watchListId, Integer userId);
	
	/** Set default watchlist of User by setting flg_default to "Y" in user_groups table
	 * @param ugDTO
	 */
	public void setDefaultWatchList(UserGroupDTO ugDTO);
	
	/**
	 * Returns all filtered firms based on User search criteria  on watchlist screen.
	 * @param watchlistCriteriaSearchDTO
	 * @return
	 */
	public List<WatchListFirmResultDTO>   getFilteredFirms(WatchlistCriteriaSearchDTO watchlistCriteriaSearchDTO);
	
	/**
	 * This method is used for getting all watch lists for a user
	 * @param userid
	 * @return watchlists 
	 */
	public List<UserGroup> getAllWatchListForAutoComplete(Integer userId);
	
}

