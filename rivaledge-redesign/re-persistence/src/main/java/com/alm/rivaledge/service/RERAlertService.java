package com.alm.rivaledge.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.alm.rivaledge.persistence.domain.lawma0_data.RERAlert;
import com.alm.rivaledge.persistence.domain.lawma0_data.RERAlertUserEmail;

@Component
public interface RERAlertService {

	// Insert data into alert_query table.
	public int insertAlertQuery(RERAlert rerAlert);
	
	// Insert data into alert_user mapping table.
	public int insertAlertUserMapping(RERAlert rerAlert);
	
	// find the alert from alert_query table
	public RERAlert findRERAlertByTYPE_QUERY(RERAlert rerAlert);
	
	// find alert by alert id
	public RERAlert findRERAlertByID(Integer alertId);
	
	// find all alerts by user id
	public List<RERAlert> findAllUserAlertsByUserId(Integer userId);
	
	// count all alert from alert_query table by type and query
	public int getRERAlertCountByTYPE_QUERY(RERAlert rerAlert);
	
	// update alert from in alert_user table
	public void updateAlertUser(RERAlert rerAlert);

	// insert  user email id
	public void insertUserEmails(RERAlertUserEmail RERAlertUserEmailObj);
	
	// find all user email ids
	public List<RERAlertUserEmail> findAllUserEmailIds(Integer userId);
	
	// delete user email id 
	public void deleteUserEmails(Integer id);
		
	// delete alert_user by id 
	public void deleteAlertUserById(Integer id);
	
	// update user email id
	public void updateAlertEmail(RERAlertUserEmail RERAlertUserEmailObj);
}
