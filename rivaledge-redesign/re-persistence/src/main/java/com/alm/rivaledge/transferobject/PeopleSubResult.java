package com.alm.rivaledge.transferobject;

import java.io.Serializable;
import java.util.List;

/**
 * POJO that serves a temporary store for the firm statistics results coming
 * from the database before they are converted into full-fledged PeopleResults.
 * 
 * @author FL867
 * @version 1.0
 * @since Sprint 7.1
 */
public class PeopleSubResult implements Serializable
{
	private static final long					serialVersionUID	= -6289478918755468368L;

	private Integer								firmId;
	private String								firmName;
	private String								firmURL;
	private List<AttorneyMoveChangesResultDTO>	headCounts;
	private List<AttorneyMoveChangesResultDTO>	movementCounts;

	public Integer getFirmId()
	{
		return firmId;
	}

	public void setFirmId(Integer firmId)
	{
		this.firmId = firmId;
	}

	public String getFirmName()
	{
		return firmName;
	}

	public void setFirmName(String firmName)
	{
		this.firmName = firmName;
	}

	public String getFirmURL()
	{
		return firmURL;
	}

	public void setFirmURL(String firmURL)
	{
		this.firmURL = firmURL;
	}

	public List<AttorneyMoveChangesResultDTO> getHeadCounts()
	{
		return headCounts;
	}

	public void setHeadCounts(List<AttorneyMoveChangesResultDTO> headCounts)
	{
		this.headCounts = headCounts;
	}

	public List<AttorneyMoveChangesResultDTO> getMovementCounts()
	{
		return movementCounts;
	}

	public void setMovementCounts(List<AttorneyMoveChangesResultDTO> movementCounts)
	{
		this.movementCounts = movementCounts;
	}
}
