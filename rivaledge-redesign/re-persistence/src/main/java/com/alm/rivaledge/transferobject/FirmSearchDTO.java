package com.alm.rivaledge.transferobject;

import java.util.Date;
import java.util.List;

import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;

public class FirmSearchDTO extends BaseSearchDTO
{
	private static final long	serialVersionUID	= -5470689533489309211L;
	private List<Firm>			selectedFirms;
	private List<String>		contentType;
	private Date				toDate;
	private Date				fromDate;
	private String				keyword;
	private List<String>		practiceArea;
	private List<String>		twitterPostType;
	
	public FirmSearchDTO()
	{
		super();
		setSortColumn(10);
		setOrderBy(-1); //no grouping
	}

	public List<Firm> getSelectedFirms()
	{
		return selectedFirms;
	}

	public void setSelectedFirms(List<Firm> selectedFirms)
	{
		this.selectedFirms = selectedFirms;
	}

	public List<String> getContentType()
	{
		return contentType;
	}

	public void setContentType(List<String> contentType)
	{
		this.contentType = contentType;
	}

	public Date getToDate()
	{
		return toDate;
	}

	public void setToDate(Date toDate)
	{
		this.toDate = toDate;
	}

	public Date getFromDate()
	{
		return fromDate;
	}

	public void setFromDate(Date fromDate)
	{
		this.fromDate = fromDate;
	}

	public String getKeyword()
	{
		return keyword;
	}

	public void setKeyword(String keyword)
	{
		this.keyword = keyword;
	}

	public List<String> getPracticeArea()
	{
		return practiceArea;
	}

	public void setPracticeArea(List<String> practiceArea)
	{
		this.practiceArea = practiceArea;
	}

	public List<String> getTwitterPostType()
	{
		return twitterPostType;
	}

	public void setTwitterPostType(List<String> twitterPostType)
	{
		this.twitterPostType = twitterPostType;
	}
	


	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((contentType == null) ? 0 : contentType.hashCode());
		result = prime * result + ((fromDate == null) ? 0 : fromDate.hashCode());
		result = prime * result + ((keyword == null) ? 0 : keyword.hashCode());
		result = prime * result + ((practiceArea == null) ? 0 : practiceArea.hashCode());
		result = prime * result + ((selectedFirms == null) ? 0 : selectedFirms.hashCode());
		result = prime * result + ((toDate == null) ? 0 : toDate.hashCode());
		result = prime * result + ((twitterPostType == null) ? 0 : twitterPostType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FirmSearchDTO other = (FirmSearchDTO) obj;
		if (contentType == null)
		{
			if (other.contentType != null)
				return false;
		}
		else if (!contentType.equals(other.contentType))
			return false;
		if (fromDate == null)
		{
			if (other.fromDate != null)
				return false;
		}
		else if (!fromDate.equals(other.fromDate))
			return false;
		if (keyword == null)
		{
			if (other.keyword != null)
				return false;
		}
		else if (!keyword.equals(other.keyword))
			return false;
		if (practiceArea == null)
		{
			if (other.practiceArea != null)
				return false;
		}
		else if (!practiceArea.equals(other.practiceArea))
			return false;
		if (selectedFirms == null)
		{
			if (other.selectedFirms != null)
				return false;
		}
		else if (!selectedFirms.equals(other.selectedFirms))
			return false;
		if (toDate == null)
		{
			if (other.toDate != null)
				return false;
		}
		else if (!toDate.equals(other.toDate))
			return false;
		if (twitterPostType == null)
		{
			if (other.twitterPostType != null)
				return false;
		}
		else if (!twitterPostType.equals(other.twitterPostType))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		return "FirmSearchDTO [selectedFirms=" + selectedFirms + ", contentType=" + contentType + ", toDate=" + toDate + ", fromDate=" + fromDate + ", keyword=" + keyword
				+ ", practiceArea=" + practiceArea + ", twitterPostType=" + twitterPostType + ", getPageNumber()=" + getPageNumber() + ", getSortColumn()=" + getSortColumn()
				+ ", getSortOrder()=" + getSortOrder() + "]";
	}
}
