package com.alm.rivaledge.persistence.domain.lawma0_data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class AdmissionExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table lawma0_data..tbl_rer_admissionsearch_log
     *
     * @mbggenerated Fri Nov 15 20:08:51 IST 2013
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table lawma0_data..tbl_rer_admissionsearch_log
     *
     * @mbggenerated Fri Nov 15 20:08:51 IST 2013
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table lawma0_data..tbl_rer_admissionsearch_log
     *
     * @mbggenerated Fri Nov 15 20:08:51 IST 2013
     */
    protected List<Criteria> oredCriteria;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..tbl_rer_admissionsearch_log
     *
     * @mbggenerated Fri Nov 15 20:08:51 IST 2013
     */
    public AdmissionExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..tbl_rer_admissionsearch_log
     *
     * @mbggenerated Fri Nov 15 20:08:51 IST 2013
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..tbl_rer_admissionsearch_log
     *
     * @mbggenerated Fri Nov 15 20:08:51 IST 2013
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..tbl_rer_admissionsearch_log
     *
     * @mbggenerated Fri Nov 15 20:08:51 IST 2013
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..tbl_rer_admissionsearch_log
     *
     * @mbggenerated Fri Nov 15 20:08:51 IST 2013
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..tbl_rer_admissionsearch_log
     *
     * @mbggenerated Fri Nov 15 20:08:51 IST 2013
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..tbl_rer_admissionsearch_log
     *
     * @mbggenerated Fri Nov 15 20:08:51 IST 2013
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..tbl_rer_admissionsearch_log
     *
     * @mbggenerated Fri Nov 15 20:08:51 IST 2013
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..tbl_rer_admissionsearch_log
     *
     * @mbggenerated Fri Nov 15 20:08:51 IST 2013
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..tbl_rer_admissionsearch_log
     *
     * @mbggenerated Fri Nov 15 20:08:51 IST 2013
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..tbl_rer_admissionsearch_log
     *
     * @mbggenerated Fri Nov 15 20:08:51 IST 2013
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table lawma0_data..tbl_rer_admissionsearch_log
     *
     * @mbggenerated Fri Nov 15 20:08:51 IST 2013
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andEntryIdIsNull() {
            addCriterion("entry_id is null");
            return (Criteria) this;
        }

        public Criteria andEntryIdIsNotNull() {
            addCriterion("entry_id is not null");
            return (Criteria) this;
        }

        public Criteria andEntryIdEqualTo(Long value) {
            addCriterion("entry_id =", value, "entryId");
            return (Criteria) this;
        }

        public Criteria andEntryIdNotEqualTo(Long value) {
            addCriterion("entry_id <>", value, "entryId");
            return (Criteria) this;
        }

        public Criteria andEntryIdGreaterThan(Long value) {
            addCriterion("entry_id >", value, "entryId");
            return (Criteria) this;
        }

        public Criteria andEntryIdGreaterThanOrEqualTo(Long value) {
            addCriterion("entry_id >=", value, "entryId");
            return (Criteria) this;
        }

        public Criteria andEntryIdLessThan(Long value) {
            addCriterion("entry_id <", value, "entryId");
            return (Criteria) this;
        }

        public Criteria andEntryIdLessThanOrEqualTo(Long value) {
            addCriterion("entry_id <=", value, "entryId");
            return (Criteria) this;
        }

        public Criteria andEntryIdIn(List<Long> values) {
            addCriterion("entry_id in", values, "entryId");
            return (Criteria) this;
        }

        public Criteria andEntryIdNotIn(List<Long> values) {
            addCriterion("entry_id not in", values, "entryId");
            return (Criteria) this;
        }

        public Criteria andEntryIdBetween(Long value1, Long value2) {
            addCriterion("entry_id between", value1, value2, "entryId");
            return (Criteria) this;
        }

        public Criteria andEntryIdNotBetween(Long value1, Long value2) {
            addCriterion("entry_id not between", value1, value2, "entryId");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNull() {
            addCriterion("user_id is null");
            return (Criteria) this;
        }

        public Criteria andUserIdIsNotNull() {
            addCriterion("user_id is not null");
            return (Criteria) this;
        }

        public Criteria andUserIdEqualTo(Integer value) {
            addCriterion("user_id =", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotEqualTo(Integer value) {
            addCriterion("user_id <>", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThan(Integer value) {
            addCriterion("user_id >", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("user_id >=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThan(Integer value) {
            addCriterion("user_id <", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("user_id <=", value, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdIn(List<Integer> values) {
            addCriterion("user_id in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotIn(List<Integer> values) {
            addCriterion("user_id not in", values, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdBetween(Integer value1, Integer value2) {
            addCriterion("user_id between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("user_id not between", value1, value2, "userId");
            return (Criteria) this;
        }

        public Criteria andAdmissionIsNull() {
            addCriterion("admission is null");
            return (Criteria) this;
        }

        public Criteria andAdmissionIsNotNull() {
            addCriterion("admission is not null");
            return (Criteria) this;
        }

        public Criteria andAdmissionEqualTo(String value) {
            addCriterion("admission =", value, "admission");
            return (Criteria) this;
        }

        public Criteria andAdmissionNotEqualTo(String value) {
            addCriterion("admission <>", value, "admission");
            return (Criteria) this;
        }

        public Criteria andAdmissionGreaterThan(String value) {
            addCriterion("admission >", value, "admission");
            return (Criteria) this;
        }

        public Criteria andAdmissionGreaterThanOrEqualTo(String value) {
            addCriterion("admission >=", value, "admission");
            return (Criteria) this;
        }

        public Criteria andAdmissionLessThan(String value) {
            addCriterion("admission <", value, "admission");
            return (Criteria) this;
        }

        public Criteria andAdmissionLessThanOrEqualTo(String value) {
            addCriterion("admission <=", value, "admission");
            return (Criteria) this;
        }

        public Criteria andAdmissionLike(String value) {
            addCriterion("admission like", value, "admission");
            return (Criteria) this;
        }

        public Criteria andAdmissionNotLike(String value) {
            addCriterion("admission not like", value, "admission");
            return (Criteria) this;
        }

        public Criteria andAdmissionIn(List<String> values) {
            addCriterion("admission in", values, "admission");
            return (Criteria) this;
        }

        public Criteria andAdmissionNotIn(List<String> values) {
            addCriterion("admission not in", values, "admission");
            return (Criteria) this;
        }

        public Criteria andAdmissionBetween(String value1, String value2) {
            addCriterion("admission between", value1, value2, "admission");
            return (Criteria) this;
        }

        public Criteria andAdmissionNotBetween(String value1, String value2) {
            addCriterion("admission not between", value1, value2, "admission");
            return (Criteria) this;
        }

        public Criteria andSearchDateIsNull() {
            addCriterion("search_date is null");
            return (Criteria) this;
        }

        public Criteria andSearchDateIsNotNull() {
            addCriterion("search_date is not null");
            return (Criteria) this;
        }

        public Criteria andSearchDateEqualTo(Date value) {
            addCriterion("search_date =", value, "searchDate");
            return (Criteria) this;
        }

        public Criteria andSearchDateNotEqualTo(Date value) {
            addCriterion("search_date <>", value, "searchDate");
            return (Criteria) this;
        }

        public Criteria andSearchDateGreaterThan(Date value) {
            addCriterion("search_date >", value, "searchDate");
            return (Criteria) this;
        }

        public Criteria andSearchDateGreaterThanOrEqualTo(Date value) {
            addCriterion("search_date >=", value, "searchDate");
            return (Criteria) this;
        }

        public Criteria andSearchDateLessThan(Date value) {
            addCriterion("search_date <", value, "searchDate");
            return (Criteria) this;
        }

        public Criteria andSearchDateLessThanOrEqualTo(Date value) {
            addCriterion("search_date <=", value, "searchDate");
            return (Criteria) this;
        }

        public Criteria andSearchDateIn(List<Date> values) {
            addCriterion("search_date in", values, "searchDate");
            return (Criteria) this;
        }

        public Criteria andSearchDateNotIn(List<Date> values) {
            addCriterion("search_date not in", values, "searchDate");
            return (Criteria) this;
        }

        public Criteria andSearchDateBetween(Date value1, Date value2) {
            addCriterion("search_date between", value1, value2, "searchDate");
            return (Criteria) this;
        }

        public Criteria andSearchDateNotBetween(Date value1, Date value2) {
            addCriterion("search_date not between", value1, value2, "searchDate");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table lawma0_data..tbl_rer_admissionsearch_log
     *
     * @mbggenerated do_not_delete_during_merge Fri Nov 15 20:08:51 IST 2013
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table lawma0_data..tbl_rer_admissionsearch_log
     *
     * @mbggenerated Fri Nov 15 20:08:51 IST 2013
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}