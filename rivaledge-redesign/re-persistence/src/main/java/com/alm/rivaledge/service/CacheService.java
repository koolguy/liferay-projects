package com.alm.rivaledge.service;

import java.io.IOException;

import org.springframework.stereotype.Component;

@Component
public interface CacheService
{
	/**
	 * Calculates the news and publications for ALL firms. This is then cached
	 * into a database table that is then read by the portlets (or any other
	 * application) that wishes to get the news and publications data. Of
	 * course, these other applications are free to filter down the data in any
	 * form or manner that they wish.
	 * 
	 * Scheduled job that extracts ALL the firm news and publications data from
	 * the LMD database and puts it into two cache tables.
	 * 
	 * 1. tbl_rer_cache_firm_newspubs: Used by RER to show Firm news & pubs 2.
	 * tbl_lmd_cache_firm_newspubs: Used by LMD to show Firm news & pubs
	 * 
	 * @return statusCode
	 */
	public int firmNewsAndPublicationsJob() throws IOException;

	/**
	 * Scheduled job that pulls in ALL the attorney profile data from LMD and
	 * stores it into a cache table, for RER. This cache table will then be used
	 * by RER and LMD to render attorney statistics by firm.
	 * 
	 * 1. tbl_rer_cache_attorney_data
	 */
	public int attorneySearchDataJob() throws IOException;

	/**
	 * Scheduled job that pulls in ALL the attorney movements data from LMD and
	 * stores it into a cache table, for RER. This cache table will then be used
	 * by RER and LMD to render attorney movement statistics by firm.
	 * 
	 * 1. tbl_rer_cache_attorney_moveschanges
	 * 
	 * @return
	 * @throws Exception
	 */
	public int attorneyMovesAndChangesJob() throws Exception;

	/**
	 * Scheduled job that uses the attorney data cache table to generate a
	 * cached store of the firm statistics.
	 * 
	 * This is NOT the same as the attorney search or movements, that is
	 * something else altogether
	 * 
	 * 1. tbl_rer_cache_firm_statistics 2. tbl_rer_cache_firm_statistics_results
	 * 
	 * @return
	 * @throws Exception
	 */
	public int firmStatisticsJob() throws Exception;

	public int eventsJob() throws IOException;
}
