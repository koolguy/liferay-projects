package com.alm.rivaledge.persistence.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alm.rivaledge.persistence.dao.lawma0_data.PersonMapper;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.Person;
import com.alm.rivaledge.persistence.domain.lawma0_data.PersonExample;
import com.alm.rivaledge.persistence.domain.lawma0_data.PersonExample.Criteria;
import com.alm.rivaledge.transferobject.PeopleSearchDTO;

/**
 * Data Access Object that handles all persistence for the Person domain object.
 * 
 * @author FL867
 * @since Sprint 2
 */
@Repository
public class PersonDAO
{
	@Autowired
	private PersonMapper	personMapper;

	/**
	 * Find a Person by primary key (person id).
	 * @param personID
	 * @return Person
	 */
	public Person findByPK(Integer personID)
	{
		return (personMapper.selectByPrimaryKey(personID));
	}
	
	public List<Person> findPersonData(PeopleSearchDTO psDTO, int resultSetSizeCap)
	{
		return (personMapper.findPersonData(psDTO));
	}
	
	public List<Person> oldFindPersonData(PeopleSearchDTO psDTO, int resultSetSizeCap)
	{
		// Instantiate the PersonExample and Criteria
		PersonExample pEx = new PersonExample();
		Criteria peCriteria = pEx.createCriteria();

		// First criterion is if the user has narrowed
		// the search to a specific set of Firms
		if ((psDTO.getSelectedFirms() != null) && (!psDTO.getSelectedFirms().isEmpty()))
		{
			// Individual firms have been specified
			// Is it one specific firm or many of them?
			if (psDTO.getSelectedFirms().size() == 1)
			{
				// Just one single firm
				peCriteria.andCompanyIdEqualTo(psDTO.getSelectedFirms().iterator().next().getCompanyId());
			}
			else
			{
				// Multiple firms have been selected
				List<Integer> companyIdsList = new ArrayList<Integer>(psDTO.getSelectedFirms().size());
				for (Firm tF : psDTO.getSelectedFirms())
				{
					companyIdsList.add(tF.getCompanyId());
				}
				peCriteria.andCompanyIdIn(companyIdsList);
			}
		}

		// Second criterion is if the customer has limited the
		// search to specific practice areas or wants to search
		// across all practice areas.
		if ((psDTO.getPracticeArea() != null) && (!psDTO.getPracticeArea().isEmpty()))
		{
			// User wants to restrict the search to specific
			// practice area(s). Is it one specific practice
			// area or many?
			if (psDTO.getPracticeArea().size() == 1)
			{
				// Just one practice area
				peCriteria.andPrimaryStdPracticeEqualTo(psDTO.getPracticeArea().iterator().next());
			}
			else
			{
				// Multiple practice areas selected
				peCriteria.andPrimaryStdPracticeIn(psDTO.getPracticeArea());
			}
		}

		// Third criterion is if the customer has limited the
		// search to specific locations or wants to search across
		// all locations.
		if ((psDTO.getLocations() != null) && (!psDTO.getLocations().isEmpty()))
		{
			// User wants to restrict the search to specific
			// location(s). Is it one specific location or many?
			if (psDTO.getLocations().size() == 1)
			{
				// Just one location
				peCriteria.andStdLocEqualTo(psDTO.getLocations().iterator().next());
			}
			else
			{
				// Multiple locations selected
				peCriteria.andStdLocIn(psDTO.getLocations());
			}
		}

		// Fourth criterion is if the customer has limited the
		// search to specific company sizes.
		// TODO

		// Fifth criterion is if the customer has limited the
		// search by date.
		// TODO

		// Set the criteria
		pEx.or(peCriteria);

		// Set the ordering
		pEx.setOrderByClause("company_id, std_name asc");

		// TODO: Set the result set size limit

		// Execute and return the result set
		return (personMapper.selectByExample(pEx));
	}

	/**
	 * Given a firm id, find all the attorneys (partners, associates, and other
	 * counsel; NOT admin and other staff).
	 * 
	 * @param firmId
	 * @return List<Person>
	 */
	public List<Person> findAttorneysByFirm(Integer firmId)
	{
		// Instantiate the PersonExample and Criteria
		PersonExample pEx = new PersonExample();

		List<String> attorneyList = new ArrayList<String>(3);
		attorneyList.add("Partner");
		attorneyList.add("Associate");
		attorneyList.add("Other Counsel/Attorney");

		// Set the criteria
		pEx.or().andCompanyIdEqualTo(firmId).andStdTitleIn(attorneyList);

		// Execute and return the result set
		return (personMapper.selectByExample(pEx));
	}
}
