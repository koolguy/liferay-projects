package com.alm.rivaledge.transferobject;

import com.alm.rivaledge.transferobject.AttorneyAction;
import com.alm.rivaledge.util.ALMConstants;

/**
 * This is the DTO that will be returned when a search for Attorney is run.
 * 
 * @author FL605
 * @since Sprint 3
 * 
 */
public class AttorneyResultDTO extends BaseSearchDTO 
{

	private static final long serialVersionUID = 8849872378553454535L;
	
	private Integer				attorneyId;    	
	private String				attorneyName; 
	private String				attorneyLink;	
	private Integer				firmId;			
	private String				firmName;		//column #5
	private String				firmLink;
	private String				title;			//column #7
	private String				location;		//column #8
	private String				practices;		//column #9
	private String				education;
	private String				admission;
	private String				graduationYear;
	private String				keywords;
	private String				tags;
	private String				contactInfo;
	private AttorneyAction		lastAction;

	
	public AttorneyResultDTO()
	{
		super();
		
		setSortColumn(-1); //No sorting by default
		setOrderBy(5); //  Firm Name by default
		
	}
	public Integer getAttorneyId()
	{
		return attorneyId;
	}

	public void setAttorneyId(Integer attorneyId)
	{
		this.attorneyId = attorneyId;
	}

	public String getAttorneyName()
	{
		return attorneyName;
	}

	public void setAttorneyName(String attorneyName)
	{
		this.attorneyName = attorneyName;
	}

	public String getAttorneyLink()
	{
		return attorneyLink;
	}

	public void setAttorneyLink(String attorneyLink)
	{
		this.attorneyLink = attorneyLink;
	}

	public Integer getFirmId()
	{
		return firmId;
	}

	public void setFirmId(Integer firmId)
	{
		this.firmId = firmId;
	}

	public String getFirmName()
	{
		return firmName;
	}

	public void setFirmName(String firmName)
	{
		this.firmName = firmName;
	}

	public String getFirmLink()
	{
		return firmLink;
	}

	public void setFirmLink(String firmLink)
	{
		this.firmLink = firmLink;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getLocation()
	{
		return location;
	}

	public void setLocation(String location)
	{
		this.location = location;
	}

	public String getPractices()
	{
		return practices;
	}

	public void setPractices(String practices)
	{
		this.practices = practices;
	}

	public String getEducation()
	{
		return education;
	}

	public void setEducation(String education)
	{
		this.education = education;
	}

	public String getAdmission()
	{
		return admission;
	}

	public void setAdmission(String admission)
	{
		this.admission = admission;
	}

	public String getGraduationYear()
	{
		return graduationYear;
	}

	public void setGraduationYear(String graduationYear)
	{
		this.graduationYear = graduationYear;
	}

	public String getKeywords()
	{
		return keywords;
	}

	public void setKeywords(String keywords)
	{
		this.keywords = keywords;
	}

	public String getTags()
	{
		return tags;
	}

	public void setTags(String tags)
	{
		this.tags = tags;
	}

	public String getContactInfo()
	{
		return contactInfo;
	}

	public void setContactInfo(String contactInfo)
	{
		this.contactInfo = contactInfo;
	}

	public AttorneyAction getLastAction()
	{
		return lastAction;
	}
	public void setLastAction(AttorneyAction lastAction)
	{
		this.lastAction = lastAction;
	}
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append(attorneyId);
		builder.append("||");
		builder.append(attorneyName);
		builder.append("||");
		builder.append((attorneyLink != null && attorneyLink.trim().length() > 0) ? attorneyLink : "\\N");
		builder.append("||");
		builder.append(firmId);
		builder.append("||");
		builder.append(firmName);
		builder.append("||");
		builder.append((firmLink != null && firmLink.trim().length() > 0) ? firmLink : "\\N");
		builder.append("||");
		builder.append(title);
		builder.append("||");
		builder.append((location != null && location.trim().length() > 0) ? location : "\\N");
		builder.append("||");
		builder.append((practices != null && practices.trim().length() > 0) ? practices : "\\N");
		builder.append("||");
		builder.append((education != null && education.trim().length() > 0) ? education : "\\N");
		builder.append("||");
		builder.append((admission != null && admission.trim().length() > 0) ? admission : "\\N");
		builder.append("||");
		builder.append((graduationYear != null && graduationYear.trim().length() > 0) ? graduationYear : "\\N");
		builder.append("||");
		builder.append((keywords != null && keywords.trim().length() > 0) ? keywords : "\\N");
		builder.append("||");
		builder.append((tags != null) ? tags : "\\N");
		builder.append("||");
		builder.append((contactInfo != null && contactInfo.trim().length() > 0) ? contactInfo : "\\N");
		if (contactInfo != null && !contactInfo.endsWith("||"))
		{
			builder.append("||");
		}
		builder.append((lastAction != null && lastAction.getActionDate() != null) ? ALMConstants.SEARCH_DATE_FORMAT.format(lastAction.getActionDate()) : "\\N");
		builder.append("||");
		builder.append((lastAction != null && lastAction.getMoveAction() != null) ? lastAction.getMoveAction() : "\\N");
		builder.append("$$");
		return builder.toString();
	}

}
