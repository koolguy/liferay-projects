package com.alm.rivaledge.persistence.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;

import com.alm.rivaledge.persistence.dao.lawma0_data.CacheMapper;
import com.alm.rivaledge.transferobject.PeopleCacheResult;
import com.alm.rivaledge.transferobject.PeopleResultDTO;
import com.alm.rivaledge.transferobject.PeopleSearchDTO;

public class ThreadedFirmStatsSearch implements Callable<PeopleResultDTO>
{
	private CacheMapper 				  cacheMapper;
	private PeopleSearchDTO 			  psDTO;
	private List<PeopleResultDTO> 		  returnList	 = new ArrayList<PeopleResultDTO>();
	private Map<Integer, PeopleResultDTO> processedFirms = new ConcurrentHashMap<Integer, PeopleResultDTO>();
	
	public ThreadedFirmStatsSearch(PeopleSearchDTO searchDTO, CacheMapper searchMapper)
	{
		super();
		this.psDTO  	 = searchDTO;
		this.cacheMapper = searchMapper;
	}

	@Override
	public PeopleResultDTO call() throws Exception
	{
		PeopleResultDTO 			thisDTO  		= null;
		List<Integer> 				attorneyList 	= cacheMapper.findAttorneysMatchingFirmStats(psDTO);
		Map<String, List<Integer>> 	attorneyMap 	= new HashMap<String, List<Integer>>(1);
		if ((attorneyList != null) && (!attorneyList.isEmpty()))
		{
			attorneyMap.put("attorneyList", ((attorneyList != null) && (!attorneyList.isEmpty())) ? attorneyList : null);
			
			for (PeopleCacheResult thisResult : cacheMapper.findFirmStats(attorneyMap))
			{
				if (processedFirms.containsKey(thisResult.getFirmId()))
				{
					// Existing PeopleResultDTO; get it from the map
					thisDTO = processedFirms.get(thisResult.getFirmId());
				}
				else
				{
					thisDTO = new PeopleResultDTO();
					thisDTO.setCompanyId(psDTO.getSelectedFirms().get(0).getCompanyId());
					thisDTO.setCompanyName(psDTO.getSelectedFirms().get(0).getCompany());
					thisDTO.setCompanyWebsite(psDTO.getSelectedFirms().get(0).getHomeUrl());
				}
				if (thisResult.getCountType().equals(PeopleResultDTO.HEAD_COUNT))
				{
					// Add this result as a PeopleSearchResult within the
					// PeopleResultDTO
					thisDTO.addPracticeOrLocationToHeadCount(thisResult);
				}
				else if (thisResult.getCountType().equals(PeopleResultDTO.MOVEMENT_COUNT))
				{
					if (((psDTO.getFromDate() != null) && (psDTO.getToDate() != null) && (thisResult.getLastActionDate() != null))
							&& ((thisResult.getLastActionDate().after(psDTO.getFromDate())) && (thisResult.getLastActionDate().before(psDTO.getToDate()))))
					{
						thisDTO.addPracticeOrLocationToMovementCount(thisResult);
					}
				}

				// Add this resultDTO to the map
				processedFirms.put(thisResult.getFirmId(), thisDTO);
			}
		}
		return (thisDTO);
	}
	
	public List<PeopleResultDTO> getReturnList()
	{
		for (Integer tF : processedFirms.keySet())
		{
			this.returnList.add(processedFirms.get(tF));
		}
		return (this.returnList);
	}
}