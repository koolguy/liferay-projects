package com.alm.rivaledge.transferobject;

import java.io.Serializable;
import java.util.Date;

import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.util.ALMConstants;

/**
 * This is the DTO that will be returned when a search for Event is run.
 * 
 * @author FL613
 * @since Sprint 4
 */
public class EventResultDTO implements Serializable
{
	private static final long	serialVersionUID	= -3376557653172171655L;

	private Integer				eventId;
	private Integer				companyId; //column #2
	private Date				eventDate;
	private String				eventURL;
	private String				eventTitle;
	private String				eventDescription;
	private String				practiceArea; //column #6
	private String				location; //column #7

	private Firm				resultFirm;  

	public Integer getEventId()
	{
		return eventId;
	}

	public void setEventId(Integer eventId)
	{
		this.eventId = eventId;
	}

	public Integer getCompanyId()
	{
		return companyId;
	}

	public void setCompanyId(Integer companyId)
	{
		this.companyId = companyId;
	}

	public Date getEventDate()
	{
		return eventDate;
	}

	public void setEventDate(Date eventDate)
	{
		this.eventDate = eventDate;
	}

	public Firm getResultFirm()
	{
		return resultFirm;
	}

	public void setResultFirm(Firm resultFirm)
	{
		this.resultFirm = resultFirm;
	}

	public String getEventURL()
	{
		return eventURL;
	}

	public void setEventURL(String eventURL)
	{
		this.eventURL = eventURL;
	}

	public String getEventTitle()
	{
		return eventTitle;
	}

	public void setEventTitle(String eventTitle)
	{
		this.eventTitle = eventTitle;
	}

	public String getEventDescription()
	{
		return eventDescription;
	}

	public void setEventDescription(String eventDescription)
	{
		this.eventDescription = eventDescription;
	}

	public String getPracticeArea()
	{
		return practiceArea;
	}

	public void setPracticeArea(String practiceArea)
	{
		this.practiceArea = practiceArea;
	}

	public String getLocation()
	{
		return location;
	}

	public void setLocation(String location)
	{
		this.location = location;
	}

	@Override
	public String toString()
	{
		//company_id, event_date, event_title, event_description, location, practice_area
		StringBuilder builder = new StringBuilder();
		builder.append(companyId);
		builder.append("||");
		builder.append((eventDate != null) ? ALMConstants.SEARCH_DATE_FORMAT.format(eventDate) : "\\N");
		builder.append("||");
		builder.append(((eventURL != null) && (eventURL.trim().length() > 0)) ? eventURL : "\\N");
		builder.append("||");
		builder.append(((eventTitle != null) && (eventTitle.trim().length() > 0)) ? eventTitle : "\\N");
		builder.append("||");
		builder.append(((eventDescription != null) && (eventDescription.trim().length() > 0)) ? eventDescription : "\\N");
		builder.append("||");
		builder.append(((location != null) && (location.trim().length() > 0)) ? location : "\\N");
		builder.append("||");
		builder.append(((practiceArea != null) && (practiceArea.trim().length() > 0)) ? practiceArea : "\\N");
		builder.append("$$");
		return builder.toString();
	}
}
