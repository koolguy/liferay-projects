package com.alm.rivaledge.persistence.dao.lawma0_data;

import java.util.List;
import java.util.Map;

import com.alm.rivaledge.transferobject.AttorneyGraduationResultDTO;
import com.alm.rivaledge.transferobject.AttorneyMoveChangesResultDTO;
import com.alm.rivaledge.transferobject.AttorneyMoveChangesSearchDTO;
import com.alm.rivaledge.transferobject.AttorneyResultDTO;
import com.alm.rivaledge.transferobject.AttorneySearchDTO;
import com.alm.rivaledge.transferobject.EventResultDTO;
import com.alm.rivaledge.transferobject.EventSearchDTO;
import com.alm.rivaledge.transferobject.FirmResultDTO;
import com.alm.rivaledge.transferobject.FirmSearchDTO;
import com.alm.rivaledge.transferobject.PeopleCacheResult;
import com.alm.rivaledge.transferobject.PeopleResult;
import com.alm.rivaledge.transferobject.PeopleResultDTO;
import com.alm.rivaledge.transferobject.PeopleSearchDTO;
import com.alm.rivaledge.transferobject.PeopleSubResult;

public interface CacheMapper
{
	/**
	 * Creates the cache table for Firm news and pubs
	 * tbl_rer_cache_firm_newspubs
	 */
	void createFirmNewsPubsCacheTable();

	/**
	 * Deletes the cache table for Firm news and pubs
	 * tbl_rer_cache_firm_newspubs
	 */
	void dropFirmNewsPubsCacheTable();

	/**
	 * Bulk insert into the cache table for Firm news
	 * tbl_rer_cache_firm_newspubs
	 */
	void bulkInsertFirmNewsCache();

	/**
	 * Bulk insert into the cache table for Firm pubs
	 * tbl_rer_cache_firm_newspubs
	 */
	void bulkInsertFirmPubsCache();
	/**
	 * Bulk insert into the cache table for Firm Tweets
	 * tbl_rer_cache_firm_newspubs
	 */
	void bulkInsertFirmTweetsCache();

	List<FirmResultDTO> findFirmNewsPubs(FirmSearchDTO searchFormTo);
	
	/**
	 * Latest top five publications by practice area
	 */
	
	List<FirmResultDTO> findFiveLatestPublicationsByPractice(FirmSearchDTO firmSearchDTO);

	int findFirmDataCount(FirmSearchDTO fsDTO);
	
	List<PeopleResult> findPeopleResultByGroupId(Integer groupId);
	
	List<PeopleResult> findPeopleResultsByCriteria(PeopleSearchDTO psDTO);

	/**
	 * Creates the cache table for Firm statistics (people movement counts
	 * across firms, practices, and office locations)
	 * tbl_rer_cache_firm_statistics tbl_rer_cache_firm_statistics_results
	 */
	void createFirmStatisticsCacheDTOTable();
//	
//	void createFirmStatisticsCacheResultTable();

	/**
	 * Deletes the cache table for Firm statistics (people movement counts
	 * across firms, practices, and office locations)
	 * tbl_rer_cache_firm_statistics_results
	 */
	void dropFirmStatisticsCacheDTOTable();
//	
//	void dropFirmStatisticsCacheResultTable();

	/**
	 * Finds all the attorneys for a given law firm.
	 */
	List<AttorneyResultDTO> findAllAttorneysByCompany(Integer companyId);
	
	Integer findAllAttorneysCountByCompany(Integer companyId);

	/**
	 * Bulk insert into the attorney cache table for Attorney search
	 * tbl_rer_cache_attorney_data
	 */
	void bulkInsertAttorneyCache();
	
	void bulkUpdateAttorneyCache();

	void dropAttorneyDataCacheTable();

	void createAttorneyDataCacheTable();

	/**
	 * Find all the attorneys who satisfy the given search DTO criteria
	 * 
	 * @param attorneySearchDTO
	 * @return
	 */
	List<AttorneyResultDTO> findAttorneyData(AttorneySearchDTO attorneySearchDTO);
	
	List<AttorneyResultDTO> findAttorneyDataWithMutipleNameString(AttorneySearchDTO attorneySearchDTO);
	
	/**
	 * Find the count of all the attorneys who satisfy the given search DTO criteria
	 * 
	 * @param attorneySearchDTO
	 * @return
	 */
	List<AttorneyResultDTO> findAttorneyDataCount(AttorneySearchDTO massageAttorneySearchDTO);
	
	/**
	 * Find the count of all the attorneys who satisfy the given search DTO criteria and would invoke after findAttorneyDataCount method,
	 * when user has entered multiple string in Attorney name String
	 * @param massageAttorneySearchDTO
	 * @return
	 */
	int findAttorneyDataCountWithMutipleNameString(AttorneySearchDTO massageAttorneySearchDTO);
	
	/**
	 *  Find all the Firms who satisfy the given search DTO criteria for No of Attorneys by Graduation Year
	 * @param attorneySearchDTO
	 * @return
	 */
	List<AttorneyResultDTO> findAttorneyDataForcharts(AttorneySearchDTO attorneySearchDTO);
	
	
	/**
	 * Find all attorneys count in different title by graduation year who satisfy the given search DTO criteria for No of Attorneys by Graduation Year 
	 * @param attorneySearchDTO
	 * @return
	 */
	List<AttorneyGraduationResultDTO> findAttorneyDataForGradcharts(AttorneySearchDTO attorneySearchDTO);
	
	/**
	 * Given a firm id, returns a list of all the attorneys for that firm who
	 * have been cached in the attorney_data cache.
	 * 
	 * @return
	 */
	List<AttorneyResultDTO> findAttorneysByFirmFromCache(Integer firmId);

	/**
	 * Persist an instance of PeopleResultDTO to the cache table.
	 * 
	 * @param thisResult
	 */
	Integer insertPeopleResultDTO(PeopleResultDTO thisResult);

	Integer findGroupIdByFirmId(Integer companyId);
	
	Integer findGroupIdByCollectionName(String collectionName);

	/**
	 * Persist an instance of PeopleResult to the cache table.
	 * 
	 * @param thisResult
	 */
	void insertPeopleResult(PeopleResult pResult);

	List<Integer> findFirmIdsFromAttorneyDataCache();

	PeopleResultDTO findPeopleResultDTOByFirm(Integer companyId);

	List<PeopleResult> findFirmStatistics(PeopleSearchDTO psDTO);
	
	List<PeopleResult> findLocationPracticeHeadCounts(PeopleSearchDTO psDTO);
	
	List<PeopleResult> findBasicHeadCounts(PeopleSearchDTO psDTO);
	
	List<PeopleResult> findFirmStatisticsMovements(PeopleSearchDTO psDTO);
	
	/**
	 * Returns the result count for given search criteria
	 * @param psDTO
	 * @return
	 */
	List<Integer> findFirmStatisticsDataCount(PeopleSearchDTO psDTO);

	PeopleResultDTO findFirmStatisticsByCollection(String collectionName);

	/**
	 * Drops the event cache table
	 * tbl_rer_cache_events
	 */
	void dropEventsCacheTable();

	List<EventResultDTO> findAllLawFirmEvents(int offset);
	
	List<EventResultDTO> findAllOrganisationEvents(int offset);

	void createEventsCacheTable();

	void bulkInsertLawFirmEventsCache();
	
	void bulkInsertOrganisationsEventsCache();

	List<EventResultDTO> findEventData(EventSearchDTO esDTO);

	Integer findEventDataCount(EventSearchDTO massageEventSearchDTO);

	List<PeopleCacheResult> findFirmStatsByCompanyId(Integer firmId);
	
	List<PeopleSubResult> findFirmStatsForCacheByCompanyId(Integer firmId);
	
	List<PeopleSubResult> findFirmStatsMovementsByCompanyId(Integer firmId);
	
	List<PeopleSubResult> findFirmStatsForPracticesByCompanyId(Integer firmId);
	
	List<PeopleSubResult> findFirmStatsMovementsForPracticesByCompanyId(Integer firmId);

	List<PeopleSubResult> findFirmStatsForLocationsByCompanyId(Integer firmId);
	
	List<PeopleSubResult> findFirmStatsMovementsForLocationsByCompanyId(Integer firmId);

	void dropAttorneyMovesChangesCacheTable();

	void createAttorneyMovesChangesCacheTable();

	void bulkInsertAttorneyMovesChangesCache();

	List<AttorneyMoveChangesResultDTO> findAttorneyMovementsCount(AttorneyMoveChangesSearchDTO searchDTO);
	
	int findAttorneyMovementsCountWithMutipleNameString(AttorneyMoveChangesSearchDTO searchDTO);

	List<AttorneyMoveChangesResultDTO> findAttorneyMovementsData(AttorneyMoveChangesSearchDTO searchDTO);
	
	List<AttorneyMoveChangesResultDTO> findAttorneyMovementsDataWithMutipleNameString(AttorneyMoveChangesSearchDTO searchDTO);

//	List<String> findFirmStatisticsPractices(PeopleSearchDTO searchDTO);
//
//	List<String> findFirmStatisticsLocations(PeopleSearchDTO searchDTO);
	
	List<String> findMasterPractices();

	void bulkInsertFirmStatistics(String localInfile);

	List<PeopleCacheResult> findFirmStats(Map<String, List<Integer>> attorneyMap);
	
	List<Integer> findAttorneysMatchingFirmStats(PeopleSearchDTO searchDTO);
	
	void dropFirmStatisticsHeadCountCacheTable();
	
	void createFirmStatisticsHeadCountCacheTable();
	
	void bulkInsertFirmStatisticsHeadCountCache();
	
	void createFirmAttorneyPracticesTable();
	
	void dropFirmAttorneyPracticesTable();
	
	void bulkInsertFirmAttorneyPracticeCache();
	
	List<Integer> findAllAttorneyFirmsFromCache();
}
