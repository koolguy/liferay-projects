package com.alm.rivaledge.persistence.domain.lmd;

import java.util.Date;

/**
 * Value Object (VO) for the Firm News and Pubs cache. This VO is used by RER to
 * write cache data for LMD.
 * 
 * @author FL867
 * @since Sprint 4
 * @version 1.0
 */
public class LMDFirmNewsPubsVO
{
	private Integer	entryId;
	private Integer	companyId;
	private String	company;
	private String	companyType;
	private String	domain;
	private String	homeUrl;
	private Integer	urlId;
	private String	dataSource;
	private String	entrySource;
	private String	category;
	private String	type;
	private String	entryMethod;
	private String	link;
	private String	title;
	private String	descrip;
	private String	loc;
	private String	city;
	private String	state;
	private String	country;
	private String	region;
	private String	date;
	private Date	stdDate;
	private String	inds;
	private String	offices;
	private Integer	confId;
	private String	confLink;
	private String	practices;
	private String	stdPractices;
	private Date	dateAdded;

	public Integer getEntryId()
	{
		return entryId;
	}

	public void setEntryId(Integer entryId)
	{
		this.entryId = entryId;
	}

	public Integer getCompanyId()
	{
		return companyId;
	}

	public void setCompanyId(Integer companyId)
	{
		this.companyId = companyId;
	}

	public String getCompany()
	{
		return company;
	}

	public void setCompany(String company)
	{
		this.company = company;
	}

	public String getCompanyType()
	{
		return companyType;
	}

	public void setCompanyType(String companyType)
	{
		this.companyType = companyType;
	}

	public String getDomain()
	{
		return domain;
	}

	public void setDomain(String domain)
	{
		this.domain = domain;
	}

	public String getHomeUrl()
	{
		return homeUrl;
	}

	public void setHomeUrl(String homeUrl)
	{
		this.homeUrl = homeUrl;
	}

	public Integer getUrlId()
	{
		return urlId;
	}

	public void setUrlId(Integer urlId)
	{
		this.urlId = urlId;
	}

	public String getDataSource()
	{
		return dataSource;
	}

	public void setDataSource(String dataSource)
	{
		this.dataSource = dataSource;
	}

	public String getEntrySource()
	{
		return entrySource;
	}

	public void setEntrySource(String entrySource)
	{
		this.entrySource = entrySource;
	}

	public String getCategory()
	{
		return category;
	}

	public void setCategory(String category)
	{
		this.category = category;
	}

	public String getType()
	{
		return type;
	}

	public void setType(String type)
	{
		this.type = type;
	}

	public String getEntryMethod()
	{
		return entryMethod;
	}

	public void setEntryMethod(String entryMethod)
	{
		this.entryMethod = entryMethod;
	}

	public String getLink()
	{
		return link;
	}

	public void setLink(String link)
	{
		this.link = link;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getDescrip()
	{
		return descrip;
	}

	public void setDescrip(String descrip)
	{
		this.descrip = descrip;
	}

	public String getLoc()
	{
		return loc;
	}

	public void setLoc(String loc)
	{
		this.loc = loc;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}

	public String getState()
	{
		return state;
	}

	public void setState(String state)
	{
		this.state = state;
	}

	public String getCountry()
	{
		return country;
	}

	public void setCountry(String country)
	{
		this.country = country;
	}

	public String getRegion()
	{
		return region;
	}

	public void setRegion(String region)
	{
		this.region = region;
	}

	public String getDate()
	{
		return date;
	}

	public void setDate(String date)
	{
		this.date = date;
	}

	public Date getStdDate()
	{
		return stdDate;
	}

	public void setStdDate(Date stdDate)
	{
		this.stdDate = stdDate;
	}

	public String getInds()
	{
		return inds;
	}

	public void setInds(String inds)
	{
		this.inds = inds;
	}

	public String getOffices()
	{
		return offices;
	}

	public void setOffices(String offices)
	{
		this.offices = offices;
	}

	public Integer getConfId()
	{
		return confId;
	}

	public void setConfId(Integer confId)
	{
		this.confId = confId;
	}

	public String getConfLink()
	{
		return confLink;
	}

	public void setConfLink(String confLink)
	{
		this.confLink = confLink;
	}

	public String getPractices()
	{
		return practices;
	}

	public void setPractices(String practices)
	{
		this.practices = practices;
	}

	public String getStdPractices()
	{
		return stdPractices;
	}

	public void setStdPractices(String stdPractices)
	{
		this.stdPractices = stdPractices;
	}

	public Date getDateAdded()
	{
		return dateAdded;
	}

	public void setDateAdded(Date dateAdded)
	{
		this.dateAdded = dateAdded;
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append(entryId);
		builder.append("||");
		builder.append(companyId);
		builder.append("||");
		builder.append(company);
		builder.append("||");
		builder.append(companyType);
		builder.append("||");
		builder.append(domain);
		builder.append("||");
		builder.append(homeUrl);
		builder.append("||");
		builder.append(urlId);
		builder.append("||");
		builder.append(dataSource);
		builder.append("||");
		builder.append(entrySource);
		builder.append("||");
		builder.append(category);
		builder.append("||");
		builder.append(type);
		builder.append("||");
		builder.append(entryMethod);
		builder.append("||");
		builder.append(link);
		builder.append("||");
		builder.append(title);
		builder.append("||");
		builder.append(descrip);
		builder.append("||");
		builder.append(loc);
		builder.append("||");
		builder.append(city);
		builder.append("||");
		builder.append(state);
		builder.append("||");
		builder.append(country);
		builder.append("||");
		builder.append(region);
		builder.append("||");
		builder.append(date);
		builder.append("||");
		builder.append(stdDate);
		builder.append("||");
		builder.append(inds);
		builder.append("||");
		builder.append(offices);
		builder.append("||");
		builder.append(confId);
		builder.append("||");
		builder.append(confLink);
		builder.append("||");
		builder.append(practices);
		builder.append("||");
		builder.append(stdPractices);
		builder.append("||");
		builder.append(dateAdded);
		builder.append("$$");
		return builder.toString();
	}
}
