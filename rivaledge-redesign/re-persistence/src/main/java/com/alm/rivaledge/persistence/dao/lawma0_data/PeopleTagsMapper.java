package com.alm.rivaledge.persistence.dao.lawma0_data;

import com.alm.rivaledge.persistence.domain.lawma0_data.PeopleTags;
import com.alm.rivaledge.persistence.domain.lawma0_data.PeopleTagsExample;
import com.alm.rivaledge.persistence.domain.lawma0_data.PeopleTagsKey;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PeopleTagsMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_tags
     *
     * @mbggenerated Wed Jul 31 18:41:43 IST 2013
     */
    int countByExample(PeopleTagsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_tags
     *
     * @mbggenerated Wed Jul 31 18:41:43 IST 2013
     */
    int deleteByExample(PeopleTagsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_tags
     *
     * @mbggenerated Wed Jul 31 18:41:43 IST 2013
     */
    int deleteByPrimaryKey(PeopleTagsKey key);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_tags
     *
     * @mbggenerated Wed Jul 31 18:41:43 IST 2013
     */
    int insert(PeopleTags record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_tags
     *
     * @mbggenerated Wed Jul 31 18:41:43 IST 2013
     */
    int insertSelective(PeopleTags record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_tags
     *
     * @mbggenerated Wed Jul 31 18:41:43 IST 2013
     */
    List<PeopleTags> selectByExample(PeopleTagsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_tags
     *
     * @mbggenerated Wed Jul 31 18:41:43 IST 2013
     */
    PeopleTags selectByPrimaryKey(PeopleTagsKey key);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_tags
     *
     * @mbggenerated Wed Jul 31 18:41:43 IST 2013
     */
    int updateByExampleSelective(@Param("record") PeopleTags record, @Param("example") PeopleTagsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_tags
     *
     * @mbggenerated Wed Jul 31 18:41:43 IST 2013
     */
    int updateByExample(@Param("record") PeopleTags record, @Param("example") PeopleTagsExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_tags
     *
     * @mbggenerated Wed Jul 31 18:41:43 IST 2013
     */
    int updateByPrimaryKeySelective(PeopleTags record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_tags
     *
     * @mbggenerated Wed Jul 31 18:41:43 IST 2013
     */
    int updateByPrimaryKey(PeopleTags record);
}