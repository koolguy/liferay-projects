package com.alm.rivaledge.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserPreferences;

/**
 * Handles all business logic for the User entity.
 * 
 * @author FL867
 * @since Sprint 6
 * @version 1.0
 */
@Component
public interface UserService
{
	/**
	 * Find a user by id (PK)
	 * 
	 * @param userId
	 * @return
	 */
	public User getUserById(Integer userId);
	
	/**
	 * Given a user id, return all the watch lists the user owns.
	 * 
	 * @param userId
	 * @return
	 */
	public List<UserGroup> getUserWatchlists(Integer userId);

	public User getUserByEmail(String currentUserEmailAddress);
	
	public UserPreferences getUserPreferenceByUserIdAndPortletId(Integer userId, String portletId, String currentPage);

	public void saveUserPreferences(Integer id, String currentPortletId, String searchCriteria, String chartCriteria, String currentPage);

}
