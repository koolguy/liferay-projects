package com.alm.rivaledge.transferobject;

import java.io.Serializable;

/**
 * The Data Transfer Object (DTO) that will be returned  when
 * a filter on watchlist screen is run.
 * @author FL605
 * @since Sprint 6
 */
public class WatchlistCriteriaResultDTO implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6973410474757243333L;
	
	private Integer 	firmId;
	private String		firmName;
	private String		firmURL;
	private Integer		attorneyCount;
	private Integer		amlawRank;
	private Integer		locationCount;
	private Integer		practiceCount;
	
	public WatchlistCriteriaResultDTO() 
	{
		
	}
	
	public Integer getFirmId() 
	{
		return firmId;
	}
	
	public void setFirmId(Integer firmId) 
	{
		this.firmId = firmId;
	}
	
	public String getFirmName() 
	{
		return firmName;
	}
	
	public void setFirmName(String firmName) 
	{
		this.firmName = firmName;
	}
	public String getFirmURL() {
		return firmURL;
	}
	
	public void setFirmURL(String firmURL)
	{
		this.firmURL = firmURL;
	}
	
	public Integer getAttorneyCount()
	{
		return attorneyCount;
	}
	
	public void setAttorneyCount(Integer attorneyCount) 
	{
		this.attorneyCount = attorneyCount;
	}
	
	public Integer getAmlawRank() 
	{
		return amlawRank;
	}
	
	public void setAmlawRank(Integer amlawRank) 
	{
		this.amlawRank = amlawRank;
	}
	
	public Integer getLocationCount() 
	{
		return locationCount;
	}
	
	public void setLocationCount(Integer locationCount) 
	{
		this.locationCount = locationCount;
	}
	
	public Integer getPracticeCount()
	{
		return practiceCount;
	}
	
	public void setPracticeCount(Integer practiceCount) 
	{
		this.practiceCount = practiceCount;
	}

	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((firmId == null) ? 0 : firmId.hashCode());
		result = prime * result
				+ ((firmName == null) ? 0 : firmName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WatchlistCriteriaResultDTO other = (WatchlistCriteriaResultDTO) obj;
		if (firmId == null) 
		{
			if (other.firmId != null)
				return false;
		} else if (!firmId.equals(other.firmId))
			return false;
		if (firmName == null) 
		{
			if (other.firmName != null)
				return false;
		} else if (!firmName.equals(other.firmName))
			return false;
		return true;
	}
	
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append(firmId);
		builder.append("||");
		builder.append(firmName);
		builder.append("||");
		builder.append((firmURL != null) ? firmURL : "\\N");
		builder.append("||");
		builder.append(attorneyCount);
		builder.append("||");
		builder.append(amlawRank);
		builder.append("||");
		builder.append(locationCount);		
		builder.append("||");
		builder.append(practiceCount);
		builder.append("$$");
		return builder.toString();
	}
	
	

}
