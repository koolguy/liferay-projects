package com.alm.rivaledge.persistence.dao.lawma0_data;

import com.alm.rivaledge.persistence.domain.lawma0_data.PersonChange;
import com.alm.rivaledge.persistence.domain.lawma0_data.PersonChangeExample;
import com.alm.rivaledge.persistence.domain.lawma0_data.PersonChangeKey;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PersonChangeMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_changes
     *
     * @mbggenerated Mon Aug 05 11:16:45 BST 2013
     */
    int countByExample(PersonChangeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_changes
     *
     * @mbggenerated Mon Aug 05 11:16:45 BST 2013
     */
    int deleteByExample(PersonChangeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_changes
     *
     * @mbggenerated Mon Aug 05 11:16:45 BST 2013
     */
    int deleteByPrimaryKey(PersonChangeKey key);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_changes
     *
     * @mbggenerated Mon Aug 05 11:16:45 BST 2013
     */
    int insert(PersonChange record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_changes
     *
     * @mbggenerated Mon Aug 05 11:16:45 BST 2013
     */
    int insertSelective(PersonChange record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_changes
     *
     * @mbggenerated Mon Aug 05 11:16:45 BST 2013
     */
    List<PersonChange> selectByExample(PersonChangeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_changes
     *
     * @mbggenerated Mon Aug 05 11:16:45 BST 2013
     */
    PersonChange selectByPrimaryKey(PersonChangeKey key);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_changes
     *
     * @mbggenerated Mon Aug 05 11:16:45 BST 2013
     */
    int updateByExampleSelective(@Param("record") PersonChange record, @Param("example") PersonChangeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_changes
     *
     * @mbggenerated Mon Aug 05 11:16:45 BST 2013
     */
    int updateByExample(@Param("record") PersonChange record, @Param("example") PersonChangeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_changes
     *
     * @mbggenerated Mon Aug 05 11:16:45 BST 2013
     */
    int updateByPrimaryKeySelective(PersonChange record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..people_changes
     *
     * @mbggenerated Mon Aug 05 11:16:45 BST 2013
     */
    int updateByPrimaryKey(PersonChange record);
}