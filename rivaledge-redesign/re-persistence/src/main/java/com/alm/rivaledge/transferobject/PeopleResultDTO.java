package com.alm.rivaledge.transferobject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * This is the DTO that will be returned when a search for people is run. This
 * is a fairly complex DTO; pay attention to the JavaDoc to understand its
 * usage.
 * 
 * @author FL867
 * @since Sprint 2
 * 
 */
@SuppressWarnings("unused")
public class PeopleResultDTO implements Serializable, Comparable<PeopleResultDTO>
{
	private static final long			serialVersionUID		= -1100110628683204656L;

	private Integer						groupingId;
	private Integer						companyId;
	private String						companyName;
	private String						companyWebsite;
	private Set<Integer>				headCountAttorneys		= new TreeSet<Integer>();
	private Set<Integer>				movementCountAttorneys	= new TreeSet<Integer>();
	private List<PeopleResult>			peopleResults			= new ArrayList<PeopleResult>();
	private Map<String, PeopleResult>	officeLocationsMap		= new TreeMap<String, PeopleResult>(new Comparator<String>()
	{
		@Override
		public int compare(String arg0, String arg1)
		{
			if ((arg0.equals("Location Not Available")) && (arg1.equals("Location Not Available")))
			{
				return (0);
			}
			if (arg0.equals("Location Not Available"))
			{
				return (1);
			}
			if (arg1.equals("Location Not Available"))
			{
				return (-1);
			}
			return (arg0.compareTo(arg1));
		}
	});
	private Map<String, PeopleResult>	practicesMap			= new TreeMap<String, PeopleResult>(new Comparator<String>()
	{
		@Override
		public int compare(String arg0, String arg1)
		{
			if ((arg0.equals("Practice Area Not Available")) && (arg1.equals("Practice Area Not Available")))
			{
				return (0);
			}
			if (arg0.equals("Practice Area Not Available"))
			{
				return (1);
			}
			if (arg1.equals("Practice Area Not Available"))
			{
				return (-1);
			}
			return (arg0.compareTo(arg1));
		}
	});
	private PeopleResult				companyPeople;
	private Integer						officesCount;
	private Integer						practicesCount;

	public static final String			FIRM					= "FIRM";
	public static final String			LOCATION				= "LOCATION";
	public static final String			PRACTICE				= "PRACTICE";

	public static final String			HEAD_COUNT				= "HEAD_COUNT";
	public static final String			MOVEMENT_COUNT			= "MOVEMENT_COUNT";

	public Integer getOfficesCount()
	{
		Integer returnCount = 0;
		for (String thisLocation : officeLocationsMap.keySet())
		{
			if ((!thisLocation.equals("Location Not Available")) 
				&& ((officeLocationsMap.get(thisLocation).getHeadCount() != null)
						&& (officeLocationsMap.get(thisLocation).getHeadCount() > 0)))
			{
				returnCount++;
			}
		}
		return (returnCount);
	}
	
	public Integer getPracticesCount()
	{
		Integer returnCount = 0;
		for (String thisPractice : practicesMap.keySet())
		{
			if ((!thisPractice.equals("Practice Area Not Available")) 
				&& ((practicesMap.get(thisPractice).getHeadCount() != null)
						&& (practicesMap.get(thisPractice).getHeadCount() > 0)))
			{
				returnCount++;
			}
		}
		return (returnCount);
	}
	
	public void addPracticeOrLocationToHeadCount(PeopleCacheResult pcResult)
	{
		// Has this attorney already been counted?
		// If so, we will not count him/her in the firm-wise counts
		// If the attorney hasn't been counted then we need to increment
		// the companyPeople counters
		PeopleResult newPR = null;
		if (pcResult.getGroupingType().equals(LOCATION))
		{
			if (officeLocationsMap.containsKey(pcResult.getLocationOrPractice()))
			{
				newPR = officeLocationsMap.get(pcResult.getLocationOrPractice());
			}
			else
			{
				newPR = new PeopleResult();
				newPR.setGroupType(pcResult.getGroupingType());
				newPR.setSearchTitle(pcResult.getLocationOrPractice());
			}
			newPR.addHeadCounts(pcResult);
			officeLocationsMap.put(pcResult.getLocationOrPractice(), newPR);
		}
		else if (pcResult.getGroupingType().equals(PRACTICE))
		{
			if (practicesMap.containsKey(pcResult.getLocationOrPractice()))
			{
				newPR = practicesMap.get(pcResult.getLocationOrPractice());
			}
			else
			{
				newPR = new PeopleResult();
				newPR.setGroupType(pcResult.getGroupingType());
				newPR.setSearchTitle(pcResult.getLocationOrPractice());
			}
			// Add to the head counts and not to the movement counts
			newPR.addHeadCounts(pcResult);
			practicesMap.put(pcResult.getLocationOrPractice(), newPR);
		}
//		peopleResults.add(newPR);

		if (companyPeople == null)
		{
			try
			{
				companyPeople = (PeopleResult) newPR.clone();
			}
			catch (CloneNotSupportedException cnsEx)
			{
				companyPeople = newPR;
			}
			companyPeople.setGroupType(FIRM);
			companyPeople.setSearchTitle(companyName);
		}

		if (!headCountAttorneys.contains(pcResult.getAttorneyId()))
		{
			// Increment the firm-level head count
			companyPeople.addHeadCounts(pcResult);
			headCountAttorneys.add(pcResult.getAttorneyId());
		}
	}

	public void addPracticeOrLocationToMovementCount(PeopleCacheResult pcResult)
	{
		// Has this attorney already been counted?
		// If so, we will not count him/her in the firm-wise counts
		// If the attorney hasn't been counted then we need to increment
		// the companyPeople counters
		PeopleResult newPR = null;
		if (pcResult.getGroupingType().equals(LOCATION))
		{
			if (officeLocationsMap.containsKey(pcResult.getLocationOrPractice()))
			{
				newPR = officeLocationsMap.get(pcResult.getLocationOrPractice());
			}
			else
			{
				newPR = new PeopleResult();
				newPR.setGroupType(pcResult.getGroupingType());
				newPR.setSearchTitle(pcResult.getLocationOrPractice());
			}
			// Add to the movement counts and not to the head counts
			newPR.addMovementCounts(pcResult);
			officeLocationsMap.put(pcResult.getLocationOrPractice(), newPR);
		}
		else if (pcResult.getGroupingType().equals(PRACTICE))
		{
			if (practicesMap.containsKey(pcResult.getLocationOrPractice()))
			{
				newPR = practicesMap.get(pcResult.getLocationOrPractice());
			}
			else
			{
				newPR = new PeopleResult();
				newPR.setGroupType(pcResult.getGroupingType());
				newPR.setSearchTitle(pcResult.getLocationOrPractice());
			}
			newPR.addHeadCounts(pcResult);
			practicesMap.put(pcResult.getLocationOrPractice(), newPR);
		}
//		peopleResults.add(newPR);

		if (companyPeople == null)
		{
			try
			{
				companyPeople = (PeopleResult) newPR.clone();
			}
			catch (CloneNotSupportedException cnsEx)
			{
				companyPeople = newPR;
			}
			companyPeople.setGroupType(FIRM);
			companyPeople.setSearchTitle(companyName);
		}

		if (!movementCountAttorneys.contains(pcResult.getAttorneyId()))
		{
			// Increment the firm-level head count
			companyPeople.addMovementCounts(pcResult);
			movementCountAttorneys.add(pcResult.getAttorneyId());
		}
	}

	public Integer getGroupingId()
	{
		return groupingId;
	}

	public void setGroupingId(Integer groupingId)
	{
		this.groupingId = groupingId;
	}

	public Integer getCompanyId()
	{
		return companyId;
	}

	public void setCompanyId(Integer companyId)
	{
		this.companyId = companyId;
	}

	public String getCompanyName()
	{
		return companyName;
	}

	public void setCompanyName(String companyName)
	{
		this.companyName = companyName;
	}

	public String getCompanyWebsite()
	{
		return companyWebsite;
	}

	public void setCompanyWebsite(String companyWebsite)
	{
		this.companyWebsite = companyWebsite;
	}

	public List<PeopleResult> getPeopleResults()
	{
		return peopleResults;
	}

	public void setPeopleResults(List<PeopleResult> peopleResults)
	{
		this.peopleResults = peopleResults;
	}

	public Map<String, PeopleResult> getOfficeLocationsMap()
	{
		return officeLocationsMap;
	}

	public void setOfficeLocationsMap(Map<String, PeopleResult> officeLocationsMap)
	{
		this.officeLocationsMap = officeLocationsMap;
	}

	public Map<String, PeopleResult> getPracticesMap()
	{
		return practicesMap;
	}

	public void setPracticesMap(Map<String, PeopleResult> practicesMap)
	{
		this.practicesMap = practicesMap;
	}

	public PeopleResult getCompanyPeople()
	{
		return companyPeople;
	}

	public void setCompanyPeople(PeopleResult companyPeople)
	{
		if (peopleResults.isEmpty())
		{
			peopleResults.add(companyPeople);
		}
		this.companyPeople = companyPeople;
	}

	public Integer getPartnerCount()
	{
		if ((this.peopleResults != null) && (!this.peopleResults.isEmpty()))
		{
			for (PeopleResult tPR : this.peopleResults)
			{
				if (tPR.getGroupType().equals(FIRM))
				{
					return tPR.getPartnerCount();
				}
			}
		}
		return (0);
	}

	public Integer getAssociateCount()
	{
		if ((this.peopleResults != null) && (!this.peopleResults.isEmpty()))
		{
			for (PeopleResult tPR : this.peopleResults)
			{
				if (tPR.getGroupType().equals(FIRM))
				{
					return tPR.getAssociateCount();
				}
			}
		}
		return (0);
	}

	public Integer getOtherCounselCount()
	{
		if ((this.peopleResults != null) && (!this.peopleResults.isEmpty()))
		{
			for (PeopleResult tPR : this.peopleResults)
			{
				if (tPR.getGroupType().equals(FIRM))
				{
					return tPR.getOtherCounselCount();
				}
			}
		}
		return (0);
	}

	public Integer getTotalAttorneyCount()
	{
		if ((this.peopleResults != null) && (!this.peopleResults.isEmpty()))
		{
			for (PeopleResult tPR : this.peopleResults)
			{
				if (tPR.getGroupType().equals(FIRM))
				{
					return (tPR.getHeadCount());
				}
			}
		}
		return (0);
	}

	public Integer getPracticeCount()
	{
		if ((practicesMap == null) || (practicesMap.isEmpty()))
		{
			return (0);
		}
		if (practicesMap.containsKey("Practice Area Not Available"))
		{
			return (practicesMap.size() - 1);
		}
		return (practicesMap.size());
	}

	public Integer getLocationCount()
	{
		if ((officeLocationsMap == null) || (officeLocationsMap.isEmpty()))
		{
			return (0);
		}
		if (officeLocationsMap.containsKey("Location Not Available"))
		{
			return (officeLocationsMap.size() - 1);
		}
		return (officeLocationsMap.size());
	}

	public void addPractice(PeopleResult anotherPR)
	{
		// Check if there is already another "Practice Area Not Available"
		if ((anotherPR.getSearchTitle() == null) 
				|| (anotherPR.getSearchTitle().trim().length() == 0) 
				|| (anotherPR.getSearchTitle().equals("Practice Area Not Available")))
		{
			anotherPR.setSearchTitle("Practice Area Not Available");
		}

		// Check for duplicates
		PeopleResult thisPR = new PeopleResult();
		thisPR.setGroupType("PRACTICE");
		if (practicesMap == null)
		{
			practicesMap = new TreeMap<String, PeopleResult>();
		}
		if (practicesMap.containsKey(anotherPR.getSearchTitle()))
		{
			thisPR = practicesMap.get(anotherPR.getSearchTitle());
		}

		thisPR.setSearchTitle(anotherPR.getSearchTitle());

		// Head counts
		thisPR.setHeadCount(thisPR.getHeadCount() + anotherPR.getHeadCount());

		// Partners
		thisPR.setPartnerCount(thisPR.getPartnerCount() + anotherPR.getPartnerCount());

		// Associates
		thisPR.setAssociateCount(thisPR.getAssociateCount() + anotherPR.getAssociateCount());

		// Other counsel
		thisPR.setOtherCounselCount(thisPR.getAssociateCount() + anotherPR.getOtherCounselCount());

		// Admin
		thisPR.setAdminCount(thisPR.getAdminCount() + anotherPR.getAdminCount());

		// Other
		thisPR.setOtherCount(thisPR.getOtherCount() + anotherPR.getOtherCount());

		if (peopleResults == null)
		{
			peopleResults = new ArrayList<PeopleResult>();
		}
		peopleResults.add(thisPR);
		practicesMap.put(anotherPR.getSearchTitle(), thisPR);
	}

	public void addLocation(PeopleResult anotherPR)
	{
		// Check if there is already another "Location Not Available"
		if ((anotherPR.getSearchTitle() == null) 
				|| (anotherPR.getSearchTitle().trim().length() == 0) 
				|| (anotherPR.getSearchTitle().equalsIgnoreCase("Location not identified"))
				|| (anotherPR.getSearchTitle().equalsIgnoreCase("N/A")) 
				|| (anotherPR.getSearchTitle().equalsIgnoreCase("Unknown Location")))
		{
			anotherPR.setSearchTitle("Location Not Available");
		}

		// Check for duplicates
		PeopleResult thisPR = new PeopleResult();
		thisPR.setGroupType("LOCATION");
		if (officeLocationsMap == null)
		{
			officeLocationsMap = new TreeMap<String, PeopleResult>();
		}
		if (officeLocationsMap.containsKey(anotherPR.getSearchTitle()))
		{
			thisPR = officeLocationsMap.get(anotherPR.getSearchTitle());
		}

		thisPR.setSearchTitle(anotherPR.getSearchTitle());

		// Head counts
		thisPR.setHeadCount(thisPR.getHeadCount() + anotherPR.getHeadCount());
//		thisPR.setHeadCountPlus(thisPR.getHeadCountPlus() + anotherPR.getHeadCountPlus());
//		thisPR.setHeadCountMinus(thisPR.getHeadCountMinus() + anotherPR.getHeadCountMinus());

		// Partners
		thisPR.setPartnerCount(thisPR.getPartnerCount() + anotherPR.getPartnerCount());
//		thisPR.setPartnerCountPlus(thisPR.getPartnerCountPlus() + anotherPR.getPartnerCountPlus());
//		thisPR.setPartnerCountMinus(thisPR.getPartnerCountMinus() + anotherPR.getPartnerCountMinus());

		// Associates
		thisPR.setAssociateCount(thisPR.getAssociateCount() + anotherPR.getAssociateCount());
//		thisPR.setAssociateCountPlus(thisPR.getAssociateCountPlus() + anotherPR.getAssociateCountPlus());
//		thisPR.setAssociateCountMinus(thisPR.getAssociateCountMinus() + anotherPR.getAssociateCountMinus());

		// Other counsel
		thisPR.setOtherCounselCount(thisPR.getAssociateCount() + anotherPR.getOtherCounselCount());
//		thisPR.setOtherCounselCountPlus(thisPR.getOtherCounselCountPlus() + anotherPR.getOtherCounselCountPlus());
//		thisPR.setOtherCounselCountMinus(thisPR.getOtherCounselCountMinus() + anotherPR.getOtherCounselCountMinus());

		// Admin
		thisPR.setAdminCount(thisPR.getAdminCount() + anotherPR.getAdminCount());
//		thisPR.setAdminCountPlus(thisPR.getAdminCountPlus() + anotherPR.getAdminCountPlus());
//		thisPR.setAdminCountMinus(thisPR.getAdminCountMinus() + anotherPR.getAdminCountMinus());

		// Other
		thisPR.setOtherCount(thisPR.getOtherCount() + anotherPR.getOtherCount());
//		thisPR.setOtherCountPlus(thisPR.getOtherCountPlus() + anotherPR.getOtherCountPlus());
//		thisPR.setOtherCountMinus(thisPR.getOtherCountMinus() + anotherPR.getOtherCountMinus());

		if (peopleResults == null)
		{
			peopleResults = new ArrayList<PeopleResult>();
		}
		peopleResults.add(thisPR);
		officeLocationsMap.put(anotherPR.getSearchTitle(), thisPR);
	}

	public void addPracticeMovements(PeopleResult anotherPR)
	{
		// Check for duplicates
		PeopleResult thisPR = new PeopleResult();
		thisPR.setGroupType("PRACTICE");
		if (practicesMap == null)
		{
			practicesMap = new TreeMap<String, PeopleResult>();
		}
		if (practicesMap.containsKey(anotherPR.getSearchTitle()))
		{
			thisPR = practicesMap.get(anotherPR.getSearchTitle());
		}

		thisPR.setSearchTitle(anotherPR.getSearchTitle());

		// Head counts
		thisPR.setHeadCountPlus(thisPR.getHeadCountPlus() + anotherPR.getHeadCountPlus());
		thisPR.setHeadCountMinus(thisPR.getHeadCountMinus() + anotherPR.getHeadCountMinus());

		// Partners
		thisPR.setPartnerCountPlus(thisPR.getPartnerCountPlus() + anotherPR.getPartnerCountPlus());
		thisPR.setPartnerCountMinus(thisPR.getPartnerCountMinus() + anotherPR.getPartnerCountMinus());

		// Associates
		thisPR.setAssociateCountPlus(thisPR.getAssociateCountPlus() + anotherPR.getAssociateCountPlus());
		thisPR.setAssociateCountMinus(thisPR.getAssociateCountMinus() + anotherPR.getAssociateCountMinus());

		// Other counsel
		thisPR.setOtherCounselCountPlus(thisPR.getOtherCounselCountPlus() + anotherPR.getOtherCounselCountPlus());
		thisPR.setOtherCounselCountMinus(thisPR.getOtherCounselCountMinus() + anotherPR.getOtherCounselCountMinus());

		// Admin
		thisPR.setAdminCountPlus(thisPR.getAdminCountPlus() + anotherPR.getAdminCountPlus());
		thisPR.setAdminCountMinus(thisPR.getAdminCountMinus() + anotherPR.getAdminCountMinus());

		// Other
		thisPR.setOtherCountPlus(thisPR.getOtherCountPlus() + anotherPR.getOtherCountPlus());
		thisPR.setOtherCountMinus(thisPR.getOtherCountMinus() + anotherPR.getOtherCountMinus());

		practicesMap.put(anotherPR.getSearchTitle(), thisPR);
	}

	public void addLocationMovements(PeopleResult anotherPR)
	{
		// Check for duplicates
		PeopleResult thisPR = new PeopleResult();
		thisPR.setGroupType("LOCATION");
		if (officeLocationsMap == null)
		{
			officeLocationsMap = new TreeMap<String, PeopleResult>();
		}
		if (officeLocationsMap.containsKey(anotherPR.getSearchTitle()))
		{
			thisPR = officeLocationsMap.get(anotherPR.getSearchTitle());
		}

		thisPR.setSearchTitle(anotherPR.getSearchTitle());

		// Head counts
		thisPR.setHeadCountPlus(thisPR.getHeadCountPlus() + anotherPR.getHeadCountPlus());
		thisPR.setHeadCountMinus(thisPR.getHeadCountMinus() + anotherPR.getHeadCountMinus());

		// Partners
		thisPR.setPartnerCountPlus(thisPR.getPartnerCountPlus() + anotherPR.getPartnerCountPlus());
		thisPR.setPartnerCountMinus(thisPR.getPartnerCountMinus() + anotherPR.getPartnerCountMinus());

		// Associates
		thisPR.setAssociateCountPlus(thisPR.getAssociateCountPlus() + anotherPR.getAssociateCountPlus());
		thisPR.setAssociateCountMinus(thisPR.getAssociateCountMinus() + anotherPR.getAssociateCountMinus());

		// Other counsel
		thisPR.setOtherCounselCountPlus(thisPR.getOtherCounselCountPlus() + anotherPR.getOtherCounselCountPlus());
		thisPR.setOtherCounselCountMinus(thisPR.getOtherCounselCountMinus() + anotherPR.getOtherCounselCountMinus());

		// Admin
		thisPR.setAdminCountPlus(thisPR.getAdminCountPlus() + anotherPR.getAdminCountPlus());
		thisPR.setAdminCountMinus(thisPR.getAdminCountMinus() + anotherPR.getAdminCountMinus());

		// Other
		thisPR.setOtherCountPlus(thisPR.getOtherCountPlus() + anotherPR.getOtherCountPlus());
		thisPR.setOtherCountMinus(thisPR.getOtherCountMinus() + anotherPR.getOtherCountMinus());

		officeLocationsMap.put(anotherPR.getSearchTitle(), thisPR);
	}
	
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append("PeopleResultDTO [groupingId=");
		builder.append(groupingId);
		builder.append(", companyId=");
		builder.append(companyId);
		builder.append(", companyName=");
		builder.append(companyName);
		builder.append(", companyWebsite=");
		builder.append(companyWebsite);
		// builder.append(", peopleResults=");
		// builder.append((peopleResults != null) ? peopleResults.size() :
		// "null");
		builder.append("]");
		return builder.toString();
	}

	@Override
	public int compareTo(PeopleResultDTO that)
	{
		return (this.getCompanyName().compareTo(that.getCompanyName()));
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj != null)
		{
			PeopleResultDTO that = (PeopleResultDTO) obj;
			return (this.getCompanyName().equals(that.getCompanyName()));
		}
		return (false);
	}
}

class FirmStatisticKey implements Comparable<FirmStatisticKey>
{
	private String key;

	public String getKey()
	{
		return key;
	}

	public void setKey(String key)
	{
		this.key = key;
	}

	@Override
	public int compareTo(FirmStatisticKey that)
	{
		return 0;
	}
}