package com.alm.rivaledge.transferobject;

import java.io.Serializable;
import java.util.List;


/**
 * This class will hold Location Tree information 
 * @author FL605
 * 
 *
 */
public class LocationList implements Serializable {
	
	
	
	
	private static final long serialVersionUID = -6310281303928610804L;
	
	/**
	 * This flag would decide whether we need to check current location node in tree or not.
	 */
	private boolean select;
	
	/**
	 * This attribute is use to uniquely identify a node in tree 
	 */
	private String key;
	
	/**
	 * label of Location node
	 */
	private String title;
	
	/**
	 * Will contain the information about all children for a particular location
	 */
	private List<LocationList> children;
	
	public boolean getSelect() 
	{
		return select;
	}

	public void setSelect(boolean select) 
	{
		this.select = select;
	}

	public String getKey() 
	{
		return key;
	}
	
	public void setKey(String key) 
	{
		this.key = key;
	}
	
	public String getTitle() 
	{
		return title;
	}
	public void setTitle(String title) 
	{
		this.title = title;
	}
	public List<LocationList> getChildren()
	{
		return children;
	}
	public void setChildren(List<LocationList> children) 
	{
		this.children = children;
	}
	
	
	
}