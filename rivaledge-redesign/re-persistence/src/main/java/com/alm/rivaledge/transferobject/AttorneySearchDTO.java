package com.alm.rivaledge.transferobject;

import java.util.List;

import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;

/**
 * This class will have all the search criteria which user has entered on the
 * Attorney search form
 * 
 * @author FL605
 * @since Sprint 3
 */
public class AttorneySearchDTO extends BaseSearchDTO 
{
	private static final long	serialVersionUID	= -3376557653172171633L;

	private List<Firm>			selectedFirms;
	private List<String>		practiceArea;
	private String				practiceAreaString;
	private List<String>		locations;
	private String				locationsString;
	private List<String>		titles;
	private String				graduationStartYear;
	private String				graduationEndYear;
	//private String				attorneyName;
	private String				lawSchooltext;
	private String				keywordsInBioText;
	private String				undergraduateSchoolText;
	private String				admissions;
	private List<String>		attorneyName;
	private List<Integer>		attorneyIdList;

	public AttorneySearchDTO()
	{
		super();
		// Override the default sort column
		setSortColumn(2);
		setSortOrder("asc");
	}

	public List<Firm> getSelectedFirms()
	{
		return selectedFirms;
	}

	public void setSelectedFirms(List<Firm> selectedFirms)
	{
		this.selectedFirms = selectedFirms;
	}

	public List<String> getPracticeArea()
	{
		return practiceArea;
	}

	public void setPracticeArea(List<String> practiceArea)
	{
		this.practiceArea = practiceArea;
	}

	public List<String> getLocations()
	{
		return locations;
	}

	public void setLocations(List<String> locations)
	{
		this.locations = locations;
	}

	public List<String> getTitles()
	{
		return titles;
	}

	public void setTitles(List<String> titles)
	{
		this.titles = titles;
	}

	public String getGraduationStartYear()
	{
		return graduationStartYear;
	}

	public void setGraduationStartYear(String graduationStartYear)
	{
		this.graduationStartYear = graduationStartYear;
	}

	public String getGraduationEndYear()
	{
		return graduationEndYear;
	}

	public void setGraduationEndYear(String graduationEndYear)
	{
		this.graduationEndYear = graduationEndYear;
	}

	/*public String getAttorneyName()
	{
		return attorneyName;
	}

	public void setAttorneyName(String attorneyName)
	{
		this.attorneyName = attorneyName;
	}*/

	public String getLawSchooltext()
	{
		return lawSchooltext;
	}

	public void setLawSchooltext(String lawSchooltext)
	{
		this.lawSchooltext = lawSchooltext;
	}

	public String getKeywordsInBioText()
	{
		return keywordsInBioText;
	}

	public void setKeywordsInBioText(String keywordsInBioText)
	{
		this.keywordsInBioText = keywordsInBioText;
	}

	public String getUndergraduateSchoolText()
	{
		return undergraduateSchoolText;
	}

	public void setUndergraduateSchoolText(String undergraduateSchoolText)
	{
		this.undergraduateSchoolText = undergraduateSchoolText;
	}

	public String getAdmissions()
	{
		return admissions;
	}

	public void setAdmissions(String admissions)
	{
		this.admissions = admissions;
	}

	public String getPracticeAreaString()
	{
		return practiceAreaString;
	}

	public void setPracticeAreaString(String practiceAreaString)
	{
		this.practiceAreaString = practiceAreaString;
	}

	public String getLocationsString()
	{
		return locationsString;
	}

	public void setLocationsString(String locationsString)
	{
		this.locationsString = locationsString;
	}

	public List<String> getAttorneyName() 
	{
		return attorneyName;
	}

	public void setAttorneyName(List<String> attorneyName) 
	{
		this.attorneyName = attorneyName;
	}

	public List<Integer> getAttorneyIdList()
	{
		return attorneyIdList;
	}

	public void setAttorneyIdList(List<Integer> attorneyIdList) 
	{
		this.attorneyIdList = attorneyIdList;
	}

	@Override
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}
	@Override
	public String toString()
	{
		return "AttorneySearchDTO [selectedFirms=" + selectedFirms + ", practiceArea=" + practiceArea + ", locations=" + locations + ", titles=" + titles
				+ ", graduationStartYear=" + graduationStartYear + ", graduationEndYear=" + graduationEndYear + ", attorneyName=" + attorneyName + ", lawSchooltext="
				+ lawSchooltext + ", keywordsInBioText=" + keywordsInBioText + ", undergraduateSchoolText=" + undergraduateSchoolText + ", admissions=" + admissions + "]";
	}

}
