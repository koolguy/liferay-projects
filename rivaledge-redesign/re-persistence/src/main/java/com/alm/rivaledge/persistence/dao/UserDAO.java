package com.alm.rivaledge.persistence.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alm.rivaledge.persistence.dao.lawma0_data.UserGroupCompanyMapper;
import com.alm.rivaledge.persistence.dao.lawma0_data.UserGroupMapper;
import com.alm.rivaledge.persistence.dao.lawma0_data.UserMapper;
import com.alm.rivaledge.persistence.dao.lawma0_data.UserPreferencesMapper;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserExample;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroupCompany;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroupCompanyExample;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroupExample;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserPreferences;
import com.alm.rivaledge.transferobject.WatchListFirmResultDTO;

/**
 * Data Access Object that handles all persistence for the Person domain object.
 * 
 * @author FL867
 * @since Sprint 3
 */
@Repository
public class UserDAO
{
	@Autowired
	private UserMapper		userMapper;

	@Autowired
	private UserGroupMapper	userGroupMapper;
	
	@Autowired
	private UserGroupCompanyMapper userGroupCompanyMapper;
	
	@Autowired
	private UserPreferencesMapper userPreferencesMapper;

	/**
	 * Finds a specific user by the primary key.
	 * 
	 * @param userId
	 * @return
	 */
	public User findByPrimaryKey(Integer userId)
	{
		return (userMapper.selectByPrimaryKey(userId));
	}

	// Gets all the watch list firms for a given user
	public List<WatchListFirmResultDTO> findWatchListFirmsForUser(Integer id)
	{
		return userMapper.findWatchListFirmsForUser(id);
	}

	/**
	 * Get all the watch lists owned by the specified user
	 * 
	 * @param userId
	 * @return
	 */
	public List<UserGroup> findUserWatchLists(Integer userId)
	{
		// Set up the search criteria
		UserGroupExample ugExample = new UserGroupExample();
		ugExample.or().andIdEqualTo(userId);

		// Execute and return
		return (userGroupMapper.selectByExample(ugExample));
	}

	public List<UserGroup> findAllWatchlists()
	{
		UserGroupExample ugExample = new UserGroupExample();
		ugExample.setDistinct(true);

		return (userGroupMapper.selectByExample(ugExample));
	}

	public List<UserGroupCompany> findWatchListFirms(Integer watchlistId)
	{
		// Set up the search criteria
		UserGroupCompanyExample ugcExample = new UserGroupCompanyExample();
		ugcExample.or().andGroupIdEqualTo(watchlistId);

		// Execute and return
		return (userGroupCompanyMapper.selectByExample(ugcExample));
	}

	public List<User> findByEmail(String currentUserEmailAddress)
	{
		// Set up the search criteria
		UserExample uEx = new UserExample();
		uEx.or().andEmailEqualTo(currentUserEmailAddress);
		
		// Execute and return
		return userMapper.selectByExample(uEx);
	}
	
	public UserPreferences findUserPreferenceByUserAndPortlet(Integer userId, String portletId, String currentPage)
	{
		UserPreferences userPreferences = new UserPreferences();
		userPreferences.setUserId(userId);
		userPreferences.setPortletId(portletId);
		userPreferences.setApplicablePage(currentPage);
		return (userPreferencesMapper.findPreferenceByExample(userPreferences));
	}
	
	public void persistUserPreferences(UserPreferences userPreferences)
	{
		if (userPreferences.isValid())
		{
			UserPreferences existingUserPreference = userPreferencesMapper.findPreferenceByExample(userPreferences);
			if (existingUserPreference == null)
			{
				userPreferencesMapper.createUserPortletPreference(userPreferences);
			}
			else
			{
				if (userPreferences.getSearchCriteriaJSON() != null)
				{
					existingUserPreference.setSearchCriteriaJSON(userPreferences.getSearchCriteriaJSON());
				}
				if (userPreferences.getChartCriteriaJSON() != null)
				{
					existingUserPreference.setChartCriteriaJSON(userPreferences.getChartCriteriaJSON());
				}
				userPreferencesMapper.updateUserPortletPreference(existingUserPreference);
			}
		}
	}
}
