package com.alm.rivaledge.transferobject;

import java.io.Serializable;
import java.util.Date;

/**
 * This is the DTO that will be returned when a search for Attorney Move Changes
 * is run.
 * 
 * @author FL605
 * @since Sprint 4
 */
public class AttorneyMoveChangesResultDTO implements Serializable
{
	private static final long	serialVersionUID	= -8616136619515647228L;

	private Integer				movesChangesId;
	private Integer				attorneyId;			// Column 1
	private String				attorneyName;		// Column 5
	private String				attorneyLink;		// Column 6
	private Integer				firmId;				// Column 2
//	private AttorneyAction		attorneyAction;		// Column 4
	private String				firmName;			// Column 7
	private String				firmLink;			// Column 8
	private String				title;				// Column 9
	private String				location;			// Column 10
	private String				practices;			// Column 11
	private String				lastAction;
	private Date				lastActionDate;
	private String				movedFrom;			// Column 12
	private String				movedTo;			// Column 13
	private String				movedFromFirmLink;			
	private String				movedToFirmLink;			
	
	public Integer getMovesChangesId()
	{
		return movesChangesId;
	}

	public void setMovesChangesId(Integer movesChangesId)
	{
		this.movesChangesId = movesChangesId;
	}

	public Integer getAttorneyId()
	{
		return attorneyId;
	}

	public void setAttorneyId(Integer attorneyId)
	{
		this.attorneyId = attorneyId;
	}

	public Integer getFirmId()
	{
		return firmId;
	}

	public void setFirmId(Integer firmId)
	{
		this.firmId = firmId;
	}

//	public AttorneyAction getAttorneyAction()
//	{
//		return attorneyAction;
//	}
//
//	public void setAttorneyAction(AttorneyAction action)
//	{
//		this.attorneyAction = action;
//	}

	public String getAttorneyName()
	{
		return attorneyName;
	}

	public void setAttorneyName(String attorneyName)
	{
		this.attorneyName = attorneyName;
	}

	public String getAttorneyLink()
	{
		return attorneyLink;
	}

	public void setAttorneyLink(String attorneyLink)
	{
		this.attorneyLink = attorneyLink;
	}

	public String getFirmName()
	{
		return firmName;
	}

	public void setFirmName(String firmName)
	{
		this.firmName = firmName;
	}

	public String getFirmLink()
	{
		return firmLink;
	}

	public void setFirmLink(String firmLink)
	{
		this.firmLink = firmLink;
	}

	public String getTitle()
	{
		return title;
	}

	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getLocation()
	{
		return location;
	}

	public void setLocation(String location)
	{
		this.location = location;
	}

	public String getPractices()
	{
		return practices;
	}

	public void setPractices(String practices)
	{
		this.practices = practices;
	}

	public String getMovedFrom()
	{
		return movedFrom;
	}

	public String getLastAction()
	{
		return lastAction;
	}

	public void setLastAction(String lastAction)
	{
		this.lastAction = lastAction;
	}

	public Date getLastActionDate()
	{
		return lastActionDate;
	}

	public void setLastActionDate(Date lastActionDate)
	{
		this.lastActionDate = lastActionDate;
	}

	public void setMovedFrom(String movedFrom)
	{
		this.movedFrom = movedFrom;
	}

	public String getMovedTo()
	{
		return movedTo;
	}

	public void setMovedTo(String movedTo)
	{
		this.movedTo = movedTo;
	}

	public String getMovedFromFirmLink() 
	{
		return movedFromFirmLink;
	}

	public void setMovedFromFirmLink(String movedFromFirmLink) 
	{
		this.movedFromFirmLink = movedFromFirmLink;
	}

	public String getMovedToFirmLink() 
	{
		return movedToFirmLink;
	}

	public void setMovedToFirmLink(String movedToFirmLink) 
	{
		this.movedToFirmLink = movedToFirmLink;
	}

	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
//		result = prime * result + ((attorneyAction == null) ? 0 : attorneyAction.hashCode());
		result = prime * result + ((attorneyId == null) ? 0 : attorneyId.hashCode());
		result = prime * result + ((attorneyName == null) ? 0 : attorneyName.hashCode());
		result = prime * result + ((firmId == null) ? 0 : firmId.hashCode());
		result = prime * result + ((firmName == null) ? 0 : firmName.hashCode());
		result = prime * result + ((movedFrom == null) ? 0 : movedFrom.hashCode());
		result = prime * result + ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((practices == null) ? 0 : practices.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + ((movedTo == null) ? 0 : movedTo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AttorneyMoveChangesResultDTO other = (AttorneyMoveChangesResultDTO) obj;
//		if (attorneyAction == null)
//		{
//			if (other.attorneyAction != null)
//				return false;
//		}
//		else if (!attorneyAction.equals(other.attorneyAction))
//			return false;
		if (attorneyId == null)
		{
			if (other.attorneyId != null)
				return false;
		}
		else if (!attorneyId.equals(other.attorneyId))
			return false;
		if (attorneyName == null)
		{
			if (other.attorneyName != null)
				return false;
		}
		else if (!attorneyName.equals(other.attorneyName))
			return false;
		if (firmId == null)
		{
			if (other.firmId != null)
				return false;
		}
		else if (!firmId.equals(other.firmId))
			return false;
		if (firmName == null)
		{
			if (other.firmName != null)
				return false;
		}
		else if (!firmName.equals(other.firmName))
			return false;
		if (movedFrom == null)
		{
			if (other.movedFrom != null)
				return false;
		}
		else if (!movedFrom.equals(other.movedFrom))
			return false;
		if (location == null)
		{
			if (other.location != null)
				return false;
		}
		else if (!location.equals(other.location))
			return false;
		if (practices == null)
		{
			if (other.practices != null)
				return false;
		}
		else if (!practices.equals(other.practices))
			return false;
		if (title == null)
		{
			if (other.title != null)
				return false;
		}
		else if (!title.equals(other.title))
			return false;
		if (movedTo == null)
		{
			if (other.movedTo != null)
				return false;
		}
		else if (!movedTo.equals(other.movedTo))
			return false;
		return true;
	}

	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder();
		builder.append(attorneyId);
		builder.append("||");
		builder.append(((attorneyName != null) && (attorneyName.trim().length() > 0)) ? attorneyName : "\\N");
		builder.append("||");
		builder.append(((attorneyLink != null) && (attorneyLink.trim().length() > 0)) ? attorneyLink : "\\N");
		builder.append("||");
		builder.append(firmId);
		builder.append("||");
		builder.append(firmName);
		builder.append("||");
		builder.append(((firmLink != null) && (firmLink.trim().length() > 0)) ? firmLink : "\\N");
		builder.append("||");
		builder.append(((title != null) && (title.trim().length() > 0)) ? title : "\\N");
		builder.append("||");
		builder.append(((location != null) && (location.trim().length() > 0)) ? location : "\\N");
		builder.append("||");
		builder.append(((practices != null) && (practices.trim().length() > 0)) ? practices : "\\N");
		builder.append("||");
		builder.append(((movedFrom != null) && (movedFrom.trim().length() > 0)) ? movedFrom : "\\N");
		builder.append("||");
		builder.append(((movedTo != null) && (movedTo.trim().length() > 0)) ? movedTo : "\\N");
		builder.append("||");
//		builder.append(((attorneyAction != null) && (attorneyAction.getActionDate() != null)) ? ALMConstants.SEARCH_DATE_FORMAT.format(attorneyAction.getActionDate()) : "\\N");
//		builder.append("||");
//		builder.append(((attorneyAction != null) && (attorneyAction.getMoveAction() != null) &&  (attorneyAction.getMoveAction().trim().length() > 0)) ? attorneyAction.getMoveAction() : "\\N");
		builder.append("$$");
		return builder.toString();
	}
}