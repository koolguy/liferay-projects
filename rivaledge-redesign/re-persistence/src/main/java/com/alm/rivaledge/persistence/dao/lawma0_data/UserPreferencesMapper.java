package com.alm.rivaledge.persistence.dao.lawma0_data;

import java.util.List;

import com.alm.rivaledge.persistence.domain.lawma0_data.UserPreferences;

/**
 * MyBatis sql mapper that maps to the UserPreferences.xml sql map.
 * 
 * @author FL867
 * @version 1.0
 * @since Sprint 9.2
 */
public interface UserPreferencesMapper
{
	/**
	 * Given a user id and portlet id, returns the corresponding user
	 * preference.
	 * 
	 * @param samplePreferences
	 * @return
	 */
	UserPreferences findPreferenceByExample(UserPreferences samplePreferences);

	/**
	 * Given a user id, returns user preferences for all the portlets that this
	 * user has preferences saved for.
	 * 
	 * @param userId
	 * @return
	 */
	List<UserPreferences> findAllPreferencesByUser(Integer userId);

	/**
	 * Persists a new user preference to the database.
	 * 
	 * @param newUserPreference
	 */
	void createUserPortletPreference(UserPreferences newUserPreference);

	/**
	 * Updates changes to the user preferences (search criteria)
	 * 
	 * @param updatedUserPreference
	 */
	void updateUserPortletPreference(UserPreferences updatedUserPreference);
}
