package com.alm.rivaledge.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alm.rivaledge.persistence.dao.CacheDAO;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.service.EventService;
import com.alm.rivaledge.transferobject.EventResultDTO;
import com.alm.rivaledge.transferobject.EventSearchDTO;
import com.alm.rivaledge.util.ALMConstants;

@Service("eventService")
public class EventServiceImpl implements EventService
{
	@Autowired
	private CacheDAO cacheDAO;
	
	@Override
	public List<EventResultDTO> getEventData(EventSearchDTO searchDTO, User currentUser)
	{
		int resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_SUBSCRIBER; // Default
		if ((currentUser != null) && (currentUser.getUsertype().equals(ALMConstants.RIVALEDGE_ADMIN)))
		{
			// This is an admin
			resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_RE_ADMIN;
		}
		
		searchDTO.setMaxResultSizeCap(resultSetSizeCap);
		
		return (cacheDAO.findEventData(searchDTO));
	}

	@Override
	public Integer getEventDataCount(EventSearchDTO searchDTO, User currentUser)
	{
		// Before we get the result set count from the database we would want to
		// apply the result set size restrictions.
		// RE Admins are allowed to get large result sets (10,000 rows max)
		// a search. All other users are limited to 1,000 rows per search.
		// So we will first check what type of user is invoking this search.
		int resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_SUBSCRIBER; // Default
		if ((currentUser != null) && (currentUser.getUsertype().equals(ALMConstants.RIVALEDGE_ADMIN)))
		{
			// This is an admin
			resultSetSizeCap = ALMConstants.MAX_RESULT_SIZE_RE_ADMIN;
		}
		
		searchDTO.setMaxResultSizeCap(resultSetSizeCap);

		// Get the result set count
		int resultCount = cacheDAO.findEventDataCount(searchDTO);
		
		// If it is greater than the cap then cut it down
		if (resultCount > resultSetSizeCap)
		{
			return (resultSetSizeCap);
		}
	return (resultCount);
	}

}
