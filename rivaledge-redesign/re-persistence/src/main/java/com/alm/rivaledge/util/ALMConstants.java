package com.alm.rivaledge.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.TreeMap;

public final class ALMConstants
{
	public static final DateFormat	SEARCH_DATE_FORMAT					= new SimpleDateFormat("yyyy-MM-dd");
	public static final String		AMLAW_100							= "AmLaw 100";
	public static final String		AMLAW_200							= "AmLaw 200";
	public static final String		NLJ_250								= "NLJ 350";

	public static final String		RIVALEDGE_ADMIN						= "admin";

	public static final int			MAX_RESULT_SIZE_SUBSCRIBER			= 5000;
	public static final int			MAX_RESULT_SIZE_RE_ADMIN			= 10000;

	public static final String		TWITTER_POST_TYPE_ALL				= "All";
	public static final String		TWITTER_POST_TYPE_BY_LAW_FIRMS		= "Posts By Law Firms";
	public static final String		TWITTER_POST_TYPE_ABOUT_LAW_FIRMS	= "Posts About Law Firms";
	public static final String		TWITTER_POST_TYPE_BY_OTHERS			= "Posts By Others I Follow";

	public static final String		ALL_FIRMS							= "All Firms";
	public static final String		RIVALEDGE_LIST						= "RivalEdge List";
	public static final String  	INDIVIDUAL_LIST      				= "Select Individual Firms";
	public static final String		ANY									= "Any";
	public static final String		ALL_Types							= "All Types";
	public static final String		ALL									= "All";
	public static final String		LAST_WEEK							= "Last Week";
	public static final String		LAST_30_DAYS						= "Last 30 Days";
	public static final String		LAST_90_DAYS						= "Last 90 Days";
	public static final String		LAST_6_MONTHS						= "Last 6 Months";
	public static final String		LAST_YEAR							= "Last Year";
	public static final String		NEXT_WEEK							= "Next Week";
	public static final String		NEXT_30_DAYS						= "Next 30 Days";
	public static final String		NEXT_6_MONTHS						= "Next 6 Months";
	public static final String		NEXT_YEAR							= "Next Year";
	public static final String[]	RIVALEDGE_RANKING_LIST				= { "AmLaw 100", "AmLaw 200", "NLJ 350" };
	public static final String		CONTENT_NEWS						= "News";
	public static final String		CONTENT_PUBLICATIONS				= "Publications";
	public static final String		CONTENT_TWITTER						= "Twitter";
	public static final String		ALL_PRACTICE_AREAS					= "All Practice Areas";
	public static final String		TYPE								= "Type";

	public static final String		CSV_EXT								= "csv";
	public static final String		XLS_EXT								= "xls";
	public static final String		PUBLICATION_DASH					= "Firm news and publications-";
	public static final String 		EVENT_FILE_NAME						= "Firm Events-";
	public static final String 		EVENT								= "Event";
	public static final String		EVENT_WORKBOOK_NAME					= "Event sheet";
	public static final String		WORKBOOK_NAME						= "Publications sheet";
	public static final String		CONTENT_TYPE_XLS					= "application/vnd.ms-excel";
	public static final String		TITLE_NO							= "No.";
	public static final String		TITLE_FIRM_NAME						= "Firm ";
	public static final String		TITLE_SOURCE						= "Source ";
	public static final String		TITLE_DESCRIPTION					= "Description ";
	public static final String		TITLE_RELATED_PRACTICE				= "Related Practice(s) ";
	public static final String		TITLE_DATE							= "Date ";
	public static final String		TITLE_TITLE							= "Title ";
	public static final String		TITLE_LINK							= "Link ";
	public static final String		URLPATTERN							= "^http(s{0,1})://[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9_/\\&\\?\\=\\-\\.\\~\\%]*";
	public static final String		HTTP								= "http://";
	public static final String		HTTPS								= "https://";
	public static final String		HTTPWWW								= "http://www.";

	// People Search constants
	public static final String		ALL_LOCATIONS						= "All Locations";
	public static final String		ALL_FIRM_SIZES						= "All Firm sizes";
	public static final String 		FIRM								= "Firm";
	public static final String		OFFICES								= "Offices";
	public static final String 		PRACTICES							= "Practices";
	public static final String 		FIRM_HEADCOUNT						= "Firm Headcount";
	public static final String 		ASSOCIATES						    = "Associates";
	public static final String 		PEOPLE_OTHER_COUNSEL				= "Other Counsel";
	public static final String 		NET_CHANGE    						= "% Chg. (Attorneys)";
	

	// People Search ids for AmLaw100, 200, watchlists, etc.
	public static final Integer		AMLAW100_ID							= -1;
	public static final Integer		AMLAW200_ID							= -2;
	public static final Integer		NLJ250_ID							= -3;
	public static final Integer		MY_FIRM_ID							= -4;

	// Attorney Search

	public static final String		PARTNERS							= "Partner";
	public static final String		ASSOCIATE							= "Associate";
	public static final String		OTHER_COUNSEL						= "Other Counsel/Attorney";
	public static final String		ADMINISTRATIVE						= "Administrative / Support Staff";
	public static final String		OTHER								= "Other";

	public static final String		TITLE_ATTORNEY						= "Attorney";
	public static final String		ATTORNEY_LOCATION					= "Location";
	public static final String		ATTORNEY_PRACTICES					= "Practice(s)";
	public static final String		ATTORNEY_EDUCATION					= "Education";
	public static final String		ATTORNEY_ADMISSION					= "Admission";
	public static final String		ATTORNEY_BIO_TEXT					= "Bio Text";
	public static final String		ATTORNEY_CONTACT_INFO				= "Contact Info";
	
	public static final String		ATTORNEY_FILE_NAME					= "People Attorney Search-";
	
	public static final String		ALL_OTHER_LOCATIONS					= "Select Individual Locations";
	public static final String 		INDIVIDUAL_PRACTICE_AREA			= "Select Individual Practice Areas";
	public static final String 		LISTED_PRACTICE						= "Search first listed practice only";
	public static final String 		NO_RESULT_MESSAGE 					= "0 Results, Please try a different search";
	//Attorney moves and changes constants
	public static final String		ALL_MOVES_CHANGES					= "All Moves and Changes";
	public static final String		ADDITIONS							= "Additions";
	public static final String		REMOVALS							= "Removals";
	public static final String		UPDATES								= "Updates";
	public static final String 		ALL_ORGANIZATIONS 					= "All Organizations";
	public static final String 		ALL_FIRMS_ORGANIZATION 				= "All Firms and Organizations";
	public static final String		ACTION								= "Action";
	public static final String 		NAME								= "Name";
	public static final String 		TITLE								= "Title";
	public static final String 		FROM								= "From";
	public static final String 		TO									= "To";
	public static final String		ATTORNEY_MOVES_FILE_NAME			= "Attorney Moves And Changes";
	public static final String		ALL_TITLES							= "All Titles";
	public static final String[] 	ALL_TITLES_LIST						= {"Partner", "Associate","Other Counsel/Attorney","Administrative / Support Staff","Other"};
	public static final String[]    CHANGE_TYPES						= {"Additions","Removals","Updates"};
	public static final String	    WATCHLIST						    = "Watchlist";
	
	
	public static final String[]    LOCATIONS 							= {
																			"Boston, MA", 
																			"Dallas, TX",
																			"Denver, CO",
																			"Houston, TX", 
																			"Las Vegas, NV", 
																			"Los Angeles, CA",
																			"Miami, FL", 
																			"New Orleans, LA",
																			"New York, NY",
																			"Orlando, FL", 
																			"Philadelphia, PA",
																			"Phoenix, AZ", 
																			"Pittsburgh, PA",
																			"San Antonio, TX", 
																			"San Francisco, CA", 
																			"Seattle, WA", 
																			"Tampa, FL", 
																			"Washington, DC" 
																			};
	public static final String[]    FIRM_SIZE 							= {
																			"1500+", 
																			"1000-1499", 
																			"750-999", 
																			"500-749", 
																			"400-499", 
																			"300-399", 
																			"200-299", 
																			"100-199", 
																			"50-99", 
																			"25-49", 
																			"1-24"  
																		  };
	public static final String WATCH_LIST = "watch_list";
	public static final String INDIVIDUAL_FIRMS = "individual_firm"; 
	public static final String INDIVIDUAL_ORGS = "individual_orgs";

	public static final String FIRST_LISTED_PRACTICE = "first_listed_practice_area"; 
	public static final String ALL_SCHOOLS = "All Schools";
	
	public static final String CHART_NEWS_COLOUR 	= "#446e49";
	public static final String CHART_PUBS_COLOUR 	= "#0c3362";
	public static final String CHART_TWITTER_COLOUR = "#b89024";
	
	public static final int DEFAULT_NO_OF_COLUMNS_CHART	= 15;
	public static final int PERCENTILE_DELTA = 5;
	
	public static final int NO_OF_EVENTS_IN_TOOLTIP = 5;
	public static final int NO_OF_PRACTICES_IN_TOOLTIP = 5;
	public static final String HOME_PAGE = "home";
	
	
	//Alerts Constants
	public static final String NEWS_AND_PUBS								= "News & Publications";
	public static final String PEOPLE_FIRM_STAT								= "People > Firm Stats";
	public static final String PEOPLE_ATTORNEY_SEARCH						= "People > Attorney Search";
	public static final String PEOPLE_ATTORNEY_MOVES						= "People > Attorney Moves";
	public static final String EVENTS										= "Events";
	public static final String DAILY										= "Daily";
	public static final String BI_WEEKLY									= "Bi-Weekly";
	public static final String WEEKLY										= "Weekly";
	public static final String MONTHLY										= "Monthly";
	
	
	public static final int DAILY_VALUE										= 1;
	public static final int BI_WEEKLY_VALUE									= 2;
	public static final int WEEKLY_VALUE									= 3;
	public static final int MONTHLY_VALUE									= 4;
	
	/**
	 * Holds the checkbox values and display values on View Settings as enum
	 * Display values can be changed without any dependencies, but be cautious in changing(or don't change at all) the names as it may have id references in JSP 
	 */
	public static enum ATTORNEY_DISPLAY_COLUMNS {
		
		firm("Firm"),
		title("Title"),
		location("Location"),
		practice("Practice"),
		education("Education"),
		admission("Admission"),
		bio_text("Bio Text"),
		contact_info("Contact Info");
		
		private String displayValue;
		
		ATTORNEY_DISPLAY_COLUMNS(String displayValue)
		{
			this.displayValue = displayValue;
		}

		public String getDisplayValue()
		{
			return displayValue;
		}

		public String getValue()
		{
			return name();
		}
	}
	
	/**
	 * Holds the checkbox values and display values on View Settings as enum
	 * Display values can be changed without any dependencies, but be cautious in changing(or don't change at all) the names as it may have id references in JSP 
	 */
	public static enum FIRM_STATS_DISPLAY_COLUMNS {
		
		partners("Partners"),
		associates("Associates"),
		other_counsel("Other Counsel"),
		admin("Admin"),
		other("Other");
		
		private String displayValue;
		
		FIRM_STATS_DISPLAY_COLUMNS(String displayValue)
		{
			this.displayValue = displayValue;
		}

		public String getDisplayValue()
		{
			return displayValue;
		}

		public String getValue()
		{
			return name();
		}
	}
	
	/**
	 * This method is used to return all locations that we are displaying in search form for Location field
	 * @return locationsMapValues which hold locations as a Key-Value pattern; key is used for displaying location as a label in Loation drop down
	 * and value is used for DB query
	 *   
	 */
	public static TreeMap<String,String> getAllLocationsConstant()
	{
		TreeMap<String,String> locationsMapValues = new TreeMap<String,String>();
		locationsMapValues.put("Boston", "MA-Boston");
		locationsMapValues.put("Chicago", "IL-Chicago");
		locationsMapValues.put("Dallas", "TX-Dallas");
		locationsMapValues.put("Denver", "CO-Denver");
		locationsMapValues.put("Houston", "TX-Houston");
		locationsMapValues.put("Las Vegas", "NV-Las Vegas");
		locationsMapValues.put("Los Angeles", "CA-Los Angeles");
		locationsMapValues.put("Miami", "FL-Miami");
		locationsMapValues.put("New Orleans", "LA-New Orleans");
		locationsMapValues.put("Philadelphia", "PA-Philadelphia");
		locationsMapValues.put("Phoenix", "AZ-Phoenix");
		locationsMapValues.put("Pittsburgh", "PA-Pittsburgh");
		locationsMapValues.put("San Antonio", "TX-San Antonio");
		locationsMapValues.put("San Diego", "CA-San Diego");
		locationsMapValues.put("San Francisco", "CA-San Francisco");
		locationsMapValues.put("Seattle", "WA-Seattle");
		locationsMapValues.put("Tampa", "FL-Tampa");
		locationsMapValues.put("Washington", "DC-Washington");
		
		return locationsMapValues;
	}
	
	/**
	 * Holds name for the chart modules
	 * Display values can be changed without any dependencies, but be cautious in changing(or don't change at all) the names as it may have id references in JSP 
	 */
	public static enum CHART_MODULE_DISPLAY_COLUMNS 
	{	
		firms("Firms"),
		people("People"),
		client("Clients"),
		events("Events"),
		specific("Specific");
		
		private String displayValue;
		
		CHART_MODULE_DISPLAY_COLUMNS(String displayValue)
		{
			this.displayValue = displayValue;
		}

		public String getDisplayValue()
		{
			return displayValue;
		}

		public String getValue()
		{
			return name();
		}
	}	
}
