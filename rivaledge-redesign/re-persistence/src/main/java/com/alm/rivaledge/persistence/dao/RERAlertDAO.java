package com.alm.rivaledge.persistence.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alm.rivaledge.persistence.dao.lawma0_data.RERAlertMapper;
import com.alm.rivaledge.persistence.domain.lawma0_data.RERAlert;
import com.alm.rivaledge.persistence.domain.lawma0_data.RERAlertUserEmail;

@Repository("rerAlertDAO")
public class RERAlertDAO {

	@Autowired
	private RERAlertMapper	rerAlertMapper;
	
	// Insert data into alert_query table.
	public int insertAlertQuery(RERAlert rerAlert){
		int alertId = rerAlertMapper.insertAlertQuery(rerAlert);
		return alertId;
	}
	
	// Insert data into alert_user mapping table.
	public int insertAlertUserMapping(RERAlert rerAlert){
		int alertUserId = rerAlertMapper.insertAlertUserMapping(rerAlert);
		return alertUserId;
	}
	
	// find the alert from alert_query table
	public RERAlert findRERAlertByTYPE_QUERY(RERAlert rerAlert){
		return rerAlertMapper.findRERAlertByTYPE_QUERY(rerAlert);
	}
	
	// find alert by alert id
	public RERAlert findRERAlertByID(Integer alertId){
		return rerAlertMapper.findRERAlertByID(alertId);
	}
	
	// find all alerts by user id
	public List<RERAlert> findAllUserAlertsByUserId(Integer userId){
		return rerAlertMapper.findAllUserAlertsByUserId(userId);
	}
	
	// count all alert from alert_query table by type and query
	public int getRERAlertCountByTYPE_QUERY(RERAlert rerAlert){
		return rerAlertMapper.getRERAlertCountByTYPE_QUERY(rerAlert);
	}
	
	// update alert from in alert_user table
	public void updateAlertUser(RERAlert rerAlert){
		rerAlertMapper.updateAlertUser(rerAlert);
	}
	
	// insert  user email id
	public void insertUserEmails(RERAlertUserEmail RERAlertUserEmailObj){
		rerAlertMapper.insertUserEmails(RERAlertUserEmailObj);
	}
	
	// find all user email ids
	public List<RERAlertUserEmail> findAllUserEmailIds(Integer userId){
		return rerAlertMapper.findAllUserEmailIds(userId);
	}

	// delete user email id 
	public void deleteUserEmails(Integer id){
		rerAlertMapper.deleteUserEmails(id);
	}

	// delete alert_user by id 
	public void deleteAlertUserById(Integer id){
		rerAlertMapper.deleteAlertUserById(id);
	}
	
	// update user email id
	public void updateAlertEmail(RERAlertUserEmail RERAlertUserEmailObj){
		rerAlertMapper.updateAlertEmail(RERAlertUserEmailObj);
	}

}
