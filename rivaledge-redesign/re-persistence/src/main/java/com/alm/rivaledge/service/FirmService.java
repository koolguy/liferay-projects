package com.alm.rivaledge.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.FirmLocation;
import com.alm.rivaledge.persistence.domain.lawma0_data.Keyword;
import com.alm.rivaledge.persistence.domain.lawma0_data.Practice;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.transferobject.EventSearchDTO;
import com.alm.rivaledge.transferobject.FirmResultDTO;
import com.alm.rivaledge.transferobject.FirmSearchDTO;
import com.alm.rivaledge.util.FirmJsonUtility;

/**
 * Handles all business logic for the Firm entity in ALM Rival Edge.
 * 
 * @author FL867
 * @since Sprint 1
 */
@Component
public interface FirmService
{
	/**
	 * Returns the list of firms in the specified ranking type (AmLaw 100, AmLaw
	 * 200, or NLJ 250)
	 * 
	 * @param rankingType
	 *            String containing "AmLaw 100" or "AmLaw 200" or "NLJ 250"
	 * @return List<Firm>
	 */
	public List<Firm> getRanking(String rankingType);

	/**
	 * Returns a Set containing all the Firms in the LMD database.
	 * 
	 * @return List<Firm>
	 */
	public List<Firm> getAllFirms();

	/**
	 * Returns a list of all the practices available across Firms in Rival Edge.
	 * 
	 * @return List<Practice>
	 */
	public List<Practice> getAllPractices();

	/**
	 * Returns a list of matching practices for auto-complete key ahead.
	 * 
	 * @param String
	 *            containing the keyword to search for. This should be at least
	 *            three characters long, not counting spaces.
	 * @return List<Practice>
	 */
	public List<Practice> getMatchingPractices(String practiceKeyword);

	/**
	 * Returns a list of all unique firm locations (no duplicates).
	 * 
	 * @return List<FirmLocation>
	 */
	public List<FirmLocation> getAllLocations();

	/**
	 * Finds a firm by its unique id (primary key)
	 * 
	 * @param companyId
	 * @return
	 */
	public Firm getFirmById(Integer companyId);

	/**
	 * Returns a list of RivalEdge List  
	 * 
	 * @return List<Firm>  
	 */
	public List<FirmJsonUtility> getRivalEdgeList(List<FirmJsonUtility> firmJsonUtilityList);
	
	/**
	 * Returns the set of last 100 keywords searched for by a specific user.
	 * @param currentUser
	 * @return Set<String>
	 */
	public List<String> getUserKeywords(User currentUser);

	public int getFirmSearchCount(FirmSearchDTO fsDTO, User currentUser);
	
	public int getEventSearchCount(EventSearchDTO esDTO, User currentUser);

	public List<FirmResultDTO> firmNewsPubsSearch(FirmSearchDTO searchFormTo);
	
	/**
	 * This method is used to get all organization list
	 * @return OrganizationResultDTO
	 */
	public List<Firm> getOrganizationsList();
	
	/**
	 * This method is used for getting firm watchlists sorted by Date added
	 * @return UserGroup 
	 */
	public List<UserGroup> getWatchListSortedByDate(Integer userId);
	
	/**
	 * Method returns list of top five publications by practice area 
	 * Specified in the FirmSearchDTO.
	 * 
	 * @param firmSearchDTO
	 * @return
	 */
	public List<FirmResultDTO> findFiveLatestPublicationsByPractice(FirmSearchDTO firmSearchDTO);
	
	/**
	 * Add keyword information to database if it is not already present
	 * @param keyword
	 */
	public void addkeyword(Keyword keyword);
	
	/**
	 * This method is to check whether we need to store keyword information in database or not
	 * if count is 0 keyword will be  persisted in DB
	 * if count is more than 0 then  we would not store keyword information in database	 
	 * @param keyword
	 * @return
	 */
	public Integer countByKeyword(String keyword);
	
	/**
	 * This method will return list of keywords for logged in user
	 * @param userId
	 * @return
	 */
	public List<String> findKeywords(Integer userId);

	public List<Firm> getFirmByName(String drilldownFirm);
}
