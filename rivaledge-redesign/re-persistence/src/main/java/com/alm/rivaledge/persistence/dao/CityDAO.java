package com.alm.rivaledge.persistence.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.alm.rivaledge.persistence.dao.lawma0_data.CityMapper;
import com.alm.rivaledge.persistence.domain.lawma0_data.City;


/**
 * Data Access Object that handles all persistence for the City domain object.
 * @author FL605
 *
 */
@Repository("cityDAO")
public class CityDAO 
{

	@Autowired
	private CityMapper 	cityMapper;
	
	
	public List<City> selectGeoRegion()
	{
		return cityMapper.selectGeoRegion();
	}
	
	public List<City> selectCountry(String geoRegion)
	{
		return cityMapper.selectCountry(geoRegion);
	}
	
	public List<City> selectCity(String country)
	{
		return cityMapper.selectCity(country);
	}
	
	public List<City> fetchCitiesForAutocomplete()
	{
		return cityMapper.selectAllCities();
	}
	
	
}
