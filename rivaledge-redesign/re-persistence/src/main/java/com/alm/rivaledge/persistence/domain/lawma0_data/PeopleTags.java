package com.alm.rivaledge.persistence.domain.lawma0_data;

import java.util.Date;

public class PeopleTags extends PeopleTagsKey {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column lawma0_data..people_tags.tags
     *
     * @mbggenerated Wed Jul 31 18:41:43 IST 2013
     */
    private String tags;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column lawma0_data..people_tags.timestamp
     *
     * @mbggenerated Wed Jul 31 18:41:43 IST 2013
     */
    private Date timestamp;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column lawma0_data..people_tags.tags
     *
     * @return the value of lawma0_data..people_tags.tags
     *
     * @mbggenerated Wed Jul 31 18:41:43 IST 2013
     */
    public String getTags() {
        return tags;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column lawma0_data..people_tags.tags
     *
     * @param tags the value for lawma0_data..people_tags.tags
     *
     * @mbggenerated Wed Jul 31 18:41:43 IST 2013
     */
    public void setTags(String tags) {
        this.tags = tags == null ? null : tags.trim();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column lawma0_data..people_tags.timestamp
     *
     * @return the value of lawma0_data..people_tags.timestamp
     *
     * @mbggenerated Wed Jul 31 18:41:43 IST 2013
     */
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column lawma0_data..people_tags.timestamp
     *
     * @param timestamp the value for lawma0_data..people_tags.timestamp
     *
     * @mbggenerated Wed Jul 31 18:41:43 IST 2013
     */
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }
}