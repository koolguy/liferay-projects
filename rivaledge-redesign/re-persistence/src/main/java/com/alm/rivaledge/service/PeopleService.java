package com.alm.rivaledge.service;

import java.util.List;

import org.springframework.stereotype.Component;

import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.transferobject.PeopleResultDTO;
import com.alm.rivaledge.transferobject.PeopleSearchDTO;

/**
 * Handles all business logic for the Person entity in ALM Rival Edge.
 * 
 * @author FL867
 * @since Sprint 2
 */
@Component
public interface PeopleService
{
	/**
	 * Call this method only if you need location and practice drill down
	 * information.
	 * 
	 * Expensive, time-consuming multi-threaded search for all the people
	 * (partners, attorneys, associates, others) who meet the criteria set forth
	 * in the search DTO.
	 * 
	 * Will also fetch all the drill down data.
	 * 
	 * @param psDTO
	 * @param currentUser
	 * @return
	 */
	public List<PeopleResultDTO> peopleSearch(PeopleSearchDTO psDTO, User currentUser);

	/**
	 * Better alternative for fetching firm statistics. As far as possible this
	 * is the method that should be called.
	 * 
	 * @param psDTO
	 * @param currentUser
	 * @return
	 */
	public List<PeopleResultDTO> peopleSearchWithoutDrilldown(PeopleSearchDTO psDTO, User currentUser);

	/**
	 * Returns the count of search results for the given search criteria
	 * 
	 * @param psDTO
	 * @param currentUser
	 * @return result count
	 */
	public int getPeopleSearchCount(PeopleSearchDTO psDTO, User currentUser);
}
