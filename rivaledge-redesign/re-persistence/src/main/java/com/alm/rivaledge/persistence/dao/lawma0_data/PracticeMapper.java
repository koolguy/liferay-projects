package com.alm.rivaledge.persistence.dao.lawma0_data;

import com.alm.rivaledge.persistence.domain.lawma0_data.Practice;
import com.alm.rivaledge.persistence.domain.lawma0_data.PracticeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PracticeMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..practices_kws
     *
     * @mbggenerated Fri Jul 19 18:14:26 GMT+05:30 2013
     */
    int countByExample(PracticeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..practices_kws
     *
     * @mbggenerated Fri Jul 19 18:14:26 GMT+05:30 2013
     */
    int deleteByExample(PracticeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..practices_kws
     *
     * @mbggenerated Fri Jul 19 18:14:26 GMT+05:30 2013
     */
    int deleteByPrimaryKey(String practiceArea);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..practices_kws
     *
     * @mbggenerated Fri Jul 19 18:14:26 GMT+05:30 2013
     */
    int insert(Practice record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..practices_kws
     *
     * @mbggenerated Fri Jul 19 18:14:26 GMT+05:30 2013
     */
    int insertSelective(Practice record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..practices_kws
     *
     * @mbggenerated Fri Jul 19 18:14:26 GMT+05:30 2013
     */
    List<Practice> selectByExample(PracticeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..practices_kws
     *
     * @mbggenerated Fri Jul 19 18:14:26 GMT+05:30 2013
     */
    int updateByExampleSelective(@Param("record") Practice record, @Param("example") PracticeExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table lawma0_data..practices_kws
     *
     * @mbggenerated Fri Jul 19 18:14:26 GMT+05:30 2013
     */
    int updateByExample(@Param("record") Practice record, @Param("example") PracticeExample example);
}