package com.alm.rivaledge.persistence.domain.lawma0_data;

/**
 * Domain object that maps to the tbl_rer_user_mapping table. Allows persistence
 * of the users upon whom the Liferay RER site template has been applied.
 * 
 * @author FL734
 * @since Sprint 9.1
 * @version 1.0
 */
public class LoginHookUserMapping 
{
	private Integer lmdUserId;
	private Integer liferayUserId;
	private Boolean isTemplateApplied;
	
	/**
	 * Returns the user id of this user in LMD; maps to the id column in the users table.
	 * 
	 * @return
	 */
	public Integer getLmdUserId() 
	{
		return lmdUserId;
	}
	
	public void setLmdUserId(Integer lmdUserId) 
	{
		this.lmdUserId = lmdUserId;
	}
	
	/**
	 * The id in the Liferay lportal table that corresponds to this user id. This is NOT the same as the 
	 * LMD user id and should not be used interchangeably with that entity.
	 * 
	 * @return
	 */
	public Integer getLiferayUserId() 
	{
		return liferayUserId;
	}
	
	public void setLiferayUserId(Integer liferayUserId) 
	{
		this.liferayUserId = liferayUserId;
	}
	
	/**
	 * Returns true or false depending on whether the Liferay site template has been applied to this
	 * user's private pages in Liferay.
	 * 
	 * @return
	 */
	public Boolean getIsTemplateApplied() 
	{
		return isTemplateApplied;
	}
	
	public void setIsTemplateApplied(Boolean isTemplateApplied) 
	{
		this.isTemplateApplied = isTemplateApplied;
	}
}
