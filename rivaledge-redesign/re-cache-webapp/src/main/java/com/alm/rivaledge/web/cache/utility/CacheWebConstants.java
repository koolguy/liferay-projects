package com.alm.rivaledge.web.cache.utility;


/**
 * 
 * This bean is used to Mavenizing the web URL parameter for re-cache-webapp application 
 * 
 * 
 * @author Sachin
 * @version 1.0
 * @since Sprint 9
 *
 */
public class CacheWebConstants {
	
	//This variable will be set by Maven, and will have the URL of the re-cache-webapp 
	private String webURL;

	public String getWebURL() {
		return webURL;
	}

	public void setWebURL(String webURL) {
		this.webURL = webURL;
	}
	
}
