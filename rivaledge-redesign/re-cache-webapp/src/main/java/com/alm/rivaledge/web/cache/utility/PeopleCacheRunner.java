package com.alm.rivaledge.web.cache.utility;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;

import com.alm.rivaledge.service.CacheService;

/**
 * Runnable wrapper for the CacheServiceImpl that allows a cache refresh
 * to be spawned off as a thread by itself.
 * 
 * @author Sachin
 * @version 1.0
 * @since Sprint 9
 */
public class PeopleCacheRunner implements Runnable
{
	
	private CacheService cacheService; 
	
	public PeopleCacheRunner(CacheService cacheService){
		this.cacheService=cacheService;
		
	}
	
	@Override
	public void run() 
	{
		// Before executing the refresh, toggle the flag to TRUE
		// so that no one else can do so at the same time.
		RefreshConstants.PEOPLE_REFRESH_FLAG = true;
		
		try
		{
			// Begin the refresh process
			System.out.println("\n\n*** Spawning the cache refresh thread...\n");
			System.out.println((cacheService == null) ? "\n\n*** NULL\n" : "\n\n*** NOT NULL\n");
			cacheService.attorneySearchDataJob();
			System.out.println("\n\n-------Attorney Search Data Job completed successfully.-------\n");
			cacheService.attorneyMovesAndChangesJob();
			System.out.println("\n\n-------Attorney Moves And Changes Job completed successfully.-------\n");
			cacheService.firmStatisticsJob();
			System.out.println("\n\n-------Firm Statistics Job completed successfully.-------\n");
		}
		catch (IOException ioEx)
		{
			//print stack trace 
			ioEx.printStackTrace();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			//print stack trace 
		}	
		
		// Toggle the flag back to false so that another refresh
		// can be fired in the future if necessary
		RefreshConstants.PEOPLE_REFRESH_FLAG = false;
	}

}
