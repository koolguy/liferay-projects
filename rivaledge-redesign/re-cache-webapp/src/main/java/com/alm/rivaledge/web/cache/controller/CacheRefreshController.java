package com.alm.rivaledge.web.cache.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.portlet.ModelAndView;

import com.alm.rivaledge.service.CacheService;
import com.alm.rivaledge.web.cache.utility.EventsCacheRunner;
import com.alm.rivaledge.web.cache.utility.FirmCacheRunner;
import com.alm.rivaledge.web.cache.utility.PeopleCacheRunner;
import com.alm.rivaledge.web.cache.utility.RefreshConstants;

/**
 * Controller that handles requests for the various cache refresh
 * tasks. Each controller uses SimpleAsyncTaskExecutor to spawn off
 * a separate thread that will actually perform the refresh. This
 * avoids having the calling thread to block and wait for the refresh
 * to finish before it can return.
 *  
 * @author Sachin
 * @version 1.0
 * @since Sprint 9
 */



@Controller
public class CacheRefreshController
{

	@Autowired
	private CacheService cacheService; 
	
	/**
	 * Refreshes the Firm - News and Pubs cache. Pulls records for news items, publications, and Tweets
	 * from the lawma0_data.data table and stuffs them into the lawma0_data.tbl_rer_cache_firm_newspubs
	 * cache table.
	 * 
	 * @param arg0 - HttpRequest
	 * @param arg1 - HttpResponse
	 * @throws Exception - Exception
	 */
	@RequestMapping(value = "/news-refresh.htm", method = RequestMethod.GET)
	public String handleNewsPubsRefreshRequest(HttpServletRequest arg0, HttpServletResponse arg1) throws Exception 
	{
		System.out.println("inside handleNewsPubsRefreshRequest");
		if (!RefreshConstants.NEWS_PUBS_REFRESH_FLAG)
		{
			SimpleAsyncTaskExecutor thisExecutor = new SimpleAsyncTaskExecutor();
			thisExecutor.setConcurrencyLimit(SimpleAsyncTaskExecutor.NO_CONCURRENCY);
			thisExecutor.execute(new FirmCacheRunner(cacheService));
			System.out.println("Firms News and Pubs Job started");
			return ("success");
		}
		else
		{
			return ("failure");
		}
	}
	
	
	/**
	 * Refreshes the Events cache. Pulls records 
	 * from the lawma0_data.data table and stuffs them into the lawma0_data.tbl_rer_cache_events
	 * cache table.
	 * 
	 * @param arg0 - HttpRequest
	 * @param arg1 - HttpResponse
	 * @throws Exception - Exception
	 */
	@RequestMapping(value = "/events-refresh.htm", method = RequestMethod.GET)
	public String handleEventsRefreshRequest(HttpServletRequest arg0, HttpServletResponse arg1) throws Exception 
	{
		System.out.println("inside handleNewsPubsRefreshRequest");
		if (!RefreshConstants.EVENTS_REFRESH_FLAG)
		{
			SimpleAsyncTaskExecutor thisExecutor = new SimpleAsyncTaskExecutor();
			thisExecutor.setConcurrencyLimit(SimpleAsyncTaskExecutor.NO_CONCURRENCY);
			thisExecutor.execute(new EventsCacheRunner(cacheService));
			System.out.println("Events Job started");
			return ("success");
		}
		else
		{
			return ("failure");
		}
	}
	
	
	
	/**
	 * Refreshes the Attorney details, Attorney Moves and Changes and Firms Statics cache. Pulls records 
	 * from the lawma0_data.data, lawma0_data.people tables and stuffs them into
	 * 1. tbl_rer_cache_attorney_data - Attorney Details 
 	 * 2. lawma0_data.tbl_rer_cache_attorney_moveschanges - Attorney Moves and Changes
 	 * 3. lawma0_data.tbl_rer_cache_firm_statistics  - Firm Statistics
	 * cache table.
	 * 
	 * @param arg0 - HttpRequest
	 * @param arg1 - HttpResponse
	 * @throws Exception - Exception
	 */
	@RequestMapping(value = "/peopledata-refresh.htm", method = RequestMethod.GET)
	public String handlePeopleRefreshRequest(HttpServletRequest arg0, HttpServletResponse arg1) throws Exception 
	{
		System.out.println("inside handleNewsPubsRefreshRequest");
		if (!RefreshConstants.PEOPLE_REFRESH_FLAG)
		{
			SimpleAsyncTaskExecutor thisExecutor = new SimpleAsyncTaskExecutor();
			thisExecutor.setConcurrencyLimit(SimpleAsyncTaskExecutor.NO_CONCURRENCY);
			thisExecutor.execute(new PeopleCacheRunner(cacheService));
			System.out.println("People Job started");
			return ("success");
		}
		else
		{
			return ("failure");
		}
	}
	
	
	
	
}
