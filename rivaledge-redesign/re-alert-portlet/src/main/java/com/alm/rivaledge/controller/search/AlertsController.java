package com.alm.rivaledge.controller.search;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.swing.text.DefaultEditorKit.CutAction;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.alm.rivaledge.model.AlertSearchModelBean;
import com.alm.rivaledge.model.AttorneyMoveSearchModelBean;
import com.alm.rivaledge.model.AttorneySearchModelBean;
import com.alm.rivaledge.model.EventSearchModelBean;
import com.alm.rivaledge.model.FirmSearchModelBean;
import com.alm.rivaledge.model.PeopleSearchModelBean;
import com.alm.rivaledge.persistence.dao.lawma0_data.RERAlertMapper;
import com.alm.rivaledge.persistence.domain.lawma0_data.Firm;
import com.alm.rivaledge.persistence.domain.lawma0_data.Practice;
import com.alm.rivaledge.persistence.domain.lawma0_data.RERAlert;
import com.alm.rivaledge.persistence.domain.lawma0_data.RERAlertUserEmail;
import com.alm.rivaledge.persistence.domain.lawma0_data.User;
import com.alm.rivaledge.persistence.domain.lawma0_data.UserGroup;
import com.alm.rivaledge.service.FirmService;
import com.alm.rivaledge.service.RERAlertService;
import com.alm.rivaledge.service.UserService;
import com.alm.rivaledge.service.WatchlistService;
import com.alm.rivaledge.transferobject.AttorneyMoveChangesSearchDTO;
import com.alm.rivaledge.transferobject.EventSearchDTO;
import com.alm.rivaledge.transferobject.UserGroupDTO;
import com.alm.rivaledge.util.ALMConstants;
import com.alm.rivaledge.util.ContentTypeJsonUtility;
import com.alm.rivaledge.util.FirmJsonUtility;
import com.alm.rivaledge.util.LocationJsonUtility;
import com.alm.rivaledge.util.PracticeJsonUtility;
import com.alm.rivaledge.util.WebUtil;
import com.google.gson.Gson;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ListUtil;
import com.liferay.portal.kernel.util.StringUtil;

/**
 * Alert controller responsible for all operation on Firm search form and results
 * screen
 * 
 * @author FL605
 * @since Sprint 1
 */
@Controller
@RequestMapping(value = "VIEW")
@SessionAttributes({ "alertSearchModelBean", "alertfirmSearchModelBean", "alertPeopleSearchModelBean", "alertAttorneyMovesSearchModelBean", "alertAttorneySearchModelBean", "alertEventSearchModelBean"})
public class AlertsController 
{
	protected Log		_log	= LogFactoryUtil.getLog(AlertsController.class.getName());

	private static String[] twitterPostTypeData = { ALMConstants.ALL, 
		ALMConstants.TWITTER_POST_TYPE_BY_LAW_FIRMS,
		ALMConstants.TWITTER_POST_TYPE_ABOUT_LAW_FIRMS,
		ALMConstants.TWITTER_POST_TYPE_BY_OTHERS
		}; 

	
	@Autowired
	private RERAlertService reralertService;
	@Autowired
	private FirmService	firmService;
	@Autowired
	private UserService	userService;
	@Autowired
	private WatchlistService	watchlistService;
	/**
	 * Populates the Model which holds the Search criteria in search form initializing to default values
	 *  
	 * @param model
	 */
	@ModelAttribute
	public void populateAlertSearchModelBean(ModelMap model,PortletRequest request)
	{
		
		User 	currentUser = WebUtil.getCurrentUser(request, userService); 
		
		if ( model.get("alertSearchModelBean") == null)
		{
			AlertSearchModelBean alertSearchModelBean = new AlertSearchModelBean();
			alertSearchModelBean.init(); //do an explicit init for defualt values
			
			
			model.addAttribute("alertSearchModelBean", alertSearchModelBean);
		}
		
		if ( model.get("alertfirmSearchModelBean") == null)
		{
			FirmSearchModelBean firmSearchModelBean = new FirmSearchModelBean();
			firmSearchModelBean.init(); //do an explicit init for defualt values
			List<UserGroup> 	allWatchListsDefaultList = null;
			
			if (currentUser != null)
			{
				allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
			}
			
			List<String> firmList = new ArrayList<String>();
			List<Integer> firmListWatchList = new ArrayList<Integer>();
			
			if (allWatchListsDefaultList == null || allWatchListsDefaultList.isEmpty())
			{
				//FirmType
				firmSearchModelBean.setFirmType(ALMConstants.RIVALEDGE_LIST);
				firmList.add(ALMConstants.AMLAW_100);
			} 
			else
			{
				firmSearchModelBean.setFirmType(ALMConstants.WATCH_LIST);
				//firmListWatchList.add("" + allWatchListsDefaultList.get(0).getGroupId());
				firmListWatchList.add(allWatchListsDefaultList.get(0).getGroupId());
			}
			firmSearchModelBean.setFirmList(firmList);
			firmSearchModelBean.setFirmListWatchList(firmListWatchList);
			model.addAttribute("alertfirmSearchModelBean", firmSearchModelBean);
		}
		if ( model.get("alertPeopleSearchModelBean") == null)
		{
			PeopleSearchModelBean alertPeopleSearchModelBean = new PeopleSearchModelBean();
			alertPeopleSearchModelBean.init(); //do an explicit init for defualt values
			List<UserGroup> 		allWatchListsDefaultList = null;
			
			if (currentUser != null)
			{
				allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
			}

			List<String> firmList = new ArrayList<String>();
			List<Integer> firmListWatchList = new ArrayList<Integer>();

			if (allWatchListsDefaultList == null || allWatchListsDefaultList.isEmpty())
			{
				//FirmType
				alertPeopleSearchModelBean.setFirmType(ALMConstants.RIVALEDGE_LIST);
				firmList.add(ALMConstants.AMLAW_100);
			}
			else
			{
				alertPeopleSearchModelBean.setFirmType(ALMConstants.WATCH_LIST);
				//firmListWatchList.add("" + allWatchListsDefaultList.get(0).getGroupId());
				firmListWatchList.add(allWatchListsDefaultList.get(0).getGroupId());
			}
			alertPeopleSearchModelBean.setFirmList(firmList);
			alertPeopleSearchModelBean.setFirmListWatchList(firmListWatchList);
			model.addAttribute("alertPeopleSearchModelBean", alertPeopleSearchModelBean);
		}
		if ( model.get("alertAttorneyMovesSearchModelBean") == null)
		{
			List<UserGroup> 			allWatchListsDefaultList 	= null;
			AttorneyMoveSearchModelBean alertAttorneyMovesSearchModelBean = new AttorneyMoveSearchModelBean();
			alertAttorneyMovesSearchModelBean.init(); //do an explicit init for defualt values
			
			if (currentUser != null)
			{
				allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
			}
			
			List<String> firmList = new ArrayList<String>();
			List<Integer> firmListWatchList = new ArrayList<Integer>();
			
			if (allWatchListsDefaultList == null || allWatchListsDefaultList.isEmpty())
			{
				//FirmType
				alertAttorneyMovesSearchModelBean.setFirmType(ALMConstants.RIVALEDGE_LIST);
				firmList.add(ALMConstants.AMLAW_100);
			} 
			else
			{
				alertAttorneyMovesSearchModelBean.setFirmType(ALMConstants.WATCH_LIST);
				firmListWatchList.add(allWatchListsDefaultList.get(0).getGroupId());
			}
			alertAttorneyMovesSearchModelBean.setFirmList(firmList);
			alertAttorneyMovesSearchModelBean.setFirmListWatchList(firmListWatchList);
			
			model.addAttribute("alertAttorneyMovesSearchModelBean", alertAttorneyMovesSearchModelBean);
		}
		
		if ( model.get("alertAttorneySearchModelBean") == null)
		{
			List<UserGroup> 		allWatchListsDefaultList = null;
			AttorneySearchModelBean alertAttorneySearchModelBean = new AttorneySearchModelBean();
			alertAttorneySearchModelBean.init(); //do an explicit init for defualt values
			if (currentUser != null)
			{
				allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
			}
			
			List<String> firmList = new ArrayList<String>();
			List<Integer> firmListWatchList = new ArrayList<Integer>();
			
			if (allWatchListsDefaultList == null || allWatchListsDefaultList.isEmpty())
			{
				//FirmType
				alertAttorneySearchModelBean.setFirmType(ALMConstants.RIVALEDGE_LIST);
				firmList.add(ALMConstants.AMLAW_100);
			} 
			else
			{
				alertAttorneySearchModelBean.setFirmType(ALMConstants.WATCH_LIST);
				firmListWatchList.add(allWatchListsDefaultList.get(0).getGroupId());
			}
			alertAttorneySearchModelBean.setFirmList(firmList);
			alertAttorneySearchModelBean.setFirmListWatchList(firmListWatchList);
			
			model.addAttribute("alertAttorneySearchModelBean", alertAttorneySearchModelBean);
		}
		
		if ( model.get("alertEventSearchModelBean") == null)
		{
			List<UserGroup> allWatchListsDefaultList = null;
			EventSearchModelBean alertEventSearchModelBean = new EventSearchModelBean();
			alertEventSearchModelBean.init(); //do an explicit init for defualt values
			if (currentUser != null)
			{
				allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
			}
			
			
			List<String> firmList = new ArrayList<String>();
			List<Integer> firmListWatchList = new ArrayList<Integer>();
			
			if (allWatchListsDefaultList == null || allWatchListsDefaultList.isEmpty())
			{
				//FirmType
				alertEventSearchModelBean.setFirmType(ALMConstants.RIVALEDGE_LIST);
				firmList.add(ALMConstants.AMLAW_100);
			}
			else
			{
				alertEventSearchModelBean.setFirmType(ALMConstants.WATCH_LIST);
				firmListWatchList.add(allWatchListsDefaultList.get(0).getGroupId());
			}
			alertEventSearchModelBean.setFirmList(firmList);
			alertEventSearchModelBean.setFirmListWatchList(firmListWatchList);
			model.addAttribute("alertEventSearchModelBean", alertEventSearchModelBean);
		}
		
	}

	@ResourceMapping("FirmsURL")
	public String getFirmsFilters(@ModelAttribute("alertfirmSearchModelBean") FirmSearchModelBean alertfirmSearchModelBean, 
			 ModelMap model, ResourceRequest request, ResourceResponse response)
	{
		// news and pubs
		System.out.println("\n\n\n/////////// GOT IN : FirmsURL \n\n\n");
		
		Gson 						gson 					= new Gson();
		FirmJsonUtility 			firmJsonUtility 		= null;
		PracticeJsonUtility 		practiceJsonUtility 	= null;
		List<FirmJsonUtility> 		firmJsonUtilityList 	= new ArrayList<FirmJsonUtility>();
		List<PracticeJsonUtility> 	practiceJsonUtilityList = new ArrayList<PracticeJsonUtility>();
		List<UserGroupDTO> 			allWatchLists 			= null;
		List<Firm>	 				firmsSet 				= firmService.getAllFirms();
		List<Practice> 				practiceList 			= firmService.getAllPractices();
		User 						currentUser				= WebUtil.getCurrentUser(request, userService);
		
		if (currentUser != null)
		{
			allWatchLists = (List<UserGroupDTO>) watchlistService.getAllFirmWatchList(currentUser.getId());
		}
		
		for (Firm firm : firmsSet)
		{
			firmJsonUtility = new FirmJsonUtility();
			firmJsonUtility.setId(firm.getCompanyId().toString());
			firmJsonUtility.setLabel(firm.getCompany());
			firmJsonUtility.setCategory("Single Firms");
			firmJsonUtilityList.add(firmJsonUtility);
		}
		for (Practice practice : practiceList)
		{
			practiceJsonUtility = new PracticeJsonUtility();
			practiceJsonUtility.setLabel(practice.getPracticeArea());
			practiceJsonUtilityList.add(practiceJsonUtility);

		}
		// converting all firms to JSON string
		//String jsonAllFirms = gson.toJson(firmService.getRivalEdgeList(firmJsonUtilityList));
		String jsonAllFirms = gson.toJson(firmJsonUtilityList);
		// converting all Practices to JSON string
		String jsonAllPractices = gson.toJson(practiceJsonUtilityList);

		// converting all Keyword to JSON string
		String jsonAllKeyWords = (currentUser != null) ? gson.toJson(firmService.findKeywords(currentUser.getId())) : "[]"; // send an empty array to avoid javascript error
																														//var keywords = ;	

		//for Auto complete
		List<ContentTypeJsonUtility> contentTypeJsonUtilityList = new ArrayList<ContentTypeJsonUtility>();
		contentTypeJsonUtilityList.add(new ContentTypeJsonUtility(ALMConstants.ALL_Types, ALMConstants.ALL_Types));
		contentTypeJsonUtilityList.add(new ContentTypeJsonUtility("News","News"));
		contentTypeJsonUtilityList.add(new ContentTypeJsonUtility("Pubs", "Publications"));
		contentTypeJsonUtilityList.add(new ContentTypeJsonUtility("Twitter", "Twitter"));
		
		// populate Models
		model.addAttribute("twitterPostTypeData",gson.toJson(twitterPostTypeData) );
		model.addAttribute("contentTypeData",  gson.toJson(contentTypeJsonUtilityList));
		model.addAttribute("firmJson", jsonAllFirms);
		model.addAttribute("practiceJson", jsonAllPractices);
		model.addAttribute("keywordJson", jsonAllKeyWords);
		model.addAttribute("allFirms", firmsSet);
		model.addAttribute("allPracticeArea", practiceList);
		model.addAttribute("allWatchLists", allWatchLists);
		model.addAttribute("allRankingList", ALMConstants.RIVALEDGE_RANKING_LIST);

		List<UserGroup> allWatchListsDefaultList = null;
		if (currentUser != null)
		{
			allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
			if (allWatchListsDefaultList.size() == 0)
			{
				model.addAttribute("allWatchListsDefaultList", "");
			}
			else
			{
				model.addAttribute("allWatchListsDefaultList", allWatchListsDefaultList.get(0));
			}
		}
		
		return "firmsearch";
	}
	
	@ResourceMapping("FirmsStatURL")
	public String getFirmsStatsFilters(@ModelAttribute("alertPeopleSearchModelBean") PeopleSearchModelBean alertPeopleSearchModelBean, 
			 ModelMap model, ResourceRequest request, ResourceResponse response)
	{
		// people firm stats
		System.out.println("\n\n\n/////////// GOT IN : FirmsStatURL \n\n\n");
		
		Gson gson = new Gson();
		FirmJsonUtility firmJsonUtility = null;
		PracticeJsonUtility practiceJsonUtility = null;
		List<UserGroupDTO> allWatchLists = null;
		List<FirmJsonUtility> firmJsonUtilityList = new ArrayList<FirmJsonUtility>();
		List<PracticeJsonUtility> practiceJsonUtilityList = new ArrayList<PracticeJsonUtility>();
		
		//Getting logged in user
		User currentUser = WebUtil.getCurrentUser(request, userService);
		
		if(currentUser != null)
		{
			allWatchLists = (List<UserGroupDTO>) watchlistService.getAllFirmWatchList(currentUser.getId());
		}	
		
		// fetching all firms information from DB to show under firms pop up
		List<Firm> firmsSet = firmService.getAllFirms();

		// fetching all Practice area information from DB to show under firms
		// pop up
		List<Practice> practiceList = firmService.getAllPractices();

		// creating firm json list for firm Auto completer
		// converting List<Firm> to List<FirmJsonUtility> as FirmJsonUtility
		// utility class have attributes(id,label,category)
		// which is required for Firms Autocompleter/Type ahead
		for (Firm firm : firmsSet)
		{
			firmJsonUtility = new FirmJsonUtility();
			firmJsonUtility.setId(firm.getCompanyId().toString());
			firmJsonUtility.setLabel(firm.getCompany());
			firmJsonUtility.setCategory("Single Firms");
			firmJsonUtilityList.add(firmJsonUtility);
		}

		// creating Practice area json list for Practice area Auto completer
		// converting List<Practice> to List<PracticeJsonUtility> as
		// PracticeJsonUtility utility class have attribute(label)
		// which is required for Firms Autocompleter/Type ahead
		for (Practice practice : practiceList)
		{
			practiceJsonUtility = new PracticeJsonUtility();
			practiceJsonUtility.setLabel(practice.getPracticeArea());
			practiceJsonUtilityList.add(practiceJsonUtility);
		}	
		
		
		// converting all firms to JSON string
		String jsonAllFirms = gson.toJson(firmService.getRivalEdgeList(firmJsonUtilityList));

		// converting all Practices to JSON string
		String jsonAllPractics = gson.toJson(practiceJsonUtilityList);
		model.addAttribute("firmJson", jsonAllFirms);
		model.addAttribute("practiceJson", jsonAllPractics);
		model.addAttribute("allFirms", firmsSet);
		model.addAttribute("allPracticeArea", practiceList);
		model.addAttribute("allWatchListsResult", allWatchLists);
		model.addAttribute("allOtherLocations", ALMConstants.LOCATIONS);
		model.addAttribute("allFirmSize", ALMConstants.FIRM_SIZE);
		model.addAttribute("allFirmSizeJson", gson.toJson(ALMConstants.FIRM_SIZE));
		model.addAttribute("allRankingList", ALMConstants.RIVALEDGE_RANKING_LIST);
		
		List<UserGroup> allWatchListsDefaultList = null;
		if(currentUser != null)
		{
			allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
			if(allWatchListsDefaultList.size() == 0)
			{
				model.addAttribute("allWatchListsDefaultList", "");
			}
			else
			{
				model.addAttribute("allWatchListsDefaultList", allWatchListsDefaultList.get(0));
			}
		}

		//model data for comparison options
		model.put("doIHaveFirm", WebUtil.doesUserBelongToAnyFirm(currentUser));
		model.put("allWatchLists", WebUtil.getUserWatchLists(currentUser, watchlistService));
		model.addAttribute("allDisplayColumns", ALMConstants.FIRM_STATS_DISPLAY_COLUMNS.values());
		
		
		return "peoplesearch";
	}
	
	@ResourceMapping("AttorneyMovesURL")
	public String getAttorneyMovesFilters(@ModelAttribute("alertAttorneyMovesSearchModelBean") AttorneyMoveSearchModelBean alertAttorneyMovesSearchModelBean, 
			 ModelMap model, ResourceRequest request, ResourceResponse response)
	{
		// people attorney moves
		System.out.println("\n\n\n/////////// GOT IN : AttorneyMovesURL \n\n\n");
		
		//creating reference of Gson which we would help converting firms and practice
				//list to json string
				Gson gson = new Gson();
				FirmJsonUtility firmJsonUtility = null;
				PracticeJsonUtility practiceJsonUtility = null;
				List<FirmJsonUtility> firmJsonUtilityList = new ArrayList<FirmJsonUtility>();
				List<PracticeJsonUtility> practiceJsonUtilityList = new ArrayList<PracticeJsonUtility>();
				
				//fetching all firms information from DB to show under firms pop up
				List<Firm> firmsSet = firmService.getAllFirms();
				//fetching all Practice area information from DB to show under firms pop up
				List<Practice> practiceList = firmService.getAllPractices();
				List<UserGroupDTO> allWatchLists = null;

				//Getting logged in user
				User currentUser = WebUtil.getCurrentUser(request, userService);
				if (currentUser != null)
				{
					allWatchLists = (List<UserGroupDTO>) watchlistService.getAllFirmWatchList(currentUser.getId());
				}
				//creating firm json list for firm Auto completer
				//converting List<Firm> to List<FirmJsonUtility> as FirmJsonUtility utility class have attributes(id,label,category)  
				//which is required for Firms Autocompleter/Type ahead
				for (Firm firm : firmsSet)
				{
					firmJsonUtility = new FirmJsonUtility();
					firmJsonUtility.setId(firm.getCompanyId().toString());
					firmJsonUtility.setLabel(firm.getCompany());
					firmJsonUtility.setCategory("Single Firms");
					firmJsonUtilityList.add(firmJsonUtility);
				}
				
				//creating Practice area json list for Practice area Auto completer
				//converting List<Practice> to List<PracticeJsonUtility> as PracticeJsonUtility utility class have attribute(label)
				//which is required for Firms Autocompleter/Type ahead
				for (Practice practice : practiceList)
				{
					practiceJsonUtility = new PracticeJsonUtility();
					practiceJsonUtility.setLabel(practice.getPracticeArea());
					practiceJsonUtilityList.add(practiceJsonUtility);
				}
				//converting all firms to JSON string
				String jsonAllFirms = gson.toJson(firmService.getRivalEdgeList(firmJsonUtilityList));
				
				//converting all Practices to JSON string
				String jsonAllPractics = gson.toJson(practiceJsonUtilityList);
				
				
				List<UserGroup> allWatchListsDefaultList = null;
				if (currentUser != null)
				{
					allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
					if (allWatchListsDefaultList.size() == 0)
					{
						model.addAttribute("allWatchListsDefaultList", "");
					}
					else
					{
						model.addAttribute("allWatchListsDefaultList", allWatchListsDefaultList.get(0));
					}
				}
				
				model.addAttribute("firmJson", jsonAllFirms);
				model.addAttribute("practiceJson", jsonAllPractics);
				model.addAttribute("allFirms", firmsSet);
				model.addAttribute("allPracticeArea", practiceList);
				model.addAttribute("allWatchLists", allWatchLists);
				model.addAttribute("allOtherLocations", ALMConstants.LOCATIONS);
				model.addAttribute("allRankingList", ALMConstants.RIVALEDGE_RANKING_LIST);	
		
		return "attorneymovesandchangessearch";
	}
	
	
	@ResourceMapping("AttorneySearchURL")
	public String getAttorneySearchFilters(@ModelAttribute("alertAttorneySearchModelBean") AttorneySearchModelBean alertAttorneySearchModelBean, 
			 ModelMap model, ResourceRequest request, ResourceResponse response)
	{
		// people attorney
		System.out.println("\n\n\n/////////// GOT IN : AttorneySearchURL \n\n\n");
		
		Gson 		 gson 		= new Gson();
		List<String> fromYear 	= new ArrayList<String>();
		List<String> toYear 	= new ArrayList<String>();
		setYearRange(fromYear, toYear);
		
		FirmJsonUtility 			firmJsonUtility 		= null;
		PracticeJsonUtility 		practiceJsonUtility 	= null;
		List<UserGroupDTO> 			allWatchLists 			= null;
		List<FirmJsonUtility> 		firmJsonUtilityList 	= new ArrayList<FirmJsonUtility>();
		List<PracticeJsonUtility> 	practiceJsonUtilityList	= new ArrayList<PracticeJsonUtility>();
		List<Firm> 					firmsSet 				= firmService.getAllFirms();
		List<Practice> 				practiceList 			= firmService.getAllPractices();
		User 						currentUser 			= (model.get("currentUser") != null) ? ((User) model.get("currentUser")) : WebUtil.getCurrentUser(request, userService); 
		
		if (currentUser != null)
		{
			allWatchLists = (List<UserGroupDTO>) watchlistService.getAllFirmWatchList(currentUser.getId());
		}	
		
		// creating firm json list for firm Auto completer
		for (Firm firm : firmsSet)
		{
			firmJsonUtility = new FirmJsonUtility();
			firmJsonUtility.setId(firm.getCompanyId().toString());
			firmJsonUtility.setLabel(firm.getCompany());
			firmJsonUtility.setCategory("Single Firms");
			firmJsonUtilityList.add(firmJsonUtility);
		}

		// creating Practice area json list for Practice area Auto completer
		for (Practice practice : practiceList)
		{
			practiceJsonUtility = new PracticeJsonUtility();
			practiceJsonUtility.setLabel(practice.getPracticeArea());
			practiceJsonUtilityList.add(practiceJsonUtility);
		}
		
		// converting all firms to JSON string
		String jsonAllFirms = gson.toJson(firmService.getRivalEdgeList(firmJsonUtilityList));
		// converting all Practices to JSON string
		String jsonAllPractics = gson.toJson(practiceJsonUtilityList);
		
		List<UserGroup> allWatchListsDefaultList = null;
		if (currentUser != null)
		{
			allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
			if (allWatchListsDefaultList.size() == 0)
			{
				model.addAttribute("allWatchListsDefaultList", "");
			}
			else
			{
				model.addAttribute("allWatchListsDefaultList", allWatchListsDefaultList.get(0));
			}
		}
		
		//populate Models
		model.addAttribute("firmJson", jsonAllFirms);
		model.addAttribute("titlesJson", gson.toJson(ALMConstants.ALL_TITLES_LIST));
		model.addAttribute("allFirms", firmsSet);
		model.addAttribute("practiceJson", jsonAllPractics);
		model.addAttribute("fromYear", fromYear);
		model.addAttribute("toYear", toYear);
		model.addAttribute("allPracticeArea", practiceList);
		model.addAttribute("allWatchLists", allWatchLists);
		model.addAttribute("allOtherLocations", ALMConstants.LOCATIONS);
		model.addAttribute("allRankingList", ALMConstants.RIVALEDGE_RANKING_LIST);
		model.addAttribute("allDisplayColumns", ALMConstants.ATTORNEY_DISPLAY_COLUMNS.values());
		
		return "attorneysearch";
	}

	@ResourceMapping("EventURL")
	public String getEventFilters(@ModelAttribute("alertEventSearchModelBean") EventSearchModelBean alertEventSearchModelBean, 
			 ModelMap model, ResourceRequest request, ResourceResponse response)
	{
		// Events
		System.out.println("\n\n\n/////////// GOT IN : EventURL \n\n\n");
		
		Gson 						gson 					= new Gson();
		FirmJsonUtility 			firmJsonUtility 		= null;
		PracticeJsonUtility 		practiceJsonUtility 	= null;
		List<UserGroupDTO> 			allWatchLists 			= null;
		List<UserGroup> 			allWatchListsAutoComplete 			= null;
		List<FirmJsonUtility> 		firmJsonUtilityList 	= new ArrayList<FirmJsonUtility>();
		List<LocationJsonUtility> 	locationJsonUtilityList = new ArrayList<LocationJsonUtility>();
		List<PracticeJsonUtility> 	practiceJsonUtilityList = new ArrayList<PracticeJsonUtility>();
		List<Firm> 					firmsSet 				= firmService.getAllFirms();
		List<Firm> 					orgList 				= firmService.getOrganizationsList();
		List<Practice> 				practiceList 			= firmService.getAllPractices();
		User						currentUser				= WebUtil.getCurrentUser(request, userService);
		
		if (currentUser != null)
		{
			allWatchLists = (List<UserGroupDTO>) watchlistService.getAllFirmWatchList(currentUser.getId());
			allWatchListsAutoComplete = (List<UserGroup>) watchlistService.getAllWatchListForAutoComplete(currentUser.getId());
			
			for (UserGroup userGroup : allWatchListsAutoComplete)
			{
				firmJsonUtility = new FirmJsonUtility();
				firmJsonUtility.setId(String.valueOf(userGroup.getGroupId()));
				firmJsonUtility.setLabel(userGroup.getGroupName());
				firmJsonUtility.setCategory("WatchLists");
				firmJsonUtilityList.add(firmJsonUtility);
			}
		}	
		
		for (Firm firm : firmsSet)
		{
			firmJsonUtility = new FirmJsonUtility();
			firmJsonUtility.setId(firm.getCompanyId().toString());
			firmJsonUtility.setLabel(firm.getCompany());
			firmJsonUtility.setCategory("Single Firms");
			firmJsonUtilityList.add(firmJsonUtility);
		}
		
		for (Firm firm : orgList)
		{
			firmJsonUtility = new FirmJsonUtility();
			firmJsonUtility.setId(firm.getCompanyId().toString());
			firmJsonUtility.setLabel(firm.getCompany());
			firmJsonUtility.setCategory("Organizations");
			firmJsonUtilityList.add(firmJsonUtility);
		}

		for (Practice practice : practiceList)
		{
			practiceJsonUtility = new PracticeJsonUtility();
			practiceJsonUtility.setLabel(practice.getPracticeArea());
			practiceJsonUtilityList.add(practiceJsonUtility);
		}
		// converting all firms to JSON string
		String jsonAllFirms = gson.toJson(firmService.getRivalEdgeList(firmJsonUtilityList));
		// converting all Practices to JSON string
		String jsonAllPractics = gson.toJson(practiceJsonUtilityList);

		TreeMap<String,String> locationsMapValues = ALMConstants.getAllLocationsConstant();
		
		for (Map.Entry<String, String> entry : locationsMapValues.entrySet())
		{
			LocationJsonUtility locationJsonUtility = new LocationJsonUtility();
			locationJsonUtility.setId(entry.getValue());
			locationJsonUtility.setLabel(entry.getKey());
			locationJsonUtilityList.add(locationJsonUtility);
		}
		// converting all locations to JSON string
		String jsonAllLocations = gson.toJson(locationJsonUtilityList);
		model.addAttribute("allOtherLocations", jsonAllLocations);
		model.addAttribute("locationsMapValues", locationsMapValues);
		
		model.addAttribute("firmJson", jsonAllFirms);
		model.addAttribute("practiceJson", jsonAllPractics);
		model.addAttribute("allFirms", firmsSet);
		model.addAttribute("allPracticeArea", practiceList);
		model.addAttribute("allWatchLists", allWatchLists);
		model.addAttribute("orgList", orgList);

		List<UserGroup> allWatchListsDefaultList = null;
		if (currentUser != null)
		{
			allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
			if (allWatchListsDefaultList.size() == 0)
			{
				model.addAttribute("allWatchListsDefaultList", "");
			}
			else
			{
				model.addAttribute("allWatchListsDefaultList", allWatchListsDefaultList.get(0));
			}
		}

		return "eventform";
	}
	
	@ActionMapping(params = "action=addAlertUserEmail")
	protected void addAlertUserEmail(ModelMap model, ActionRequest request, ActionResponse response) throws Exception
	{
		String userIdStr = request.getParameter("txtAlertUserEmailUserId");
		String personName = request.getParameter("txtAlertUserPersonName");
		String emailId = request.getParameter("txtAlertUserEmail");
		String idStr = request.getParameter("txtAlertUserEmailId");
		int id = 0;
		boolean isEdit = false;
		if(idStr!=null && idStr.length()>0){
			id = Integer.parseInt(idStr);
			isEdit = true;
		}
		RERAlertUserEmail obj = new RERAlertUserEmail();
		obj.setEmailId(emailId);
		obj.setId(id);
		obj.setPersonName(personName);
		obj.setUserId(Integer.parseInt(userIdStr));
		if(isEdit){
			reralertService.updateAlertEmail(obj);
		}else{
			// Get the current user from the render request
			User currentUser = WebUtil.getCurrentUser(request, userService);
			
			// Get all the alerts of current user just to extract email ids and userid
			List<RERAlertUserEmail> resultsDB = reralertService.findAllUserEmailIds(currentUser.getId());
			
			boolean isPresent = false;
			
			for(RERAlertUserEmail email: resultsDB){
				if(obj.getEmailId().equalsIgnoreCase(email.getEmailId())){
					response.setRenderParameter("error", "Email already exists!!");
					isPresent = true;
				}
			}
			if(!isPresent){
				reralertService.insertUserEmails(obj);
			}
		}
		response.setRenderParameter("page", "emails");
	}
	
	@ActionMapping(params = "action=deleteAlertUserEmail")
	protected void deleteAlertUserEmail(ModelMap model, ActionRequest request, ActionResponse response) throws Exception
	{
		String idStr = request.getParameter("txtAlertUserEmailId");
		response.setRenderParameter("page", "emails");
		reralertService.deleteUserEmails(Integer.parseInt(idStr));
	}
	@ActionMapping(params = "action=deleteAlert")
	protected void deleteAlert(ModelMap model, ActionRequest request, ActionResponse response) throws Exception
	{
		
		String idStr = request.getParameter("alertUserId");
		reralertService.deleteAlertUserById(Integer.parseInt(idStr));
		response.setRenderParameter("page", "");
	}
	
	public List<RERAlertUserEmail> getUserEmails(User currentUser){
		
		List<RERAlertUserEmail> results = new ArrayList<RERAlertUserEmail>();
		
		RERAlertUserEmail defaultEmailObj = new RERAlertUserEmail();
		defaultEmailObj.setEmailId(currentUser.getEmail());
		defaultEmailObj.setPrimary(true);
		defaultEmailObj.setUserId(currentUser.getId());
		defaultEmailObj.setPersonName(currentUser.getFirstname() + " " + currentUser.getLastname());
		
		results.add(defaultEmailObj);
		
		// Get all the alerts of current user just to extract email ids and userid
		List<RERAlertUserEmail> resultsDB = reralertService.findAllUserEmailIds(currentUser.getId());
		
		if(resultsDB!= null && resultsDB.size()>0){
			results.addAll(resultsDB);
		}
		
		return results;
	}
	
	@RenderMapping(params = "page=emails")
	protected ModelAndView renderEmailsView(ModelMap model, RenderRequest request, RenderResponse response) throws Exception
	{	
		
		// Get the current user from the render request
		User currentUser = WebUtil.getCurrentUser(request, userService);
		
		List<RERAlertUserEmail> results = getUserEmails(currentUser);
		model.addAttribute("userEmails", results);
		model.addAttribute("userId", currentUser.getId());
		
		return new ModelAndView("emails", model);
	}
	
	@RenderMapping
	protected ModelAndView defaultRenderView(ModelMap model, RenderRequest request, RenderResponse response) throws Exception
	{	
		
		// Get the current user from the render request
		User currentUser = WebUtil.getCurrentUser(request, userService);
		
		// Get all the alerts of current user
		List<RERAlert> results = reralertService.findAllUserAlertsByUserId(currentUser.getId());
		
		if(results!= null && results.size()>0){
			
		}else{
			results = new ArrayList<RERAlert>();
		}
		
		int countActive = 0;
		for(RERAlert alert : results){
			if(alert.getIsActive()>0){
				countActive ++;
			}
		}
		
		String alertStatusStr = "";
		if(results.size()>1){
			alertStatusStr = results.size() +" Alerts - "+ countActive +" Active " +(results.size() - countActive ) +" Inactive";
		}else{
			alertStatusStr = results.size() +" Alert - "+ countActive +" Active " +(results.size() - countActive ) +" Inactive";
		}
		
		model.addAttribute("alertStatusStr", alertStatusStr);
		model.addAttribute("userAlerts", results);
		
		List<RERAlertUserEmail> resultsEmails = getUserEmails(currentUser);
		model.addAttribute("userEmails", resultsEmails);
		
		return new ModelAndView("alerts", model);
	}
	
	@ActionMapping(params = "action=saveFirmsFilters")
	protected void saveFirmsFilters(@ModelAttribute("alertfirmSearchModelBean") FirmSearchModelBean alertfirmSearchModelBean,
			ActionRequest request, ActionResponse response) throws Exception
	{
		this.setNewsPubsBean(alertfirmSearchModelBean, request, response);
		String searchjson = WebUtil.getJson(alertfirmSearchModelBean);
		checkAndSaveAlert(searchjson, request, response);
		printAlertData(request);
	}
	@ActionMapping(params = "action=saveFirmsStatsFilters")
	protected void saveFirmsStatsFilters(@ModelAttribute("alertPeopleSearchModelBean") PeopleSearchModelBean alertPeopleSearchModelBean,
			ActionRequest request, ActionResponse response) throws Exception
	{
		this.setFirmStatsBean(alertPeopleSearchModelBean, request, response);
		String searchjson = WebUtil.getJson(alertPeopleSearchModelBean);
		checkAndSaveAlert(searchjson, request, response);
		printAlertData(request);
	}
	@ActionMapping(params = "action=saveAttorneyMovesFilters")
	protected void saveAttorneyMovesFilters(@ModelAttribute("alertAttorneyMovesSearchModelBean") AttorneyMoveSearchModelBean alertAttorneyMovesSearchModelBean,
			ActionRequest request, ActionResponse response) throws Exception
	{
		this.setAttorneyMovesBean(alertAttorneyMovesSearchModelBean, request, response);
		String searchjson = WebUtil.getJson(alertAttorneyMovesSearchModelBean);
		checkAndSaveAlert(searchjson, request, response);
		printAlertData(request);
	}
	@ActionMapping(params = "action=saveAttorneySearchFilters")
	protected void saveAttorneySearchFilters(@ModelAttribute("alertAttorneySearchModelBean") AttorneySearchModelBean alertAttorneySearchModelBean,
			ActionRequest request, ActionResponse response) throws Exception
	{
		this.setAttorneyBean(alertAttorneySearchModelBean, request, response);
		String searchjson = WebUtil.getJson(alertAttorneySearchModelBean);
		checkAndSaveAlert(searchjson, request, response);
		printAlertData(request);
	}
	@ActionMapping(params = "action=saveEventFilters")
	protected void saveEventFilters(@ModelAttribute("alertEventSearchModelBean") EventSearchModelBean alertEventSearchModelBean,
			ModelMap model, ActionRequest request, ActionResponse response) throws Exception
	{
		this.setEventBean(alertEventSearchModelBean, request, response);
		String searchjson = WebUtil.getJson(alertEventSearchModelBean);
		checkAndSaveAlert(searchjson, request, response);
		printAlertData(request);
	}
	private void setFirmStatsBean(@ModelAttribute("alertPeopleSearchModelBean") PeopleSearchModelBean peopleSearchModelBean,
			ActionRequest request, ActionResponse response){
		
		String firmType = peopleSearchModelBean.getFirmType();
		List<Firm> firmsList = null;
		List<Firm> firmListWatchList = new ArrayList<Firm>();
		List<String> firmsListForResults = new ArrayList<String>();

		if (ALMConstants.ALL_FIRMS.equals(firmType))
		{
			firmsList = new ArrayList<Firm>();
			firmsList.addAll(firmService.getAllFirms());
			firmsListForResults.add(firmType);
			peopleSearchModelBean.setFirmListWatchList(null);
			peopleSearchModelBean.setFirmList(null);
		}
		else if (ALMConstants.WATCH_LIST.equals(firmType))
		{
			firmsList = new ArrayList<Firm>();
			User currentUser = WebUtil.getCurrentUser(request, userService);
			String 	watchListsName = request.getParameter("Firmstext");
			
			List<Firm> firmsWatchList = null;
			if (currentUser != null)
			{
				firmsWatchList = watchlistService.findAllFirmsInManyWatchlists(peopleSearchModelBean.getFirmListWatchList(),
						currentUser.getId());
			}

			firmListWatchList.addAll(firmsWatchList);
			firmsList.addAll(firmsWatchList);
			/*
			for (Firm firmValue : firmsWatchList)
			{
				if (firmValue != null)
				{
					firmsListForResults.add(firmValue.getCompany());
				}
			}*/

			if (firmsWatchList.size() == 0)
			{
				List<Firm> notAvailableFirms = new ArrayList<Firm>();
				Firm tempFirm = new Firm();
				tempFirm.setCompanyId(0);
				notAvailableFirms.add(tempFirm);
			}
			else
			{
			}
			peopleSearchModelBean.setFirmList(null);

			if(watchListsName == null)
			{
				List<UserGroup> allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
				if(allWatchListsDefaultList != null)
				{
					firmsListForResults.add(allWatchListsDefaultList.get(0).getGroupName());
				}
			}
			else
			{
				firmsListForResults.add(watchListsName);
			}
		}
		else if (ALMConstants.RIVALEDGE_LIST.equals(firmType))
		{
			firmsList = new ArrayList<Firm>();
			firmsList.addAll(firmService.getRanking(peopleSearchModelBean.getFirmList().get(0)));
			firmsListForResults.add(peopleSearchModelBean.getFirmList().get(0));
			peopleSearchModelBean.setFirmListWatchList(null);
		}
		else
		{ // User selected individual values
			firmsList = new ArrayList<Firm>();
			for (String firmId : peopleSearchModelBean.getFirmList())
			{
				try
				{
					Firm firmObject = firmService.getFirmById(Integer.parseInt(firmId));
					firmsList.add(firmObject);
					firmsListForResults.add(firmObject.getCompany());
				}
				catch (NumberFormatException nfEx)
				{
					// Do nothing.
				}
			}

			peopleSearchModelBean.setFirmListWatchList(null);
		}

		peopleSearchModelBean.setSelectedFirms(firmsListForResults.toString().replace("[", "").replace("]", ""));
		
		
		
		// Other Parameter
		peopleSearchModelBean.setSelectedLocations(peopleSearchModelBean.getLocations().toString().replace("[", "").replace("]", ""));
		peopleSearchModelBean.setSelectedPracticeArea(peopleSearchModelBean.getPracticeArea().toString().replace("[", "").replace("]", ""));
		peopleSearchModelBean.setSelectedFirmSize(peopleSearchModelBean.getFirmSizeList().toString().replace("[", "").replace("]", ""));
	}
	private void setNewsPubsBean(@ModelAttribute("alertfirmSearchModelBean") FirmSearchModelBean firmSearchModelBean,
			ActionRequest request, ActionResponse response){
		String 			firmType 			= firmSearchModelBean.getFirmType();
		List<Firm> 		firmsList 			= new ArrayList<Firm>();
		List<String> 	firmsListForResults	= new ArrayList<String>();
		
		if (ALMConstants.ALL_FIRMS.equals(firmType))
		{
			firmsList.addAll(firmService.getAllFirms());
			firmsListForResults.add(firmType);
			firmSearchModelBean.setFirmListWatchList(null);
			firmSearchModelBean.setFirmList(null);
		}
		else if (ALMConstants.WATCH_LIST.equals(firmType))
		{
			List<Firm> 		firmsWatchList 	= null;
			List<UserGroup> allWatchListsDefaultList = null;
			String 			watchListsName = request.getParameter("Firmstext");
			User 			currentUser	= WebUtil.getCurrentUser(request, userService);
			
			if (currentUser != null)
			{
				firmsWatchList = watchlistService.findAllFirmsInManyWatchlists(firmSearchModelBean.getFirmListWatchList(), currentUser.getId());
			}
			
			if (firmsWatchList.size() == 0)
			{
				List<Firm> notAvailableFirms = new ArrayList<Firm>();
				Firm tempFirm = new Firm();
				tempFirm.setCompanyId(-1);
				notAvailableFirms.add(tempFirm);
			}
			firmSearchModelBean.setFirmList(null);
			if(watchListsName == null)
			{
				allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
				if(allWatchListsDefaultList != null)
				{
					firmsListForResults.add(allWatchListsDefaultList.get(0).getGroupName());
				}
			}
			else
			{
				firmsListForResults.add(watchListsName);
			}
		}
		else if (ALMConstants.RIVALEDGE_LIST.equals(firmType))
		{
			firmsList.addAll(firmService.getRanking(firmSearchModelBean.getFirmList().get(0)));
			firmsListForResults.add(firmSearchModelBean.getFirmList().get(0));
			firmSearchModelBean.setFirmListWatchList(null);
		}
		else
		{
			for (String firmId : firmSearchModelBean.getFirmList())
			{
				Firm firmObject = firmService.getFirmById(Integer.parseInt(firmId));
				firmsList.add(firmObject);
				firmsListForResults.add(firmObject.getCompany());
			}
			firmSearchModelBean.setFirmListWatchList(null);
		}
		firmSearchModelBean.setSelectedFirms(firmsListForResults.toString().replace("[", "").replace("]", ""));
		
		
		// Other parameters
		firmSearchModelBean.setSelectedContentType(firmSearchModelBean.getContentType().toString().replace("[", "").replace("]", ""));
		firmSearchModelBean.setSelectedTwitterPostType(firmSearchModelBean.getTwitterType().toString().replace("[", "").replace("]", ""));
		firmSearchModelBean.setSelectedPracticeArea(firmSearchModelBean.getPracticeArea().toString().replace("[", "").replace("]", ""));
	}
	
	private void setAttorneyMovesBean(@ModelAttribute("alertAttorneyMovesSearchModelBean") AttorneyMoveSearchModelBean attorneyMoveSearchModelBean,
			ActionRequest request, ActionResponse response){
		String 			firmType 			= attorneyMoveSearchModelBean.getFirmType();
		List<Firm> 		firmsList 			= new ArrayList<Firm>();
		List<Firm> 		firmListWatchList 	= new ArrayList<Firm>();
		List<String>	firmsListForResults = new ArrayList<String>();
		User 			currentUser 		=  WebUtil.getCurrentUser(request, userService); 
		
		if (firmType != null && !"".equals(firmType))
		{
			if (firmType.equals(ALMConstants.ALL_FIRMS))
			{
				//set the firms filter to null as user has selected 'All Firms'
				firmsListForResults.add(firmType);
				attorneyMoveSearchModelBean.setFirmListWatchList(null);
			}
			
			
			else if (ALMConstants.WATCH_LIST.equals(firmType))
			{
				List<Firm> firmsWatchList = null;
				if (currentUser != null)
				{
					firmsWatchList = watchlistService.findAllFirmsInManyWatchlists(attorneyMoveSearchModelBean.getFirmListWatchList(), currentUser.getId());
				}
				
				firmListWatchList.addAll(firmsWatchList);
				firmsList.addAll(firmsWatchList);

				for (Firm firmValue : firmsWatchList)
				{
					if (firmValue != null)
					{
						firmsListForResults.add(firmValue.getCompany());
					}
				}
				
				if (firmsWatchList.size() == 0)
				{
					List<Firm> 	notAvailableFirms 	= new ArrayList<Firm>();
					Firm 		tempFirm 			= new Firm();
					tempFirm.setCompanyId(0);
					notAvailableFirms.add(tempFirm);
				}
				attorneyMoveSearchModelBean.setFirmList(null);
				
			}
			else if (ALMConstants.RIVALEDGE_LIST.equals(firmType))
			{
				firmsList.addAll(firmService.getRanking(attorneyMoveSearchModelBean.getFirmList().get(0)));
				firmsListForResults.add(attorneyMoveSearchModelBean.getFirmList().get(0));
				attorneyMoveSearchModelBean.setFirmListWatchList(null);
			}
			else
			{
//				for (String firmId : attorneyMoveSearchModelBean.getFirmList())
//				{
//					attorneyMoveSearchModelBean.setFirmListWatchList(null);
//				}
				attorneyMoveSearchModelBean.setFirmListWatchList(null);
				//attorneyMoveChangesSearchDTO.setSelectedFirms(firmsList);
				attorneyMoveSearchModelBean.setSelectedFirms(firmsListForResults.toString().replace("[", "").replace("]", ""));
			}
		}
		
		// Other parameters
		if (attorneyMoveSearchModelBean.getLocations() != null
				&& !attorneyMoveSearchModelBean.getLocations().contains(ALMConstants.ALL_LOCATIONS))
		{
			attorneyMoveSearchModelBean.setSelectedLocation(attorneyMoveSearchModelBean.getLocations().toString().replace("[", "").replace("]", ""));
		}
		if (attorneyMoveSearchModelBean.getPracticeArea() != null
				&& !attorneyMoveSearchModelBean.getPracticeArea().contains(ALMConstants.ALL_PRACTICE_AREAS))
		{
			attorneyMoveSearchModelBean.setSelectedPracticeArea(attorneyMoveSearchModelBean.getPracticeArea().toString().replace("[", "").replace("]", ""));
		}
		
		if (attorneyMoveSearchModelBean.getChangeType() != null && !attorneyMoveSearchModelBean.getChangeType().contains(ALMConstants.ALL_MOVES_CHANGES))
		{
			attorneyMoveSearchModelBean.setSelectedChangeTypes(attorneyMoveSearchModelBean.getChangeType().toString().replace("[", "").replace("]", ""));	
		}
		
		if (attorneyMoveSearchModelBean.getTitles() != null && !attorneyMoveSearchModelBean.getTitles().contains(ALMConstants.ALL_TITLES))
		{
			attorneyMoveSearchModelBean.setSelectedTitles(attorneyMoveSearchModelBean.getTitles().toString().replace("[", "").replace("]", ""));	
		} 	
		
		
		attorneyMoveSearchModelBean.setSelectedChangeTypes(attorneyMoveSearchModelBean.getChangeType().toString().replace("[", "").replace("]", ""));
		attorneyMoveSearchModelBean.setSelectedLocation(attorneyMoveSearchModelBean.getLocations().toString().replace("[", "").replace("]", ""));
		attorneyMoveSearchModelBean.setSelectedPracticeArea(attorneyMoveSearchModelBean.getPracticeArea().toString().replace("[", "").replace("]", ""));
		attorneyMoveSearchModelBean.setSelectedTitles(attorneyMoveSearchModelBean.getTitles().toString().replace("[", "").replace("]", ""));
	}
	
	private void setAttorneyBean(@ModelAttribute("alertAttorneySearchModelBean") AttorneySearchModelBean attorneySearchModelBean,
			ActionRequest request, ActionResponse response){
		String 			firmType 			= attorneySearchModelBean.getFirmType();
		List<Firm> 		firmsList 			= new ArrayList<Firm>();
		List<String> 	firmsListForResults	= new ArrayList<String>();
		
		if (ALMConstants.ALL_FIRMS.equals(firmType))
		{
			firmsList.addAll(firmService.getAllFirms());
			firmsListForResults.add(firmType);
			attorneySearchModelBean.setFirmListWatchList(null);
			attorneySearchModelBean.setFirmList(null);
		}
		else if (ALMConstants.WATCH_LIST.equals(firmType))
		{
			List<Firm> 		firmsWatchList 	= null;
			List<UserGroup> allWatchListsDefaultList = null;
			String 			watchListsName = request.getParameter("Firmstext");
			User 			currentUser	= WebUtil.getCurrentUser(request, userService);
			
			if (currentUser != null)
			{
				firmsWatchList = watchlistService.findAllFirmsInManyWatchlists(attorneySearchModelBean.getFirmListWatchList(), currentUser.getId());
			}
			
			if (firmsWatchList.size() == 0)
			{
				List<Firm> notAvailableFirms = new ArrayList<Firm>();
				Firm tempFirm = new Firm();
				tempFirm.setCompanyId(-1);
				notAvailableFirms.add(tempFirm);
			}
			attorneySearchModelBean.setFirmList(null);
			if(watchListsName == null)
			{
				allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
				if(allWatchListsDefaultList != null)
				{
					firmsListForResults.add(allWatchListsDefaultList.get(0).getGroupName());
				}
			}
			else
			{
				firmsListForResults.add(watchListsName);
			}
		}
		else if (ALMConstants.RIVALEDGE_LIST.equals(firmType))
		{
			firmsList.addAll(firmService.getRanking(attorneySearchModelBean.getFirmList().get(0)));
			firmsListForResults.add(attorneySearchModelBean.getFirmList().get(0));
			attorneySearchModelBean.setFirmListWatchList(null);
		}
		else
		{
			for (String firmId : attorneySearchModelBean.getFirmList())
			{
				Firm firmObject = firmService.getFirmById(Integer.parseInt(firmId));
				firmsList.add(firmObject);
				firmsListForResults.add(firmObject.getCompany());
			}
			attorneySearchModelBean.setFirmListWatchList(null);
		}
		attorneySearchModelBean.setSelectedFirms(firmsListForResults.toString().replace("[", "").replace("]", ""));
		
		// Other Parameters
		attorneySearchModelBean.setSelectedPracticeArea(attorneySearchModelBean.getPracticeArea().toString().replace("[", "").replace("]", ""));
		attorneySearchModelBean.setSelectedLocation(attorneySearchModelBean.getLocations().toString().replace("[", "").replace("]", ""));
		attorneySearchModelBean.setSelectedTitles(attorneySearchModelBean.getTitle().toString().replace("[", "").replace("]", ""));
		
		String fromDateText = attorneySearchModelBean.getFromDate();
		String toDateText = attorneySearchModelBean.getToDate();
		if (ALMConstants.ANY.equals(fromDateText) && ALMConstants.ANY.equals(toDateText)) 
		{
			attorneySearchModelBean.setFromDate(null);
			attorneySearchModelBean.setToDate(null);
		}
		else
		{
			attorneySearchModelBean.setFromDate(attorneySearchModelBean.getFromDate());
			attorneySearchModelBean.setToDate(attorneySearchModelBean.getToDate());
		}
	}
	
	private void setEventBean(@ModelAttribute("alertEventSearchModelBean") EventSearchModelBean eventSearchModelBean,
			ActionRequest request, ActionResponse response){
		String 			firmType 			= eventSearchModelBean.getFirmType();
		List<Firm> 		firmsList 			= new ArrayList<Firm>();
		List<String> 	firmsListForResults	= new ArrayList<String>();
		
		if (ALMConstants.ALL_FIRMS.equals(firmType))
		{
			firmsList.addAll(firmService.getAllFirms());
			firmsListForResults.add(firmType);
			eventSearchModelBean.setFirmListWatchList(null);
			eventSearchModelBean.setFirmList(null);
		}
		else if (ALMConstants.ALL_ORGANIZATIONS.equals(firmType))
		{
			firmsList.addAll(firmService.getOrganizationsList());
			firmsListForResults.add(firmType);
			eventSearchModelBean.setFirmListWatchList(null);
			eventSearchModelBean.setFirmList(null);
		}
		else if (ALMConstants.ALL_FIRMS_ORGANIZATION.equals(firmType))
		{
			firmsList.addAll(firmService.getAllFirms());
			firmsList.addAll(firmService.getOrganizationsList());
			firmsListForResults.add(firmType);
			eventSearchModelBean.setFirmListWatchList(null);
			eventSearchModelBean.setFirmList(null);
		}
		else if (ALMConstants.WATCH_LIST.equals(firmType))
		{
			List<Firm> 		firmsWatchList 	= null;
			List<UserGroup> allWatchListsDefaultList = null;
			String 			watchListsName = request.getParameter("Firmstext");
			User 			currentUser	= WebUtil.getCurrentUser(request, userService);
			
			if (currentUser != null)
			{
				firmsWatchList = watchlistService.findAllFirmsInManyWatchlists(eventSearchModelBean.getFirmListWatchList(), currentUser.getId());
			}
			
			if (firmsWatchList.size() == 0)
			{
				List<Firm> notAvailableFirms = new ArrayList<Firm>();
				Firm tempFirm = new Firm();
				tempFirm.setCompanyId(-1);
				notAvailableFirms.add(tempFirm);
			}
			eventSearchModelBean.setFirmList(null);
			if(watchListsName == null)
			{
				allWatchListsDefaultList = firmService.getWatchListSortedByDate(currentUser.getId());
				if(allWatchListsDefaultList != null)
				{
					firmsListForResults.add(allWatchListsDefaultList.get(0).getGroupName());
				}
			}
			else
			{
				firmsListForResults.add(watchListsName);
			}
		}
		else if (ALMConstants.RIVALEDGE_LIST.equals(firmType))
		{
			firmsList.addAll(firmService.getRanking(eventSearchModelBean.getFirmList().get(0)));
			firmsListForResults.add(eventSearchModelBean.getFirmList().get(0));
			eventSearchModelBean.setFirmListWatchList(null);
		}
		else
		{
			for (String firmId : eventSearchModelBean.getFirmList())
			{
				Firm firmObject = firmService.getFirmById(Integer.parseInt(firmId));
				firmsList.add(firmObject);
				firmsListForResults.add(firmObject.getCompany());
			}
			eventSearchModelBean.setFirmListWatchList(null);
		}
		eventSearchModelBean.setSelectedFirms(firmsListForResults.toString().replace("[", "").replace("]", ""));
		
		// Other parameters
		String locationString = null;
		if(eventSearchModelBean.getLocations() != null)
		{
			locationString = eventSearchModelBean.getLocations().toString().replace("[", "").replace("]", "");
		}
		final String WEBINAR = "Webinar"; 
		final String INPERSON = "In Person";
		
		if(locationString != null && !(ALMConstants.ALL_LOCATIONS.equals(locationString) || WEBINAR.equals(locationString) || INPERSON.equals(locationString)))
		{
			List<String> locationDisplayLabel = eventSearchModelBean.getLocations();
			String allLocs = "";
			//Convert selected location into location label name 
			for(String location : locationDisplayLabel)
			{
				allLocs += location.substring(3, location.length());
				allLocs += ",";
			}
			eventSearchModelBean.setSelectedLocation(allLocs.substring(0, allLocs.length()-1));
		}
		else
		{
			eventSearchModelBean.setSelectedLocation(locationString);
		}
		eventSearchModelBean.setSelectedPracticeArea(eventSearchModelBean.getPracticeArea().toString().replace("[", "").replace("]", ""));
	}
	
	@ResourceMapping("getAlertById")
	protected String getAlertById(ModelMap model, ResourceRequest request, ResourceResponse response) throws Exception
	{
		String alertId = request.getParameter("alertId");
		String alertUserId = request.getParameter("alertUserId");
		RERAlert rerAlert = null;
		String json = "";
		if(alertId.length()>0){
			rerAlert = reralertService.findRERAlertByID(Integer.parseInt(alertId));
			json = rerAlert.getSearchJson();
		}
		
		String jsp = "";
		
		if(rerAlert!=null){
			String type = rerAlert.getAlertType();
			
			if(ALMConstants.NEWS_AND_PUBS.equalsIgnoreCase(type))
			{
				FirmSearchModelBean alertfirmSearchModelBean = (FirmSearchModelBean) WebUtil.getObject(json, FirmSearchModelBean.class);
				jsp = getFirmsFilters(alertfirmSearchModelBean, model, request, response);
				model.addAttribute("alertfirmSearchModelBean", alertfirmSearchModelBean);
			}
			else if(ALMConstants.PEOPLE_FIRM_STAT.equalsIgnoreCase(type))
			{
				PeopleSearchModelBean alertPeopleSearchModelBean = (PeopleSearchModelBean) WebUtil.getObject(json, PeopleSearchModelBean.class);
				jsp = getFirmsStatsFilters(alertPeopleSearchModelBean, model, request, response);
				model.addAttribute("alertPeopleSearchModelBean", alertPeopleSearchModelBean);
			}
			else if(ALMConstants.PEOPLE_ATTORNEY_MOVES.equalsIgnoreCase(type))
			{
				AttorneyMoveSearchModelBean alertAttorneyMovesSearchModelBean = (AttorneyMoveSearchModelBean) WebUtil.getObject(json, AttorneyMoveSearchModelBean.class);
				jsp = getAttorneyMovesFilters(alertAttorneyMovesSearchModelBean, model, request, response);
				model.addAttribute("alertAttorneyMovesSearchModelBean", alertAttorneyMovesSearchModelBean);
			}
			else if(ALMConstants.PEOPLE_ATTORNEY_SEARCH.equalsIgnoreCase(type))
			{
				AttorneySearchModelBean alertAttorneySearchModelBean = (AttorneySearchModelBean) WebUtil.getObject(json, AttorneySearchModelBean.class);
				jsp = getAttorneySearchFilters(alertAttorneySearchModelBean, model, request, response);
				model.addAttribute("alertAttorneySearchModelBean", alertAttorneySearchModelBean);
			}
			else if(ALMConstants.EVENTS.equalsIgnoreCase(type))
			{
				EventSearchModelBean alertEventSearchModelBean = (EventSearchModelBean) WebUtil.getObject(json, EventSearchModelBean.class);
				jsp = getEventFilters(alertEventSearchModelBean, model, request, response);
				model.addAttribute("alertEventSearchModelBean", alertEventSearchModelBean);
			}
		}
		return jsp;
	}

	private void printAlertData(ActionRequest request){
		AlertSearchModelBean alertSearchModelBean = new AlertSearchModelBean(request);
//		alertSearchModelBean.printBean();
	}
	
	protected void checkAndSaveAlert(String json, ActionRequest request, ActionResponse response) throws Exception
	{
		
		/*
		 * Need to make these checks
		 * 1. If this is edit alert or save a new one
		 * 2. If the alert name is already present, don't save.
		 * 3. In case of edit, If the new alert name is already used elsewhere then dont save.
		 */
		
		
		AlertSearchModelBean asmb = new AlertSearchModelBean(request);
//		asmb.printBean();
		
		// Get the current user from the render request
		User currentUser = WebUtil.getCurrentUser(request, userService);
		
		// Get all the alerts of current user
		List<RERAlert> resultsDB = reralertService.findAllUserAlertsByUserId(currentUser.getId());
		
		String alertIdStr = request.getParameter("alertId");
		String alertUserIdStr = request.getParameter("alertUserId");
		
		int alertUserId = 0;
		int alertId = 0;
		boolean isEdit = false;
		
		if(alertUserIdStr!=null && alertUserIdStr.length()>0){
			alertUserId = Integer.parseInt(alertUserIdStr);
			isEdit = true;
		}
		if(alertIdStr!=null && alertIdStr.length()>0){
			alertId = Integer.parseInt(alertIdStr); 
			isEdit = true;
		}
		
		RERAlert alert = new RERAlert();
		
		if(isEdit){
			alert.setAlertId(alertId);
			alert.setAlertUserId(alertUserId);
		}
		boolean isAlertNamePresent = false;
		
		for(RERAlert alertObj: resultsDB){
			
			if(alert.getAlertUserId()==alertObj.getAlertUserId()){
				
			}else if(alertObj.getAlertName().equalsIgnoreCase(asmb.getAlertName())){
				isAlertNamePresent = true;
			}
		}
		
		if(isAlertNamePresent){
			System.out.println("Alert with the name \'"+asmb.getAlertName()+"\' already exists!!");
			response.setRenderParameter("error", "Alert with the name \'"+asmb.getAlertName()+"\' already exists!!");
		}else{
			
			alert.setAlertEmails(asmb.getAlertEmails());
			alert.setAlertFrequency(asmb.getAlertFrequency());
			alert.setAlertName(asmb.getAlertName());
			alert.setAlertType(asmb.getAlertType());
			
			if(asmb.isActive()){
				alert.setIsActive(1);
			}else{
				alert.setIsActive(0);
			}
			
			if(json!=null && json.length()>0){
				
			}else{
				json = "{}";
			}
			alert.setSearchJson(json);
			
			alert.setUserId(currentUser.getId()); 
			alert.setDateAdded(new Date());
			alert.setLastTriggered(new Date());
			// Try to find if alert already exists with type and json(query) string
			int results = reralertService.getRERAlertCountByTYPE_QUERY(alert);
			// if there are no results then add this json as a new alert along with type
			if(results == 0){
	//			alertId = reralertService.insertAlertQuery(alert);
				reralertService.insertAlertQuery(alert);
				alertId = alert.getAlertId();
				//alert.setAlertId(alertId);
			}else{
				// get the existing alert id which should be added to the new alert_user row.
				RERAlert alertT = reralertService.findRERAlertByTYPE_QUERY(alert);
				alertId = alertT.getAlertId();
				alert.setAlertId(alertId);
			}
			
			if(isEdit){
				reralertService.updateAlertUser(alert);
			}else{
				int alertUserid = reralertService.insertAlertUserMapping(alert);
			}
		}
	}
	
	
	@ActionMapping(params = "action=saveAlert")
	protected void saveAlert(ActionRequest request, ActionResponse response) throws Exception
	{
		AlertSearchModelBean asmb = new AlertSearchModelBean(request);
//		asmb.printBean();
		
		RERAlert alert = new RERAlert();
		alert.setAlertEmails(asmb.getAlertEmails());
		alert.setAlertFrequency(asmb.getAlertFrequency());
		alert.setAlertName(asmb.getAlertName());
		alert.setAlertType(asmb.getAlertType());
		alert.setSearchJson("this is a query for "+asmb.getAlertType());
		
		
		// Try to find if alert already exists with type and json(query) string
		
		int results = reralertService.getRERAlertCountByTYPE_QUERY(alert);
		
		// if there are no results then add this json as a new alert along with type
		if(results == 0){
			int alertId = reralertService.insertAlertQuery(alert);
		}else{ 
			// get the existing alert id which should be added to the new alert_user row.
			RERAlert alertT = reralertService.findRERAlertByTYPE_QUERY(alert);
		}
	}

	/**
	 * This method is for populating Graduation start and end year on Attorney
	 * search Screen It would start from 1900 and will have values till current
	 * year
	 * 
	 * @param fromYear
	 * @param toYear
	 * 
	 */
	private void setYearRange(List<String> fromYear, List<String> toYear)
	{
		Calendar cal = Calendar.getInstance(); // This gets the current date and  time.
		
		int currentYear = cal.get(Calendar.YEAR);
		fromYear.add(ALMConstants.ANY);
		toYear.add(ALMConstants.ANY);
		for (Integer i = 1900; i <= currentYear; i++)
		{
			fromYear.add(i.toString());
		}
		for (Integer i = 1900; i <= 2050; i++)
		{
			toYear.add(i.toString());
		}
	}
	
}
