<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="com.liferay.portal.model.Layout"%>
<%@page import="com.liferay.portal.service.LayoutLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletURL"%>
<%@page import="com.liferay.portlet.PortletURLFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="com.alm.rivaledge.util.ALMConstants"%>
<%@ taglib prefix="liferay-portlet" uri="http://liferay.com/tld/portlet"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="liferay-util" uri="http://liferay.com/tld/util"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui.css" />

<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:renderURL var="viewAlertsTab">
</portlet:renderURL>

<portlet:renderURL var="viewEmailsTab">
	<portlet:param name="page" value="emails"/>
</portlet:renderURL>

<script type="text/javascript">
	function isValidEmail(email) {
	  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	  return regex.test(email);
	}
	var hideRow = false;
	$(document).ready(function(){
		$('#Cancel-Alert-User-Email-Edit').click(function(){
			if(hideRow)
				$('#addAlertEmailRow').stop().slideToggle(500);
			hideRow = true;
		});
		$('#Add-Email-Button-1, #Add-Email-Button-2').click(function(){
			hideRow = false;
			$('#Cancel-Alert-User-Email-Edit').click();
			$('#txtAlertUserEmailId').val("");
			$('#addAlertEmailRow').stop().slideToggle(500);
		});
		$('.edit-alert-email').click(function(){
			hideRow = false;
			$('#Cancel-Alert-User-Email-Edit').click();
			//txtAlertUserPersonName
			//txtAlertUserEmail
			//txtAlertUserEmailId
			var name = $(this).closest("tr").find("td.tdAlertUserPersonName").text();
			name = $.trim(name);
			var email = $(this).closest("tr").find("td.tdAlertUserEmail").text();
			email = $.trim(email);
			var idStr = $(this).prop("id");
			var idStrArr = idStr.split("-");
			var id = idStrArr[1];
			$("#txtAlertUserPersonName").val(name);
			$("#txtAlertUserEmail").val($.trim(email));
			$("#txtAlertUserEmailId").val(id);
			$("#addAlertEmailRow").show();
		});
		$("#btnEditSaveAlertUserEmail").click(function(){
			var emailForm = $(".emailForm");
			var email = $.trim($("#txtAlertUserEmail").val());
			var name = $("#txtAlertUserPersonName").val();
			var isError = false;
			if(!isValidEmail(email)){
				alert("Email entered is not proper.");
				isError = true;
			}
			if(name.length>0){}else{
				alert("Name entered is not proper.");
				isError = true;
			}
			
			var isEdit = false;
			
			if($("#txtAlertUserEmailId").val().length>0){
				isEdit = true;
			}
			
			if(!isEdit){
				$(".tdAlertUserEmail").each(function(){
					if($.trim($(this).text())==email){
						alert("Email already exists!!!");
						isError = true;
					}
				});
			}else{
				$(".tdAlertUserEmail").each(function(){
					if($("#txtAlertUserEmailId").val() == $(this).closest("tr").prop("title")){
						
					}else{
						if($.trim($(this).text())==email){
							alert("Email already exists!!!");
							isError = true;
						}
					}
				});
			}
			
			if(!isError){
				Liferay.Portlet.hideBusyIcon("#bodyId"); // Hide Busy Icon
				emailForm.submit();
			}
		});
	});
</script>
<div id="Alerts-Page">
	<div class="breadcrumbs">
		<span>Manage Alerts</span>
	</div>
	<p class="breadcrumb-subheading">Add, Modify or Delete your alerts below</p>
	<!--<div class="flRight">
      <ul class="menuRight flRight">
        <li><a class="btn icon help" href="#">Help</a></li>
      </ul>
      <div class="clear">&nbsp;</div>
    </div>-->
    <ul id="tabs">
      <li><a href="<%=viewAlertsTab %>" title="tab2"><span>Alerts List</span></a></li>
      <li><a class="active-menu" href="<%=viewEmailsTab %>" title="tab3"><span>Manage Emails</span></a></li>
    </ul>
    <div class="Alerts-Module-Content alerts">
	<div class="resultBar mart2 marbtm1 martp2" style="width:100%; float:left;">


	<input type="button" class="buttonOne marbtm2 flLeft Add-Alert-Button" value="Add Email" title="Add Email" id="Add-Email-Button-1">
	<table width="100%" border="0" class="tblOne Add-Alert-Table"
		cellspacing="0" cellpadding="0">

        <colgroup>
        <col width="50">
        <col width="200">
        <col width="300">
        <col width="90">
        <col width="90">
        <col width="200">
        <col width="200">
        <col width="200">
        <col width="200">
        </colgroup>
		<thead>
			<tr>
                <th>&nbsp;</th>
				<th align="center">Name</th>
				<th align="left">Email Address</th>
				<th align="left">&nbsp;</th>
				<th align="left">&nbsp;</th>
				<th align="left">&nbsp;</th>
				<th align="left">&nbsp;</th>
				<th align="left">&nbsp;</th>
				<th align="left">&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<c:choose>
				<c:when test="${fn:length(userEmails) gt 0}">
					<c:forEach var="results" items="${userEmails}" varStatus="i">
						<tr class="even" value="${results.id}" title="${results.id}">
	                        <td>&nbsp;</td>
							<td align="left" class="tdAlertUserPersonName">${results.personName}</td>
							<td align="left" class="tdAlertUserEmail">${results.emailId}</td>
							<td align="left">
								<c:choose><c:when test="${not results.primary}">
									<a id="id-${results.id}" class="edit-black btn icon edit edit-alert-email" href="javascript:void(0);" title="Edit">Edit</a>
								</c:when></c:choose>
							</td>
							<td align="left">
								<c:choose><c:when test="${not results.primary}">
									<portlet:actionURL var="deleteAlertUserEmailURL">
										<portlet:param name="action" value="deleteAlertUserEmail"/>
										<portlet:param name="txtAlertUserEmailId" value="${results.id}"/>
									</portlet:actionURL>
								
									<a class="delete" href="<%=deleteAlertUserEmailURL %>" onClick="Liferay.Portlet.showBusyIcon('#bodyId','Loading...');" title="Delete">Delete</a>
								</c:when></c:choose>
							</td>

	                        <td>&nbsp;</td>
	                        <td>&nbsp;</td>
						</tr>
					</c:forEach>
				</c:when>
				<c:otherwise>
					<tr id="emptyAlertEmailRow" class="${i.index % 2 == 0 ? 'evenone' : 'oddone'} ">
						<td colspan="4" align="center">0 Results, Please try a different search</td>
					</tr>
				</c:otherwise>
			</c:choose>
			<tr id="addAlertEmailRow" style="display:none;" class="">
				<portlet:actionURL var="addAlertUserEmailURL">
					<portlet:param name="action" value="addAlertUserEmail"/>
				</portlet:actionURL>
				<form:form class="emailForm" method="post" action="<%=addAlertUserEmailURL %>" id="">
					<td>&nbsp;</td>
					<td align="left"><input class="" id="txtAlertUserPersonName" name="txtAlertUserPersonName" type="text" value="" /></td>
					<td align="left"><input class="" id="txtAlertUserEmail" name="txtAlertUserEmail" type="text" value="" /></td>
					<td align="left" valign="bottom" style="">
					<input class="" id="txtAlertUserEmailUserId" name="txtAlertUserEmailUserId" type="hidden" value="${userId}" />
					<input class="" id="txtAlertUserEmailId" name="txtAlertUserEmailId" type="hidden" value="" />
					<input class="buttonOne flLeft" type="button" value="Save" id="btnEditSaveAlertUserEmail"></td>
					<td align="left" valign="bottom" style="">
					<input class="buttonTwo flLeft" type="reset" value="Cancel" id="Cancel-Alert-User-Email-Edit" >
					</td>
				</form:form>
			</tr>
		</tbody>
	</table>
<input type="button" class="buttonOne martp2 marbtm2 flLeft Add-Alert-Button" value="Add Email" title="Add Email" id="Add-Email-Button-2">
	</div>
    </div>
	<div class="clear">&nbsp;</div>
</div>