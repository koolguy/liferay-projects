<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>

<%
	boolean isHomePage = false;
	ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
	if (themeDisplay != null)
	{
		String friendlyURL = themeDisplay.getLayout().getFriendlyURL();
		if (friendlyURL != null && !friendlyURL.equalsIgnoreCase("")
			&& (friendlyURL.equalsIgnoreCase("/almost") || friendlyURL.equalsIgnoreCase("/home") ) )
		{
			isHomePage = true;
		}
	}
%>