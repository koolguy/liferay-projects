<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="com.liferay.portal.model.Layout"%>
<%@page import="com.liferay.portal.service.LayoutLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletURL"%>
<%@page import="com.liferay.portlet.PortletURLFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="com.alm.rivaledge.util.ALMConstants"%>
<%@ taglib prefix="liferay-portlet" uri="http://liferay.com/tld/portlet"%>
<%@ taglib prefix="portlet" 		uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" 			 	uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="liferay-ui"   	uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="liferay-util" 	uri="http://liferay.com/tld/util"%>
<%@ taglib prefix="spring"          uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form"            uri="http://www.springframework.org/tags/form"%>
<%@taglib  prefix="liferay-theme" 	uri="http://liferay.com/tld/theme" %>
<%@ taglib prefix="fn" 				uri="http://java.sun.com/jsp/jstl/functions" %>
<portlet:defineObjects/>
<liferay-theme:defineObjects />
<portlet:actionURL var="saveAlertURL">
<portlet:param name="action" value="saveAlert"/>
</portlet:actionURL>
<portlet:renderURL var="viewAlertsTab">
</portlet:renderURL>
<portlet:renderURL var="viewEmailsTab">
<portlet:param name="page" value="emails"/>
</portlet:renderURL>
<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui.css" />
<portlet:resourceURL id="getAlertById" var="getAlertByIdURL" 	/>
<portlet:resourceURL id="FirmsURL" var="FirmsURL" 	/>
<portlet:resourceURL id="FirmsStatURL"  var="FirmsStatURL" />
<portlet:resourceURL id="AttorneyMovesURL"  var="AttorneyMovesURL"/>
<portlet:resourceURL id="AttorneySearchURL"  var="AttorneySearchURL"/>
<portlet:resourceURL id="EventURL"  var="EventURL" />
<style>
.resultAlertData {
	display: none;
}
#<portlet:namespace/>applySearch {
	display:none;
}
.itemText {
	padding-left: 3px !important;
	color: gray;
}
/*.searchBoxAlert .typeSelAlert {
	background: url("/re-theme/images/icnlist.png") no-repeat scroll 0 0 rgba(0, 0, 0, 0) !important;
	border: 1px solid #DDD !important;
	cursor: pointer !important;
	float: none !important;
	height: 28px !important;
	margin-left: -1px !important;
	width: 30px !important;
	border-radius: 4px !important;
}
.searchBoxAlert .inputAlert {
	background: none repeat scroll 0 0 rgba(0, 0, 0, 0);
	border: medium none;
	float: left;
	padding: 5px 5px 6px;
}
.searchBoxAlert {
	background: none repeat scroll 0 0 #FFFFFF;
	border-color: #DDDDDD;
	border-image: none;
	border-style: solid none solid solid;
	border-width: 1px medium 1px 1px;
	float: none;
	width: 157px;
}
*/
.searchCriteriaTable{
	display:none;
}
</style>
<script>


var selectedPortletURL="";
var lastAlertBeanTDID = "";
var editAlertTable = "";
var isAnythingOpened = false;
function enableEdit()
{
	$("#displayEdit").stop().slideToggle(500);
}

function setValuesOfExistingAlert(alertIdRowStr){
	var tempalertIdRowStr = "#" + alertIdRowStr;
	
	$("#spanEditAlertType").text($.trim($(tempalertIdRowStr + " .resultAlertType").text()));
	$("#txtEditAlertName").val($.trim($(tempalertIdRowStr + " .resultAlertName").text()));
	$("#txtEditAlertFreq").val($.trim($(tempalertIdRowStr + " .resultAlertFrequency").text()));
	$(".radioAlertFreq").each(function(){
		if($(this).val()==$.trim($(tempalertIdRowStr + " .resultAlertFrequency").text())){
			$("#txtEditAlertFreq").val($(this).closest("li").text());
			$(this).prop("checked", true);
		}
	});
	
	if($.trim($("#spanEditAlertType").text())=="People > Attorney Search" || $.trim($("#spanEditAlertType").text())=="People > Firm Stats"  || $.trim($("#spanEditAlertType").text())=="People > Attorney Moves"){
		$(".dailyRadio").prop("disabled", true);
		$(".dailyRadio").prop("checked", false);
		$(".dailyRadio").closest("li").hide();
	}else{
		$(".dailyRadio").prop("disabled", false);
		$(".dailyRadio").closest("li").show();
		
	}
	
	$("#txtEditAlertEmail").val($.trim($(tempalertIdRowStr + " .resultAlertEmails").text()));
	
	var emails = $(tempalertIdRowStr + " .resultAlertEmails").text().split(";");
	
	$(".alertEditEmailId").each(function(){
		for(var i=0;i<emails.length;i++){
			if($(this).val()==emails[i]){
				$(this).prop("checked", true);
			}
		}
	});
	var isActive = $(tempalertIdRowStr + " .resultAlertActive").text();
	if(isActive == "1")
		$("#isEditActive").attr("checked", true);
	$("#spanEditAlertLastTrig").text($.trim($(tempalertIdRowStr + ".resultAlertLastTrig").text()));
}

function applyAlertTypeOnClick(){
	var selectedAlertType = $("input[type='radio'][name='alertType']:checked").val();
		
	if(selectedAlertType=="<%=ALMConstants.NEWS_AND_PUBS%>")
	{
		selectedPortletURL = "<%=FirmsURL.toString()%>";
	}
	else if(selectedAlertType=="<%=ALMConstants.PEOPLE_FIRM_STAT%>")
	{
		selectedPortletURL = "<%=FirmsStatURL.toString()%>";
		
	}
	else if(selectedAlertType=="<%=ALMConstants.PEOPLE_ATTORNEY_MOVES%>")
	{
		selectedPortletURL = "<%=AttorneyMovesURL.toString()%>";
	}
	else if(selectedAlertType=="<%=ALMConstants.PEOPLE_ATTORNEY_SEARCH%>")
	{
		selectedPortletURL= "<%=AttorneySearchURL.toString()%>";
		$(".dailyRadioFreq").prop("checked", false);
		$(".dailyRadioFreq").closest("li").show();
	}
	else if(selectedAlertType=="<%=ALMConstants.EVENTS%>")
	{
		selectedPortletURL="<%=EventURL.toString()%>";
	}

	$("#txtAlertType").val(selectedAlertType);
	
	editAlertTable = $(".Edit-Alert-Table").detach();
	$("#editAlertTableDiv").append(editAlertTable);
	if(lastAlertBeanTDID!=""){
		$(lastAlertBeanTDID).html("");
	}
	lastAlertBeanTDID = "";
	if(selectedPortletURL.length>0){
		
		Liferay.Portlet.showBusyIcon("#bodyId", "Loading...");
		$.ajax({
			url:selectedPortletURL,
			method: "GET",
			success: function(data)
			{
				Liferay.Portlet.hideBusyIcon("#bodyId");
				$("#searchCriteriaDiv").html(data);
				$(".searchCriteriaTable").show();
			},
			error: function(jqXHR, textStatus, errorThrown) {
				Liferay.Portlet.hideBusyIcon("#bodyId");
				alert("error:" + textStatus + " - exception:" + errorThrown);
			}
		}); 
		//$("#AlertType").stop().slideToggle(500);
	}
}

$(document).ready(function() {
	
	$("#txtAlertName, #txtEditAlertName").focus(function(){
		if($(this).val()==""){
			$(this).prop("placeholder", "");
		}
	}).focusout(function(){
		if($(this).val()==""){
			$(this).prop("placeholder", "Alert Name");
		}
	});
	$("#txtAlertType, #txtEditAlertType").focus(function(){
		if($(this).val()==""){
			$(this).prop("placeholder", "");
		}
	}).focusout(function(){
		if($(this).val()==""){
			$(this).prop("placeholder", "Alert Type");
		}
	});

	$(".alertTypeRadio").click(function(){
		$(".dailyRadioFreq").prop("disabled", false);
		$(".dailyRadioFreq").closest("li").show();
		$("#txtAlertType").val($(this).val());
		applyAlertTypeOnClick();
	});
	$(".editAlertTypeRadio").click(function(){
		$(".dailyRadio").prop("disabled", false);
		$("#txtAlertType").val($(this).val());
		applyAlertTypeOnClick();
	});
	$(".peopleRadio").click(function(){
		
		$(".dailyRadioFreq").closest("li").hide();
		$(".dailyRadioFreq").prop("disabled", true);
		$(".dailyRadioFreq").prop("checked", false);
		if($("#txtAlertFreq").val()=="Daily"){
			if(($(this).hasClass("peopleRadio"))){
				$("#clearAlertFreq").click();
			}
		}
	});
	
	$(".edit-alert").click(function(event){
		event.preventDefault();
		var alertIdRowStr = $(this).closest('tr').attr("id");
		var tokens = alertIdRowStr.split("-"); 
		var alertUserId = tokens[1];
		var alertId = tokens[2];
		
		$(".selectedAlertNameSpan").removeClass("selectedAlertNameSpan");
		$(this).closest('tr').find(".resultAlertName").addClass("selectedAlertNameSpan");
		
		setValuesOfExistingAlert(alertIdRowStr);
		
		$('.Add-Alerts-Popup').hide();
		
		$.ajax({
		    url:'<%=getAlertByIdURL%>',
		    method: "GET",
		    data: {
		    	"alertId": alertId, 
		    	"alertUserId": alertUserId
		    },
		    success: function(data)
	        {
				$("#editAlertIDs #_edit_alert_id").val(alertId);
				$("#editAlertIDs #_edit_alert_user_id").val(alertUserId);
		    	var alertBeanTDId = "#editalert-" + alertUserId + "-" + alertId;
		    	editAlertTable = $(".Edit-Alert-Table").detach();
		    	if(lastAlertBeanTDID!=""){
		    		$(lastAlertBeanTDID).html("");
					$(lastAlertBeanTDID).closest("tr").hide();
		    	}
		    	
		    	$(alertBeanTDId).append(editAlertTable);
				editAlertTable.show();
				$(alertBeanTDId).closest("tr").show();
				$("#EditAlertEmail").hide();
				$("#EditAlertName").hide();
		    	$(alertBeanTDId).append(data);
		    	lastAlertBeanTDID = alertBeanTDId;
	        },
	        error: function(jqXHR, textStatus, errorThrown) {
	            alert("error:" + textStatus + " - exception:" + errorThrown);
	        }
	    });
		
	});
	
	$(".radioAlertFreq").change(function(e){
		$("#txtEditAlertFreq").val($("input[type='radio'][name='alertEditFrequency']:checked").val());
		e.stopPropagation();
	});
	$(".radioAlertFreqAdd").change(function(e){
		e.stopPropagation();
		$("#txtAlertFreq").val($("input[type='radio'][name='alertFrequency']:checked").val());
		//e.stopPropagation();
	});
	
	$("#btnEditSaveAlert").click(function(){
		
		//$("#editAlertIDs #_edit_alert_id").val(alertId);
		//$("#editAlertIDs #_edit_alert_user_id").val(alertUserId);
		var alertUserId = $("#editAlertIDs #_edit_alert_user_id").val();
		var alertId = $("#editAlertIDs #_edit_alert_id").val();
		
		var beanForm = $(".beanForm");
		$("#txtEditAlertFreq").val($("input[type='radio'][name='alertEditFrequency']:checked").val());
		var selectedAlertType = $("#spanEditAlertType").text();
		var selectedAlertFrequency = $("#txtEditAlertFreq").val();
		var selectedAlertName = $.trim($("#txtEditAlertName").val());
		var selectedAlertEmail = $("#txtEditAlertEmail").val();
		
		var emailStr = "";
		$(".alertEditEmailId").each(function(){
			if($(this).is(':checked')){
				if(emailStr.length>0){
					emailStr += ";";	
				}
				emailStr += $(this).val();
			}
		});
	
		$("#txtEditAlertEmail").val(emailStr);
		
		selectedAlertEmail = emailStr;
		var isActive = $('input#isEditActive').is(':checked');
		var isErrorP = false;
		$(".resultAlertName").each(function(){
			if($(this).hasClass("selectedAlertNameSpan")){}else{
				if(selectedAlertName==$(this).text()){
					alert("An alert with this name already exists!!!");
					isErrorP = true;
				}
			}
		});
		if(isErrorP){
			return;
		}
		if("Daily" == selectedAlertFrequency){
			selectedAlertFrequency = 1;
		}
		if("Bi-Weekly" == selectedAlertFrequency){
			selectedAlertFrequency = 2;
		}
		if("Weekly" == selectedAlertFrequency){
			selectedAlertFrequency = 3;
		}
		if("Monthly" == selectedAlertFrequency){
			selectedAlertFrequency = 4;
		}
		if(!(selectedAlertName.length>0)){
			alert("'Please Enter Alert name!");
			return;
		}
		$('<input>').attr({
			type: 'hidden',
			value: alertUserId,
			name: 'alertUserId'
		}).appendTo(beanForm);
		$('<input>').attr({
			type: 'hidden',
			value: alertId,
			name: 'alertId'
		}).appendTo(beanForm);
		$('<input>').attr({
			type: 'hidden',
			value: selectedAlertType,
			name: 'selectedAlertType'
		}).appendTo(beanForm);
		$('<input>').attr({
			type: 'hidden',
			value: selectedAlertFrequency,
			name: 'selectedAlertFrequency'
		}).appendTo(beanForm);
		$('<input>').attr({
			type: 'hidden',
			value: selectedAlertName,
			name: 'selectedAlertName'
		}).appendTo(beanForm);
		$('<input>').attr({
			type: 'hidden',
			value: selectedAlertEmail,
			name: 'selectedAlertEmail'
		}).appendTo(beanForm);
		$('<input>').attr({
			type: 'hidden',
			value: isActive,
			name: 'isActive'
		}).appendTo(beanForm);
		
		showLoadingIcon();
		beanForm.submit();
	});
	
	$("#btnSaveAlert").click(function(){
		
		var beanForm = $(".beanForm");
		
		var isError = false;
		
		var selectedAlertType = $("#txtAlertType").val();
		var selectedAlertFrequency = $("#txtAlertFreq").val();
		var selectedAlertName = $.trim($("#txtAlertName").val());
		$(".resultAlertName").each(function(){
			if(selectedAlertName==$(this).text()){
				alert("An alert with this name already exists!!!");
				isError = true;
				return;
			}
		});
		
		var emailStr = "";
		$(".alertEmailId").each(function(){
			if($(this).is(':checked')){
				if(emailStr.length>0){
					emailStr += ";";	
				}
				emailStr += $(this).val();
			}
		});
		
		var selectedAlertEmail = emailStr;
		var isActive = $('input#isActive').is(':checked');
		
		if("<%=ALMConstants.NEWS_AND_PUBS%>" === selectedAlertType 
			|| "<%=ALMConstants.PEOPLE_FIRM_STAT%>" === selectedAlertType 
			|| "<%=ALMConstants.PEOPLE_ATTORNEY_MOVES%>" === selectedAlertType 
			|| "<%=ALMConstants.PEOPLE_ATTORNEY_SEARCH%>" === selectedAlertType 
			|| "<%=ALMConstants.EVENTS%>" === selectedAlertType ){
				
		}else{
			alert("Please select an alert type and click apply.");
			isError = true;
			return;
		}
		
		if(!(selectedAlertName.length>0)){
			alert("Please Enter proper name");
			isError = true;
			return;
		}
		var isFreqSelected = false;
		if("Daily" == selectedAlertFrequency){
			selectedAlertFrequency = 1;
			isFreqSelected = true;
		}
		if("Bi-Weekly" == selectedAlertFrequency){
			selectedAlertFrequency = 2;
			isFreqSelected = true;
		}
		if("Weekly" == selectedAlertFrequency){
			selectedAlertFrequency = 3;
			isFreqSelected = true;
		}
		if("Monthly" == selectedAlertFrequency){
			selectedAlertFrequency = 4;
			isFreqSelected = true;
		}
		if(!isFreqSelected){
			alert("Please select frequency.");
			isError = true;
			return;
		}
		
		$('<input>').attr({
			type: 'hidden',
			value: selectedAlertType,
			name: 'selectedAlertType'
		}).appendTo(beanForm);
		$('<input>').attr({
			type: 'hidden',
			value: selectedAlertFrequency,
			name: 'selectedAlertFrequency'
		}).appendTo(beanForm);
		$('<input>').attr({
			type: 'hidden',
			value: selectedAlertName,
			name: 'selectedAlertName'
		}).appendTo(beanForm);
		$('<input>').attr({
			type: 'hidden',
			value: selectedAlertEmail,
			name: 'selectedAlertEmail'
		}).appendTo(beanForm);
		$('<input>').attr({
			type: 'hidden',
			value: isActive,
			name: 'isActive'
		}).appendTo(beanForm);
		if(!isError){
		
		showLoadingIcon();
			beanForm.submit();
		}
		
	});
	
	$("#btnAlertFreqApply").click(function(){
		$("#txtAlertFreq").val($("input[type='radio'][name='alertFrequency']:checked").val());
		$("#AlertName").stop().slideToggle(500);
	});
	
	
	
	$(".alertEditEmailId").change(function(){
		var emailStr = "";
		$(".alertEditEmailId").each(function(){
			if($(this).is(':checked')){
				if(emailStr.length>0){
					emailStr += ";";	
				}
				emailStr += $(this).val();
			}
		});
	
		$("#txtEditAlertEmail").val(emailStr);
	});
	
	$(".alertEmailId").change(function(){
		var emailStr = "";
		$(".alertEmailId").each(function(){
			if($(this).is(':checked')){
				if(emailStr.length>0){
					emailStr += ";";	
				}
				emailStr += $(this).val();
			}
		});
	
		$("#txtAlertEmail").val(emailStr);
	});
	
	$("#btnAlertEmailApply").click(function(){
		var emailStr = "";
		$(".alertEmailId").each(function(){
			if($(this).is(':checked')){
				if(emailStr.length>0){
					emailStr += ";";	
				}
				emailStr += $(this).val();
			}
		});
	
		$("#txtAlertEmail").val(emailStr);
		$("#AlertEmail").stop().slideToggle(500);
	});

	$("#btnEditAlertFreqApply").click(function(){
		$("#txtEditAlertFreq").val($("input[type='radio'][name='alertEditFrequency']:checked").val());
		$("#EditAlertName").stop().slideToggle(500);
	});
	
	$("#btnEditAlertEmailApply").click(function(){
		var emailStr = "";
		$(".alertEditEmailId").each(function(){
			if($(this).is(':checked')){
				if(emailStr.length>0){
					emailStr += ";";	
				}
				emailStr += $(this).val();
			}
		});
	
		$("#txtEditAlertEmail").val(emailStr);
		$("#EditAlertEmail").stop().slideToggle(500);
	});

	
	$("#applyAlertType").click(function(){
		//applyAlertTypeOnClick();
		$("#AlertType").stop().slideToggle(500);
	});
	
	$("input[name='alertType']").change(function() {	
		
	});
	$("#AlertType").hide();
	$("#AlertTypeArrow").click(function(){
		$("#AlertType").stop().slideToggle(500);
		$("#AlertName").hide();
		$("#AlertEmail").hide();
		isAnythingOpened = true;
	});	
	
	$("#AlertName").hide();
	$("#AlertNameArrow").click(function(){
		$("#AlertName").stop().slideToggle(500);
		$("#AlertType").hide();
		$("#AlertEmail").hide();
		isAnythingOpened = true;
	});
	$("#AlertEmail").hide();
	$("#AlertEmailArrow").click(function(){
		$("#AlertEmail").stop().slideToggle(500);
		$("#AlertType").hide();
		$("#AlertName").hide();
		isAnythingOpened = true;
	});
	
	$("#EditAlertName").hide();
	$("#EditAlertNameArrow").click(function(){
		$("#EditAlertName").stop().slideToggle(500);
		$("#EditAlertEmail").hide();
		isAnythingOpened = true;
	});
	$("#EditAlertEmail").hide();
	$("#EditAlertEmailArrow").click(function(){
		$("#EditAlertEmail").stop().slideToggle(500);
		$("#EditAlertName").hide();
		isAnythingOpened = true;
	});
	
	$('#btnAdd').click(function(){
		$('.filtersPage').hide()
		$('#additional').show()
	})
	$('#btnSave').click(function(){
		$('.filtersPage').show()
		$('#additional').hide()
	});
	
	//Show-Hide Popup to add Alerts
	$('#Add-Alert-Button-1, #Add-Alert-Button-2').click(function(){
		//$('.Add-Alerts-Popup').css('display', 'block');
		editAlertTable = $(".Edit-Alert-Table").detach();
		$("#editAlertTableDiv").append(editAlertTable);
		$("#editAlertTableDiv").hide();
		if(lastAlertBeanTDID!=""){
			$(lastAlertBeanTDID).html("");
		}
		lastAlertBeanTDID = "";
		$('.Add-Alerts-Popup').show(500);
	});
	
	
	$('#Close-Alert-Popup').click(function(){
		//$('.Add-Alerts-Popup').css('display', 'none');
		$("#searchCriteriaDiv").html("");
		$('.Add-Alerts-Popup').stop().slideToggle('fast');
	});
	
	$('#Cancel-Alert-Popup').click(function(){
		$('.Add-Alerts-Popup').css('display', 'none');
	});
	$('#Cancel-Alert-Popup-Edit').click(function(){
		editAlertTable = $(".Edit-Alert-Table").detach();
		$("#editAlertTableDiv").append(editAlertTable);
		$("#editAlertTableDiv").hide();
		if(lastAlertBeanTDID!=""){
			$(lastAlertBeanTDID).html("");
		}
		lastAlertBeanTDID = "";
	});
	
	var handler = function(event){    
    
	}
	$(document).bind("click", handler);
	
	$("#checkDuplicates").click(function(){
		
		$('[id]').each(function(){
		  var ids = $('[id="'+this.id+'"]');
		  console.warn('Checking for #'+this.id);
		  if(ids.length>1 && ids[0]==this)
		    console.error('Multiple IDs #'+this.id);
		});
	});
	
	$("#clearAlertType").click(function(){
		//defaultAlertType check
		$(".alertTypeRadio, .peopleRadio").prop('checked', false);
		$("#txtAlertType").val("");
	});
	$("#clearAlertFreq").click(function(){
		//defaultAlertFreq check
		$(".defaultFreq").prop("checked", true);
		$("#txtAlertFreq").val($("input[type='radio'][name='alertFrequency']:checked").val());
	});
	$("#clearAlertEmail").click(function(){
		//otherEmails uncheck
		$(".otherEmails").prop("checked", false);
		var emailStr = "";
		$(".alertEmailId").each(function(){
			if($(this).is(':checked')){
				if(emailStr.length>0){
					emailStr += ";";	
				}
				emailStr += $(this).val();
			}
		});
	
		$("#txtAlertEmail").val(emailStr);
	});
		
	$("#clearEditAlertEmail").click(function(){
		//otherEmails uncheck
		$(".otherEditEmails").prop("checked", false);
		var emailStr = "";
		$(".alertEditEmailId").each(function(){
			if($(this).is(':checked')){
				if(emailStr.length>0){
					emailStr += ";";	
				}
				emailStr += $(this).val();
			}
		});
	
		$("#txtEditAlertEmail").val(emailStr);
	});
		
	$("#applyEditAlertFreq").click(function(){
		//defaultAlertFreq check
		$(".defaultEditFreq").prop("checked", true);
		$("#txtEditAlertFreq").val($("input[type='radio'][name='alertEditFrequency']:checked").val());
	});	
	
	var handler = function(event){
		//alert(event.target);
		if($(event.target).is("#AlertTypeArrow") || $(event.target).is("#AlertType *") || $(event.target).is("#AlertType")){
			//$("#AlertType").stop().slideToggle(500);
		}else
			$("#AlertType").fadeOut();	

		if($(event.target).is("#AlertNameArrow") || $(event.target).is("#AlertName *") || $(event.target).is("#AlertName")){
			//$("#AlertName").stop().slideToggle(500);
		}else
			$("#AlertName").fadeOut();
			
		if($(event.target).is("#AlertEmailArrow") || $(event.target).is("#AlertEmail") || $(event.target).is("#AlertEmail *")){
			//$("#AlertEmail").stop().slideToggle(500);
		}else
			$("#AlertEmail").fadeOut();	
			
		if($(event.target).is("#EditAlertNameArrow") || $(event.target).is("#EditAlertName") || $(event.target).is("#EditAlertName *")){
			//$("#AlertEmail").stop().slideToggle(500);
		}else
			$("#EditAlertName").fadeOut();	
		if($(event.target).is("#EditAlertEmailArrow") || $(event.target).is("#EditAlertEmail") || $(event.target).is("#EditAlertEmail *")){
			//$("#AlertEmail").stop().slideToggle(500);
		}else
			$("#EditAlertEmail").fadeOut();	
	}
	$(document).bind("click", handler);
});
function applyFrequency(){
	$("#AlertName .radioAlertFreqAdd").each(function(){
		if($(this).is(":checked")){
			$("#txtAlertFreq").val($(this).val());
		}
	});
}
function applyEmail(){
	var emailStr = "";
	$(".alertEmailId").each(function(){
		if($(this).is(':checked')){
			if(emailStr.length>0){
				emailStr += ";";	
			}
			emailStr += $(this).val();
		}
	});
	$("#txtAlertEmail").val(emailStr);
	
}
function applyAlertType(){
	$("#AlertType .alertTypeRadio").each(function(){
		if($(this).is(":checked")){
			//$("#txtAlertType").val($(this).val());
			$(this).prop("checked", false);
			$("#txtAlertType").val("");
		}
	});
}
$(document).ready(function(){
	applyFrequency();
	applyAlertType();
	applyEmail();
});
</script>

<div id="Alerts-Page">
	<div class="breadcrumbs"><span>Manage Alerts</span></div>
	<p class="breadcrumb-subheading">Add, Modify or Delete your alerts below</p>
	<!--<div class="flRight">
    <ul class="menuRight flRight">
      <li><a class="btn icon help" href="#">Help</a></li>
    </ul>
    <div class="clear">&nbsp;</div>
  </div>-->
  <ul id="tabs">
    <li><a class="active-menu" href="<%=viewAlertsTab %>" title="tab2"><span>Alerts List</span></a></li>
    <li><a  href="<%=viewEmailsTab %>" title="tab3"><span>Manage Emails</span></a></li>
  </ul>
  <form:form commandName="alertSearchModelBean" method="post" action="" id="alertSearchModelBean">
    <div id="content" class="Alerts-Module-Content">
      <div id="dropdown">
        <div class="flLeft marbtm3 martp1"> <span class="Active-Inactive-Alerts flLeft marlt2 martp1 mart3"> 
		<strong>${alertStatusStr}</strong></span>
          <input type="button" class="buttonOne flLeft Add-Alert-Button" value="Add Alert" title="Add Alert" id="Add-Alert-Button-1">

          <div class="Add-Alerts-Popup" style="display:none">
            <table width="100%" border="0" class="tblOne Add-Alert-Table" cellspacing="0" cellpadding="0">
            <colgroup>
              <col width="50">
              <col width="215">
              <col width="150">
              <col width="215">
              <col width="215">
              <col width="130">
              <col width="220">
              <col width="100">
              <col width="100">
              <col width="85">
              </colgroup>
              <thead>
                <tr>
                  <th></th>
                  <th align="center">Alert Type</th>
                  <th align="center">Alert Name</th>
                  <th align="left" style="padding-left: 20px;">How Often</th>
                  <th align="left">Alert Email(s)</th>
                  <th align="left" style="padding-left: 20px;">Active</th>
                  <th align="left">Last Triggered</th>
                  <th align="right">&nbsp;</th>
                  <th align="right"><!-- <span id="Close-Alert-Popup">Close</span> --></th>
                  <th>&nbsp;</th>
                </tr>
              </thead>
              <tbody>
                <tr class="even">
                  <td></td>
                  <td  align="left">
				  <input class="AlertTypeField inputAlert" id="txtAlertType" type="text" readonly="readonly" placeholder="Alert Type" />
                    <input type="button" class="DropArrow typeSelAlert" id="AlertTypeArrow">

                    <div class="PopDropFields" id="AlertType" style="z-index: 2;">
                      <ul>
                        <li>
                          <form:radiobutton class="alertTypeRadio" path="alertType" value="<%=ALMConstants.NEWS_AND_PUBS %>"/>
                          News & Publications

                          </p>
                        </li>

                        <li> 
                          <!--  <input type="radio" />
                          <label>People > Firm Stats</label> -->
                          <form:radiobutton class="alertTypeRadio peopleRadio" path="alertType" value="<%=ALMConstants.PEOPLE_FIRM_STAT%>"/>
                          People > Firm Stats

                          </p>
                        </li>

                        <li> 
                          <!-- <input type="radio" />
                          <label>People > Attorney Search</label> -->
                          <form:radiobutton class="alertTypeRadio peopleRadio" path="alertType" value="<%=ALMConstants.PEOPLE_ATTORNEY_SEARCH%>"/>
                          People > Attorney Search

                          </p>
                        </li>

                        <li> 
                          <!-- <input type="radio" />
                          <label>People > Attorney Moves</label> -->
                          <form:radiobutton class="alertTypeRadio peopleRadio" path="alertType" value="<%=ALMConstants.PEOPLE_ATTORNEY_MOVES%>"/>
                          People > Attorney Moves

                          </p>
                        </li>

                        <li> 
                          <!--  <input type="radio" />
                          <label>Events</label> -->
                          <form:radiobutton class="alertTypeRadio" path="alertType" value="<%=ALMConstants.EVENTS%>"/>
                          Events

                          </p>
                        </li>
                      </ul>
                      <div class="btmdiv">
                        <input type="button" value="Apply" class="buttonOne" id="applyAlertType" />
                        <input type="button" value="Clear All" class="buttonTwo" id="clearAlertType" />
                      </div>
                    </div></td>
                  <td  align="left"><input class="AlertTypeField " id="txtAlertName"  type="text" maxlength="60"  placeholder="Alert Name"  /></td>
                  <td align="left"><input style="padding-left: 20px;" class="AlertTypeField  inputAlert" id="txtAlertFreq" type="text" readonly="readonly" placeholder="Frequency" />
                    <input type="button" class="DropArrow typeSelAlert" id="AlertNameArrow" >

                    <div class="PopDropFields" id="AlertName">
                      <ul>
                        <li>
                          <form:radiobutton path="alertFrequency" class="dailyRadioFreq radioAlertFreqAdd" value="<%=ALMConstants.DAILY%>"/>
                          <%=ALMConstants.DAILY%>

                          </p>
                        </li>
                        <li>
                          <form:radiobutton class="radioAlertFreqAdd" path="alertFrequency" value="<%=ALMConstants.BI_WEEKLY%>"/>
                          <%=ALMConstants.BI_WEEKLY%>

                          </p>
                        </li>
                        <li>
                          <form:radiobutton class="radioAlertFreqAdd defaultFreq" checked="checked" path="alertFrequency" value="<%=ALMConstants.WEEKLY%>"/>
                          <%=ALMConstants.WEEKLY%>

                          </p>
                        </li>
                        <li>
                          <form:radiobutton class="radioAlertFreqAdd" path="alertFrequency" value="<%=ALMConstants.MONTHLY%>"/>
                          <%=ALMConstants.MONTHLY%>

                          </p>
                        </li>
                      </ul>
                      <div class="btmdiv">
                        <input type="button" value="Apply" class="buttonOne" id="btnAlertFreqApply">
                        <input type="button" value="Clear All" class="buttonTwo" id="clearAlertFreq">
                      </div>
                    </div></td>
                  <td align="left"><input class="AlertTypeField inputAlert" readonly="readonly" id="txtAlertEmail" type="text" placeholder="Email" />
                    <input type="button" class="DropArrow typeSelAlert" id="AlertEmailArrow">

                    <div class="PopDropFields" id="AlertEmail">
                      <ul class="Alert-Email-List">
                        <c:choose>
                          <c:when test="${fn:length(userEmails) gt 0}">
                            <c:forEach var="results" items="${userEmails}" 	varStatus="i">


                              <li>
                                <c:choose>
                                  <c:when test="${not results.primary}">
                                    <input type="checkbox" class="alertEmailId otherEmails" value="${results.emailId}" />
                                    <label>${results.emailId}</label>
                                  </c:when>
                                  <c:otherwise>
                                    <input type="checkbox" checked="checked" disabled="disabled" class="alertEmailId" value="${results.emailId}" />
                                    <label>${results.emailId}</label>
                                  </c:otherwise>
                                </c:choose>

                              </li>
                            </c:forEach>
                          </c:when>
                        </c:choose>
                      </ul>
                      <div class="btmdiv">
                        <input type="button" value="Apply" class="buttonOne" id="btnAlertEmailApply">
                        <input type="button" value="Clear All" class="buttonTwo" id="clearAlertEmail">
                      </div>
                    </div></td>
                  <td style="padding-left: 20px;" align="left" class="Bold-Entries"><input type="checkbox" id="isActive" />


                    &nbsp;
                    <label for="isActive">Active</label></td>


                  <td align="left" width="220px" class="Bold-Entries"></td>
                  <td align="right"><input class="buttonOne flLeft" type="button" value="Save" id="btnSaveAlert"></td>
                  <td align="left"><input class="buttonTwo flLeft" type="button" value="Cancel" id="Cancel-Alert-Popup" ></td>
                  <td></td>
                </tr>
              </tbody>
            </table>



            <table width="100%" class="searchCriteriaTable">

              <tr>
                <td style="vertical-align:top; font-weight: bold; padding-top:41px;" valign="top"><span class="Select-Filter-Heading">Select Filters</span></td>
                <td valign="top"><div class="Add-Alerts-Popup-Filters" id="searchCriteriaDiv"> </div></td>

              </tr>
            </table>

          </div>

        </div>
        <div class="alerts">
          <table width="100%" border="0" class="tblOne" cellspacing="0" cellpadding="0">
            <colgroup>
            <col width="40">
            <col width="300">
            <col width="140">
            <col width="200">
            <col width="300">
            <col width="200">
            <col width="300">
            <col width="200">
            <col width="200">
            </colgroup>
            <thead>
              <tr>
                <th></th>
                <th align="center">Alert Type</th>
                <th align="center">Alert Name</th>
                <th align="left"  style="padding-left: 20px;">How Often</th>
                <th align="left">Alert Email(s)</th>
                <th align="left" style="padding-left: 20px;">Active</th>
                <th align="left">Last Triggered</th>
                <th align="left">&nbsp;</th>
                <th align="left">&nbsp;</th>
              </tr>
            </thead>
            <c:choose>
              <c:when test="${fn:length(userAlerts) gt 0}">
                <c:forEach  var="results" items="${userAlerts}" varStatus="i">
                  <tr class="${i.index % 2 == 0 ? 'even' : 'odd'} " id='alert-${results.alertUserId}-${results.alertId}'>
                    <td></td>
                    <td align="left"><span class='resultAlertType resultAlertData'>${results.alertType}</span> ${results.alertType} </td>
                    <td align="left"><span class='resultAlertName resultAlertData'>${results.alertName}</span> ${results.alertName} </td>

                    <td  style="padding-left: 20px;" align="left">
						<span class='resultAlertFrequency resultAlertData2'>
                      <c:choose>
                        <c:when test="${results.alertFrequency eq '1' }"> Daily </c:when>
                        <c:when test="${results.alertFrequency eq '2' }"> Bi-Weekly </c:when>
                        <c:when test="${results.alertFrequency eq '3' }"> Weekly </c:when>
                        <c:when test="${results.alertFrequency eq '4' }"> Monthly </c:when>
                      </c:choose>
					  	</span>


					  </td>



                    <td  align="left"><span class='resultAlertEmails resultAlertData'>${results.alertEmailsStr}</span>
                      <c:choose>
                        <c:when test="${fn:length(results.alertEmails) gt 0}">
                          <c:forEach  var="email" items="${results.alertEmails}" varStatus="j"> ${email} </c:forEach>


                        </c:when>
                      </c:choose></td>


                    <td  align="left" style="padding-left: 20px;"><span class='resultAlertActive resultAlertData'>${results.isActive}</span>

                      <c:choose>
                        <c:when test="${results.isActive eq '1' }"> Yes </c:when>


                        <c:otherwise> No </c:otherwise>


                      </c:choose></td>

                    <td  align="left"><span class='resultAlertLastTrig resultAlertData'>${results.lastTriggered}</span>

                      <c:choose>
                        <c:when test="${empty results.lastTriggered}"> -- </c:when>


                        <c:otherwise> ${results.lastTriggered} </c:otherwise>


                      </c:choose></td>

                    <td  align="left"><a class="edit-black btn icon edit edit-alert" href="#" title="Edit">Edit</a></td>


                    <td  align="left"><portlet:actionURL var="deleteAlertURL">

                        <portlet:param name="action" value="deleteAlert"/>
                        <portlet:param name="alertUserId" value="${results.alertUserId}"/>
                      </portlet:actionURL>

                      <a class="delete" href="<%=deleteAlertURL %>" onClick="showLoadingIcon();" title="Delete">Delete</a></td>


                  </tr>
                  <tr class=" ${i.index % 2 == 0 ? 'even' : 'odd'} " style="display:none;">
                    <td colspan="10" id='editalert-${results.alertUserId}-${results.alertId}'></td>


                  </tr>
                </c:forEach>
              </c:when>
              <c:otherwise>
                <tr class="${i.index % 2 == 0 ? 'evenone' : 'oddone'} ">
                  <td colspan="8" align="left" style="padding: 12px 5px 5px 10px;">You haven't created an alert yet. Use the button to add a new alert.</td>
                </tr>
              </c:otherwise>
            </c:choose>
          </table>
          <div class="Add-Alert-Button-Bottom">
          <hr>
          <input type="button" class="buttonOne flLeft Add-Alert-Button" value="Add Alert" title="Add Alert" id="Add-Alert-Button-2">
          </div>
        </div>
        <div id="editAlertTableDiv">
          <table width="100%" border="0" class="tblOne Edit-Alert-Table" style="display:none" cellspacing="0" cellpadding="0">
            <colgroup>
            <col width="50">
            <col width="215">
            <col width="150">
            <col width="215">
            <col width="215">
            <col width="130">
            <col width="220">
            <col width="100">
            <col width="100">
            <col width="85">
            </colgroup>
            <tbody>
              <tr class="even">
                <td>&nbsp;</td>
                <td align="left"><span id="spanEditAlertType"></span></td>
                <td align="center"><input class="AlertTypeField" id="txtEditAlertName"  maxlength="60" type="text" value="" placeholder="Alert Name" /></td>

                <td>
				<li>
				<input class="AlertTypeField inputAlert" readonly="readonly" id="txtEditAlertFreq" type="text" value="" placeholder="Alert Frequency" />
                  <input type="button" class="DropArrow typeSelAlert" id="EditAlertNameArrow" />

                  <div class="PopDropFieldsEditAlert" id="EditAlertName">
                    <ul>
                      <li>
                        <input type="radio" class="radioAlertFreq dailyRadio" name="alertEditFrequency" value="<%=ALMConstants.DAILY%>"/>
                        <%=ALMConstants.DAILY%></li>
                      <li>
                        <input type="radio" class="radioAlertFreq"  name="alertEditFrequency" value="<%=ALMConstants.BI_WEEKLY%>"/>
                        <%=ALMConstants.BI_WEEKLY%></li>
                      <li>
                        <input type="radio" class="radioAlertFreq defaultEditFreq"  name="alertEditFrequency" value="<%=ALMConstants.WEEKLY%>"/>
                        <%=ALMConstants.WEEKLY%></li>
                      <li>
                        <input type="radio" class="radioAlertFreq"  name="alertEditFrequency" value="<%=ALMConstants.MONTHLY%>"/>
                        <%=ALMConstants.MONTHLY%></li>
                    </ul>
                    <div class="popupsubmit">
                      <input type="button" value="Apply" class="buttonOne" id="btnEditAlertFreqApply">
                      <input type="button" value="Clear All" class="buttonTwo" id="applyEditAlertFreq">
                    </div>
                  </div>
                  </li>
                </td>
                <td><li>


                    <input class="AlertTypeField inputAlert " readonly="readonly" id="txtEditAlertEmail" type="text" value="" placeholder="Alert Email" />
                    <input type="button" class="DropArrow typeSelAlert" id="EditAlertEmailArrow">

                    <div class="PopDropFieldsEditAlert" id="EditAlertEmail">
                      <ul class="Alert-Email-List">
                        <c:choose>
                          <c:when test="${fn:length(userEmails) gt 0}">
                            <c:forEach var="results" items="${userEmails}" 	varStatus="i">
                              <li>

                                <c:choose>
                                  <c:when test="${not results.primary}">
                                    <input type="checkbox" class="alertEditEmailId otherEditEmails"  name="alertEditEmail" value="${results.emailId}" />
                                    <label>${results.emailId}</label>
                                  </c:when>
                                  <c:otherwise>
                                    <input type="checkbox" checked="checked" disabled="disabled" class="alertEditEmailId" name="alertEditEmail" value="${results.emailId}" />
                                    <label>${results.emailId}</label>
                                  </c:otherwise>
                                </c:choose>

                              </li>
                            </c:forEach>
                          </c:when>
                        </c:choose>
                      </ul>
                      <div class="popupsubmit">
                        <input type="button" value="Apply" class="buttonOne" id="btnEditAlertEmailApply" style="margin:0 10px 0 0">
                        <input type="button" value="Clear All" class="buttonTwo" id="clearEditAlertEmail">
                      </div>
                    </div>

                  </li></td>
                <td class="Bold-Entries"><input type="checkbox" id="isEditActive" />


                  &nbsp;
                  <label for="isEditActive">Active</label></td>


                <td class="Bold-Entries"><span id="spanEditAlertLastTrig"></span></td>
                <td><input class="buttonOne flLeft" type="button" value="Save" id="btnEditSaveAlert"></td>
                <td><input class="buttonTwo flLeft" type="button" value="Cancel" id="Cancel-Alert-Popup-Edit" ></td>
              </tr>
            </tbody>
          </table>
        </div>
        <div id="editAlertIDs">
          <%-- For dynamically holding the value of currect edit alert id and alert_user id --%>
          <input type="hidden" id="_edit_alert_id" value="" />
          <input type="hidden" id="_edit_alert_user_id" value="" />
        </div>
      </div>
    </div>
  </form:form>

</div>

