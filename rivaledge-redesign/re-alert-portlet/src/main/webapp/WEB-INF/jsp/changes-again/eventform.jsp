<%@page import="com.alm.rivaledge.util.ALMConstants"%>
<%@page import="com.alm.rivaledge.transferobject.EventSearchDTO"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>

<%@ taglib prefix="liferay-portlet" uri="http://liferay.com/tld/portlet"%>
<%@ taglib prefix="portlet" 		uri="http://java.sun.com/portlet_2_0"%> 
<%@ taglib prefix="c" 			 	uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="liferay-ui"   	uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="liferay-util" 	uri="http://liferay.com/tld/util"%>
<%@ taglib prefix="spring"          uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form"            uri="http://www.springframework.org/tags/form"%>

<portlet:defineObjects/>

<%@ include file="./common.jsp" %>

<portlet:actionURL var="submitURL">
	<portlet:param name="action" value="saveEventFilters"/>
</portlet:actionURL>

<style>

.ui-autocomplete-category {
	font-weight: bold;
	padding: .2em .4em;
	margin: .8em 0 .2em;
	line-height: 1.5;
}

.ui-autocomplete {
	max-height: 200px;
	width:220px;
	overflow-y: auto;
	/* prevent horizontal scrollbar */
	overflow-x: hidden;
}
</style>

<script type="text/javascript">
    $(function() {
        $(".webwidget_tab").webwidget_tab({
            head_text_color: '#888',
            head_current_text_color: '#222'
        });
    });
</script>

<script type="text/javascript">

/**
*Autoselect previously selected search critria
*/
	function initializeSearchCriteria()
	{
		applyFirms();
		initializeDate();
		applyLocations();
		applyPracticeArea();
		
		refreshCounters();
	}
/**
 * Initialized the user selected Date from previous search and this is retained through out the session
 */
	function initializeDate()
		{
			
			var dateTextStr = '${eventSearchModelBean.dateText}';
			
			if(dateTextStr.match('^Any') || dateTextStr.match('^Last') || dateTextStr.match('^Next'))
				{
					$("#datePeriodSelect option[value='"+dateTextStr+"']").attr('selected','selected'); 
					$('#period').prop("checked", true);
				}
			else{ // we have date range parse it and set the Date on datePicker
					$( "#from" ).datepicker( "setDate", dateTextStr.split("-")[0]);
					$( "#to" ).datepicker( "setDate", dateTextStr.split("-")[1]);
				}
			
				$("#dateText").val(dateTextStr);
	
		}

	function applyFirms()
	{
		var allSelectedValue = [];
		var allSelectedIds = [];
		
		 $("#Firmstext").val('');
		 $("#selectedFirms").val('');
		 
		var isFirmChecked = false;
		var allWatchListCounter = 0;
		
		$('.individualfirmsWatchList input[type=checkbox]:checked').each(function() {
			 allSelectedValue.push($(this).attr('labelAttr'));
			 allSelectedIds.push($(this).val());
			 $("#firm_watchlist").prop("checked", true);
			 $('#firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
			 
			 /* var watchListId = $('#defaultWatchListId').val();
				
				$('#individualfirmsWatchListDIV input[type=checkbox]').each(function() {
					 if(this.value == watchListId)
						{
							$(this).prop("checked", true);
						}
				}); */
				
				 $('#individualfirmsWatchListDIV input[type=checkbox]:checked').each(function() 
					{
							allWatchListCounter++;
					});
				 		
				$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (' + allWatchListCounter +' Selected)');
							isFirmChecked = true;
	 			});
		
		$('.individualfirms input[type=checkbox]:checked').each(function() {
			 allSelectedValue.push($(this).attr('labelAttr'));
			 allSelectedIds.push($(this).val());
			 isFirmChecked = true;
	 	});

		$('.individualfirmsOrganization input[type=checkbox]:checked').each(function() {
			 allSelectedValue.push($(this).attr('labelAttr'));
			 allSelectedIds.push($(this).val());
			 isFirmChecked = true;
	 	});
		
		if(allSelectedValue != ''  &&  allSelectedIds != '') 
			{
				$("#Firmstext").val(allSelectedValue.join(",")); //Use a comma separator for values which show up on UI
				$("#selectedFirms").val(allSelectedIds.join(";"));
				 isFirmChecked = true;
			}
		else if($(".rivaledgeListAMLAW_100").is(":checked"))
		{
			$("#Firmstext").val('<%=ALMConstants.AMLAW_100 %>');
			$("#selectedFirms").val('<%=ALMConstants.AMLAW_100 %>');
			$('#firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
			$('#individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
			isFirmChecked = true;
		} 
		else if($(".rivaledgeListAMLAW_200").is(":checked"))
		{
			
			$("#Firmstext").val('<%=ALMConstants.AMLAW_200 %>');
			$("#selectedFirms").val('<%=ALMConstants.AMLAW_200 %>');
			$('#firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
			$('#individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
			isFirmChecked = true;
		} 
		else if($(".rivaledgeListNLJ_250").is(":checked"))
		{
			
			$("#Firmstext").val('<%=ALMConstants.NLJ_250 %>');
			$("#selectedFirms").val('<%=ALMConstants.NLJ_250 %>');
			$('#firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
			$('#individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
			isFirmChecked = true;
		} 
		else
		{
			$("#all_firm_org, #all_firm, #all_org, #firm_watchlist, #firm_individual, #org_individual").each(function(){
				
				 $(".rivaledgeListAMLAW_100").prop("checked", false);
				 $(".rivaledgeListAMLAW_200").prop("checked", false);
				 $(".rivaledgeListNLJ_250").prop("checked", false);
				 $('#individualfirms input[type=checkbox]:checked').removeAttr('checked'); 
				 $("#firm_watchlist").prop("checked", false);
				
				if($("#all_firm").is(":checked"))
					{
						$("#Firmstext").val('<%=ALMConstants.ALL_FIRMS %>');
						$("#selectedFirms").val('<%=ALMConstants.ALL_FIRMS %>');
						$('#firm_watchlist').val("<%=ALMConstants.ALL_FIRMS %>");
						isFirmChecked = true;
					} 
				else if($("#all_org").is(":checked"))
				{
					$("#Firmstext").val('<%=ALMConstants.ALL_ORGANIZATIONS %>');
					$("#selectedFirms").val('<%=ALMConstants.ALL_ORGANIZATIONS %>');
					$('#firm_watchlist').val("<%=ALMConstants.ALL_ORGANIZATIONS %>");
					isFirmChecked = true;
				} 
				else if($("#all_firm_org").is(":checked"))
				{
					$("#Firmstext").val('<%=ALMConstants.ALL_FIRMS_ORGANIZATION %>');
					$("#selectedFirms").val('<%=ALMConstants.ALL_FIRMS_ORGANIZATION %>');
					$('#firm_watchlist').val("<%=ALMConstants.ALL_FIRMS_ORGANIZATION %>");
					isFirmChecked = true;
				} else
					{
						<%-- if($(this).is(":checked")){
							var value =$(this).val();
						  //$("#Firmstext").val(value);
						 	//$("#selectedFirms").val(value);
							$("#Firmstext").val('AMLAW 100');
							$("#selectedFirms").val('AMLAW 100');
							$('#firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
							isFirmChecked = true;
						}  --%>
						var watchListId = $('#defaultWatchListId').val();
						
						$('#individualfirmsWatchListDIV input[type=checkbox]').each(function() {
							
							 if(this.value == watchListId)
								{
									$(this).prop("checked", true);
									$("#Firmstext").val($(this).attr('labelAttr'));
									$("#selectedFirms").val($(this).val());

									$("#firm_watchlist").prop("checked", true);
									$('#firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
									isFirmChecked = true;
								}
						});
						
					}
			});
			
		}
		
		if(isFirmChecked == false)
			{
			
				$("#Firmstext").val('AmLaw 100');
				$("#selectedFirms").val('AmLaw 100');
				$('#firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
				 $("#firm_watchlist").prop("checked", true);
				 $(".rivaledgeListAMLAW_100").prop("checked", true);
				 //$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" checked="false" /> Watchlists (0 Selected)');
			}
	}

	function applyLocations()
	{
	
	var checked= false;
		
		 if($("#loc_all").is(":checked"))
		 {
				$("#selectedLocation").val($("#loc_all").val());
				checked = true;
		 } 
		 if($("#loc_inperson").is(":checked"))
		 {
				$("#selectedLocation").val($("#loc_inperson").val());	
				checked = true;
		 } 
		 if($("#loc_webinars").is(":checked"))
		 {
				$("#selectedLocation").val($("#loc_webinars").val());		
				checked = true;
		 } 
		if(!checked)
		 {
			 var allValsLocation = [];
			
				 $('#allOtherLocationsDiv input[type=checkbox]:checked').each(function() { 
					 //allValsLocation.push($(this).val());
					 allValsLocation.push($(this).attr('locationValue'));
			    });
			if(allValsLocation.length > 0){
				$("#loc_individual").prop("checked", true);
			    $("#selectedLocation").val(allValsLocation.join(";"));		
			}				 
		}
		 
	}


	function applyPracticeArea()
	{

		 if($(".allPracticeArea").is(":checked"))
		 {
				$("#selectedPracticeArea").val($(".allPracticeArea").val());			
		 } 
		 else
		 {
			var allValsPracticeArea = [];
				$("#allPracticeAreaIndividual").prop("checked", true);
			
				 $('#practiceAreaDiv input[type=checkbox]:checked').each(function() { 
				 	allValsPracticeArea.push($(this).val());
			    });
				 
			    $("#selectedPracticeArea").val(allValsPracticeArea.join(";"));		
		}
			
	}
	
	function applyDate()
		{
		  var  checkDate=false;
		  var dateValue;
		  
		  if($("#dateRange").is(":checked"))
		   {
		   
		   var fromDate = new Date($("#from").val());
		   var toDate = new Date($("#to").val()); 
		   if(!ValidateDate($("#from").val()))
		   {
		     checkDate=true;
		        $('#fromdateError').css('display','block');      
		   }
		   else
		   {
		    checkDate=false;
		    $('#fromdateError').css('display','none');
		   }  
		   if(!ValidateDate($("#to").val()))
		    {
		    checkDate=true;
		    $('#todateError').css('display','block');
		    
		    }  
		   else
		   {
		    if(!checkDate)
		    {
		     checkDate=false;
		     $('#todateError').css('display','none'); 
		    }
		   
		   }
		   if(!(toDate >= fromDate) && !checkDate)
		   {
		    $('#dateValidError').css('display','block');
		    checkDate=true;
		   } 
		   else
		   {
		    $('#dateValidError').css('display','none');
		   }
		  
		   if(checkDate)
		   {
		   return checkDate; 
		   }
		  
	  }
	  
	  if($("#period").is(":checked"))
	  {
	   
	   dateValue=$("#datePeriodSelect").val();
	   
	  } 
	  else if($("#dateRange").is(":checked"))
	  {
	   dateValue=$("#from").val() + "-"+ $("#to").val();
	  } 
	  
	  $("#dateText").val(dateValue);
	}
	
	
	
	function toggleViewSettings(vsId)
	{
		
		//get the position of the placeholder element
	   var vsPos   = $(vsId).offset();
	   var vsHeight = $(vsId).height();
	   var vsWidth = $(vsId).width();
	   var popupWidth = $("#view_settings").width();
	   
	    //show the menu directly over the placeholder
	   $("#view_settings").css({  position: "absolute", "left": (vsPos.left - popupWidth + vsWidth) + "px", "top":(vsPos.top + vsHeight)  + "px" });
	   $("#view_settings").toggle();
	}
	
	
	function applyViewSettings()
	{
		$("#view_settings").toggle();
		search();
	}
	
	function resetViewSettings()
	{
		$('input:radio[name=orderBy]:nth(1)').prop('checked',true);  //firms/orgs, the 2nd radiobutton
		$('input:radio[name=searchResultsPerPage]:nth(2)').prop('checked',true);  //100, the 3rd radiobutton
		//$("#view_settings").toggle(); // dont close the popup, give a chance to end user to know what the default values are let him click apply
		//search();
	}
	
	function cancelViewSettings()
	{
		$("#view_settings").toggle();
	}
	
	function changeOnWatchList()
	{
		
		var allWatchListCounter = 0;
		 $('#individualfirmsWatchList input[type=checkbox]:checked').each(function() {
			 allWatchListCounter++;
		    });
			
			$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists ('+allWatchListCounter+') Selected');
			
			if(allWatchListCounter == 0)
			{
				
				$('#all_firm_org').prop("checked", false);
				$('#firm_watchlist').prop("checked", false);
				$('#all_firm').prop("checked", false);
				$('#firm_individual').prop("checked", false);
				$('#all_org').prop("checked", false);
				$('#org_individual').prop("checked", false);
				$('#individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
				$('#individualfirms input[type=checkbox]:checked').removeAttr('checked');
				$('#individualfirmsOrganization input[type=checkbox]:checked').removeAttr('checked');
			}
		else
			{
				$('#firm_watchlist').prop("checked", true);
				$('#firm_individual').prop("checked", false);
				$('#all_firm_org').prop("checked", false);
				$('#all_firm').prop("checked", false);
				$('#org_individual').prop("checked", false);
				$('#individualfirms input[type=checkbox]:checked').removeAttr('checked');
				$('#individualfirmsOrganization input[type=checkbox]:checked').removeAttr('checked');
				$("#firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/> Firms (0) Selected');
				$(".individualfirms-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="selectAllOrganizations" /> Organizations (0) Selected');
			}
			
	}
	
	function refreshCounters()
	{
		
		var allWatchListCounter = 0;
		var allFirmsCounterTemp = 0;
		var allOrgsCounterTemp = 0;
		var allWatchListSelectedCounter = 0;
		var counter = 0;
		var orgsCounter = 0;
		var watchlistCounter = 0;
		
		 //$('#individualfirmsWatchList input[type=checkbox]:checked').each(function() {
		$('#individualfirmsWatchListDIV input[type=checkbox]:checked').each(function() {
		 	allWatchListCounter++;
	    });
		
		$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists ('+allWatchListCounter+') Selected');
	
		var allFirmsCounter = 0;
		$('#individualfirms input[type=checkbox]:checked').each(function() {
		 allFirmsCounter++;
		});
		$("#firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/> Firms ('+allFirmsCounter+') Selected');
		
		var allOrgsCounter = 0;
		$('#individualfirmsOrganization input[type=checkbox]:checked').each(function() {
			 allOrgsCounter++;
	    });
				
		$(".individualfirms-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="selectAllOrganizations" /> Organizations ('+allOrgsCounter+') Selected');
		
		var allLocationCounter = 0;
		 $('#allOtherLocationsDiv input[class=allLocationsCheckBox]:checked').each(function() {
			 allLocationCounter++;
		    });
		$("#locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="selectAllLocations" /> Locations ('+allLocationCounter+') Selected');

		var allPracticeCounter = 0;
		$('#allOtherPracticeArea input[type=checkbox]:checked').each(function() {
			 allPracticeCounter++;
		    });
			
		$("#practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" /> PracticeAreas ('+allPracticeCounter+') Selected');
	
		$('#individualfirms input[type=checkbox]:checked').each(function() {
			 allFirmsCounterTemp++;
			});
		 
			 $('#individualfirms input[type=checkbox]:not(:checked)').each(function() 
				{
					 	counter++;
				});
			 
			 if(counter == 0)
			 {
				 $("#firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms" checked="true" />  Firms ('+allFirmsCounterTemp+') Selected');
			 }
			 else
			 {
				 $("#firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms" />  Firms ('+allFirmsCounterTemp+') Selected');
			 }
			 
			 $('#individualfirmsOrganization input[type=checkbox]:checked').each(function() {
				 allOrgsCounterTemp++;
				});
			 
				 $('#individualfirmsOrganization input[type=checkbox]:not(:checked)').each(function() 
					{
						 	orgsCounter++;
					});
				 
				 if(orgsCounter == 0)
				 {
					 $("#organizationsCounter").html('<input type="checkbox" value="selectAllOrganizations" id="selectAllOrganizations" checked="true" />  Organizations ('+allOrgsCounterTemp+') Selected');
				 }
				 else
				 {
					 $("#organizationsCounter").html('<input type="checkbox" value="selectAllOrganizations" id="selectAllOrganizations" />  Organizations ('+allOrgsCounterTemp+') Selected');
				 }
				 
				 $('#individualfirmsWatchListDIV input[type=checkbox]:checked').each(function() {
					 allWatchListSelectedCounter++;
					});
				 
					 $('#individualfirmsWatchListDIV input[type=checkbox]:not(:checked)').each(function() 
						{
							 	watchlistCounter++;
						});
					 
					 if(watchlistCounter == 0 && allWatchListSelectedCounter != 0)
					 {
						 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" checked="true"/> Watchlists ('+allWatchListSelectedCounter+') Selected');
					 }
					 else
					 {
						 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList"/> Watchlists ('+allWatchListSelectedCounter+') Selected');
					 }
			
	}
					
	function changeOnIndividualFirms()
	{
		var allFirmsCounter = 0;
		var allNotSelectedFirms = 0;
		 $('#individualfirms input[type=checkbox]:checked').each(function() {
			 allFirmsCounter++;
		    });
		 
		 $('#individualfirms input[type=checkbox]:not(:checked)').each(function() {
			 allNotSelectedFirms++;
			});
			
		 if(allNotSelectedFirms == 0)
		  {
			$("#firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms" checked="true"/> Firms ('+allFirmsCounter+') Selected');
		  }
		 else
		  {
			$("#firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/> Firms ('+allFirmsCounter+') Selected');
		  }
			if(allFirmsCounter == 0)
			{
				$('#firm_watchlist').prop("checked", false);
				$('#firm_individual').prop("checked", false);
				$('#all_firm').prop("checked", false);
				$('#org_individual').prop("checked", false);
				$('#individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
				$('#individualfirms input[type=checkbox]:checked').removeAttr('checked');
				$('#individualfirmsOrganization input[type=checkbox]:checked').removeAttr('checked');

				var watchListId = $('#defaultWatchListId').val();
				
				$('#individualfirmsWatchListDIV input[type=checkbox]').each(function() {
					 if(this.value == watchListId)
						{
							$(this).prop("checked", true);
						}
				});
			}
		else
			{
				$('#firm_individual').prop("checked", true);
				$('#firm_watchlist').prop("checked", false);
				$('#all_firm').prop("checked", false);
				$('#org_individual').prop("checked", false);
				$('#individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
				$('#individualfirmsOrganization input[type=checkbox]:checked').removeAttr('checked');
				$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0) Selected');
				$(".individualfirms-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="selectAllOrganizations" /> Organizations (0) Selected');
				
				$('#individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
				
			}
	}
	
	
	function changeOnIndividualOrgs()
	{
		var allOrgsCounter = 0;
		var allOrgsNotSelectedCounter = 0;
		 $('#individualfirmsOrganization input[type=checkbox]:checked').each(function() {
			 allOrgsCounter++;
		    });
			
		 $('#individualfirmsOrganization input[type=checkbox]:not(:checked)').each(function() {
			 allOrgsNotSelectedCounter++;
		 });	
		 
		 if(allOrgsNotSelectedCounter == 0)
		 {
			 $(".individualfirms-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="selectAllOrganizations" checked="true" /> Organizations ('+allOrgsCounter+') Selected');
		 }
		 else
		 {
			 $(".individualfirms-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="selectAllOrganizations" /> Organizations ('+allOrgsCounter+') Selected');
		 }
			
			if(allOrgsCounter == 0)
			{
				$('#firm_watchlist').prop("checked", false);
				$('#firm_individual').prop("checked", false);
				$('#all_firm').prop("checked", false);
				$('#org_individual').prop("checked", false);
				$('#individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
				$('#individualfirms input[type=checkbox]:checked').removeAttr('checked');
				$('#individualfirmsOrganization input[type=checkbox]:checked').removeAttr('checked');
				
				var watchListId = $('#defaultWatchListId').val();
				
				$('#individualfirmsWatchListDIV input[type=checkbox]').each(function() {
					 if(this.value == watchListId)
						{
							$(this).prop("checked", true);
						}
				});
			}
		else
			{
				$('#firm_watchlist').prop("checked", false);
				$('#firm_individual').prop("checked", false);
				$('#all_firm').prop("checked", false);
				$('#org_individual').prop("checked", true);
				$('#individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
				$('#individualfirms input[type=checkbox]:checked').removeAttr('checked');
				$("#firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/> Firms (0) Selected');
				$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0) Selected');
			}
		
	}
	
	function changeOnIndividualLocations()
	{
		var allLocationCounter = 0;
		var loclistCounter = 0;
		
		$('#allOtherLocationsDiv input[class=allLocationsCheckBox]:checked').each(function() {
			allLocationCounter++;
		});
				 
		 $('#allOtherLocationsDiv input[type=checkbox]:not(:checked)').each(function() {
			loclistCounter++;
		});	
		 
		 if(loclistCounter == 0)
		 {
			 $("#locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="selectAllLocations" checked="true"/> Locations ('+allLocationCounter+') Selected');
		 }
		 else
		 {
			 $("#locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="selectAllLocations" /> Locations ('+allLocationCounter+') Selected');
		 }
		
			
		if(allLocationCounter == 0)
			{
				$('#loc_individual').prop("checked", false);
				$('#loc_all').prop("checked", true);
				$('#selectedLocation').val('<%=ALMConstants.ALL_LOCATIONS %>');
			}
		else
			{
				$('#loc_all, #loc_inperson, #loc_webinars').prop("checked", false);
				$('#loc_individual').prop("checked", true);
			}
	}
	
	function changeOnIndivudualPracticeAreas()
	{
		var allPracticeCounter = 0;
		var practicelistCounter = 0;
		
		$('#allOtherPracticeArea input[type=checkbox]:checked').each(function() {
			allPracticeCounter++;
		});
		
		 $('#allOtherPracticeArea input[type=checkbox]:not(:checked)').each(function() 
			{
				practicelistCounter++;
			});
			
			if(allPracticeCounter == 0)
			{
				$('#allPracticeAreaIndividual').prop("checked", false);
				$('.allPracticeArea').prop("checked", true);
				$('#selectedPracticeArea').val('<%=ALMConstants.ALL_PRACTICE_AREAS %>');
			}
		else
			{
				$('#allPracticeAreaIndividual').prop("checked", true);
				$('.allPracticeArea').prop("checked", false);
			}
			
			if(practicelistCounter == 0)
			 {
				$("#practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" checked="true"/> PracticeAreas ('+allPracticeCounter+') Selected');
			 }
			 else
			 {
				 $("#practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" /> PracticeAreas ('+allPracticeCounter+') Selected');
			 }
			
	}
	
	function clearDate()
	{	
		$('#popupDate').find("option").attr("selected", false);
		$('#popupDate').find('input[type=text]').val('');
		$('#popupDate').find('input[type=radio]:checked').removeAttr('checked');				
		$("#dateText").val('<%=ALMConstants.LAST_WEEK%>');	
		$("#period").prop("checked", true); 
		$("#datePeriodSelect").val('<%=ALMConstants.LAST_WEEK%>');	
		$("#from").datepicker('enable');
		$("#to").datepicker('enable');
		applyDate();
	}
	
	function clearFirms()
	{
		$('#popup').find("option").attr("selected", false);
		$('#popup').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#popup').find('input[type=radio]:checked').removeAttr('checked');
		
		$("#firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/> Firms (0) Selected');
		$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0) Selected');
		$(".individualfirms-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="selectAllOrganizations" /> Organizations (0) Selected');
		//$("input[class='allFirmsCheckBoxWatchList']").first().prop("checked", true);
		applyFirms();
		changeOnWatchList();
	}
	
	function clearLocations()
	{
		$('#popupLocation').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#popupLocation').find('input[type=radio]:checked').removeAttr('checked');
		$("#locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="selectAllLocations" /> Locations (0) Selected');
		$('#loc_all').prop("checked", true);
		applyLocations();
	}
	
	
	function clearPracticeAreas()
	{
		$('#popupPracticeArea').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#popupPracticeArea').find('input[type=radio]:checked').removeAttr('checked');
		$('#allOtherPracticeArea input').find('input[type=checkbox]:checked').removeAttr('checked');
		$("#practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" /> Practice Area (0) Selected');
		$('.allPracticeArea').prop('checked', true);	
		applyPracticeArea();
	}
	
	function clearKeywords()
	{
		//keyword reset
		$( "#keywords" ).val("");
		
	}
	
	function clearAll()
	{
		clearFirms();
		clearLocations();
		clearPracticeAreas();
		clearKeywords();
		clearDate();
		//initializeSearchCriteria();
	}
	
	
	
	
	function setPage(goToPage)
	{
		// Sets the page number to the one selected by the user
		// and fires an AJAX submit to refresh with the new page
		// Does NOT tinker with the sort settings
		$("#goToPage").val(goToPage);
		ajaxSubmit();
	}
	
	function sortResults(sortColumn)
	{
		// Changes the sort order to the new column selected by
		// the user along with the sort direction (ascending)
		// Also RESETS the page to 1		
		var lastSortColumn  =  $("#sortColumn").val();		
		if(lastSortColumn==sortColumn)
		{
			var lastSortOrder= $("#sortOrder").val();
			if(lastSortOrder=="asc")
			{
				$("#sortOrder").val("desc");
			}
			else if(lastSortOrder=="desc")
			{
				$("#sortOrder").val("asc");
			}
			else
			{
				$("#sortOrder").val("asc");
			}
		}
		else
		{
			$("#sortOrder").val("asc");
			
		}
		
		$("#sortColumn").val(sortColumn);
		$("#goToPage").val(1);
		search();
	}
	
	function sortDesc(sortColumn)
	{
		// Changes the sort order to the new column selected by
		// the user along with the sort direction (descending)
		// Also RESETS the page to 1
		$("#sortColumn").val(sortColumn);
		$("#sortOrder").val("desc");
		$("#goToPage").val(1);
		ajaxSubmit();
	}
	
	/**
	* Single point to fire search action
	*/
	function search()
	{
		$('#alertEventSearchModelBean').submit();
		
		<%--  $.ajax({
			    url:"<%=applySearchURL.toString()%>",
			    method: "GET",
			    data: 
			    	{
			    	"firmType":$("input[name=firmType]:checked").val(),
		    		"firmList":$("#selectedFirms").val(),
		    		"dateText":$("#dateText").val(),
			    	"practiceArea":$("#selectedPracticeArea").val(),
			    	"locations":$("#selectedLocation").val(),
			    	"keywords":$("#keywords").val(),
					"goToPage":1,
		    		"sortColumn":$("#sortColumn").val(),
			    	"sortOrder":$("#sortOrder").val(),
		    		"orderBy" :$("input[name=orderBy]:checked").val(),
		    		"searchResultsPerPage" :$("input[name=searchResultsPerPage]:checked").val()
			    },
			    success: function(data)
			        {
			    	$("#detailsPage").html(data);
			    	
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			            alert("error:" + textStatus + " - exception:" + errorThrown);
			            }
			    });  --%>

	}
	
 	function ajaxSubmit()
	{
 		
	 search();
	<%--  $.ajax({
		    url:"<%=applySearchURL.toString()%>",
		    method: "GET",
		    data: 
		    	{
		    	"firmType":$("input[name=firmType]:checked").val(),
	    		"firmList":$("#selectedFirms").val(),
	    		"dateText":$("#dateText").val(),
		    	"practiceArea":$("#selectedPracticeArea").val(),
		    	"locations":$("#selectedLocation").val(),
		    	"keywords":$("#keywords").val(),
				"goToPage":$("#goToPage").val(),
	    		"sortColumn":$("#sortColumn").val(),
		    	"sortOrder":$("#sortOrder").val(),
	    		"orderBy" :$("input[name=orderBy]:checked").val(),
	    		"searchResultsPerPage" :$("input[name=searchResultsPerPage]:checked").val()
		    	},
		    success: function(data)
		        {
		    	$("#detailsPage").html(data);
		    	
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		            alert("error:" + textStatus + " - exception:" + errorThrown);
		            }
		    });  --%>
	}
</script>
		
<script>

$.widget( "custom.catcomplete", $.ui.autocomplete, {
	_renderMenu: function( ul, items ) {
		var that = this,
		currentCategory = "";
		$.each( items, function( index, item ) {
			if ( item.category != currentCategory ) {
			ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
			currentCategory = item.category;
			}
	  that._renderItemData( ul, item );
	});
}
});

$(function() {

	var practiceData = ${practiceJson};
	var locationData = ${allOtherLocations};
	//setting all available locations
	/* var availableLocation = new Array();

		<c:forEach items="${allOtherLocations}" var="loc">
			availableLocation.push("${loc}");
		</c:forEach> */

	function split( val ) {
		return val.split( /,\s*/ );
	}

	function extractLast( term ) {
		return split( term ).pop();
	}


	$("#selectedLocation")
	// don't navigate away from the field on tab when selecting an item
	.bind( "keydown", function( event ) {
	  if ( event.keyCode === $.ui.keyCode.TAB &&
	      $( this ).data( "ui-autocomplete" ).menu.active ) {
	    event.preventDefault();
	  }
	})
	.autocomplete({
	  minLength: 3,
	  source: function( request, response ) {
	    // delegate back to autocomplete, but extract the last term
	    response( $.ui.autocomplete.filter(
	    		locationData, extractLast( request.term ) ) );
	  },
		change: function(event, ui) {		
			
			$('#popup').find('input[type=checkbox]:checked').removeAttr('checked');
			$('#popup').find('input[type=radio]:checked').removeAttr('checked');
			this.value = this.value.substring(0, this.value.length - 1);
			var selectedValues = this.value.split(',');
			var output = "";
			var currentvalue="";
			
			$.each(selectedValues, function(key, line) {
				line=line.trim();	
				
				if(line!=""){	
					currentvalue=$("input[locationvalue='"+line+"'].allLocationsCheckBox").val();
					if(typeof(currentvalue)!="undefined"){
						output= output + line + ",";
						$("input[locationvalue='"+line+"'].allLocationsCheckBox").prop("checked", true);
						allOtherLocsChange();
					}	
				}
				
			});	
			$('#selectedLocation').val(output);		
	    },
	  focus: function() {
	    // prevent value inserted on focus
	    return false;
	  },
	  select: function( event, ui ) {
	    var terms = split( this.value );
	    // remove the current input
	    terms.pop();
	    // add the selected item
	    terms.push( ui.item.value );
	    // add placeholder to get the comma-and-space at the end
	    terms.push("");
		
	    this.value = terms.join(",");
	    return false;
	  }
	});
	
	function allOtherLocsChange()
	{
		var allLocsCounter = 0;
		$('#allOtherLocationsDiv input[type=checkbox]:checked').each(function() 
		{
			allLocsCounter++;
		});
		$("#locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="selectAllLocations" /> Locations ('+allLocsCounter+') Selected');
		
		if(allLocsCounter > 0)
		{
			 $('#loc_individual').prop("checked", true);
 			 $('#loc_all').prop("checked", false);
			 $('#loc_inperson').prop("checked", false);
			 $('#loc_webinars').prop("checked", false);
		}
		else
		{
			$('#loc_individual').prop("checked", false);
		}
	}

});
// Checking for selected date validation
function ValidateDate(txtDate)
{
    var currVal = txtDate;
    if(currVal == '')
        return false;
    
    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
    var dtArray = currVal.match(rxDatePattern); // is format OK?
    
    if (dtArray == null) 
        return false;
    
    //Checks for mm/dd/yyyy format.
    dtMonth = dtArray[1];
    dtDay= dtArray[3];
    dtYear = dtArray[5];        
    
    if (dtMonth < 1 || dtMonth > 12) 
        return false;
    else if (dtDay < 1 || dtDay> 31) 
        return false;
    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
        return false;
    else if (dtMonth == 2) 
    {
        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
        if (dtDay> 29 || (dtDay ==29 && !isleap)) 
                return false;
    }
    return true;
}

$(function() 
{
	var data = ${firmJson};
	var practiceData = ${practiceJson};
	
	function split( val ) 
	{
		return val.split( /,\s*/ );
	}

	function extractLast( term ) 
	{
		return split( term ).pop();
	}

	$( "#selectedPracticeArea" )
		//don't navigate away from the field on tab when selecting an item
		.bind( "keydown", function( event ) 
		{
			if ( event.keyCode === $.ui.keyCode.TAB && $( this ).data( "ui-autocomplete" ).menu.active ) 
			{
				event.preventDefault();
			}
		})
		.autocomplete({minLength: 3, source: function( request, response ) 
		{
			//delegate back to autocomplete, but extract the last term
			response( $.ui.autocomplete.filter(practiceData, extractLast( request.term )));
		},
		focus: function() 
		{
			//prevent value inserted on focus
			return false;
		},
		change: function(event, ui) {		
			
			$('#popup').find('input[type=checkbox]:checked').removeAttr('checked');
			$('#popup').find('input[type=radio]:checked').removeAttr('checked');
			this.value = this.value.substring(0, this.value.length - 1);
			var selectedValues = this.value.split(',');
			var output="";
			var currentvalue="";
			
			$.each(selectedValues, function(key, line) {
				line=line.trim();	
				
				if(line!=""){	
					currentvalue=$("input[value='"+line+"']#practiceId").val();
					
					if(typeof(currentvalue)!="undefined"){
						output= output + line + ",";
						$("input[value='"+line+"']#practiceId").prop("checked", true);
						allOtherPracticeAreasChange();
					}	
				}
					
			});		
			$('#selectedPracticeArea').val(output);
				
	    },
		select: function( event, ui ) 
		{
			var terms = split( this.value );
			//remove the current input
			terms.pop();
			//add the selected item
			terms.push( ui.item.value );
			//add placeholder to get the comma-and-space at the end
			terms.push( "" );
			this.value = terms.join( "," );
			return false;
		}
	});


$( "#Firmstext" ).catcomplete({
	minLength: 3,
	source: function( request, response ) {
		// delegate back to autocomplete, but extract the last term
		response( $.ui.autocomplete.filter(
		data, extractLast( request.term ) ) );
	},
	focus: function() {
		// prevent value inserted on focus
		return false;
	},
	change: function(event, ui) {		
		
		$('#popup').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#popup').find('input[type=radio]:checked').removeAttr('checked');
		
		this.value = this.value.substring(0, this.value.length - 2);
		var selectedValues = this.value.split(',');
		
		var output = '';
		var currentvalue="";		
		$.each(selectedValues, function(key, line) {
			line=line.trim();
			
			if(line!=""){				
				currentvalue=$("input[labelattr='"+line+"'].allFirmsCheckBoxFirms").val();
				if(typeof(currentvalue)!="undefined"){
					output= output + currentvalue+ ",";
					$("input[labelattr='"+line+"'].allFirmsCheckBoxFirms").prop("checked", true);
					allOtherFirmChange();
				}	
							
			}			
		});		
		
		if(output == '')
		{
			$.each(selectedValues, function(key, line) {
				line=line.trim();		
				if(line!=""){				
					currentvalue=$("input[labelattr='"+line+"'].allFirmsCheckBoxWatchList").val();
					if(typeof(currentvalue)!="undefined"){
							output= output + currentvalue+ ",";	
						$("input[labelattr='"+line+"'].allFirmsCheckBoxWatchList").prop("checked", true);
						allOtherWatchlistChange();
					}	
								
				}			
			});
		}
		
		if(output == '')
		{
			$.each(selectedValues, function(key, line) {
				line=line.trim();		
				if(line!=""){				
					currentvalue=$("input[labelattr='"+line+"'].allFirmsCheckBoxOrg").val();
					if(typeof(currentvalue)!="undefined"){
						output= output + currentvalue+ ",";	
						$("input[labelattr='"+line+"'].allFirmsCheckBoxOrg").prop("checked", true);
						allOtherOrgsChange();
					}	
								
				}			
			});
		}
		output = output.substring(0, output.length - 1);
		$('#selectedFirms').val(output);		
    },
	select: function( event, ui ) {
		var terms = split( this.value );		
		var termsValue = split( this.id );
		
		if( ui.item.value.indexOf("AmLaw 100") !== -1){
			return false;
		}
		
		// remove the current input
		terms.pop();
		termsValue.pop();
		// add the selected item
		
		terms.push( ui.item.value );
		termsValue.push( ui.item.id );
		// add placeholder to get the comma-and-space at the end
		terms.push( "" );
		termsValue.push( "" );
		this.value = terms.join( ", " );
		
		$('#selectedFirms').val($('#selectedFirms').val().replace("AmLaw 100","") + "," + ui.item.id);
		var sleVar=$('#selectedFirms').val();
		return false;
	}
});

function allOtherFirmChange()
{
	var allFirmsCounter = 0;
	$('#individualfirms input[type=checkbox]:checked').each(function() 
	{
		allFirmsCounter++;
	});
	$("#individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms" /> Firms ('+allFirmsCounter+') Selected');
	
	if(allFirmsCounter > 0)
	{
		 $('#firm_individual').prop("checked", true);
	}
	else
	{
		$('#firm_individual').prop("checked", false);
	}
}

function allOtherWatchlistChange()
{
	var allWatchListsCounter = 0;
	$('#individualfirmsWatchListDIV input[type=checkbox]:checked').each(function() 
	{
		allWatchListsCounter++;
	});
	$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists ('+allWatchListsCounter+') Selected');
	
	if(allWatchListsCounter > 0)
	{
		 $('#firm_watchlist').prop("checked", true);
	}
	else
	{
		$('#firm_watchlist').prop("checked", false);
	}
}
function allOtherOrgsChange()
{
	var allOrgsCounter = 0;
	$('#individualfirmsOrganization input[type=checkbox]:checked').each(function() 
	{
		allOrgsCounter++;
	});
	$("#organizationsCounter").html('<input type="checkbox" value="selectAllOrganizations" id="selectAllOrganizations" />  Organizations ('+allOrgsCounter+') Selected');
	
	if(allOrgsCounter > 0)
	{
		 $('#org_individual').prop("checked", true);
	}
	else
	{
		$('#org_individual').prop("checked", false);
	}
}

function allOtherPracticeAreasChange()
{
	var allPracticeAreasCounter = 0;
	$('#allOtherPracticeArea input[type=checkbox]:checked').each(function() 
	{
		allPracticeAreasCounter++;
	});
	$("#practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" /> Practice Areas ('+allPracticeAreasCounter+') Selected');
	
	if(allPracticeAreasCounter > 0)
	{
		 $('#allPracticeAreaIndividual').prop("checked", true);
		 $('.allPracticeArea').prop("checked", false);
	}
	else
	{
		$('#allPracticeAreaIndividual').prop("checked", false);
	}
}


	$( "#from" ).datepicker({
		changeMonth: true,
		changeYear: true,
		showOn: "button",
		buttonImage: "<%=request.getContextPath()%>/images/calendar.gif",
		buttonImageOnly: true,
		defaultDate: "+1w",
		onClose: function( selectedDate ) {
		$( "#to" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	
	$( "#to" ).datepicker({
		changeMonth: true,
		changeYear: true,
		showOn: "button",
		buttonImage: "<%=request.getContextPath()%>/images/calendar.gif",
		buttonImageOnly: true,
		defaultDate: "+1w",
		onClose: function( selectedDate ) {
		$( "#from" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
});
</script>

<script>

$(document).ready(function(){
	// Show or hide popup
	$('#hide, #popup').click(function(e){		
		 $("#popupPracticeArea").hide();
		 $("#popupFirmSize").hide();
		 $("#popupLocation").hide();
	     e.stopPropagation();	   
	});  
	// Show or hide PracticeArea popup
	 $('#practiceAreaId, #popupPracticeArea').click(function(e){
		 $("#popup").hide(); 
		 $("#popupFirmSize").hide();
		 $("#popupLocation").hide();
	     e.stopPropagation();   
	});
	// Show or hide Firm popup
	$('#firmSizeId, #popupFirmSize').click(function(e){
		 $("#popup").hide(); 
		 $("#popupPracticeArea").hide();
		 $("#popupLocation").hide();
	     e.stopPropagation();   
	});
	
	// Show or hide Location popup
	$('#locationId, #popupLocation').click(function(e){
	
		 $("#popup").hide(); 
		 $("#popupPracticeArea").hide();
		 $("#popupFirmSize").hide();
	     e.stopPropagation();   
	});

 	  $("input[id='allOtherLocation']").click(function(){
 		$('#allOtherLocationsDiv input[type=checkbox]:checked').removeAttr('checked');
 		$("#locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="selectAllLocations" /> Locations (0) Selected');
		});  
	
	$(document).click(function(){
	    $("#popup").hide(); 
	    $("#popupPracticeArea").hide();
	    $("#popupFirmSize").hide();
	    $("#popupLocation").hide();		
	});
	
	 $('.rivaledgeListAMLAW_100').bind('click', function (event) {
			
		 $('.rivaledgeListAMLAW_100').prop("checked", true);
		 $('.rivaledgeListAMLAW_200').prop("checked", false);
		 $('.rivaledgeListNLJ_250').prop("checked", false);
		 $('#firm_watchlist').prop("checked", true);
		 $('#individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
	 	 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0 Selected)');
	 	
	 	$("#firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/> Firms (0) Selected');
		$(".individualfirms-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="selectAllOrganizations" /> Organizations (0) Selected');
		$('#individualfirms input[type=checkbox]:checked').removeAttr('checked');
		$('#individualfirmsOrganization input[type=checkbox]:checked').removeAttr('checked');

		$("#Firmstext").val('<%=ALMConstants.AMLAW_100 %>');
		$("#selectedFirms").val('<%=ALMConstants.AMLAW_100 %>');
		$('#firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
		$('#individualfirms input[type=checkbox]:checked').removeAttr('checked');
		$('#individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
		
	});
	
	$('.rivaledgeListAMLAW_200').bind('click', function (event) {
		
		 $('.rivaledgeListAMLAW_100').prop("checked", false);
		 $('.rivaledgeListAMLAW_200').prop("checked", true);
		 $('.rivaledgeListNLJ_250').prop("checked", false);
		 $('#firm_watchlist').prop("checked", true);
		 $('#individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
	 	 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0 Selected)');
		 	
	 	 $("#firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/> Firms (0) Selected');
			$(".individualfirms-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="selectAllOrganizations" /> Organizations (0) Selected');
			$('#individualfirms input[type=checkbox]:checked').removeAttr('checked');
			
			$('#individualfirmsOrganization input[type=checkbox]:checked').removeAttr('checked');
			$("#Firmstext").val('<%=ALMConstants.AMLAW_200 %>');
			$("#selectedFirms").val('<%=ALMConstants.AMLAW_200 %>');
			$('#firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
			$('#individualfirms input[type=checkbox]:checked').removeAttr('checked');
			$('#individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');

	});
	
	$('.rivaledgeListNLJ_250').bind('click', function (event) {
		
		 $('.rivaledgeListAMLAW_100').prop("checked", false);
		 $('.rivaledgeListAMLAW_200').prop("checked", false);
		 $('.rivaledgeListNLJ_250').prop("checked", true);
		 $('#firm_watchlist').prop("checked", true);
		 $('#individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
	 	 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0 Selected)');
		 	
	 	 $("#firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/> Firms (0) Selected');
			$(".individualfirms-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="selectAllOrganizations" /> Organizations (0) Selected');
			$('#individualfirms input[type=checkbox]:checked').removeAttr('checked');
			$('#individualfirmsOrganization input[type=checkbox]:checked').removeAttr('checked');

			$("#Firmstext").val('<%=ALMConstants.NLJ_250 %>');
			$("#selectedFirms").val('<%=ALMConstants.NLJ_250 %>');
			$('#firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
			$('#individualfirms input[type=checkbox]:checked').removeAttr('checked');
			$('#individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
	});
	
	//Firm Size's toggle off
	$('#firmSizeId').click(
			function () {
			    $("#popupFirmSize").stop().slideToggle(500);    
		}); 
	//Practice Area's toggle off
	$('#practiceAreaId').click(
			function () {
			    $("#popupPracticeArea").stop().slideToggle(500);    
		});
	//Location's toggle off
	 $('#locationId').click(
		    function () {
		        $("#popupLocation").stop().slideToggle(500);    
		}); 
	
	$('#fromdateError').hide();
	$('#todateError').hide();
	$('#dateValidError').hide();
	
	$("#popupDate").hide(); 

$('#contentTypeId, #popupContentType').click(function(e){
	 $("#popup").hide(); 
	 $("#popupDate").hide();
	 $("#popupPracticeArea").hide();
    e.stopPropagation();   
});


$('#datenone, #popupDate').click(function(e){
	 $("#popup").hide(); 
	 $("#popupContentType").hide();
	 $("#popupPracticeArea").hide();
   e.stopPropagation();   
});  

$('#ui-datepicker-div').click(function(e){
	 $("#popup").hide(); 
	 $("#popupContentType").hide();
	 $("#popupPracticeArea").hide();
   e.stopPropagation();   
});


$(document).click(function(){
    $("#popup").hide(); 
    $("#popupContentType").hide();
    $("#popupDate").hide();
    $("#popupPracticeArea").hide();
	
});

var date = new Date();
currentDate = (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear();
date.setDate(date.getDate() - 7);
lastWeekDate = (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear();

$('#btnAdd').click(function(){
	$('.filtersPage').hide();
	$('#additional').show();
})
$('#btnSave').click(function(){
	$('.filtersPage').show();
	$('#additional').hide();
});

	    	
$('#hide').click(
    function () {
        $("#popup").stop().slideToggle(500); 
    });
    
	$('#contentTypeId').click(
    function () {
        //show its submenu        
        $("#popupContentType").stop().slideToggle(500);    
    });

	$('#datenone').click(
	function () {
	    //show its submenu
	    $("#popupDate").stop().slideToggle(500);    
	}); 

// Function for Event search based on selected criteria like Firms,Practice Area,Location, Dates and Keywords
 $("#applySearch").click(function() {	
	$("#goToPage").val(1);
	search();
}); 

	$("input[id='allfirms']").change(function(){
	var checked = $("#allfirms").is(":checked");
	if(checked){   
	 $("#allOtherFirmDiv input").attr("disabled", true);
	 $('#allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
	 $('#popup option:selected').removeAttr("selected");
	 $('#ApplyFirm').removeAttr("disabled");
	 $('#ApplyFirm').removeClass("ui-state-disabled");
	}
	});
	
	$("input[id='allOtherFirm']").change(function(){
	var checked = $("#allOtherFirm").is(":checked");
	if(checked){   
	 $("#allOtherFirmDiv input").removeAttr('disabled');
	   $('#popup option:selected').removeAttr("selected");
	}else{
	 $("#allOtherFirmDiv input").attr("disabled", true);
	}
	
	});
	
	$("input[id='allFirmsCheckBox']").change(function(){
	var lengthofCheckBoxes = $("input[id='allFirmsCheckBox']:checked").length;
	if(lengthofCheckBoxes>0){
		$('#ApplyFirm').removeAttr("disabled");
		$('#ApplyFirm').removeClass("ui-state-disabled");
	}
	
	});
	
	/* Date period selection logic*/
	$("#datePeriodSelect").change(function() {
		 $("#period").prop("checked", true);
		 $("#dateRange").removeAttr("checked");
		 applyDate();
	});

	$("#from, #to").change(function() {
		 $("#dateRange").prop("checked", true);
		 $("#period").removeAttr("checked");
		 applyDate();
	});
	
	 $('.allFirmsCheckBoxWatchList').click(function() {
	
	 $('#firm_watchlist').prop("checked", true);
	 $('#org_individual').prop("checked", false);
	 
	 $('#all_firm_org').prop("checked", false);
	 $('#all_firm').prop("checked", false);
	 $('#firm_individual').prop("checked", false);
	 $('#all_org').prop("checked", false);
	 $('#org_individual').prop("checked", false);
	 
	 $('#individualfirms').find('input[type=checkbox]:checked').removeAttr('checked');
	 $('#individualfirmsOrganization').find('input[type=checkbox]:checked').removeAttr('checked');
	 $("#firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/> Firms (0 Selected)');
	 $(".individualfirms-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="selectAllOrganizations" /> Organizations (0 Selected)');
	 $('#firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
	 
	});
  
 $('.individualfirms-Watchlist').click(function() {
		var checked = $("#selectAllWatchList").is(":checked");
		var allWatchListCounter = 0;
		var watchlistCounter = 0;
		var isFlag = true;
		
		if(checked){   
			 //$('#individualfirmsWatchList .allFirmsCheckBoxWatchList').prop('checked', true);
			 $('#individualfirmsWatchListDIV .allFirmsCheckBoxWatchList').prop('checked', true);
			 $('#selectAllWatchList').prop("checked", true);
			 $('#firm_watchlist').prop("checked", true);
			 $('#all_firm_org').prop("checked", false);
			 $('#all_firm').prop("checked", false);
			 $('#firm_individual').prop("checked", false);
			 $('#all_org').prop("checked", false);
			 $('#org_individual').prop("checked", false);
			 $('#individualfirms').find('input[type=checkbox]:checked').removeAttr('checked');
			 $('#individualfirmsOrganization').find('input[type=checkbox]:checked').removeAttr('checked');
			 $('#firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
			 
		}else{
			 $('#individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
			 $('#selectAllWatchList').prop("checked", false);
			 
			 var watchListId = $('#defaultWatchListId').val();
				
				$('#individualfirmsWatchListDIV input[type=checkbox]').each(function() {
					 if(this.value == watchListId)
						{
							$(this).prop("checked", true);
							isFlag = false;
						}
				});
				$('#firm_watchlist').prop("checked", true);
				if(isFlag == true)
				{
					$(".rivaledgeListAMLAW_100").prop("checked", true);
				}
		}
		
		/*  $('#individualfirmsWatchListDIV :checked').each(function() {
			 allWatchListCounter++;
		 });
			
		 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists ('+allWatchListCounter+') Selected'); */
		 
		 $('#individualfirmsWatchListDIV input[type=checkbox]:checked').each(function() {
			 allWatchListCounter++;
			});
		 
			 $('#individualfirmsWatchListDIV input[type=checkbox]:not(:checked)').each(function() 
				{
					 	watchlistCounter++;
				});
			 
			 if(watchlistCounter == 0)
			 {
				 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" checked="true"/> Watchlists ('+allWatchListCounter+') Selected');
			 }
			 else
			 {
				 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList"/> Watchlists ('+allWatchListCounter+') Selected');
			 }
	});

 
 $('#firmsCounter').click(function() {
	 
		var checked = $("#selectAllFirms").is(":checked");
		var allFirmsListCounter = 0;
		var isFlag = true;
		if(checked)
		{   
			 $('#individualfirms .allFirmsCheckBoxFirms').prop('checked', true);
		}
		else
		{
			$('#individualfirms .allFirmsCheckBoxFirms').prop('checked', false);
		}
		
		 $('#individualfirms input[type=checkbox]:checked').each(function() {
			 allFirmsListCounter++;
		 });
		 
		 if(allFirmsListCounter == 0)
			 {
				 $("#firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/> Firms ('+allFirmsListCounter+') Selected');
				 $('#firm_individual').prop("checked", false);
				 $('#selectAllFirms').prop("checked", false);
				 
					var watchListId = $('#defaultWatchListId').val();
					
					$('#individualfirmsWatchListDIV input[type=checkbox]').each(function() {
						 if(this.value == watchListId)
							{
								$(this).prop("checked", true);
								$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (1) Selected');
								isFlag = false;
							}
					});
					$('#firm_watchlist').prop("checked", true);
					if(isFlag == true)
					{
						$(".rivaledgeListAMLAW_100").prop("checked", true);
					}
			 }
		 else
			 {
			 
				 $("#firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/> Firms ('+allFirmsListCounter+') Selected');
				 $('#firm_individual').prop("checked", true);
				 $('#selectAllFirms').prop("checked", true);
				 $('#individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
				 $('#firm_watchlist').prop("checked", false);
				 $(".rivaledgeListAMLAW_100").prop("checked", false);
				 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0) Selected');
				 $('#individualfirmsOrganization input[type=checkbox]:checked').removeAttr('checked');
				 $(".individualfirms-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="selectAllOrganizations" /> Organizations (0) Selected');
			 }
		 
		
	});
 
 $('#locationsCounter').click(function() {
	 
		var checked = $("#selectAllLocations").is(":checked");
		var allLocationsListCounter = 0;
		
		if(checked)
		{   
			 $('#allOtherLocationsDiv .allLocationsCheckBox').prop('checked', true);
		}
		else
		{
			$('#allOtherLocationsDiv .allLocationsCheckBox').prop('checked', false);
		}
		
		 $('#allOtherLocationsDiv input[type=checkbox]:checked').each(function() {
			 allLocationsListCounter++;
		 });
		 
		
		 
		 if(allLocationsListCounter == 0)
			 {
				 $("#locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="selectAllLocations" /> Locations ('+allLocationsListCounter+') Selected');
				 $('#loc_individual').prop("checked", false);
				 $('#selectAllLocations').prop("checked", false);
				 $('#loc_all').prop("checked", true);
				 $('#selectedLocation').val('<%=ALMConstants.ALL_LOCATIONS %>');
				 
			 }
		 else
			 {
				 $("#locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="selectAllLocations" /> Locations ('+allLocationsListCounter+') Selected');
				 $('#loc_individual').prop("checked", true);
				 $('#selectAllLocations').prop("checked", true);
				 $('#loc_all').prop("checked", false);
				 $('#loc_inperson').prop("checked", false);
				 $('#loc_webinars').prop("checked", false);
			 }
		
	});
 
 $('#practiceAreaCounter').click(function() {
	 
		var checked = $("#selectAllPracticeAreas").is(":checked");
		var allPracticeAreasListCounter = 0;
		
		if(checked)
		{   
			 $('#allOtherPracticeArea #practiceId').prop('checked', true);
			 $('#practiceArea1').prop('checked', false);
		}
		else
		{
			$('#allOtherPracticeArea #practiceId').prop('checked', false);
		}
		
		 $('#allOtherPracticeArea input[type=checkbox]:checked').each(function(){
			 allPracticeAreasListCounter++;
		 });
		 
		 if(allPracticeAreasListCounter == 0)
			 {
				 $("#practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" /> Practice Areas ('+allPracticeAreasListCounter+') Selected');
				 $('#allPracticeAreaIndividual').prop("checked", false);
				 $('#selectAllPracticeAreas').prop("checked", false);
				 $('.allPracticeArea').prop("checked", true);
				 $('#selectedPracticeArea').val('<%=ALMConstants.ALL_PRACTICE_AREAS %>');

			 }
		 else
			 {
				 $("#practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" /> Practice Areas  ('+allPracticeAreasListCounter+') Selected');
				 $('#allPracticeAreaIndividual').prop("checked", true);
				 $('#selectAllPracticeAreas').prop("checked", true);
			 }
	}); 
	
$('#organizationsCounter').click(function() {
	 
	var checked = $("#selectAllOrganizations").is(":checked");
	var allOrgCounter = 0;
	var isFlag = true;
	
	if(checked)
	{   
		 $('#individualfirmsOrganization .allFirmsCheckBoxOrg').prop('checked', true);
	}
	else
	{
		$('#individualfirmsOrganization .allFirmsCheckBoxOrg').prop('checked', false);
	}
	
	 $('#individualfirmsOrganization input[type=checkbox]:checked').each(function() {
		 allOrgCounter++;
	 });
	 
	 if(allOrgCounter == 0)
		 {
			 $("#organizationsCounter").html('<input type="checkbox" value="selectAllOrganizations" id="selectAllOrganizations" /> Organizations ('+allOrgCounter+') Selected');
			 $('#org_individual').prop('checked', false);
			 
				var watchListId = $('#defaultWatchListId').val();
				
				$('#individualfirmsWatchListDIV input[type=checkbox]').each(function() {
					 if(this.value == watchListId)
						{
							$(this).prop("checked", true);
							$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (1) Selected');
							isFlag = false;
						}
				});
				$('#firm_watchlist').prop("checked", true);
				
				if(isFlag == true)
				{
					$(".rivaledgeListAMLAW_100").prop("checked", true);
				}
		 }
	 else
		 {
			 $("#organizationsCounter").html('<input type="checkbox" value="selectAllOrganizations" id="selectAllOrganizations" /> Organizations ('+allOrgCounter+') Selected');
			 $('#org_individual').prop('checked', true);
			 $('#selectAllOrganizations').prop('checked', true);
			 $('#all_firm_org').prop('checked', false);
			 $('#firm_watchlist').prop('checked', false);
			 $('#all_firm').prop('checked', false);
			 $('#firm_individual').prop('checked', false);
			 $('#all_org').prop('checked', false);
			 $("#firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms" /> Firms (0) Selected');
			 $('#individualfirms input[type=checkbox]:checked').removeAttr('checked');
			 $('#individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
			 $('#firm_watchlist').prop("checked", false);
			 $(".rivaledgeListAMLAW_100").prop("checked", false);
			 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0) Selected');
		 }
	
}); 	
 
$('#resetAll').bind('click', function (event) {
	clearAll();
});

	$('#clearButton').bind('click', function (event) {
		clearFirms();
		//$("#popup").toggle();
	});
	
	$('#clearButtonContentType').bind('click', function (event) {
		$('#popupContentType').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#allContentType').prop('checked', true);
		$("#allOtherContentType input").attr("disabled", true);	
	});
	
	$('#clearButtonDate').bind('click', function (event) {
		clearDate();
	}); 

	$('#clearPracticeArea').bind('click', function (event) {
		clearPracticeAreas();	
	});
	
	$('#clearButtonLocations').bind('click', function (event) {
		clearLocations();
	});
	
// Function for showing selecting firms 	
	$('#ApplyFirm').bind('click', function (event) {
			applyFirms();
			$("#popup").toggle();
		});
	
// Function for showing selecting date 
$('#ApplyDate').bind('click', function (event) {
	var checkDate = applyDate();
	if(!checkDate){
	 $("#popupDate").toggle();
	}

	});
	
//Function for showing selected Practice Area 
$('#ApplyPracticeArea').bind('click', function (event) {
	applyPracticeArea();
	 $("#popupPracticeArea").toggle();

});

//Function for showing selected Location 
$('#ApplyLocations').bind('click', function (event) {		
		applyLocations();
	 $("#popupLocation").toggle();	
});

//Function for showing selected Location counter
$("input[class='allLocationsCheckBox']").change(function(){
	changeOnIndividualLocations();
	applyLocations();
});  

//Function for showing selected PracticeArea counter
$("#practiceAreaDiv input[type=checkbox]").change(function(){
	changeOnIndivudualPracticeAreas();
	applyPracticeArea();
});

//Function for showing selected Organizations and their counter for firms drop down
$("input[class='allFirmsCheckBoxOrg']").change(function(){
	changeOnIndividualOrgs();
	applyFirms();
});
//Function for showing selected firms and their counter for firms drop down
$("input[class='allFirmsCheckBoxFirms']").change(function(){
	changeOnIndividualFirms();
	applyFirms();
});

//Function for showing selected Watchlist and their counter for firms drop down
$("input[class='allFirmsCheckBoxWatchList']").change(function(){
	 var allWatchListCounter = 0;
	 var watchlistCounter = 0;
	 $('.rivaledgeListAMLAW_100').prop("checked", false);
	 $('.rivaledgeListAMLAW_200').prop("checked", false);
	 $('.rivaledgeListNLJ_250').prop("checked", false);
	 
	 $('#individualfirmsWatchListDIV input[type=checkbox]:checked').each(function() {
		 allWatchListCounter++;
	 });
		
	 $('#individualfirmsWatchListDIV input[type=checkbox]:not(:checked)').each(function() 
		{
			watchlistCounter++;
		});
			 
			 if(watchlistCounter == 0)
			 {
				 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" checked="true"/> Watchlists ('+allWatchListCounter+') Selected');
			 }
			 else
			 {
				 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList"/> Watchlists ('+allWatchListCounter+') Selected');
			 }
	 
	 
		//$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList"/> Watchlists ('+allWatchListCounter+') Selected');
	
});

//Firms selection

//When "All-" radio button(usually starts with "All " is selected then deselect all the children checkboxes(starts with Indivudual) 
//Note : The individual radio buttons are deselected automatically
$("#all_firm_org, #all_firm, #all_org").change(function(){
	if($(this).is(":checked"))
	{
		$("#firmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/>  Firms (0) Selected');
		$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0) Selected');
		$(".individualfirms-Organization").html('<input type="checkbox" value="selectAllOrganizations" id="selectAllOrganizations" /> Organizations (0) Selected');
		$('#individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
		$('#individualfirms input[type=checkbox]:checked').removeAttr('checked');
		$('#individualfirmsOrganization input[type=checkbox]:checked').removeAttr('checked');
		applyFirms();
	}
	
});

// This is for "Individual" radio buttons
$("#firm_watchlist, #firm_individual, #org_individual").change(function(){
	if($(this).is(":checked"))
	{
		changeOnWatchList();
		changeOnIndividualFirms();
		changeOnIndividualOrgs();
	}
});

//Practice Area  selection
$(".allPracticeArea").change(function(){
	if($(this).is(":checked"))
	{
		
		$("#practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" /> Practice Area (0) Selected');
		$('#allOtherPracticeArea input[type=checkbox]:checked').removeAttr('checked');
		$('#allPracticeAreaIndividual').removeAttr('checked');
		applyPracticeArea();
	}
});


$("#allPracticeAreaIndividual").change(function(){
	if($(this).is(":checked"))
	{
		changeOnIndivudualPracticeAreas();
	}
});

//Locations  selection

$("#loc_all, #loc_inperson, #loc_webinars").change(function(){
	if($(this).is(":checked"))
	{
		
		$("#locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="selectAllLocations" /> Locations (0) Selected');
		$('#allOtherLocationsDiv input[type=checkbox]:checked').removeAttr('checked');
		$('#loc_individual').removeAttr('checked');
		applyLocations();
	}
	
});

//This is for "Individual" radio buttons
$("#loc_individual").change(function(){
	if($(this).is(":checked"))
	{
		changeOnIndividualLocations();
	}
	
});

 $("#additionalFilter").click(function () {
  var link = $(this);

  $('#additionalFilterDiv').slideToggle('slow', function() {
            if ($(this).is(":visible")) {
                 link.text('Hide Filter');                
            } else {
                 link.text('Additional Filter');                
            }        
        });
});
 
	//Spring will add <input> tags with names prefixed with "_" for type "radio" and "checkbox".
	//On form POST submit they all are carried to server which is unneccessary
	//hence removing all such input box to make page and jQuery selections less heavy :) on document ready.
	
	//NOTE : If any inputs added with name prefixed with "_" purposefully will also get removed. So be cautious
	$("#alertEventSearchModelBean input[name^=_]").remove();
	//autoselect previously selected search critria
	initializeSearchCriteria();
	
	//fire a search for the default search criteria
	//search();
});

	if(<%=isHomePage%>)
	{
		
		$('#homePageShowEvent').hide();
	}
	else
	{
		$('#homePageShowEvent').show();
	}
 	
</script>
<%
	String displayStr = "";
	if(isHomePage)
	{
		displayStr = "display:none";
	}
	
%>
<div id="homePageShowEvent" style="<%=displayStr%>" >
<form:form class="beanForm"  commandName="alertEventSearchModelBean" method="post" action="${submitURL}" id="alertEventSearchModelBean">
 	<form:hidden path="goToPage"  id="goToPage" />
 	<form:hidden path="sortColumn" id="sortColumn" />
 	<form:hidden path="sortOrder"  id="sortOrder" />

<div class="breadcrumbs"><span>EVENTS</span></div>
<div id="dropdown">

<ul id="droplist">
 
 <li>
        <label>Firm(s) and/or Organizations</label>
        <div class="srchBox">
        
        <!-- Condition for showing Firm/Watchlist default view for Firm(s) drop down -->
        
           <c:choose>
              <c:when test="${empty allWatchListsDefaultList or allWatchListsDefaultList == ''}">
          			<input type="text" name="Firmstext" id="Firmstext"  value="<%=ALMConstants.AMLAW_100 %>" style="text-overflow:ellipsis;" class="input" />
          	  </c:when>
          	  <c:otherwise>
          			<input type="text" name="Firmstext" id="Firmstext"  value="${allWatchListsDefaultList.groupName}" style="text-overflow:ellipsis;" class="input" />
          	  </c:otherwise>
          	</c:choose>
          	<input type="button" name="search" value="" class="srchBack" id="testDiv" />
          <div class="clear">&nbsp;</div>
        </div>
        <input type="button" name="search" id="hide"  value="" class="typeSel" />
       
        <div class="rel">
		 <div  id="popup" class="firmPage">
			<p>
                <form:radiobutton path="firmType" id="all_firm_org" value ="<%=ALMConstants.ALL_FIRMS_ORGANIZATION %>"/><%=ALMConstants.ALL_FIRMS_ORGANIZATION %></p>
              	<p> <form:radiobutton path="firmType" id="firm_watchlist" value=""/>Select Watchlist/RivalEdge List</p>
             	<div class="individualfirms-Watchlist" >
                	<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0 Selected)<br />
               </div>
 				 <div  class="individualfirmsWatchList" id="individualfirmsWatchList"> 
 				 
 				 	<form:checkbox  class="rivaledgeListAMLAW_100" path="firmList"  value="<%=ALMConstants.AMLAW_100 %>" /><%=ALMConstants.AMLAW_100 %><br>
					<form:checkbox  class="rivaledgeListAMLAW_200" path="firmList"  value="<%=ALMConstants.AMLAW_200 %>" /><%=ALMConstants.AMLAW_200 %><br>
					<form:checkbox  class="rivaledgeListNLJ_250" path="firmList"  value="<%=ALMConstants.NLJ_250 %>" /><%=ALMConstants.NLJ_250 %><br>
  
 				 	<div  class="individualfirmsWatchListDIV" id="individualfirmsWatchListDIV"> 
		                <c:forEach var="watchlist"  items="${allWatchLists}">     
	    	  				 <form:checkbox  class="allFirmsCheckBoxWatchList" path="firmListWatchList"  value="${watchlist.groupId}" labelAttr="${watchlist.groupName}" />${watchlist.groupName}<br>
	  				 	</c:forEach>
					</div>
			 	  </div> 
              <p>
                <form:radiobutton path="firmType" id="all_firm" value ="<%=ALMConstants.ALL_FIRMS %>"/><%=ALMConstants.ALL_FIRMS %>
              <p>
              <p>
                <!-- <input type="radio" name="firmIndividualsValue" id="firmIndividualsValue"/> -->
                 <form:radiobutton  id="firm_individual" path="firmType"  value="<%=ALMConstants.INDIVIDUAL_FIRMS %>"/>Select Individual Firms
                </p>
              <div class="individualfirms-first" id="firmsCounter">
                <input type="checkbox" value="selectAllFirms" id="selectAllFirms" /> Firms (0 Selected)<br />
              </div>
              <div class="individualfirms" id="individualfirms">
                <c:forEach var="firm"  items="${allFirms}">      
      				 <form:checkbox  class="allFirmsCheckBoxFirms" path="firmList"  value="${firm.companyId}" labelAttr="${firm.company}"/>${firm.company}<br>
  			 </c:forEach>
			 <br />
              </div>
              <p>
              	<form:radiobutton path="firmType" id="all_org" value ="<%=ALMConstants.ALL_ORGANIZATIONS %>"/><%=ALMConstants.ALL_ORGANIZATIONS %>
              </p>
              <p>
              	<form:radiobutton path="firmType"  id="org_individual" value="<%=ALMConstants.INDIVIDUAL_ORGS%>"/>Select Individual Organizations
              </p>
			  <div class="individualfirms-Organization" id="organizationsCounter">
                <input type="checkbox" value="selectAllOrganizations" id="selectAllOrganizations" />  Organizations (0 Selected)<br />
              </div>
              <div class="individualfirmsOrganization" id="individualfirmsOrganization">
                <c:forEach var="org"  items="${orgList}">      
      				  <form:checkbox  path="firmList" class="allFirmsCheckBoxOrg" value="${org.companyId}" labelAttr="${org.company}" />${org.company}<br>
  			 	</c:forEach>
              </div>
                <div class="popupsubmit">
                	<input type="button" class="buttonOne" value="Apply" id="ApplyFirm">
                	<input type="button" class="buttonTwo" value="Clear All" id="clearButton">
                </div>
		</div>
	   </div>
	   <c:if test="${not empty allWatchListsDefaultList}">
	    	<input type="hidden" name="defaultWatchListId" id="defaultWatchListId" value="${allWatchListsDefaultList.groupId}" />
	   </c:if>
 </li>
               <li>
					  <label>Practice Area(s)</label>
						  <div class="srchBox">
							<input type="text"  class="input" name="selectedPracticeArea" id="selectedPracticeArea"  style="text-overflow:ellipsis;"/>
							<input type="button" name="search" value="" class="srchBack" />
							<div class="clear">&nbsp;</div>
						  </div>
						 <input type="button" name="search" value=""  id="practiceAreaId" class="typeSel" />
						<div class="rel">
							  <div  id="popupPracticeArea" class="firmPage">					
									
								<p><form:radiobutton path="practiceArea" class="allPracticeArea" value="<%=ALMConstants.ALL_PRACTICE_AREAS %>" /><%=ALMConstants.ALL_PRACTICE_AREAS%></p>
						   		<p> <input  type="radio"  name="allPracticeArea"  id="allPracticeAreaIndividual" value="Select Individual Practice Areas"> Select Individual Practice Areas</p>

									<div class="individualfirms-first" id="practiceAreaCounter"> <input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" /> Practice Areas (0 Selected)<br> </div>
								  <div style="background-color:#FFFFFF;" id="practiceAreaDiv">										 

										 <div style="background-color:#FFFFFF;" id="allOtherPracticeArea">
										    <c:forEach var="practice" items="${allPracticeArea}">								
												<form:checkbox path="practiceArea" id="practiceId" value="${practice.practiceArea}"/> ${practice.practiceArea}<br>
											</c:forEach>
										 </div>	
									</div>
								<div class="popupsubmit">
								 <input type="button" class="buttonOne" value="Apply" id="ApplyPracticeArea" />
								 <input type="button" class="buttonTwo" value="Clear All" id="clearPracticeArea" />
								</div>	
						</div>
						</div>
				</li>
				
 		<li>
        		<label>Location(s)</label>
					  <div class="srchBox">
						<%-- <input type="text"  value="<%=ALMConstants.ALL_LOCATIONS %>" class="input" name="selectedLocation" id="selectedLocation"  /> --%>
						<input type="text" class="input" name="selectedLocation" id="selectedLocation"  style="text-overflow:ellipsis;"/> 
						<input type="button" name="search" value="" class="srchBack" />
						<div class="clear">&nbsp;</div>
					  </div>
					 <input type="button" name="search" value=""  id="locationId" class="typeSel" />
					<div class="rel">	 
						<div  id="popupLocation" class="firmPage">
							<p><form:radiobutton path="locations"  id="loc_all" value="<%=ALMConstants.ALL_LOCATIONS%>" /> <%=ALMConstants.ALL_LOCATIONS %></p>	
							<p><form:radiobutton path="locations"  id="loc_inperson" value="In Person" /> All In-Person</p>
							<p><form:radiobutton path="locations"  id="loc_webinars" value="Webinar"/> All Webinars</p>
							<p><input type="radio"  id="loc_individual" value="Select Individual Locations" /> Select Individual Locations</p>
							<div class="individualfirms-first" id="locationsCounter"> 

								<input type="checkbox" value="selectAllLocations" id="selectAllLocations" /> Locations (0) Selected<br> </div>
							<div style="background-color:#FFFFFF;" id="allOtherLocationsDiv">		

							<c:forEach items="${locationsMapValues}" var="loc">
								<form:checkbox path="locations" class="allLocationsCheckBox" value="${loc.value}" locationValue="${loc.key}"/>${loc.key}<br>
							</c:forEach>	
							   
							</div>
							<div class="popupsubmit">				
							<input type="button" class="buttonOne" value="Apply" id="ApplyLocations">
							<input type="button" class="buttonTwo" value="Clear All" id="clearButtonLocations">
							</div>
						</div>
					</div>
 		</li>
 
   <li>
        <label>Dates</label>
        <div class="srchBox">
           <form:input path="dateText" id="dateText" readonly="true" cssClass="input" cssStyle="width:140px"/>
        <div class="clear">&nbsp;</div>
        </div>
        <input type="button" name="search" id="datenone" value="" class="drpDwn" />
        <div class="rel">
        <div id="popupDate" class="datePage">
        	<input type="radio" name="date" id="period"  />&nbsp;Period
			<select id="datePeriodSelect">
				<option value="Any">Any</option>
				<option value="<%=ALMConstants.NEXT_WEEK%>">Next Week</option>
				<option value="<%=ALMConstants.NEXT_30_DAYS%>">Next 30 Days</option>
				<option value="<%=ALMConstants.NEXT_6_MONTHS%>">Next 6 Months</option>
				<option value="<%=ALMConstants.NEXT_YEAR%>">Next Year</option>
				<option value="Last Week">Last Week</option>
				<option value="Last 30 Days">Last 30 Days</option>
				<option value="Last 90 Days">Last 90 Days</option>
				<option value="Last 6 Months">Last 6 Months</option>
				<option value="Last Year">Last Year</option>  
				
			</select>
			<br>
			<input type="radio" name="date" id="dateRange" checked="checked"/>&nbsp;Date Range<br>

			<div class="flLeft"><label for="from">From</label>
			<input type="text" id="from" name="from"  /></div>
			<div class="flLeft"><label for="to">To</label>
			<input type="text" id="to" name="to" /></div>
			<div class="clear"></div>
			<span class="error" id="fromdateError"> Invalid  From Date</span>
			<span class="error" id="todateError"> Invalid  To Date</span>
			<span class="error" id="dateValidError"> To date should be more than from date</span>
            <div class="popupsubmit">				
                <input type="button" class="buttonOne" value="Apply" id="ApplyDate">
                <input type="button" class="buttonTwo" value="Clear All" id="clearButtonDate">
			</div>
        </div>
        </div>
    </li>
	
	<%-- Sachin: RER 317: Moved the Keyword option from addition filters to first row --%>
	<li>
		<label>Keywords</label>
   		<div class="srchBox">
	        <form:input path="keywords" id="keywords" cssClass="input" />
	        <input type="button" name="search" value="" class="srchBack" />
	     	<div class="clear">&nbsp;</div>
		</div>
	</li> 
	

	  <li>
        <label>&nbsp;</label>
         <input type="button" value="Apply" class="buttonOne" style="background:url(<%=request.getContextPath()%>/images/btn1.png) 0 0 repeat-x; border:1px solid #bf8d1f; -webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px; font:normal 12px Arial, Helvetica, sans-serif; color:#fff; padding:5px 10px; cursor:pointer; text-shadow: 0px 0px #FFF;" id="applySearch"/>
        
      </li>
      <li>
        <label>&nbsp;</label>
        <input type="button" value="Reset All" style="background:url(<%=request.getContextPath()%>/images/btn2.png) 0 0 repeat-x; border:1px solid #565656; -webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px; font:normal 12px Arial, Helvetica, sans-serif; color:#fff; padding:5px 10px; cursor:pointer; text-shadow: 0px 0px #FFF;" class="buttonTwo" id="resetAll" />
      </li>
	   <li>
        <label>&nbsp;</label>
        <input type="hidden"  name="selectedFirms" id="selectedFirms"  title="" customAttr="" />
      </li>
      </ul>
 <div class="clear">&nbsp;</div>
</div>

  <%--Remove this Div when Addition Filter div is enabled, because this div will add additional space which is not desired when the Additional filter is restored back --%>
       <div style="height: 30px;">
       </div>
		  <%--<div class="filtersPage">
		  	<div class="barSec" style="background:url(<%=request.getContextPath()%>/images/additinalBack.png) center 0 no-repeat; width:100%; height:24px; margin:10px 0">
			  <!-- <a href="#" class="dwnBx" id="btnAdd">Additional Filters</a>   --> <!-- Removed Additional filter option to fix Bug RER 317 -->
			</div>	
		  </div>
		  --%>
		  
	<%-- Sachin: RER 317 Move the Keyword search up to the first row  and Eliminate the tab for the Additional Filters pull down.  --%>
	  
	<%--  	  <div class="filtersPage" id="additional" style="display:none">
			<div id="dropdown">
			  <ul id="droplist">
				<li>
		        <label>Keywords</label>
		        <div class="srchBox">
		           <form:input path="keywords" id="keywords" cssClass="input" />
		          <input type="button" name="search" value="" class="srchBack" />
		          <div class="clear">&nbsp;</div>
		        </div>
		       
		      </li>
		      
			  </ul>
			  <div class="clear">&nbsp;</div>
			</div>
			<div class="barSec" style="background:url(<%=request.getContextPath()%>/images/additinalBack.png) center 0 no-repeat; width:100%; height:24px; margin:10px 0"><a href="#" class="upBx" id="btnSave">Hide Filter</a></div>
		--%>
  
  <%--    <div class="flRight">   	  
      <ul class="menuRight flRight">
        <li><a class="btn icon alert" href="#">Create Alert</a></li>
         <li><a class="btn icon print" href="#" onClick="openPopUp();" >Print</a></li>
        <li><a id="exports" class="btn icon export" href="#">Export</a>
          <div id="actionBox" class="actionSec" style="">
            <h5>Actions</h5>
            <ul class="reset">
              <li><span>&nbsp;</span><a href="#"  onclick="setFileType('xls')">Export to Excel</a></li>
              <li><span>&nbsp;</span><a href="#"  onclick="setFileType('csv')">Export to CSV</a></li>
            </ul>
            <div class="clear">&nbsp;</div>
          </div>
        </li>
      </ul>
      <div class="clear">&nbsp;</div>
       </div>--%>
  
   <div id="view_settings" style="display: none; position:absolute; background:#F0F0F0; width:310px !important" class="viewBox popusdiv ClickPopup newspublicationPage charts">
					<div class="popHeader">
						<a href="#" class="btn icon closewhite closeOne flRight" style="margin-top: 2px" onClick="cancelViewSettings();">&nbsp;Close</a>
							SETTINGS: Events
					</div>
                    <div class="section-two">
                      <h6>Group Results By</h6>
                        <ul class="reset list4">
                             <li><form:radiobutton path="orderBy" value="-1"/><span>None</span>
                             <li><form:radiobutton path="orderBy" value="9" /><span>Firm/Organization</span></li>
                             <li><form:radiobutton path="orderBy" value="7"/><span>Location</span></li>
                             <li><form:radiobutton path="orderBy" value="8"/><span>Practice</span></li>
                        </ul>
                        <div class="clear">&nbsp;</div>
                    </div>
					<div class="section-two" style="width:140px;">
							<h6>Display No of Results</h6>
							<ul class="reset list4">
								<li><form:radiobutton path="searchResultsPerPage"  value="25"  /><span>25</span></li>
                                <li><form:radiobutton path="searchResultsPerPage"  value="50"  /><span>50</span></li>
                                <li><form:radiobutton path="searchResultsPerPage"  value="100" /><span>100</span></li>
                                <li><form:radiobutton path="searchResultsPerPage"  value="500" /><span>500</span></li>
							</ul>
							<div class="clear">&nbsp;</div>
					</div>
					<div class="clear">&nbsp;</div>
					<hr>
					<div class="btmdiv">
						<input type="button" value="Reset All" class="buttonTwo flLeft settingReset" onClick="resetViewSettings();" />
						<input type="button" class="buttonTwo flRight rightReset" value="Cancel"  onClick="cancelViewSettings();" />
						<input type="button" value="Apply" class="buttonOne flRight" style="margin: 0 5px 0 0;" onClick="applyViewSettings();"  />
						<div class="clear">&nbsp;</div>
					</div>
				</div>


</form:form>
<!-- Showing Deatils and Analysis tab -->
</div>

<%-- 
 <form name="exportForm" method="post" id="exportForm" action="<portlet:resourceURL id="exportFile"/>">
	<input type="hidden" name="fileType" value="" id="fileType" />
 </form> --%>
<script src="<%=request.getContextPath()%>/js/s_code.js"></script>
<script>

	// Omniture SiteCatalyst - START
	s.prop22 = "premium";
	s.pageName="rer:events-form";
	s.channel="rer:events-form";
	s.server="rer";
	s.prop1="events-form"
	s.prop2="events-form";
	s.prop3="events-form";
	s.prop4="events-form";
	s.prop21=s.eVar21='current.user@foo.bar';	
	s.events="event2";
	s.events="event27";
	
	s.t();
	// Omniture SiteCatalyst - END
</script> 
