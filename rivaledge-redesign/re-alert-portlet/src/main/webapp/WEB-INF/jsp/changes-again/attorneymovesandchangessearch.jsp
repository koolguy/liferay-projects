<%@page import="com.alm.rivaledge.transferobject.AttorneyMoveChangesSearchDTO"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.alm.rivaledge.util.ALMConstants"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@ taglib 	prefix="liferay-portlet"	uri="http://liferay.com/tld/portlet" %>
<%@ taglib 	prefix="portlet"			uri="http://java.sun.com/portlet_2_0" %>
<%@ taglib	prefix="c" 					uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib	prefix="liferay-ui" 		uri="http://liferay.com/tld/ui" %>
<%@ taglib  prefix="liferay-util" 		uri="http://liferay.com/tld/util"%>
<%@taglib   prefix="liferay-theme" 		uri="http://liferay.com/tld/theme" %>
<%@ taglib prefix="spring"          uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form"            uri="http://www.springframework.org/tags/form"%>
<portlet:defineObjects />

<%-- <liferay-theme:defineObjects />
 --%>
<%@ include file="./common.jsp" %>

<%-- <portlet:resourceURL var="applySearchUrl" id="applySearchUrl" /> --%>
<!--
 <script src="/re-attorney-moveschanges/js/jquery-1.9.1.js"></script>
<script src="/re-attorney-moveschanges/js/jquery-ui.js"></script> 
 <script src="/re-attorney-moveschanges/js/main.js"></script>  
<script src="/re-attorney-moveschanges/js/s_code.js"></script>
-->

<link rel="stylesheet" href="<%=request.getContextPath()%>/css/jquery-ui.css" />

<portlet:actionURL var="submitURL">
	<portlet:param name="action" value="saveAttorneyMovesFilters"/>
</portlet:actionURL>

<script type="text/javascript">

/**
*Autoselect previously selected search critria
*/
	
	function <portlet:namespace/>initializeSearchCriteria()
	{
		<portlet:namespace/>applyFirms();
		<portlet:namespace/>initializeDate();
		<portlet:namespace/>applyLocations();
		<portlet:namespace/>applyPracticeArea();
		<portlet:namespace/>applyChangeType();
		<portlet:namespace/>applyTitle();
		
		<portlet:namespace/>refreshCounters();
	}
	
	function <portlet:namespace/>allOtherFirmChange()
	{
		var allFirmsCounter = 0;
		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() 
		{
			allFirmsCounter++;
		});
		$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="Abrams"> Firms ('+allFirmsCounter+') Selected');
		
		if(allFirmsCounter > 0)
		{
			 $('#<portlet:namespace/>allOtherFirm').prop("checked", true);
		}
		else
		{
			$('#<portlet:namespace/>allOtherFirm').prop("checked", false);
		}
	}
	
	function <portlet:namespace/>applyViewSettings()
	{
		
		<portlet:namespace/>search();
	}
	
	
	
	function <portlet:namespace/>search()
	{
		$('#alertAttorneyMovesSearchModelBean').submit();
	}

	
	function toggleViewSetting(vsId)
	{
		 //get the position of the placeholder element
	   	var vsPos   = $(vsId).offset();
	   	var vsHeight = $(vsId).height();
	   	var vsWidth = $(vsId).width();
	    //show the menu directly over the placeholder
	    var popupWidth = $("#<portlet:namespace/>viewSettingsPopup").width();
	    $("#<portlet:namespace/>viewSettingsPopup").css({  position: "absolute", "left": (vsPos.left - popupWidth + vsWidth) + "px", "top":(vsPos.top + vsHeight)  + "px" });
	    
	   
		$("#<portlet:namespace/>viewSettingsPopup").toggle(); 
	}
	
	
	function <portlet:namespace/>initializeDate()
	{
		var dateTextStr = '${alertAttorneyMovesSearchModelBean.dateText}';

		if(dateTextStr.match('^Any') || dateTextStr.match('^Last'))
			{
				$("#<portlet:namespace/>datePeriodSelect option[value='"+dateTextStr+"']").attr('selected','selected'); 
				$('#<portlet:namespace/>period').prop("checked", true);
			}
		else{ // we have date range parse it and set the Date on datePicker
				$( "#<portlet:namespace/>from" ).datepicker( "setDate", dateTextStr.split("-")[0]);
				$( "#<portlet:namespace/>to" ).datepicker( "setDate", dateTextStr.split("-")[1]);
			}
		
			$("#<portlet:namespace/>dateText").val(dateTextStr);

	}
	
	//This method would trigger when user clicks on "Clear All" button from firm popup
	//and would reset the firm value to default slection.
	function <portlet:namespace/>clearFirms()
	{		
		$('#<portlet:namespace/>popup').find("option").attr("selected", false);
		$('#<portlet:namespace/>popup').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>popup').find('input[type=radio]:checked').removeAttr('checked');
		
		$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/>  Firms (0) Selected');
	
		$("#<portlet:namespace/>firm_watchlist").prop("checked", true);
		
		var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
		
		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]').each(function() {
			 if(this.value == watchListId)
				{
					$(this).prop("checked", true);
				}
		});
		
		<portlet:namespace/>applyFirms();
	}
	
	function <portlet:namespace/>clearDate()
	{	
		$('#<portlet:namespace/>popupDate').find("option").attr("selected", false);
		$('#<portlet:namespace/>popupDate').find('input[type=text]').val('');
		$('#<portlet:namespace/>popupDate').find('input[type=radio]:checked').removeAttr('checked');				
		$("#<portlet:namespace/>dateText").val('<%=ALMConstants.LAST_WEEK%>');	
		$("#<portlet:namespace/>period").prop("checked", true); 
		$("#<portlet:namespace/>datePeriodSelect").val('<%=ALMConstants.LAST_WEEK%>');	
		$("#<portlet:namespace/>from").datepicker('enable');
		$("#<portlet:namespace/>to").datepicker('enable');
		<portlet:namespace/>applyDate();
	}
	
	
	
	
	function <portlet:namespace/>clearLocations()
	{
		$('#<portlet:namespace/>popupLocation').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>popupLocation').find('input[type=radio]:checked').removeAttr('checked');
		$("#locationsCounter").html('<input type="checkbox" value="Abrams"> Locations (0) Selected');
		$('#<portlet:namespace/>allLocations').prop("checked", true);
		<portlet:namespace/>applyLocations();
	}

	function <portlet:namespace/>clearPracticeAreas()
	{
		$('#<portlet:namespace/>popupPracticeArea').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>popupPracticeArea').find('input[type=radio]:checked').removeAttr('checked');
		$('#<portlet:namespace/>allOtherPracticeArea input').find('input[type=checkbox]:checked').removeAttr('checked');
		$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="Abrams"> Practice Area (0) Selected');
		$('.allPracticeArea').prop('checked', true);	
		<portlet:namespace/>applyPracticeArea();
	}
		
	
	
	function <portlet:namespace/>clearChangeType()
	{
		$('#<portlet:namespace/>changeType').find('input[type=checkbox]:checked').removeAttr('checked');
		//$('#allChangeType').prop("checked", true);
		$('#<portlet:namespace/>AdditionsChangeType').prop("checked", true);
		$('#<portlet:namespace/>RemovalsChangeType').prop("checked", true);
		<portlet:namespace/>applyChangeType();
	}
	
	function <portlet:namespace/>clearTitles()
	{
		$('#<portlet:namespace/>titlesDiv').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>allTitles').prop("checked", true);
		<portlet:namespace/>applyTitle();
	}
	
	
	function <portlet:namespace/>clearAll()
	{
		<portlet:namespace/>clearFirms();
		<portlet:namespace/>clearLocations();
		<portlet:namespace/>clearPracticeAreas();
		<portlet:namespace/>clearDate();
		<portlet:namespace/>clearChangeType();
		<portlet:namespace/>clearTitles();
		//initializeSearchCriteria();
	}
	
	function <portlet:namespace/>ValidateDate(txtDate)
	{
	    var currVal = txtDate;
	    if(currVal == '')
	        return false;
	    
	    var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; //Declare Regex
	    var dtArray = currVal.match(rxDatePattern); // is format OK?
	    
	    if (dtArray == null) 
	        return false;
	    
	    //Checks for mm/dd/yyyy format.
	    dtMonth = dtArray[1];
	    dtDay= dtArray[3];
	    dtYear = dtArray[5];        
	    
	    if (dtMonth < 1 || dtMonth > 12) 
	        return false;
	    else if (dtDay < 1 || dtDay> 31) 
	        return false;
	    else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31) 
	        return false;
	    else if (dtMonth == 2) 
	    {
	        var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
	        if (dtDay> 29 || (dtDay ==29 && !isleap)) 
	                return false;
	    }
	    return true;
	}
	
	


		
	//Apply Buttons functions
		function <portlet:namespace/>applyFirms()
		{
			var checkFlag = $("#<portlet:namespace/>allfirms").is(":checked");
			var valueofchecked =$("input[name='firmType']:checked").val();
			if(valueofchecked=="All Firms"){
				checkFlag=true;
				 $('#<portlet:namespace/>allfirms').prop("checked", true);
			}
			
			var allSelectedValue = [];
			var allSelectedIds = [];
			
			 $("#<portlet:namespace/>Firmstext").val('');
			 $("#<portlet:namespace/>selectedFirms").val('');
			 
			var isFirmChecked = false;
			var allWatchListCounter = 0;			
			
				$('.individualfirmsWatchList input[type=checkbox]:checked').each(function() {
					
					 allSelectedValue.push($(this).attr('labelAttr'));
					 allSelectedIds.push($(this).val());
					 $("#<portlet:namespace/>firm_watchlist").prop("checked", true);
					 $('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
					 
					 $('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').each(function() 
						{
								allWatchListCounter++;
						});
								
						$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (' + allWatchListCounter +' Selected)');
					 
			 	});
			
			$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
				 allSelectedValue.push($(this).attr('labelAttr'));
				 allSelectedIds.push($(this).val());
				 $("#<portlet:namespace/>allOtherFirm").prop("checked", true);
				 
		 	});
			
			if(allSelectedValue != ''  &&  allSelectedIds != '') 
				{
				
					$("#<portlet:namespace/>Firmstext").val(allSelectedValue.join(",")); //Use a comma separator for values which show up on UI
					$("#<portlet:namespace/>selectedFirms").val(allSelectedIds.join(";"));
				}
			else
			{
				
					$("#<portlet:namespace/>allfirms, .rivaledgeListAMLAW_100, .rivaledgeListAMLAW_200, .rivaledgeListNLJ_250").each(function(){
					if(checkFlag)
						{
							$("#<portlet:namespace/>Firmstext").val('<%=ALMConstants.ALL_FIRMS %>');
							$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.ALL_FIRMS %>');
							
							 $('#<portlet:namespace/>firm_watchlist').prop("checked", false);
							 $('#<portlet:namespace/>allOtherFirm').prop("checked", false);
							 $('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
							 
							 $('#<portlet:namespace/>individualfirmsWatchList').find('input[type=checkbox]:checked').removeAttr('checked');
							 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="Abrams" id="<portlet:namespace/>firmCounter"> Firms (0) Selected');
							
						} 
					
					 else
						{
						 if($(this).is(":checked")){
								var value =$(this).val();
							 	//$("#selectedFirms").val(value);
								$("#<portlet:namespace/>Firmstext").val(value);
								$("#<portlet:namespace/>selectedFirms").val('<%=ALMConstants.AMLAW_100%>');
								$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
							} 
						}
				
					
				});
				
		}
		
	}	
		

	
	
	function <portlet:namespace/>applyDate()
	{
		var checkDate=false;
		var dateValue;
		  
		if($("#<portlet:namespace/>dateRange").is(":checked"))
		{
			var fromDate = new Date($("#<portlet:namespace/>from").val());
		   	var toDate = new Date($("#<portlet:namespace/>to").val()); 
		   	if(!<portlet:namespace/>ValidateDate($("#<portlet:namespace/>from").val()))
		   	{
		    	checkDate=true;
		        $('#<portlet:namespace/>fromdateError').css('display','block');      
		   	}
		   	else
		   {
		    	checkDate=false;
		    	$('#<portlet:namespace/>fromdateError').css('display','none');
		   }  
		   if(!<portlet:namespace/>ValidateDate($("#<portlet:namespace/>to").val()))
		   {
		    	checkDate=true;
		    	$('#<portlet:namespace/>todateError').css('display','block');
		   }  
		   else
		   {
			if(!checkDate)
		    {
		     checkDate=false;
		     $('#<portlet:namespace/>todateError').css('display','none'); 
		    }
		   
		   }
		   if(!(toDate >= fromDate) && !checkDate)
		   {
		    $('#<portlet:namespace/>dateValidError').css('display','block');
		    checkDate=true;
		   } 
		   else
		   {
		    $('#<portlet:namespace/>dateValidError').css('display','none');
		   }
		  
		   if(checkDate)
		   {
		   return checkDate; 
		   }
		  
	  }
	  
	  if($("#<portlet:namespace/>period").is(":checked"))
	  {
	   
	   dateValue=$("#<portlet:namespace/>datePeriodSelect").val();
	   
	  } 
	  else if($("#<portlet:namespace/>dateRange").is(":checked"))
	  {
	   dateValue=$("#<portlet:namespace/>from").val() + "-"+ $("#<portlet:namespace/>to").val();
	  } 
	  
	  $("#<portlet:namespace/>dateText").val(dateValue);
	}
	
	function <portlet:namespace/>applyLocations()
	{
		var checked= false;
		
		 if($("#<portlet:namespace/>allLocations").is(":checked"))
		 {
				$("#<portlet:namespace/>selectedLocation").val($("#<portlet:namespace/>allLocations").val());
				checked = true;
		 } 
		 
		if(!checked)
		 {
			 var allValsLocation = [];
			
				$('#<portlet:namespace/>allOtherLocationsDiv input[type=checkbox]:checked').each(function() { 
					 allValsLocation.push($(this).val());
			    });
				 
			if(allValsLocation.length > 0){
				$("#<portlet:namespace/>allOtherLocation").prop("checked", true);
			    $("#<portlet:namespace/>selectedLocation").val(allValsLocation.join(";"));		
			}
		}
	}

	
	
	function <portlet:namespace/>applyPracticeArea()
	{

		 if($(".allPracticeArea").is(":checked"))
		 {
				$("#<portlet:namespace/>selectedPracticeArea").val($(".allPracticeArea").val());			
		 } 
		 else
		 {
			var allValsPracticeArea = [];
			var allPracticeCounter = 0;
			
			$('#<portlet:namespace/>practiceAreaDiv input[type=checkbox]:checked').each(function() 
			{ 
			 	allValsPracticeArea.push($(this).val());
			 	allPracticeCounter++;
			});
				 
			$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="Abrams"> PracticeAreas ('+allPracticeCounter+') Selected');
			
			if(allValsPracticeArea.length > 0)
			{
				$("#<portlet:namespace/>allPracticeAreaIndividual").prop("checked", true);
				$("#<portlet:namespace/>selectedPracticeArea").val(allValsPracticeArea.join(";"));		
			}
		}
	}
	
	function <portlet:namespace/>applyChangeType()
	{
		var allValsChangeType = [];
		 $('#<portlet:namespace/>changeType :checked').each(function() {
			 allValsChangeType.push($(this).val());
		    });
		 $("#<portlet:namespace/>selectedChangeType").val(allValsChangeType.join(";"));
	}
	

	
	function <portlet:namespace/>applyTitle()
	{
		var allValsTitles = [];
		 $('#<portlet:namespace/>popupTitles :checked').each(function() {
			 allValsTitles.push($(this).val());
		    });
		    $("#<portlet:namespace/>selectedTitles").val(allValsTitles.join(";"));
		 $("#<portlet:namespace/>popupTitles").toggle();
	}
	

	/*Refresh counter values*/
	
	function <portlet:namespace/>refreshCounters()
	{
		var allFirmsCounter = 0;
		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
		 allFirmsCounter++;
		});
		$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/>  Firms ('+allFirmsCounter+') Selected');
		
		var allLocationCounter = 0;
		 $('#<portlet:namespace/>allOtherLocationsDiv input[class=allLocationsCheckBox]:checked').each(function() {
			 allLocationCounter++;
		    });
		$("#locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="selectAllLocations" />  Locations ('+allLocationCounter+') Selected');

		var allPracticeCounter = 0;
		$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:checked').each(function() {
			 allPracticeCounter++;
		    });
		$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" /> PracticeAreas ('+allPracticeCounter+') Selected');
	}




	function <portlet:namespace/>changeOnIndividualPracticeAreas()
	{
			var allPracticeCounter = 0;
		
			$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:checked').each(function() {
				 allPracticeCounter++;
		    });
			
			$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="Abrams"> PracticeAreas ('+allPracticeCounter+') Selected');
			
			if(allPracticeCounter == 0)
			{
				$('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", false);
				$('.allPracticeArea').prop("checked", true);
			}
			else
			{
				$('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", true);
				$('.allPracticeArea').prop("checked", false);
			}
	}
	
	
	
	function <portlet:namespace/>changeOnIndividualFirms()
	 {
	  
	  var allFirmsCounter = 0;
	   $('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
	    allFirmsCounter++;
	      });
	   
	   $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms ('+allFirmsCounter+') Selected');
	   if(allFirmsCounter == 0)
	   {
	    $('#<portlet:namespace/>firm_watchlist').prop("checked", false);
	    $('#<portlet:namespace/>allOtherFirm').prop("checked", false);
	    $('#<portlet:namespace/>allfirms').prop("checked", false);
	    
	    $('#<portlet:namespace/>individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
	    $('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
	    

	    var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
	    
	    $('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]').each(function() {
	      if(this.value == watchListId)
	      {
	       $(this).prop("checked", true);
	      }
	    });
	   }
	  else
	   {
	    $('#<portlet:namespace/>allOtherFirm').prop("checked", true);
	    $('#<portlet:namespace/>firm_watchlist').prop("checked", false);
	    $('#<portlet:namespace/>allfirms').prop("checked", false);
	    
	    $('#<portlet:namespace/>individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
	    $('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
	   }
	 }
	
$.widget( "custom.catcomplete", $.ui.autocomplete, {
	_renderMenu: function( ul, items ) {
		var that = this,
		currentCategory = "";
		$.each( items, function( index, item ) {
			if ( item.category != currentCategory ) {
			ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
			currentCategory = item.category;
			}
	  that._renderItemData( ul, item );
	});
}
});

$(document).ready(function(){
	
	$("#alertAttorneyMovesSearchModelBean input, #alertAttorneyMovesSearchModelBean select, #alertAttorneyMovesSearchModelBean span").each(function(){
		var idVal = '<portlet:namespace/>' + $(this).attr("id");
		$(this).attr("id", idVal );
		
	});
	$(".menu").hover(
		function(){$(".sub").slideToggle(400);},
		function(){$(".sub").hide();}
	);
	
	//Spring will add <input> tags with names starting with "_" for type "radio" and "checkbox".
	//On form POST submit they all are carried to server which is unneccessary
	//hence removing all such input box to make page and jQuery selections less heavy :) on document ready.
	
	//NOTE : If any inputs added with name prefixed with "_" purposefully will also get removed. So be cautious
	$("#alertAttorneyMovesSearchModelBean input[name^=_]").remove();
	
  	$('#<portlet:namespace/>fromdateError').hide();
	$('#<portlet:namespace/>todateError').hide();
	$('#<portlet:namespace/>dateValidError').hide();
	
	  $( "#<portlet:namespace/>from" ).datepicker({
		  changeMonth: true,
		  changeYear: true,
		  showOn: "button",
		  buttonImage: "<%=themeDisplay.getPathThemeImages()%>/calendar.gif",
		  buttonImageOnly: true,
		  defaultDate: "+1w",
		  onClose: function( selectedDate ) {
		  $( "#<portlet:namespace/>to" ).datepicker( "option", "minDate", selectedDate );
		  }
		  });
		  $( "#<portlet:namespace/>to" ).datepicker({
		  changeMonth: true,
		  changeYear: true,
		  showOn: "button",
		  buttonImage: "<%=themeDisplay.getPathThemeImages()%>/calendar.gif",
		  buttonImageOnly: true,
		  defaultDate: "+1w",
		  onClose: function( selectedDate ) {
		  $( "#<portlet:namespace/>from" ).datepicker( "option", "maxDate", selectedDate );
		  }
		  });
	
	var date = new Date();
	var currentDate = (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear();
	$( "#<portlet:namespace/>to" ).datepicker( "setDate", currentDate);
	date.setDate(date.getDate() - 7);
	var lastWeekDate = (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear();
	$( "#<portlet:namespace/>from" ).datepicker( "setDate", lastWeekDate);
	$("#<portlet:namespace/>dateText").val(lastWeekDate +"-"+currentDate);
	
  
	//$("#firm_watchlist").prop("checked", true);
	var data = ${firmJson};
	var practiceData = ${practiceJson};
	$("#graduationTo option[value='2013']").attr('selected', 'selected');	

	var availableLocation = [
                         "Boston, MA",
                         "Dallas, TX",
                         "Denver, CO",
                         "Houston, TX",
                         "Las Vegas, NV",
                         "Los Angeles, CA",
                         "Miami, FL",
                         "New Orleans, LA",
                         "New York, NY",
                         "Orlando, FL",
                         "Philadelphia, PA",
                         "Phoenix, AZ",
                         "Pittsburgh, PA",
                         "San Antonio, TX",
                         "San Francisco, CA",
                         "Seattle, WA",
                         "Tampa, FL",
                         "Washington, DC"                     
                       ];



function split( val ) {
	return val.split( /,\s*/ );
}

function extractLast( term ) {
	return split( term ).pop();
}
  

$("#<portlet:namespace/>selectedLocation")
// don't navigate away from the field on tab when selecting an item
.bind( "keydown", function( event ) {
  if ( event.keyCode === $.ui.keyCode.TAB &&
      $( this ).data( "ui-autocomplete" ).menu.active ) {
    event.preventDefault();
  }
})
.autocomplete({
  minLength: 0,
  source: function( request, response ) {
    // delegate back to autocomplete, but extract the last term
    response( $.ui.autocomplete.filter(
    		availableLocation, extractLast( request.term ) ) );
  },
  focus: function() {
    // prevent value inserted on focus
    return false;
  },
  select: function( event, ui ) {
    var terms = split( this.value );
    // remove the current input
    terms.pop();
    // add the selected item
    terms.push( ui.item.value );
    // add placeholder to get the comma-and-space at the end
    terms.push("");
    this.value = terms.join(";");
    return false;
  }
});


$( "#<portlet:namespace/>selectedPracticeArea" )
	//don't navigate away from the field on tab when selecting an item
	.bind( "keydown", function( event ) {
		if ( event.keyCode === $.ui.keyCode.TAB &&
		$( this ).data( "ui-autocomplete" ).menu.active ) {
		event.preventDefault();
		}
	})
	.autocomplete({
	minLength: 3,
	source: function( request, response ) {
		//delegate back to autocomplete, but extract the last term
		response( $.ui.autocomplete.filter(
			practiceData, extractLast( request.term ) ) );
	},
	focus: function() {
		//prevent value inserted on focus
		return false;
	},
	select: function( event, ui ) {
		var terms = split( this.value );
		//remove the current input
		terms.pop();
		//add the selected item
		terms.push( ui.item.value );
		//add placeholder to get the comma-and-space at the end
		terms.push( "" );
		this.value = terms.join( ";" );
		return false;
	}
});



  $( "#<portlet:namespace/>Firmstext" ).catcomplete({
	minLength: 3,
	source: function( request, response ) {
		// delegate back to autocomplete, but extract the last term
		response( $.ui.autocomplete.filter(
		data, extractLast( request.term ) ) );
	},
	focus: function() {
		// prevent value inserted on focus
		return false;
	},
	change: function(event, ui) {		
		
		
		$('#<portlet:namespace/>popup').find('input[type=checkbox]:checked').removeAttr('checked');
		$('#<portlet:namespace/>popup').find('input[type=radio]:checked').removeAttr('checked');
		
		var selectedValues = this.value.split(',');
		var output = '';
		var currentvalue="";		
		$.each(selectedValues, function(key, line) {
			line=line.trim();		
			if(line!=""){				
				currentvalue=$("input[labelattr='"+line+"'].allFirmsCheckBox").val();
				if(typeof(currentvalue)!="undefined"){
					output= output + currentvalue+ ",";
					$("input[labelattr='"+line+"'].allFirmsCheckBox").prop("checked", true);
					<portlet:namespace/>allOtherFirmChange();
				}	
							
			}			
		});		
		output = output.substring(0, output.length - 1);
		$('#<portlet:namespace/>selectedFirms').val(output);		
    },
	select: function( event, ui ) {
		var terms = split( this.value );		
		var termsValue = split( this.id );
		if( ui.item.value.indexOf("AmLaw 100") !== -1){
			return false;
		}
		
		// remove the current input
		terms.pop();
		termsValue.pop();
		// add the selected item
		
		terms.push( ui.item.value );
		termsValue.push( ui.item.id );
		// add placeholder to get the comma-and-space at the end
		terms.push( "" );
		termsValue.push( "" );
		this.value = terms.join( ", " );	
		$('#<portlet:namespace/>selectedFirms').val($('#<portlet:namespace/>selectedFirms').val().replace("AmLaw 100","") + "," + ui.item.id);		
		$('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]').each(function()
   		{ 
			if($(this).val() == (ui.item.id) )
			{
				$(this).prop("checked", true);
				<portlet:namespace/>allOtherFirmChange();
				return false;
			}
		});
		
		return false;
	}
}); 
 
  
  
	
  $('.rivaledgeListAMLAW_100').bind('click', function (event) {
  		$('.rivaledgeListAMLAW_100').prop("checked", true);
  		$('.rivaledgeListAMLAW_200').prop("checked", false);
  		$('.rivaledgeListNLJ_250').prop("checked", false);
  		$('#<portlet:namespace/>firm_watchlist').prop("checked", true);
  		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
  	 	$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0 Selected)');
  	 	
  	 	$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms (0) Selected');
  		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
  		$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
  		$('#<portlet:namespace/>Firmstext').val("<%=ALMConstants.AMLAW_100 %>");
  	});
  	
  	$('.rivaledgeListAMLAW_200').bind('click', function (event) {
  		
  		$('.rivaledgeListAMLAW_100').prop("checked", false);
  		$('.rivaledgeListAMLAW_200').prop("checked", true);
  		$('.rivaledgeListNLJ_250').prop("checked", false);
  		$('#<portlet:namespace/>firm_watchlist').prop("checked", true);
  		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
  	 	$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0 Selected)');
  	 	
  	 	$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms (0) Selected');
  		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
  		$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
  		$('#<portlet:namespace/>Firmstext').val("<%=ALMConstants.AMLAW_200 %>");
  	});
  	
  	$('.rivaledgeListNLJ_250').bind('click', function (event) {
  		
  		$('.rivaledgeListAMLAW_100').prop("checked", false);
  		$('.rivaledgeListAMLAW_200').prop("checked", false);
  		$('.rivaledgeListNLJ_250').prop("checked", true);
  		$('#<portlet:namespace/>firm_watchlist').prop("checked", true);
  		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
  	 	$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0 Selected)');
  	 	
  	 	$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms (0) Selected');
  		$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
  		$('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
  		$('#<portlet:namespace/>Firmstext').val("<%=ALMConstants.NLJ_250 %>");

  	});
  	
  	$("input[class='allFirmsCheckBoxWatchList']").change(function(){
  		 $('.rivaledgeListAMLAW_100').prop("checked", false);
  		 $('.rivaledgeListAMLAW_200').prop("checked", false);
  		 $('.rivaledgeListNLJ_250').prop("checked", false);
  	});
  	
  	 $('.allFirmsCheckBoxWatchList').click(function() {
  		 var allWatchListCounter = 0;
  		 $('#<portlet:namespace/>firm_watchlist').prop("checked", true);
  		 $('#<portlet:namespace/>allfirms').prop("checked", false);
  		 
  		 $('#<portlet:namespace/>allOtherFirm').prop("checked", false);
  		 
  		 $('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
  		 
  		
  		$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').each(function() 
  		{
  			allWatchListCounter++;
  		});
  		
  		 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (' + allWatchListCounter +' Selected)');
  		 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms (0 Selected)');
  		 $('#<portlet:namespace/>firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
  		 $('.rivaledgeListAMLAW_100').prop("checked", false);
  		 $('.rivaledgeListAMLAW_200').prop("checked", false);
  		 $('.rivaledgeListNLJ_250').prop("checked", false);
  		 
  		});
  	 
  	  //Function for showing selected PracticeArea name
  	  $("input[id='<portlet:namespace/>allFirmsCheckBoxCounterAttorneyMovesAndChanges']").change(function(){
  		  
  	   
  		<portlet:namespace/>changeOnIndividualFirms();
  	   
  	 	<portlet:namespace/>applyFirms();
  	   
  	  });
  	 
 	//Select All firms by selecting one check box
	 $('#<portlet:namespace/>individualFirmsCounter').click(function() {
	 
		var checked = $("#<portlet:namespace/>selectAllFirms").is(":checked");
		var allFirmsListCounter = 0;
		if(checked)
		{   
			 $('#<portlet:namespace/>allOtherFirmDiv #<portlet:namespace/>allFirmsCheckBoxCounterAttorneyMovesAndChanges').prop('checked', true);
		}
		else
		{
			$('#<portlet:namespace/>allOtherFirmDiv #<portlet:namespace/>allFirmsCheckBoxCounterAttorneyMovesAndChanges').prop('checked', false);
		}
		
		 $('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
			 allFirmsListCounter++;
		 });
		 
		 if(allFirmsListCounter == 0)
			 {
				 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms ('+allFirmsListCounter+') Selected');
				 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (1) Selected)');
				 $('#<portlet:namespace/>allfirms').prop("checked", false);
				 $('#<portlet:namespace/>allOtherFirm').prop("checked", false);
				 $('#<portlet:namespace/>selectAllFirms').prop("checked", false);
				 
					var watchListId = $('#<portlet:namespace/>defaultWatchListId').val();
					
					$('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]').each(function() {
						 if(this.value == watchListId)
							{
								$(this).prop("checked", true);
							}
					});
					 $('#<portlet:namespace/>firm_watchlist').prop("checked", true);
			 }
		 else
			 {
				 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="<portlet:namespace/>selectAllFirms"/> Firms ('+allFirmsListCounter+') Selected');
				 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="<portlet:namespace/>selectAllWatchList" /> Watchlists (0) Selected)');
				 $('#<portlet:namespace/>allOtherFirm').prop("checked", true);
				 $('#<portlet:namespace/>selectAllFirms').prop("checked", true);
				 $('#<portlet:namespace/>individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
				 $('#<portlet:namespace/>firm_watchlist').prop("checked", false);
				 $('#<portlet:namespace/>allfirms').prop("checked", false);
			 }
	});

  
  
	  $('#<portlet:namespace/>hide, #<portlet:namespace/>popup').click(function(e){

		 $("#<portlet:namespace/>popupPracticeArea").hide();	
		 $("#<portlet:namespace/>popupLocation").hide();
		 $("#<portlet:namespace/>popupTitles").hide();	
		 $("#<portlet:namespace/>popupDate").hide();
		 $("#<portlet:namespace/>popupChangeType").hide();
		 
			var allFirmsCounter = 0;
			 
			$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:not(:checked)').each(function() {
				allFirmsCounter++;
			});

			if(allFirmsCounter != 0) 
			{
				$("#<portlet:namespace/>firmCounter").prop("checked", false);
			} 
			else
			{
				$("#<portlet:namespace/>firmCounter").prop("checked", true);
			}
	     e.stopPropagation();	   
	});   


	$('#<portlet:namespace/>practiceAreaId, #<portlet:namespace/>popupPracticeArea').click(function(e){
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupLocation").hide();
		 $("#<portlet:namespace/>popupTitles").hide();
		 $("#<portlet:namespace/>popupDate").hide();
		 $("#<portlet:namespace/>popupChangeType").hide();
		 
		 var allPracticeCounter = 0;
			$('#<portlet:namespace/>allOtherPracticeArea input[type=checkbox]:not(:checked)').each(function() {
					allPracticeCounter++;
			});

			if(allPracticeCounter != 0) 
			{
				$("#practiceSelection").prop("checked", false);
			} 
			else
			{
				$("#practiceSelection").prop("checked", true);
			}
		 
	     e.stopPropagation();   
	});
	 
	
	$('#ui-datepicker-div').click(function(e){
		
		$("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupPracticeArea").hide();
		 $("#<portlet:namespace/>popupLocation").hide();
		 $("#<portlet:namespace/>popupTitles").hide();	
		 $("#<portlet:namespace/>popupChangeType").hide();
	    e.stopPropagation();   
	});

	$('#<portlet:namespace/>datenone, #<portlet:namespace/>popupDate').click(function(e){
		
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupPracticeArea").hide();
		 $("#<portlet:namespace/>popupLocation").hide();
		 $("#<portlet:namespace/>popupTitles").hide();	
		 $("#<portlet:namespace/>popupChangeType").hide();
	  	 e.stopPropagation();   
	});

	$('#<portlet:namespace/>locationId, #<portlet:namespace/>popupLocation').click(function(e){
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupPracticeArea").hide();
		 $("#<portlet:namespace/>popupTitles").hide();	
		 $("#<portlet:namespace/>popupDate").hide();
		 $("#<portlet:namespace/>popupChangeType").hide();
		 
		 var allLocationsCounter = 0;
		 $('#<portlet:namespace/>allOtherLocationsDiv input[type=checkbox]:not(:checked)').each(function() {
			 allLocationsCounter++;
		 });
		 
		 if(allLocationsCounter != 0)
		{
		 	$('#locationSelection').prop("checked", false);
		}
		else
		{
			$("#locationSelection").prop("checked", true);
		}
	     e.stopPropagation();   
	});

	
	$('#<portlet:namespace/>titleDropdown, #<portlet:namespace/>popupTitles').click(function(e){
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupPracticeArea").hide();
		 $("#<portlet:namespace/>popupLocation").hide();
		 $("#<portlet:namespace/>popupDate").hide();
		 $("#<portlet:namespace/>popupChangeType").hide();
	     e.stopPropagation();   
	});
	
	$('#<portlet:namespace/>popupChangeType, #<portlet:namespace/>changeTypeDropdown').click(function(e){
		 $("#<portlet:namespace/>popup").hide(); 
		 $("#<portlet:namespace/>popupPracticeArea").hide();
		 $("#<portlet:namespace/>popupLocation").hide();
		 $("#<portlet:namespace/>popupDate").hide();
		 $("#<portlet:namespace/>popupTitles").hide();	
	     e.stopPropagation();   
	});
	
	
	$(document).click(function(){
	    $("#<portlet:namespace/>popup").hide(); 
	    $("#<portlet:namespace/>popupPracticeArea").hide();
	    $("#<portlet:namespace/>popupLocation").hide();		 
	    $("#<portlet:namespace/>popupTitles").hide();
	    $("#<portlet:namespace/>popupDate").hide();
	    $("#<portlet:namespace/>popupChangeType").hide();
	});
  
	$('#<portlet:namespace/>btnAdd').click(function(){
		$('.filtersPage').hide();
		$('#<portlet:namespace/>additional').show();
	})
	$('#<portlet:namespace/>btnSave').click(function(){
		$('.filtersPage').show();
		$('#<portlet:namespace/>additional').hide();
	});
	
	$('#<portlet:namespace/>hide').click(
    	function () {
        //open firms submenu/popup
        $("#<portlet:namespace/>popup").stop().slideToggle(500);    
    });  
    
    
    
    $('#<portlet:namespace/>titleDropdown').click(
    	function () {
    	 //open Title submenu/popup
    	 $("#<portlet:namespace/>popupTitles").stop().slideToggle(500);    
   });
	
	$('#<portlet:namespace/>locationId').click(
		 function () {
		 //open location submenu/popup
		 $("#<portlet:namespace/>popupLocation").stop().slideToggle(500);    
	});
	
	$('#<portlet:namespace/>practiceAreaId').click(
		function () {
		//open practice area submenu/popup
		$("#<portlet:namespace/>popupPracticeArea").stop().slideToggle(500);    
	});
	
	$('#<portlet:namespace/>changeTypeDropdown').click(
		function () {
		//open practice area submenu/popup
		$("#<portlet:namespace/>popupChangeType").stop().slideToggle(500);    
	});
	
	$('#<portlet:namespace/>datenone').click(
		function () {
		    //show date submenu
		    $("#<portlet:namespace/>popupDate").stop().slideToggle(500);    
	}); 
	

	//UI Logic for Firms
	$("input[id='<portlet:namespace/>allfirms']").change(function(){
		var checked = $("#<portlet:namespace/>allfirms").is(":checked");
		if(checked){		 
		 $('#<portlet:namespace/>firm_watchlist').prop("checked", false);
		 $('#<portlet:namespace/>allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
		 $('#<portlet:namespace/>individualfirmsWatchList').find('input[type=checkbox]:checked').removeAttr('checked');
		 $("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="Abrams" id="<portlet:namespace/>firmCounter"> Firms (0) Selected');
		}
	});
	
	$('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]').change(function(){
	
			var allFirmsCounter = 0;
			 $('#<portlet:namespace/>allOtherFirmDiv input[type=checkbox]:checked').each(function() {
				 allFirmsCounter++;
			    });
				
				$("#<portlet:namespace/>individualFirmsCounter").html('<input type="checkbox" value="Abrams" id="<portlet:namespace/>firmCounter"> Firms ('+allFirmsCounter+') Selected');
				
				if(allFirmsCounter > 0)
				{
					 $('#<portlet:namespace/>allOtherFirm').prop("checked", true);
				}
				else{
					$('#<portlet:namespace/>allOtherFirm').prop("checked", false);
				}
			
	});

	$('#<portlet:namespace/>clearButton').bind('click', function (event) {
		<portlet:namespace/>clearFirms();
		//applyFirms();
	});
	
	
	
	$("input[id='<portlet:namespace/>allLocations']").change(function(){
		var checked = $("#<portlet:namespace/>allLocations").is(":checked");
		if(checked){  		
		 $('#<portlet:namespace/>allOtherLocationsDiv').find('input[type=checkbox]:checked').removeAttr('checked');		
		 $('#<portlet:namespace/>ApplyLocations').removeAttr('disabled');
		 $('#<portlet:namespace/>ApplyLocations').removeClass("ui-state-disabled");
		}
	});
	
	$("input[id='<portlet:namespace/>allPracticeArea']").change(function(){
		var checked = $("#<portlet:namespace/>allPracticeArea").is(":checked");
		if(checked){  		
		 $('#<portlet:namespace/>allOtherPracticeArea').find('input[type=checkbox]:checked').removeAttr('checked');	
		 $('#<portlet:namespace/>ApplyPracticeArea').removeAttr("disabled");
		 $('#<portlet:namespace/>ApplyPracticeArea').removeClass("ui-state-disabled");
		}
	});
	
	$("input[id='<portlet:namespace/>allTitles']").change(function(){
		var checked = $("#<portlet:namespace/>allTitles").is(":checked");
		if(checked){  	
		 $('#<portlet:namespace/>allOtherTitlesDiv').find('input[type=checkbox]:checked').removeAttr('checked');
		 $('#<portlet:namespace/>ApplyTitles').removeAttr("disabled");
		 $('#<portlet:namespace/>ApplyTitles').removeClass("ui-state-disabled");
		}
	});
	
	
	$('#<portlet:namespace/>allOtherTitlesDiv input[type=checkbox]').change(function(){	
		var allTitlesCounter = 0;
		 $('#<portlet:namespace/>allOtherTitlesDiv input[type=checkbox]:checked').each(function() {
			 allTitlesCounter++;
		    });				
			if(allTitlesCounter > 0)
			{
				 $('#<portlet:namespace/>allTitles').prop("checked", false);
			}
					
});

	
	$("input[id='<portlet:namespace/>allChangeType']").change(function(){
		var checked = $("#<portlet:namespace/>allChangeType").is(":checked");
		if(checked){	
		 $('#<portlet:namespace/>allOtherChangeTypeDiv').find('input[type=checkbox]:checked').removeAttr('checked');
		}
	});
	
	$('#<portlet:namespace/>allOtherChangeTypeDiv input[type=checkbox]').change(function(){
		var allChangeTypeCounter = 0;
		 $('#<portlet:namespace/>allOtherChangeTypeDiv input[type=checkbox]:checked').each(function() {
			 allChangeTypeCounter++;
		    });				
			if(allChangeTypeCounter > 0)
			{
				 $('#<portlet:namespace/>allChangeType').prop("checked", false);
			}
					
	});
	
	$("input[name='allLocationsCheckBox']").change(function(){
		var lengthofCheckBoxes = $("input[name='allLocationsCheckBox']:checked").length;
		if(lengthofCheckBoxes>0){
		$('#<portlet:namespace/>ApplyLocations').removeAttr("disabled");
		$('#<portlet:namespace/>ApplyLocations').removeClass("ui-state-disabled");
		$('#<portlet:namespace/>allOtherLocation').prop("checked", true);
		} 
	}); 
	
	$("input[name='practiceAreaCheckBox']").change(function(){
		var lengthofCheckBoxes = $("input[name='practiceAreaCheckBox']:checked").length;
		if(lengthofCheckBoxes>0){
		$('#<portlet:namespace/>ApplyPracticeArea').removeAttr("disabled");
		$('#<portlet:namespace/>ApplyPracticeArea').removeClass("ui-state-disabled");
		$('#<portlet:namespace/>allPracticeArea').prop("checked", false);
		} 
	});
	
	$("input[name='allTitlesCheckBox']").change(function(){
		var lengthofCheckBoxes = $("input[name='allTitlesCheckBox']:checked").length;
		if(lengthofCheckBoxes>0){
		$('#<portlet:namespace/>ApplyTitles').removeAttr("disabled");
		$('#<portlet:namespace/>ApplyTitles').removeClass("ui-state-disabled");
		$('#<portlet:namespace/>allTitles').prop("checked", false);
		
		} else{
		$('#<portlet:namespace/>ApplyTitles').attr("disabled", true);
		$('#<portlet:namespace/>ApplyTitles').addClass("ui-state-disabled");
		}
	
	});
	
	//UI Logic for Practice Area
	
	$(".allPracticeArea").change(function(){
		var checked = $(this).is(":checked");
		if(checked){  		
		 $('#<portlet:namespace/>allOtherPracticeArea').find('input[type=checkbox]:checked').removeAttr('checked');	
		 $('#<portlet:namespace/>allPracticeAreaIndividual').removeAttr('checked');	
		 $("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="Abrams"> PracticeArea (0) Selected');
		}
	});
	
	$("#<portlet:namespace/>allPracticeAreaIndividual, #<portlet:namespace/>practiceAreaDiv input[type=checkbox]").click(function(){
		<portlet:namespace/>changeOnIndividualPracticeAreas();
	});
	
	$('#<portlet:namespace/>clearPracticeArea').bind('click', function (event) {
		<portlet:namespace/>clearPracticeAreas();
	});

	
	
	//Change Type popupup values selection 
	
	$("input[name='allChangeTypeCheckBoxMovesAndChanges']").change(function(){
		var lengthofCheckBoxes = $("input[name='allChangeTypeCheckBoxMovesAndChanges']:checked").length;
		if(lengthofCheckBoxes>0){
		$('#<portlet:namespace/>ApplyallChangeType').removeAttr("disabled");
		$('#<portlet:namespace/>ApplyallChangeType').removeClass("ui-state-disabled");
		$('#<portlet:namespace/>allChangeType').prop("checked", false);
		} else{
		$('#<portlet:namespace/>ApplyallChangeType').attr("disabled", true);
		$('#<portlet:namespace/>ApplyallChangeType').addClass("ui-state-disabled");
		}
	
	});
	
	$('#<portlet:namespace/>resetAll').bind('click', function (event) {
		
		<portlet:namespace/>clearAll();
		
		//name reset
		$('#<portlet:namespace/>name').val('');
		
		//keyword reset
		$('#keywords').val(''); 
		
		//hiding date error divs
		$('#<portlet:namespace/>fromdateError').hide();
		$('#<portlet:namespace/>todateError').hide();
		$('#<portlet:namespace/>dateValidError').hide();
		
	});
	
	//Function for showing selected PracticeArea counter	
	$("input[id='<portlet:namespace/>practiceAreaCheckBox']").click(function(){
		
		var allPracticeAreaCounter = 0;
				 $('#<portlet:namespace/>allOtherPracticeArea :checked').each(function() {
					 allPracticeAreaCounter++;
				    });
					
					$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="Abrams"> PracticeArea ('+allPracticeAreaCounter+') Selected');
					
					if(allPracticeAreaCounter == 0)
					{
					$('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", false);
					$('#<portlet:namespace/>allPracticeArea').prop("checked", false);
					}
				else
					{
					$('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", true);
					$('#<portlet:namespace/>allPracticeArea').prop("checked", false);
					}
				
	});
	
	$("input[id='<portlet:namespace/>allPracticeArea']").click(function(){
 		$('#<portlet:namespace/>practiceAreaCheckBox input[type=checkbox]:checked').removeAttr('checked');
 		$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="Abrams"> Practice Area (0) Selected');
	});
	
	
	//Function for showing selected Locations counter	
	$("input[id='<portlet:namespace/>allLocationsCheckBox']").click(function(){
		
		var allLocationsCounter = 0;
				 $('#<portlet:namespace/>allOtherLocationsDiv :checked').each(function() {
					 allLocationsCounter++;
				    });
					
					$("#<portlet:namespace/>individualLocationCounter").html('<input type="checkbox" value="Abrams"> Locations ('+allLocationsCounter+') Selected');
					
					if(allLocationsCounter == 0)
					{
					$('#<portlet:namespace/>allOtherLocation').prop("checked", false);
					$('#<portlet:namespace/>allLocations').prop("checked", false);
					}
				else
					{
					$('#<portlet:namespace/>allOtherLocation').prop("checked", true);
					$('#<portlet:namespace/>allLocations').prop("checked", false);
					}
				
	});
	
	$("input[id='<portlet:namespace/>allLocations']").click(function(){
 		$('#<portlet:namespace/>allLocationsCheckBox input[type=checkbox]:checked').removeAttr('checked');
 		$("#<portlet:namespace/>individualLocationCounter").html('<input type="checkbox" value="Abrams"> Locations (0) Selected');
	});

	$("input[id='<portlet:namespace/>practiceAreaCheckBox']").click(function(){
		
		var allPracticeAreaCounter = 0;
				 $('#<portlet:namespace/>practiceAreaDiv :checked').each(function() {
					 allPracticeAreaCounter++;
				    });
					
					$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="Abrams"> PracticeArea ('+allPracticeAreaCounter+') Selected');
					
					if(allPracticeAreaCounter == 0)
					{
					$('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", false);
					$('#<portlet:namespace/>allPracticeArea').prop("checked", false);
					}
				else
					{
					$('#<portlet:namespace/>allPracticeAreaIndividual').prop("checked", true);
					$('#<portlet:namespace/>allPracticeArea').prop("checked", false);
					}
				
	});


$("input[id='<portlet:namespace/>allPracticeArea']").click(function(){
		$('#<portlet:namespace/>practiceAreaCheckBox input[type=checkbox]:checked').removeAttr('checked');
		$("#<portlet:namespace/>practiceAreaCounter").html('<input type="checkbox" value="Abrams"> PracticeArea (0) Selected');
});


	

	
	
	/* $('#clearButton').bind('click', function (event) {
		clearFirms();
	});
	 */
	
	
	
	$('#<portlet:namespace/>clearPracticeArea').bind('click', function (event) {		
		<portlet:namespace/>clearPracticeAreas();
	});
	
	$('#<portlet:namespace/>clearButtonTitles').bind('click', function (event) {
		$('#<portlet:namespace/>popupTitles').find('input[type=checkbox]:checked').removeAttr('checked');		
		$('#<portlet:namespace/>allTitles').prop('checked', true);		
	});
	
	$('#<portlet:namespace/>clearButtonLocations').bind('click', function (event) {		
		<portlet:namespace/>clearLocations();
		
	});
	
	$('#<portlet:namespace/>clearButtonDate').bind('click', function (event) {
		$('#<portlet:namespace/>popupDate').find("option").attr("selected", false);
		$('#<portlet:namespace/>popupDate').find('input[type=text]').val('');
		$('#<portlet:namespace/>popupDate').find('input[type=radio]:checked').removeAttr('checked');				
		$("#<portlet:namespace/>dateText").val('<%=ALMConstants.LAST_WEEK%>');	
		$("#<portlet:namespace/>period").prop("checked", true); 
		$("#<portlet:namespace/>datePeriodSelect").val('<%=ALMConstants.LAST_WEEK%>');	
		$("#<portlet:namespace/>from").datepicker('enable');
		$("#<portlet:namespace/>to").datepicker('enable');
		<portlet:namespace/>applyDate();
	}); 
	
	//Change Type clear button event:Will clear all selected values in and reset change type values to default
	$('#<portlet:namespace/>clearButtonallChangeType').bind('click', function (event) {
		<portlet:namespace/>clearChangeType();
	});

	
	$('#<portlet:namespace/>ApplyFirm').bind('click', function (event) {
		<portlet:namespace/>applyFirms();
		$("#<portlet:namespace/>popup").toggle();
	});
	
	$('#<portlet:namespace/>ApplyPracticeArea').bind('click', function (event) {
		<portlet:namespace/>applyPracticeArea();
		 $("#<portlet:namespace/>popupPracticeArea").toggle();
	
	});
	
	$('#<portlet:namespace/>ApplyTitles').bind('click', function (event) {
		<portlet:namespace/>applyTitle();	
	});
	
	$('#<portlet:namespace/>ApplyLocations').bind('click', function (event) {		
		if($("#<portlet:namespace/>allLocations").is(":checked")){
			$("#<portlet:namespace/>selectedLocation").val('<%=ALMConstants.ALL_LOCATIONS%>');			
		} else{
				var allValsLocation = [];
				 $('#<portlet:namespace/>allOtherLocationsDiv :checked').each(function() {
					 allValsLocation.push($(this).val());
				    });
				    $("#<portlet:namespace/>selectedLocation").val(allValsLocation.join(";"));		
			   }
		
		 $("#<portlet:namespace/>popupLocation").toggle();	
	});
	
 	$('#<portlet:namespace/>ApplyDate').bind('click', function (event) {	
		
 		<portlet:namespace/>applyDate();
		 $("#<portlet:namespace/>popupDate").toggle();

	}); 

	$('#<portlet:namespace/>ApplyallChangeType').bind('click', function (event) {
		var allValsChnageType = [];		
		 $('#<portlet:namespace/>popupChangeType :checked').each(function() {		
				 allValsChnageType.push($(this).val());			
		    });
		    $("#<portlet:namespace/>selectedChangeType").val(allValsChnageType.join(";"));
		 $("#<portlet:namespace/>popupChangeType").toggle();
	
	});
	


	
/* 
	//this method get invoked when user clicks on Apply button from Practice Area popup and set selected practice area values to Practice area text box
	$('#ApplyPracticeArea').bind('click', function (event) {
		applyPracticeArea();
		 $("#popupPracticeArea").toggle();
	 */
	 
	// Function for Event search based on selected criteria like Firms,Practice Area,Location, Dates and Keywords
	 $("#<portlet:namespace/>applySeach").click(function() {	
		 <portlet:namespace/>search();
	}); 
	
	//autoselect previously selected search critria
	<portlet:namespace/>initializeSearchCriteria();
	$("#<portlet:namespace/>popupTitles").hide();
});

(function(){
      var del = 200;
      $('.icontent').hide().prev('a').hover(function(){
        $(this).next('.icontent').stop('fx', true).slideToggle(del);
      });
    })();
    
</script>
<script type="text/javascript">
	function setPage(goToPage)
	{
		// Sets the page number to the one selected by the user
		// and fires an AJAX submit to refresh with the new page
		// Does NOT tinker with the sort settings
		$("#<portlet:namespace/>goToPage").val(goToPage);
		$("#alertAttorneyMovesSearchModelBean").submit();
	}
	
	function sortResults(sortColumn)
	{
		// Changes the sort order to the new column selected by
		// the user along with the sort direction (ascending)
		// Also RESETS the page to 1
		
		var lastSortColumn  =  $("#<portlet:namespace/>sortColumn").val();
		
		if(lastSortColumn==sortColumn)
		{
			var lastSortOrder= $("#<portlet:namespace/>sortOrder").val();
			if(lastSortOrder=="asc")
			{
				$("#<portlet:namespace/>sortOrder").val("desc");
			}
			else if(lastSortOrder=="desc")
			{
				$("#<portlet:namespace/>sortOrder").val("asc");
			}
			else
			{
				$("#<portlet:namespace/>sortOrder").val("asc");
			}
		}
		else
		{
			$("#<portlet:namespace/>sortOrder").val("asc");
			
		}
		
		$("#<portlet:namespace/>sortColumn").val(sortColumn);
		$("#<portlet:namespace/>goToPage").val(1);
		$("#alertAttorneyMovesSearchModelBean").submit();
	}
	
	/* function sortDesc(sortColumn)
	{
		// Changes the sort order to the new column selected by
		// the user along with the sort direction (descending)
		// Also RESETS the page to 1
		$("#sortColumn").val(sortColumn);
		$("#sortOrder").val("desc");
		$("#goToPage").val(1);
		$("#alertAttorneyMovesSearchModelBean").submit();
	} */
	
	if(<%=isHomePage%>)
	{
		
		$('#homePageShowAttorneyMC').hide();
	}
	else
	{
		$('#homePageShowAttorneyMC').show();
	}
	
</script>

<%
	String displayStr = "";
	if(isHomePage)
	{
		displayStr = "display:none";
	}
	
%>

<div id="homePageShowAttorneyMC" style="<%=displayStr%>" >
<div class="breadcrumbs"><span>Moves and Changes</span></div>

<form:form class="beanForm"  commandName="alertAttorneyMovesSearchModelBean" method="post" action="${submitURL}" id="alertAttorneyMovesSearchModelBean">
 	<form:hidden path="goToPage"  id="goToPage" />
 	<form:hidden path="sortColumn" id="sortColumn" />
 	<form:hidden path="sortOrder"  id="sortOrder" /> 	
	
	<div id="dropdown">
		<!-- dropdown_title_start -->
		<ul id="droplist">			
 		<li><label>Firm(s)</label>
				<div class="srchBox" style="width:182px">
					
					     <c:choose>
			              <c:when test="${empty allWatchListsDefaultList}">
			          			<input type="text" name="Firmstext" id="Firmstext"  value="<%=ALMConstants.AMLAW_100 %>" style="text-overflow:ellipsis;" class="input" />
			          	  </c:when>
			          	  <c:otherwise>
			          			<input type="text" name="Firmstext" id="Firmstext"  value="" style="text-overflow:ellipsis;" class="input" />
			          	  </c:otherwise>
			          	</c:choose>
 
 
						  <!--  <input type="text" name="Firmstext" id="Firmstext"  style="text-overflow:ellipsis;" class="input" /> -->
						   <input type="button" name="search" value="" class="srchBack" id="testDiv" />
						   <div class="clear">&nbsp;</div>
				</div> <input type="button" name="search" id="hide" value="" class="typeSel" />


				<div class="rel">
					<div id="<portlet:namespace/>popup" class="firmPage">
						<p><form:radiobutton path="firmType" id="allfirms" value="<%=ALMConstants.ALL_FIRMS %>" />&nbsp;<%=ALMConstants.ALL_FIRMS %></p>
						<p> <form:radiobutton path="firmType" id="firm_watchlist" value=""/>Select Watchlist/ RivalEdge List</p>
					
						
						<div class="individualfirms-Watchlist" >
                			<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0 Selected)<br />
               			</div>
               			<div class="individualfirmsWatchList Select-Individual-Firms" id="<portlet:namespace/>individualfirmsWatchList"> 
						<form:checkbox  class="rivaledgeListAMLAW_100" path="firmList"  value="<%=ALMConstants.AMLAW_100 %>" /><%=ALMConstants.AMLAW_100 %><br>
						<form:checkbox  class="rivaledgeListAMLAW_200" path="firmList"  value="<%=ALMConstants.AMLAW_200 %>" /><%=ALMConstants.AMLAW_200 %><br>
						<form:checkbox  class="rivaledgeListNLJ_250" path="firmList"  value="<%=ALMConstants.NLJ_250 %>" /><%=ALMConstants.NLJ_250 %><br>
  
 				 		<div  class="individualfirmsWatchListDIV" id="<portlet:namespace/>individualfirmsWatchListDIV"> 
			                <c:forEach var="watchlist"  items="${allWatchLists}">     
		    	  				 <form:checkbox  class="allFirmsCheckBoxWatchList" path="firmListWatchList"  value="${watchlist.groupId}" labelAttr="${watchlist.groupName}" />${watchlist.groupName}<br>
		  				 	</c:forEach>
						</div>
						</div>
						<br>
						
						<p><form:radiobutton path="firmType" value="<%=ALMConstants.INDIVIDUAL_LIST %>" id="allOtherFirm"/>&nbsp;<%=ALMConstants.INDIVIDUAL_LIST %></p>
						
						<div class="individualfirms-first" id="<portlet:namespace/>individualFirmsCounter"> 
	               			 <input type="checkbox" value="selectAllFirms" id="selectAllFirms"/>Firms (0 Selected)<br />
              			</div>
						
						<div style="background-color: #FFFFFF;" class="Select-Individual-Firms"	id="<portlet:namespace/>allOtherFirmDiv">
							<c:forEach var="firm" items="${allFirms}">
								<form:checkbox  class="allFirmsCheckBox" id="allFirmsCheckBoxCounterAttorneyMovesAndChanges" path="firmList" value="${firm.companyId}" labelAttr="${firm.company}"/>${firm.company}<br>
							</c:forEach>
						</div>
 						<div class="popupsubmit">
 						 <input type="button" class="buttonOne" value="Apply" id="ApplyFirm">
							<input type="button" class="buttonTwo" value="Clear All" id="clearButton">
						</div>

					</div>
				</div>
				 <c:if test="${not empty allWatchListsDefaultList}">
					<input type="hidden" name="defaultWatchListId" id="defaultWatchListId" value="${allWatchListsDefaultList.groupId}" />
				</c:if>
			</li>
 		
			<li><label>Change Type</label>
				<div class="srchBox">
					<input type="text" name="search" value="" class="input" style="text-overflow: ellipsis;"
						id="selectedChangeType" /> <input type="button" name="search"
						value="" class="srchBack" />
					<div class="clear">&nbsp;</div>
				</div> <input type="button" name="search" value="" class="typeSel"
				id="changeTypeDropdown" />

				<div class="rel">
					<div id="<portlet:namespace/>popupChangeType" class="firmPage" style="width: 220px">
						<div id="<portlet:namespace/>changeType" style="background-color: #FFFFFF;">
							<form:checkbox path="changeType"  name="allChangeType" id="allChangeType" value="<%=ALMConstants.ALL_MOVES_CHANGES%>" />&nbsp;<%=ALMConstants.ALL_MOVES_CHANGES%><br>
							<div id="<portlet:namespace/>allOtherChangeTypeDiv">
								<form:checkbox path="changeType" name="allChangeTypeCheckBoxMovesAndChanges" value="<%=ALMConstants.ADDITIONS%>" id="AdditionsChangeType" />&nbsp;Additions<br>
								<form:checkbox path="changeType" name="allChangeTypeCheckBoxMovesAndChanges" value="<%=ALMConstants.REMOVALS%>"  id="RemovalsChangeType"/>&nbsp;Removals<br>
								<form:checkbox path="changeType" name="allChangeTypeCheckBoxMovesAndChanges" value="<%=ALMConstants.UPDATES%>"/>&nbsp;Updates<br>
							</div>
						</div>
						<div class="popupsubmit">
						<input type="button" class="buttonOne"
							value="Apply" id="ApplyallChangeType">
						<input type="button" class="buttonTwo" value="Clear All"
							id="clearButtonallChangeType"> 
						</div>
					</div>
				</div></li>
			<li><label>Location(s)</label>
				<div class="srchBox">
					<input type="text" value="<%=ALMConstants.ALL_LOCATIONS%>"
						class="input" name="selectedLocation" id="selectedLocation" style="text-overflow: ellipsis;" /> <input
						type="button" name="search" value="" class="srchBack" />
					<div class="clear">&nbsp;</div>
				</div> <input type="button" name="search" value="" id="locationId"
				class="typeSel" />
				<div class="rel">
					<div id="<portlet:namespace/>popupLocation" class="firmPage" style="width: 250px">
						<%-- <p>
							<form:radiobutton  name="location"  path="locations"  
								value="<%=ALMConstants.ALL_LOCATIONS%>" id="allLocations" 
								/>&nbsp;<%=ALMConstants.ALL_LOCATIONS%></p>

						<p>
							<input type="radio"	name="locations" value="Select Individual Locations" id="allOtherLocation" />&nbsp;Select
							Individual Locations
						</p>  --%>
						
						<p><form:radiobutton path="locations"  id="allLocations" value="<%=ALMConstants.ALL_LOCATIONS%>" /> <%=ALMConstants.ALL_LOCATIONS %></p>	
						<p><input type="radio"  id="allOtherLocation" value="Select Individual Locations" /> Select Individual Locations</p>
						
						<div class="individualfirms-first" id="<portlet:namespace/>individualLocationCounter"> 
								<input type="checkbox" value="Abrams"> Locations (0) Selected<br> </div>
						<div style="background-color:#FFFFFF;" class="Select-Individual-Firms" id="<portlet:namespace/>allOtherLocationsDiv">		
							<c:forEach items="${allOtherLocations}" var="location">
								<form:checkbox path="locations" name="allLocationsCheckBox" id="allLocationsCheckBox" class="allLocationsCheckBox" value="${location}"/>${location}<br>
							</c:forEach>	   
						</div>				
						<div class="popupsubmit">
						<input type="button" class="buttonOne" value="Apply" id="ApplyLocations">
						<input type="button" class="buttonTwo" value="Clear All" id="clearButtonLocations">
						</div>
					</div>
				</div></li>
			<li><label>Practice Area(s)</label>
				<div class="srchBox">
					<input type="text" value="All Practice Areas" class="input" style="text-overflow: ellipsis;"
						name="selectedPracticeArea" id="selectedPracticeArea" /> <input
						type="button" name="search" value="" class="srchBack" />
					<div class="clear">&nbsp;</div>
				</div> <input type="button" name="search" value="" id="practiceAreaId"
				class="drpDwn" />
				<div class="rel">
					<div id="<portlet:namespace/>popupPracticeArea" class="firmPage" style="width: 270px">
					
					<!-- <input type="radio" name="allPracticeArea"
							id="allPracticeArea" value="All Practice Areas"
							checked="checked">&nbsp;All Practice Areas<br> -->
					<p><form:radiobutton path="practiceArea" name="allPracticeArea" id="allPracticeArea" class="allPracticeArea" value="<%=ALMConstants.ALL_PRACTICE_AREAS %>" /><%=ALMConstants.ALL_PRACTICE_AREAS%></p>
					<p> <input  type="radio"  name="allPracticeArea"  id="allPracticeAreaIndividual" value="Select Individual Practice Areas">&nbsp;Select Individual Practice Areas</p>
					<div class="individualfirms-first" id="<portlet:namespace/>practiceAreaCounter"> <input type="checkbox" id="allPracticeAreasChecked">&nbsp;Practice Area (0 Selected)<br> </div>
					
						<!-- <div style="background-color: #FFFFFF;"
							id="practiceAreaDiv">
							<div id="allOtherPracticeArea">
								<c:forEach var="practice"
									items="${attorneymoveschangesModel['allPracticeArea']}">
									<input type="checkbox" name="practiceAreaCheckBox" id="practiceAreaCheckBox"
										value="${practice.practiceArea}">&nbsp;${practice.practiceArea}<br>
								</c:forEach>
							</div>
						</div>  -->
						 <div style="background-color:#FFFFFF;" id="<portlet:namespace/>practiceAreaDiv">										 
								<div style="background-color:#FFFFFF;" class="Select-Individual-Firms" id="<portlet:namespace/>allOtherPracticeArea">
									<c:forEach var="practice" items="${allPracticeArea}">								
										<form:checkbox path="practiceArea" value="${practice.practiceArea}"/> ${practice.practiceArea}<br>
									</c:forEach>
								</div>	
						 </div>
						 
						<div class="clear"></div> 
						<div class="popupsubmit">
						<input type="button" class="buttonOne" value="Apply" id="ApplyPracticeArea" />
						<input type="button" class="buttonTwo" value="Clear All" id="clearPracticeArea" />
						</div>
					</div>
				</div></li>

			<li><label>Date(s)</label>
				<div class="srchBox">
					 <!-- <input name="Datetext" id="datetext" value="" readonly="readonly"
						class="input" style="width: 140px" />  -->
					 <form:input path="dateText" id="dateText" readonly="true" value="" cssClass="input" cssStyle="width:140px"/>
					
					<div class="clear">&nbsp;</div>
				</div> <input type="button" name="search" id="datenone" value=""
				class="drpDwn" />
				<div class="rel">
					<div id="<portlet:namespace/>popupDate" class="datePage">
						<input type="radio" name="date" id="period" />&nbsp;Period<br> <select
							id="datePeriodSelect">
							<option value="Any">Any</option>
							<option value="Last Week">Last Week</option>
							<option value="Last 30 Days">Last 30 Days</option>
							<option value="Last 90 Days">Last 90 Days</option>
							<option value="Last 6 Months">Last 6 Months</option>
							<option value="Last Year">Last Year</option>
						</select> <br> <input type="radio" name="date" id="dateRange"
							checked="checked" />&nbsp;Date Range<br>

						<div class="flLeft">
							<label for="from">From</label> <input type="text" id="from"
								name="from" />
						</div>
						<div class="flLeft">
							<label for="to">To</label> <input type="text" id="to" name="to" />
						</div>
						<div class="clear"></div>
						<span class="error" id="fromdateError"> Invalid From Date</span> <span
							class="error" id="todateError"> Invalid To Date</span> <span
							class="error" id="dateValidError"> To date should be more
							than from date</span> 
							<div class="popupsubmit Popup-Calendar">
							<input type="button" value="Apply"  class="buttonOne"
							id="ApplyDate">
							<input type="button" value="Clear All" class="buttonTwo"
							id="clearButtonDate"> 
							</div>
					</div>
				</div></li>
			<li><label>&nbsp;</label> <input type="button" value="Apply"
				class="buttonOne"
				style="background:url(<%=themeDisplay.getPathThemeImages()%>/btn1.png) 0 0 repeat-x; border:1px solid #bf8d1f; -webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px; font:normal 12px Arial, Helvetica, sans-serif; color:#fff; padding:5px 10px; cursor:pointer; text-shadow: 0px 0px #FFF;"
				id="applySeach" /></li>
			<li><label>&nbsp;</label> <input type="button" value="Reset All"
				style="background:url(<%=themeDisplay.getPathThemeImages()%>/btn2.png) 0 0 repeat-x; border:1px solid #565656; -webkit-border-radius:3px; -moz-border-radius:3px; border-radius:3px; font:normal 12px Arial, Helvetica, sans-serif; color:#fff; padding:5px 10px; cursor:pointer; text-shadow: 0px 0px #FFF;"
				class="buttonTwo" id="resetAll" /></li>
			<li><label>&nbsp;</label> 
			<%-- <input type="hidden" name="selectedFirms" id="selectedFirms" value="<%=ALMConstants.AMLAW_100%>" /> --%> 
			<input type="hidden"  name="selectedFirms" id="selectedFirms"  title="" customAttr="" /></li>

		</ul>
		<div class="clear">&nbsp;</div>
	</div>
	
	<div class="filtersPage" id="<portlet:namespace/>additional" style="">
		<div id="dropdown">
			<!-- dropdown_title_start -->
			<ul id="droplist">
				<li><label>Title(s)</label>
					<div class="srchBox">
						<input type="text" name="search" value="All" class="input" style="text-overflow: ellipsis;" id="selectedTitles" />
							 <input type="button" name="search" value="" class="srchBack" />
						<div class="clear">&nbsp;</div>
					</div> <input type="button" name="search" value="" class="typeSel"
					id="titleDropdown" />

					<div class="rel">
						<div id="<portlet:namespace/>popupTitles" class="firmPage" style="width: 220px">

							<div style="background-color: #FFFFFF;" id="<portlet:namespace/>titlesDiv">
								<form:checkbox  name="allTitles" path="titles"
									value="<%=ALMConstants.ALL_TITLES%>" id="allTitles" />&nbsp;<%=ALMConstants.ALL_TITLES%>
								<div id="<portlet:namespace/>allOtherTitlesDiv">
									<form:checkbox path="titles" name="allTitlesCheckBox" value="<%=ALMConstants.PARTNERS%>" />&nbsp;<%=ALMConstants.PARTNERS%><br>
									<form:checkbox path="titles" name="allTitlesCheckBox" value="<%=ALMConstants.ASSOCIATE%>" />&nbsp;<%=ALMConstants.ASSOCIATE%><br>
									<form:checkbox path="titles" name="allTitlesCheckBox" value="<%=ALMConstants.OTHER_COUNSEL%>" />&nbsp;<%=ALMConstants.OTHER_COUNSEL%><br>
									<form:checkbox path="titles" name="allTitlesCheckBox" value="<%=ALMConstants.ADMINISTRATIVE%>" />&nbsp;<%=ALMConstants.ADMINISTRATIVE%><br>
									<form:checkbox path="titles" name="allTitlesCheckBox" value="<%=ALMConstants.OTHER%>" />&nbsp;<%=ALMConstants.OTHER%><br>
								</div>
							</div>
							
							<div class="popupsubmit">
								<input type="button" class="buttonOne" value="Apply" id="ApplyTitles">
								<input type="button" class="buttonTwo" value="Clear All" id="clearButtonTitles">
							</div>

						</div>
					</div></li>

				<li><label>Name</label>
					<div class="srchBox" style="width: 175px">
						<!-- <input type="text" name="search" class="input"
							style="width: 140px" id="name" size="100" /> -->
						<form:input path="name" id="name" name="search" value="" cssClass="input" cssStyle="width:140px" size="100"/>
						<input
							type="button" name="search" value="" class="srchBack" />
						<div class="clear">&nbsp;</div>
					</div></li>
			<!-- Sachin: code commented to fix the bug 415:  Eliminate Keyword in Bio Search Field -->
			<!-- 	<li>
					<div class="col2-1">
						<label>Keywords in Bio</label>
						<div class="srchBox">
							<form:input path="keywords" id="keywords" name="keywordsearch" value="" cssClass="input" cssStyle="width:140px" size="100"/>
							<div class="clear">&nbsp;</div>
						</div>
					
					</div>
				</li>  -->
			</ul>
			<div class="clear">&nbsp;</div>
		</div>
		
	</div>

<!-- 	<ul id="tabs">
		<li><a class="active" href="#" title="tab2"><span>Details</span></a></li>
		<li><a href="#" title="tab3"><span>Analysis</span></a></li>
	</ul> 
	<div id="resultsDataDiv"></div> -->
	
	<div style="display: none; position:absolute; margin:0; padding:0; background:#F0F0F0; border:none; width:400px !important;" class="viewBox popusdiv ClickPopup newspublicationPage charts" id="<portlet:namespace/>viewSettingsPopup">
              <div class="popHeader" style="font-weight:bold;"><a style="margin-top:2px" class="btn icon closewhite closeOne flRight" href="#" id="viewSettingClose">Close</a> SETTINGS: ATTORNEY MOVES AND CHANGES
                <div class="clear">&nbsp;</div>
              </div>
              <div class="popMiddle">           
                <div class="col3-4">
                  <h6>Group By:</h6>
                  <form action="#" class="marlt3">
                    <ul class="reset list5">                   
                        
						  
						  <li> <form:radiobutton path="orderBy" value="-1"/>None <br /> </li>
						  <li> <form:radiobutton path="orderBy" value="6" />Firm <br />  </li>
						  <li> <form:radiobutton path="orderBy" value="8"/>Title <br /> </li>
						  <li><form:radiobutton path="orderBy" value="9"/>Location <br /></li> 
						  <li> <form:radiobutton path="orderBy" value="10"/>Practice <br /> </li>
                    </ul>
                  </form>
                </div>
                <div class="col3-4">
                  <h6>Display No. of Results</h6>
                  <form:radiobutton path="searchResultsPerPage"  value="25"  />25<br />
			     <form:radiobutton path="searchResultsPerPage"  value="50"  />50<br />
			     <form:radiobutton path="searchResultsPerPage"  value="100" />100<br />
			     <form:radiobutton path="searchResultsPerPage"  value="500" />500<br />
                </div>
                <div class="clear">&nbsp;</div>
              </div>
              <div class="popFooter">
                <div class="flLeft">
                  <input type="button" class="buttonTwo" value="Reset All" id="resetAllViewSetting">
                </div>
                <div class="flRight">
                  <input type="button" class="buttonOne" value="Apply" id="ApplySetting" onclick="<portlet:namespace/>applyViewSettings();">
                  <input type="button" class="buttonTwo" value="Cancel" id="CancelSetting">
                </div>
                <div class="clear">&nbsp;</div>
              </div>
            </div>
</form:form>
</div>
<!-- 
<script type="text/javascript">

//Omniture SiteCatalyst - START
s.prop22 = "premium";
s.pageName="rer:Attorny-Moves-changes-search";
s.channel="rer:Attorny-Moves-changes";
s.server="rer";
s.prop1="Attorny-Moves-changes"
s.prop2="Attorny-Moves-changes";
s.prop3="Attorny-Moves-changes";
s.prop4="Attorny-Moves-changes";
s.prop21=s.eVar21='current.user@foo.bar';	
s.events="event2";
s.events="event27";
s.t();
//Omniture SiteCatalyst - END
</script>
 -->

