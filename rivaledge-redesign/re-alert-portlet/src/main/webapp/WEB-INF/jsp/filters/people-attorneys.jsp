<%@page import="com.alm.rivaledge.transferobject.FirmSearchDTO"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.alm.rivaledge.util.ALMConstants"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>

<%@ taglib prefix="portlet" 		uri="http://java.sun.com/portlet_2_0"%> 
<%@ taglib prefix="liferay-util"    uri="http://liferay.com/tld/util"%>
<%@ taglib prefix="spring"          uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form"            uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="liferay-portlet" uri="http://liferay.com/tld/portlet"%>
<%@ taglib prefix="c" 			 	uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="liferay-ui"   	uri="http://liferay.com/tld/ui"%>

<portlet:defineObjects />

<link rel="stylesheet" href="<%=renderRequest.getContextPath()%>/css/jquery-ui.css" />
<!-- 
<script src="<%=renderRequest.getContextPath()%>/js/jquery-1.9.1.js"></script>
<script src="<%=renderRequest.getContextPath()%>/js/jquery-ui.js"></script>
<script src="<%=renderRequest.getContextPath()%>/js/highcharts.js"></script>
<script src="<%=renderRequest.getContextPath()%>/js/exporting.src.js"></script>
<script src="<%=renderRequest.getContextPath()%>/js/s_code.js"></script>
 -->
<portlet:actionURL var="changeSearchCriteria">
	<portlet:param name="action" value="changeSearchCriteria"/>
</portlet:actionURL>

<%--
This script tag contains functions related to Attorney Search actions 
 It has only function definitions and no jQuery hooks or handlers.
Please make sure you have all the function definitions at single place and loaded first
--%>

<portlet:resourceURL var="attorneySearch" id="attorneySearch" >
</portlet:resourceURL>

<portlet:resourceURL var="attorneyLawSchool" id="attorneyLawSchool" >
</portlet:resourceURL>

<portlet:resourceURL var="attorneyAdmissions" id="attorneyAdmissions" >
</portlet:resourceURL>

<portlet:resourceURL var="attorneyKeywords" id="attorneyKeywords" >
</portlet:resourceURL>

<portlet:resourceURL var="updateModelBean" id="updateModelBean" >
</portlet:resourceURL>    

<script type="text/javascript">
	
	/* View Settings UI logic*/
	
	var displayColumnsChanged = false;
	var groupByValuesChanged = false;
	var resultsPerPageValuesChanged = false;
	
	$(document).ready(function(){
		
		//Type ahead for Name field
		$("#name").autocomplete({
			  source: function(request, response){
			    $.post('<%=attorneySearch.toString()%>', {data:$("#name").val()}, function(data){     
			        response($.map(data, function(item){
			        return {
			            label: item
			        }
			        }))
			    }, "json");
			  },
			  minLength: 3,
			  dataType: "json",
			  cache: false,
			  focus: function(event, ui) {
			    return false;
			  },
			  select: function(event, ui) {
			    this.value = ui.item.label;
			    return false;
			  }
		});
		
		//Type ahead for Law School field
		<%-- $("#selectedLawSchool").autocomplete({
			  source: function(request, response){
			    $.post('<%=attorneyLawSchool.toString()%>', {data:$("#selectedLawSchool").val()}, function(data){     
			        response($.map(data, function(item){
			        return {
			            label: item
			        }
			        }))
			    }, "json");
			  },
			  minLength: 3,
			  dataType: "json",
			  cache: false,
			  focus: function(event, ui) {
			    return false;
			  },
			  select: function(event, ui) {
			    this.value = ui.item.label;
			    return false;
			  }
		});
		
		//Type ahead for Admissions field
		$("#selectedAdmissions").autocomplete({
			  source: function(request, response){
			    $.post('<%=attorneyAdmissions.toString()%>', {data:$("#selectedAdmissions").val()}, function(data){     
			        response($.map(data, function(item){
			        return {
			            label: item
			        }
			        }))
			    }, "json");
			  },
			  minLength: 3,
			  dataType: "json",
			  cache: false,
			  focus: function(event, ui) {
			    return false;
			  },
			  select: function(event, ui) {
			    this.value = ui.item.label;
			    return false;
			  }
		});
		
		//Type ahead for Keywords field
		$("#keywordsInBio").autocomplete({
			  source: function(request, response){
			    $.post('<%=attorneyKeywords.toString()%>', {data:$("#keywordsInBio").val()}, function(data){     
			        response($.map(data, function(item){
			        return {
			            label: item
			        }
			        }))
			    }, "json");
			  },
			  minLength: 3,
			  dataType: "json",
			  cache: false,
			  focus: function(event, ui) {
			    return false;
			  },
			  select: function(event, ui) {
			    this.value = ui.item.label;
			    return false;
			  }
		}); --%>
		
	});
	
	/*
	* User has changed the Display Columns selection
	* Push them to hidden field that will be send to server on ajax call
	*/
	function changeOnDisplayColumns()
	{
		displayColumnsChanged = true;
		var displayColumnsList = [];
		$("input[name=displayColumns]:checked").each(function(){
			displayColumnsList.push($(this).val());
		});
		
		$("#displayColumns_hidden").val(displayColumnsList.join(";"));
		//alert($("#displayColumns_hidden").val());
	}
	
	/* Tracks if User changed Group By values
	 *
	 */
	function changeOnGroupByValues()
	{
		groupByValuesChanged = true;
	}
	
	/* Tracks if User changed Results per page values
	 *
	 */
	function changeOnResultsPerPageValues()
	{
		resultsPerPageValuesChanged = true;
	}
	
	/*
	* Apply view settings
	*/
	function applyViewSettings() 
	{
	
		if(!groupByValuesChanged && !resultsPerPageValuesChanged) // don't hit server as user changed only displayColumns
		{
			toggleSearchTableColumns();
			
			//Ajax call to server for setting display columns to get those columns in print page
			$.ajax({
			    url:"<%=updateModelBean.toString()%>",
			    method: "POST",
			    data: 
			    {
			    "displayColumns":$("#displayColumns_hidden").val()
			    },
			    success: function(data)
		        {
		    	
		        },
			   	error: function(jqXHR, textStatus, errorThrown) 
			   	{
			    	alert("error:" + textStatus + " - exception:" + errorThrown);
				}
			});
		}
		else
		{
			search(); //user changed something else we need fresh data from server
		}
		
		displayColumnsChanged = false;
		groupByValuesChanged = false;		
		resultsPerPageValuesChanged = false;
		$(".attorneysearch").toggle();  
	}
	
	function toggleViewSettings(vsId)
	{
		
		//get the position of the placeholder element
		var vsPos   = $(vsId).offset();
	   	var vsHeight = $(vsId).height();
	   	var vsWidth = $(vsId).width();
	    //show the menu directly over the placeholder
	    var popupWidth = $(".attorneysearch").width();
	    $(".attorneysearch").css({  position: "absolute", "left": (vsPos.left - popupWidth + vsWidth) + "px", "top":(vsPos.top + vsHeight)  + "px" });
	    
	    $(".attorneysearch").toggle();
	}	
	
	function cancelViewSettings()
	{
		$(".attorneysearch").toggle(); 
	}
	
	//reset view setting popup to default state
	function resetViewSettings() {
		//clear all
		$('.attorneysearch').find('input[type=checkbox]:checked').removeAttr('checked');
		$('.attorneysearch').find('input[type=radio]:checked').removeAttr('checked');

		//select interested fields
		$('input:radio[name=orderBy]:nth(1)').prop('checked',true);  //Firm name, the 2nd radiobutton
		groupByValuesChanged = true;
		$('input:radio[name=searchResultsPerPage]:nth(2)').prop('checked',true);  //100, the 3rd radiobutton
		resultsPerPageValuesChanged = true;	
		for(var i = 0; i < 5 ; i++) //only first 5 columns shud be visible
		{
			$('input:checkbox[name=displayColumns]:nth('+ i +')').prop('checked',true);
		}
		changeOnDisplayColumns(); //notify that displayColumns are changed, to refresh the displayColumns_hidden field
		//$(".attorneysearch").stop().slideToggle(500); //Keep user to that popup view and let user click on Apply
	}
	
	
	/*
	* Hide or display columns based on user selection
	* This will be invoked as and when 1. The search results are rendered
	*								   2. User changes the display columns and apply on view settings popup
	*/
	function toggleSearchTableColumns()
	{
		var columnIndex = 1;  //Exclude first 2 columns 2 = Attorney name
		
		$("input[name=displayColumns]").each(function(){
			if($(this).is(":checked"))
				{
					$("#attorneySearchResultsTable th:nth-child(" + (columnIndex+1) + "), #attorneySearchResultsTable td:nth-child(" + (columnIndex+1) + ")").show();
				}
			else
				{
					$("#attorneySearchResultsTable th:nth-child(" + (columnIndex+1) + "), #attorneySearchResultsTable td:nth-child(" + (columnIndex+1) + ")").hide();
				}
			columnIndex++;
		});
	}
	
	
	/**
	*Autoselect previously selected search critria
	*/
	function initializeSearchCriteria()
	{
		applyFirms();
		applyLocations();
		applyPracticeArea();
		applyTitles();
		refreshCounters();
	}
	
	//Apply Buttons functions
	function applyFirms()
	{
		
		var checkFlag = $("#allfirms").is(":checked");
		var valueofchecked =$("input[name='firmType']:checked").val();
		if(valueofchecked=="<%=ALMConstants.ALL_FIRMS%>"){		
			checkFlag=true;
			 $('#allfirms').prop("checked", true);
		}
		
		var allSelectedValue = [];
		var allSelectedIds = [];
		
		 $("#Firmstext").val('');
		 $("#selectedFirms").val('');
		 
		var isFirmChecked = false;
		var allWatchListCounter = 0;
		var counter = 0;
		var allFirmsCounter = 0
	
		
			$('.individualfirmsWatchList input[type=checkbox]:checked').each(function() {
				
				 allSelectedValue.push($(this).attr('labelAttr'));
				 allSelectedIds.push($(this).val());
				 $("#firm_watchlist").prop("checked", true);
				 $('#firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
				 
				 var watchListId = $('#defaultWatchListId').val();
					
					$('#individualfirmsWatchListDIV input[type=checkbox]').each(function() {
						 if(this.value == watchListId)
							{
								$(this).prop("checked", true);
							}
					});
					 //$('#firm_watchlist').prop("checked", true);
				 
				 $('#individualfirmsWatchListDIV input[type=checkbox]:checked').each(function() 
					{
							allWatchListCounter++;
					});
							
					$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (' + allWatchListCounter +' Selected)');
		 		});
		 
		$('#allOtherFirmDiv input[type=checkbox]:checked').each(function() {
			
			
			 allSelectedValue.push($(this).attr('labelAttr'));
			 allSelectedIds.push($(this).val());
			 $("#allOtherFirm").prop("checked", true);
			 $(".rivaledgeListAMLAW_100").prop("checked", false);
			 $(".rivaledgeListAMLAW_200").prop("checked", false);
			 $(".rivaledgeListNLJ_250").prop("checked", false);
	 	});
		
		if(allSelectedValue != ''  &&  allSelectedIds != '') 
			{
				$("#Firmstext").val(allSelectedValue.join(",")); //Use a comma separator for values which show up on UI
				$("#selectedFirms").val(allSelectedIds.join(";"));
			}
		else
		{
			//$("#allfirms, #firm_watchlist, #firm_individual").each(function(){
				$("#allfirms, .rivaledgeListAMLAW_100, .rivaledgeListAMLAW_200, .rivaledgeListNLJ_250").each(function(){
				
				if($("#allfirms").is(":checked"))
					{
					
					
						$("#Firmstext").val('<%=ALMConstants.ALL_FIRMS %>');
						$("#selectedFirms").val('<%=ALMConstants.ALL_FIRMS %>');
						$('#firm_watchlist').val("<%=ALMConstants.ALL_FIRMS %>");
					} 
				else if($(".rivaledgeListAMLAW_100").is(":checked"))
				{
					
				
					$("#Firmstext").val('<%=ALMConstants.AMLAW_100 %>');
					$("#selectedFirms").val('<%=ALMConstants.AMLAW_100 %>');
					$('#firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
					$('#individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
				} 
				else if($(".rivaledgeListAMLAW_200").is(":checked"))
				{
					
					$("#Firmstext").val('<%=ALMConstants.AMLAW_200 %>');
					$("#selectedFirms").val('<%=ALMConstants.AMLAW_200 %>');
					$('#firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
					$('#individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
				} 
				else if($(".rivaledgeListNLJ_250").is(":checked"))
				{
					
					$("#Firmstext").val('<%=ALMConstants.NLJ_250 %>');
					$("#selectedFirms").val('<%=ALMConstants.NLJ_250 %>');
					$('#firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
					$('#individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
				} 
				 else
					{
					
					 if($(this).is(":checked")){
							var value =$(this).val();
						  //$("#Firmstext").val(value);
						 	//$("#selectedFirms").val(value);
							$("#Firmstext").val('AMLAW 100');
							$("#selectedFirms").val('AMLAW 100');
							$('#firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
						} 
					}
			
				
			});
		}
		
	}
	
	function applyLocations()
	{

		 if($("#allLocations").is(":checked"))
		 {
				$("#selectedLocation").val($("#allLocations").val());
		 } 
		
		 else
		 {
			 var allValsLocation = [];
			 var allLocationCounter = 0;
				$('#allOtherLocationsDiv input[type=checkbox]:checked').each(function() { 
					 allValsLocation.push($(this).val());
					 allLocationCounter++;
			    });
				$("#locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="selectAllLocations" />  Locations ('+allLocationCounter+') Selected');

			if(allValsLocation.length > 0){
				$("#allOtherLocations").prop("checked", true);
			    $("#selectedLocation").val(allValsLocation.join(";"));
			    //alert($("#selectedLocation").val());
			}
			
		}
	}


	function applyPracticeArea()
	{
		
		if($(".allPracticeArea").is(":checked"))
		 {
				$("#selectedPracticeArea").val($(".allPracticeArea").val());			
		 } 
		 else
		 {
			var allValsPracticeArea = [];
			
			var allPracticeCounter = 0;
				
			
			$('#practiceAreaDiv input[type=checkbox]:checked').each(function() { 
				 	allValsPracticeArea.push($(this).val());
				 	allPracticeCounter++;
			 });
				 
			$("#practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" /> PracticeAreas ('+allPracticeCounter+') Selected');
			
			if(allValsPracticeArea.length > 0)
			{
				$("#allPracticeAreaIndividual").prop("checked", true);
				$("#selectedPracticeArea").val(allValsPracticeArea.join(";"));		
			}
		}
	}
	
	
	function applyTitles()
	{
		
		var allValsTitles = [];
		 $('#titleSelectDiv :checked').each(function() {
			 allValsTitles.push($(this).val());
		    });
		 $("#selectedTitles").val(allValsTitles.join(";"));
		    
	}
	
	
	/* Clear Buttons functions*/
	
	function clearAll()
	{
		
		var watchListId = $('#defaultWatchListId').val();
		var isFlag = false;
		//Firms reset
		clearFirms();
		
		//Practice Area reset
		clearPracticeAreas();
		
		//Location reset
		clearLocations();
		//Titles reset
		
		clearTitles();
		
		//School graduation reset
		$('#graduationFrom').val('<%=ALMConstants.ANY%>'); 
		$('#graduationTo').val('<%=ALMConstants.ANY%>'); 
		
		//law school reset
		$('#selectedLawSchool').val(''); 

		//Admissions reset
		$('#selectedAdmissions').val('');
		
		//name reset
		$('#name').val('');
		
		//keyword reset
		$('#keywordsInBio').val(''); 
		
		$('#individualfirmsWatchListDIV input[type=checkbox]').each(function() {
			 if(this.value == watchListId)
				{
					$(this).prop("checked", true);
					$('#firm_watchlist').prop("checked", true);
					$("#Firmstext").val($(this).attr('labelattr')); 
					$("#selectedFirms").val($(this).val());
					$('#firm_watchlist').val('<%=ALMConstants.WATCH_LIST %>');
					isFlag = true;
				}
		});
		
		if(isFlag == false)
		{
			$('#firm_watchlist').prop("checked", true);
			$('.rivaledgeListAMLAW_100').prop("checked", true);
			$("#Firmstext").val('<%=ALMConstants.AMLAW_100 %>');
			$("#selectedFirms").val('<%=ALMConstants.AMLAW_100 %>');
			$('#firm_watchlist').val('<%=ALMConstants.RIVALEDGE_LIST %>');
		}
	}
	
function clearFirms()
{
	
	$('#popup').find("option").attr("selected", false);
	$('#popup').find('input[type=checkbox]:checked').removeAttr('checked');
	$('#popup').find('input[type=radio]:checked').removeAttr('checked');
	
	$("#individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/>  Firms (0) Selected');

	$("#RivalEdgeList").prop("checked", true);
	$('#Select1').val('<%=ALMConstants.AMLAW_100%>');
	applyFirms();
}

function clearLocations()
{
	$('#popupLocation').find('input[type=checkbox]:checked').removeAttr('checked');
	$('#popupLocation').find('input[type=radio]:checked').removeAttr('checked');
	$("#locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="selectAllLocations" />  Locations (0) Selected');
	$('#allLocations').prop("checked", true);
	applyLocations();
}


function clearPracticeAreas()
{
	$('#popupPracticeArea').find('input[type=checkbox]:checked').removeAttr('checked');
	$('#popupPracticeArea').find('input[type=radio]:checked').removeAttr('checked');
	$('#allOtherPracticeArea input').find('input[type=checkbox]:checked').removeAttr('checked');
	$("#practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" /> Practice Areas (0) Selected');
	$('.allPracticeArea').prop('checked', true);	
	applyPracticeArea();
}

function clearTitles()
{
	$('#titleSelectDiv').find('input[type=checkbox]:checked').removeAttr('checked');
	$('#allTitles').prop("checked", true);
	applyTitles();
}
	
/*Refresh counter values*/
	
function refreshCounters()
{
	var allFirmsCounter = 0;
	$('#allOtherFirmDiv input[type=checkbox]:checked').each(function() {
	 allFirmsCounter++;
	});
	$("#individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/>  Firms ('+allFirmsCounter+') Selected');
	
	var allLocationCounter = 0;
	 $('#allOtherLocationsDiv input[class=allLocationsCheckBox]:checked').each(function() {
		 allLocationCounter++;
	    });
	$("#locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="selectAllLocations" />  Locations ('+allLocationCounter+') Selected');

	var allPracticeCounter = 0;
	$('#allOtherPracticeArea input[type=checkbox]:checked').each(function() {
		 allPracticeCounter++;
	    });
	$("#practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" /> PracticeAreas ('+allPracticeCounter+') Selected');
}

	
	
	/* Change on individual checkboxes(select/deselct) functions*/
	
	function changeOnIndividualLocations()
	{
		var allLocationsCounter = 0;
		 $('#allOtherLocationsDiv :checked').each(function() {
			 allLocationsCounter++;
		    });
				$("#locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="selectAllLocations" />  Locations ('+allLocationsCounter+') Selected');
		 
			if(allLocationsCounter == 0)
			{
				$('#allOtherLocations').prop("checked", false);
				$('#allLocations').prop("checked", true);
				
			}
			else
			{
				$('#allOtherLocations').prop("checked", true);
				$('#allLocations').prop("checked", false);
			}
					
	}

	function changeOnIndividualPracticeAreas()
	{
		
			var allPracticeCounter = 0;
		
			$('#allOtherPracticeArea input[type=checkbox]:checked').each(function() {
				 allPracticeCounter++;
		    });
			
			$("#practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" /> PracticeAreas ('+allPracticeCounter+') Selected');
			
			if(allPracticeCounter == 0)
			{
				$('#allPracticeAreaIndividual').prop("checked", false);
				$('.allPracticeArea').prop("checked", true);
			}
			else
			{
				$('#allPracticeAreaIndividual').prop("checked", true);
				$('.allPracticeArea').prop("checked", false);
			}
	}

	
	
	/* Search Actions' functions*/
	
	function ajaxSubmit()
	{
		
		//$("#searchForm").mask("Waiting...");
		 $.ajax({
			    url: "${applySearchUrl}",
			    method: "GET",
			    data: {
			    	"firmType":$("input[name=firmType]:checked").val(),
		    		"firmList":$("#selectedFirms").val(),
			    	"locations":$("#selectedLocation").val(),
			    	"practiceArea":$("#selectedPracticeArea").val(),
			    	"title":$("#selectedTitles").val(),
			    	"fromDate":$("#graduationFrom").val(),
			    	"toDate":$("#graduationTo").val(),
			    	"lawSchools":$("#selectedLawSchool").val(),
			    	"admissions":$("#selectedAdmissions").val(),
			    	"name":$("#name").val(),
			    	"keywords":$("#keywordsInBio").val(),
			    	"displayColumns" : $("#displayColumns_hidden").val(),
			    	"goToPage":$("#goToPage").val(),
			    	"sortColumn":$("#sortColumn").val(),
				    "sortOrder":$("#sortOrder").val(),
			    	"orderBy" :$("input[name=orderBy]:checked").val(),
			    	"searchResultsPerPage" :$("input[name=searchResultsPerPage]:checked").val()
			    	},
			    success: function(data){
			    		
			    	$("#attorneyResultDiv").html(data);
			    	$('body').removeClass("modal");
			        },
			    error: function(jqXHR, textStatus, errorThrown) {
			            alert("error:" + textStatus + " - exception:" + errorThrown);
			   		}
			    }); 
	}

	function search()
	{
		$('#attorneySearchModelBean').submit();
	}
	
	function setPage(goToPage)
	{
		// Sets the page number to the one selected by the user
		// and fires an AJAX submit to refresh with the new page
		// Does NOT tinker with the sort settings
		$("#goToPage").val(goToPage);
		search();
	}
	
	function sortResults(sortColumn)
	{
		// Changes the sort order to the new column selected by
		// the user along with the sort direction (ascending)
		// Also RESETS the page to 1		
		var lastSortColumn  =  $("#sortColumn").val();
		
		if(lastSortColumn==sortColumn)
		{
			var lastSortOrder= $("#sortOrder").val();
			if(lastSortOrder=="asc")
			{
				$("#sortOrder").val("desc");
			}
			else if(lastSortOrder=="desc")
			{
				$("#sortOrder").val("asc");
			}
			else
			{
				$("#sortOrder").val("asc");
			}
		}
		else
		{
			$("#sortOrder").val("asc");
			
		}
		
		$("#sortColumn").val(sortColumn);
		$("#goToPage").val(1);
		search();
	}
	
	
	function sortDesc(sortColumn)
	{
		// Changes the sort order to the new column selected by
		// the user along with the sort direction (descending)
		// Also RESETS the page to 1
		$("#sortColumn").val(sortColumn);
		$("#sortOrder").val("desc");
		$("#goToPage").val(1);
		search();
	}
	
</script>

<script>


$.widget( "custom.catcomplete", $.ui.autocomplete, {
	_renderMenu: function( ul, items ) {
		var that = this,
		currentCategory = "";
		$.each( items, function( index, item ) {
			if ( item.category != currentCategory ) {
			ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
			currentCategory = item.category;
			}
	  that._renderItemData( ul, item );
	});
}
});

  $(document).ready(function() {
  
	var data = ${firmJson};
	var practiceData = ${practiceJson};
	
	//dont initialize here 
	//$("#RivalEdgeList").prop("checked", true);
	//$("#graduationTo option[value='2013']").attr('selected', 'selected');	


	var availableLocation = new Array();

	<c:forEach items="${allOtherLocations}" var="loc">
		availableLocation.push("${loc}");
	</c:forEach>

function split( val ) {
	return val.split( /,\s*/ );
}

function extractLast( term ) {
	return split( term ).pop();
}
  

$("#selectedLocation").bind( "keydown", function( event ) {
// don't navigate away from the field on tab when selecting an item
  if ( event.keyCode === $.ui.keyCode.TAB &&
      $( this ).data( "ui-autocomplete" ).menu.active ) {
    		event.preventDefault();
  		}
	}).autocomplete({
  		minLength: 0,
  		source: function( request, response ) {
    	// delegate back to autocomplete, but extract the last term
    	response( $.ui.autocomplete.filter(
    		availableLocation, extractLast( request.term ) ) );
  		},
  		focus: function() {
    	// prevent value inserted on focus
    	return false;
  		}, select: function( event, ui ) {
    		var terms = split( this.value );
    		// remove the current input
    		terms.pop();
    		// add the selected item
    		terms.push( ui.item.value );
    		// add placeholder to get the comma-and-space at the end
    		terms.push("");
    		this.value = terms.join(";");
    		//alert(this.value);
    		
    		$('#allOtherLocationsDiv input[type=checkbox]').each(function()
    		{ 
				if($(this).val() == (ui.item.value) )
				{
					$(this).prop("checked", true);
					changeOnIndividualLocations();
					return false;
				}
			});
    		return false;
  		}
	});


$( "#selectedPracticeArea" )
	//don't navigate away from the field on tab when selecting an item
	.bind( "keydown", function( event ) {
		if ( event.keyCode === $.ui.keyCode.TAB &&
		$( this ).data( "ui-autocomplete" ).menu.active ) {
		event.preventDefault();
		}
	})
	.autocomplete({
	minLength: 3,
	source: function( request, response ) {
		//delegate back to autocomplete, but extract the last term
		response( $.ui.autocomplete.filter(
			practiceData, extractLast( request.term ) ) );
	},
	focus: function() {
		//prevent value inserted on focus
		return false;
	},
	select: function( event, ui ) {
		var terms = split( this.value );
		//remove the current input
		terms.pop();
		//add the selected item
		terms.push( ui.item.value );
		//add placeholder to get the comma-and-space at the end
		terms.push( "" );
		this.value = terms.join( ";" );
		
		$('#allOtherPracticeArea input[type=checkbox]').each(function()
   		{ 
			if($(this).val() == (ui.item.value) )
			{
				$(this).prop("checked", true);
				changeOnIndividualPracticeAreas();
				return false;
			}
		});
		
		return false;
	}
});



  $( "#Firmstext" ).catcomplete({
	minLength: 3,
	source: function( request, response ) {
		// delegate back to autocomplete, but extract the last term
		response( $.ui.autocomplete.filter(
		data, extractLast( request.term ) ) );
	},
	focus: function() {
		// prevent value inserted on focus
		return false;
	},
	select: function( event, ui ) {
		var terms = split( this.value );		
		var termsValue = split( this.id );
		if( ui.item.value.indexOf("AmLaw 100") !== -1){
			return false;
		}
		
		// remove the current input
		terms.pop();
		termsValue.pop();
		// add the selected item
		
		terms.push( ui.item.value );
		termsValue.push( ui.item.id );
		// add placeholder to get the comma-and-space at the end
		terms.push( "" );
		termsValue.push( "" );
		this.value = terms.join( ", " );	
		$('#selectedFirms').val($('#selectedFirms').val().replace("AmLaw 100","") + "," + ui.item.id);
		
		$('#allOtherFirmDiv').find('input[type=checkbox]').each(function()
   		{ 
			if($(this).val() == (ui.item.id) )
			{
				$(this).prop("checked", true);
				allOtherFirmChange();
				return false;
			}
		});
		
		return false;
	}
});  
  
  
  
	 $('#hide, #popup').click(function(e){
		 $("#popupPracticeArea").hide();		
		 $("#popupLocation").hide();
		 $("#popupTitles").hide();	
	     e.stopPropagation();	   
	});  

	

	$('#practiceAreaId, #popupPracticeArea').click(function(e){
		 $("#popup").hide(); 
		 $("#popupLocation").hide();
		 $("#popupTitles").hide();	
	     e.stopPropagation();   
	});
	


	$('#locationId, #popupLocation').click(function(e){
		 $("#popup").hide(); 
		 $("#popupPracticeArea").hide();
		 $("#popupTitles").hide();	
	     e.stopPropagation();   
	});

	
	$('#titleDropdown, #popupTitles').click(function(e){
		 $("#popup").hide(); 
		 $("#popupPracticeArea").hide();
		 $("#popupLocation").hide();
	     e.stopPropagation();   
	});
	$(document).click(function(){
	    $("#popup").hide(); 
	    $("#popupPracticeArea").hide();
	    $("#popupLocation").hide();		 
	    $("#popupTitles").hide();	
	});
  
  
  
	$('#btnAdd').click(function(){
		$('.filtersPage').hide()
		$('#additional').show()
	})
	$('#btnSave').click(function(){
		$('.filtersPage').show()
		$('#additional').hide()
	});
	
	$('#hide').click(
    function () {
        //show its submenu
        $("#popup").stop().slideToggle(500);    
    });   
    
    $('#titleDropdown').click(
    	    function () {
    	        //show its submenu
    	        $("#popupTitles").stop().slideToggle(500);    
    	    });
	
	$('#locationId').click(
		    function () {
		        $("#popupLocation").stop().slideToggle(500);    
	});
	
	$('#practiceAreaId').click(
		function () {
		    $("#popupPracticeArea").stop().slideToggle(500);    
	});
	
	
	$("#allLocations").change(function(){
		var checked = $(this).is(":checked");
		if(checked){  		
		 $('#allOtherLocationsDiv').find('input[type=checkbox]:checked').removeAttr('checked');	
		 $('#allOtherLocations').removeAttr('checked');	
		 $("#locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="selectAllLocations" />  Locations (0) Selected');
		}
	});
	
	$("#allOtherLocations, #allOtherLocationsDiv input[type=checkbox]").click(function(){
	
		changeOnIndividualLocations();
	});
	

	//UI Logic for Practice Area
	
	$(".allPracticeArea").change(function(){
		var checked = $(this).is(":checked");
		if(checked){  		
			 $('#allOtherPracticeArea').find('input[type=checkbox]:checked').removeAttr('checked');	
			 $('#allPracticeAreaIndividual').removeAttr('checked');	
			 $("#practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" /> PracticeAreas (0) Selected');
		}
	});
	
	$("#allPracticeAreaIndividual, #practiceAreaDiv input[type=checkbox]").click(function(){
		changeOnIndividualPracticeAreas();
	});
	
	
	//UI Logic for Titles
	
	$("input[id='allTitles']").change(function(){
		var checked = $("#allTitles").is(":checked");
		if(checked){  		
			 $('#allOtherTitlesDiv').find('input[type=checkbox]:checked').removeAttr('checked');
		}
	});
	
	$("input[class='allTitlesCheckBox']").change(function(){
		var lengthofCheckBoxes = $("input[class='allTitlesCheckBox']:checked").length;
		if(lengthofCheckBoxes>0){
			$('#allTitles').prop("checked", false);
		}
	
	});
	
	$('#allfirms').bind('click', function (event) {
		$("#Firmstext").val('<%=ALMConstants.ALL_FIRMS %>');
		$("#selectedFirms").val('<%=ALMConstants.ALL_FIRMS %>');
		$('#firm_watchlist').val('<%=ALMConstants.ALL_FIRMS %>');
		$('#individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
		$('#allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
		$('#allOtherFirm').prop("checked", false);
		$('#selectAllFirms').prop("checked", false);
		$('#firm_watchlist').prop("checked", false);
		
	});
	
	$('.rivaledgeListAMLAW_100').bind('click', function (event) {
		
		$('.rivaledgeListAMLAW_100').prop("checked", true);
		$('.rivaledgeListAMLAW_200').prop("checked", false);
		$('.rivaledgeListNLJ_250').prop("checked", false);
		$('#firm_watchlist').prop("checked", true);
		$('#individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
	 	$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0 Selected)');
	 	
	 	$("#individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/> Firms (0) Selected');
		$('#allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
		$('#firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
		$('#Firmstext').val("<%=ALMConstants.AMLAW_100 %>");
	});
	
	$('.rivaledgeListAMLAW_200').bind('click', function (event) {
		
		$('.rivaledgeListAMLAW_100').prop("checked", false);
		$('.rivaledgeListAMLAW_200').prop("checked", true);
		$('.rivaledgeListNLJ_250').prop("checked", false);
		$('#firm_watchlist').prop("checked", true);
		$('#individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
	 	$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0 Selected)');
	 	
	 	$("#individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/> Firms (0) Selected');
		$('#allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
		$('#firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
		$('#Firmstext').val("<%=ALMConstants.AMLAW_200 %>");
	});
	
	$('.rivaledgeListNLJ_250').bind('click', function (event) {
		
		$('.rivaledgeListAMLAW_100').prop("checked", false);
		$('.rivaledgeListAMLAW_200').prop("checked", false);
		$('.rivaledgeListNLJ_250').prop("checked", true);
		$('#firm_watchlist').prop("checked", true);
		$('#individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
	 	$(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0 Selected)');
	 	
	 	$("#individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/> Firms (0) Selected');
		$('#allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
		$('#firm_watchlist').val("<%=ALMConstants.RIVALEDGE_LIST %>");
		$('#Firmstext').val("<%=ALMConstants.NLJ_250 %>");

	});
	
	$("input[class='allFirmsCheckBoxWatchList']").change(function(){
		 $('.rivaledgeListAMLAW_100').prop("checked", false);
		 $('.rivaledgeListAMLAW_200').prop("checked", false);
		 $('.rivaledgeListNLJ_250').prop("checked", false);
	});
	
	 $('.allFirmsCheckBoxWatchList').click(function() {
		 var allWatchListCounter = 0;
		 $('#firm_watchlist').prop("checked", true);
		 $('#allfirms').prop("checked", false);
		 
		 $('#allOtherFirm').prop("checked", false);
		 
		 $('#allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
		 
		
		$('#individualfirmsWatchListDIV input[type=checkbox]:checked').each(function() 
		{
			allWatchListCounter++;
		});
		
		 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (' + allWatchListCounter +' Selected)');
		 $("#individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/> Firms (0 Selected)');
		 $('#firm_watchlist').val("<%=ALMConstants.WATCH_LIST %>");
		 $('.rivaledgeListAMLAW_100').prop("checked", false);
		 $('.rivaledgeListAMLAW_200').prop("checked", false);
		 $('.rivaledgeListNLJ_250').prop("checked", false);
		 
		});
	 
	 
	 $('#individualFirmsCounter').click(function() {
		 
			var checked = $("#selectAllFirms").is(":checked");
			var allFirmsListCounter = 0;
			if(checked)
			{   
				 $('#allOtherFirmDiv #allFirmsCheckBoxCounter').prop('checked', true);
				 $(".rivaledgeListAMLAW_100").prop("checked", false);
				 $(".rivaledgeListAMLAW_200").prop("checked", false);
				 $(".rivaledgeListNLJ_250").prop("checked", false);
			}
			else
			{
				$('#allOtherFirmDiv #allFirmsCheckBoxCounter').prop('checked', false);
			}
			
			 $('#allOtherFirmDiv input[type=checkbox]:checked').each(function() {
				 allFirmsListCounter++;
			 });
			 
			 if(allFirmsListCounter == 0)
				 {
					 $("#individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/> Firms ('+allFirmsListCounter+') Selected');
					 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (1) Selected)');
					 $('#allfirms').prop("checked", false);
					 $('#allOtherFirm').prop("checked", false);
					 $('#selectAllFirms').prop("checked", false);
					 
						var watchListId = $('#defaultWatchListId').val();
						
						$('#individualfirmsWatchListDIV input[type=checkbox]').each(function() {
							 if(this.value == watchListId)
								{
									$(this).prop("checked", true);
								}
						});
						 $('#firm_watchlist').prop("checked", true);
				 }
			 else
				 {
					 $("#individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/> Firms ('+allFirmsListCounter+') Selected');
					 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0) Selected)');
					 $('#allOtherFirm').prop("checked", true);
					 $('#selectAllFirms').prop("checked", true);
					 $('#individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
					 $('#firm_watchlist').prop("checked", false);
					 $('#allfirms').prop("checked", false);
				 }
		});
			
	 $('#practiceAreaCounter').click(function() {
		 
			var checked = $("#selectAllPracticeAreas").is(":checked");
			var allPracticeAreasListCounter = 0;
			
			if(checked)
			{   
				 $('#allOtherPracticeArea #practiceId').prop('checked', true);
				 $('#practiceArea1').prop('checked', false);
			}
			else
			{
				$('#allOtherPracticeArea #practiceId').prop('checked', false);
			}
			
			 $('#allOtherPracticeArea input[type=checkbox]:checked').each(function(){
				 allPracticeAreasListCounter++;
			 });
			 
			 if(allPracticeAreasListCounter == 0)
				 {
					 $("#practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" /> Practice Areas ('+allPracticeAreasListCounter+') Selected');
					 $('#allPracticeAreaIndividual').prop("checked", false);
					 $('#selectAllPracticeAreas').prop("checked", false);
					 $('.allPracticeArea').prop("checked", true);
					 $('#selectedPracticeArea').val('<%=ALMConstants.ALL_PRACTICE_AREAS %>');
				 }
			 else
				 {
					 $("#practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" /> Practice Areas  ('+allPracticeAreasListCounter+') Selected');
					 $('#allPracticeAreaIndividual').prop("checked", true);
					 $('#selectAllPracticeAreas').prop("checked", true);
				 }
		}); 
	 
	 $('#locationsCounter').click(function() {
		 
			var checked = $("#selectAllLocations").is(":checked");
			var allLocationsListCounter = 0;
			if(checked)
			{   
				 $('#allOtherLocationsDiv .allLocationsCheckBox').prop('checked', true);
			}
			else
			{
				$('#allOtherLocationsDiv .allLocationsCheckBox').prop('checked', false);
			}
			
			 $('#allOtherLocationsDiv input[type=checkbox]:checked').each(function() {
				 allLocationsListCounter++;
			 });
			 
			 if(allLocationsListCounter == 0)
				 {
					 $("#locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="selectAllLocations" /> Locations ('+allLocationsListCounter+') Selected');
					 $('#allLocations').prop("checked", true);
					 $('#allOtherLocations').prop("checked", false);
					 $('#selectAllLocations').prop("checked", false);
					 $('#selectedLocation').val('<%=ALMConstants.ALL_LOCATIONS %>');
				 }
			 else
				 {
					 $("#locationsCounter").html('<input type="checkbox" value="selectAllLocations" id="selectAllLocations" /> Locations ('+allLocationsListCounter+') Selected');
					 $('#allLocations').prop("checked", false);
					 $('#allOtherLocations').prop("checked", true);
					 $('#selectAllLocations').prop("checked", true);
				 }
		});
	 
	//Function for showing selected Location name
	 $("input[class='allLocationsCheckBox']").change(function(){
			changeOnIndividualLocations();
			applyLocations();
		});
	 
	//Function for showing selected PracticeArea name
	 $("input[id='practiceId']").change(function(){
	 	changeOnIndividualPracticeAreas();
	 	applyPracticeArea();
	 });
	
		//Function for showing selected PracticeArea name
	 $("input[id='allFirmsCheckBoxCounter']").change(function(){
		 

		 changeOnIndividualFirms();
		 
	 	 applyFirms();
	 	
	 });
		
	function changeOnIndividualPracticeAreas()
		{
			var allPracticeCounter = 0;
			 $('#allOtherPracticeArea input[type=checkbox]:checked').each(function() {
				allPracticeCounter++;
				
			    });
				
				$("#practiceAreaCounter").html('<input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" /> PracticeAreas ('+allPracticeCounter+') Selected');
				
				if(allPracticeCounter == 0)
				{
					$('#allPracticeAreaIndividual').prop("checked", false);
					$('.allPracticeArea').prop("checked", true);
					$('#selectedPracticeArea').val('<%=ALMConstants.ALL_PRACTICE_AREAS %>');
				}
			else
				{
					$('#allPracticeAreaIndividual').prop("checked", true);
					$('.allPracticeArea').prop("checked", false);
				}
		}
	
	function changeOnIndividualFirms()
	{
		
		var allFirmsCounter = 0;
		 $('#allOtherFirmDiv input[type=checkbox]:checked').each(function() {
			 allFirmsCounter++;
		    });
			
			$("#individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/> Firms ('+allFirmsCounter+') Selected');
			if(allFirmsCounter == 0)
			{
				$('#firm_watchlist').prop("checked", false);
				$('#allOtherFirm').prop("checked", false);
				$('#allfirms').prop("checked", false);
				
				$('#individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
				$('#allOtherFirmDiv input[type=checkbox]:checked').removeAttr('checked');
				

				var watchListId = $('#defaultWatchListId').val();
				
				$('#individualfirmsWatchListDIV input[type=checkbox]').each(function() {
					 if(this.value == watchListId)
						{
							$(this).prop("checked", true);
						}
				});
			}
		else
			{
				$('#allOtherFirm').prop("checked", true);
				$('#firm_watchlist').prop("checked", false);
				$('#allfirms').prop("checked", false);
				
				$('#individualfirmsWatchList input[type=checkbox]:checked').removeAttr('checked');
				$('#individualfirmsWatchListDIV input[type=checkbox]:checked').removeAttr('checked');
			}
	}

//UI Logic for allFirms
	
	$("input[id='allfirms']").change(function(){
		allFirmChange();
	});

	function allFirmChange()
	{
		var checked = $("#allfirms").is(":checked");
		if(checked)
		{		 
			 $('#firm_watchlist').prop("checked", false);
			 $('#allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
			 $('#individualfirmsWatchList').find('input[type=checkbox]:checked').removeAttr('checked');
			 $("#individualFirmsCounter").html('<input type="checkbox" value="Abrams" id="firmCounter"> Firms (0) Selected');
		}
	}
	
	$("input[id='RivalEdgeList']").change(function(){
		rivalEdgeListChange();
	});
	
	function rivalEdgeListChange()
	{
		var checked = $("#RivalEdgeList").is(":checked");
		if(checked)
		{
			$('#allfirms').prop("checked", false);
			$('#popup option:nth(0)').attr("selected", "selected");
			$('#allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
			$("#individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/>  Firms (0) Selected');
		}
	}
	
	$("#Select1").change(function(){
		individualSelectChange();
	}); 
	
	function individualSelectChange()
	{
		var count = $("#Select1 :selected").length;
		if(count > 0)
		{	
			$('#allfirms').prop("checked", false);
			$('#allOtherFirmDiv').find('input[type=checkbox]:checked').removeAttr('checked');
			$('#RivalEdgeList').prop("checked", true);
			$("#individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/>  Firms (0) Selected');
		}
	}
	
	$('#allOtherFirmDiv input[type=checkbox]').change(function(){
		allOtherFirmChange();
	});

	function allOtherFirmChange()
	{
		var allFirmsCounter = 0;
		$('#allOtherFirmDiv input[type=checkbox]:checked').each(function() 
		{
			allFirmsCounter++;
		});
		$('#popup option:selected').removeAttr("selected");
		$("#individualFirmsCounter").html('<input type="checkbox" value="selectAllFirms" id="selectAllFirms"/>  Firms ('+allFirmsCounter+') Selected');
		
		if(allFirmsCounter > 0)
		{
			 $('#allOtherFirm').prop("checked", true);
			 $('.allFirmsCheckBoxWatchList').prop("checked", false);
			 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" />  Watchlists (0) Selected');
			 
		}
		else
		{
			$('#allOtherFirm').prop("checked", false);
			$('#firm_watchlist').prop("checked", true);
			
			var watchListId = $('#defaultWatchListId').val();
			
			$('#individualfirmsWatchListDIV input[type=checkbox]').each(function() {
				 if(this.value == watchListId)
					{
						$(this).prop("checked", true);
					}
				 
				 $(".individualfirms-Watchlist").html('<input type="checkbox" value="selectWatchList" id="selectAllWatchList" />  Watchlists (1) Selected');
			});
		}
		
		 $('.rivaledgeListAMLAW_100').prop("checked", false);
		 $('.rivaledgeListAMLAW_200').prop("checked", false);
		 $('.rivaledgeListNLJ_250').prop("checked", false);
	}
	
	//DisplayColumns select ad deselect

	$("input[name=displayColumns]").click(function(){
		changeOnDisplayColumns();
	});

	$("input[name=orderBy]").click(function(){
		changeOnGroupByValues();	
	});

	$("input[name=searchResultsPerPage]").click(function(){
		changeOnResultsPerPageValues();	
	});
	
	$('#resetAll').bind('click', function (event) {
		clearAll();
	});
	
	
	//Firm clear button event: Will clear all slected values in firms popup and reset to default value
	$('#clearButton').bind('click', function (event) {
		clearFirms();
		var watchListId = $('#defaultWatchListId').val();
		
		$('#individualfirmsWatchListDIV input[type=checkbox]').each(function() {
			 if(this.value == watchListId)
				{
					$(this).prop("checked", true);
				}
		});
		applyFirms();
	});

	$('#clearPracticeArea').bind('click', function (event) {
		clearPracticeAreas();
	});
	
	$('#clearButtonTitles').bind('click', function (event) {
		clearTitles();
	});
	
	$('#clearButtonLocations').bind('click', function (event) {
		clearLocations();
	});

	
	
	$('#ApplyFirm').bind('click', function (event) {
		applyFirms();
		$("#popup").toggle();
	
	});
	
	$('#ApplyPracticeArea').bind('click', function (event) {
		applyPracticeArea();
		$("#popupPracticeArea").toggle();
	
	});
	
	$('#ApplyTitles').bind('click', function (event) {
		applyTitles();
		$("#popupTitles").toggle();
	
	});
	
	
	$('#ApplyLocations').bind('click', function (event) {		
		applyLocations();
		$("#popupLocation").toggle();	
	});

	$("#applySearch").click(function() {
		$("#goToPage").val(1);
		//search();
	});
	
	//autoselect previously selected search critria
	initializeSearchCriteria();
	//fire a search for the default search criteria
	//search();
		
});

$(document).ready(function(){
	$(".menu").hover(
		function(){$(".sub").slideToggle(400);},
		function(){$(".sub").hide();}
	);
});

(function(){
      var del = 200;
      $('.icontent').hide().prev('a').hover(function(){
        $(this).next('.icontent').stop('fx', true).slideToggle(del);
      });
    })();
    
</script>

<form:form  commandName="attorneySearchModelBean" method="post" action="${changeSearchCriteria}" id="attorneySearchModelBean">
 	<form:hidden path="goToPage"  id="goToPage" />
 	<form:hidden path="sortColumn" id="sortColumn" />
 	<form:hidden path="sortOrder"  id="sortOrder" />
<div class="breadcrumbs"><span>ATTORNEY SEARCH</span></div>
<%--  <form name="searchForm" method="post" action="" id="searchForm"> --%>
	<div id="dropdown">
		<!-- dropdown_title_start -->
		<ul id="droplist">

			<li><label>Firm(s)</label>
				<div class="srchBox" style="width:182px">
					
					<%-- <input type="text" name="Firmstext" id="Firmstext" value="<%=ALMConstants.AMLAW_100 %>"
						style="text-overflow: ellipsis;width:139px" class="input" /> --%>
					
					     <c:choose>
			              <c:when test="${empty allWatchListsDefaultList or allWatchListsDefaultList == ''}">
			          			<input type="text" name="Firmstext" id="Firmstext"  value="<%=ALMConstants.AMLAW_100 %>" style="text-overflow:ellipsis;" class="input" />
			          	  </c:when>
			          	  <c:otherwise>
			          			<input type="text" name="Firmstext" id="Firmstext"  value="${allWatchListsDefaultList.groupName}" style="text-overflow:ellipsis;" class="input" />
			          	  </c:otherwise>
			          	</c:choose>
 
						  <!--  <input type="text" name="Firmstext" id="Firmstext"  style="text-overflow:ellipsis;" class="input" /> -->
						   <input type="button" name="search" value="" class="srchBack" id="testDiv" />
						   <div class="clear">&nbsp;</div>
				</div> <input type="button" name="search" id="hide" value="" class="typeSel" />


				<div class="rel">
					<div id="popup" class="firmPage">
						<p><form:radiobutton path="firmType" id="allfirms" value="<%=ALMConstants.ALL_FIRMS %>" />&nbsp;<%=ALMConstants.ALL_FIRMS %></p>
						<%-- <p><form:radiobutton path="firmType" id="RivalEdgeList"   value="<%=ALMConstants.RIVALEDGE_LIST %>" />&nbsp;<%=ALMConstants.RIVALEDGE_LIST %></p> --%>
						<p> <form:radiobutton path="firmType" id="firm_watchlist" value=""/>Select Watchlist/ RivalEdge List</p>
						<%-- <form:select path="firmList" id="Select1" multiple="false" size="4" cssStyle="width:220px">
							<form:options items="${allRankingList}" />
						</form:select> --%>
						
						<div class="individualfirms-Watchlist" >
                			<input type="checkbox" value="selectWatchList" id="selectAllWatchList" /> Watchlists (0 Selected)<br />
               			</div>
               			<div  class="individualfirmsWatchList" id="individualfirmsWatchList"> 
						<form:checkbox  class="rivaledgeListAMLAW_100" path="firmList"  value="<%=ALMConstants.AMLAW_100 %>" /><%=ALMConstants.AMLAW_100 %><br>
						<form:checkbox  class="rivaledgeListAMLAW_200" path="firmList"  value="<%=ALMConstants.AMLAW_200 %>" /><%=ALMConstants.AMLAW_200 %><br>
						<form:checkbox  class="rivaledgeListNLJ_250" path="firmList"  value="<%=ALMConstants.NLJ_250 %>" /><%=ALMConstants.NLJ_250 %><br>
  
 				 		<div  class="individualfirmsWatchListDIV" id="individualfirmsWatchListDIV"> 
			                <c:forEach var="watchlist"  items="${allWatchLists}">     
		    	  				 <form:checkbox  class="allFirmsCheckBoxWatchList" path="firmListWatchList"  value="${watchlist.groupId}" labelAttr="${watchlist.groupName}" />${watchlist.groupName}<br>
		  				 	</c:forEach>
						</div>
						</div>
						<br>
						
						<p><form:radiobutton path="firmType" value="<%=ALMConstants.INDIVIDUAL_LIST %>" id="allOtherFirm"/>&nbsp;<%=ALMConstants.INDIVIDUAL_LIST %></p>
						
						<div class="individualfirms-first" id="individualFirmsCounter"> 
	               			 <input type="checkbox" value="selectAllFirms" id="selectAllFirms"/>Firms (0 Selected)<br />
              			</div>
						
						<div style="background-color: #FFFFFF;"	id="allOtherFirmDiv">
							<c:forEach var="firm" items="${allFirms}">
								<form:checkbox  class="allFirmsCheckBox" id="allFirmsCheckBoxCounter" path="firmList" value="${firm.companyId}" labelAttr="${firm.company}"/>${firm.company}<br>
							</c:forEach>
						</div>
 						<div class="popupsubmit">
 						 <input type="button" class="buttonOne" value="Apply" id="ApplyFirm">
							<input type="button" class="buttonTwo" value="Clear All" id="clearButton">
						</div>

					</div>
				</div>
				 <c:if test="${not empty allWatchListsDefaultList}">
					<input type="hidden" name="defaultWatchListId" id="defaultWatchListId" value="${allWatchListsDefaultList.groupId}" />
				 </c:if>
			</li>
				
			  <li>
        		<label>Location(s)</label>
					  <div class="srchBox">
						<%-- <input type="text"  value="<%=ALMConstants.ALL_LOCATIONS %>" class="input" name="selectedLocation" id="selectedLocation"  /> --%>
						<input type="text" class="input" name="selectedLocation" id="selectedLocation" style="text-overflow:ellipsis;"  /> 
						<input type="button" name="search" value="" class="srchBack" />
						<div class="clear">&nbsp;</div>
					  </div>
					 <input type="button" name="search" value=""  id="locationId" class="typeSel" />
					<div class="rel">	 
						<div  id="popupLocation" class="firmPage">
							<p><form:radiobutton path="locations"  id="allLocations" value="<%=ALMConstants.ALL_LOCATIONS%>" /> <%=ALMConstants.ALL_LOCATIONS %></p>	
							<p><input type="radio"  id="allOtherLocations" value="Select Individual Locations" /> Select Individual Locations</p>
							<div class="individualfirms-first" id="locationsCounter"> 
								<input type="checkbox" value="selectAllLocations" id="selectAllLocations" />  Locations (0) Selected<br> </div>
							<div style="background-color:#FFFFFF;" id="allOtherLocationsDiv">		
							<c:forEach items="${allOtherLocations}" var="loc">
								<form:checkbox path="locations" class="allLocationsCheckBox" value="${loc}"/>${loc}<br>
							</c:forEach>	   
							</div>
							<div class="popupsubmit">				
							<input type="button" class="buttonOne" value="Apply" id="ApplyLocations">
							<input type="button" class="buttonTwo" value="Clear All" id="clearButtonLocations">
							</div>
						</div>
					</div>
 				</li>
		
		   <li>
					  <label>Practice Area(s)</label>
						  <div class="srchBox">
							<input type="text"  class="input" name="selectedPracticeArea" id="selectedPracticeArea" style="text-overflow:ellipsis;" />
							<input type="button" name="search" value="" class="srchBack" />
							<div class="clear">&nbsp;</div>
						  </div>
						 <input type="button" name="search" value=""  id="practiceAreaId" class="typeSel" />
						<div class="rel">
							  <div  id="popupPracticeArea" class="firmPage">					
									
								<p><form:radiobutton path="practiceArea" class="allPracticeArea" value="<%=ALMConstants.ALL_PRACTICE_AREAS %>" /><%=ALMConstants.ALL_PRACTICE_AREAS%></p>
						   		<p> <input  type="radio"  name="allPracticeArea"  id="allPracticeAreaIndividual" value="Select Individual Practice Areas"> Select Individual Practice Areas</p>
									<div class="individualfirms-first" id="practiceAreaCounter"> <input type="checkbox" value="selectAllPracticeAreas" id="selectAllPracticeAreas" /> Practice Areas (0 Selected)<br> </div>
								  <div style="background-color:#FFFFFF;" id="practiceAreaDiv">										 
										 <div style="background-color:#FFFFFF;" id="allOtherPracticeArea">
										    <c:forEach var="practice" items="${allPracticeArea}">								
												<form:checkbox path="practiceArea" id="practiceId" value="${practice.practiceArea}"/> ${practice.practiceArea}<br>
											</c:forEach>
										 </div>	
									</div>
									<br>
						<%-- <p>
							<form:checkbox path="firstListedPractice" id="firstListedPractice" value="<%=ALMConstants.FIRST_LISTED_PRACTICE %>"/>Search first listed practice only
						</p> --%>

						<div class="clear"></div>
								<div class="popupsubmit">
								 <input type="button" class="buttonOne" value="Apply" id="ApplyPracticeArea" />
								 <input type="button" class="buttonTwo" value="Clear All" id="clearPracticeArea" />
								</div>	
								
									
						</div>
						</div>
				</li>
				
				
				<li><label>Title(s)</label>
				<div class="srchBox">
					<input type="text" name="search" value="All" class="input" id="selectedTitles" style="text-overflow: ellipsis;width:110px" /> 
					<input type="button" name="search" value="" class="srchBack" />
					<div class="clear">&nbsp;</div>
				</div> 
				<input type="button" name="search" value="" class="typeSel"	id="titleDropdown" />

				<div class="rel">
					<div id="popupTitles" class="firmPage" style="width:175px !important">
					<div id="titleSelectDiv">
						<div style="background-color: #FFFFFF;">
						<form:checkbox path="title"  id="allTitles" value="<%=ALMConstants.ALL%>"/>&nbsp;<%=ALMConstants.ALL%>  <br>
							
							<div id="allOtherTitlesDiv">
								<form:checkbox path="title" class="allTitlesCheckBox" 
								    value="<%=ALMConstants.PARTNERS%>"/>&nbsp;<%=ALMConstants.PARTNERS%><br>
								<form:checkbox path="title" class="allTitlesCheckBox"
									value="<%=ALMConstants.ASSOCIATE%>"/><%=ALMConstants.ASSOCIATE%><br>
								<form:checkbox path="title" class="allTitlesCheckBox"
									value="<%=ALMConstants.OTHER_COUNSEL%>"/><%=ALMConstants.OTHER_COUNSEL%><br>
								<form:checkbox path="title" class="allTitlesCheckBox"
									value="<%=ALMConstants.ADMINISTRATIVE%>"/><%=ALMConstants.ADMINISTRATIVE%><br>
								<form:checkbox path="title" class="allTitlesCheckBox"
									value="<%=ALMConstants.OTHER%>"/><%=ALMConstants.OTHER%><br>
							</div>
						</div>
						</div>
						<div class="popupsubmit">
						<input type="button" class="buttonOne" value="Apply" id="ApplyTitles">
						<input type="button" class="buttonTwo" value="Clear All" id="clearButtonTitles">
						
						</div>
					</div>
				</div></li>
				
				<li><label>&nbsp;</label><input type="submit" value="Apply"	class="buttonOne" id="applySearch"/></li>
				<li><label>&nbsp;</label><input type="button" value="Reset All"	class="buttonTwo" id="resetAll" /></li>
			<li><input type="hidden" name="selectedFirms" id="selectedFirms" title="" customAttr="" /></li>

		</ul>
		<div class="clear">&nbsp;</div>
	</div>
	<div class="filtersPage">
		<div class="barSec">
			<a href="#" class="dwnBx" id="btnAdd">Additional Filters</a>
		</div>
	</div>
	<div class="filtersPage" id="additional" style="display: none">
		<div id="dropdown">
			<!-- dropdown_title_start -->
			<ul id="droplist">
				<li>
					<div class="col2-1" style="width:212px">
						<label>Law School Graduation</label>
						<span class="flLeft marlt1 mart1" style="margin-top: 5px; color:#777777">From</span>
						<div class="flLeft selectBox" style="width: 74px">
							<form:select path="fromDate" id="graduationFrom">
								<form:options items="${fromYear}" />
							</form:select>
						</div>
						<span class="flLeft marlt1 mart1"  style="margin-top: 5px; color:#777777">To</span>
						<div class="flLeft selectBox" style="width: 74px">
							<form:select path="toDate" id="graduationTo">
								<form:options items="${toYear}" />
							</form:select>
						</div>
					</div>
				</li>
				<li><label>Law School</label>
					<div class="srchBox">
						 <form:input path="lawSchools" type="text" name="search" class="input"	id="selectedLawSchool" />
							<input type="button" name="search" value="" class="srchBack" />
						<div class="clear">&nbsp;</div>
					</div> 
					<!-- <input type="button" name="search" value="" class="typeSel" /> -->
					</li>

				<li><label>Admissions</label>
					<div class="srchBox">
						<form:input path="admissions" type="text" name="search" value="" class="input"
							id="selectedAdmissions" />
							  <input type="button"	name="search" value="" class="srchBack" /> 
						<div class="clear">&nbsp;</div>
					</div>
					<!--  <input type="button" name="search" value="" class="typeSel" /> -->
					 </li>
				<li><label>Name</label>
					<div class="srchBox" style="width: 175px">
						<form:input path="name" type="text" name="search" class="input"
							style="width: 140px" id="name" /> <input type="button"
							name="search" value="" class="srchBack" />
						<div class="clear">&nbsp;</div>
					</div></li>
					
					<li>
					<div class="col2-1">
						<label>Keywords in Bio</label>
						<div class="srchBox">
							<form:input path="keywords" type="text" name="search" class="input" id="keywordsInBio"
								 /> <input type="button" name="search" value=""
								class="srchBack" />
							<div class="clear">&nbsp;</div>
						</div>
					</div>
				</li>
			</ul>
			<div class="clear">&nbsp;</div>
		</div>
		<div class="barSec">
			<a href="#" class="upBx" id="btnSave">Hide Filter</a>
		</div>
		
	</div>
	
	<div class="attorneysearch" id="viewSettingPopup" style="display:none">
              <div class="popHeader"><span id="viewSettingCloseSpan"><a style="margin-top:2px" class="btn icon closewhite closeOne flRight" href="javascript:void(0);"  id="viewSettingClose"  onclick="cancelViewSettings();">Close</a> </span>SETTINGS: ATTORNEY SEARCH
                <div class="clear">&nbsp;</div>
              </div>
              <div class="popMiddle">
                <div class="col3-4">
                  <h6>Column Display:</h6>
                  <div class="marlt3">
					<ul class="reset list5">
							<c:forEach var="dis_column" items="${allDisplayColumns}">								
								<li><form:checkbox path="displayColumns" value="${dis_column.value}" /> ${dis_column.displayValue}</li>
							</c:forEach>
							<!-- to hold the ; separated values to send to controller -->
							<input type="hidden" id="displayColumns_hidden"/>
													
					</ul>
				</div>
                </div>
                <div class="col3-4">
                  <h6>Group By:</h6>
				<div class="marlt3">
					<div id="groupByDiv">
						<ul class="reset list5">
						
						<li><form:radiobutton path="orderBy" value="-1"/>&nbsp;None </li> 
						<li><form:radiobutton path="orderBy" value="5"/>&nbsp;Firm </li>
						<li><form:radiobutton path="orderBy" value="7"/>&nbsp;Title</li> 
						<li><form:radiobutton path="orderBy" value="8"/>&nbsp;Location </li>
						<li><form:radiobutton path="orderBy" value="9"/>&nbsp;Practice </li>
						</ul>
					</div>
				</div>
			</div>
                <div class="col3-4">
                  <h6>Display No. of Results</h6>
				<div class="marlt3">
					<ul class="reset list5">
					<li><form:radiobutton path="searchResultsPerPage"  value="25"  />&nbsp;25</li>
					<li><form:radiobutton path="searchResultsPerPage"  value="50"  />&nbsp;50</li>
					<li><form:radiobutton path="searchResultsPerPage"  value="100" />&nbsp;100</li>
					<li><form:radiobutton path="searchResultsPerPage"  value="500" />&nbsp;500</li>
					</ul>
				</div>
			</div>
                <div class="clear">&nbsp;</div>
              </div>
              <div class="popFooter">
                <div class="flLeft">
                  <input type="button" class="buttonTwo" value="Reset All" id="resetAllViewSetting" onclick="resetViewSettings();">
                </div>
                <div class="flRight">
                	<input type="hidden"  value="" id="selectedColumns">
                	<input type="hidden"  value="" id="selectedGroupBy">
                  	<input type="button" class="buttonOne" value="Apply" id="ApplySetting" onclick="applyViewSettings();">
                  	<input type="button" class="buttonTwo" value="Cancel" id="CancelSetting" onclick="cancelViewSettings();">
                </div>
                <div class="clear">&nbsp;</div>
              </div>
	     </div>
	
</form:form>

<script type="text/javascript">
	// Omniture SiteCatalyst - START
	s.prop22 = "premium";
	s.pageName="rer:attorney-search";
	s.channel="rer:attorney-search";
	s.server="rer";
	s.prop1="attorney-search"
	s.prop2="attorney-search";
	s.prop3="attorney-search";
	s.prop4="attorney-search";
	s.prop21=s.eVar21='current.user@foo.bar';	
	s.events="event2";
	s.events="event27";
	s.t();
	// Omniture SiteCatalyst - END
</script>
