$(document).ready(function(){
	
	// Tabs Content
	$('.htabs').each(function(){
		var $active, $content, $links = $(this).find('a');
		$active = $links.first().addClass('active');
		$content = $($active.attr('href'));
		$links.not(':first').each(function () {
			$($(this).attr('href')).hide();
		});
		
		$(this).on('click', 'a', function(e){
			$active.removeClass('active');
			$content.hide();
			$active = $(this);
			$content = $($(this).attr('href'));
			$active.addClass('active');
			$content.show();
			e.preventDefault();
		});
	});			
	
	$('.bxslider').bxSlider({
	 minSlides: 1,
	  maxSlides: 1,
	  infiniteLoop: false,
	  hideControlOnEnd: true
	});
	
	// Accordion
	$('.rinnermenu1').show();
		$('.rinnermenu').hide();
		$(".rsubmenu1").addClass("ractive");
		$('.rsubmenu1').click(function() {
		$('.rinnermenu').hide();
		$(".rsubmenu").removeClass("ractive");
		$('.rsubmenu1').removeClass('ractive');
	 	$('.rinnermenu1').slideUp('normal');
		if($(this).next().is(':hidden') == true) {
			$(this).addClass('ractive');
			$(this).next().slideDown('normal');
		 } 
	 });
	
	$('.rinnermenu').hide();
		$('.rsubmenu').click(function() {
		$(".rsubmenu1").removeClass("ractive");
		$('.rinnermenu1').hide();
		$('.rsubmenu').removeClass('ractive');
	 	$('.rinnermenu').slideUp('normal');
		if($(this).next().is(':hidden') == true) {
			$(this).addClass('ractive');
			$(this).next().slideDown('normal');
		 } 
	 });
	
	 //Teacher popup
	$('.studentname').click(function (){
		$('.studentpopup').toggle();
	});
	// write testmonial Search
	$('.testmonialpostsearchlistbox').hide();
	$('.testmonialdiv').click(function() {
		$('.testmonialdiv').addClass('active');
		$('.testmonialpostsearchlistbox').slideUp(500);
		if($('.testmonialpostsearchlistbox').is(':hidden') == true) {
			$(this).addClass('active');
			$('.testmonialpostsearchlistbox').slideDown(500);
		 } 
		 else
		 {
			 $(this).removeClass('active');
			
		 }
	 });
	 
	 // faqs Search
	$('.faqssearchlistbox').hide();
	$('.testmonialdiv').click(function() {
		$('.testmonialdiv').addClass('active');
		$('.faqssearchlistbox').slideUp(500);
		if($('.faqssearchlistbox').is(':hidden') == true) {
			$(this).addClass('active');
			$('.faqssearchlistbox').slideDown(500);
		 } 
		 else
		 {
			 $(this).removeClass('active');
			
		 }
	 });
	 $('.testmonialpostsearchlistbox').hide();
	$('.testmonialdivcomnt').click(function() {
		$('.testmonialdivcomnt').addClass('active');
		$('.testmonialpostsearchlistbox').slideUp(500);
		if($('.testmonialpostsearchlistbox').is(':hidden') == true) {
			$(this).addClass('active');
			$('.testmonialpostsearchlistbox').slideDown(500);
		 } 
		 else
		 {
			 $(this).removeClass('active');
			
		 }
	 });
	 
	  // Search Box
	var inputWdith = '220px';
	var inputWdithReturn = '100px';
	$('.shopsearchinput').focus(function(){
		$(this).val(function() {
		$(this).val('');
		$(this).animate({width: inputWdith}, 400 )
	});
	$('.shopsearchinput').blur(function(){
		$(this).val('Search');
		$(this).animate({width: inputWdithReturn},400 )
		});
	});
	
	////faqs
//		$('.questnposted').click(function() {
//		$('.questnposted').removeClass('ractive');
//	 	$('.faq-answr').slideUp('normal');
//		if($(this).next().is(':hidden') == true) {
//			$(this).addClass('ractive');
//			$(this).next().slideDown('normal');
//		 } 
//	 });
//	$('.faq-answr').hide();  
//	
	//farum accordian
		$('.farumpostsectioninner').click(function() {
		$('.foruminnerpostnew').removeClass('pactive');
	 	$('.foruminnerpostnew').slideUp('normal');
		if($(this).next().is(':hidden') == true) {
			$(this).addClass('pactive');
			$(this).next().slideDown('normal');
		 } 
		 else
		 {
			 $(this).removeClass('pactive');
			
		 }
	 });
	$('.foruminnerpostnew').hide();  
	//Callus
		$('.phnpart').toggle(
			function(){ $('.callus_start').animate({'right' :"0px"}, 458);},
			function(){ $('.callus_start').animate({'right' :"-200px"}, 458);}
		);	
		////Callus
//		$('.phnpart').toggle(
//			function(){ $('.callusdiv').animate({'right' :"0px"}, 458);},
//			function(){ $('.callusdiv').animate({'right' :"-200px"}, 458);}
//		);
	//Infografic hover
		$(".startinfo").hide();
		$(".start").hover( 
			function(){  $(".startinfo").show();},
			function(){$(".startinfo").hide();}
		);
		$(".academicskills_info").hide();
		$(".academicskills").hover( 
			function(){  $(".academicskills_info").show();},
			function(){$(".academicskills_info").hide();}
		);
		$(".vedioindex_info").hide();
		$(".vedio_index").hover( 
			function(){  $(".vedioindex_info").show();},
			function(){$(".vedioindex_info").hide();}
		);
		$(".dictionaryindex_info").hide();
		$(".dictionary_index").hover( 
			function(){  $(".dictionaryindex_info").show();},
			function(){$(".dictionaryindex_info").hide();}
		);
		$(".presentatinindex_info").hide();
		$(".presentatin_index").hover( 
			function(){  $(".presentatinindex_info").show();},
			function(){$(".presentatinindex_info").hide();}
		);
		$(".lessionindex_info").hide();
		$(".lession_index").hover( 
			function(){  $(".lessionindex_info").show();},
			function(){$(".lessionindex_info").hide();}
		);
		$(".pointsindex_info").hide();
		$(".points_index").hover( 
			function(){  $(".pointsindex_info").show();},
			function(){$(".pointsindex_info").hide();}
		);
		$(".conceptmapindex_info").hide();
		$(".conceptmap_index").hover( 
			function(){  $(".conceptmapindex_info").show();},
			function(){$(".conceptmapindex_info").hide();}
		);
		$(".worksheetindex_info").hide();
		$(".worksheet_index").hover( 
			function(){  $(".worksheetindex_info").show();},
			function(){$(".worksheetindex_info").hide();}
		);
		$(".inlinetestindex_info").hide();
		$(".inlinetest_index").hover( 
			function(){  $(".inlinetestindex_info").show();},
			function(){$(".inlinetestindex_info").hide();}
		);
		$(".reportindex_info").hide();
		$(".report_index").hover( 
			function(){  $(".reportindex_info").show();},
			function(){$(".reportindex_info").hide();}
		);
		$(".farumindex_info").hide();
		$(".farum_index").hover( 
			function(){  $(".farumindex_info").show();},
			function(){$(".farumindex_info").hide();}
		);
		$(".master_info").hide();
		$(".master").hover( 
			function(){  $(".master_info").show();},
			function(){$(".master_info").hide();}
		);
	
	//Rigister
		$('.registerclickup').click(function() {
		$('.registerinner').toggle();
		$('.logininner').hide();
		});
		
	//Login
		$('.loginclicktop').click(function() {
		$('.logininner').toggle();
		$('.registerinner').hide();
		});

});

