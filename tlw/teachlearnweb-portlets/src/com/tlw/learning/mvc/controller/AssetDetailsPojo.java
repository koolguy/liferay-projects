package com.tlw.learning.mvc.controller;

import java.io.Serializable;

public class AssetDetailsPojo implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
    /** The city. */
    private String title;
    
    /** The city. */
    private long fileEntryId;
    
    /** The city. */
    private long groupId;
    
    /** The city. */
    private long companyId;
    
    /** The city. */
    private long folderId;
    
    /** The city. */
    private String extension;
    
    /** The city. */
    private String mimeType;
    
    /** The city. */
    private String description;
    
    /** The city. */
    private long entryId;
    
    /** The city. */
    private long userId;
    
    /** The city. */
    private long classPK;
    
    /** The city. */
    private long height;
    
    /** The city. */
    private long width;

    /** The city. */
    private long size;

    /** The city. */
    private String assetType;
    
    
	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public long getFileEntryId() {
		return fileEntryId;
	}

	public void setFileEntryId(long fileEntryId) {
		this.fileEntryId = fileEntryId;
	}

	public long getGroupId() {
		return groupId;
	}

	public void setGroupId(long groupId) {
		this.groupId = groupId;
	}

	public long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(long companyId) {
		this.companyId = companyId;
	}

	public long getFolderId() {
		return folderId;
	}

	public void setFolderId(long folderId) {
		this.folderId = folderId;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getEntryId() {
		return entryId;
	}

	public void setEntryId(long entryId) {
		this.entryId = entryId;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public long getClassPK() {
		return classPK;
	}

	public void setClassPK(long classPK) {
		this.classPK = classPK;
	}

	public long getHeight() {
		return height;
	}

	public void setHeight(long height) {
		this.height = height;
	}

	public long getWidth() {
		return width;
	}

	public void setWidth(long width) {
		this.width = width;
	}
    
	public String getAssetType() {
		return assetType;
	}

	public void setAssetType(String assetType) {
		this.assetType = assetType;
	}
    
    
}
