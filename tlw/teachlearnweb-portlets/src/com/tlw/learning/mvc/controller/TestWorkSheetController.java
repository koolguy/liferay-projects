package com.tlw.learning.mvc.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletRequest;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.theme.ThemeDisplay;
import com.teachlearnweb.dao.entity.PLPAnswer;
import com.teachlearnweb.dao.entity.PLPQuestion;
import com.teachlearnweb.dao.entity.PLPStudentWrokSheet;
import com.teachlearnweb.dao.entity.PLPTopicMaster;
import com.tlw.assessment.vo.AssessmentConstants;
import com.tlw.assessment.vo.WSQuestions;
import com.tlw.controller.AbstractBaseController;
import com.tlw.reports.mvc.vo.WorkSheetReport;
import com.tlw.util.WebUtil;


@Controller("workSheetController")
@Scope("session")
@RequestMapping(value = "VIEW")
public class TestWorkSheetController extends AbstractBaseController{


    /** The logger. */
    private static final Logger logger = Logger.getLogger(TestWorkSheetController.class);
    private WSQuestions wsAssessment;
    
    public TestWorkSheetController(){
    	wsAssessment = new WSQuestions();
    }

    private Map<String, Object> getModel() {
    	Map<String, Object> model = new HashMap<String, Object>();
    	model.put("timeRemaining", wsAssessment.getTimeRemaining());
    	model.put("currentQuestionNum", wsAssessment.getCurrentQuestionNum());
    	model.put("questionCount", this.getQuestionCount());
    	//model.put("currentQuestion", this.getCurrentQuestion());
    	model.put("currentQuestion", wsAssessment.getCurrentQuestion());
    	//model.put("options", this.getOptions());
    	model.put("options", wsAssessment.getPlpAnswersList());
    	model.put("answeredCount", wsAssessment.getAnsweredCount());
    	model.put("unAnsweredCount", this.getQuestionCount() - wsAssessment.getAnsweredCount());
    	model.put("assessmentRunning", Boolean.TRUE);
    	model.put("questions", wsAssessment.getQuestions());
    	model.put("correctAnswerId", wsAssessment.getCorrectAnswerId());
    	model.put("userSelectedAnswerId", wsAssessment.getUserSelectedAnswerId());
    	model.put("worksheetId", wsAssessment.getWorkSheetId());
    	model.put("userId", wsAssessment.getUserId());
    	model.put("subjectName", wsAssessment.getSubjectName());
    	model.put("subTopicName", wsAssessment.getSubTopicName());
    	model.put("topicName", wsAssessment.getTopicName());
    	model.put("workSheetId", wsAssessment.getWorkSheetId());
    	return model;
    }
    @ResourceMapping("startWSAssessment")
    public ModelAndView startAssessment(@RequestParam Integer workSheetId, ResourceRequest request, ResourceResponse response) {
    	logger.debug("workSheetId in TestWorkSheetController---"+workSheetId);
    	logger.debug("START");
    	List<PLPQuestion> plpQuestionsList = null;
    	PLPStudentWrokSheet workSheet = null; 
    	ThemeDisplay themeDisplay = WebUtil.getThemeDisplay(request);
        if(workSheetId != null){
        	try {
        		wsAssessment.setWorkSheetId(workSheetId);
        		workSheet = serviceLocator.getPlpStudentWorkSheetService().getPLPStudentWorkSheetByWorkSheetId(workSheetId, themeDisplay.getUserId());
			} catch (HibernateException e) {
				logger.error("Hibernate Exception Caught while fetching worksheet by worksheetid in StartAssessment method in TetsWorkSheetController..",e);
				e.printStackTrace();
			}
        }
    	 
        PLPTopicMaster parentTopicMaster = null;
        PLPTopicMaster subTopicMaster = null;
        if(workSheet != null){
			wsAssessment.setSubjectName(workSheet.getSubjectMaster().getSubjectName());
			
			try {
				parentTopicMaster = serviceLocator.getTopicMasterService().getTopicMasterByTopicId(workSheet.getTopicId(),
						workSheet.getClassMaster().getClassId(), workSheet.getSubjectMaster().getSubjectId());
				subTopicMaster = serviceLocator.getTopicMasterService().getTopicMasterByTopicId(workSheet.getSubtopicId(), 
						workSheet.getClassMaster().getClassId(), workSheet.getSubjectMaster().getSubjectId());
			} catch (HibernateException e) {
				logger.error("Hibernate Exception Caught While fetching topic master", e);
				e.printStackTrace();
			}
			if(subTopicMaster != null && subTopicMaster.getTopicName() != null) {
				wsAssessment.setSubTopicName(subTopicMaster.getTopicName());
			}
			if(parentTopicMaster != null && parentTopicMaster.getTopicName() != null) {
				wsAssessment.setTopicName(parentTopicMaster.getTopicName());
			}
			wsAssessment.setUserId(workSheet.getUserId());
		}
        if(workSheet != null && workSheet.getQuestionIdList() != null){
        	plpQuestionsList = getPLPQuestionList(request, workSheet.getQuestionIdList());
        }
    		
    	if (plpQuestionsList != null && !plpQuestionsList.isEmpty()) {
    		logger.debug("plpQuestionsList.size()----------------" + plpQuestionsList.size());
    		wsAssessment.setQuestions(plpQuestionsList);
    		wsAssessment.setCurrentQuestionNum(0);
    		wsAssessment.setTimeRemaining(plpQuestionsList.size() * AssessmentConstants.MINUTE_PER_QUESTION);
    		Map<Integer, List<PLPAnswer>> answersByQuestionIds = new HashMap<Integer, List<PLPAnswer>>();
    		for (PLPQuestion plpQuestion : plpQuestionsList) {
    			List<PLPAnswer> answers = serviceLocator.getPlpAnswerService().getAnswerByQuestionId(plpQuestion.getQuestionId());
    			answersByQuestionIds.put(plpQuestion.getQuestionId(), answers);
			}
    		wsAssessment.setOptions(answersByQuestionIds);
			wsAssessment.setPlpAnswersList(wsAssessment.getOptions().get(plpQuestionsList.get(0).getQuestionId()));
			logger.debug("wsAssessment.getPlpAnswersList().size()----------------" + wsAssessment.getPlpAnswersList().size());
//			List<PLPAnswer> answer = serviceLocator.getPlpAnswerService().getAnswerByQuestionId(plpQuestionsList.get(0).getQuestionId());
			PLPQuestion question = plpQuestionsList.get(0);
			wsAssessment.setCurrentQuestion(question);
			Integer correctAnswerId = getCorrectAnswerId(question);
			wsAssessment.setCorrectAnswerId(correctAnswerId);
			Integer userSelectedAnswerId = getUserAnswerIdForQuestion();
    		logger.debug("userSelectedAnswerId------------------------------"+userSelectedAnswerId);
    		wsAssessment.setUserSelectedAnswerId(userSelectedAnswerId);
	    	if (logger.isDebugEnabled()) {
	    		logger.debug("timeRemaining has been set to initial value>>" + wsAssessment.getTimeRemaining());
	    	}
    	}
		    wsAssessment.setAnsweredCount(0);
    	
    	logger.debug("END");
        return new ModelAndView("../learning/showWSQuestion", getModel());
    }
    
    @ResourceMapping("fetchWSQuestion")
    public ModelAndView fetchQuestion(@RequestParam Integer questionNum, @RequestParam Integer workSheetId, ResourceRequest request, ResourceResponse response) {
    	logger.debug("START");
    	logger.debug("questionNum: " + questionNum);
//    	PLPStudentWrokSheet workSheet = null;
//    	List<PLPQuestion> plpQuestionsList = null;
//    	ThemeDisplay themeDisplay = WebUtil.getThemeDisplay(request);
    	logger.debug("workSheetId from fetchwsquestion----"+workSheetId);
//    	if(workSheetId != null){
//        	try {
//        		wsAssessment.setWorkSheetId(workSheetId);
//        		workSheet = serviceLocator.getPlpStudentWorkSheetService().getPLPStudentWorkSheetByWorkSheetId(workSheetId, themeDisplay.getUserId());
//			} catch (HibernateException e) {
//				logger.error("Hibernate Exception Caught while fetching worksheet by worksheetid in StartAssessment method in TetsWorkSheetController..",e);
//				e.printStackTrace();
//			}
//        }
    	
//    	PLPTopicMaster parentTopicMaster = null;
//        PLPTopicMaster subTopicMaster = null;
//    	if(workSheet != null){
//    		try {
//				parentTopicMaster = serviceLocator.getTopicMasterService().getTopicMasterByTopicId(workSheet.getTopicId());
//				subTopicMaster = serviceLocator.getTopicMasterService().getTopicMasterByTopicId(workSheet.getSubtopicId());
//			} catch (HibernateException e) {
//				logger.error("Hibernate Exception Caught While fetching topic master", e);
//				e.printStackTrace();
//			}
//    		if(workSheet.getSubjectMaster() != null && workSheet.getSubjectMaster().getSubjectName() != null) {
//    			wsAssessment.setSubjectName(workSheet.getSubjectMaster().getSubjectName());
//    		}
//    		if(subTopicMaster != null && subTopicMaster.getTopicName() != null) {
//    			wsAssessment.setSubTopicName(subTopicMaster.getTopicName());
//    		}
//			if(parentTopicMaster != null && parentTopicMaster.getTopicName() != null) {
//				wsAssessment.setTopicName(parentTopicMaster.getTopicName());
//			}
//			wsAssessment.setUserId(workSheet.getUserId());
//		}
//    	
//    	if(workSheet != null && workSheet.getQuestionIdList() != null){
//    		 plpQuestionsList = wsAssessment.getPlpQuestionsList();
//    	}
//    	
//    	if(plpQuestionsList != null && !plpQuestionsList.isEmpty() && plpQuestionsList.size() > 0) {
//			wsAssessment.setQuestions(plpQuestionsList);
//		}
    	if(questionNum > -1) {
    		wsAssessment.setCurrentQuestionNum(questionNum);
    		wsAssessment.setCurrentQuestion(wsAssessment.getQuestions().get(questionNum));
    		wsAssessment.setPlpAnswersList(wsAssessment.getOptions().get(wsAssessment.getCurrentQuestion().getQuestionId()));
    		Integer userSelectedAnswerId = getUserAnswerIdForQuestion();
    		logger.debug("userSelectedAnswerId------------------------------"+userSelectedAnswerId);
    		wsAssessment.setUserSelectedAnswerId(userSelectedAnswerId);

    	/*	List<PLPQuestion> question = serviceLocator.getPlpQuestionService().getQuestionById(wsAssessment.getCurrentQuestionNum());
    		if(question != null) {
	    		Integer userSelectedAnswerId = getUserAnswerIdForQuestion(wsAssessment.getCurrentQuestionNum(), question);
	    		System.out.println("userSelectedAnswerId------------------------------"+userSelectedAnswerId);
	    		wsAssessment.setUserSelectedAnswerId(userSelectedAnswerId);
	    		wsAssessment.setCurrentQuestion(question.get(0));
    		}
    		*/
    	}
//    	List<PLPAnswer> answer = serviceLocator.getPlpAnswerService().getAnswerByQuestionId(wsAssessment.getCurrentQuestion().getQuestionId());
//    	if(answer != null && answer.get(0) != null && answer.get(0).getAnswerId() != null) {
//    		wsAssessment.setCorrectAnswerId(answer.get(0).getAnswerId());
//    	}
    	//wsAssessment.setPlpAnswersList(wsAssessment.getOptions().get(wsAssessment.getCurrentQuestionNum()));
//    	wsAssessment.setPlpAnswersList(wsAssessment.getOptions().get(wsAssessment.getCurrentQuestion().getQuestionId()));

    	logger.debug("END");
        return new ModelAndView("../learning/showWSQuestion", getModel());
    }
    
    /**
     * Method to fetch plpQuestionsList by questionIds.
     * 
     * @param request
     */
    public  List<PLPQuestion> getPLPQuestionList(PortletRequest request, String questionIdsList) {
    	List<PLPQuestion> plpQuestionsList = null;
    	logger.debug("questionIdsList from fetchQuestion-------------"+questionIdsList);
     	if(questionIdsList != null){
     		plpQuestionsList = getPLPQuestionsListByQuestionIdsList(questionIdsList);
     	}
     	return plpQuestionsList;
    }
    
    /**
     * Method to fetch plpQuestionsList by questionIdsList.
     * 
     * @param questionIdsList
     * @return
     */
    public  List<PLPQuestion> getPLPQuestionsListByQuestionIdsList(String questionIdsList) {
    	List<PLPQuestion> plpQuestionsList = null;
    	if(questionIdsList != null){
     		try {
 				plpQuestionsList = serviceLocator.getPlpQuestionService()
 						.getQuestionsListByQIds(questionIdsList);
 				
 			} catch (HibernateException e) {
 				logger.error("Hibernate Exception Caught in TestWorkSheetController.fetchQuestion()", e);
 				e.printStackTrace();
 			}
     	}
		return plpQuestionsList;
    	
    }


    /**
     * Method to fetch userAnswerId for question.
     * 
     * @param currentQuestionNum
     * @param questionList
     * @return
     */
   private Integer getUserAnswerIdForQuestion() {
		Integer userAnswerId = null;
		PLPQuestion question = wsAssessment.getCurrentQuestion();
		if(question != null) {
			userAnswerId = question.getAnswerId();
		}
		return userAnswerId;
	}

/**
    * Method used to fetch correctAnswerId based on question.
    * 
    * @param questionNum
    */
    private Integer getCorrectAnswerId(PLPQuestion question) {
    	Integer correctAnswerId = null;
    	List<PLPAnswer> answers = wsAssessment.getOptions().get(question.getQuestionId());
    	if(answers != null && !answers.isEmpty()) {
			for (PLPAnswer plpAnswer : answers) {
				logger.debug("plpAnswer.getAnswer(): " + plpAnswer.getAnswer());
				logger.debug("plpAnswer.getIsCorrect(): " + plpAnswer.getIsCorrect());
				if (1 == plpAnswer.getIsCorrect()) {
					correctAnswerId = plpAnswer.getAnswerId();
				}
			}
		}
		return correctAnswerId;
	}

    /**
     * Method used to fetch correctAnswer object based on question.
     * 
     * @param questionNum
     */
     private PLPAnswer getCorrectAnswer(Integer questionNum) {
     	PLPAnswer plpAnswer = null;
    	List<PLPAnswer> plpAnswersList = wsAssessment.getOptions().get(questionNum);
 		for(PLPAnswer answer : plpAnswersList){
 			if(answer.getPLPQuestion().getQuestionId().equals(questionNum) && answer.getIsCorrect() == 1) {
 				plpAnswer = answer;
 				break;
 			}
 		}
 		return plpAnswer;
 	}

	/**
     * Method to fetch options for question.
     * 
     * @param questionNum
     * @return
     */
	private Map<Integer, List<PLPAnswer>> getOptionForAnswer(Integer questionNum) {
		Map<Integer, List<PLPAnswer>> questionAndAnswerOptionsMap = null;
		if(Validator.isNotNull(questionNum)) {
			questionAndAnswerOptionsMap = serviceLocator.getPlpAnswerService().getQuestionsAndAnswersMap(questionNum);
		}
		return questionAndAnswerOptionsMap;
	}

	@ResourceMapping("submitWSAssessment")
    public void submitAssessment(@RequestParam Integer worksheetId, ResourceRequest request, ResourceResponse response) throws IOException {
    	logger.debug("START");
    	PLPStudentWrokSheet workSheet = null;
    	ThemeDisplay themeDisplay = WebUtil.getThemeDisplay(request);
    	try {
			workSheet = serviceLocator.getPlpStudentWorkSheetService().getPLPStudentWorkSheetByWorkSheetId(worksheetId, themeDisplay.getUserId());
		} catch (HibernateException e) {
			logger.error("HibernateException Caught While Fetching plpStudentWorkSheet by workSheetId.", e);
			e.printStackTrace();
		}
    	if(workSheet != null) {
    		workSheet.setStatus(AssessmentConstants.TestStatus.COMPLETED);
    	}
    	
    	
//    		PLPTopicMaster parentTopicMaster = null;
//    		PLPTopicMaster subTopicMaster = null;
//    		try {
//				parentTopicMaster = serviceLocator.getTopicMasterService().getTopicMasterByTopicId(workSheet.getTopicId());
//				subTopicMaster = serviceLocator.getTopicMasterService().getTopicMasterByTopicId(workSheet.getSubtopicId());
//			} catch (HibernateException e) {
//				logger.error("Hibernate Exception Caught While fetching topic master", e);
//				e.printStackTrace();
//			}
    		
    	try {
			serviceLocator.getPlpStudentWorkSheetService().updatePLPStudentWorkSheet(workSheet);
			String topicOrsubtopicName = wsAssessment.getSubTopicName();
			if (topicOrsubtopicName == null) {
				topicOrsubtopicName = wsAssessment.getTopicName();
			}
			Date date = Calendar.getInstance().getTime();
    		SimpleDateFormat dformat = new SimpleDateFormat("dd/MMM/yyyy");
    		String now = dformat.format(date);
//    		if(workSheet != null && workSheet.getSubtopicId() != null && parentTopicMaster != null && parentTopicMaster.getTopicName() != null){
//    			 topicOrsubtopicName =  parentTopicMaster.getTopicName();
//    		}
//    		if(workSheet != null && workSheet.getTopicId() != -1 && subTopicMaster != null && subTopicMaster.getTopicName() != null){
//    			 topicOrsubtopicName =  subTopicMaster.getTopicName();
//    		}
    			serviceLocator.getRewardPointsService().addRewardPoints(
    				WebUtil.getUserId(request), 10, " Has Completed "+topicOrsubtopicName+" Worksheet", now);
    		
		} catch (HibernateException e) {
			logger.error("Hibernate Exception Caught While updating workSheet.", e);
			e.printStackTrace();
		}
    	
    	logger.debug("Assessment updated.");
    	//response.getWriter().print();
    	response.flushBuffer();
    	logger.debug("END");
    }
	
	@ResourceMapping("showWSReport")
	public ModelAndView showWSReport(@RequestParam Integer worksheetId, ResourceRequest request, ResourceResponse response) {
			logger.debug("START");
		Map<String, List<WorkSheetReport>> qAndaMap = new HashMap<String, List<WorkSheetReport>>();
	    List<WorkSheetReport> wsReportsList = new ArrayList<WorkSheetReport>();
	    List<PLPQuestion> plpQuestionsList = null;
		PLPStudentWrokSheet workSheet = null;
		WorkSheetReport wsReport = null;
		ThemeDisplay themeDisplay = WebUtil.getThemeDisplay(request);
		try {
			workSheet = serviceLocator.getPlpStudentWorkSheetService().getPLPStudentWorkSheetByWorkSheetId(worksheetId, themeDisplay.getUserId());
		} catch (HibernateException e) {
			logger.error("HibernateException Caught While Processing showWSReport in TestWorkSheetController--", e);
			e.printStackTrace();
		}
		
		if(workSheet != null && workSheet.getQuestionIdList() != null) {
			plpQuestionsList = getPLPQuestionsListByQuestionIdsList(workSheet.getQuestionIdList());
		}
		if(plpQuestionsList != null && !plpQuestionsList.isEmpty() && plpQuestionsList.size() > 0) {
			logger.debug("plpQuestionsList.size() in showWSReport in TestWorkSheetController------->"+plpQuestionsList.size());
	    		for(PLPQuestion question : plpQuestionsList) {
	    			wsReport = new WorkSheetReport();
	    			PLPQuestion tempQuestion = serviceLocator.getPlpQuestionService().getQuestionById(question.getQuestionId()).get(0);
	    			wsAssessment.setOptions(getOptionForAnswer(question.getQuestionId()));
	    			wsReport = new WorkSheetReport();
	    			wsReport.setQuestion(tempQuestion.getQuestion());
	    			wsReport.setAnswer(getCorrectAnswer(question.getQuestionId()).getAnswer());
	    			PLPAnswer answer = null;
					try {
						answer = serviceLocator.getPlpAnswerService().getPLPAnswerByAnswerId(question.getAnswerId());
					} catch (HibernateException e) {
						logger.error("Hibernate Exception Caught While showing wereport", e);
						e.printStackTrace();
					}
	    			if(answer != null) {
	    			wsReport.setUserAnswer(answer.getAnswer());
	    			}
	    			wsReportsList.add(wsReport);
	    		}
	    		qAndaMap.put("wsReportsList", wsReportsList);
		}
		return new ModelAndView("../learning/ShowWSReport", qAndaMap);
		
	}

	public Integer getQuestionCount() {
		logger.debug("START");
		if (wsAssessment != null) {
			logger.debug("wsAssessmentCount: " + wsAssessment.getQuestionCount());
			return wsAssessment.getQuestionCount();
		}
		logger.debug("END");
		return 0;
	}

	public Integer getCurrentQuestionNum() {
		logger.debug("START");
		logger.debug("currentQuestionNum: " + wsAssessment.getCurrentQuestionNum());
		logger.debug("END");
		return wsAssessment.getCurrentQuestionNum();
	}

    public PLPQuestion getCurrentQuestion() {
    	logger.debug("START");
    	logger.debug("currentQuestionNum: " + wsAssessment.getCurrentQuestionNum());
    	if (wsAssessment != null && wsAssessment.getQuestions() != null && !wsAssessment.getQuestions().isEmpty()) {
    		return wsAssessment.getQuestions().get(wsAssessment.getCurrentQuestionNum() - 1);
    	}
    	logger.debug("END");
    	return null;
    }
    public List<PLPAnswer> getOptions() {
    	logger.debug("START");
    	logger.debug("currentQuestionNum: " + wsAssessment.getCurrentQuestionNum());
    	if (wsAssessment != null) {
    		return getOptionForAnswer(this.getCurrentQuestionNum()).get(this.getCurrentQuestion().getQuestionId());
    	}
    	logger.debug("END");
    	return null;
    }
    public String getTimeRemaining() {
    	logger.debug("START");
    	logger.debug("timeRemaining: " + wsAssessment.getTimeRemaining());
    	logger.debug("END");
        return this.getTimeRemainingMinites();
    }

    @ResourceMapping("wstimeRemaining")
	public void serveTimeRemaining(ResourceRequest request, ResourceResponse response) throws IOException {
    	if (wsAssessment.getTimeRemaining() > 0) {
    		wsAssessment.setTimeRemaining(((wsAssessment.getTimeRemaining()) - 1));
    		response.getWriter().println(getTimeRemainingMinites());
    		response.flushBuffer();
    	} else {
    		response.getWriter().println(-1);
    		response.flushBuffer();
    	}
	}

    public String getTimeRemainingMinites() {
    	String time = "00:00";
    	if (wsAssessment.getTimeRemaining() > 0) {
    		long mins = wsAssessment.getTimeRemaining() / 60;
    		time = (mins<10 ? "0"+mins : ""+mins);
    		long secs = wsAssessment.getTimeRemaining() % 60;
    		time += " : " + (secs<10 ? "0"+secs : ""+secs);
    	}
    	return time;
    }

    @ResourceMapping("updateWSAnswer")
	public void updateAnswer(@RequestParam Integer answerId, @RequestParam  Integer questionId, ResourceRequest request, ResourceResponse response) throws IOException {
    	logger.debug("START");
    	logger.debug("Selected answer: " + answerId);
    	if (wsAssessment != null) {
    		//final PLPQuestion question = wsAssessment.getQuestions().get(wsAssessment.getCurrentQuestionNum());
//    		List<PLPQuestion> questionsList = serviceLocator.getPlpQuestionService().getQuestionById(questionId);
    		final PLPQuestion question = wsAssessment.getCurrentQuestion();
    		try {
				question.setAnswerId(answerId);

				if(wsAssessment.getAnsweredCount() < wsAssessment.getQuestionCount()) {
					wsAssessment.setAnsweredCount(((wsAssessment.getAnsweredCount()) +1));
				}
				serviceLocator.getPlpQuestionService().updateAnswerIdForQuestion(question);
				
			} catch (Exception e) {
				logger.error("Exception Caught While Updating answer for plpQuestion", e);
			}
    		
    		logger.debug("answer saved.");
    	}
		logger.debug("END");
	}
    
    public List<String> getQuestionsList(String questionIdsList){
		List<String> tempList =null;
		String[] tempArray = null;
		if(questionIdsList != null && questionIdsList.contains(",")){
			tempArray = questionIdsList.split(",");
		}
		if(tempArray != null && tempArray.length > 0){
			tempList = Arrays.asList(tempArray);
		}
		logger.debug("questionIds list: " + tempList);
		if(tempList != null && !tempList.isEmpty()) {
			return tempList;
		} else {
			tempList = new ArrayList<String>();
			return tempList;
		}
	}

}
