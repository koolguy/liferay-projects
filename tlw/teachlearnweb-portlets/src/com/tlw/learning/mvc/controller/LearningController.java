/*
 * This code is property of Teach Learn Web, Inc. Use, duplication and disclosure
 * in any form without permission of copyright holder is prohibited.
 *
 * (C) Copyright Teach Learn Web, Inc. 2012. All rights reserved.
 */
package com.tlw.learning.mvc.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.asset.model.AssetCategory;
import com.liferay.portlet.asset.model.AssetEntry;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.teachlearnweb.dao.entity.ClassMaster;
import com.teachlearnweb.dao.entity.DTAssessment;
import com.teachlearnweb.dao.entity.PLPQuestion;
import com.teachlearnweb.dao.entity.PLPStudentWrokSheet;
import com.teachlearnweb.dao.entity.PLPTopicMaster;
import com.teachlearnweb.dao.entity.Skill;
import com.teachlearnweb.dao.entity.SubjectMaster;
import com.teachlearnweb.service.vo.User;
import com.tlw.assessment.vo.AssessmentConstants;
import com.tlw.assessment.vo.SkillWiseAssessmentReport;
import com.tlw.catalogmanagement.mvc.vo.Course;
import com.tlw.controller.AbstractBaseController;
import com.tlw.util.WebUtil;

/**
 * The Class RegistrationController, is responsible to handle the user
 * registration request.
 * @author Vissu Kumar
 * @author Mallikarjun
 */
/**
 * @author Mallikarjun
 * 
 */
@Controller("learningController")
@Scope("session")
@RequestMapping(value = "VIEW")
public class LearningController extends AbstractBaseController {

	/** The LOG. */
	private static final Logger LOG = Logger
			.getLogger(LearningController.class);

	/** The errormsg key. */
//	private static final String ERROR_MSG_KEY = "errormsg";

	/** Map to hold Asset Entries. */
	private Map<String, List<AssetDetailsPojo>> assetEntryMap = new HashMap<String, List<AssetDetailsPojo>>();

	public List<AssetDetailsPojo> assetEntryList = null;

	public List<AssetDetailsPojo> allAssetEntriesDetailsList = null;

	public String defaultLeftSection = null;

	String VIDEO_EXTENSIONS = "wmv_swf_avi_mpg_vob_mov_mkv_qt_mpeg_rm_mp4";
	String PRESENTATION_EXTENSIONS = "ppt_pptx_swf_pdf";
	String POINTS_EXTENSIONS = "txt";
	String NOTES_EXTENSIONS = "pdf";
	String DICTONARY_EXTENSIONS = "";
	String WORKSHEET_EXTENSIONS = "";
	String INLINE_EXTENSIONS = "";

	String VIDEO = "Video";
	String PRESENTATION = "Presentation";
	String POINTS = "Points";
	String NOTES = "Notes";
	String DICTONARY = "Dictionary";
	String WORKSHEET = "Worksheet";
	String INLINE = "Inline";
	String subjectName = "";
	String className = "";
	String syllabusName = "";
	String strNumberOfTopics = null;
	String strNumberOfAllSubTopics = null;
	String OVERVIEW = "Overview";
	String defaultSelSection = "";
	String VALUE_TRUE = "TRUE";
	String VALUE_FALSE = "FALSE";
	String defaultLoading = VALUE_FALSE;

	public String strSubTopicId;

	private static final String VIEW_WORKSHEETS = "viewWorkSheets";

	/**
	 * Show registration.
	 * 
	 * @param renderResponse
	 *            the render response
	 * @return the string
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@RenderMapping
	public ModelAndView showCourses(RenderRequest renderReq,
			RenderResponse renderResponse) {

		String level1Child = null;
		String level2Child = null;
		LOG.debug("@RenderMapping: showCourses: invoked" + subjectName);
		if (null != renderReq.getPortletSession().getAttribute(
				"topics_assetCatName", PortletSession.APPLICATION_SCOPE)) {
			
			subjectName = renderReq
					.getPortletSession()
					.getAttribute("topics_assetCatName",
							PortletSession.APPLICATION_SCOPE).toString();			
		}
		
		if (null != renderReq.getPortletSession().getAttribute(
				"subject_className", PortletSession.APPLICATION_SCOPE)) {
			
			className = renderReq
			.getPortletSession()
			.getAttribute("subject_className",
					PortletSession.APPLICATION_SCOPE).toString();
		}
		
		if (null != renderReq.getPortletSession().getAttribute(
				"subject_syllabus", PortletSession.APPLICATION_SCOPE)) {
		
			syllabusName = renderReq
			.getPortletSession()
			.getAttribute("subject_syllabus",
					PortletSession.APPLICATION_SCOPE).toString();
		}
		
		if (null != renderReq.getPortletSession().getAttribute(
				"strNumberOfTopics", PortletSession.APPLICATION_SCOPE)) {
			
			strNumberOfTopics = renderReq
			.getPortletSession()
			.getAttribute("strNumberOfTopics",
					PortletSession.APPLICATION_SCOPE).toString();
		}
		
		if (null != renderReq.getPortletSession().getAttribute(
				"strNumberOfAllSubTopics", PortletSession.APPLICATION_SCOPE)) {
		
			strNumberOfAllSubTopics = renderReq
			.getPortletSession()
			.getAttribute("strNumberOfAllSubTopics",
					PortletSession.APPLICATION_SCOPE).toString();			
		}
		
		if (null != renderReq.getPortletSession().getAttribute(
				"subject_defaultLoading", PortletSession.APPLICATION_SCOPE)) {
			defaultLoading = renderReq
					.getPortletSession()
					.getAttribute("subject_defaultLoading",
							PortletSession.APPLICATION_SCOPE).toString();
		}
		LOG.debug("isDefaultLoading :" + defaultLoading);
		if (null != defaultLoading &&
				VALUE_TRUE.equalsIgnoreCase(defaultLoading)) {
			// LOG.debug("isDefaultLoading :"+defaultLoading);
			level1Child = OVERVIEW;
			level2Child = "";
			defaultSelSection = "Subject Status";
			String subjectId = null;
			if (null != renderReq.getPortletSession().getAttribute(
					"subject_subjectId", PortletSession.APPLICATION_SCOPE)) {
				subjectId = renderReq
						.getPortletSession()
						.getAttribute("subject_subjectId",
								PortletSession.APPLICATION_SCOPE).toString();
			}

			callOnDefaultLoading(subjectId);
			defaultLoading = VALUE_FALSE;

		} else { // not default loading

			if (null != renderReq.getPortletSession().getAttribute(
					"topics_level1Child", PortletSession.APPLICATION_SCOPE)) {
				level1Child = renderReq
						.getPortletSession()
						.getAttribute("topics_level1Child",
								PortletSession.APPLICATION_SCOPE).toString();
			}
			if (null != renderReq.getPortletSession().getAttribute(
					"topics_level2Child", PortletSession.APPLICATION_SCOPE)) {
				level2Child = renderReq
						.getPortletSession()
						.getAttribute("topics_level2Child",
								PortletSession.APPLICATION_SCOPE).toString();
			}

			LOG.debug("showCourses:  subjectName from session :" + subjectName);
			LOG.debug("showCourses:  level1Child from session :" + level1Child);
			LOG.debug("showCourses:  level2Child from session :" + level2Child);

		}

		// setting subjectName in session.
		renderReq.getPortletSession().setAttribute("subjectName", subjectName);
		boolean wsFlag = Validator.isNotNull(renderReq.getPortletSession().getAttribute("wsFlag")) ? Boolean.valueOf(renderReq.getPortletSession().getAttribute("wsFlag").toString()) : false;
		LOG.debug("wsFlag from rendermapping---------------"+wsFlag);
		Integer subTopicId = Validator.isNotNull(renderReq.getPortletSession().getAttribute("subTopicId")) ? (Integer)renderReq.getPortletSession().getAttribute("subTopicId") : null;
		LOG.debug("subTopicId from rendermapping---------------"+subTopicId);
		
		Map model = new HashMap();
		model.put("topics_assetCatName", subjectName);
		model.put("subject_className", className);
		model.put("subject_syllabus", syllabusName);
		model.put("strNumberOfTopics", strNumberOfTopics);
		model.put("strNumberOfAllSubTopics", strNumberOfAllSubTopics);		
		model.put("topics_level1Child", level1Child);
		model.put("topics_level2Child", level2Child);
		model.put("assetEntryMap", assetEntryMap);
		model.put("defaultSelSection", defaultSelSection);
		model.put("wsFlag", wsFlag);
		model.put("subTopicId", subTopicId);
				
		renderReq.getPortletSession().setAttribute("subTopicId", null);
		renderReq.getPortletSession().setAttribute("wsFlag", false);
		
		LOG.debug("returning :  subjectName ");
		return new ModelAndView("showContentList", model);
	}

	/**
	 * Gets the command object.
	 * 
	 * @return the command object
	 */
	@ModelAttribute("courseModelBean")
	public Course getCommandObject() {
		LOG.debug("@ModelAttribute getCommandObject invoked");
		LOG.debug("Creating object of model RegistrationModelBean..");
		Course courseModelBean = new Course();

		return courseModelBean;
	}

	/**
	 * Model Object for displaying Asset Categories.
	 * 
	 * @param portletRequest
	 * @param portletResponse
	 * @return Map<Long, List<AssetCategory>>
	 * @throws SystemException
	 */
	@ModelAttribute(value = "assetEntryList")
	public List<AssetDetailsPojo> getAssetCategoryList(
			PortletRequest portletRequest, PortletResponse portletResponse)
			throws SystemException {

		LOG.debug("@ModelAttribute getAssetCategoryList invoked");

		return assetEntryList;
	}

	/**
	 * Show actionShowSeletedAssets.
	 * 
	 * @param ResourceRequest
	 *            the resource request
	 * @return the ModelAndView Description: Serves ajax request to get selected
	 *         assests
	 * 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@ResourceMapping("actionShowSeletedAssets")
	public ModelAndView actionShowSeletedAssets(ResourceRequest resourceRequest)
			throws PortalException, SystemException {

		defaultLoading = VALUE_FALSE;

		LOG.debug("@ResourceMapping actionShowSeletedAssets invoked");
		assetEntryList = null;
		Map model = new HashMap();
		if (null != resourceRequest.getParameter("key")) {
			LOG.debug("selected section" + resourceRequest.getParameter("key"));
			defaultSelSection = resourceRequest.getParameter("key");
			// String selectedLeftSectionForCss =
			// resourceRequest.getParameter("key").toLowerCase().replaceAll(" ",
			// "");
			String selectedSection = resourceRequest.getParameter("key")
					.toString();

			assetEntryList = assetEntryMap.get(resourceRequest
					.getParameter("key"));
			model = new HashMap();
			model.put("selectedSection", selectedSection);
			model.put("selectedAssetsList", assetEntryList);
			model.put("defaultSelSection", defaultSelSection);
			model.put("strSubTopicId", strSubTopicId);
			
			// model.put("selectedLeftSectionForCss",
			// selectedLeftSectionForCss);

		}
		return new ModelAndView("showAssetsContent", model);

	}

	/**
	 * Show actionShowSeletedAssets.
	 * 
	 * @param ResourceRequest
	 *            the resource request
	 * @return the ModelAndView Description: Serves ajax request to get selected
	 *         assests
	 * 
	 */
	@ResourceMapping("showContent")
	public ModelAndView showContent(ResourceRequest resourceRequest)
			throws PortalException, SystemException {

		defaultLoading = VALUE_FALSE;
		LOG.debug("@ResourceMapping showContent invoked");
		LOG.debug("selected assetType....."
				+ resourceRequest.getParameter("assetType"));

		String groupId = resourceRequest.getParameter("groupId");
		String folderId = resourceRequest.getParameter("folderId");
		String title = resourceRequest.getParameter("title");
		String assetType = resourceRequest.getParameter("assetType");
		String assetExtension = resourceRequest.getParameter("assetExtension");
		
		if(null != assetExtension){
		assetExtension = assetExtension.toLowerCase();
		}
		AssetDetailsPojo assetPojo = new AssetDetailsPojo();
		assetPojo.setGroupId(Long.valueOf(groupId));
		assetPojo.setFolderId(Long.valueOf(folderId));
		assetPojo.setTitle(title);
		assetPojo.setAssetType(assetType);
		assetPojo.setExtension(assetExtension);

		Map<String, AssetDetailsPojo> model = new HashMap<String, AssetDetailsPojo>();

		model.put("assetPojo", assetPojo);
		return new ModelAndView("openContent", model);

	}

	@SuppressWarnings({ "rawtypes", "unused" })
	@EventMapping(value = "{http:teachlearnweb.com/events}topicsMap")
	public void fetchTopicId(EventRequest eventRequest,
			EventResponse eventResponse, Model model) {
		LOG.debug("@EventMapping fetchTopicId invoked");
		defaultLoading = VALUE_FALSE;
		LOG.debug("In Process event :");
		Event event = eventRequest.getEvent();
		LOG.debug("event value *****************" + event.getValue());

		String subjectName = Validator.isNotNull(eventRequest
				.getPortletSession().getAttribute("subjectName")) ? (String) eventRequest
				.getPortletSession().getAttribute("subjectName") : "";
		// start
		HashMap topicsMap = new HashMap();
		List<PLPStudentWrokSheet> wsList = null;
		String topicId = null;
		String level2ChildName = null;
		String level1ChildName = null;

		if (event.getValue() != null) {
			topicsMap = (HashMap) event.getValue();
			topicId = (String) topicsMap.get("topicsId");
			level2ChildName = (String) topicsMap.get("subTopicName");
			level1ChildName = (String) topicsMap.get("parentTopicName");
			
			strSubTopicId = topicId;
			LOG.debug("topicId----------" + topicId);
			LOG.debug("level2ChildName-------------------"
					+ level2ChildName);
			LOG.debug("level1ChildName-------------------"
					+ level1ChildName);
			LOG.debug("@EventMapping fetchTopicId() Method in LearningController -- topicId----------"
					+ topicId);
		}
		
		ThemeDisplay themeDisplay = WebUtil.getThemeDisplay(eventRequest);
		themeDisplay.getUser().getExpandoBridge().getClassName();
		int dtPercentage = assessmentStatus(eventRequest);
		//dtPercentage = 100;//for testing
		LOG.debug("dtPercentage................................"+dtPercentage);
		if (dtPercentage == 100) {
			createWorkSheetBySubTopicId(eventRequest, subjectName, topicId, level2ChildName, level1ChildName);
		}
		// End

		Map<String, List<AssetDetailsPojo>> assetEntriesMap = null;
		try {
			assetEntriesMap = populateAssestsBasedOnCategory(eventRequest,
					Long.parseLong(String.valueOf(topicId)));
		} catch (NumberFormatException e) {
			LOG.debug("Exception Caught while fetching populateAssestsBasedOnCategory");
			e.printStackTrace();
		}
		LOG.debug("assetEntriesMap***" + assetEntriesMap);

		model.addAttribute("assetEntriesMap", assetEntriesMap);
		eventResponse.setRenderParameter("render", "showCourse");

	}

	/**
	 * Helper Method to check to create worksheet for subTopicId.
	 * 
	 * @param eventRequest
	 * @param subjectName
	 * @param topicId
	 * @param level2ChildName
	 * @param level1ChildName
	 */
	private void createWorkSheetBySubTopicId(EventRequest eventRequest, String subjectName,	String topicId, String level2ChildName,	String level1ChildName) {
		Integer subTopicId = null;
		boolean wsFlag = false;
		PLPTopicMaster subTopicMaster = null;
		PLPTopicMaster parentTopicMaster = null;
		ThemeDisplay themedisplay = WebUtil.getThemeDisplay(eventRequest);
		ClassMaster classMaster = null;
		SubjectMaster subjectMaster = null;
		try {
			//User user = serviceLocator.getUserService().findUserByUserId(themedisplay.getCompanyId(), themedisplay.getUserId());
			final String className = (String) eventRequest.getPortletSession().getAttribute("subject_className", PortletSession.APPLICATION_SCOPE);
			LOG.debug("className from session: " + className);
			//User user = serviceLocator.getUserService().findUserByUserId(themedisplay.getCompanyId(), themedisplay.getUserId());
			if(className != null) {
				classMaster = serviceLocator.getAssessmentService().getClassMasterByClassName(className);
			}
		} catch (HibernateException e) {
			LOG.error("Hibernate Exception Caught While fetching class master in Learing Controller", e);
			e.printStackTrace();
		}
		
		if(subjectName != null) {
			try {
					subjectMaster = serviceLocator.getAssessmentService().getSubjectMasterbySbujectName(subjectName);
			} catch (HibernateException e) {
				LOG.error("Hibernate Exception Caught While fetching subject master in Learing Controller", e);
				e.printStackTrace();
			}
		}
		if (level1ChildName != null) {
			try {
				LOG.debug("level1ChildName......................"+level1ChildName);
				parentTopicMaster = serviceLocator.getTopicMasterService()
						.getTopicMasterByTopicName(level1ChildName.trim(),
								classMaster.getClassId(), subjectMaster.getSubjectId());
				LOG.debug("parentTopicMaster.................."+parentTopicMaster);
			} catch (HibernateException e) {
				LOG.error(
						"Hibernate Exception Caught While Fetching topicMaster Object",
						e);
				e.printStackTrace();
			}
		}
		if(level2ChildName != null && classMaster != null && classMaster.getClassId() != null 
				&& subjectMaster != null && subjectMaster.getSubjectId() != null
				&& parentTopicMaster != null) {
			try {
				LOG.debug("level2ChildName......................"+level2ChildName);
				subTopicMaster = serviceLocator.getTopicMasterService()
								.getTopicMasterByTopicName(level2ChildName.trim(), parentTopicMaster.getId().getTopicId(),  
										classMaster.getClassId(), subjectMaster.getSubjectId());
				LOG.debug("subTopicMaster.................."+subTopicMaster);
			} catch (HibernateException e) {
				LOG.error("Hibernate Exception Caught While fetching topicMaster in createWorkSheetBySubTopicId", e);
			}
		}
		if(subTopicMaster != null && subTopicMaster.getId().getTopicId() != -1) {
			LOG.debug("@EventMapping fetchTopicId() Method in LearningController -- subTopicMaster.getTopicId()----------"
					+ subTopicMaster.getId().getTopicId());
			try {
				wsFlag = serviceLocator.getPlpStudentWorkSheetService().checkWorkSheetListForSubTopicId(subTopicMaster.getId().getTopicId(), themedisplay.getUserId(),
							classMaster.getClassId(), subjectMaster.getSubjectId());
				LOG.debug("wsFlag from Learnign Controller---------------"+wsFlag);
			} catch (HibernateException e) {
				LOG.error("HibernateException Caught While fetching wsList----", e);
				e.printStackTrace();
			}
			
		}
		if (wsFlag) {
			LOG.debug("wsFlag................................"+wsFlag);
			LOG.debug("wsFlag-------------"+ wsFlag);
			eventRequest.getPortletSession().setAttribute("wsFlag", wsFlag);
			eventRequest.getPortletSession().setAttribute("subTopicId", subTopicMaster.getId().getTopicId());
		} else if(subTopicMaster != null && subTopicMaster.getId().getTopicId() != -1) {
			try {
				LOG.debug("Before create PLP work sheet..........................................................................................................................");
				LOG.debug("Before create PLP work sheet--------------------.....................................");
				createPLPStudentWorkSheet(eventRequest, subjectName, level2ChildName, level1ChildName, classMaster.getClassId(), subjectMaster.getSubjectId());
				wsFlag = serviceLocator.getPlpStudentWorkSheetService().checkWorkSheetListForSubTopicId(subTopicMaster.getId().getTopicId(), themedisplay.getUserId(),
						classMaster.getClassId(), subjectMaster.getSubjectId());
				LOG.debug("wsFlag-------------"+ wsFlag);
				LOG.debug("subTopicId-------------"+ subTopicId);
				eventRequest.getPortletSession().setAttribute("wsFlag", wsFlag);
				eventRequest.getPortletSession().setAttribute("subTopicId", subTopicMaster.getId().getTopicId());
			} catch (HibernateException e) {
				LOG.debug("Exception Caught while either saving plpStudentWorkSheet or retrieving plpStudentWorkSheet.");
				e.printStackTrace();
			}

		}
	}
	
	/**
	 * Method to create workSheet.
	 * 
	 * @param renderReq
	 * @param skillNames
	 */
	private void createPLPStudentWorkSheet(PortletRequest portletRequest,
			String subjectName, String subTopicId, String parentTopicId, Short classId, Short subjectId) {
		List<String> skillNames = new ArrayList<String>();
		System.out.println("************createPLPStudentWorkSheet() Method start in Learning Controller************");
		LOG.debug("************createPLPStudentWorkSheet() Method start in Learning Controller************");
		SubjectMaster subjectMaster = null;
		PLPTopicMaster parentTopicMaster = null;
		PLPTopicMaster subTopicMaster = null;
		ClassMaster classMaster = null;
		List<DTAssessment> dtAssessmentsList = null;
		User user = null;

		try {
			subjectMaster = serviceLocator.getAssessmentService()
					.getSubjectMasterbySbujectName(subjectName);
			System.out.println("subjectId......................"+subjectMaster.getSubjectId());
			System.out.println("SubjectName .................."+subjectMaster.getSubjectName());
			LOG.debug("subjectId......................"+subjectMaster.getSubjectId());
			LOG.debug("SubjectName .................."+subjectMaster.getSubjectName());
		} catch (HibernateException e) {
			LOG.error(
					"Hibernate Exception Caught While Fetching subjectMaster Object",
					e);
			e.printStackTrace();
		}

		if (parentTopicId != null) {
			try {
				parentTopicMaster = serviceLocator.getTopicMasterService()
						.getTopicMasterByTopicName(parentTopicId, classId, subjectId);
				System.out.println("TopicId......................"+parentTopicId);
				System.out.println("Topic Name .................."+parentTopicMaster.getTopicName());
				LOG.debug("TopicId......................"+parentTopicId);
				LOG.debug("Topic Name .................."+parentTopicMaster.getTopicName());
			} catch (HibernateException e) {
				LOG.error(
						"Hibernate Exception Caught While Fetching topicMaster Object",
						e);
				e.printStackTrace();
			}
		}

		if (subTopicId != null && parentTopicMaster != null) {
			try {
				subTopicMaster = serviceLocator.getTopicMasterService()
						.getTopicMasterByTopicName(subTopicId, parentTopicMaster.getId().getTopicId(), classId, subjectId);
				System.out.println("subTopicId......................"+subTopicId);
				System.out.println("subTopic Name .................."+subTopicMaster.getTopicName());
				LOG.debug("subTopicId......................"+subTopicId);
				LOG.debug("subTopic Name .................."+subTopicMaster.getTopicName());
			} catch (HibernateException e) {
				LOG.error(
						"Hibernate Exception Caught While Fetching topicMaster Object",
						e);
				e.printStackTrace();
			}
		}

		/*
		 * if(subjectMaster != null && subjectMaster.getSubjectId() != null) {
		 * try { topicMaster =
		 * serviceLocator.getTopicMasterService().getTopicMasterBySubjectId
		 * (subjectMaster.getSubjectId()); } catch (HibernateException e) {
		 * LOG.error
		 * ("Hibernate Exception Caught While Fetching topicMaster Object", e);
		 * e.printStackTrace(); } }
		 */

		if (classId != null)
		{
			try {
				classMaster = serviceLocator.getAssessmentService()
						.getClassMasterByClassId(classId);
				System.out.println("classMaster in Learning Controller"+classMaster);
				LOG.debug("classMaster in Learning Controller"+classMaster);
			} catch (HibernateException e) {
				LOG.error(
						"Hibernate Exception Caught While Fetching classMaster Object",
						e);
				e.printStackTrace();
			}
		}
		LOG.debug("className:--------" + classMaster.getClassName());
		long userId = 0;
		try {
			//user = WebUtil.getUser(portletRequest);
			userId = WebUtil.getUserId(portletRequest);
			System.out.println("User-------------------------."+userId);
		} catch (HibernateException e) {
			LOG.error("Hibernate Exception Caught While Fetching User Object",
					e);
			e.printStackTrace();
		}

		if (subjectMaster != null && subjectMaster.getSubjectId() != null) {
			LOG.debug("user.getUserId():--------" + userId);
			try {

				dtAssessmentsList = getDTAssessmentByUserId(userId,
						subTopicMaster.getId().getClassId(),
						subjectMaster.getSubjectId(),
						AssessmentConstants.TEST_TYPE_DIAGNOSTIC_TEST);
			} catch (HibernateException e) {
				LOG.debug(
						"Hibernate Exception Caught While Fetching dtAssessmentsList",
						e);
				e.printStackTrace();
			}
			if (dtAssessmentsList != null && !dtAssessmentsList.isEmpty()
					&& dtAssessmentsList.size() > 0) {
				LOG.debug("dtAssessmentsList.size().........."+dtAssessmentsList.size());
//				System.out.println("dtAssessmentsList.size().........."+dtAssessmentsList.size());
				
				DTAssessment assessment = dtAssessmentsList.get(0);
				LOG.debug("assessment.getAssessmentId().........."+assessment.getAssessmentId());
				skillNames = processDTAssessmentsList(assessment
						.getAssessmentId());
				LOG.debug("skillNames Size.........."+skillNames.size());
//				System.out.println("skillNames Size.........."+skillNames.size());
			}
		}

		List<PLPQuestion> plpQuestionsList = null;
		if (skillNames != null && !skillNames.isEmpty()
				&& subjectMaster != null && classMaster != null) {
			System.out.println("In skills if.........................");
			PLPStudentWrokSheet workSheet = null;
			for (String skill : skillNames) {
				System.out.println("skill...................."+skill);
				// create worksheet
				workSheet = new PLPStudentWrokSheet();
				workSheet.setCreatedDate(new java.util.Date());
				workSheet
						.setEndDate(new java.util.Date(workSheet
								.getCreatedDate().getTime()
								+ (7 * 24 * 60 * 60 * 1000)));
				workSheet.setStartDate(new java.util.Date());
				workSheet.setUserId(WebUtil.getUserId(portletRequest));
				workSheet.setSubjectMaster(subjectMaster);
				workSheet.setClassMaster(classMaster);
				if(parentTopicMaster != null && parentTopicMaster.getId() != null && parentTopicMaster.getId().getTopicId() != -1) {
					workSheet.setTopicId(parentTopicMaster.getId().getTopicId());
				}
				if(subTopicMaster != null && subTopicMaster.getId() != null && subTopicMaster.getId().getTopicId() != -1) {
					workSheet.setSubtopicId(subTopicMaster.getId().getTopicId());
				}
				
				workSheet.setStatus(AssessmentConstants.TestStatus.ASSIGNED);
				LOG.debug("parentTopicMaster............"+parentTopicMaster.getTopicName());
				LOG.debug("subTopicMaster............"+subTopicMaster.getTopicName());
				Skill skillObject = serviceLocator
						.getAssessmentDetailsService().getSkillBySkillName(
								skill);
				if (skillObject != null && skillObject.getId() != null
						&& skillObject.getId().getSkillId() > -1) {
					LOG.debug("skillObject------------------------"
							+ skillObject);
					LOG.debug("skillObject.getId().getSkillId()------------------------"
							+ skillObject.getId().getSkillId());
					workSheet.setSkillIdList(Short.toString(skillObject.getId()
							.getSkillId()));
					
					plpQuestionsList = serviceLocator.getPlpQuestionService()
							.getQuestions(classMaster.getClassId(),
									subjectMaster.getSubjectId(),
									skillObject.getId().getSkillId(),workSheet.getSubtopicId(), workSheet.getTopicId());
					System.out.println("plpQuestionsList.size()------------------------>"+plpQuestionsList.size());
					LOG.debug("plpQuestionsList.size()------------------------>"+plpQuestionsList.size());
				}

				String questionIdsList = null;
				if (plpQuestionsList != null && !plpQuestionsList.isEmpty()
						&& plpQuestionsList.size() > 0) {
					questionIdsList = getQuestionIdsList(plpQuestionsList);
					System.out.println("questionIdsList------------------------>"+questionIdsList);
					LOG.debug("questionIdsList------------------------>"+questionIdsList);
				}
				workSheet.setQuestionIdList(questionIdsList);

				workSheet.setIsDeleted(new Byte("0"));
				workSheet.setTopN(new Byte("10"));

				if (subjectMaster != null && subTopicMaster != null
						&& parentTopicMaster != null && classMaster != null
						&& questionIdsList != null) {
					System.out.println("Before Saving worksheet.....................");
					try {
						serviceLocator.getPlpStudentWorkSheetService()
								.savePLPStudentWorkSheets(workSheet);
						LOG.debug("Worksheet saved");
					} catch (HibernateException e) {
						LOG.debug("Exception While Saving PLPStudentWorkSheet");
						e.printStackTrace();
					}
					System.out.println("After Saving worksheet.....................");
				}
			}
		}
	}
	
	/**
	 * Resourece Mapping  to view EOC TEsts.
	 * 
	 * @param topicId
	 * @param resourceRequest
	 * @param resourceResponse
	 * @return 
	 * @throws IOException
	 */
	@ResourceMapping("viewEOCTests")
    public ModelAndView viewEOCTests(@RequestParam String topicName, ResourceRequest resourceRequest,
                    ResourceResponse resourceResponse)
                    throws IOException {
            LOG.debug("@ResourceMapping viewEOCTests invoked");
            LOG.debug("topicName: " + topicName);
            final String subjectName = (String) resourceRequest.getPortletSession()
            				.getAttribute("topics_assetCatName", PortletSession.APPLICATION_SCOPE);
        	LOG.debug("subjectName from session: " + subjectName);
        	
        	List<PLPStudentWrokSheet> wsList = null;
        	Map<String, Object> model = new HashMap<String, Object>();
        	model.put("IsEOCTest", Boolean.TRUE);
        	if (subjectName != null) {
	        
        		wsList = createEOCTestByTopicId(resourceRequest, subjectName, topicName);
        		if (wsList != null) {
        			System.out.println("wsList in view EOC Tests...................."+wsList.size());
        		}

	    		model.put("wsList", wsList);
	    		resourceRequest.setAttribute("EOCTest", "EOCTest");
        	} else {
        		LOG.debug("subjectName from session is null.. cannot find EOC Test for " + topicName);
        	}

    		return new ModelAndView("../learning/"+VIEW_WORKSHEETS, model);
    }
	
	/**
	 * Helper Method to check to create worksheet for topicId.
	 * 
	 * @param eventRequest
	 * @param subjectName
	 * @param topicId
	 * @param level2ChildName
	 * @param level1ChildName
	 */
	private List<PLPStudentWrokSheet> createEOCTestByTopicId(ResourceRequest eventRequest, String subjectName, String topicName) {
		PLPTopicMaster topicMaster = null;
		List<PLPStudentWrokSheet> wsList = null;
		ThemeDisplay themedisplay = WebUtil.getThemeDisplay(eventRequest);
		ClassMaster classMaster = null;
		SubjectMaster subjectMaster = null;
		try {
			final String className = (String) eventRequest.getPortletSession().getAttribute("subject_className", PortletSession.APPLICATION_SCOPE);
			LOG.debug("className from session: " + className);
			//User user = serviceLocator.getUserService().findUserByUserId(themedisplay.getCompanyId(), themedisplay.getUserId());
			if(className != null) {
				classMaster = serviceLocator.getAssessmentService().getClassMasterByClassName(className);
			}
		} catch (HibernateException e) {
			LOG.error("Hibernate Exception Caught While fetching class master in Learing Controller", e);
			e.printStackTrace();
		}
		
		if(subjectName != null) {
			try {
					subjectMaster = serviceLocator.getAssessmentService().getSubjectMasterbySbujectName(subjectName);
			} catch (HibernateException e) {
				LOG.error("Hibernate Exception Caught While fetching subject master in Learing Controller", e);
				e.printStackTrace();
			}
		}
		
		if(topicName != null && classMaster != null && classMaster.getClassId() != null && subjectMaster != null && subjectMaster.getSubjectId() != null){
			try {
				topicMaster = serviceLocator.getTopicMasterService().getTopicMasterByTopicName(topicName, classMaster.getClassId(), subjectMaster.getSubjectId());
			} catch (HibernateException e) {
				LOG.error("Hibernate Exception Caught While Fetching topicMaster in LearningController.createWorkSheetByTopicId()", e);
				e.printStackTrace();
			}
		}
		
		if (topicMaster != null && topicMaster.getId().getTopicId() != -1) {
			LOG.debug("@EventMapping fetchTopicId() Method in LearningController -- topicId----------"
					+ topicMaster.getId().getTopicId());
			try {
				wsList = serviceLocator.getWorkSheetService().getEOCTestsByTopicId(topicMaster.getId().getTopicId(), themedisplay.getUserId(), 
						classMaster.getClassId(), subjectMaster.getSubjectId());
			} catch (HibernateException e) {
				LOG.error("HibernateException Caught While fetching wsList----", e);
				e.printStackTrace();
			}
			
		}
		
		if(wsList != null && !wsList.isEmpty() && wsList.size() > 0) {
			LOG.debug("wsList.size() from Learnign Controller---------------"+wsList.size());
			//eventRequest.getPortletSession().setAttribute("wsList", wsList);
		} else if (topicMaster != null && topicMaster.getId().getTopicId() != -1) {
			try {
				LOG.debug("Before create PLP work sheet.....................................");
				createEOCTest(eventRequest, subjectName, topicName, classMaster.getClassId(), subjectMaster.getSubjectId());
				wsList = serviceLocator.getWorkSheetService().getEOCTestsByTopicId(topicMaster.getId().getTopicId(), themedisplay.getUserId(),
						classMaster.getClassId(), subjectMaster.getSubjectId());
				System.out.println("wsList............."+wsList.size());
				if(wsList != null && !wsList.isEmpty() && wsList.size() > 0) {
					LOG.debug("wsList.size() from Learnign Controller---------------"+wsList.size());
					//eventRequest.getPortletSession().setAttribute("wsList", wsList);
					
				}
				
			} catch (HibernateException e) {
				LOG.debug("Exception Caught while either saving plpStudentWorkSheet or retrieving plpStudentWorkSheet.");
				e.printStackTrace();
			}

		}
		
		return wsList;
	}


	/**
	 * Helper Method to create workSheet for topicId.
	 * 
	 * @param eventRequest
	 * @param subjectName2
	 * @param level1ChildName
	 */
	private void createEOCTest(PortletRequest portletRequest, String subjectName, String parentTopicId, Short classId, Short subjectId) {
		LOG.debug("createPLPStudentWorkSheet() Method Start in Learning Controller");
		List<String> skillNames = new ArrayList<String>();
		SubjectMaster subjectMaster = null;
		PLPTopicMaster parentTopicMaster = null;
		PLPTopicMaster subTopicMaster = null;
		ClassMaster classMaster = null;
		List<DTAssessment> dtAssessmentsList = null;
		User user = null;

		try {
			subjectMaster = serviceLocator.getAssessmentService()
					.getSubjectMasterbySbujectName(subjectName);
		} catch (HibernateException e) {
			LOG.error(
					"Hibernate Exception Caught While Fetching subjectMaster Object",
					e);
			e.printStackTrace();
		}

		if (parentTopicId != null) {
			try {
				parentTopicMaster = serviceLocator.getTopicMasterService()
						.getTopicMasterByTopicName(parentTopicId, classId, subjectId);
			} catch (HibernateException e) {
				LOG.error(
						"Hibernate Exception Caught While Fetching topicMaster Object",
						e);
				e.printStackTrace();
			}
		}

		if (classId != null) {
			try {
				classMaster = serviceLocator.getAssessmentService()
						.getClassMasterByClassId(classId);
			} catch (HibernateException e) {
				LOG.error(
						"Hibernate Exception Caught While Fetching classMaster Object",
						e);
				e.printStackTrace();
			}
		}
		LOG.debug("classMaster:--------" + classMaster);
		LOG.debug("subjectMaster:--------" + subjectMaster);

		try {
			user = WebUtil.getUser(portletRequest);
		} catch (HibernateException e) {
			LOG.error("Hibernate Exception Caught While Fetching User Object",
					e);
			e.printStackTrace();
		}

		if (subjectMaster != null && subjectMaster.getSubjectId() != null
				&& user != null) {
			try {

				dtAssessmentsList = getDTAssessmentByUserId(user.getUserId(),
						parentTopicMaster.getId().getClassId(),
						subjectMaster.getSubjectId(),
						AssessmentConstants.TEST_TYPE_DIAGNOSTIC_TEST);
			} catch (HibernateException e) {
				LOG.debug(
						"Hibernate Exception Caught While Fetching dtAssessmentsList",
						e);
				e.printStackTrace();
			}
			if (dtAssessmentsList != null && !dtAssessmentsList.isEmpty()
					&& dtAssessmentsList.size() > 0) {
				LOG.debug("dtAssessmentsList.size().........."+dtAssessmentsList.size());
				
				DTAssessment assessment = dtAssessmentsList.get(0);
				LOG.debug("assessment.getAssessmentId().........."+assessment.getAssessmentId());
				skillNames = processDTAssessmentsList(assessment
						.getAssessmentId());
				LOG.debug("skillNames Size.........."+skillNames.size());
			}
		}

		List<PLPQuestion> plpQuestionsList = null;
		if (skillNames != null && !skillNames.isEmpty()
				&& skillNames.size() > 0) {
			PLPStudentWrokSheet workSheet = null;
			for (String skill : skillNames) {
				// create worksheet
				workSheet = new PLPStudentWrokSheet();
				workSheet.setCreatedDate(new java.util.Date());
				workSheet
						.setEndDate(new java.util.Date(workSheet
								.getCreatedDate().getTime()
								+ (7 * 24 * 60 * 60 * 1000)));
				workSheet.setStartDate(new java.util.Date());
				workSheet.setUserId(WebUtil.getUserId(portletRequest));
				workSheet.setSubjectMaster(subjectMaster);
				workSheet.setClassMaster(classMaster);
				workSheet.setSubtopicId(null);
				if(parentTopicMaster != null && parentTopicMaster.getId() != null && parentTopicMaster.getId().getTopicId() != -1) {
					workSheet.setTopicId(parentTopicMaster.getId().getTopicId());
				}
				workSheet.setStatus(AssessmentConstants.TestStatus.ASSIGNED);
				LOG.debug("parentTopicMaster............"+parentTopicMaster);
				LOG.debug("subTopicMaster............"+subTopicMaster);
				LOG.debug("parentTopicMaster.getId().getClassId()............"+parentTopicMaster.getId().getClassId());
				LOG.debug("subjectMaster Name............"+subjectMaster.getSubjectName() + ".....subjectMaster Id ..........."+ subjectMaster.getSubjectId());
				LOG.debug("classMaster Name............"+classMaster.getClassName() + ".....classMaster Id ..........."+ classMaster.getClassId());
				Skill skillObject = serviceLocator
						.getAssessmentDetailsService().getSkillBySkillName(
								skill);
				if (skillObject != null && skillObject.getId() != null
						&& skillObject.getId().getSkillId() > -1) {
					LOG.debug("skillObject------------------------"
							+ skillObject);
					LOG.debug("skillObject.getId().getSkillId()------------------------"
							+ skillObject.getId().getSkillId());
					workSheet.setSkillIdList(Short.toString(skillObject.getId()
							.getSkillId()));
					plpQuestionsList = serviceLocator.getPlpQuestionService()
							.getQuestions(classMaster.getClassId(),
									subjectMaster.getSubjectId(),
									skillObject.getId().getSkillId(), workSheet.getTopicId());
				}

				String questionIdsList = null;
				if (plpQuestionsList != null && !plpQuestionsList.isEmpty()
						&& plpQuestionsList.size() > 0) {
					questionIdsList = getQuestionIdsList(plpQuestionsList);
				}
				System.out.println("plpQuestionsList.size()................"+plpQuestionsList.size());
				workSheet.setQuestionIdList(questionIdsList);

				workSheet.setIsDeleted(new Byte("0"));
				workSheet.setTopN(new Byte("10"));

				System.out.println("Before saving worksheets...................");
				if (subjectMaster != null /*&& subTopicMaster != null*/
						&& parentTopicMaster != null && classMaster != null
						&& questionIdsList != null && plpQuestionsList != null
						&& plpQuestionsList.size() > 0) {
					try {
						serviceLocator.getPlpStudentWorkSheetService()
								.savePLPStudentWorkSheets(workSheet);
						System.out.println("After Saving Worksheets................"+plpQuestionsList.size());
					} catch (HibernateException e) {
						LOG.debug("Exception While Saving PLPStudentWorkSheet");
						e.printStackTrace();
					}
				}
				System.out.println("Finish Worksheets................****************************************************************************");
			}
		}
	
	}


	/**
	 * Action Mapping to show worksheets.
	 * 
	 * @param actionRequest
	 * @param actionResponse
	 */
	@ActionMapping(params = "myaction=showWorkSheets")
	public void showWorkSheets(ActionRequest actionRequest,
			ActionResponse actionResponse) {
		LOG.debug("action mapping to show worksheets");
		LOG.debug("showWorkSheets() Method Start in LearningController");
		actionResponse.setRenderParameter("myaction", "viewWorkSheets");
	}
	

	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@ResourceMapping("viewWorkSheets")
    public ModelAndView showWorkSheets(@RequestParam Integer subTopicId, ResourceRequest request, ResourceResponse response) throws IOException {
		LOG.debug("subTopicId -- view worksheets resource---------------------"+subTopicId);
    	LOG.debug("START");
    	Map<String, List<PLPStudentWrokSheet>> model = new HashMap<String, List<PLPStudentWrokSheet>>();
    	List<PLPStudentWrokSheet> wsList = null;
    	ThemeDisplay themeDisplay = WebUtil.getThemeDisplay(request);
    	ClassMaster classMaster = null;
    	SubjectMaster subjectMaster = null;
		try {
//			User user = serviceLocator.getUserService().findUserByUserId(themeDisplay.getCompanyId(), themeDisplay.getUserId());
			final String className = (String) request.getPortletSession().getAttribute("subject_className", PortletSession.APPLICATION_SCOPE);
			LOG.debug("className from session: " + className);
			if(className != null) {
				classMaster = serviceLocator.getAssessmentService().getClassMasterByClassName(className);
			}

		} catch (HibernateException e) {
			LOG.error("Hibernate Exception Caught While fetching class master in Learing Controller", e);
			e.printStackTrace();
		}
		final String subjectName = (String) request.getPortletSession().getAttribute("topics_assetCatName", PortletSession.APPLICATION_SCOPE);
		//need  to get subjectname.
		if(subjectName != null) {
			try {
					subjectMaster = serviceLocator.getAssessmentService().getSubjectMasterbySbujectName(subjectName);
			} catch (HibernateException e) {
				LOG.error("Hibernate Exception Caught While fetching subject master in Learing Controller", e);
				e.printStackTrace();
			}
		}
	
		
    	try {
			wsList = serviceLocator.getWorkSheetService().getWorkSheetsBySubTopicId(subTopicId, themeDisplay.getUserId(),
					classMaster.getClassId(), subjectMaster.getSubjectId());
		} catch (HibernateException e) {
			LOG.error("Hibernate Exception Caught While Fetching wsList in ShowWorkSheets resource --", e);
		}
    	
    	model.put("wsList", wsList);
       	// to redirect to viewWorkSheets render method.
    	return new ModelAndView(VIEW_WORKSHEETS, model);
    	
    }
	
	
	/**
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@ResourceMapping("viewTopicWorkSheets")
    public ModelAndView showTopicWorkSheets(@RequestParam Integer parentTopicId, ResourceRequest request, ResourceResponse response) throws IOException {
		LOG.debug("parentTopicId -- view worksheets resource---------------------"+parentTopicId);
    	LOG.debug("START");
    	Map<String, List<PLPStudentWrokSheet>> model = new HashMap<String, List<PLPStudentWrokSheet>>();
    	List<PLPStudentWrokSheet> wsList = null;
    	ThemeDisplay themeDisplay = WebUtil.getThemeDisplay(request);
    	
    	ClassMaster classMaster = null;
    	SubjectMaster subjectMaster = null;
		try {
//			User user = serviceLocator.getUserService().findUserByUserId(themeDisplay.getCompanyId(), themeDisplay.getUserId());
			final String className = (String) request.getPortletSession().getAttribute("subject_className", PortletSession.APPLICATION_SCOPE);
			LOG.debug("className from session: " + className);
			if(className != null) {
				classMaster = serviceLocator.getAssessmentService().getClassMasterByClassName(className);
			}
		} catch (HibernateException e) {
			LOG.error("Hibernate Exception Caught While fetching class master in Learing Controller", e);
			e.printStackTrace();
		}
		//need  to get subjectname.
		final String subjectName = (String) request.getPortletSession().getAttribute("topics_assetCatName", PortletSession.APPLICATION_SCOPE);
		if(subjectName != null) {
			try {
					subjectMaster = serviceLocator.getAssessmentService().getSubjectMasterbySbujectName(subjectName);
			} catch (HibernateException e) {
				LOG.error("Hibernate Exception Caught While fetching subject master in Learing Controller", e);
				e.printStackTrace();
			}
		}
    	try {
			wsList = serviceLocator.getWorkSheetService().getEOCTestsByTopicId(parentTopicId, themeDisplay.getUserId(), 
					classMaster.getClassId(), subjectMaster.getSubjectId());
		} catch (HibernateException e) {
			LOG.error("Hibernate Exception Caught While Fetching wsList in ShowWorkSheets resource --", e);
			e.printStackTrace();
		}
    	
    	model.put("wsList", wsList);
       	// to redirect to viewWorkSheets render method.
    	return new ModelAndView(VIEW_WORKSHEETS, model);
    	
    }


	/**
	 * Render Mapping to view worksheets.
	 * 
	 * @param renderRequest
	 * @param renderResponse
	 * @return
	 */
	@RenderMapping(params = "myaction=viewWorkSheets")
	public String viewWorkSheets(RenderRequest renderRequest,
			RenderResponse renderResponse) {
		LOG.debug("render mapping to view worksheets");
		LOG.debug("viewWorkSheets() Method Start in LearningController");
		return VIEW_WORKSHEETS;

	}

	
	/**
	 * 
	 * @param dtAssessmentsList
	 */
	private List<String> processDTAssessmentsList(Integer assessmentId) {
		// To DO : Need to process subjectId.
		LOG.debug("processDTAssessmentsList() Method Start in Learning Controller");
		Map<Short, SkillWiseAssessmentReport> assessmentReport = this.serviceLocator
				.getAssessmentDetailsService()
				.getAssessmentReport(assessmentId);
		Set<Short> skillIds = assessmentReport.keySet();
		List<Float> percents = new ArrayList<Float>();
		List<String> skillNames = new ArrayList<String>();
		Map<List<String>, List<Float>> skillAndPercentileMap = new HashMap<List<String>, List<Float>>();

		for (Short skillId : skillIds) {
			SkillWiseAssessmentReport skillReport = assessmentReport
					.get(skillId);
			if (skillReport.getPercentage() < AssessmentConstants.DTTEST_PERCENTAGE) {
				percents.add(skillReport.getPercentage());
				// skillNames.add("\'" + skillReport.getSkillName() + "\'");
				skillNames.add(skillReport.getSkillName());
			}
		}
		LOG.debug("percents: " + percents);
		LOG.debug("skillNames: " + skillNames);

		skillAndPercentileMap.put(skillNames, percents);
		// model.put("percents", percents);
		// model.put("skillNames", skillNames);

		LOG.debug("END");
		return skillNames;

	}

	/**
	 * Method to fetch dtAssessments list.
	 * 
	 * @param userId
	 * @return
	 */
	private List<DTAssessment> getDTAssessmentByUserId(long userId, Short classId,
			Short subjectId, Short diagnosisType) {
		LOG.debug("getDTAssessmentByUserId() Method Start in Learning Controller");
		return serviceLocator.getAssessmentService().getAssessments(
				WebUtil.convertLongToInt(userId), classId, subjectId, diagnosisType);
	}

	/**
	 * Method to fetch user assessment diagosis test status.
	 * 
	 * @param request
	 * @return
	 */
	public int assessmentStatus(PortletRequest request) {
		LOG.debug("START");
		// fetch assessments
		List<DTAssessment> assessments = this.serviceLocator
				.getAssessmentService().getAssessments(
						(int) WebUtil.getUserId(request),
						AssessmentConstants.TEST_TYPE_DIAGNOSTIC_TEST);
		int percentCompleted = 0;
		if (assessments != null) {
			LOG.debug("assessments: " + assessments.size());
			int completedCount = 0;
			if (!assessments.isEmpty()) {
				for (DTAssessment assessment : assessments) {
					if (AssessmentConstants.TestStatus.COMPLETED
							.equalsIgnoreCase(assessment.getStatus())) {
						completedCount++;
					}
				}
				percentCompleted = (completedCount / assessments.size()) * 100;
			}
		}
		LOG.debug("percentCompleted: " + percentCompleted);
		/*
		 * Map<String, Object> model = new HashMap<String, Object>();
		 * model.put("percent", percentCompleted); model.put("status",
		 * "Completed");
		 */

		LOG.debug("END");
		return percentCompleted;
	}

	/**
	 * Method to get questionsIds separated by comma for worksheet.
	 * 
	 * @param plpQuestionsList
	 */
	private String getQuestionIdsList(List<PLPQuestion> plpQuestionsList) {
		String questionIdsList = null;
		int count = 0;
		for (PLPQuestion question : plpQuestionsList) {
			if (question != null && question.getQuestionId() != null) {
				if (count >= 1) {
					questionIdsList = questionIdsList + ","
							+ question.getQuestionId().toString();
				} else {
					questionIdsList = question.getQuestionId().toString();
				}
				count++;
			}
		}
		LOG.debug("questionIdsList------------String--->" + questionIdsList);
//		System.out.println("questionIds------------String----------->" + questionIdsList);
		
		return questionIdsList;
	}

	/*
	 * @RenderMapping public ModelAndView showContent(RenderRequest renderReq,
	 * RenderResponse renderResponse) {
	 * LOG.debug("@RenderMapping showContent invoked");
	 * 
	 * //return "showContentList"; }
	 */
	private Map<String, List<AssetDetailsPojo>> populateAssestsBasedOnCategory(
			PortletRequest portletRequest, long categoryId) {

		System.out.println("...........................populateAssetsbasedOnCategory Start.........................................");
		Map<String, List<AssetDetailsPojo>> assetEntriesMap = null;
		List<AssetCategory> listOfChild3Categories = WebUtil
				.getChildCategories(categoryId, 0);

//		System.out.println("listOfChild3Categories size:..........."+listOfChild3Categories.size());
		LOG.debug("getAssetEntryMap invoked : " + listOfChild3Categories);
		if (null != listOfChild3Categories) {
			assetEntriesMap = new HashMap<String, List<AssetDetailsPojo>>();
			List<AssetDetailsPojo> tempList = null;
			for (Iterator<AssetCategory> it = listOfChild3Categories.iterator(); it
					.hasNext();) {
				AssetCategory assetCategory = it.next();
				LOG.debug("child3: " + assetCategory.getName());
//				System.out.println("child3:..........."+assetCategory.getName());

				tempList = buildAssestsList(assetCategory.getCategoryId(),
						portletRequest, assetCategory.getName());
//				System.out.println("tempList size:..........."+tempList);
				LOG.debug("VIDEOs size: " + tempList);
				assetEntriesMap.put(assetCategory.getName(), tempList);

			}
		}
		LOG.debug("assetEntriesMap populateAssestsBasedOnCategory"
				+ assetEntriesMap);
		assetEntryMap = assetEntriesMap;
//		System.out.println("...........................populateAssetsbasedOnCategory End.........................................");
		return assetEntriesMap;
	}

	public List<AssetDetailsPojo> buildAssestsList(long categoryId,
			PortletRequest portletRequest,String assetType) {
		LOG.debug("assetType assetType :" + assetType);
//		System.out.println("assetType assetType :....." + assetType);
		
		List<AssetEntry> allAssetEntryListTemp = WebUtil.fetchAssetEntries(
				categoryId, portletRequest);
//		System.out.println("allAssetEntryListTemp Size :....." + allAssetEntryListTemp.size());
		allAssetEntriesDetailsList = new ArrayList<AssetDetailsPojo>();
		AssetDetailsPojo assetDetailsPojo = null;
		LOG.debug(" allAssetEntryListTemp :" + allAssetEntryListTemp);
		try {
			// Set<String> extensionTypeSet = new HashSet<String>();
			for (AssetEntry assestEntry : allAssetEntryListTemp) {
				assetDetailsPojo = new AssetDetailsPojo();
				DLFileEntry dlFileEntry = null;
				if (null != assestEntry) {
					LOG.debug("assestEntry : " + assestEntry);
//					System.out.println("assestEntry.........."+assestEntry.getTitle());

					assetDetailsPojo.setClassPK(assestEntry.getClassPK());
					assetDetailsPojo.setCompanyId(assestEntry.getCompanyId());
					assetDetailsPojo.setDescription(assestEntry
							.getDescription());
					assetDetailsPojo.setEntryId(assestEntry.getEntryId());
					assetDetailsPojo.setGroupId(assestEntry.getGroupId());
					assetDetailsPojo.setHeight(assestEntry.getHeight());
					assetDetailsPojo.setMimeType(assestEntry.getMimeType());
					assetDetailsPojo.setTitle(assestEntry.getTitle());
					assetDetailsPojo.setUserId(assestEntry.getUserId());
					assetDetailsPojo.setWidth(assestEntry.getWidth());

					// get DLFileEntry object
					dlFileEntry = WebUtil.fetchDLFileEntry(assestEntry
							.getClassPK());
					
					
				} else {
					LOG.debug("assestEntry : " + assestEntry);

				}
				// set some values from DLFileEntry object
				if (null != dlFileEntry) {
					LOG.debug("dlFileEntry : " + dlFileEntry);

					assetDetailsPojo.setExtension(dlFileEntry.getExtension());
					assetDetailsPojo.setFileEntryId(dlFileEntry
							.getFileEntryId());
					assetDetailsPojo.setFolderId(dlFileEntry.getFolderId());
					assetDetailsPojo.setUserId(dlFileEntry.getUserId());
					assetDetailsPojo.setSize(dlFileEntry.getSize());
				} else {
					LOG.debug("dlFileEntry : " + dlFileEntry);
				}
				LOG.debug("assetDetailsPojo.getExtension() :"
						+ assetDetailsPojo.getExtension());

				assetDetailsPojo.setAssetType(assetType);

				allAssetEntriesDetailsList.add(assetDetailsPojo);

				// set file extension to java.util.Set
				// to identity which types of docs uploaded for this category
				// String matchedGroupExtension =
				// getMatchedExtensionGroupOrLabel(assetDetailsPojo.getExtension(),true);
				// LOG.debug("Extension() :"+assetDetailsPojo.getExtension()+" :matchedGroupExtension: "+matchedGroupExtension);
				// extensionTypeSet.add(matchedGroupExtension);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return allAssetEntriesDetailsList;
	}

	long longCategoryId = 0;

	public void callOnDefaultLoading(String subjectId) {
		LOG.debug("callOnSubjectEvent:");
		defaultLoading = VALUE_TRUE;
		long vocabularyId = 0L;

		// ////////////////////////
		LOG.debug("---------------START---------------------");
		List<AssetCategory> rootCategoryList = null;
		rootCategoryList = new ArrayList<AssetCategory>();
		AssetCategory assetCategory = WebUtil.getAssetCategoryById(Long
				.parseLong(subjectId));
		LOG.debug("assetCategoryName :Topics: " + assetCategory.getName());
		rootCategoryList.add(assetCategory);
		rootCategoryList = WebUtil.removeDuplicates(rootCategoryList);

		LOG.debug("rootCategoryList : " + rootCategoryList);

		Map<String, List<AssetDetailsPojo>> assetEntriesMap = null;
		for (AssetCategory assetCategory2 : rootCategoryList) {
			long catId = assetCategory2.getCategoryId();
			long vocabId = assetCategory2.getVocabularyId();
			List<AssetCategory> subjectList = WebUtil.getChildCategories(catId,
					vocabId);
			if (null != subjectList && subjectList.size() > 0) {
				for (int i = 0; i < subjectList.size(); i++) {

					AssetCategory level1Child = subjectList.get(i);
					LOG.debug("level1Child: @ " + i + " "
							+ level1Child.getName());

					if ("Overview".equalsIgnoreCase(level1Child.getName())) {
						LOG.debug("overview found:");
						assetEntriesMap = populateAssestsBasedOnCategory(null,
								level1Child.getCategoryId());
						LOG.debug("assetEntriesMap : " + assetEntriesMap);

						break;
					}
				}
			}
		}
		LOG.debug("---------------END--------------------" + assetEntriesMap);
		assetEntryMap = assetEntriesMap;

		// /////////////////////

	}

}
