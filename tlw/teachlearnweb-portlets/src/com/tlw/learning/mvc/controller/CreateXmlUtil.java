package com.tlw.learning.mvc.controller;
import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
 
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
 
public class CreateXmlUtil {
 
	public static void main(String s[]){		
		CreateXmlUtil util =new CreateXmlUtil();
		util.createViewerXml();		
	}
	
	public CreateXmlUtil(){
		
	}
	
	public void  createViewerXml() {
		
		try {
 
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
		Attr attr = null;
 
		// root elements
		Document doc = docBuilder.newDocument();
		Element rootElement = doc.createElement("presentation");
		doc.appendChild(rootElement);
		
		// set attribute to presentation element
		attr = doc.createAttribute("width");
		attr.setValue("720");
		rootElement.setAttributeNode(attr);
 
		attr = doc.createAttribute("height");
		attr.setValue("540");
		rootElement.setAttributeNode(attr);
 
		attr = doc.createAttribute("defaultSlideDuration");
		attr.setValue("5");
		rootElement.setAttributeNode(attr);
		
		attr = doc.createAttribute("start");
		attr.setValue("play");
		rootElement.setAttributeNode(attr);
		
		attr = doc.createAttribute("buildNum");
		attr.setValue("7.0.7.7746");
		rootElement.setAttributeNode(attr);
				
 
		//presentationTitle
		Element presentationTitle = doc.createElement("presentationTitle");
		presentationTitle.appendChild(doc.createTextNode("PL360"));
		rootElement.appendChild(presentationTitle);
 
		//speakers
		Element speakers = doc.createElement("speakers");
		rootElement.appendChild(speakers);
		
		//search
		Element search = doc.createElement("search");
		attr = doc.createAttribute("url");
		attr.setValue("srchdata.xml");
		search.setAttributeNode(attr); 
		rootElement.appendChild(search);
		
		//presentationBg
		Element presentationBg = doc.createElement("presentationBg");
		attr = doc.createAttribute("url");
		attr.setValue("bg.swf");
		presentationBg.setAttributeNode(attr); 
		rootElement.appendChild(presentationBg);
		
		// MASTERS
		Element masters = doc.createElement("masters");
			Element master = doc.createElement("master");
			attr = doc.createAttribute("url");
			attr.setValue("Master0.swf");
			master.setAttributeNode(attr); 
		
			attr = doc.createAttribute("id");
			attr.setValue("1");
			master.setAttributeNode(attr); 
		
			attr = doc.createAttribute("totalFrames");
			attr.setValue("1");
			master.setAttributeNode(attr);
			
			Element backdrop = doc.createElement("backdrop");
			attr = doc.createAttribute("frameNum");
			attr.setValue("0");
			backdrop.setAttributeNode(attr); 
			master.appendChild(backdrop);
			
			masters.appendChild(master);		
		rootElement.appendChild(masters);
		
		// LAYOUTS
		Element layouts = doc.createElement("layouts");
		Element layout0 = doc.createElement("layout");
		attr = doc.createAttribute("url");
		attr.setValue("Layout0.swf");
		layout0.setAttributeNode(attr); 
	
		attr = doc.createAttribute("id");
		attr.setValue("1");
		layout0.setAttributeNode(attr); 
	
		attr = doc.createAttribute("totalFrames");
		attr.setValue("1");
		layout0.setAttributeNode(attr);	
		
		backdrop = doc.createElement("backdrop");
		attr = doc.createAttribute("displayBgSp");
		attr.setValue("true");
		backdrop.setAttributeNode(attr); 
		layout0.appendChild(backdrop);
		
		layouts.appendChild(layout0);
	
		// layout2
		Element layout1 = doc.createElement("layout");
		attr = doc.createAttribute("url");
		attr.setValue("Layout1.swf");
		layout1.setAttributeNode(attr); 
	
		attr = doc.createAttribute("id");
		attr.setValue("2");
		layout1.setAttributeNode(attr); 
	
		attr = doc.createAttribute("totalFrames");
		attr.setValue("1");
		layout1.setAttributeNode(attr);	
		
		backdrop = doc.createElement("backdrop");
		attr = doc.createAttribute("displayBgSp");
		attr.setValue("true");
		backdrop.setAttributeNode(attr); 
		layout1.appendChild(backdrop);
		
		layouts.appendChild(layout1);	
		
	rootElement.appendChild(layouts);
	
	
	// SLIDES
	Element slides = doc.createElement("slides");
	
	for (int i = 0; i < 5; i++) {
		
		Element slide = doc.createElement("slide");
	
		attr = doc.createAttribute("id");
		attr.setValue(i+"");
		slide.setAttributeNode(attr); 
	
		attr = doc.createAttribute("showInTOC");
		attr.setValue("");
		slide.setAttributeNode(attr); 
	
		attr = doc.createAttribute("inSequence");
		attr.setValue("");
		slide.setAttributeNode(attr); 
		
		attr = doc.createAttribute("frameRate");
		attr.setValue("30");
		slide.setAttributeNode(attr); 
	
		attr = doc.createAttribute("totalFrames");
		attr.setValue("1");
		slide.setAttributeNode(attr); 
	
		attr = doc.createAttribute("swfConvertorEngine");
		attr.setValue("Nova");
		slide.setAttributeNode(attr); 
	
		attr = doc.createAttribute("slideDuration");
		attr.setValue("5");
		slide.setAttributeNode(attr); 
	
		attr = doc.createAttribute("advance");
		attr.setValue("auto");
		slide.setAttributeNode(attr); 
	
		attr = doc.createAttribute("blockMovement");
		attr.setValue("false");
		slide.setAttributeNode(attr); 
			
		attr = doc.createAttribute("type");
		attr.setValue("normal");
		slide.setAttributeNode(attr);
		
		// inner tags for slide
		// content
		Element content = doc.createElement("content");
		attr = doc.createAttribute("url");
		attr.setValue("Slide0.swf");
		content.setAttributeNode(attr); 
		
		attr = doc.createAttribute("id");
		attr.setValue(i+"");
		content.setAttributeNode(attr);		
		
		slide.appendChild(content);
		
		// backdrop
		Element backdrop1 = doc.createElement("backdrop");
		attr = doc.createAttribute("displayBgSp");
		attr.setValue("true");
		backdrop1.setAttributeNode(attr); 		
		slide.appendChild(backdrop1);
		
		// master
		Element master1 = doc.createElement("master");
		attr = doc.createAttribute("id");
		attr.setValue("1");
		master1.setAttributeNode(attr);
		
		attr = doc.createAttribute("frameNum");
		attr.setValue("0");
		master1.setAttributeNode(attr); 		
		
		
		slide.appendChild(master1);
		
		// backdrop
		Element layout = doc.createElement("layout");
		attr = doc.createAttribute("id");
		attr.setValue("1");
		layout.setAttributeNode(attr); 	
		
		attr = doc.createAttribute("frameNum");
		attr.setValue("0");
		layout.setAttributeNode(attr); 		
		
		slide.appendChild(layout);
		
		// clickAnimations
		Element clickAnimations = doc.createElement("clickAnimations");
		
		for (int j = 0; j < 3; j++) {
			
			Element click = doc.createElement("click");
		
			attr = doc.createAttribute("startTime");
			attr.setValue("167");
			click.setAttributeNode(attr); 
		
			attr = doc.createAttribute("isStartRelative");
			attr.setValue("false");
			click.setAttributeNode(attr); 
			
			
			attr = doc.createAttribute("duration");
			attr.setValue("2670");
			click.setAttributeNode(attr); 
			
			clickAnimations.appendChild(click);
			
		}
		
		slide.appendChild(clickAnimations);
		
		//flvContents
		Element flvContents = doc.createElement("flvContents");
		slide.appendChild(flvContents);
		
		//slideTitle
		Element slideTitle = doc.createElement("slideTitle");
		slideTitle.appendChild(doc.createTextNode("ConedTec"));
		slide.appendChild(slideTitle);
 

		// notes
		Element notes = doc.createElement("notes");
		
		attr = doc.createAttribute("isHTML");
		attr.setValue("true");
		notes.setAttributeNode(attr); 
		
		notes.appendChild(doc.createTextNode("&lt;br&gt;"));
		slide.appendChild(notes);
 
		
		// slideThumbnail
		Element slideThumbnail = doc.createElement("slideThumbnail");
		
		attr = doc.createAttribute("url");
		attr.setValue("thumb.swf");
		slideThumbnail.setAttributeNode(attr); 
		
		attr = doc.createAttribute("frame");
		attr.setValue("2");
		slideThumbnail.setAttributeNode(attr); 

		slide.appendChild(slideThumbnail);
 
		slides.appendChild(slide);		
	}
	rootElement.appendChild(slides);
	
 /*

		// firstname elements
		Element firstname = doc.createElement("firstname");
		firstname.appendChild(doc.createTextNode("yong"));
		staff.appendChild(firstname);
 
		// lastname elements
		Element lastname = doc.createElement("lastname");
		lastname.appendChild(doc.createTextNode("mook kim"));
		staff.appendChild(lastname);
 
		// nickname elements
		Element nickname = doc.createElement("nickname");
		nickname.appendChild(doc.createTextNode("mkyong"));
		staff.appendChild(nickname);
 
		// salary elements
		Element salary = doc.createElement("salary");
		salary.appendChild(doc.createTextNode("100000"));
		staff.appendChild(salary);
 
*/		// write the content into xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);
		
		File file = new File("F:/file.xml");
		try {
			file.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		StreamResult result = new StreamResult(new File("F:/file.xml"));
 
		// Output to console for testing
		// StreamResult result = new StreamResult(System.out);
 
		transformer.transform(source, result);
 
		System.out.println("File saved!");
 
	  } catch (ParserConfigurationException pce) {
		pce.printStackTrace();
	  } catch (TransformerException tfe) {
		tfe.printStackTrace();
	  }
	}
}