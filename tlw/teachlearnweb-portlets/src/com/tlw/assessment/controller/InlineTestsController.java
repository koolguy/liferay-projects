/*
 * This code is property of Teach Learn Web, Inc. Use, duplication and disclosure
 * in any form without permission of copyright holder is prohibited.
 *
 * (C) Copyright Teach Learn Web, Inc. 2012. All rights reserved.
 */
package com.tlw.assessment.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.RenderResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.teachlearnweb.dao.entity.DifficultyLevel;
import com.tlw.assessment.util.AssessmentUtil;
import com.tlw.assessment.vo.InlineTest;
import com.tlw.controller.AbstractBaseController;

/**
 * The Class is responsible to handle the assessment related requests.
 * @author Govinda
 */
@Controller("inlineTestsController")
@RequestMapping(value = "VIEW")
public class InlineTestsController extends AbstractBaseController {
    /** The logger. */
    private static final Logger logger = Logger.getLogger(InlineTestsController.class);
    private List<InlineTest> inlineTests = null;

    private Map<String, Object> getModel() {
    	Map<String, Object> model = new HashMap<String, Object>();
    	model.put("inlineTests", this.inlineTests);
    	return model;
    }

    /**
     * Show registration.
     * @param renderResponse
     *            the render response
     * @return the string
     */
    @RenderMapping
    public ModelAndView showInlineTests(RenderResponse renderResponse) {
    	logger.debug("START");
    	// need to get testId from request param here
//    	List<DifficultyLevel> levels = this.serviceLocator.getAssessmentService().getDifficultiesBySubject();
//    	this.inlineTests = AssessmentUtil.populateInlineTests(levels);
    	if (this.inlineTests != null) {
    		logger.debug("in show inlineTests: " + this.inlineTests.size());
    	}
    	logger.debug("END");
    	return new ModelAndView("inlineTests", getModel());
    }
}
