/*
 * This code is property of Teach Learn Web, Inc. Use, duplication and disclosure
 * in any form without permission of copyright holder is prohibited.
 *
 * (C) Copyright Teach Learn Web, Inc. 2012. All rights reserved.
 */
package com.tlw.assessment.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.teachlearnweb.dao.entity.DTAnswer;
import com.teachlearnweb.dao.entity.DTAssessmentDetails;
import com.teachlearnweb.dao.entity.SubjectMaster;
import com.tlw.assessment.util.IntegerEditor;
import com.tlw.assessment.vo.Assessment;
import com.tlw.assessment.vo.AssessmentConstants;
import com.tlw.assessment.vo.SkillWiseAssessmentReport;
import com.tlw.assessment.vo.StyleBasedAssessmentReport;
import com.tlw.controller.AbstractBaseController;
import com.tlw.util.WebUtil;

/**
 * The Class is responsible to handle the assessment related requests.
 * @author Govinda
 */
@Controller("testController")
@Scope("session")
@RequestMapping(value = "VIEW")
public class TestController extends AbstractBaseController {
    /** The logger. */
    private static final Logger logger = Logger.getLogger(TestController.class);
    private Assessment currentAssessment = null;
    private int currentQuestionNum = 1;
    private int answeredCount = 0;
    private long timeRemaining = 0;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Integer.class, new IntegerEditor());
		binder.setDisallowedFields(new String[] {"assessmentId", "answerId"});
	}

    private Map<String, Object> getModel() {
    	Map<String, Object> model = new HashMap<String, Object>();
    	if (this.currentAssessment != null) {
	    	model.put("timeRemaining", this.getTimeRemaining());
	    	model.put("currentQuestionNum", this.getCurrentQuestionNum());
	    	model.put("questionCount", this.getQuestionCount());
	    	model.put("currentQuestion", this.getCurrentQuestion());
	    	model.put("options", this.getOptions());
	    	model.put("answeredCount", this.answeredCount);
	    	model.put("unAnsweredCount", this.getQuestionCount() - this.answeredCount);
	    	model.put("assessmentRunning", Boolean.TRUE);
	    	model.put("questions", currentAssessment.getQuestions());
	    	model.put("assessmentId", currentAssessment.getAssessmentId());
	    	model.put("subjectAndTopic", currentAssessment.getSubjectName());
	    	model.put("testId", currentAssessment.getDtAssessment().getDTTest().getTestId());
    	} else {
    		model.put("noData", Boolean.TRUE);
    	}
    	return model;
    }
    @ResourceMapping("startAssessment")
    public ModelAndView startAssessment(@RequestParam Integer assessmentId, ResourceRequest request, ResourceResponse response) {
    	logger.debug("START");
    	// get the questions and other details with provided assessment id
    	currentAssessment = this.serviceLocator.getAssessmentDetailsService().getAssessment(assessmentId);
    	if (currentAssessment != null) {
	    	// reset the values
	    	currentQuestionNum = 1;
	    	this.timeRemaining = currentAssessment.getTimeAllotted() * 60;
	    	if (logger.isDebugEnabled()) {
	    		logger.debug("timeRemaining has been set to initial value>>" + timeRemaining);
	    	}
	    	// how many questions answered already
	    	this.answeredCount = 0;
	    	for (DTAssessmentDetails assessDetail : this.currentAssessment.getQuestions()) {
	    		if (assessDetail.getAnswer() != null) {
	    			this.answeredCount += 1;
	    		}
	    	}
    	}
    	logger.debug("END");
        return new ModelAndView("../assessment/showQuestion", getModel());
    }
    
    @ResourceMapping("fetchQuestion")
    public ModelAndView fetchQuestion(@RequestParam Integer questionNum, ResourceRequest request, ResourceResponse response) {
    	logger.debug("START");
    	logger.debug("questionNum: " + questionNum);
    	if (questionNum > -1 && questionNum < currentAssessment.getQuestionCount()) {
    		this.currentQuestionNum = questionNum + 1;
    	}
    	logger.debug("END");
        return new ModelAndView("../assessment/showQuestion", getModel());
    }

    @ResourceMapping("submitAssessment")
    public void submitAssessment(ResourceRequest request, ResourceResponse response) throws IOException {
    	logger.debug("START");
    	// update assessment
    	this.currentAssessment.setStatus(AssessmentConstants.TestStatus.COMPLETED);
    	this.currentAssessment.getDtAssessment().setStatus(AssessmentConstants.TestStatus.COMPLETED);
    	this.serviceLocator.getAssessmentDetailsService().updateAssessmentDetails(this.currentAssessment);
    	// if subject test
    	/*if (AssessmentConstants.TEST_TYPE_SUBJECT_TEST == currentAssessment
    						.getDtAssessment().getDTTest().getTestId()) {
    	*/	
    	
    		String tempTestString =  "Test";
    		if(currentAssessment.getDtAssessment().getDTTest().getTestId() == AssessmentConstants.TEST_TYPE_SUBJECT_TEST) {
    			tempTestString = "Subject Test";
    		} 
			if(currentAssessment.getDtAssessment().getDTTest().getTestId() == AssessmentConstants.TEST_TYPE_DIAGNOSTIC_TEST) {
				tempTestString = "Diagnostic Test";	
			 }
			if(currentAssessment.getDtAssessment().getDTTest().getTestId() == AssessmentConstants.TEST_TYPE_DIAGNOSTIC_TEST_STYLE_BASED) {
				tempTestString = "Diagnosis Style Based Test";
			}
    		Date date = Calendar.getInstance().getTime();
    		SimpleDateFormat dformat = new SimpleDateFormat("dd/MMM/yyyy");
    		String now = dformat.format(date);
    		serviceLocator.getRewardPointsService().addRewardPoints(
    				WebUtil.getUserId(request), 10, "Has Completed "+currentAssessment.getSubjectName()+tempTestString, now);
    		
    	//}
    	logger.debug("Assessment saved");
    	response.getWriter().print("0");
    	response.flushBuffer();
    	// to redirect to showQuestion render method.
    	logger.debug("END");
    }

	public Integer getQuestionCount() {
		logger.debug("START");
		if (currentAssessment != null) {
			logger.debug("questionCount: " + currentAssessment.getQuestionCount());
			return currentAssessment.getQuestionCount();
		}
		logger.debug("END");
		return 0;
	}

	public Integer getCurrentQuestionNum() {
		logger.debug("START");
		logger.debug("currentQuestionNum: " + currentQuestionNum);
		logger.debug("END");
		return currentQuestionNum;
	}

    public DTAssessmentDetails getCurrentQuestion() {
    	logger.debug("START");
    	logger.debug("currentQuestionNum: " + currentQuestionNum);
    	if (currentAssessment != null) {
    		return currentAssessment.getQuestions().get(currentQuestionNum - 1);
    	}
    	logger.debug("END");
    	return null;
    }
    public List<DTAnswer> getOptions() {
    	logger.debug("START");
    	logger.debug("currentQuestionNum: " + currentQuestionNum);
    	if (currentAssessment != null) {
    		return currentAssessment.getOptions().get(this.getCurrentQuestion().getDTQuestion().getQuestionId());
    	}
    	logger.debug("END");
    	return null;
    }
    public String getTimeRemaining() {
    	logger.debug("START");
    	logger.debug("timeRemaining: " + timeRemaining);
    	logger.debug("END");
        return this.getTimeRemainingMinites();
    }

    @ResourceMapping("timeRemaining")
	public void serveTimeRemaining(ResourceRequest request, ResourceResponse response) throws IOException {
//    	logger.debug("timeRemaining: " + timeRemaining);
    	if (this.timeRemaining > 0) {
//    		logger.debug("inside timeRemaining: ");
    		this.timeRemaining = this.timeRemaining - 1;
    		response.getWriter().print(this.getTimeRemainingMinites());
    		response.flushBuffer();
    	} else {
    		logger.debug("timeRemaining: else");
    		response.getWriter().print(-1);
    		response.flushBuffer();
    	}
	}

    public String getTimeRemainingMinites() {
    	String time = "00:00";
    	if (this.timeRemaining > 0) {
    		long mins = timeRemaining / 60;
    		time = (mins<10 ? "0"+mins : ""+mins);
    		long secs = timeRemaining % 60;
    		time += " : " + (secs<10 ? "0"+secs : ""+secs);
    	}
    	return time;
    }

    @ResourceMapping("updateAnswer")
	public void updateAnswer(@RequestParam Integer answerId, ResourceRequest request, ResourceResponse response) throws IOException {
    	logger.debug("START");
    	logger.debug("Selected answer: " + answerId);
    	if (currentAssessment != null) {
    		final DTAssessmentDetails question = currentAssessment.getQuestions().get(currentQuestionNum - 1);
    		if (question.getAnswer() == null) {
    			this.answeredCount += 1;
    		}
    		question.setAnswer(Integer.toString(answerId));
    		logger.debug("answer saved.");
    	}
		logger.debug("END");
	}

    @ResourceMapping("openReport")
    public ModelAndView openReport(@RequestParam Integer assessmentId,
    		@RequestParam Integer testId, ResourceRequest request, ResourceResponse response) {
    	logger.debug("START");
    	// get the questions and other details with provided assessment id
    	logger.debug("assessmentId: " + assessmentId);
    	logger.debug("testId: " + testId);
    	ModelAndView modelAndView = prepareReport(assessmentId, testId);

    	logger.debug("END");
        return modelAndView;
    }

    @RenderMapping(params = "render=printReport")
    public ModelAndView printReport(@RequestParam Integer assessmentId,
    		@RequestParam Integer testId, RenderRequest request, RenderResponse response) {
    	logger.debug("START");
    	// get the questions and other details with provided assessment id
    	logger.debug("assessmentId: " + assessmentId);
    	logger.debug("testId: " + testId);
    	ModelAndView modelAndView = prepareReport(assessmentId, testId);
    	// change view for print report
    	if (AssessmentConstants.TEST_TYPE_DIAGNOSTIC_TEST.intValue() == testId) {
    		modelAndView.setViewName("../assessment/printReport");
    	} else if (AssessmentConstants.TEST_TYPE_DIAGNOSTIC_TEST_STYLE_BASED.intValue() == testId ||
    			AssessmentConstants.TEST_TYPE_SUBJECT_TEST.intValue() == testId) {
    		modelAndView.setViewName("../assessment/printPIReport");
    	}

    	logger.debug("END");
        return modelAndView;
    }

    private ModelAndView prepareReport(Integer assessmentId, Integer testId) {
    	logger.debug("START");
    	// get the questions and other details with provided assessment id
    	Map<String, Object> model = new HashMap<String, Object>();
    	String view = "../assessment/showReport";
    	logger.debug("testId: " + testId);

    	final SubjectMaster subjectMaster = this.serviceLocator
    					.getAssessmentService().getSubjectByAssessment(assessmentId);
    	if (subjectMaster != null) {
    		logger.debug("SubjectName: " + subjectMaster.getSubjectName());
    		model.put("subjectName", subjectMaster.getSubjectName());
    	}
    	if (AssessmentConstants.TEST_TYPE_DIAGNOSTIC_TEST.intValue() == testId) {
	    	Map<Short, SkillWiseAssessmentReport> assessmentReport = this.serviceLocator
	    				.getAssessmentDetailsService().getAssessmentReport(assessmentId);

	    	Set<Short> skillIds = assessmentReport.keySet();
	    	List<Float> percents = new ArrayList<Float>();
	    	List<String> skillNames = new ArrayList<String>();
	    	for (Short skillId : skillIds) {
	    		SkillWiseAssessmentReport skillReport = assessmentReport.get(skillId);
	    		percents.add(skillReport.getPercentage());
	    		if (skillReport.getSkillName() != null) {
	    			skillNames.add("\'" + skillReport.getSkillName().replaceAll(" ", "<br>") + "\'");
	    		}
			}
	    	model.put("percents", percents);
	    	model.put("skillNames", skillNames);
//	    	model.put("reportType", "BAR_CHART");
	    	logger.debug("percents: " + percents);
	    	logger.debug("skillNames: " + skillNames);
	    	view = "../assessment/showReport";
    	} else if (AssessmentConstants.TEST_TYPE_DIAGNOSTIC_TEST_STYLE_BASED.intValue() == testId ||
    			AssessmentConstants.TEST_TYPE_SUBJECT_TEST.intValue() == testId) {
    		Map<String, Integer> reportMap = null;
    		int questionCount = 0;
    		if (AssessmentConstants.TEST_TYPE_DIAGNOSTIC_TEST_STYLE_BASED.intValue() == testId) {
	    		StyleBasedAssessmentReport report = this.serviceLocator.getAssessmentDetailsService().getStyleBasedReport(assessmentId);
	    		reportMap = report.getStylewiseAnswerCount();
	    		questionCount = report.getQuestionCount();
	    		model.put("isDiagnosticStyleReport", "true");
    		} else if (AssessmentConstants.TEST_TYPE_SUBJECT_TEST.intValue() == testId) {
    			reportMap = this.serviceLocator.getAssessmentDetailsService().getSubjectTestReport(assessmentId);
    			// question count
    			for (String key : reportMap.keySet()) {
    				questionCount += reportMap.get(key);
				}
    		}
    		StringBuffer buffer = new StringBuffer();
    		Set<String> categories = reportMap.keySet();
    		buffer.append("[");
    		for (String category : categories) {
    			buffer.append("['" + category + "', " + reportMap.get(category) + "],");
			}
    		if (buffer.toString().endsWith(",")) {
    			buffer.setCharAt(buffer.length()-1, ' ');
    		}
    		buffer.append("]");
    		// model values
    		model.put("reportMap", reportMap);
    		model.put("questionCount", questionCount);
    		model.put("reportDataArray", buffer.toString());

    		logger.debug("reportDataArray: " + buffer.toString());
//	    	model.put("reportType", "PI_CHART");

	    	view = "../assessment/showPIReport";
    	}
    	model.put("testId", testId);
    	model.put("assessmentId", assessmentId);

    	logger.debug("END");
        return new ModelAndView(view, model);
    }
}
