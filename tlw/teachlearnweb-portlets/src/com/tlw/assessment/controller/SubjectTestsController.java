/*
 * This code is property of Teach Learn Web, Inc. Use, duplication and disclosure
 * in any form without permission of copyright holder is prohibited.
 *
 * (C) Copyright Teach Learn Web, Inc. 2012. All rights reserved.
 */
package com.tlw.assessment.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletSession;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.teachlearnweb.dao.entity.ClassMaster;
import com.teachlearnweb.dao.entity.DTAnswer;
import com.teachlearnweb.dao.entity.DTAssessment;
import com.teachlearnweb.dao.entity.DTAssessmentDetails;
import com.teachlearnweb.dao.entity.DTQuestion;
import com.teachlearnweb.dao.entity.DTTest;
import com.teachlearnweb.dao.entity.DifficultyLevel;
import com.teachlearnweb.dao.entity.SubjectMaster;
import com.tlw.assessment.vo.AssessmentConstants;
import com.tlw.assessment.vo.SubjectTest;
import com.tlw.controller.AbstractBaseController;
import com.tlw.util.Constants;
import com.tlw.util.WebUtil;

/**
 * The Class is responsible to handle the assessment related requests.
 * @author Govinda
 */
@Controller("subjectTestsController")
@Scope("session")
@RequestMapping(value = "VIEW")
public class SubjectTestsController extends AbstractBaseController {
    /** The logger. */
    private static final Logger logger = Logger.getLogger(SubjectTestsController.class);
//    private List<DTAssessment> existingTests = null;
//    private List<DifficultyLevel> levels = null;
    private List<SubjectTest> subjectTests = null;
    private SubjectMaster selectedSubject = null;
//    private Short subjectId;

    private Map<String, Object> getModel() {
    	Map<String, Object> model = new HashMap<String, Object>();
    	model.put("subjectTests", this.subjectTests);
    	model.put("selectedSubject", this.selectedSubject);
    	return model;
    }

    /**
     * Show registration.
     * @param renderResponse
     *            the render response
     * @return the string
     */
    @ResourceMapping("fetchSubjectTests")
    public ModelAndView showSubjectTests(ResourceRequest request, ResourceResponse response) {
    	logger.debug("START");
    	// request param
//    	final long subId = (Long) request.getPortletSession().getAttribute("subject_subjectId", PortletSession.APPLICATION_SCOPE);
    	final String subjectName = (String) request.getPortletSession().getAttribute("topics_assetCatName", PortletSession.APPLICATION_SCOPE);
    	final String className = (String) request.getPortletSession().getAttribute("subject_className", PortletSession.APPLICATION_SCOPE);
//    	final Short subjectId = (short) subId;
    	logger.debug("subjectName from session: " + subjectName);
    	logger.debug("className from session: " + className);
    	if (subjectName != null && !"".equals(subjectName)) {
	    	this.subjectTests = new ArrayList<SubjectTest>();
	    	this.selectedSubject = this.serviceLocator.getAssessmentService().getSubjectMasterbySbujectName(subjectName);
    	}
    	final ClassMaster classMaster = this.serviceLocator.getAssessmentService().getClassMasterByClassName(className);
    	if (this.selectedSubject != null && classMaster != null) {
    		int userId = (int)WebUtil.getUserId(request);
    		logger.debug("userId: " + userId);
	    	List<DTAssessment> existingTests = this.serviceLocator.getAssessmentService().getAssessments(userId,
	    						classMaster.getClassId(), this.selectedSubject.getSubjectId(),
	    						AssessmentConstants.TEST_TYPE_SUBJECT_TEST);
	    	if (existingTests != null && !existingTests.isEmpty()) {
	    		logger.debug("existingTests.size: " + existingTests.size());
		    	for (DTAssessment assessment : existingTests) {
		    		final SubjectTest sTest = new SubjectTest();
		    		final DifficultyLevel dLevel = this.serviceLocator.getAssessmentService()
		    												.getDifficultyLevelByAssessment(assessment.getAssessmentId());
		    		sTest.setDifficultyLevel(dLevel);
		    		sTest.setAssessment(assessment);
	
		    		this.subjectTests.add(sTest);
				}
	    	} else {
	    		logger.debug("no existing tests available");
	    	}
	    	// levels 
	    	List<DifficultyLevel> levels = this.serviceLocator.getAssessmentService()
	    								.getDifficultiesBySubject(this.selectedSubject.getSubjectId(),
	    									classMaster.getClassId());
	    	if (levels != null && !levels.isEmpty()) {
	    		logger.debug("levels.size: " + levels.size());
	    		for (DifficultyLevel level : levels) {
		    		final SubjectTest sTest = new SubjectTest();
		    		sTest.setDifficultyLevel(level);
		    		sTest.setAssessment(null);
		    		boolean found = false;
		    		for (SubjectTest subjectTest : this.subjectTests) {
		    			if (level.getLevelId() != null && level.getLevelId().equals(subjectTest.getDifficultyLevel().getLevelId())) {
		    				found = true;
		    			}
					}
		    		if ( ! found) {
		    			logger.debug("level " + level.getDescription() + " not found, so adding.");
		    			this.subjectTests.add(sTest);
		    		}
				}
	    	} else {
	    		logger.debug("no levels available");
	    	}
	    	if (this.subjectTests != null) {
	    		logger.debug("this.subjectTests.size: " + this.subjectTests.size());
	    	}
    	} else {
    		logger.error("subject or class not found");
    	}
    	logger.debug("END");
    	return new ModelAndView("../assessment/subjectTests", getModel());
    }

    @ResourceMapping("newAssessment")
    public void newAssessment(@RequestParam Short subjectId, @RequestParam Short levelId,
    		ResourceRequest request, ResourceResponse response) throws IOException {
    	logger.debug("START");
    	DTAssessment dtAssessment = null;
    	// create a new assessment for the subjectId given and pick questions of the level given
    	logger.debug("subjectId: " + subjectId);
    	logger.debug("levelId: " + levelId);
    	final String className = (String) request.getPortletSession().getAttribute("subject_className", PortletSession.APPLICATION_SCOPE);
    	int userId = (int) WebUtil.getUserId(request);
    	logger.debug("className from session: " + className);
    	ClassMaster classMaster = this.serviceLocator.getAssessmentService().getClassMasterByClassName(className);
    	if (classMaster != null) {
    		logger.debug("classId: " + classMaster.getClassId());
    		List<DTQuestion> questions = this.serviceLocator.getAssessmentService().getQuestions(classMaster.getClassId(), subjectId, levelId);
    		if (questions != null && !questions.isEmpty()) {
    			logger.debug("questions.size: " + questions.size());
    			SubjectMaster subjectMaster = new SubjectMaster();
    			subjectMaster.setSubjectId(subjectId);
    			DTTest dtTest = new DTTest();
    			dtTest.setTestId(AssessmentConstants.TEST_TYPE_SUBJECT_TEST);

    			dtAssessment = new DTAssessment();
				dtAssessment.setAssessmentDate(new java.util.Date());
				dtAssessment.setMinutePerQuestion(new BigDecimal(Constants.MINUTE_PER_QUESTION_FACTOR * questions.size()));
				dtAssessment.setQuestionCount(questions.size());
				dtAssessment.setUserId(userId);
				dtAssessment.setDTTest(dtTest);
				dtAssessment.setClassMaster(classMaster);
				dtAssessment.setSubjectMaster(subjectMaster);
				dtAssessment.setStatus(AssessmentConstants.TestStatus.ASSIGNED);
//				dtAssessment.setDTAssessmentDetailses(dtAssessmentDetailsSet);

				// save assessment
				this.serviceLocator.getAssessmentService().saveAssessment(dtAssessment);
		    	logger.debug("assessId after save: " + dtAssessment.getAssessmentId());

				// insert assessementDetails
				List<DTAssessmentDetails> dtAssessmentDetailsList = new ArrayList<DTAssessmentDetails>();
				DTAssessmentDetails dtAssessmentDetails = null;
				for(DTQuestion question : questions) {
					DTAnswer dtAnswer = this.serviceLocator.getAssessmentService().getAnswerForQuestion(question.getQuestionId());
					if (dtAnswer != null) {
						dtAssessmentDetails = new DTAssessmentDetails();
						dtAssessmentDetails.setDTAssessment(dtAssessment);
						dtAssessmentDetails.setDTQuestion(question);
						dtAssessmentDetails.setMarksScored(new BigDecimal("0"));
						dtAssessmentDetails.setDTAnswer(dtAnswer);

						dtAssessmentDetailsList.add(dtAssessmentDetails);
					} else {
						logger.debug("Answer not found for " + question.getDependentId() + " so not adding this question.");
					}
				}
				this.serviceLocator.getAssessmentDetailsService().saveAssessmentDetailList(dtAssessmentDetailsList);
    		} else {
    			logger.debug("no questions found for subject " + subjectId);
    		}
    	} else {
    		logger.error("no class master found for class name " + className);
    	}
    	if (dtAssessment != null && dtAssessment.getAssessmentId() != null) {
    		logger.debug("newAssessment id: " + dtAssessment.getAssessmentId());
    		response.getWriter().print(""+dtAssessment.getAssessmentId());
	    	response.flushBuffer();
	    } else {
	    	logger.debug("newAssessment: unable to create. returning '*Cannot create assessment*'");
	    	response.getWriter().print("*Cannot create assessment*");
	    	response.flushBuffer();
	    }
    	logger.debug("END");
    }
}
