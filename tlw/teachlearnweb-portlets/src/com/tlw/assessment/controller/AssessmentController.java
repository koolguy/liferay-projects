/*
 * This code is property of Teach Learn Web, Inc. Use, duplication and disclosure
 * in any form without permission of copyright holder is prohibited.
 *
 * (C) Copyright Teach Learn Web, Inc. 2012. All rights reserved.
 */
package com.tlw.assessment.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.teachlearnweb.dao.entity.DTAssessment;
import com.tlw.controller.AbstractBaseController;
import com.tlw.util.WebUtil;

/**
 * The Class is responsible to handle the assessment related requests.
 * @author Govinda
 */
@Controller("assessmentController")
@Scope("session")
@RequestMapping(value = "VIEW")
public class AssessmentController extends AbstractBaseController {
    /** The logger. */
    private static final Logger logger = Logger.getLogger(AssessmentController.class);
    private List<DTAssessment> assessments = null;

    private Map<String, Object> getModel() {
    	Map<String, Object> model = new HashMap<String, Object>();
    	model.put("assessments", this.assessments);
    	model.put("testTypeId", 1);
    	return model;
    }

    /**
     * Show registration.
     * @param renderResponse
     *            the render response
     * @return the string
     */
    @RenderMapping
    public ModelAndView showAssessments(RenderRequest renderRequest, RenderResponse renderResponse) {
    	logger.debug("START");
    	// need to get testId from request param here
    	int userId = (int) WebUtil.getUserId(renderRequest);
    	logger.debug("User id from session: " + userId);
    	assessments = this.serviceLocator.getAssessmentService().getDiagnosticTests(userId);
    	if (this.assessments != null) {
    		logger.debug("in show assessments: " + this.assessments.size());
    	}
    	logger.debug("END");
    	return new ModelAndView("assessments", getModel());
    }
}
