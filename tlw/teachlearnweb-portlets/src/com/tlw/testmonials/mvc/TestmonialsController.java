/*
 * This code is property of Teach Learn Web, Inc. Use, duplication and disclosure
 * in any form without permission of copyright holder is prohibited.
 *
 * (C) Copyright Teach Learn Web, Inc. 2012. All rights reserved.
 */
package com.tlw.testmonials.mvc;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.util.WebUtils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.util.PortalUtil;
import com.teachlearnweb.dao.entity.Testmonial;
import com.teachlearnweb.service.vo.User;
import com.tlw.controller.AbstractBaseController;
import com.tlw.util.Constants;
import com.tlw.util.WebUtil;
/**
 * The Class MyProfileController, is responsible to handle the user.
 * 
 */
@Controller("value=testMonialsController")
@RequestMapping(value = "VIEW")
public class TestmonialsController extends AbstractBaseController {

	/** The LOG. */
	private static final Log LOG = LogFactory
			.getLog(TestmonialsController.class);

	/**
	 * Default Render Mapping Method.
	 * 
	 * @param renderRequest
	 * @param renderResponse
	 * @return view name
	 */
	@RenderMapping
	public ModelAndView showTestmonials(RenderRequest renderRequest,
			RenderResponse renderResponse) {
		
		List<TestmonialVo> lstTestmonials = new ArrayList<TestmonialVo>();
		TestmonialVo vo = new TestmonialVo();
		List<Testmonial> testmonialList = serviceLocator.getTestmonialService()
				.fetchTestMonial();

		for (Iterator iterator = testmonialList.iterator(); iterator.hasNext();) {
			
			vo = new TestmonialVo();
			try {
				Testmonial testmonial = (Testmonial) iterator.next();
				LOG.debug("user Id Entity:" + testmonial.getUserId());
				BeanUtils.copyProperties(vo, testmonial);
				LOG.debug("user Id from VO:" + vo.getUserId());
				User user = (User) WebUtil.getUserByUserId(vo.getUserId(),
						PortalUtil.getHttpServletRequest(renderRequest));

				vo.setUserId(testmonial.getUserId());
				vo.setDescription(testmonial.getDescription());
				vo.setFirstName(user.getFirstName());
				System.out.println("testmonial.getUserId()..........."+testmonial.getUserId());
				System.out.println("user.getFirstName())..........."+user.getFirstName());
				System.out.println("WebUtil.getAssetCategoryTitle(user.getClassName())..........."+user.getClassName());
				vo.setClassName(WebUtil.getAssetCategoryTitle(user.getClassName()));
				vo.setLastName(user.getLastName());
				
				lstTestmonials.add(vo);
				

			} catch (IllegalAccessException e) {
				LOG.debug(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch blockst
				LOG.debug(e);
				e.printStackTrace();
			}
		}

		Map model = new HashMap();
		model.put("testMonialsList", lstTestmonials);
		LOG.debug("returning :  TESTIMONIALS_JSP new");
		return new ModelAndView(Constants.TESTIMONIALS_JSP, model);
		
	}
	
	@ActionMapping(params = "action=postTestmonial")
	public void postTestmonial(
			ActionRequest actionRequest,
			ActionResponse actionResponse, SessionStatus sessionStatus) throws PortalException, SystemException {
		String desc = ParamUtil.get(actionRequest, "tesmonialDesc", "Default Tesmonial");
		LOG.debug("In Testmonial ***********************************************************************************************************");
		LOG.debug("desc.................................."+desc);
		try {
			serviceLocator.getTestmonialService().postTestmonial(desc,WebUtil.getUserId(actionRequest));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Gets TestMonials object.
	 * 
	 * @return the List of Testmonials list
	 */
	 @ModelAttribute("isRegisteredUser")
	 public boolean checkIsRegisterdUser(PortletRequest request){
		 boolean isRegisteredUser = false;
		 
		 Object userObj1 = WebUtils.getSessionAttribute(PortalUtil.getHttpServletRequest(request),Constants.TLW_USER);
		 LOG.debug("userObj1 :"+userObj1);
		 // check for null
		 if(null != userObj1){
			 isRegisteredUser = true;
		 }else{
			 isRegisteredUser = false;
		 }
		 return isRegisteredUser;
	 }
	 /**
		 * Gets TestMonials object.
		 * 
		 * @return the List of Testmonials list
		 */

	 @ModelAttribute("isAdmin")
	 public String checkIsAdmin(PortletRequest request){
		 String strIsAdmin = null;		 
		 boolean booleanIsAdmin = WebUtil.checkRole(request, "administrator");
		 
		 LOG.debug("booleanIsAdmin latest:"+booleanIsAdmin);
		 if(booleanIsAdmin){
			 strIsAdmin = "TRUE";
		 }else{
			 strIsAdmin = "FALSE";
		 }

		 return strIsAdmin;
	 }
	/**
	 * Gets TestMonials object.
	 * 
	 * @return the List of Testmonials list
	 */
	
/*	 @ModelAttribute("testMonialsList")
	 public List<TestmonialVo> fetchTestMonials(PortletRequest request) {
		 
		List<TestmonialVo> lstTestmonials = new ArrayList<TestmonialVo>();
		TestmonialVo vo = new TestmonialVo();
		List<Testmonial> testmonialList = serviceLocator.getTestmonialService()
				.fetchTestMonial();

		for (Iterator iterator = testmonialList.iterator(); iterator.hasNext();) {
			
			vo = new TestmonialVo();
			try {
				Testmonial testmonial = (Testmonial) iterator.next();
				LOG.debug("user Id Entity:" + testmonial.getUserId());
				BeanUtils.copyProperties(vo, testmonial);
				LOG.debug("user Id from VO:" + vo.getUserId());
				User user = (User) WebUtil.getUserByUserId(vo.getUserId(),
						PortalUtil.getHttpServletRequest(request));

				vo.setUserId(testmonial.getUserId());
				vo.setDescription(testmonial.getDescription());
				vo.setFirstName(user.getFirstName());
				vo.setClassName(user.getClassName());
				vo.setLastName(user.getLastName());
				
				lstTestmonials.add(vo);
				

			} catch (IllegalAccessException e) {
				LOG.debug(e);
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch blockst
				LOG.debug(e);
				e.printStackTrace();
			}
		}
		return lstTestmonials;

	}*/
	 @ActionMapping(params = "action=approveTestmonial")
		public void approveTestmonial(
				@RequestParam Long testmonialId,
				ActionRequest actionRequest,
				ActionResponse actionResponse, SessionStatus sessionStatus) throws PortalException, SystemException {
	 
		 LOG.debug("apprroving testmonial  "+testmonialId);
		 
		 try {
			int count  = serviceLocator.getTestmonialService().approveTestmonial(testmonialId);
			
			LOG.debug("approved count "+count);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		 
	 }
	 

}