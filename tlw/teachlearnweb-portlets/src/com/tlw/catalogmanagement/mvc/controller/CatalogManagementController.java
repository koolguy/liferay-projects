/*
 * This code is property of Teach Learn Web, Inc. Use, duplication and disclosure
 * in any form without permission of copyright holder is prohibited.
 *
 * (C) Copyright Teach Learn Web, Inc. 2012. All rights reserved.
 */
package com.tlw.catalogmanagement.mvc.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.asset.model.AssetCategory;
import com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil;
import com.teachlearnweb.dao.entity.ClassMaster;
import com.teachlearnweb.dao.entity.DTAnswer;
import com.teachlearnweb.dao.entity.DTAssessment;
import com.teachlearnweb.dao.entity.DTAssessmentDetails;
import com.teachlearnweb.dao.entity.DTQuestion;
import com.teachlearnweb.dao.entity.DTTest;
import com.teachlearnweb.dao.entity.Skill;
import com.teachlearnweb.dao.entity.SubjectMaster;
import com.teachlearnweb.dao.entity.UserCategories;
import com.tlw.assessment.vo.AssessmentConstants;
import com.tlw.catalogmanagement.mvc.vo.CategoryVO;
import com.tlw.catalogmanagement.mvc.vo.Course;
import com.tlw.controller.AbstractBaseController;
import com.tlw.registration.mvc.vo.RegistrationModelBean;
import com.tlw.shoppingcart.mvc.vo.CartVO;
import com.tlw.util.Constants;
import com.tlw.util.WebUtil;

/**
 * The Class RegistrationController, is responsible to handle the user
 * registration request.
 * @author asharma
 * @author arvindk
 */
@Controller("value=catalogManagementController")
@Scope("session")
@RequestMapping(value = "VIEW")
public class CatalogManagementController extends AbstractBaseController {
    /** The LOG. */
    private static final Logger LOG = Logger
            .getLogger(CatalogManagementController.class);
    /**
     * Show registration.
     * @param renderResponse
     *            the render response
     * @return the string
     */
    @RenderMapping
    public String showCourses(RenderRequest renderRequest, RenderResponse renderResponse) {
        String map = renderRequest.getParameter("actionMapping");
        map = renderRequest.getParameter("shoppingMapping")!=null?renderRequest.getParameter("shoppingMapping"):renderRequest.getParameter("actionMapping");
        LOG.debug("map..."+map);
        String categoryId = renderRequest.getParameter("categoryId");
        renderRequest.setAttribute("categoryId", categoryId);
        if(null  == map || "".equals(map)){
        	map = "showCourses";
        }else{
        	if(null != renderRequest.getParameter("billing")){
        	//	map = "billing";
        	}
        	// if paymentSuccess, create assessments
        	if ("paymentSuccess".equalsIgnoreCase(map)) {
        		// TODO: move this method call to proper location, once payment gateway integration is done
        		try {
					createAssessment(categoryId, renderRequest);
				} catch (NumberFormatException e) {
					LOG.error("ERROR while creating diagnostic tests for categoryId " + categoryId);
					e.printStackTrace();
				} catch (SystemException e) {
					LOG.error("ERROR while creating diagnostic tests for categoryId " + categoryId);
					e.printStackTrace();
				}
        	}
        	LOG.debug("categoryId param: " + categoryId);
        	AssetCategory assetCategory = WebUtil.getAssetCategoryById(Long.parseLong(categoryId));
            //User loggedInUser = WebUtil.getUser(renderRequest);
            renderRequest.setAttribute("assetCategory",assetCategory);
            //renderRequest.setAttribute("loggedInUser",loggedInUser);
            renderRequest.setAttribute("parentCategory",WebUtil.getAssetCategoryById(assetCategory.getParentCategoryId()));
            renderRequest.setAttribute("amount",WebUtil.getAmount(assetCategory.getCategoryId()));
        }
        LOG.debug("map: " + map);
    	return map;
    }

    /**
     * Gets the command object.
     * @return the command object
     */
    @ModelAttribute("course")
    public Course getCommandObject() {
        LOG.debug("Creating object of model RegistrationModelBean..");
        Course course = new Course();
        /*FaceBookService facebookService = serviceLocator.getFaceBookService();
        registrationModelBean.setFacebookCancelUrl(facebookService
                .getOnCancelRedirectionUrl());*/
        return course;
    } 
    
    
    /**
	 * Model Object for displaying Asset Categories.
	 * 
	 * @param portletRequest
	 * @param portletResponse
	 * @return Map<Long, List<AssetCategory>>
	 * @throws SystemException
	 */
	@ModelAttribute(value = "assetCategoryList")
	public List<CategoryVO> getAssetCategoryList(PortletRequest portletRequest,
							PortletResponse portletResponse)throws SystemException {		
		String vocabularyName  = "Course Management";			
		List<CategoryVO> assetCategoryList = WebUtil.getAllAssetCategoriesByVocabulary(portletRequest, portletResponse, vocabularyName);	
		/*for (AssetCategory assetCategory : assetCategoryList) {
			LOG.debug("categoryName.............."+assetCategory.getName());
			LOG.debug("categoryId.............."+assetCategory.getCategoryId());
			LOG.debug("Parent categoryId.............."+assetCategory.getParentCategoryId());
			List<AssetEntry> assetEntryList= WebUtil.fetchAssetEntries(assetCategory.getCategoryId(), portletRequest);
			
			if(assetEntryList.size()>0){
				for (AssetEntry assetEntry : assetEntryList) {
					LOG.debug("Tiltle................"+assetEntry.getTitle());
				}
			}
			
			LOG.debug("vocabularyId.............."+assetCategory.getVocabularyId());
		}*/
		return assetCategoryList;
	}
	
	@ActionMapping(params = "action=subscribeCourse")
	public void subscribeCourse(@RequestParam String categoryId,
			@ModelAttribute RegistrationModelBean registrationBean,
			BindingResult bindingResult, ActionRequest actionRequest,
			ActionResponse actionResponse, SessionStatus sessionStatus)
			throws PortalException, SystemException {

		LOG.info("Request received to subscribeCourse the user...");
		LOG.debug("categoryId: " + categoryId);
		Date date = Calendar.getInstance().getTime();
		SimpleDateFormat dformat = new SimpleDateFormat("dd/MMM/yyyy");
		String now = dformat.format(date);
		serviceLocator.getUserService().addCourse(
				WebUtil.getUserId(actionRequest), Long.parseLong(categoryId));
		serviceLocator.getRewardPointsService().addRewardPoints(
				WebUtil.getUserId(actionRequest), 500, "Purchased Course",now);
		HttpServletRequest servletreq = PortalUtil.getHttpServletRequest(actionRequest);
		HttpSession session = PortalUtil.getOriginalServletRequest(servletreq).getSession();
		session.removeAttribute("cartItems");
		actionResponse.setRenderParameter("actionMapping", "paymentSuccess");
		actionResponse.setRenderParameter("categoryId", categoryId);

	}

	// not in use
	@ActionMapping(params = "action=makePayment")
	public void makePayment(@RequestParam String categoryId,
			@RequestParam String categoryName,
			@ModelAttribute RegistrationModelBean registrationBean,
			BindingResult bindingResult, ActionRequest actionRequest,
			ActionResponse actionResponse, SessionStatus sessionStatus)
			throws PortalException, SystemException {

		LOG.info("Request received to subscribeCourse the user...");
		System.out
				.println("Make Payment...........cata..........categoryId.................."
						+ categoryId);
		// serviceLocator.getUserService().addCourse(WebUtil.getUserId(actionRequest),categoryId);

//		createAssessment(categoryId, actionRequest);
		actionResponse.setRenderParameter("actionMapping", "makePayment");
		actionResponse.setRenderParameter("categoryId", categoryId);

	}

	private void createAssessment(String categoryId, PortletRequest request) throws NumberFormatException,
			SystemException {
		LOG.debug("START");
		LOG.debug("CategoryId(class subscribed): " + categoryId);
		List<AssetCategory> childCategoriesList = AssetCategoryLocalServiceUtil
				.getChildCategories(Long.valueOf(categoryId));
		String categoryName = null;
		final AssetCategory parentCategory = WebUtil.getAssetCategoryById(Long.valueOf(categoryId));
		if (parentCategory != null) {
			categoryName = parentCategory.getName();
		}
		LOG.debug("childCategoriesList size: " + childCategoriesList.size()
				+ "------------categoryName(class subscribed): " + categoryName);
		// diagnostic tests(subject wise)
		ClassMaster classMaster = null;
		int tempUserId = -1;
		if (childCategoriesList != null && !childCategoriesList.isEmpty()
				&& childCategoriesList.size() > 0) {
			// class master
			classMaster = serviceLocator
			.getAssessmentService().getClassMasterByClassName(
					categoryName);
			// user id
			long userId = WebUtil.getUserId(request);
			if (userId != 0) {
				tempUserId = (int) userId;
			}
			if (classMaster != null) {
				for (AssetCategory assetCategory : childCategoriesList) {
					if (assetCategory != null) {
						List<DTQuestion> dtQuestionList = null;
						DTTest dtTest = null;
						SubjectMaster subjectMaster = serviceLocator
								.getAssessmentService()
								.getSubjectMasterbySbujectName(
										assetCategory.getName());
						if (subjectMaster != null) {
							dtTest = new DTTest();
							dtTest.setTestId(AssessmentConstants.TEST_TYPE_DIAGNOSTIC_TEST);
							final List<Skill> skills = serviceLocator.getAssessmentService()
													.getSkills(subjectMaster.getSubjectId());
							dtQuestionList = serviceLocator.getAssessmentService()
									.getSkillwiseQuestions(skills, classMaster.getClassId(),
											subjectMaster.getSubjectId(), AssessmentConstants.QUESTIONS_PER_SKILL);
							int questionCount = 0;
							if (dtQuestionList != null && !dtQuestionList.isEmpty()) {
								questionCount = dtQuestionList.size();
								LOG.debug("Question count for subject " + subjectMaster.getSubjectName() + " is " + questionCount);
								// insert into db
								this.insertAssessment(dtQuestionList, questionCount, tempUserId, classMaster, dtTest, subjectMaster);
							} else {
								LOG.error("NO questions found for the subject " + subjectMaster.getSubjectName());
							}
						}
					}
				}
			} else {
				LOG.error("Classmaster for class " + categoryName + " is null");
			}
			// diagnostic tests(style based)
			List<DTQuestion> styleQuestionList = serviceLocator.getAssessmentService().getStyleBasedQuestions();
			if (styleQuestionList != null && ! styleQuestionList.isEmpty()) {
				LOG.debug("styleQuestionList.size: " + styleQuestionList.size());
				int questionCount = styleQuestionList.size();
				DTTest dtTest = new DTTest();
				dtTest.setTestId(AssessmentConstants.TEST_TYPE_DIAGNOSTIC_TEST_STYLE_BASED);
				SubjectMaster subjectMaster = new SubjectMaster();
				subjectMaster.setSubjectId(AssessmentConstants.SUBJECT_STYLE_BASED);
				this.insertAssessment(styleQuestionList, questionCount, tempUserId, classMaster, dtTest, subjectMaster);
			} else {
				LOG.error("No style based questions found");
			}
		}
		LOG.debug("END");
	}

	private DTAssessment insertAssessment(List<DTQuestion> dtQuestionList, int questionCount, int tempUserId, 
			ClassMaster classMaster, DTTest dtTest, SubjectMaster subjectMaster) {
		LOG.debug("START");
		DTAssessment dtAssessment = new DTAssessment();
		Set<DTAssessmentDetails> dtAssessmentDetailsSet = new HashSet<DTAssessmentDetails>();

		dtAssessment.setAssessmentDate(new java.util.Date());
		dtAssessment.setQuestionCount(questionCount);
		dtAssessment.setUserId(tempUserId);
		dtAssessment.setSubjectMaster(subjectMaster);
		dtAssessment.setClassMaster(classMaster);
		dtAssessment.setDTTest(dtTest);
		dtAssessment
				.setDTAssessmentDetailses(dtAssessmentDetailsSet);
		dtAssessment.setMinutePerQuestion(new BigDecimal(Constants.MINUTE_PER_QUESTION_FACTOR * questionCount));
		dtAssessment.setStatus(AssessmentConstants.TestStatus.ASSIGNED);

		if (dtQuestionList != null && !dtQuestionList.isEmpty()
				&& dtQuestionList.size() > 0) {
			DTAssessmentDetails dtAssessmentDetails = null;
			for (DTQuestion question : dtQuestionList) {
				if (question != null) {
					dtAssessmentDetails = new DTAssessmentDetails();
					DTAnswer dtAnswer = serviceLocator
							.getAssessmentService()
							.getAnswerForQuestion(
									question.getQuestionId());
					dtAssessmentDetails
							.setDTAssessment(dtAssessment);
					dtAssessmentDetails.setDTQuestion(question);
					dtAssessmentDetails
							.setMarksScored(new BigDecimal("0"));
					if (dtAnswer != null
							&& dtAnswer.getAnswer() != null) {
						// dtAssessmentDetails.setAnswer(dtAnswer.getAnswer());
						dtAssessmentDetails
								.setDTAnswer(dtAnswer);
						dtAssessmentDetailsSet
						.add(dtAssessmentDetails);
					} else {
						LOG.error("DTAnswer is null for question " + question.getQuestionId());
					}
				} else {
					LOG.error("DTQuestion is null");
				}
			}
			LOG.debug("AssessmentDetails list is prepared");
		}
		// insert
		if (dtAssessment != null && dtQuestionList != null
				&& !dtQuestionList.isEmpty()
				&& dtQuestionList.size() > 0 && dtTest != null
				&& dtAssessmentDetailsSet != null
				&& dtAssessmentDetailsSet.size() > 0) {
			serviceLocator.getAssessmentService().saveAssessment(
					dtAssessment);
			LOG.debug("Assessment saved");
		}
		if (dtAssessmentDetailsSet != null
				&& dtAssessmentDetailsSet.size() > 0) {
			List<DTAssessmentDetails> dtAssessmentDetailsList = new ArrayList<DTAssessmentDetails>();
			dtAssessmentDetailsList.addAll(dtAssessmentDetailsSet);
			serviceLocator.getAssessmentDetailsService()
					.saveAssessmentDetailList(
							dtAssessmentDetailsList);
			LOG.debug("AssessmentDetailsList saved");
		}
		LOG.debug("END");
		return dtAssessment;
	}

	@ResourceMapping("addToCart")
	public String addToCart(@RequestParam Long categoryId,
			ResourceRequest actionRequest, ResourceResponse actionResponse,
			SessionStatus sessionStatus) throws PortalException,
			SystemException {
		PortletRequest portletRequest = actionRequest;
		PortletResponse portletResponse = actionResponse;
		LOG.debug("Request received to subscribeCourse the user...");
		LOG.debug("categoryId.................." + categoryId);
		String vocabularyName = "Course Management";
		HttpServletRequest servletreq = PortalUtil.getHttpServletRequest(actionRequest);
		HttpSession session = PortalUtil.getOriginalServletRequest(servletreq).getSession();
		//PortletSession session = actionRequest.getPortletSession();
		Map<Long,CartVO> cartItemsMap = (Map<Long,CartVO>) session.getAttribute("cartItems");
		Map categoriesMap = null;
		List<CategoryVO> assetCategoryList = WebUtil
				.getAllAssetCategoriesByVocabulary(portletRequest,
						actionResponse, vocabularyName);
		categoriesMap = assetCategoryList.get(0).getCategoriesMap();
		if (null != categoriesMap) {
			if (null == cartItemsMap) {
				cartItemsMap = new HashMap<Long,CartVO>();
			}
			System.out
					.println("cartItemsList size before add.................."
							+ cartItemsMap.size());
			CartVO cartVO = new CartVO();
			cartVO.setItemType("Class");
			cartVO.setPrice(Double.valueOf(WebUtil.getAmount(categoryId)));
			cartVO.setCategoryId(categoryId);
			cartVO.setCategory((AssetCategory)categoriesMap.get(categoryId));
			cartItemsMap.put(categoryId,cartVO);
			LOG.debug("cartItemsMap size after add: " + cartItemsMap.size());
			actionRequest.setAttribute("cartItemsSize",cartItemsMap.size());
			session.setAttribute("cartItems", cartItemsMap);
		}
		return "cartSuccess";
	}

	@ResourceMapping("actionShowDescription")
	public ModelAndView actionShowDescription(ResourceRequest resourceRequest)
			throws PortalException, SystemException {
		Map<String, AssetCategory> model = null;
		if (null != resourceRequest.getParameter("categoryId")) {
			model = new HashMap<String, AssetCategory>();
			long categoryId = Long.valueOf(resourceRequest
					.getParameter("categoryId"));
			LOG.debug("categoryId: " + categoryId);
			AssetCategory category = WebUtil.getAssetCategoryById(categoryId);
			LOG.debug("asset category: " + category);
			// resourceRequest.setAttribute("catgory", model);
			List<UserCategories> userCategories = serviceLocator.getUserService().categoriesByUserId(WebUtil.getUserId(resourceRequest));
			String isSubscribed = "false";
			boolean isPurchased = false;
			if(null != userCategories && userCategories.size() > 0){
				isPurchased = true;
				LOG.debug("isPurchased: " + isPurchased);
			}
			for(UserCategories userCat : userCategories){
				if(userCat.getCategoryId() == categoryId){
					isSubscribed = "true";
				}
			}
			resourceRequest.setAttribute("isSubscribed", isSubscribed);
			resourceRequest.setAttribute("isRegisteredUser", isUserLoggedIn(resourceRequest));
			resourceRequest.setAttribute("isPurchased", isPurchased);
//			resourceRequest.setAttribute("isRegisteredUser", true);
			
			model.put("category", category);
			
		}
		return new ModelAndView("courseDetails", model);

	}
	
	@ResourceMapping("showClasses")
	public ModelAndView showClasses(ResourceRequest resourceRequest)
			throws PortalException, SystemException {
		Map<String, List<AssetCategory>> model = null;
		if (null != resourceRequest.getParameter("categoryId")) {
			String parent = resourceRequest.getParameter("categoryName");
			model = new HashMap<String, List<AssetCategory>>();
			LOG.debug("parent category Name: " + parent);
			long parentCategoryId = Long.valueOf(resourceRequest
					.getParameter("categoryId"));
			LOG.debug("parentCategoryId.........."+parentCategoryId);
			long vocabularyId = 0;
			List<AssetCategory> subCategories = WebUtil.getChildCategories(parentCategoryId, vocabularyId);
			// resourceRequest.setAttribute("catgory", model);
			LOG.debug("subCategories.........." + subCategories);
			Map<Integer,AssetCategory> subCatMap = new HashMap<Integer, AssetCategory>();
			try{
			for(AssetCategory subCat:subCategories){
				String catName = subCat.getName();
				if(null!=catName){
				int classNo = 0;
				
				if(catName.contains("Class")){
					classNo = Integer.valueOf(catName.substring(catName.indexOf(" ")+1,catName.length()));
				}else classNo = Integer.valueOf(catName);
				subCatMap.put(classNo,subCat);
				LOG.debug("catName..." + catName);
			}
			}
			}catch(Exception exception){exception.printStackTrace();}
			resourceRequest.setAttribute("subCategoriesMap",subCatMap);
			resourceRequest.setAttribute("parentCategory",parent);
			model.put("subCategories", subCategories);
		}
		return new ModelAndView("showClasses", model);
	}
	
	 public boolean isUserLoggedIn(PortletRequest request){
		 return WebUtil.getThemeDisplay(request).isSignedIn();
	 }
	
}
