package com.tlw.catalogmanagement.mvc.vo;
import com.liferay.portlet.asset.model.AssetCategory;
import java.util.Map;
import java.util.List;
public class CategoryVO implements java.io.Serializable{

	//private List<AssetCategory> subCategoriesList;
	private List<AssetCategory> category;
	private Map subCategoriesMap;
	private Map categoriesMap;
	
	
	public Map getCategoriesMap() {
		return categoriesMap;
	}
	public void setCategoriesMap(Map categoriesMap) {
		this.categoriesMap = categoriesMap;
	}
	public Map getSubCategoriesMap() {
		return subCategoriesMap;
	}
	public void setSubCategoriesMap(Map subCategoriesMap) {
		this.subCategoriesMap = subCategoriesMap;
	}
	public List<AssetCategory> getCategory() {
		return category;
	}
	public void setCategory(List<AssetCategory> category) {
		this.category = category;
	}
}
