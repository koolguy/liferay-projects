package com.tlw.topics;

import java.util.Comparator;
import java.util.Collections;

import org.apache.log4j.Logger;

import com.liferay.portlet.asset.model.AssetCategory;

public class AssestCategoryComparator implements Comparator {
	
	/** The LOG. */
	private static final Logger LOG = Logger.getLogger(TopicsController.class);

	  public int compare(Object obj1, Object obj2) {
	    AssetCategory assetCategory1 = (AssetCategory) obj1;
	    AssetCategory assetCategory2 = (AssetCategory) obj2;
	    
	    //LOG.debug("compare invoked:");
	   // LOG.debug("assetCategory1.getCategoryId()"+assetCategory1.getCategoryId());
	   // LOG.debug("assetCategory1.getCategoryId()"+assetCategory2.getCategoryId());
	    // Assending order
	    if(assetCategory1.getCategoryId() < assetCategory2.getCategoryId()){
	    	//LOG.debug("returing -1");
	    	return -1;
	    }else if(assetCategory1.getCategoryId() > assetCategory2.getCategoryId()){
	    	//LOG.debug("returing 1");
	    	return 1;
	    }else{
	    	//LOG.debug("returing 0");
	    	return 0;
	    }
	    
	    }
}

