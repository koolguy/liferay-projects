/*
 * This code is property of Teach Learn Web, Inc. Use, duplication and disclosure
 * in any form without permission of copyright holder is prohibited.
 *
 * (C) Copyright Teach Learn Web, Inc. 2012. All rights reserved.
 */
package com.tlw.topics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.xml.namespace.QName;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portlet.asset.model.AssetCategory;
import com.tlw.controller.AbstractBaseController;
import com.tlw.registration.mvc.vo.RegistrationModelBean;
import com.tlw.util.WebUtil;


/**
 * The Class RegistrationController, is responsible to handle the user
 * registration request.
 * 
 * @author asharma
 * @author arvindk
 */
@Controller("topicsController")
@Scope("session")
@RequestMapping(value = "VIEW")
public class TopicsController extends AbstractBaseController {

	/** The LOG. */
	private static final Logger LOG = Logger.getLogger(TopicsController.class);

	/** The errormsg key. */
	private static final String ERROR_MSG_KEY = "errormsg";
	
	List<AssetCategory> rootCategoryList =  null;
	String level1ChildLabel = null;
	String level2ChildLabel = null;

	/**
	 * Show registration.
	 * 
	 * @param renderResponse
	 *            the render response
	 * @return the string
	 */
	@RenderMapping
	public ModelAndView showSubjects(RenderRequest renderRequest, RenderResponse renderResponse) {
		
		LOG.debug("called showSubjects rendermapping.----.."+renderRequest.getPortletSession());		
		
				
		Map<String, Object> model = new HashMap<String, Object>();    	
		model.put("rootCategoryList", rootCategoryList);
		model.put("level1ChildLabel", level1ChildLabel);
		model.put("level2ChildLabel", level2ChildLabel);
		
		LOG.debug("after called showSubjects rendermapping...");
		return new ModelAndView("showTopics",model);
	}
	
	
	/**
	 * Gets the command object.
	 * 
	 * @return the command object
	 */
	
	 @ModelAttribute("rootCategoryList")
	 public List<AssetCategory> getCommandObject(PortletRequest request) {
		LOG.debug("Creating object of model RegistrationModelBean.."+ request.getPortletSession());
		LOG.debug("making level1ChildLabel s as null");
		level1ChildLabel = null;
		level2ChildLabel = null;

		LOG.debug("SubjectId from Session : "
				+ request.getPortletSession().getAttribute("subject_subjectId",
						PortletSession.APPLICATION_SCOPE));
		if (null != request.getPortletSession().getAttribute("subject_subjectId",
				PortletSession.APPLICATION_SCOPE)) {
			String subjectId = request.getPortletSession()
					.getAttribute("subject_subjectId", PortletSession.APPLICATION_SCOPE)
					.toString();
			LOG.debug("subject_subjectId" + subjectId);
			rootCategoryList = new ArrayList<AssetCategory>();
			AssetCategory assetCategory = WebUtil.getAssetCategoryById(Long
					.parseLong(subjectId));
			LOG.debug("assetCategory ....Name....Topics "
					+ assetCategory.getName());
			rootCategoryList.add(assetCategory);
			rootCategoryList = WebUtil.removeDuplicates(rootCategoryList);
		}

		return rootCategoryList;
	}
	

	
	
	@ActionMapping(params = "action=startCourse")
	public void subscribeCourse(
			@RequestParam Long subjectId,
			@ModelAttribute RegistrationModelBean registrationBean,
			BindingResult bindingResult, ActionRequest actionRequest,
			ActionResponse actionResponse, SessionStatus sessionStatus) throws PortalException, SystemException {
		
		LOG.info("Request received to Start Course the user...");
		LOG.debug("subjectId.................."+subjectId);
		//serviceLocator.getUserService().addCourse(WebUtil.getUserId(actionRequest),subjectId);
		
	}
	
	@EventMapping(value = "{http:teachlearnweb.com/events}subjectId")
	public void fetchSubjectId(EventRequest eventRequest, EventResponse eventResponse, Model model){
		Event event = eventRequest.getEvent();
		
		level1ChildLabel = null;
		LOG.debug("In Process event ********"+Long.parseLong((String) event.getValue()));
		LOG.debug("event value *****************"+event.getValue());
		rootCategoryList = new ArrayList<AssetCategory>();
		AssetCategory assetCategory = WebUtil.getAssetCategoryById(Long.parseLong((String) event.getValue()));
		LOG.debug("assetCategory ....Name....Topics: "+assetCategory.getName());
		rootCategoryList.add(assetCategory);
		rootCategoryList = WebUtil.removeDuplicates(rootCategoryList);
		model.addAttribute("rootCategoryList", rootCategoryList);
		
		// set default right menu to be highlighted
		level1ChildLabel = "Overview";
		eventResponse.setRenderParameter("render","showTopics");
		
	}
	
	@RenderMapping(params = "render=showTopics")
	public ModelAndView showTopics(RenderResponse renderResponse) {
		LOG.debug("In render show topics **** just rendering");
		
		Map model = new HashMap();    	
		model.put("rootCategoryList", rootCategoryList);		
		return new ModelAndView("showTopics",model);
		
		}
	
	@ActionMapping(params = "action=showCourse")
	public void showCourse(
			@RequestParam Long topicsId,
			@ModelAttribute RegistrationModelBean registrationBean,
			BindingResult bindingResult, ActionRequest actionRequest,
			ActionResponse actionResponse, SessionStatus sessionStatus) throws PortalException, SystemException {
		
		HashMap topicsMap = new HashMap();
		String level1Child = actionRequest.getParameter("level1Child");
		String level2Child = actionRequest.getParameter("level2Child");
		String showTopicsLabel = "";
		
		if(null != level1Child){
			showTopicsLabel = level1Child + ": ";
		}
		if(null != level2Child){
			showTopicsLabel = showTopicsLabel + level2Child;
		}
		
		LOG.info("Request received to Show Course the user...");
		LOG.info("level1Child :"+level1Child);
		LOG.info("level2Child :"+level2Child);
		
		level1ChildLabel = level1Child;
		level2ChildLabel = level2Child;
		LOG.info("Request received to Show Course the user...");
		//javax.xml.namespace.QName qName = new QName("http:teachlearnweb.com/events", "topicsId", "x");
		javax.xml.namespace.QName qName = new QName("http:teachlearnweb.com/events", "topicsMap", "x");
		actionRequest.getPortletSession().setAttribute("topics_level1Child", level1Child,PortletSession.APPLICATION_SCOPE);
		actionRequest.getPortletSession().setAttribute("topics_level2Child", level2Child,PortletSession.APPLICATION_SCOPE);
		actionRequest.getPortletSession().setAttribute("subject_defaultLoading", "FALSE",PortletSession.APPLICATION_SCOPE);
		LOG.debug("topicsId.................."+topicsId);
	//	actionResponse.setRenderParameter("level1ChildLabel", level1Child);
	//	actionResponse.setRenderParameter("level2ChildLabel", level2Child);
		
		topicsMap.put("parentTopicName", level1Child);
		topicsMap.put("subTopicName", level2Child);
		topicsMap.put("topicsId", String.valueOf(topicsId));
		
		//actionResponse.setEvent(qName, String.valueOf(topicsId));
		actionResponse.setEvent(qName, topicsMap);
		
		
	}

	
}
