/*
 * This code is property of BrightSky, Inc. Use, duplication and disclosure
 * in any form without permission of copyright holder is prohibited.
 *
 * (C) Copyright BrightSky, Inc. 2011. All rights reserved.
 */
package com.tlw.topics.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import com.liferay.portlet.asset.model.AssetCategory;

/**
 * This is a model managed bean that represents a user that is
 * registering with Teach Learn Web.
 * 
 * @author vissum
 */
public class TopicsModelBean implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4047585584918603376L;
    
    private List<AssetCategory> rootCategoryList = new ArrayList<AssetCategory>();

	public List<AssetCategory> getRootCategoryList() {
		return rootCategoryList;
	}

	public void setRootCategoryList(List<AssetCategory> rootCategoryList) {
		this.rootCategoryList = rootCategoryList;
	}
    

}
