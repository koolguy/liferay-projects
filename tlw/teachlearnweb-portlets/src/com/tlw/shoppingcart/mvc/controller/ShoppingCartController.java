/*
 * This code is property of Teach Learn Web, Inc. Use, duplication and disclosure
 * in any form without permission of copyright holder is prohibited.
 *
 * (C) Copyright Teach Learn Web, Inc. 2012. All rights reserved.
 */
package com.tlw.shoppingcart.mvc.controller;

import java.util.HashMap;
import java.util.Map;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.tlw.controller.AbstractBaseController;
import com.tlw.shoppingcart.mvc.vo.CartVO;
import com.tlw.util.WebUtil;


/**
 * The Class RegistrationController, is responsible to handle the user
 * registration request.
 * 

 * @author Srini G 
 */
@Controller("value=shoppingCartController")
@Scope("session")
@RequestMapping(value = "VIEW")
public class ShoppingCartController extends AbstractBaseController {

	/** The LOG. */
	private static final Logger LOG = Logger
			.getLogger(ShoppingCartController.class);

	/** The errormsg key. */
	private static final String ERROR_MSG_KEY = "errormsg";

	/*@Autowired
	@Qualifier("shippingFormValidator")
	private Validator shippingFormValidator;
*/
	/**
	 * Show registration.
	 * 
	 * @param renderResponse
	 *            the render response
	 * @return the string
	 */
	 @RenderMapping
	public String showCart(RenderRequest renderRequest, RenderResponse renderResponse) {
		 
		 HttpServletRequest servletreq = PortalUtil.getHttpServletRequest(renderRequest);
		 HttpSession session = PortalUtil.getOriginalServletRequest(servletreq).getSession();
			//PortletSession session = actionRequest.getPortletSession();
			Map cartItemsMap = (Map) session.getAttribute("cartItems");
			if(null!=cartItemsMap)
				LOG.debug("cartItemsMap........................"+cartItemsMap.size());
			renderRequest.setAttribute("cartItems",cartItemsMap);
			renderRequest.setAttribute("totalCartAmount",WebUtil.getTotalCartAmount(cartItemsMap));
    	return "view";
    }

	/**
	 * Gets the command object.
	 * 
	 * @return the command object
	 
	@ModelAttribute("registrationModelBean")
	public RegistrationModelBean getCommandObject() {
		LOG.debug("Creating object of model RegistrationModelBean..");
		RegistrationModelBean registrationModelBean = new RegistrationModelBean();
		/*
		 * FaceBookService facebookService =
		 * serviceLocator.getFaceBookService();
		 * registrationModelBean.setFacebookCancelUrl(facebookService
		 * .getOnCancelRedirectionUrl());
		 
		return registrationModelBean;
	}*/
	/**
	 * Gets the command object.
	 * 
	 * @return the command object
	 */
	
	/**
	 * Gets the command object.
	 * 
	 * @return the command object
	 
	@ModelAttribute("getCategories")
	public List<AssetCategory> getCategories(PortletRequest request) {
		String vocabularyName  = "Course Management";			
		List<AssetCategory> assetCategoryList = WebUtil.getRootCategories(vocabularyName, request);	
		
		return assetCategoryList;
	}
	@ActionMapping(params = "action=doRegistration")
	public void doRegistration(
			@ModelAttribute RegistrationModelBean registrationBean,
			BindingResult bindingResult, ActionRequest actionRequest,
			ActionResponse actionResponse, SessionStatus sessionStatus) throws PortalException, SystemException {
		
		LOG.info("Request received to register the user...");
		registrationFormValidator.validate(registrationBean, bindingResult);	
	
		long scopeGroupId = WebUtil.getThemeDisplay(actionRequest).getScopeGroupId();
		registrationBean.setDuplicateEmail(false);
		registrationBean.setShowForm(true);
		if (!bindingResult.hasErrors()) {
			
			User rBean = populateRegistrationModelBean(registrationBean, actionRequest);
			
			User reigisteredUser = null;
			
			try{
				reigisteredUser = serviceLocator.getUserService().addUser(rBean,WebUtil.getUserId(actionRequest),WebUtil.getCompanyId(actionRequest),scopeGroupId);
			}catch (Exception e) {
				if (e instanceof DuplicateUserEmailAddressException)
				{
					registrationBean.setDuplicateEmail(true);
				}
			}
			
			if(null != reigisteredUser){
				registrationBean.setSuccess(true);
				registrationBean.setShowForm(false);
			}
			//LOG.info("reigisteredUser UserId...................................................................................................................."+reigisteredUser.getUserId());
        }
		
		
	}
	*/
	

	/*private User populateRegistrationModelBean(
			RegistrationModelBean registrationBean, ActionRequest actionRequest) {
		com.teachlearnweb.service.vo.User rBean = new com.teachlearnweb.service.vo.User();
		try{
		BeanUtils.copyProperties(registrationBean, rBean);
		rBean.setLocale(WebUtil.getThemeDisplay(actionRequest).getLocale());
		/*SimpleDateFormat format = new SimpleDateFormat();
		Date myDate = format.parse(registrationBean.getDob());
		rBean.setDay(String.valueOf(myDate.getDay()));
		rBean.setMonth(String.valueOf(myDate.getMonth()));
		rBean.setYear(String.valueOf(myDate.getYear()));*/
		/*}catch(Exception exception){
			exception.printStackTrace();
		}
		return rBean;
	}*/
		 
		@ResourceMapping("getCartItems")
		public ModelAndView getCartItems(ResourceRequest resourceRequest)
				throws PortalException, SystemException {
			
			HttpServletRequest servletreq = PortalUtil.getHttpServletRequest(resourceRequest);
			HttpSession session = PortalUtil.getOriginalServletRequest(servletreq).getSession();
			//PortletSession session = actionRequest.getPortletSession();
			Map<Long,CartVO> cartItemsMap = (Map<Long,CartVO>) session.getAttribute("cartItems");
			//LOG.debug("cartItemsMap........................"+cartItemsMap.size());
			//LOG.debug("...................."+couponDetail.getCouponCode());
			
			Map model = new HashMap();
			model.put("cartItems", cartItemsMap);
			model.put("totalCartAmount", WebUtil.getTotalCartAmount(cartItemsMap));
			return new ModelAndView("showCartItems",model);
		}
		@ResourceMapping("removeCartItem")
		public ModelAndView removeCartItem(ResourceRequest resourceRequest)
				throws PortalException, SystemException {
			Long catgoryId = Long.valueOf(resourceRequest.getParameter("categoryId"));
			LOG.debug("...................catgoryId"+catgoryId);
			HttpServletRequest servletreq = PortalUtil.getHttpServletRequest(resourceRequest);
			HttpSession session = PortalUtil.getOriginalServletRequest(servletreq).getSession();
			//PortletSession session = actionRequest.getPortletSession();
			Map<Long,CartVO> cartItemsMap = (Map<Long,CartVO>) session.getAttribute("cartItems");
			LOG.debug("...................."+cartItemsMap);
			if(cartItemsMap!= null){
				LOG.debug(".before..................."+cartItemsMap.size());
				LOG.debug(".before..................."+cartItemsMap.containsKey(catgoryId));
				cartItemsMap.remove(catgoryId);
				LOG.debug("..after.................."+cartItemsMap.size());
			}
			session.setAttribute("cartItems",cartItemsMap);

			Map model = new HashMap();
			model.put("totalCartAmount", WebUtil.getTotalCartAmount(cartItemsMap));
			model.put("cartItems", cartItemsMap);
			return new ModelAndView("showCartItems",model);
		}

}
