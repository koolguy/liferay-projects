package com.tlw.shoppingcart.mvc.vo;
import java.io.Serializable;

import com.liferay.portlet.asset.model.AssetCategory;
public class CartVO implements Serializable{
	private String itemType;
	private long categoryId;
	private AssetCategory category;
	private double price;
	private String parentCategory;
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getParentCategory() {
		return parentCategory;
	}
	public void setParentCategory(String parentCategory) {
		this.parentCategory = parentCategory;
	}
	public String getItemType() {
		return itemType;
	}
	public void setItemType(String itemType) {
		this.itemType = itemType;
	}
	public long getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}
	public AssetCategory getCategory() {
		return category;
	}
	public void setCategory(AssetCategory category) {
		this.category = category;
	}

}
