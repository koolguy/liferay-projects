package com.tlw.util;

public class Constants {

	public static final String TLW_USER = "tlwUser";
	public static final String MYPROFILE_JSP = "myProfile";
	public static final String TESTIMONIALS_JSP = "testmonials";
	public static final String PROFILE_IMAGE_JSP = "profileImage";
	public static final String EDIT_PROFILE_IMAGE_JSP = "editProfileImage";
	public static final int  MINUTE_PER_QUESTION_FACTOR = 1;
	public static final int PASSWORD_MIN_LENGTH = 8;
	public static final String STATUS_PENDING = "pending";
	public static final String STATUS_APPROVED = "approved";
	public static final String STATUS_DECLINED = "declined";
	

}
