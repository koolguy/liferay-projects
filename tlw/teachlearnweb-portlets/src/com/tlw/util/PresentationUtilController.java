/*
 * This code is property of Teach Learn Web, Inc. Use, duplication and disclosure
 * in any form without permission of copyright holder is prohibited.
 *
 * (C) Copyright Teach Learn Web, Inc. 2012. All rights reserved.
 */
package com.tlw.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderResponse;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portlet.asset.model.AssetCategory;
import com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil;
import com.tlw.controller.AbstractBaseController;

/**
 * The Class RegistrationController, is responsible to handle the user
 * registration request.
 * 
 * @author asharma
 * @author arvindk
 */
@Controller("value=presentationUtilController")
@RequestMapping(value = "VIEW")
public class PresentationUtilController extends AbstractBaseController {

	/** The LOG. */
	private static final Logger LOG = Logger
			.getLogger(PresentationUtilController.class);

	PresentationModelBean presentationModelBean = null;
	/**
	 * Show registration.
	 * 
	 * @param renderResponse
	 *            the render response
	 * @return the string
	 */
	@RenderMapping
	public String showPrensentationUtil(RenderResponse renderResponse) {
		
		return "PresentationUtilForm";
	}
	
	/**
	 * Show registration.
	 * 
	 * @param renderResponse
	 *            the render response
	 * @return the string
	 */
	/*@RenderMapping(params = "action=showChangePassword")
	public String showChangePassword(RenderResponse renderResponse) {
		LOG.debug("change password..........");
		return "changePassword";
	}
*/
	/**
	 * Gets the command object.
	 * 
	 * @return the command object
	 */
	/*@ModelAttribute("registrationModelBean")
	public RegistrationModelBean getCommandObject() {
		LOG.debug("Creating object of model RegistrationModelBean..");
		RegistrationModelBean registrationModelBean = new RegistrationModelBean();
		
		 * FaceBookService facebookService =
		 * serviceLocator.getFaceBookService();
		 * registrationModelBean.setFacebookCancelUrl(facebookService
		 * .getOnCancelRedirectionUrl());
		 
		return registrationModelBean;
	}*/
	
	/**
	 * Gets the command object.
	 * 
	 * @return the command object
	 */
	@ModelAttribute("getSyllabus")
	public List<AssetCategory> getSyllabus() {		
		
		LOG.debug("getSyllabus invoked");
		long categoryIdOfSyllabus =  0; 
		List<AssetCategory> syllabusList = null;
		try {
			syllabusList = AssetCategoryLocalServiceUtil
										.getChildCategories(categoryIdOfSyllabus);
			
			syllabusList = WebUtil.removeDuplicates(syllabusList);
			
			LOG.debug("syllabusList "+syllabusList);
	
		}catch (Exception e) {
		  LOG.error(e);
		}
		
		return syllabusList;
		}
	
	/**
	 * Gets the command object.
	 * 
	 * @return the command object
	 */
	@ModelAttribute("getclasses")
	public List<AssetCategory> getclasses() {
	
		List<AssetCategory> classesListMain = new ArrayList<AssetCategory>();
		
		LOG.debug("getClasses invoked");
		long categoryIdOfSyllabus =  0; 
		List<AssetCategory> syllabusList = null;
		List<AssetCategory> classesList = null;
		List<AssetCategory> subjectList = null;
		List<AssetCategory> topicsList = null;
		List<AssetCategory> subTopicsList = null;
		
		int syllabusListCount = 0;
		int classesListCount = 0;
		int subjectListCount = 0;
		int topicsListCount = 0;
		int subTopicsListCount = 0;
		
		
		try {
			syllabusList = AssetCategoryLocalServiceUtil
			.getChildCategories(categoryIdOfSyllabus);
			
			syllabusList = WebUtil.removeDuplicates(syllabusList);
			LOG.debug("--syllabus--START "+syllabusList.size());
			
			
	for (Iterator iterator = syllabusList.iterator(); iterator.hasNext();) {
				AssetCategory syllabusCategory = (AssetCategory) iterator.next();
				LOG.debug("--syllabusId--"+syllabusCategory.getCategoryId()+"--syllabusName--"+syllabusCategory.getName());	
				
				 syllabusListCount++;
				
			classesList = AssetCategoryLocalServiceUtil
				.getChildCategories(syllabusCategory.getCategoryId());	
			
			classesList = WebUtil.removeDuplicates(classesList);
			
		for (Iterator iterator0 = classesList.iterator(); iterator0.hasNext();) {
			AssetCategory classCategory = (AssetCategory) iterator0.next();
			LOG.debug("--classID--"+classCategory.getCategoryId()+"--className--"+classCategory.getName());
			classesListMain.add(classCategory);		
			
			classesListCount++;
			
			subjectList = AssetCategoryLocalServiceUtil
			.getChildCategories(classCategory.getCategoryId());
			
			subjectList = WebUtil.removeDuplicates(subjectList);
			
			
			for (Iterator iterator2 = subjectList.iterator(); iterator2.hasNext();) {
				AssetCategory subjectCategory = (AssetCategory) iterator2.next();
				LOG.debug("--SubjectID--"+subjectCategory.getCategoryId()+"--SubjectName--"+subjectCategory.getName());	
				
				topicsList = AssetCategoryLocalServiceUtil
				.getChildCategories(subjectCategory.getCategoryId());
				
				subjectListCount++;
				
				topicsList = WebUtil.removeDuplicates(topicsList);
				
				for (Iterator iterator3 = topicsList.iterator(); iterator3.hasNext();) {
					AssetCategory topicsCategory = (AssetCategory) iterator3.next();
					LOG.debug("--TopicID--"+topicsCategory.getCategoryId()+"--TopicName--"+topicsCategory.getName());

					topicsListCount++;
					
					subTopicsList = AssetCategoryLocalServiceUtil
					.getChildCategories(topicsCategory.getCategoryId());
					
					
					subTopicsList = WebUtil.removeDuplicates(subTopicsList);
					
					for (Iterator iterator4 = subTopicsList.iterator(); iterator4.hasNext();) {
						AssetCategory subTopicsCategory = (AssetCategory) iterator4.next();
						LOG.debug("--SubTopicID--"+subTopicsCategory.getCategoryId()+"--SubTopicName--"+subTopicsCategory.getName());	
						subTopicsListCount++;
				}	
		}			
				LOG.debug("--SubTopics--END");
			}	
			LOG.debug("--Topics--END");
		}	
			LOG.debug("--classes--END");			
	}
	
	LOG.debug("Syllabus Count: "+syllabusListCount);
	LOG.debug("Class Count: "+classesListCount);
	LOG.debug("subjectListCount: "+subjectListCount);
	LOG.debug("topicsListCount: "+topicsListCount);
	LOG.debug("subTopicsListCount: "+subTopicsListCount);
	
	
	
	
	
	
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		
		LOG.debug("classesList :"+classesListMain);
		
		classesListMain = WebUtil.removeDuplicates(classesListMain);
		LOG.debug("classesList 2 :"+classesListMain);
		
		return classesListMain;
	}
	/**
	 * Gets the command object.
	 * 
	 * @return the command object
	 */
	/*@ModelAttribute("getCategories")
	public List<AssetCategory> getCategories(PortletRequest request) {
		LOG.debug("getCategories invoked");
		String vocabularyName  = "Course Management";			
		List<AssetCategory> assetCategoryList = WebUtil.getRootCategories(vocabularyName, request);	
		
		
		for (Iterator iterator = assetCategoryList.iterator(); iterator
				.hasNext();) {
			AssetCategory assetCategory = (AssetCategory) iterator.next();
			LOG.debug("assetCategory--->"+assetCategory.getName());
			LOG.debug("assetCategory--->"+assetCategory.getCategoryId());
			
		}
		
		
		return assetCategoryList;
	}*/
	
	/**
	 * Gets the command object.
	 * 
	 * @return the command object
	 */
	@ModelAttribute("registrationModelBean")
	public PresentationModelBean getCommandObject() {
		LOG.debug("Creating object of model PresentationModelBean..");
		
		if(null == presentationModelBean){
			 presentationModelBean = new PresentationModelBean();
		}
		
		LOG.debug("PresentationModelBean:"+presentationModelBean);
		/*
		 * FaceBookService facebookService =
		 * serviceLocator.getFaceBookService();
		 * registrationModelBean.setFacebookCancelUrl(facebookService
		 * .getOnCancelRedirectionUrl());
		 */
		return presentationModelBean;
	}
	
	
	@ActionMapping(params = "action=doPresentation")
	public void doPresentation(
			@ModelAttribute PresentationModelBean modelBean,
			BindingResult bindingResult, ActionRequest actionRequest,
			ActionResponse actionResponse, SessionStatus sessionStatus) throws PortalException, SystemException {
			
		presentationModelBean = new PresentationModelBean();
		
		presentationModelBean.setClassId(modelBean.getClassId());
		presentationModelBean.setTopicId(modelBean.getTopicId());
		presentationModelBean.setSubTopicId(modelBean.getSubTopicId());
		
		LOG.debug("classId: "+modelBean.getClassId() );
		LOG.debug("TopicId: "+modelBean.getTopicId());
		LOG.debug("SubTopicId: "+modelBean.getSubTopicId());
		
		Map model = new HashMap();
		//model.put("registrationModelBean", presentationModelBean);
		
		
		
	}
}
