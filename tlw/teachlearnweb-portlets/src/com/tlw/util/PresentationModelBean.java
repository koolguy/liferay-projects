/*
 * This code is property of BrightSky, Inc. Use, duplication and disclosure
 * in any form without permission of copyright holder is prohibited.
 *
 * (C) Copyright BrightSky, Inc. 2011. All rights reserved.
 */
package com.tlw.util;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Locale;
import java.util.TimeZone;

/**
 * This is a model managed bean that represents a user that is
 * registering with Teach Learn Web.
 * 
 * @author vissum
 */
public class PresentationModelBean implements Serializable {

	 /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4047585584918603376L;

    
    private String syllabusId = null;
    
    public String getSyllabusId() {
		return syllabusId;
	}

	public void setSyllabusId(String syllabusId) {
		this.syllabusId = syllabusId;
	}

	private String classId = null;
    
    private String topicId = null;
    
    private String subTopicId = null;

	public String getClassId() {
		return classId;
	}

	public void setClassId(String classId) {
		this.classId = classId;
	}

	public String getTopicId() {
		return topicId;
	}

	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}

	public String getSubTopicId() {
		return subTopicId;
	}

	public void setSubTopicId(String subTopicId) {
		this.subTopicId = subTopicId;
	}
    
    
    
}
