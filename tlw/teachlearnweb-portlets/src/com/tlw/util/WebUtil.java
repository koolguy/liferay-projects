/*
 * This code is property of BrightSky, Inc. Use, duplication and
 * disclosure
 * in any form without permission of copyright holder is prohibited.
 * (C) Copyright BrightSky, Inc. 2011. All rights reserved.
 */
package com.tlw.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.util.WebUtils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.FileVersion;
import com.liferay.portal.model.Country;
import com.liferay.portal.model.Role;
import com.liferay.portal.service.CountryServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.RoleServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.asset.AssetRendererFactoryRegistryUtil;
import com.liferay.portlet.asset.model.AssetCategory;
import com.liferay.portlet.asset.model.AssetCategoryProperty;
import com.liferay.portlet.asset.model.AssetEntry;
import com.liferay.portlet.asset.model.AssetRenderer;
import com.liferay.portlet.asset.model.AssetRendererFactory;
import com.liferay.portlet.asset.model.AssetVocabulary;
import com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetCategoryPropertyServiceUtil;
import com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetVocabularyServiceUtil;
import com.liferay.portlet.asset.service.persistence.AssetEntryQuery;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.model.DLFileEntryConstants;
import com.liferay.portlet.documentlibrary.model.DLFileShortcut;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.portlet.journal.model.JournalArticle;
import com.teachlearnweb.service.ServiceLocator;
import com.teachlearnweb.service.vo.User;
import com.tlw.catalogmanagement.mvc.vo.CategoryVO;
import com.tlw.shoppingcart.mvc.vo.CartVO;

/**
 * The Class WebUtil.
 * 
 * @author vvisvesw
 */
public class WebUtil {
	/** The Constant LOG. */
	private static final Log log = LogFactory.getLog(WebUtil.class);

	/** The ctx. */
	private static ApplicationContext ctx = null;

	/**
	 * Gets the context.
	 * 
	 * @param request
	 *            the request
	 * @return the context
	 */
	public static ApplicationContext getContext(HttpServletRequest request) {
		if (ctx == null) {
			ServletContext servletContext = request.getSession()
					.getServletContext();
			ctx = WebApplicationContextUtils
					.getWebApplicationContext(servletContext);
		}
		return ctx;
	}

	public static ThemeDisplay getThemeDisplay(PortletRequest portletRequest) {
		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest
				.getAttribute(com.liferay.portal.kernel.util.WebKeys.THEME_DISPLAY);
		return themeDisplay;
	}

	/**
	 * Gets the user id.
	 * 
	 * @param request
	 *            the request
	 * @return the user id
	 */
	public static long getUserId(PortletRequest request) {
		return PortalUtil.getUserId(request);
	}

	/**
	 * Returns the company Id associated with the community that is hosting the
	 * portlet associated with the current JSF FacesContext.
	 * 
	 * @return the company id
	 * @throws PortalException
	 *             the portal exception
	 * @throws SystemException
	 *             the system exception
	 */
	public static long getCompanyId(PortletRequest request)
			throws PortalException, SystemException {
		return PortalUtil.getCompanyId(request);
	}

	/**
	 * Gets the user.
	 * 
	 * @param portletRequest
	 *            the portlet request
	 * @return the user
	 */

	public static User getUser(PortletRequest portletRequest) {

		User user = null;
		try {
			user = getUser(getCompanyId(portletRequest),
					PortalUtil.getHttpServletRequest(portletRequest));
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}

	/**
	 * Gets the user.
	 * 
	 * @param request
	 *            the request
	 * @return the user
	 */
	public static User getUser(long companyId, HttpServletRequest request) {
		User user = null;
		Object userObj = WebUtils.getSessionAttribute(request,
				Constants.TLW_USER);
		log.info("Retrieved Bright sky user from session:" + userObj);
		if (userObj == null) {
			long userId = PortalUtil.getUserId(request);
			log.info("As session user object is null so getting from DB:"
					+ userId);
			ServiceLocator serviceLocator = getContext(request).getBean(
					"serviceLocator", ServiceLocator.class);
			user = serviceLocator.getUserService().findUserByUserId(companyId,
					userId);
			WebUtils.setSessionAttribute(request, Constants.TLW_USER, user);
		} else {
			user = (User) userObj;
		}
		return user;
	}

	public static User getUserByUserId(long userId, HttpServletRequest request) {

		ServiceLocator serviceLocator = getContext(request).getBean(
				"serviceLocator", ServiceLocator.class);
		User user = serviceLocator.getUserService().findUserByUserId(
				PortalUtil.getCompanyId(request), userId);

		return user;
	}

	/**
	 * Update UserSession.
	 * 
	 * @param request
	 *            the request
	 * 
	 */
	public static void updateUserSession(long companyId,
			HttpServletRequest request) {
		try {
			User user = null;
			long userId = PortalUtil.getUserId(request);
			ServiceLocator serviceLocator = WebUtil.getContext(request)
					.getBean("serviceLocator", ServiceLocator.class);
			user = serviceLocator.getUserService().findUserByUserId(companyId,
					userId);
			WebUtils.setSessionAttribute(request, Constants.TLW_USER, user);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Gets the user.
	 * 
	 * @param request
	 *            the request
	 * @return the user
	 */
	/*
	 * public static User getUser(long companyId, HttpServletRequest request) {
	 * User user = null; Object userObj = WebUtils.getSessionAttribute(request,
	 * Constants.TLW_USER); LOG.info("Retrieved Bright sky user from session:" +
	 * userObj); if (userObj == null) { long userId =
	 * PortalUtil.getUserId(request);
	 * LOG.info("As session user object is null so getting from DB:" + userId);
	 * ServiceLocator serviceLocator = getContext(request).getBean(
	 * "serviceLocator", ServiceLocator.class); user =
	 * serviceLocator.getUserService().findUserByUserId(companyId, userId);
	 * WebUtils.setSessionAttribute(request, Constants.TLW_USER, user); } else {
	 * user = (User) userObj; } return user; }
	 */

	/**
	 * Gets Asset Categories based on Vocabulary Name.
	 * 
	 * @param portletRequest
	 * @param portletResponse
	 * @param vocabularyName
	 * @return List<AssetCategory>
	 * @throws SystemException
	 */
	public static List<CategoryVO> getAllAssetCategoriesByVocabulary(
			PortletRequest portletRequest, PortletResponse portletResponse,
			String vocabularyName) throws SystemException {

		ThemeDisplay themeDisplay = getThemeDisplay(portletRequest);
		List<CategoryVO> categoryList = null;
		List<AssetVocabulary> vocabularies = null;
		List<AssetCategory> assetCategoryList = null;
		vocabularies = AssetVocabularyServiceUtil.getGroupVocabularies(
				themeDisplay.getLayout().getGroupId(), vocabularyName, -1, -1,
				null);
		if (vocabularies != null && vocabularies.size() > 0) {
			// get the first and only vocabulary
			AssetVocabulary vocabulary = vocabularies.get(0);
			log.debug("vocabulary Name..............."
					+ vocabulary.getName());
			assetCategoryList = AssetCategoryLocalServiceUtil
					.getVocabularyCategories(vocabulary.getVocabularyId(), -1,
							-1, null);
		}
		categoryList = new ArrayList<CategoryVO>();
		Map subCategoriesMap = new HashMap();
		Map categoriesMap = new HashMap();
		CategoryVO categoryVO = null;
		List<AssetCategory> subCategoryList = null;
		List<AssetCategory> firstLevelCategories = new ArrayList<AssetCategory>();
		categoryVO = new CategoryVO();

		for (AssetCategory category : assetCategoryList) {
			if (category.getParentCategoryId() == 0) {
				firstLevelCategories.add(category);
			}
			categoriesMap.put(category.getCategoryId(), category);
			subCategoryList = new ArrayList<AssetCategory>();
			// categoryVO.setCategory(categor

			for (AssetCategory subCategory : assetCategoryList) {
				if (subCategory.getParentCategoryId() == category
						.getCategoryId()) {
					subCategoryList.add(subCategory);
				}
			}
//			log.debug("---------SubcategoryList"
//					+ subCategoryList.size());
//			log.debug("---------SubcategoryList"
//					+ category.getCategoryId());
			subCategoriesMap.put(category.getCategoryId(), subCategoryList);
		}
		log.debug("---------Subcategorymap size"
				+ subCategoriesMap.size());
		categoryVO.setSubCategoriesMap(subCategoriesMap);
		categoryVO.setCategoriesMap(categoriesMap);
		categoryVO.setCategory(firstLevelCategories);
		categoryList.add(categoryVO);

		return categoryList;
	}

	public static AssetCategory getAssetCategoryById(long categoryId) {
		AssetCategory assetCategory = null;
		try {
			assetCategory = AssetCategoryLocalServiceUtil
					.getCategory(categoryId);
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return assetCategory;
	}

	public static List<AssetCategory> getChildCategories(long parentCategoryId,
			long vocabularyId) {

		List<AssetCategory> vocabularyCategories = null;
		try {
			/*log.debug("test"
					+ AssetCategoryLocalServiceUtil
							.getChildCategoriesCount(parentCategoryId));*/
			if (AssetCategoryLocalServiceUtil
					.getChildCategoriesCount(parentCategoryId) > 0) {

				vocabularyCategories = AssetCategoryLocalServiceUtil
						.getChildCategories(parentCategoryId);
			}

		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return vocabularyCategories;
	}

	public static List<AssetCategory> getChildCategoriesByVocabularyID(
			long parentCategoryId, long vocabularyId) {

		List<AssetCategory> vocabularyCategories = null;
		try {
			if (AssetCategoryLocalServiceUtil
					.getChildCategoriesCount(parentCategoryId) > 0) {
				vocabularyCategories = AssetCategoryLocalServiceUtil
						.getVocabularyCategories(parentCategoryId,
								vocabularyId, -1, -1, null);

			}

		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return vocabularyCategories;
	}

	/**
	 * Removes duplicate strings and returns list of unique strings
	 * 
	 * @param list
	 * @return List<String>
	 */
	public static List<AssetCategory> removeDuplicates(List<AssetCategory> list) {
		Set<AssetCategory> set = new HashSet<AssetCategory>();
		set.addAll(list);
		list = new ArrayList<AssetCategory>();
		list.addAll(set);
		return list;
	}

	/**
	 * Returns the list of Asset Entries for the given Asset Category.
	 * 
	 * @param assetCategory
	 * @param portletRequest
	 * @return List<AssetEntry>
	 */
	public static List<AssetEntry> fetchAssetEntries(long categoryId,
			PortletRequest portletRequest) {

		List<AssetEntry> assetEntryList = new ArrayList<AssetEntry>();
		long[] allCateogyIds = { categoryId };
		long[] classNameIds = {
				PortalUtil.getClassNameId(DLFileEntryConstants.getClassName()),
				PortalUtil.getClassNameId(DLFileShortcut.class.getName()),
				PortalUtil.getClassNameId(JournalArticle.class) };
		AssetEntryQuery assetEntryQuery = new AssetEntryQuery();
		assetEntryQuery.setClassNameIds(classNameIds);
		assetEntryQuery.setStart(-1);
		assetEntryQuery.setEnd(-1);
		assetEntryQuery.setAllCategoryIds(allCateogyIds);
		try {
			List<AssetEntry> allAssetEntryList = AssetEntryLocalServiceUtil
					.getEntries(assetEntryQuery);
			//System.out.println("AssetEntry Size............."+allAssetEntryList.size());
			for (AssetEntry assetEntry : allAssetEntryList) {
				List<AssetCategory> categoryList = assetEntry.getCategories();
				for (AssetCategory category : categoryList) {
				/*	System.out.println("category.getCategoryId().............."+ category.getCategoryId());
					System.out.println("allCateogyIds[0].............."+ allCateogyIds[0]);*/
					/*log.debug("category.getCategoryId().............."
							+ category.getCategoryId());
					log.debug("allCateogyIds[0].............."
							+ allCateogyIds[0]);*/
					if (category.getCategoryId() == allCateogyIds[0]) {
						assetEntryList.add(assetEntry);
					}
				}
			}
			//System.out.println("After For loop AssetEntry Size............."+allAssetEntryList.size());
		} catch (SystemException e) {

		} catch (Exception e) {

		}

		return assetEntryList;
	}

	public HttpServletRequest getHttpServletRequest(
			PortletRequest portletRequest) {
		HttpServletRequest httpRequest = PortalUtil
				.getHttpServletRequest(portletRequest);
		httpRequest = PortalUtil.getOriginalServletRequest(httpRequest);
		return httpRequest;
	}

	public long getPlIdByPortletId(PortletRequest portletRequest,
			String portletID) {

		// Retrieve layout id of another portlet
		long plid = 0;
		try {
			plid = PortalUtil.getPlidFromPortletId(
					getThemeDisplay(portletRequest).getScopeGroupId(),
					portletID);
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return plid;
	}

	/**
	 * Returns the DLFileEntry Asset Entry ClassPK.
	 * 
	 * @param classPK
	 * @return DLFileEntry
	 */

	public static DLFileEntry fetchDLFileEntry(long classPK) {

		DLFileEntry dlFileEntry = null;

		try {
			dlFileEntry = DLFileEntryLocalServiceUtil.getFileEntry(classPK);
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}

		return dlFileEntry;

	}

	public static String getAmount(long categoryId) {

		List<AssetCategoryProperty> categoryProperties = null;
		try {
			categoryProperties = AssetCategoryPropertyServiceUtil
					.getCategoryProperties(categoryId);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String amount = null;

		for (AssetCategoryProperty categoryProperty : categoryProperties) {
			if ("amount".equalsIgnoreCase(categoryProperty.getKey())) {
				amount = categoryProperty.getValue();
				break;
			}
		}
		log.debug("amount........" + amount);
		return amount;
	}

	public static String getDiscount(long categoryId) {

		List<AssetCategoryProperty> categoryProperties = null;
		try {
			categoryProperties = AssetCategoryPropertyServiceUtil
					.getCategoryProperties(categoryId);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		String amount = null;

		for (AssetCategoryProperty categoryProperty : categoryProperties) {
			if ("discount".equalsIgnoreCase(categoryProperty.getKey())) {
				amount = categoryProperty.getValue();
				break;
			}
		}
		log.debug("amount........" + amount);
		return amount;
	}

	/**
	 * Method to convert long to int.
	 * 
	 * @param l
	 * @return int
	 */
	public static int convertLongToInt(long l) {
		if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
			throw new IllegalArgumentException(l
					+ " cannot be cast to int without changing its value.");
		}
		return (int) l;
	}

	public static List<AssetCategory> getRootCategories(String vocabularyName,
			PortletRequest portletRequest) {

		List<AssetVocabulary> vocabularies = null;
		List<AssetCategory> assetCategoryList = null;
		try {
			ThemeDisplay themeDisplay = getThemeDisplay(portletRequest);
			vocabularies = AssetVocabularyServiceUtil.getGroupVocabularies(
					themeDisplay.getLayout().getGroupId(), vocabularyName, -1,
					-1, null);
			if (vocabularies != null && vocabularies.size() > 0) {
				// get the first and only vocabulary
				AssetVocabulary vocabulary = vocabularies.get(0);
				assetCategoryList = AssetCategoryLocalServiceUtil
						.getVocabularyRootCategories(
								vocabulary.getVocabularyId(), -1, -1, null);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return assetCategoryList;
	}

	public static List<Country> getCountries() {

		List<Country> countryList = null;
		try {
			countryList = CountryServiceUtil.getCountries();

		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return countryList;
	}

	public static int getChildCategoriesCount(long parentCategoryId) {

		int count = 0;
		try {

			count = AssetCategoryLocalServiceUtil
					.getChildCategoriesCount(parentCategoryId);

		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return count;
	}

	public static int getNumberOfSubTopicsInSubject(long subjectCategoryId) {

		int topicsListCount = 0;
		int subTopicsListCount = 0;
		List<AssetCategory> topicsList;
		try {
			topicsList = AssetCategoryLocalServiceUtil
					.getChildCategories(subjectCategoryId);

			List<AssetCategory> subTopicsList =

			// subjectListCount++;
			topicsList = WebUtil.removeDuplicates(topicsList);

			for (Iterator iterator3 = topicsList.iterator(); iterator3
					.hasNext();) {
				AssetCategory topicsCategory = (AssetCategory) iterator3.next();
				// log.debug("--TopicID--" + topicsCategory.getCategoryId()
				// + "--TopicName--" + topicsCategory.getName());
				topicsListCount++;
				subTopicsList = AssetCategoryLocalServiceUtil
						.getChildCategories(topicsCategory.getCategoryId());
				subTopicsList = WebUtil.removeDuplicates(subTopicsList);

				for (Iterator iterator4 = subTopicsList.iterator(); iterator4
						.hasNext();) {
					AssetCategory subTopicsCategory = (AssetCategory) iterator4
							.next();
					// log.debug("--SubTopicID--"
					// + subTopicsCategory.getCategoryId()
					// + "--SubTopicName--" + subTopicsCategory.getName());
					subTopicsListCount++;
				}
			}

			log.info("topicsListCount...." + topicsListCount);
			log.info("subTopicsListCount...." + subTopicsListCount);
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return subTopicsListCount;
	}

	/**
	 * Gets user roles
	 * 
	 * @param portletRequest
	 * @return List<Role>
	 */
	public static List<Role> getUserRoles(PortletRequest portletRequest) {

		List<Role> roles = null;

		try {
			roles = RoleServiceUtil.getUserRoles(getUserId(portletRequest));
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return roles;
	}

	/**
	 * Checks if user has given role
	 * 
	 * @param portletRequest
	 * @param role
	 * @return boolean
	 */
	public static boolean checkRole(PortletRequest portletRequest, String role) {

		boolean isRole = false;

		com.liferay.portal.model.User user;
		try {
			user = UserLocalServiceUtil.getUserById(getUserId(portletRequest));
			isRole = RoleLocalServiceUtil.hasUserRole(
					getUserId(portletRequest), user.getCompanyId(), role, true);
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return isRole;
	}

	public static String toCamelCase(String s) {
		String[] parts = s.split("_");
		String camelCaseString = "";
		for (String part : parts) {
			camelCaseString = camelCaseString + toProperCase(part);
		}
		return camelCaseString;
	}

	public static String toProperCase(String s) {
		return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
	}
	public static Double getTotalCartAmount(Map cartsMap) {
		Double totalPrice = 0.00;
		if(null!=cartsMap){
		for(Object key:cartsMap.keySet()){
			CartVO vo = (CartVO)cartsMap.get(key);
			totalPrice = totalPrice + vo.getPrice();
		}
		}
		return totalPrice;
	}
	
	/**
	 * Returns the FileEntry Object based on ClassPK.
	 * @param classPK
	 * @param renderRequest
	 * @param renderResponse
	 * @return FileEntry
	 * @throws Exception
	 */
	public static FileEntry getFileEntry(long classPK, RenderRequest renderRequest, RenderResponse renderResponse) throws Exception {

		log.info("Start of getFileEntry() method of WebUtil::");
		AssetRendererFactory assetRendererFactory =
			AssetRendererFactoryRegistryUtil.getAssetRendererFactoryByClassName(DLFileEntryConstants.getClassName());
		FileEntry fileEntry = null;
		AssetRenderer assetRenderer;
		try {
			assetRenderer = assetRendererFactory.getAssetRenderer(classPK);
			assetRenderer.render(renderRequest, renderResponse, AssetRenderer.TEMPLATE_FULL_CONTENT);
			FileVersion fileVersion = (FileVersion) renderRequest.getAttribute("DOCUMENT_LIBRARY_FILE_VERSION");
			fileEntry = fileVersion.getFileEntry();			
		}
		catch (PortalException e) {
			log.info("WebUtil.getFileEntry() PortalException :::", e);
		}
		catch (SystemException e) {
			log.info("WebUtil.getFileEntry() SystemException :::", e);
		}
		log.info( "End of getFileEntry() method of WebUtil::");
		return fileEntry;
	}
	
	public static AssetCategory fetchAssetCategoryByCategoryId(long categoryId){
		AssetCategory assetCategory = null;
		try {
			assetCategory = AssetCategoryLocalServiceUtil.getAssetCategory(categoryId);
		} catch (PortalException e) {			
			e.printStackTrace();
		} catch (SystemException e) {			
			e.printStackTrace();
		}
		return assetCategory;
	}
	
	public static String getAssetCategoryTitle(String categoryId){
		String title = null;
		try {
			AssetCategory assetCategory = AssetCategoryLocalServiceUtil.getCategory(Long.valueOf(categoryId));
			title =  assetCategory.getName();
			System.out.println("title..."+title);
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return title;
	}
}