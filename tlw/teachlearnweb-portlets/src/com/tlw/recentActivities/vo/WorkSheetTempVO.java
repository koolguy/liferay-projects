package com.tlw.recentActivities.vo;

import java.io.Serializable;

/**
 * This class WorkSheetTempVO, is reponsible to to hold worksheet temp variables.
 * 
 * @author satya
 *
 */
public class WorkSheetTempVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5860658844755135831L;

	/**
	 * default constructor.
	 */
	public WorkSheetTempVO() {
		
	}
	
	public WorkSheetTempVO(long wstotlaRewardsPoints, String className,
			String subjectName, String wsStatus, String parentTopicName,
			String subTopicName) {
		super();
		this.wstotlaRewardsPoints = wstotlaRewardsPoints;
		this.className = className;
		this.subjectName = subjectName;
		this.wsStatus = wsStatus;
		this.parentTopicName = parentTopicName;
		this.subTopicName = subTopicName;
	}
	
	
	private long wstotlaRewardsPoints;
	private String className;
	private String subjectName;
	private String wsStatus;
	private String parentTopicName;
	
	public String getParentTopicName() {
		return parentTopicName;
	}
	public void setParentTopicName(String parentTopicName) {
		this.parentTopicName = parentTopicName;
	}
	public String getSubTopicName() {
		return subTopicName;
	}
	public void setSubTopicName(String subTopicName) {
		this.subTopicName = subTopicName;
	}
	private String subTopicName;
	
	public long getWstotlaRewardsPoints() {
		return wstotlaRewardsPoints;
	}
	public void setWstotlaRewardsPoints(long wstotlaRewardsPoints) {
		this.wstotlaRewardsPoints = wstotlaRewardsPoints;
	}
	public String getClassName() {
		return className;
	}
	public void setClassName(String className) {
		this.className = className;
	}
	public String getSubjectName() {
		return subjectName;
	}
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	public String getWsStatus() {
		return wsStatus;
	}
	public void setWsStatus(String wsStatus) {
		this.wsStatus = wsStatus;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((className == null) ? 0 : className.hashCode());
		result = prime * result
				+ ((parentTopicName == null) ? 0 : parentTopicName.hashCode());
		result = prime * result
				+ ((subTopicName == null) ? 0 : subTopicName.hashCode());
		result = prime * result
				+ ((subjectName == null) ? 0 : subjectName.hashCode());
		result = prime * result
				+ ((wsStatus == null) ? 0 : wsStatus.hashCode());
		result = prime * result
				+ (int) (wstotlaRewardsPoints ^ (wstotlaRewardsPoints >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WorkSheetTempVO other = (WorkSheetTempVO) obj;
		if (className == null) {
			if (other.className != null)
				return false;
		} else if (!className.equals(other.className))
			return false;
		if (parentTopicName == null) {
			if (other.parentTopicName != null)
				return false;
		} else if (!parentTopicName.equals(other.parentTopicName))
			return false;
		if (subTopicName == null) {
			if (other.subTopicName != null)
				return false;
		} else if (!subTopicName.equals(other.subTopicName))
			return false;
		if (subjectName == null) {
			if (other.subjectName != null)
				return false;
		} else if (!subjectName.equals(other.subjectName))
			return false;
		if (wsStatus == null) {
			if (other.wsStatus != null)
				return false;
		} else if (!wsStatus.equals(other.wsStatus))
			return false;
		if (wstotlaRewardsPoints != other.wstotlaRewardsPoints)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WorkSheetTempVO [wstotlaRewardsPoints=" + wstotlaRewardsPoints
				+ ", className=" + className + ", subjectName=" + subjectName
				+ ", wsStatus=" + wsStatus + ", parentTopicName="
				+ parentTopicName + ", subTopicName=" + subTopicName + "]";
	}
	
}
