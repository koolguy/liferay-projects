package com.tlw.recentActivities.vo;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * This class RecentActivitiesVO, is responsible to hold recentActivities.
 * 
 * @author satya
 *
 */
public class RecentActivitiesVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6526313406333553759L;
	
	public RecentActivitiesVO(int workSheetsCount, long rewardPoints,
			String userNmae, long userId,
			Set<WorkSheetTempVO> workSheetTempVOSet, WorkSheetTempVO wsTempVO) {
		super();
		this.workSheetsCount = workSheetsCount;
		this.rewardPoints = rewardPoints;
		this.userNmae = userNmae;
		this.userId = userId;
		this.workSheetTempVOSet = workSheetTempVOSet;
		this.wsTempVO = wsTempVO;
	}
	
	/**
	 * Default Constructor.
	 */
	public RecentActivitiesVO() {
		
	}
	
	private int workSheetsCount;
	private long rewardPoints;
	private String userNmae;
	private long userId;
	private Set<WorkSheetTempVO> workSheetTempVOSet = new HashSet<WorkSheetTempVO>();
	private WorkSheetTempVO wsTempVO = new WorkSheetTempVO();
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (rewardPoints ^ (rewardPoints >>> 32));
		result = prime * result + (int) (userId ^ (userId >>> 32));
		result = prime * result
				+ ((userNmae == null) ? 0 : userNmae.hashCode());
		result = prime
				* result
				+ ((workSheetTempVOSet == null) ? 0 : workSheetTempVOSet
						.hashCode());
		result = prime * result + workSheetsCount;
		result = prime * result
				+ ((wsTempVO == null) ? 0 : wsTempVO.hashCode());
		return result;
	}
	@Override
	public String toString() {
		return "RecentActivitiesVO [workSheetsCount=" + workSheetsCount
				+ ", rewardPoints=" + rewardPoints + ", userNmae=" + userNmae
				+ ", userId=" + userId + ", workSheetTempVOSet="
				+ workSheetTempVOSet + ", wsTempVO=" + wsTempVO + "]";
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RecentActivitiesVO other = (RecentActivitiesVO) obj;
		if (rewardPoints != other.rewardPoints)
			return false;
		if (userId != other.userId)
			return false;
		if (userNmae == null) {
			if (other.userNmae != null)
				return false;
		} else if (!userNmae.equals(other.userNmae))
			return false;
		if (workSheetTempVOSet == null) {
			if (other.workSheetTempVOSet != null)
				return false;
		} else if (!workSheetTempVOSet.equals(other.workSheetTempVOSet))
			return false;
		if (workSheetsCount != other.workSheetsCount)
			return false;
		if (wsTempVO == null) {
			if (other.wsTempVO != null)
				return false;
		} else if (!wsTempVO.equals(other.wsTempVO))
			return false;
		return true;
	}
	
	public WorkSheetTempVO getWsTempVO() {
		return wsTempVO;
	}
	public void setWsTempVO(WorkSheetTempVO wsTempVO) {
		this.wsTempVO = wsTempVO;
	}
	
	public Set<WorkSheetTempVO> getWorkSheetTempVOSet() {
		return workSheetTempVOSet;
	}
	public void setWorkSheetTempVOSet(Set<WorkSheetTempVO> workSheetTempVOSet) {
		this.workSheetTempVOSet = workSheetTempVOSet;
	}
	public int getWorkSheetsCount() {
		return workSheetsCount;
	}
	public void setWorkSheetsCount(int workSheetsCount) {
		this.workSheetsCount = workSheetsCount;
	}
	public long getRewardPoints() {
		return rewardPoints;
	}
	public void setRewardPoints(long rewardPoints) {
		this.rewardPoints = rewardPoints;
	}
	public String getUserNmae() {
		return userNmae;
	}
	public void setUserNmae(String userNmae) {
		this.userNmae = userNmae;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
}
