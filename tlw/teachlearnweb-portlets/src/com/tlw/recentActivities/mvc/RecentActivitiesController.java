package com.tlw.recentActivities.mvc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.util.PortalUtil;
import com.teachlearnweb.dao.entity.DTAssessment;
import com.teachlearnweb.dao.entity.PLPStudentWrokSheet;
import com.teachlearnweb.dao.entity.PLPTopicMaster;
import com.teachlearnweb.dao.entity.RewardPoints;
import com.teachlearnweb.service.vo.User;
import com.tlw.assessment.vo.AssessmentConstants;
import com.tlw.controller.AbstractBaseController;
import com.tlw.recentActivities.vo.RecentActivitiesVO;
import com.tlw.recentActivities.vo.WorkSheetTempVO;

/**
 * The Class RecentActivitiesController, is responsible to handle the
 * recent Activities request.
 * 
 * @author satya.
 *
 */

@Controller("recentActivitiesController")
@Scope("session")
@RequestMapping(value = "VIEW")
public class RecentActivitiesController extends AbstractBaseController{

	/** The LOG. */
	private static final Logger logger = Logger.getLogger(RecentActivitiesController.class);
	
	/**
	 * default Render Mapping Method.
	 * 
	 * @param renderReq
	 * @param renderResponse
	 * @return
	 */
	@RenderMapping
	public ModelAndView showRecentActivities(RenderRequest renderReq, RenderResponse renderResponse) {
		logger.info("Default Render Mapping Method showRecentActivities Start");
		Map<String, List<RecentActivitiesVO>> activitiesMap = new HashMap<String, List<RecentActivitiesVO>>();
		List<RecentActivitiesVO> recentActivitiesList = getRecentActivitiesList(renderReq);
		activitiesMap.put("activitiesList", recentActivitiesList);
		
		List<RewardPoints> activitiesList = fetchRecentActivitiesList();
		Map<String, List<RewardPoints>> recentActivitiesMap = new HashMap<String, List<RewardPoints>>();
		recentActivitiesMap.put("recentActivitiesList", activitiesList);
		
		return new ModelAndView("recentActivities",recentActivitiesMap);
	}
	
	public List<RewardPoints> fetchRecentActivitiesList(){
		logger.debug("RecentActivitiesController.fetchRecentActivitiesList() Method Start");
		List<RewardPoints> rewardPointsList = new ArrayList<RewardPoints>();
		rewardPointsList = serviceLocator.getRewardPointsService().fetchRewardPoints();
		return rewardPointsList;
		
	}
	
	/**
	 * Helper method to return activities list.
	 * 
	 * @param renderReq
	 * @return
	 */
	public List<RecentActivitiesVO> getRecentActivitiesList(RenderRequest renderReq){
		logger.debug("RecentActivitiesController.getRecentActivitiesList() Method Start");
		List<RecentActivitiesVO> recentActivitiesList = new ArrayList<RecentActivitiesVO>();
		List<RecentActivitiesVO> activitiesList = getRecentActivities(renderReq);
		List<RecentActivitiesVO> dtactivitiesList = getRecentActivitiesForDT(renderReq);
		List<RecentActivitiesVO> dtactivitiesList1 = getRecentActivitiesForDT1(renderReq);
		List<RecentActivitiesVO> dtactivitiesList2 = getRecentActivitiesForDT2(renderReq);
		
		if(activitiesList != null && !activitiesList.isEmpty() && activitiesList.size() > 0) {
			System.out.println("activitiesList.size()----------------------------->"+activitiesList.size());
			recentActivitiesList.addAll(activitiesList);
		}
		if(dtactivitiesList != null && !dtactivitiesList.isEmpty() && dtactivitiesList.size() > 0) {
			System.out.println("dtactivitiesList.size()----------------------------->"+dtactivitiesList.size());
			recentActivitiesList.addAll(dtactivitiesList);
		}
		if(dtactivitiesList1 != null && !dtactivitiesList1.isEmpty() && dtactivitiesList1.size() > 0) {
			System.out.println("dtactivitiesList1.size()----------------------------->"+dtactivitiesList1.size());
			recentActivitiesList.addAll(dtactivitiesList1);
		}
		if(dtactivitiesList2 != null && !dtactivitiesList2.isEmpty() && dtactivitiesList2.size() > 0) {
			System.out.println("dtactivitiesList.size()----------------------------->"+dtactivitiesList.size());
			recentActivitiesList.addAll(dtactivitiesList2);
		}
		if(recentActivitiesList != null && recentActivitiesList.size() > 0) {
			System.out.println("Total Recent Activities size------------>"+recentActivitiesList.size());
		}
		return recentActivitiesList;
		
	}

	/**
	 * Method to fetch recentActivities.
	 * 
	 * @param request
	 * @return
	 */
	public List<RecentActivitiesVO> getRecentActivities(PortletRequest request) {
		List<PLPStudentWrokSheet> wsList = null;
		List<RecentActivitiesVO> recentActivitiesVOList = null;
		RecentActivitiesVO recentActivityVO = null;
		WorkSheetTempVO wsTempVO = null;
		long totalRewardPoints = 0l;
		User user = null;
		try {
			wsList = serviceLocator.getPlpStudentWorkSheetService().fetchAllCompletedWorkSheets(AssessmentConstants.TestStatus.COMPLETED);
			if(wsList != null && !wsList.isEmpty() && wsList.size() > 0) {
				logger.debug("wsList.size()------------->"+wsList.size());
			}
		} catch (HibernateException e) {
			logger.error("HibernateException Caught While Fetching workSheets from RecentActivitiesCntroller.getRecentActivities( )", e);
		}
		
		if(wsList != null && !wsList.isEmpty() && wsList.size() > 0) {
			recentActivitiesVOList = new ArrayList<RecentActivitiesVO>();
			logger.debug("wsList.size()------------->"+wsList.size());
			for(PLPStudentWrokSheet ws : wsList) {
				if(ws != null) {
					
					PLPTopicMaster parentTopicMaster = null;
		    		PLPTopicMaster subTopicMaster = null;
		    		try {
						parentTopicMaster = serviceLocator.getTopicMasterService().getTopicMasterByTopicId(ws.getTopicId(), ws.getClassMaster().getClassId(), ws.getSubjectMaster().getSubjectId());
						subTopicMaster = serviceLocator.getTopicMasterService().getTopicMasterByTopicId(ws.getSubtopicId(), ws.getClassMaster().getClassId(), ws.getSubjectMaster().getSubjectId());
					} catch (HibernateException e) {
						logger.error("Hibernate Exception Caught While fetching topic master", e);
						e.printStackTrace();
					}
					recentActivityVO = new RecentActivitiesVO();
					wsTempVO = new WorkSheetTempVO();
					wsTempVO.setClassName(ws.getClassMaster().getClassName());
					wsTempVO.setSubjectName(ws.getSubjectMaster().getSubjectName());
					wsTempVO.setParentTopicName(parentTopicMaster.getTopicName());
					if(subTopicMaster != null && subTopicMaster.getTopicName() != null) {
						wsTempVO.setSubTopicName(subTopicMaster.getTopicName());
					}
					wsTempVO.setWsStatus(AssessmentConstants.TestStatus.COMPLETED);
					
						try {
							user = serviceLocator.getUserService().findUserByUserId(PortalUtil.getCompanyId(request), ws.getUserId());
						} catch (HibernateException e) {
							logger.error("HibernateException Caught While Fetching user from RecentActivitiesCntroller.getRecentActivities( )", e);
						}
					
					if(user != null) {
						recentActivityVO.setUserId(user.getUserId());
						recentActivityVO.setUserNmae(user.getFirstName() +" "+user.getLastName());
						totalRewardPoints = serviceLocator.getUserService().populateRewardPoints(user.getUserId());
						recentActivityVO.setRewardPoints(totalRewardPoints);
						recentActivityVO.setWsTempVO(wsTempVO);
					}
				}
			}
			recentActivitiesVOList.add(recentActivityVO);
		}
		
		return recentActivitiesVOList;
	}
	
	/**
	 * Method to fetch dtrecentActivities.
	 * 
	 * @param request
	 * @return
	 */
	public List<RecentActivitiesVO> getRecentActivitiesForDT(PortletRequest request) {
		List<DTAssessment> dtAssessmentsList = null;
		List<RecentActivitiesVO> recentActivitiesDTVOList = null;
		RecentActivitiesVO recentActivityDTVO = null;
		WorkSheetTempVO dtTempVO = null;
		User dtuser = null;
		long totalRewardPoints = 0l;
		
		try {
			dtAssessmentsList = serviceLocator.getAssessmentService().fetchAllCompletedDTAssessmentsBySubjectTest(AssessmentConstants.TEST_TYPE_SUBJECT_TEST, AssessmentConstants.TestStatus.COMPLETED);
			if(dtAssessmentsList != null && !dtAssessmentsList.isEmpty() && dtAssessmentsList.size() > 0) {
				logger.debug("dtAssessmentsList.size()------------->"+dtAssessmentsList.size());
			}
		} catch (HibernateException e) {
			logger.error("HibernateException Caught While Fetching dtAssessments from RecentActivitiesCntroller.getRecentActivities( )", e);	
		}
		
		
		if(dtAssessmentsList != null && !dtAssessmentsList.isEmpty() && dtAssessmentsList.size() > 0) {
			logger.debug("dtAssessmentsList.size()------------->"+dtAssessmentsList.size());
			recentActivitiesDTVOList = new ArrayList<RecentActivitiesVO>();
			for(DTAssessment assessment : dtAssessmentsList) {
				if(assessment != null) {
					recentActivityDTVO = new RecentActivitiesVO();
					dtTempVO = new WorkSheetTempVO();
					dtTempVO.setClassName(assessment.getClassMaster().getClassName());
					dtTempVO.setSubjectName(assessment.getSubjectMaster().getSubjectName());
					
					try {
						dtuser = serviceLocator.getUserService().findUserByUserId(PortalUtil.getCompanyId(request), assessment.getUserId());
					} catch (HibernateException e) {
						logger.error("HibernateException Caught While Fetching user from RecentActivitiesCntroller.getRecentActivities( )", e);
					}
					if(dtuser != null) {
						recentActivityDTVO.setUserId(dtuser.getUserId());
						recentActivityDTVO.setUserNmae(dtuser.getFirstName() +" "+dtuser.getLastName());
						totalRewardPoints = serviceLocator.getUserService().populateRewardPoints(dtuser.getUserId());
						recentActivityDTVO.setRewardPoints(totalRewardPoints);
						recentActivityDTVO.setWsTempVO(dtTempVO);
					}
				}
				recentActivitiesDTVOList.add(recentActivityDTVO);
			}
		}
		
		return recentActivitiesDTVOList;
	}
	
	/**
	 * Method to fetch dtrecentActivities.
	 * 
	 * @param request
	 * @return
	 */
	public List<RecentActivitiesVO> getRecentActivitiesForDT1(PortletRequest request) {
		List<DTAssessment> dtAssessmentsList = null;
		List<RecentActivitiesVO> recentActivitiesDTVOList = null;
		RecentActivitiesVO recentActivityDTVO = null;
		WorkSheetTempVO dtTempVO = null;
		User dtuser = null;
		long totalRewardPoints = 0l;
		
		try {
			dtAssessmentsList = serviceLocator.getAssessmentService().fetchAllCompletedDTAssessmentsBySubjectTest(AssessmentConstants.TEST_TYPE_DIAGNOSTIC_TEST, AssessmentConstants.TestStatus.COMPLETED);
			if(dtAssessmentsList != null && !dtAssessmentsList.isEmpty() && dtAssessmentsList.size() > 0) {
				logger.debug("dtAssessmentsList.size()------------->"+dtAssessmentsList.size());
			}
		} catch (HibernateException e) {
			logger.error("HibernateException Caught While Fetching dtAssessments from RecentActivitiesCntroller.getRecentActivities( )", e);	
		}
		
		
		if(dtAssessmentsList != null && !dtAssessmentsList.isEmpty() && dtAssessmentsList.size() > 0) {
			logger.debug("dtAssessmentsList.size()------------->"+dtAssessmentsList.size());
			recentActivitiesDTVOList = new ArrayList<RecentActivitiesVO>();
			for(DTAssessment assessment : dtAssessmentsList) {
				if(assessment != null) {
					recentActivityDTVO = new RecentActivitiesVO();
					dtTempVO = new WorkSheetTempVO();
					dtTempVO.setClassName(assessment.getClassMaster().getClassName());
					dtTempVO.setSubjectName(assessment.getSubjectMaster().getSubjectName());
					
					try {
						dtuser = serviceLocator.getUserService().findUserByUserId(PortalUtil.getCompanyId(request), assessment.getUserId());
					} catch (HibernateException e) {
						logger.error("HibernateException Caught While Fetching user from RecentActivitiesCntroller.getRecentActivities( )", e);
					}
					if(dtuser != null) {
						recentActivityDTVO.setUserId(dtuser.getUserId());
						recentActivityDTVO.setUserNmae(dtuser.getFirstName() +" "+dtuser.getLastName());
						totalRewardPoints = serviceLocator.getUserService().populateRewardPoints(dtuser.getUserId());
						recentActivityDTVO.setRewardPoints(totalRewardPoints);
						recentActivityDTVO.setWsTempVO(dtTempVO);
					}
				}
				recentActivitiesDTVOList.add(recentActivityDTVO);
			}
		}
		
		return recentActivitiesDTVOList;
	}

	/**
	 * Method to fetch dtrecentActivities.
	 * 
	 * @param request
	 * @return
	 */
	public List<RecentActivitiesVO> getRecentActivitiesForDT2(PortletRequest request) {
		List<DTAssessment> dtAssessmentsList = null;
		List<RecentActivitiesVO> recentActivitiesDTVOList = null;
		RecentActivitiesVO recentActivityDTVO = null;
		WorkSheetTempVO dtTempVO = null;
		User dtuser = null;
		long totalRewardPoints = 0l;
		
		try {
			dtAssessmentsList = serviceLocator.getAssessmentService().fetchAllCompletedDTAssessmentsBySubjectTest(AssessmentConstants.TEST_TYPE_DIAGNOSTIC_TEST_STYLE_BASED, AssessmentConstants.TestStatus.COMPLETED);
			if(dtAssessmentsList != null && !dtAssessmentsList.isEmpty() && dtAssessmentsList.size() > 0) {
				logger.debug("dtAssessmentsList.size()------------->"+dtAssessmentsList.size());
			}
		} catch (HibernateException e) {
			logger.error("HibernateException Caught While Fetching dtAssessments from RecentActivitiesCntroller.getRecentActivities( )", e);	
		}
		
		
		if(dtAssessmentsList != null && !dtAssessmentsList.isEmpty() && dtAssessmentsList.size() > 0) {
			logger.debug("dtAssessmentsList.size()------------->"+dtAssessmentsList.size());
			recentActivitiesDTVOList = new ArrayList<RecentActivitiesVO>();
			for(DTAssessment assessment : dtAssessmentsList) {
				if(assessment != null) {
					recentActivityDTVO = new RecentActivitiesVO();
					dtTempVO = new WorkSheetTempVO();
					dtTempVO.setClassName(assessment.getClassMaster().getClassName());
					dtTempVO.setSubjectName(assessment.getSubjectMaster().getSubjectName());
					
					try {
						dtuser = serviceLocator.getUserService().findUserByUserId(PortalUtil.getCompanyId(request), assessment.getUserId());
					} catch (HibernateException e) {
						logger.error("HibernateException Caught While Fetching user from RecentActivitiesCntroller.getRecentActivities( )", e);
					}
					if(dtuser != null) {
						recentActivityDTVO.setUserId(dtuser.getUserId());
						recentActivityDTVO.setUserNmae(dtuser.getFirstName() +" "+dtuser.getLastName());
						totalRewardPoints = serviceLocator.getUserService().populateRewardPoints(dtuser.getUserId());
						recentActivityDTVO.setRewardPoints(totalRewardPoints);
						recentActivityDTVO.setWsTempVO(dtTempVO);
					}
				}
				recentActivitiesDTVOList.add(recentActivityDTVO);
			}
		}
		
		return recentActivitiesDTVOList;
	}

	
	/**
	 * Method to fetch dtrecentActivities.
	 * 
	 * @param request
	 * @return
	 */
	public List<RecentActivitiesVO> getRecentActivitiesForDT3(PortletRequest request) {
		List<DTAssessment> dtAssessmentsList = null;
		List<RecentActivitiesVO> recentActivitiesDTVOList = null;
		RecentActivitiesVO recentActivityDTVO = null;
		WorkSheetTempVO dtTempVO = null;
		User dtuser = null;
		long totalRewardPoints = 0l;
		
		try {
			dtAssessmentsList = serviceLocator.getAssessmentService().fetchAllCompletedDTAssessmentsBySubjectTest(AssessmentConstants.TEST_TYPE_EOC_TEST, AssessmentConstants.TestStatus.COMPLETED);
			if(dtAssessmentsList != null && !dtAssessmentsList.isEmpty() && dtAssessmentsList.size() > 0) {
				logger.debug("dtAssessmentsList.size()------------->"+dtAssessmentsList.size());
			}
		} catch (HibernateException e) {
			logger.error("HibernateException Caught While Fetching dtAssessments from RecentActivitiesCntroller.getRecentActivities( )", e);	
		}
		
		
		if(dtAssessmentsList != null && !dtAssessmentsList.isEmpty() && dtAssessmentsList.size() > 0) {
			logger.debug("dtAssessmentsList.size()------------->"+dtAssessmentsList.size());
			recentActivitiesDTVOList = new ArrayList<RecentActivitiesVO>();
			for(DTAssessment assessment : dtAssessmentsList) {
				if(assessment != null) {
					recentActivityDTVO = new RecentActivitiesVO();
					dtTempVO = new WorkSheetTempVO();
					dtTempVO.setClassName(assessment.getClassMaster().getClassName());
					dtTempVO.setSubjectName(assessment.getSubjectMaster().getSubjectName());
					
					try {
						dtuser = serviceLocator.getUserService().findUserByUserId(PortalUtil.getCompanyId(request), assessment.getUserId());
					} catch (HibernateException e) {
						logger.error("HibernateException Caught While Fetching user from RecentActivitiesCntroller.getRecentActivities( )", e);
					}
					if(dtuser != null) {
						recentActivityDTVO.setUserId(dtuser.getUserId());
						recentActivityDTVO.setUserNmae(dtuser.getFirstName() +" "+dtuser.getLastName());
						totalRewardPoints = serviceLocator.getUserService().populateRewardPoints(dtuser.getUserId());
						recentActivityDTVO.setRewardPoints(totalRewardPoints);
						recentActivityDTVO.setWsTempVO(dtTempVO);
					}
				}
				recentActivitiesDTVOList.add(recentActivityDTVO);
			}
		}
		
		return recentActivitiesDTVOList;
	}



	
}
