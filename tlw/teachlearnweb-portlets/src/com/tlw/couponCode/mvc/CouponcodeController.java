/*
 * This code is property of Teach Learn Web, Inc. Use, duplication and disclosure
 * in any form without permission of copyright holder is prohibited.
 *
 * (C) Copyright Teach Learn Web, Inc. 2012. All rights reserved.
 */
package com.tlw.couponCode.mvc;

import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ParamUtil;
import com.teachlearnweb.dao.entity.CouponCode;
import com.tlw.controller.AbstractBaseController;

/**
 * The Class MyProfileController, is responsible to handle the user.
 * 
 */
@Controller("value=couponCodeController")
@RequestMapping(value = "VIEW")
public class CouponcodeController extends AbstractBaseController {

	/** The LOG. */
	private static final Log LOG = LogFactory
			.getLog(CouponcodeController.class);
	
	private static final String ALPHA_NUM =  
        "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 

	/**
	 * Default Render Mapping Method.
	 * 
	 * @param renderRequest
	 * @param renderResponse
	 * @return view name
	 */
	@RenderMapping
	public String showCouponCode(RenderRequest renderRequest,
			RenderResponse renderResponse) {
		return "couponCode";
	}
	
	@ActionMapping(params = "action=generateCouponCode")
	public void generateCouponCode(
			ActionRequest actionRequest,
			ActionResponse actionResponse, SessionStatus sessionStatus) throws PortalException, SystemException {
		String desc = ParamUtil.get(actionRequest, "tesmonialDesc", "Default amount");
		String amount = ParamUtil.get(actionRequest, "amount", "Default Tesmonial");
		LOG.debug("In generateCouponCode ");
		LOG.debug("desc.................................."+desc);
		LOG.debug("amount.................................."+amount);
		
		 StringBuffer sb = new StringBuffer(10);  
	      for (int i=0;  i<10;  i++) {  
	         int ndx = (int)(Math.random()*ALPHA_NUM.length());  
	         sb.append(ALPHA_NUM.charAt(ndx));  
	      }  
	      
	      LOG.debug("coupon code.................................."+sb.toString());
	      
	     try {
			serviceLocator.getCouponCodeService().addCouponcode(sb.toString(), desc, amount);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@ModelAttribute("couponCodeList")
	 public List<CouponCode> fetchTestMonials() {		
		List<CouponCode> couponCodeList= serviceLocator.getCouponCodeService().fetchCouponCodes();
		return couponCodeList;
	}
}