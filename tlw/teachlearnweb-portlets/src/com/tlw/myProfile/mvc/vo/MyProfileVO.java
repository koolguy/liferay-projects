/*
 * This code is property of BrightSky, Inc. Use, duplication and disclosure
 * in any form without permission of copyright holder is prohibited.
 *
 * (C) Copyright BrightSky, Inc. 2011. All rights reserved.
 */
package com.tlw.myProfile.mvc.vo;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Locale;
import java.util.TimeZone;

/**
 * This is a model managed bean that represents a user that is
 * registering with Teach Learn Web.
 * 
 * @author vissum
 */
public class MyProfileVO implements Serializable {

	 /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -4047585584918603376L;

    /** The address1. */
    private String address1;

    /** The address2. */
    private String address2;

    /** The city. */
    private String city;

   

    /** The dob. */
    private String dob;

    /** The email address. */
    private String emailAddress;

    /** The fax. */
    private int fax;

    /** The first name. */
    private String firstName;
    
    
    private Locale locale;

    /** The gender. */
    private String gender = "M";

    /** The home phone. */
    private String homePhone;

    /** The last name. */
    private String lastName;

    /** The mobile. */
    private String mobile;

    /** The password. */
    private String password;
    
    /** The password. */
    private String password1;
    
    /** The password. */
    private String password2;
   

	/** The screen name. */
    private String screenName;

    /** The user id. */
    private long userId;

    /** The uuid. */
    private transient String uUid;
    
   
    /** The error mssage key. */
    private String errormsg;

    /** The show error. */
    private boolean showError;

    /** The str j captcha. */
    private String strJCaptcha;

    /** The success. */
    private boolean success;

    /** The show form. */
    private boolean showForm = true;

    /** The toggle modal. */
    private boolean toggleModal = true;

    /** The modal rendered. */
    private boolean modalRendered = false;

    /** The terms. */
    private boolean terms;

    /** The facebook cancel url. */
    private String facebookCancelUrl;
    
    /** The first name. */
    private String middleName;
    
    /** The first name. */
    private long contactId;

    boolean autoScreenName = true;
    boolean sendEmail = true;
    String jobTitle = null;
    long portraitId = 0;
    String state;
    String country;
    String parentsName;
    private String parentName;
	private String schoolName;
	private String className;
	private Boolean leadershipIndicator;
	private Boolean enrollmentBenefitIndicator;
	private String sectionName;
	private Timestamp hireDate;
	private String phone;
	private String schoolCity;
	private String schoolLocality;
	private String syllabus;
	

	/** The contact info msg. */
    private Boolean contactInfoMsg;
    
    /** The error. */
    private Boolean error;

    /** The message. */
    private Boolean message;
    
    /** The has error. */
    private Boolean hasError;
    
    /** The error on address. */
    private Boolean errorOnAddress;

    /** The error on schoolInfo. */
    private Boolean errorOnSchoolInfo;
    
    /** The error on schoolInfo. */
    private Boolean errorOnMyProfile;
    
    
    /** The old password. */
    private String oldPassword;

    /** The confirm password. */
    private String confirmPassword;

    /** The new password. */
    private String newPassword;
    
    private Boolean errorOnChangePwd;
    
    
    public Boolean getErrorOnChangePwd() {
		return errorOnChangePwd;
	}

	public void setErrorOnChangePwd(Boolean errorOnChangePwd) {
		this.errorOnChangePwd = errorOnChangePwd;
	}

	public String getOldPassword() {
		return oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
    
    public String getSchoolCity() {
		return schoolCity;
	}

	public void setSchoolCity(String schoolCity) {
		this.schoolCity = schoolCity;
	}

	public String getSchoolLocality() {
		return schoolLocality;
	}

	public void setSchoolLocality(String schoolLocality) {
		this.schoolLocality = schoolLocality;
	}

	public String getSyllabus() {
		return syllabus;
	}

	public void setSyllabus(String syllabus) {
		this.syllabus = syllabus;
	}


	/**
     * Gets the modal rendered.
     * @return the modal rendered
     */
    public boolean getModalRendered() {
        return modalRendered;
    }

    /**
     * Sets the modal rendered.
     * @param modalRendered
     *            the new modal rendered
     */
    public void setModalRendered(boolean modalRendered) {
        this.modalRendered = modalRendered;
    }

    /**
     * Sets the success.
     * @param success
     *            the new success
     */
    public void setSuccess(boolean success) {
        this.success = success;
    }

    /**
     * Checks if is success.
     * @return true, if is success
     */
    public boolean isSuccess() {
        return success;
    }

    /**
     * Sets the show form.
     * @param showForm
     *            the new show form
     */
    public void setShowForm(boolean showForm) {
        this.showForm = showForm;
    }

    /**
     * Checks if is show form.
     * @return true, if is show form
     */
    public boolean isShowForm() {
        return showForm;
    }

    /**
     * Sets the toggle modal.
     * @param toggleModal
     *            the new toggle modal
     */
    public void setToggleModal(boolean toggleModal) {
        this.toggleModal = toggleModal;
    }

    /**
     * Checks if is toggle modal.
     * @return true, if is toggle modal
     */
    public boolean isToggleModal() {
        return toggleModal;
    }

    /**
     * Gets the default timezone of the host server. The timezone is
     * needed by
     * the convertDateTime for formatting the time dat values.
     * @return timezone for the current JVM
     */
    public TimeZone getTimeZone() {
        return java.util.TimeZone.getDefault();
    }

    /**
     * Sets the terms.
     * @param terms
     *            the new terms
     */
    public void setTerms(boolean terms) {
        this.terms = terms;
    }

    /**
     * Gets the terms.
     * @return the terms
     */
    public boolean getTerms() {
        return terms;
    }

    /**
     * Gets the address1.
     * @return the address1
     */
    public String getAddress1() {
        return address1;
    }

    /**
     * Sets the address1.
     * @param address1
     *            the new address1
     */
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    /**
     * Gets the address2.
     * @return the address2
     */
    public String getAddress2() {
        return address2;
    }

    /**
     * Sets the address2.
     * @param address2
     *            the new address2
     */
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    /**
     * Gets the city.
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets the city.
     * @param city
     *            the new city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Gets the confirm password.
     * @return the confirm password
     */
    public String getConfirmPassword() {
        return confirmPassword;
    }

    /**
     * Sets the confirm password.
     * @param confirmPassword
     *            the new confirm password
     */
    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    /**
     * Gets the dob.
     * @return the dob
     */
    public String getDob() {
        return dob;
    }

    /**
     * Sets the dob.
     * @param dob
     *            the new dob
     */
    public void setDob(String dob) {
        this.dob = dob;
    }

    /**
     * Gets the fax.
     * @return the fax
     */
    public int getFax() {
        return fax;
    }

    /**
     * Sets the fax.
     * @param fax
     *            the new fax
     */
    public void setFax(int fax) {
        this.fax = fax;
    }

    /**
     * Gets the first name.
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the first name.
     * @param firstName
     *            the new first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the gender.
     * @return the gender
     */
    public String getGender() {
        return gender;
    }

    /**
     * Sets the gender.
     * @param gender
     *            the new gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     * Gets the home phone.
     * @return the home phone
     */
    public String getHomePhone() {
        return homePhone;
    }

    /**
     * Sets the home phone.
     * @param homePhone
     *            the new home phone
     */
    public void setHomePhone(String homePhone) {
        this.homePhone = homePhone;
    }

    /**
     * Gets the last name.
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the last name.
     * @param lastName
     *            the new last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the mobile.
     * @return the mobile
     */
    public String getMobile() {
        return mobile;
    }

    /**
     * Sets the mobile.
     * @param mobile
     *            the new mobile
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }   

    /**
     * Gets the screen name.
     * @return the screen name
     */
    public String getScreenName() {
        return screenName;
    }

    /**
     * Sets the screen name.
     * @param screenName
     *            the new screen name
     */
    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    /**
     * Gets the user id.
     * @return the user id
     */
    public long getUserId() {
        return userId;
    }

    /**
     * Sets the user id.
     * @param userId
     *            the new user id
     */
    public void setUserId(long userId) {
        this.userId = userId;
    }

    /**
     * Gets the u uid.
     * @return the u uid
     */
    public String getuUid() {
        return uUid;
    }

    /**
     * Sets the u uid.
     * @param uUid
     *            the new u uid
     */
    public void setuUid(String uUid) {
        this.uUid = uUid;
    }

    /**
     * Gets the str j captcha.
     * @return the str j captcha
     */
    public String getStrJCaptcha() {
        return strJCaptcha;
    }

    /**
     * Sets the str j captcha.
     * @param strJCaptcha
     *            the new str j captcha
     */
    public void setStrJCaptcha(String strJCaptcha) {
        this.strJCaptcha = strJCaptcha;
    }

    /**
     * Gets the email address.
     * @return the email address
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the email address.
     * @param emailAddress
     *            the new email address
     */
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    /**
     * Gets the password.
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password.
     * @param password
     *            the new password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Checks if is show error.
     * @return true, if is show error
     */
    public boolean isShowError() {
        return showError;
    }

    /**
     * Sets the show error.
     * @param showError
     *            the new show error
     */
    public void setShowError(boolean showError) {
        this.showError = showError;
    }

    /**
     * Gets the facebook cancel url.
     *
     * @return the facebookCancelUrl
     */
    public String getFacebookCancelUrl() {
        return facebookCancelUrl;
    }

    /**
     * Sets the facebook cancel url.
     *
     * @param facebookCancelUrl the facebookCancelUrl to set
     */
    public void setFacebookCancelUrl(String facebookCancelUrl) {
        this.facebookCancelUrl = facebookCancelUrl;
    }

    /**
     * Gets the errormsg.
     *
     * @return the errormsg
     */
    public String getErrormsg() {
        return errormsg;
    }

    /**
     * Sets the errormsg.
     *
     * @param errormsg the errormsg to set
     */
    public void setErrormsg(String errormsg) {
        this.errormsg = errormsg;
    }

	public void setContactId(long contactId) {
		this.contactId = contactId;
		
	}

	public String getMiddleName() {
		return middleName;
	}
	


    public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	
	public long getContactId() {
		return contactId;
	}
	

    public boolean isAutoScreenName() {
		return autoScreenName;
	}

	public void setAutoScreenName(boolean autoScreenName) {
		this.autoScreenName = autoScreenName;
	}

	public boolean isSendEmail() {
		return sendEmail;
	}

	public void setSendEmail(boolean sendEmail) {
		this.sendEmail = sendEmail;
	}
	

    public String getPassword1() {
		return password1;
	}

	public void setPassword1(String password1) {
		this.password1 = password1;
	}

	public String getPassword2() {
		return password2;
	}

	public void setPassword2(String password2) {
		this.password2 = password2;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public long getPortraitId() {
		return portraitId;
	}

	public void setPortraitId(long portraitId) {
		this.portraitId = portraitId;
	}
	
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getParentsName() {
		return parentsName;
	}

	public void setParentsName(String parentsName) {
		this.parentsName = parentsName;
	}
	
	public String getParentName() {
		return parentName;
	}

	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	public String getSchoolName() {
		return schoolName;
	}

	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public Boolean getLeadershipIndicator() {
		return leadershipIndicator;
	}

	public void setLeadershipIndicator(Boolean leadershipIndicator) {
		this.leadershipIndicator = leadershipIndicator;
	}

	public Boolean getEnrollmentBenefitIndicator() {
		return enrollmentBenefitIndicator;
	}

	public void setEnrollmentBenefitIndicator(Boolean enrollmentBenefitIndicator) {
		this.enrollmentBenefitIndicator = enrollmentBenefitIndicator;
	}

	public String getSectionName() {
		return sectionName;
	}

	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

	public Timestamp getHireDate() {
		return hireDate;
	}

	public void setHireDate(Timestamp hireDate) {
		this.hireDate = hireDate;
	}

	
	 /**
     * Gets the error.
     * @return the error
     */
    public Boolean getError() {
        return error;
    }

    /**
     * Sets the error.
     * @param errr
     *            the new error
     */
    public void setError(final Boolean errr) {
        this.error = errr;
    }
    
    /**
     * Gets the message.
     * @return the message
     */
    public Boolean getMessage() {
        return message;
    }

    /**
     * Sets the message.
     * @param msg
     *            the new message
     */
    public void setMessage(final Boolean msg) {
        this.message = msg;
    }
    
    /**
     * Gets the contact info msg.
     *
     * @return the contact info msg
     */
    public Boolean getContactInfoMsg() {
        return contactInfoMsg;
    }

    /**
     * Sets the contact info msg.
     *
     * @param pMessage the new contact info msg
     */
    public void setContactInfoMsg(Boolean pMessage) {
        this.contactInfoMsg = pMessage;
    }
    
    /**
     * Gets the checks for error.
     * @return the checks for error
     */
    public Boolean getHasError() {
        return hasError;
    }

    /**
     * Sets the checks for error.
     * @param hasErr the new checks for error
     */
    public void setHasError(Boolean hasErr) {
        this.hasError = hasErr;
    }

    /**
     * Gets the error on address.
     *
     * @return the error on address
     */
    public Boolean getErrorOnAddress() {
        return errorOnAddress;
    }

    /**
     * Sets the error on address.
     *
     * @param errOnAddress the new error on address
     */
    public void setErrorOnAddress(Boolean errOnAddress) {
        this.errorOnAddress = errOnAddress;
    }
    
    /**
     * Gets the error on address.
     *
     * @return the error on address
     */
    public Boolean getErrorOnSchoolInfo() {
        return errorOnSchoolInfo;
    }

    /**
     * Sets the error on address.
     *
     * @param errOnAddress the new error on address
     */
    public void setErrorOnSchoolInfo(Boolean errOnSchoolInfo) {
        this.errorOnSchoolInfo = errOnSchoolInfo;
    }
    
    /**
     * Gets the error on address.
     *
     * @return the error on address
     */
    public Boolean getErrorOnMyProfile() {
        return errorOnMyProfile;
    }

    /**
     * Sets the error on address.
     *
     * @param errOnAddress the new error on address
     */
    public void setErrorOnMyProfile(Boolean errOnMyProfile) {
        this.errorOnMyProfile = errOnMyProfile;
    }

    
}
