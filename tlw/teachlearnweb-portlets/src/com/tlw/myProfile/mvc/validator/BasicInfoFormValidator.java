package com.tlw.myProfile.mvc.validator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.tlw.myProfile.mvc.vo.MyProfileVO;


@Component("basicInfoFormValidator")
public class BasicInfoFormValidator implements Validator {
	
	/** The logger. */
    private static final Log LOG = LogFactory.getLog(BasicInfoFormValidator.class);
	
	public boolean supports(Class<?> klass) {
		return MyProfileVO.class.isAssignableFrom(klass);
	}

	public void validate(Object target, Errors errors) {
		MyProfileVO myProfileVO = (MyProfileVO)target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "NotEmpty.firstName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "NotEmpty.lastName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emailAddress", "error.blankfield");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "parentName", "NotEmpty.parentName");
		
	}
}
