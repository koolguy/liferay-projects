package com.tlw.myProfile.mvc.validator;

import java.util.Calendar;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.tlw.myProfile.mvc.vo.MyProfileVO;
import com.tlw.util.DateUtil;


@Component("myProfileFormValidator")
public class MyProfileFormValidator implements Validator {
	
	/** The logger. */
    private static final Log LOG = LogFactory.getLog(MyProfileFormValidator.class);
	
	public boolean supports(Class<?> klass) {
		return MyProfileVO.class.isAssignableFrom(klass);
	}

	public void validate(Object target, Errors errors) {
		LOG.debug("################# From MyProfileFormValidator ##################");
		MyProfileVO myProfileVO = (MyProfileVO)target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "NotEmpty.firstName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "NotEmpty.lastName");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dob", "NotEmpty.dob");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "parentName", "NotEmpty.parentName");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emailAddress", "NotEmpty.emailAddress");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "schoolName", "NotEmpty.schoolName");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address1", "NotEmpty.address1");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "NotEmpty.phone");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "className", "NotEmpty.className");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "sectionName", "NotEmpty.sectionName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "schoolCity", "NotEmpty.schoolCity");
		
		if (myProfileVO.getDob() == null || myProfileVO.getDob().equals("")) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dob","NotEmpty.dob");
        } else {
            Calendar cal = DateUtil.convertStringToCalendar(myProfileVO.getDob(),"dd-MM-yyyy");
            Calendar cal1 = Calendar.getInstance();
            cal1.set(Calendar.HOUR_OF_DAY, 0);
            cal1.set(Calendar.MINUTE, 0);
            cal1.set(Calendar.SECOND, 0);
            cal1.set(Calendar.MILLISECOND, 0);

            if (cal.getTime().after(cal1.getTime())) {
                errors.rejectValue("dob", "error.date.valid");
            }
        }
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "state", "NotEmpty.state");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "country", "NotEmpty.country");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", "NotEmpty.city");
		
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty.password");
		LOG.debug("Validating user email address.");
	    validateEmailAddress(myProfileVO, errors);
	}
	
	
	/**
     * Validate email address.
     * @param registrationBean the registration bean
     * @param errors the errors
     */
    private void validateEmailAddress(
            final MyProfileVO myProfileVO, final Errors errors) {
        if (myProfileVO.getEmailAddress() == null || myProfileVO.getEmailAddress().equals("")) {
        	ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emailAddress", "NotEmpty.emailAddress");
            //ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emailAddress","error.blankfield");
        } else {
            com.tlw.util.ValidationHelper.checkEmail("emailAddress",myProfileVO.getEmailAddress(), errors);
        }
    }
    
}
