package com.tlw.myProfile.mvc.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;



import com.tlw.myProfile.mvc.vo.MyProfileVO;
import com.tlw.util.Constants;

/**
 * The Class ChangePasswordValidator.
 */
@Component("pwdValidator")
public class ChangePasswordValidator implements Validator {

    public boolean supports(Class<?> klass) {
        return MyProfileVO.class.isAssignableFrom(klass);
    }

    public void validate(final Object target, final Errors errors) {
    	MyProfileVO myProfileVO = (MyProfileVO)target;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "oldPassword","error.blankfield");

        if (isEmpty(myProfileVO.getNewPassword()) && (isEmpty(myProfileVO.getConfirmPassword()))) {

            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "newPassword","error.blankfield");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors,"confirmPassword", "error.blankfield");
        } else if ((myProfileVO.getNewPassword().length() > 0 && myProfileVO.getNewPassword()
                .length() < Constants.PASSWORD_MIN_LENGTH)
                || (myProfileVO.getConfirmPassword().length() > 0 && myProfileVO
                        .getConfirmPassword().length() < Constants.PASSWORD_MIN_LENGTH)) {
            if (myProfileVO.getNewPassword().length() > 0
                    && myProfileVO.getNewPassword().length() < Constants.PASSWORD_MIN_LENGTH)
                errors.rejectValue("newPassword", "error.password.length");
            if (myProfileVO.getConfirmPassword().length() > 0
                    && myProfileVO.getConfirmPassword().length() < Constants.PASSWORD_MIN_LENGTH)
                errors.rejectValue("confirmPassword", "error.password.length");
        }

    }

    /**
     * Checks if is empty.
     * @param str the str
     * @return true, if is empty
     */
    private boolean isEmpty(String str) {
        if (str == null || str.equals("")) {
            return true;
        } else
            return false;

    }
}
