/*
 * This code is property of Teach Learn Web, Inc. Use, duplication and disclosure
 * in any form without permission of copyright holder is prohibited.
 *
 * (C) Copyright Teach Learn Web, Inc. 2012. All rights reserved.
 */
package com.tlw.myProfile.mvc.controller;

import java.io.InputStream;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.ContactBirthdayException;
import com.liferay.portal.NoSuchUserException;
import com.liferay.portal.UserPortraitSizeException;
import com.liferay.portal.UserPortraitTypeException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadException;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Contact;
import com.liferay.portal.security.auth.PrincipalException;
import com.liferay.portal.service.ContactLocalServiceUtil;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.asset.model.AssetCategory;
import com.teachlearnweb.dao.entity.RewardPoints;
import com.teachlearnweb.dao.entity.TLWUser;
import com.teachlearnweb.dao.entity.UserCategories;
import com.teachlearnweb.service.vo.User;
import com.tlw.controller.AbstractBaseController;
import com.tlw.myProfile.mvc.vo.MyProfileVO;
import com.tlw.util.Constants;
import com.tlw.util.WebUtil;


/**
 * The Class MyProfileController, is responsible to handle the user.
 * 
 */
@Controller("value=myProfileController")
@RequestMapping(value = "VIEW")
public class MyProfileController extends AbstractBaseController {

	/** The LOG. */
	private static final Log LOG = LogFactory.getLog(MyProfileController.class);
	
	/** The errormsg key. */
	private static final String ERROR_MSG_KEY = "errormsg";

	@Autowired
	@Qualifier("myProfileFormValidator")
	private Validator myProfileFormValidator;
	
	@Autowired
	@Qualifier("basicInfoFormValidator")
	private Validator basicInfoFormValidator;
	
	
	@Autowired
	@Qualifier("schoolInfoFormValidator")
	private Validator schoolInfoFormValidator;
	
	 /** The change password validator. */
    @Autowired
    @Qualifier("pwdValidator")
    private Validator pwdValidator;
	
	
	/** The Constant SHOW_EDIT_PROFILE_IMAGE. */
    private static final String SHOW_EDIT_PROFILE_IMAGE = "showEditProfileImage";

    /** The Constant CANCEL_EDIT_PROFILE_IMAGE. */
    private static final String CANCEL_EDIT_PROFILE_IMAGE = "cancelEditProfileImage";
    
    

    /** The Constant ACTION. */
    private static final String ACTION = "action";
	
    /**
	 * Default Render Mapping Method.
	 *
	 * @param renderRequest
	 * @param renderResponse
	 * @return view name
	 */
	@RenderMapping
	public String showMyProfile(RenderRequest renderRequest,RenderResponse renderResponse) {
		
		String WS_STATUS = "Completed";
		long userId = WebUtil.getUser(renderRequest).getUserId();
		int wsCompletedStatusCount = getWorkSheetStatusByUserid(WS_STATUS, userId);
		renderRequest.setAttribute("wsCompletedStatusCount", wsCompletedStatusCount);
		
		int testsFinishedCount = getTestsFinishedByStatusAndUserId(WS_STATUS, userId);
		renderRequest.setAttribute("testsFinishedCount", testsFinishedCount);		
		
		return Constants.MYPROFILE_JSP;
	}
	
	
	
	/**
	 * Method to fetch worksheets count by userid.
	 * 
	 * @param wS_STATUS
	 * @param userId
	 */
	private int getWorkSheetStatusByUserid(String status, long userId) {
		LOG.debug("getWorkSheetStatusByUserid() Method Start in MyProfileController");
		int count = 0;
		try {
			count = serviceLocator.getPlpStudentWorkSheetService().getWorkSheetsStatusByUserId(userId, status);
		} catch (HibernateException e) {
			LOG.error("Exception Caught WileFetching workSheets Completed Status Count..", e);
			e.printStackTrace();
		}
		return count;
		
	}
	
	/**
	 * Method to fetch worksheets count by userid.
	 * 
	 * @param wS_STATUS
	 * @param userId
	 */
	private int getTestsFinishedByStatusAndUserId(String status, long userId) {
		LOG.debug("getTestsFinishedByStatusAndUserId() Method Start in MyProfileController");
		int count = 0;
		try {
			//count = serviceLocator.getPlpStudentWorkSheetService().getWorkSheetsStatusByUserId(userId, status);
			count = serviceLocator.getAssessmentService().getTestsFinishedByStatusAndUserId(userId, status);
			
		} catch (HibernateException e) {
			LOG.error("Exception Caught WileFetching workSheets Completed Status Count..", e);
			e.printStackTrace();
		}
		return count;
		
	}




	/**
	 * Gets the command object.
	 * 
	 * @return the command object
	 */
	@ModelAttribute("myProfileVO")
	public MyProfileVO getCommandObject(PortletRequest request) {
		LOG.debug("Creating object of model MyProfileVO..");
		MyProfileVO myProfileVO = new MyProfileVO();
		
		
		LOG.debug("Get TLWUser command Object. ");
        
        try {

            final User user = WebUtil.getUser(request);
            if (user != null) {
            	
            	myProfileVO.setPortraitId(user.getPortraitId());
            	myProfileVO.setFirstName(user.getFirstName());
            	myProfileVO.setLastName(user.getLastName());
            	myProfileVO.setGender(user.getGender());
            	myProfileVO.setDob(user.getDob());
            	myProfileVO.setEmailAddress(user.getEmailAddress());
            	myProfileVO.setParentName(user.getParentName());
            	myProfileVO.setPhone(user.getPhone());
            	myProfileVO.setSchoolName(user.getSchoolName());
            	myProfileVO.setClassName(user.getClassName());
            	myProfileVO.setSectionName(user.getSectionName());
            	myProfileVO.setAddress1(user.getAddress1());
            	myProfileVO.setCity(user.getCity());
            	myProfileVO.setState(user.getState());
            	myProfileVO.setCountry(user.getCountry());
            	myProfileVO.setSchoolCity(user.getSchoolCity());
            	myProfileVO.setSchoolLocality(user.getSchoolLocality());
            	myProfileVO.setSyllabus(user.getSyllabus());
               
            }
        } catch (Exception e) {
            LOG.error(e.getMessage());
        }
        return myProfileVO;
	}
	
	/**
     * Method to add image.
     * @param response the response
     * @param request the request
	 * @throws Exception 
     */
    @ActionMapping(params = "myaction=updateUserProfile")
    public void updateUserProfile(@ModelAttribute MyProfileVO myProfileVO, 
    		BindingResult bindingResult,ActionRequest actionRequest, ActionResponse actionResponse,SessionStatus sessionStatus) throws Exception 
    {
    	
    	myProfileVO.setError(true);
    	myProfileVO.setMessage(false);
    	myProfileVO.setContactInfoMsg(false);
    	myProfileVO.setHasError(false);
        boolean bGetUserIntoSession = true;
        
        basicInfoFormValidator.validate(myProfileVO, bindingResult);
    	
    	if (!bindingResult.hasErrors()) {
    	
	    	ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	    		    	
	    	String firstName = myProfileVO.getFirstName();
			String lastName = myProfileVO.getLastName();
			String gender = myProfileVO.getGender();
			String dob = myProfileVO.getDob();
			String emailAddress = myProfileVO.getEmailAddress();
			String parentName = myProfileVO.getParentName();
			
			String schoolName = myProfileVO.getSchoolName();
			String classOrGradeName = myProfileVO.getClassName();
			String sectionName = myProfileVO.getSectionName();
			
			LOG.debug("firstName------>"+firstName);
			LOG.debug("lastName------>"+lastName);
			LOG.debug("dob------>"+dob);
			LOG.debug("emailAddress------>"+emailAddress);
			LOG.debug("parentName------>"+parentName);
			LOG.debug("gender------>"+gender);
			
			com.liferay.portal.model.User liferayUser = themeDisplay.getUser();
			liferayUser.setUserId(liferayUser.getUserId());
			liferayUser.setFirstName(firstName);
			liferayUser.setLastName(lastName);
					
			TLWUser tlwUser = new TLWUser();
			tlwUser.setUserId(liferayUser.getUserId());
			tlwUser.setClassName(classOrGradeName);
			tlwUser.setEnrollmentBenefitIndicator(true);
			tlwUser.setLeadershipIndicator(true);
			tlwUser.setParentName(parentName);
			tlwUser.setSchoolName(schoolName);
			tlwUser.setSectionName(sectionName);
			
			liferayUser = serviceLocator.getUserService().updateUserProfile(liferayUser);
		    serviceLocator.getUserService().updateTLWUserProfile(tlwUser);
		    
		    myProfileVO.setError(false);
	    	myProfileVO.setContactInfoMsg(true);
		    
	    	SessionMessages.add(actionRequest, "success-profile-msg");
			//SessionMessages.add(actionRequest, "request_processed");
    	}
		
    }
    
    
    
    
    @ActionMapping(params = "editAction=cancelBasicInfo")
    public void cancelBasicInfo(ActionRequest actionRequest, ActionResponse actionResponse) {
        actionResponse.setRenderParameter("editAction", "cancelledBasicInfo");
    }
    
    @RenderMapping(params = "editAction=cancelledBasicInfo")
    public String cancelBasicInfoPanel(RenderResponse renderResponse) {
        return Constants.MYPROFILE_JSP;
    }
    
    @RenderMapping(params = "editAction=editImagePopUpURL")
    public String editProfileImage(RenderResponse renderResponse) {
    	LOG.debug("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ From editProfile Image @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
        return "editProfileImage";
    }
    
    /**
     * Method to add image.
     * @param response the response
     * @param request the request
	 * @throws Exception 
     */
    @ActionMapping(params = "myaction=editImage")
    public void editImage(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception 
    {
    	try {
			
			updatePortrait(actionRequest);
			WebUtil.updateUserSession(WebUtil.getCompanyId(actionRequest),PortalUtil.getHttpServletRequest(actionRequest));
			SessionMessages.add(actionRequest, "success-profile-msg");
			//SessionMessages.add(actionRequest, "request_processed");
			//actionResponse.setRenderParameter(ACTION, SHOW_EDIT_PROFILE_IMAGE);
			//sendRedirect(actionRequest, actionResponse);
		}
		catch (Exception e) {
			if (e instanceof NoSuchUserException || e instanceof PrincipalException) {
				SessionErrors.add(actionRequest, e.getClass());
				//actionResponse.setRenderParameter(ACTION, SHOW_EDIT_PROFILE_IMAGE);
				actionResponse.setRenderParameter("editImageStatus", "true");
			}
			else if (e instanceof UploadException || e instanceof UserPortraitSizeException || e instanceof UserPortraitTypeException) {
				SessionErrors.add(actionRequest, e.getClass());
				//actionResponse.setRenderParameter(ACTION, SHOW_EDIT_PROFILE_IMAGE);
				actionResponse.setRenderParameter("editImageStatus", "true");
			}
			else {
				SessionErrors.add(actionRequest, "image-upload-error-msg");
				actionResponse.setRenderParameter("editImageStatus", "true");
			}
		}
		
		//actionResponse.setRenderParameter("editAction", "editImagePopUpURL");
    }
    
    protected void updatePortrait(ActionRequest actionRequest)throws Exception 
    {
    	UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
    	//ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
    	
    	User tlwUser = WebUtil.getUser(actionRequest);
		InputStream inputStream = uploadPortletRequest.getFileAsStream("fileName");
		if (inputStream == null) {
			throw new UploadException();
		}
		byte[] bytes = FileUtil.getBytes(inputStream);
		UserServiceUtil.updatePortrait(tlwUser.getUserId(), bytes);
		
		
    }
    
    /**
     * Edits the contact info.
     * @param ActionRequest the action request
     * @param actionResponse the action response
     */
    @ActionMapping(params = "editAction=updateContactInfo")
    public void updateContactInfo(@ModelAttribute MyProfileVO myProfileVO, 
    		BindingResult bindingResult,ActionRequest actionRequest, ActionResponse actionResponse,SessionStatus sessionStatus) {
        
    	LOG.debug("#################### From updateContactInfo ##################");
    	
    	String address1 = myProfileVO.getAddress1();
    	String city = myProfileVO.getCity();
		String state = myProfileVO.getState();
		String country = myProfileVO.getCountry();
		
		LOG.debug("address1----------->"+address1);
		LOG.debug("city----------->"+city);
		LOG.debug("state----------->"+state);
		LOG.debug("country----------->"+country);
		SessionMessages.add(actionRequest, "success-profile-msg");
		//SessionMessages.add(actionRequest, "request_processed");
    	
    }
    
    
    /**
     * Edits the School info.
     * @param ActionRequest the action request
     * @param actionResponse the action response
     */
    @ActionMapping(params = "editAction=updateSchoolInfo")
    public void updateSchoolInfo(@ModelAttribute MyProfileVO myProfileVO, 
    		BindingResult bindingResult,ActionRequest actionRequest, ActionResponse actionResponse,SessionStatus sessionStatus) {
    	
    	LOG.debug("@@@@@@@@@@@@@@@@@@@ From the updateSchoolInfo");
    	
    	myProfileVO.setErrorOnSchoolInfo(true);
    	myProfileVO.setMessage(false);
    	myProfileVO.setContactInfoMsg(false);
    	myProfileVO.setHasError(false);
        boolean bGetUserIntoSession = true;
        
        schoolInfoFormValidator.validate(myProfileVO, bindingResult);
    	
    	if (!bindingResult.hasErrors()) {
    	
	    	ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	    	
	    	/*String schoolName = ParamUtil.getString(actionRequest, "schoolName");
			String classOrGradeName = ParamUtil.getString(actionRequest, "classOrGradeName");
			String sectionName = ParamUtil.getString(actionRequest, "sectionName");
			*/
			
	    	
	    	String schoolName = myProfileVO.getSchoolName();
			String classOrGradeName = myProfileVO.getClassName();
			String sectionName = myProfileVO.getSectionName();
			String parentName = myProfileVO.getParentName();
			
			
			
			com.liferay.portal.model.User liferayUser = themeDisplay.getUser();
			
			LOG.debug("schoolName-------->"+schoolName);
			LOG.debug("classOrGradeName-------->"+classOrGradeName);
			LOG.debug("sectionName-------->"+sectionName);
			LOG.debug("parentName-------->"+parentName);
			
			
			TLWUser tlwUser = new TLWUser();
			tlwUser.setUserId(liferayUser.getUserId());
			tlwUser.setClassName(classOrGradeName);
			tlwUser.setEnrollmentBenefitIndicator(true);
			tlwUser.setLeadershipIndicator(true);
			tlwUser.setParentName(parentName);
			tlwUser.setSchoolName(schoolName);
			tlwUser.setSectionName(sectionName);
			
		    serviceLocator.getUserService().updateTLWUserProfile(tlwUser);
		    
		    myProfileVO.setErrorOnSchoolInfo(false);
	    	myProfileVO.setContactInfoMsg(true);
	    	SessionMessages.add(actionRequest, "success-profile-msg");
		    //SessionMessages.add(actionRequest, "request_processed");
    	}
		
    }
    
    /**
     * Edits the School info.
     * @param ActionRequest the action request
     * @param actionResponse the action response
     * @throws SystemException 
     * @throws PortalException 
     * @throws ParseException 
     */
    @ActionMapping(params = "editAction=updateMyProfile")
    public void updateMyProfile(@ModelAttribute MyProfileVO myProfileVO, 
    		BindingResult bindingResult,ActionRequest actionRequest, ActionResponse actionResponse,SessionStatus sessionStatus) throws PortalException, SystemException, ParseException {
    	try{
    		LOG.debug("@@@@@@@@@@@@@@@@@@@ From the Update MyProfile");
	    	myProfileVO.setErrorOnMyProfile(true);
	    	myProfileVO.setMessage(false);
	    	myProfileVO.setContactInfoMsg(false);
	    	myProfileVO.setHasError(false);
	        
	    	myProfileFormValidator.validate(myProfileVO, bindingResult);
	    	
	    	if (!bindingResult.hasErrors()) {
	    		
	    		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
	    		com.liferay.portal.model.User liferayUser = themeDisplay.getUser();
	    		
	    		String firstName = myProfileVO.getFirstName();
				String lastName = myProfileVO.getLastName();
	    		String dob = myProfileVO.getDob();
	    		String parentName = myProfileVO.getParentName();
	    		String emailAddress = myProfileVO.getEmailAddress();
	    		String address1 = myProfileVO.getAddress1();
	    		String schoolName = myProfileVO.getSchoolName();
	    		String phone = myProfileVO.getPhone();
				String classOrGradeName = myProfileVO.getClassName();
				String sectionName = myProfileVO.getSectionName();
				String schoolCity = myProfileVO.getSchoolCity();
				
				LOG.debug("dob------------>"+dob);
				LOG.debug("parentName------------>"+parentName);
				LOG.debug("emailAddress------------>"+emailAddress);
				LOG.debug("schoolName------------>"+schoolName);
				LOG.debug("phone------------>"+phone);
				LOG.debug("classOrGradeName------------>"+classOrGradeName);
				LOG.debug("sectionName------------>"+sectionName);
				
				User currentUser = WebUtil.getUser(actionRequest);
				
				TLWUser tlwUser = new TLWUser();
				tlwUser.setUserId(liferayUser.getUserId());
				tlwUser.setClassName(classOrGradeName);
				tlwUser.setEnrollmentBenefitIndicator(true);
				tlwUser.setLeadershipIndicator(true);
				tlwUser.setParentName(parentName);
				tlwUser.setSchoolName(schoolName);
				tlwUser.setSectionName(sectionName);
				tlwUser.setPhone(phone);
				tlwUser.setSchoolCity(schoolCity);
				tlwUser.setSchoolLocality("Panjakutta, Banjara Hills,");
				tlwUser.setSyllabus("CBSE");
				tlwUser.setJobTitle(currentUser.getJobTitle());
				
				String[] dobStringArray = dob.split("-");
				
				int birthdayDay = Integer.parseInt(dobStringArray[0]);
				int birthdayMonth = Integer.parseInt(dobStringArray[1]);
				int birthdayYear = Integer.parseInt(dobStringArray[2]);
				
				Date birthday = PortalUtil.getDate(birthdayMonth, birthdayDay, birthdayYear,ContactBirthdayException.class);
				
				LOG.debug("birthday ----------> " + birthday.toString());
				
				try{
					serviceLocator.getUserService().updateTLWUserProfile(tlwUser);
				}catch (Exception e) {
					e.printStackTrace();
				}
				
			    liferayUser.setFirstName(firstName);
			    liferayUser.setLastName(lastName);
			    liferayUser.setEmailAddress(emailAddress);
			    
			    Contact contact = null;
			    try{
			    	liferayUser = serviceLocator.getUserService().updateUserProfile(liferayUser);
			    	contact = ContactLocalServiceUtil.getContact(liferayUser.getContactId());
			    	contact.setBirthday(birthday);
			    	serviceLocator.getUserService().updateContact(contact);
			    }catch (Exception e) {
			    	e.printStackTrace();
				}
			    
			    
				myProfileVO.setErrorOnMyProfile(false);
		    	myProfileVO.setContactInfoMsg(true);
			   
		    	WebUtil.updateUserSession(WebUtil.getCompanyId(actionRequest),PortalUtil.getHttpServletRequest(actionRequest));
			    SessionMessages.add(actionRequest, "success-profile-msg");
	    	
	    	}
    	}catch (Exception e) {
			e.printStackTrace();
		}
    	
    	
    }
    
    
    
    
    
    /**
	 * Gets the command object.
	 * 
	 * @return the command object
	 */
	@ModelAttribute("packagesList")
	public List<AssetCategory> showPackages(PortletRequest request) {
		LOG.debug("Creating object of model RegistrationModelBean..");
		List<UserCategories> categoriesList = serviceLocator.getUserService()
				.categoriesByUserId(WebUtil.getUserId(request));
		
		List<AssetCategory> packagesList = new ArrayList<AssetCategory>();
		AssetCategory assetCategory = null;
		
		for (UserCategories object : categoriesList) {

			LOG.debug("userId...." + object.getUserId());
			LOG.debug("categoryId...." + object.getCategoryId());

			assetCategory = WebUtil.getAssetCategoryById(object
					.getCategoryId());
			LOG.debug("Current category ..........1............."
					+ assetCategory.getName());

			
			packagesList.add(assetCategory);

		}
		packagesList = WebUtil.removeDuplicates(packagesList);
		
		return packagesList;
	}
	 /**
	 * Gets the command object.
	 * 
	 * @return the command object
	 */
	@ModelAttribute("rewardPoints")
	public List<RewardPoints> getRewardPoints(PortletRequest request) {
    
	List<RewardPoints> rewards = serviceLocator.getRewardPointsService().rewardPointsByUserId(WebUtil.getUserId(request));
	return rewards;
	}
	

	
	 /**
     * Change password.
     * @param user
     *            the user
     * @param bindingResult
     *            the binding result
     * @param response
     *            the response
     * @param sessionStatus
     *            the session status
     * @throws SystemException
     * @throws PortalException
     */
    @ActionMapping(params = "editAction=changePassword")
    public void changePassword(@ModelAttribute MyProfileVO myProfileVO, 
    		BindingResult bindingResult,ActionRequest actionRequest, ActionResponse actionResponse,SessionStatus sessionStatus)
        throws PortalException, SystemException {
    	
    	myProfileVO.setErrorOnMyProfile(false);
    	myProfileVO.setErrorOnChangePwd(true);
        pwdValidator.validate(myProfileVO, bindingResult);
        if (!bindingResult.hasErrors()) {
            try {
                if (!myProfileVO.getNewPassword().equals(myProfileVO.getConfirmPassword())) {
                	
                	SessionErrors.add(actionRequest, "error-changepassword-password-mismatch");
                    //throw new BusinessException("Password did not match.","error.changepassword.passwordmismatch");
                }else{
                /*serviceLocator.getUserService().changePassword(
                        WebUtil.getUserId(actionRequest), myProfileVO.getOldPassword(),
                        myProfileVO.getNewPassword(), myProfileVO.getConfirmPassword());
                */
                
	                User user = serviceLocator.getUserService().updatePassword(WebUtil.getUserId(actionRequest), myProfileVO.getNewPassword(), myProfileVO.getConfirmPassword(), false);
	                SessionMessages.add(actionRequest, "success-change-pwd-msg");
	        		myProfileVO.setErrorOnChangePwd(false);
                }
            } catch (Exception e) {
                
                e.printStackTrace();
            }
            sessionStatus.setComplete();
        }
    }
	
	
	
}