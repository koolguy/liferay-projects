package com.tlw.subjects;

import java.io.Serializable;

public class SubjectVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/** The name. */
    private String subjectName;
    
    /** The categoryId. */
    private long categoryId;
    
    /** The vocabularyId. */
    private long vocabularyId;
    
    /** The parentCategoryId. */
    private long parentCategoryId;

    /** The name. */
    private String className;
    
    private String numberOfChildCategories;
    
    /** The parentCategoryId. */
    private String syllabusName ;

    
	public String getSyllabusName() {
		return syllabusName;
	}

	public void setSyllabusName(String syllabusName) {
		this.syllabusName = syllabusName;
	}

	public long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(long categoryId) {
		this.categoryId = categoryId;
	}

	public long getVocabularyId() {
		return vocabularyId;
	}

	public void setVocabularyId(long vocabularyId) {
		this.vocabularyId = vocabularyId;
	}

	public long getParentCategoryId() {
		return parentCategoryId;
	}

	public void setParentCategoryId(long parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getNumberOfChildCategories() {
		return numberOfChildCategories;
	}

	public void setNumberOfChildCategories(String numberOfChildCategories) {
		this.numberOfChildCategories = numberOfChildCategories;
	}
    
}
