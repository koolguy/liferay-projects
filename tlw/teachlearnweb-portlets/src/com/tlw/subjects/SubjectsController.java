/*
 * This code is property of Teach Learn Web, Inc. Use, duplication and disclosure
 * in any form without permission of copyright holder is prohibited.
 *
 * (C) Copyright Teach Learn Web, Inc. 2012. All rights reserved.
 */
package com.tlw.subjects;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.xml.namespace.QName;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portlet.asset.model.AssetCategory;
import com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil;
import com.teachlearnweb.dao.entity.DTAssessment;
import com.teachlearnweb.dao.entity.UserCategories;
import com.tlw.assessment.vo.AssessmentConstants;
import com.tlw.controller.AbstractBaseController;
import com.tlw.registration.mvc.vo.RegistrationModelBean;
import com.tlw.util.WebUtil;

/**
 * The Class RegistrationController, is responsible to handle the user
 * registration request.
 * 
 * @author asharma
 * @author arvindk
 */
@Controller("subjectsController")
@Scope("session")
@RequestMapping(value = "VIEW")
public class SubjectsController extends AbstractBaseController {

	/** The logger. */
	private static final Logger logger = Logger.getLogger(SubjectsController.class);

	/** The errormsg key. */
	private static final String ERROR_MSG_KEY = "errormsg";

	/**
	 * Show registration.
	 * 
	 * @param renderResponse
	 *            the render response
	 * @return the string
	 */
	@RenderMapping
	public ModelAndView showSubjects(RenderRequest renderRequest, RenderResponse renderResponse) {
		return new ModelAndView("showSubjects", this.assessmentStatus(renderRequest));
	}

	/**
	 * Gets the command object.
	 * 
	 * @return the command object
	 */
	@ModelAttribute("rootCategoryList")
	public List<SubjectVo> getCommandObject(PortletRequest request) {
		logger.debug("Creating object of model RegistrationModelBean..");
		List<UserCategories> categoriesList = serviceLocator.getUserService()
				.categoriesByUserId(WebUtil.getUserId(request));
		
		List<AssetCategory> classListInEachSyllubus = new ArrayList<AssetCategory>();
		AssetCategory classCategory = null;
		List<SubjectVo> subjectsList = null;
		
		for (UserCategories userCategory : categoriesList) {
			logger.debug("userId...." + userCategory.getUserId());
			logger.debug("userCategoryId...." + userCategory.getCategoryId());

			classCategory = WebUtil.getAssetCategoryById(userCategory.getCategoryId());
			classListInEachSyllubus.add(classCategory);
			logger.debug("className....Xth..........."+ classCategory.getName());			
		}
		classListInEachSyllubus = WebUtil.removeDuplicates(classListInEachSyllubus);
		
		
		AssetCategory assesCategory = null;
		SubjectVo  subjectVo = null;		
		subjectsList = new ArrayList<SubjectVo>();
		for (AssetCategory classCategoryTemp : classListInEachSyllubus) {						
			long classCategoryId = (Long) classCategoryTemp.getCategoryId();
			long classVocabularyId = (Long) classCategoryTemp.getVocabularyId();
			String className = (String) classCategoryTemp.getName();			
			long syllabusCategoryId = classCategoryTemp.getParentCategoryId();
		
			AssetCategory syllabusCategory = WebUtil.getAssetCategoryById(syllabusCategoryId);
			String syllabusName = syllabusCategory.getName();
			
			logger.debug("syllabus :"+syllabusName);

			// take all subjects in  
			List<AssetCategory> subjectListInEachClass = WebUtil.getChildCategories(classCategoryId, classVocabularyId);
			
			for (AssetCategory subjectCategory : subjectListInEachClass) {
				subjectVo = new SubjectVo();
				subjectVo.setCategoryId(subjectCategory.getCategoryId());
				subjectVo.setParentCategoryId(subjectCategory.getParentCategoryId());
				subjectVo.setSubjectName(subjectCategory.getName());
				subjectVo.setVocabularyId(subjectCategory.getCategoryId());
				subjectVo.setClassName(className);
				subjectVo.setNumberOfChildCategories(String.valueOf(WebUtil.getChildCategoriesCount(subjectCategory.getCategoryId())));
				subjectVo.setSyllabusName(syllabusName);
				
				subjectsList.add(subjectVo);
			}		
		}
		
		return subjectsList;
	}
	
	
	@ActionMapping(params = "action=startCourse")
	public void subscribeCourse(
			@RequestParam Long subjectId,
			@RequestParam String className,
			@RequestParam String syllabusName,
			@ModelAttribute RegistrationModelBean registrationBean,
			BindingResult bindingResult, ActionRequest actionRequest,
			ActionResponse actionResponse, SessionStatus sessionStatus) throws PortalException, SystemException {
		
		javax.xml.namespace.QName qName = new QName("http:teachlearnweb.com/events", "subjectId", "x");
		actionResponse.setEvent(qName, String.valueOf(subjectId));
		try {			
			
			// get Subject Details
			AssetCategory subjectCategory = WebUtil.getAssetCategoryById(subjectId);
			
			logger.debug("subjectId "+subjectId);
			logger.debug("subjectName "+subjectCategory.getName());
			
			int intNumberOfTopics = AssetCategoryLocalServiceUtil.getChildCategoriesCount(subjectId);
			
			logger.debug("number of Topics : "+intNumberOfTopics);
			
			String strNumberOfTopics = String.valueOf(intNumberOfTopics);
			
			
			int numberOfAllSubTopics = WebUtil.getNumberOfSubTopicsInSubject(subjectId);
			
			String strNumberOfAllSubTopics = String.valueOf(numberOfAllSubTopics);
			
			
			// SET REQUIRED VALUES INTO SESSION			
			actionRequest.getPortletSession().setAttribute("subject_subjectId", subjectId,PortletSession.APPLICATION_SCOPE);
			actionRequest.getPortletSession().setAttribute("topics_assetCatName", subjectCategory.getName(),PortletSession.APPLICATION_SCOPE);
			actionRequest.getPortletSession().setAttribute("subject_className", className,PortletSession.APPLICATION_SCOPE);
			actionRequest.getPortletSession().setAttribute("subject_syllabus", syllabusName,PortletSession.APPLICATION_SCOPE);
			actionRequest.getPortletSession().setAttribute("strNumberOfTopics", strNumberOfTopics,PortletSession.APPLICATION_SCOPE);
			actionRequest.getPortletSession().setAttribute("strNumberOfAllSubTopics", strNumberOfAllSubTopics,PortletSession.APPLICATION_SCOPE);
			// SET DEFAULT LOADING AS TRUE
			actionRequest.getPortletSession().setAttribute("subject_defaultLoading", "TRUE",PortletSession.APPLICATION_SCOPE);
				
			logger.info("redirecting to learning page..../group/guest/learning");
			actionResponse.sendRedirect("/group/guest/learning");
		
			
			} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("Errror +"+e);
		}
		//serviceLocator.getUserService().addCourse(WebUtil.getUserId(actionRequest),subjectId);
		
	}
	
	@RenderMapping(params = "action=startCourse")
	public String showTopics(RenderResponse renderResponse) {
		logger.debug("In render..................");
		return "showTopics";
	}

	@ResourceMapping("showSubjects")
    public ModelAndView showSubjects(ResourceRequest request, ResourceResponse response) {
		logger.debug("show Subjects..................IN Resource URL...........");
        return new ModelAndView("../learning/showSubjects", this.assessmentStatus(request));
    }

    public Map<String, Object> assessmentStatus(PortletRequest request) {
    	logger.debug("START");
    	// fetch assessments
//    	List<DTAssessment> assessments = this.serviceLocator.getAssessmentService().getAssessments((int)WebUtil.getUserId(request),
//    								AssessmentConstants.TEST_TYPE_DIAGNOSTIC_TEST);
    	
    	List<DTAssessment> assessments = this.serviceLocator.getAssessmentService().getDiagnosticTests((int)WebUtil.getUserId(request));
    	float percentCompleted = 0;
    	Map<String, Object> model = new HashMap<String, Object>();
    	if (assessments != null && ! assessments.isEmpty()) {
    		logger.debug("assessments: " + assessments.size());
    		int completedCount = 0;
    		for (DTAssessment assessment : assessments) {
    			logger.debug("assessment.testId: " + assessment.getDTTest().getTestId());
    			logger.debug("assessment.id: " + assessment.getAssessmentId());
    			logger.debug("assessment.status: " + assessment.getStatus());
				if (AssessmentConstants.TestStatus.COMPLETED.equalsIgnoreCase(assessment.getStatus())) {
					completedCount++;
				}
			}
    		percentCompleted = (completedCount / (float)assessments.size()) * 100.0f;
    		percentCompleted = roundTwoDecimals(percentCompleted);
    		model.put("percent", percentCompleted);
    		model.put("status", "Completed");
    	} else {
    		model.put("status", null);
    	}
    	logger.debug("percentCompleted: " + percentCompleted);

    	logger.debug("END");
    	return model;
    }

    private float roundTwoDecimals(float d) { 
        DecimalFormat twoDForm = new DecimalFormat("#.##"); 
        return Float.valueOf(twoDForm.format(d));
    }
} 
