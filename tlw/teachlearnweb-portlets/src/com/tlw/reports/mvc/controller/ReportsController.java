/*
 * This code is property of Teach Learn Web, Inc. Use, duplication and disclosure
 * in any form without permission of copyright holder is prohibited.
 *
 * (C) Copyright Teach Learn Web, Inc. 2012. All rights reserved.
 */
package com.tlw.reports.mvc.controller;

import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.User;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.tlw.controller.AbstractBaseController;
import com.tlw.reports.mvc.vo.ReportsModelBean;
import com.tlw.util.WebUtil;

/**
 * The Class ReportsController, is responsible to handle the user
 * reports request.
 * 
 * @author asharma
 * @author arvindk
 */
@Controller("value=reportsController")
@RequestMapping(value = "VIEW")
public class ReportsController extends AbstractBaseController {

	/** The LOG. */
	private static final Log LOG = LogFactory
			.getLog(ReportsController.class);

	/** The errormsg key. */
	private static final String ERROR_MSG_KEY = "errormsg";

	@Autowired
	@Qualifier("reportsFormValidator")
	private Validator reportsFormValidator;

	/**
	 * Show reports.
	 * 
	 * @param renderResponse
	 *            the render response
	 * @return the string
	 */
	@RenderMapping
	public String showReports(RenderResponse renderResponse) {
		return "reportsForm";
	}

	/**
	 * Gets the command object.
	 * 
	 * @return the command object
	 */
	@ModelAttribute("reportsModelBean")
	public ReportsModelBean getCommandObject() {
		LOG.debug("Creating object of model ReportsModelBean..");
		ReportsModelBean reportsModelBean = new ReportsModelBean();
		/*
		 * FaceBookService facebookService =
		 * serviceLocator.getFaceBookService();
		 * reportsModelBean.setFacebookCancelUrl(facebookService
		 * .getOnCancelRedirectionUrl());
		 */
		return reportsModelBean;
	}

	@ActionMapping(params = "action=doReports")
	public void doReports(
			@ModelAttribute ReportsModelBean reportsBean,
			BindingResult bindingResult, ActionRequest actionRequest,
			ActionResponse actionResponse, SessionStatus sessionStatus) throws PortalException, SystemException {
		LOG.debug("Request received to register the user...");

		reportsFormValidator.validate(reportsBean, bindingResult);
		
		popilateUserObject();

		boolean autoPassword = true;
		long companyId = WebUtil.getCompanyId(actionRequest);
		long creatorUserId = 0;
		String password1 = null;
		String screenName = reportsBean.getFirstName() + reportsBean.getLastName();
		String emailAddress = reportsBean.getEmailAddress();
		boolean autoScreenName = true;
		String password2 = null;
		String middleName = null;
		String openId = "1234";
		String lastName = reportsBean.getLastName();
		
		long facebookId = 0;
		String firstName = reportsBean.getFirstName();
		Locale locale = WebUtil.getThemeDisplay(actionRequest).getLocale();
		
		int birthdayMonth = 5;
		int prefixId  = 0;
		int birthdayDay= 12;
		int suffixId = 0;
		boolean male = true;
		long[] organizationIds = null;
		int birthdayYear = 1981;
		long[] groupIds = null;
		String jobTitle = "Student";
		long[] roleIds = null;
		boolean sendEmail = true;
		ServiceContext serviceContext = null;
		long[] userGroupIds = null;
		
		try {
			User user = UserLocalServiceUtil.addUser(creatorUserId, companyId, autoPassword,
					password1, password2, autoScreenName, screenName, emailAddress,
					facebookId, openId, locale, firstName, middleName, lastName,
					prefixId, suffixId, male, birthdayMonth, birthdayDay,
					birthdayYear, jobTitle, groupIds, organizationIds, roleIds,
					userGroupIds, sendEmail, serviceContext);
			
			LOG.debug("The new uerID..........................."+user.getUserId());
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*
		 * UserLocalServiceUtil.addUser(WebUtil.getUserId(actionRequest),
		 * WebUtil.getCompanyId(actionRequest), true, null, null, true, null,
		 * reportsBean.getEmailAddress(), null, null, null,
		 * reportsBean.getFirstName(), null,
		 * reportsBean.getLastName(), null, null, true, null, null, null,
		 * null, null, null, null, null, true, null);
		 */
		LOG.debug("Request received to register the user...");
		LOG.debug("Email Address .............."
				+ reportsBean.getEmailAddress());
		LOG.debug("Password .............."
				+ reportsBean.getPassword());

	}

	private User popilateUserObject() {
		
		
		return null;
		
	}

}
