package com.tlw.reports.mvc.vo;

import java.io.Serializable;

/**
 * Class to prepare a worksheet report .
 * 
 * @author satya
 *
 */
public class WorkSheetReport implements Serializable{

	//default
	public WorkSheetReport(){
		
	}
	private String question;
	private String answer;
	private String userAnswer;
	private Integer userSelectedAnswerId;
	
	@Override
	public String toString() {
		return "WorkSheetReport [question=" + question + ", answer=" + answer
				+ ", userAnswer=" + userAnswer + ", userSelectedAnswerId="
				+ userSelectedAnswerId + "]";
	}
	public Integer getUserSelectedAnswerId() {
		return userSelectedAnswerId;
	}
	public void setUserSelectedAnswerId(Integer userSelectedAnswerId) {
		this.userSelectedAnswerId = userSelectedAnswerId;
	}
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public String getUserAnswer() {
		return userAnswer;
	}
	public void setUserAnswer(String userAnswer) {
		this.userAnswer = userAnswer;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((answer == null) ? 0 : answer.hashCode());
		result = prime * result
				+ ((question == null) ? 0 : question.hashCode());
		result = prime * result
				+ ((userAnswer == null) ? 0 : userAnswer.hashCode());
		result = prime
				* result
				+ ((userSelectedAnswerId == null) ? 0 : userSelectedAnswerId
						.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WorkSheetReport other = (WorkSheetReport) obj;
		if (answer == null) {
			if (other.answer != null)
				return false;
		} else if (!answer.equals(other.answer))
			return false;
		if (question == null) {
			if (other.question != null)
				return false;
		} else if (!question.equals(other.question))
			return false;
		if (userAnswer == null) {
			if (other.userAnswer != null)
				return false;
		} else if (!userAnswer.equals(other.userAnswer))
			return false;
		if (userSelectedAnswerId == null) {
			if (other.userSelectedAnswerId != null)
				return false;
		} else if (!userSelectedAnswerId.equals(other.userSelectedAnswerId))
			return false;
		return true;
	}

}
