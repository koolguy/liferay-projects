package com.tlw.reports.mvc.validator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.tlw.reports.mvc.vo.ReportsModelBean;


@Component("reportsFormValidator")
public class ReportsFormValidator implements Validator {
	
	/** The logger. */
    private static final Log LOG = LogFactory
            .getLog(ReportsFormValidator.class);
	
	public boolean supports(Class<?> klass) {
		return ReportsModelBean.class.isAssignableFrom(klass);
	}

	public void validate(Object target, Errors errors) {
		ReportsModelBean reportsFormBean = (ReportsModelBean)target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "NotEmpty.firstName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "NotEmpty.lastName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emailAddress", "error.blankfield");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", "NotEmpty.city");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty.password");
		LOG.debug("Validating user email address.");
	    validateEmailAddress(reportsFormBean, errors);
	}
	
	
	/**
     * Validate email address.
     * @param reportsBean the reports bean
     * @param errors the errors
     */
    private void validateEmailAddress(
            final ReportsModelBean reportsBean, final Errors errors) {
        if (reportsBean.getEmailAddress() == null
                || reportsBean.getEmailAddress().equals("")) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emailAddress",
                    "error.blankfield");
        } else {
            com.tlw.util.ValidationHelper.checkEmail("emailAddress",
            		reportsBean.getEmailAddress(), errors);
        }
    }

    /**
     * Validate password.
     * @param reportsBean the reports bean
     * @param errors the errors
     */
    /*private void validatePassword(final ReportsModelBean reportsBean,
            final Errors errors) {
        if (reportsBean.getPassword() == null
                || reportsBean.getPassword().equals("")) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password",
                    "error.blankfield");
        } else if (reportsBean.getPassword().length() < Constants.PASSWORD_MIN_LENGTH) {
            errors.rejectValue("password", "error.password.length");
        }

        if (reportsBean.getConfirmPassword() == null
                || reportsBean.getConfirmPassword().equals("")) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors,
                    "confirmPassword", "error.blankfield");
        } else if (reportsBean.getConfirmPassword().length() < Constants.PASSWORD_MIN_LENGTH) {
            errors.rejectValue("confirmPassword", "error.password.length");
        }
    }*/
}
