/*
 * This code is property of Teach Learn Web, Inc. Use, duplication and disclosure
 * in any form without permission of copyright holder is prohibited.
 *
 * (C) Copyright Teach Learn Web, Inc. 2012. All rights reserved.
 */
package com.tlw.calendar;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.portlet.PortletRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portlet.calendar.model.CalEvent;
import com.liferay.portlet.calendar.service.CalEventLocalServiceUtil;
import com.tlw.controller.AbstractBaseController;
import com.tlw.util.WebUtil;

/**
 * The Class RegistrationController, is responsible to handle the user
 * registration request.
 * 
 * @author asharma
 * @author arvindk
 */
@Controller("calendarController")
@Scope("session")
@RequestMapping(value = "VIEW")
public class CalendarController extends AbstractBaseController {

	/** The LOG. */
	private static final Logger LOG = Logger
			.getLogger(CalendarController.class);

	/** The errormsg key. */
	private static final String ERROR_MSG_KEY = "errormsg";

	/**
	 * Show registration.
	 * 
	 * @param renderResponse
	 *            the render response
	 * @return the string
	 */
	@RenderMapping
	public String showEvents(RenderResponse renderResponse) {
		return "showEvents";
	}

	/**
	 * Gets the command object.
	 * 
	 * @return the command object
	 */
	@ModelAttribute("calEvents")
	public List<CalEvent> getCommandObject(PortletRequest request) {
		LOG.debug("Creating object of model Cal Events..");
		List<CalEvent> calEvents = null;
		List<CalEvent> calEventsList = new ArrayList<CalEvent>();

		// Calendar cal = CalendarFactoryUtil.getCalendar();
		Date date = new Date();
		try {
			calEvents = CalEventLocalServiceUtil.getEvents(WebUtil
					.getThemeDisplay(request).getScopeGroupId(), "", -1, -1);
			// calEvents=
			// CalEventLocalServiceUtil.getEvents(WebUtil.getThemeDisplay(request).getScopeGroupId(),
			// cal );
			LOG.debug("calEvents size" + calEvents.size());
			for (CalEvent calEvent : calEvents) {
				Date startDate = calEvent.getStartDate();
				LOG.debug("date..................." + date);
				LOG.debug("startDate.............." + startDate);
				if (checkEventExpired(date, startDate)) {
					calEventsList.add(calEvent);
				}
			}
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return calEventsList;
	}

	private boolean checkEventExpired(Date date1, Date date2) {
		boolean flag = false;

		if (date1.compareTo(date2) > 0) {
			LOG.debug("Date1 is after Date2");
		} else if (date1.compareTo(date2) < 0) {
			flag = true;
			LOG.debug("Date1 is before Date2");
		} else if (date1.compareTo(date2) == 0) {
			LOG.debug("Date1 is equal to Date2");
		} else {
			LOG.debug("How to get here?");
		}

		return flag;

	}

	@ResourceMapping("showEvents")
	public String showCalEvents(ResourceRequest request,
			ResourceResponse response) {
		LOG.debug("show Events..................IN Resource URL...........");
		return "../calendar/showEvents";
	}

}
