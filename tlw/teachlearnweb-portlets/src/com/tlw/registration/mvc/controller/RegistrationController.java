/*
 * This code is property of Teach Learn Web, Inc. Use, duplication and disclosure
 * in any form without permission of copyright holder is prohibited.
 *
 * (C) Copyright Teach Learn Web, Inc. 2012. All rights reserved.
 */
package com.tlw.registration.mvc.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.DuplicateUserEmailAddressException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Country;
import com.liferay.portlet.asset.model.AssetCategory;
import com.teachlearnweb.service.vo.User;
import com.tlw.controller.AbstractBaseController;
import com.tlw.registration.mvc.vo.RegistrationModelBean;
import com.tlw.util.WebUtil;

/**
 * The Class RegistrationController, is responsible to handle the user
 * registration request.
 * 
 * @author asharma
 * @author arvindk
 */
@Controller("value=registrationController")
@Scope("session")
@RequestMapping(value = "VIEW")
public class RegistrationController extends AbstractBaseController {

	/** The LOG. */
	private static final Logger LOG = Logger
			.getLogger(RegistrationController.class);

	/** The errormsg key. */
	private static final String ERROR_MSG_KEY = "errormsg";

	@Autowired
	@Qualifier("registrationFormValidator")
	private Validator registrationFormValidator;

	private boolean alphaNumericSortEnabled = true;

	/**
	 * Show registration.
	 * 
	 * @param renderResponse
	 *            the render response
	 * @return the string
	 */
	@RenderMapping
	public String showRegistration(RenderResponse renderResponse) {
		
		return "registrationForm";
	}
	
	/**
	 * Show registration.
	 * 
	 * @param renderResponse
	 *            the render response
	 * @return the string
	 */
	@RenderMapping(params = "action=showChangePassword")
	public String showChangePassword(RenderResponse renderResponse) {
		LOG.debug("change password..........");
		return "changePassword";
	}

	/**
	 * Gets the command object.
	 * 
	 * @return the command object
	 */
	@ModelAttribute("registrationModelBean")
	public RegistrationModelBean getCommandObject() {
		LOG.debug("Creating object of model RegistrationModelBean..");
		RegistrationModelBean registrationModelBean = new RegistrationModelBean();
		/*
		 * FaceBookService facebookService =
		 * serviceLocator.getFaceBookService();
		 * registrationModelBean.setFacebookCancelUrl(facebookService
		 * .getOnCancelRedirectionUrl());
		 */
		return registrationModelBean;
	}
	/**
	 * Gets the command object.
	 * 
	 * @return the command object
	 */
	@ModelAttribute("getCountries")
	public List<Country> getCountries() {
					
		List<Country> countryList = WebUtil.getCountries();	
		
		return countryList;
	}
	/**
	 * Gets the command object.
	 * 
	 * @return the command object
	 */
	@ModelAttribute("getCategories")
	public List<AssetCategory> getCategories(PortletRequest request) {
		String vocabularyName  = "Course Management";			
		List<AssetCategory> assetCategoryList = WebUtil.getRootCategories(vocabularyName, request);	
		
		return assetCategoryList;
	}
	@ActionMapping(params = "action=doRegistration")
	public void doRegistration(
			@ModelAttribute RegistrationModelBean registrationBean,
			BindingResult bindingResult, ActionRequest actionRequest,
			ActionResponse actionResponse, SessionStatus sessionStatus) throws PortalException, SystemException {
		
		LOG.info("Request received to register the user...");
		 System.out.println("**********************************************RegistraionStart................................................................................**************************************************************************************");
		registrationFormValidator.validate(registrationBean, bindingResult);	
	   
		long scopeGroupId = WebUtil.getThemeDisplay(actionRequest).getScopeGroupId();
		registrationBean.setDuplicateEmail(false);
		registrationBean.setShowForm(true);
		/*System.out.println("...............scopeGroupId.."+scopeGroupId);
		System.out.println("...............bindingResult.hasErrors().."+bindingResult.hasErrors());*/
		if (!bindingResult.hasErrors()) {
			LOG.info("Request received to register the user and successfully validated...");
			User rBean = populateRegistrationModelBean(registrationBean, actionRequest);
			rBean.setDob("1/Jan/1970"); //default DOB
			User registeredUser = null;
			
			try{
				registeredUser = serviceLocator.getUserService().addUser(rBean,WebUtil.getUserId(actionRequest),WebUtil.getCompanyId(actionRequest),scopeGroupId);
			}catch (Exception e) {
				if (e instanceof DuplicateUserEmailAddressException)
				{
					registrationBean.setDuplicateEmail(true);
				}
			}
			
			if(null != registeredUser){
				registrationBean.setSuccess(true);
				registrationBean.setShowForm(false);
				String phoneNo = registeredUser.getPhone();
				if(StringUtils.isNotBlank(phoneNo)){
					registrationBean.setSendMessage(true);
					sendMessageToMobile(registeredUser);
				}
				
			}
			//LOG.info("reigisteredUser UserId...................................................................................................................."+reigisteredUser.getUserId());
        }
		
		 System.out.println("**********************************************Registraion End................................................................................**************************************************************************************");
		
		
	}
	
	private void sendMessageToMobile(User registeredUser) {
		System.out.println("Sending Message to mobile : " + registeredUser.getPhone());
	}

	@ActionMapping(params = "action=changePassword")
	public void changePassword(
			@ModelAttribute RegistrationModelBean registrationBean,
			BindingResult bindingResult, ActionRequest actionRequest,
			ActionResponse actionResponse, SessionStatus sessionStatus) throws PortalException, SystemException {
		LOG.debug("Change Password Entry............................................................");
		User user = serviceLocator.getUserService().updatePassword(WebUtil.getUserId(actionRequest), registrationBean.getPassword1(), registrationBean.getPassword2(), false);
		
		LOG.debug("Updated password...................."+user.getPassword());
		
	}

	private User populateRegistrationModelBean(
			RegistrationModelBean registrationBean, ActionRequest actionRequest) {
		com.teachlearnweb.service.vo.User rBean = new com.teachlearnweb.service.vo.User();
		try{
		BeanUtils.copyProperties(registrationBean, rBean);
		rBean.setLocale(WebUtil.getThemeDisplay(actionRequest).getLocale());
		/*SimpleDateFormat format = new SimpleDateFormat();
		Date myDate = format.parse(registrationBean.getDob());
		rBean.setDay(String.valueOf(myDate.getDay()));
		rBean.setMonth(String.valueOf(myDate.getMonth()));
		rBean.setYear(String.valueOf(myDate.getYear()));*/
		}catch(Exception exception){
			exception.printStackTrace();
		}
		return rBean;
	}
	 @ResourceMapping("getClasses")
		public ModelAndView getClasses(ResourceRequest resourceRequest) throws PortalException, SystemException {
	    	Map<String,List<AssetCategory>> model = null;
	    	
	    	if(null != resourceRequest.getParameter("categoryId")){
	    		
	    	model = new HashMap<String, List<AssetCategory>>();
			LOG.info("Request received to classes...");
			long categoryId = Long.valueOf(resourceRequest.getParameter("categoryId"));
			List<AssetCategory> classesList = WebUtil.getChildCategories(categoryId,0);
			
			if(alphaNumericSortEnabled ){
				classesList = sortOnClass(classesList);
			}
			LOG.debug("categoryID--size*--->"+classesList.size());
			model.put("classes", classesList);

	    	}
			return new ModelAndView("ajaxClasses",model);
		}

	private List<AssetCategory> sortOnClass(List<AssetCategory> classesList) {
		List<AssetCategory> list = new ArrayList<AssetCategory>();
		Map<Integer, AssetCategory> map = new TreeMap<Integer, AssetCategory>();
		try{
		for(AssetCategory cat :  classesList){
			String classNo = cat.getName().split("Class ")[1];
			map.put(Integer.parseInt(classNo),  cat);
		}
		for(Integer key : map.keySet()){
			list.add(map.get(key));
		}
		}catch (Exception e) {
			list = classesList;
			System.out.println("Exception while sorting Categories for Registration portlet" + e.getMessage());
			LOG.error("Exception while sorting Categories for Registration portlet" + e.getMessage());
			//e.printStackTrace();
		}
		return list;
	}
}
