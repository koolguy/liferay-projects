package com.tlw.registration.mvc.validator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import com.tlw.util.ValidationHelper;
import com.tlw.registration.mvc.vo.RegistrationModelBean;


@Component("registrationFormValidator")
public class RegistrationFormValidator implements Validator {
	
	/** The logger. */
    private static final Log LOG = LogFactory
            .getLog(RegistrationFormValidator.class);
	
	public boolean supports(Class<?> klass) {
		return RegistrationModelBean.class.isAssignableFrom(klass);
	}

	public void validate(Object target, Errors errors) {
		RegistrationModelBean registrationFormBean = (RegistrationModelBean)target;
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "firstName", "NotEmpty.firstName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "lastName", "NotEmpty.lastName");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emailAddress", "error.blankfield");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "parentName", "NotEmpty.parentName");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "schoolName", "NotEmpty.schoolName");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "className", "NotEmpty.className");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "sectionName", "NotEmpty.sectionName");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "state", "NotEmpty.state");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "country", "NotEmpty.country");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", "NotEmpty.city");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "schoolCity", "NotEmpty.schoolCity");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "schoolLocality", "NotEmpty.schoolLocality");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "syllabus", "NotEmpty.syllabus");
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "jobTitle", "NotEmpty.jobTitle");
		
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "NotEmpty.password");
		LOG.debug("Validating user email address.");
		//validateEmailAddress(registrationFormBean, errors);
		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "NotEmpty.phone");

		

        Errors emailErrors = new BeanPropertyBindingResult(target, errors.getObjectName());
        ValidationUtils.rejectIfEmptyOrWhitespace(emailErrors, "emailAddress", "NotEmpty.emailAddress");
       // validateEmailAddress(registrationFormBean, emailErrors);
        Errors phoneErrors = new BeanPropertyBindingResult(target, errors.getObjectName());
        ValidationUtils.rejectIfEmptyOrWhitespace(phoneErrors, "phone", "NotEmpty.phone");

        //validatePhoneNo(registrationFormBean, phoneErrors);

        if(emailErrors.hasErrors() && phoneErrors.hasErrors()){
            errors.rejectValue("emailAddress", "email.or.phone.required");
        }else if(!emailErrors.hasErrors() && phoneErrors.hasErrors()){ //email populated
        	validateEmailAddress(registrationFormBean, errors); // check if email is valid
        }else if(emailErrors.hasErrors() && !phoneErrors.hasErrors()){ //phone populated
        	validatePhoneNo(registrationFormBean, errors); // check if phone is valid
        }else if(!emailErrors.hasErrors() && !phoneErrors.hasErrors()){ //both are populated
        	validateEmailAddress(registrationFormBean, errors); // check if email is valid
        	validatePhoneNo(registrationFormBean, errors); // check if phone is valid
        }
		
		
		//validatePhoneNo(registrationFormBean, errors);
	    //validateDob(registrationFormBean, errors);
	    //ValidationHelper.checkName("firstName", registrationFormBean.getFirstName(), errors,"firstname.invalid");
	    //ValidationHelper.checkName("lastName", registrationFormBean.getLastName(), errors,"lastname.invalid");
	    //ValidationHelper.checkName("parentName", registrationFormBean.getParentName(), errors,"parentname.invalid");
	    //ValidationHelper.checkName("schoolName", registrationFormBean.getSchoolName(), errors,"schoolname.invalid");
	    //ValidationHelper.checkName("schoolcity", registrationFormBean.getSchoolCity(), errors,"schoolcity.invalid");
	   // ValidationHelper.checkName("state", registrationFormBean.getState(), errors,"state.invalid");
	   // ValidationHelper.checkName("city", registrationFormBean.getCity(), errors,"city.invalid");
	    
	}
	
	
	/**
     * Validate email address.
     * @param registrationBean the registration bean
     * @param errors the errors
     */
    private void validateEmailAddress(
            final RegistrationModelBean registrationBean, final Errors errors) {
    	if (registrationBean.getEmailAddress() == null
                || registrationBean.getEmailAddress().equals("")) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "emailAddress",
                    "error.blankfield");
        } else {
            com.tlw.util.ValidationHelper.checkEmail("emailAddress",
                    registrationBean.getEmailAddress(), errors);
        }
    }

    /**
     * Validate password.
     * @param registrationBean the registration bean
     * @param errors the errors
     */
    /*private void validatePassword(final RegistrationModelBean registrationBean,
            final Errors errors) {
        if (registrationBean.getPassword() == null
                || registrationBean.getPassword().equals("")) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password",
                    "error.blankfield");
        } else if (registrationBean.getPassword().length() < Constants.PASSWORD_MIN_LENGTH) {
            errors.rejectValue("password", "error.password.length");
        }

        if (registrationBean.getConfirmPassword() == null
                || registrationBean.getConfirmPassword().equals("")) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors,
                    "confirmPassword", "error.blankfield");
        } else if (registrationBean.getConfirmPassword().length() < Constants.PASSWORD_MIN_LENGTH) {
            errors.rejectValue("confirmPassword", "error.password.length");
        }
    }*/
	/**
     * Validate email address.
     * @param registrationBean the registration bean
     * @param errors the errors
     */
    private void validatePhoneNo(
            final RegistrationModelBean registrationBean, final Errors errors) {
        
    
        if (registrationBean.getPhone() == null
                || registrationBean.getPhone().equals("")) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "NotEmpty.phone");
        } else {
            com.tlw.util.ValidationHelper.checkPhoneNumber("phone", registrationBean.getPhone(), errors);
        }
    
    }
    private void validateDob(
            final RegistrationModelBean registrationBean, final Errors errors) {
            if (registrationBean.getDob() == null
                || registrationBean.getDob().equals("")) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dob", "NotEmpty.dob");
        } else {
            com.tlw.util.ValidationHelper.checkDob("dob", registrationBean.getDob(), errors);
        }
    
    }
        
}
