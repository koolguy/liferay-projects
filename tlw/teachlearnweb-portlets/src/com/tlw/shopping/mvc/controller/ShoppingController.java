/*
 * This code is property of Teach Learn Web, Inc. Use, duplication and disclosure
 * in any form without permission of copyright holder is prohibited.
 *
 * (C) Copyright Teach Learn Web, Inc. 2012. All rights reserved.
 */
package com.tlw.shopping.mvc.controller;

import java.util.HashMap;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.util.PortalUtil;
import com.teachlearnweb.dao.entity.CouponCode;
import com.tlw.controller.AbstractBaseController;
import com.tlw.util.WebUtil;


/**
 * The Class RegistrationController, is responsible to handle the user
 * registration request.
 * 

 * @author Srini G 
 */
@Controller("value=shoppingController")
@Scope("session")
@RequestMapping(value = "VIEW")
public class ShoppingController extends AbstractBaseController {

	/** The LOG. */
	private static final Logger LOG = Logger
			.getLogger(ShoppingController.class);

	/** The errormsg key. */
	private static final String ERROR_MSG_KEY = "errormsg";

	/*@Autowired
	@Qualifier("shippingFormValidator")
	private Validator shippingFormValidator;
*/
	/**
	 * Show registration.
	 * 
	 * @param renderResponse
	 *            the render response
	 * @return the string
	 */
	 @RenderMapping
	public String showShopping(RenderRequest renderRequest, RenderResponse renderResponse) {
        String map = renderRequest.getParameter("actionMapping");
        
        if(null==map){
        	map = "billing";
        }
        
        LOG.debug("map------------------"+map);
        	/*AssetCategory assetCategory = WebUtil.getAssetCategoryById(Long.valueOf(renderRequest.getParameter("categoryId")));
            User loggedInUser = WebUtil.getUser(renderRequest);
            renderRequest.setAttribute("assetCategory",assetCategory);
            renderRequest.setAttribute("loggedInUser",loggedInUser);
            renderRequest.setAttribute("parentCategory",WebUtil.getAssetCategoryById(assetCategory.getParentCategoryId()));
            renderRequest.setAttribute("amount",WebUtil.getAmount(assetCategory.getCategoryId()));
        */
        HttpServletRequest servletreq = PortalUtil.getHttpServletRequest(renderRequest);
		HttpSession session = PortalUtil.getOriginalServletRequest(servletreq).getSession();
		Map cartMap = (Map)session.getAttribute("cartItems");
		
			renderRequest.setAttribute("cartItems",cartMap);
			renderRequest.setAttribute("totalCartAmount",WebUtil.getTotalCartAmount(cartMap));
    	return map;
    }

	/**
	 * Gets the command object.
	 * 
	 * @return the command object
	 
	@ModelAttribute("registrationModelBean")
	public RegistrationModelBean getCommandObject() {
		LOG.debug("Creating object of model RegistrationModelBean..");
		RegistrationModelBean registrationModelBean = new RegistrationModelBean();
		/*
		 * FaceBookService facebookService =
		 * serviceLocator.getFaceBookService();
		 * registrationModelBean.setFacebookCancelUrl(facebookService
		 * .getOnCancelRedirectionUrl());
		 
		return registrationModelBean;
	}*/
	/**
	 * Gets the command object.
	 * 
	 * @return the command object
	 */
	
	/**
	 * Gets the command object.
	 * 
	 * @return the command object
	 
	@ModelAttribute("getCategories")
	public List<AssetCategory> getCategories(PortletRequest request) {
		String vocabularyName  = "Course Management";			
		List<AssetCategory> assetCategoryList = WebUtil.getRootCategories(vocabularyName, request);	
		
		return assetCategoryList;
	}
	@ActionMapping(params = "action=doRegistration")
	public void doRegistration(
			@ModelAttribute RegistrationModelBean registrationBean,
			BindingResult bindingResult, ActionRequest actionRequest,
			ActionResponse actionResponse, SessionStatus sessionStatus) throws PortalException, SystemException {
		
		LOG.info("Request received to register the user...");
		registrationFormValidator.validate(registrationBean, bindingResult);	
	
		long scopeGroupId = WebUtil.getThemeDisplay(actionRequest).getScopeGroupId();
		registrationBean.setDuplicateEmail(false);
		registrationBean.setShowForm(true);
		if (!bindingResult.hasErrors()) {
			
			User rBean = populateRegistrationModelBean(registrationBean, actionRequest);
			
			User reigisteredUser = null;
			
			try{
				reigisteredUser = serviceLocator.getUserService().addUser(rBean,WebUtil.getUserId(actionRequest),WebUtil.getCompanyId(actionRequest),scopeGroupId);
			}catch (Exception e) {
				if (e instanceof DuplicateUserEmailAddressException)
				{
					registrationBean.setDuplicateEmail(true);
				}
			}
			
			if(null != reigisteredUser){
				registrationBean.setSuccess(true);
				registrationBean.setShowForm(false);
			}
			//LOG.info("reigisteredUser UserId...................................................................................................................."+reigisteredUser.getUserId());
        }
		
		
	}
	*/
	

	/*private User populateRegistrationModelBean(
			RegistrationModelBean registrationBean, ActionRequest actionRequest) {
		com.teachlearnweb.service.vo.User rBean = new com.teachlearnweb.service.vo.User();
		try{
		BeanUtils.copyProperties(registrationBean, rBean);
		rBean.setLocale(WebUtil.getThemeDisplay(actionRequest).getLocale());
		/*SimpleDateFormat format = new SimpleDateFormat();
		Date myDate = format.parse(registrationBean.getDob());
		rBean.setDay(String.valueOf(myDate.getDay()));
		rBean.setMonth(String.valueOf(myDate.getMonth()));
		rBean.setYear(String.valueOf(myDate.getYear()));*/
		/*}catch(Exception exception){
			exception.printStackTrace();
		}
		return rBean;
	}*/
	 @ActionMapping(params = "action=addShipping")
		public void addShipping(@RequestParam String categoryId,
				@RequestParam String categoryName,
				ActionRequest actionRequest,
				ActionResponse actionResponse,SessionStatus sessionStatus)
				throws PortalException, SystemException {

			LOG.info("Request received to subscribeCourse the user...");
			System.out
					.println("Make Payment............shipping form.........categoryId.................."
							+ categoryId);
			// serviceLocator.getUserService().addCourse(WebUtil.getUserId(actionRequest),categoryId);

			
			actionResponse.setRenderParameter("actionMapping", "makePayment");
			actionResponse.setRenderParameter("categoryId", categoryId);

		}
	 
	 @ActionMapping(params = "action=shopping")
		public void shopping(@RequestParam String categoryId,
				@RequestParam String categoryName,
				ActionRequest actionRequest,
				ActionResponse actionResponse)
				throws PortalException, SystemException {

			LOG.info("Request received to subscribeCourse the user...");
			System.out
					.println("Make Payment............shopping.........categoryId.................."
							+ categoryId);
			// serviceLocator.getUserService().addCourse(WebUtil.getUserId(actionRequest),categoryId);
			actionResponse.setRenderParameter("actionMapping", "billing");
			actionResponse.setRenderParameter("categoryId", categoryId);

		}
		
	 
		@ResourceMapping("getDiscount")
		public ModelAndView getDiscount(ResourceRequest resourceRequest)
				throws PortalException, SystemException {
			String coupon = resourceRequest.getParameter("coupon");
			CouponCode couponDetail = serviceLocator.getCouponCodeService().fetchCouponDetailByCode(coupon);
			LOG.debug("couponDetail.getAmount()........................"+couponDetail.getAmount());
			LOG.debug("couponDetail.getCouponCode()...................."+couponDetail.getCouponCode());
			
			resourceRequest.setAttribute("discount",couponDetail.getAmount());
			Map model = new HashMap<String, String>();
			return new ModelAndView("showDiscount",model);
		}
		@ResourceMapping("shipping")
		public String getShipping(ResourceRequest resourceRequest)
				throws PortalException, SystemException {
			//resourceRequest.setAttribute("discount","50");
		//Map model = new HashMap<String, String>();
			return "shippingForm";
		}
		
	

}
