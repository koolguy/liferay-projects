/*
 * This code is property of Teach Learn Web, Inc. Use, duplication and disclosure
 * in any form without permission of copyright holder is prohibited.
 *
 * (C) Copyright Teach Learn Web, Inc. 2012. All rights reserved.
 */
package com.tlw.profileImage.mvc.controller;

import java.io.InputStream;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.liferay.portal.NoSuchUserException;
import com.liferay.portal.UserPortraitSizeException;
import com.liferay.portal.UserPortraitTypeException;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.UploadException;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.FileUtil;
import com.liferay.portal.security.auth.PrincipalException;
import com.liferay.portal.service.UserServiceUtil;
import com.liferay.portal.util.PortalUtil;
import com.teachlearnweb.dao.entity.RewardPoints;
import com.teachlearnweb.service.vo.User;
import com.tlw.controller.AbstractBaseController;
import com.tlw.util.Constants;
import com.tlw.util.WebUtil;


/**
 * The Class MyProfileController, is responsible to handle the user.
 * 
 */
@Controller("value=profileImageController")
@RequestMapping(value = "VIEW")
public class ProfileImageController extends AbstractBaseController {

	/** The LOG. */
	private static final Log logger  = LogFactory.getLog(ProfileImageController.class);
	
	/** The Constant SHOW_EDIT_PROFILE_IMAGE. */
    private static final String SHOW_EDIT_PROFILE_IMAGE = "showEditProfileImage";

    /** The Constant CANCEL_EDIT_PROFILE_IMAGE. */
    private static final String CANCEL_EDIT_PROFILE_IMAGE = "cancelEditProfileImage";
    
    

    /** The Constant ACTION. */
    private static final String ACTION = "action";
	
	/**
	 * Default Render Mapping Method.
	 *
	 * @param renderRequest
	 * @param renderResponse
	 * @return view name
	 */
	@RenderMapping
	public String showProfileImage(RenderRequest renderRequest,RenderResponse renderResponse) {
		logger.debug("From the ProfileImageController default RenderMapping.");
		return Constants.PROFILE_IMAGE_JSP;
	}
	
	/**
     * Method to add image.
     * @param response the response
     * @param request the request
	 * @throws Exception 
     */
    @ActionMapping(params = "myaction=editImage")
    public void editImage(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception 
    {
    	try {
			
			updatePortrait(actionRequest);
			SessionMessages.add(actionRequest, "request_processed");
			//actionResponse.setRenderParameter(ACTION, SHOW_EDIT_PROFILE_IMAGE);
		}
		catch (Exception e) {
			if (e instanceof NoSuchUserException || e instanceof PrincipalException) {
				SessionErrors.add(actionRequest, e.getClass());
				actionResponse.setRenderParameter(ACTION, SHOW_EDIT_PROFILE_IMAGE);
			}
			else if (e instanceof UploadException || e instanceof UserPortraitSizeException || e instanceof UserPortraitTypeException) {
				SessionErrors.add(actionRequest, e.getClass());
				actionResponse.setRenderParameter(ACTION, SHOW_EDIT_PROFILE_IMAGE);
			}
			else {
				throw e;
			}
		}
		
    }
    
    protected void updatePortrait(ActionRequest actionRequest)throws Exception 
    {
    	UploadPortletRequest uploadPortletRequest = PortalUtil.getUploadPortletRequest(actionRequest);
    	//ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
    	
    	User tlwUser = WebUtil.getUser(actionRequest);
		InputStream inputStream = uploadPortletRequest.getFileAsStream("fileName");
		if (inputStream == null) {
			throw new UploadException();
		}
		byte[] bytes = FileUtil.getBytes(inputStream);
		UserServiceUtil.updatePortrait(tlwUser.getUserId(), bytes);
		
		
    }
    
    /**
     * Method to render EditProfileImage.
     * @param response the response
     * @param request the request
	 * @throws Exception 
     */
    @ActionMapping(params = "myaction=renderEditProfileImage")
    public void renderEditProfileImage(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception 
    {
    	actionResponse.setRenderParameter(ACTION, SHOW_EDIT_PROFILE_IMAGE);
    }
    
    /**
     * Method to render CancelEditProfileImage.
     * @param response the response
     * @param request the request
	 * @throws Exception 
     */
    @ActionMapping(params = "myaction=cancelEditProfileImage")
    public void cancelEditProfileImage(ActionRequest actionRequest, ActionResponse actionResponse) throws Exception 
    {
    	actionResponse.setRenderParameter(ACTION, CANCEL_EDIT_PROFILE_IMAGE);
    }
    
    /**
     * Method to render editProfileImage.jsp.
     * @param response the response
     * @return the string
     */
    @RenderMapping(params = "action=showEditProfileImage")
    public String showEditProfileImage(RenderResponse response) {
    	 return Constants.EDIT_PROFILE_IMAGE_JSP;
    }
    
    /**
     * Method to render ProfileImage.jsp.
     * @param response the response
     * @return the string
     */
    @RenderMapping(params = "action=cancelEditProfileImage")
    public String cancelEditProfileImage(RenderResponse response) {
        return Constants.PROFILE_IMAGE_JSP;
    }
    
    @ResourceMapping("showProfile")
    public String showProfile(ResourceRequest request, ResourceResponse response) {
    	logger.debug("show Profile..................IN Resource URL...........");
        return "../profileImage/profileImage";
    }
    
    /**
	 * Gets the command object.
	 * 
	 * @return the command object
	 */
	@ModelAttribute("rewardPoints")
	public List<RewardPoints> getRewardPoints(PortletRequest request) {
    
	List<RewardPoints> rewards = serviceLocator.getRewardPointsService().rewardPointsByUserId(WebUtil.getUserId(request));
	return rewards;
	}
    
}