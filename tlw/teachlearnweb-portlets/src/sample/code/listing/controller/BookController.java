package sample.code.listing.controller;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.RenderResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import sample.code.listing.domain.Book;

import com.teachlearnweb.service.vo.BookVO;
import com.tlw.controller.AbstractBaseController;

/**
 * BookController shows the books in the book Catalog.
 * @author asarin
 *
 */
@Controller("bookController")
@RequestMapping(value = "VIEW")
public class BookController extends AbstractBaseController{
	private static final Log logger = LogFactory.getLog(BookController.class);

	// --maps the incoming portlet request to this method
	@RenderMapping
	public String showBooks(RenderResponse response) {
		return "home";
	}

	// -- @ModelAttribute here works as the referenceData method
	@ModelAttribute(value="books")
	public List<Book> getBooks() {
		List<Book> books = new ArrayList<Book>();
		List<BookVO> bookVoList = serviceLocator.getBookService().getBooks();
		logger.debug("bookVoList size.............."+bookVoList.size());
		/*for (int i = 0; i < bookVoList.size(); i++) {
			BeanUtils.copyProperties(bookVoList(i), booksList(i));
		}*/
		
		
		books.add(new Book("AspectJ in Action, Second Edition", "Ramnivas Laddad", Long.valueOf("1933988053")));
		books.add(new Book("ActiveMQ in Action", "Bruce Snyder, Dejan Bosanac, and Rob Davies", Long.valueOf("1933988940")));
		books.add(new Book("Hadoop in Action", "Chuck Lam", Long.valueOf("9781935182191")));
		books.add(new Book("JUnit in Action, Second Edition", "Petar Tahchiev, Felipe Leme, Vincent Massol, and Gary Gregory", Long.valueOf("9781935182023")));
		
		return books;
	}
}
