<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<c:if
	test="${fn:length(selectedAssetsList) eq 0 and fn:toLowerCase(selectedSection) != 'presentation' and fn:toLowerCase(selectedSection) != 'video'}">
	<span>-Please wait for updation-</span>
</c:if>
<!-- Video start -->
<c:if test="${fn:toLowerCase(selectedSection) eq 'video'}">
	<%-- <ul>
		<c:forEach var="assetEntry" items="${selectedAssetsList}">
 			<li class="worksheetstausbox">
                                    <div class="worksheetboxtop">
                                        <a href="#" onclick="javascript:showContent('${assetEntry.groupId}','${assetEntry.folderId}','${assetEntry.title}','${assetEntry.assetType}','${assetEntry.extension}')">
                                         <p class="worksheetnumber">${assetEntry.title}</p>
                                         <div class="outerdiv">
                                            <div class="worksheetthumb"><img src="<%=themeDisplay.getPathThemeImages()%>/worksheeticon.png"></div>	
                                            <!-- <div class="worksheetreport">
                                                <p class="wsnofq">30 Questions</p>
                                                <p class="wsnoft">15 Minutes</p>
                                                <p class="wsttatus">Not Started</p>
                                            </div> -->
                                         </div>
                                            </a>
                                       </div>
                                    <div class="worksheetbottom outerdiv">
                                        <div class="wsschedul"><a rel="closedemosection" class="lightboxopen"></a></div>
                                        <div class="wsstime">
                                            <span>12 MAY</span>
                                            <span>11.30 pm</span>
                                        </div>
                                  	</div>
                                </li>
		</c:forEach>
		</ul> --%>
	<script type="text/javascript" src="/tlw-theme/JWPlayer/jwplayer.js" ></script>
	<div id='mediaplayer'></div>
	
		<script type="text/javascript">
	  jwplayer('mediaplayer').setup({
	    'flashplayer': '/tlw-theme/JWPlayer/player.swf',
	    'id': 'playerID',
	    'width': '100%',
	    'height': '100%',
	    'provider': 'rtmp',
		'streamer' : "rtmp://s22iyal2hsocxz.cloudfront.net/cfx/st/",
		//file : "PoweredLearning/CBSE/Maths/Class 10/Areas Related to Circles/Perimeter_and_Area_of_a_Circle_12_2.flv"
		'file' : '${fn:toLowerCase(subject_syllabus)}/${fn:toLowerCase(subject_className)}/${fn:toLowerCase(topics_assetCatName)}/${fn:toLowerCase(topics_level1Child)}/${fn:toLowerCase(topics_level2Child)}/1.flv',
	    'image': '/tlw-theme/images/logo.png',
	    'dock': 'true', 
		'stretching': 'uniform',
		'screencolor' : 'FFFFFF',
		'plugins': {
		         'viral-2': {'oncomplete':'False','onpause':'False','functions':'All'}
		}
	  });
	</script>
</c:if>
<!-- Video end -->

<!-- Presentation start -->
<c:if test="${fn:toLowerCase(selectedSection) eq 'presentation'}">
	<div id="flashRoot" style="height: 100%;"></div>
	<script src='<%=themeDisplay.getPathThemeJavaScript()%>/loadflash.js'>
		
	</script>
	<script type="text/javascript" language="JavaScript">
		showFlash("/presentations/${strSubTopicId}/viewer.swf", '100%', '100%', false);
	</script>
</c:if>
<!-- Presentation end -->

<!-- All sections other than Video and Presentation start -->
<c:if
	test="${(fn:toLowerCase(selectedSection) != 'video') and (fn:toLowerCase(selectedSection) != 'presentation')}">
	<ul>
		<c:forEach var="assetEntry" items="${selectedAssetsList}">
			<li class="worksheetstausbox">
				<div class="worksheetboxtop">
					<a href="#"
						onclick="javascript:showContent('${assetEntry.groupId}','${assetEntry.folderId}','${assetEntry.title}','${assetEntry.assetType}','${assetEntry.extension}')">
						<p class="worksheetnumber">${assetEntry.title}</p>
						<div class="outerdiv">
							<div class="worksheetthumb">
								<img
									src="<%=themeDisplay.getPathThemeImages()%>/worksheeticon.png">
							</div>
						</div> </a>
				</div>
				<div class="worksheetbottom outerdiv">
					<div class="wsschedul">
						<a rel="closedemosection" class="lightboxopen"></a>
					</div>
					<div class="wsstime">
						<span>12 MAY</span> <span>11.30 pm</span>
					</div>
				</div>
			</li>
		</c:forEach>
	</ul>
</c:if>
<!-- All sections other than Video and Presentation start -->
<%-- 
<!-- Notes start -->	
	<c:if test="${selectedSection == 'NOTES' or selectedSection == 'Notes'}" >
		<ul>
		<c:forEach var="assetEntry" items="${selectedAssetsList}">
                            	<li class="worksheetstausbox">
                                    <div class="worksheetboxtop">
                                        <a href="#" onclick="javascript:showContent('${assetEntry.groupId}','${assetEntry.folderId}','${assetEntry.title}','${assetEntry.assetType}','${assetEntry.extension}')">
                                         <p class="worksheetnumber">${assetEntry.title}</p>
                                         <div class="outerdiv">
                                            <div class="worksheetthumb"><img src="<%=themeDisplay.getPathThemeImages()%>/worksheeticon.png"></div>	
                                           <!--  <div class="worksheetreport">
                                                <p class="wsnofq">30 Questions</p>
                                                <p class="wsnoft">15 Minutes</p>
                                                <p class="wsttatus">Not Started</p>
                                            </div> -->
                                         </div>
                                            </a>
                                       </div>
                                    <div class="worksheetbottom outerdiv">
                                        <div class="wsschedul"><a rel="closedemosection" class="lightboxopen"></a></div>
                                        <div class="wsstime">
                                            <span>12 MAY</span>
                                            <span>11.30 pm</span>
                                        </div>
                                  	</div>
                                </li>
 			</c:forEach>
 	</ul>  
	</c:if>
<!-- Notes end -->	

<!-- Dictionary start -->	
	<c:if test="${selectedSection == 'Dictionary'}" >
	<ul>
		<c:forEach var="assetEntry" items="${selectedAssetsList}">
                            	<li class="worksheetstausbox">
                                    <div class="worksheetboxtop">
                                        <a href="#" onclick="javascript:showContent('${assetEntry.groupId}','${assetEntry.folderId}','${assetEntry.title}','${assetEntry.assetType}','${assetEntry.extension}')">
                                         <p class="worksheetnumber">${assetEntry.title}</p>
                                         <div class="outerdiv">
                                            <div class="worksheetthumb"><img src="<%=themeDisplay.getPathThemeImages()%>/worksheeticon.png"></div>	
                                            <!-- <div class="worksheetreport">
                                                <p class="wsnofq">30 Questions</p>
                                                <p class="wsnoft">15 Minutes</p>
                                                <p class="wsttatus">Not Started</p>
                                            </div> -->
                                         </div>
                                            </a>
                                       </div>
                                    <div class="worksheetbottom outerdiv">
                                        <div class="wsschedul"><a rel="closedemosection" class="lightboxopen"></a></div>
                                        <div class="wsstime">
                                            <span>12 MAY</span>
                                            <span>11.30 pm</span>
                                        </div>
                                  	</div>
                                </li>
 			</c:forEach>
 	</ul>  
	</c:if>
<!-- Dictionary end -->	

<!-- Worksheet start -->	
	<c:if test="${selectedSection == 'Worksheet'}" >
	<ul>
		<c:forEach var="assetEntry" items="${selectedAssetsList}">
                            	<li class="worksheetstausbox">
                                    <div class="worksheetboxtop">
                                        <a href="#" onclick="javascript:showContent('${assetEntry.groupId}','${assetEntry.folderId}','${assetEntry.title}','${assetEntry.assetType}','${assetEntry.extension}')">
                                         <p class="worksheetnumber">${assetEntry.title}</p>
                                         <div class="outerdiv">
                                            <div class="worksheetthumb"><img src="<%=themeDisplay.getPathThemeImages()%>/worksheeticon.png"></div>	
                                           <!--  <div class="worksheetreport">
                                                <p class="wsnofq">30 Questions</p>
                                                <p class="wsnoft">15 Minutes</p>
                                                <p class="wsttatus">Not Started</p>
                                            </div> -->
                                         </div>
                                            </a>
                                       </div>
                                    <div class="worksheetbottom outerdiv">
                                        <div class="wsschedul"><a rel="closedemosection" class="lightboxopen"></a></div>
                                        <div class="wsstime">
                                            <span>12 MAY</span>
                                            <span>11.30 pm</span>
                                        </div>
                                  	</div>
                                </li>
 			</c:forEach>
 	</ul>  
 	</c:if>
<!-- Worksheet end -->	

<!-- Inline Test start -->	
	<c:if test="${selectedSection == 'Inline Test'}" >
			<ul>
				<c:forEach var="assetEntry" items="${selectedAssetsList}">
                            	<li class="inlineteststausbox">
                                    <div class="inlinetestboxtop">
                                         <a href="#">
                                         <p class="inlinenumber">${assetEntry.title}</p>
                                         <div class="outerdiv">
                                            <div class="inlinethumb"><img src="images/inlinetesticon.png"></div>	
                                            <div class="inlinetestreport">
                                                <p class="itnofq">5 Questions</p>
                                                <p class="itnoft">5 Minutes</p>
                                                <p class="itnoft">5 Reward Points</p>
                                                <p class="itstatus">Not Started</p>
                                            </div>
                                         </div>
                                            </a>
                                       </div>
                                    <div class="inlinetestbottom outerdiv">
                                        <div class="itschedul"><a rel="closedemosection" class="lightboxopen">Scheduled</a></div>
                                        <div class="itstime">
                                            <span>07 DEC</span>
                                            <span>11.30 pm</span>
                                        </div>
                                  	</div>
                                </li>
				</c:forEach>
           </ul>
	</c:if>
 --%>
<!-- Inline Test end -->

