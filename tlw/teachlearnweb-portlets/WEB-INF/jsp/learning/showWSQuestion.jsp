<%@page import="com.teachlearnweb.dao.entity.PLPAnswer"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.teachlearnweb.dao.entity.DTAnswer"%>
<%@page import="java.util.List"%>
<%@page import="com.teachlearnweb.dao.entity.DTQuestion"%>
<%@page import="java.util.Map"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<portlet:defineObjects/>
<liferay-theme:defineObjects/>


<c:set var="options" value="${options}"/>

<!-- popup Box-->
<div id="starttestnow" class="lightbox">
	<div class="outerdiv setlightboxheight">
		<div class="lightboxwrapper">
			<div class="lightboxcontanerright">
				<div class="lbcheightfix">
					<div class="topbarbg">
						<div class="topbgleft">
							<p>${subjectName} | ${topicName} | ${subTopicName}</p>
							<p> WorkSheetId  ${workSheetId}</p>
						</div>
						<div class="topbgright">
							<p>${questionCount} Questions Total</p>
							<p>${answeredCount} Answered | ${unAnsweredCount} Unanswered</p>
						</div>
					</div>

					<div class="centerboxlb">
						<div class="questionsdiv" style="width: 79%;">
							<p>${currentQuestion.question}</p>
							<c:forEach items="${options}" var="option">
								<input type="radio" name="answer" class="answers" value="${option.answerId}" onchange="<portlet:namespace/>updateAnswer(this.value, ${currentQuestion.questionId});" 
									${option.answerId != userSelectedAnswerId?'':'checked="checked"'}>${option.answer} <br>
							</c:forEach>
						</div>
					</div>
					<table class="starttesttable">
						<tr>
							<td class="td1">
								<ul class="paginingdiv">
								<c:forEach items="${questions}" var="que" varStatus="itr">
									<li>
										<a href="JavaScript:void(0);" class="${(currentQuestionNum)==itr.index?'active':''} ${(que.answerId > 0)?'completed':''}"
												onclick="<portlet:namespace/>fetchQuestion(${itr.index}, ${workSheetId});">
											${itr.index<9?'0':''}${itr.index+1}
										</a>
									</li>
								</c:forEach>
								</ul>
							</td>
							<td class="td2">&nbsp;</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="lightboxright">
			<div class="lightboxrightside">
				<button class="finishtest" onclick="<portlet:namespace/>submitAssessment('${userId}', '${worksheetId}');">
					Finish Test
				</button>
				<button class="canceltest" onclick="<portlet:namespace/>cancelAssessment();">Cancel Test</button>
				<div class="timecoundown">
					<p class="remainingtime">Remaining Time:</p>
					<p class="mins" id="<portlet:namespace/>timeRemaining">
						${timeRemaining}
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
