<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<portlet:defineObjects/>
<liferay-theme:defineObjects/>
 <c:set var="assetPojoPro" value="${assetPojo}"/>
 <!-- Video -->
 <c:if test="${assetPojo.extension == 'swf'}" >
<!-- <object type='application/x-shockwave-flash' data='/documents/${assetPojoPro.groupId}/${assetPojoPro.folderId}/${assetPojoPro.title}' width='100%' height='100%'>
    <param name='flashvars'  />
    <param name='allowScriptAccess' value='always' />
    <param name='movie' value='/documents/${assetPojoPro.groupId}/${assetPojoPro.folderId}/${assetPojoPro.title}' />
    <param name='bgcolor' value='#009999'>
</object> -->

<object classid="clsid:d27cdb6e-ae6d-11cf-96b8-444553540000" 
	codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=6,0,40,0" 
    width="100%" height="100%" id="mymoviename"> 
	<param name='movie' value='/documents/${assetPojoPro.groupId}/${assetPojoPro.folderId}/${assetPojoPro.title}' /> 
	<param name="quality" value="high" /> 
    <param name="bgcolor" value="#009999" /> 
	<param name="menu" value="true" />
	<param name="play" value="true" />
	
    <embed src="/documents/${assetPojoPro.groupId}/${assetPojoPro.folderId}/${assetPojoPro.title}" menu="true" play="true"
         bgcolor="#009999" width="100%" height="100%" 
		name="mymoviename" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer"> 
     </embed> 
  </object> 
 </c:if> 
 
  <!--Presentation -->
 <c:if test="${assetPojo.extension == 'ppt' or assetPojo.extension == 'pptx'}" >
	<a href="/documents/${assetPojoPro.groupId}/${assetPojoPro.folderId}/${assetPojoPro.title}" >${assetPojoPro.title}</a> 
 </c:if>
 
 <!-- Points - PDF -->
 <c:if test="${assetPojo.extension == 'pdf'}" >
   <iframe src="/documents/${assetPojoPro.groupId}/${assetPojoPro.folderId}/${assetPojoPro.title}#toolbar=0" width="100%" height="100%">       
 </iframe>
 </c:if>
 
 <c:if test="${assetPojo.extension == 'jpg' or assetPojo.extension == 'png' or assetPojo.extension == 'jpeg' or assetPojo.extension == 'gif' or assetPojo.extension == 'bmp'}" >
   <img src="/documents/${assetPojoPro.groupId}/${assetPojoPro.folderId}/${assetPojoPro.title}" width="100%" height="100%">       
 </c:if>
