<%@page
	import="com.tlw.util.WebUtil,java.util.List,com.liferay.portlet.asset.model.AssetCategory"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page import="com.liferay.portlet.asset.model.AssetEntry"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<script>
function changeURL(url) {
	location.href=url;
}
</script>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<portlet:defineObjects />
<portlet:actionURL var="subscribeCourse">
	<portlet:param name="action" value="makePayment" />
	<portlet:param name="categoryId" value="CateGoRyId" />
</portlet:actionURL>

<div class="">
	<c:if test="${fn:length(rootCategoryList) == 0}">
		<ul class="canvasleft">
			<a href="#">
				<li class="greybg">
					<table width="100%">
						<tbody>
							<tr>
								<td class="leftbox1">
									<p class="moresub">
										Please purchase click <a href="/web/guest/classes">here</a>to purchase Courses 
									</p></td>
							</tr>


						</tbody>
					</table></li> </a>
		</ul>
	</c:if>
	<ul class="canvasleft">
		<c:forEach var="subject" items="${rootCategoryList}">
			<portlet:actionURL var="subjectActionURL">
				<portlet:param name="action" value="startCourse" />	
				<portlet:param name="className" value="${subject.className}" />
				<portlet:param name="syllabusName" value="${subject.syllabusName}" />	
				<portlet:param name="subjectId" value="${subject.categoryId}" /></portlet:actionURL>
			
				<li class="bluebgdash">
				<!-- Use this below fix for IE -->
					<a href="javascript:void(0);" onClick="setTimeout(changeURL('${fn:trim(subjectActionURL)}'),10)"> 
					<table width="100%">
						<tbody>
							<tr>
								<td class="leftbox1">
									<div class="leftdata">
										<p class="mathsriewes">
											<img
												src="<%=themeDisplay.getPathThemeImages()%>/mathstopic_review.png"
												class="canvaslefticons">${subject.numberOfChildCategories}
											Topics 
										</p>
										<p class="mathsriewes">
											<img
												src="<%=themeDisplay.getPathThemeImages()%>/mathsworksheet_icons.png"
												class="canvaslefticons"> 16 Worksheets
										</p>
										<p class="mathsriewes">
											<img
												src="<%=themeDisplay.getPathThemeImages()%>/mathstesticons.png"
												class="canvaslefticons"> ${subject.numberOfChildCategories} Tests
										</p>
									</div>
								</td>
								<td align="center" class="rightbox1"><img
									style="width: 40px;"
									src="<%=themeDisplay.getPathThemeImages()%>/${fn:toLowerCase(subject.subjectName)}.jpg">
									<p class="maths" style="text-transform:uppercase;">${subject.subjectName}</p>
									<p class="mtsubtitle">${subject.className} : ${subject.syllabusName}</p>
								</td>
							</tr>
						</tbody>
					</table>
		 </a>
			</li>
			
		</c:forEach>
		<!-- Diagnostic tests  Assesments Govind-->
		<a href="javascript:void(0);" onClick="setTimeout(changeURL('./assessment'),10)" href= id="<portlet:namespace/>DTStatus">
			<li class="greybg">
				<table width="100%">
					<tbody>
						<tr>
							<td class="leftbox1">
								<div class="diagnosticleftdata">
									<p>
										<img
											src="<%=themeDisplay.getPathThemeImages()%>/diagnostictest_icon.png">
									</p>
								</div>
							</td>
							<td align="center" style="vertical-align: middle;"><c:if
									test="${fn:length(rootCategoryList) == 0}">

									<p class="diagtest">
										Please purchase Courses <br>
									</p>

								</c:if> <c:if test="${fn:length(rootCategoryList) > 0}">
									<c:if test="${empty status}">
										<p class="diagtest">No tests found</p>
									</c:if>
									<c:if test="${not empty status}">
										<p class="diagnostic">${percent}%</p>
										<p class="diagnasticksubtitle">${status}</p>
										<p class="diagtest">Diagnostic Test</p>
										<!-- <p class="testdate">24th Dec 2012</p> -->
									</c:if>

								</c:if></td>
						</tr>
					</tbody>
				</table>
		</li> </a>
		<!-- Diagnostic tests -->
	</ul>
</div>
