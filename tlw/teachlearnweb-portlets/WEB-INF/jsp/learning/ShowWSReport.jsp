<%@page import="com.tlw.reports.mvc.vo.WorkSheetReport"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="java.util.List"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ page contentType="text/html" isELIgnored="false"%>

<portlet:defineObjects/>
<liferay-theme:defineObjects/>
<!-- Javascript goes in the document HEAD -->
<script type="text/javascript">
function altRows(id){
	if(document.getElementsByTagName){  
		
		var table = document.getElementById(id);  
		var rows = table.getElementsByTagName("tr"); 
		 
		for(i = 0; i < rows.length; i++){          
			if(i % 2 == 0){
				rows[i].className = "evenrowcolor";
			}else{
				rows[i].className = "oddrowcolor";
			}      
		}
	}
}

window.onload=function(){
	altRows('alternatecolor');
}
</script>


<!-- CSS goes in the document HEAD or added to your external stylesheet -->
<style type="text/css">
table.altrowstable {
	font-family: verdana,arial,sans-serif;
	font-size:11px;
	color:#333333;
	border-width: 1px;
	border-color: #a9c6c9;
	border-collapse: collapse;
}
table.altrowstable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
}
table.altrowstable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #a9c6c9;
}
.oddrowcolor{
	background-color:#d4e3e5;
}
.evenrowcolor{
	background-color:#c3dde0;
}
</style>






<%
	List<WorkSheetReport> wsReportList = Validator.isNotNull(request.getAttribute("wsReportsList")) ? (List<WorkSheetReport>)request.getAttribute("wsReportsList") : null;
%>

<!-- popup Box for report -->
<div id="performance" class="lightbox">
	<div class="outerdiv setlightboxheight">
		<div class="lightboxwrapper">
			<div class="lightboxcontanerright">
				<div class="lbcheightfix">
					<div class="topbarbg">
						<div class="topbgleft">
							<h3>WorkSheet Report</h3>
						</div>
						<div class="topbgright">
							<!-- <h5>11 May 2012</h5> -->
						</div>
					</div>
					<div class="centerboxlb">
						<div id="fullgraph" style="width:80%; height:80%;margin-left:30px;"></div>
						
						<table class="altrowstable" id="alternatecolor" style="margin-left:20px;">

							<%
							if(wsReportList != null && !wsReportList.isEmpty() && wsReportList.size() > 0) {
							%>
								<thead>
								<tr>
								<th colspan="2">Question</th>
								<th colspan="2">CorrectAnswer</th>
								<th colspan="2">userAnswer</th>
								<th colspan="2">Status</th>
								</tr>
								</thead>
								<tbody>
							<%} %>
							
							<%
								if(wsReportList != null && !wsReportList.isEmpty() && wsReportList.size() > 0) {
									String Status = null;
									for(WorkSheetReport wsr : wsReportList) {
										if(wsr != null && wsr.getAnswer().equals(wsr.getUserAnswer())) {
											Status = "Correct";
										} else {
											Status = "Not Correct";
										}
							%>
								
								<tr> 
								<td colspan="2"><%=wsr.getQuestion() %></td>
								<td colspan="2"><%=wsr.getAnswer() %></td>
								<td colspan="2"><%=wsr.getUserAnswer() %></td>
								<td colspan="2"><%=Status %></td>
								</tr>
							
							<% } }  %>
							</tbody>
							</table>
						
					</div>
				</div>
			</div>
		</div>
		<div class="lightboxright">
			<div class="lightboxrightside">
				<%-- <a href="#" class="fullreportprint"><img
					src="<%=themeDisplay.getPathThemeImages()%>/printicon.png">Print Report</a> --%>
				<a href="JavaScript:void(0);"
					class="fullreportclose" onclick="<portlet:namespace/>closeReport();">Close</a>
				<div class="rewardspointspur">
					<p>
						You can easily look at<br> your weak skills and <br>
						improve according <br> to this
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
