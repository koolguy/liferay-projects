<%@page import="com.tlw.assessment.vo.AssessmentConstants"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.teachlearnweb.dao.entity.PLPStudentWrokSheet"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page import="com.liferay.portlet.asset.model.AssetEntry"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<portlet:resourceURL var="subjectTestsURL" id="fetchSubjectTests" />
<portlet:resourceURL var="viewWorkSheetsURL" id="viewWorkSheets" />
<portlet:resourceURL var="eocTestsURL" id="viewEOCTests" />
<portlet:actionURL var="assetURL">
	<portlet:param name="action" value="actionShowSeletedAssets" />
	<portlet:param name="key" value="aSsEtMiMe" />
</portlet:actionURL> 
<%-- <portlet:actionURL var="workSheetActionURL">
	<portlet:param name="myaction" value="showWorkSheets" />
</portlet:actionURL> --%>

<script type="text/javascript">

function submitURL(actionName,key){
	var url = '<%=assetURL%>';
	url = url.replace('aSsEtMiMe',key);
	document.forms[0].action=url;
	document.forms[0].submit();
}

  var preSelSectionId = "";
function fetchAssestListContent(assetKey,assetKeyWithOutSpace){

	$('#'+assetKeyWithOutSpace).addClass('active');	
	$('#'+preSelSectionId).removeClass('active');
	preSelSectionId = assetKeyWithOutSpace;	
	var assetsUrl = '<portlet:resourceURL
		id="actionShowSeletedAssets"></portlet:resourceURL>'+
		'?action=actionShowSeletedAssets&key='+ assetKey;

	$.ajax({
	      type: "GET",
	      url: assetsUrl,
	      data: {},
	      success: function(data) {
	    		  $('#showAssetsContent').html(data);
	      }
   	});	
}

function showContent(groupId,folderId,title,assetType,assetExtension){
	
	var url = '<portlet:resourceURL
		id="showContent"></portlet:resourceURL>'+
		'?action=showContent&groupId='+ groupId+'&folderId='+ folderId+'&title='+title+'&assetType='+assetType+'&assetExtension='+assetExtension;
        var xhr
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                 xhr=new XMLHttpRequest();
		   }else {					// code for IE6, IE5
		          xhr=new ActiveXObject("Microsoft.XMLHTTP");
		 }		                                
		  xhr.open("GET", url, true);
		  xhr.onreadystatechange = function() {
		  if (xhr.readyState == 4 && xhr.status==200) {
              	document.getElementById("showAssetsContent").innerHTML = xhr.responseText;		                                	
             }
        	}
    xhr.send();
}
// Govinda
$(document).ready(function() {
	$('#<portlet:namespace/>subjectTestsLabel').on('click', function() {
		$(this).addClass('active');
		$('#'+preSelSectionId).removeClass('active');
		preSelSectionId = $(this).attr('id');

		$.ajax({
		      type: "GET",
		      url: "${subjectTestsURL}",
		      data: {},
		      success: function(data) {
		    	  if (data != '-1') {
		    		  $('#showAssetsContent').html(data);
		    	  }
		      }
	   	});
	});
	
	 <c:if test="${wsFlag eq true}">
	$('#<portlet:namespace/>viewWorkSheets').on('click', function() {
		$(this).addClass('active');
		$('#'+preSelSectionId).removeClass('active');
		preSelSectionId = $(this).attr('id');

		$.ajax({
		      type: "GET",
		      url: "${viewWorkSheetsURL}",
		      data: {<portlet:namespace/>subTopicId: ${subTopicId}},
		      success: function(data) {
		    	  if (data != '-1') {
		    		  $('#showAssetsContent').html(data);
		    	  }
		      }
	   	});
	});
	</c:if>

	
	
});

$(document).ready(function() {
	$('.contentbg').css('overflow', 'auto');
	//Niranjan - added for default pages...
	var topicName = "${topics_level2Child}";
	if(topicName.toLowerCase() == 'overview'){
		if($('#MindMaps').length != 0) {
				fetchAssestListContent('Mind Maps','MindMaps')
				$('#MindMaps').addClass('active');
		}
		
	} else{
		
		if($('#Notes').length != 0) {
			fetchAssestListContent('Notes','Notes');
			$('#Notes').addClass('active');
		}
	}
	
	// Mallikarjun added for EOC
	// schedule a click event on left side link
	$('#<portlet:namespace />eocTestsLabel').live('click', function() {
		var topicName = '${topics_level1Child}';
		preSelSectionId = '<portlet:namespace/>eocTestsLabel';
		// load EOC tests
		viewEOCTests(topicName);
	});
	
	
});


function viewEOCTests(topicName) {
		
	$.ajax({
	      type: "GET",
	      url: "${eocTestsURL}",
	      data: {<portlet:namespace/>topicName: topicName},
	      success: function(data) {
	    	  if (data != '-1') {
	    		  $('#showAssetsContent').html(data);
	    	  }
	      }
   	});
}
</script>
<%
//retrieving worksheets from session from eventmapping
List<PLPStudentWrokSheet> wsList =
		Validator.isNotNull(renderRequest.getPortletSession().getAttribute("wsList")) ?  (List<PLPStudentWrokSheet>) renderRequest.getPortletSession().getAttribute("wsList") :null;

PortletURL workSheetActionURL = renderResponse.createActionURL();
workSheetActionURL.setParameter("myaction", "showWorkSheets");

%>
<%--style="width:101%" removed --%>
<div id="sitewidth">

	<div id="offcanvasmenu" style="width: 1782px; left: 0px;">
		<!--offcanvas menu topbar-->
		<%@ include file="../common/dashboard.jsp"%>
	</div>

	<div class="scrollsection">
		<section id="maincontent">
		<div class="wraper topbg outerdiv">
			<!-- Right Section -->


			<!-- Left Section -->
			<div class="leftwrap" >
				<div class="offcantab" style="border-right:3px solid yellow;">
					<a href="#"  click="refreshOffCanTab();" style="color:white;font-size:16px;">&lt;&lt; Back</a>

				</div>
				<div class="mainnavsection">
					<ul>
						<c:forEach items="${assetEntryMap}" var="entry">
							<c:if test="${fn:toLowerCase(entry.key) != 'subject test'}">
								<li><a id="${fn:replace(entry.key, ' ', '')}"
									class="${(fn:toLowerCase(defaultSelSection)) == (fn:toLowerCase(entry.key)) ? 'active':''}"
									href="#"
									onclick="javascript:fetchAssestListContent('${entry.key}','${fn:replace(entry.key, ' ', '')}')">
										<c:set var="cssName1"
											value="${fn:replace(entry.key, ' ', '')}" /> <c:set
											var="cssName2" value="${fn:toLowerCase(cssName1)}" /> <span
										class="mthumb ${cssName2}"></span> <span class="mthumbname">${entry.key}</span>
								</a></li>
							</c:if>
						</c:forEach>
						<c:if test="${fn:toLowerCase(topics_level1Child) == 'overview'}">
							<li><a href="#" id="<portlet:namespace/>subjectTestsLabel">
									<span class="mthumb subjecttest"></span> <span
									class="mthumbname">Subject Test</span> </a></li>
						</c:if>
						<c:if test="${fn:toLowerCase(topics_level2Child) == 'overview'}">
							<li><a href="#" id="<portlet:namespace/>eocTestsLabel">
									<span class="mthumb subjecttest"></span> <span
									class="mthumbname">Topic Level Test</span> </a></li>
						</c:if>

						<c:if test="${wsFlag eq true}">
							<li><a href="#" id="<portlet:namespace/>viewWorkSheets">
									<span class="mthumb worksheet"></span> <span class="mthumbname">Worksheet</span>
							</a></li>
						</c:if>

					</ul>
				</div>
			</div>

			<!-- Content Section -->
			<div class="contentwrap">
				<div class="infowrap">
					<div class="Maintitle">${subject_syllabus} :
						${subject_className} : ${topics_assetCatName}</div>
					<!-- Top -->
					<div class="outerdiv pagetitleandsocial">
						<div class="pagetitle">
							${topics_level1Child}
							<c:if test="${topics_level2Child != ''}"> : </c:if>
							${topics_level2Child}
						</div>
						<div>
							<ul class="outerdiv">

								<li><a href="/group/guest/dashboard"><img
										src="<%=themeDisplay.getPathThemeImages()%>/logo.png"
										alt="Logo" class="canvaslogo" style="float: right">
								</a></li>

							</ul>
						</div>
					</div>
					<!-- Center content start here -->
					<div class="contentbg subjectstatusbg scroll"
						id="showAssetsContent" style="width: 100%">
						<c:if test="${fn:toLowerCase(topics_level1Child) == 'overview'}">
							<ul>
								<li class="subjectstatus">
									<ul class="statuslist">
										<li><a href="#">Sleeping</a>
										</li>
										<li><a href="#" class="active">Started</a>
										</li>
										<li><a href="#">Catching Up</a>
										</li>
										<li><a href="#">Mastered</a>
										</li>
									</ul>
									<div class="subjecttopics">
										<h2>${topics_assetCatName}</h2>
										<ul class="topicsreviews">
											<li><img
												src="<%=themeDisplay.getPathThemeImages()%>/topicswhite.png">${strNumberOfTopics}
												Topics</li>
											<li><img
												src="<%=themeDisplay.getPathThemeImages()%>/worksheetwhite.png">${strNumberOfAllSubTopics}
												Worksheets</li>
											<!--  <li><img src="images/mathstesticons.png">10 Tests Completed</li> -->
										</ul>
									</div></li>
								<!-- <li class="subjectstatus">
                            	<ul class="statuslist">
                                	<li><a href="#">Sleeping</a></li>
                                    <li><a href="#" class="active">Started</a></li>
                                    <li><a href="#">Catching Up</a></li>
                                    <li><a href="#">Mastered</a></li>
                                </ul>
                                <div class="subjecttopics">
                                	<h2>Numbers</h2>
                                	<ul class="topicsreviews">
                                    	<li><img src="images/topicswhite.png">02 Topics Reviewed</li>
                                        <li><img src="images/worksheetwhite.png">03 Worksheets Finished</li>
                                        <li><img src="images/mathstesticons.png">06 Tests Completed</li>
                                    </ul>
                            </div>
                            </li>
                            <li class="subjectstatus">
                            <div>
                            	<ul class="statuslist">
                                	<li><a href="#" class="active">Sleeping</a></li>
                                    <li><a href="#">Started</a></li>
                                    <li><a href="#">Catching Up</a></li>
                                    <li><a href="#">Mastered</a></li>
                                </ul>
                                <div class="subjecttopics">
                                	<h2>Fractions</h2>
                                	<ul class="topicsreviews">
                                    	<li><img src="images/topicswhite.png">01 Topics Reviewed</li>
                                        <li><img src="images/worksheetwhite.png">01 Worksheets Finished</li>
                                        <li><img src="images/mathstesticons.png">01 Tests Completed</li>
                                    </ul>
                                </div>
                            </div> 
                            </li>
 -->
							</ul>
						</c:if>
					</div>
					<!-- Overveiw display End -->

				</div>
			</div>
		</div>
		</section>
		<!-- Footer Section -->
	</div>
</div>
