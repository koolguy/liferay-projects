<%@page import="com.tlw.assessment.vo.AssessmentConstants"%>
<%@page import="java.util.ArrayList"%>
<%@page import="javax.portlet.PortletSession"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="com.teachlearnweb.dao.entity.PLPStudentWrokSheet"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>

<portlet:defineObjects/>
<liferay-theme:defineObjects/>

<portlet:resourceURL var="saveWSAssessmentURL" id="submitWSAssessment" />
<portlet:resourceURL var="fetchWSQuestionURL" id="fetchWSQuestion" />
<portlet:resourceURL var="updateWSAnswerURL" id="updateWSAnswer" />
<portlet:resourceURL var="testWorkSheetURL" id="startWSAssessment" />
<portlet:resourceURL var="wstimeURL" id="wstimeRemaining"/>
<portlet:resourceURL var="reportURL" id="showWSReport"/>

<script>
function displayHTML(data) {
   $("#<portlet:namespace/>popupRoot").html(data);
   $(".lightboxopen").trigger('ShowAssessment');
}
function displayReport(data) {
	   $("#<portlet:namespace/>reportRoot").html(data);
	   $(".reportlightboxopen").trigger('ShowReport');
}

function <portlet:namespace/>showWSReport(worksheetId) {
$.ajax({
   type: "POST",
   url: "${reportURL}",
   data: {<portlet:namespace/>worksheetId: worksheetId },
   success: function(data) {
 	  		displayReport(data);
			}
});
}

function startWSTestAssessment(workSheetId) {
	   $.ajax({
	      type: "POST",
	      url: "${testWorkSheetURL}",
	      data: {<portlet:namespace/>workSheetId: workSheetId},
	      success: function(data) {
	    	  displayHTML(data);
	    	  intervalId = self.setInterval(<portlet:namespace />getTimeRemaining, 1000);
	    	  $('a.lightbox_close').click(function(){
	    			clearTimer();
	    		});
	      	}
	   });
}
function <portlet:namespace/>clearTimer() {
	if (intervalId != null) {
		window.clearInterval(intervalId);
	}
}
function <portlet:namespace/>getTimeRemaining() {
	$.ajax({
	      type: "GET",
	      url: "${wstimeURL}",
	      data: {},
	      success: function(data) {
	    	  if(data != '-1') {
	    		  $("#<portlet:namespace/>timeRemaining").html(data);
	    	  } else {
	    		  <portlet:namespace/>clearTimer();
	    		  alert('Time up');
	    		  <portlet:namespace/>cancelAssessment();
	    	  }
		  }
	});
}
/* submit assessment */
function <portlet:namespace/>submitAssessment(userId, worksheetId) {
	var divId = userId + "_" + worksheetId;
	var reportHTML = "<a rel=\"performance\" class=\"reportlightboxopen\" onclick=\"<portlet:namespace/>showWSReport('" + worksheetId + "');\">" +
    			"<div class='worksheetbottom outerdiv'><div class='wsschedul'>Completed</div></div></a>";
	$.ajax({
	      type: "POST",
	      url: "${saveWSAssessmentURL}",
	      data: {<portlet:namespace/>worksheetId: worksheetId},
	      success: function(data) {
	    	  if(data != '-1') {
	    		  <portlet:namespace/>clearTimer();
	    		 // displayHTML(data);
	    		  alert('Saved successfully');
	    		  $('#' + divId).html(reportHTML);
	    		  <portlet:namespace/>cancelAssessment();
	    	  }
		  }
	});
}

function <portlet:namespace/>cancelAssessment() {
	$('a.lightbox_close').trigger('click');
}

function <portlet:namespace/>fetchQuestion(qId, workSheetId) {
	$.ajax({
	      type: "POST",
	      url: "${fetchWSQuestionURL}",
	      data: {<portlet:namespace/>questionNum: qId,
	    	 <portlet:namespace/>workSheetId: workSheetId},
	      success: function(data) {
	    	  displayHTML(data);
	      	}
   });
}

function <portlet:namespace/>updateAnswer(selectedAnswerId, questionId) {
	$.ajax({
	      type: "POST",
	      url: "${updateWSAnswerURL}",
	      data: {<portlet:namespace/>answerId: selectedAnswerId,
	    	  <portlet:namespace/>questionId: questionId},
	      success: function(data) {
				if (data == "-1") {
					alert("Error saving the answer");
				}
	      	}
   });
}
var intervalId = null;
</script>

<%

 List<PLPStudentWrokSheet> wsList =
				Validator.isNotNull(request.getAttribute("wsList")) ?  (List<PLPStudentWrokSheet>)request.getAttribute("wsList") :null;
 
 
 /* List<PLPStudentWrokSheet> wsList =
	Validator.isNotNull( renderRequest.getPortletSession().getAttribute("wsList")) ?  (List<PLPStudentWrokSheet>) renderRequest.getPortletSession().getAttribute("wsList") :null; */
%>
<% if(wsList != null && !wsList.isEmpty() && wsList.size() > 0) { 
%>
<ul>
<%
	System.out.println("wsList.size() in JS Pview ws----"+wsList.size());
	int index = 1;
	for(PLPStudentWrokSheet worksheet : wsList) {
		if(worksheet != null) {
	int questionIdsListsize = 	getQuestionListSizeForWorkSheet(worksheet);
	List<String> questionList = getQuestionsListForWorkSheet(worksheet);
	String questionIdsList = worksheet.getQuestionIdList();
%>
  	<li class="worksheetstausbox">
          <div class="worksheetboxtop">
               <a href="#">
               <%
               String EOCString = Validator.isNotNull(resourceRequest.getAttribute("EOCTest")) ? (String)resourceRequest.getAttribute("EOCTest") : null;
               if(Validator.isNotNull(EOCString)) {
            	   System.out.println("EOCString from viewworksheets jsp----------------"+EOCString);
            	%>
               <p class="worksheetnumber">EOCTest - <%=index %></p>
               <%} else { %>
                 <p class="worksheetnumber">Worksheet - <%=index %></p>
               <% } %>
               <div class="outerdiv">
                  <div class="worksheetthumb"><img src="<%=themeDisplay.getPathThemeImages()%>/worksheeticon.png"></div>	
                  <div class="worksheetreport">
                      <p class="wsnofq"><%=questionIdsListsize %> Questions</p>
                      <% if(questionList != null && !questionList.isEmpty() && questionList.size() > 0) { %>
                      <p class="wsnoft"><%=(questionList.size() * AssessmentConstants.MINUTE_PER_QUESTION) / 60 %> Minutes</p>
                      <% } else { %>
                      <p class="wsnoft">15 Minutes</p>
                      <%} %>
                  </div>
               </div>
                  </a>
             </div>
           
           <%
           	if(AssessmentConstants.TestStatus.ASSIGNED.equalsIgnoreCase(worksheet.getStatus())) {
           %>  
           <div id="<%=worksheet.getUserId()%>_<%=worksheet.getWorksheetId()%>">
          <a rel="starttestnow" class="lightboxopen" onclick="startWSTestAssessment(<%=worksheet.getWorksheetId() %>);">
          <div class="worksheetbottom outerdiv">
              <div class="wsschedul">
              	Not Started
              </div>
          </div>
          </a>
          </div>
          
          <% } else if(AssessmentConstants.TestStatus.COMPLETED.equalsIgnoreCase(worksheet.getStatus())) { %>
          
          <div id="<%=worksheet.getUserId()%>_<%=worksheet.getWorksheetId()%>">
          <a rel="performance" class="reportlightboxopen" onclick="<portlet:namespace/>showWSReport(<%=worksheet.getWorksheetId() %>);">
          <div class="worksheetbottom outerdiv">
              <div class="wsschedul">
              	Completed
              </div>
          </div>
          </a>
         </div>
          <%} %>
      </li>
<%
index++;
	} } // end of for and if %>
</ul>
<%} else {%>
<c:if test="${not empty IsEOCTest}">
	<span>No EOC Tests available.</span>
</c:if>
<c:if test="${empty IsEOCTest}">
	<span>No WorkSheets Available.</span>
</c:if>
<% }  %>

<%!
	public int getQuestionListSizeForWorkSheet(PLPStudentWrokSheet worksheet){
		int size = 0;
		List tempList =null;
		String[] tempArray = null;
		String questionIdsList = worksheet.getQuestionIdList();
		if(questionIdsList != null && questionIdsList.contains(",")){
			tempArray = questionIdsList.split(",");
		}
		if(tempArray != null && tempArray.length > 0){
			tempList = new ArrayList();
			for(int i=0; i<tempArray.length;i++){
				tempList.add(tempArray[i]);
			}
		}
		if(tempList != null && !tempList.isEmpty()){
			return tempList.size();
		}else{
			tempList = new ArrayList();
			return tempList.size();
		}
	}
%>

<%!
	public List<String> getQuestionsListForWorkSheet(PLPStudentWrokSheet worksheet){
		int size = 0;
		List<String> tempList =null;
		String[] tempArray = null;
		String questionIdsList = worksheet.getQuestionIdList();
		if(questionIdsList != null && questionIdsList.contains(",")){
			tempArray = questionIdsList.split(",");
		}
		if(tempArray != null && tempArray.length > 0){
			tempList = new ArrayList();
			for(int i=0; i<tempArray.length;i++){
				tempList.add(tempArray[i]);
			}
		}
		if(tempList != null && !tempList.isEmpty()){
			return tempList;
		}else{
			tempList = new ArrayList<String>();
			return tempList;
		}
	}
%>


<!-- include the lightbox content inside below div for the selected assessment -->
<div id="<portlet:namespace/>popupRoot"></div>

<!-- include the lightbox content inside below div for the selected assessment -->
<div id="<portlet:namespace/>reportRoot"></div>
