<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ page import="com.teachlearnweb.dao.entity.CouponCode" %>
  
 <script>
  $(function() {
    $( "#from" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 3,
      onClose: function( selectedDate ) {
        $( "#to" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#to" ).datepicker({
      defaultDate: "+1w",
      changeMonth: true,
      numberOfMonths: 3,
      onClose: function( selectedDate ) {
        $( "#from" ).datepicker( "option", "maxDate", selectedDate );
      }
    });
  });
  </script>

<portlet:defineObjects />
<liferay-theme:defineObjects />




<portlet:actionURL var="generateCouponcodeURL">
	<portlet:param name="action" value="generateCouponCode" />
</portlet:actionURL>

<form:form name="testmonialForm" method="post"
	action="${generateCouponcodeURL}">

	<div class="innerLeft">
		<!-- Testmonial Post -->
		<div class="posttestmonial">
			<div class="testmonialpost">
				<p class="testmonialdivcomnt">Add Coupon Code</p>
			</div>

			<div class="testmonialpostsearchlistbox" style="display: none;height: 98px;">
				
				<p>
					<input name="amount" type="text" style="width:200px;height: 2px;"
						placeholder="Type amount...">
				</p>
				<p>
					<input name="tesmonialDesc" type="text"
						placeholder="Write Desciption...">
				</p>
				
				<p>
					<input type="submit" class="sendbtn" value="Add"
						style="background: #DE696F; padding: 15px; color: white; width: 90px; text-align: center; border-radius: 6px; -moz-border-radius: 6px; -webkit-border-radius: 6px; border: 1px solid #B86165; font-size: 18px; text-transform: uppercase; cursor: pointer;" />
				</p>
				<div class="addfirendssearchresult "></div>
			</div>

		</div>
		<div class="postedtestmonials">
			<ul class="socialactivelist">
			
				<c:forEach var="coupon" items="${couponCodeList}">
				
					<li>
					<table class="postdtesttable">
						<tbody>
							<tr>
								<td class="td1"><img
									src="<%=themeDisplay.getPathThemeImages()%>/coupon_icon.png"
									height="85" width="145" style="padding-top: 12px;">
								</td>
								<td class="td2">
									<p class="activeby">
										${coupon.couponCode}<br>
									</p>
									<p class="activedec">Amount : ${coupon.amount}</p>
									<p class="activetime">${coupon.description}</p></td>
							</tr>
						</tbody>
					</table></li>
				
				</c:forEach>			
				
			</ul>
		</div>
	</div>
</form:form>
</body>
</html>