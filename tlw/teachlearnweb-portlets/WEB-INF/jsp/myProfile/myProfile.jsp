<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>


<%@page import="com.tlw.util.WebUtil"%>
<%@page import="com.liferay.portal.kernel.upload.UploadException"%>
<%@page import="com.liferay.portal.UserPortraitSizeException"%>
<%@page import="com.liferay.portal.kernel.util.PrefsPropsUtil"%>
<%@page import="com.liferay.portal.kernel.util.PropsKeys"%>
<%@page import=" com.liferay.portal.UserPortraitTypeException"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@ page import="com.teachlearnweb.service.vo.User" %>
<%@ page import="com.liferay.portal.model.UserConstants" %>
<%@ page import="com.liferay.portlet.PortletURLUtil" %>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState" %>


<portlet:defineObjects />
<liferay-theme:defineObjects />

<%
	User currentUser = WebUtil.getUser(renderRequest);
	String currentURL = PortletURLUtil.getCurrent(renderRequest, renderResponse).toString();             
	int wscount = Validator.isNotNull(renderRequest.getAttribute("wsCompletedStatusCount")) ? Integer.parseInt(renderRequest.getAttribute("wsCompletedStatusCount").toString()) : 0;
	int testsFinishedCount = Validator.isNotNull(renderRequest.getAttribute("testsFinishedCount")) ? Integer.parseInt(renderRequest.getAttribute("testsFinishedCount").toString()) : 0;
	
	System.out.println("wscount from profile jsp-------------"+wscount);
%>

<portlet:actionURL var="editImageActionUrl">
	<portlet:param name="myaction" value="editImage" />
</portlet:actionURL>

<portlet:actionURL var="updateMyProfileActionUrl">
	<portlet:param name="editAction" value="updateMyProfile" />
</portlet:actionURL>

<portlet:actionURL var="cancelBasicInfoActionUrl">
	<portlet:param name="editAction" value="cancelBasicInfo" />
</portlet:actionURL>

<portlet:renderURL var="editUserPortraitURL" windowState="<%= LiferayWindowState.POP_UP.toString() %>">
	<portlet:param name="editAction" value="editImagePopUpURL" />
	<portlet:param name="redirect" value="<%= currentURL %>" />
	<portlet:param name="p_u_i_d" value="<%= String.valueOf(currentUser.getUserId()) %>" />
	<portlet:param name="portrait_id" value="<%= String.valueOf(currentUser.getPortraitId()) %>" />
</portlet:renderURL>

<portlet:actionURL var="changePasswordURL">
	<portlet:param name="editAction" value="changePassword" />
</portlet:actionURL>



<style type="text/css">

label {
font-size: 100%;
display: block;
margin-bottom: 3px;
clear: both;
}

.imageUploadDiv{
	float: left;
	padding: 25px 0 0 0;
}

.editProfileSubBtn {
	background: #0b785e;
	padding: 10px 0;
	border: 2px solid #dde4e9;
	border-radius: 4px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	color: #fff;
	font-size: 16px;
	cursor: pointer;
	text-align: center;
	display: block;
	width: 150px;
	margin: 20px auto;
}

.error_msg{
	color:red;
}

.photoedit img {
	float: left;
	margin: 25px 20px 0px 25px;
}

.editProfileSubBtn {
	background: #0b785e;
	padding: 10px 0;
	border: 2px solid #dde4e9;
	border-radius: 4px;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	color: #fff;
	font-size: 19px;
	cursor: pointer;
	text-align: center;
	display: block;
	width: 150px;
	margin: 20px auto;
}

.mandetory {
	color: #ff0000;
	margin: 0 0 0 10px;
	font-size: 14px;
}

.changePassword{
	clear: both;
	margin: 0px 50px 0 32px;
	float: left;
}
.changePassword .changePasswordInfo{
	color: #2c2c2c;
	font-size: 14px;
	font-weight: 600;
	border-bottom: 1px solid #acada6;
	width: 400px;
}

</style>


<script>
    $(function() {
         $('#dob').datepick({dateFormat: 'd/M/yy'});
        });
</script>

<script type="text/javascript">
	function show(boxid) {//alert("show----"+boxid);
		document.getElementById(boxid).style.display = "block";
	}

	function hide(boxid) {//alert("hide----"+boxid);
		document.getElementById(boxid).style.display = "none";
	}
</script>






	<div id="sitewidth">

		<div id="offcanvasmenu" style="width: 1782px; left: 0px;">
			<!--offcanvas menu topbar-->
			<%@ include file="../common/dashboard.jsp"%>
		</div>

		<div class="scrollsection">
			<section id="maincontent">
			<div class="wraper topbg outerdiv">
				<!-- Right Section -->
				<div class="rightwrap">
					<div class="rSubTitle profiletitle">
						<p class="rstSubject">Current Level</p>
					</div>
				</div>

				<!-- Left Section -->
				<div class="leftwrap">
					
					
					<div class="offcantab" style="border-right:3px solid yellow;">
					<a href="#" style="color:white;font-size:16px;">&lt;&lt; Back</a>

				</div>
				</div>

				<!-- Content Section -->
				<div class="contentwrap">
					<div class="infowrap">
						<div class="Maintitle">My Profile</div>
					</div>
				</div>
			</div>
			<div class="wraper outerdiv">
				<!-- Right Section -->
				<div class="rightwrap">
					<div class="rightmenusection profilerbg" style="height: 544px !important;">
						<div id="rightmainmenu">
							<c:forEach items="${rewardPoints}" var="reward">
           						<c:set var="count" value="${count + reward.rewardPoints}"/>
           					</c:forEach>
							<div class="currentlevel">
									<c:if test="${count ge 30 and count lt 500}">
									<p class="sliverbadge">
										<img src="<%=themeDisplay.getPathThemeImages()%>/medal.png" width="100" height="100">
									</p>
									</c:if>
									<c:if test="${count ge 500 and count lt 1000}">
									<p class="sliverbadge">
										<img src="<%=themeDisplay.getPathThemeImages()%>/medal.png" width="100" height="100">
									</p>
									</c:if>
									<c:if test="${count ge 1000 and count lt 2000}">
									<p class="sliverbadge">
										<img src="<%=themeDisplay.getPathThemeImages()%>/trophy.png" width="100" height="100">
									</p>
									</c:if>
									<c:if test="${count ge 2000}">
									<p class="sliverbadge">
										<img src="<%=themeDisplay.getPathThemeImages()%>/shield.png" width="100" height="100">
									</p>
									</c:if>
								<%-- <h1>Sliver Badge</h1> --%>
								<table class="slivertable">
									<!-- <tr>
										<td><div class="tablenumber">29</div>
										</td>
										<td class="td2">Topics Reviewed</td>
									</tr> -->
									<tr>
										<td><div class="tablenumber"><%=testsFinishedCount%></div>
										</td>
										<td class="td2">Tests Finished</td>
									</tr>
									<tr>
										<td><div class="tablenumber"><%=wscount%></div>
										</td>
										<td class="td2">Worksheets Finished</td>
									</tr>
									<tr>
										<td class="td1">${count}</td>
										<td class="td2">Rewards Points Earned</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- Content Section -->
				<div class="contentwrap">
					<div class="infowrap">
						<!-- Top -->
						<!--<div class="outerdiv pagetitleandsocial">
                          <div class="outerdiv">
                          
                          </div>
                        </div>-->
						<div class="contentbg2  scroll">
							<div class="profilebg">
								<div class="personprofile">
									
									<form:form name="viewImageForm" commandName="myProfileVO" method="post">
									<liferay-ui:success key="success-change-pwd-msg" message="Your password has been changed successfully." />	
									<liferay-ui:error key="error-changepassword-password-mismatch" message="New Password and Confirm Password does not match." />	
									
									<liferay-ui:success key="success-profile-msg" message="Your profile is successfully updated." />
									
										<div id="view_image" style="display: block;">
											<div class="photoedit">
												<div class="edit" id="edit_link">
													<a href="javascript:void(0);" onclick="hide('view_image'); show('edit_image');" id="tt_editImage" style="color: #696767;"> 
														Edit
													</a> 
												</div>
												<img class="profileviewer" src="/image/user_male_portrait?img_id='${myProfileVO.portraitId}'&amp;t='<%=Math.random() %>'"/>
												<div class="persondetails">
													<h1>
														<c:out value="${myProfileVO.firstName}" />&nbsp;
														<c:out value="${myProfileVO.lastName}" />
													</h1>
													<p>
														Student<br>
													</p>
													<div id="changePwdDiv">
													<a href="javascript:void(0);" onclick="hide('view_image'); show('change_password');"  style="color: #696767;"> 
														Change Password
													</a> 
												</div>
												</div>
												
											</div>
										</div>
									</form:form>
									
									
									
<form:form name="changePasswordForm" id="changePasswordForm" method="post"
	commandName="myProfileVO" action="${changePasswordURL}">
<div id="change_password" style="display: none;">
<div class="photoedit">
<div class="changePassword">
<h3 class="changePasswordInfo">Change Password</h3>
<table>
	<tr>
		<td align="right">
			<b><span class="mandetory">*</span> Old Password</b>
		</td>
		<td style="padding-left: 5px;">
			<form:password path="oldPassword" />
			<c:if test="${myProfileVO.errorOnChangePwd}">
				<div class="error_msg">
	                <form:errors path="oldPassword" />
	            </div>
            </c:if>
		</td>
	</tr>
	
	<tr>
		<td align="right">
			<b><span class="mandetory">*</span>	New Password</b>
		</td>
		<td style="padding-left: 5px;">
			<form:password path="newPassword" />
			<c:if test="${myProfileVO.errorOnChangePwd}">
				<div class="error_msg">
	                 <form:errors path="newPassword" />
	             </div>
             </c:if>
		</td>
	</tr>
	
	<tr>
		<td align="right">
			<b><span class="mandetory">*</span>	Confirm Password</b>
		</td>
		<td style="padding-left: 5px;">
			<form:password path="confirmPassword" />
			<c:if test="${myProfileVO.errorOnChangePwd}">
				<div class="error_msg">
	                <form:errors path="confirmPassword" />
	            </div>
            </c:if>
		</td>
	</tr>
	
</table>

<table align="right"> 
	<tr>
		<td>
			<a class="editProfileSubBtn" name="lnkSubmitForm" href="javascript:void(0);" onclick="javascript : document.getElementById('changePasswordForm').submit();"> 
				<span>Change</span> 
			</a>&nbsp;&nbsp;
		</td>
		<td style="padding-left: 5px;font-size: 20px;">
			<input tabindex="3" type="reset" value="Reset" />&nbsp;&nbsp;
		</td>
		<td style="padding-left: 5px;font-size: 20px;">										
			<a href="${cancelBasicInfoActionUrl}" name="cancel">Cancel</a>
		</td>
	</tr>
</table>
</div>	


	</div>
	</div>
</form:form>
									
									
								
<aui:form action="<%= editImageActionUrl.toString() %>" enctype="multipart/form-data" method="post" name="editImageForm">
				
	<liferay-ui:error exception="<%= UploadException.class %>" message="an-unexpected-error-occurred-while-uploading-your-file" />
	<liferay-ui:error exception="<%= UserPortraitSizeException.class %>">
	<%
		long imageMaxSize = PrefsPropsUtil.getLong(PropsKeys.USERS_IMAGE_MAX_SIZE) / 1024;
	%>
	<liferay-ui:message arguments="<%= imageMaxSize %>"
			key="please-enter-a-file-with-a-valid-file-size-no-larger-than-x" />
	</liferay-ui:error>
	<liferay-ui:error exception="<%= UserPortraitTypeException.class %>"
		message="please-enter-a-file-with-a-valid-file-type" />
		
		<liferay-ui:error key="image-upload-error-msg" message="We can't able to process your request. Please try again later." />
				
	<div id="edit_image" style="display: none;">
		<div class="photoedit">
			<div>
				<div>

				<img class="profileviewer" src="<%=themeDisplay.getPathThemeImages()%>/user_male_portrait?img_id='${myProfileVO.portraitId}'&amp;t='<%=Math.random() %>'"/>
				</div>
				<div class="imageUploadDiv">
				
					<aui:input styleClass="font-size:16px;"
						label='Upload a GIF or JPEG that is 120x100 pixels.'
						name="fileName" size="50" type="file" />

					<aui:button-row>
						<aui:button type="submit" value="Upload"/>
						<a href="${cancelBasicInfoActionUrl}" name="cancel">Cancel</a>
						</aui:button-row>
					
				</div>
				</div>
		</div>
	</div>
</aui:form>	
								

	<form:form name="viewMyProfileForm" commandName="myProfileVO" method="post">
			<div id="view_myProfile" style="display: block;">

				<div class="photoedit">
					<div class="edit" id="edit_link">
						<a href="javascript:void(0);" onclick="hide('view_myProfile'); show('edit_myProfile');" id="tt_editMyProfile" style="color: #696767;"> 
							Edit
						</a> 
					</div>

					<%--<img class="profileviewer"
						src="<%=currentUser.getPortraitURL(themeDisplay)%>" />--%>
						
					<div class="pesonalinfo">
						<h3 class="studentpersonalinfo">Personal Information</h3>
						<table class="personalinformation">
							<tr>
								<td><b>DOB:</b>
								</td>
								<td><c:out value="${myProfileVO.dob}" />
								</td>
							</tr>
							<tr>
								<td><b>Parent Name:</b>
								</td>
								<td><c:out value="${myProfileVO.parentName}" />
								</td>
							</tr>
							<tr>
								<td><b>Parent Email ID:</b>
								</td>
								<td><c:out value="${myProfileVO.emailAddress}" />
								</td>
							</tr>
							<tr>
								<td style="vertical-align: top;"><b>Address :</b>
								</td>
								<td><c:out value="${myProfileVO.address1}" />
								</td>
							</tr>
						</table>
					</div>
					<div class="schoolinfo">
						<h3 class="studentschoolinfo">School Information</h3>
						<table class="personalinformation">
							<tr>
								<td><b>School :</b>
								</td>
								<td><c:out value="${myProfileVO.schoolName}" />
								</td>
							</tr>
							<tr>
								<td><b>Phone : </b>
								</td>
								<td><c:out value="${myProfileVO.phone}" />
								</td>
							</tr>
							<tr>
								<td style="vertical-align: top;"><b>Address :</b>
								</td>
								<td><c:out value="${myProfileVO.schoolCity}" /></td>
							</tr>
							<tr>
								<td><b>Class/Grade :</b>
								</td>
								<td><c:out value="${myProfileVO.className}" />
								</td>
							</tr>
							<tr>
								<td><b>Section:</b>
								</td>
								<td><c:out value="${myProfileVO.sectionName}" /></td>
							</tr>
						</table>
					</div>
				</div>

			</div>
		</form:form>



		<form:form name="editMyProfileForm" id="editMyProfileForm" commandName="myProfileVO" method="post" action="<%=updateMyProfileActionUrl.toString() %>">
			<div id="edit_myProfile" style="display: none;">

				<div class="photoedit">
					<%--<img class="profileviewer"
						src="<%=currentUser.getPortraitURL(themeDisplay)%>" />--%>
					<div class="pesonalinfo">
						<h3 class="studentpersonalinfo">Personal Information</h3>
						<table class="personalinformation">
						<tr>
								<td><b>First Name:</b>
								</td>
								<td><form:input path="firstName" class="textfield"
										maxlength="40" style="width: 172px;" />
								<c:if test="${myProfileVO.errorOnMyProfile}">
                                          <div class="error_msg">
		                					<form:errors path="firstName" />
		            				</div>
		            			</c:if>		
								</td>
							</tr>
							
							<tr>
								<td><b>Last Name:</b>
								</td>
								<td><form:input path="lastName" class="textfield"
										maxlength="40" style="width: 172px;" />
								<c:if test="${myProfileVO.errorOnMyProfile}">
                                          <div class="error_msg">
		                					<form:errors path="lastName" />
		            				</div>
		            			</c:if>		
								</td>
							</tr>
							
							<tr>
								<td><b>DOB:</b>
								</td>
								<td><form:input path="dob" class="textfield"
										maxlength="40" style="width: 172px;" />
								<c:if test="${myProfileVO.errorOnMyProfile}">
                                          <div class="error_msg">
		                					<form:errors path="dob" />
		            				</div>
		            			</c:if>		
								</td>
							</tr>
							<tr>
								<td><b>Parent Name:</b>
								</td>
								<td><form:input path="parentName" class="textfield"
										maxlength="40" style="width: 172px;" />
										<c:if test="${myProfileVO.errorOnMyProfile}">
                                            <div class="error_msg">
				                					<form:errors path="parentName" />
				            				</div>
				            			</c:if>
								</td>
							</tr>
							<tr>
								<td><b>Parent Email ID:</b>
								</td>
								<td><form:input path="emailAddress"
										class="textfield" maxlength="40" style="width: 172px;" />
										<c:if test="${myProfileVO.errorOnMyProfile}">
                                            <div class="error_msg">
				                					<form:errors path="emailAddress" />
				            				</div>
				            			</c:if>
								</td>
							</tr>
							<tr>
								<td style="vertical-align: top;"><b>Address :</b>
								</td>
								<td><form:textarea path="address1" rows="5"
										cols="30" />
										
								<c:if test="${myProfileVO.errorOnMyProfile}">
                                          <div class="error_msg">
		                					<form:errors path="address1" />
		            				</div>
		            			</c:if>
          					</td>
							</tr>
						</table>
					</div>
					<div class="schoolinfo">
						<h3 class="studentschoolinfo">School Information</h3>
						<table class="personalinformation">
							<tr>
								<td><b>School :</b>
								</td>
								<td><form:input path="schoolName" class="textfield"
										maxlength="40" style="width: 172px;" />
								 <c:if test="${myProfileVO.errorOnMyProfile}">
                                          <div class="error_msg">
		                					<form:errors path="schoolName" />
		            				</div>
		            			</c:if>		
								</td>
							</tr>
							<tr>
								<td><b>Phone : </b>
								</td>
								<td><form:input path="phone" class="textfield" maxlength="40" style="width: 172px;"/>
								<c:if test="${myProfileVO.errorOnMyProfile}">
                                          <div class="error_msg">
		                					<form:errors path="phone" />
		            				</div>
		            			</c:if>
								</td>
							</tr>
							<tr>
								<td style="vertical-align: top;"><b>Address :</b>
								</td>
								<td><form:textarea path="schoolCity" rows="5"
										cols="30" />
										
								<c:if test="${myProfileVO.errorOnMyProfile}">
                                          <div class="error_msg">
		                					<form:errors path="schoolCity" />
		            				</div>
		            			</c:if></td>
							</tr>
							<tr>
								<td><b>Class/Grade :</b>
								</td>
								<td><form:input path="className" class="textfield"
										maxlength="40" style="width: 172px;" />
								<c:if test="${myProfileVO.errorOnMyProfile}">
                                          <div class="error_msg">
		                					<form:errors path="className" />
		            				</div>
	            				</c:if>		
								</td>
							</tr>
							<tr>
								<td><b>Section:</b>
								</td>
								<td><form:input path="sectionName" class="textfield"
										maxlength="40" style="width: 172px;" /> <c:if
										test="${myProfileVO.errorOnMyProfile}">
										<div class="error_msg">
											<form:errors path="sectionName" />
										</div>
									</c:if></td>
							</tr>
						</table>

					</div>
					<div align="center">
					<table>
						<tr>
							<td>
								<a class="editProfileSubBtn" name="lnkSubmitForm" href="javascript:void(0);" onclick="javascript : document.getElementById('editMyProfileForm').submit();"> 
										<span>Submit</span> 
									</a>&nbsp;&nbsp;
							</td>
							<td style="padding-left: 5px;font-size: 20px;">										
								<a href="${cancelBasicInfoActionUrl}" name="cancel">Cancel</a>
							</td>
						</tr>
					</table>
				
					</div>
				</div>
				
			</div>
		</form:form>

		<h3>Packages Status</h3>


		<table class="pakagestatusdiv">


			<c:forEach var="category" items="${packagesList}">
				<c:set var="categoryId" value="${category.categoryId}" />
				<tr>
					<td>
						<div class="packages">
							<div class="packagecost">
								<p class="packagenumber">
									<c:set var="className" value="${fn:split(category.name, ' ')}" />
									
									<c:out value="${className[1]}" />
								</p>


								<%
									long categoryId = (Long) pageContext.getAttribute("categoryId");
								%>
								<span><%=WebUtil.getAmount(categoryId)%></span>
							</div>
							<div class="packagecostdetails">
								<h4>
									<c:out value="${category.name}" />
								</h4>
								<h5>CBSE</h5>
								<p>
									Mathematics, Science, English<br> 
								</p>
							</div>
						</div></td>
					<td>
						</td>
				</tr>

			</c:forEach>
				</table>
	</div>
</div>
	<%-- <div class="packagesstatus">
		<h3>Monitored by:</h3>
		<ul class="monitoredpersons">
			<li>
				<div class="singleperson">
					<img src="<%=themeDisplay.getPathThemeImages()%>/christengeorge.jpg">
					<h4>Christen George</h4>
					<span>Parent</span>
				</div></li>
			<li class="monitorpersonsdetails">
				<div class="singlepersonrgt">
					<img src="<%=themeDisplay.getPathThemeImages()%>/nancygeorge.jpg">
					<h4>Nancy George</h4>
					<span>Parent</span>
				</div></li>
			<li>
				<div class="singleperson">
					<img src="<%=themeDisplay.getPathThemeImages()%>/karenshawn.jpg">
					<h4>Karen Shawn</h4>
					<span>Tutor</span>
				</div></li>
			<li class="monitorpersonsdetails">
				<div class="singlepersonrgt">
					<img src="<%=themeDisplay.getPathThemeImages()%>/johnsoncandy.jpg">
					<h4>John Candy</h4>
					<span>Tutor</span>
				</div></li>
			<li>
				<div class="singleperson">
					<img src="<%=themeDisplay.getPathThemeImages()%>/paulwesley.jpg">
					<h4>Paul Wesley</h4>
					<span>Tutor</span>
				</div></li>
			<li class="monitorpersonsdetails">
				<div class="singlepersonrgt">
					<img src="<%=themeDisplay.getPathThemeImages()%>/clairewright.jpg">
					<h4>Claire Wright</h4>
					<span>Tutor</span>
				</div></li>
		</ul>

	</div> --%>
					</div>
				</div>
			</div>
		</div>
		</section>

	</div>
</div>



<c:if test="${myProfileVO.errorOnMyProfile}">
<script type="text/javascript">
show('edit_myProfile');
hide('view_myProfile');
</script>
</c:if>


<c:if test="${myProfileVO.errorOnChangePwd}">
<script type="text/javascript">
show('change_password');
hide('view_image');
</script>
</c:if>

<%

if(renderRequest.getParameter("editImageStatus") != null) {

if(renderRequest.getParameter("editImageStatus").contains("true"))
  {	
%>
<script type="text/javascript">
show('edit_image');
hide('view_image');
</script>

<% 	
  }
}
%>