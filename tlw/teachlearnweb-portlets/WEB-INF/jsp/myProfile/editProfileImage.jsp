<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>

<%@page import="com.tlw.util.WebUtil"%>
<%@page import="com.liferay.portal.kernel.upload.UploadException"%>
<%@page import="com.liferay.portal.UserPortraitSizeException"%>
<%@page import="com.liferay.portal.kernel.util.PrefsPropsUtil"%>
<%@page import="com.liferay.portal.kernel.util.PropsKeys"%>
<%@page import=" com.liferay.portal.UserPortraitTypeException"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portal.model.User"%>


<portlet:defineObjects />
<liferay-theme:defineObjects />

<%
	
	User currentUser = themeDisplay.getUser();
	
%>

<portlet:actionURL var="editImageActionUrl">
	<portlet:param name="myaction" value="editImage" />
</portlet:actionURL>

<portlet:actionURL var="cancelEditImageActionUrl">
	<portlet:param name="myaction" value="cancelEditProfileImage" />
</portlet:actionURL>

<liferay-ui:error key="error-msg" message="We can't able to process your request. Please try again." />

<aui:form action="<%= editImageActionUrl.toString() %>"
	enctype="multipart/form-data" method="post" name="editImageForm">


	<div class="profilebg">

		<liferay-ui:error exception="<%= UploadException.class %>"
			message="an-unexpected-error-occurred-while-uploading-your-file" />

		<liferay-ui:error exception="<%= UserPortraitSizeException.class %>">

			<%
				long imageMaxSize = PrefsPropsUtil.getLong(PropsKeys.USERS_IMAGE_MAX_SIZE) / 1024;
				%>

			<liferay-ui:message arguments="<%= imageMaxSize %>"
				key="please-enter-a-file-with-a-valid-file-size-no-larger-than-x" />
		</liferay-ui:error>

		<liferay-ui:error exception="<%= UserPortraitTypeException.class %>"
			message="please-enter-a-file-with-a-valid-file-type" />

		<img class="user-logo"
			src="<%= currentUser.getPortraitURL(themeDisplay) %>" />

		<aui:fieldset>
			<aui:input styleClass="font-size:100%;"
				label='<%= LanguageUtil.format(pageContext, "upload-a-gif-or-jpeg-that-is-x-pixels-tall-and-x-pixels-wide", new Object[] {"120", "100"}, false) %>'
				name="fileName" size="50" type="file" />

			<aui:button-row>
				<aui:button type="submit" value="Upload" />

				
			</aui:button-row>
		</aui:fieldset>
	</div>
</aui:form>



