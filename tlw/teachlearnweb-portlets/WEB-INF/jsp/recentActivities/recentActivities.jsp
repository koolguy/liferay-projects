<%@page import="org.hibernate.HibernateException"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.teachlearnweb.dao.entity.RewardPoints"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.tlw.util.WebUtil"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.tlw.recentActivities.vo.RecentActivitiesVO"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<%
	final Logger logger = Logger.getLogger(this.getClass());
	List<RecentActivitiesVO> activitiesList = null;
	if(request.getAttribute("activitiesList") != null) {
		 activitiesList = (List<RecentActivitiesVO>) request.getAttribute("activitiesList");
	}
	
	List<RewardPoints> recentActivitiesList = Validator.isNotNull(request.getAttribute("recentActivitiesList")) ? (List<RewardPoints>)request.getAttribute("recentActivitiesList") : null;
	if(recentActivitiesList != null && !recentActivitiesList.isEmpty() && recentActivitiesList.size() > 0) {
		System.out.println("recentActivitiesList.size() from recentActivities JSP---->"+recentActivitiesList.size());
	}
%>

<div class="innerLeft">
     <p class="newoffers">RECENT ACTIVITY</p>
 </div>
<div class="clearfix" style="height:600px;width:700px;overflow:scroll;">

<%
	if(recentActivitiesList != null && !recentActivitiesList.isEmpty() && recentActivitiesList.size() > 0) {
		for(RewardPoints activity : recentActivitiesList) {
		com.liferay.portal.model.User  activityUser = null;
		try{
			System.out.println("activity.getUserId()------------------------------>"+activity.getUserId());
			activityUser = UserLocalServiceUtil.getUser(activity.getUserId());
			System.out.println("activityUser from recentActivities JSP---->"+activityUser+"------"+activityUser.getFirstName());
		}catch(com.liferay.portal.NoSuchUserException ex) {
			logger.error("NoSuchUserException Caught in recentActivities JSP--", ex);
		}catch(Exception ex){
			logger.error("Exception Caught in recentActivities JSP--", ex);
		}
		
		if(activityUser != null) {
%>

 			 <div class="rewardpointactivity clearfix">
                       <ul class="recentactivity">
                             <li>
                                   <table width="100%">
                                            <tr>
                                                <td class="tdleft"><img src="<%= activityUser.getPortraitURL(themeDisplay) %>" height="40" width="40"></td>
                                                <td>
                                                    <p class="qstn">
                                                    <%=activityUser.getFirstName()+" "+activityUser.getLastName() %>&nbsp;<%=activity.getDescription() %> And Earned <%=activity.getRewardPoints()%>&nbsp; Reward Points.
                                                    </p>
                                                    <p class="postdate"><%=activity.getRewardDate() %></p>
                                                </td>
                                            </tr>
                                   </table>
                             </li>
                      </ul>
               </div>
<%
	} } }
%>

</div>



 <%-- <div class="clearfix" id="wrapper">
    	<div class="innerLeft">
        	<p class="newoffers">RECENT ACTIVITY</p>
        </div>

<%
	if(activitiesList != null && !activitiesList.isEmpty() && activitiesList.size() > 0) {
		for(RecentActivitiesVO activity : activitiesList) {
		String topicName = Validator.isNotNull(activity.getWsTempVO().getSubTopicName()) ? activity.getWsTempVO().getSubTopicName() : activity.getWsTempVO().getParentTopicName();	
		com.liferay.portal.model.User  activityUser = UserLocalServiceUtil.getUser(activity.getUserId());
%>

 			 <div class="rewardpointactivity clearfix">
                       <ul class="recentactivity">
                             <li>
                                   <table width="100%">
                                            <tr>
                                                <td class="tdleft"><img src="<%= activityUser.getPortraitURL(themeDisplay) %>" height="40" width="40"></td>
                                                <td>
                                                    <p class="qstn"><%=activity.getUserNmae()%> &nbsp;has Completed &nbsp;<%=topicName%> &nbsp;WorkSheet and Earned &nbsp;<%=activity.getRewardPoints()%> &nbsp;Reward Points.</p>
                                                </td>
                                            </tr>
                                   </table>
                             </li>
                      </ul>
               </div>
<%
	} }
%>

</div> --%>