<%@page import="org.apache.velocity.runtime.directive.Foreach"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page
	import="com.liferay.portlet.asset.model.AssetCategory,com.tlw.util.WebUtil,java.util.List,com.teachlearnweb.service.vo.User"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<liferay-theme:defineObjects />
<%String path = request.getContextPath();
String pathToImages = themeDisplay.getPathThemeImages();
%>
<portlet:defineObjects />
		<portlet:actionURL var="subscribeURL">
			<portlet:param name="action" value="subscribeCourse" />
			<portlet:param name="categoryId" value="CateGoRyId" />
		</portlet:actionURL>
		<% Long categoryId = Long.parseLong((String)request.getParameter("categoryId"));
     AssetCategory assetCategory = WebUtil.getAssetCategoryById(categoryId);
     //User loggedInUser = WebUtil.getUser(renderRequest);
%>
<!-- Body Section -->    
    <section class="clearfix">
    	<div id="wrapper">
        	<div class="clearfix mt20">
            	<div class="innerLeft">
                <p class="head-green">Secure Payment </p>
            	
                <!-- Payment steps -->

                <div id="payprocesssteps" class="payprocesssteps">
                    <ul class="payprosteplist clearfix">
                        <li>
                            <span class="stepno">1</span>
                            <span class="stepname">Billing</span>
                        </li>
                        <li>
                            <span class="stepno">2</span>
                            <span class="stepname">Shipping</span>
                        </li>
                        <li class="active">
                            <span class="stepno">3</span>
                            <span class="stepname">Payment</span>
                        </li>
                        <li>
                            <span class="stepno">4</span>
                            <span class="stepname">Confirmation</span>
                        </li>
                    </ul>
                </div>
				<div class="shippingform clearfix">
                        <div class="paytypetabssection">
                        <ul class="paytypetabs">
                        <li><a href="#" class="active">Debit Card</a></li>
                        <li><a href="#">Credit Card</a></li>
                        <li><a href="#">Net Banking</a></li>
                        <li><a href="#">Paypal</a></li>
                        </ul>
                        </div>
                        <div class="paytypeinfosection">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                        <td class="td1">We accept</td>
                        <td class="td2"><p class="paycardtype"><img src="<%= themeDisplay.getPathThemeImages() %>/visa.png"><img src="<%= themeDisplay.getPathThemeImages() %>/mastercard.png"><img src="<%= themeDisplay.getPathThemeImages() %>/americanexpress.png"><img src="<%= themeDisplay.getPathThemeImages() %>/maestro.png"></p></td>
                        </tr>
                        <tr>
                        <td class="td1">Card Number</td>
                        <td class="td2"><input type="number" class="cardnoinput"><input type="number" class="cardnoinput"><input type="number" class="cardnoinput"><input type="number" class="cardnoinput"></td>
                        </tr>
                        <tr>
                        <td class="td1">Name on Card</td>
                        <td class="td2"><input type="text" class="cardname" name="name"></td>
                        </tr>
                        <tr>
                        <td class="td1">CVV Number</td>
                        <td class="td2"><input type="number" class="cvvno" name="num"> <a href="#">?</a></td>
                        </tr>
                        <tr>
                        <td class="td1"></td>
                        <td class="td2"><input type="checkbox" name="check" >&nbsp;I agree to terms and conditions</td>
                        </tr>
                        </table>
                        
                        </div>
                        </div>
                        <div class="shoptabs">
                        <a href="javascript:callSubscribe(${categoryId});" class="shippingshopping">Confirm</a>
                        </div>
                        </div>
            	<div class="innerRight">
                	<%--<div class="searchbtn">
                            <input type="search" name="search" value="Cross Polination and its causes"><a href="searchresults.html"><img src="images/searchicon.png"></a>
                         </div> --%>
                         
                         <h3>Summary</h3>
						<ul class="shopsummarylist">
                                            	<li>
                                                <table class="shopsummarytable">
                                                   <c:forEach items="${cartItems}" var="cartItems">
                               						 <c:set var="categoryId" value="${cartItemMap.key}"/>
                               						 <c:set var="cartItem" value="${cartItemMap.value}"/>
                                					 <c:set var="category" value="${cartItem.category}"/>
                                                    <tr>
                                                        <td class="td1"><img src="<%=pathToImages%>/sbook1.jpg"></td>
                                                        <td class="td2">
                                                        	<p class="sumfor">${category.name}</p>
                                                            <p class="sumitemanem">${category.description}</p>
                                                            <%--<p class="sumby">by Rashmi Bansalr</p> --%>
                                                        </td>
                                                        <td class="td3">Rs ${cartItem.price}</td>
                                                    </tr>
                                                  </c:forEach>
                                                </table>
                                                </li>
                                                <li>
                                                	<table class="totallist">
                                                        <tr>
                                                            <td>Total Amount</td>
                                                            <td class="td2">${totalCartAmount}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Discount</td>
                                                            <td class="td2"><div class="discountdisplay"></div></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Net Payable</td>
                                                            <td class="td2"><div class="totaldisplay">${totalCartAmount}</div></td>
                                                        </tr>
                                                    </table>
                                                </li>
                                                
                        </ul>
                   
               	</div>
            </div>
      </div>
    </section>
<script>
function callSubscribe(categoryId){
	 var url = "<%=subscribeURL.toString()%>";
	 //alert(url);
	 url = url.replace("CateGoRyId",categoryId)
	 window.location.href=url;
}
</script>
