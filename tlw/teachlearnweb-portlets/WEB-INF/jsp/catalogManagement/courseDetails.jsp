<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page
	import="com.liferay.portlet.asset.model.AssetCategory,com.tlw.util.WebUtil"%>
<link rel="stylesheet" href="../css/styles.css" type="text/css">
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<liferay-theme:defineObjects />

<portlet:defineObjects />

<%String path = request.getContextPath();
String pathToImages = themeDisplay.getPathThemeImages();
%>


<title>Teach Learn Web</title>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1" />

<c:set var="categoryId" value="${category.categoryId}" />
<c:set var="categoryName" value="${category.name}" />

<% long categoryId = (Long) pageContext.getAttribute("categoryId"); 

  String name = null;
  String categoryName = (String )pageContext.getAttribute("categoryName");
  if(categoryName.contains("Class")){
	  String[] className = categoryName.split(" ");
	  name = className[1];
  }
  else {
	  name = categoryName;
  }
  
%>

<%--<div class="classnumber">
	<p class="number" style="font-size: 96px;margin-top: 28px;text-align:center"><%= name %></p>
	<%
	  if(null != WebUtil.getAmount(categoryId)){
		  %>
		  <p class="classfee"><img
							src="<%=pathToImages%>/icon-rupees-cart.png" alt=""
							><%= WebUtil.getAmount(categoryId)%></p>
	<%
		}
	%>
	
</div>


<p class="classdescrip">${category.description}</p>
<div class="fRight mt20">

	<a rel="login" class="btn-blue trydemo lightboxopen" style="margin-right: 10px;">Try Demo</a> 
	<c:set var="subscribe" value="true"/>
	<c:choose>
	<c:when test="${fn:contains(isSubscribed, subscribe)}">
	<span class="btn-blue subscribe">Subscribed Course</span>
		
	   </c:when>
	   <c:otherwise>
	   <a href="javascript:callSubscribe(${category.categoryId}, '${category.name}');"
			class="btn-blue subscribe">Subscribe Now
	   </a>
	  </c:otherwise>
	</c:choose>
</div>

######
<c:set var="categoryList" value="${assetCategoryList[0]}" />
<c:set var="subCatMap" value="${categoryList.subCategoriesMap}" />

<div id="mainWrapper" class="creambg" style="padding: 10px;">
	<div id="wrapper">
		<c:forEach var="subCategory" items="${subCatMap[category.categoryId]}">
			<div class="innerLeft">
				<p class="head-green">${subCategory.name} / Topics</p>
				<ul class="topics">
					<c:forEach var="thirdLevelCategory"
						items="${subCatMap[subCategory.categoryId]}">
						<li>${thirdLevelCategory.name}</li>
					</c:forEach>

				</ul>
		
			</div>
		</c:forEach>
		
		
	</div>
</div>--%>

<c:set var="categoryList" value="${assetCategoryList[0]}" />
<c:set var="subCatMap" value="${categoryList.subCategoriesMap}" />
<div class="innerLeft">

	<ul class="classsyllabus left">
		<a href="javascript:void(0)">
			<li>
				<div class="topclass">
					<p class="classfee">${parentCategoryName}</p>
				</div>
				<div class="classnumber">
					<img src="<%=pathToImages%>/number<%=name%>.png">
				</div>

				<p class="price">
					Price:<img src="<%=pathToImages%>/icon-rupees-cart.png" /><%=(null != WebUtil.getAmount(categoryId)?WebUtil.getAmount(categoryId):"")%></p>
		</li> </a>
	</ul>
	<div class="cartdesp" style="width: 288px">
		<p class="packagecost">
			Package Price: <img src="<%=pathToImages%>/icon-rupees-cart.png" /><%=(null != WebUtil.getAmount(categoryId)?WebUtil.getAmount(categoryId):"")%></p>
		<p>
		<table>
			<tr>
				<td><span class="subject"> Subjects&nbsp;: </span>
				</td>
				<td><ul><c:forEach var="subject" varStatus="counter1"
						items="${subCatMap[category.categoryId]}">
						<li class="subject_li" style="margin:5px; float: left"><a href="javascript:void(0)"
							onclick="switchid('cl${counter1.index}');" class="class_sub">
							${subject.name}</a>
							</li>
					</c:forEach>
					</ul>
				</td>
			</tr>
		</table>
		</p>
		<p class="validity">Validity : 1 Year</p>
		<div class="outerdiv addcartbuynow">
			<c:set var="subscribe" value="true" />



		</div>

	</div>
	<div class="fRightaddtocart" style="float: left;width: 210px;">
		<a rel="subscribe" href="/web/guest/demo"
			class="btn-blue trydemo lightboxopen">Try Demo</a></br>

		<c:choose>
			<c:when test="${!isRegisteredUser}">
				<a href="javascript:alert('Please login to subscribe the Class');"
					class="addtocart">Add to Cart</a>
				<a href="javascript:alert('Please login to subscribe the Class');"
					class="buyitnow">Buy it Now</a>
			</c:when>
			
			
			<c:when test="${fn:contains(isSubscribed, subscribe)}">

				<a href="javascript:alert('Subscribed course');" class="addtocart">Add
					to Cart</a>
				<a href="javascript:alert('Subscribed course');" class="buyitnow">Buy
					it Now</a>

			</c:when>


			<c:otherwise>
			<c:if test="${!isPurchased}">
				<a href="javascript:addToCart('${category.categoryId}');"
					class="addtocart">Add to Cart</a>
				<a
					href="javascript:callSubscribe('${category.categoryId}', '${category.name}');"
					class="buyitnow">Buy it Now</a>
					</c:if>
			</c:otherwise>
		</c:choose>
	</div>
</div>

<c:forEach var="subCategory" varStatus="counter"
	items="${subCatMap[category.categoryId]}">
	<div id='cl${counter.index}'>
		<p class="head-red">${subCategory.name}</p>
		<ul class="topics">
			<c:forEach var="thirdLevelCategory"
				items="${subCatMap[subCategory.categoryId]}">
				<li>${thirdLevelCategory.name}</li>
			</c:forEach>

		</ul>
	</div>
</c:forEach>
<%--<ul class="usefulmatters">
                	<li>
                        <img src="<%=pathToImages%>/classesvedioIcon.png" alt="" class="mathstopics">
                        <span class="lession_videos">300 </span>
                        <span class="lessonvedios">Lesson Videos</span>
                    </li>
                    <li>
                        <img src="<%=pathToImages%>/classesworksheetIcon.png" alt="" class="mathstopics">
                        <span class="lession_videos">100 </span>
                        <span class="lessonvedios">Worksheets</span>
                    </li>
                    <li>
                    	<img src="<%=pathToImages%>/inlinetestIcon.png" alt="" class="mathstopics">
                        <span class="lession_videos">702 </span>
                        <span class="lessonvedios">Subtopic Test</span>
                   </li>
                </ul> --%>
<script>
function callDemo(){
alert('test');
window.href.location='video';
}
</script>