<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page import="com.liferay.portlet.asset.model.AssetCategory"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<liferay-theme:defineObjects />



<portlet:defineObjects />
<%--uncomment to enable payment pages<portlet:actionURL var="subscribeURL">
	<portlet:param name="action" value="shopping" />
	<portlet:param name="categoryId" value="CateGoRyId" />
	<portlet:param name="categoryName" value="CatName" />
</portlet:actionURL>--%>
<portlet:actionURL var="subscribeURL">
			<portlet:param name="action" value="subscribeCourse" />
			<portlet:param name="categoryId" value="CateGoRyId" />
		</portlet:actionURL>

<portlet:renderURL var="cartItemsURL">
	<portlet:param name="actionMapping" value="makePayment" />
</portlet:renderURL>
<portlet:resourceURL var="addToCartURL" id="addToCart">
	<portlet:param name="action" value="addToCart" />
	<portlet:param name="categoryId" value="CateGoRyId" />
</portlet:resourceURL> 
<%String path = request.getContextPath();
String pathToImages = themeDisplay.getPathThemeImages();

java.util.List cartItems = (java.util.List) renderRequest.getPortletSession().getAttribute("cartItems");
pageContext.setAttribute("cartItems",cartItems);



%>



<body>
	<div id="mainWrapper">
		<!-- Header Section -->



		<!-- Body Section -->
		<%--<section class="clearfix">
			<div id="wrapper">
			
				<p class="head-voilet">Classes</p>
						<div class="clearfix mt20">
							<div class="htabs ytabs">
								<ul>
									<c:set var="categoryList" value="${assetCategoryList[0]}" />
									<c:set var="subCatMap" value="${categoryList.subCategoriesMap}" />
									<c:forEach var="maincategory" items="${categoryList.category}"
										varStatus="mainStatus">
										<a href="#${maincategory.name}" id="main_${mainStatus.count}"
											onclick="javascript:getSubcats(${mainStatus.count});">
											<li>${maincategory.name}</li>
										</a>
									</c:forEach>
								</ul>

								<div class="clearfix ytabs-cont">
									<c:forEach var="firstLevel" items="${categoryList.category}"
										varStatus="firstStatus">
										<c:set var="categoryId" value="${firstLevel.categoryId}" />
										<div id="${firstLevel.name}">
											<c:forEach var="subCategory" items="${subCatMap[categoryId]}"
												varStatus="status">
												<span><a href="#${firstLevel.name}"
													onclick="javascript:showDescription(${subCategory.categoryId})">${subCategory.name}</a></span>
												<input type="hidden"
													name="${firstStatus.count}_index_${status.count}"
													id="${firstStatus.count}_index_${status.count}"
													value="${subCategory.categoryId}" />
											</c:forEach>
										</div>
									</c:forEach>
								</div>
							</div>
							<div id="contentbgid"></div>
						</div>
					
			</div>

			<div id="subcategorydata"></div>
		</section>--%>
<!-- Body Section --> 
<c:set var="categoryList" value="${assetCategoryList[0]}" />
<c:set var="subCatMap" value="${categoryList.subCategoriesMap}" />   
    <section class="clearfix">
    	
        	<div class="clearfix  mt20">
            <div class="innerLeft">
             <ul class="subnavlinks clearfix">
             	<c:forEach var="maincategory" items="${categoryList.category}"
										varStatus="mainStatus">
						<input type="hidden" name="mainCat${mainStatus.count}" id="mainCat${mainStatus.count}" value="${maincategory.name}@${maincategory.categoryId}">
					<%-- 	<li><a href="#" onclick="javascript:getSubcats('${maincategory.categoryId}','${maincategory.name}');" class="active">${maincategory.name}</a></li> --%>
									
				</c:forEach>
             </ul>
			<div id="classesdiv"></div>
			<div id="subcategorydata"></div>
          </div> 
            
            </div>
            	
               
      
    </section>	

		<!-- Footer Section -->
	</div>
	<!--login pop up-->
	<div id="login" class="lightbox">
		<div class="outerdiv setlightboxheight">
			<div class="lightboxwrapper">
				<div class="logindiv">
					<p class="loginpage">Login</p>
					<table>
						<tbody>
							<tr>
								<td class="logintype"><label>User Type:</label></td>
								<td class="td2"><select>
										<option>Student</option>
										<option>Parent</option>
								</select></td>
							</tr>
							<tr>
								<td class="logintype"><label>Login</label></td>
								<td class="td2"><input type="text" name="login"></td>
							</tr>
							<tr>
								<td class="logintype"><label>Password</label></td>
								<td class="td2"><input type="password" name="Pw"></td>
							</tr>
						</tbody>
					</table>
					</form>
					<button class="loginclick">Login</button>
					<div class="clearfix" resiternow>
						<p class="registeruser">
							If you are not registered/subscribed user. Kindly <a href="#"
								class="registerclick lightboxopen" rel="register">register</a>
							yourself.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Register popup-->
	<div id="register" class="lightbox">
		<div class="outerdiv setlightboxheight">
			<div class="lightboxwrapper">
				<div class="registernow">
					<p class="resiternowtxt">Register Now</p>
					<ul>
						<li><a href="#">I'm a Student</a></li>
						<li><a href="#">I'm a Parent</a></li>
						<li><a href="#">I'm a Teacher</a></li>
					</ul>
					<p class="corporatereg">
						For corporate Registrations please call us at 9898989898 or Enroll
						your School <a href="#" class="moreq">Here</a>
					</p>
				</div>
			</div>
		</div>
	</div>
</body>




<script>
 $(document).ready(function() {
	
	var vals = $('#mainCat1').val();
	var cat = vals.split('@');
	 getSubcats(cat[1],cat[0]);
	 
	});
 
 function getSubcats(categoryId,categoryName){
	 var url = '<portlet:resourceURL
			id="showClasses"></portlet:resourceURL>'+
			'?action=actionShowDescription&categoryId='+categoryId+'&categoryName='+categoryName;
	        var xhr;
	        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
	                 xhr=new XMLHttpRequest();
			   }else {					// code for IE6, IE5
			          xhr=new ActiveXObject("Microsoft.XMLHTTP");
			 }		 
	       // alert('url'+url);
			  xhr.open("GET", url, true);
			  xhr.onreadystatechange = function() {
			  if (xhr.readyState == 4 && xhr.status==200) {
				  	document.getElementById("classesdiv").innerHTML = xhr.responseText;
	              	}
	        	}
	    xhr.send();
	}
 

//here you place the ids of every element you want.
var ids=new Array('cl0','cl1','cl2','cl3','cl4','cl5','cl6','cl7','cl8');

function switchid(id){	
	hideallids();
	if(document.getElementById(id)){
		$(document.getElementById(id)).show();
	}
	//showdiv(id);
}

function hideallids(){
	//loop through the array and hide each element by id
	for (var i=0;i<ids.length;i++){
		if(document.getElementById(ids[i])){
			$(document.getElementById(ids[i])).hide();//hidediv(ids[i]);
		}
	}		  
}


 function showCartItems(){
	 var url = "<%=cartItemsURL.toString()%>";
	 //alert(url);
	 //url = url.replace("CateGoRyId",categoryId)
	 window.location.href=url;
 }
 
 function addToCart(categoryId){
	 var cartUrl = "${addToCartURL}";
//alert(url);
	cartUrl = cartUrl.replace("CateGoRyId",categoryId)
	 //window.location.href=url;
	 
			$.ajax({
			      type: "GET",
			      url: cartUrl,      
			      success: function(data) {
			    	  var successData = data.split('######')
			alert(successData[0])
			$('#cartsize').html(successData[1])
			}
			 });
		
 }
 function showDescription(assetKey){
		var url = '<portlet:resourceURL
			id="actionShowDescription"></portlet:resourceURL>'+
			'?action=actionShowDescription&categoryId='+assetKey;
	        var xhr;
	        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
	                 xhr=new XMLHttpRequest();
			   }else {					// code for IE6, IE5
			          xhr=new ActiveXObject("Microsoft.XMLHTTP");
			 }		 
	       // alert('url'+url);
			  xhr.open("GET", url, true);
			  xhr.onreadystatechange = function() {
			  if (xhr.readyState == 4 && xhr.status==200) {
				  //var response = xhr.responseText.split("######");
	              	document.getElementById("classesdiv").innerHTML = xhr.responseText;
	              	switchid('cl0');
	              	//document.getElementById("subcategorydata").innerHTML = response[1];
	              	}
	        	}
	    xhr.send();
	}
 function callSubscribe(categoryId, categoryName){
 	 var url = "<%=subscribeURL.toString()%>";
 	 url = url.replace("CateGoRyId",categoryId);
 	 url = url.replace("CatName",categoryName);
 	 window.location.href=url;
 }

 function callDemo(){
 window.location.href='video';
 }

/* function callSubscribe(categoryId){
	 var url = "<%=subscribeURL.toString()%>";
	 url = url.replace("CateGoRyId",categoryId)
	 window.location.href=url;
}*/


 </script>