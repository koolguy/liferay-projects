<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://alloy.liferay.com/tld/aui" prefix="aui" %>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<%@page import="com.tlw.util.WebUtil"%>
<%@page import="com.liferay.portal.kernel.upload.UploadException" %>
<%@page import="com.liferay.portal.UserPortraitSizeException" %>
<%@page import="com.liferay.portal.kernel.util.PrefsPropsUtil" %>
<%@page import="com.liferay.portal.kernel.util.PropsKeys" %>
<%@page import=" com.liferay.portal.UserPortraitTypeException" %>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil" %>
<%@page import="com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil"%>



<portlet:defineObjects />
<liferay-theme:defineObjects />

<%
	
	com.liferay.portal.model.User currentUser = themeDisplay.getUser();
    com.teachlearnweb.service.vo.User loggedInuser = WebUtil.getUser(currentUser.getCompanyId(),request);
	
%>

<portlet:actionURL var="imageActionUrl">
    <portlet:param name="myaction" value="renderEditProfileImage" />
</portlet:actionURL>

<form action="<%= imageActionUrl.toString() %>"  method="post" name="imageForm">

<ul class="canvasright">
            	<li class="greybg2">
                    	<table width="100%">
                        	<tbody>
                            	<tr>
	                                <td class="rightleft">
                                    	<div class="rightleftdata">
                                            <h3 class="profile">Profile</h3>
                                            <p>
                                            <img  class="profileviewer" src="<%= currentUser.getPortraitURL(themeDisplay) %>" /></p>
                                           <!--  <p>
                                            <a class="profileowner" href="javascript: document.imageForm.submit();">Change</a>
                                            
                                            </p> -->
                                            
                                   		</div>
                                    </td>
                                   <c:forEach items="${rewardPoints}" var="reward">
                   		<c:set var="count" value="${count + reward.rewardPoints}"/>
                   		</c:forEach>
                                   	
                                    	<td class="rightbox1" align="right">
                                            <p class="profileowner"><a href="/group/guest/profile"><%=currentUser.getFirstName()+","+currentUser.getLastName() %></a></p>
                                            	<p class="posubtitle">
                                            	
                                            	<%= WebUtil.getAssetCategoryTitle(loggedInuser.getClassName())%>,
                                            	
												<% if(loggedInuser.getSectionName() != null ){ 
												      out.println("Section "+loggedInuser.getSectionName());
												    }
												%><br>
											    <% if(loggedInuser.getSchoolName() != null){
													  out.println(loggedInuser.getSchoolName());
												   }
												%></p>
											  <a class="lightboxopen viewreward" rel="rewardpoints">${count}
												
											  </a>
											  <p class="rewpoints" ref="rewardpoints">Reward Points</p>
                                		</td>
                                    </tr>
                                   
                                
                             </tbody>
                        </table>
                    </li>                        
                    
                    <li class="greenbg">
                    	<table width="100%">
                        	<tbody>
                            	<tr>
	                                <td class="leftbox1">
                                    	<div class="leftdata">
                                            <h3 class="social">Social</h3>
                                             <p class="moresub">Social Networking <br>
											 Coming soon</p>
                                   		</div>
                                    </td>
                                   
                                   	
                                    	<td align="center" class="rightbox1">
                                        
                                            <p><img src="<%=themeDisplay.getPathThemeImages()%>/social_profile_icon.png" alt="" class="subjectcandidate"></p>
                                            	
                                		</td>
                                    </tr>
                                   
                                
                             </tbody>
                        </table>
                    </li>
                   
                   <li class="greybg2">
                    	<table width="100%">
                        	<tbody>
                            	<tr>
	                                <td class="leftbox1">
                                    	<div class="leftdata">
                                            <h3 class="profile">Stores</h3>
                                            <p class="marthaimg"><img src="<%=themeDisplay.getPathThemeImages()%>/cartoon_image.png" alt=""></p>
                                            <p class="cartmembers">Coming soon <br>
												
                                   		</div>
                                    </td>
                                   
                                   	
                                    	<td align="center" class="rightbox1">
                                            <p class="newgodiess">New Goodies</p>
                                            	<p class="godiesnum">215+</p>
                                                                      <p class="carticon"><img src="<%=themeDisplay.getPathThemeImages()%>/cart_icon.png" alt=""></p>
                                		</td>
                                    </tr>
                                   
                                
                             </tbody>
                        </table>
                    </li>
                </ul>


  	</form>
<!-- popup Box for REWARDS-->
<div id="rewardpoints" class="lightbox">
	<div class="outerdiv setlightboxheight">
    	<div class="lightboxwrapper">
            	<div class="lbcheightfix">
                	<div class="topbarbg">
                    <div class="topbgleft">
                    	<h2 class="reward_head">Reward Points</h2>
                        </div>
                        <div class="topbgright">
                        </div>
                    </div>
                   <div class="centerboxlb">
                   		<table class="rewardspoints_details">
                   		
                        	<tr>
                            	<td>Total <span>${fn:length(rewardPoints)} Task(s)</span> gained you <span>${count} Points</span><br>
                                   <!--  You've purchased <span>11 Item (s)</span> spending <span>80 Points</span><br>
                                    Finishing the <span>34 Expired Tasks (s)</span> can earn you <span>832+ Points</span> -->
                                </td>
                           		<td class="td2"><!--<div class="rewardgifts"><h1>754</h1></div>More Reward Points are available</td>-->
                             </tr>
                        </table>
                    	<table class="rewardsmath">
                    	<c:forEach items="${rewardPoints}" var="rewardDetails">
                        	<tr>
                            	<td class="td1"><img src="<%=themeDisplay.getPathThemeImages()%>/motion_icon.png"></td>
                           		<td class="td2">${rewardDetails.description}
                           		</td>
                                <td class="td3">${rewardDetails.rewardDate}<br>
												<span></span></td>
                                <td class="td4">${rewardDetails.rewardPoints}</td>
                            </tr>    
                         </c:forEach>
                             
                        </table>
                    </div>
            		
            	</div>
            </div>
 
    </div>  
</div>
<script>
$(document).ready(function(){
	var lbg = $("<div class='lightbox_bg'></div>");
	$('body').append(lbg);
	
	var lclose = $("<a class='lightbox_close'>Close This</a>");
	$('.lightbox').append(lclose);
	
	$('.lightboxopen').click(function(){
	var myval = $(this).attr("rel");
	var moniter_wdith = window.innerWidth;
	var moniter_height = window.innerHeight;
	var lightboxinfo_wdith = $("#" + myval).width();	
	var lightboxinfo_height= $("#" + myval).height();
	var remain_wdith =  moniter_wdith - lightboxinfo_wdith;		
	var remain_height =  moniter_height - lightboxinfo_height;		
	var byremain_wdith = remain_wdith/2;
	var byremain_height = remain_height/2;
	var byremain_height2 = byremain_height;

	// Lightbox Height set
	//var viewportwidth3 = $('.lightboxcontanerright').height();
	//alert(viewportwidth3);

	$("#" + myval).css({left:byremain_wdith});
	$("#" + myval).css({top:byremain_height2});
	$('.lightbox_bg').show()
			$("#" + myval).animate({
			opacity: 1,
		}, 10 ,function() {
    		var viewportwidth3 = $('.lightbox').height();
			$(".lbcheightfix").css({height:viewportwidth3});
  		});
	});
	
	$('a.lightbox_close').click(function(){
		var myval2 =$(this).parent().attr('id');
		$("#" + myval2).animate({
			opacity: 0,
			top: "-2000px"
		},10,function(){
			$('.lightbox_bg').hide()
		});
	});
	
	$('a.lbclose').click(function(){
		var myval2 =$(this).parents().attr('id');
		$("#" + myval2).animate({
			opacity: 0,
			top: "-2000px"
		},10,function(){
			$('.lightbox_bg').hide()
		});
	});
	
});
</script>

