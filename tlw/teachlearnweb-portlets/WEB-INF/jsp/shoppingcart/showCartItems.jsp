<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page import="com.liferay.portlet.asset.model.AssetCategory,com.tlw.util.WebUtil"%>
<link rel="stylesheet" href="../css/styles.css" type="text/css">
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<portlet:defineObjects />
<%
String pathToImages = "/tlw-theme/images";
%>
     	<div class="topbarbg">
                    <div class="topbgleft">
                    	<p class="totalcart">Total <span>${fn:length(cartItems)} Item</span>(s) in your Cart worth <span>Rs.${totalCartAmount}</span></p>
                        </div>
                        <div class="topbgright">
                        	
                        </div>
                    </div>
                  
                   <div class="centerboxlb">
                    	<table class="cartworth">
                        	<tr>
                            	<!--<td class="td1">Select</td>-->
                           		<td class="td2">Items</td>
                                <td class="td3">Quantity</td>
                                <td class="td4">Price</td>
                                <td class="td5">Remove</td></tr>
                                <c:forEach items="${cartItems}" var="cartItemMap">
                                <c:set var="categoryId" value="${cartItemMap.key}"/>
                                <c:set var="cartItem" value="${cartItemMap.value}"/>
                                <c:set var="category" value="${cartItem.category}"/>
                            	<tr>
                            	<!-- <td><input type="checkbox"  name="check" value=""></td> -->
                                <td class="td2"><img src="<%=pathToImages%>/peterrabit.png">
                                	<p>${category.name}</p>
                                   	<p class="talerabit">${category.description}<br>
									<span></span>
                                    </p>
                                </td>
                                <td>
                                <c:choose>
                                <c:when test='${cartItem.itemType == "Class" }'>
                                	1
                                    </c:when>
                                    <c:otherwise>
                                    <select>
                                      <option value="1">1</option>
                                      <option value="2">2</option>
                                      <option value="3">3</option>
                                      <option value="4">4</option>
                                    </select>
                                    </c:otherwise>
                                    </c:choose>
                                    </td>
                                <td>Rs. ${cartItem.price}</td>
                                <td><a href="javascript:removeCartItem('${categoryId}')"><img src="<%=pathToImages%>/close.png"></a></td></tr>
                                </c:forEach>
                                
                        </table>
                        <div class="bottomtext">
                        	<p class="bottomleft">Items: ${fn:length(cartItems)}</p>
                            <!-- <p class="bottomright">Shipping: Rs. 25<br> -->
                            <span>Grand Total: Rs. ${totalCartAmount}</span></p>
                        </div>
                        	<a href="javascript:proceedToPay();" class="proceedbtn paynowbtn">Proceed</a>
                    </div>
                    ######${fn:length(cartItems)}
                    