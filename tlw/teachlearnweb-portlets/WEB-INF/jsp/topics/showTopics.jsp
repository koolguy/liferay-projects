<%@ page
	import="com.tlw.util.WebUtil,java.util.List,com.liferay.portlet.asset.model.AssetCategory,com.tlw.topics.AssestCategoryComparator,java.util.Collections"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page import="com.liferay.portlet.asset.model.AssetEntry"%>
<%@ page import="java.util.StringTokenizer"%>
<%@ page import="java.util.Iterator"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<portlet:defineObjects />
<%-- <div style="display: none; visibility: hidden;" id="topicLeftNav">
<ul>
<li>
	<a href="#" id="<portlet:namespace/>eocTestsLabel" class="active">
		<span class="mthumb subjecttest"></span>
        <span class="mthumbname">EOC Test</span>
    </a>
</li>
</ul>
</div>

<portlet:resourceURL var="eocTestsURL" id="viewEOCTests" />

<script>
$(document).ready(function() {
	$('.rmenutab a').on('click', function() {
		var topicName = $(this).attr('rel');
		// clear left nav
		var htmlLeftNav = $('#topicLeftNav').html();
		$('.mainnavsection').html(htmlLeftNav);
		preSelSectionId = '<portlet:namespace/>eocTestsLabel';
		// load EOC tests
		viewEOCTests(topicName);
		// schedule a click event on left side link
		$('#<portlet:namespace/>eocTestsLabel').live('click', function() {
			// load EOC tests
			viewEOCTests(topicName);
		});
	});
});
function viewEOCTests(topicName) {
	$.ajax({
	      type: "GET",
	      url: "${eocTestsURL}",
	      data: {<portlet:namespace/>topicName: topicName},
	      success: function(data) {
	    	  if (data != '-1') {
	    		  $('#showAssetsContent').html(data);
	    	  }
	      }
   	});
}
</script>
 --%>
 
<%!

public String toCamel(String strTopic){		
	String strTopicName = "";
    StringTokenizer tokenizer = new StringTokenizer(strTopic , " ");
    while(tokenizer.hasMoreTokens()){	    	
    String token = tokenizer.nextToken();	        
    strTopicName = strTopicName + token.substring(0, 1).toUpperCase() + token.substring(1, token.length());
    strTopicName = strTopicName + " ";        
    }
	return strTopicName;	    
}

public List<AssetCategory> moveOverviewToTop(List<AssetCategory> topicsList) {
	AssetCategory assetCategoryTemp = null;
	for (Iterator iterator = topicsList.iterator(); iterator.hasNext();) {
		AssetCategory assetCategory = (AssetCategory) iterator.next();
		if ("overview".equalsIgnoreCase(assetCategory.getName())) {
			assetCategoryTemp = assetCategory;			
			iterator.remove();
			break;
		}		
	}
	
	if(null != assetCategoryTemp){		
		topicsList.add(0, assetCategoryTemp);		
	}
	
	return topicsList;
}		



%>
<!-- Right Section -->
<div class="rightwrap">
	<div class="rSubTitle">
		<p class="rstSubject">Topics</p>
	</div>
	<div class="wraper outerdiv">
		<!-- Right Section -->
		<div class="rightwrap">
			<div class="rightwrap">
				<div class="rightmenusection">
					<div id="rightmainmenu">
					
						<c:if test="${fn:length(rootCategoryList) == 0}">
		
					<table width="100%">
						<tbody>
							<tr>
								<td>
									<p class="moresub">
										Topics Not Available <br> Coming soon
									</p></td>
							</tr>


						</tbody>
					</table>
	</c:if>

						<c:forEach var="category" items="${rootCategoryList}">
							<c:set var="categoryId" value="${category.categoryId}" />
							<c:set var="vocabularyId" value="${category.vocabularyId}" />
							<%
								long categoryId = (Long) pageContext.getAttribute("categoryId");
								long vocabularyId = (Long) pageContext.getAttribute("vocabularyId");
								List<AssetCategory> topicsList = WebUtil.getChildCategories(categoryId, vocabularyId);
								
								Collections.sort(topicsList, new AssestCategoryComparator());
								topicsList = moveOverviewToTop(topicsList);
								
								if(null != topicsList && topicsList.size() > 0){
								for (int i = 0; i < topicsList.size(); i++) {
									AssetCategory level1Child = topicsList.get(i);
				             %>

							<c:set var="level1ChildLocal" value="<%=level1Child.getName()%>" />

							<div
								class="rmenutab ${level1ChildLabel == level1ChildLocal ? 'active ractive':''}">

								<c:if test="${level1ChildLocal eq 'Overview'}">
									<a rel="<%= level1Child.getName() %>"
										href="<portlet:actionURL>
							<portlet:param name="action" value="showCourse" />	
							<portlet:param name="level1Child" value="<%=level1Child.getName()%>"  />  
							<portlet:param name="topicsId" value="<%= String.valueOf(level1Child.getCategoryId()) %>" />					
						    </portlet:actionURL>" title="<%=level1Child.getCategoryId()%>" ><%= WebUtil.toCamelCase(level1Child.getName())  %></a>
								</c:if>

								<c:if test="${level1ChildLocal ne 'Overview'}">
									<a href="javascript:void(0)" title="<%=level1Child.getCategoryId()%>"  rel="<%= level1Child.getName() %>"><%= WebUtil.toCamelCase(level1Child.getName())%></a>
								</c:if>

							</div>
							<c:if test="${level1ChildLocal ne 'Overview'}">
								<div class="rSubmenu">
									<ul>
										<%
							        List<AssetCategory> level2List = WebUtil.getChildCategories(
											   level1Child.getCategoryId(), level1Child.getVocabularyId());
									   
										if(null != level2List && level2List.size()>0){
											
										Collections.sort(level2List, new AssestCategoryComparator());
											level2List = moveOverviewToTop(level2List);
											
									   for(int j=0;j<level2List.size();j++){
										   AssetCategory level2Child = level2List.get(j);
						%>
										<c:set var="level2ChildLocal"
											value="<%=level2Child.getName()%>" />
										<li
											class="${((level2ChildLabel == level2ChildLocal) and (level1ChildLabel == level1ChildLocal)) ? 'active':''}"><a
											href="<portlet:actionURL>
							<portlet:param name="action" value="showCourse" />	
							<portlet:param name="topicsId" value="<%= String.valueOf(level2Child.getCategoryId()) %>" />
							<portlet:param name="level1Child" value="<%= level1Child.getName() %>"  />
							<portlet:param name="level2Child" value="<%= level2Child.getName() %>"  />					
						    </portlet:actionURL>" title="<%=level2Child.getCategoryId()%>" ><%= WebUtil.toCamelCase(level2Child.getName()) %></a>
											<!-- <p class="rrcaltab">
												<a rel="calender" class="lightboxopen"> <img
													src="images/menu_calendericon.png">
												</a>
											</p> -->
										</li>


										<%
							}
										}
						%>


									</ul>
								</div>
							</c:if>
							<%
					}
								}
					%>

						</c:forEach>

					</div>
				</div>
			</div>
			<!-- Left Section -->

			<!-- Content Section -->

		</div>

		<%-- <div class="wraper outerdiv">
            	<!-- Right Section -->
                    <div class="rightwrap">
                	<div class="rightwrap">
			<div class="rightmenusection">
			<div id="rightmainmenu">

			<c:forEach var="category" items="${rootCategoryList}">

				<c:set var="categoryId" value="${category.categoryId}" />
				<c:set var="vocabularyId" value="${category.vocabularyId}" />

				<%
					long categoryId = (Long) pageContext.getAttribute("categoryId");
								long vocabularyId = (Long) pageContext
										.getAttribute("vocabularyId");
								List<AssetCategory> subjectList = WebUtil.getChildCategories(
										categoryId, vocabularyId);
								for (int i = 0; i < subjectList.size(); i++) {
									AssetCategory level1Child = subjectList.get(i);
				%>

				<div class="${level1ChildLabel==level1Child.getName()?'rmenutab active':'rmenutab'}">
						 <a href="<portlet:actionURL>
							<portlet:param name="action" value="showCourse" />	
							<portlet:param name="level1Child" value="<%=level1Child.getName()%>"  />
							<portlet:param name="topicsId" value="<%= String.valueOf(level1Child.getCategoryId()) %>" />					
						    </portlet:actionURL>"><%=level1Child.getName()%>
					     </a>
						</div>
				<div class="rSubmenu">
					<ul>
						<%
							List<AssetCategory> level2List = WebUtil.getChildCategories(
											   level1Child.getCategoryId(), level1Child.getVocabularyId());
									   
										if(null != level2List && level2List.size()>0){
									   for(int j=0;j<level2List.size();j++){
										   AssetCategory level2Child = level2List.get(j);
						%>
						<li class="${level2ChildLabel==level2Child.getName()?'active':''}">
						<a href="<portlet:actionURL>
							<portlet:param name="action" value="showCourse" />	
							<portlet:param name="topicsId" value="<%= String.valueOf(level2Child.getCategoryId()) %>" />
							<portlet:param name="level1Child" value="<%=level1Child.getName()%>"  />
							<portlet:param name="level2Child" value="<%=level2Child.getName()%>"  />					
						</portlet:actionURL>"><%=level2Child.getName()%></a>
							<p class="">
								<span>12 May</span><span>11.20 pm</span>
							</p> 
						</li>

						<%
							}
										}
						%>


					</ul>
				</div>

				<%
					}
				%>

			</c:forEach>
		</div>
	</div>
	</div>
	</div>
	</div> --%>
	</div>