<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="org.apache.log4j.Logger"%>
<%@page import="com.liferay.portal.NoSuchLayoutException"%>
<%@page
	import="java.util.Date,javax.portlet.PortletURL,com.liferay.portlet.PortletURLFactoryUtil,javax.portlet.PortletRequest"%>
<%@page
	import="com.tlw.util.WebUtil,java.util.List,com.liferay.portlet.asset.model.AssetCategory,com.tlw.util.DateUtil"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page import="com.liferay.portlet.asset.model.AssetEntry"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>

<%@ page import="com.liferay.portlet.PortletURLFactoryUtil"%>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayPortletURL"%>
<%@ page import="javax.portlet.PortletRequest"%>
<%@ page import="javax.portlet.PortletURL"%>
<%@ page import="javax.portlet.PortletMode"%>

<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ page import="com.liferay.portal.model.Layout" %>
<%@page import="com.liferay.portal.service.LayoutLocalServiceUtil"%>

<%@ page import="java.util.StringTokenizer" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>


<portlet:defineObjects />
<liferay-theme:defineObjects />





<ul class="canvascenter">
	<li class="calenderbg">
		<table width="100%">
			<tbody>
				<tr>
					<td class="centerleftdata">
						<div class="center_left">
							<p class="calender"><a href="/group/guest/calendar">Calendar</a></p>
							<p class="taskdate">
								Tasks:
								<%= DateUtil.getDateInstance(new Date()) %></p>
						</div></td>
					<td align="center">
						<p>
							<a href="/group/guest/calendar"><img
								src="<%=themeDisplay.getPathThemeImages()%>/calender_icon.png"
								class="calender_icon"></a>
						</p></td>
				</tr>
			</tbody>
		</table></li>
	</a>
	
	<% 
	
	final Logger LOG = Logger.getLogger(this.getClass());
	String friendlyURL = "/calendar";
	String employeeSearchPortletURL = null;
	Layout layout1 = null;
	String portletId = null;
	try{
	layout1 = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getLayout().getGroupId(), true, friendlyURL);
	}catch (NoSuchLayoutException ex){
		LOG.error("NoSuchLayoutException Exception Caught for calendar layout", ex);
	}
	if(Validator.isNotNull(layout1)) {
	 	employeeSearchPortletURL = layout1.getTypeSettingsProperty("column-1");
	 	StringTokenizer st = new StringTokenizer(employeeSearchPortletURL,",");
		if (st.hasMoreTokens()) {
			portletId = st.nextToken();
		}
	}
	
	%>
	
	<c:if test="${fn:length(calEvents) == 0}">
		<li class="greybg3">
			<table width="100%">
				<tbody>
					<tr>
						<td class="centertd">
							<div class="center_center">
								<img
									src="<%=themeDisplay.getPathThemeImages()%>/calenderworksheet_icon.png">
							</div></td>
							
							

						<td class="centerdata2">
							<p class="centercenterpara1">
								<a href='#'>No Events Available</a>
							</p>						
						
					</tr>
				</tbody>
			</table></li>
	</c:if>


	<c:forEach var="event" items="${calEvents}">


		<li class="greybg3">
			<table width="100%">
				<tbody>
					<tr>
						<td class="centertd">
							<div class="center_center">
								<img
									src="<%=themeDisplay.getPathThemeImages()%>/calenderworksheet_icon.png">
							</div></td>
							
							<c:set var="startDate" value="${event.startDate}" />
							<c:set var="eventId" value="${event.eventId}" />
							

							<%
							String eventId = Long.toString((Long)(pageContext.getAttribute("eventId")));
							
							PortletURL employeeSearchUrl = PortletURLFactoryUtil.create(request,portletId,layout1.getPlid(),PortletRequest.RENDER_PHASE);
							
							employeeSearchUrl.setParameter("_8_struts_action","/calendar/view_event");
							employeeSearchUrl.setParameter("_8_eventId",eventId);
							employeeSearchUrl.setWindowState(WindowState.NORMAL);
							
							%>


						<td class="centerdata2">
							<p class="centercenterpara1">
								<a href='<%= employeeSearchUrl.toString()%>'>${event.title}</a>
							</p>
							<p class="centercenterpara_subtitle">${event.type}</p>
							<p class="centercenterpara_lecturername">Duration :
								${event.durationHour} hours - ${event.durationMinute} mins.</p></td>
						
						<%

                                   Date startDate = (Date) pageContext.getAttribute("startDate");
                                   String[] dateString = (DateUtil.getDateInstance(startDate)).split(",");
                                   String timeString = DateUtil.getTimeInstance(startDate);
                                   
                                   
                                   %>
						<td class="greybginnerright">
							<div>
								<p class="date"><%=dateString[0]%><br /><%=timeString%></p>

							</div></td>
					</tr>
				</tbody>
			</table></li>

	</c:forEach>
</ul>





