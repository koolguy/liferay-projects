<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<portlet:actionURL var="changePasswordURL">
	<portlet:param name="action" value="changePassword" />
</portlet:actionURL>

<form:form name="changePasswordForm" method="post"
	commandName="registrationModelBean" action="${changePasswordURL}">

	<li><label for="email"><span class="mandetory">*</span>
			Old Password</label> <form:password path="password" />
		<div class="error_message">
			<form:errors path="password" />
		</div></li>
	<li><label for="email"><span class="mandetory">*</span>
			New Password</label> <form:password path="password1" />
		<div class="error_message">
			<form:errors path="password1" />
		</div></li>
	<li><label for="email"><span class="mandetory">*</span>
			Confirm Password</label> <form:password path="password2" />
		<div class="error_message">
			<form:errors path="password2" />
		</div></li>
		
		<li><input tabindex="3" type="submit" value="Change" /><input tabindex="3" type="reset" value="Reset" /></li>

</form:form>