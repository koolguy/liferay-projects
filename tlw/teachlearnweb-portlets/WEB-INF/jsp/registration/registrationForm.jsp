<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
 
 
<portlet:defineObjects />
<liferay-theme:defineObjects />

<portlet:actionURL var="registerationURL">
	<portlet:param name="action" value="doRegistration" />
</portlet:actionURL>
 
 
 <script>
    $(function() {
     	$('#dob').datepick({dateFormat: 'd/M/yy'});
    });
</script>

 <style>
	.error_message {
		color: #C62E2E;
	}
	.form-register {
		background: #69C469;
		width: 302px;
		min-height: 148px;
		margin: 5px 0 0 0;
		position: absolute;
		z-index: 9999;
		right: 214px;
	}
	
	.registerclickup {
		background: url(<%=themeDisplay.getPathThemeRoot()%>/images/registerbg.png) no-repeat #69c469;
		position: relative;
		top: 0;
		left: -134px;
		height: 30px;
		width: 115px;
		cursor: pointer;
		margin-top: -30px;
	}
</style>

	      
       <!-- Registration Start -->
       <div class="registertop">
        <div class="registerclickup">
           <p>Register <span style="color:red;font-weight:bold;">FREE</span></p>
        </div>
        <div class="registerinner" id="registerInnerId">
           <form:form class="form-register" method="post" commandName="registrationModelBean" action="${registerationURL}">
           		<c:if test="${registrationModelBean.success == true}">
					
					<div class="success_message" style="padding: 5px;color: black;">
					<c:choose>
    					  <c:when test="${registrationModelBean.sendMessage == true}">
    					    <span>Registered successfully , Auto generated password for login is sent to your Mobile</span>
        				  </c:when>
    					  <c:otherwise>
							<span>Registered successfully , Please check your mail for auto	generated password for login</span>
    					  </c:otherwise>
					</c:choose>
					</div>
				</c:if>
							
				<div class="clearfix">
					<c:if test="${registrationModelBean.duplicateEmail == true}">
						<div class="error_message">
							<div style="margin: 12px 0 0px 18px;">* The email id is already registered.</div>
						</div>
					</c:if>
				</div> 	
           
                <table width="100%" class="registertable">
					<tbody>
                        <tr>
                            <td class="td1"><label for="firstName">First Name</label></td>
                            <td>
                            	<form:input path="firstName" class="enteremail"/>
								<div class="error_message">
									<form:errors path="firstName" />
								</div>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1"><label for="firstName">Last Name</label></td>
                            <td>
                            	<form:input path="lastName" class="enteremail"/>
								<div class="error_message">
									<form:errors path="lastName" />
								</div>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1"><label for="gender">Gender</label></td>
                            <td>
                            	<form:radiobutton path="gender" value="M" tabindex="7" style="width:10%" /><label>Male</label> 
                            	<form:radiobutton path="gender" value="F" style="width:10%" /><label>Female</label>
								<div class="error_message">
									<form:errors path="gender" />
								</div>
							</td>
                        </tr>
                        <%-- 
                        <tr>
                            <td class="td1"><label for="dob">Date of Birth</label></td>
                            <td>
                            	<form:input path="dob" id="dob" class="enteremail"/>
								<div class="error_message">
									<form:errors path="dob" />
								</div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td class="td1"><label for="email">Parent's Name</label></td>
                            <td>
                            	<form:input path="parentName" class="enteremail"/>
								<div class="error_message">
									<form:errors path="parentName" />
								</div>
                            </td>
                        </tr>
                         --%>
                        
                        <tr>
                            <td class="td1"><label for="board">Board</label></td>
                            <td>
                            	<form:select path="syllabus" id="syllabus" style="width: 146px;" class="syllabus" onchange="javascript:getClasses();">
									<form:option value="">Select</form:option>
									<c:forEach var="category" items="${getCategories}">
										<form:option value="${category.categoryId}">${category.name}</form:option>
									</c:forEach>
								</form:select>
								<div class="error_message">
								<form:errors path="syllabus" />
								</div>
                            </td>
                        </tr>
                        <tr>
                            <td class="td1"><label for="class">Class</label></td>
                            <td>
                            	<form:select path="className" id="className" style="width: 146px;" class="className" >
									<form:option value="">Select</form:option>
									<div id="selectclass"></div>
								</form:select>
								<div class="error_message">
									<form:errors path="className" />
								</div>
                            </td>
                        </tr> 
                        
                        <tr>
                            <td class="td1"><label for="email">Email</label></td>
                            <td>
                            	<form:input path="emailAddress" class="enteremail"/>
								<div class="error_message">
									<form:errors path="emailAddress" />
								</div>
                            </td>
                        </tr>
                         
                        <tr>
                            <td class="td1"><label for="mobile">Mobile</label></td>
                            <td>
                            	<form:input path="phone" class="enteremail"/>
								<div class="error_message">
									<form:errors path="phone" />
								</div>
                            </td>
                        </tr> 
                        <%--
                         <tr>
                            <td class="td1"><label for="role">You are a</label></td>
                            <td></td>
                        </tr>
                         --%>
                        <tr>
                            <td class="td1"></td>
                            <td></td>
                        </tr>
   					 </tbody>
				 </table>

				<div class="selectoptions">
					<table width="100%">
						<tbody>
							<tr>
								 <td><form:radiobutton path="jobTitle" value="Student" class="student"/>Student</td>
								 <td><form:radiobutton path="jobTitle" value="Teacher" class="teacher"/>Educator</td>
						 		<%-- <td><form:radiobutton path="jobTitle" value="Parent" class="parent"/>Parent</td>  --%>
							</tr>
							<tr>
								 <td>&nbsp;</td><td>&nbsp;</td><%--<td>&nbsp;</td>  --%>
							</tr>
						</tbody>
					</table>
				</div>
				<input type="submit" style="background:#fff; border:1px solid #fff;padding:6px;border-radius:15px; width:92px; -moz-border-radius:15px;-webkit-border-radius:15px; color:#000;font-family: 'lucida_sansregular';cursor:pointer;float:right; margin: 0 38px 10px 0;font-weight: normal;font-size: 13px;" value="Register"><br><br>
			</form:form>
      </div>
    </div>
	<!-- Registration End -->
	
	
	
<script>
 $(document).ready(function() {
	
	 getClasses();
	 
	});

	function getClasses(){
		var categoryId = document.getElementById('syllabus').value;
		var url = '<portlet:resourceURL id="getClasses"></portlet:resourceURL>'+'?one=one&categoryId='+categoryId;
		var xhr;
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
        	xhr=new XMLHttpRequest();
		}else {// code for IE6, IE5
		    xhr=new ActiveXObject("Microsoft.XMLHTTP");
		}		 
       
		xhr.open("GET", url, true);
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status==200) {
				var temp = xhr.responseText.split('###');
				$(".className").empty();
				var select = document.getElementById("className");
			 	for(var i=0;i<temp.length-1;i++){
			  		var opt = temp[i].split(':::');
			  		select.options[i+1] = new Option(opt[1],opt[0]);
			    }
			 }
		 }
		 xhr.send();
	}
</script>

<script type="text/javascript">
	function show(boxid) {
		document.getElementById(boxid).style.display = "block";
	}

	function hide(boxid) {
		document.getElementById(boxid).style.display = "none";
	}
</script>

<c:if test="${registrationModelBean.success == true}">
	<script type="text/javascript">
		show('registerInnerId');
	</script>
</c:if>

<c:if test="${registrationModelBean.showForm == true}">
	<script type="text/javascript">
		show('registerInnerId');
	</script>
</c:if>
	
