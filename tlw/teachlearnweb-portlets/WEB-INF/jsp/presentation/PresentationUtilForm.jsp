<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<portlet:actionURL var="presentationURL">
	<portlet:param name="action" value="doPresentation" />
</portlet:actionURL>

 <script>
/* $(document).ready(function() {
	$('#<portlet:namespace/>selectclassId').on('change', function(){		
		$.ajax({
		      type: "GET",
		      url: "${selectclassURL}",
		      data: {},
		      success: function(data) {
		    	  if (data != '-1') {
		    		  $('#showAssetsContent').html(data);
		    	  }
		      }
	   	});
	});	
});
 */
$(document).ready(function() {
	$('.dropdown').on('change', function(){		
		alert("test");
		$('#prId').submit();
		
	});	
});


</script>

<body>
	<form:form id="prId" name="prname" method="post"
		commandName="registrationModelBean" action="${presentationURL}">	
		
		<form:select id="<portlet:namespace/>syllabusId" class="dropdown" path="syllabusId"  >
			<form:option value="select">Select Syllabus</form:option>						
			<c:forEach var="bean" items="${getSyllabus}">
						<form:option value="${bean.categoryId}">${bean.name}</form:option>
			</c:forEach>
		</form:select>
		
		<form:select id="<portlet:namespace/>classId" class="dropdown" path="classId"  >
			<form:option value="select">Select Class</form:option>
				<c:forEach var="bean" items="${getclasses}">
						<form:option value="${bean.categoryId}">${bean.name}</form:option>
			</c:forEach>
		</form:select>
		
		<form:select id="<portlet:namespace/>topicId" class="dropdown" path="topicId" >
			<form:option value="select">Select Topic</form:option>
			<c:forEach var="bean" items="${gettopic}">
						<form:option value="${bean.categoryId}">${bean.name}</form:option>
			</c:forEach>			
		</form:select>
		
		<form:select id="<portlet:namespace/>subTopicId" path="subTopicId" class="dropdown" >
			<form:option value="">Select Sub Topic</form:option>
			<c:forEach var="bean" items="${getsubtopic}">
						<form:option value="${bean.categoryId}">${bean.name}</form:option>
			</c:forEach>
	    </form:select>				
	</form:form>	
</body>
