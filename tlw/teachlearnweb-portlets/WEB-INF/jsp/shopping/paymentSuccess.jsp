<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<section class="clearfix">
    	<div id="wrapper">
        	<div class="clearfix mt20">
            	<div class="innerLeft">
                <p class="head-green">Secure Payment </p>
					<!-- Payment steps -->
                                            <div class="payprocesssteps">
                                            	<ul class="payprosteplist clearfix">
                                                	<li>
                                                    	<span class="stepno">1</span>
                                                        <span class="stepname">Billing</span>
                                                    </li>
                                                    <li>
                                                    	<span class="stepno">2</span>
                                                        <span class="stepname">Shipping</span>
                                                    </li>
                                                    <li>
                                                    	<span class="stepno">3</span>
                                                        <span class="stepname">Payment</span>
                                                    </li>
                                                    <li class="active">
                                                    	<span class="stepno">4</span>
                                                        <span class="stepname">Confirmation</span>
                                                    </li>
                                                </ul>
                                            </div>
                                        	
                                            <!--Confirmation -->
                                            <div class="confirm">
                                            	<h4>You have successfully Completed your Shopping. Please <a href="/group/guest/dashboard">Click</a> here to got to Dash board</h4>
                                            </div>
                                           
            </div>

            
            </div>
            </div>
            </section>