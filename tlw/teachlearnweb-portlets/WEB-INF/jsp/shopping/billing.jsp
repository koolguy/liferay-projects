<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page import="com.liferay.portlet.asset.model.AssetCategory"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<liferay-theme:defineObjects />

<portlet:actionURL var="addShippingURL">
<portlet:param name="action" value="addShipping"/>
<portlet:param name="categoryId" value="CateGoRyId" />
	<portlet:param name="categoryName" value="CatName" />
</portlet:actionURL>

<portlet:defineObjects />
<portlet:resourceURL var="discountURL" id="getDiscount"/>
<portlet:resourceURL var="shippingURL" id="shipping"/>

<portlet:resourceURL var="paymentURL" id="payment"/>
<%
String pathToImages = themeDisplay.getPathThemeImages();
%>


<!-- Body Section -->    
    <section class="clearfix">
    	<div id="wrapper">
        	<div class="clearfix mt20">
            	<div class="innerLeft">
                <p class="head-green">Secure Payment </p>
            	
                <!-- Payment steps -->
                <div id="payprocesssteps">
                <div class="payprocesssteps">
                    <ul class="payprosteplist clearfix">
                        <li class="active">
                            <span class="stepno">1</span>
                            <span class="stepname">Billing</span>
                        </li>
                        <li>
                            <span class="stepno">2</span>
                            <span class="stepname">Shipping</span>
                        </li>
                        <li>
                            <span class="stepno">3</span>
                            <span class="stepname">Payment</span>
                        </li>
                        <li>
                            <span class="stepno">4</span>
                            <span class="stepname">Confirmation</span>
                        </li>
                    </ul>
                </div>
                <div class="shippingform">
                    <!--<div class="redeem">
                        <h3>Redeem Points</h3>
                        <%-- ${loggedInUser.rewardPoints} --%>
                        <select>
                          <option value="0">--Coming Soon--</option>
                         
                        </select>
                    </div>-->
                    <table class="discountcoupon">
                        <tr>
                            <td>Discount Coupon</td>
                            <td><input type="text" name="coupon" id="coupon" onblur="javascript:getDiscount();"></td>
                        </tr>
                    
                    </table>
                    
                    <table class="totalamount">
                        <tr><td class="td1" colspan="2"></td></tr>
                        <tr>
                            <td>Total Amount</td>
                            <td class="td2"><div id="amountdisplay">${totalCartAmount}</div>
                            	<input type="hidden" id="amount" name="amount" value="${totalCartAmount}"/>
                            </td>
                            
                        </tr>
                        <tr>
                            <td>Discount</td>
                            <td class="td2"><div class="discountdisplay"></div>
                            <input type="hidden" id="discount" name="discount" value=""/>
                            </td>
                        </tr>
                        <tr>
                            <td>Net Payable</td>
                            <td class="td2"><div class="totaldisplay">${totalCartAmount}</div>
                            <input type="hidden" id="totalamount" name="totalamount" value="${totalCartAmount}"/>
                            </td>
                        </tr>
                    </table>
                    <div class="shoptabs">
                    <a href="javascript:getShipping()" class="shippingshopping">Confirm</a>
                	</div>
                </div>
                </div>
            </div>
            	<div class="innerRight">
                	<%--<div class="searchbtn">
                            <input type="search" name="search" value="Cross Polination and its causes"><a href="searchresults.html"><img src="images/searchicon.png"></a>
                         </div> --%>
                         
                         <h3>Summary</h3>
						<ul class="shopsummarylist">
                                            	<li>
                                                <table class="shopsummarytable">
                                                   <c:forEach items="${cartItems}" var="cartItemMap">
                               						 <c:set var="categoryId" value="${cartItemMap.key}"/>
                               						 <c:set var="cartItem" value="${cartItemMap.value}"/>
                                					 <c:set var="category" value="${cartItem.category}"/>
                                                    <tr>
                                                        <td class="td1"><img src="<%=pathToImages%>/sbook1.jpg"></td>
                                                        <td class="td2">
                                                        	<p class="sumfor">${category.name}</p>
                                                            <p class="sumitemanem">${category.description}</p>
                                                            <%--<p class="sumby">by Rashmi Bansalr</p> --%>
                                                        </td>
                                                        <td class="td3">Rs ${cartItem.price}</td>
                                                    </tr>
                                                  </c:forEach>
                                                </table>
                                                </li>
                                                <li>
                                                	<table class="totallist">
                                                        <tr>
                                                            <td>Total Amount</td>
                                                            <td class="td2">${totalCartAmount}</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Discount</td>
                                                            <td class="td2"><div class="discountdisplay"></div></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Net Payable</td>
                                                            <td class="td2"><div class="totaldisplay">${totalCartAmount}</div></td>
                                                        </tr>
                                                    </table>
                                                </li>
                                                
                        </ul>
                   
               	</div>
            </div>
      </div>
    </section>
<script>
function getDiscount() {
	var discountUrl =  '${discountURL}';
	var coupon = $('#coupon').val();
	discountUrl = discountUrl+"&coupon="+coupon;
	   $.ajax({
	      type: "GET",
	      url: discountUrl,      
	      success: function(data) {
	    	  var discount= data;
	    	  var amount = $('#amount').val();
	    	  var totalAmount = $('#totalamount').val();
	    	  totalAmount = amount - discount;
	    	  		//$('#amountdisplay').html(amount);
	    	  		$('.discountdisplay').html("-"+discount);
	    	  		$('.totaldisplay').html(totalAmount);
				}
	   });
	}
function getShipping(){
	$.ajax({
	      type: "GET",
	      url: '${shippingURL}',      
	      success: function(data) {
	$('#payprocesssteps').html(data)
	}
	 });
}
function submitShipping(){
	
	//$('#shippingformdata').submit();
	var url = '${addShippingURL}';
	url = url.replace('CateGoRyId','${assetCategory.categoryId}');
	//url.replace('CatName','${assetCategory.name}');
	 window.location.href=url;

}
</script>