<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page import="com.liferay.portlet.asset.model.AssetCategory"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<liferay-theme:defineObjects />
<portlet:defineObjects/>

					<!-- Payment steps -->
                <div id="payprocesssteps" class="payprocesssteps">
                    <ul class="payprosteplist clearfix">
                        <li>
                            <span class="stepno">1</span>
                            <span class="stepname">Billing</span>
                        </li>
                        <li class="active">
                            <span class="stepno">2</span>
                            <span class="stepname">Shipping</span>
                        </li>
                        <li>
                            <span class="stepno">3</span>
                            <span class="stepname">Payment</span>
                        </li>
                        <li>
                            <span class="stepno">4</span>
                            <span class="stepname">Confirmation</span>
                        </li>
                    </ul>
                </div>
                 <form action="${addShippingURL}" id="shippingformdata">
                    <table class="shippingtable">
                                                    <tbody>
                                                        <tr>
                                                            <td class="td1">First Name <span class="red">*</span></td>
                                                            <td><input type="text" name="name" placeholder="your first name"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td1">Last Name <span class="red">*</span></td>
                                                            <td><input type="text" name="name" placeholder="your last name"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td1">Address <span class="red">*</span></td>
                                                            <td><textarea placeholder="your address"></textarea></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td1">Landmark <span class="red">*</span></td>
                                                            <td><input type="text" name="name" placeholder="nearst place"></td>
                                                        </tr>
                                                         <tr>
                                                            <td class="td1">Pin Code <span class="red">*</span></td>
                                                            <td><input type="text" name="name" placeholder="pin number"></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td1">City <span class="red">*</span></td>
                                                            <td><input type="text" name="name" placeholder="hyderabad etc"></td>
                                                        </tr>
                                                        <tr>
                                                         	<td class="td1">State <span class="red">*</span></td>

                                                            <td><select class="selectstate">
                                                                  <option value="Andhra Pradesh">Andhra Pradesh</option>
                                                                  <option value=""></option>
                                                                  <option value=""></option>
                                                                  <option value=""></option>
                                                             </select> </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td1">Country <span class="red">*</span></td>
                                                            <td class="tdcountry">India <span>(we only ship in India)</span> </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="td1">Mobile <span class="red">*</span></td>
                                                            <td>0- <input type="number" name="num" placeholder="number" class="mobilenum"></td>
                                                        </tr>
                                                    </tbody>
                                                    </table>
													
                                                <div class="shoptabs">
                                               
                                            	 <a href="javascript:submitShipping();" class="shippingshopping">Confirm</a> 
                                            	</div>
                                            	</form>
