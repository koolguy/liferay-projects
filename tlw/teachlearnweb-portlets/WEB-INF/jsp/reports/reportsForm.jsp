<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Student Reports</title>
</head>

<portlet:actionURL var="reportsURL">
	<portlet:param name="action" value="doReports" />
</portlet:actionURL>
<body>	
	<form:form name="reportsForm" method="post"
		commandName="reportsModelBean" action="${reportsURL}">
		
		<table border="1" style="background-color:lightgreen;" >
			<tr>
				<td>Name:</td>
				<td>James</td>
			</tr>
		</table>
		
		<table border = "1" style="background-color:lightblue;" >
			<tr>
				<th>S No</th>
				<th>Skill</th>
				<th>Answered Right</th>
				<th>Answered Wrong</th>
				<th>Graph</th>
				<th>E/G/A/W</th>
			</tr>
			<tr>
				<td align="center">1</td>
				<td align="center">Fundamental Concept</td>
				<td align="center">16</td>
				<td align="center">4</td>
				<td align="center"></td>
				<td align="center">W</td>
			</tr>
			<tr>
				<td align="center">2</td>
				<td align="center">Application Concept</td>
				<td align="center">16</td>
				<td align="center">4</td>
				<td align="center"></td>
				<td align="center">E</td>
			</tr>
			<tr>
				<td align="center">3</td>
				<td align="center">Modulation Concept</td>
				<td align="center">16</td>
				<td align="center">4</td>
				<td align="center"></td>
				<td align="center">A</td>
			</tr>
		</table>
	</form:form>
</body>
</html>