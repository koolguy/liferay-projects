<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<portlet:resourceURL var="saveAssessmentURL" id="submitAssessment" />
<portlet:resourceURL var="fetchQuestionURL" id="fetchQuestion" />
<portlet:resourceURL id="updateAnswer" var="updateAnswerURL"/>
<portlet:resourceURL var="testURL" id="startAssessment" />
<portlet:resourceURL var="timeURL" id="timeRemaining"/>
<portlet:resourceURL var="reportURL" id="openReport" />
<portlet:resourceURL var="printReportURL" id="printReport" />

<script>
function displayHTML(data) {
   $("#popupRoot").html(data);
   $(".lightboxopen").trigger('ShowAssessment');
}
function displayReport(data) {
	   $("#reportRoot").html(data);
	   $(".reportlightboxopen").trigger('ShowReport');
}

function <portlet:namespace/>printReport(serverURL) {
	var reportWin = window.open(serverURL, "Report", "location=0,menubar=0,resizable=0,status=0,toolbar=0,width=800,height=600");
	event.preventDefault();
}

function <portlet:namespace/>showReport(assessmentId, testId) {
   $.ajax({
      type: "POST",
      url: "${reportURL}",
      data: {
    	  	<portlet:namespace/>assessmentId: assessmentId,
    	  	<portlet:namespace/>testId: testId
    	  	},
      success: function(data) {
    	  		displayReport(data);
			}
   });
}

function <portlet:namespace/>startAssessment(assessmentId) {
   $.ajax({
      type: "POST",
      url: "${testURL}",
      data: {<portlet:namespace/>assessmentId: assessmentId },
      success: function(data) {
    	  if (data == null && data.indexOf('*No assessment found*') > 0) {
    		  alert('Sorry, no test found');
    	  } else {
	    	  displayHTML(data);
	    	  intervalId = self.setInterval(<portlet:namespace />getTimeRemaining, 1000);
	    	  $('a.lightbox_close').click(function(){
	    		  <portlet:namespace/>clearTimer();
	    	  });
    	  }
      	}
   });
}
function <portlet:namespace/>clearTimer() {
	if (intervalId != null) {
		window.clearInterval(intervalId);
	}
}
function <portlet:namespace/>getTimeRemaining() {
	$.ajax({
	      type: "GET",
	      url: "${timeURL}",
	      data: {},
	      success: function(data) {
	    	  if(data != null && data.indexOf('-1') < 0) {
	    		  $("#<portlet:namespace/>timeRemaining").html(data);
	    	  } else {
	    		  <portlet:namespace/>clearTimer();
	    		  alert('Time up');
	    		  // submit the assessment
	    		  $('.finishtest').trigger('click');
	    	  }
		  }
	});
}
/* submit assessment */
function <portlet:namespace/>submitAssessment(assessmentId, testId) {
	var reportHTML = "<p class='viewreports'><a rel='performance' class='reportlightboxopen' "
			+ " onclick=\"<portlet:namespace/>showReport('" + assessmentId + "', '" + testId + "');\">Click to View Report</a></p>";
	var saveAssessmentURL = '${saveAssessmentURL}';
	$.ajax({
	      type: "POST",
	      url: "${saveAssessmentURL}",
	      data: {},
	      success: function(data) {
	    	  if(data != '-1') {
	    		  <portlet:namespace/>clearTimer();
	    		  alert('Finished successfully');
	    		  // change link from start test to show report
	    		  $('#<portlet:namespace/>' + assessmentId).html(reportHTML);
	    		  <portlet:namespace/>cancelAssessment();
	    	  }
		  }
	});
}

function <portlet:namespace/>cancelAssessment() {
	<portlet:namespace/>clearTimer();
	$('a.lightbox_close').trigger('click');
}

function <portlet:namespace/>closeReport() {
	$('a.lightbox_close').trigger('click');
}

function <portlet:namespace/>fetchQuestion(qId) {
	$.ajax({
	      type: "POST",
	      url: "${fetchQuestionURL}",
	      data: {<portlet:namespace/>questionNum: qId},
	      success: function(data) {
	    	  displayHTML(data);
	      	}
   });
}

function <portlet:namespace/>updateAnswer(selectedAnswerId) {
	$.ajax({
	      type: "POST",
	      url: "${updateAnswerURL}",
	      data: {<portlet:namespace/>answerId: selectedAnswerId},
	      success: function(data) {
				if (data == "-1") {
					alert("Error saving the answer");
				}
	      	}
   });
}
var intervalId = null;

</script>