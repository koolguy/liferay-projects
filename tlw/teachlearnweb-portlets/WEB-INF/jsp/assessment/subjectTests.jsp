<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<portlet:defineObjects/>
<liferay-theme:defineObjects/>

<portlet:resourceURL var="newAssessmentURL" id="newAssessment" />
<portlet:resourceURL var="saveAssessmentURL" id="submitAssessment" />
<portlet:resourceURL var="fetchQuestionURL" id="fetchQuestion" />
<portlet:resourceURL id="updateAnswer" var="updateAnswerURL"/>
<portlet:resourceURL var="testURL" id="startAssessment" />
<portlet:resourceURL var="timeURL" id="timeRemaining"/>
<portlet:resourceURL var="reportURL" id="openReport" />
<portlet:resourceURL var="printReportURL" id="printReport" />

<script>
function displayHTML(data) {
   $("#<portlet:namespace/>popupRoot").html(data);
   $(".lightboxopen").trigger('ShowAssessment');
}
function displayReport(data) {
	   $("#<portlet:namespace/>reportRoot").html(data);
	   $(".reportlightboxopen").trigger('ShowReport');
}

function <portlet:namespace/>printReport(serverURL) {
	var reportWin = window.open(serverURL, "Report", "location=0,menubar=0,resizable=0,status=0,toolbar=0,width=800,height=600");
	event.preventDefault();
}

function <portlet:namespace/>showReport(assessmentId, testId) {
   $.ajax({
      type: "POST",
      url: "${reportURL}",
      data: {
	  	  	<portlet:namespace/>assessmentId: assessmentId,
		  	<portlet:namespace/>testId: testId
		  	},
      success: function(data) {
    	  		displayReport(data);
			}
   });
}
var newTestSourceId = null;
function <portlet:namespace/>newAssessment(subjectId, levelId) {
	   $.ajax({
	      type: "POST",
	      url: "${newAssessmentURL}",
	      data: {
	    	  <portlet:namespace/>subjectId: subjectId,
	    	  <portlet:namespace/>levelId: levelId
	    	  },
	      success: function(data) {
	    	  if (data != null && data.indexOf('*Cannot create assessment*') > -1) {
	    		  alert('Sorry, cannot start test.');
	    	  } else {
					var sourceId = "<portlet:namespace/>" + subjectId + "_" + levelId;
	    		   /* var divId = "<portlet:namespace/>" + data; */
				   var assessmentHTML = "<a rel='starttestnow' class='lightboxopen' " + 
								"onclick=\"<portlet:namespace/>startAssessment('" + data + "', '" + sourceId + "');\">" +
								"<span class='ovstdownload'>Resume</span></a>";
					newTestSourceId = sourceId;
			    	$('#' + sourceId).html(assessmentHTML);
			    	/* $('#' + sourceId).attr("id", divId); */
			    	<portlet:namespace/>startAssessment(data);
		    	  }
	      }
   });
}

function <portlet:namespace/>startAssessment(assessmentId, newSourceId) {
	if (newSourceId != null) {
		newTestSourceId = newSourceId;
	} 
   $.ajax({
      type: "POST",
      url: "${testURL}",
      data: {<portlet:namespace/>assessmentId: assessmentId },
      success: function(data) {
    	  if (data == null || data.indexOf('*No assessment found*') > -1) {
    		  alert('Sorry, no test found');
    	  } else {
	    	  displayHTML(data);
	    	  intervalId = self.setInterval(<portlet:namespace />getTimeRemaining, 1000);
	    	  $('a.lightbox_close').click(function(){
	    		  <portlet:namespace/>clearTimer();
	    	  });
    	  }
      	}
   });
}
function <portlet:namespace/>clearTimer() {
	if (intervalId != null) {
		window.clearInterval(intervalId);
	}
}
function <portlet:namespace/>getTimeRemaining() {
	$.ajax({
	      type: "GET",
	      url: "${timeURL}",
	      data: {},
	      success: function(data) {
	    	  if(data != null && data.indexOf('-1') < 0) {
	    		  $("#<portlet:namespace/>timeRemaining").html(data);
	    	  } else {
	    		  <portlet:namespace/>clearTimer();
	    		  alert('Time up');
	    		  // submit the assessment
	    		  $('.finishtest').trigger('click');
	    	  }
		  }
	});
}
/* submit assessment */
function <portlet:namespace/>submitAssessment(assessmentId, testId) {
	var reportHTML = "<a rel='performance' class='reportlightboxopen' "
				+ " onclick=\"<portlet:namespace/>showReport('" + assessmentId + "', '" + testId + "');\">"
				+ "<span class='ovstdownload'>Completed</span></a>";
	var saveAssessmentURL = '${saveAssessmentURL}';
	$.ajax({
	      type: "POST",
	      url: "${saveAssessmentURL}",
	      data: {},
	      success: function(data) {
	    	  if(data != '-1') {
	    		  <portlet:namespace/>clearTimer();
	    		  alert('Finished successfully');
	    		  // change link from start test to show report
	    		  if (newTestSourceId != null) {
	    			  $('#' + newTestSourceId).html(reportHTML);
	    			  newTestSourceId = null;
	    		  } else {
		    		  $('#<portlet:namespace/>' + assessmentId).html(reportHTML);
	    		  }
	    		  <portlet:namespace/>cancelAssessment();
	    	  }
		  }
	});
}

function <portlet:namespace/>cancelAssessment() {
	<portlet:namespace/>clearTimer();
	newTestSourceId = null;
	$('a.lightbox_close').trigger('click');
}

function <portlet:namespace/>closeReport() {
	$('a.lightbox_close').trigger('click');
}

function <portlet:namespace/>fetchQuestion(qId) {
	$.ajax({
	      type: "POST",
	      url: "${fetchQuestionURL}",
	      data: {<portlet:namespace/>questionNum: qId},
	      success: function(data) {
	    	  displayHTML(data);
	      	}
   });
}

function <portlet:namespace/>updateAnswer(selectedAnswerId) {
	$.ajax({
	      type: "POST",
	      url: "${updateAnswerURL}",
	      data: {<portlet:namespace/>answerId: selectedAnswerId},
	      success: function(data) {
				if (data == "-1") {
					alert("Error saving the answer");
				}
	      	}
   });
}
var intervalId = null;

</script>
<div class="maincontent scroll">
	<c:if test="${empty subjectTests}">
		<div>No tests found ${(not empty selectedSubject)? 'for' : ''} ${selectedSubject.subjectName}</div>
	</c:if>
	<ul>
		<c:forEach items="${subjectTests}" var="subjectTest">
			<li class="resbox">
				<div class="ovinfobox ovsttatebg">
					<a href="#">
						<p class="modeltestpapernumber">${selectedSubject.subjectName} Test</p>
						<div class="outerdiv">
							<div class="ovstthumb">
								<img src="<%=themeDisplay.getPathThemeImages()%>/sstest_icon.png">
							</div>
							<div class="ovstreport">
								<p class="ovnofq">
									${subjectTest.difficultyLevel.description} <br>MODE
								</p>
							</div>
						</div>
					</a>
				</div>
				
				    
				
				<c:choose>
				
					<c:when test="${empty subjectTest.assessment}" >
					
					
					<div class="resbottom ovsttatetabbg outerdiv" id="<portlet:namespace/>${selectedSubject.subjectId}_${subjectTest.difficultyLevel.levelId}">
						<a rel="starttestnow" class="lightboxopen" onclick="<portlet:namespace/>newAssessment('${selectedSubject.subjectId}', '${subjectTest.difficultyLevel.levelId}');">
							<span class="ovstdownload">Start a test</span>
						</a>
					</div>
					</c:when>
					<c:when test="${fn:toLowerCase(subjectTest.assessment.status) eq 'saved'}">
					<div class="resbottom ovsttatetabbg outerdiv" id="<portlet:namespace/>${subjectTest.assessment.assessmentId}">
						<a rel="starttestnow" class="lightboxopen" 
									onclick="<portlet:namespace/>startAssessment('${subjectTest.assessment.assessmentId}');">
							<span class="ovstdownload">Resume</span>
						</a>
					</div>
					</c:when>
					<c:when test="${fn:toLowerCase(subjectTest.assessment.status) eq 'assigned'}" >
					<div class="resbottom ovsttatetabbg outerdiv" id="<portlet:namespace/>${subjectTest.assessment.assessmentId}">
						<a rel="starttestnow" class="lightboxopen" 
									onclick="<portlet:namespace/>startAssessment('${subjectTest.assessment.assessmentId}');">
							<span class="ovstdownload">Resume</span>
						</a>
					</div>
					</c:when>
					<c:when test="${fn:toLowerCase(subjectTest.assessment.status) eq 'completed'}" >
					
					
					<div class="resbottom ovsttatetabbg outerdiv" id="<portlet:namespace/>${subjectTest.assessment.assessmentId}">
						<a rel="performance" class="reportlightboxopen" 
							onclick="<portlet:namespace/>showReport('${subjectTest.assessment.assessmentId}', '${subjectTest.assessment.DTTest.testId}');">
							<span class="ovstdownload">Completed</span>
						</a>
					</div>
					</c:when>
				</c:choose>
				
			</li>
		</c:forEach>
	</ul>
</div>

<!-- include the lightbox content inside below div for the selected assessment -->
<div id="<portlet:namespace/>popupRoot"></div>

<!-- include the lightbox content inside below div for the selected report -->
<div id="<portlet:namespace/>reportRoot"></div>
