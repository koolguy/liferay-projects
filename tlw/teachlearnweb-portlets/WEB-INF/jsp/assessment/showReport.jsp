<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<portlet:defineObjects/>
<liferay-theme:defineObjects/>

<script type="text/javascript" src="<%=themeDisplay.getPathThemeJavaScript()%>/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="<%=themeDisplay.getPathThemeJavaScript()%>/plugins/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="<%=themeDisplay.getPathThemeJavaScript()%>/plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="<%=themeDisplay.getPathThemeJavaScript()%>/plugins/jqplot.pointLabels.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=themeDisplay.getPathThemeCss()%>/jquery.jqplot.min.css" />

<portlet:renderURL var="printReportURL" windowState="<%=javax.portlet.WindowState.MAXIMIZED.toString() %>">
	<portlet:param name="render" value="printReport"></portlet:param>
	<portlet:param name="assessmentId" value="${assessmentId}"></portlet:param>
	<portlet:param name="testId" value="${testId}"></portlet:param>
</portlet:renderURL>

<!-- popup Box for report -->
<div id="performance" class="lightbox">
	<div class="outerdiv setlightboxheight">
		<div class="lightboxwrapper">
			<div class="lightboxcontanerright">
				<div class="lbcheightfix">
					<div class="topbarbg">
						<div class="topbgleft">
							<h3>${subjectName}</h3>
							<!-- <h4>Diagnostic Test</h4> -->
						</div>
						<div class="topbgright">
							<!-- <h5>11 May 2012</h5> -->
						</div>
					</div>
					<div class="centerboxlb">
						<h2>Performance</h2>
						<div id="fullgraph" style="width:90%; height:80%;margin-left:30px;"></div>
						<!-- <div class="downcontent">
							<h4>Diagnosis</h4>
							<p>
								You seem to be more a READ-WRITE learner that indicates your
								interest to learn and explore things through reading and writing
								methods. However, for better retention of concepts in Math and
								Science learning styles like VISUAL, AURAL and KINESTHETIC play
								an important role.<br> Therefore, we recommend that you
								focus on other styles that will help you master subjects in any
								field. Please find the report attached to give you a better
								understanding of this analysis
							</p>
						</div> -->
					</div>
				</div>
			</div>
		</div>
		<div class="lightboxright">
			<div class="lightboxrightside">
				<a href="#" class="fullreportprint" onclick="<portlet:namespace/>printReport('${printReportURL}');"><img
					src="<%=themeDisplay.getPathThemeImages()%>/printicon.png">Print Report</a>
				<a href="JavaScript:void(0);"
					class="fullreportclose" onclick="<portlet:namespace/>closeReport();">Close</a>
				<div class="rewardspointspur">
					<p>
						You can easily look at<br> your weak skills and <br>
						improve according <br> to this
					</p>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
$(document).ready(function(){
    var percents = ${percents};
    // Can specify a custom tick Array.
    // Ticks should match up one for each y value (category) in the series.
    var ticks = ${skillNames};

    var plot1 = $.jqplot('fullgraph', [percents], {
        // The "seriesDefaults" option is an options object that will
        // be applied to all series in the chart.
        // rendererOptions: {fillToZero: true}
    	fontSize: '5px',
    	animate: !$.jqplot.use_excanvas,
        seriesDefaults:{
            renderer:$.jqplot.BarRenderer,
            pointLabels: { show: true, edgeTolerance: -15 },
            rendererOptions: {fillToZero: true, barWidth: 40}
        },
        axesDefaults: {
        	min: 0
        },
        axes: {
            // Use a category axis on the x axis and use our custom ticks.
            xaxis: {
                renderer: $.jqplot.CategoryAxisRenderer,
                ticks: ticks
            },
            // Pad the y axis just a little so bars can get close to, but
            // not touch, the grid boundaries.  1.2 is the default padding.
            yaxis: {
            	max: 100,
                pad: 1.05,
                tickOptions: {formatString: '%d%%'}
            }
        },
        highlighter: { show: false }
    });
});
</script>
