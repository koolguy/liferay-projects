<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<portlet:defineObjects/>
<liferay-theme:defineObjects/>

<%@include file="./common.jsp" %>

<section id="maincontent"> <!-- Content Section -->
<div class="contentwrap">
	<!-- Top -->
	<div class="outerdiv pagetitleandsocial">
		<div class="pagetitle">${topicName}Topic to be displayed here: InlineTest</div>
		<div class="topsocial">
			<ul class="outerdiv">
				<li><a href="#"> <span class="tsthumb facebook"></span> <span
						class="tsthumbname">Facebook</span> </a></li>
				<li><a href="#"> <span class="tsthumb twitter"></span> <span
						class="tsthumbname">Twitter</span> </a></li>
				<li><a href="#"> <span class="tsthumb gplus"></span> <span
						class="tsthumbname">Google Plus</span> </a></li>
			</ul>
		</div>
	</div>
	<div class="contentbg  scroll">
		<div class="maincontent ">
			<ul>
			<c:forEach items="${inlineTests}" var="inlineTest">
				<li class="inlineteststausbox">
					<div class="inlinetestboxtop">
						<a href="JavaScript:void(0);">
							<p class="inlinenumber">${inlineTest.levelDesc} Test</p>
							<div class="outerdiv">
								<div class="inlinethumb">
									<img src="<%=themeDisplay.getPathThemeImages()%>/inlinetesticon.png">
								</div>
								<div class="inlinetestreport">
									<p class="itnofq">${inlineTest.questionsCount} Questions</p>
									<p class="itnoft">${inlineTest.timeInMinutes} Minutes</p>
									<p class="itnoft">${inlineTest.rewardPoints} Reward Points</p>
									<p class="itstatus">Not Started</p>
								</div>
							</div> </a>
					</div>
					<div class="inlinetestbottom outerdiv">
						<div class="itschedul">
							<a rel="starttestnow" class="lightboxopen" onclick="startInlineTest('topicId', '${inlineTest.levelId}');">Scheduled</a>
						</div>
						<div class="itstime">
							<span>07 DEC</span> <span>11.30 pm</span>
						</div>
					</div>
				</li>
			</c:forEach>
			</ul>
		</div>
	</div>
</div>
</section>

<!-- include the lightbox content inside below div for the selected assessment -->
<div id="<portlet:namespace/>popupRoot"></div>
