<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<portlet:defineObjects/>
<liferay-theme:defineObjects/>

<script type="text/javascript" src="<%=themeDisplay.getPathThemeJavaScript()%>/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="<%=themeDisplay.getPathThemeJavaScript()%>/plugins/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="<%=themeDisplay.getPathThemeJavaScript()%>/plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="<%=themeDisplay.getPathThemeJavaScript()%>/plugins/jqplot.pointLabels.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=themeDisplay.getPathThemeCss()%>/jquery.jqplot.min.css" />

<!-- popup Box for report -->
<div class="centerboxlb">
	<h2>Performance</h2>
	<div id="fullgraph" style="width: 90%; height:100%;margin-left:20px;"></div>
</div>
<script>
$(document).ready(function(){
    var percents = ${percents};
    // Can specify a custom tick Array.
    // Ticks should match up one for each y value (category) in the series.
    var ticks = ${skillNames};

    var plot1 = $.jqplot('fullgraph', [percents], {
        // The "seriesDefaults" option is an options object that will
        // be applied to all series in the chart.
        // rendererOptions: {fillToZero: true}
    	fontSize: '5px',
    	animate: !$.jqplot.use_excanvas,
        seriesDefaults:{
            renderer:$.jqplot.BarRenderer,
            pointLabels: { show: true, edgeTolerance: -15 },
            rendererOptions: {fillToZero: true, barWidth: 40}
        },
        axesDefaults: {
        	min: 0
        },
        axes: {
            // Use a category axis on the x axis and use our custom ticks.
            xaxis: {
                renderer: $.jqplot.CategoryAxisRenderer,
                ticks: ticks
            },
            // Pad the y axis just a little so bars can get close to, but
            // not touch, the grid boundaries.  1.2 is the default padding.
            yaxis: {
            	max: 100,
                pad: 1.05,
                tickOptions: {formatString: '%d%%'}
            }
        },
        highlighter: { show: false }
    });

    // print window
    $('#footer').hide();
    $(document.body).ready(function() {
    	print_win();
    });
});
function chkstate() {
	if(document.readyState=="complete") {
		window.close()
	} else {
		setTimeout("chkstate()", 2000);
	}
}
function print_win() {
	window.print();
	chkstate();
}
</script>
