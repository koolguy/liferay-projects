<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<portlet:defineObjects/>
<liferay-theme:defineObjects/>
<script type="text/javascript">
$(document).ready(function() {
	$('.contentbg').css('overflow', 'auto');
});
}
</script>
<%@include file="./common.jsp" %>

<div id="sitewidth" style="width:100%">
<div id="offcanvasmenu" style="width: 1782px; left: 0px;">
<!--offcanvas menu topbar-->
   <%@ include file="../common/dashboard.jsp" %>
</div>
<div class="scrollsection">
<section id="maincontent">
<div class="wraper topbg outerdiv">
	<!-- Right Section -->
	<!-- <div class="rightwrap">
		<div class="rSubTitle testreporttitle">
			<p class="rstSubject">Tests</p>
		</div>
	</div> -->

	<!-- Left Section -->
	<div class="leftwrap">
		<div class="offcantab">
			<a href="#" class="Offcanmenu"></a>
			<!-- <span class="tlnotification">10</span> -->
		</div>
	</div>

	<!-- Content Section -->
	<div class="contentwrap" >
		<div class="infowrap" >
			<div class="Maintitle">Diagnostic Test</div>
		</div>
	</div>
</div>
<div class="wraper outerdiv">
	<!-- Right Section -->
	<!-- <div class="rightwrap">
		<div class="rightmenusection testreportrbg">
			<div id="rightmainmenu">
				<ul class="testreportmenu">
					<li><a href="#" class="active">Diagnostic Test 1</a></li>
					<li><a href="#">Diagnostic Test 2</a></li>
					<li><a href="#">Diagnostic Test 3</a></li>
					<li><a href="#">Diagnostic Test 4</a></li>
					<li><a href="cumulativereport.html">Cumulative Report</a></li>
				</ul>
			</div>
		</div>
	</div> -->
	<!-- Content Section -->
	<div class="contentwrap" style="background:#fff">
		<div class="infowrap">
			<!-- Top -->
			<div class="outerdiv pagetitleandsocial">
				<h2>Diagnostic Test</h2>
				<img src="<%=themeDisplay.getPathThemeImages()%>/logo.png" alt="Logo" style="margin: -55px 30px 0 10px;float: right;" class="canvaslogo" style="float:right" id="aui_3_4_0_1_1131">
			</div>
			<div class="contentbg socialcontentbg"> <!-- scroll class removed -->
				<div id="testcontent">
					<div class="outerdiv">
					<c:if test="${fn:length(assessments) <= 0}">
						<div class="testLearingstyle">
							<div class="outerdiv m20">
								<div class="outerdiv">
									<!-- Title And Status -->
									<div class="drcontentwrapper">
										<h2>No tests found</h2>
									</div>
								</div>
							</div>
						</div>
					</c:if>
					<c:forEach items="${assessments}" var="assessment">
						<div class="testLearingstyle">
							<div class="outerdiv m20">
								<div class="outerdiv">
									<!-- Title And Status -->
									<div class="drcontentwrapper">
										<h2>${assessment.subjectMaster.subjectName}</h2>
										<p>${assessment.questionCount} Questions , ${assessment.minutePerQuestion} Minutes</p>
									</div>
								</div>
								<!-- Chart Status -->
								<div class="chartbox" id="<portlet:namespace/>${assessment.assessmentId}">
									<c:if test="${assessment.status ne 'Completed'}">
										<img src="<%=themeDisplay.getPathThemeImages()%>/notstartedyet.png">
										<p class="viewreports">
											<a rel="starttestnow" class="lightboxopen" 
												onclick="<portlet:namespace/>startAssessment('${assessment.assessmentId}');">
												Start Test Now
											</a>
										</p>
									</c:if>
									<c:if test="${assessment.status eq 'Completed'}">
										<!-- <img src="images/scienceskillgraph.png"> -->
                                        <p class="viewreports">
                                        	<a rel="performance" class="reportlightboxopen"
                                        		onclick="<portlet:namespace/>showReport('${assessment.assessmentId}', '${assessment.DTTest.testId}');">
                                        		Click to View Report
                                        	</a>
                                        </p>
									</c:if>
								</div>
							</div>
						</div>
					</c:forEach>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</section>
</div>
</div>
<!-- include the lightbox content inside below div for the selected assessment -->
<div id="popupRoot"></div>

<!-- include the lightbox content inside below div for the selected report -->
<div id="reportRoot"></div>
