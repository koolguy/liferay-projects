<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page contentType="text/html" isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme" %>

<portlet:defineObjects/>
<liferay-theme:defineObjects/>

<script type="text/javascript" src="<%=themeDisplay.getPathThemeJavaScript()%>/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="<%=themeDisplay.getPathThemeJavaScript()%>/plugins/jqplot.pieRenderer.min.js"></script>
<script type="text/javascript" src="<%=themeDisplay.getPathThemeJavaScript()%>/plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="<%=themeDisplay.getPathThemeJavaScript()%>/plugins/jqplot.pointLabels.min.js"></script>
<link rel="stylesheet" type="text/css" href="<%=themeDisplay.getPathThemeCss()%>/jquery.jqplot.min.css" />

<!-- popup Box for report -->
<div class="centerboxlb">
	<div id="fullpigraph" style="width:90%; height:100%; margin-left:20px;"></div>
	    <table class="mylearningtable">
	    	<!-- <tr>
	        	<td class="td1">No.</td>
	            <td>Style</td>
	            <td class="td3">No.of Questions Answered</td>
	        </tr> -->
	     <c:forEach items="${reportMap}" var="reportEntry" varStatus="itr">
	         <tr>
	         	<td>${itr.count}</td>
	             <td>${reportEntry.key}</td>
	             <td>${reportEntry.value}</td>
	         </tr>
	     </c:forEach>
		<c:if test="${empty isDiagnosticStyleReport}">
			<tr>
				<td align="right" colspan="2" >Total Questions</td>
				<td>${questionCount}</td>
			</tr>
		</c:if>
	    </table>
	<!-- <div class="downcontent">
		<h4>Diagnosis</h4>
		<p>
			You seem to be more a READ-WRITE learner that indicates your
			interest to learn and explore things through reading and writing
			methods. However, for better retention of concepts in Math and
			Science learning styles like VISUAL, AURAL and KINESTHETIC play
			an important role.<br> Therefore, we recommend that you
			focus on other styles that will help you master subjects in any
			field. Please find the report attached to give you a better
			understanding of this analysis
		</p>
	</div> -->
</div>
<script>
$(document).ready(function(){
    //var reportDataArray = ${reportDataArray};
    jQuery.jqplot.config.enablePlugins = true;
    var plot1 = jQuery.jqplot('fullpigraph', [${reportDataArray}], {
        // The "seriesDefaults" option is an options object that will
        // be applied to all series in the chart.
        seriesDefaults:{
        	shadow: false,
            renderer: jQuery.jqplot.PieRenderer, 
            rendererOptions: {
            	sliceMargin: 4,
            	showDataLabels: true
            }
        },
        legend: { show:true, location: 'e' }
    });

    // print window
    $('#footer').hide();
    $(document.body).ready(function() {
    	print_win();
    });
});
function chkstate() {
	if(document.readyState=="complete") {
		window.close()
	} else {
		setTimeout("chkstate()", 2000);
	}
}
function print_win() {
	window.print();
	chkstate();
}
</script>
