<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />


<portlet:actionURL var="postTestmonialURL">
	<portlet:param name="action" value="postTestmonial" />
</portlet:actionURL>

 <form:form name="testmonialForm" method="post"
		 action="${postTestmonialURL}">
		
<div class="innerLeft">
                	    <!-- Testmonial Post -->
                               <c:if test="${isRegisteredUser}">
                                <div class="posttestmonial">
                                    <div class="testmonialpost">
                                    	<p class="testmonialdivcomnt">Post Your Testmonial</p>
                                    </div>                                   
                        			<div class="testmonialpostsearchlistbox" style="display: none;">
                            			<p><input name="tesmonialDesc" type="text" placeholder="Write Your Testmonail..."></p>
                                        <p><input type="submit" class="sendbtn" value="Submit" style="background: #DE696F;padding: 15px;color: white;width: 90px;text-align: center;
											border-radius: 6px;-moz-border-radius: 6px;-webkit-border-radius: 6px;border: 1px solid #B86165;font-size: 18px;text-transform: uppercase;cursor: pointer;"/></p>
                                        <div class="addfirendssearchresult ">
                                        </div>
                        			</div>
                        			
                                </div>
                                </c:if>
                                
                             <div class="postedtestmonials">
                             	  <ul class="socialactivelist">
         							<c:forEach var="testmonial" items="${testMonialsList}">
         							<!--non admin -->
         							<c:if test="${fn:toLowerCase(testmonial.status) == 'approved'}" >                    	  
                               		<li>
                                        <table class="postdtesttable">
                                            <tbody><tr>
                                                <td class="td1"><img src="<%=themeDisplay.getPathThemeImages()%>/Testimonials.jpg" height="100" width="100"></td>
                                                <td class="td2">
                                                	<p class="activeby">${testmonial.lastName} ${testmonial.firstName}<br> </p>
                                                    <p class="activedec">${testmonial.className}</p>
                                                    <p class="activetime">${testmonial.description}</p>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </li>
                                    </c:if>
                                    <!--  admin -->
                                    <c:if test="${fn:toLowerCase(testmonial.status) == 'pending' and fn:toLowerCase(isAdmin) =='true'}" >                    	  
                               		<li>
                                        <table class="postdtesttable">
                                            <tbody><tr>
                                                <td class="td1"><img src="<%=themeDisplay.getPathThemeImages()%>/Testimonials.jpg" height="100" width="100"></td>
                                                <td class="td2">
                                                	<p class="activeby">${testmonial.lastName} ${testmonial.firstName}<br> </p>
                                                    <p class="activedec">${testmonial.className}</p>
                                                    <p class="activetime">${testmonial.description}</p>                                                   
                                                    <p class="activetime" style="text-align:right" >
                                                    <a href="<portlet:actionURL>
																<portlet:param name="action" value="approveTestmonial" />
																<portlet:param name="testmonialId" value="${testmonial.testId}" />	
															</portlet:actionURL>">			
                                                    Approve
                                                    </a></p>
                                                </td>
                                           </tr>
                                        </tbody></table>
                                    </li>
                                    </c:if>
                                   </c:forEach>
                                   <%--  <li>
                                        <table class="postdtesttable">
                                            <tbody><tr>
                                                <td class="td1"><img src="<%=themeDisplay.getPathThemeImages()%>/Testimonials.jpg" height="100" width="100"></td>
                                                <td class="td2">
                                                	<p class="activeby">Anjali<br> </p>
                                                    <p class="activedec">Class 10</p>
                                                    <p class="activetime">Teach Learn Web is very useful for quick revision before my exams as the chapter tests helped me revise important topics once again.</p>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </li>
                                    <li>
                                        <table class="postdtesttable">
                                            <tbody><tr>
                                                <td class="td1"><img src="<%=themeDisplay.getPathThemeImages()%>/Testimonials.jpg" height="100" width="100"></td>
                                                <td class="td2">
                                                	<p class="activeby">Padmaja<br> </p>
                                                    <p class="activedec">Teacher</p>
                                                    <p class="activetime">3.	Sometimes retaining important concepts is difficult in kids as they tend to get distracted soon. I saw kids looking at the videos in Teach Learn Web and they started taking more and more worksheets as they get rewarded if they do more worksheets. As a teacher I was very impressed at their methodology.</p>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </li>
                                    <li>
                                        <table class="postdtesttable">
                                            <tbody><tr>
                                                <td class="td1"><img src="<%=themeDisplay.getPathThemeImages()%>/Testimonials.jpg" height="100" width="100"></td>
                                                <td class="td2">
                                                	<p class="activeby">Sashwat<br> </p>
                                                    <p class="activedec">Class 9</p>
                                                    <p class="activetime">A month ago I was confused how to start preparing for the exams and tried searching the internet for help. I saw Teach Learn Web program, and subscribed to see if it helps. Now, I am all geared up as the chapter level mind maps helped me tick off each sub topic once it is completed which helped me plan my time efficiently. Thank you Teach Learn Web.</p>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </li>
         --%>
                                  </ul>
                             </div>
			</div>
</form:form>
</body>
</html>