
package com.teachlearnweb.service;

import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Component;

import com.teachlearnweb.dao.entity.DTAssessmentDetails;
import com.teachlearnweb.dao.entity.Skill;
import com.tlw.assessment.vo.Assessment;
import com.tlw.assessment.vo.SkillWiseAssessmentReport;
import com.tlw.assessment.vo.StyleBasedAssessmentReport;

/**
 * Interface AssessmentDetailsService declares DML operations on AssessmentDetails table.
 * 
 * @author Govinda.
 */
@Component
public interface AssessmentDetailsService {
//	public void addAssessmentDetails(DTAssessmentDetails assessment);
//	public void removeAssessmentDetails(Integer assessmentId);
	public Assessment getAssessment(int assessmentId) throws HibernateException;
	public Map<Short, SkillWiseAssessmentReport> getAssessmentReport(final int assessmentId);
	public StyleBasedAssessmentReport getStyleBasedReport(final int assessmentId);
	/**
	 * To prepare Subject Test report
	 * @param assessmentId
	 * @return
	 */
	public Map<String, Integer> getSubjectTestReport(final int assessmentId);

	/**
	 * Method to update assessmentDetail.
	 * 
	 * @param assessmentDetailsList
	 */
	public void updateAssessmentDetails(Assessment assessment);
	/**
	 * Method to save assessmentDetailList.
	 * 
	 * @param assessmentDetailsList
	 */
	public void saveAssessmentDetailList(List<DTAssessmentDetails> assessmentDetailList);
	
	/**
	 * Method to save assessmentDetail.
	 * 
	 * @param assessmentDetail
	 */
	public void saveAssessmentDetail(DTAssessmentDetails assessmentDetail);
	
	/**
	 * Method to fetch DTAssessmentDetail by questionId.
	 * 
	 * @param questionId
	 * @return
	 */
	public DTAssessmentDetails getDTAssessmentDetailsByQuestionId(int questionId);
	
	/**
	 * Method to fetch skill by skillName.
	 * 
	 * @param skillName
	 * @return
	 */
	public Skill getSkillBySkillName(String skillName);
}
