package com.teachlearnweb.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.teachlearnweb.service.AssessmentDetailsService;
import com.teachlearnweb.service.AssessmentService;
import com.teachlearnweb.service.BookService;
import com.teachlearnweb.service.CouponCodeService;
import com.teachlearnweb.service.PLPAnswerService;
import com.teachlearnweb.service.PLPQuestionsService;
import com.teachlearnweb.service.PLPStudentWorkSheetService;
import com.teachlearnweb.service.PLPTestService;
import com.teachlearnweb.service.RewardPointsService;
import com.teachlearnweb.service.ServiceLocator;
import com.teachlearnweb.service.TestmonialService;
import com.teachlearnweb.service.TopicMasterService;
import com.teachlearnweb.service.UserService;
import com.teachlearnweb.service.WorkSheetService;

/**
 * 
 * Class Used to locate the Service Objects.
 * 
 * @author GalaxE.
 *
 */
@SuppressWarnings("rawtypes")
@Component(value = "serviceLocator")
public class ServiceLocatorImpl implements ServiceLocator {
	private static ServiceLocatorImpl ourInstance = new ServiceLocatorImpl();
	@Autowired 
	@Qualifier(value="bookService")
	private BookService bookService;
	
	@Autowired 
	@Qualifier(value="userService")
	private UserService userService;
	
	@Autowired 
	@Qualifier(value="testmonialService")
	private TestmonialService testmonialService;
	
	@Autowired 
	@Qualifier(value="couponCodeService")
	private CouponCodeService couponCodeService;	

	@Autowired 
	@Qualifier(value="rewardPointsService")
	private RewardPointsService rewardPointsService;
	
	@Autowired
	@Qualifier(value="assessmentService")
	private AssessmentService assessmentService;

	@Autowired 
	@Qualifier(value="assessmentDetailsService")
	private AssessmentDetailsService assessmentDetailsService;
	
	@Autowired 
	@Qualifier(value="workSheetService")
	private WorkSheetService workSheetService;
	
	@Autowired 
	@Qualifier(value="plpQuestionsService")
	private PLPQuestionsService plpQuestionService;
	
	@Autowired 
	@Qualifier(value="plpAnswerService")
	private PLPAnswerService plpAnswerService;
	
	@Autowired 
	@Qualifier(value="plpTestService")
	private PLPTestService plpTestService;
	
	@Autowired
	@Qualifier(value="topicMasterService")
	private TopicMasterService topicMasterService;
	
	public static ServiceLocatorImpl getOurInstance() {
		return ourInstance;
	}

	public static void setOurInstance(ServiceLocatorImpl ourInstance) {
		ServiceLocatorImpl.ourInstance = ourInstance;
	}

	public TopicMasterService getTopicMasterService() {
		return topicMasterService;
	}

	public void setTopicMasterService(TopicMasterService topicMasterService) {
		this.topicMasterService = topicMasterService;
	}

	@Autowired 
	@Qualifier(value="plpStudentWorkSheetService")
	private PLPStudentWorkSheetService plpStudentWorkSheetService;

	public PLPStudentWorkSheetService getPlpStudentWorkSheetService() {
		return plpStudentWorkSheetService;
	}

	public void setPlpStudentWorkSheetService(
			PLPStudentWorkSheetService plpStudentWorkSheetService) {
		this.plpStudentWorkSheetService = plpStudentWorkSheetService;
	}

	public PLPTestService getPlpTestService() {
		return plpTestService;
	}

	public void setPlpTestService(PLPTestService plpTestService) {
		this.plpTestService = plpTestService;
	}

	public PLPAnswerService getPlpAnswerService() {
		return plpAnswerService;
	}

	public void setPlpAnswerService(PLPAnswerService plpAnswerService) {
		this.plpAnswerService = plpAnswerService;
	}

	public PLPQuestionsService getPlpQuestionService() {
		return plpQuestionService;
	}

	public void setPlpQuestionService(PLPQuestionsService plpQuestionService) {
		this.plpQuestionService = plpQuestionService;
	}

	public WorkSheetService getWorkSheetService() {
		return workSheetService;
	}

	public void setWorkSheetService(WorkSheetService workSheetService) {
		this.workSheetService = workSheetService;
	}

	public AssessmentService getAssessmentService() {
		return assessmentService;
	}

	public void setAssessmentService(AssessmentService assessmentService) {
		this.assessmentService = assessmentService;
	}

	public static ServiceLocatorImpl getInstance() {
		return ourInstance;
	}

	private ServiceLocatorImpl() {
	}

	/**
	 * @return the bookService
	 */
	public BookService getBookService() {
		return bookService;
	}

	/**
	 * @param bookService the bookService to set
	 */
	public void setBookService(BookService bookService) {
		this.bookService = bookService;
	}

	
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	/**
	 * @return the assessmentDetailsService
	 */
	public AssessmentDetailsService getAssessmentDetailsService() {
		return assessmentDetailsService;
	}

	/**
	 * @param assessmentDetailsService the assessmentDetailsService to set
	 */
	public void setAssessmentDetailsService(
			AssessmentDetailsService assessmentDetailsService) {
		this.assessmentDetailsService = assessmentDetailsService;
	}

	public TestmonialService getTestmonialService() {
		return testmonialService;
	}

	public void setTestmonialService(TestmonialService testmonialService) {
		this.testmonialService = testmonialService;
	}


	public CouponCodeService getCouponCodeService() {
		return couponCodeService;
	}

	public void setCouponCodeService(CouponCodeService couponCodeService) {
		this.couponCodeService = couponCodeService;
	}
	

	public RewardPointsService getRewardPointsService() {
		return rewardPointsService;
	}

	public void setRewardPointsService(RewardPointsService rewardPointsService) {
		this.rewardPointsService = rewardPointsService;
	}

	
}