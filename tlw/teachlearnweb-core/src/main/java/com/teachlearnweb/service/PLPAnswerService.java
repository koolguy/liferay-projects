package com.teachlearnweb.service;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.teachlearnweb.dao.entity.PLPAnswer;

/**
 * Class is responsible for PLPAnswerService related operations.
 * 
 * @author satya.
 *
 */

@Component
public interface PLPAnswerService {

	/**
	 * Method to fetch answer by questionId.
	 * 
	 * @param questionId
	 * @return
	 */
	public List<PLPAnswer> getAnswerByQuestionId(Integer questionId);
	
	/**
	 * Method to fetch questionId and corresponding plpAnswers.
	 * 
	 * @param questionId
	 * @param answersList
	 * @return
	 */
	public Map<Integer, List<PLPAnswer>> getQuestionsAndAnswersMap(Integer questionId);
	
	/**
	 * Method to fetch plpAnswer by answerId.
	 * 
	 * @param answerId
	 * @return
	 */
	public PLPAnswer getPLPAnswerByAnswerId(Integer answerId);
	
	/**
	 * Method to fetch  plpAsnwerslisy by questionIds.
	 * 
	 * @param questionIds
	 * @return
	 */
	public List<PLPAnswer> getAnswersByQuestionIds(String questionIds);

}
