package com.teachlearnweb.service;

import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Component;

import com.teachlearnweb.dao.entity.ClassMaster;
import com.teachlearnweb.dao.entity.DTAnswer;
import com.teachlearnweb.dao.entity.DTAssessment;
import com.teachlearnweb.dao.entity.DTQuestion;
import com.teachlearnweb.dao.entity.DTTest;
import com.teachlearnweb.dao.entity.DifficultyLevel;
import com.teachlearnweb.dao.entity.Skill;
import com.teachlearnweb.dao.entity.SubjectMaster;

/**
 * Class is responsible for assessment service related operations.
 * 
 * @author satya.
 *
 */

@Component
public interface AssessmentService {
	
	
	/**
	 * Method to fetch questions randomly.
	 * 
	 * @return List<DTQuestion>
	 */
	public List<DTQuestion> getQuestions();
	
	/**
	 * Method to to fetch answers based on questionId.
	 * 
	 * @param questionId
	 * @return  List<DTAnswer>
	 */
	public List<DTAnswer> getAnswers(int questionId);
	
	/**
	 * Method to save assessmentList.
	 * 
	 * @param assessmentDetailsList
	 */
	public void saveAssessmentList(List<DTAssessment> assessmentsList);
	
	/**
	 * Method to save DTAssessments.
	 * 
	 * @param assessment
	 */
	public void saveAssessment(DTAssessment assessment);
	
	/**
	 * Method to fetch dtTest based on classid.
	 * 
	 * @param classId
	 * @return ClassMaster.
	 */
	public DTTest getDTTest(short classId);
	
	/**
	 * Method to fetch DTAnswer based on questionId.
	 * 
	 * @param questionId
	 * @return
	 */
	public DTAnswer getAnswerForQuestion(Integer questionId);

	/**
	 * To get all assessments list for a given user under a given test
	 * @param userId
	 * @param testId
	 * @return
	 * @throws HibernateException
	 */
	public List<DTAssessment> getDiagnosticTests(final Integer userId);
	public List<DTAssessment> getAssessments(final Integer userId, final Short testId) throws HibernateException;
	public List<DTAssessment> getAssessments(final Integer userId, final Short classId, final Short testId) throws HibernateException;
	public List<DTAssessment> getAssessments(final Integer userId, final Short classId,
			final Short subjectId, final Short testId) throws HibernateException;
	public List<DifficultyLevel> getDifficultiesBySubject(final short subjectId, final short classId) throws HibernateException;
	public DifficultyLevel getDifficultyLevelByAssessment(final Integer assessmentId) throws HibernateException;	
	public SubjectMaster getSubject(Short subjectId);
	public List<DTQuestion> getQuestions(final Short classId, final Short subjectId, final Short levelId);
	public List<DTQuestion> getStyleBasedQuestions();
	public DTAssessment getAssessment(final Integer assessmentId);
	public SubjectMaster getSubjectByAssessment(final Integer assessmentId);
	/**
	 * Method to fetch questions based on classId and subjectId.
	 * @author govinda 
	 * @return List<DTQuestion>
	 */
	public List<DTQuestion> getSkillwiseQuestions(final List<Skill> skills, final Short classId,
			final Short subjectId, final Short questionsPerSkill);
	public List<Skill> getSkills(short subjectId);
	/**
	 * Method to fetch questions based on classId and subjectId.
	 * 
	 * @return List<DTQuestion>
	 */
	public List<DTQuestion> getQuestions(short classId, short subjectId);
	
	/**
	 * Method to fetch subjectMaster by subjectName.
	 * 
	 * @param subjectName
	 * @return
	 */
	public SubjectMaster getSubjectMasterbySbujectName(String subjectName);
	
	/**
	 * Method to fecth classMaster by classId.
	 * 
	 * @param classId
	 * @return
	 */
	public ClassMaster getClassMasterByClassId(short classId);
	
	/**
	 * Method to fetch DTTest based on classId and subjectId.
	 * 
	 * @param classId
	 * @param subjectId
	 * @return DTTest.
	 */
	public DTTest getDTTest(short classId, short subjectId);
	
	/**
	 * Method to fetch classMaster by className.
	 * 
	 * @param className
	 * @return
	 */
	public ClassMaster getClassMasterByClassName(String className);
	
	/**
	 * Method to fetch user completed workSheets.
	 * 
	 * @param userId
	 * @param status
	 * @return
	 */
	public int getTestsFinishedByStatusAndUserId(long userId, String status);

	/**
	 * Method to fetch all completed dt assessments by subject tests.
	 * 
	 * @param testId
	 * @param status
	 * @return
	 */
	public List<DTAssessment> fetchAllCompletedDTAssessmentsBySubjectTest(Short testId, String status);
	
	
	
}
