package com.teachlearnweb.service;

import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Component;

import com.teachlearnweb.dao.entity.PLPQuestion;

/**
 * Class is responsible for PLPQuestionsService related operations.
 * 
 * @author satya.
 *
 */

@Component
public interface PLPQuestionsService {

	/**
	 * Method to fetch question.
	 * 
	 * @param questionId
	 * @return PLPQuestion
	 */
	public  List<PLPQuestion> getQuestionById(Integer questionId);
	
	/**
	 * Method to fetch list of questions.
	 *  
	 * @param questionIdsList
	 * @return
	 */
	public List<PLPQuestion> getQuestionsListByQIds(String questionIdsList);
	
	/**
	 * Method to updated answer in plpQuestion table for particular question.
	 * 
	 * @param questionId
	 */
	public void updateAnswerIdForQuestion(PLPQuestion question) throws HibernateException;
	
	/**
	 * Method to fetch questions.
	 * 
	 * @param classId
	 * @param subjectId
	 * @param skillId
	 * @param subTopicId
	 * @param topicId
	 * @return
	 */
	public List<PLPQuestion> getQuestions(Short classId, Short subjectId, Short skillId, Integer subTopicId, Integer topicId);
	
	/**
	 * Method to fetch questions based on userId, classId, topicId and subjectId.
	 * 
	 * @param userid
	 * @param classId
	 * @param subjectId
	 * @param topicId
	 * @return
	 */
	public List<PLPQuestion> getQuestions(Short classId, Short subjectId, Short skillId, Integer topicId);
}
