package com.teachlearnweb.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.teachlearnweb.dao.PLPStudentWorkSheetDAO;
import com.teachlearnweb.dao.entity.PLPStudentWrokSheet;
import com.teachlearnweb.service.PLPStudentWorkSheetService;

/**
 * Class is responsible for plpStudentWorkSheet service related operations.
 * 
 * @author Satya.
 */

@Service(value = "plpStudentWorkSheetService")
public class PLPStudentWorkSheetServiceImpl implements PLPStudentWorkSheetService{

	// logger
	private static final Logger logger = Logger	.getLogger(PLPStudentWorkSheetServiceImpl.class);
		
	@Autowired
	@Qualifier(value="plpStudentWorkSheetDao")
	private PLPStudentWorkSheetDAO plpStudentWorkSheetDao;
	
	public PLPStudentWorkSheetDAO getPlpStudentWorkSheetDao() {
		return plpStudentWorkSheetDao;
	}

	public void setPlpStudentWorkSheetDao(
			PLPStudentWorkSheetDAO plpStudentWorkSheetDao) {
		this.plpStudentWorkSheetDao = plpStudentWorkSheetDao;
	}

	public List<PLPStudentWrokSheet> getPLPStudentWorkSheetsByUserId(long userId) {
		logger.debug("getPLPStudentWorkSheetsByUserId() Method Start in PLPStudentWorkSheetServiceImpl");
		return getPlpStudentWorkSheetDao().getPLPStudentWorkSheetsByUserId(userId);
	}

	/**
	 * Method to save plpStudentWorkSheetsList.
	 * 
	 * @param wsList
	 */
	public void savePLPStudentWorkSheetsList(List<PLPStudentWrokSheet> wsList) throws HibernateException {
		logger.debug("savePLPStudentWorkSheetsList(wsList) Method Start in PLPStudentWorkSheetServiceImpl.");
		if(wsList != null && !wsList.isEmpty() && wsList.size() > 0) {
			getPlpStudentWorkSheetDao().savePLPStudentWorkSheetsList(wsList);
		}
	}

	/**
	 * Method to fetch plpStudentWorkSheet.
	 * 
	 * @param workSheet
	 */
	public void savePLPStudentWorkSheets(PLPStudentWrokSheet workSheet)	throws HibernateException {
		logger.debug("savePLPStudentWorkSheets() Method Start in PLPStudentWorkSheetServiceImpl.");
		if(workSheet != null) {
			getPlpStudentWorkSheetDao().savePLPStudentWorkSheets(workSheet);
		}
	}

	/**
	 * Method to fetch worksheet by worksheetId.
	 * 
	 * @param workSheetId
	 */
	public PLPStudentWrokSheet getPLPStudentWorkSheetByWorkSheetId(Integer workSheetId, long userId) {
		logger.debug("getPLPStudentWorkSheetByWorkSheetId(workSheetId) Method Start in PLPStudentWorkSheetServiceImpl.");
		return getPlpStudentWorkSheetDao().getPLPStudentWorkSheetByWorkSheetId(workSheetId, userId);
	}

	/**
	 * Method to update plpStudentWorkSheet.
	 * 
	 * @param workSheet
	 */
	public void updatePLPStudentWorkSheet(PLPStudentWrokSheet workSheet) {
		logger.debug("updatePLPStudentWorkSheet(workSheet) Method Start in PLPStudentWorkSheetServiceImpl.");
		getPlpStudentWorkSheetDao().updatePLPStudentWorkSheet(workSheet);
	}

	/**
	 * Method to check whether workSheets are there or not for the subTopicId.
	 * 
	 * @param subTopicid
	 * @return
	 */
	public boolean checkWorkSheetListForSubTopicId(Integer subTopicid, long userId, Short classId, Short subjectId) {
		logger.debug("checkWorkSheetListForSubTopicId(subTopicId, userId, classId, sujectId) Method Start in PLPStudentWorkSheetServieImpl.");
		return getPlpStudentWorkSheetDao().checkWorkSheetListForSubTopicId(subTopicid, userId, classId, subjectId);
	}

	/**
	 * Method to check whether workSheets are there or not for the topicId.
	 * 
	 * @param topicId.
	 * @return
	 */
	public boolean checkEOCTesteForTopicId(Integer topicId, long userId, Short classId, Short subjectId) {
		logger.debug("checkWorkSheetListForSubTopicId() Method Start in PLPStudentWorkSheetServieImpl.");
		return getPlpStudentWorkSheetDao().checkEOCTesteForTopicId(topicId, userId, classId, subjectId);
	}
	
	/**
	 * Method to fetch user completed workSheets.
	 * 
	 * @param userId
	 * @param status
	 * @return
	 */
	public int getWorkSheetsStatusByUserId(long userId, String status) {
		logger.debug("getWorkSheetsStatusByUserId(userid, status) Method Start in PLPStudentWorkSheetServieImpl.");
		return getPlpStudentWorkSheetDao().getWorkSheetsStatusByUserId(userId, status);	
	}

	/**
	 * Method to fetch all completed worksheets.
	 * 
	 * @param status
	 * @return
	 */
	public List<PLPStudentWrokSheet> fetchAllCompletedWorkSheets(String status) {
		logger.debug("fetchAllCompletedWorkSheets(status) Method Start in PLPStudentWorkSheetServieImpl.");
		return getPlpStudentWorkSheetDao().fetchAllCompletedWorkSheets(status);	
	}

}
