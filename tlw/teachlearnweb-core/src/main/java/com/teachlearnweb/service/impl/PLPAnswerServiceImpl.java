package com.teachlearnweb.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.teachlearnweb.dao.PLPAnswerDAO;
import com.teachlearnweb.dao.PLPQuestionsDAO;
import com.teachlearnweb.dao.entity.PLPAnswer;
import com.teachlearnweb.service.PLPAnswerService;

/**
 * Class is responsible for  PLPQuestionsService related operations.
 * 
 * @author satya.
 *
 */
@Service(value="plpAnswerService")
public class PLPAnswerServiceImpl implements PLPAnswerService{

	// logger
		private static final Logger logger = Logger.getLogger(PLPAnswerServiceImpl.class);
		
		@Autowired
		@Qualifier(value="plpAnswerDAO")
		private PLPAnswerDAO plpAnswerDAO;
		
		public PLPAnswerDAO getPlpAnswerDAO() {
			return plpAnswerDAO;
		}

		public void setPlpAnswerDAO(PLPAnswerDAO plpAnswerDAO) {
			this.plpAnswerDAO = plpAnswerDAO;
		}

	/**
	 * Method to fetch answer by questionId.
	 * 
	 * @param questionId
	 * @return
	 */
	public List<PLPAnswer> getAnswerByQuestionId(Integer questionId) {
		logger.debug("getAnswerByQuestionId() Method Start in PLPAnswerServiceImpl");
		return getPlpAnswerDAO().getAnswerByQuestionId(questionId);
	}

	/**
	 * Method to fetch questionId and corresponding plpAnswers.
	 * 
	 * @param questionId
	 * @param answersList
	 * @return
	 */
	public Map<Integer, List<PLPAnswer>> getQuestionsAndAnswersMap(Integer questionId) {
		logger.debug("getQuestionsAndAnswersMap() Method Start in PLPAnswerServiceImpl");
		return getPlpAnswerDAO().getQuestionsAndAnswersMap(questionId);
	}

	/**
	 * Method to fetch plpAnswer by answerId.
	 * 
	 * @param answerId
	 * @return
	 */
	public PLPAnswer getPLPAnswerByAnswerId(Integer answerId) {
		logger.debug("getPLPAnswerByAnswerId() Method Start in PLPAnswerServiceImpl");
		List<PLPAnswer> plpAnswerList = getPlpAnswerDAO().getPLPAnswerByAnswerId(answerId);
		PLPAnswer plpAnswer = null;
		if(plpAnswerList != null && !plpAnswerList.isEmpty() && plpAnswerList.size() > 0) {
			plpAnswer = plpAnswerList.get(0);
		}
		return plpAnswer;
	}
	
	/**
	 * Method to fetch  plpAsnwerslisy by questionIds.
	 * 
	 * @param questionIds
	 * @return
	 */
	public List<PLPAnswer> getAnswersByQuestionIds(String questionIds) {
		logger.debug("getAnswersByQuestionIds() Method Start in PLPAnswerServiceImpl");
		List<PLPAnswer> plpAnswerList = new ArrayList<PLPAnswer>();
		List<String> questionsList = extraceQuestionIds(questionIds);
		if(questionsList != null && !questionsList.isEmpty() && questionsList.size() > 0) {
			for(String tempQId : questionsList) {
				PLPAnswer answer = null;
				try {
					answer = getPLPAnswerByAnswerId(Integer.parseInt(tempQId));
				} catch (NumberFormatException e) {
					logger.debug("Number Format Exception Caught While fetching PLPAnswer in PLPAnswerServiceImpl");
					e.printStackTrace();
				}
				catch (HibernateException e) {
					logger.debug("Hibernate Exception Caught While fetching PLPAnswer in PLPAnswerServiceImpl");
					e.printStackTrace();
				}
				if(answer != null && answer.getAnswerId() != null) {
					plpAnswerList.add(answer);
				}
		}
		}
		return plpAnswerList;
	}

	private List<String> extraceQuestionIds(String questionIds) {
		logger.debug("extraceQuestionIds() Method Start in PLPAnswerServiceImpl");
		List<String> questionidsList = new ArrayList<String>();
		String[] tempArray = questionIds.split(",");
		for(int i=0; i<tempArray.length;i++) {
			String tempQuestionId = tempArray[i];
			questionidsList.add(tempQuestionId);
		}
		return questionidsList;
	}


}
