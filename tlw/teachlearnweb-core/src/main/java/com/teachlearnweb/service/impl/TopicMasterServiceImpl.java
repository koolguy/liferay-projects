package com.teachlearnweb.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.teachlearnweb.dao.TopicMasterDAO;
import com.teachlearnweb.dao.entity.PLPTopicMaster;
import com.teachlearnweb.service.TopicMasterService;

/**
 * Class is responsible for assessment service related operations.
 * 
 * @author satya.
 *
 */
@Service(value="topicMasterService")
public class TopicMasterServiceImpl implements TopicMasterService{

	// logger
	private static final Logger logger = Logger.getLogger(TopicMasterServiceImpl.class);
	
	@Autowired
	@Qualifier(value="topicMasterDao")
	private TopicMasterDAO topicMasterDao;
			
	public TopicMasterDAO getTopicMasterDao() {
		return topicMasterDao;
	}

	public void setTopicMasterDao(TopicMasterDAO topicMasterDao) {
		this.topicMasterDao = topicMasterDao;
	}

	

	/**
	 * Method to fetch plptopicmaster by subTopicId.
	 * 
	 * @return
	 */
	public PLPTopicMaster getTopicMasterBySubTopicId(Integer subTopicId, Short classId, Short subjectId) {
		logger.debug("getTopicMasterBySubTopicId(subTopicId) Method Start in TopicMasterServiceImpl");
		return getTopicMasterDao().getTopicMasterBySubTopicId(subTopicId, classId, subjectId);
	}

	/**
	 * Method to fetch plptopicmaster by topicId.
	 * 
	 * @return
	 */
	public PLPTopicMaster getTopicMasterByTopicId(Integer topicId, Short classId, Short subjectId) {
		logger.debug("getTopicMasterByTopicId(topicId) Method Start in TopicMasterServiceImpl");
		return getTopicMasterDao().getTopicMasterByTopicId(topicId, classId, subjectId);
	}

	/**
	 * Method to fetch TopicMaster by topicName.
	 * 
	 * @param topicName
	 * @return
	 */
	public PLPTopicMaster getTopicMasterByTopicName(String topicName, Short classId, Short subjectId) {
		logger.debug("getTopicMasterByTopicName(topicName, classId, subjectId) Method Start in TopicMasterServiceImpl");
		return getTopicMasterDao().getTopicMasterByTopicName(topicName, classId, subjectId);
	}

	/**
	 * Method to fetch TopicMaster by topicName.
	 * 
	 * @param topicName
	 * @return
	 */
	public PLPTopicMaster getTopicMasterByTopicName(String topicName, Integer parentTopicId, Short classId, Short subjectId) {
		logger.debug("getTopicMasterByTopicName(topicName, classId, subjectId) Method Start in TopicMasterServiceImpl");
		return getTopicMasterDao().getTopicMasterByTopicName(topicName, parentTopicId, classId, subjectId);
	}
	
}
