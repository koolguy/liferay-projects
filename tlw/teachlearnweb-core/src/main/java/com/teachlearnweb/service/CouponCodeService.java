package com.teachlearnweb.service;


import java.util.List;

import com.teachlearnweb.dao.entity.CouponCode;
import com.teachlearnweb.dao.entity.Testmonial;
import com.teachlearnweb.service.vo.User;

/**
 * Service interface for the BookCatalog portlet.
 * 
 * @author asarin
 *
 */
public interface CouponCodeService {
	
	public void addCouponcode(String couponCode ,String desc, String amount) throws Exception;

	public List<CouponCode> fetchCouponCodes();

	public CouponCode fetchCouponDetailByCode(String coupon);
	
	
	
}
