package com.teachlearnweb.service.impl;

import java.text.ParseException;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.liferay.portal.DuplicateUserEmailAddressException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.CalendarFactoryUtil;
import com.liferay.portal.model.Address;
import com.liferay.portal.model.Contact;
import com.liferay.portal.service.AddressServiceUtil;
import com.liferay.portal.service.ContactLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.UserServiceUtil;
import com.teachlearnweb.dao.RewardPointsDAO;
import com.teachlearnweb.dao.UserCategoriesDAO;
import com.teachlearnweb.dao.UserDAO;
import com.teachlearnweb.dao.entity.RewardPoints;
import com.teachlearnweb.dao.entity.TLWUser;
import com.teachlearnweb.dao.entity.UserCategories;
import com.teachlearnweb.service.UserService;
import com.teachlearnweb.service.vo.User;
import com.tlw.service.util.MyProfileServiceUtil;
import com.tlw.service.util.RegistrantionServiceUtil;

@Service(value = "userService")
public class UserServiceImpl implements UserService {
	private static final Logger LOG = Logger.getLogger(UserServiceImpl.class);

	/**
	 * Instance variable for employeeDao.
	 */
	@Autowired
	UserDAO userDao;

	@Autowired
	UserCategoriesDAO userCategoriesDao;

	@Autowired
	RewardPointsDAO rewardPointsDAO;

	public RewardPointsDAO getRewardPointsDAO() {
		return rewardPointsDAO;
	}

	public void setRewardPointsDAO(RewardPointsDAO rewardPointsDAO) {
		this.rewardPointsDAO = rewardPointsDAO;
	}

	public UserDAO getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDAO userDao) {
		this.userDao = userDao;
	}

	public UserCategoriesDAO getUserCategoriesDao() {
		return userCategoriesDao;
	}

	public void setUserCategoriesDao(UserCategoriesDAO userCategoriesDao) {
		this.userCategoriesDao = userCategoriesDao;
	}

	public User addUser(User registrationBean, long creatorUserId,
			long companyId, long scopeGroupId) throws Exception {

		LOG.debug("addUser..Email............"
				+ registrationBean.getEmailAddress());
		System.out.println("addUser..Email............"
				+ registrationBean.getEmailAddress());
		User user = null;
		TLWUser tlwUser = new TLWUser();
		// RegistrantionServiceUtil.add(creatorUserId, companyId, locale,
		// registrationBean, autoScreenName, sendEmail);
		try {
			try {
				user = RegistrantionServiceUtil.add(creatorUserId, companyId,
						registrationBean, scopeGroupId);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			tlwUser.setUserId(user.getUserId());
			//tlwUser.setSchoolName(registrationBean.getSchoolName());
			tlwUser.setClassName(registrationBean.getClassName());
			//tlwUser.setSectionName(registrationBean.getSectionName());
			//tlwUser.setSectionName("A");
			tlwUser.setParentName(registrationBean.getParentName());
			//tlwUser.setSchoolCity(registrationBean.getSchoolCity());
			//tlwUser.setSchoolLocality(registrationBean.getSchoolLocality());
			tlwUser.setSyllabus(registrationBean.getSyllabus());
			tlwUser.setPhone(registrationBean.getPhone());
			tlwUser.setEnrollmentBenefitIndicator(true);
			tlwUser.setLeadershipIndicator(true);
			tlwUser.setJobTitle(registrationBean.getJobTitle());
			userDao.addUser(tlwUser);
		}catch (DuplicateUserEmailAddressException e) {
			throw new DuplicateUserEmailAddressException();
		}
		catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;

	}

	public com.liferay.portal.model.User updateUserProfile(
			com.liferay.portal.model.User liferayUser) {

		try {
			liferayUser = MyProfileServiceUtil.updateUserProfile(liferayUser);

		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return liferayUser;

	}

	public void updateTLWUserProfile(TLWUser tlwUser) {
		LOG.debug("########### From updateTLWUserProfile ########## ");
		try {
			userDao.updateTLWUserProfile(tlwUser);
		} catch (Exception e) {
			LOG.debug("Exception raised while updating TLW USer"
					+ e.getMessage());
		}
	}

	public User findUserByUserId(long companyId, long userId) {
		User user = new User();
		try {
			com.liferay.portal.model.User liferayUser = UserLocalServiceUtil
					.getUserById(companyId, userId);

			user = populateUser(user, liferayUser);

		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}

	public User updatePassword(long userId, String password1, String password2,
			boolean passwordReset) {
		User user = new User();
		try {
			com.liferay.portal.model.User liferayUser = UserLocalServiceUtil
					.updatePassword(userId, password1, password2, passwordReset);

			user = populateUser(user, liferayUser);

		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return user;
	}

	private User populateUser(User user,
			com.liferay.portal.model.User liferayUser) {

		user.setUserId(liferayUser.getUserId());
		user.setuUid(liferayUser.getUuid());
		user.setFirstName(liferayUser.getFirstName());
		user.setLastName(liferayUser.getLastName());
		user.setMiddleName(liferayUser.getMiddleName());
		user.setEmailAddress(liferayUser.getEmailAddress());
		user.setContactId(liferayUser.getContactId());
		user.setScreenName(liferayUser.getScreenName());
		user.setJobTitle(liferayUser.getJobTitle());
		user.setPassword(liferayUser.getPasswordUnencrypted());

		long rewardPoints = populateRewardPoints(liferayUser.getUserId());

		TLWUser tlwUser = userDao.findEmployeeByUserId(liferayUser.getUserId());
		if (null != tlwUser) {
			user.setSchoolName(tlwUser.getSchoolName());
			
			/*AssetCategory asset = null;
			long categoryId = 0;
			if(null != tlwUser.getClassName()){
				categoryId = Long.parseLong(tlwUser.getClassName().trim());
			}
			try {
				asset = AssetCategoryLocalServiceUtil.getCategory(categoryId);
			} catch (PortalException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
			
			user.setClassName(tlwUser.getClassName().trim());
			user.setSectionName(tlwUser.getSectionName());
			user.setParentName(tlwUser.getParentName());
			user.setRewardPoints(rewardPoints);
			user.setSchoolCity(tlwUser.getSchoolCity());
			user.setSchoolLocality(tlwUser.getSchoolLocality());
			user.setSyllabus(tlwUser.getSyllabus());
			user.setPhone(tlwUser.getPhone());
			user.setJobTitle(tlwUser.getJobTitle());
			
		}
		user.setPortraitId(liferayUser.getPortraitId());
		LOG.debug("portraitID---------------------> " + liferayUser.getPortraitId());

		try {
			Contact contact = ContactLocalServiceUtil.getContact(liferayUser
					.getContactId());
			String gender = null;
			if (contact.getMale()) {
				gender = "Male";
			} else {
				gender = "Female";
			}
			user.setGender(gender);
			
			Calendar birthdayCal = CalendarFactoryUtil.getCalendar();
			birthdayCal.setTime(contact.getBirthday());
			
			int birthdayDate = birthdayCal.get(Calendar.DATE);
			int birthdayMonth = birthdayCal.get(Calendar.MONTH);
			int birthdayYear = birthdayCal.get(Calendar.YEAR);
			
			String dob = birthdayDate+"-"+birthdayMonth+"-"+birthdayYear;
			LOG.debug(birthdayDate+"-"+birthdayMonth+"-"+birthdayYear);
			
			user.setDob(dob);
			
			List<Address> address  = AddressServiceUtil.getAddresses(Contact.class.getName(), liferayUser.getContactId());
			//List<Address> address = AddressLocalServiceUtil.getAddresses(liferayUser.getCompanyId(), Contact.class.getName(), liferayUser.getContactId());
			if(address != null && address.size() > 0 ){
				Address userAddress = address.get(0);
				user.setAddress1(userAddress.getStreet1());
			}
			
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return user;
	}

	public long populateRewardPoints(long userId) {
		List<RewardPoints> rewardPoints = rewardPointsDAO
				.rewardPointsByUserId(userId);
		long rwdPoints = 0L;
		for (RewardPoints rewardPoints2 : rewardPoints) {
			rwdPoints = rwdPoints + rewardPoints2.getRewardPoints();
		}
		return rwdPoints;

	}

	public void addCourse(long userId, Long categoryId) {
		UserCategories userCategories = new UserCategories();
		userCategories.setUserId(userId);
		userCategories.setCategoryId(categoryId);
		userCategoriesDao.addCourse(userCategories);

	}

	public List<UserCategories> categoriesByUserId(long userId) {
		return userCategoriesDao.categoriesByUserId(userId);
	}

	public long uploadImageFile(long userId, byte[] bytes) {
		LOG.debug("######## uploadImageFile UserServiceImpl ##################");
		com.liferay.portal.model.User user= null;
		try {
			 user =  UserServiceUtil.updatePortrait(userId, bytes);
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
		return user.getPortraitId();
	}

	public void updateContact(Contact contact) {
		
		try {
			 MyProfileServiceUtil.updateContact(contact);
		} catch (PortalException e) {
			e.printStackTrace();
		} catch (SystemException e) {
			e.printStackTrace();
		}
	}

}
