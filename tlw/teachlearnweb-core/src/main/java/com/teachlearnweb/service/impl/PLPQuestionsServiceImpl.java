package com.teachlearnweb.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.teachlearnweb.dao.DTQuestionDAO;
import com.teachlearnweb.dao.PLPQuestionsDAO;
import com.teachlearnweb.dao.entity.DifficultyLevel;
import com.teachlearnweb.dao.entity.PLPQuestion;
import com.teachlearnweb.service.PLPQuestionsService;
import com.tlw.assessment.vo.AssessmentConstants;

/**
 * Class is responsible for  PLPQuestionsService related operations.
 * 
 * @author satya.
 *
 */
@Service(value="plpQuestionsService")
public class PLPQuestionsServiceImpl implements PLPQuestionsService{

	// logger
	private static final Logger logger = Logger.getLogger(PLPQuestionsServiceImpl.class);
	
	@Autowired
	@Qualifier(value="plpQuestionDAO")
	private PLPQuestionsDAO plpQuestionsDAO;
	
	@Autowired
	@Qualifier(value="dtQuestionsDao")
	private DTQuestionDAO dtQuestionDao;
	
	public PLPQuestionsDAO getPlpQuestionsDAO() {
		return plpQuestionsDAO;
	}

	public void setPlpQuestionsDAO(PLPQuestionsDAO plpQuestionsDAO) {
		this.plpQuestionsDAO = plpQuestionsDAO;
	}

	public DTQuestionDAO getDtQuestionDao() {
		return dtQuestionDao;
	}

	public void setDtQuestionDao(DTQuestionDAO dtQuestionDao) {
		this.dtQuestionDao = dtQuestionDao;
	}

	/**
	 * Method to fetch question.
	 * 
	 * @param questionId
	 * @return PLPQuestion
	 */
	public  List<PLPQuestion> getQuestionById(Integer questionId) {
		logger.debug("getQuestionById() Method start in PLPQuestionsServiceImpl");
		return getPlpQuestionsDAO().getQuestionById(questionId);
	}

	/**
	 * Method to fetch list of questions.
	 *  
	 * @param questionIdsList
	 * @return
	 */
	public List<PLPQuestion> getQuestionsListByQIds(String questionIdsList) {
		logger.debug("getQuestionsListByQIds() Method start in PLPQuestionsServiceImpl");
		List<PLPQuestion> plpQuestionsList = getPlpQuestionsDAO().getQuestionsListByQIds(questionIdsList);
		//Added below code for dependent questions.
		List<PLPQuestion> questionsList = new ArrayList<PLPQuestion>();
		if(plpQuestionsList != null && !plpQuestionsList.isEmpty() && plpQuestionsList.size() > 0) {
			for(PLPQuestion question : plpQuestionsList) {
				PLPQuestion dependentQuestion = null;
				if(question != null && question.getDependentId() > 0) {
					try {
						dependentQuestion = getPlpQuestionsDAO().getQuestion(question.getDependentId());
					} catch (Exception e) {
						logger.error("Exception Caught while fetching dependent question object", e);
						e.printStackTrace();
					}
				}
				if(dependentQuestion != null) {
					question.setQuestion(dependentQuestion.getQuestion()+" "+question.getQuestion());
				}
				questionsList.add(question);
			}
		}
		return questionsList;
	}

	/**
	 * Method to updated answer in plpQuestion table for particular question.
	 * 
	 * @param questionId
	 */
	public void updateAnswerIdForQuestion(PLPQuestion question)	throws HibernateException {
		logger.debug("updateAnswerIdForQuestion() Method start in PLPQuestionsServiceImpl");
		 getPlpQuestionsDAO().updateAnswerIdForQuestion(question);
	}

	/**
	 * Method to fetch questions.
	 * 
	 * @param classId
	 * @param subjectId
	 * @param skillId
	 * @param subTopicId
	 * @param topicId
	 * @return
	 */
	public List<PLPQuestion> getQuestions(Short classId, Short subjectId, Short skillId, Integer subTopicId, Integer topicId) {
		logger.debug("getQuestions() Method start in PLPQuestionsServiceImpl");
		return getPlpQuestionsDAO().getQuestions(classId, subjectId, skillId,subTopicId, topicId);
	}

	
	/**
	 * Method to fetch questions based on userId, classId, topicId and subjectId.
	 * 
	 * @param userid
	 * @param classId
	 * @param subjectId
	 * @param topicId
	 * @return
	 */
	public List<PLPQuestion> getQuestions(Short classId, Short subjectId, Short skillId, Integer topicId) {
		logger.debug("getQuestions(classId, subjectId, skillId, topicId) Method Start in PLPQuestionsDAOImpl.");
		List<PLPQuestion> tempQuestionsList = null;
		List<PLPQuestion> plpQuestionsList = new ArrayList<PLPQuestion>();
		List<DifficultyLevel> difficultyLevelsList = null;
		
		try {
			 difficultyLevelsList =  getDtQuestionDao().getDifficultyLevels();
		} catch(HibernateException e) {
			logger.error("HIbernate Exxception While fetching difficulty levels in PLPQuestionServiceImpl.", e);
			e.printStackTrace();
		}
		
		if(difficultyLevelsList != null && !difficultyLevelsList.isEmpty() && difficultyLevelsList.size() > 0) {
			for(DifficultyLevel level : difficultyLevelsList) {
				if(level != null && level.getDescription() != null) {
					tempQuestionsList =  getPlpQuestionsDAO().getQuestions(classId, subjectId, skillId, topicId, level.getLevelId());
				}
				if(tempQuestionsList != null && tempQuestionsList.size() > 0) {
					plpQuestionsList.addAll(tempQuestionsList);
				}
			}
		}
		if(plpQuestionsList != null && plpQuestionsList.size() > 0) {
			logger.debug("plpQuestionsList.size() in getQuestions(userId, classId, subjectId, topicId)"+plpQuestionsList.size());
			System.out.println("plpQuestionsList.size() in getQuestions(userId, classId, subjectId, topicId)"+plpQuestionsList.size());
		}
		 return plpQuestionsList;
	}

}
