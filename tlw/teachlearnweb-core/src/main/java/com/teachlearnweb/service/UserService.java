package com.teachlearnweb.service;

import java.util.List;


import com.liferay.portal.model.Contact;
import com.teachlearnweb.dao.entity.UserCategories;
import com.teachlearnweb.service.vo.User;
import com.teachlearnweb.dao.entity.TLWUser;



/**
 * Service interface for the BookCatalog portlet.
 * 
 * @author asarin
 *
 */
public interface UserService {
	
	public User addUser(User registrationBean,long creatorUserId,long companyId, long scopeGroupId) throws Exception;
	
	public com.liferay.portal.model.User updateUserProfile(com.liferay.portal.model.User liferayUser);
	
	public void updateContact(Contact contact);

	public User findUserByUserId(long companyId, long userId);
	
	public User updatePassword(long userId,String password1,String password2,boolean passwordReset);

	public void addCourse(long userId, Long categoryId);
	
	public List<UserCategories> categoriesByUserId(long userId);
	
	public void updateTLWUserProfile(TLWUser tlwUser);

	public long uploadImageFile(long userId, byte[] bytes);
	
	public long populateRewardPoints(long userId);
}
