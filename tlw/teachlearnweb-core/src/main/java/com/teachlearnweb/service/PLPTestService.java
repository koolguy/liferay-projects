package com.teachlearnweb.service;

import java.util.List;
import org.springframework.stereotype.Component;
import com.teachlearnweb.dao.entity.PLPTest;

/**
 * Class is responsible for PLPTestService related operations.
 * 
 * @author satya.
 *
 */
@Component
public interface PLPTestService {

	/**
	 * Method used to save plpTest.
	 * 
	 * @param plpTest
	 * @return
	 */
	public void savePLPTest(PLPTest plpTest);
	
	
	/**
	 * Method used to save plpTestList.
	 * 
	 * @param plpTestList
	 */
	public void savePLPTestList(List<PLPTest> plpTestList);
}
