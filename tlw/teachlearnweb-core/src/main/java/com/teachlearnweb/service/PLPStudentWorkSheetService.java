package com.teachlearnweb.service;

import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Component;

import com.teachlearnweb.dao.entity.PLPStudentWrokSheet;

/**
 * Class is responsible for PLPStudentWorkSheet service related operations.
 * 
 * @author satya.
 *
 */

@Component
public interface PLPStudentWorkSheetService {

	/**
	 * Method to fetch plpstudentworksheet list by userId.
	 * 
	 * @param userId
	 * @return
	 */
	public List<PLPStudentWrokSheet> getPLPStudentWorkSheetsByUserId(long userId);
	
	/**
	 * Method to save plpStudentWorkSheetsList.
	 * 
	 * @param wsList
	 */
	public void savePLPStudentWorkSheetsList(List<PLPStudentWrokSheet> wsList) throws HibernateException;
	
	/**
	 * Method to fetch plpStudentWorkSheet.
	 * 
	 * @param workSheet
	 */
	public void savePLPStudentWorkSheets(PLPStudentWrokSheet workSheet) throws HibernateException;
	
	/**
	 * Method to fetch worksheet by worksheetId.
	 * 
	 * @param workSheetId
	 */
	public PLPStudentWrokSheet getPLPStudentWorkSheetByWorkSheetId(Integer workSheetId, long userId);

	/**
	 * Method to update plpStudentWorkSheet.
	 * 
	 * @param workSheet
	 */
	public void updatePLPStudentWorkSheet(PLPStudentWrokSheet workSheet);
	
	/**
	 * Method to check whether workSheets are there or not for the subTopicId.
	 * 
	 * @param subTopicid
	 * @return
	 */
	public boolean checkWorkSheetListForSubTopicId(Integer subTopicid, long userId, Short classId, Short subjectId);
	
		
	/**
	 * Method to fetch user completed workSheets.
	 * 
	 * @param userId
	 * @param status
	 * @return
	 */
	public int getWorkSheetsStatusByUserId(long userId, String status);

	/**
	 * Method to fetch all completed worksheets.
	 * 
	 * @param status
	 * @return
	 */
	public List<PLPStudentWrokSheet> fetchAllCompletedWorkSheets(String status);
	
	/**
	 * Method to check whether workSheets are there or not for the subTopicId.
	 * 
	 * @param subTopicid
	 * @return
	 */

	public boolean checkEOCTesteForTopicId(Integer topicId, long userId, Short classId, Short subjectId);

}
