package com.teachlearnweb.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.teachlearnweb.dao.AnswerDAO;
import com.teachlearnweb.dao.PLPTestDAO;
import com.teachlearnweb.dao.entity.PLPTest;
import com.teachlearnweb.service.PLPTestService;

/**
 * 
 * @author Satya
 */
@Service(value = "plpTestService")
public class PLPTestServiceImpl implements PLPTestService{

	// logger
	private static final Logger logger = Logger.getLogger(PLPTestServiceImpl.class);
	
	@Autowired
	@Qualifier(value="plpTestDAO")
	private PLPTestDAO plpTestDao;
	
	public PLPTestDAO getPlpTestDao() {
		return plpTestDao;
	}

	public void setPlpTestDao(PLPTestDAO plpTestDao) {
		this.plpTestDao = plpTestDao;
	}

	/**
	 * Method used to save plpTest.
	 * 
	 * @param plpTest
	 * @return
	 */
	public void savePLPTest(PLPTest plpTest) {
		logger.debug("savePLPTest() Method Start in PLPTestServiceImp") ;
		getPlpTestDao().save(plpTest);
	}

	/**
	 * Method used to save plpTestList.
	 * 
	 * @param plpTestList
	 */
	public void savePLPTestList(List<PLPTest> plpTestList) {
		logger.debug("savePLPTestList() Method Start in PLPTestServiceImp") ;
		getPlpTestDao().savePLPTestList(plpTestList);
	}

}
