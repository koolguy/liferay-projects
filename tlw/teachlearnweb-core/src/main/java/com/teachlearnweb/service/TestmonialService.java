package com.teachlearnweb.service;


import java.util.List;

import com.teachlearnweb.dao.entity.Testmonial;
import com.teachlearnweb.service.vo.User;

/**
 * Service interface for the BookCatalog portlet.
 * 
 * @author asarin
 *
 */
public interface TestmonialService {
	
	public User postTestmonial(String desc, long userId) throws Exception;
	
	public int approveTestmonial(long testmonialId) throws Exception;

	public List<Testmonial> fetchTestMonial();	
}
