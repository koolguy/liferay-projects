package com.teachlearnweb.service;

import java.util.List;

import com.teachlearnweb.dao.entity.RewardPoints;

public interface RewardPointsService {

	public void addRewardPoints(long userId , long points,String description, String date);

	List<RewardPoints> rewardPointsByUserId(long userId);

	/**
	 * Method to fetch all reward points.
	 * 
	 * @return List<RewardPoints>
	 */
	List<RewardPoints> fetchRewardPoints();
}
