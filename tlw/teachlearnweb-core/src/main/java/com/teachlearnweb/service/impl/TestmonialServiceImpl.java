package com.teachlearnweb.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teachlearnweb.dao.TestmonialsDAO;
import com.teachlearnweb.dao.entity.Testmonial;
import com.teachlearnweb.service.TestmonialService;
import com.teachlearnweb.service.vo.User;

@Service(value = "testmonialService")
public class TestmonialServiceImpl implements TestmonialService {
	private static final Logger LOG = Logger.getLogger(TestmonialServiceImpl.class);

	@Autowired
	TestmonialsDAO testmonialsDao;

	public TestmonialsDAO getTestmonialsDao() {
		return testmonialsDao;
	}

	public void setTestmonialsDao(TestmonialsDAO testmonialsDao) {
		this.testmonialsDao = testmonialsDao;
	}

	public User postTestmonial(String desc, long userId) throws Exception {
		Testmonial testmonial = new Testmonial();
		testmonial.setDescription(desc);
		testmonial.setUserId(userId);
		testmonial.setStatus("pending");  
		//testmonial.hashCode();
	    //long id = testmonial.hashCode();
		
		//testmonial.setTestmonialId(id);
		
		testmonialsDao.postTestmonial(testmonial);
		
		return null;
	}
	
	public List<Testmonial> fetchTestMonial(){
		return testmonialsDao.fetchTestmonial();
	}
	
	public int approveTestmonial(long testId) throws Exception	
	{
		
		int count = testmonialsDao.approveTestmonial(testId);	
		
		return count;
		
	}

	
}
