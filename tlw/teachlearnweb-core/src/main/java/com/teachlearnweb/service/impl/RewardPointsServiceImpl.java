package com.teachlearnweb.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.service.UserLocalServiceUtil;

import com.teachlearnweb.dao.RewardPointsDAO;
import com.teachlearnweb.dao.UserCategoriesDAO;
import com.teachlearnweb.dao.UserDAO;
import com.teachlearnweb.dao.entity.RewardPoints;
import com.teachlearnweb.dao.entity.TLWUser;
import com.teachlearnweb.dao.entity.UserCategories;
import com.teachlearnweb.dao.impl.RewardPointsDAOImpl;
import com.teachlearnweb.service.RewardPointsService;
import com.teachlearnweb.service.UserService;
import com.teachlearnweb.service.vo.User;
import com.tlw.service.util.MyProfileServiceUtil;
import com.tlw.service.util.RegistrantionServiceUtil;

@Service(value = "rewardPointsService")
public class RewardPointsServiceImpl implements RewardPointsService {
	
	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil
			.getLog(RewardPointsServiceImpl.class);

	
	@Autowired
	RewardPointsDAO rewardPointsDao;		

	public RewardPointsDAO getRewardPointsDao() {
		return rewardPointsDao;
	}

	public void setRewardPointsDao(RewardPointsDAO rewardPointsDao) {
		this.rewardPointsDao = rewardPointsDao;
	}

	public List<RewardPoints> rewardPointsByUserId(long userId) {
		return rewardPointsDao.rewardPointsByUserId(userId);
	}

	public void addRewardPoints(long userId , long points,String description,String rewardDate) {
		RewardPoints rewardPoints = new RewardPoints();
		rewardPoints.setUserId(userId);
		rewardPoints.setRewardPoints(points);
		rewardPoints.setDescription(description);
		rewardPoints.setRewardDate(rewardDate);
		rewardPointsDao.addRewardPoints(rewardPoints);
	}

	/**
	 * Method to fetch all reward points.
	 * 
	 * @return List<RewardPoints>
	 */
	public List<RewardPoints> fetchRewardPoints() {
		logger.debug("fetchRewardPoints() Method Start in RewardPointsServiceImpl");
		return getRewardPointsDao().fetchRewardPoints();
	}

	

}
