package com.teachlearnweb.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teachlearnweb.dao.CouponCodeDAO;
import com.teachlearnweb.dao.TestmonialsDAO;
import com.teachlearnweb.dao.entity.CouponCode;
import com.teachlearnweb.dao.entity.Testmonial;
import com.teachlearnweb.service.CouponCodeService;
import com.teachlearnweb.service.TestmonialService;
import com.teachlearnweb.service.vo.User;

@Service(value = "couponCodeService")
public class CouponCodeServiceImpl implements CouponCodeService {
	
	@Autowired
	CouponCodeDAO couponCodeDAO;	

	public CouponCodeDAO getCouponCodeDAO() {
		return couponCodeDAO;
	}

	public void setCouponCodeDAO(CouponCodeDAO couponCodeDAO) {
		this.couponCodeDAO = couponCodeDAO;
	}

	public void addCouponcode(String couponCode, String description, String amount)
			throws Exception {
		
		CouponCode coupon= new CouponCode();
		coupon.setCouponCode(couponCode);
		coupon.setDescription(description);
		coupon.setAmount(amount);
		couponCodeDAO.addCouponcode(coupon);
		
	}

	public List<CouponCode> fetchCouponCodes() {
		return couponCodeDAO.fetchCouponCodes();
	}

	public CouponCode fetchCouponDetailByCode(String coupon) {		
		return couponCodeDAO.fetchCouponDetailByCode(coupon);
	}

	
	
}
