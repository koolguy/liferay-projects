package com.teachlearnweb.service.impl;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teachlearnweb.dao.AnswerDAO;
import com.teachlearnweb.dao.AssessmentDAO;
import com.teachlearnweb.dao.AssessmentDetailsDAO;
import com.teachlearnweb.dao.SkillDAO;
import com.teachlearnweb.dao.StyleCodeDAO;
import com.teachlearnweb.dao.UserDAO;
import com.teachlearnweb.dao.entity.DTAnswer;
import com.teachlearnweb.dao.entity.DTAssessment;
import com.teachlearnweb.dao.entity.DTAssessmentDetails;
import com.teachlearnweb.dao.entity.Skill;
import com.teachlearnweb.dao.entity.StyleCode;
import com.teachlearnweb.service.AssessmentDetailsService;
import com.tlw.assessment.vo.Assessment;
import com.tlw.assessment.vo.SkillWiseAssessmentReport;
import com.tlw.assessment.vo.StyleBasedAssessmentReport;

/**
 * 
 * @author Govinda
 */
@Service(value = "assessmentDetailsService")
public class AssessmentDetailsServiceImpl implements AssessmentDetailsService {
	// logger
	private static final Logger logger = Logger
			.getLogger(AssessmentDetailsServiceImpl.class);

	@Autowired
	StyleCodeDAO styleCodeDAO;
	/**
	 * Instance variable for assessmentDetailsDAO.
	 */
	@Autowired
	AnswerDAO answerDAO;

	@Autowired
	AssessmentDAO assessmentDAO;

	@Autowired
	AssessmentDetailsDAO assessmentDetailsDAO;

	@Autowired
	UserDAO userDAO;

	@Autowired
	SkillDAO skillDAO;
	
	/**
	 * @return the skillDAO
	 */
	public SkillDAO getSkillDAO() {
		return skillDAO;
	}
	/**
	 * @param skillDAO the skillDAO to set
	 */
	public void setSkillDAO(SkillDAO skillDAO) {
		this.skillDAO = skillDAO;
	}
	/**
	 * @return the answerDAO
	 */
	public AnswerDAO getAnswerDAO() {
		return answerDAO;
	}

	/**
	 * @param answerDAO the answerDAO to set
	 */
	public void setAnswerDAO(AnswerDAO answerDAO) {
		this.answerDAO = answerDAO;
	}

	/**
	 * @return the assessmentDAO
	 */
	public AssessmentDAO getAssessmentDAO() {
		return assessmentDAO;
	}

	/**
	 * @param assessmentDAO the assessmentDAO to set
	 */
	public void setAssessmentDAO(AssessmentDAO assessmentDAO) {
		this.assessmentDAO = assessmentDAO;
	}

	/**
	 * @return the assessmentDetailsDAO
	 */
	public AssessmentDetailsDAO getAssessmentDetailsDAO() {
		return assessmentDetailsDAO;
	}

	/**
	 * @param assessmentDetailsDAO
	 *            the assessmentDetailsDAO to set
	 */
	public void setAssessmentDetailsDAO(
			AssessmentDetailsDAO assessmentDetailsDAO) {
		this.assessmentDetailsDAO = assessmentDetailsDAO;
	}

	/**
	 * @return the userDAO
	 */
	public UserDAO getUserDAO() {
		return userDAO;
	}

	/**
	 * @param userDAO
	 *            the userDAO to set
	 */
	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

	public Assessment getAssessment(final int assessmentId) {
		if (logger.isDebugEnabled()) {
			logger.debug("START");
		}
		if (logger.isDebugEnabled()) {
			logger.debug("assessmentId::" + assessmentId);
		}
		Assessment assessment = new Assessment(assessmentId);
		try {
			List<DTAssessmentDetails> assessmentDetails = assessmentDetailsDAO
					.getAssessmentDetails(assessmentId);
			assessment.setQuestions(assessmentDetails);

			for (DTAssessmentDetails assessmentDetail : assessmentDetails) {
				// fetch options
				List<DTAnswer> answers = this.answerDAO.getAnswers(assessmentDetail.getDTQuestion().getQuestionId());
				assessment.getOptions().put(assessmentDetail.getDTQuestion().getQuestionId(), answers);
			}
			if (logger.isDebugEnabled()) {
				logger.debug("questions size::" + assessment.getQuestionCount());
			}
			DTAssessment dtAssessment = this.assessmentDAO
					.getAssessment(assessmentId);
//			Hibernate.initialize(dtAssessment.getSubjectMaster());
			assessment.setSubjectName(dtAssessment.getSubjectMaster().getSubjectName());
			if (logger.isDebugEnabled()) {
				logger.debug("subject name::" + assessment.getSubjectName());
			}
			assessment.setAssessmentDate(dtAssessment.getAssessmentDate());
			assessment.setTimeAllotted(dtAssessment.getMinutePerQuestion()
					.longValue());
			assessment.setStatus(dtAssessment.getStatus());
			assessment.setDtAssessment(dtAssessment);
			if (logger.isDebugEnabled()) {
				logger.debug("assessment::" + assessment);
			}
		} catch (Exception e) {
			logger.error("Exception occured..." + e, e);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("END");
		}
		return assessment;
	}

	// this is for style based exams
	public StyleBasedAssessmentReport getStyleBasedReport(final int assessmentId) {
		if (logger.isDebugEnabled()) {
			logger.debug("START");
		}
		if (logger.isDebugEnabled()) {
			logger.debug("assessmentId::" + assessmentId);
		}
		StyleBasedAssessmentReport report = null;
		Map<String, Integer> styleAnswerCount = null;
		try {
			int totalQCount = 0;
			List<StyleCode> styles = styleCodeDAO.getAllStyleCodes();
			if (styles != null) {
				report = new StyleBasedAssessmentReport();
				styleAnswerCount = new LinkedHashMap<String, Integer>();
				for (StyleCode styleCode : styles) {
					final List<DTAssessmentDetails> assessmentDetails = assessmentDetailsDAO
							.getStyleBasedAssessmentDetails(assessmentId,
									styleCode.getCodeId());
					if (assessmentDetails != null) {
						logger.debug("assessmentDetails.size: " + assessmentDetails.size());
						logger.debug("styleCode.getCodeId: " + styleCode.getCodeId());
						logger.debug("styleCode.getCodeDesc: " + styleCode.getStyleCode());
						totalQCount += assessmentDetails.size();
						if (styleCode.getCodeId() > 0) {
							styleAnswerCount.put(styleCode.getStyleCode(), assessmentDetails.size());
						} else {
							styleAnswerCount.put("Unanswered", assessmentDetails.size());
						}
					} else {
						logger.warn("No questions found for style "
								+ styleCode.getStyleCode());
					}
				}
//				for (String styleCode : stylePercents.keySet()) {
//					Float qCount = stylePercents.get(styleCode);
//					Float stylePercent = ((qCount / totalQCount) * 100.0f);
//					// set only percents more than zero
//					if (stylePercent > 0.0) {
//						stylePercents.put(styleCode, stylePercent);
//					} else {
//						stylePercents.remove(styleCode);
//					}
//				}
				report.setQuestionCount(totalQCount);
				report.setStylewiseAnswerCount(styleAnswerCount);
			} else {
				logger.warn("No styles found");
			}
		} catch (Exception e) {
			logger.error("Exception occured..." + e, e);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("END");
		}
		return report;
	}

	public Map<Short, SkillWiseAssessmentReport> getAssessmentReport(final int assessmentId) {
		if (logger.isDebugEnabled()) {
			logger.debug("START");
		}
		if (logger.isDebugEnabled()) {
			logger.debug("assessmentId::" + assessmentId);
		}
		Map<Short, SkillWiseAssessmentReport> reportDetails = null;
		try {
			Short subjectId = null;
			reportDetails = new LinkedHashMap<Short, SkillWiseAssessmentReport>();
			List<DTAssessmentDetails> assessmentDetails = assessmentDetailsDAO
													.getAssessmentDetails(assessmentId);

			for (DTAssessmentDetails assessDetail : assessmentDetails) {
				subjectId = assessDetail.getDTQuestion().getSubjectMaster().getSubjectId();
				logger.info("subjectId::" + subjectId);
				final Short skillId = assessDetail.getDTQuestion().getSkillId();
				SkillWiseAssessmentReport skillWiseReport = reportDetails.get(skillId);
				if (skillWiseReport == null) {
					logger.debug("skillId::" + skillId);
					skillWiseReport = new SkillWiseAssessmentReport(skillId);
					reportDetails.put(skillId, skillWiseReport);
				}
				// inc question count
				skillWiseReport.setQuestionCount(skillWiseReport.getQuestionCount() + 1);
				// add this question to this obj
				String actualAnswer = String.valueOf(assessDetail.getDTAnswer().getAnswerId());
				logger.info("qId: " + assessDetail.getDTQuestion().getQuestionId());
				logger.info("actualAnswer: " + actualAnswer);
				logger.info("userChoice: " + assessDetail.getAnswer());
				if (actualAnswer != null && actualAnswer.equals(assessDetail.getAnswer())) {
					// increment correctly answered count
					skillWiseReport.setCorrectCount(skillWiseReport.getCorrectCount() + 1);
				}
			}
			
			List<Skill> skills = this.skillDAO.getSkillsBySubject(subjectId);
			logger.info("skills:: " + skills);
			for (Skill skill : skills) {
				logger.info("skillId: " + skill.getId().getSkillId());
				logger.info("skillName: " + skill.getId().getSkillName());
				SkillWiseAssessmentReport skillWiseReport = reportDetails.get(skill.getId().getSkillId());
				if (skillWiseReport != null) {
					skillWiseReport.setSkillName(skill.getId().getSkillName());
					Float percent = (skillWiseReport.getCorrectCount() / Float.valueOf(skillWiseReport.getQuestionCount()) * 100.0f);
					skillWiseReport.setPercentage(percent);
					logger.info("qcount: " + skillWiseReport.getQuestionCount());
					logger.info("correct count: " + skillWiseReport.getCorrectCount());
					logger.info("Percentage: " + percent);
				}
			}
		} catch (Exception e) {
			logger.error("Exception occured..." + e, e);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("END");
		}
		return reportDetails;
	}

	/**
	 * To prepare Subject Test report
	 * @param assessmentId
	 * @return
	 */
	public Map<String, Integer> getSubjectTestReport(final int assessmentId) {
		if (logger.isDebugEnabled()) {
			logger.debug("START");
		}
		if (logger.isDebugEnabled()) {
			logger.debug("assessmentId::" + assessmentId);
		}
		Map<String, Integer> reportDetails = null;
		try {
			Short subjectId = null;
			Integer questionCount = 0;
			Integer correctCount = 0;
			Integer wrongCount = 0;
			Integer unAnsweredCount = 0;
			reportDetails = new LinkedHashMap<String, Integer>();
			List<DTAssessmentDetails> assessmentDetails = assessmentDetailsDAO
													.getAssessmentDetails(assessmentId);
			if (assessmentDetails != null) {
				questionCount = assessmentDetails.size();
				for (DTAssessmentDetails assessDetail : assessmentDetails) {
					subjectId = assessDetail.getDTQuestion().getSubjectMaster().getSubjectId();
					logger.info("subjectId::" + subjectId);

					String actualAnswer = String.valueOf(assessDetail.getDTAnswer().getAnswerId());
					logger.info("qId: " + assessDetail.getDTQuestion().getQuestionId());
					logger.info("actualAnswer: " + actualAnswer);
					logger.info("userChoice: " + assessDetail.getAnswer());
					if (actualAnswer != null && actualAnswer.equals(assessDetail.getAnswer())) {
						// increment correctly answered count
						correctCount += 1;
					} else if (assessDetail.getAnswer() == null || "".equals(assessDetail.getAnswer())) {
						unAnsweredCount += 1;
					}
				}
				wrongCount = questionCount - (correctCount + unAnsweredCount);
				// prepare map
				reportDetails.put("Answered Correct", correctCount);
				reportDetails.put("Answered Wrong", wrongCount);
				reportDetails.put("Unanswered", unAnsweredCount);
			} else {
				logger.error("No questions found for the assessment " + assessmentId);
			}
		} catch (Exception e) {
			logger.error("Exception occured..." + e, e);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("END");
		}
		return reportDetails;
	}

	/**
	 * Method to update assessmentDetail.
	 * 
	 * @param assessmentDetailsList
	 */
	public void updateAssessmentDetails(Assessment assessment) {
		logger.debug("START");
		this.assessmentDetailsDAO.updateAssessmentDetails(assessment.getQuestions());
		this.assessmentDAO.update(assessment.getDtAssessment());
		logger.debug("END");
	}
	/**
	 * Method to save assessmentDetailList.
	 * 
	 * @param assessmentDetailsList
	 */
	public void saveAssessmentDetailList(List<DTAssessmentDetails> assessmentDetailList) {
		logger.debug("AssessmentDetailsServiceImpl.saveAssessmentDetail() Method Start");
		if(assessmentDetailList != null && !assessmentDetailList.isEmpty() && assessmentDetailList.size() > 0){
			 getAssessmentDetailsDAO().saveAssessmentDetailsList(assessmentDetailList);
		}
		
	}

	/**
	 * Method to fetch DTAssessmentDetail by questionId.
	 * 
	 * @param questionId
	 * @return
	 */
	public DTAssessmentDetails getDTAssessmentDetailsByQuestionId(int questionId) {
		logger.debug("AssessmentDetailsServiceImpl.getDTAssessmentDetailsByQuestionId() Method Start");
		DTAssessmentDetails dtAssessmentDetails = getAssessmentDetailsDAO().getDTAssessmentDetailsByQuestionId(questionId);
		return dtAssessmentDetails;
	}
	
	/**
	 * Method to save assessmentDetail.
	 * 
	 * @param assessmentDetail
	 */
	public void saveAssessmentDetail(DTAssessmentDetails assessmentDetail) {
		logger.debug("AssessmentDetailsServiceImpl.saveAssessmentDetail() Method Start");
		 getAssessmentDetailsDAO().saveAssessmentDetail(assessmentDetail);
		
	}
	
	/**
	 * Method to fetch skill by skillName.
	 * 
	 * @param skillName
	 * @return
	 */
	public Skill getSkillBySkillName(String skillName) {
		logger.debug("AssessmentDetailsServiceImpl.getSkillBySkillName() Method Start");
		return getSkillDAO().getSkillBySkillName(skillName);
	}
}
