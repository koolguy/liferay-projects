package com.teachlearnweb.service.impl;

import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.teachlearnweb.dao.WorkSheetDAO;
import com.teachlearnweb.dao.entity.PLPStudentWrokSheet;
import com.teachlearnweb.service.WorkSheetService;

/**
 * Class is responsible for worksheet service related operations.
 * 
 * @author satya.
 *
 */

@Service(value="workSheetService")
public class WorkSheetServiceImpl implements WorkSheetService{

	// logger
	private static final Logger logger = Logger.getLogger(WorkSheetServiceImpl.class);
	
	@Autowired
	@Qualifier(value = "workSheetDAO")
	private WorkSheetDAO workSheetDAO;
	
	public WorkSheetDAO getWorkSheetDAO() {
		return workSheetDAO;
	}

	public void setWorkSheetDAO(WorkSheetDAO workSheetDAO) {
		this.workSheetDAO = workSheetDAO;
	}


	/**
	 * Method to fetch worksheets by worksheetId.
	 * 
	 * @return List<WorkSheet>
	 */
	public List<PLPStudentWrokSheet> getWorkSheetsByWorkSheetId(
			Integer workSheetId) {
		logger.debug("getWorkSheetsByWorkSheetId() Method Start in WorkSheetServiceImpl");
		return getWorkSheetDAO().getWorkSheetsByWorkSheetId(workSheetId);
	}

	/**
	 * Method to fetch worksheets by topicId.
	 * 
	 * @return List<WorkSheet>
	 */
	public List<PLPStudentWrokSheet> getEOCTestsByTopicId(Integer topicId, long userId, Short classId, Short subjectId) {
		logger.debug("getEOCTestsByTopicId(topicId, userId, classId, subjectId) Method Start in WorkSheetServiceImpl");
		return getWorkSheetDAO().getEOCTestsByTopicId(topicId, userId, classId, subjectId);
	}

	/**
	 * Method to fetch worksheets by subTopicId.
	 * 
	 * @return List<WorkSheet>
	 */
	public List<PLPStudentWrokSheet> getWorkSheetsBySubTopicId(
			Integer subTopicId, long userId, Short classId, Short subjectId) {
		logger.debug("getWorkSheetsBySubTopicId(subTopicId, userId, classId, subjectId) Method Start in WorkSheetServiceImpl");
		return getWorkSheetDAO().getWorkSheetsBySubTopicId(subTopicId, userId, classId, subjectId);
	}

}
