package com.teachlearnweb.service.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.teachlearnweb.dao.AnswerDAO;
import com.teachlearnweb.dao.AssessmentDAO;
import com.teachlearnweb.dao.ClassMasterDAO;
import com.teachlearnweb.dao.DTAnswerDAO;
import com.teachlearnweb.dao.DTQuestionDAO;
import com.teachlearnweb.dao.DTTestDAO;
import com.teachlearnweb.dao.DifficultyLevelsDAO;
import com.teachlearnweb.dao.SubjectsDAO;
import com.teachlearnweb.dao.entity.ClassMaster;
import com.teachlearnweb.dao.entity.DTAnswer;
import com.teachlearnweb.dao.entity.DTAssessment;
import com.teachlearnweb.dao.entity.DTQuestion;
import com.teachlearnweb.dao.entity.DTTest;
import com.teachlearnweb.dao.entity.DifficultyLevel;
import com.teachlearnweb.dao.entity.Skill;
import com.teachlearnweb.dao.entity.SubjectMaster;
import com.teachlearnweb.service.AssessmentService;
import com.tlw.assessment.vo.AssessmentConstants;

/**
 * Class is responsible for assessment service related operations.
 * 
 * @author satya.
 *
 */
@Service(value="assessmentService")
public class AssessmentServiceImpl implements AssessmentService {
	// logger
	private static final Logger logger = Logger.getLogger(AssessmentServiceImpl.class);

	@Autowired
	@Qualifier(value="difficultyLevelsDAO")
	private DifficultyLevelsDAO difficultyLevelsDao;

	@Autowired
	@Qualifier(value="subjectsDAO")
	private SubjectsDAO subjectsDao;

	@Autowired
	@Qualifier(value="assessmentDAO")
	private AssessmentDAO assessmentDao;

	@Autowired
	@Qualifier(value="dtAnswerDao")
	private DTAnswerDAO dtAnswerDao;
	
	@Autowired
	@Qualifier(value="dtQuestionsDao")
	private DTQuestionDAO dtQuestionsDao;
	
	@Autowired
	@Qualifier(value="dtTestDao")
	private DTTestDAO dtTestDao;
	
	@Autowired
	@Qualifier(value="answerDAO")
	private AnswerDAO answerDAO;
	
	@Autowired
	@Qualifier(value="classMasterDAO")
	private ClassMasterDAO classMasterDAO;

	public ClassMasterDAO getClassMasterDAO() {
		return classMasterDAO;
	}
	public void setClassMasterDAO(ClassMasterDAO classMasterDAO) {
		this.classMasterDAO = classMasterDAO;
	}
	/**
	 * @return the difficultyLevelsDao
	 */
	public DifficultyLevelsDAO getDifficultyLevelsDao() {
		return difficultyLevelsDao;
	}
	/**
	 * @param difficultyLevelsDao the difficultyLevelsDao to set
	 */
	public void setDifficultyLevelsDao(DifficultyLevelsDAO difficultyLevelsDao) {
		this.difficultyLevelsDao = difficultyLevelsDao;
	}
	/**
	 * @return the subjectsDao
	 */
	public SubjectsDAO getSubjectsDao() {
		return subjectsDao;
	}
	/**
	 * @param subjectsDao the subjectsDao to set
	 */
	public void setSubjectsDao(SubjectsDAO subjectsDao) {
		this.subjectsDao = subjectsDao;
	}

	public AnswerDAO getAnswerDAO() {
		return answerDAO;
	}

	public void setAnswerDAO(AnswerDAO answerDAO) {
		this.answerDAO = answerDAO;
	}

	public DTTestDAO getDtTestDao() {
		return dtTestDao;
	}

	public void setDtTestDao(DTTestDAO dtTestDao) {
		this.dtTestDao = dtTestDao;
	}

	public AssessmentDAO getAssessmentDao() {
		return assessmentDao;
	}

	public void setAssessmentDao(AssessmentDAO assessmentDao) {
		this.assessmentDao = assessmentDao;
	}

	public DTQuestionDAO getDtQuestionsDao() {
		return dtQuestionsDao;
	}

	public void setDtQuestionsDao(DTQuestionDAO dtQuestionsDao) {
		this.dtQuestionsDao = dtQuestionsDao;
	}

	public DTAnswerDAO getDtAnswerDao() {
		return dtAnswerDao;
	}

	public void setDtAnswerDao(DTAnswerDAO dtAnswerDao) {
		this.dtAnswerDao = dtAnswerDao;
	}

	/**
	 * Method to fetch questions randomly.
	 * 
	 * @return List<DTQuestion>
	 */
	public List<DTQuestion> getQuestions() {
		return getDtQuestionsDao().getQuestions();
	}
	
	/**
	 * Method to to fetch answers based on questionId.
	 * 
	 * @param questionId
	 * @return  List<DTAnswer>
	 */
	public List<DTAnswer> getAnswers(int questionId){
		return getDtAnswerDao().getAnswers(questionId);
		
	}
	
	/**
	 * Method to save DTAssessmentsList.
	 * 
	 * @param dtAssessmentList
	 */
	public void saveAssessmentList(List<DTAssessment> assessmentsList) {
		logger.debug("AssessmentServiceImpl.saveDTAssessment() Method Start");
		if(assessmentsList != null && !assessmentsList.isEmpty() && assessmentsList.size() > 0){
			 getAssessmentDao().saveDTAssessmentList(assessmentsList);
		}
	}
	
	/**
	 * Method to save DTAssessments.
	 * 
	 * @param assessment
	 */
	public void saveAssessment(DTAssessment assessment) {
		logger.debug("AssessmentServiceImpl.saveAssessment() Method Start");
		 getAssessmentDao().saveAssessment(assessment);
		
	}

	/**
	 * Method to fetch DTTest based on classId.
	 * 
	 * @param classId
	 * @return DTTest.
	 */
	public DTTest getDTTest(short classId) {
		logger.debug("AssessmentServiceImpl.getDTTest() Method Start");
		return getDtTestDao().getDTTest(classId);
	}

	/**
	 * Method to fetch DTAnswer based on questionId.
	 * 
	 * @param questionId
	 * @return
	 */
	public DTAnswer getAnswerForQuestion(Integer questionId) {
		
		return getAnswerDAO().getAnswerForQuestion(questionId);
	}

	public DTAssessment getAssessment(final Integer assessmentId) {
		return getAssessmentDao().getAssessment(assessmentId);
	}

	public SubjectMaster getSubjectByAssessment(final Integer assessmentId) {
		SubjectMaster subjectMaster = null;
		logger.debug("START");
		logger.debug("assessmentId: " + assessmentId);
		final DTAssessment assessment = getAssessmentDao().getAssessment(assessmentId);
		if (assessment != null) {
			logger.debug("SubjectId: " + assessment.getSubjectMaster().getSubjectId());
			subjectMaster = this.getSubject(assessment.getSubjectMaster().getSubjectId());
		}
		return subjectMaster;
	}

	/**
	 * To get all assessments list for a given user under a given test
	 * @param userId
	 * @param testId
	 * @return
	 * @throws HibernateException
	 */
	public List<DTAssessment> getAssessments(final Integer userId, final Short classId,
			final Short subjectId, final Short testId) throws HibernateException {
		if (logger.isDebugEnabled()) {
			logger.debug("START");
		}
		if (logger.isDebugEnabled()) {
			logger.debug("testId::" + testId);
			logger.debug("classId::" + classId);
			logger.debug("subjectId::" + subjectId);
		}
		List<DTAssessment> assessments = null;
		try {
			assessments = assessmentDao.getAssessments(userId, classId, subjectId, testId);
			if (logger.isDebugEnabled()) {
				logger.debug("assessment::" + assessments);
			}
		} catch (Exception e) {
			logger.error("Exception occured..." + e, e);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("END");
		}
		return assessments;
	}


	public List<DTAssessment> getDiagnosticTests(final Integer userId) throws HibernateException {
		if (logger.isDebugEnabled()) {
			logger.debug("START");
		}
		List<DTAssessment> dTests = null;
		dTests = this.getAssessments(userId, AssessmentConstants.TEST_TYPE_DIAGNOSTIC_TEST);
		if (dTests != null) {
			if (logger.isDebugEnabled()) {
				logger.debug("dTests.size: " + dTests.size());
			}
			final List<DTAssessment> styleBased = this.getAssessments(userId, AssessmentConstants.TEST_TYPE_DIAGNOSTIC_TEST_STYLE_BASED);
			if (styleBased != null && !styleBased.isEmpty()) {
				dTests.add(0, styleBased.get(0));
				if (logger.isDebugEnabled()) {
					logger.debug("styleBased.size: " + styleBased.size());
				}
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("END");
		}
		return dTests;
	}

	public List<DTAssessment> getAssessments(final Integer userId, final Short testId) throws HibernateException {
		return this.getAssessments(userId, null, testId);
	}

	public List<DTAssessment> getAssessments(final Integer userId, final Short classId, final Short testId) throws HibernateException {
		return this.getAssessments(userId, classId, null, testId);
	}

	public DifficultyLevel getDifficultyLevelByAssessment(final Integer assessmentId) throws HibernateException {
		if (logger.isDebugEnabled()) {
			logger.debug("START");
		}
		DifficultyLevel level = null;
		try {
			level = difficultyLevelsDao.getDifficultyLevelByAssessment(assessmentId);
			if (logger.isDebugEnabled()) {
				logger.debug("level::" + level);
			}
		} catch (Exception e) {
			logger.error("Exception occured..." + e, e);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("END");
		}
		return level;
	}

	public SubjectMaster getSubject(Short subjectId) {
		logger.debug("START");
		logger.debug("subjectId: " + subjectId);
		return subjectsDao.getSubject(subjectId);
	}

	/**
	 * to get all levels available for a given subject and class (from dt_question table)
	 * @param userId
	 * @param testId
	 * @return
	 * @throws HibernateException
	 */
	public List<DifficultyLevel> getDifficultiesBySubject(final short subjectId, final short classId) throws HibernateException {
		if (logger.isDebugEnabled()) {
			logger.debug("START");
		}
		List<DifficultyLevel> levels = null;
		try {
			levels = difficultyLevelsDao.getDifficultiesBySubject(subjectId, classId);
			if (logger.isDebugEnabled()) {
				logger.debug("levels::" + levels);
			}
		} catch (Exception e) {
			logger.error("Exception occured..." + e, e);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("END");
		}
		return levels;
	}
	
	/**
	 * Method to fetch questions based on classId and subjectId.
	 * 
	 * @return List<DTQuestion>
	 */
	public List<DTQuestion> getQuestions(short classId, short subjectId) {
		logger.debug("getQuestions(classId, subjectId) Method start in AssessmentServiceimpl");
		return getDtQuestionsDao().getQuestions(classId, subjectId);
	}
	/**
	 * Method to fetch questions based on classId and subjectId.
	 * @author govinda 
	 * @return List<DTQuestion>
	 */
	public List<DTQuestion> getQuestions(final Short classId, final Short subjectId, final Short levelId) {
		logger.debug("getQuestions(classId, subjectId) Method start in AssessmentServiceimpl");
		return getDtQuestionsDao().getQuestions(classId, subjectId, levelId);
	}
	public List<DTQuestion> getStyleBasedQuestions() {
		logger.debug("getStyleBasedQuestions() Method start in AssessmentServiceimpl");
		return getDtQuestionsDao().getStyleBasedQuestions();
	}
	/**
	 * Method to fetch questions based on classId and subjectId.
	 * @author govinda 
	 * @return List<DTQuestion>
	 */
	public List<DTQuestion> getSkillwiseQuestions(final List<Skill> skills, final Short classId,
			final Short subjectId, final Short questionsPerSkill) {
		return getDtQuestionsDao().getSkillwiseQuestions(skills, classId, subjectId, questionsPerSkill);
	}
	public List<Skill> getSkills(short subjectId) {
		return getDtQuestionsDao().getSkills(subjectId);
	}
	/**
	 * Method to fetch subjectMaster by subjectName.
	 * 
	 * @param subjectName
	 * @return
	 */
	public SubjectMaster getSubjectMasterbySbujectName(String subjectName) {
		logger.debug("getSubjectMasterbySbujectName() Method start in AssessmentServiceimpl");
		return subjectsDao.getSubjectMasterbySbujectName(subjectName);
	}
	
	/**
	 * Method to fecth classMaster by classId.
	 * 
	 * @param classId
	 * @return
	 */
	public ClassMaster getClassMasterByClassId(short classId) {
		logger.debug("getClassMasterByClassId() Method start in AssessmentServiceimpl");
		return getClassMasterDAO().getClassMasterByClassId(classId);
	}
	
	/**
	 * Method to fetch DTTest based on classId and subjectId.
	 * 
	 * @param classId
	 * @param subjectId
	 * @return DTTest.
	 */
	public DTTest getDTTest(short classId, short subjectId) {
		logger.debug("getDTTest(classId, subjectId) Method start in AssessmentServiceimpl");
		return getDtTestDao().getDTTest(classId, subjectId);
	}
	
	/**
	 * Method to fetch classMaster by className.
	 * 
	 * @param className
	 * @return
	 */
	public ClassMaster getClassMasterByClassName(String className) {
		logger.debug("getClassMasterByClassName() Method start in AssessmentServiceimpl");
		return getClassMasterDAO().getClassMasterByClassName(className);
	}
	//@Override
	public int getTestsFinishedByStatusAndUserId(long userId, String status) {
		logger.debug("getTestsFinishedByStatusAndUserId(userid, status) Method Start.");
		return getAssessmentDao().getTestsFinishedByStatusAndUserId(userId, status);	
	}
	
	/**
	 * Method to fetch all completed dt assessments by subject tests.
	 * 
	 * @param testId
	 * @param status
	 * @return
	 */
	public List<DTAssessment> fetchAllCompletedDTAssessmentsBySubjectTest(Short testId, String status) {
		logger.debug("fetchAllCompletedDTAssessmentsBySubjectTest(testid, status) Method Start.");
		return getAssessmentDao().fetchAllCompletedDTAssessmentsBySubjectTest(testId, status);	

	}
	
}