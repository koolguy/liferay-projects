package com.teachlearnweb.service;

/**
 * @author GalaxE.
 * 
 */
public interface ServiceLocator {
	public BookService getBookService();
	public UserService getUserService();	
	public RewardPointsService getRewardPointsService();
	public AssessmentService getAssessmentService();
	public AssessmentDetailsService getAssessmentDetailsService();
	public WorkSheetService getWorkSheetService();
	public PLPQuestionsService getPlpQuestionService();
	public PLPAnswerService getPlpAnswerService();
	public PLPTestService getPlpTestService();
	public PLPStudentWorkSheetService getPlpStudentWorkSheetService();
	public TopicMasterService getTopicMasterService();
	public TestmonialService getTestmonialService(); 
	public CouponCodeService getCouponCodeService();
	
}
