package com.teachlearnweb.service;

import java.util.List;
import org.springframework.stereotype.Component;
import com.teachlearnweb.dao.entity.PLPStudentWrokSheet;


/**
 * interface declares worksheet service related operations.
 * 
 * @author satya
 *
 */

@Component
public interface WorkSheetService {

	/**
	 * Method to fetch worksheets by worksheetId.
	 * 
	 * @return List<WorkSheet>
	 */
	public List<PLPStudentWrokSheet> getWorkSheetsByWorkSheetId(Integer workSheetId);
	
	
	/**
	 * Method to fetch worksheets by topicId.
	 * 
	 * @return List<WorkSheet>
	 */
	public  List<PLPStudentWrokSheet> getEOCTestsByTopicId(Integer topicId, long userId, Short classId, Short subjectId);
	
	
	/**
	 * Method to fetch worksheets by subTopicId.
	 * 
	 * @return List<WorkSheet>
	 */
	public  List<PLPStudentWrokSheet> getWorkSheetsBySubTopicId(Integer subTopicId, long userId, Short classId, Short subjectId);
		
	
}
