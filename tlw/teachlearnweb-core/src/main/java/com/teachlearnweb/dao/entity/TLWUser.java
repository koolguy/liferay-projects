
package com.teachlearnweb.dao.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.teachlearnweb.dao.entity.User;

/**
 * Class is used to capture the characteristics of ci_employee. 
 * This is reusable class for ci_employee.
 * @author GalaxE.
 */
@Entity
@Table(name = "tlw_user")
public class TLWUser implements java.io.Serializable {

		
	private static final long serialVersionUID = -2828861827483771607L;
	// Fields

	private Long userId;
	private String parentName;
	private String schoolName;
	private String className;
	private Boolean leadershipIndicator;
	private Boolean enrollmentBenefitIndicator;
	private String sectionName;
	private Timestamp hireDate;
	private String schoolCity;
	private String schoolLocality;
	private String syllabus;
	private String phone;
	private String jobTitle;

	// Constructors

	/** default constructor */
	public TLWUser() {

	}

	/** minimal constructor */
	public TLWUser(
		Long userId, User userByUserId, String schoolName, String className, Boolean leadershipIndicator, Boolean enrollmentBenefitIndicator,
		String sectionName) {

		this.userId = userId;
		this.schoolName = schoolName;
		this.className = className;
		this.leadershipIndicator = leadershipIndicator;
		this.enrollmentBenefitIndicator = enrollmentBenefitIndicator;
		this.sectionName = sectionName;
	}

	/** full constructor */
	public TLWUser(
		Long userId, User userBySupervisorId, User userByAdminAssistId, User userByUserId, String parentName, String schoolName,
		String className, Boolean leadershipIndicator, Boolean enrollmentBenefitIndicator, String sectionName, Timestamp hireDate) {

		this.userId = userId;
		this.parentName = parentName;
		this.schoolName = schoolName;
		this.className = className;
		this.leadershipIndicator = leadershipIndicator;
		this.enrollmentBenefitIndicator = enrollmentBenefitIndicator;
		this.sectionName = sectionName;
		this.hireDate = hireDate;
	}

	// Property accessors
	/**
	 * Getter method for userId
	 * @return userId
	 */
	@Id
	@Column(name = "userId", unique = true, nullable = false)
	public Long getUserId() {

		return this.userId;
	}

	/**
	 * Setter method for userId
	 * @param userId
	 */
	public void setUserId(Long userId) {

		this.userId = userId;
	}

	
	/**
	 * Getter method for parentName
	 * @return parentName String
	 */
	@Column(name = "parentName", nullable = true, length = 25)
	public String getParentName() {

		return this.parentName;
	}

	/**
	 * Setter method for parentName
	 * @param parentName
	 */
	public void setParentName(String parentName) {

		this.parentName = parentName;
	}

	/**
	 * Getter method for schoolName
	 * @return schoolName String
	 */
	@Column(name = "schoolName", nullable = true, length = 50)
	public String getSchoolName() {

		return this.schoolName;
	}

	/**
	 * Setter method for schoolName
	 * @param schoolName
	 */
	public void setSchoolName(String schoolName) {

		this.schoolName = schoolName;
	}

	/**
	 * Getter method for className
	 * @return className String
	 */
	@Column(name = "className", nullable = false, length = 25)
	public String getClassName() {

		return this.className;
	}

	/**
	 * Setter method for className
	 * @param className
	 */
	public void setClassName(String className) {

		this.className = className;
	}

	/**
	 * Getter method for leadershipIndicator
	 * @return leadershipIndicator
	 */
	@Column(name = "leadershipIndicator", nullable = false)
	public Boolean getLeadershipIndicator() {

		return this.leadershipIndicator;
	}

	/**
	 * Setter method for leadershipIndicator
	 * @param leadershipIndicator
	 */
	public void setLeadershipIndicator(Boolean leadershipIndicator) {

		this.leadershipIndicator = leadershipIndicator;
	}

	/**
	 * Getter method for enrollmentBenefitIndicator
	 * @return enrollmentBenefitIndicator
	 */
	@Column(name = "enrollmentBenefitIndicator", nullable = false)
	public Boolean getEnrollmentBenefitIndicator() {

		return this.enrollmentBenefitIndicator;
	}

	/**
	 * Setter method for enrollmentBenefitIndicator
	 * @param enrollmentBenefitIndicator
	 */
	public void setEnrollmentBenefitIndicator(Boolean enrollmentBenefitIndicator) {

		this.enrollmentBenefitIndicator = enrollmentBenefitIndicator;
	}

	/**
	 * Getter method for department
	 * @return department String
	 */
	@Column(name = "sectionName", nullable = true, length = 100)
	public String getSectionName() {

		return this.sectionName;
	}

	/**
	 * Setter method for department
	 * @param department
	 */
	public void setSectionName(String sectionName) {

		this.sectionName = sectionName;
	}

	/**
	 * Getter method for hireDate
	 * @return hireDate of type Timestamp
	 */
	@Column(name = "hireDate", nullable = true, length = 0)
	public Timestamp getHireDate() {

		return this.hireDate;
	}

	/**
	 * Setter method for hireDate
	 * @param hireDate
	 */
	public void setHireDate(Timestamp hireDate) {

		this.hireDate = hireDate;
	}
	
	@Column(name = "schoolCity", nullable = true, length = 100)
	public String getSchoolCity() {
		return schoolCity;
	}

	public void setSchoolCity(String schoolCity) {
		this.schoolCity = schoolCity;
	}

	@Column(name = "schoolLocality", nullable = true, length = 100)
	public String getSchoolLocality() {
		return schoolLocality;
	}

	public void setSchoolLocality(String schoolLocality) {
		this.schoolLocality = schoolLocality;
	}

	@Column(name = "syllabus", nullable = false, length = 100)
	public String getSyllabus() {
		return syllabus;
	}

	public void setSyllabus(String syllabus) {
		this.syllabus = syllabus;
	}

	@Column(name = "phone", nullable = false, length = 100)
	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	@Column(name = "jobTitle", nullable = false, length = 100)
	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

}
