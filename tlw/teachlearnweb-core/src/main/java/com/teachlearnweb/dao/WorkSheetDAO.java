package com.teachlearnweb.dao;

import java.util.List;
import org.springframework.stereotype.Component;
import com.teachlearnweb.dao.entity.PLPStudentWrokSheet;

/**
 * Interface WorkSheetDAO declares DML operations on tlw_plp_studentworksheet table.
 * 
 * @author satya
 *
 */

@Component
public interface WorkSheetDAO extends GenericDAO<PLPStudentWrokSheet>{

	/**
	 * Method to fetch worksheets by worksheetId.
	 * 
	 * @return List<WorkSheet>
	 */
	public List<PLPStudentWrokSheet> getWorkSheetsByWorkSheetId(Integer workSheetId);
	
	
	/**
	 * Method to fetch worksheets by topicId.
	 * 
	 * @return List<WorkSheet>
	 */
	public  List<PLPStudentWrokSheet> getEOCTestsByTopicId(Integer topicId, long userId, Short classId, Short subjectId);
	
	
	/**
	 * Method to fetch worksheets by subTopicId.
	 * 
	 * @return List<WorkSheet>
	 */
	public  List<PLPStudentWrokSheet> getWorkSheetsBySubTopicId(Integer subTopicId, long userId, Short classId, Short subjectId);
	
	
}
