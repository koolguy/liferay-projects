package com.teachlearnweb.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.PLPQuestionsDAO;
import com.teachlearnweb.dao.entity.PLPQuestion;
import com.tlw.assessment.vo.AssessmentConstants;

/**
 * Class provides the implementations of PLPQuestionsDAO Interface to perform DML
 * operations.
 * 
 * @author satya.
 */

@Repository(value = "plpQuestionDAO")
@Transactional
public class PLPQuestionsDAOImpl extends AbstractDAO<PLPQuestion> implements PLPQuestionsDAO{

	// logger
	private static final Logger logger = Logger.getLogger(PLPQuestionsDAOImpl.class);
	
	/**
	 * Method to fetch question.
	 * 
	 * @param questionId
	 * @return PLPQuestion
	 */
	public List<PLPQuestion> getQuestionById(Integer questionId) {
		logger.debug("getQuestionById() Method Start in PLPQuestionsDAOImpl");
		List<PLPQuestion> plpQuestionsList = null;
		final StringBuilder queryString = new StringBuilder();
		try{
			queryString.append("from PLPQuestion as question where question.questionId = ? and question.skillId != "+AssessmentConstants.CONSTATN_ZERO);
			if(logger.isDebugEnabled()){
				logger.debug("questionId------------->"+questionId);
			}
			plpQuestionsList = (List<PLPQuestion>) hibernateTemplate.find(queryString.toString(), questionId);
			if(plpQuestionsList != null && !plpQuestionsList.isEmpty() && plpQuestionsList.size() > 0){
				if(logger.isDebugEnabled()){
					logger.debug("plpQuestionsList.size()------------->"+plpQuestionsList.size());
				}
			}
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in PLPQuestionsDAOImpl.getQuestionById()----", ex);
			throw ex;
		}
		return plpQuestionsList;
	}
	
	/**
	 * Method to fetch list of questions.
	 * 
	 * @param questionIdsList
	 * @return List<PLPQuestion> 
	 */
	public List<PLPQuestion> getQuestionsListByQIds(String questionIdsList){
		List<String> questionIdList = getQuestionsList(questionIdsList);
		List<PLPQuestion> plpQuestionsList = new ArrayList<PLPQuestion>();
		for(String questionId : questionIdList) {
			plpQuestionsList.addAll(getQuestionById(Integer.parseInt(questionId)));
		}
		return plpQuestionsList;
	}
	
	/**
	 * Helper Method to create list of questionIds.
	 * 
	 * @param questionIdsList
	 * @return List<String>
	 */
	 public List<String> getQuestionsList(String questionIdsList){
			List<String> tempList =null;
			String[] tempArray = null;
			if(questionIdsList != null && questionIdsList.contains(",")){
				tempArray = questionIdsList.split(",");
			}
			if(tempArray != null && tempArray.length > 0){
				tempList = new ArrayList<String>();
				for(int i=0; i<tempArray.length;i++){
					tempList.add(tempArray[i]);
				}
			}
			if(tempList != null && !tempList.isEmpty()){
				for(String str : tempList){
					logger.debug("questionid---------------------"+str);
				}
				return tempList;
			}else{
				tempList = new ArrayList<String>();
				return tempList;
			}
		}
	 

	 /**
		 * Method to updated answer in plpQuestion table for particular question.
		 * 
		 * @param questionId
		 */
	public void updateAnswerIdForQuestion(PLPQuestion question) throws HibernateException {
		logger.debug("updateAnswerIdForQuestion() Method Start in PLPQuestionsDAOImpl.");
		saveOrUpdate(question);
		
	}

	/**
	 * Method to fetch plpQuestions by skillId.
	 * @param skillId
	 * @return
	 */
	public List<PLPQuestion> getQuestions(Short classId, Short subjectId, Short skillId, Integer subTopicId, Integer topicId) {
		logger.debug("getQuestions() Method Start in PLPQuestionsDAOImpl.");
		System.out.println("DAO Start................................");
		List<PLPQuestion> plpQuestionsList = null;
		final StringBuilder queryString = new StringBuilder();
		try{
			queryString.append("from PLPQuestion as question where question.skillId=? and question.classId=? and question.subjectId=? and question.subTopicId=? and question.topicId=? order by rand()");
			hibernateTemplate.setMaxResults(20);
			if(logger.isDebugEnabled()){
				logger.debug("classId------------->"+classId);
				logger.debug("subjectId------------->"+subjectId);
				logger.debug("skillId------------->"+skillId);
				logger.debug("topicId------------->"+topicId);
				logger.debug("subTopicId------------->"+subTopicId);
			}
			System.out.println("classId------------->"+classId);
			System.out.println("subjectId------------->"+subjectId);
			System.out.println("skillId------------->"+skillId);
			System.out.println("topicId------------->"+topicId);
			System.out.println("subTopicId------------->"+subTopicId);
			plpQuestionsList = (List<PLPQuestion>) hibernateTemplate.find(queryString.toString(), skillId ,classId, subjectId,subTopicId, topicId);
			System.out.println("plpQuestionsList size in DAO.................."+plpQuestionsList.size());
			if(plpQuestionsList != null && !plpQuestionsList.isEmpty()){
				logger.debug("plpQuestionsList.size()------------->"+plpQuestionsList.size());
			} else {
				logger.debug("plpQuestionsList is empty");
			}
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in PLPQuestionsDAOImpl.getQuestions()----", ex);
			throw ex;
		}
		System.out.println("DAO End................................");
		return plpQuestionsList;
	}

	/**
	 * Method to fetch questions based on userId, classId, topicId and subjectId.
	 * 
	 * @param userid
	 * @param classId
	 * @param subjectId
	 * @param topicId
	 * @return
	 */
	public List<PLPQuestion> getQuestions(Short classId, Short subjectId, Short skillId, Integer topicId) {
		logger.debug("getQuestions(userId, classId, subjectId, topicId) Method Start in PLPQuestionsDAOImpl.");
		List<PLPQuestion> plpQuestionsList = null;
		final StringBuilder queryString = new StringBuilder();
		try{
			queryString.append("from PLPQuestion as question where question.classId = ? and question.subjectId = ? and question.skillId = ? and question.topicId = ? order by rand()");
			hibernateTemplate.setMaxResults(20);
			if(logger.isDebugEnabled()){
				logger.debug("userid------------->"+skillId);
				logger.debug("classId------------->"+classId);
				logger.debug("subjectId------------->"+subjectId);
				logger.debug("topicId------------->"+topicId);
			}
			plpQuestionsList = (List<PLPQuestion>) hibernateTemplate.find(queryString.toString(), classId, subjectId, skillId, topicId);
			if(plpQuestionsList != null && !plpQuestionsList.isEmpty() && plpQuestionsList.size() > 0){
				if(logger.isDebugEnabled()){
					logger.debug("plpQuestionsList.size()------------->"+plpQuestionsList.size());
				}
			}
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in PLPQuestionsDAOImpl.getQuestions()----", ex);
			throw ex;
		}
		return plpQuestionsList;
	}

	/**
	 * Method to fetch plpquestion by questionId.
	 * 
	 * @param questionsId
	 * @return
	 */
	public PLPQuestion getQuestion(Integer questionsId) {
		logger.debug("getQuestion() Method Start in PLPQuestionsDAOImpl");
		PLPQuestion question = null;
		try {
			question = findById(PLPQuestion.class, questionsId);
			if(question != null && question.getQuestionId() != null) {
				logger.debug("question.getPLPQuestion()--from PLPQuestionDAOIMPL--------->"+question.getQuestion());
			}
		} catch (HibernateException e) {
			logger.error("Hibernate Exception Caught While Fetching Question by questionId.", e);
			e.printStackTrace();
		}
		return question;
	}

	/**
	 * Method to fetch questions.
	 * 
	 * @param userid
	 * @param classId
	 * @param subjectId
	 * @param topicId
	 * @param levelDesc
	 * @return
	 */
	public List<PLPQuestion> getQuestions(Short classId, Short subjectId, Short skillId, Integer topicId, Short levelId) {
		logger.debug("getQuestions(classId, subjectId, skillId, topicId, leveldesc) Method Start in PLPQuestionsDAOImpl.");
		List<PLPQuestion> plpQuestionsList = null;
		final StringBuilder queryString = new StringBuilder();
		try{
			queryString.append("from PLPQuestion as question where question.classId = ? and question.subjectId = ? and question.skillId = ? and question.topicId = ? and question.levelId = ? order by rand()");
			hibernateTemplate.setMaxResults(5);
			if(logger.isDebugEnabled()){
				logger.debug("userid------------->"+skillId);
				logger.debug("classId------------->"+classId);
				logger.debug("subjectId------------->"+subjectId);
				logger.debug("topicId------------->"+topicId);
			}
			plpQuestionsList = (List<PLPQuestion>) hibernateTemplate.find(queryString.toString(), classId, subjectId, skillId, topicId, levelId);
			if(plpQuestionsList != null && !plpQuestionsList.isEmpty() && plpQuestionsList.size() > 0){
				if(logger.isDebugEnabled()){
					logger.debug("plpQuestionsList.size()------------->"+plpQuestionsList.size());
				}
			}
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in PLPQuestionsDAOImpl.getQuestions()----", ex);
			throw ex;
		}
		return plpQuestionsList;
	}

}
