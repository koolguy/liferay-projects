package com.teachlearnweb.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.teachlearnweb.dao.entity.DTQuestion;
import com.teachlearnweb.dao.entity.DifficultyLevel;
import com.teachlearnweb.dao.entity.Skill;
import com.teachlearnweb.dao.entity.SubjectMaster;

/**
 * Class is responsible for dtQuestion related operations.
 * 
 * @author satya.
 *
 */
@Component
public interface DTQuestionDAO extends GenericDAO<DTQuestion> {
	
	/**
	 * Method to fetch questions randomly.
	 * 
	 * @return List<DTQuestion>
	 */
	public List<DTQuestion> getQuestions();
	
	/**
	 * Method to fetch subjects.
	 * 
	 * @return  List<SubjectMaster> 
	 */
	public List<SubjectMaster> getSubjects();
	
	/**
	 * Method to fetch difficultyLevels.
	 * 
	 * @return  List<SubjectMaster> 
	 */
	public List<DifficultyLevel> getDifficultyLevels();
	
	/**
	 * Method to fetch skill based on subject.
	 * 
	 * @param subjectId
	 * @return List<Skill>
	 */
	public List<Skill> getSkills(short subjectId);
	
	/**
	 * Method to fetch skill.
	 * 
	 * @param subjectId
	 * @return List<Skill>
	 */
	public List<Skill> getSkills();
	
	/**
	 * Method to fetch questions based on classId and subjectId.
	 * 
	 * @return List<DTQuestion>
	 */
	public List<DTQuestion> getQuestions(short classId, short subjectId);
	/**
	 * @author govinda
	 * @param classId
	 * @param subjectId
	 * @param levelId
	 * @return
	 */
	public List<DTQuestion> getQuestions(final Short classId, final Short subjectId, final Short levelId);	
	public List<DTQuestion> getStyleBasedQuestions();
	/**
	 * Method to fetch questions based on classId and subjectId.
	 * @author govinda 
	 * @return List<DTQuestion>
	 */
	public List<DTQuestion> getSkillwiseQuestions(final List<Skill> skills, final Short classId,
			final Short subjectId, final Short questionsPerSkill);
}
