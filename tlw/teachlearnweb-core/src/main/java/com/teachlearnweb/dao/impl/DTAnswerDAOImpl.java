package com.teachlearnweb.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.kernel.util.Validator;
import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.DTAnswerDAO;
import com.teachlearnweb.dao.entity.DTAnswer;

/**
 * Class is responsible for dtAnswer related operations.
 * 
 * @author satya.
 *
 */

@Repository(value="dtAnswerDao")
@Transactional
public class DTAnswerDAOImpl extends AbstractDAO<DTAnswer> implements DTAnswerDAO{

	/** The logger. */
    private static final Logger logger = Logger.getLogger(DTAnswerDAOImpl.class);
    
	/**
	 * Method to to fetch answers based on questionId.
	 * 
	 * @param questionId
	 * @return  List<DTAnswer>
	 */
	@SuppressWarnings("unchecked")
	public List<DTAnswer> getAnswers(int questionId) {
		List<DTAnswer> answersList = null;
		Session session = getSession();
		Transaction tx= session.beginTransaction();
		try {
			Query query = session.createQuery("from DTAnswer as dta where dta.DTQuestion.questionId =:questionId");
			query.setParameter("questionId", questionId);
			answersList = query.list();
			tx.commit();
		} catch (Exception e) {
			if(tx != null)
				tx.rollback();
			logger.error("Exception Caught while Processing DTAnswerDAOImpl.getAnswers()", e);
		}
		if(Validator.isNotNull(answersList)){
			logger.debug("answersList.size() in DTAnswerDAOImpl.getAnswers()----->"+answersList.size());
			return answersList;
		} else {
			return new ArrayList<DTAnswer>();
		}
	}
	
}
