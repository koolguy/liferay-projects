package com.teachlearnweb.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.AssessmentDetailsDAO;
import com.teachlearnweb.dao.DTTestDAO;
import com.teachlearnweb.dao.entity.ClassMaster;
import com.teachlearnweb.dao.entity.DTAssessmentDetails;
import com.teachlearnweb.dao.entity.DTTest;

/**
 * Class provides the implementations of ClassMasterDAO Interface to perform DML
 * operations.
 * 
 * @author satya.
 */

@Repository(value = "dtTestDao")
@Transactional
public class DTTestDAOImpl extends AbstractDAO<DTTest> implements DTTestDAO {
	
	// logger
		private static final Logger logger = Logger.getLogger(DTTestDAOImpl.class);

		/**
		 * Method to fetch classMaster based on classId.
		 * 
		 * @param classId
		 * @return
		 */
		public DTTest getDTTest(short classId) {
			logger.debug("getDTTest() Method Start in DTTestDAOImpl");
			logger.debug("classId in DTTestDAOImpl.getDTTest()--->"+classId);
			
			DTTest dtTest = findById(DTTest.class, classId);
			
			return dtTest;
		}

		/**
		 * Method to fetch DTTest based on classId and subjectId.
		 * 
		 * @param classId
		 * @return DTTest.
		 */
		public DTTest getDTTest(short classId, short subjectId) {
			logger.debug("getDTTest(classId, subjectId) Method Start in DTTestDAOImpl");
			logger.debug("classId in DTTestDAOImpl.getDTTest(classId, subjectId)--->"+classId);
			logger.debug("subjectId in DTTestDAOImpl.getDTTest(classId, subjectId)--->"+subjectId);
			
			DTTest dtTest = null;
			List<DTTest> dtTestList = null;
			final StringBuilder queryString = new StringBuilder();
			try{
				queryString.append("from DTTest as test where test.classMaster.classId = ? and test.subjectMaster.subjectId = ?");
				if(logger.isDebugEnabled()){
					logger.debug("classId------------->"+classId);
					logger.debug("subjectId------------->"+subjectId);
				}
				dtTestList = (List<DTTest>) hibernateTemplate.find(queryString.toString(), classId, subjectId);
				if(dtTestList != null && !dtTestList.isEmpty() && dtTestList.size() > 0){
					if(logger.isDebugEnabled()){
						logger.debug("dtTest------------->"+dtTest);
					}
					dtTest = dtTestList.get(0);
				}
				
			}catch(HibernateException ex) {
				logger.error("Hibernate Exception Caught in DTTestDAOImpl.getDTTest()----", ex);
				throw ex;
			}
			return dtTest;
		}

}
