package com.teachlearnweb.dao.entity;
// default package
// Generated Jan 28, 2013 10:08:44 PM by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * DTQuestion generated by hbm2java
 */
@Entity
@Table(name = "tlw_dt_question", catalog = "tlw")
public class DTQuestion implements java.io.Serializable {

	private Integer questionId;
	private DifficultyLevel difficultyLevel;
	private ClassMaster classMaster;
	private QuestionType questionType;
	private SubjectMaster subjectMaster;
	private BookMaster bookMaster;
	private String errorComment;
	private BigDecimal marksAllotted;
	private String question;
	private short skillId;
	private String solution;
	private int subTopicId;
	private int topicId;
	private Integer dependentId;
	private Set<DTAnswer> DTAnswers = new HashSet<DTAnswer>(0);
	private Set<DTAssessmentDetails> DTAssessmentDetailses = new HashSet<DTAssessmentDetails>(
			0);

	public DTQuestion() {
	}

	public DTQuestion(DifficultyLevel difficultyLevel, ClassMaster classMaster,
			QuestionType questionType, SubjectMaster subjectMaster,
			BookMaster bookMaster, BigDecimal marksAllotted, short skillId,
			int subTopicId, int topicId) {
		this.difficultyLevel = difficultyLevel;
		this.classMaster = classMaster;
		this.questionType = questionType;
		this.subjectMaster = subjectMaster;
		this.bookMaster = bookMaster;
		this.marksAllotted = marksAllotted;
		this.skillId = skillId;
		this.subTopicId = subTopicId;
		this.topicId = topicId;
	}

	public DTQuestion(DifficultyLevel difficultyLevel, ClassMaster classMaster,
			QuestionType questionType, SubjectMaster subjectMaster,
			BookMaster bookMaster, String errorComment, BigDecimal marksAllotted,
			String question, short skillId, String solution, int subTopicId,
			int topicId, Integer dependentId, Set<DTAnswer> DTAnswers,
			Set<DTAssessmentDetails> DTAssessmentDetailses) {
		this.difficultyLevel = difficultyLevel;
		this.classMaster = classMaster;
		this.questionType = questionType;
		this.subjectMaster = subjectMaster;
		this.bookMaster = bookMaster;
		this.errorComment = errorComment;
		this.marksAllotted = marksAllotted;
		this.question = question;
		this.skillId = skillId;
		this.solution = solution;
		this.subTopicId = subTopicId;
		this.topicId = topicId;
		this.dependentId = dependentId;
		this.DTAnswers = DTAnswers;
		this.DTAssessmentDetailses = DTAssessmentDetailses;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "QuestionId", unique = true, nullable = false)
	public Integer getQuestionId() {
		return this.questionId;
	}

	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "LevelId", nullable = false)
	public DifficultyLevel getDifficultyLevel() {
		return this.difficultyLevel;
	}

	public void setDifficultyLevel(DifficultyLevel difficultyLevel) {
		this.difficultyLevel = difficultyLevel;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ClassId", nullable = false)
	public ClassMaster getClassMaster() {
		return this.classMaster;
	}

	public void setClassMaster(ClassMaster classMaster) {
		this.classMaster = classMaster;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "QuestionTypeId", nullable = false)
	public QuestionType getQuestionType() {
		return this.questionType;
	}

	public void setQuestionType(QuestionType questionType) {
		this.questionType = questionType;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "SubjectId", nullable = false)
	public SubjectMaster getSubjectMaster() {
		return this.subjectMaster;
	}

	public void setSubjectMaster(SubjectMaster subjectMaster) {
		this.subjectMaster = subjectMaster;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BookId", nullable = false)
	public BookMaster getBookMaster() {
		return this.bookMaster;
	}

	public void setBookMaster(BookMaster bookMaster) {
		this.bookMaster = bookMaster;
	}

	@Column(name = "ErrorComment", length = 500)
	public String getErrorComment() {
		return this.errorComment;
	}

	public void setErrorComment(String errorComment) {
		this.errorComment = errorComment;
	}

	@Column(name = "MarksAllotted", nullable = false, precision = 5, scale = 0)
	public BigDecimal getMarksAllotted() {
		return this.marksAllotted;
	}

	public void setMarksAllotted(BigDecimal marksAllotted) {
		this.marksAllotted = marksAllotted;
	}

	@Column(name = "Question", length = 5000)
	public String getQuestion() {
		return this.question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	@Column(name = "SkillId", nullable = false)
	public short getSkillId() {
		return this.skillId;
	}

	public void setSkillId(short skillId) {
		this.skillId = skillId;
	}

	@Column(name = "Solution", length = 2000)
	public String getSolution() {
		return this.solution;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	@Column(name = "SubTopicId", nullable = false)
	public int getSubTopicId() {
		return this.subTopicId;
	}

	public void setSubTopicId(int subTopicId) {
		this.subTopicId = subTopicId;
	}

	@Column(name = "TopicId", nullable = false)
	public int getTopicId() {
		return this.topicId;
	}

	public void setTopicId(int topicId) {
		this.topicId = topicId;
	}

	@Column(name = "DependentId")
	public Integer getDependentId() {
		return this.dependentId;
	}

	public void setDependentId(Integer dependentId) {
		this.dependentId = dependentId;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "DTQuestion")
	public Set<DTAnswer> getDTAnswers() {
		return this.DTAnswers;
	}

	public void setDTAnswers(Set<DTAnswer> DTAnswers) {
		this.DTAnswers = DTAnswers;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "DTQuestion")
	public Set<DTAssessmentDetails> getDTAssessmentDetailses() {
		return this.DTAssessmentDetailses;
	}

	public void setDTAssessmentDetailses(
			Set<DTAssessmentDetails> DTAssessmentDetailses) {
		this.DTAssessmentDetailses = DTAssessmentDetailses;
	}

}
