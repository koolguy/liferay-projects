package com.teachlearnweb.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.RewardPointsDAO;
import com.teachlearnweb.dao.entity.RewardPoints;
import com.tlw.util.LoggerUtil;

@Repository(value = "rewardPointsDao")
@Transactional
public class RewardPointsDAOImpl extends AbstractDAO<RewardPoints> implements
		RewardPointsDAO {

	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil
			.getLog(RewardPointsDAOImpl.class);

	public void addRewardPoints(RewardPoints rewardPoints) {
		LoggerUtil.infoLogger(logger,
				"Start of add rewardPoints method DAOImpl:: ");
		logger.debug("rewardPoints.getDescription()............."+rewardPoints.getDescription());
		logger.debug("rewardPoints.userId............."+rewardPoints.getUserId());
		saveOrUpdate(rewardPoints);
		//save(rewardPoints);
		LoggerUtil.infoLogger(logger,
				"End of add rewardPoints method DAOImpl:: ");

	}

	
	@SuppressWarnings("unchecked")
	public List<RewardPoints> rewardPointsByUserId(long userId) {

		LoggerUtil
				.infoLogger(logger,
						"Start of getCategories By UerId method of Uer Categories DAOImpl:: ");
		List<RewardPoints> rewardPointsList = null;
		final StringBuilder queryString = new StringBuilder();
		try {
			queryString.append("from RewardPoints as model where ");
			queryString.append("userId");
			queryString.append("= ?");

			rewardPointsList = hibernateTemplate.find(queryString.toString(),
					userId);

		} catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "HibernateException ::", e);
			throw e;
		}

		LoggerUtil.debugLogger(logger, "End of  DAOImpl:: ");
		return rewardPointsList;
	}


	/**
	 * Method to fetch all reward points.
	 * 
	 * @return List<RewardPoints>
	 */
	public List<RewardPoints> fetchRewardPoints() {
		logger.debug("fetchRewardPoints() Method Start in RewardPointsDAOImpl");
		List<RewardPoints> rewardPointsList = null;
		final StringBuilder queryString = new StringBuilder();
		try {
			queryString.append("from RewardPoints rp order by rp.rewardDate desc");
			rewardPointsList = hibernateTemplate.find(queryString.toString());
			hibernateTemplate.setMaxResults(10);
			if(rewardPointsList != null && !rewardPointsList.isEmpty() && rewardPointsList.size() > 0) {
				logger.debug("rewardPointsList.size()----->"+rewardPointsList.size());
			} else {
				new ArrayList<RewardPoints>();
			}

		} catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "HibernateException ::", e);
			throw e;
		}

		return rewardPointsList;
	}

}
