package com.teachlearnweb.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.DifficultyLevelsDAO;
import com.teachlearnweb.dao.entity.DifficultyLevel;

/**
 * Class provides the implementations of AssessmentDAO Interface to perform DML
 * operations.
 * 
 * @author Govinda.
 */
@Repository(value = "difficultyLevelsDAO")
@Transactional
public class DifficultyLevelsDAOImpl extends AbstractDAO<DifficultyLevel> implements DifficultyLevelsDAO {
	// logger
	private static final Logger logger = Logger.getLogger(DifficultyLevelsDAOImpl.class);
	
	public List<DifficultyLevel> getDifficultyLevels() throws HibernateException {
		logger.debug("START");
		List<DifficultyLevel> levels = null;
		levels = this.findAll(DifficultyLevel.class);
		logger.debug("END");
		return levels;
	}

	public DifficultyLevel getDifficultyLevel(Integer levelId) {
		return findById(DifficultyLevel.class, levelId.shortValue());
	}

	public DifficultyLevel getDifficultyLevelByAssessment(final Integer assessmentId) throws HibernateException {
		logger.debug("START");
		DifficultyLevel level = null;
		final StringBuilder queryString = new StringBuilder();
		Transaction tx = null;
		try {
//			queryString.append("from DifficultyLevel as D, DTQuestion as Q where " +
//					"D.levelId=Q.difficultyLevel.levelId and Q.questionId = " +
//					"(select A.DTQuestion.questionId from DTAssessmentDetails as A " +
//					"where A.DTAssessment.assessmentId=? limit 1)");
			queryString.append("select dl.* from tlw_difficultylevel as dl, tlw_dt_question as que where " +
					"dl.levelId=que.levelId and que.questionId = " +
					"(select ad.questionId from tlw_dt_assessmentdetails as ad " +
					"where ad.assessmentId=" + assessmentId + " limit 1)");

			if (logger.isDebugEnabled()) {
				logger.debug("assessmentId::" + assessmentId);
			}
			tx = getSession().beginTransaction();
			List levelList = this.getSession().createSQLQuery(queryString.toString()).addEntity("dl", DifficultyLevel.class).list();
			if (logger.isDebugEnabled()) {
				logger.debug("levelList::" + levelList);
			}
			if (levelList != null && !levelList.isEmpty()) {
				level = (DifficultyLevel) levelList.get(0);
			} else {
				logger.warn("levels list is empty from query");
			}

//			level = (DifficultyLevel) this. hibernateTemplate. find(queryString.toString(), assessmentId);
			if (level != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("level ::" + level.getDescription());
				}
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) tx.rollback();
			logger.error("Hibernate exception occured while fetching assessment details.", e);
			throw e;
		}
		logger.debug("END");
		return level;
	}

	public List<DifficultyLevel> getDifficultiesBySubject(final short subjectId, final short classId) throws HibernateException {
		logger.debug("START");
		List<DifficultyLevel> levels = null;
		final StringBuilder queryString = new StringBuilder();
		Transaction tx = null;
		try {
			queryString.append("select dl.* from tlw_difficultylevel as dl where " +
					"levelid in (select distinct levelid from tlw_dt_question where " +
					"subjectid=" + subjectId + " and classid=" + classId + ")");

			if (logger.isDebugEnabled()) {
				logger.debug("subjectId::" + subjectId);
				logger.debug("classId::" + classId);
			}
			tx = getSession().beginTransaction();
			List<DifficultyLevel> levelList = (List<DifficultyLevel>)this.getSession().createSQLQuery(queryString.toString()).addEntity("dl", DifficultyLevel.class).list();
			if (levelList != null && !levelList.isEmpty()) {
				logger.warn("levels list size: " + levelList.size());
				levels = new ArrayList<DifficultyLevel>();
				// remove dummy levels if any (levelId <= 0)
				for (DifficultyLevel difficultyLevel : levelList) {
					if (difficultyLevel.getLevelId() > 0) {
						levels.add(difficultyLevel);
					}
				}
			} else {
				logger.warn("levels list is empty from query");
			}
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) tx.rollback();
			logger.error("Hibernate exception occured while fetching assessment details.", e);
			throw e;
		}
		logger.debug("END");
		logger.debug("END");
		return levels;
	}

}
