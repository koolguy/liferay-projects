package com.teachlearnweb.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.PLPAnswerDAO;
import com.teachlearnweb.dao.entity.PLPAnswer;

/**
 * Class provides the implementations of PLPAnswersDAO Interface to perform DML
 * operations.
 * 
 * @author satya.
 */

@Repository(value = "plpAnswerDAO")
@Transactional
public class PLPAnswerDAOImpl extends AbstractDAO<PLPAnswer> implements  PLPAnswerDAO{

	// logger
		private static final Logger logger = Logger.getLogger(PLPAnswerDAOImpl.class);
		
	/**
	 * Method to fetch answer by questionId.
	 * 
	 * @param questionId
	 * @return
	 */
	public List<PLPAnswer> getAnswerByQuestionId(Integer questionId) {
		logger.debug("getAnswerByQuestionId() Method Start in PLPAnswerDAOImpl");
		PLPAnswer plpAnswer = null;
		List<PLPAnswer> plpAnswersList = null;
		final StringBuilder queryString = new StringBuilder();
		try{
			queryString.append("from PLPAnswer as answer where answer.PLPQuestion.questionId = ?");
			if(logger.isDebugEnabled()){
				logger.debug("questionId------------->"+questionId);
			}
			plpAnswersList = (List<PLPAnswer>) hibernateTemplate.find(queryString.toString(), questionId);
			if(plpAnswersList != null && !plpAnswersList.isEmpty() && plpAnswersList.size() > 0){
				if(logger.isDebugEnabled()){
					logger.debug("plpAnswersList------------->"+plpAnswersList.size());
				}
			} else {
				plpAnswersList = new ArrayList<PLPAnswer>();
			}
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in PLPAnswerDAOImpl.getAnswerByQuestionId()----", ex);
			throw ex;
		}
		return 	plpAnswersList;
	}

	
	/**
	 * Method to fetch questionId and corresponding plpAnswers.
	 * 
	 * @param questionId
	 * @param answersList
	 * @return
	 */
	public Map<Integer, List<PLPAnswer>> getQuestionsAndAnswersMap(Integer questionId) {
		logger.debug("getAnswerByQuestionId() Method Start in PLPAnswerDAOImpl");
		 Map<Integer, List<PLPAnswer>> questionAndAnswersMap = null;
		List<PLPAnswer> plpAnswersList = null;
		final StringBuilder queryString = new StringBuilder();
		try{
			questionAndAnswersMap = new HashMap<Integer, List<PLPAnswer>>();
			plpAnswersList = new ArrayList<PLPAnswer>();
			queryString.append("from PLPAnswer as answer where answer.PLPQuestion.questionId = ?");
			if(logger.isDebugEnabled()){
				logger.debug("questionId------------->"+questionId);
			}
			plpAnswersList = (List<PLPAnswer>) hibernateTemplate.find(queryString.toString(), questionId);
			if(plpAnswersList != null && !plpAnswersList.isEmpty() && plpAnswersList.size() > 0){
				if(logger.isDebugEnabled()){
					logger.debug("plpAnswersList------------->"+plpAnswersList.size());
				}
				questionAndAnswersMap.put(questionId, plpAnswersList);
			} else {
				questionAndAnswersMap = new HashMap<Integer, List<PLPAnswer>>();
			}
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in PLPAnswerDAOImpl.getAnswerByQuestionId()----", ex);
			throw ex;
		}
		return questionAndAnswersMap;
	}


	/**
	 * Method to fetch plpAnswer by answerId.
	 * 
	 * @param answerId
	 * @return
	 */
	public List<PLPAnswer> getPLPAnswerByAnswerId(Integer answerId) {
		logger.debug("getPLPAnswerByAnswerId(answerId) Method Start in PLPAnswerDAOImpl");
		PLPAnswer plpAnswer = null;
		List<PLPAnswer> plpAnswersList = null;
		final StringBuilder queryString = new StringBuilder();
		try{
			queryString.append("from PLPAnswer as answer where answer.answerId = ?");
			if(logger.isDebugEnabled()){
				logger.debug("answerId------------->"+answerId);
			}
			plpAnswersList = (List<PLPAnswer>) hibernateTemplate.find(queryString.toString(), answerId);
			if(plpAnswersList != null && !plpAnswersList.isEmpty() && plpAnswersList.size() > 0){
				if(logger.isDebugEnabled()){
					logger.debug("plpAnswersList------------->"+plpAnswersList.size());
				}
			} 
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in PLPAnswerDAOImpl.getAnswerByQuestionId()----", ex);
			throw ex;
		}
		return 	plpAnswersList;
	}

}
