
package com.teachlearnweb.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Component;

import com.teachlearnweb.dao.entity.DTAnswer;

/**
 * Interface AssessmentDetailsDAO declares DML operations on AssessmentDetails table.
 * 
 * @author Govinda.
 */
@Component
public interface AnswerDAO extends GenericDAO<DTAnswer> {
//	public void addAssessmentDetails(DTAssessmentDetails assessment);
//	public void removeAssessmentDetails(Integer assessmentId);
	public List<DTAnswer> getAnswers(final Integer questionId) throws HibernateException;
	
	/**
	 * Method to fetch DTAnswer based on questionId.
	 * 
	 * @param questionId
	 * @return
	 */
	public DTAnswer getAnswerForQuestion(Integer questionId);
}