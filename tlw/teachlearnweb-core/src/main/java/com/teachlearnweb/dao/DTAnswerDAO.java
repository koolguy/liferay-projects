package com.teachlearnweb.dao;

import java.util.List;
import org.springframework.stereotype.Component;
import com.teachlearnweb.dao.entity.DTAnswer;

/**
 * Class is responsible for dtAnswer related operations.
 * 
 * @author satya.
 *
 */
@Component
public interface DTAnswerDAO extends GenericDAO<DTAnswer> {
	
	/**
	 * Method to to fetch answers based on questionId.
	 * 
	 * @param questionId
	 * @return  List<DTAnswer>
	 */
	public List<DTAnswer> getAnswers(int questionId);
}
