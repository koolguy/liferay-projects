
package com.teachlearnweb.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Component;

import com.teachlearnweb.dao.entity.DTAssessment;

/**
 * Interface AssessmentDAO declares DML operations on Assessment table.
 * 
 * @author Govinda.
 */
@Component
public interface AssessmentDAO extends GenericDAO<DTAssessment> {
//	public void addAssessment(DTAssessment assessment);
//	public void removeAssessment(Integer assessmentId);

	public List<DTAssessment> getAssessments(final Integer userId, final Short classId,
			final Short subjectId, final Short testId) throws HibernateException;

	public DTAssessment getAssessment(Integer assessmentId);
	
	/**
	 * Method to save DTAssessmentsList.
	 * 
	 * @param dtAssessmentList
	 */
	public void saveDTAssessmentList(List<DTAssessment> dtAssessmentList);
	
	/**
	 * Method to save DTAssessments.
	 * 
	 * @param assessment
	 */
	public int getTestsFinishedByStatusAndUserId(long userId,String status);
	
	/**
	 * Method to fetch assessments by userId.
	 * 
	 * @param userId
	 */
	public List<DTAssessment> getAssessments(Long userId);
	
	/**
	 * Method to save DTAssessments.
	 * 
	 * @param assessment
	 */
	public void saveAssessment(DTAssessment assessment);
	
	/**
	 * Method to fetch all completed dt assessments by subject tests.
	 * 
	 * @param testId
	 * @param status
	 * @return
	 */
	public List<DTAssessment> fetchAllCompletedDTAssessmentsBySubjectTest(Short testId, String status);
	
}

