package com.teachlearnweb.dao.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.UserCategoriesDAO;
import com.teachlearnweb.dao.TestmonialsDAO;
import com.teachlearnweb.dao.entity.TLWUser;
import com.teachlearnweb.dao.entity.Testmonial;
import com.teachlearnweb.dao.entity.UserCategories;
import com.tlw.util.LoggerUtil;

/**
 * Class provides the implementations of EmployeeDAO Interface to perform DML
 * operations.
 * 
 * @author GalaxE.
 */
@Repository(value = "testmonialsDao")
@Transactional
public class TestMonialsDAOImpl extends AbstractDAO<Testmonial>
		implements TestmonialsDAO {

	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil
			.getLog(TestMonialsDAOImpl.class);

	public void postTestmonial(Testmonial testmonial) {
		LoggerUtil.infoLogger(logger,
				"Start of add userCategories method of Quick Links DAOImpl:: ");
		saveOrUpdate(testmonial);
		LoggerUtil.infoLogger(logger,
				"End of add userCategories method of Quick Links DAOImpl:: ");

	}

	public List<Testmonial> fetchTestmonial() {
		LoggerUtil.infoLogger(logger, "Start of fetch testmonials DAOImpl:: ");
		List<Testmonial> testmonialList = null;
		final StringBuilder queryString = new StringBuilder();
		try {
			queryString.append("from Testmonial as model");			

			testmonialList = hibernateTemplate.find(queryString.toString());
			

		}
		catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "TestmonialImpl HibernateException ::", e);
			throw e;
		}

		LoggerUtil.debugLogger(logger, "TestmonialImpl method ");
		return testmonialList;
		
	}

	public List<Testmonial> fetchTestmonialById(long testMonialId) {
		
		LoggerUtil.infoLogger(logger, "Start of fetch testmonials DAOImpl:: ");
		List<Testmonial> testmonialList = null;
		final StringBuilder queryString = new StringBuilder();
		try {
			
			queryString.append("from Testmonial as model where ");	
			queryString.append("testmonialId");
			queryString.append("= ?");			

			testmonialList = hibernateTemplate.find(queryString.toString(),testMonialId );
		}
		catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "TestmonialImpl HibernateException ::", e);
			throw e;
		}

		LoggerUtil.debugLogger(logger, "TestmonialImpl method ");
		return testmonialList;
}

	public int approveTestmonial(long testId) {
		
		LoggerUtil.infoLogger(logger, "Start of fetch testmonials DAOImpl:: ");
		
		final StringBuilder queryString = new StringBuilder();
		int count= 0;
		try {
			
			queryString.append("update Testmonial set status = 'approved' where testId = "+testId);	

			LoggerUtil.debugLogger(logger, queryString.toString());
			count = hibernateTemplate.bulkUpdate(queryString.toString());
			
			LoggerUtil.debugLogger(logger, "updated");
			
			Testmonial test = new Testmonial();
		     hibernateTemplate.saveOrUpdate(test);
		}
		catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "TestmonialImpl HibernateException ::", e);
			throw e;
		}

		LoggerUtil.debugLogger(logger, "TestmonialImpl method ");

       return count;		
		
		// TODO Auto-generated method stub
	}

	


}
