
package com.teachlearnweb.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Component;

import com.teachlearnweb.dao.entity.Skill;

/**
 * Interface AssessmentDAO declares DML operations on Assessment table.
 * 
 * @author Govinda.
 */
@Component
public interface SkillDAO extends GenericDAO<Skill> {
	public List<Skill> getSkillsBySubject(final Short subjectId) throws HibernateException;
	
	/**
	 * Method to fetch skill by skillName.
	 * 
	 * @param skillName
	 * @return
	 */
	public Skill getSkillBySkillName(String skillName);
}

