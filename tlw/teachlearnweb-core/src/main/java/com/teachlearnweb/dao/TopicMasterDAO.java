package com.teachlearnweb.dao;

import org.springframework.stereotype.Component;

import com.teachlearnweb.dao.entity.PLPTopicMaster;

/**
 * Interface TopicMasterDAO declares DML operations on TopicMaster table.
 * 
 * @author satya.
 */
@Component
public interface TopicMasterDAO extends GenericDAO<PLPTopicMaster> {

	
	/**
	 * Method to fetch plptopicmaster by subTopicId.
	 * 
	 * @return
	 */
	public PLPTopicMaster getTopicMasterBySubTopicId(Integer subTopicId, Short classId, Short subjectId);

	/**
	 * Method to fetch plptopicmaster by topicId.
	 * 
	 * @return
	 */
	public PLPTopicMaster getTopicMasterByTopicId(Integer topicId, Short classId, Short subjectId);
	
	/**
	 * Method to fetch TopicMaster by topicName.
	 * 
	 * @param topicName
	 * @return
	 */
	public PLPTopicMaster getTopicMasterByTopicName(String topicName, Short classId, Short subjectId);

	public PLPTopicMaster getTopicMasterByTopicName(String topicName, Integer parentTopicId, Short classId, Short subjectId);
}
