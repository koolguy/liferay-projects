package com.teachlearnweb.dao;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;
import com.teachlearnweb.dao.entity.PLPAnswer;

/**
 * Interface PLPAnswerDAO declares DML operations on tlw_plp_answer table.
 * 
 * @author satya.
 */

@Component
public interface PLPAnswerDAO extends GenericDAO<PLPAnswer>{

	/**
	 * Method to fetch answer by questionId.
	 * 
	 * @param questionId
	 * @return
	 */
	public List<PLPAnswer> getAnswerByQuestionId(Integer questionId);
	
	/**
	 * Method to fetch questionId and corresponding plpAnswers.
	 * 
	 * @param questionId
	 * @param answersList
	 * @return
	 */
	public Map<Integer, List<PLPAnswer>> getQuestionsAndAnswersMap(Integer questionId);
	
	/**
	 * Method to fetch plpAnswer by answerId.
	 * 
	 * @param answerId
	 * @return
	 */
	public List<PLPAnswer> getPLPAnswerByAnswerId(Integer answerId);
}
