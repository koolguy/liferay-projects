package com.teachlearnweb.dao;

import org.springframework.stereotype.Component;
import com.teachlearnweb.dao.entity.ClassMaster;

/**
 * Interface ClassMasterDAO declares DML operations on ClassMaster table.
 * 
 * @author satya.
 */
@Component
public interface ClassMasterDAO extends GenericDAO<ClassMaster>{

	/**
	 * Method to fecth classMaster by classId.
	 * 
	 * @param classId
	 * @return
	 */
	public ClassMaster getClassMasterByClassId(short classId);
	
	/**
	 * Method to fetch classMaster by className.
	 * 
	 * @param className
	 * @return
	 */
	public ClassMaster getClassMasterByClassName(String className);
}
