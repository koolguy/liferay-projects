package com.teachlearnweb.dao;

import java.util.List;

import com.teachlearnweb.dao.entity.CouponCode;

public interface CouponCodeDAO {
	public void addCouponcode(CouponCode couponCode) throws Exception;

	public List<CouponCode> fetchCouponCodes();

	public CouponCode fetchCouponDetailByCode(String coupon);
}
