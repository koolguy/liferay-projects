package com.teachlearnweb.dao.impl;

import java.util.List;

import org.hibernate.HibernateException;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.CouponCodeDAO;

import com.teachlearnweb.dao.entity.CouponCode;

import com.tlw.util.LoggerUtil;

/**
 * Class provides the implementations of EmployeeDAO Interface to perform DML
 * operations.
 * 
 * @author GalaxE.
 */
@Repository(value = "couponCodeDao")
@Transactional
public class CouponCodeDAOImpl extends AbstractDAO<CouponCode>
		implements CouponCodeDAO {

	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil
			.getLog(CouponCodeDAOImpl.class);

	public void addCouponcode(CouponCode couponCode) {
		LoggerUtil.infoLogger(logger,
				"Start of add userCategories method of Quick Links DAOImpl:: ");
		saveOrUpdate(couponCode);
		LoggerUtil.infoLogger(logger,
				"End of add userCategories method of Quick Links DAOImpl:: ");

	}

	public List<CouponCode> fetchCouponCodes() {
		LoggerUtil.infoLogger(logger, "Start of fetch CouponCode DAOImpl:: ");
		List<CouponCode> testmonialList = null;
		final StringBuilder queryString = new StringBuilder();
		try {
			queryString.append("from CouponCode as model");			

			testmonialList = hibernateTemplate.find(queryString.toString());
			

		}
		catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "CouponCode HibernateException ::", e);
			throw e;
		}

		LoggerUtil.debugLogger(logger, "CouponCode method ");
		return testmonialList;
		
	}

	public CouponCode fetchCouponDetailByCode(String coupon) {
		LoggerUtil.infoLogger(logger, "Start of fetch CouponCode DAOImpl:: ");
		
		final StringBuilder queryString = new StringBuilder();
		List<CouponCode>  couponCodeList = null;
		CouponCode couponCode = null;
		
		try {
			queryString.append("from CouponCode as model where ");					
			queryString.append("couponCode ");
			queryString.append("like ?");
			
			couponCodeList = hibernateTemplate.find(queryString.toString(),coupon);
			if(couponCodeList.size()>0)
			couponCode = couponCodeList.get(0);
			
			

		}
		catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "CouponCode HibernateException ::", e);
			throw e;
		}
		return couponCode;
	}

	


}
