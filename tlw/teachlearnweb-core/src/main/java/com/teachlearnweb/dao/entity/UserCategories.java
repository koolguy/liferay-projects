
package com.teachlearnweb.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Class is used to capture the characteristics of ci_employee. 
 * This is reusable class for ci_employee.
 * @author GalaxE.
 */
@Entity
@Table(name = "user_categories")
public class UserCategories implements java.io.Serializable {

		
	private static final long serialVersionUID = -2828861827483771607L;
	// Fields

	private Long userId;
	

	private Long categoryId;
	
	// Constructors

	/** default constructor */
	public UserCategories() {

	}

	/** minimal constructor */
	public UserCategories(
		Long userId, Long categoryId) {
		this.userId = userId;
		this.categoryId = categoryId;		
	}
	
	@Id
	@Column(name = "userId", unique = false, nullable = false)
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Id
	@Column(name = "categoryId", unique = false, nullable = false)
	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	
}
