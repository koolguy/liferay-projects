package com.teachlearnweb.dao.entity;
// default package
// Generated Jan 28, 2013 10:08:44 PM by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * DTAnswer generated by hbm2java
 */
@Entity
@Table(name = "tlw_dt_answer", catalog = "tlw")
public class DTAnswer implements java.io.Serializable {

	private Integer answerId;
	private StyleCode styleCode;
	private DTQuestion DTQuestion;
	private String answer;
	private Integer dependedQuestionId;
	private byte isCorrect;
	private Set<DTAssessmentDetails> DTAssessmentDetailses = new HashSet<DTAssessmentDetails>(
			0);

	public DTAnswer() {
	}

	public DTAnswer(StyleCode styleCode, DTQuestion DTQuestion, byte isCorrect) {
		this.styleCode = styleCode;
		this.DTQuestion = DTQuestion;
		this.isCorrect = isCorrect;
	}

	public DTAnswer(StyleCode styleCode, DTQuestion DTQuestion, String answer,
			Integer dependedQuestionId, byte isCorrect,
			Set<DTAssessmentDetails> DTAssessmentDetailses) {
		this.styleCode = styleCode;
		this.DTQuestion = DTQuestion;
		this.answer = answer;
		this.dependedQuestionId = dependedQuestionId;
		this.isCorrect = isCorrect;
		this.DTAssessmentDetailses = DTAssessmentDetailses;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "AnswerId", unique = true, nullable = false)
	public Integer getAnswerId() {
		return this.answerId;
	}

	public void setAnswerId(Integer answerId) {
		this.answerId = answerId;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "StyleCodeId", nullable = false)
	public StyleCode getStyleCode() {
		return this.styleCode;
	}

	public void setStyleCode(StyleCode styleCode) {
		this.styleCode = styleCode;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "QuestionId", nullable = false)
	public DTQuestion getDTQuestion() {
		return this.DTQuestion;
	}

	public void setDTQuestion(DTQuestion DTQuestion) {
		this.DTQuestion = DTQuestion;
	}

	@Column(name = "Answer", length = 2000)
	public String getAnswer() {
		return this.answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	@Column(name = "DependedQuestionId")
	public Integer getDependedQuestionId() {
		return this.dependedQuestionId;
	}

	public void setDependedQuestionId(Integer dependedQuestionId) {
		this.dependedQuestionId = dependedQuestionId;
	}

	@Column(name = "IsCorrect", nullable = false)
	public byte getIsCorrect() {
		return this.isCorrect;
	}

	public void setIsCorrect(byte isCorrect) {
		this.isCorrect = isCorrect;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "DTAnswer")
	public Set<DTAssessmentDetails> getDTAssessmentDetailses() {
		return this.DTAssessmentDetailses;
	}

	public void setDTAssessmentDetailses(
			Set<DTAssessmentDetails> DTAssessmentDetailses) {
		this.DTAssessmentDetailses = DTAssessmentDetailses;
	}

}
