package com.teachlearnweb.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.AnswerDAO;
import com.teachlearnweb.dao.entity.DTAnswer;

/**
 * Class provides the implementations of AssessmentDAO Interface to perform DML
 * operations.
 * 
 * @author Govinda.
 */
@Repository(value = "answerDAO")
@Transactional
public class AnswerDAOImpl extends AbstractDAO<DTAnswer> implements AnswerDAO {
	// logger
	private static final Logger logger = Logger.getLogger(AnswerDAOImpl.class);
	@Autowired
	private SessionFactory sessionFactory;

	public List<DTAnswer> getAnswers(final Integer questionId) throws HibernateException {
		logger.debug("START");
		List<DTAnswer> answers = null;
		final StringBuilder queryString = new StringBuilder();
		try {
			queryString.append("from DTAnswer as answer where answer.DTQuestion.questionId=?");
			if (logger.isDebugEnabled()) {
				logger.debug("questionId::" + questionId);
				logger.debug("queryString::" + queryString);
			}
			answers = (List<DTAnswer>) hibernateTemplate.find(queryString.toString(), questionId);
			if (answers != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("answers size::" + answers.size());
				}
			}
		} catch (HibernateException e) {
			logger.error("Hibernate exception occured while fetching answers for " + questionId, e);
			throw e;
		}
		logger.debug("END");
		return answers;
	}

	/**
	 * Method to fetch DTAnswer based on questionId.
	 * 
	 * @param questionId
	 * @return
	 */
	public DTAnswer getAnswerForQuestion(Integer questionId) {
		logger.debug("getAnswerForQuestion() Method Start in AnswerDAOImpl");
		List<DTAnswer> dtAnswers = null;
		DTAnswer answer = null;
		final StringBuilder queryString = new StringBuilder();
		try {
			queryString.append("from DTAnswer as answer where answer.DTQuestion.questionId=?");
			if (logger.isDebugEnabled()) {
				logger.debug("questionId::" + questionId);
				logger.debug("queryString::" + queryString);
			}
			dtAnswers = (List<DTAnswer>) hibernateTemplate.find(queryString.toString(), questionId);
			if (dtAnswers != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("answers size::" + dtAnswers.size());
				}
				for(DTAnswer dtAnswer : dtAnswers) {
					if(dtAnswer.getIsCorrect() == 1){
						answer =  dtAnswer;
						break;
					}
				}
			}
		} catch (HibernateException e) {
			logger.error("Hibernate exception occured while fetching answers for " + questionId, e);
			throw e;
		}
		logger.debug("END");
		return answer;
	}
}