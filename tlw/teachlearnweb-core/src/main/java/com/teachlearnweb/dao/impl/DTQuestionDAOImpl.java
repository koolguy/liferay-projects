package com.teachlearnweb.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.liferay.portal.kernel.transaction.Transactional;
import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.DTQuestionDAO;
import com.teachlearnweb.dao.entity.DTQuestion;
import com.teachlearnweb.dao.entity.DifficultyLevel;
import com.teachlearnweb.dao.entity.Skill;
import com.teachlearnweb.dao.entity.SubjectMaster;
import com.tlw.assessment.vo.AssessmentConstants;

/**
 * Class is responsible for dtQuestion related operations.
 * 
 * @author satya.
 *
 */
@Repository(value="dtQuestionsDao")
@Transactional
public class DTQuestionDAOImpl extends AbstractDAO<DTQuestion>  implements  DTQuestionDAO{

	/** The logger. */
    private static final Logger logger = Logger.getLogger(DTQuestionDAOImpl.class);
    
	/**
	 * Method to fetch questions randomly.
	 * 
	 * @return List<DTQuestion>
	 */
	@SuppressWarnings({ "unchecked" })
	public List<DTQuestion> getQuestions() {
		List<DTQuestion> questionsList = new ArrayList<DTQuestion>();
		List<SubjectMaster> subjectsList = getSubjects();
		List<DifficultyLevel> difficultLevelList = getDifficultyLevels();
		List<Skill> skillList = getSkills();
		
		Session session = getSession();
		Transaction tx = session.beginTransaction();
		try {
			if(subjectsList != null && !subjectsList.isEmpty() && subjectsList.size() > 0 && difficultLevelList != null && !difficultLevelList.isEmpty() && difficultLevelList.size() > 0){
			for(SubjectMaster subject : subjectsList){
				List<Skill> filteredSkillList = getFilteredSkillListForSubject(subject, skillList);
				for(Skill skill : filteredSkillList) {
					for(DifficultyLevel level : difficultLevelList){
						List<DTQuestion> tempList = null;
						String hql = "from DTQuestion as question where question.subjectMaster.subjectName=:subjectName and question.difficultyLevel.description=:levelDesc and question.skillId =:skillId order by rand()";
						Query query = session.createQuery(hql);
						query.setParameter("subjectName", subject.getSubjectName());
						query.setParameter("levelDesc", level.getDescription());
						query.setParameter("skillId", skill.getId().getSkillId());
						query.setMaxResults(3);
						tempList = query.list();
						if(tempList != null && !tempList.isEmpty() && tempList.size() > 0){
							questionsList.addAll(tempList);
						}
					}
				}
			}
				tx.commit();
			}
			
		} catch (HibernateException e) {
			if(tx != null)
				tx.rollback();
			logger.error("Exception caught in DTQuestionDAOImpl.getQuestions()", e);
		}
		if(questionsList != null && !questionsList.isEmpty() && questionsList.size() > 0){
			logger.debug("questionsList.size() in DTQuestionDAOImpl.getQuestions()---->"+questionsList.size());
			return questionsList;
		} else {
			return new ArrayList<DTQuestion>();
		}
	}
	
	/**
	 * Helper Method to fetch skillId based on subject.
	 * 
	 * @param subject
	 * @param skillList
	 * @return
	 */
	private List<Skill> getFilteredSkillListForSubject(SubjectMaster subject,	List<Skill> skillList) {
		logger.debug("DTQuestionDAOImpl.getSkillIdForSubject() Method Strat in DTQuestionDAOImpl");
		List<Skill> skillsList = new ArrayList<Skill>();
		if(subject != null  && skillList != null && !skillList.isEmpty() && skillList.size() > 0) {
			for(Skill skill : skillList) {
				if(skill != null && skill.getId() != null &&  subject.getSubjectId() != null && subject.getSubjectId().equals(skill.getId().getSubjectId())){
					skillsList.add(skill);
				}
			}
		}
		return skillsList;
	}

	/**
	 * Method to fetch subjects.
	 * 
	 * @return  List<SubjectMaster> 
	 */
	@SuppressWarnings("unchecked")
	public List<SubjectMaster> getSubjects(){
		logger.debug("DTQuestionDAOImpl.getSubjects() Method Strat in DTQuestionDAOImpl");
		List<SubjectMaster> subjectsList = null;
		Session session = getSession();
		Transaction tx= null;
		try {
			tx= session.beginTransaction();
			String subjectHql = "from SubjectMaster";
			subjectsList = session.createQuery(subjectHql).list();
			tx.commit();
			
		} catch (HibernateException e) {
			if(tx != null)
				tx.rollback();
			logger.error("Exception caught in DTQuestionDAOImpl.getSubjects()", e);
		}
		if(subjectsList != null && !subjectsList.isEmpty() && subjectsList.size() > 0){
			return subjectsList;
		} else {
			return new ArrayList<SubjectMaster>();
		}
		
	}
	
	
	/**
	 * Method to fetch difficultyLevels.
	 * 
	 * @return  List<SubjectMaster> 
	 */
	@SuppressWarnings("unchecked")
	public List<DifficultyLevel> getDifficultyLevels(){
		logger.debug("DTQuestionDAOImpl.getDifficultyLevels() Method Strat in DTQuestionDAOImpl");
		List<DifficultyLevel> difficultLevelList = null;
		Session session = getSession();
		Transaction tx= null;
		try {
			tx= session.beginTransaction();
			String diffLevelHql = "from DifficultyLevel";
			difficultLevelList = session.createQuery(diffLevelHql).list();
			tx.commit();
			
		} catch (HibernateException e) {
			if(tx != null)
			tx.rollback();
			logger.error("Exception caught in DTQuestionDAOImpl.getDifficultyLevels()", e);
		}
		if(difficultLevelList != null && !difficultLevelList.isEmpty() && difficultLevelList.size() > 0){
			return difficultLevelList;
		} else {
			return new ArrayList<DifficultyLevel>();
		}
	}
	
	/**
	 * Method to fetch skill based on subject.
	 * 
	 * @param subjectId
	 * @return List<Skill>
	 */
	@SuppressWarnings("unchecked")
	public List<Skill> getSkills(short subjectId) {
		logger.debug("DTQuestionDAOImpl.getSkills(subjectId) Method Strat in DTQuestionDAOImpl");
		List<Skill> skillsList = null;
		Session session = getSession();
		Transaction tx= null;
		try {
			tx= session.beginTransaction();
			String skillHql = "from Skill as skill where skill.subjectMaster.subjectId =:subjectId";
			skillsList = session.createQuery(skillHql).setParameter("subjectId", subjectId).list();
			tx.commit();
			
		} catch (HibernateException e) {
			if(tx != null)
			tx.rollback();
			logger.error("Exception caught in DTQuestionDAOImpl.getSkills()", e);
		}
		if(skillsList != null && !skillsList.isEmpty() && skillsList.size() > 0){
			return skillsList;
		} else {
			return new ArrayList<Skill>();
		}
	}
	
	/**
	 * Method to fetch skill.
	 * 
	 * @param subjectId
	 * @return List<Skill>
	 */
	@SuppressWarnings("unchecked")
	public List<Skill> getSkills() {
		logger.debug("DTQuestionDAOImpl.getSkills() Method Strat in DTQuestionDAOImpl");
		List<Skill> skillsList = null;
		Session session = getSession();
		Transaction tx= null;
		try {
			tx= session.beginTransaction();
			String skillHql = "from Skill";
			skillsList = session.createQuery(skillHql).list();
			tx.commit();
			
		} catch (HibernateException e) {
			if(tx != null)
			tx.rollback();
			logger.error("Exception caught in DTQuestionDAOImpl.getSkills()", e);
		}
		if(skillsList != null && !skillsList.isEmpty() && skillsList.size() > 0){
			return skillsList;
		} else {
			return new ArrayList<Skill>();
		}
	}

	/**
	 * Method to fetch questions based on classId and subjectId.
	 * 
	 * @return List<DTQuestion>
	 */
	public List<DTQuestion> getQuestions(short classId, short subjectId) {
		logger.debug("DTQuestionDAOImpl.getQuestions(classId, subjectId) Method Strat in DTQuestionDAOImpl");
		List<DTQuestion> dtQuestionsList = null;
		final StringBuilder queryString = new StringBuilder();
		Session session = getSession();
		Transaction tx = session.beginTransaction();
		try {
			queryString.append("from DTQuestion as question where question.classMaster.classId = :classId " +
					"and question.subjectMaster.subjectId = :subjectId and question.questionType.questionTypeId!=" + 
					AssessmentConstants.QuestionType.STYLE_BASED);
			if (logger.isDebugEnabled()) {
				logger.debug("classId------------->"+classId);
				logger.debug("subjectId------------->"+subjectId);
			}
			Query query = session.createQuery(queryString.toString());
			query.setParameter("subjectId", subjectId);
			query.setParameter("classId", classId);
			query.setMaxResults(20);
			dtQuestionsList = (List<DTQuestion>) query.list();
			if (dtQuestionsList != null && !dtQuestionsList.isEmpty()) {
				if (logger.isDebugEnabled()) {
					logger.debug("dtQuestionsList.size()------------->"+dtQuestionsList.size());
				}
			} else {
				dtQuestionsList = new ArrayList<DTQuestion>();
			}
			tx.commit();
		} catch(HibernateException ex) {
			if (tx != null) tx.rollback();
			logger.error("Hibernate Exception Caught in PLPQuestionsDAOImpl.getQuestionById()----", ex);
			throw ex;
		}
		return dtQuestionsList;
	}

	/**
	 * Method to fetch questions based on classId and subjectId.
	 * 
	 * @return List<DTQuestion>
	 */
	public List<DTQuestion> getStyleBasedQuestions() {
		logger.debug("START");
		List<DTQuestion> dtQuestionsList = null;
		final StringBuilder queryString = new StringBuilder();
		Transaction tx = null;
		try {
			queryString.append("from DTQuestion as question where question.questionType.questionTypeId=" + 
					AssessmentConstants.QuestionType.STYLE_BASED + " order by rand()");
			tx = getSession().beginTransaction();
			Query query = getSession().createQuery(queryString.toString());
			query.setMaxResults(20);
			dtQuestionsList = (List<DTQuestion>) query.list();
			if (dtQuestionsList != null && !dtQuestionsList.isEmpty()) {
				if(logger.isDebugEnabled()) {
					logger.debug("dtQuestionsList.size()------------->"+dtQuestionsList.size());
				}
			} else {
				dtQuestionsList = new ArrayList<DTQuestion>();
			}
			tx.commit();
		} catch (HibernateException ex) {
			if (tx != null) tx.rollback();
			logger.error("Hibernate Exception Caught in PLPQuestionsDAOImpl.getQuestionById()----", ex);
			throw ex;
		}
		logger.debug("END");
		return dtQuestionsList;
	}

	/**
	 * Method to fetch questions based on classId and subjectId.
	 * @author govinda 
	 * @return List<DTQuestion>
	 */
	public List<DTQuestion> getSkillwiseQuestions(final List<Skill> skills, final Short classId,
			final Short subjectId, final Short questionsPerSkill) {
		logger.debug("START");
		if (skills == null || skills.isEmpty()) {
			logger.error("Skills list is empty");
			return null;
		}
		List<DTQuestion> dtQuestionsList = new ArrayList<DTQuestion>();
		List<DTQuestion> dtSkillwiseQuestions = null;
		final StringBuilder queryString = new StringBuilder();
		Transaction tx = null;
		try {
			logger.debug("Skill count " + skills.size());
			// fetch questions for each skill
			tx = getSession().beginTransaction();
			for (Skill skill : skills) {
				queryString.append("from DTQuestion as Q where Q.classMaster.classId=:classId and " +
						"Q.subjectMaster.subjectId=:subjectId and Q.skillId=:skillId " +
						"and Q.questionType.questionTypeId!=" + AssessmentConstants.QuestionType.STYLE_BASED +
						" order by rand()");
				if(logger.isDebugEnabled()){
					logger.debug("classId------------->"+classId);
					logger.debug("subjectId------------->"+subjectId);
					logger.debug("skillId------------->"+skill.getId().getSkillId());
				}
				Query query = getSession().createQuery(queryString.toString());
				query.setParameter("subjectId", subjectId);
				query.setParameter("classId", classId);
				query.setParameter("skillId", skill.getId().getSkillId());
				query.setMaxResults(questionsPerSkill);
				dtSkillwiseQuestions = (List<DTQuestion>) query.list();

				if(logger.isDebugEnabled()) {
					logger.debug("dtSkillwiseQuestions------------->" + dtSkillwiseQuestions);
				}
				if (dtSkillwiseQuestions != null) {
					logger.debug(dtSkillwiseQuestions.size() + " questions found for the skill " + skill.getId().getSkillName());
					if (dtSkillwiseQuestions.size() > questionsPerSkill) {
						logger.debug("questions are more than expected, trimming to required count");
						dtSkillwiseQuestions = dtSkillwiseQuestions.subList(0, questionsPerSkill);
					}
					dtQuestionsList.addAll(dtSkillwiseQuestions);
					dtSkillwiseQuestions = null;
				}
			}
			tx.commit();
		} catch(HibernateException ex) {
			if (tx != null) tx.rollback();
			logger.error("Hibernate Exception Caught in PLPQuestionsDAOImpl.getQuestionById()----", ex);
			throw ex;
		}
		logger.debug("Total questions " + dtQuestionsList.size());
		logger.debug("END");
		return dtQuestionsList;
	}

	/**
	 * Method to fetch questions based on classId and subjectId.
	 * @author govinda 
	 * @return List<DTQuestion>
	 */
	public List<DTQuestion> getQuestions(final Short classId, final Short subjectId, final Short levelId) {
		logger.debug("START");
		List<DTQuestion> dtQuestionsList = null;
		final StringBuilder queryString = new StringBuilder();
		Transaction tx = null;
		try{
			queryString.append("from DTQuestion as Q where Q.classMaster.classId=:classId and " +
					"Q.subjectMaster.subjectId=:subjectId and Q.difficultyLevel.levelId=:levelId " +
					"and Q.questionType.questionTypeId!=" + AssessmentConstants.QuestionType.STYLE_BASED + " order by rand()");
			if(logger.isDebugEnabled()){
				logger.debug("classId------------->"+classId);
				logger.debug("subjectId------------->"+subjectId);
				logger.debug("levelId------------->"+levelId);
			}
//			dtQuestionsList = (List<DTQuestion>) hibernateTemplate.find(queryString.toString(), classId, subjectId, levelId);
			tx = getSession().beginTransaction();
			Query query = getSession().createQuery(queryString.toString());
			query.setParameter("subjectId", subjectId);
			query.setParameter("classId", classId);
			query.setParameter("levelId", levelId);
			query.setMaxResults(20);
			dtQuestionsList = (List<DTQuestion>) query.list();

			if(dtQuestionsList != null && !dtQuestionsList.isEmpty()) {
				if(logger.isDebugEnabled()) {
					logger.debug("dtQuestionsList.size()------------->"+dtQuestionsList.size());
				}
				if (dtQuestionsList.size() > 20) {
					dtQuestionsList = dtQuestionsList.subList(0, 20);
				}
			} else {
				dtQuestionsList = new ArrayList<DTQuestion>();
			}
			tx.commit();
		} catch(HibernateException ex) {
			if (tx != null) tx.rollback();
			logger.error("Hibernate Exception Caught in PLPQuestionsDAOImpl.getQuestionById()----", ex);
			throw ex;
		}
		logger.debug("END");
		return dtQuestionsList;
	}
}
