package com.teachlearnweb.dao;

import java.util.List;

import org.springframework.stereotype.Component;
import com.teachlearnweb.dao.entity.PLPTest;

/**
 * Interface PLPTestDAO declares DML operations on plpTest table.
 * 
 * @author satya.
 */
@Component
public interface PLPTestDAO extends GenericDAO<PLPTest> {

	/**
	 * Method used to save plpTest.
	 * 
	 * @param plpTest
	 * @return
	 */
	public void savePLPTest(PLPTest plpTest);
	
	
	/**
	 * Method used to save plpTestList.
	 * 
	 * @param plpTestList
	 */
	public void savePLPTestList(List<PLPTest> plpTestList);
}
