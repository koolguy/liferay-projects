package com.teachlearnweb.dao.impl;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.PLPStudentWorkSheetDAO;
import com.teachlearnweb.dao.entity.DTTest;
import com.teachlearnweb.dao.entity.PLPStudentWrokSheet;

/**
 * Class provides the implementations of PLPStudentWorkSheetDAO Interface to perform DML
 * operations.
 * 
 * @author Satya.
 */
@Repository(value = "plpStudentWorkSheetDao")
@Transactional
public class PLPStudentWorkSheetDAOImpl extends AbstractDAO<PLPStudentWrokSheet> implements PLPStudentWorkSheetDAO{

	// logger
		private static final Logger logger = Logger.getLogger(PLPStudentWorkSheetDAOImpl.class);
		
	/**
	 * Method to fetch plpstudentworksheet list by userId.
	 * 
	 * @param userId
	 * @return
	 */
	public List<PLPStudentWrokSheet> getPLPStudentWorkSheetsByUserId(long userId) {
		logger.debug("getPLPStudentWorkSheetsByUserId() Method Start in PLPStudentWorkSheetDAOImpl");
		List<PLPStudentWrokSheet> plpStudentWSList = null;
		final StringBuilder queryString = new StringBuilder();
		try{
			queryString.append("from PLPStudentWrokSheet ws where ws.userId = ?");
			if(logger.isDebugEnabled()){
				logger.debug("userId------------->"+userId);
			}
			
			plpStudentWSList = (List<PLPStudentWrokSheet>) hibernateTemplate.find(queryString.toString(), userId);
			if(plpStudentWSList != null && !plpStudentWSList.isEmpty() && plpStudentWSList.size() > 0){
				if(logger.isDebugEnabled()){
					logger.debug("plpStudentWSList.size()------------->"+plpStudentWSList.size());
				}
			}
			
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in PLPStudentWorkSheetDAOImpl.getPLPStudentWorkSheetsByUserId()----", ex);
			throw ex;
		}
		return plpStudentWSList;
	}

	/**
	 * Method to save plpStudentWorkSheetsList.
	 * 
	 * @param wsList
	 */
	public void savePLPStudentWorkSheetsList(List<PLPStudentWrokSheet> wsList) throws HibernateException {
		logger.debug("savePLPStudentWorkSheetsList(wsList) Method Start in PLPStudentWorkSheetDAOIMpl.");
		if(wsList != null && !wsList.isEmpty() && wsList.size() > 0) {
			for(PLPStudentWrokSheet workSheet : wsList) {
				if(workSheet != null) {
					save(workSheet);
				}
			}
		}
		
	}

	/**
	 * Method to fetch plpStudentWorkSheet.
	 * 
	 * @param workSheet
	 */
	public void savePLPStudentWorkSheets(PLPStudentWrokSheet workSheet) throws HibernateException {
		logger.debug("savePLPStudentWorkSheets() Method Start in PLPStudentWorkSheetDAOIMpl.");
		save(workSheet);
	}
	
	/**
	 * Method to convert long to int.
	 * @param l
	 * @return int
	 */
	public int convertLongToInt(long l) {
	    if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
	        throw new IllegalArgumentException
	            (l + " cannot be cast to int without changing its value.");
	    }
	    return (int) l;
	}

	/**
	 * Method to fetch worksheet by worksheetId.
	 * 
	 * @param workSheetId
	 */
	public PLPStudentWrokSheet getPLPStudentWorkSheetByWorkSheetId(Integer workSheetId, long userId) {
		logger.debug("getPLPStudentWorkSheet(worksheetId) Method Start in PLPStudentWorksheetDAOImpl");
		List<PLPStudentWrokSheet> plpStudentWSList = null;
		PLPStudentWrokSheet pLPStudentWrokSheet = null;
		final StringBuilder queryString = new StringBuilder();
		
		try{		
			queryString.append("from PLPStudentWrokSheet ws where ws.worksheetId = ? and ws.userId = ?");
			if(logger.isDebugEnabled()) {
				System.out.println("topicId------------->"+workSheetId);
				System.out.println("userId------------->"+userId);							
				System.out.println("queryString: "+queryString.toString());
			}
			
			plpStudentWSList = hibernateTemplate.find(queryString.toString(), workSheetId, userId);
			if(plpStudentWSList != null && !plpStudentWSList.isEmpty() && plpStudentWSList.size() > 0) {
				pLPStudentWrokSheet = plpStudentWSList.get(0);
			}
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in PLPStudentWorkSheetDAOImpl.getPLPStudentWorkSheetsByUserId()----", ex);
			throw ex;
		}
		return pLPStudentWrokSheet;
	}

	/**
	 * Method to update plpStudentWorkSheet.
	 * 
	 * @param workSheet
	 */
	public void updatePLPStudentWorkSheet(PLPStudentWrokSheet workSheet) {
		logger.debug("updatePLPStudentWorkSheet() Method Start in PLPStudentWorksheetDAOImpl");
		saveOrUpdate(workSheet);
	}

	/**
	 * Method to check whether workSheets are there or not for the subTopicId.
	 * 
	 * @param subTopicid
	 * @return
	 */

	public boolean checkWorkSheetListForSubTopicId(Integer subTopicid, long userId, Short classId, Short subjectId) {
		final StringBuilder queryString = new StringBuilder();
		Boolean isExists = false;
		
		if(logger.isDebugEnabled()){
			logger.debug("subTopicid------------->"+subTopicid);
			logger.debug("userId------------->"+userId);
			logger.debug("classId------------->"+classId);
			logger.debug("subjectId------------->"+subjectId);
		}
		
		queryString.append("from PLPStudentWrokSheet ws where ws.subtopicId = ?  and ws.userId = ? and ws.classMaster.classId = ? and ws.subjectMaster.subjectId = ?");
		
		try{
			List<PLPStudentWrokSheet> wsList =  hibernateTemplate.find(queryString.toString(), subTopicid, userId, classId, subjectId);
			if(wsList != null && !wsList.isEmpty() && wsList.size() > 0) {
				System.out.println("Topic found...........................");
				isExists = true;
			} 
			
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in PLPStudentWorkSheetDAOImpl.getPLPStudentWorkSheetsByUserId()----", ex);
			throw ex;
		}
		return isExists;
		
	}
	
	
	/**
	 * Method to check whether workSheets are there or not for the subTopicId.
	 * 
	 * @param subTopicid
	 * @return
	 */

	public boolean checkEOCTesteForTopicId(Integer topicId, long userId, Short classId, Short subjectId) {
		final StringBuilder queryString = new StringBuilder();
		Boolean isExists = false;
		
		if(logger.isDebugEnabled()){
			logger.debug("topicid------------->"+topicId);
			logger.debug("userId------------->"+userId);
			logger.debug("classId------------->"+classId);
			logger.debug("subjectId------------->"+subjectId);
		}
		
		queryString.append("from PLPStudentWrokSheet ws where ws.topicId = ?  and ws.userId = ? and ws.classMaster.classId = ? and ws.subjectMaster.subjectId = ? and ws.subtopicId is null");
		
		try{
			List<PLPStudentWrokSheet> wsList =  hibernateTemplate.find(queryString.toString(), topicId, userId, classId, subjectId);
			if(wsList != null && !wsList.isEmpty() && wsList.size() > 0) {
				System.out.println("Topic found...........................");
				isExists = true;
			} 
			
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in PLPStudentWorkSheetDAOImpl.getPLPStudentWorkSheetsByUserId()----", ex);
			throw ex;
		}
		return isExists;
		
	}

	/**
	 * Method to check whether workSheets are there or not for the topicId.
	 * 
	 * @param topicId.
	 * @return
	 */
	public boolean checkWorkSheetListForTopicId(Integer topicId) {
		List<PLPStudentWrokSheet> plpStudentWSList = null;
		final StringBuilder queryString = new StringBuilder();
		Boolean isExists = false;
		try{
			queryString.append("from PLPStudentWrokSheet ws where ws.topicId = ?");
			if(logger.isDebugEnabled()){
				logger.debug("topicId------------->"+topicId);
			}
			
			plpStudentWSList = (List<PLPStudentWrokSheet>) hibernateTemplate.find(queryString.toString(), topicId);
			if(plpStudentWSList != null && !plpStudentWSList.isEmpty() && plpStudentWSList.size() > 0){
				if(logger.isDebugEnabled()){
					logger.debug("plpStudentWSList.size()------------->"+plpStudentWSList.size());
				}
				isExists = true;
			}
			
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in PLPStudentWorkSheetDAOImpl.getPLPStudentWorkSheetsByUserId()----", ex);
			throw ex;
		}
		return isExists;
	}

	/**
	 * Method to fetch user completed workSheets.
	 * 
	 * @param userId
	 * @param status
	 * @return
	 */
	public int getWorkSheetsStatusByUserId(long userId,	String status) {
		logger.debug("getWorkSheetsByUserId(userId, status) Method Start in PLPStudentWorkSheetDAOImpl");
		List<PLPStudentWrokSheet> plpStudentWSList = null;
		int count = 0;
		final StringBuilder queryString = new StringBuilder();
		try{
			queryString.append("from PLPStudentWrokSheet ws where ws.userId = ? and ws.status = ?");
			if(logger.isDebugEnabled()){
				logger.debug("userId------------->"+userId);
				logger.debug("status------------->"+status);
			}
			
			plpStudentWSList = (List<PLPStudentWrokSheet>) hibernateTemplate.find(queryString.toString(), userId, status);  //NV - removed convertLongToInt(userId) becoz of error
			if(plpStudentWSList != null && !plpStudentWSList.isEmpty() && plpStudentWSList.size() > 0){
				if(logger.isDebugEnabled()){
					logger.debug("plpStudentWSList.size()------------->"+plpStudentWSList.size());
				}
				count = plpStudentWSList.size();
			}
			
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in PLPStudentWorkSheetDAOImpl.getPLPStudentWorkSheetsByUserId()----", ex);
			throw ex;
		}
		return count;
	}

	/**
	 * Method to fetch all completed worksheets.
	 * 
	 * @param status
	 * @return
	 */
	public List<PLPStudentWrokSheet> fetchAllCompletedWorkSheets(String status) {
		logger.debug("fetchAllCompletedWorkSheets Method Start in PLPStudentWorkSheetDAOImpl");
		List<PLPStudentWrokSheet> plpStudentWSList = null;
		final StringBuilder queryString = new StringBuilder();
		try{
			queryString.append("from PLPStudentWrokSheet ws where ws.status = ?");
			if(logger.isDebugEnabled()){
				logger.debug("status------------->"+status);
			}
			
			plpStudentWSList = (List<PLPStudentWrokSheet>) hibernateTemplate.find(queryString.toString(), status);
			if(plpStudentWSList != null && !plpStudentWSList.isEmpty() && plpStudentWSList.size() > 0){
				if(logger.isDebugEnabled()){
					logger.debug("plpStudentWSList.size()------------->"+plpStudentWSList.size());
				}
			}
			
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in PLPStudentWorkSheetDAOImpl.fetchAllCompletedWorkSheets()----", ex);
			throw ex;
		}
		return plpStudentWSList;
	}
}
