package com.teachlearnweb.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.TopicMasterDAO;
import com.teachlearnweb.dao.entity.PLPTopicMaster;

/**
 * Class provides the implementations of TopicMasterDAO Interface to perform DML
 * operations.
 * 
 * @author Satya.
 */
@Repository(value = "topicMasterDao")
@Transactional
public class TopicMasterDAOImpl extends AbstractDAO<PLPTopicMaster> implements TopicMasterDAO {

	// logger
		private static final Logger logger = Logger.getLogger(TopicMasterDAOImpl.class);
	
	/**
	 * Method to fetch plptopicmaster by subTopicId.
	 * 
	 * @return
	 */
	public PLPTopicMaster getTopicMasterBySubTopicId(Integer subTopicId, Short classId, Short subjectId) {
		logger.debug("getTopicMasterBySubTopicId(subTopicId) Method Start in TopicMastertDAOImpl");
		final StringBuilder queryString = new StringBuilder();
		List<PLPTopicMaster> topicMastersList = null;
		PLPTopicMaster plpTopicMaster = null;
		try {
			queryString.append("from PLPTopicMaster as topic where topic.id.subtopicId=? and topic.id.classId = ? and topic.id.subjectId = ?");
			if (logger.isDebugEnabled()) {
				logger.debug("subjectId::" + subTopicId);
				logger.debug("classId::" + classId);
				logger.debug("subjectId::" + subjectId);
			}
			topicMastersList = (List<PLPTopicMaster>) hibernateTemplate.find(queryString.toString(), subTopicId, classId, subjectId);
			if (topicMastersList != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("topicMastersList size::" + topicMastersList.size());
				}
			}
			if(topicMastersList != null && !topicMastersList.isEmpty() && topicMastersList.size() > 0) {
				plpTopicMaster = topicMastersList.get(0);
			}
		} catch (HibernateException e) {
			logger.error("Hibernate exception occured while fetching PLPTopicMaster for " + subTopicId, e);
			throw e;
		}
		return plpTopicMaster;
	}

	
	/**
	 * Method to fetch plptopicmaster by topicId.
	 * 
	 * @return
	 */
	public PLPTopicMaster getTopicMasterByTopicId(Integer topicId, Short classId, Short subjectId) {
		logger.debug("getTopicMasterByTopicId(topicId) Method Start in TopicMastertDAOImpl");
		final StringBuilder queryString = new StringBuilder();
		List<PLPTopicMaster> topicMastersList = null;
		PLPTopicMaster plpTopicMaster = null;
		try {
			queryString.append("from PLPTopicMaster as topic where topic.id.topicId=? and topic.id.classId = ? and topic.id.subjectId = ?");
			if (logger.isDebugEnabled()) {
				logger.debug("subjectId::" + topicId);
				logger.debug("classId::" + classId);
				logger.debug("subjectId::" + subjectId);
			}
			topicMastersList = (List<PLPTopicMaster>) hibernateTemplate.find(queryString.toString(), topicId, classId, subjectId);
			if (topicMastersList != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("topicMastersList size::" + topicMastersList.size());
				}
			}
			if(topicMastersList != null && !topicMastersList.isEmpty() && topicMastersList.size() > 0) {
				plpTopicMaster = topicMastersList.get(0);
			}
		} catch (HibernateException e) {
			logger.error("Hibernate exception occured while fetching PLPTopicMaster for " + topicId, e);
			throw e;
		}
		return plpTopicMaster;
	}

	/**
	 * Method to fetch TopicMaster by topicName.
	 * 
	 * @param topicName
	 * @return
	 */
	public PLPTopicMaster getTopicMasterByTopicName(String topicName, Short classId, Short subjectId) {
		logger.debug("getTopicMasterByTopicName(topicName, classId, subjectId) Method Start in TopicMastertDAOImpl");
		final StringBuilder queryString = new StringBuilder();
		List<PLPTopicMaster> topicMastersList = null;
		PLPTopicMaster plpTopicMaster = null;
		try {
			queryString.append("from PLPTopicMaster as topic where topic.topicName=? and topic.id.classId = ? and topic.id.subjectId = ?");
			if (logger.isDebugEnabled()) {
				logger.debug("topicName::" + topicName);
				logger.debug("classId::" + classId);
				logger.debug("subjectId::" + subjectId);
			}
			topicMastersList = (List<PLPTopicMaster>) hibernateTemplate.find(queryString.toString(), topicName, classId, subjectId);
			if (topicMastersList != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("topicMastersList size::" + topicMastersList.size());
				}
			}
			if(topicMastersList != null && !topicMastersList.isEmpty() && topicMastersList.size() > 0) {
				plpTopicMaster = topicMastersList.get(0);
			}
		} catch (HibernateException e) {
			logger.error("Hibernate exception occured while fetching PLPTopicMaster for " + topicName, e);
			throw e;
		}
		return plpTopicMaster;
	}

	/**
	 * Method to fetch TopicMaster by topicName.
	 * 
	 * @param topicName
	 * @return
	 */
	public PLPTopicMaster getTopicMasterByTopicName(String topicName, Integer parentTopicId, Short classId, Short subjectId) {
		logger.debug("getTopicMasterByTopicName(topicName, parentTopicId, classId, subjectId) Method Start in TopicMastertDAOImpl");
		final StringBuilder queryString = new StringBuilder();
		List<PLPTopicMaster> topicMastersList = null;
		PLPTopicMaster plpTopicMaster = null;
		try {
			queryString.append("from PLPTopicMaster as topic where topic.topicName=? and topic.id.parentId = ? and topic.id.classId = ? and topic.id.subjectId = ?");
			if (logger.isDebugEnabled()) {
				logger.debug("topicName::" + topicName);
				logger.debug("parentTopicId::" + parentTopicId);
				logger.debug("classId::" + classId);
				logger.debug("subjectId::" + subjectId);
			}
			topicMastersList = (List<PLPTopicMaster>) hibernateTemplate.find(queryString.toString(), topicName, parentTopicId, classId, subjectId);
			if (topicMastersList != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("topicMastersList size::" + topicMastersList.size());
				}
			}
			if(topicMastersList != null && !topicMastersList.isEmpty()) {
				plpTopicMaster = topicMastersList.get(0);
			}
		} catch (HibernateException e) {
			logger.error("Hibernate exception occured while fetching PLPTopicMaster for " + topicName, e);
			throw e;
		}
		return plpTopicMaster;
	}

}
