package com.teachlearnweb.dao.entity;
// default package
// Generated Jan 28, 2013 10:08:44 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * SkillId generated by hbm2java
 */
@Embeddable
public class SkillId implements java.io.Serializable {

	private short classId;
	private String shortCode;
	private short skillId;
	private String skillName;
	private short subjectId;

	public SkillId() {
	}

	public SkillId(short classId, String shortCode, short skillId,
			String skillName, short subjectId) {
		this.classId = classId;
		this.shortCode = shortCode;
		this.skillId = skillId;
		this.skillName = skillName;
		this.subjectId = subjectId;
	}

	@Column(name = "ClassId", nullable = false)
	public short getClassId() {
		return this.classId;
	}

	public void setClassId(short classId) {
		this.classId = classId;
	}

	@Column(name = "ShortCode", nullable = false, length = 5)
	public String getShortCode() {
		return this.shortCode;
	}

	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}

	@Column(name = "SkillId", nullable = false)
	public short getSkillId() {
		return this.skillId;
	}

	public void setSkillId(short skillId) {
		this.skillId = skillId;
	}

	@Column(name = "SkillName", nullable = false, length = 100)
	public String getSkillName() {
		return this.skillName;
	}

	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}

	@Column(name = "SubjectId", nullable = false)
	public short getSubjectId() {
		return this.subjectId;
	}

	public void setSubjectId(short subjectId) {
		this.subjectId = subjectId;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof SkillId))
			return false;
		SkillId castOther = (SkillId) other;

		return (this.getClassId() == castOther.getClassId())
				&& ((this.getShortCode() == castOther.getShortCode()) || (this
						.getShortCode() != null
						&& castOther.getShortCode() != null && this
						.getShortCode().equals(castOther.getShortCode())))
				&& (this.getSkillId() == castOther.getSkillId())
				&& ((this.getSkillName() == castOther.getSkillName()) || (this
						.getSkillName() != null
						&& castOther.getSkillName() != null && this
						.getSkillName().equals(castOther.getSkillName())))
				&& (this.getSubjectId() == castOther.getSubjectId());
	}

	public int hashCode() {
		int result = 17;

		result = 37 * result + this.getClassId();
		result = 37 * result
				+ (getShortCode() == null ? 0 : this.getShortCode().hashCode());
		result = 37 * result + this.getSkillId();
		result = 37 * result
				+ (getSkillName() == null ? 0 : this.getSkillName().hashCode());
		result = 37 * result + this.getSubjectId();
		return result;
	}

}
