
package com.teachlearnweb.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "tlw_rewardpoints", catalog = "tlw")
public class RewardPoints implements java.io.Serializable {

		
	private static final long serialVersionUID = -2828861827483771607L;
	// Fields

	private Long rewardPointsId;
	
	@Id
	@GeneratedValue (strategy = GenerationType.IDENTITY )
	@Column(name = "rewardPointsId",nullable = false)
	public Long getRewardPointsId() {
		return rewardPointsId;
	}

	public void setRewardPointsId(Long rewardPointsId) {
		this.rewardPointsId = rewardPointsId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((description == null) ? 0 : description.hashCode());
		result = prime * result
				+ ((rewardPoints == null) ? 0 : rewardPoints.hashCode());
		result = prime * result
				+ ((rewardPointsId == null) ? 0 : rewardPointsId.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "RewardPoints [rewardPointsId=" + rewardPointsId + ", userId="
				+ userId + ", rewardPoints=" + rewardPoints + ", description="
				+ description + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RewardPoints other = (RewardPoints) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (rewardPoints == null) {
			if (other.rewardPoints != null)
				return false;
		} else if (!rewardPoints.equals(other.rewardPoints))
			return false;
		if (rewardPointsId == null) {
			if (other.rewardPointsId != null)
				return false;
		} else if (!rewardPointsId.equals(other.rewardPointsId))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

	private Long userId;
	

	private Long rewardPoints;
	
	private String description;
	
	private String rewardDate;
	
	// Constructors

	/** default constructor */
	public RewardPoints() {

	}

	/** minimal constructor */
	public RewardPoints(
		Long userId, Long rewardPoints,String description, String rewardDate) {
		this.userId = userId;
		this.rewardPoints = rewardPoints;	
		this.description = description;	
		this.rewardDate = rewardDate;
	}
	
	@Column(name = "userId", unique = false, nullable = false)
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Column(name = "rewardPoints", unique = false, nullable = false)
	public Long getRewardPoints() {
		return rewardPoints;
	}

	public void setRewardPoints(Long rewardPoints) {
		this.rewardPoints = rewardPoints;
	}

	public String getDescription() {
		return description;
	}

	@Column(name = "description", unique = false, nullable = false)
	public void setDescription(String description) {
		this.description = description;
	}
	@Column(name = "rewardDate", unique = false, nullable = false)
	public void setRewardDate(String rewardDate) {
		this.rewardDate = rewardDate;
	}
	
	public String getRewardDate() {
		return rewardDate;
	}

	
}
