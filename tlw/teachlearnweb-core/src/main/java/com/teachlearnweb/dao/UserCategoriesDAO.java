
package com.teachlearnweb.dao;

import java.util.List;

import org.springframework.stereotype.Component;
import com.teachlearnweb.dao.entity.UserCategories;

/**
 * Interface EmployeeDAO declares DML operations on Employee table.
 * 
 * @author GalaxE.
 */
@Component
public interface UserCategoriesDAO extends GenericDAO<UserCategories> {

	public void addCourse(UserCategories userCategories);
	public List<UserCategories> categoriesByUserId(long userId) ;
}

