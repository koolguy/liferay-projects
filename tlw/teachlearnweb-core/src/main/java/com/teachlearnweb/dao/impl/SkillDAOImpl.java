package com.teachlearnweb.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.SkillDAO;
import com.teachlearnweb.dao.entity.Skill;

/**
 * Class provides the implementations of AssessmentDAO Interface to perform DML
 * operations.
 * 
 * @author Govinda.
 */
@Repository(value = "skillDAO")
@Transactional
public class SkillDAOImpl extends AbstractDAO<Skill> implements SkillDAO {
	// logger
	private static final Logger logger = Logger.getLogger(SkillDAOImpl.class);

	public List<Skill> getSkillsBySubject(final Short subjectId) throws HibernateException {
		logger.debug("START");
		List<Skill> skills = null;
		final StringBuilder queryString = new StringBuilder();
		queryString.append("from Skill as S where S.subjectMaster.subjectId=?");
		skills = (List<Skill>) hibernateTemplate.find(queryString.toString(), subjectId);
		logger.debug("END");
		return skills;
	}

	/**
	 * Method to fetch skill by skillName.
	 * 
	 * @param skillName
	 * @return
	 */
	public Skill getSkillBySkillName(String skillName) {
		logger.debug("getSkillBySkillName() Method Start in SkillDAOImpl");
		List<Skill> skillsList = null;
		Skill skill = null;
		final StringBuilder queryString = new StringBuilder();
		try {
			queryString.append("from Skill as S where S.id.skillName=?");
			skillsList = (List<Skill>) hibernateTemplate.find(
					queryString.toString(), skillName.trim());
		} catch (HibernateException e) {
			logger.debug("Exception Caught While Processing getSkillBySkillName() in SkillDAOImpl", e);
		}
		if(skillsList != null && !skillsList.isEmpty() && skillsList.size() > 0) {
			skill = skillsList.get(0);
		}
		logger.debug("END");
		
		return skill;
	}
}
