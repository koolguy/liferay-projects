package com.teachlearnweb.dao.entity;
// default package
// Generated Jan 28, 2013 10:08:44 PM by Hibernate Tools 3.4.0.CR1

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * ClassMaster generated by hbm2java
 */
@Entity
@Table(name = "tlw_classmaster", catalog = "tlw")
public class ClassMaster implements java.io.Serializable {

	private Short classId;
	private String className;
	private Set<DTTest> DTTests = new HashSet<DTTest>(0);
	private Set<BookMaster> bookMasters = new HashSet<BookMaster>(0);
	private Set<PLPTopicMaster> PLPTopicMasters = new HashSet<PLPTopicMaster>(0);
	private Set<DTAssessment> DTAssessments = new HashSet<DTAssessment>(0);
	private Set<DTQuestion> DTQuestions = new HashSet<DTQuestion>(0);
	private Set<PLPQuestion> PLPQuestions = new HashSet<PLPQuestion>(0);
	private Set<PLPStudentWrokSheet> PLPStudentWrokSheets = new HashSet<PLPStudentWrokSheet>(
			0);

	public ClassMaster() {
	}

	public ClassMaster(String className) {
		this.className = className;
	}

	public ClassMaster(String className, Set<DTTest> DTTests,
			Set<BookMaster> bookMasters, Set<PLPTopicMaster> PLPTopicMasters,
			Set<DTAssessment> DTAssessments, Set<DTQuestion> DTQuestions,
			Set<PLPQuestion> PLPQuestions,
			Set<PLPStudentWrokSheet> PLPStudentWrokSheets) {
		this.className = className;
		this.DTTests = DTTests;
		this.bookMasters = bookMasters;
		this.PLPTopicMasters = PLPTopicMasters;
		this.DTAssessments = DTAssessments;
		this.DTQuestions = DTQuestions;
		this.PLPQuestions = PLPQuestions;
		this.PLPStudentWrokSheets = PLPStudentWrokSheets;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ClassId", unique = true, nullable = false)
	public Short getClassId() {
		return this.classId;
	}

	public void setClassId(Short classId) {
		this.classId = classId;
	}

	@Column(name = "ClassName", nullable = false, length = 25)
	public String getClassName() {
		return this.className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "classMaster")
	public Set<DTTest> getDTTests() {
		return this.DTTests;
	}

	public void setDTTests(Set<DTTest> DTTests) {
		this.DTTests = DTTests;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "classMaster")
	public Set<BookMaster> getBookMasters() {
		return this.bookMasters;
	}

	public void setBookMasters(Set<BookMaster> bookMasters) {
		this.bookMasters = bookMasters;
	}

	@OneToMany(fetch = FetchType.LAZY)
	public Set<PLPTopicMaster> getPLPTopicMasters() {
		return this.PLPTopicMasters;
	}

	public void setPLPTopicMasters(Set<PLPTopicMaster> PLPTopicMasters) {
		this.PLPTopicMasters = PLPTopicMasters;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "classMaster")
	public Set<DTAssessment> getDTAssessments() {
		return this.DTAssessments;
	}

	public void setDTAssessments(Set<DTAssessment> DTAssessments) {
		this.DTAssessments = DTAssessments;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "classMaster")
	public Set<DTQuestion> getDTQuestions() {
		return this.DTQuestions;
	}

	public void setDTQuestions(Set<DTQuestion> DTQuestions) {
		this.DTQuestions = DTQuestions;
	}

	@OneToMany(fetch = FetchType.LAZY)
	public Set<PLPQuestion> getPLPQuestions() {
		return this.PLPQuestions;
	}

	public void setPLPQuestions(Set<PLPQuestion> PLPQuestions) {
		this.PLPQuestions = PLPQuestions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "classMaster")
	public Set<PLPStudentWrokSheet> getPLPStudentWrokSheets() {
		return this.PLPStudentWrokSheets;
	}

	public void setPLPStudentWrokSheets(
			Set<PLPStudentWrokSheet> PLPStudentWrokSheets) {
		this.PLPStudentWrokSheets = PLPStudentWrokSheets;
	}

}
