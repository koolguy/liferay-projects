
package com.teachlearnweb.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Component;

import com.teachlearnweb.dao.entity.DTAssessmentDetails;

/**
 * Interface AssessmentDetailsDAO declares DML operations on AssessmentDetails table.
 * 
 * @author Govinda.
 */
@Component
public interface AssessmentDetailsDAO extends GenericDAO<DTAssessmentDetails> {
	public List<DTAssessmentDetails> getStyleBasedAssessmentDetails(final int assessmentId, final short styleCodeId) throws HibernateException;
	public List<DTAssessmentDetails> getAssessmentDetails(int assessmentId) throws HibernateException;
	
	/**
	 * Method to update assessmentDetail.
	 * 
	 * @param assessmentDetailsList
	 */
	public void updateAssessmentDetails(List<DTAssessmentDetails> assessmentDetailsList);
	/**
	 * Method to save assessmentDetailList.
	 * 
	 * @param assessmentDetailsList
	 */
	public void saveAssessmentDetailsList(List<DTAssessmentDetails> assessmentDetailsList);
	
	/**
	 * Method to save assessmentDetail.
	 * 
	 * @param assessmentDetail
	 * @return 
	 */
	public void saveAssessmentDetail(DTAssessmentDetails assessmentDetail);
	
	/**
	 * Method to fetch DTAssessmentDetail by questionId.
	 * 
	 * @param questionId
	 * @return
	 */
	public DTAssessmentDetails getDTAssessmentDetailsByQuestionId(int questionId);
}