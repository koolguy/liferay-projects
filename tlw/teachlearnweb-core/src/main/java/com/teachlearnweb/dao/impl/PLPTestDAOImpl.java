package com.teachlearnweb.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.PLPTestDAO;
import com.teachlearnweb.dao.entity.PLPTest;

/**
 * Class provides the implementations of PLPTestDAO Interface to perform DML
 * operations.
 * 
 * @author Satya.
 */
@Repository(value = "plpTestDAO")
@Transactional
public class PLPTestDAOImpl extends AbstractDAO<PLPTest> implements PLPTestDAO{

	// logger
	private static final Logger logger = Logger.getLogger(PLPTestDAOImpl.class);
	
	/**
	 * Method used to save plpTest.
	 * 
	 * @param plpTest
	 * @return
	 */
	public void savePLPTest(PLPTest plpTest) {
		logger.debug("savePLPTest() Method Start in PLPTestDAOImpl");
		 save(plpTest);
	}

	/**
	 * Method used to save plpTestList.
	 * 
	 * @param plpTestList
	 */
	public void savePLPTestList(List<PLPTest> plpTestList) {
		logger.debug("savePLPTestList() Method Start in PLPTestDAOImpl");
		if(plpTestList != null && !plpTestList.isEmpty() && plpTestList.size() > 0){
			for(PLPTest test : plpTestList) {
				savePLPTest(test);
			}
		}
		
	}

}
