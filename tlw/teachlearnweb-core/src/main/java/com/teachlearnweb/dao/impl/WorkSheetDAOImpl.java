package com.teachlearnweb.dao.impl;

import java.util.List;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.WorkSheetDAO;
import com.teachlearnweb.dao.entity.PLPStudentWrokSheet;

/**
 * Class provides the implementations of WorkSheetDAO Interface to perform DML
 * operations.
 * 
 * @author Satya.
 */
@Repository(value = "workSheetDAO")
@Transactional
public class WorkSheetDAOImpl extends AbstractDAO<PLPStudentWrokSheet> implements WorkSheetDAO{

	// logger
	private static final Logger logger = Logger.getLogger(WorkSheetDAOImpl.class);
		
	/**
	 * Method to fetch worksheets by topicId.
	 * 
	 * @return List<WorkSheet>
	 */
	@SuppressWarnings("unchecked")
	public List<PLPStudentWrokSheet> getEOCTestsByTopicId(Integer topicId, long userId, Short classId, Short subjectId) {
		logger.debug("getEOCTestsByTopicId(topicId, userId, classId, subjectId) Method Start in WorkSheetDAOImpl");
		List<PLPStudentWrokSheet> workSheetsList = null;
		final StringBuilder queryString = new StringBuilder();
		try{
			queryString.append("from PLPStudentWrokSheet as ws where ws.topicId = ? and ws.userId = ? and ws.classMaster.classId = ? and ws.subjectMaster.subjectId = ? and ws.subtopicId is null");
			if(logger.isDebugEnabled()){
				logger.debug("topicId------------->"+topicId);
				logger.debug("userId-------------->"+userId);
				logger.debug("classId------------->"+classId);
				logger.debug("subjectId-------------->"+subjectId);
			}
			workSheetsList = (List<PLPStudentWrokSheet>)hibernateTemplate.find(queryString.toString(), topicId, userId, classId, subjectId);
			if(workSheetsList != null){
				if(logger.isDebugEnabled()){
					logger.debug("workSheetsList.size()------------->"+workSheetsList.size());
				}
			}
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in WorkSheetDAOImpl.getWorkSheetsByTopicId()----", ex);
			throw ex;
		}
		return workSheetsList;
	}

	/**
	 * Method to fetch worksheets by subTopicId.
	 * 
	 * @return List<WorkSheet>
	 */
	@SuppressWarnings("unchecked")
	public List<PLPStudentWrokSheet> getWorkSheetsBySubTopicId(
			Integer subTopicId, long userId, Short classId, Short subjectId) {
		logger.debug("getWorkSheetsBySubTopicId(subTopicId, userId, classId, subjectId) Method Start in WorkSheetDAOImpl");
		List<PLPStudentWrokSheet> workSheetsList = null;
		final StringBuilder queryString = new StringBuilder();
		try{
			queryString.append("from PLPStudentWrokSheet as ws where ws.subtopicId = ? and ws.userId = ? and ws.classMaster.classId = ? and ws.subjectMaster.subjectId = ?");
			if(logger.isDebugEnabled()){
				logger.debug("subTopicId------------->"+subTopicId);
				logger.debug("userid------------->"+userId);
				logger.debug("classId------------->"+classId);
				logger.debug("subjectId------------->"+subjectId);
			}
			workSheetsList = hibernateTemplate.find(queryString.toString(), subTopicId, userId, classId, subjectId);
			if(workSheetsList != null){
				if(logger.isDebugEnabled()){
					logger.debug("workSheetsList.size()------------->"+workSheetsList.size());
				}
			}
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in WorkSheetDAOImpl.getWorkSheetsBySubTopicId()----", ex);
			throw ex;
		}
		return workSheetsList;
	}
	
	/**
	 * Method to fetch worksheets by worksheetId.
	 * 
	 * @return List<WorkSheet>
	 */
	@SuppressWarnings("unchecked")
	public List<PLPStudentWrokSheet> getWorkSheetsByWorkSheetId(Integer workSheetId) {
		logger.debug("getWorkSheetsByWorkSheetId() Method Start in WorkSheetDAOImpl");
		return (List<PLPStudentWrokSheet>) findById(PLPStudentWrokSheet.class, workSheetId);
	}
	

}
