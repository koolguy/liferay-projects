
package com.teachlearnweb.dao.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Class is used to capture the characteristics of ci_employee. 
 * This is reusable class for ci_employee.
 * @author GalaxE.
 */
@Entity
@Table(name = "tlw_couponcode")
public class CouponCode implements java.io.Serializable {

		
	private static final long serialVersionUID = -2828861827483771607L;
	// Fields
	
	private Long couponId;

	private String couponCode;
	

	private String description;
	
	private String amount;
	
	// Constructors

	
	

	/** default constructor */
	public CouponCode() {

	}

	/** minimal constructor */
	public CouponCode(
		String couponCode, String description,String amount) {
		super();		
		this.couponCode = couponCode;
		this.description = description;		
		this.amount = amount;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "couponId", unique = true, nullable = false)
	public Long getCouponId() {
		return couponId;
	}

	public void setCouponId(Long couponId) {
		this.couponId = couponId;
	}

	@Column(name = "couponCode", unique = false, nullable = false)
	public String getCouponCode() {
		return couponCode;
	}

	public void setCouponCode(String couponCode) {
		this.couponCode = couponCode;
	}

	@Column(name = "description", unique = false, nullable = false)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "amount", unique = false, nullable = false)
	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}
	

	
	
}
