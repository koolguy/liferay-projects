package com.teachlearnweb.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.AssessmentDetailsDAO;
import com.teachlearnweb.dao.entity.DTAssessmentDetails;

/**
 * Class provides the implementations of AssessmentDAO Interface to perform DML
 * operations.
 * 
 * @author Govinda.
 */
@Repository(value = "assessmentDetailsDAO")
@Transactional
public class AssessmentDetailsDAOImpl extends AbstractDAO<DTAssessmentDetails>
		implements AssessmentDetailsDAO {
	// logger
	private static final Logger logger = Logger
			.getLogger(AssessmentDetailsDAOImpl.class);
	@Autowired
	private SessionFactory sessionFactory;

	public List<DTAssessmentDetails> getStyleBasedAssessmentDetails(final int assessmentId, final short styleCodeId) throws HibernateException {
		logger.debug("START");
		List<DTAssessmentDetails> assessmentDetails = null;
		final StringBuilder queryString = new StringBuilder();
		try {
			// if style code id is available
			if (styleCodeId > 0) {
				queryString.append("from DTAssessmentDetails as D, DTAnswer as A " +
						"where D.answer IS NOT NULL and D.answer=A.answerId and A.styleCode.codeId=? and D.DTAssessment.assessmentId=?");
				assessmentDetails = (List<DTAssessmentDetails>) hibernateTemplate.find(queryString.toString(), styleCodeId, assessmentId);
			} else {
				// otherwise get all questions which were not answered
				queryString.append("from DTAssessmentDetails as D " +
						"where D.answer IS NULL and D.DTAssessment.assessmentId=?");
				assessmentDetails = (List<DTAssessmentDetails>) hibernateTemplate.find(queryString.toString(), assessmentId);
			}
			if (logger.isDebugEnabled()) {
				logger.debug("assessmentId::" + assessmentId);
				logger.debug("styleCodeId::" + styleCodeId);
				logger.debug("queryString::" + queryString);
			}
			if (assessmentDetails != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("assessmentDetailsList size::" + assessmentDetails.size());
				}
			}
		} catch (HibernateException e) {
			logger.error("Hibernate exception occured while fetching assessment details.", e);
			throw e;
		}
		logger.debug("END");
		return assessmentDetails;
	}

	public List<DTAssessmentDetails> getAssessmentDetails(final int assessmentId) throws HibernateException {
		logger.debug("START");
		List<DTAssessmentDetails> assessmentDetails = null;
		final StringBuilder queryString = new StringBuilder();
		try {
//			queryString.append("from DTAssessmentDetails as d INNER JOIN fetch d.DTQuestion as q INNER JOIN fetch q.DTAnswers as a where assessmentId=?");
			queryString.append("from DTAssessmentDetails as d INNER JOIN fetch d.DTQuestion as q where assessmentId=?");
			if (logger.isDebugEnabled()) {
				logger.debug("assessmentId::" + assessmentId);
				logger.debug("queryString::" + queryString);
			}
			assessmentDetails = (List<DTAssessmentDetails>) hibernateTemplate.find(queryString.toString(), assessmentId);
			if (assessmentDetails != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("assessmentDetailsList size::" + assessmentDetails.size());
				}
			}
		} catch (HibernateException e) {
			logger.error("Hibernate exception occured while fetching assessment details.", e);
			throw e;
		}
		logger.debug("END");
		return assessmentDetails;
	}

	/**
	 * Method to update assessmentDetail.
	 * 
	 * @param assessmentDetailsList
	 */
	public void updateAssessmentDetails(List<DTAssessmentDetails> assessmentDetailsList) {
		logger.debug("START");
		if(assessmentDetailsList != null && !assessmentDetailsList.isEmpty() && assessmentDetailsList.size() > 0){
			for(DTAssessmentDetails assessmentDetail : assessmentDetailsList) {
				update(assessmentDetail);
			}
		}
		logger.debug("END");
	}
	/**
	 * Method to save assessmentDetailList.
	 * 
	 * @param assessmentDetailsList
	 */
	public void saveAssessmentDetailsList(List<DTAssessmentDetails> assessmentDetailsList) {
		logger.debug("AssessmentDetailsDAOImpl.saveAssessmentDetails() Method Start");
		if(assessmentDetailsList != null && !assessmentDetailsList.isEmpty() && assessmentDetailsList.size() > 0){
			for(DTAssessmentDetails assessmentDetail : assessmentDetailsList) {
				saveAssessmentDetail(assessmentDetail);
			}
		}
		
	}
	
	

	/**
	 * Method to fetch DTAssessmentDetail by questionId.
	 * 
	 * @param questionId
	 * @return
	 */
	public DTAssessmentDetails getDTAssessmentDetailsByQuestionId(int questionId) {
		logger.debug("AssessmentDetailsDAOImpl.getDTAssessmentDetailsByQuestionId() Method Start");
		DTAssessmentDetails dtAssessmentDetails = null;
		Session session = getSession();
		Transaction tx= null;
		try {
			tx= session.beginTransaction();
			String dtaHql = "from DTAssessmentDetails dta where dta.DTQuestion.questionId :=questionId";
			dtAssessmentDetails = (DTAssessmentDetails) session.createQuery(dtaHql).setParameter("questionId", questionId);
			tx.commit();
			
		} catch (HibernateException e) {
			if(tx != null)
			tx.rollback();
			logger.error("Exception caught in DTQuestionDAOImpl.getSkills()", e);
		}
		return dtAssessmentDetails;
	}

	/**
	 * Method to save assessmentDetail.
	 * 
	 * @param assessmentDetail
	 */
	public void saveAssessmentDetail(DTAssessmentDetails assessmentDetail) {
		logger.debug("AssessmentDetailsDAOImpl.saveAssessmentDetail() Method Start");
		if(assessmentDetail != null) {
			save(assessmentDetail);
		}
		
	}
}
