package com.teachlearnweb.dao.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.teachlearnweb.dao.entity.TLWUser;


/**
 * Class is used to capture the characteristics of user.
 * This is a reusable class for users.
 * 
 * @author GalaxE
 */
@Entity
@Table(name="User_", uniqueConstraints = {@UniqueConstraint(columnNames={"companyId", "emailAddress"}), @UniqueConstraint(columnNames={"companyId", "screenName"}), @UniqueConstraint(columnNames="contactId"), @UniqueConstraint(columnNames={"companyId", "userId"})}
)

public class User  implements java.io.Serializable {
	
   
	 private static final long serialVersionUID = 4013119039465008257L;
	 // Fields    


     private Long userId;
     private String uuid;
     private Long companyId;
     private Timestamp createDate;
     private Timestamp modifiedDate;
     private Short defaultUser;
     private Long contactId;
     private String password;
     private Short passwordEncrypted;
     private Short passwordReset;
     private Timestamp passwordModifiedDate;
     private String digest;
     private String reminderQueryQuestion;
     private String reminderQueryAnswer;
     private Integer graceLoginCount;
     private String screenName;
     private String emailAddress;
     private Long facebookId;
     private String openId;
     private Long portraitId;
     private String languageId;
     private String timeZoneId;
     private String greeting;
     private String comments;
     private String firstName;
     private String middleName;
     private String lastName;
     private String jobTitle;
     private Timestamp loginDate;
     private String loginIp;
     private Timestamp lastLoginDate;
     private String lastLoginIp;
     private Timestamp lastFailedLoginDate;
     private Integer failedLoginAttempts;
     private Short lockout;
     private Timestamp lockoutDate;
     private Short agreedToTermsOfUse;
     private Short emailAddressVerified;
     private Integer status;
   

    // Constructors

    /** default constructor */
    public User() {
    }

	/** minimal constructor */
    public User(Long userId) {
        this.userId = userId;
    }
    
    /** full constructor */
    public User(Long userId, String uuid, Long companyId, Timestamp createDate, Timestamp modifiedDate, Short defaultUser, Long contactId, String password, Short passwordEncrypted, Short passwordReset, Timestamp passwordModifiedDate, String digest, String reminderQueryQuestion, String reminderQueryAnswer, Integer graceLoginCount, String screenName, String emailAddress, Long facebookId, String openId, Long portraitId, String languageId, String timeZoneId, String greeting, String comments, String firstName, String middleName, String lastName, String jobTitle, Timestamp loginDate, String loginIp, Timestamp lastLoginDate, String lastLoginIp, Timestamp lastFailedLoginDate, Integer failedLoginAttempts, Short lockout, Timestamp lockoutDate, Short agreedToTermsOfUse, Short emailAddressVerified, Integer status, Set<TLWUser> CIEmployeesForAdminAssistId, Set<TLWUser> CIEmployeesForSupervisorId) {
        this.userId = userId;
        this.uuid = uuid;
        this.companyId = companyId;
        this.createDate = createDate;
        this.modifiedDate = modifiedDate;
        this.defaultUser = defaultUser;
        this.contactId = contactId;
        this.password = password;
        this.passwordEncrypted = passwordEncrypted;
        this.passwordReset = passwordReset;
        this.passwordModifiedDate = passwordModifiedDate;
        this.digest = digest;
        this.reminderQueryQuestion = reminderQueryQuestion;
        this.reminderQueryAnswer = reminderQueryAnswer;
        this.graceLoginCount = graceLoginCount;
        this.screenName = screenName;
        this.emailAddress = emailAddress;
        this.facebookId = facebookId;
        this.openId = openId;
        this.portraitId = portraitId;
        this.languageId = languageId;
        this.timeZoneId = timeZoneId;
        this.greeting = greeting;
        this.comments = comments;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.jobTitle = jobTitle;
        this.loginDate = loginDate;
        this.loginIp = loginIp;
        this.lastLoginDate = lastLoginDate;
        this.lastLoginIp = lastLoginIp;
        this.lastFailedLoginDate = lastFailedLoginDate;
        this.failedLoginAttempts = failedLoginAttempts;
        this.lockout = lockout;
        this.lockoutDate = lockoutDate;
        this.agreedToTermsOfUse = agreedToTermsOfUse;
        this.emailAddressVerified = emailAddressVerified;
        this.status = status;
    
       
    }

   
    // Property accessors
    @Id 
    
    @Column(name="userId", unique=true, nullable=false)

    public Long getUserId() {
        return this.userId;
    }
    
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    
    @Column(name="uuid_", length=75)

    public String getUuid() {
        return this.uuid;
    }
    
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    
    @Column(name="companyId")

    public Long getCompanyId() {
        return this.companyId;
    }
    
    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }
    
    @Column(name="createDate", length=0)

    public Timestamp getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
    
    @Column(name="modifiedDate", length=0)

    public Timestamp getModifiedDate() {
        return this.modifiedDate;
    }
    
    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
    
    @Column(name="defaultUser")

    public Short getDefaultUser() {
        return this.defaultUser;
    }
    
    public void setDefaultUser(Short defaultUser) {
        this.defaultUser = defaultUser;
    }
    
    @Column(name="contactId", unique=true)

    public Long getContactId() {
        return this.contactId;
    }
    
    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }
    
    @Column(name="password_", length=75)

    public String getPassword() {
        return this.password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    @Column(name="passwordEncrypted")

    public Short getPasswordEncrypted() {
        return this.passwordEncrypted;
    }
    
    public void setPasswordEncrypted(Short passwordEncrypted) {
        this.passwordEncrypted = passwordEncrypted;
    }
    
    @Column(name="passwordReset")

    public Short getPasswordReset() {
        return this.passwordReset;
    }
    
    public void setPasswordReset(Short passwordReset) {
        this.passwordReset = passwordReset;
    }
    
    @Column(name="passwordModifiedDate", length=0)

    public Timestamp getPasswordModifiedDate() {
        return this.passwordModifiedDate;
    }
    
    public void setPasswordModifiedDate(Timestamp passwordModifiedDate) {
        this.passwordModifiedDate = passwordModifiedDate;
    }
    
    @Column(name="digest")

    public String getDigest() {
        return this.digest;
    }
    
    public void setDigest(String digest) {
        this.digest = digest;
    }
    
    @Column(name="reminderQueryQuestion", length=75)

    public String getReminderQueryQuestion() {
        return this.reminderQueryQuestion;
    }
    
    public void setReminderQueryQuestion(String reminderQueryQuestion) {
        this.reminderQueryQuestion = reminderQueryQuestion;
    }
    
    @Column(name="reminderQueryAnswer", length=75)

    public String getReminderQueryAnswer() {
        return this.reminderQueryAnswer;
    }
    
    public void setReminderQueryAnswer(String reminderQueryAnswer) {
        this.reminderQueryAnswer = reminderQueryAnswer;
    }
    
    @Column(name="graceLoginCount")

    public Integer getGraceLoginCount() {
        return this.graceLoginCount;
    }
    
    public void setGraceLoginCount(Integer graceLoginCount) {
        this.graceLoginCount = graceLoginCount;
    }
    
    @Column(name="screenName", length=75)

    public String getScreenName() {
        return this.screenName;
    }
    
    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }
    
    @Column(name="emailAddress", length=75)

    public String getEmailAddress() {
        return this.emailAddress;
    }
    
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    
    @Column(name="facebookId")

    public Long getFacebookId() {
        return this.facebookId;
    }
    
    public void setFacebookId(Long facebookId) {
        this.facebookId = facebookId;
    }
    
    @Column(name="openId", length=1024)

    public String getOpenId() {
        return this.openId;
    }
    
    public void setOpenId(String openId) {
        this.openId = openId;
    }
    
    @Column(name="portraitId")

    public Long getPortraitId() {
        return this.portraitId;
    }
    
    public void setPortraitId(Long portraitId) {
        this.portraitId = portraitId;
    }
    
    @Column(name="languageId", length=75)

    public String getLanguageId() {
        return this.languageId;
    }
    
    public void setLanguageId(String languageId) {
        this.languageId = languageId;
    }
    
    @Column(name="timeZoneId", length=75)

    public String getTimeZoneId() {
        return this.timeZoneId;
    }
    
    public void setTimeZoneId(String timeZoneId) {
        this.timeZoneId = timeZoneId;
    }
    
    @Column(name="greeting")

    public String getGreeting() {
        return this.greeting;
    }
    
    public void setGreeting(String greeting) {
        this.greeting = greeting;
    }
    
    @Column(name="comments")

    public String getComments() {
        return this.comments;
    }
    
    public void setComments(String comments) {
        this.comments = comments;
    }
    
    @Column(name="firstName", length=75)

    public String getFirstName() {
        return this.firstName;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    @Column(name="middleName", length=75)

    public String getMiddleName() {
        return this.middleName;
    }
    
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    
    @Column(name="lastName", length=75)

    public String getLastName() {
        return this.lastName;
    }
    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    @Column(name="jobTitle", length=100)

    public String getJobTitle() {
        return this.jobTitle;
    }
    
    public void setJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
    }
    
    @Column(name="loginDate", length=0)

    public Timestamp getLoginDate() {
        return this.loginDate;
    }
    
    public void setLoginDate(Timestamp loginDate) {
        this.loginDate = loginDate;
    }
    
    @Column(name="loginIP", length=75)

    public String getLoginIp() {
        return this.loginIp;
    }
    
    public void setLoginIp(String loginIp) {
        this.loginIp = loginIp;
    }
    
    @Column(name="lastLoginDate", length=0)

    public Timestamp getLastLoginDate() {
        return this.lastLoginDate;
    }
    
    public void setLastLoginDate(Timestamp lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }
    
    @Column(name="lastLoginIP", length=75)

    public String getLastLoginIp() {
        return this.lastLoginIp;
    }
    
    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }
    
    @Column(name="lastFailedLoginDate", length=0)

    public Timestamp getLastFailedLoginDate() {
        return this.lastFailedLoginDate;
    }
    
    public void setLastFailedLoginDate(Timestamp lastFailedLoginDate) {
        this.lastFailedLoginDate = lastFailedLoginDate;
    }
    
    @Column(name="failedLoginAttempts")

    public Integer getFailedLoginAttempts() {
        return this.failedLoginAttempts;
    }
    
    public void setFailedLoginAttempts(Integer failedLoginAttempts) {
        this.failedLoginAttempts = failedLoginAttempts;
    }
    
    @Column(name="lockout")

    public Short getLockout() {
        return this.lockout;
    }
    
    public void setLockout(Short lockout) {
        this.lockout = lockout;
    }
    
    @Column(name="lockoutDate", length=0)

    public Timestamp getLockoutDate() {
        return this.lockoutDate;
    }
    
    public void setLockoutDate(Timestamp lockoutDate) {
        this.lockoutDate = lockoutDate;
    }
    
    @Column(name="agreedToTermsOfUse")

    public Short getAgreedToTermsOfUse() {
        return this.agreedToTermsOfUse;
    }
    
    public void setAgreedToTermsOfUse(Short agreedToTermsOfUse) {
        this.agreedToTermsOfUse = agreedToTermsOfUse;
    }
    
    @Column(name="emailAddressVerified")

    public Short getEmailAddressVerified() {
        return this.emailAddressVerified;
    }
    
    public void setEmailAddressVerified(Short emailAddressVerified) {
        this.emailAddressVerified = emailAddressVerified;
    }
    
    @Column(name="status")

    public Integer getStatus() {
        return this.status;
    }
    
    public void setStatus(Integer status) {
        this.status = status;
    }

   

   

}