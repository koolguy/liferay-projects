
package com.teachlearnweb.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.teachlearnweb.dao.entity.StyleCode;

/**
 * 
 * @author Govinda.
 */
@Component
public interface StyleCodeDAO extends GenericDAO<StyleCode> {
	/**
	 * Method to fetch skill by skillName.
	 * 
	 * @param skillName
	 * @return
	 */
	public List<StyleCode> getAllStyleCodes();
}

