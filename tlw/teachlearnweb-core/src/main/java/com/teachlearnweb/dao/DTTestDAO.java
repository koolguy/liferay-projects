package com.teachlearnweb.dao;

import org.springframework.stereotype.Component;
import com.teachlearnweb.dao.entity.DTTest;

/**
 * Interface DTTestDAO declares DML operations on DTTest table.
 * 
 * @author satya.
 */

@Component
public interface DTTestDAO extends GenericDAO<DTTest>{

	/**
	 * Method to fetch DTTest based on classId.
	 * 
	 * @param classId
	 * @return DTTest.
	 */
	public DTTest getDTTest(short classId);
	
	/**
	 * Method to fetch DTTest based on classId and subjectId.
	 * 
	 * @param classId
	 * @return DTTest.
	 */
	public DTTest getDTTest(short classId, short subjectId);
	
}
