
package com.teachlearnweb.dao.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Class is used to capture the characteristics of ci_employee. 
 * This is reusable class for ci_employee.
 * @author GalaxE.
 */
@Entity
@Table(name = "tlw_testmonial")
public class Testmonial implements java.io.Serializable {

		
	private static final long serialVersionUID = -2828861827483771607L;
	// Fields
	
	private Long testId;
	private Long userId;	
	private String description;
	private String status;	
	// Constructors
	

	/** default constructor */
	public Testmonial() {

	}

	/** minimal constructor */
	public Testmonial(
		long userId, String description,String status) {
		super();		
		this.userId = userId;
		this.description = description;		
		this.status = status;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "testId", unique = true, nullable = false)
	public Long getTestId() {
		return testId;
	}

	public void setTestId(Long testId) {
		this.testId = testId;
	}

	@Column(name = "userId", unique = false, nullable = false)
	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	@Column(name = "description", unique = false, nullable = false)
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "status", unique = false, nullable = false)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}	
}
