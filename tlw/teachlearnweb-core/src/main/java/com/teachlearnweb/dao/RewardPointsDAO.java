package com.teachlearnweb.dao;

import java.util.List;

import com.teachlearnweb.dao.entity.RewardPoints;

public interface RewardPointsDAO {
	
	public void addRewardPoints(RewardPoints rewardPoints);
	
	List<RewardPoints> rewardPointsByUserId(long userId);

	/**
	 * Method to fetch all reward points.
	 * 
	 * @return List<RewardPoints>
	 */
	List<RewardPoints> fetchRewardPoints();
}
