package com.teachlearnweb.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.SubjectsDAO;
import com.teachlearnweb.dao.entity.SubjectMaster;

/**
 * Class provides the implementations of AssessmentDAO Interface to perform DML
 * operations.
 * 
 * @author Govinda.
 */
@Repository(value = "subjectsDAO")
@Transactional
public class SubjectsDAOImpl extends AbstractDAO<SubjectMaster> implements SubjectsDAO {
	// logger
	private static final Logger logger = Logger.getLogger(SubjectsDAOImpl.class);
	
	public List<SubjectMaster> getSubjects() throws HibernateException {
		logger.debug("START");
		List<SubjectMaster> subjects = null;
		subjects = this.findAll(SubjectMaster.class);
		logger.debug("END");
		return subjects;
	}

	public SubjectMaster getSubject(Short subjectId) {
		return findById(SubjectMaster.class, subjectId);
	}

	/**
	 * Method to fetch subjectMaster by subjectName.
	 * 
	 * @param subjectName
	 * @return
	 */
	public SubjectMaster getSubjectMasterbySbujectName(String subjectName) {
		// TODO Auto-generated method stub
		SubjectMaster subjectMaster = null;
		List<SubjectMaster> subjectMasterList = null;
		final StringBuilder queryString = new StringBuilder();
		try{
			queryString.append("from SubjectMaster as subject where subject.subjectName = ? ");
			if(logger.isDebugEnabled()){
				logger.debug("subjectName------------->"+subjectName);
			}
			subjectMasterList = (List<SubjectMaster>) hibernateTemplate.find(queryString.toString(), subjectName);
			if(subjectMasterList != null && !subjectMasterList.isEmpty() && subjectMasterList.size() > 0){
				subjectMaster = subjectMasterList.get(0);
				if(logger.isDebugEnabled()){
					logger.debug("subjectMaster------------->"+subjectMaster);
				}
			}
			
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in PLPQuestionsDAOImpl.getQuestionById()----", ex);
			throw ex;
		}
		return subjectMaster;
	}
}
