package com.teachlearnweb.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.ClassMasterDAO;
import com.teachlearnweb.dao.entity.ClassMaster;

/**
 * Class provides the implementations of ClassMasterDAO Interface to perform DML
 * operations.
 * 
 * @author satya.
 */
@Repository(value = "classMasterDAO")
@Transactional
public class ClassMasterDAOImpl extends AbstractDAO<ClassMaster> implements ClassMasterDAO{

	// logger
		private static final Logger logger = Logger.getLogger(ClassMasterDAOImpl.class);
	/**
	 * Method to fecth classMaster by classId.
	 * 
	 * @param classId
	 * @return
	 */
	public ClassMaster getClassMasterByClassId(short classId) {
		logger.debug("getClassMasterByClassId() Method start in ClassMasterDAOImpl");
		ClassMaster classMaster = null;
		List<ClassMaster> classMasterList = null;
		final StringBuilder queryString = new StringBuilder();
		try{
			queryString.append("from ClassMaster as classMaster where classMaster.classId = ? ");
			if(logger.isDebugEnabled()){
				logger.debug("classId------------->"+classId);
			}
			classMasterList = (List<ClassMaster>) hibernateTemplate.find(queryString.toString(), classId);
			if(classMasterList != null && !classMasterList.isEmpty() && classMasterList.size() > 0){
				if(logger.isDebugEnabled()){
					logger.debug("classMaster------------->"+classMaster);
				}
				classMaster = classMasterList.get(0);
			}
			
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in ClassMasterDAOImpl.getClassMasterByClassId()----", ex);
			throw ex;
		}
		return classMaster;
	}
	
	/**
	 * Method to fetch classMaster by className.
	 * 
	 * @param className
	 * @return
	 */
	public ClassMaster getClassMasterByClassName(String className) {
		logger.debug("getClassMasterByClassName() Method start in ClassMasterDAOImpl");
		ClassMaster classMaster = null;
		List<ClassMaster> classMasterList = null;
		final StringBuilder queryString = new StringBuilder();
		try{
			queryString.append("from ClassMaster as classMaster where classMaster.className = ? ");
			if(logger.isDebugEnabled()){
				logger.debug("className------------->"+className);
			}
			classMasterList = (List<ClassMaster>) hibernateTemplate.find(queryString.toString(), className);
			if(classMasterList != null && !classMasterList.isEmpty() && classMasterList.size() > 0){
				classMaster = classMasterList.get(0);
				if(logger.isDebugEnabled()){
					logger.debug("classMaster------------->"+classMaster);
				}
			}
			
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in ClassMasterDAOImpl.getClassMasterByClassId()----", ex);
			throw ex;
		}
		return classMaster;
	}

}
