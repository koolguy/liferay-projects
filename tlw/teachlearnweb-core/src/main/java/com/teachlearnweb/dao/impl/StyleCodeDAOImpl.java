package com.teachlearnweb.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.StyleCodeDAO;
import com.teachlearnweb.dao.entity.Skill;
import com.teachlearnweb.dao.entity.StyleCode;

/**
 * Class provides the implementations of AssessmentDAO Interface to perform DML
 * operations.
 * 
 * @author Govinda.
 */
@Repository(value = "styleCodeDAO")
@Transactional
public class StyleCodeDAOImpl extends AbstractDAO<StyleCode> implements StyleCodeDAO {
	// logger
	private static final Logger logger = Logger.getLogger(StyleCodeDAOImpl.class);
	public List<StyleCode> getAllStyleCodes() {
		logger.debug("START");
		return findAll(StyleCode.class);
	}
}
