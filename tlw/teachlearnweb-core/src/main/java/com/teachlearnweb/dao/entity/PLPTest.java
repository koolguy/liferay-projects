package com.teachlearnweb.dao.entity;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tlw_plp_test", catalog = "tlw")
public class PLPTest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2170323686398796887L;
	
	private Integer plpTestId;
	private Integer questionId;
	private String question;
	private String answer;
	private Long userId;
	private Integer answerId;
	
	public PLPTest() {
		
	}

	public PLPTest(Integer plpTestId, Integer questionId, String question,
			String answer, Long userId, Integer answerId) {
		super();
		this.plpTestId = plpTestId;
		this.questionId = questionId;
		this.question = question;
		this.answer = answer;
		this.userId = userId;
		this.answerId = answerId;
	}
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "PLPTestId", unique = true, nullable = false)
	public Integer getPlpTestId() {
		return plpTestId;
	}
	public void setPlpTestId(Integer plpTestId) {
		this.plpTestId = plpTestId;
	}
	
	@Column(name = "QuestionId", nullable = false)
	public Integer getQuestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
		this.questionId = questionId;
	}
	
	@Column(name = "Question", nullable = false)
	public String getQuestion() {
		return question;
	}
	public void setQuestion(String question) {
		this.question = question;
	}
	
	@Column(name = "Answer", nullable = false)
	public String getAnswer() {
		return answer;
	}
	public void setAnswer(String answer) {
		this.answer = answer;
	}
	
	public void setAnswerId(Integer answerId) {
		this.answerId = answerId;
	}

	@Column(name = "UserId", nullable = false)
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
	
	@Column(name = "answerId", nullable = false)
	public Integer getAnswerId() {
		return answerId;
	}

	@Override
	public String toString() {
		return "PLPTest [plpTestId=" + plpTestId + ", questionId=" + questionId
				+ ", question=" + question + ", answer=" + answer + ", userId="
				+ userId + ", answerId=" + answerId + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((answer == null) ? 0 : answer.hashCode());
		result = prime * result
				+ ((answerId == null) ? 0 : answerId.hashCode());
		result = prime * result
				+ ((plpTestId == null) ? 0 : plpTestId.hashCode());
		result = prime * result
				+ ((question == null) ? 0 : question.hashCode());
		result = prime * result
				+ ((questionId == null) ? 0 : questionId.hashCode());
		result = prime * result + ((userId == null) ? 0 : userId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PLPTest other = (PLPTest) obj;
		if (answer == null) {
			if (other.answer != null)
				return false;
		} else if (!answer.equals(other.answer))
			return false;
		if (answerId == null) {
			if (other.answerId != null)
				return false;
		} else if (!answerId.equals(other.answerId))
			return false;
		if (plpTestId == null) {
			if (other.plpTestId != null)
				return false;
		} else if (!plpTestId.equals(other.plpTestId))
			return false;
		if (question == null) {
			if (other.question != null)
				return false;
		} else if (!question.equals(other.question))
			return false;
		if (questionId == null) {
			if (other.questionId != null)
				return false;
		} else if (!questionId.equals(other.questionId))
			return false;
		if (userId == null) {
			if (other.userId != null)
				return false;
		} else if (!userId.equals(other.userId))
			return false;
		return true;
	}

	
	
	
	
	
}
