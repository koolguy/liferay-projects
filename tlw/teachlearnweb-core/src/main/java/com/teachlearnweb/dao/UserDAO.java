
package com.teachlearnweb.dao;

import java.util.List;
import org.springframework.stereotype.Component;
import com.teachlearnweb.dao.entity.TLWUser;
import com.teachlearnweb.dao.entity.UserCategories;

/**
 * Interface EmployeeDAO declares DML operations on Employee table.
 * 
 * @author GalaxE.
 */
@Component
public interface UserDAO extends GenericDAO<TLWUser> {

	/**
	 * Retrieves List of Employees
	 * @return List<CIEmployee>
	 */
	public List<TLWUser> getEmployees();

	/**
	 * Finds Employee by userId.
	 * @param userId
	 * @return CIEmployee
	 */
	public TLWUser findEmployeeByUserId(long userId);
	
	/**
	 * Add TLWUser.
	 * @param ciQuickLink
	 */
	public void addUser(TLWUser ciQuickLink);
	
	public void updateTLWUserProfile(TLWUser tlwUser);

}
