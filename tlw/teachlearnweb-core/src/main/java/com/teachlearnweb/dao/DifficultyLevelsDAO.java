
package com.teachlearnweb.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Component;

import com.teachlearnweb.dao.entity.DifficultyLevel;

/**
 * Interface AssessmentDAO declares DML operations on Assessment table.
 * 
 * @author Govinda.
 */
@Component
public interface DifficultyLevelsDAO extends GenericDAO<DifficultyLevel> {

	public List<DifficultyLevel> getDifficultyLevels() throws HibernateException;

	public DifficultyLevel getDifficultyLevel(Integer levelId);

	public DifficultyLevel getDifficultyLevelByAssessment(final Integer assessmentId) throws HibernateException;

	public List<DifficultyLevel> getDifficultiesBySubject(final short subjectId, final short classId) throws HibernateException;
}

