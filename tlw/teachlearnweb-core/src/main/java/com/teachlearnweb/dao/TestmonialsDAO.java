package com.teachlearnweb.dao;

import java.util.List;

import com.teachlearnweb.dao.entity.Testmonial;

public interface TestmonialsDAO {
	public void postTestmonial(Testmonial testmonial);

	public List<Testmonial> fetchTestmonial();
	
	public List<Testmonial> fetchTestmonialById(long testMonialId);
	
	public int approveTestmonial(long testMonialId);
}
