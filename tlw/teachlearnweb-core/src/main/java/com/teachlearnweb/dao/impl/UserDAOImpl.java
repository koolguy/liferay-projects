
package com.teachlearnweb.dao.impl;

import java.util.List;
import org.springframework.stereotype.Repository;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.UserDAO;
import com.teachlearnweb.dao.entity.TLWUser;
import com.teachlearnweb.dao.entity.UserCategories;
import com.tlw.util.LoggerUtil;

/**
 * Class provides the implementations of EmployeeDAO Interface to perform DML
 * operations.
 * 
 * @author GalaxE.
 */
@Repository(value = "userDao")
public class UserDAOImpl extends AbstractDAO<TLWUser> implements UserDAO {

	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(UserDAOImpl.class);
	
	/**
	 * Retrieves List of Employees
	 * @return List<CIEmployee>
	 */
	public List<TLWUser> getEmployees() {
		LoggerUtil.debugLogger(logger, "getEmployees() Method Starts in EmployeeDAOImpl Class.");
		List<TLWUser> ciEmployee = null;
		ciEmployee = findAll(TLWUser.class);
		LoggerUtil.debugLogger(logger, "getEmployees() Method Ends in EmployeeDAOImpl Class.");
		return ciEmployee;

	}

	/**
	 * Finds Employee by userId.
	 * @param userId
	 * @return CIEmployee
	 */
	public TLWUser findEmployeeByUserId(long userId) {
		LoggerUtil.debugLogger(logger, "findEmployeeByUserId() Method Starts in EmployeeDAOImpl Class.");
		TLWUser ciEmployee = null;
		ciEmployee = findById(TLWUser.class, userId);
		LoggerUtil.debugLogger(logger, "findEmployeeByUserId() Method Ends in EmployeeDAOImpl Class.");
		return ciEmployee;
	}
	
	/**
	 * Add TLWUser.
	 * @param ciQuickLink
	 */
	public void addUser(TLWUser tlwUser) {
		try{
			LoggerUtil.debugLogger(logger, "Start of addQuickLinks() method of Quick Links DAOImpl:: ");
			saveOrUpdate(tlwUser);
			LoggerUtil.debugLogger(logger, "End of addQuickLinks() method of Quick Links DAOImpl:: ");
		}catch (Exception e) {
			LoggerUtil.debugLogger(logger,"Exception raised while adding the tlw_user::" + e.getMessage());
			
		}
	}
	
	
	/**
	 * Update TLWUser.
	 * @param tlwUser
	 */
	public void updateTLWUserProfile(TLWUser tlwUser) {
		update(tlwUser);
	}

	
}
