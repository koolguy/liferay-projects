package com.teachlearnweb.dao.impl;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.UserCategoriesDAO;
import com.teachlearnweb.dao.UserDAO;
import com.teachlearnweb.dao.entity.TLWUser;
import com.teachlearnweb.dao.entity.UserCategories;
import com.tlw.util.LoggerUtil;

/**
 * Class provides the implementations of EmployeeDAO Interface to perform DML
 * operations.
 * 
 * @author GalaxE.
 */
@Repository(value = "userCategoriesDao")
@Transactional
public class UserCategoriesDAOImpl extends AbstractDAO<UserCategories>
		implements UserCategoriesDAO {

	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil
			.getLog(UserCategoriesDAOImpl.class);

	public void addCourse(UserCategories userCategories) {
		LoggerUtil.infoLogger(logger,
				"Start of add userCategories method of Quick Links DAOImpl:: ");
		saveOrUpdate(userCategories);
		LoggerUtil.infoLogger(logger,
				"End of add userCategories method of Quick Links DAOImpl:: ");

	}
/*
	public List<UserCategories> categoriesByUserId(Long userId) {
		LoggerUtil
				.debugLogger(logger,
						"getGeographyCountry() Method starts in LocationDAOImpl Class.");
		StringBuffer geographyCountryQuery = new StringBuffer();
		geographyCountryQuery.append("Select * from user_categories where userId ="
				+ "=?");
		Query query = getSession().createSQLQuery(
				geographyCountryQuery.toString());
		query.setParameter(0, userId);
		List<UserCategories> obj = query.list();
		LoggerUtil.debugLogger(logger,
				"getGeographyCountry() Method Ends in LocationDAOImpl Class.");
		return obj;
	}*/
	
	
	@SuppressWarnings("unchecked")
	public List<UserCategories> categoriesByUserId(long userId) {

		LoggerUtil.infoLogger(logger, "Start of getCategories By UerId method of Uer Categories DAOImpl:: ");
		List<UserCategories> ciQuickLinkList = null;
		final StringBuilder queryString = new StringBuilder();
		try {
			queryString.append("from UserCategories as model where ");
			queryString.append("userId");
			queryString.append("= ?");
			

			ciQuickLinkList = hibernateTemplate.find(queryString.toString(), userId);
			

		}
		catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "QuickLinksDAOImpl.getQuickLinks() HibernateException ::", e);
			throw e;
		}

		LoggerUtil.debugLogger(logger, "End of getQuickLinks() method of Quick Links DAOImpl:: ");
		return ciQuickLinkList;
	}



}
