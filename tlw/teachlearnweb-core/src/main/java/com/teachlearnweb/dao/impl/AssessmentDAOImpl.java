package com.teachlearnweb.dao.impl;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.teachlearnweb.dao.AbstractDAO;
import com.teachlearnweb.dao.AssessmentDAO;
import com.teachlearnweb.dao.entity.DTAssessment;
import com.tlw.assessment.vo.AssessmentConstants;

/**
 * Class provides the implementations of AssessmentDAO Interface to perform DML
 * operations.
 * 
 * @author Govinda.
 */
@Repository(value = "assessmentDAO")
@Transactional
public class AssessmentDAOImpl extends AbstractDAO<DTAssessment> implements AssessmentDAO {
	// logger
	private static final Logger logger = Logger.getLogger(AssessmentDAOImpl.class);
//	@Autowired
//	private SessionFactory sessionFactory;
	
	public List<DTAssessment> getAssessments(final Integer userId, final Short classId,
			final Short subjectId, final Short testId) throws HibernateException {
		logger.debug("START");
		List<DTAssessment> assessments = null;
		final StringBuilder queryString = new StringBuilder();
		try {
//			select a.*, s.subjectname from tlw_dt_assessment as a inner join tlw_subjectmaster as s where a.TestId=10 and a.userid=71 and a.subjectid=s.subjectid
			queryString.append("from DTAssessment as A INNER JOIN fetch A.subjectMaster as S where " +
					"A.subjectMaster.subjectId=S.subjectId and A.userId=" + userId + " and A.DTTest.testId=" + testId);

			if (classId != null) {
				queryString.append(" and A.classMaster.classId=" + classId);
			}
			if (subjectId != null) {
				queryString.append(" and A.subjectMaster.subjectId=" + subjectId);
			}
			if (logger.isDebugEnabled()) {
				logger.debug("userId::" + userId);
				logger.debug("testId::" + testId);
				logger.debug("subjectId::" + subjectId);
				logger.debug("classId::" + classId);
			}
			assessments = (List<DTAssessment>) hibernateTemplate.find(queryString.toString());
			if (assessments != null) {
				if (logger.isDebugEnabled()) {
					logger.debug("assessments size::" + assessments.size());
				}
			}
		} catch (HibernateException e) {
			logger.error("Hibernate exception occured while fetching assessment details.", e);
			throw e;
		}
		logger.debug("END");
		return assessments;
	}

	public DTAssessment getAssessment(Integer assessmentId) {
		return findById(DTAssessment.class, assessmentId);
	}
	
	/**
	 * Method to save DTAssessmentsList.
	 * 
	 * @param dtAssessmentList
	 */
	public void saveDTAssessmentList(List<DTAssessment> dtAssessmentList) {
		logger.debug("AssessmentDAOImpl.saveDTAssessmentList() Method Start");
		if(dtAssessmentList != null && !dtAssessmentList.isEmpty() && dtAssessmentList.size() > 0){
			for(DTAssessment dtAssessment : dtAssessmentList) {
				saveAssessment(dtAssessment);
			}
		}
		
	}
	
	/**
	 * Method to save DTAssessments.
	 * 
	 * @param assessment
	 */
	public void saveAssessment(DTAssessment assessment){
		save(assessment);
	}

	
	/**
	 * Method to fetch assessments by userId.
	 * 
	 * @param userId
	 * @return 
	 */
	public List<DTAssessment> getAssessments(Long userId) {
		logger.debug("getAssessments() Method Start in AssessmentDAOImpl");
		List<DTAssessment> assessmentsList = null;
		final StringBuilder queryString = new StringBuilder();
		try{
			queryString.append("from DTAssessment assessment where assessment.userId = ?");
			if(logger.isDebugEnabled()){
				logger.debug("userId------------->"+userId);
			}
			assessmentsList = (List<DTAssessment>) hibernateTemplate.find(queryString.toString(), userId);
			if(assessmentsList != null && !assessmentsList.isEmpty() && assessmentsList.size() > 0){
				if(logger.isDebugEnabled()){
					logger.debug("assessmentsList.size()------------->"+assessmentsList.size());
				}
			}
			
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in PLPStudentWorkSheetDAOImpl.getPLPStudentWorkSheetsByUserId()----", ex);
			throw ex;
		}
		return assessmentsList;
		
	}

	//@Override
	public int getTestsFinishedByStatusAndUserId(long userId, String status) {
		// TODO Auto-generated method stub
	
		logger.debug("getTestsFinishedByStatusAndUserId(userId, status) Method Start");
		List<DTAssessment> dtTestList = null;
		int count = 0;
		final StringBuilder queryString = new StringBuilder();
		try{
			queryString.append("from DTAssessment ws where ws.userId = ? and ws.status = ? and ws.DTTest.testId= ?");
			if(logger.isDebugEnabled()){
				logger.debug("userId------------->"+userId);
				logger.debug("status------------->"+status);  
			}
			
			dtTestList = (List<DTAssessment>) hibernateTemplate.find(queryString.toString(), convertLongToInt(userId), status, AssessmentConstants.TEST_TYPE_SUBJECT_TEST);
			if(dtTestList != null && !dtTestList.isEmpty() && dtTestList.size() > 0){
				if(logger.isDebugEnabled()){
					logger.debug("getTestsFinishedByStatusAndUserId()------------->"+dtTestList.size());
				}
				count = dtTestList.size();
			}
			
			logger.debug("getTestsFinishedByStatusAndUserId count:"+count);
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in getTestsFinishedByStatusAndUserId----", ex);
			throw ex;
		}
		return count;

	}
	
	/**
	 * Method to convert long to int.
	 * @param l
	 * @return int
	 */
	public int convertLongToInt(long l) {
	    if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
	        throw new IllegalArgumentException
	            (l + " cannot be cast to int without changing its value.");
	    }
	    return (int) l;
	}

	/**
	 * Method to fetch all completed dt assessments by subject tests.
	 * 
	 * @param testId
	 * @param status
	 * @return
	 */
	public List<DTAssessment> fetchAllCompletedDTAssessmentsBySubjectTest(Short testId, String status) {
		logger.debug("AssessmentDAOImpl.fetchAllCompletedDTAssessmentsBySubjectTest() Method Start");
		List<DTAssessment> dtTestList = null;
		final StringBuilder queryString = new StringBuilder();
		try{
			queryString.append("from DTAssessment ws where ws.DTTest.testId = ? and ws.status = ?");
			if(logger.isDebugEnabled()){
				logger.debug("userId------------->"+testId);
				logger.debug("status------------->"+status);  
			}
			
			dtTestList = (List<DTAssessment>) hibernateTemplate.find(queryString.toString(), testId, status);
			if(dtTestList != null && !dtTestList.isEmpty() && dtTestList.size() > 0){
				if(logger.isDebugEnabled()){
					logger.debug("getTestsFinishedByStatusAndUserId()------------->"+dtTestList.size());
				}
			}
			
		}catch(HibernateException ex) {
			logger.error("Hibernate Exception Caught in getTestsFinishedByStatusAndUserId----", ex);
			throw ex;
		}

		return dtTestList;
	}

}
