
package com.teachlearnweb.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Component;

import com.teachlearnweb.dao.entity.SubjectMaster;

/**
 * Interface AssessmentDAO declares DML operations on Assessment table.
 * 
 * @author Govinda.
 */
@Component
public interface SubjectsDAO extends GenericDAO<SubjectMaster> {

	public List<SubjectMaster> getSubjects() throws HibernateException;

	public SubjectMaster getSubject(Short subjectId);
	
	/**
	 * Method to fetch subjectMaster by subjectName.
	 * 
	 * @param subjectName
	 * @return
	 */
	public SubjectMaster getSubjectMasterbySbujectName(String subjectName);

}

