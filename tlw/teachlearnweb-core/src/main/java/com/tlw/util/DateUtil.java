package com.tlw.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {
	
	//
	public static String getTimeInstance(Date now){
		return DateFormat.getTimeInstance(DateFormat.SHORT).format(now);
	}
	
	public static String getDateInstance(Date now){
		return DateFormat.getDateInstance(DateFormat.MEDIUM).format(now);
	}
	
	//Converting Month String To Integer
	public static int formatDate(String month) throws ParseException{
		  Date date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(month);
	      Calendar cal = Calendar.getInstance();
	      cal.setTime(date);
	      int month1 = cal.get(Calendar.MONTH);
	      return month1;
	}
	    

	/**
	 * @param dob
	 * @param dateFormat
	 * @return
	 */
	public static Calendar convertStringToCalendar(String dob, String dateFormat) {
		Calendar cal = null;

        if (dob != null) {
            try {
                DateFormat formatter = new SimpleDateFormat(dateFormat);
                Date date = formatter.parse(dob);
                cal = Calendar.getInstance();
                cal.setTime(date);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return cal;
   
	}
}
