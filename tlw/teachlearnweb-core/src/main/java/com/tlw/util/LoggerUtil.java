
package com.tlw.util;

import com.liferay.portal.kernel.log.Log;

/**
 * Utility Class for checking if the respective logger is enabled.
 * 
 * @author GalaxE.
 */

public class LoggerUtil {

	/**
	 * Method to check if Error log level is enabled.
	 * @param logger
	 * @param message
	 * @param t
	 */
	public static void errorLogger(Log logger, String message, Throwable t) {

		if (logger.isErrorEnabled()) {
			logger.error(message+" ---> "+t.getMessage());
			//logger.error(message, t);
		}
	}

	/**
	 * Method to check if Debug log level is enabled.
	 * @param logger
	 * @param message
	 */
	public static void debugLogger(Log logger, String message) {

		if (logger.isDebugEnabled()) {
			logger.debug(message);
		}
	}

	/**
	 * Method to check if Info log level is enabled.
	 * @param logger
	 * @param message
	 */
	public static void infoLogger(Log logger, String message) {

		if (logger.isInfoEnabled()) {
			logger.info(message);
		}
	}

	/**
	 * Method to check if Trace log level is enabled.
	 * @param logger
	 * @param message
	 */
	public static void traceLogger(Log logger, String message) {

		if (logger.isTraceEnabled()) {
			logger.trace(message);
		}
	}
}
