package com.tlw.assessment.vo;

import java.io.Serializable;

public class InlineTest implements Serializable {
	private static final long serialVersionUID = 1L;
	private Short levelId;
	private String levelDesc;
	private Integer questionsCount;
	private Integer timeInMinutes;
	private Integer rewardPoints;
	/**
	 * @return the levelId
	 */
	public Short getLevelId() {
		return levelId;
	}
	/**
	 * @param levelId the levelId to set
	 */
	public void setLevelId(Short levelId) {
		this.levelId = levelId;
	}
	/**
	 * @return the levelDesc
	 */
	public String getLevelDesc() {
		return levelDesc;
	}
	/**
	 * @param levelDesc the levelDesc to set
	 */
	public void setLevelDesc(String levelDesc) {
		this.levelDesc = levelDesc;
	}
	/**
	 * @return the questionsCount
	 */
	public Integer getQuestionsCount() {
		return questionsCount;
	}
	/**
	 * @param questionsCount the questionsCount to set
	 */
	public void setQuestionsCount(Integer questionsCount) {
		this.questionsCount = questionsCount;
	}
	/**
	 * @return the timeInMinutes
	 */
	public Integer getTimeInMinutes() {
		return timeInMinutes;
	}
	/**
	 * @param timeInMinutes the timeInMinutes to set
	 */
	public void setTimeInMinutes(Integer timeInMinutes) {
		this.timeInMinutes = timeInMinutes;
	}
	/**
	 * @return the rewardPoints
	 */
	public Integer getRewardPoints() {
		return rewardPoints;
	}
	/**
	 * @param rewardPoints the rewardPoints to set
	 */
	public void setRewardPoints(Integer rewardPoints) {
		this.rewardPoints = rewardPoints;
	}
}
