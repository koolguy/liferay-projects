package com.tlw.assessment.util;

import java.beans.PropertyEditorSupport;

/**
 * Property editor for Integer type properties.
 * 
 * @author Govinda
 */
public class IntegerEditor extends PropertyEditorSupport {
	@Override
	public String getAsText() {
		String returnVal = "";
		if(getValue() instanceof Integer) {
			returnVal = String.valueOf((Integer)getValue());
		}
		if(getValue() != null && getValue() instanceof String[]) {
			returnVal = ((String[]) getValue())[0];
		}
		return returnVal;
	}

	@Override
	public void setAsText(String text) throws IllegalArgumentException {
		setValue(Integer.valueOf(text));
	}
}
