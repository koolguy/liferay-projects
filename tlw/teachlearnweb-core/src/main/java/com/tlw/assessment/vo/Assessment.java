package com.tlw.assessment.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.teachlearnweb.dao.entity.DTAnswer;
import com.teachlearnweb.dao.entity.DTAssessment;
import com.teachlearnweb.dao.entity.DTAssessmentDetails;

/**
 * Represents one question.
 * 
 * @author govinda
 */
public class Assessment implements Serializable {
	private static final long serialVersionUID = 1L;
	private int assessmentId;
	private Date assessmentDate;
	private DTAssessment dtAssessment;
	private List<DTAssessmentDetails> questions;
	private Map<Integer, List<DTAnswer>> options;
	private Long timeAllotted;
	private String status;
	private String subjectName;

	public Assessment(int assessmentId) {
		this.assessmentId = assessmentId;
		this.options = new HashMap<Integer, List<DTAnswer>>();
	}

	/**
	 * @return the subjectName
	 */
	public String getSubjectName() {
		return subjectName;
	}
	/**
	 * @param subjectName the subjectName to set
	 */
	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}
	/**
	 * @return the dtAssessment
	 */
	public DTAssessment getDtAssessment() {
		return dtAssessment;
	}
	/**
	 * @param dtAssessment the dtAssessment to set
	 */
	public void setDtAssessment(DTAssessment dtAssessment) {
		this.dtAssessment = dtAssessment;
	}
	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * @return the assessmentId
	 */
	public int getAssessmentId() {
		return assessmentId;
	}
	/**
	 * @param assessmentId the assessmentId to set
	 */
	public void setAssessmentId(int assessmentId) {
		this.assessmentId = assessmentId;
	}
	/**
	 * @return the assessmentDate
	 */
	public Date getAssessmentDate() {
		return assessmentDate;
	}
	/**
	 * @param assessmentDate the assessmentDate to set
	 */
	public void setAssessmentDate(Date assessmentDate) {
		this.assessmentDate = assessmentDate;
	}
	/**
	 * @return the questions
	 */
	public List<DTAssessmentDetails> getQuestions() {
		return questions;
	}
	/**
	 * @param questions the questions to set
	 */
	public void setQuestions(List<DTAssessmentDetails> questions) {
		this.questions = questions;
	}
	/**
	 * @return the answers
	 */
	public Map<Integer, List<DTAnswer>> getOptions() {
		return options;
	}
	/**
	 * @param answers the answers to set
	 */
	public void setOptions(Map<Integer, List<DTAnswer>> options) {
		this.options = options;
	}
//	public List<DTAnswer> getOptions(Integer assessmentId) {
//		List<DTAnswer> qOptions = this.options.get(assessmentId);
//		if (qOptions == null) {
//			qOptions = new ArrayList<DTAnswer>();
//			this.options.put(assessmentId, qOptions);
//		}
//		return qOptions;
//	}
	/**
	 * @return the timeAllotted
	 */
	public Long getTimeAllotted() {
		return timeAllotted;
	}
	/**
	 * @param timeAllotted the timeAllotted to set
	 */
	public void setTimeAllotted(Long timeAllotted) {
		this.timeAllotted = timeAllotted;
	}
	public int getQuestionCount() {
		if (this.questions != null) {
			return this.questions.size();
		}
		return 0;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Assessment) {
			Assessment otherA = (Assessment) obj;
			if (this.assessmentId == otherA.assessmentId) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("\nID: " + this.assessmentId);
		buffer.append("\nDate: " + this.assessmentDate);
		buffer.append("\ntime alloted: " + this.timeAllotted);
		buffer.append("\nquestions count: " + this.getQuestionCount());
		return buffer.toString();
	}
}
