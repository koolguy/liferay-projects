package com.tlw.assessment.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.teachlearnweb.dao.entity.DTAnswer;
import com.teachlearnweb.dao.entity.DTAssessmentDetails;
import com.teachlearnweb.dao.entity.DifficultyLevel;
import com.tlw.assessment.vo.InlineTest;
import com.tlw.assessment.vo.Question;

public final class AssessmentUtil {
	public static Question populateQuestion(DTAssessmentDetails assessmentDetails) {
		final int questionId = assessmentDetails.getDTQuestion().getQuestionId();
		final Question question = new Question(questionId);
		question.setQuestion(assessmentDetails.getDTQuestion().getQuestion());
		question.setActualAnswer(assessmentDetails.getDTAnswer().getAnswerId());

		if (assessmentDetails.getDTQuestion().getMarksAllotted() != null) {
			question.setMarksAllotted(assessmentDetails.getDTQuestion().getMarksAllotted().floatValue());
		}
		if (assessmentDetails.getMarksScored() != null) {
			question.setMarksScored(assessmentDetails.getMarksScored().floatValue());
		}
		if (assessmentDetails.getAnswer() != null) {
			question.setAnswer(Integer.parseInt(assessmentDetails.getAnswer()));
		}
		return question;
	}

	public static Map<Integer, String> populateAnswers(final List<DTAnswer> answers) {
		final Map<Integer, String> options = new HashMap<Integer, String>();
		for (DTAnswer dtAnswer : answers) {
			options.put(dtAnswer.getAnswerId(), dtAnswer.getAnswer());
		}
		return options;
	}

	public static List<InlineTest> populateInlineTests(final List<DifficultyLevel> levels) {
		List<InlineTest> tests = null;
		if(levels != null) {
			tests = new ArrayList<InlineTest>();
			InlineTest test = null;
			for (DifficultyLevel level : levels) {
				test = new InlineTest();
				test.setLevelId(level.getLevelId());
				test.setLevelDesc(level.getDescription());
				if (level.getLevelId() == 1) {
					test.setQuestionsCount(5);
					test.setRewardPoints(5);
					test.setTimeInMinutes(5);
				} else if (level.getLevelId() == 2) {
					test.setQuestionsCount(10);
					test.setRewardPoints(10);
					test.setTimeInMinutes(10);
				} else if (level.getLevelId() == 3) {
					test.setQuestionsCount(20);
					test.setRewardPoints(20);
					test.setTimeInMinutes(15);
				}
				tests.add(test);
			}
			// add random level also
			test = new InlineTest();
			test.setLevelId(Short.valueOf("-1"));
			test.setLevelDesc("Random");
			test.setQuestionsCount(30);
			test.setTimeInMinutes(30);
			test.setRewardPoints(25);
			tests.add(test);
		}
		return tests;
	}
//
//	public static SubjectTest populateSubjectTest(final DTAssessment assessment) {
//		List<InlineTest> tests = null;
//		if(levels != null) {
//			tests = new ArrayList<InlineTest>();
//			InlineTest test = null;
//			for (DifficultyLevel level : levels) {
//				test = new InlineTest();
//				test.setLevelId(level.getLevelId());
//				test.setLevelDesc(level.getDescription());
//				if (level.getLevelId() == 1) {
//					test.setQuestionsCount(5);
//					test.setRewardPoints(5);
//					test.setTimeInMinutes(5);
//				} else if (level.getLevelId() == 2) {
//					test.setQuestionsCount(10);
//					test.setRewardPoints(10);
//					test.setTimeInMinutes(10);
//				} else if (level.getLevelId() == 3) {
//					test.setQuestionsCount(20);
//					test.setRewardPoints(20);
//					test.setTimeInMinutes(15);
//				}
//				tests.add(test);
//			}
//			// add random level also
//			test = new InlineTest();
//			test.setLevelId(Short.valueOf("-1"));
//			test.setLevelDesc("Random");
//			test.setQuestionsCount(30);
//			test.setTimeInMinutes(30);
//			test.setRewardPoints(25);
//			tests.add(test);
//		}
//		return tests;
//	}
}
