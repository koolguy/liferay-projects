package com.tlw.assessment.vo;

public final class AssessmentConstants {
	public static final Short QUESTIONS_PER_SKILL = 5;
	public static final Short TEST_TYPE_DIAGNOSTIC_TEST = 1;
	public static final Short TEST_TYPE_DIAGNOSTIC_TEST_STYLE_BASED = 2;
	public static final Short TEST_TYPE_SUBJECT_TEST = 3;
	public static final Short TEST_TYPE_EOC_TEST = 4;

	public static final Short DTTEST_PERCENTAGE = 50;
	public static final Short SUBJECT_STYLE_BASED = 0;
	public static final Short MINUTE_PER_QUESTION = 60;
	public static final String QUESTION_MARK_SYMBOL = "?";
	public static final Short CONSTATN_ZERO = 0;

	public static final class TestStatus {
		public static final String ASSIGNED = "Assigned";
		public static final String COMPLETED = "Completed";
		public static final String SAVED = "Saved";
	}
	public static final class QuestionType {
		public static final Short STYLE_BASED = 0;
	}
}
