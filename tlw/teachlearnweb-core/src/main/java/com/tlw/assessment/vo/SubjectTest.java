package com.tlw.assessment.vo;

import java.io.Serializable;

import com.teachlearnweb.dao.entity.DTAssessment;
import com.teachlearnweb.dao.entity.DifficultyLevel;
import com.teachlearnweb.dao.entity.SubjectMaster;

public class SubjectTest implements Serializable {
	private static final long serialVersionUID = 1L;
	private DifficultyLevel difficultyLevel;
	private DTAssessment assessment;
	private SubjectMaster selectedSubject;

	/**
	 * @return the selectedSubject
	 */
	public SubjectMaster getSelectedSubject() {
		return selectedSubject;
	}
	/**
	 * @param selectedSubject the selectedSubject to set
	 */
	public void setSelectedSubject(SubjectMaster selectedSubject) {
		this.selectedSubject = selectedSubject;
	}
	/**
	 * @return the difficultyLevel
	 */
	public DifficultyLevel getDifficultyLevel() {
		return difficultyLevel;
	}
	/**
	 * @param difficultyLevel the difficultyLevel to set
	 */
	public void setDifficultyLevel(DifficultyLevel difficultyLevel) {
		this.difficultyLevel = difficultyLevel;
	}
	/**
	 * @return the assessment
	 */
	public DTAssessment getAssessment() {
		return assessment;
	}
	/**
	 * @param assessment the assessment to set
	 */
	public void setAssessment(DTAssessment assessment) {
		this.assessment = assessment;
	}
}
