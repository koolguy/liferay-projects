package com.tlw.assessment.vo;

import java.io.Serializable;

/**
 * Represents one question.
 * 
 * @author govinda
 */
public class SkillWiseAssessmentReport implements Serializable {
	private static final long serialVersionUID = 1L;
	private Short skillId;
	private String skillName;
	private Integer questionCount;
	private Integer correctCount;
	private Float percentage;

	public SkillWiseAssessmentReport(final Short skillId) {
		this.skillId = skillId;
		this.questionCount = 0;
		this.correctCount = 0;
		percentage = 0.0f;
	}
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("\n ID: " + this.skillId);
		buffer.append("\n questions count: " + this.questionCount);
		buffer.append("\n correct count: " + this.correctCount);
		buffer.append("\n percentage: " + this.percentage);
		return buffer.toString();
	}
	/**
	 * @return the skillName
	 */
	public String getSkillName() {
		return skillName;
	}
	/**
	 * @param skillName the skillName to set
	 */
	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}
	/**
	 * @return the skillId
	 */
	public Short getSkillId() {
		return skillId;
	}
	/**
	 * @param skillId the skillId to set
	 */
	public void setSkillId(Short skillId) {
		this.skillId = skillId;
	}
	/**
	 * @return the questionCount
	 */
	public Integer getQuestionCount() {
		return questionCount;
	}
	/**
	 * @param questionCount the questionCount to set
	 */
	public void setQuestionCount(Integer questionCount) {
		this.questionCount = questionCount;
	}
	/**
	 * @return the correctCount
	 */
	public Integer getCorrectCount() {
		return correctCount;
	}
	/**
	 * @param correctCount the correctCount to set
	 */
	public void setCorrectCount(Integer correctCount) {
		this.correctCount = correctCount;
	}
	/**
	 * @return the percentage
	 */
	public Float getPercentage() {
		return percentage;
	}
	/**
	 * @param percentage the percentage to set
	 */
	public void setPercentage(Float percentage) {
		this.percentage = percentage;
	}
}
