package com.tlw.assessment.vo;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Represents one question.
 * 
 * @author govinda
 */
public class StyleBasedAssessmentReport implements Serializable {
	private static final long serialVersionUID = 1L;
	// <styleIdDesc, percent>
	private Map<String, Integer> stylewiseAnswerCount;
	private Integer questionCount;

	public StyleBasedAssessmentReport() {
		this.questionCount = 0;
		this.stylewiseAnswerCount = new LinkedHashMap<String, Integer>();
	}
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("\n questions count: " + this.questionCount);
		buffer.append("\n percentages: " + this.stylewiseAnswerCount);
		return buffer.toString();
	}
	/**
	 * @return the stylewiseAnswerCount
	 */
	public Map<String, Integer> getStylewiseAnswerCount() {
		return stylewiseAnswerCount;
	}
	/**
	 * @param stylewiseAnswerCount the stylewiseAnswerCount to set
	 */
	public void setStylewiseAnswerCount(Map<String, Integer> stylewiseAnswerCount) {
		this.stylewiseAnswerCount = stylewiseAnswerCount;
	}
	/**
	 * @return the questionCount
	 */
	public Integer getQuestionCount() {
		return questionCount;
	}
	/**
	 * @param questionCount the questionCount to set
	 */
	public void setQuestionCount(Integer questionCount) {
		this.questionCount = questionCount;
	}
}
