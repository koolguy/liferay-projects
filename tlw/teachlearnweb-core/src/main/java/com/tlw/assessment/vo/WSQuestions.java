package com.tlw.assessment.vo;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.teachlearnweb.dao.entity.PLPAnswer;
import com.teachlearnweb.dao.entity.PLPQuestion;

public class WSQuestions implements  Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8930153982005446749L;
	
	private Integer workSheetId;
	private Date assessmentDate;
	private List<PLPQuestion> questions;
	private Map<Integer, List<PLPAnswer>> options;
	private Integer correctAnswerId;
	private Long timeAllotted;
	private Integer userSelectedAnswerId;
	
	private int currentQuestionNum = 1;
    private int answeredCount = 0;
    private long timeRemaining = 0;
    private List<PLPQuestion> plpQuestionsList = null;
    private List<PLPAnswer> plpAnswersList = null;
    private long userId;
    
    private String subjectName;
    private String topicName;
    private String subTopicName;
    
    private PLPQuestion currentQuestion;
   
    public WSQuestions(Integer workSheetId, Date assessmentDate,
			List<PLPQuestion> questions, Map<Integer, List<PLPAnswer>> options,
			Integer correctAnswerId, Long timeAllotted,
			Integer userSelectedAnswerId, int currentQuestionNum,
			int answeredCount, long timeRemaining,
			List<PLPQuestion> plpQuestionsList, List<PLPAnswer> plpAnswersList, long userId, String subjectName, String topicname, String subTopicName,PLPQuestion currentQuestion) {
		super();
		this.userId = userId;
		this.workSheetId = workSheetId;
		this.assessmentDate = assessmentDate;
		this.questions = questions;
		this.options = options;
		this.correctAnswerId = correctAnswerId;
		this.timeAllotted = timeAllotted;
		this.userSelectedAnswerId = userSelectedAnswerId;
		this.currentQuestionNum = currentQuestionNum;
		this.answeredCount = answeredCount;
		this.timeRemaining = timeRemaining;
		this.plpQuestionsList = plpQuestionsList;
		this.plpAnswersList = plpAnswersList;
		this.subjectName = subjectName;
		this.subTopicName = subTopicName;
		this.topicName = topicName;
		this.currentQuestion = currentQuestion;
	}

    
	public Integer getUserSelectedAnswerId() {
		return userSelectedAnswerId;
	}

	public void setUserSelectedAnswerId(Integer userSelectedAnswerId) {
		this.userSelectedAnswerId = userSelectedAnswerId;
	}

	public Integer getWorkSheetId() {
		return workSheetId;
	}

	public void setWorkSheetId(Integer workSheetId) {
		this.workSheetId = workSheetId;
	}

	public Integer getCorrectAnswerId() {
		return correctAnswerId;
	}

	public void setCorrectAnswerId(Integer correctAnswerId) {
		this.correctAnswerId = correctAnswerId;
	}

	

	public WSQuestions() {
		this.options = new HashMap<Integer, List<PLPAnswer>>();
	}
	
	/**
	 * @return the assessmentDate
	 */
	public Date getAssessmentDate() {
		return assessmentDate;
	}
	/**
	 * @param assessmentDate the assessmentDate to set
	 */
	public void setAssessmentDate(Date assessmentDate) {
		this.assessmentDate = assessmentDate;
	}
	/**
	 * @return the questions
	 */
	public List<PLPQuestion> getQuestions() {
		return questions;
	}
	/**
	 * @param questions the questions to set
	 */
	public void setQuestions(List<PLPQuestion> questions) {
		this.questions = questions;
	}
	/**
	 * @return the answers
	 */
	public Map<Integer, List<PLPAnswer>> getOptions() {
		return options;
	}
	/**
	 * @param answers the answers to set
	 */
	public void setOptions(Map<Integer, List<PLPAnswer>> options) {
		this.options = options;
	}

	/**
	 * @return the timeAllotted
	 */
	public Long getTimeAllotted() {
		return timeAllotted;
	}
	/**
	 * @param timeAllotted the timeAllotted to set
	 */
	public void setTimeAllotted(Long timeAllotted) {
		this.timeAllotted = timeAllotted;
	}
	public int getQuestionCount() {
		if (this.questions != null) {
			return this.questions.size();
		}
		return 0;
	}
	public int getCurrentQuestionNum() {
		return currentQuestionNum;
	}

	public void setCurrentQuestionNum(int currentQuestionNum) {
		this.currentQuestionNum = currentQuestionNum;
	}

	public int getAnsweredCount() {
		return answeredCount;
	}

	public void setAnsweredCount(int answeredCount) {
		this.answeredCount = answeredCount;
	}

	public long getTimeRemaining() {
		return timeRemaining;
	}

	public void setTimeRemaining(long timeRemaining) {
		this.timeRemaining = timeRemaining;
	}

	public List<PLPQuestion> getPlpQuestionsList() {
		return plpQuestionsList;
	}

	public void setPlpQuestionsList(List<PLPQuestion> plpQuestionsList) {
		this.plpQuestionsList = plpQuestionsList;
	}

	public List<PLPAnswer> getPlpAnswersList() {
		return plpAnswersList;
	}

	public void setPlpAnswersList(List<PLPAnswer> plpAnswersList) {
		this.plpAnswersList = plpAnswersList;
	}

	public long getUserId() {
		return userId;
	}


	public void setUserId(long userId) {
		this.userId = userId;
	}


	public String getSubjectName() {
		return subjectName;
	}


	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}


	public String getTopicName() {
		return topicName;
	}


	public void setTopicName(String topicName) {
		this.topicName = topicName;
	}


	public String getSubTopicName() {
		return subTopicName;
	}


	public void setSubTopicName(String subTopicName) {
		this.subTopicName = subTopicName;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + answeredCount;
		result = prime * result
				+ ((assessmentDate == null) ? 0 : assessmentDate.hashCode());
		result = prime * result
				+ ((correctAnswerId == null) ? 0 : correctAnswerId.hashCode());
		result = prime * result + currentQuestionNum;
		result = prime * result + ((options == null) ? 0 : options.hashCode());
		result = prime * result
				+ ((plpAnswersList == null) ? 0 : plpAnswersList.hashCode());
		result = prime
				* result
				+ ((plpQuestionsList == null) ? 0 : plpQuestionsList.hashCode());
		result = prime * result
				+ ((questions == null) ? 0 : questions.hashCode());
		result = prime * result
				+ ((timeAllotted == null) ? 0 : timeAllotted.hashCode());
		result = prime * result
				+ (int) (timeRemaining ^ (timeRemaining >>> 32));
		result = prime * result + (int) (userId ^ (userId >>> 32));
		result = prime
				* result
				+ ((userSelectedAnswerId == null) ? 0 : userSelectedAnswerId
						.hashCode());
		result = prime * result
				+ ((workSheetId == null) ? 0 : workSheetId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WSQuestions other = (WSQuestions) obj;
		if (answeredCount != other.answeredCount)
			return false;
		if (assessmentDate == null) {
			if (other.assessmentDate != null)
				return false;
		} else if (!assessmentDate.equals(other.assessmentDate))
			return false;
		if (correctAnswerId == null) {
			if (other.correctAnswerId != null)
				return false;
		} else if (!correctAnswerId.equals(other.correctAnswerId))
			return false;
		if (currentQuestionNum != other.currentQuestionNum)
			return false;
		if (options == null) {
			if (other.options != null)
				return false;
		} else if (!options.equals(other.options))
			return false;
		if (plpAnswersList == null) {
			if (other.plpAnswersList != null)
				return false;
		} else if (!plpAnswersList.equals(other.plpAnswersList))
			return false;
		if (plpQuestionsList == null) {
			if (other.plpQuestionsList != null)
				return false;
		} else if (!plpQuestionsList.equals(other.plpQuestionsList))
			return false;
		if (questions == null) {
			if (other.questions != null)
				return false;
		} else if (!questions.equals(other.questions))
			return false;
		if (timeAllotted == null) {
			if (other.timeAllotted != null)
				return false;
		} else if (!timeAllotted.equals(other.timeAllotted))
			return false;
		if (timeRemaining != other.timeRemaining)
			return false;
		if (userId != other.userId)
			return false;
		if (userSelectedAnswerId == null) {
			if (other.userSelectedAnswerId != null)
				return false;
		} else if (!userSelectedAnswerId.equals(other.userSelectedAnswerId))
			return false;
		if (workSheetId == null) {
			if (other.workSheetId != null)
				return false;
		} else if (!workSheetId.equals(other.workSheetId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "WSQuestions [workSheetId=" + workSheetId + ", assessmentDate="
				+ assessmentDate + ", questions=" + questions + ", options="
				+ options + ", correctAnswerId=" + correctAnswerId
				+ ", timeAllotted=" + timeAllotted + ", userSelectedAnswerId="
				+ userSelectedAnswerId + ", currentQuestionNum="
				+ currentQuestionNum + ", answeredCount=" + answeredCount
				+ ", timeRemaining=" + timeRemaining + ", plpQuestionsList="
				+ plpQuestionsList + ", plpAnswersList=" + plpAnswersList
				+ ", userId=" + userId + "]";
	}

	public PLPQuestion getCurrentQuestion() {
		return currentQuestion;
	}

	public void setCurrentQuestion(PLPQuestion currentQuestion) {
		this.currentQuestion = currentQuestion;
	}
}
