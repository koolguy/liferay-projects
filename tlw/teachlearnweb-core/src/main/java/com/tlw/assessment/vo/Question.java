package com.tlw.assessment.vo;

import java.io.Serializable;
import java.util.Map;

/**
 * Represents one question.
 * 
 * @author govinda
 */
public class Question implements Serializable {
	private static final long serialVersionUID = 1L;
	private int questionId;
	private String question;
	private Map<Integer, String> options;
	private Integer actualAnswer;
	private Integer answer;
	private Float marksAllotted;
	private Float marksScored;

	public Question(int questionId) {
		this.questionId = questionId;
	}

	/**
	 * @return the marksAllotted
	 */
	public Float getMarksAllotted() {
		return marksAllotted;
	}

	/**
	 * @param marksAllotted the marksAllotted to set
	 */
	public void setMarksAllotted(Float marksAllotted) {
		this.marksAllotted = marksAllotted;
	}

	/**
	 * @return the marksScored
	 */
	public Float getMarksScored() {
		return marksScored;
	}

	/**
	 * @param marksScored the marksScored to set
	 */
	public void setMarksScored(Float marksScored) {
		this.marksScored = marksScored;
	}

	/**
	 * @return the actualAnswer
	 */
	public Integer getActualAnswer() {
		return actualAnswer;
	}

	/**
	 * @param actualAnswer the actualAnswer to set
	 */
	public void setActualAnswer(Integer actualAnswer) {
		this.actualAnswer = actualAnswer;
	}

	/**
	 * @return the questionId
	 */
	public int getQuestionId() {
		return questionId;
	}
	/**
	 * @param questionId the questionId to set
	 */
	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}
	/**
	 * @return the question
	 */
	public String getQuestion() {
		return question;
	}
	/**
	 * @param question the question to set
	 */
	public void setQuestion(String question) {
		this.question = question;
	}
	/**
	 * @return the options
	 */
	public Map<Integer, String> getOptions() {
		return options;
	}
	/**
	 * @param options the options to set
	 */
	public void setOptions(Map<Integer, String> options) {
		this.options = options;
	}
	/**
	 * @return the answer
	 */
	public Integer getAnswer() {
		return answer;
	}
	/**
	 * @param answer the answer to set
	 */
	public void setAnswer(Integer answer) {
		this.answer = answer;
	}
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Question) {
			Question otherQ = (Question) obj;
			if (this.questionId == otherQ.questionId) {
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}
	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("\nID: " + this.questionId);
		buffer.append("\nquestion: " + this.question);
		buffer.append("\noptions: " + options.values());
		buffer.append("\nmarks allotted: " + this.marksAllotted);
		return buffer.toString();
	}
}
