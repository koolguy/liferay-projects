/**
 * Copyright (c) 2010 portletfaces.org All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tlw.service.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Contact;
import com.liferay.portal.service.ContactLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;



/**
 * Service API for adding/updating registrants.
 * 
 * @author chandu
 */
public class MyProfileServiceUtil {
	/** The LOG. */
	private static final Log logger = LogFactory.getLog(MyProfileServiceUtil.class);

	public static com.liferay.portal.model.User updateUserProfile(com.liferay.portal.model.User liferayUser) throws PortalException, SystemException {
		
		liferayUser = UserLocalServiceUtil.updateUser(liferayUser);
		return liferayUser;
	}
	
	public static void updateContact(Contact contact) throws PortalException, SystemException {
		ContactLocalServiceUtil.updateContact(contact);
	}

}
