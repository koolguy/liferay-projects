/**
 * Copyright (c) 2010 portletfaces.org All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tlw.service.util;

import java.text.ParseException;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.ListType;
import com.liferay.portal.service.ListTypeServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.teachlearnweb.service.vo.User;
import com.tlw.util.DateUtil;



/**
 * Service API for adding/updating registrants.
 * 
 * @author  vmukku
 */
public class RegistrantionServiceUtil {
	/** The LOG. */
	private static final Log logger = LogFactory.getLog(RegistrantionServiceUtil.class);

	public static final String CONTACT_CLASS_NAME = "com.liferay.portal.model.Contact";
	public static final String PHONE_CLASS_NAME = "com.liferay.portal.model.Contact.phone";

	public static User add(long creatorUserId, long companyId, User registrant, long scopeGroupId
		) throws Exception, PortalException, SystemException, ParseException {
		boolean autoPassword = true;
		String password1 = StringPool.BLANK;
		String password2 = StringPool.BLANK;
		String screenName = null;
		boolean autoScreenName = registrant.isAutoScreenName();
		boolean sendEmail = registrant.isSendEmail();

		if (autoScreenName) {
			screenName = StringPool.BLANK;
		}
		else {
			screenName = registrant.getScreenName();
		}

		String emailAddress = registrant.getEmailAddress();
		long facebookId = 0;
		String openId = StringPool.BLANK;
		String firstName = registrant.getFirstName();
		String middleName = registrant.getMiddleName();
		String lastName = registrant.getLastName();
		logger.debug("firstName............."+firstName);
		logger.debug("lastName............."+lastName);
		logger.debug("emailAddress............."+emailAddress);
		System.out.println("firstName............."+firstName);
		System.out.println("lastName............."+lastName);
		System.out.println("emailAddress............."+emailAddress);
		int prefixId = 0;
		int suffixId = 0;
		boolean male = false;
		if(null != registrant.getGender() && "M".equalsIgnoreCase(registrant.getGender())){
			male = true;
		}
		else{
			male = false;
		}
		System.out.println("DOB " + registrant.getDob());
		String[] dob = registrant.getDob().split("/");
		
		int birthdayDay = Integer.parseInt(dob[0]);
		int birthdayMonth = DateUtil.formatDate(dob[1]);
		int birthdayYear = Integer.parseInt(dob[2]);
		
		String jobTitle = StringPool.BLANK;
		long[] groupIds = {scopeGroupId};
		long[] organizationIds = new long[] {};
		long[] roleIds = new long[] {};

		long[] userGroupIds = new long[] {};
		ServiceContext serviceContext = new ServiceContext();
		logger.debug("scopeGroupId in registration Util................."+scopeGroupId);

		// Add the user to the Liferay database (create an account).
		com.liferay.portal.model.User user = UserLocalServiceUtil.addUser(creatorUserId, companyId, autoPassword, password1, password2,
				autoScreenName, screenName, emailAddress, facebookId, openId, registrant.getLocale(), firstName, middleName, lastName,
				prefixId, suffixId, male, birthdayMonth, birthdayDay, birthdayYear, jobTitle, groupIds, organizationIds,
				roleIds, userGroupIds, sendEmail, serviceContext);
		
		//AddressLocalServiceUtil.addAddress(user.getUserId(), Address.class.getName(), PortalUtil.getClassNameId(Address.class.getName()), registrant.getAddress1(), null, null, registrant.getCity(), null, null, null, null, null, null);

		/*
		AddressLocalServiceUtil.addAddress(user.getUserId(), Address.class.getName(),
				PortalUtil.getClassNameId(Address.class.getName()),registrant.getAddress1(), 
				registrant.getAddress2(), null,registrant.getCity(), "",0, Long.valueOf(registrant.getCountry()), 0, true, true);
*/
		registrant.setUserId(user.getUserId());
		registrant.setContactId(user.getContactId());

		return registrant;
	}


	
	private static int getMobilePhoneTypeId() throws SystemException {
		int businessPhoneTypeId = 0;
		List<ListType> phoneTypes = ListTypeServiceUtil.getListTypes(PHONE_CLASS_NAME);

		for (ListType phoneType : phoneTypes) {

			if (phoneType.getName().equals("Mobile")) {
				businessPhoneTypeId = phoneType.getListTypeId();

				break;
			}
		}

		return businessPhoneTypeId;
	}
}
