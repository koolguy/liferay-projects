<%--
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@ include file="/html/portlet/calendar/init.jsp"%>

<%
	String tabs1 = ParamUtil.getString(request, "tabs1", tabs1Default);

	String eventType = ParamUtil.getString(request, "eventType");

	PortletURL portletURL = renderResponse.createRenderURL();

	portletURL.setParameter("struts_action", "/calendar/view");
	portletURL.setParameter("tabs1", tabs1);

	String[] urlArray = PortalUtil.stripURLAnchor(
			portletURL.toString(), "&#");

	String urlWithoutAnchor = urlArray[0];
	String urlAnchor = urlArray[1];
%>
<div id="sitewidth">

	<div id="offcanvasmenu" style="width: 1782px; left: 0px;">
		<!--offcanvas menu topbar-->
		<%@ include file="dashboard.jsp"%>
	</div>

	<div class="scrollsection">
		<section id="maincontent">
		<div class="wraper topbg outerdiv">
			<!-- Right Section -->
			<div class="rightwrap">
				<div class="rSubTitle calendartitle">
					<p class="rstSubject">Calendar</p>
				</div>
			</div>

			<!-- Left Section -->
			<div class="leftwrap">
				<div class="offcantab">
					<a href="#" class="Offcanmenu" style="text-indent: 0px"></a>
					<!--  <span
						class="tlnotification">10</span> -->
				</div>
			</div>

			<!-- Content Section -->
			<div class="contentwrap">
				<div class="infowrap">
					<div class="Maintitle">Tasks</div>
				</div>
			</div>
		</div>

		<!-- Right Section -->
		<div class="rightwrap">
			<div class="rightmenusection calendarrbg">
				<div id="rightmainmenu">
					<div id="calendar"></div>
					<script>
						$('#calendar').datepicker(
								{
									inline : true,
									firstDay : 1,
									showOtherMonths : true,
									dayNamesMin : [ 'Sun', 'Mon', 'Tue', 'Wed',
											'Thu', 'Fri', 'Sat' ]
								});
					</script>
				</div>
			</div>
		</div>

		<!-- Content Section -->

		<div class="contentwrap">
			<div class="infowrap">
				<div class="tlw_cal">
					<aui:form method="post" name="fm">
						<liferay-util:include page="/html/portlet/calendar/tabs1.jsp" />

						<c:choose>
							<c:when test='<%= tabs1.equals("summary") %>'>
								<%@ include file="/html/portlet/calendar/summary.jspf"%>
							</c:when>
							<c:when test='<%= tabs1.equals("day") %>'>
								<%@ include file="/html/portlet/calendar/day.jspf"%>
							</c:when>
							<c:when test='<%= tabs1.equals("week") %>'>
								<%@ include file="/html/portlet/calendar/week.jspf"%>
							</c:when>
							<c:when test='<%= tabs1.equals("month") %>'>
								<%@ include file="/html/portlet/calendar/month.jspf"%>
							</c:when>
							<c:when test='<%= tabs1.equals("year") %>'>
								<%@ include file="/html/portlet/calendar/year.jspf"%>
							</c:when>
							<c:when test='<%= tabs1.equals("events") %>'>
								<%@ include file="/html/portlet/calendar/events.jspf"%>
							</c:when>
							<c:when test='<%= tabs1.equals("export-import") %>'>
								<%@ include file="/html/portlet/calendar/export_import.jspf"%>
							</c:when>
						</c:choose>
					</aui:form>

					<%
						if (!tabs1.equals("summary")) {
							PortalUtil.addPortletBreadcrumbEntry(request,
									LanguageUtil.get(pageContext, tabs1), currentURL);
						}
					%>

				</div>
			</div>
		</div>
		</section>
	</div>
</div>


