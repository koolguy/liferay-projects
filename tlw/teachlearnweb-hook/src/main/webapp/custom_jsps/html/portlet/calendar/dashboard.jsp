<%@page import="java.util.StringTokenizer"%>
<%@page import="com.liferay.portal.model.Layout"%>
<%@page import="com.liferay.portal.service.LayoutLocalServiceUtil"%>
<%@page
	import="java.util.Date,javax.portlet.PortletURL,com.liferay.portlet.PortletURLFactoryUtil,javax.portlet.PortletRequest"%>
<%@page
	import="java.util.List,com.liferay.portlet.asset.model.AssetCategory,com.liferay.portal.util.PortalUtil"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ page import="com.liferay.portlet.asset.model.AssetEntry"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<style>
#content {
	margin-left: 0px;
	margin-right: 0px;
}

.portlet-column-content {
	padding: 0px;
}
</style>

<% 
	String friendlyURL = "/dashboard";
	
	Layout layout1 = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getLayout().getGroupId(), true, friendlyURL);
	
	String layoutType =  null;
	String portletId = null;
	
	layoutType = layout1.getTypeSettingsProperty("column-1");
	StringTokenizer st = new StringTokenizer(layoutType,",");
	if (st.hasMoreTokens()) {
		portletId = st.nextToken();
	}
	PortletURL subjectURL = PortletURLFactoryUtil.create(request,portletId,layout1.getPlid(),PortletRequest.RESOURCE_PHASE);
	subjectURL.setParameter("p_p_resource_id","showSubjects");
	
	
	layoutType = layout1.getTypeSettingsProperty("column-3");
	st = new StringTokenizer(layoutType,",");
	if (st.hasMoreTokens()) {
		portletId = st.nextToken();
	}
	PortletURL profileURL = PortletURLFactoryUtil.create(request,portletId,layout1.getPlid(),PortletRequest.RESOURCE_PHASE);
	profileURL.setParameter("p_p_resource_id","showProfile");
	
	layoutType = layout1.getTypeSettingsProperty("column-2");
	st = new StringTokenizer(layoutType,",");
	if (st.hasMoreTokens()) {
		portletId = st.nextToken();
	}
	PortletURL calURL = PortletURLFactoryUtil.create(request,portletId,layout1.getPlid(),PortletRequest.RESOURCE_PHASE);
	calURL.setParameter("p_p_resource_id","showEvents");
	%>



<script>
/* For offcan menu */
function invokeDashboardEvents() {
	showSubjects();
	showEvents();
	showProfile();
}

function showSubjects() {
   $.ajax({
      type: "GET",
      url: '<%=subjectURL%>',      
      success: function(data) {
    	  		$('#subjectsDiv').html(data);
			}
   });
}

function showEvents() {
	   $.ajax({
	      type: "GET",
	      url: '<%=calURL%>',           
	      success: function(data) {
	    	  		$('#eventsDiv').html(data);
				}
	   });
	}
	
function showProfile() {
	
	   $.ajax({
	      type: "GET",
	      url: '<%=profileURL%>',          
	      success: function(data) {
	    	  		$('#profileDiv').html(data);
				}
	   });
	}
</script>

<div class="canvastopbar">
<div class="topnav_left"><a href="/group/guest/dashboard"><img
	src="/tlw-theme/images/logo.png" alt="Logo"
	class="canvaslogo"></a>
<ul class="topnavlist">
	<li><a href="/group/guest/dashboard">MY ACCOUNT</a></li>
	<li><a href="/web/guest/home">HOME</a></li>
</ul>
</div>
<div class="topnav_right">

<div class="setting_login">
<ul>
	<li><%-- <a href="#"><img
						src="<%=themeDisplay.getPathThemeImages()%>/settingsicon.png"
						alt=""> </a></li> --%>
	<li><a href="<%=themeDisplay.getURLSignOut()%>" style="font-size: 14px;color: #636363;" >LOGOUT</a></li>
</ul>
</div>
<%-- <div class="searchbox">
			<form>
				<input type="search" placeholder="Search..." name="Searchbox"
					id="expand"><a href="#"><img
					src="<%=themeDisplay.getPathThemeImages()%>/searcharrow.png"
					class="searcharrow"> </a>
			</form>
		</div> --%></div>
</div>


<div class="offcanvascontentsection">

<div class="canvasleftsection">
<div class="innerpad" id="subjectsDiv">Loading...</div>
</div>

<div class="canvascentersection">
<div class="innerpad" id="eventsDiv">Loading...</div>
</div>

<div class="canvasrightsection">
<div class="innerpad" id="profileDiv">Loading...</div>
</div>
</div>