package com.liferay.portal.events;

import java.util.Collection;
import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.PortalSessionContext;
import com.liferay.portal.kernel.struts.LastPath;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;

public class CustomLoginPostAction extends Action {

	public static boolean USER_MULTIPLE_SESSIONS_ALLOWED = true;
	public CustomLoginPostAction() {
		super();
		USER_MULTIPLE_SESSIONS_ALLOWED = GetterUtil.getBoolean(PropsUtil.get("user.multiple.sessions.allowed"), true);
	}

	public void run(HttpServletRequest request, HttpServletResponse response)
	throws ActionException {
		try {
			debug("CustomLoginPostAction for User: " + request.getRemoteUser());
			HttpSession currentUserSession = request.getSession();
			LastPath lastPath = new LastPath(request.getContextPath(),"/group/guest/dashboard");

			debug("lastPath = " + lastPath.toString());

			currentUserSession.setAttribute(WebKeys.LAST_PATH, lastPath);

			//boolean multipleLoginsAllowed = GetterUtil.getBoolean(PropsUtil.get("user.multiple.sessions.allowed"), true);

			if(!USER_MULTIPLE_SESSIONS_ALLOWED){
				_log.info("Multiple Logins not allowed... Checking for existing active sessions for the logged in user");
				disableMultipleLogins(request, response);
			}else{
				_log.info("Multiple Logins allowed...");
			}

		} catch (Exception e) {
			e.printStackTrace();
			//throw new ActionException(e);
		}
	}

	private void disableMultipleLogins(HttpServletRequest request, HttpServletResponse response) {
		HttpSession currentUserSession = request.getSession();
		Collection<HttpSession> allSessions = PortalSessionContext.values();
		debug("Total session Count : " + allSessions.size());
		debug("Current Session Id : " + currentUserSession.getId());
		try{
			for(HttpSession session : allSessions){
				debug("*************************************************************************************************************");
				Enumeration<String> en = session.getAttributeNames();
				debug("Ssssion Ids : " + session.getId());
				while(en.hasMoreElements()){
					String attrName = en.nextElement();
					debug(attrName + " = " + session.getAttribute(attrName));
					if(attrName.equalsIgnoreCase(WebKeys.USER_ID)){
						Long userId = (Long) session.getAttribute(attrName);
						Long currentUserId = (Long) currentUserSession.getAttribute(attrName);
						if(Validator.isNotNull(currentUserId) && Validator.isNotNull(userId) && userId.longValue() == currentUserId.longValue() && !session.getId().equals(currentUserSession.getId())){
							_log.info("An Active session already Exists for the current User : ["+ currentUserId +"] hence, Access denied!!");
							currentUserSession.invalidate();
							response.sendRedirect("/web/guest/access-denied");
							return;
						}
					}
				}
			}
		}catch (Exception e) {
			_log.info("Error while disabling multiple logins for user : " + e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * Helper Method to create debug log messages
	 * 
	 * @param msg
	 *            Log message as String
	 */
	private void debug(String msg) {
		if (_log.isDebugEnabled()) {
			_log.debug(msg);
		}
	}
	private static Log _log = LogFactoryUtil.getLog(CustomLoginPostAction.class);

}