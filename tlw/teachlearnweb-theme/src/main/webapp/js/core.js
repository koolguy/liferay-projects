$(document).ready(function() {
	// Assign height for layout,Left and Right Section
	var viewportHeight = $(window).height();// window.innerHeight;
	var layoutheight = viewportHeight - 34;
	var leftmenuheight = viewportHeight - 124;
	var rightmenuheight = viewportHeight - 124;
	var contentheight = viewportHeight - 172;
//	$('#maincontent').css('height', layoutheight);
	$('.mainnavsection').css('height', leftmenuheight);
	$('.rightmenusection').css('height', rightmenuheight);
	$('.contentbg').css('height', contentheight);
	$('.contentbg').css('overflow', 'auto');

	$(window).resize(function() {
		var viewportHeight = $(window).height();// window.innerHeight;
		var layoutheight = viewportHeight - 34;
		var leftmenuheight = viewportHeight - 124;
		var rightmenuheight = viewportHeight - 124;
		var contentheight = viewportHeight - 172;
	//	$('#maincontent').css('height', layoutheight);
		$('.mainnavsection').css('height', leftmenuheight);
		$('.rightmenusection').css('height', rightmenuheight);
		$('.contentbg').css('height', contentheight);
		$('.contentbg').css('overflow', 'auto');
	});

	// Off Canvas
	var viewportwidth = $(window).width();// window.innerWidth;
	var mainmovewidth = viewportwidth - 138;
	$('#offcanvasmenu').css({
		"width" : mainmovewidth + "px"
	});
	var offcansc = -Math.abs(mainmovewidth)
	$('#offcanvasmenu').css({
		"left" : offcansc + "px"
	});
	$('.offcantab').toggle(function() {
		var layoutheight = viewportHeight - 34;
		$('#maincontent').css('height', layoutheight);
		$('.scrollsection').animate({
			'left' : mainmovewidth
		}, 458);

		$("#column-2").css({
			display : "none"
		});
		
		refreshOffCanTab();

		$('#offcanvasmenu').animate({
			left : '0'
		}, 458);
		$('.Offcanmenu').css("background-image", "url(/teachlearnweb-theme/images/right_grey.png)");
	}, function() {
		$('#maincontent').css('height', "");
		$('#offcanvasmenu').animate({
			'left' : offcansc
		}, 458);
		

		$("#column-2").css({
			float : "right",
			display : "block"
		});

		$('.scrollsection').animate({
			left : '0',
			width : '100%'
		}, 458);
		$('.Offcanmenu').css("background-image", "url(/teachlearnweb-theme/images/left_grey.png)");
	});

//Accordion
/*$('.rmenutab').click(function() {
	$('.rmenutab').removeClass('ractive');
 	$('.rSubmenu').slideUp('normal');
	if($(this).next().is(':hidden') == true) {
		$(this).addClass('ractive');
		$(this).next().slideDown('normal');
	 } 
 });
$('.rSubmenu').hide();*/

$('.rmenutab').click(function() {
	
	
	$('.rSubmenu').slideUp('normal');
	if ($(this).hasClass('ractive')) {
		$(this).removeClass('ractive')
	} else {
		$('.rmenutab.ractive').removeClass('ractive');
		$(this).addClass('ractive')
		$(this).next().slideDown('normal');
	}
 });
$('.rSubmenu').hide();
$('li.active').parent().parent().slideDown('normal');

// Custom Scroll bar
$('.scroll').jScrollPane();
$(function(){
		$('#slider').anythingSlider({buildNavigation:false});
		 
	});

});

function refreshOffCanTab() {
	invokeDashboardEvents();
}
$(document).ready(function() {
	$('.portlet-msg-error').toggleClass('portlet-msg-error custom_message');
	});