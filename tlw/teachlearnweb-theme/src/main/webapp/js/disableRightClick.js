
var ie4   = (document.all)? true: false;
var isNS4 = (document.layers) ? true : false;
var isIE5 = (document.all && document.getElementById) ? true : false;
var isNS6 = (!document.all && document.getElementById) ? true : false;

/* BrowserComp:ADD-25/11/2010-Mohsin:START */
var isChSaf = false; //flag for Chrome and Safari
/* BrowserComp:ADD-25/11/2010-Mohsin:END */


function callInit(){

	try {
		var useragent = new String(navigator.userAgent); /* BrowserComp:ADD-25/11/2010-MohsinK */

		//document.onkeydown		= disablekeyboardnavigation;

		if(isIE5){
			document.onmousedown = disablerightclick;

		/* BrowserComp:ADD-25/11/2010-MohsinK-START */

		}else if((useragent.indexOf("Chrome") != -1) || (useragent.indexOf("Safari") != -1)){
			isChSaf = true;
			document.onmouseup   = disablerightclick;

		/* BrowserComp:ADD-25/11/2010-MohsinK-END */	

		} else {
			document.onclick	 = disablerightclick;
		}

		if (document.layers || (!document.all && document.getElementById)) {
			//alert('layers');
			window.captureEvents(Event.MOUSEDOWN);
			document.captureEvents(Event.KEYDOWN);
		}
	} catch (e0) {
		//alert('RS_01_eng[callInit] error occurred => '+e0.message);
	}
}
//-------------------------------------------------------------------------------------------------
function disablerightclick(e){
	var mouse_click	= "";

	try {
		/* BrowserComp:ADD-25/11/2010-MohsinK-START */
		
		if(isChSaf){
			mouse_click	= e.which;

		/* BrowserComp:ADD-25/11/2010-MohsinK-END */
		}else if(window.event) {/* BrowserComp:MOD-25/11/2010-MohsinK */
			mouse_click	= event.button;		//IE
		} else {
			mouse_click	= e.which;			//firefox
		}

        /* BrowserComp:ADD-25/11/2010-MohsinK-START */
		if(isChSaf && mouse_click == 3 && (event.srcElement.tagName != "U")){
			document.oncontextmenu = returnFalse;
		}

		if(isChSaf && (event.srcElement.tagName == "U")){
			document.oncontextmenu = returnTrue;
		}
        /* BrowserComp:ADD-25/11/2010-MohsinK-END */

		//alert('navigator---'+navigator.appName+', mouse_click--'+mouse_click+', isIE5-'+isIE5);
		if (mouse_click == 2 || mouse_click == 3 || mouse_click == 4){
			if (isIE5 && mouse_click == 2 && (event.srcElement.tagName != "U")){
				alert("Mouse Right Click Disabled");
				return false;
			} else if(!isIE5 && mouse_click == 3 && (e.target.tagName != 'U')){
				alert("Mouse Right Click Disabled");
				e.preventDefault();
				e.stopPropagation();
				return false;
			}
		}
	} catch (e1) {
		//alert('RS_01_eng[disablerightclick] error occurred => '+e.message);
		mouse_click = "";
	}	
	return true;
}
callInit();

function returnFalse () {return false}

//----------------------------------------------------------------------

function returnTrue () {return true}
