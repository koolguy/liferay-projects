$(document).ready(function(){
	var lbg = $("<div class='lightbox_bg'></div>");
	$('body').append(lbg);
	
	$('.reportlightboxopen').live('ShowReport', function(){
		prepareLightBox(this);
	});

	$('.lightboxopen').live('ShowAssessment', function(){
		prepareLightBox(this);
	});
	
	$('.viewlightboxopen').live('ViewReward', function(){
		prepareLightBox(this);
	});

	$('a.lbclose').click(function(){
		var myval2 =$(selectedObj).parent().parent().parent().parent().attr('id');
		$("#" + myval2).animate({
			opacity: 0,
			top: "-2000px"
		},10,function(){
			$('.lightbox_bg').hide()
		});
	});
});
function prepareLightBox(selectedObj) {
	var lclose = $("<a class='lightbox_close'>Close This</a>");
	$('.lightbox').append(lclose);
	
	$('a.lightbox_close').click(function(){
		var myval2 =$(this).parent().attr('id');
		$("#" + myval2).animate({
			opacity: 0,
			top: "-2000px"
		},10,function(){
			$('.lightbox_bg').hide()
		});
	});


	var myval = $(selectedObj).attr("rel");
	var moniter_wdith = window.innerWidth;
	var moniter_height = window.innerHeight;
	var lightboxinfo_wdith = $("#" + myval).width();	
	var lightboxinfo_height= $("#" + myval).height();
	var remain_wdith =  moniter_wdith - lightboxinfo_wdith;		
	var remain_height =  moniter_height - lightboxinfo_height;		
	var byremain_wdith = remain_wdith/2;
	var byremain_height = remain_height/2;
	var byremain_height2 = byremain_height;

	// Lightbox Height set
	//var viewportwidth3 = $('.lightboxcontanerright').height();
	//alert(viewportwidth3);

	$("#" + myval).css({left:byremain_wdith});
	$("#" + myval).css({top:byremain_height2});
	$('.lightbox_bg').show();
	$("#" + myval).animate({
		opacity: 1,
		}, 10 ,function() {
    			var viewportwidth3 = $('.lightbox').height();
			$(".lbcheightfix").css({height:viewportwidth3});
  		});	
}
