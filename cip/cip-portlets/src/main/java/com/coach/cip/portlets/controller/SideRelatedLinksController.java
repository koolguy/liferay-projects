
package com.coach.cip.portlets.controller;

import java.util.HashMap;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.xml.namespace.QName;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;

@Controller("sideRelatedLinksController")
@RequestMapping(value = "VIEW")
public class SideRelatedLinksController extends AbstractBaseController {

	/** The Constant LOG. */
	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(SideRelatedLinksController.class);
	private static final String SIDERELATEDLINKS = "sideRelatedLinks";
	
	/**
	 * Renders sideRelatedLinks view.
	 * 
	 * @param request
	 * @param response
	 * @return 
	 */
	@RenderMapping
	public String showSideRelatedLinks(RenderRequest request,RenderResponse response) {
		LoggerUtil.debugLogger(logger,"Render mapping for sideRelatedlInks view in SideRelatedLinksController");
		return SIDERELATEDLINKS;
	}
	
	/**
	 * Action Mapping to publish sideRelateLink event.
	 * 
	 * @param request
	 * @param response
	 */
	//@ActionMapping(params = "myaction=publishSideRelatedlinkURL")
	public void publishSideRelatedlinkURL(ActionRequest request, ActionResponse response, Model model){
		LoggerUtil.debugLogger(logger,"Action mapping to publish event in SideRelatedLinksController");
		HashMap<String, String> sideRelatedLinksMap = new HashMap<String, String>();
		String fileURL = ParamUtil.getString(request, "fileURL", "");
		String fileTitle = ParamUtil.getString(request, "fileTitle", "");
		LoggerUtil.debugLogger(logger,"Action mapping to publish event in SideRelatedLinksController -- fileURL----"+fileURL);
		LoggerUtil.debugLogger(logger,"Action mapping to publish event in SideRelatedLinksController -- fileTitle ----"+fileTitle);
		if(fileTitle != null && !fileTitle.isEmpty()){
			model.addAttribute("fileTitle", fileTitle);
		}
		
		sideRelatedLinksMap.put("fileURL", fileURL);
		sideRelatedLinksMap.put("fileTitle", fileTitle);
		sideRelatedLinksMap.put("fileType", request.getParameter("fileType") != null ? request.getParameter("fileType") : "");
		sideRelatedLinksMap.put("classPK", request.getParameter("classPK") != null ? request.getParameter("classPK") : "");
		sideRelatedLinksMap.put("typeOfLink", request.getParameter("typeOfLink") != null ? request.getParameter("typeOfLink") : "");
		sideRelatedLinksMap.put("mimeType", request.getParameter("mimeType") != null ? request.getParameter("mimeType") : "");

		QName qName = new QName("http://coach.com/events","ipc.sideRelatedLinksFileURL");
		response.setEvent(qName, sideRelatedLinksMap);
		
	}
}
