/**
 * 
 */
package com.coach.cip.common.dto;

import java.io.Serializable;

/**
 * @author GalaxE
 *
 */
public class DirectReportVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5074999271373596916L;
	private String directReportId;
	private String directReportFirstName;
	private String directReportLastName;
	public String getDirectReportId() {
		return directReportId;
	}
	public void setDirectReportId(String directReportId) {
		this.directReportId = directReportId;
	}
	public String getDirectReportFirstName() {
		return directReportFirstName;
	}
	public void setDirectReportFirstName(String directReportFirstName) {
		this.directReportFirstName = directReportFirstName;
	}
	public String getDirectReportLastName() {
		return directReportLastName;
	}
	public void setDirectReportLastName(String directReportLastName) {
		this.directReportLastName = directReportLastName;
	}
	
	

}
