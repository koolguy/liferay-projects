
package com.coach.cip.portlets.controller;

import java.io.IOException;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.coach.cip.common.dto.DirectReportVO;
import com.coach.cip.common.dto.EmployeeProfileVO;
import com.coach.cip.common.dto.NewHiresVO;
import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.coach.cip.util.WebUtil;
import com.coach.employees.service.beans.GetEmployeesByIDFault;
import com.coach.employees.service.beans.GetEmployeesByIDRequest;
import com.coach.employees.service.beans.GetEmployeesByIDRequest.EMPByUserId;
import com.coach.employees.service.beans.GetEmployeesByIDResponse;
import com.coach.employees.service.beans.ObjectFactory;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import com.liferay.portal.model.Layout;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.model.JournalTemplate;
import com.liferay.portlet.journal.service.JournalTemplateLocalServiceUtil;

/**
 * Class is used to capture the new hires.
 * @author GalaxE.
 */
@Controller("newHiresController")
public class NewHiresController extends AbstractBaseController {

	/** The Log Constant */
	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(NewHiresController.class);

	List<NewHiresVO> allNewHiresList = null;
	@Autowired
	@Qualifier("employeeServiceTemplate")
	private WebServiceTemplate webServiceTemplate;
	private static final ObjectFactory WS_CLIENT_FACTORY = new ObjectFactory();

	/**
	 * Default Constructor.
	 */
	public NewHiresController() {

	}

	/**
	 * Constructor which initializes webserviceTemplate.
	 * @param webServiceTemplate
	 */
	public NewHiresController(WebServiceTemplate webServiceTemplate) {

		LoggerUtil.debugLogger(logger, "Inside the NewHiresController contructor ::");
		this.webServiceTemplate = webServiceTemplate;
	}

	/**
	 * Render the default page i.e, All New Hires Page but based on condition it
	 * will display new hires page.
	 * @param renderResponse
	 * @return newHiresPage
	 * @throws PortalException
	 * @throws SystemException
	 */
	@RenderMapping
	public String showNewHires(RenderRequest renderRequest, RenderResponse renderResponse, Model model) throws PortalException, SystemException {

		LoggerUtil.debugLogger(logger, "Inside the method showNewHires() in NewHiresController ::");

		Layout currentLayout = WebUtil.getThemeDisplay(renderRequest).getLayout();
		Locale locale = WebUtil.getThemeDisplay(renderRequest).getLocale();

		if (currentLayout.getFriendlyURL().contains(Constants.EMPLOYEE_DIRECTORY_FRIENDLY_URL) ||
			(currentLayout.getFriendlyURL().contains(Constants.EMPLOYEE) && currentLayout.getFriendlyURL().contains(Constants.DIRECTORY)) ||
			Constants.EMPLOYEE_DIRECTORY_HTML_TITLE.equalsIgnoreCase(currentLayout.getHTMLTitle(locale)) ||
			Constants.EMPLOYEE_DIRECTORY_HTML_TITLE.equalsIgnoreCase(currentLayout.getName(locale))) {

			return Constants.NEW_HIRES;
		}
		// Auditing the action starts
		if (Validator.isNotNull(allNewHiresList)) {
			WebUtil.createAuditRecord(this.getClass().getName(), Constants.NEW_HIRES_ACTION_TYPE, Constants.ALL);
		}
		// Auditing the action ends
		return Constants.ALL_NEW_HIRES;

	}

	/**
	 * Get new hires details list and most five recent new hires list.
	 * @param portletRequest
	 * @param model
	 * @return newHiresList
	 * @throws PortalException
	 * @throws SystemException
	 * @throws RemoteException
	 * @throws DocumentException
	 * @throws ParseException
	 */
	@ModelAttribute(value = "newHiresList")
	public List<NewHiresVO> getNewHiresList(PortletRequest portletRequest, Model model) throws PortalException, SystemException, RemoteException {

		LoggerUtil.debugLogger(logger, "Start of model getNewHiresList() method of NewHiresController::");

		List<NewHiresVO> newHiresList = new ArrayList<NewHiresVO>();
		allNewHiresList = new ArrayList<NewHiresVO>();
		NewHiresVO newHiresVO;
		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String strDate = null;
		Date joiningDate = null;
		String templateId = Constants.EMPTY;
		String templateName = Constants.EMPTY;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
		List<JournalArticle> articlesList =
			WebUtil.getArticlesByType(themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId(), Constants.NEWHIRES_ARTICLE_TYPE, -1);
		try {

			for (JournalArticle journalArticle : articlesList) {

				templateId = journalArticle.getTemplateId();
				if (Validator.isNotNull(templateId)) {

					JournalTemplate journalTemplate = JournalTemplateLocalServiceUtil.getTemplate(themeDisplay.getScopeGroupId(), templateId);
					com.liferay.portal.kernel.xml.Document templatedocument = SAXReaderUtil.read(journalTemplate.getName());

					if (Validator.isNotNull(templatedocument) && Validator.isNotNull(templatedocument.getRootElement())) {

						templateName = templatedocument.getRootElement().elementText(Constants.TAMPLATE_NAME);

					}
				}

				if (Validator.isNotNull(templateName) && templateName.equals(Constants.NEWHIRES_TEMPLATE_NAME)) {

					newHiresVO = new NewHiresVO();
					if (Validator.isNotNull(journalArticle.getContent())) {

						com.liferay.portal.kernel.xml.Document document = SAXReaderUtil.read(journalArticle.getContent());
						if (Validator.isNotNull(document)) {

							newHiresVO.setFirstName(document.selectSingleNode(
								Constants.DYNAMIC_ELEMENT_NAME + Constants.FIRSTNAME + Constants.DYNAMIC_CONTENT).getText());
							newHiresVO.setLastName(document.selectSingleNode(
								Constants.DYNAMIC_ELEMENT_NAME + Constants.LASTNAME + Constants.DYNAMIC_CONTENT).getText());
							newHiresVO.setDesignation(document.selectSingleNode(
								Constants.DYNAMIC_ELEMENT_NAME + Constants.DESIGNATION + Constants.DYNAMIC_CONTENT).getText());
							newHiresVO.setDocumentPath(document.selectSingleNode(
								Constants.DYNAMIC_ELEMENT_NAME + Constants.DOCUMENT + Constants.DYNAMIC_CONTENT).getText());
							newHiresVO.setDocument(document.selectSingleNode(
								Constants.DYNAMIC_ELEMENT_NAME + Constants.DOCUMENT_DOC + Constants.DYNAMIC_CONTENT).getText());
							newHiresVO.setEmployeeId(document.selectSingleNode(
								Constants.DYNAMIC_ELEMENT_NAME + Constants.EMPLOYEEID + Constants.DYNAMIC_CONTENT).getText());
							strDate =
								document.selectSingleNode(Constants.DYNAMIC_ELEMENT_NAME + Constants.DATE + Constants.DYNAMIC_CONTENT).getText();
							if (strDate != null && !strDate.isEmpty()) {
								try {

									joiningDate = simpleDateFormat
											.parse(strDate);
									java.sql.Timestamp joiningTime = new java.sql.Timestamp(
											joiningDate.getTime());
									newHiresVO.setDate(joiningTime);
								} catch (ParseException e) {

									LoggerUtil.errorLogger(logger,
											e.getMessage(), e);
								}
							}

						}
					}

					allNewHiresList.add(newHiresVO);

				}
				else {

					LoggerUtil.debugLogger(logger, "New Hires Template and Structure name should be 'NewHires'");
				}
			}

		}
		catch (DocumentException e) {

			LoggerUtil.errorLogger(logger, e.getMessage(), e);
		}		
		catch (Exception e) {

			LoggerUtil.errorLogger(logger, e.getMessage(), e);
		}
		if (allNewHiresList != null && !allNewHiresList.isEmpty()) {

			Collections.sort(allNewHiresList);
			model.addAttribute(Constants.ALL_NEW_HIRES_LIST, allNewHiresList);
			/**
			 * Pick latest five new hires.
			 */
			int index = 0;
			for (NewHiresVO newHires : allNewHiresList) {

				if (index < 5) {

					newHiresList.add(newHires);
					index++;
				}
				else {
					break;
				}
			}
		}
		else {
			model.addAttribute(Constants.NO_NEW_HIRES, Constants.MSG_NO_NEW_HIRES);
		}
		LoggerUtil.debugLogger(logger, "End of model getNewHiresList() method of NewHiresController::");
		return newHiresList;
	}

	/**
	 * Get All new hires list with attachment.
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 * @throws RemoteException
	 */
	@ActionMapping(params = "myaction=getAllNewHiresList")
	public void getAllNewHiresList(ActionRequest actionRequest, ActionResponse actionResponse, Model model) throws RemoteException, IOException {

		LoggerUtil.debugLogger(logger, "Inside the method getAllNewHiresList() of NewHiresController ::");
		model.addAttribute(Constants.ALL_NEW_HIRES_LIST, allNewHiresList);
		actionResponse.sendRedirect(WebUtil.getPortalURL(actionRequest) + Constants.ALL_NEW_HIRES_URL);

	}

	/**
	 * Get the Employee Profile based on Employee ID by calling the Web Service
	 * method getEmployeeById().
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 *            contains the Details of the Employee.
	 * @throws WindowStateException
	 */
	@ActionMapping(params = "myaction=getEmployeeProfile")
	public void getEmployeeProfile(ActionRequest actionRequest, ActionResponse actionResponse, Model model) throws WindowStateException {

		LoggerUtil.debugLogger(logger, "Inside getEmployeeProfile() method of NewHireController::");
		String empId = actionRequest.getParameter(Constants.EMPLOYEE_ID);
		EmployeeProfileVO employeeProfileVO = null;
		employeeProfileVO = getEmployeeByID(empId, model);
		model.addAttribute(Constants.SEARCHED_EMP_OBJECT, employeeProfileVO);
		// Auditing the action starts
		if (Validator.isNotNull(employeeProfileVO)) {
			WebUtil.createAuditRecord(this.getClass().getName(), Constants.NEW_HIRES_ACTION_TYPE, employeeProfileVO.getEmpName());
		}
		// Auditing the action ends
		actionResponse.setWindowState(WindowState.MAXIMIZED);
		actionResponse.setRenderParameter(Constants.MY_ACTION, Constants.NEWHIRES_EMPLOYEE_PROFILE_PAGE);

	}

	/**
	 * Navigate to Employee Profile Page.
	 * @return navigation string {employeeDirectorySearchDetail}
	 */
	@RenderMapping(params = "myaction=newHiresEmployeeProfilePage")
	public String showSearchDetail(RenderRequest request, RenderResponse response, Model model) {

		LoggerUtil.debugLogger(logger, "Inside showSearchDetail() method of NewHireController::");
		return Constants.EMPLOYEE_PROFILE_NEWHIRES;
	}

	/**
	 * Open the attached document for the new hires details.
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 * @throws WindowStateException
	 */
	@ActionMapping(params = "myaction=getAttachment")
	public void getAttachment(ActionRequest actionRequest, ActionResponse actionResponse, Model model) throws WindowStateException {

		LoggerUtil.debugLogger(logger, "Inside getAttachment() method of NewHireController::");
		String employeeId = actionRequest.getParameter(Constants.EMP_ID);
		if (actionRequest.getParameter(Constants.DOCUMENT_PATH) != null && !actionRequest.getParameter(Constants.DOCUMENT_PATH).isEmpty()) {

			String documentPath = WebUtil.getPortalURL(actionRequest) + actionRequest.getParameter(Constants.DOCUMENT_PATH);
			model.addAttribute(Constants.DOCUMENT_PATH, documentPath);
			// Auditing the action starts
			if (Validator.isNotNull(documentPath)) {
				WebUtil.createAuditRecord(this.getClass().getName(), Constants.NEW_HIRES_ACTION_TYPE, documentPath);
			}
			// Auditing the action ends
		}
		else if (employeeId != null && !employeeId.isEmpty()) {

			for (NewHiresVO newHires : allNewHiresList) {

				if (newHires.getEmployeeId().equalsIgnoreCase(employeeId)) {
					model.addAttribute(Constants.WEBCONTENT_DOCUMENT, newHires.getDocument());
					// Auditing the action starts
					if (Validator.isNotNull(newHires)) {
							WebUtil.createAuditRecord(this.getClass().getName(), Constants.NEW_HIRES_ACTION_TYPE, newHires.getFirstName() + " " +
								newHires.getLastName() + " " + Constants.NEW_HIRES_DETAILS);
					}
					// Auditing the action ends
				}
			}

		}

		model.addAttribute(Constants.ATTACHMENTH_HEADER, Constants.NEW_HIRES_HEADER);
		actionResponse.setRenderParameter(Constants.MY_ACTION, Constants.ATTACHMENT_PAGE);
	}

	/**
	 * Navigate to Attachment Page.
	 * @return
	 */
	@RenderMapping(params = "myaction=attachmentPage")
	public String showAttachment() {

		LoggerUtil.debugLogger(logger, "Inside render mapping showAttachment() method of NewHiresController::");
		return Constants.ATTACHMENT;
	}

	/**
	 * Get Employee details based on the EmployeeId by calling the web service
	 * method employeesByID().
	 * @param employeeID
	 * @param model
	 * @return EmployeeProfileVO
	 */
	public EmployeeProfileVO getEmployeeByID(String employeeID, Model model) {

		LoggerUtil.debugLogger(logger, "Start of model getEmployeeByID() method of NewHiresController::");
		GetEmployeesByIDRequest employeesByIDRequest = WS_CLIENT_FACTORY.createGetEmployeesByIDRequest();
		EMPByUserId userId = new EMPByUserId();
		EmployeeProfileVO employeeProfileVO = null;
		DirectReportVO directReportVO = null;
		List<DirectReportVO> directReportVoList = new ArrayList<DirectReportVO>();
		List<com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee> employeeDetailsList = null;
		userId.setStrADUserID(employeeID);
		employeesByIDRequest.setEMPByUserId(userId);
		try {
			WebUtil.trust();
			Object responseObject = webServiceTemplate.marshalSendAndReceive(employeesByIDRequest);

			if (responseObject instanceof GetEmployeesByIDResponse) {

				GetEmployeesByIDResponse employeeByIdResponse = (GetEmployeesByIDResponse) responseObject;
				if (employeeByIdResponse != null && employeeByIdResponse.getEmployees() != null &&
					employeeByIdResponse.getEmployees().getEmployee() != null) {

					employeeDetailsList = employeeByIdResponse.getEmployees().getEmployee();
					if (employeeDetailsList != null && !employeeDetailsList.isEmpty() && employeeDetailsList.size() > 0) {

						for (com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee employee : employeeDetailsList) {

							if (employeeID != null && !employeeID.isEmpty() && employee.getUSERID() != null && !employee.getUSERID().isEmpty() &&
								employeeID.equalsIgnoreCase(employee.getUSERID())) {

								employeeProfileVO = new EmployeeProfileVO();
								employeeProfileVO.setEmpName(employee.getFIRSTNAME() + Constants.SPACE + employee.getLASTNAME());
								employeeProfileVO.setDesignation(employee.getTITLE());
								employeeProfileVO.setEmpPhone(employee.getPHONE());
								employeeProfileVO.setFax(employee.getFAX());
								employeeProfileVO.setEmailAddress(employee.getEMAIL());
								employeeProfileVO.setOffice(employee.getOFFICENAME());
								employeeProfileVO.setBuildingName(employee.getBUILDINGNAME());
								employeeProfileVO.setAddress1(employee.getADDRESS());
								employeeProfileVO.setAddress2(employee.getADDRESS2());
								employeeProfileVO.setAddress3(employee.getADDRESS3());
								employeeProfileVO.setFloor(employee.getFLOOR());
								employeeProfileVO.setCity(employee.getCITY());
								employeeProfileVO.setCountry(employee.getCOUNTRY());
								employeeProfileVO.setZip(employee.getZIP());
								employeeProfileVO.setEmpDepartment(employee.getDEPARTMENT());
								employeeProfileVO.setEmpGeography(employee.getGEOGRAPHY());

								if (employee.getMANAGER() != null) {

									employeeProfileVO.setManagerFirstName(employee.getMANAGER().getFIRSTNAME());
									employeeProfileVO.setManagerLastname(employee.getMANAGER().getLASTNAME());
									employeeProfileVO.setManagerId(employee.getMANAGER().getUSERID());
								}
								if (employee.getASSISTANT() != null) {

									employeeProfileVO.setAssistantFirstName(employee.getASSISTANT().getFIRSTNAME());
									employeeProfileVO.setAssistantLastName(employee.getASSISTANT().getLASTNAME());
									employeeProfileVO.setAssistantId(employee.getASSISTANT().getUSERID());
								}

								if (employee.getDIRECTREPORTS() != null && employee.getDIRECTREPORTS().getDIRECTREPORT() != null) {

									List<com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT> directReportsList =
										employee.getDIRECTREPORTS().getDIRECTREPORT();

									for (com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT directReport : directReportsList) {
										directReportVO = new DirectReportVO();
										directReportVO.setDirectReportFirstName(directReport.getFIRSTNAME());
										directReportVO.setDirectReportLastName(directReport.getLASTNAME());
										directReportVO.setDirectReportId(directReport.getUSERID());
										directReportVoList.add(directReportVO);
									}
									employeeProfileVO.setDirectReports(directReportVoList);
								}

							}
						}
					}
				}
			}
			else if (responseObject instanceof GetEmployeesByIDFault) {

				GetEmployeesByIDFault employeeFault = (GetEmployeesByIDFault) responseObject;
				model.addAttribute(Constants.FAULT_MESSAGE, Constants.ERR_WEBSERVICE_PROCESSING);
				LoggerUtil.debugLogger(logger, employeeFault.getErrCode());
			}

		}
		catch (SoapFaultClientException sfce) {

			LoggerUtil.errorLogger(logger, sfce.getMessage(), sfce);
			model.addAttribute(Constants.FAULT_MESSAGE, Constants.ERR_WEBSERVICE_PROCESSING);
		}
		catch (Exception ex) {

			LoggerUtil.errorLogger(logger, ex.getMessage(), ex);
			model.addAttribute(Constants.FAULT_MESSAGE, Constants.ERR_WEBSERVICE_PROCESSING);
		}
		LoggerUtil.debugLogger(logger, "End of model getEmployeeByID() method of NewHiresController::");
		return employeeProfileVO;
	}
}
