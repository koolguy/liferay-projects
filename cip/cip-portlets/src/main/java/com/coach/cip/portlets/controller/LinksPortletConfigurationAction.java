
package com.coach.cip.portlets.controller;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletConfig;
import javax.portlet.PortletPreferences;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.DefaultConfigurationAction;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portlet.PortletPreferencesFactoryUtil;

/**
 * Configuration class for links portlet
 * 
 * @author Galaxe.
 */
@Controller("linksPortletConfigurationAction")
@RequestMapping(value = "CONFIG")
public class LinksPortletConfigurationAction extends DefaultConfigurationAction {

	/** The Constant LOG. */
	private static final Log logger = LogFactoryUtil.getLog(LinksPortletConfigurationAction.class);

	/**
	 * Process action method
	 * @param portletConfig
	 * @param actionRequest
	 * @param actionResponse
	 * @throws Exception
	 */
	@Override
	public void processAction(PortletConfig portletConfig, ActionRequest actionRequest, ActionResponse actionResponse) throws Exception {

		LoggerUtil.debugLogger(logger, "Start of processAction() method of LinksPortletConfigurationAction::");
		String portletResource = ParamUtil.getString(actionRequest, "portletResource");
		PortletPreferences portletPreferences = PortletPreferencesFactoryUtil.getPortletSetup(actionRequest, portletResource);
		String actionType = ParamUtil.getString(actionRequest, "actionType");
		int deleteIndex = ParamUtil.getInteger(actionRequest, "deleteIndex", -1);
		String[] linkNamesArr = portletPreferences.getValues("linkNamesArr", new String[] {});
		String[] linkUrlsArr = portletPreferences.getValues("linkUrlsArr", new String[] {});
		if ("addRow".equalsIgnoreCase(actionType)) {

			linkNamesArr = addStringElement(linkNamesArr, "Link Name");
			linkUrlsArr = addStringElement(linkUrlsArr, "#");

		}
		if ("deleteRow".equalsIgnoreCase(actionType)) {
			if (deleteIndex != -1) {
				linkNamesArr = deletStringElement(linkNamesArr, deleteIndex);
				linkUrlsArr = deletStringElement(linkUrlsArr, deleteIndex);
			}

		}
		else if ("save".equalsIgnoreCase(actionType)) {
			for (int index = 0; index < linkNamesArr.length; index++) {
				linkNamesArr[index] = ParamUtil.getString(actionRequest, "linkName" + index, "");
				linkUrlsArr[index] = ParamUtil.getString(actionRequest, "linkUrl" + index, "");
			}
		}
		portletPreferences.setValues("linkNamesArr", linkNamesArr);
		portletPreferences.setValues("linkUrlsArr", linkUrlsArr);
		portletPreferences.store();
		LoggerUtil.debugLogger(logger, "End of processAction() method of LinksPortletConfigurationAction::");
	}

	/**
	 * Render method
	 * @param portletConfig
	 * @param renderRequest
	 * @param renderResponse
	 * @throws Exception
	 */
	@Override
	public String render(PortletConfig portletConfig, RenderRequest renderRequest, RenderResponse renderResponse) throws Exception {

		return "/WEB-INF/jsp/" + Constants.LINKS_PORTLET_CONFIG_JSP + ".jsp";
	}

	/**
	 * Returns a new string array after adding source array and string element
	 * into it.
	 * @param src
	 * @param str
	 * @return String[]
	 */
	static public String[] addStringElement(String[] src, String str) {

		LoggerUtil.debugLogger(logger, "Start of addStringElement() method of LinksPortletConfigurationAction::");
		int srcLength = 0;
		if(Validator.isNotNull(src)){
			srcLength = src.length;
		}
		String[] target = new String[srcLength + 1];
		int index;
		for (index = 0; index < srcLength; index++) {
			target[index] = src[index];
		}
		target[index] = str;
		LoggerUtil.debugLogger(logger, "End of addStringElement() method of LinksPortletConfigurationAction::");
		return target;
	}

	/**
	 * Returns a new string array after copying all elements from source array
	 * except the element at given index.
	 * @param src
	 * @param deleteIndex
	 * @return String[]
	 */
	static public String[] deletStringElement(String[] src, int deleteIndex) {

		LoggerUtil.debugLogger(logger, "Start of deletStringElement() method of LinksPortletConfigurationAction::");
		List<String> target = new ArrayList<String>();
		for (int index = 0; index < src.length; index++) {
			if (index != deleteIndex) {
				target.add(src[index]);
			}
		}
		LoggerUtil.debugLogger(logger, "End of deletStringElement() method of LinksPortletConfigurationAction::");
		return target.toArray(new String[src.length - 1]);
	}
}
