
package com.coach.cip.portlets.controller;

import java.io.IOException;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.coach.cip.common.dto.DirectReportVO;
import com.coach.cip.common.dto.EmployeeProfileVO;
import com.coach.cip.common.dto.PromotionsVO;
import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.coach.cip.util.WebUtil;
import com.coach.employees.service.beans.GetEmployeesByIDFault;
import com.coach.employees.service.beans.GetEmployeesByIDRequest;
import com.coach.employees.service.beans.GetEmployeesByIDRequest.EMPByUserId;
import com.coach.employees.service.beans.GetEmployeesByIDResponse;
import com.coach.employees.service.beans.ObjectFactory;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import com.liferay.portal.model.Layout;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.model.JournalTemplate;
import com.liferay.portlet.journal.service.JournalTemplateLocalServiceUtil;

/**
 * Class is used to capture the promotions.
 * @author GalaxE.
 */
@Controller("promotionsController")
public class PromotionsController extends AbstractBaseController {

	/** The Log Constant */
	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(PromotionsController.class);

	List<PromotionsVO> allPromotionsList = null;

	@Autowired
	@Qualifier("employeeServiceTemplate")
	private WebServiceTemplate webServiceTemplate;
	private static final ObjectFactory WS_CLIENT_FACTORY = new ObjectFactory();

	/**
	 * Default constructor.
	 */
	public PromotionsController() {

	}

	/**
	 * Constructor which initializes webserviceTemplate.
	 * @param webServiceTemplate
	 */
	public PromotionsController(WebServiceTemplate webServiceTemplate) {

		LoggerUtil.debugLogger(logger, "Inside the PromotionsController constructor ::");
		this.webServiceTemplate = webServiceTemplate;
	}

	/**
	 * Render the default page i.e, promotions page.
	 * @param renderResponse
	 * @return promotions page
	 * @throws PortalException
	 * @throws SystemException
	 */
	@RenderMapping
	public String showPromotions(RenderRequest renderRequest, RenderResponse renderResponse) throws PortalException, SystemException {

		LoggerUtil.debugLogger(logger, "Inside the method showPromotions() of PromotionsController ::");

		Layout currentLayout = WebUtil.getThemeDisplay(renderRequest).getLayout();
		Locale locale = WebUtil.getThemeDisplay(renderRequest).getLocale();

		if (currentLayout.getFriendlyURL().contains(Constants.EMPLOYEE_DIRECTORY_FRIENDLY_URL) ||
			(currentLayout.getFriendlyURL().contains(Constants.EMPLOYEE) && currentLayout.getFriendlyURL().contains(Constants.DIRECTORY)) ||
			Constants.EMPLOYEE_DIRECTORY_HTML_TITLE.equalsIgnoreCase(currentLayout.getHTMLTitle(locale)) ||
			Constants.EMPLOYEE_DIRECTORY_HTML_TITLE.equalsIgnoreCase(currentLayout.getName(locale))) {

			return Constants.PROMOTIONS;
		}
		// Auditing the action starts
		if (Validator.isNotNull(allPromotionsList)) {
			WebUtil.createAuditRecord(this.getClass().getName(), Constants.PROMOTIONS_ACTION_TYPE, Constants.ALL);
		}
		// Auditing the action ends
		return Constants.ALL_PROMOTIONS;
	}

	/**
	 * Get the Most five recent promotions details.
	 * @param portletRequest
	 * @param model
	 * @return promotionsList
	 * @throws PortalException
	 * @throws SystemException
	 * @throws RemoteException
	 */
	@ModelAttribute(value = "promotionsList")
	public List<PromotionsVO> getPromotionsList(PortletRequest portletRequest, Model model) throws PortalException, SystemException, RemoteException {

		LoggerUtil.debugLogger(logger, "Start of model getPromotionsList() method of PromotionsController::");

		List<PromotionsVO> promotionsList = new ArrayList<PromotionsVO>();
		PromotionsVO promotionsVO;
		allPromotionsList = new ArrayList<PromotionsVO>();
		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		String strDate = null;
		Date joiningDate = null;
		String templateId = Constants.EMPTY;
		String templateName = Constants.EMPTY;
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(Constants.DATE_FORMAT);
		List<JournalArticle> articlesList =
			WebUtil.getArticlesByType(themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId(), Constants.PROMOTION_ARTICLE_TYPE, -1);
		try {
			for (JournalArticle journalArticle : articlesList) {

				templateId = journalArticle.getTemplateId();
				if (Validator.isNotNull(templateId)) {

					JournalTemplate journalTemplate = JournalTemplateLocalServiceUtil.getTemplate(themeDisplay.getScopeGroupId(), templateId);
					com.liferay.portal.kernel.xml.Document templatedocument = SAXReaderUtil.read(journalTemplate.getName());

					if (Validator.isNotNull(templatedocument) && Validator.isNotNull(templatedocument.getRootElement())) {

						templateName = templatedocument.getRootElement().elementText(Constants.TAMPLATE_NAME);

					}
				}

				if (Validator.isNotNull(templateName) && templateName.equals(Constants.PRMOTIONS_TEMPLATE_NAME)) {
					promotionsVO = new PromotionsVO();

					if (Validator.isNotNull(journalArticle.getContent())) {

						com.liferay.portal.kernel.xml.Document document = SAXReaderUtil.read(journalArticle.getContent());
						if (Validator.isNotNull(document)) {

							promotionsVO.setFirstName(document.selectSingleNode(
								Constants.DYNAMIC_ELEMENT_NAME + Constants.FIRSTNAME + Constants.DYNAMIC_CONTENT).getText());
							promotionsVO.setLastName(document.selectSingleNode(
								Constants.DYNAMIC_ELEMENT_NAME + Constants.LASTNAME + Constants.DYNAMIC_CONTENT).getText());
							promotionsVO.setDocument(document.selectSingleNode(
								Constants.DYNAMIC_ELEMENT_NAME + Constants.DOCUMENT_DOC + Constants.DYNAMIC_CONTENT).getText());
							promotionsVO.setDesignation(document.selectSingleNode(
								Constants.DYNAMIC_ELEMENT_NAME + Constants.DESIGNATION + Constants.DYNAMIC_CONTENT).getText());
							promotionsVO.setPromotionDetails(document.selectSingleNode(
								Constants.DYNAMIC_ELEMENT_NAME + Constants.PROMOTIONDETAILS + Constants.DYNAMIC_CONTENT).getText());
							promotionsVO.setDocumentPath(document.selectSingleNode(
								Constants.DYNAMIC_ELEMENT_NAME + Constants.DOCUMENT + Constants.DYNAMIC_CONTENT).getText());
							promotionsVO.setEmployeeId(document.selectSingleNode(
								Constants.DYNAMIC_ELEMENT_NAME + Constants.EMPLOYEEID + Constants.DYNAMIC_CONTENT).getText());
							strDate =
								document.selectSingleNode(Constants.DYNAMIC_ELEMENT_NAME + Constants.DATE + Constants.DYNAMIC_CONTENT).getText();
							if (strDate != null && !strDate.isEmpty()) {
								try {

									joiningDate = simpleDateFormat
											.parse(strDate);
									java.sql.Timestamp joiningTime = new java.sql.Timestamp(
											joiningDate.getTime());
									promotionsVO.setDate(joiningTime);
								} catch (ParseException e) {

									LoggerUtil.errorLogger(logger,
											e.getMessage(), e);
								}
							}

						}

					}
					allPromotionsList.add(promotionsVO);
				}
				else {

					LoggerUtil.debugLogger(logger, "Promotions Template and Structure name should be 'Promotions'");
				}

			}
		}
		catch (DocumentException e) {

			LoggerUtil.errorLogger(logger, e.getMessage(), e);
		}
		catch (Exception e) {

			LoggerUtil.errorLogger(logger, e.getMessage(), e);
		}
		if (allPromotionsList != null && !allPromotionsList.isEmpty()) {

			Collections.sort(allPromotionsList);
			model.addAttribute(Constants.ALL_PROMOTIONS_LIST, allPromotionsList);
			// Picking latest five Promotions

			int index = 0;
			for (PromotionsVO promotion : allPromotionsList) {

				if (index < 5) {

					promotionsList.add(promotion);
					index++;
				}
				else {
					break;
				}
			}
		}
		else {

			model.addAttribute(Constants.NO_PROMOTIONS, Constants.MSG_NO_PROMOTIONS);
		}
		LoggerUtil.debugLogger(logger, "End of model getPromotionsList() method of PromotionsController::");
		return promotionsList;
	}

	/**
	 * Get All Promotions Details with attachment.
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 * @throws RemoteException
	 */
	@ActionMapping(params = "myaction=getAllPromotionsList")
	public void getAllPromotionsList(ActionRequest actionRequest, ActionResponse actionResponse, Model model) throws RemoteException, IOException {

		LoggerUtil.debugLogger(logger, "Inside the method getAllPromotionsList() of PromotionsController ::");
		model.addAttribute(Constants.ALL_PROMOTIONS_LIST, allPromotionsList);
		actionResponse.sendRedirect(WebUtil.getPortalURL(actionRequest) + Constants.ALL_PROMOTIONS_URL);

	}

	/**
	 * Get the Employee Profile based on Employee ID by calling the Web Service
	 * method getEmployeeById().
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 *            contains the Details of the Employee.
	 * @throws WindowStateException
	 */
	@ActionMapping(params = "myaction=getEmployeeProfile")
	public void getEmployeeProfile(ActionRequest actionRequest, ActionResponse actionResponse, Model model) throws WindowStateException {

		LoggerUtil.debugLogger(logger, "Inside getEmployeeProfile() method of PromotionsController ::");
		String empId = actionRequest.getParameter(Constants.EMPLOYEE_ID);
		EmployeeProfileVO employeeProfileVO = null;
		employeeProfileVO = getEmployeeByID(empId, model);
		model.addAttribute(Constants.SEARCHED_EMP_OBJECT, employeeProfileVO);
		// Auditing the action starts
		if (Validator.isNotNull(employeeProfileVO)) {
			WebUtil.createAuditRecord(this.getClass().getName(), Constants.PROMOTIONS_ACTION_TYPE, employeeProfileVO.getEmpName());
		}
		// Auditing the action ends
		actionResponse.setWindowState(WindowState.MAXIMIZED);
		actionResponse.setRenderParameter(Constants.MY_ACTION, Constants.PROMOTIONS_EMPLOYEE_PROFILE_PAGE);

	}

	/**
	 * Navigate to Employee Profile Page.
	 * @return navigation string {employeeDirectorySearchDetail}
	 */
	@RenderMapping(params = "myaction=promotionsEmployeeProfilePage")
	public String showSearchDetail(RenderRequest request, RenderResponse response, Model model) {

		LoggerUtil.debugLogger(logger, "Inside the method showSearchDetail() of PromotionsController ::");
		return Constants.EMPLOYEE_PROFILE_PROMOTIONS;
	}

	/**
	 * To open the attached document for promotions.
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 * @throws WindowStateException
	 */
	@ActionMapping(params = "myaction=getAttachment")
	public void getAttachment(ActionRequest actionRequest, ActionResponse actionResponse, Model model) throws WindowStateException {

		LoggerUtil.debugLogger(logger, "Inside the method getAttachment() of PromotionsController ::");
		String employeeId = actionRequest.getParameter(Constants.EMP_ID);

		if (actionRequest.getParameter(Constants.DOCUMENT_PATH) != null && !actionRequest.getParameter(Constants.DOCUMENT_PATH).isEmpty()) {

			String documentPath = WebUtil.getPortalURL(actionRequest) + actionRequest.getParameter(Constants.DOCUMENT_PATH);
			model.addAttribute(Constants.DOCUMENT_PATH, documentPath);
			// Auditing the action starts
			if (Validator.isNotNull(documentPath)) {
				WebUtil.createAuditRecord(this.getClass().getName(), Constants.PROMOTIONS_ACTION_TYPE, documentPath);
			}
			// Auditing the action ends
		}
		else if (employeeId != null && !employeeId.isEmpty()) {

			for (PromotionsVO promotions : allPromotionsList) {

				if (promotions.getEmployeeId().equalsIgnoreCase(employeeId)) {
					model.addAttribute(Constants.WEBCONTENT_DOCUMENT, promotions.getDocument());
					// Auditing the action starts
					if (Validator.isNotNull(promotions)) {
							WebUtil.createAuditRecord(this.getClass().getName(), Constants.PROMOTIONS_ACTION_TYPE, promotions.getFirstName() + " " +
								promotions.getLastName() + " " + Constants.PROMOTIONS_DETAILS);
					}
					// Auditing the action ends
				}
			}
		}
		model.addAttribute(Constants.ATTACHMENTH_HEADER, Constants.PROMOTIONS_HEADER);
		actionResponse.setRenderParameter(Constants.MY_ACTION, Constants.ATTACHMENT_PAGE);

	}

	/**
	 * Navigate to Attachment Page.
	 * @return
	 */
	@RenderMapping(params = "myaction=attachmentPage")
	public String showAttachment() {

		LoggerUtil.debugLogger(logger, "Inside the method showAttachment() of PromotionsController ::");
		return Constants.ATTACHMENT;
	}

	/**
	 * Get Employee details based on the EmployeeId by calling the web service
	 * method employeesByID().
	 * @param employeeID
	 * @param model
	 * @return EmployeeProfileVO
	 */
	public EmployeeProfileVO getEmployeeByID(String employeeID, Model model) {

		LoggerUtil.debugLogger(logger, "Start of  getEmployeeByID() method of PromotionsController::");
		GetEmployeesByIDRequest employeesByIDRequest = WS_CLIENT_FACTORY.createGetEmployeesByIDRequest();
		EMPByUserId userId = new EMPByUserId();
		DirectReportVO directReportVO = null;
		List<DirectReportVO> directReportVoList = new ArrayList<DirectReportVO>();
		EmployeeProfileVO employeeProfileVO = null;
		List<com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee> employeeDetailsList = null;
		userId.setStrADUserID(employeeID);
		employeesByIDRequest.setEMPByUserId(userId);
		try {
			WebUtil.trust();
			Object responseObject = webServiceTemplate.marshalSendAndReceive(employeesByIDRequest);

			if (responseObject instanceof GetEmployeesByIDResponse) {

				GetEmployeesByIDResponse employeeByIdResponse = (GetEmployeesByIDResponse) responseObject;
				if (employeeByIdResponse != null && employeeByIdResponse.getEmployees() != null &&
					employeeByIdResponse.getEmployees().getEmployee() != null) {

					employeeDetailsList = employeeByIdResponse.getEmployees().getEmployee();
					if (employeeDetailsList != null && !employeeDetailsList.isEmpty() && employeeDetailsList.size() > 0) {
						for (com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee employee : employeeDetailsList) {

							if (employeeID != null && !employeeID.isEmpty() && employee.getUSERID() != null && !employee.getUSERID().isEmpty() &&
								employeeID.equalsIgnoreCase(employee.getUSERID())) {

								employeeProfileVO = new EmployeeProfileVO();
								employeeProfileVO.setEmpName(employee.getFIRSTNAME() + Constants.SPACE + employee.getLASTNAME());
								employeeProfileVO.setDesignation(employee.getTITLE());
								employeeProfileVO.setEmpPhone(employee.getPHONE());
								employeeProfileVO.setFax(employee.getFAX());
								employeeProfileVO.setEmailAddress(employee.getEMAIL());
								employeeProfileVO.setOffice(employee.getOFFICENAME());
								employeeProfileVO.setBuildingName(employee.getBUILDINGNAME());
								employeeProfileVO.setAddress1(employee.getADDRESS());
								employeeProfileVO.setAddress2(employee.getADDRESS2());
								employeeProfileVO.setAddress3(employee.getADDRESS3());
								employeeProfileVO.setFloor(employee.getFLOOR());
								employeeProfileVO.setCity(employee.getCITY());
								employeeProfileVO.setCountry(employee.getCOUNTRY());
								employeeProfileVO.setZip(employee.getZIP());
								employeeProfileVO.setEmpDepartment(employee.getDEPARTMENT());
								employeeProfileVO.setEmpGeography(employee.getGEOGRAPHY());

								if (employee.getMANAGER() != null) {

									employeeProfileVO.setManagerFirstName(employee.getMANAGER().getFIRSTNAME());
									employeeProfileVO.setManagerLastname(employee.getMANAGER().getLASTNAME());
									employeeProfileVO.setManagerId(employee.getMANAGER().getUSERID());
								}
								if (employee.getASSISTANT() != null) {

									employeeProfileVO.setAssistantFirstName(employee.getASSISTANT().getFIRSTNAME());
									employeeProfileVO.setAssistantLastName(employee.getASSISTANT().getLASTNAME());
									employeeProfileVO.setAssistantId(employee.getASSISTANT().getUSERID());
								}

								if (employee.getDIRECTREPORTS() != null && employee.getDIRECTREPORTS().getDIRECTREPORT() != null) {

									List<com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT> directReportsList =
										employee.getDIRECTREPORTS().getDIRECTREPORT();

									for (com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT directReport : directReportsList) {
										directReportVO = new DirectReportVO();
										directReportVO.setDirectReportFirstName(directReport.getFIRSTNAME());
										directReportVO.setDirectReportLastName(directReport.getLASTNAME());
										directReportVO.setDirectReportId(directReport.getUSERID());
										directReportVoList.add(directReportVO);
									}
									employeeProfileVO.setDirectReports(directReportVoList);
								}
							}
						}
					}
				}
			}
			else if (responseObject instanceof GetEmployeesByIDFault) {

				GetEmployeesByIDFault employeeFault = (GetEmployeesByIDFault) responseObject;
				model.addAttribute(Constants.FAULT_MESSAGE, Constants.ERR_WEBSERVICE_PROCESSING);
				LoggerUtil.debugLogger(logger, employeeFault.getErrCode());

			}
		}
		catch (SoapFaultClientException sfce) {

			LoggerUtil.errorLogger(logger, sfce.getMessage(), sfce);
			model.addAttribute(Constants.FAULT_MESSAGE, Constants.ERR_WEBSERVICE_PROCESSING);
		}
		catch (Exception ex) {

			LoggerUtil.errorLogger(logger, ex.getMessage(), ex);
			model.addAttribute(Constants.FAULT_MESSAGE, Constants.ERR_WEBSERVICE_PROCESSING);
		}
		LoggerUtil.debugLogger(logger, "End of  getEmployeeByID() method of PromotionsController::");
		return employeeProfileVO;
	}

}
