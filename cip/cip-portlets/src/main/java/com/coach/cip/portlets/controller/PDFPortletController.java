package com.coach.cip.portlets.controller;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.WindowStateException;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.coach.cip.util.WebUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ResourceConstants;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;

/**
 * The Class PDFPortletController, serves the request related to the Assets
 * operations like rendering the page, generating events to open the Assets (
 * Webcontent, PDF, Doc files etc).
 * 
 * @author GalaxE.
 */

@Controller("pdfPortletController")
@RequestMapping(value = "VIEW")
public class PDFPortletController extends AbstractBaseController {

	/** The Constant LOG. */
	private static final Log logger = LogFactoryUtil
			.getLog(PDFPortletController.class);

	/**
	 * Default Render Mapping Method. If Specific Render mapping is not found,
	 * this mapping is invoked.
	 * 
	 * @param renderResponse
	 * @return
	 */
	@RenderMapping
	public String showPDFPortlet(RenderResponse renderResponse) {
		return Constants.PDFPORTLET;
	}

	/**
	 * Event mapping to process SideNavigation event in
	 * PDFPortletController.
	 * 
	 * @param eventRequest
	 * @param eventResponse
	 */
	@SuppressWarnings("rawtypes")
	@EventMapping(value = "{http://coach.com/events}ipc.fileURL")
	public void processSideNavigationEvent(EventRequest eventRequest,
			EventResponse eventResponse) {
		LoggerUtil.debugLogger(logger, "Start of processSideNavigationEvent() method of PDF Portlet Controller::");
		Event event = eventRequest.getEvent();
		HashMap fileMap = (HashMap) event.getValue();
		eventResponse.setRenderParameter("title", fileMap.get("fileTitle")
				.toString());
		eventResponse.setRenderParameter("fileURL", fileMap.get("fileURL")
				.toString());
		eventResponse.setRenderParameter("fileType", fileMap.get("fileType")
				.toString());
		eventResponse.setRenderParameter("classPK", fileMap.get("classPK")
				.toString());
		eventResponse.setRenderParameter("typeOfLink", fileMap.get("typeOfLink")
				.toString());
		eventResponse.setRenderParameter("mimeType", fileMap.get("mimeType")
				.toString());
		eventResponse.setRenderParameter("backButton", fileMap
				.get("backButton").toString());
		if("TRANSMITTAL".equalsIgnoreCase(fileMap.get("typeOfLink").toString())){
			eventResponse.setRenderParameter("myaction", "renderTransmittalFullDetail");
		}
		LoggerUtil.debugLogger(logger, "End of processSideNavigationEvent() method of PDF Portlet Controller::");
	}

	/**
	 * Event mapping to process SideRelatedLinksController event in
	 * PDFPortletController.
	 * 
	 * @param request
	 * @param response
	 */
	@SuppressWarnings("unchecked")
	@EventMapping(value = "{http://coach.com/events}ipc.sideRelatedLinksFileURL")
	public void processSideRelatedLinksEvent(EventRequest request,
			EventResponse response) {
		LoggerUtil.debugLogger(logger, "Start of processSideRelatedLinksEvent() method of PDF Portlet Controller::");
		Event event = request.getEvent();
		HashMap<String, String> sideRelatedLinksMap = (HashMap<String, String>) event
				.getValue();
		response.setRenderParameter("title",
				sideRelatedLinksMap.get("fileTitle").toString());
		response.setRenderParameter("fileURL",
				sideRelatedLinksMap.get("fileURL").toString());
		response.setRenderParameter("fileType", sideRelatedLinksMap.get("fileType")
				.toString());
		response.setRenderParameter("classPK", sideRelatedLinksMap.get("classPK")
				.toString());
		response.setRenderParameter("typeOfLink", sideRelatedLinksMap.get("typeOfLink")
				.toString());
		response.setRenderParameter("mimeType", sideRelatedLinksMap.get("mimeType")
				.toString());
		
		LoggerUtil.debugLogger(logger, "End of processSideRelatedLinksEvent() method of PDF Portlet Controller::");

	}
	
	
	/*Transmittal methods */

	@RenderMapping(params = "myaction=renderTransmittalFullDetail")
	public String showTransmittalFullDetail(RenderRequest renderRequest, RenderResponse renderResponse, Model model) throws NumberFormatException, PortalException, SystemException {

		model.addAttribute("journalArticleId", renderRequest.getParameter("fileURL"));
		String articleTitle = renderRequest.getParameter("title");
		renderResponse.setTitle(articleTitle.toUpperCase());
		LoggerUtil.debugLogger(logger, "End of showTransmittalFullDetail() method of PDFPortletController::");
		return Constants.PDFPORTLET;
		//return Constants.TRANSMITTAL_FULL_DETAIL_JSP;
	}



	/**
	 * Action Mapping Method which sets the required parameters in response
	 * object to display the selected document in 70 % frame and triggers
	 * "http://coach.com/events","ipc.highlighterMap" type event to highlight
	 * the related Transmittal journal article in PDF Portlet.
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 * @throws WindowStateException
	 * @throws NumberFormatException
	 * @throws PortalException
	 * @throws SystemException
	 */
	@ActionMapping(params = "action=getTransmittalAttachment")
	public void getTransmittalAttachment(ActionRequest actionRequest, ActionResponse actionResponse, Model model) throws WindowStateException, NumberFormatException, PortalException, SystemException {

		LoggerUtil.debugLogger(logger, "Start of getTransmittalAttachment() method of PDFPortletController::");
		String redirect = actionRequest.getParameter("redirect");
		String journalArticleId = actionRequest.getParameter("journalArticleId");
		LoggerUtil.debugLogger(logger, "journalArticleId from request is" + journalArticleId);
		String documentPath = actionRequest.getParameter("documentPath");
		LoggerUtil.debugLogger(logger, "DocumentPath===========>" + documentPath);
		documentPath = documentPath.replace("+", " ");
		String portletTitle = actionRequest.getParameter("portletTitle");
		LoggerUtil.debugLogger(logger, "portletTitle===========>" + portletTitle);
		try{
			DLFileEntry dlFileEntry = DLFileEntryLocalServiceUtil.getDLFileEntryByUuidAndGroupId(documentPath.substring(documentPath.lastIndexOf("/")+1), WebUtil.getThemeDisplay(actionRequest).getScopeGroupId());
			model.addAttribute("dlFileEntry", dlFileEntry);
		}catch(Exception e){
			LoggerUtil.errorLogger(logger, "Fetching File Entry Exception===========>",e);
		}
		model.addAttribute("documentPath", documentPath);
		//actionResponse.setWindowState(WindowState.MAXIMIZED);
		actionResponse.setRenderParameter("action", "renderAttachement");
		actionResponse.setRenderParameter("redirect", redirect);
		//actionResponse.setRenderParameter("portletTitle", portletTitle);
		actionResponse.setRenderParameter("title", portletTitle);
		actionResponse.setRenderParameter("typeOfLink", "TRANSMITTAL_ATTACHMENT");
		actionResponse.setRenderParameter("fileURL", documentPath);
		
		LoggerUtil.debugLogger(logger, "End of getTransmittalAttachment() method of PDFPortletController::");
	}

	/**
	 * Render Mapping Method which renders the document in 70% frame.
	 * @param renderRequest
	 * @param renderResponse
	 */
	@RenderMapping(params = "action=renderAttachement")
	public String renderAttachement(RenderRequest renderRequest, RenderResponse renderResponse) {

		LoggerUtil.debugLogger(logger, "Start of renderAttachement() method of PDFPortletController::");
		String portletTitle = ParamUtil.getString(renderRequest, "title");
		if (Validator.isNotNull(portletTitle))
			renderResponse.setTitle(portletTitle);
		LoggerUtil.debugLogger(logger, "End of renderAttachement() method of PDFPortletController::");
		//return Constants.TRANSMITTAL_ATTACHMENT_JSP;
		return Constants.PDFPORTLET;
	}
	
	
	@ResourceMapping("checkExternalAccess")
	public ModelAndView checkExternalAccess(ResourceRequest request, ResourceResponse response) throws IOException, SystemException {
		LoggerUtil.debugLogger(logger,"Inside populateArticleIds method in PDFPortletController");
		response.setContentType("text/html; charset=utf-8");
		Map<String, String> modelMap = new HashMap<String, String>();
		String classpk = request.getParameter("classpk");
		String filetype = request.getParameter("filetype");
		String fileURL = "";
		//String internalAccessHeaderValue = WebUtil.isExternalNetwork(request);
		if(WebUtil.isExternalNetwork(request)){
		  try{ 	
			boolean assetExternalAccess = WebUtil.hasAssetExternalAccess(WebUtil.getThemeDisplay(request).getCompanyId(),filetype,
					ResourceConstants.SCOPE_INDIVIDUAL,classpk,0,ActionKeys.VIEW);
			if(!assetExternalAccess){
				fileURL = Constants.NO_USER_PRIVILEGE;
			}
		  }catch(Exception e) {
			  LoggerUtil.errorLogger(logger, "PDFPortlet jsp - Internal Access only Exception ",e);
			}
		} 		
		
		modelMap.put("portletInstanceId", WebUtil.getPortletInstanceId(request, "column-2"));
			
		
		modelMap.put("fileURL", fileURL);
		return new ModelAndView("pdfPortletResourceMapping", modelMap);
	}
	
	
	@ResourceMapping("populateSideNavigation")
	public ModelAndView populateSideNavigation(ResourceRequest request, ResourceResponse response) throws IOException, SystemException 
	{
		LoggerUtil.debugLogger(logger, "Start of processSideNavigationEvent() method of PDF Portlet Controller::");
		response.setContentType("text/html; charset=utf-8");
		String sideNavigationPortletNameSpace = StringPool.UNDERLINE + WebUtil.getPortletInstanceId(request, "column-1").split("INSTANCE")[0];
		ThemeDisplay themeDisplay= WebUtil.getThemeDisplay(request);
		
		String fileURL = request.getParameter(sideNavigationPortletNameSpace + "fileURL");
		String classPK = request.getParameter(sideNavigationPortletNameSpace + "classPK");
		String fileType = request.getParameter(sideNavigationPortletNameSpace + "fileType");
		String typeOfLink = request.getParameter(sideNavigationPortletNameSpace + "typeOfLink");
		String title = request.getParameter(sideNavigationPortletNameSpace+"fileTitle");
		//ThemeDisplay themeDisplay = WebUtil.getThemeDisplay( request);
		
		/* Checking the Asset accesibility oustide the network : Start */
		if(WebUtil.isExternalNetwork(request))
		{	
			  try{ 	
				boolean assetExternalAccess = WebUtil.hasAssetExternalAccess(themeDisplay.getCompanyId(),fileType,
						ResourceConstants.SCOPE_INDIVIDUAL,classPK,0,ActionKeys.VIEW);
				if(!assetExternalAccess){
					fileURL = Constants.NO_USER_PRIVILEGE;
				}
			  }catch(Exception e) {
				  LoggerUtil.errorLogger(logger, "PDFPortlet jsp - Internal Access only Exception ",e);
				}
		} 
		/* Checking the Asset accesibility oustide the network : End */
		
		Map<String, String> modelMap = new HashMap<String, String>();
		modelMap.put("title",title );
		
		modelMap.put("fileURL", fileURL);
		modelMap.put("fileType", request.getParameter(sideNavigationPortletNameSpace+"fileType"));
		modelMap.put("classPK", request.getParameter(sideNavigationPortletNameSpace+"classPK"));
		modelMap.put("typeOfLink",typeOfLink );
		modelMap.put("mimeType", request.getParameter(sideNavigationPortletNameSpace+"mimeType"));
		modelMap.put("backButton", request.getParameter(sideNavigationPortletNameSpace+"backButton"));

		if("TRANSMITTAL".equalsIgnoreCase(typeOfLink)){
			String url = themeDisplay.getPortalURL() + themeDisplay.getPathFriendlyURLPrivateGroup() +
			themeDisplay.getParentGroup().getFriendlyURL() +
			themeDisplay.getLayout().getFriendlyURL()+"?"+request.getParameter("redirect");
			
			url = URLEncoder.encode(url);
			modelMap.put("journalArticleId", fileURL);
			modelMap.put("redirect", url);
			 return new ModelAndView("populateTransmittalDetail", modelMap); 
		}else{
		
		  return new ModelAndView("populateSideNavigationLinks", modelMap);
		} 
	}
	
	/**
	 * serves as a resource to construct pdf portlet.
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 * @throws SystemException
	 */
	@ResourceMapping("populateSideRelatedLinks")
	public ModelAndView populateSideRelatedLinks(ResourceRequest request, ResourceResponse response) throws IOException, SystemException 
	{
		LoggerUtil.debugLogger(logger, "Start of populateSideRelatedLinks() method of PDF Portlet Controller::");
		response.setContentType("text/html; charset=utf-8");
		String portletInstance = WebUtil.getPortletInstanceId(request, "column-1");
		String sideRelatedLinskPortletNameSpace = StringPool.UNDERLINE + portletInstance.split(",")[1].split("INSTANCE")[0];
		String fileURL = request.getParameter(sideRelatedLinskPortletNameSpace + "fileURL");
		String classPK = request.getParameter(sideRelatedLinskPortletNameSpace + "classPK");
		String fileType = request.getParameter(sideRelatedLinskPortletNameSpace + "fileType");
		ThemeDisplay themeDisplay = WebUtil.getThemeDisplay( request);
		
		/* Checking the Asset accesibility oustide the network : Start */
		if(WebUtil.isExternalNetwork(request))
		{	
			  try{ 	
				boolean assetExternalAccess = WebUtil.hasAssetExternalAccess(themeDisplay.getCompanyId(),fileType,
						ResourceConstants.SCOPE_INDIVIDUAL,classPK,0,ActionKeys.VIEW);
				if(!assetExternalAccess){
					fileURL = Constants.NO_USER_PRIVILEGE;
				}
			  }catch(Exception e) {
				  LoggerUtil.errorLogger(logger, "PDFPortlet jsp - Internal Access only Exception ",e);
				}
		} 
		/* Checking the Asset accesibility oustide the network : End */
		
		Map<String, String> modelMap = new HashMap<String, String>();
		modelMap.put("title", request.getParameter(sideRelatedLinskPortletNameSpace+"fileTitle"));
		modelMap.put("fileURL", fileURL);
		modelMap.put("fileType", request.getParameter(sideRelatedLinskPortletNameSpace+"fileType"));
		modelMap.put("classPK", request.getParameter(sideRelatedLinskPortletNameSpace+"classPK"));
		modelMap.put("typeOfLink", request.getParameter(sideRelatedLinskPortletNameSpace+"typeOfLink"));
		modelMap.put("mimeType", request.getParameter(sideRelatedLinskPortletNameSpace+"mimeType"));
		modelMap.put("backButton", request.getParameter(sideRelatedLinskPortletNameSpace+"backButton"));
		return new ModelAndView("populateSideRelatedLinks", modelMap);
	}
	

}
