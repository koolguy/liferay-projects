
package com.coach.cip.portlets.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.coach.cip.common.dto.CountryVO;
import com.coach.cip.common.dto.GeographyVO;
import com.coach.cip.common.dto.NotificationForLocationVO;
import com.coach.cip.common.dto.NotificationVO;
import com.coach.cip.common.dto.RoleVO;
import com.coach.cip.common.dto.UserVO;
import com.coach.cip.model.entity.Address;
import com.coach.cip.model.entity.CIGeography;
import com.coach.cip.model.entity.CINotification;
import com.coach.cip.model.entity.CINotificationForLocation;
import com.coach.cip.model.entity.Country;
import com.coach.cip.model.entity.Region;
import com.coach.cip.model.entity.Role;
import com.coach.cip.model.entity.User;
import com.coach.cip.services.LocationService;
import com.coach.cip.services.NotificationService;
import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;

/**
 * Class works as a controller for the notification actions.
 * @author GalaxE.
 */

@Controller("notificationController")
@RequestMapping(value = "VIEW")
public class ManagingNotificationController extends AbstractBaseController{

	/** The Constant LOG. */
	private static final Log logger = LogFactoryUtil.getLog(ManagingNotificationController.class);

	/**
	 * List to hold the Notifications.
	 */
	List<NotificationVO> notificaitonVoList;
	/**
	 * List to hold the Geographies.
	 */
	List<GeographyVO> geographyVOList;
	/**
	 * Hold the selected Personal Message.
	 */
	NotificationVO notification;

	/**
	 * Default Render Mapping Method. If Specific Render mapping is not found,
	 * this mapping is invoked.
	 * @param renderRequest
	 * @param renderResponse
	 * @return view name
	 */
	@RenderMapping
	public String showNotifications(RenderResponse renderResponse) {

		LoggerUtil.debugLogger(logger, "Render Mapping method for Notification form.");
		return "allNotifications";
	}

	/**
	 * Render Mapping Method for Add Notification Form. This Render Mapping
	 * is invoked after deleteNotificaiton() method on click of
	 * "Add Notification" buttons.
	 * @param renderRequest
	 * @return view name
	 */

	@RenderMapping(params = "myaction=addNotificationForm")
	public String showAddNotification(RenderResponse renderResponse) {

		LoggerUtil.debugLogger(logger, "Render Mapping method for Add Notification form.");
		return "addNotification";
	}

	/**
	 * Render Mapping Method for Edit Notification Form. This Render Mapping
	 * is invoked after addNotification() method on click of
	 * "Add Notification" buttons.
	 * @param renderRequest
	 * @return view name
	 */

	@RenderMapping(params = "myaction=editNotificationForm")
	public String showEditNotification() {

		LoggerUtil.debugLogger(logger, "Render Mapping method for Edit Notification form.");
		return "editNotification";
	}

	/**
	 * Model Object for populating Notification form data.
	 * @param portletRequest
	 * @return List<NotificationVO>
	 */

	@ModelAttribute(value = "notificaitonVoList")
	public List<NotificationVO> getNotification(PortletRequest portletRequest) {

		LoggerUtil.debugLogger(logger, "Command Object or Model Object for populating Notification form data.");
		List<CINotification> notificationDoList = null;
		try {
			notificationDoList = (List<CINotification>) serviceLocator.getNotificationService().getAllNotifications();
		}
		catch (Exception e) {
			LoggerUtil.errorLogger(logger, "NotificationController.getNotification()", e);
		}
		if (notificationDoList != null && !notificationDoList.isEmpty() && notificationDoList.size() != 0) {
			notificaitonVoList = convertNotificationDOToVOList(notificationDoList);
		}
		else {
			notificaitonVoList = new ArrayList<NotificationVO>();
		}
		portletRequest.setAttribute("notificaitonVoList", notificaitonVoList);
		return notificaitonVoList;
	}

	/**
	 * Method is used for updating the reference data or model data list with
	 * the updated values (while adding , editing or deleting Notifications.
	 * @param actionRequest
	 * @param model
	 */
	public void updateNotificationModel(ActionRequest actionRequest, Model model) {

		model.addAttribute("notificaitonVoList", getNotification(actionRequest));
	}

	/**
	 * Model Object for populating Geography data for Locations.
	 * @param portletRequest
	 * @return List<GeographyVO>
	 */

	@SuppressWarnings("unchecked")
	@ModelAttribute(value = "geographyList")
	public List<GeographyVO> getAllGeographies(PortletRequest portletRequest) {

		List<CIGeography> geographyDOList = new ArrayList<CIGeography>();
		LocationService<CIGeography> geographyLocationService = serviceLocator.getLocationService();

		geographyVOList = new ArrayList<GeographyVO>();
		try {
			geographyDOList = geographyLocationService.getAllGeography(CIGeography.class);
		}
		catch (Exception e) {
			LoggerUtil.errorLogger(logger, "NotificationController.getAllGeographies()", e);
		}
		if (geographyDOList != null && !geographyDOList.isEmpty() && geographyDOList.size() != 0) {
			geographyVOList = convertGeographyDOToVOList(geographyDOList);
		}
		portletRequest.setAttribute("geographyVOList", geographyVOList);
		return geographyVOList;
	}

	/**
	 * Action Mapping Method for Add Notification Form. This Action Mapping
	 * method is invoked in Add phase on click of "Submit" or "Cancel" button.
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 */
	@ActionMapping(params = "myaction=addNotification")
	public void addNotification(ActionRequest actionRequest, ActionResponse actionResponse, Model model) {

		LoggerUtil.debugLogger(logger, "Start of addNotification() method of Notification Controller::");
		if (actionRequest.getParameter("addNotification").equals("Submit")) {
			LoggerUtil.debugLogger(logger, "Adding Notification.");
			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			Set<Role> roleDOSet = null;
			Set<User> userDOSet = null;
			User user = null;
			CINotification ciNotification = null;
			NotificationVO notificationVo = new NotificationVO();
			String locationType = actionRequest.getParameter("locationType");
			String notificationType = actionRequest.getParameter("notificationType");
			String displayDate = actionRequest.getParameter("displayDate");
			String expirationDate = actionRequest.getParameter("expirationDate");
			String url = actionRequest.getParameter("notificationUrl");
			notificationVo.setNotificationSubject(actionRequest.getParameter("notificationSubject"));
			notificationVo.setNotificationDescription(actionRequest.getParameter("notificationDescription"));
			notificationVo.setNotificationType(actionRequest.getParameter("notificationType"));
			notificationVo.setCreateDate(new Timestamp(new Date().getTime()));
			notificationVo.setModifiedDate(new Timestamp(new Date().getTime()));
			notificationVo.setDisplayDate(displayDate);
			notificationVo.setNotificationUrl(url);
			notificationVo.setExpirationDate(expirationDate);
			try {
				if (notificationType != null && notificationType.equals("Regular")) {

					if (locationType != null && !locationType.isEmpty() && !locationType.equals("0")) {
						notificationVo.setNotificationForLocations(populatingNotificationForLocationVO(actionRequest));
					}

					String[] roleIds = actionRequest.getParameterValues("roles");
					Role role = null;
					if (roleIds != null) {
						roleDOSet = new HashSet<Role>();
						for (String s1 : roleIds) {
							role = serviceLocator.getNotificationService().getRole(Long.valueOf(s1));
							roleDOSet.add(role);
						}
					}
				}

				String[] userIds = actionRequest.getParameterValues("users");
				User usr = null;
				if (userIds != null) {
					userDOSet = new HashSet<User>();
					for (String u1 : userIds) {
						usr = serviceLocator.getNotificationService().getUser(Long.valueOf(u1));
						userDOSet.add(usr);
					}
				}
				ciNotification = convertNotificationVOToDO(notificationVo);
				user = serviceLocator.getNotificationService().getUser(themeDisplay.getUserId());
				ciNotification.setNotificationForRoles(roleDOSet);
				ciNotification.setNotificationForUsers(userDOSet);
				ciNotification.setUserByModifiedBy(user);
				ciNotification.setUserByCreatedBy(user);
				serviceLocator.getNotificationService().addNotification(ciNotification);
				// model.addAttribute("message",
				// "Succesfully saved the Message");
				SessionMessages.add(actionRequest, Constants.PERSONALMSG_ADMIN_SUCCESS_ADD_SAVE);
				model.addAttribute("notificationDescription", Constants.PERSONALMSG_ADMIN_SUCCESS_ADD_SAVE);
			}
			catch (Exception ex) {
				LoggerUtil.errorLogger(logger, "NotificationController.addNotification()", ex);
				// model.addAttribute("message",
				// "Error while Saving the Message");
				SessionErrors.add(actionRequest, Constants.PERSONALMSG_ADMIN_ERROR_ADD_SAVE);
				model.addAttribute("notificationDescription", Constants.PERSONALMSG_ADMIN_ERROR_ADD_SAVE);
			}
			updateNotificationModel(actionRequest, model);
		}
		else if (actionRequest.getParameter("addNotification").equals("Reset")) {
			actionResponse.setRenderParameter("myaction", "addNotificationForm");
		}
		LoggerUtil.debugLogger(logger, "End of addNotification() method of Personal Message Controller::");
	}

	/**
	 * Action Mapping Method for Edit Notification Form. This Action Mapping
	 * method is invoked on click of "Notification" to edit. Method
	 * populates the Edit form data.
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 */

	@SuppressWarnings("unchecked")
	@ActionMapping(params = "myaction=editNotification")
	public void editNotification(ActionRequest actionRequest, ActionResponse actionResponse, Model model) {

		LoggerUtil.debugLogger(logger, "Start of editNotification() method of Managing Notification Controller::");
		LocationService<Address> addressLocationService = serviceLocator.getLocationService();
		LocationService<Region> stateLocationService = serviceLocator.getLocationService();
		LocationService<CIGeography> countryLocationService = serviceLocator.getLocationService();
		LocationService<CIGeography> geographyLocationService = serviceLocator.getLocationService();

		notification = searchList(actionRequest.getParameter("notificationId"));
		for (NotificationForLocationVO notificationForLocationVo : notification.getNotificationForLocations()) {
			model.addAttribute("notificationForLocationId", notificationForLocationVo.getCinflId());
			model.addAttribute("locationType", notificationForLocationVo.getLocationType());

			if ((notificationForLocationVo.getLocationType().equals("Building")) && notificationForLocationVo.getState() != null &&
				notificationForLocationVo.getCity() != null && notificationForLocationVo.getOffice() != null) {
				String selectedOffice = notificationForLocationVo.getOffice();
				List<String> buildingsList =
					addressLocationService.getAllDistinctAddressLocation(Address.class, "street2", "street3", notificationForLocationVo.getOffice());
				List<String> officesList =
					addressLocationService.getAllDistinctAddressLocation(Address.class, "street3", "city", notificationForLocationVo.getCity());
				List<String> cityList =
					addressLocationService.getAllDistinctAddressLocation(Address.class, "city", "regionId", notificationForLocationVo.getState());
				List<Region> selectedCountryStateList =
					stateLocationService.getAllLocation(Region.class, "regionId", Long.valueOf(notificationForLocationVo.getState()));
				List<Region> stateList =
					stateLocationService.getAllLocation(Region.class, "countryId", selectedCountryStateList.get(0).getCountryId());
				List<Object> geographyCountryId =
					geographyLocationService.getGeographyCountry("countryId", selectedCountryStateList.get(0).getCountryId());
				Object[] geographyCountry = (Object[]) geographyCountryId.get(0);
				String geographyId = geographyCountry[0].toString();
				String countryId = geographyCountry[1].toString();

				List<CIGeography> countryList = countryLocationService.getAllLocation(CIGeography.class, "geographyId", Long.valueOf(geographyId));

				model.addAttribute("selectedGeographyId", geographyId);
				model.addAttribute("countryList", countryList.get(0).getCiGeographycountries());
				model.addAttribute("selectedCountryId", selectedCountryStateList.get(0).getCountryId());
				model.addAttribute("stateList", stateList);
				model.addAttribute("selectedStateId", selectedCountryStateList.get(0).getRegionId());
				model.addAttribute("cityList", cityList);
				model.addAttribute("officesList", officesList);
				model.addAttribute("selectedOffice", selectedOffice);
				model.addAttribute("buildingsList", buildingsList);

			}
			else if ((notificationForLocationVo.getLocationType().equals("Office")) && notificationForLocationVo.getState() != null &&
				notificationForLocationVo.getCity() != null) {

				List<String> officesList =
					addressLocationService.getAllDistinctAddressLocation(Address.class, "street3", "city", notificationForLocationVo.getCity());
				List<String> cityList =
					addressLocationService.getAllDistinctAddressLocation(Address.class, "city", "regionId", notificationForLocationVo.getState());
				List<Region> selectedCountryStateList =
					stateLocationService.getAllLocation(Region.class, "regionId", Long.valueOf(notificationForLocationVo.getState()));
				List<Region> stateList =
					stateLocationService.getAllLocation(Region.class, "countryId", selectedCountryStateList.get(0).getCountryId());
				List<Object> geographyCountryId =
					geographyLocationService.getGeographyCountry("countryId", selectedCountryStateList.get(0).getCountryId());
				Object[] geographyCountry = (Object[]) geographyCountryId.get(0);
				String geographyId = geographyCountry[0].toString();
				String countryId = geographyCountry[1].toString();

				List<CIGeography> countryList = countryLocationService.getAllLocation(CIGeography.class, "geographyId", Long.valueOf(geographyId));

				model.addAttribute("selectedGeographyId", geographyId);
				model.addAttribute("countryList", countryList.get(0).getCiGeographycountries());
				model.addAttribute("selectedCountryId", selectedCountryStateList.get(0).getCountryId());
				model.addAttribute("stateList", stateList);
				model.addAttribute("selectedStateId", selectedCountryStateList.get(0).getRegionId());
				model.addAttribute("cityList", cityList);
				model.addAttribute("officesList", officesList);
				model.addAttribute("selectedOffice", notificationForLocationVo.getLocationName());

			}
			else if ((notificationForLocationVo.getLocationType().equals("City")) && notificationForLocationVo.getState() != null) {

				List<String> cityList =
					addressLocationService.getAllDistinctAddressLocation(Address.class, "city", "regionId", notificationForLocationVo.getState());
				List<Region> selectedCountryStateList =
					stateLocationService.getAllLocation(Region.class, "regionId", Long.valueOf(notificationForLocationVo.getState()));
				List<Region> stateList =
					stateLocationService.getAllLocation(Region.class, "countryId", selectedCountryStateList.get(0).getCountryId());
				List<Object> geographyCountryId =
					geographyLocationService.getGeographyCountry("countryId", selectedCountryStateList.get(0).getCountryId());
				Object[] geographyCountry = (Object[]) geographyCountryId.get(0);
				String geographyId = geographyCountry[0].toString();
				String countryId = geographyCountry[1].toString();

				List<CIGeography> countryList = countryLocationService.getAllLocation(CIGeography.class, "geographyId", Long.valueOf(geographyId));
                if(Validator.isNull(notificationForLocationVo.getCity())){
                	notificationForLocationVo.setCity(notificationForLocationVo.getLocationName());
                }
				model.addAttribute("selectedGeographyId", geographyId);
				model.addAttribute("countryList", countryList.get(0).getCiGeographycountries());
				model.addAttribute("selectedCountryId", selectedCountryStateList.get(0).getCountryId());
				model.addAttribute("stateList", stateList);
				model.addAttribute("selectedStateId", selectedCountryStateList.get(0).getRegionId());
				model.addAttribute("cityList", cityList);

			}
			else if (notificationForLocationVo.getLocationType().equals("Region")) {

				List<Region> selectedCountryStateList =
					stateLocationService.getAllLocation(Region.class, "regionId", Long.valueOf(notificationForLocationVo.getLocationName()));
				List<Region> stateList =
					stateLocationService.getAllLocation(Region.class, "countryId", selectedCountryStateList.get(0).getCountryId());
				List<Object> geographyCountryId =
					geographyLocationService.getGeographyCountry("countryId", selectedCountryStateList.get(0).getCountryId());
				Object[] geographyCountry = (Object[]) geographyCountryId.get(0);
				String geographyId = geographyCountry[0].toString();
				String countryId = geographyCountry[1].toString();

				List<CIGeography> countryList = countryLocationService.getAllLocation(CIGeography.class, "geographyId", Long.valueOf(geographyId));

				model.addAttribute("selectedGeographyId", geographyId);
				model.addAttribute("countryList", countryList.get(0).getCiGeographycountries());
				model.addAttribute("selectedCountryId", selectedCountryStateList.get(0).getCountryId());
				model.addAttribute("stateList", stateList);
				model.addAttribute("selectedStateId", selectedCountryStateList.get(0).getRegionId());
			}
			else if (notificationForLocationVo.getLocationType().equals("Country")) {

				List<Object> geographyCountryId =
					geographyLocationService.getGeographyCountry("countryId", Long.valueOf(notificationForLocationVo.getLocationName()));
				Object[] geographyCountry = (Object[]) geographyCountryId.get(0);
				String geographyId = geographyCountry[0].toString();
				String countryId = geographyCountry[1].toString();

				List<CIGeography> countryList = countryLocationService.getAllLocation(CIGeography.class, "geographyId", Long.valueOf(geographyId));

				model.addAttribute("selectedGeographyId", geographyId);
				model.addAttribute("countryList", countryList.get(0).getCiGeographycountries());
				model.addAttribute("selectedCountryId", notificationForLocationVo.getLocationName());
			}
			else if (notificationForLocationVo.getLocationType().equals("Geography")) {

				model.addAttribute("selectedGeographyId", notificationForLocationVo.getLocationName());
			}
		}
		model.addAttribute("notification", notification);
		LoggerUtil.debugLogger(logger, "End of editNotification() method of Managing Notification Controller::");
		actionResponse.setRenderParameter("myaction", "editNotificationForm");
	}

	/**
	 * Action Mapping Method for Saving the edited Notification. This Action
	 * Mapping method is invoked on click of "Submit" to save the changes to the
	 * Message or "Cancel" to navigate to Notification.
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 */

	@ActionMapping(params = "myaction=updateNotification")
	public void updateNotification(ActionRequest actionRequest, ActionResponse actionResponse, Model model) {

		LoggerUtil.debugLogger(logger, "Start of updateNotification() method of Managing Notification Controller::");
		if (actionRequest.getParameter("editNotification").equals("Submit")) {

			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			Set<Role> roleDOSet = null;
			Set<User> userDOSet = null;
			Set<CINotificationForLocation> notificationForLocation = new HashSet<CINotificationForLocation>();
			String displayDate = actionRequest.getParameter("displayDate");
			String expirationDate = actionRequest.getParameter("expirationDate");
			String locationType = actionRequest.getParameter("locationType");
			String notificationType = actionRequest.getParameter("notificationType");
			String url = actionRequest.getParameter("notificationUrl");

			try {
				if (notificationType != null && notificationType.equals("Regular")) {

					if (locationType != null && !locationType.isEmpty() && !locationType.equals("0")) {
						notificationForLocation = convertNotificationForLocationVOToDO(populatingNotificationForLocationVO(actionRequest));
					}
					String[] roleIds = actionRequest.getParameterValues("roles");
					Role role = null;
					if (roleIds != null) {
						roleDOSet = new HashSet<Role>();
						for (String s1 : roleIds) {
							role = serviceLocator.getNotificationService().getRole(Long.valueOf(s1));
							roleDOSet.add(role);
						}
					}
				}
				
				String[] userIds = actionRequest.getParameterValues("users");
				User user = null;
				if (userIds != null) {
					userDOSet = new HashSet<User>();
					for (String u1 : userIds) {
						user = serviceLocator.getNotificationService().getUser(Long.valueOf(u1));
						userDOSet.add(user);
					}
				}

				CINotification ciNotification = serviceLocator.getNotificationService().getNotification(notification.getNotificationId());
				ciNotification.setDisplayDate(dateFormatConversion(displayDate));
				ciNotification.setUrl(url);
				ciNotification.setExpirationDate(dateFormatConversion(expirationDate));
				ciNotification.setNotificationSubject(actionRequest.getParameter("notificationSubject"));
				ciNotification.setNotificationDescription(actionRequest.getParameter("notificationDescription"));
				ciNotification.setNotificationType(notificationType);
				ciNotification.setModifiedDate(new Timestamp(new Date().getTime()));
				ciNotification.setNotificationForRoles(roleDOSet);
				ciNotification.setNotificationForUsers(userDOSet);
				ciNotification.setUserByModifiedBy(serviceLocator.getNotificationService().getUser(themeDisplay.getUserId()));
				ciNotification.setUserByCreatedBy(serviceLocator.getNotificationService().getUser(
					notification.getUserByCreatedBy().getUserId()));
				if (ciNotification.getNotificationType() != null &&
					(!ciNotification.getNotificationType().equals("Regular") || locationType.isEmpty() || locationType.equals("0")) &&
					ciNotification.getNotificationForLocation().size() > 0) {
					ciNotification.getNotificationForLocation().clear();
				}
				else {
					ciNotification.setNotificationForLocation(notificationForLocation);

					/*
					 * Added to set the messageId value to ci_notificaitonforlocation
					 * table as we are unable to set through @ManytoOne
					 * annotations.
					 */

					for (CINotificationForLocation ciNotificationForLocation : ciNotification.getNotificationForLocation()) {
						ciNotificationForLocation.setNotification(ciNotification);
					}
				}
				serviceLocator.getNotificationService().updateNotification(ciNotification);
				SessionMessages.add(actionRequest, Constants.PERSONALMSG_ADMIN_SUCCESS_UPDATE);
				model.addAttribute("notificationDescription", Constants.PERSONALMSG_ADMIN_SUCCESS_UPDATE);
			}
			catch (Exception ex) {
				LoggerUtil.errorLogger(logger, "NotificationController.updateNotification():::", ex);
				SessionErrors.add(actionRequest, Constants.PERSONALMSG_ADMIN_ERROR_UPDATE);
				model.addAttribute("notificationDescription", Constants.PERSONALMSG_ADMIN_ERROR_UPDATE);
			}
			updateNotificationModel(actionRequest, model);
		}
		LoggerUtil.debugLogger(logger, "End of updateNotification() method of Managing Notification Controller::");
	}

	/**
	 * Action Mapping Method for Delete Notification Form. This Action
	 * Mapping method is invoked in Delete phase on click of "Notification" or
	 * "Add Notification" button.
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 */
	@ActionMapping(params = "myaction=deletNotificationForm")
	public void deleteNotification(ActionRequest actionRequest, ActionResponse actionResponse, Model model) {

		LoggerUtil.debugLogger(logger, "Start of deleteNotification() method of Managing NOtification Controller::");
		if (actionRequest.getParameter("notificationOperation").equals("Add Notifications")) {
			LoggerUtil.debugLogger(logger, "Redirecting to Add Notifications Form.");
			actionResponse.setRenderParameter("myaction", "addNotificationForm");
		}
		else if (actionRequest.getParameter("notificationOperation").equals("Delete Notifications")) {
			NotificationService notificationService = serviceLocator.getNotificationService();
			String msgId[] = actionRequest.getParameterValues("rowIds");
			if (msgId != null) {
				for (String messageId : msgId) {
					try {
						notificationService.deleteNotification(Long.valueOf(messageId));
						SessionMessages.add(actionRequest, Constants.PERSONALMSG_ADMIN_SUCCESS_DELETE);
						model.addAttribute("notificationDescription", Constants.PERSONALMSG_ADMIN_SUCCESS_DELETE);
					}
					catch (Exception e) {
						LoggerUtil.errorLogger(logger, "NotificationController.deleteNotification():::", e);
						SessionErrors.add(actionRequest, Constants.PERSONALMSG_ADMIN_ERROR_DELETE);
						model.addAttribute("notificationDescription", Constants.PERSONALMSG_ADMIN_ERROR_DELETE);
					}
				}
				updateNotificationModel(actionRequest, model);
			}
			else {
				model.addAttribute("notificationDescription", "Select the Notification(s) to delete");
			}
		}
		LoggerUtil.debugLogger(logger, "End of deleteNotification() method of Managing Notification Controller::");
	}

	/**
	 * Helper method to convert String Date to Timestamp format.
	 * @param stringDate
	 * @return Timestamp
	 */
	public Timestamp dateFormatConversion(String stringDate) {

		Date date = null;

		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

		try {
			if (Validator.isNotNull(stringDate )) {
				date = (Date) formatter.parse(stringDate);
			}
			if(Validator.isNull(date)){
				date = new Date();
			}
		}
		catch (ParseException e1) {

			LoggerUtil.errorLogger(logger, "NotificationController.dateFormatConversion() Parse Exception :::", e1);
		}

		return new Timestamp(date.getTime());
	}

	/**
	 * Helper method to return Notification Object from the model attribute.
	 * @param notificationId
	 * @return NotificationVO
	 */
	public NotificationVO searchList(String notificationId) {

		NotificationVO notificationVo = null;
		for (NotificationVO pmVO : notificaitonVoList) {
			if (pmVO.getNotificationId().equals(Long.valueOf(notificationId))) {
				notificationVo = pmVO;
			}
		}
		return notificationVo;
	}

	/**
	 * Helper method for converting List of Notification Domain Object to
	 * Value Object.
	 * @param ciNotification
	 * @return List<NotificationVO>
	 */
	private List<NotificationVO> convertNotificationDOToVOList(List<CINotification> ciNotification) {

		notificaitonVoList = new ArrayList<NotificationVO>();

		for (CINotification notification : ciNotification) {
			notificaitonVoList.add(convertNotificationDOToVO(notification));
		}
		return notificaitonVoList;
	}

	/**
	 * Helper method for converting Notification Domain Object to Value
	 * Object.
	 * @param ciNotification
	 * @return NotificationVO
	 */
	private NotificationVO convertNotificationDOToVO(CINotification ciNotification) {

		NotificationVO notificaitonVo = new NotificationVO();
		Format formatter = new SimpleDateFormat("MM/dd/yyyy");
		notificaitonVo.setCreateDate(ciNotification.getCreateDate());
		notificaitonVo.setDisplayDate(formatter.format(ciNotification.getDisplayDate()));
		notificaitonVo.setExpirationDate(formatter.format(ciNotification.getExpirationDate()));
		notificaitonVo.setNotificationDescription(ciNotification.getNotificationDescription());
		notificaitonVo.setNotificationForLocations(convertNotificationForLocationDOToVO(ciNotification.getNotificationForLocation()));
		notificaitonVo.setNotificationForRoles(convertRoleDOToVO(ciNotification.getNotificationForRoles()));
		notificaitonVo.setNotificationForUsers(convertUserDOToVO(ciNotification.getNotificationForUsers()));		
		notificaitonVo.setNotificationId(ciNotification.getNotificationId());
		notificaitonVo.setNotificationType(ciNotification.getNotificationType());
		notificaitonVo.setModifiedDate(ciNotification.getModifiedDate());
		notificaitonVo.setNotificationSubject(ciNotification.getNotificationSubject());
		notificaitonVo.setUserByCreatedBy(convertUserDOToVO(ciNotification.getUserByCreatedBy()));
		notificaitonVo.setUserByModifiedBy(convertUserDOToVO(ciNotification.getUserByModifiedBy()));
		notificaitonVo.setNotificationUrl(ciNotification.getUrl());

		return notificaitonVo;
	}

	/**
	 * Helper method for converting Notification For Location Domain Object to Value
	 * Object.
	 * @param notificationForLocation
	 * @return Set<NotificationForLocationVO>
	 */
	private Set<NotificationForLocationVO> convertNotificationForLocationDOToVO(Set<CINotificationForLocation> notificationForLocation) {

		Set<NotificationForLocationVO> notificationForLocationVOSet = new HashSet<NotificationForLocationVO>();
		for (CINotificationForLocation ciNotificationForLocation : notificationForLocation) {
			NotificationForLocationVO notificationForLocationVO = new NotificationForLocationVO();
			notificationForLocationVO.setCinflId(ciNotificationForLocation.getCinflId());
			notificationForLocationVO.setLocationType(ciNotificationForLocation.getLocationType());
			notificationForLocationVO.setLocationName(ciNotificationForLocation.getLocationName());
			notificationForLocationVO.setState(ciNotificationForLocation.getState());
			notificationForLocationVO.setCity(ciNotificationForLocation.getCity());
			notificationForLocationVO.setOffice(ciNotificationForLocation.getOffice());
			notificationForLocationVOSet.add(notificationForLocationVO);
		}
		return notificationForLocationVOSet;
	}

	/**
	 * Helper method for converting Role Domain Object to Value Object.
	 * @param role
	 * @return Set<RoleVO>
	 */
	private Set<RoleVO> convertRoleDOToVO(Set<Role> role) {

		Set<RoleVO> roleVOSet = new HashSet<RoleVO>();
		for (Role rle : role) {
			RoleVO roleVO = new RoleVO();
			roleVO.setClassNameId(rle.getClassNameId());
			roleVO.setClassPk(rle.getClassPk());
			roleVO.setCompanyId(rle.getCompanyId());
			roleVO.setDescription(rle.getDescription());
			roleVO.setName(rle.getName());
			roleVO.setRoleId(rle.getRoleId());
			roleVO.setSubtype(rle.getSubtype());
			roleVO.setTitle(rle.getTitle());
			roleVO.setType(rle.getType());

			roleVOSet.add(roleVO);
		}
		return roleVOSet;
	}

	/**
	 * Helper method for converting User Domain Object to Value Object.
	 * @param user
	 * @return Set<UserVO>
	 */
	
	private Set<UserVO> convertUserDOToVO(Set<User> user) {

		Set<UserVO> userVOSet = new HashSet<UserVO>();
		for (User usr : user) {
			UserVO userVo = new UserVO();
			
			userVo.setCompanyId(usr.getCompanyId());
			userVo.setUserId(usr.getUserId());
			userVo.setFirstName(usr.getFirstName());
			userVo.setMiddleName(usr.getMiddleName());
			userVo.setLanguageId(usr.getLanguageId());
			userVo.setLastName(usr.getLastName());
			userVo.setJobTitle(usr.getJobTitle());
			userVo.setEmailAddress(usr.getEmailAddress());
			userVo.setScreenName(usr.getScreenName());
	
			
			userVOSet.add(userVo);
		}
		return userVOSet;
	}
	
	/**
	 * Helper method for converting User Domain Object to Value Object.
	 * @param user
	 * @return UserVO
	 */
	private UserVO convertUserDOToVO(User user) {

		UserVO userVO = new UserVO();

		userVO.setUserId(user.getUserId());
		userVO.setUuid(user.getUuid());
		userVO.setCompanyId(user.getCompanyId());
		userVO.setCreateDate(user.getCreateDate());
		userVO.setModifiedDate(user.getModifiedDate());
		userVO.setDefaultUser(user.getDefaultUser());
		userVO.setContactId(user.getContactId());
		userVO.setPassword(user.getPassword());
		userVO.setPasswordEncrypted(user.getPasswordEncrypted());
		userVO.setPasswordReset(user.getPasswordReset());
		userVO.setPasswordModifiedDate(user.getPasswordModifiedDate());
		userVO.setDigest(user.getDigest());
		userVO.setReminderQueryQuestion(user.getReminderQueryQuestion());
		userVO.setReminderQueryAnswer(user.getReminderQueryAnswer());
		userVO.setGraceLoginCount(user.getGraceLoginCount());
		userVO.setScreenName(user.getScreenName());
		userVO.setEmailAddress(user.getEmailAddress());
		userVO.setFacebookId(user.getFacebookId());
		userVO.setOpenId(user.getOpenId());
		userVO.setPortraitId(user.getPortraitId());
		userVO.setLanguageId(user.getLanguageId());
		userVO.setTimeZoneId(user.getTimeZoneId());
		userVO.setGreeting(user.getGreeting());
		userVO.setComments(user.getComments());
		userVO.setFirstName(user.getFirstName());
		userVO.setMiddleName(user.getMiddleName());
		userVO.setLastName(user.getLastName());
		userVO.setJobTitle(user.getJobTitle());
		userVO.setLoginDate(user.getLoginDate());
		userVO.setLoginIp(user.getLoginIp());
		userVO.setLastLoginDate(user.getLastLoginDate());
		userVO.setLastLoginIp(user.getLastLoginIp());
		userVO.setLastFailedLoginDate(user.getLastFailedLoginDate());
		userVO.setFailedLoginAttempts(user.getFailedLoginAttempts());
		userVO.setLockout(user.getLockout());
		userVO.setLockoutDate(user.getLockoutDate());
		userVO.setAgreedToTermsOfUse(user.getAgreedToTermsOfUse());
		userVO.setEmailAddressVerified(user.getEmailAddressVerified());
		userVO.setStatus(user.getStatus());
		userVO.setAdminAssistSet(user.getCIEmployeesForAdminAssistId());
		userVO.setUserMetricsSet(user.getCIUserMetrics());
		userVO.setQuickLinksSet(user.getCIQuickLinks());
		userVO.setNotificationsCreatorSet(user.getCINotificationForCreatedBy());
		userVO.setNotificationsModifierSet(user.getCINotificationForModifiedBy());
		userVO.setSupervisorSet(user.getCIEmployeesForSupervisorId());
		userVO.setStoreEmployeesSet(user.getCIStoreEmployees());
		userVO.setUserSet(user.getCIEmployeesForUserId());

		return userVO;
	}

	/**
	 * Helper method for converting List of Notification Value Object to
	 * Domain Object.
	 * @param notificaitonVO
	 * @return List<CINotification>
	 */
	private List<CINotification> convertNotificationVOToDOList(List<NotificationVO> notificationVo) {

		List<CINotification> notificationDoList = new ArrayList<CINotification>();

		for (NotificationVO notification : notificationVo) {
			notificationDoList.add(convertNotificationVOToDO(notification));
		}
		return notificationDoList;
	}

	/**
	 * Helper method for converting NOtification Value Object to Domain
	 * Object.
	 * @param notificationVO
	 * @return CINotification
	 */
	private CINotification convertNotificationVOToDO(NotificationVO notificationVo) {

		Date displayDate = null;
		Date expirationDate = null;
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		CINotification notification = new CINotification();

		try {
			displayDate = (Date) formatter.parse(notificationVo.getDisplayDate());
			expirationDate = (Date) formatter.parse(notificationVo.getExpirationDate());
		}
		catch (ParseException e1) {
			LoggerUtil.errorLogger(logger, "NotificationController.convertNotificationVOToDO():::", e1);
		}

		notification.setCreateDate(notificationVo.getCreateDate());
		if (displayDate != null) {
			notification.setDisplayDate(new Timestamp(displayDate.getTime()));
		}
		if (expirationDate != null) {
			notification.setExpirationDate(new Timestamp(expirationDate.getTime()));
		}
		notification.setNotificationDescription(notificationVo.getNotificationDescription());
		notification.setNotificationId(notificationVo.getNotificationId());
		notification.setNotificationType(notificationVo.getNotificationType());
		notification.setModifiedDate(notificationVo.getModifiedDate());
		notification.setNotificationSubject(notificationVo.getNotificationSubject());
		notification.setUrl(notificationVo.getNotificationUrl());
		// personalMessage.setUserByCreatedBy(convertNotificationUserVOToDO(personalMessageVO.getUserByCreatedBy()));
		// personalMessage.setUserByModifiedBy(convertNotificationUserVOToDO(personalMessageVO.getUserByModifiedBy()));
		// personalMessage.setMessageForRoles(convertNotificationRoleVOToDO(personalMessageVO.getMessageForRoles()));
		notification.setNotificationForLocation(convertNotificationForLocationVOToDO(notificationVo.getNotificationForLocations()));

		/*
		 * Added to set the messageId value to ci_meesgaeforlocation table as we
		 * are unable to set through @ManytoOne annotations.
		 */
		for (CINotificationForLocation ciNotificationForLocation : notification.getNotificationForLocation()) {

			ciNotificationForLocation.setNotification(notification);
		}

		return notification;
	}

	/**
	 * Helper method for converting Notification for Location Value Object to Domain
	 * Object.
	 * @param notificationForLocationVO
	 * @return Set<CINotificationForLocation>
	 */
	private Set<CINotificationForLocation> convertNotificationForLocationVOToDO(Set<NotificationForLocationVO> notificationForLocationVO) {

		Set<CINotificationForLocation> notificationForLocationDOSet = new HashSet<CINotificationForLocation>();
		for (NotificationForLocationVO msgForLocationVO : notificationForLocationVO) {
			CINotificationForLocation ciNotificationForLocation = new CINotificationForLocation();
			ciNotificationForLocation.setCinflId(msgForLocationVO.getCinflId());
			ciNotificationForLocation.setLocationType(msgForLocationVO.getLocationType());
			ciNotificationForLocation.setLocationName(msgForLocationVO.getLocationName());
			ciNotificationForLocation.setState(msgForLocationVO.getState());
			ciNotificationForLocation.setCity(msgForLocationVO.getCity());
			ciNotificationForLocation.setOffice(msgForLocationVO.getOffice());

			notificationForLocationDOSet.add(ciNotificationForLocation);
		}
		return notificationForLocationDOSet;
	}

	/**
	 * Helper method for converting Role Value Object to Domain Object.
	 * @param notificationForRoles
	 * @return Set<Role>
	 */
	private Set<Role> convertNotificationRoleVOToDO(Set<RoleVO> notificationForRoles) {

		Set<Role> rolesSet = new HashSet<Role>();
		if (notificationForRoles != null && !notificationForRoles.isEmpty() && notificationForRoles.size() != 0) {
			for (RoleVO role : notificationForRoles) {
				Role tempRole = new Role();
				tempRole.setType(role.getType());
				tempRole.setTitle(role.getTitle());
				tempRole.setSubtype(role.getSubtype());
				tempRole.setRoleId(role.getRoleId());
				tempRole.setName(role.getName());
				tempRole.setDescription(role.getDescription());
				tempRole.setCompanyId(role.getCompanyId());
				tempRole.setClassPk(role.getClassPk());
				tempRole.setClassNameId(role.getClassNameId());
				rolesSet.add(tempRole);
			}
		}
		return rolesSet;
	}

	/**
	 * Helper method for converting User Value Object to Domain Object.
	 * @param userVO
	 * @return User
	 */
	private User convertNotificationUserVOToDO(UserVO userVO) {

		User user = new User();
		if (userVO != null) {
			userVO.setUserId(userVO.getUserId());
			userVO.setUuid(userVO.getUuid());
			userVO.setCompanyId(userVO.getCompanyId());
			userVO.setCreateDate(userVO.getCreateDate());
			userVO.setModifiedDate(userVO.getModifiedDate());
			userVO.setDefaultUser(userVO.getDefaultUser());
			userVO.setContactId(userVO.getContactId());
			userVO.setPassword(userVO.getPassword());
			userVO.setPasswordEncrypted(userVO.getPasswordEncrypted());
			userVO.setPasswordReset(userVO.getPasswordReset());
			userVO.setPasswordModifiedDate(userVO.getPasswordModifiedDate());
			userVO.setDigest(userVO.getDigest());
			userVO.setReminderQueryQuestion(userVO.getReminderQueryQuestion());
			userVO.setReminderQueryAnswer(userVO.getReminderQueryAnswer());
			userVO.setGraceLoginCount(userVO.getGraceLoginCount());
			userVO.setScreenName(userVO.getScreenName());
			userVO.setEmailAddress(userVO.getEmailAddress());
			userVO.setFacebookId(userVO.getFacebookId());
			userVO.setOpenId(userVO.getOpenId());
			userVO.setPortraitId(userVO.getPortraitId());
			userVO.setLanguageId(userVO.getLanguageId());
			userVO.setTimeZoneId(userVO.getTimeZoneId());
			userVO.setGreeting(userVO.getGreeting());
			userVO.setComments(userVO.getComments());
			userVO.setFirstName(userVO.getFirstName());
			userVO.setMiddleName(userVO.getMiddleName());
			userVO.setLastName(userVO.getLastName());
			userVO.setJobTitle(userVO.getJobTitle());
			userVO.setLoginDate(userVO.getLoginDate());
			userVO.setLoginIp(userVO.getLoginIp());
			userVO.setLastLoginDate(userVO.getLastLoginDate());
			userVO.setLastLoginIp(userVO.getLastLoginIp());
			userVO.setLastFailedLoginDate(userVO.getLastFailedLoginDate());
			userVO.setFailedLoginAttempts(userVO.getFailedLoginAttempts());
			userVO.setLockout(userVO.getLockout());
			userVO.setLockoutDate(userVO.getLockoutDate());
			userVO.setAgreedToTermsOfUse(userVO.getAgreedToTermsOfUse());
			userVO.setEmailAddressVerified(userVO.getEmailAddressVerified());
			userVO.setStatus(userVO.getStatus());
			userVO.setAdminAssistSet(userVO.getAdminAssistSet());
			userVO.setUserMetricsSet(userVO.getUserMetricsSet());
			userVO.setQuickLinksSet(userVO.getQuickLinksSet());
			userVO.setNotificationsCreatorSet(userVO.getNotificationsCreatorSet());
			userVO.setNotificationsModifierSet(userVO.getNotificationsModifierSet());
			userVO.setSupervisorSet(userVO.getSupervisorSet());
			userVO.setStoreEmployeesSet(userVO.getStoreEmployeesSet());
			userVO.setUserSet(userVO.getUserSet());
		}
		return user;
	}

	/**
	 * Helper method for populating Location specific Notification to the
	 * Value Object from ActionRequest.
	 * @param actionRequest
	 * @return Set<NotificationForLocationVO>
	 */
	public Set<NotificationForLocationVO> populatingNotificationForLocationVO(ActionRequest actionRequest) {

		LoggerUtil.debugLogger(logger, "Start of populatingNotificationForLocation() method of Managing Notification Controller::");

		String notificationForLocationId = actionRequest.getParameter("notificationForLocationId");
		String locationType = actionRequest.getParameter("locationType");
		String geographyId = actionRequest.getParameter("geographyId");
		String countryId = actionRequest.getParameter("countryId");
		String stateId = actionRequest.getParameter("stateId");
		String cityName = actionRequest.getParameter("cityName");
		String officeName = actionRequest.getParameter("officeName");
		String buildingName = actionRequest.getParameter("buildingName");
		Set<NotificationForLocationVO> notificationForLocationVOSet = new HashSet<NotificationForLocationVO>();

		NotificationForLocationVO notificationForLocationVO = new NotificationForLocationVO();
		if (null != notificationForLocationId && !notificationForLocationId.isEmpty()) {
			notificationForLocationVO.setCinflId(Long.valueOf(notificationForLocationId));
		}

		if (locationType.equals("Building") && buildingName != null && !buildingName.equals("0")) {
			notificationForLocationVO.setLocationType(locationType);
			notificationForLocationVO.setLocationName(buildingName);
			notificationForLocationVO.setState(stateId);
			notificationForLocationVO.setCity(cityName);
			notificationForLocationVO.setOffice(officeName);
		}
		else if (locationType.equals("Office") && officeName != null && !officeName.equals("0")) {
			notificationForLocationVO.setLocationType(locationType);
			notificationForLocationVO.setLocationName(officeName);
			notificationForLocationVO.setState(stateId);
			notificationForLocationVO.setCity(cityName);
		}
		else if (locationType.equals("City") && cityName != null && !cityName.equals("0")) {
			notificationForLocationVO.setLocationType(locationType);
			notificationForLocationVO.setLocationName(cityName);
			notificationForLocationVO.setState(stateId);
		}
		else if (locationType.equals("Region") && stateId != null && !stateId.equals("0")) {
			notificationForLocationVO.setLocationType(locationType);
			notificationForLocationVO.setLocationName(stateId);
		}
		else if (locationType.equals("Country") && countryId != null && !countryId.equals("0")) {
			notificationForLocationVO.setLocationType(locationType);
			notificationForLocationVO.setLocationName(countryId);
		}
		else if (locationType.equals("Geography") && geographyId != null && !geographyId.equals("0")) {
			notificationForLocationVO.setLocationType(locationType);
			notificationForLocationVO.setLocationName(geographyId);
		}

		notificationForLocationVOSet.add(notificationForLocationVO);

		LoggerUtil.debugLogger(logger, "End of populatingNotificationForLocation() method of Managing Notification Controller::");
		return notificationForLocationVOSet;
	}

	/**
	 * Helper method for converting List of Geography Domain Object to Value
	 * Object.
	 * @param geographyDOList
	 * @return List<GeographyVO>
	 */
	private List<GeographyVO> convertGeographyDOToVOList(List<CIGeography> geographyDOList) {

		geographyVOList = new ArrayList<GeographyVO>();
		for (CIGeography ciGeography : geographyDOList) {
			geographyVOList.add(convertGeographyDOToVO(ciGeography));
		}
		return geographyVOList;
	}

	/**
	 * Helper method for converting Geography Domain Object to Value Object.
	 * @param ciGeography
	 * @return GeographyVO
	 */
	private GeographyVO convertGeographyDOToVO(CIGeography ciGeography) {

		GeographyVO geographyVO = new GeographyVO();
		geographyVO.setGeographyId(ciGeography.getGeographyId());
		geographyVO.setGeographyName(ciGeography.getGeographyName());

		return geographyVO;
	}

	/**
	 * Helper method for converting Country Domain Object to Value Object.
	 * @param ciGeographycountries
	 * @return Set<CountryVO>
	 */
	private Set<CountryVO> convertGeographyCountryDotoVO(Set<Country> ciGeographycountries) {

		Set<CountryVO> countryVOSet = new HashSet<CountryVO>();
		for (Country country : ciGeographycountries) {
			CountryVO countryVO = new CountryVO();
			countryVO.setCountryId(country.getCountryId());
			countryVO.setName(country.getName());
			countryVO.setNumber(country.getNumber());
			countryVO.setIdd(country.getIdd());
			countryVO.setActive(country.getActive());
			countryVO.setA2(country.getA2());
			countryVO.setA3(country.getA3());
			countryVO.setZipRequired(country.getZipRequired());
			countryVOSet.add(countryVO);
		}
		return countryVOSet;
	}

	/**
	 * Resource Mapping Method for populating list of Countries available for
	 * the selected Geography.
	 * @param request
	 * @param response
	 * @return ModelAndView
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@ResourceMapping("populateCountries")
	public ModelAndView populateCountries(ResourceRequest request, ResourceResponse response) throws IOException {

		response.setContentType("text/html; charset=utf-8");
		LocationService<CIGeography> countryLocationService = serviceLocator.getLocationService();
		String geoId = request.getParameter("geoId");
		List<CIGeography> list = countryLocationService.getAllLocation(CIGeography.class, "geographyId", Long.valueOf(geoId));
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("countryList", convertGeographyCountryDotoVO(list.get(0).getCiGeographycountries()));
		return new ModelAndView("countyList", model);
	}

	/**
	 * Resource Mapping Method for populating list of States available for the
	 * selected Country.
	 * @param request
	 * @param response
	 * @return ModelAndView
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@ResourceMapping("populateStates")
	public ModelAndView populateStates(ResourceRequest request, ResourceResponse response) throws IOException {

		response.setContentType("text/html; charset=utf-8");
		LocationService<Region> stateLocationService = serviceLocator.getLocationService();
		String countryId = request.getParameter("countryId");
		List<Region> list = stateLocationService.getAllLocation(Region.class, "countryId", Long.valueOf(countryId));
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("statesList", list);
		return new ModelAndView("StatesList", model);
	}

	/**
	 * Resource Mapping Method for populating list of Cities available for the
	 * selected State.
	 * @param request
	 * @param response
	 * @return ModelAndView
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@ResourceMapping("populateCities")
	public ModelAndView populateCities(ResourceRequest request, ResourceResponse response) throws IOException {

		response.setContentType("text/html; charset=utf-8");
		LocationService<Address> cityLocationService = serviceLocator.getLocationService();
		String stateId = request.getParameter("stateId");
		List<String> cityList = cityLocationService.getAllDistinctAddressLocation(Address.class, "city", "regionId", stateId);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("cityList", cityList);
		return new ModelAndView("CityList", model);
	}

	/**
	 * Resource Mapping Method for populating list of Offices available for the
	 * selected City.
	 * @param request
	 * @param response
	 * @return ModelAndView
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@ResourceMapping("populateOffices")
	public ModelAndView populateOffices(ResourceRequest request, ResourceResponse response) throws IOException {

		response.setContentType("text/html; charset=utf-8");
		LocationService<Address> officeLocationService = serviceLocator.getLocationService();
		String cityName = request.getParameter("cityId");
		List<String> officesList = officeLocationService.getAllDistinctAddressLocation(Address.class, "street3", "city", cityName);
		Map<String, Object> model = new HashMap<String, Object>();
		officesList.removeAll(Collections.singleton(null));
		officesList.removeAll(Collections.singleton(""));
		model.put("officesList", officesList);
		return new ModelAndView("OfficesList", model);
	}

	/**
	 * Resource Mapping Method for populating list of Buildings available for
	 * the selected Office.
	 * @param request
	 * @param response
	 * @return ModelAndView
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@ResourceMapping("populateBuildings")
	public ModelAndView populateBuildings(ResourceRequest request, ResourceResponse response) throws IOException {

		response.setContentType("text/html; charset=utf-8");
		LocationService<Address> buildingLocationService = serviceLocator.getLocationService();
		String officeName = request.getParameter("officeId");
		List<String> buildingsList = buildingLocationService.getAllDistinctAddressLocation(Address.class, "street2", "street3", officeName);
		Map<String, Object> model = new HashMap<String, Object>();
		buildingsList.removeAll(Collections.singleton(null));
		buildingsList.removeAll(Collections.singleton(""));
		model.put("buildingsList", buildingsList);
		return new ModelAndView("BuildingList", model);
	}

	/**
	 * Method to populate users based on roles.
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@ResourceMapping("populateUsers")
	public ModelAndView populateUsers(ResourceRequest request, ResourceResponse response) throws IOException {
				
		//Long [] roleLongIds = new Long[0];
		Set<com.liferay.portal.model.User> userList = new HashSet<com.liferay.portal.model.User>();
		String[] notifyRoleIds = new String[0];
		String roleIds = request.getParameter("notify_roleIds");
		notifyRoleIds = roleIds.split(",");		
		Map<String, Object> userMap = new HashMap<String, Object>();
		//roleLongIds = ArrayUtil.toLongArray(notifyRoleIds);		
		
		Long[] roleLongIds = new Long[notifyRoleIds.length];  
		for (int i = 0; i < notifyRoleIds.length; i++) {  
			roleLongIds[i] = Long.valueOf(notifyRoleIds[i]);  
		}
		
		try {
			for(long roleId : roleLongIds) {
				
				userList.addAll(UserLocalServiceUtil.getRoleUsers(roleId));
				
			}			
			
		} catch (SystemException e) {
			e.printStackTrace();
		}		
		userMap.put("userList", userList);		
		return new ModelAndView("notifyUsers", userMap);
	}

	
	
}
