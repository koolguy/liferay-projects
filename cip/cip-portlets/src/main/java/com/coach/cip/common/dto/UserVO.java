package com.coach.cip.common.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import com.coach.cip.model.entity.CIEmployee;
import com.coach.cip.model.entity.CINotification;
import com.coach.cip.model.entity.CIPersonalMessage;
import com.coach.cip.model.entity.CIQuickLink;
import com.coach.cip.model.entity.CIStoreEmployee;
import com.coach.cip.model.entity.CIUserMetric;
/**
 * 
 * Class is used to capture the characteristics of user.
 * 
 * @author GalaxE.
 *
 */
public class UserVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -676286886406888542L;
	private Long userId;
	private String uuid;
	private Long companyId;
	private Timestamp createDate;
	private Timestamp modifiedDate;
	private Short defaultUser;
	private Long contactId;
	private String password;
	private Short passwordEncrypted;
	private Short passwordReset;
	private Timestamp passwordModifiedDate;
	private String digest;
	private String reminderQueryQuestion;
	private String reminderQueryAnswer;
	private Integer graceLoginCount;
	private String screenName;
	private String emailAddress;
	private Long facebookId;
	private String openId;
	private Long portraitId;
	private String languageId;
	private String timeZoneId;
	private String greeting;
	private String comments;
	private String firstName;
	private String middleName;
	private String lastName;
	private String jobTitle;
	private Timestamp loginDate;
	private String loginIp;
	private Timestamp lastLoginDate;
	private String lastLoginIp;
	private Timestamp lastFailedLoginDate;
	private Integer failedLoginAttempts;
	private Short lockout;
	private Timestamp lockoutDate;
	private Short agreedToTermsOfUse;
	private Short emailAddressVerified;
	private Integer status;
	private Set<CIEmployee> adminAssistSet = new HashSet<CIEmployee>(0);
	private Set<CIUserMetric> userMetricsSet = new HashSet<CIUserMetric>(0);
	private Set<CIQuickLink> quickLinksSet = new HashSet<CIQuickLink>(0);
	private Set<CIPersonalMessage> personalMessagesCreatorSet = new HashSet<CIPersonalMessage>(
			0);
	private Set<CIPersonalMessage> personalMessagesModifierSet = new HashSet<CIPersonalMessage>(
			0);
	private Set<CIEmployee> supervisorSet = new HashSet<CIEmployee>(0);
	private Set<CIStoreEmployee> storeEmployeesSet = new HashSet<CIStoreEmployee>(
			0);
	private Set<CIEmployee> userSet = new HashSet<CIEmployee>(0);
	
	private Set<CINotification> notificationsCreatorSet = new HashSet<CINotification>(
			0);
	private Set<CINotification> notificationsModifierSet = new HashSet<CINotification>(
			0);

	// Constructors

	/** default constructor */
	public UserVO() {
	}

	/** minimal constructor */
	public UserVO(Long userId) {
		this.userId = userId;
	}

	/** full constructor */
	public UserVO(Long userId, String uuid, Long companyId,
			Timestamp createDate, Timestamp modifiedDate, Short defaultUser,
			Long contactId, String password, Short passwordEncrypted,
			Short passwordReset, Timestamp passwordModifiedDate, String digest,
			String reminderQueryQuestion, String reminderQueryAnswer,
			Integer graceLoginCount, String screenName, String emailAddress,
			Long facebookId, String openId, Long portraitId, String languageId,
			String timeZoneId, String greeting, String comments,
			String firstName, String middleName, String lastName,
			String jobTitle, Timestamp loginDate, String loginIp,
			Timestamp lastLoginDate, String lastLoginIp,
			Timestamp lastFailedLoginDate, Integer failedLoginAttempts,
			Short lockout, Timestamp lockoutDate, Short agreedToTermsOfUse,
			Short emailAddressVerified, Integer status,
			Set<CIEmployee> adminAssistSet, Set<CIUserMetric> userMetricsSet,
			Set<CIQuickLink> quickLinksSet,
			Set<CIPersonalMessage> personalMessagesCreatorSet,
			Set<CIPersonalMessage> personalMessagesModifierSet,
			Set<CIEmployee> supervisorSet,
			Set<CIStoreEmployee> storeEmployeesSet, Set<CIEmployee> userSet,
			Set<CINotification> notificationsCreatorSet,
			Set<CINotification> notificationsModifierSet) {
		this.userId = userId;
		this.uuid = uuid;
		this.companyId = companyId;
		this.createDate = createDate;
		this.modifiedDate = modifiedDate;
		this.defaultUser = defaultUser;
		this.contactId = contactId;
		this.password = password;
		this.passwordEncrypted = passwordEncrypted;
		this.passwordReset = passwordReset;
		this.passwordModifiedDate = passwordModifiedDate;
		this.digest = digest;
		this.reminderQueryQuestion = reminderQueryQuestion;
		this.reminderQueryAnswer = reminderQueryAnswer;
		this.graceLoginCount = graceLoginCount;
		this.screenName = screenName;
		this.emailAddress = emailAddress;
		this.facebookId = facebookId;
		this.openId = openId;
		this.portraitId = portraitId;
		this.languageId = languageId;
		this.timeZoneId = timeZoneId;
		this.greeting = greeting;
		this.comments = comments;
		this.firstName = firstName;
		this.middleName = middleName;
		this.lastName = lastName;
		this.jobTitle = jobTitle;
		this.loginDate = loginDate;
		this.loginIp = loginIp;
		this.lastLoginDate = lastLoginDate;
		this.lastLoginIp = lastLoginIp;
		this.lastFailedLoginDate = lastFailedLoginDate;
		this.failedLoginAttempts = failedLoginAttempts;
		this.lockout = lockout;
		this.lockoutDate = lockoutDate;
		this.agreedToTermsOfUse = agreedToTermsOfUse;
		this.emailAddressVerified = emailAddressVerified;
		this.status = status;
		this.adminAssistSet = adminAssistSet;
		this.userMetricsSet = userMetricsSet;
		this.quickLinksSet = quickLinksSet;
		this.personalMessagesCreatorSet = personalMessagesCreatorSet;
		this.personalMessagesModifierSet = personalMessagesModifierSet;
		this.supervisorSet = supervisorSet;
		this.storeEmployeesSet = storeEmployeesSet;
		this.userSet = userSet;
		this.notificationsCreatorSet = notificationsCreatorSet;
		this.notificationsModifierSet = notificationsModifierSet;
	}

	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getUuid() {
		return this.uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public Long getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public Short getDefaultUser() {
		return this.defaultUser;
	}

	public void setDefaultUser(Short defaultUser) {
		this.defaultUser = defaultUser;
	}

	public Long getContactId() {
		return this.contactId;
	}

	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Short getPasswordEncrypted() {
		return this.passwordEncrypted;
	}

	public void setPasswordEncrypted(Short passwordEncrypted) {
		this.passwordEncrypted = passwordEncrypted;
	}

	public Short getPasswordReset() {
		return this.passwordReset;
	}

	public void setPasswordReset(Short passwordReset) {
		this.passwordReset = passwordReset;
	}

	public Timestamp getPasswordModifiedDate() {
		return this.passwordModifiedDate;
	}

	public void setPasswordModifiedDate(Timestamp passwordModifiedDate) {
		this.passwordModifiedDate = passwordModifiedDate;
	}

	public String getDigest() {
		return this.digest;
	}

	public void setDigest(String digest) {
		this.digest = digest;
	}

	public String getReminderQueryQuestion() {
		return this.reminderQueryQuestion;
	}

	public void setReminderQueryQuestion(String reminderQueryQuestion) {
		this.reminderQueryQuestion = reminderQueryQuestion;
	}

	public String getReminderQueryAnswer() {
		return this.reminderQueryAnswer;
	}

	public void setReminderQueryAnswer(String reminderQueryAnswer) {
		this.reminderQueryAnswer = reminderQueryAnswer;
	}

	public Integer getGraceLoginCount() {
		return this.graceLoginCount;
	}

	public void setGraceLoginCount(Integer graceLoginCount) {
		this.graceLoginCount = graceLoginCount;
	}

	public String getScreenName() {
		return this.screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public Long getFacebookId() {
		return this.facebookId;
	}

	public void setFacebookId(Long facebookId) {
		this.facebookId = facebookId;
	}

	public String getOpenId() {
		return this.openId;
	}

	public void setOpenId(String openId) {
		this.openId = openId;
	}

	public Long getPortraitId() {
		return this.portraitId;
	}

	public void setPortraitId(Long portraitId) {
		this.portraitId = portraitId;
	}

	public String getLanguageId() {
		return this.languageId;
	}

	public void setLanguageId(String languageId) {
		this.languageId = languageId;
	}

	public String getTimeZoneId() {
		return this.timeZoneId;
	}

	public void setTimeZoneId(String timeZoneId) {
		this.timeZoneId = timeZoneId;
	}

	public String getGreeting() {
		return this.greeting;
	}

	public void setGreeting(String greeting) {
		this.greeting = greeting;
	}

	public String getComments() {
		return this.comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return this.middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getJobTitle() {
		return this.jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public Timestamp getLoginDate() {
		return this.loginDate;
	}

	public void setLoginDate(Timestamp loginDate) {
		this.loginDate = loginDate;
	}

	public String getLoginIp() {
		return this.loginIp;
	}

	public void setLoginIp(String loginIp) {
		this.loginIp = loginIp;
	}

	public Timestamp getLastLoginDate() {
		return this.lastLoginDate;
	}

	public void setLastLoginDate(Timestamp lastLoginDate) {
		this.lastLoginDate = lastLoginDate;
	}

	public String getLastLoginIp() {
		return this.lastLoginIp;
	}

	public void setLastLoginIp(String lastLoginIp) {
		this.lastLoginIp = lastLoginIp;
	}

	public Timestamp getLastFailedLoginDate() {
		return this.lastFailedLoginDate;
	}

	public void setLastFailedLoginDate(Timestamp lastFailedLoginDate) {
		this.lastFailedLoginDate = lastFailedLoginDate;
	}

	public Integer getFailedLoginAttempts() {
		return this.failedLoginAttempts;
	}

	public void setFailedLoginAttempts(Integer failedLoginAttempts) {
		this.failedLoginAttempts = failedLoginAttempts;
	}

	public Short getLockout() {
		return this.lockout;
	}

	public void setLockout(Short lockout) {
		this.lockout = lockout;
	}

	public Timestamp getLockoutDate() {
		return this.lockoutDate;
	}

	public void setLockoutDate(Timestamp lockoutDate) {
		this.lockoutDate = lockoutDate;
	}

	public Short getAgreedToTermsOfUse() {
		return this.agreedToTermsOfUse;
	}

	public void setAgreedToTermsOfUse(Short agreedToTermsOfUse) {
		this.agreedToTermsOfUse = agreedToTermsOfUse;
	}

	public Short getEmailAddressVerified() {
		return this.emailAddressVerified;
	}

	public void setEmailAddressVerified(Short emailAddressVerified) {
		this.emailAddressVerified = emailAddressVerified;
	}

	public Integer getStatus() {
		return this.status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Set<CIEmployee> getAdminAssistSet() {
		return adminAssistSet;
	}

	public void setAdminAssistSet(Set<CIEmployee> adminAssistSet) {
		this.adminAssistSet = adminAssistSet;
	}

	public Set<CIUserMetric> getUserMetricsSet() {
		return userMetricsSet;
	}

	public void setUserMetricsSet(Set<CIUserMetric> userMetricsSet) {
		this.userMetricsSet = userMetricsSet;
	}

	public Set<CIQuickLink> getQuickLinksSet() {
		return quickLinksSet;
	}

	public void setQuickLinksSet(Set<CIQuickLink> quickLinksSet) {
		this.quickLinksSet = quickLinksSet;
	}

	public Set<CIPersonalMessage> getPersonalMessagesCreatorSet() {
		return personalMessagesCreatorSet;
	}

	public void setPersonalMessagesCreatorSet(
			Set<CIPersonalMessage> personalMessagesCreatorSet) {
		this.personalMessagesCreatorSet = personalMessagesCreatorSet;
	}

	public Set<CIPersonalMessage> getPersonalMessagesModifierSet() {
		return personalMessagesModifierSet;
	}

	public void setPersonalMessagesModifierSet(
			Set<CIPersonalMessage> personalMessagesModifierSet) {
		this.personalMessagesModifierSet = personalMessagesModifierSet;
	}

	public Set<CIEmployee> getSupervisorSet() {
		return supervisorSet;
	}

	public void setSupervisorSet(Set<CIEmployee> supervisorSet) {
		this.supervisorSet = supervisorSet;
	}

	public Set<CIStoreEmployee> getStoreEmployeesSet() {
		return storeEmployeesSet;
	}

	public void setStoreEmployeesSet(Set<CIStoreEmployee> storeEmployeesSet) {
		this.storeEmployeesSet = storeEmployeesSet;
	}

	public Set<CIEmployee> getUserSet() {
		return userSet;
	}

	public void setUserSet(Set<CIEmployee> userSet) {
		this.userSet = userSet;
	}

	
	public Set<CINotification> getNotificationsCreatorSet() {
		return notificationsCreatorSet;
	}

	public void setNotificationsCreatorSet(
			Set<CINotification> notificationsCreatorSet) {
		this.notificationsCreatorSet = notificationsCreatorSet;
	}

	public Set<CINotification> getNotificationsModifierSet() {
		return notificationsModifierSet;
	}

	
	public void setNotificationsModifierSet(
			Set<CINotification> notificationsModifierSet) {
		this.notificationsModifierSet = notificationsModifierSet;
	}

	
}
