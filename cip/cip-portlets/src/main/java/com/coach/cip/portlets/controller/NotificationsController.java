package com.coach.cip.portlets.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.xml.namespace.QName;
import javax.xml.transform.TransformerException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;
import org.springframework.ws.transport.context.TransportContextHolder;
import org.springframework.ws.transport.http.CommonsHttpConnection;

import com.coach.cip.common.dto.HRCaseServiceVO;
import com.coach.cip.common.dto.NotificationForLocationVO;
import com.coach.cip.common.dto.NotificationVO;
import com.coach.cip.common.dto.RoleVO;
import com.coach.cip.common.dto.UserVO;
import com.coach.cip.model.entity.CINotification;
import com.coach.cip.model.entity.CINotificationForLocation;
import com.coach.cip.model.entity.Role;
import com.coach.cip.model.entity.User;
import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.coach.cip.util.WebUtil;
import com.coach.notifications.hrtaskparents.service.beans.Execute;
import com.coach.notifications.hrtaskparents.service.beans.ExecuteResponse;
import com.coach.notifications.service.beans.GetRecords;
import com.coach.notifications.service.beans.GetRecordsResponse;
import com.coach.notifications.service.beans.GetRecordsResponse.GetRecordsResult;
import com.coach.notifications.service.beans.ObjectFactory;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;

/**
 * Class is used to capture the characteristics of notifications.
 * 
 * @author GalaxE.
 *
 */

@Controller("notificationsController")
@RequestMapping(value = "VIEW")
public class NotificationsController extends AbstractBaseController{

	/** The Constant LOG. */
	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(NotificationsController.class);
	private static final String NOTIFICATIONS = "notifications";
	
	@Autowired
	@Qualifier("notificationsServiceTemplate")
	private WebServiceTemplate webServiceTemplate;
	
	@Autowired
	@Qualifier("notificationsHRCaseServiceTemplate")
	private WebServiceTemplate hrCaseWebServiceTemplate;
	
	@Autowired
	@Qualifier("notificationsHRTaskParentsServiceTemplate")
	private WebServiceTemplate hrtaskParentsWebServiceTemplate;
	
	
	private static final ObjectFactory WS_CLIENT_FACTORY = new ObjectFactory();
	private static final com.coach.notifications.hrcase.service.beans.ObjectFactory WS_HRCASE_CLIENT_FACTORY = new com.coach.notifications.hrcase.service.beans.ObjectFactory();
	private static final com.coach.notifications.hrtaskparents.service.beans.ObjectFactory WS_HRTASKPARENTS_CLIENT_FACTORY = new com.coach.notifications.hrtaskparents.service.beans.ObjectFactory();
	
	/**
	 * Method used to render notifications view.
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception 
	 */
	@RenderMapping
	public String showNotifications(RenderRequest request,RenderResponse response, Model model) throws Exception {
		LoggerUtil.debugLogger(logger,"Render mapping for Notifications view in NotificationsController");
		
		com.liferay.portal.model.User user = (com.liferay.portal.model.User)request.getAttribute(WebKeys.USER);
		LoggerUtil.debugLogger(logger, "user from notification Controller--------------->"+user.getEmailAddress()+"---"+user.getScreenName());
		
		List<GetRecordsResult> recordsList= null;
		List<com.coach.notifications.hrcase.service.beans.GetRecordsResponse.GetRecordsResult> hrCaseNotificationsList = null;
		recordsList = gerServiceNowNotifications(user, model);
	//	hrCaseNotificationsList = getHRCaseNotificationList(user,model);
		String caseExecutiveResponse = getHRTaskParentsNotification(model, user);
		LoggerUtil.infoLogger(logger, "caseExecutiveResponse from rendermapping--------->"+caseExecutiveResponse);
		List<HRCaseServiceVO> hrCaseServiceVOList = hrCaseServiceVOList(caseExecutiveResponse);
		
		if(Validator.isNotNull(hrCaseServiceVOList)){
			model.addAttribute("hrCaseServiceVOList", hrCaseServiceVOList);
		}
		model.addAttribute("recordsList", recordsList);
		model.addAttribute("hrCaseNotificationsList", hrCaseNotificationsList);
		return NOTIFICATIONS;
	}
	
	/**
	 * Helper method to process hr case notifications.
	 * 
	 * @param caseExecutiveResponse
	 * @return
	 */
	public List<HRCaseServiceVO> hrCaseServiceVOList(String caseExecutiveResponse) {
		List<HRCaseServiceVO> hrCaseServiceVOList = null;
		List<String> tempList = null;
		if(Validator.isNotNull(caseExecutiveResponse) && caseExecutiveResponse.contains(",")){
			LoggerUtil.infoLogger(logger, "caseExecutiveResponse from NotificationController.hrCaseServiceVOList()--------->"+caseExecutiveResponse);
				String[] tempArray = caseExecutiveResponse.split(",");
				if(Validator.isNotNull(tempArray)){
					tempList = new ArrayList<String>();
					for(int i=0;i<tempArray.length; i++){
						LoggerUtil.infoLogger(logger, "tempArray[i] from NotificationController.hrCaseServiceVOList()--------->"+tempArray[i]);
						if(Validator.isNotNull(tempArray[i])){
							tempList.add(tempArray[i]);
					}}
				}
		} else if(Validator.isNotNull(caseExecutiveResponse) && caseExecutiveResponse.contains("|")){
			hrCaseServiceVOList = new ArrayList<HRCaseServiceVO>();
			HRCaseServiceVO hrCaseServiceVO = filterHRCaseServiceVO(caseExecutiveResponse);
			if(Validator.isNotNull(hrCaseServiceVO)){
				hrCaseServiceVOList.add(hrCaseServiceVO);
			}
			LoggerUtil.infoLogger(logger, "hrCaseServiceVOList.size() from NotificationController.hrCaseServiceVOList()----END----->"+hrCaseServiceVOList.size());
		}
		if(Validator.isNotNull(tempList)){
			LoggerUtil.infoLogger(logger, "tempList.size() from NotificationController.hrCaseServiceVOList()--------->"+tempList.size());
			hrCaseServiceVOList = new ArrayList<HRCaseServiceVO>();
			for(String tempString : tempList){
				HRCaseServiceVO hrCaseServiceVO = filterHRCaseServiceVO(tempString);
				if(Validator.isNotNull(hrCaseServiceVO)){
					hrCaseServiceVOList.add(hrCaseServiceVO);
				}
			}
			LoggerUtil.infoLogger(logger, "hrCaseServiceVOList.size() from NotificationController.hrCaseServiceVOList()--------->"+hrCaseServiceVOList.size());
		}
		
		
		return hrCaseServiceVOList;
	}
	
	/**
	 * Helper method which returns HRCaseServiceVO.
	 * 
	 * @param tempString
	 * @return
	 */
	public HRCaseServiceVO filterHRCaseServiceVO(String tempString) {
		HRCaseServiceVO hrCaseServiceVO = null;
		if(Validator.isNotNull(tempString) && tempString.contains("|") && Validator.isNotNull(tempString.split("\\|")[0]) && Validator.isNotNull(tempString.split("\\|")[1])){
			hrCaseServiceVO = new HRCaseServiceVO();
			hrCaseServiceVO.setCaseSysId(tempString.split("\\|")[0]);
			hrCaseServiceVO.setCaseDescription(tempString.split("\\|")[1]);
			LoggerUtil.infoLogger(logger, "hrCaseServiceVO.filterHRCaseServiceVOList() from NotificationController.hrCaseServiceVOList()--------->"+hrCaseServiceVO.getCaseDescription());
			LoggerUtil.infoLogger(logger, "hrCaseServiceVO.filterHRCaseServiceVOList() from NotificationController.hrCaseServiceVOList()--------->"+hrCaseServiceVO.getCaseSysId());
		}
		 return hrCaseServiceVO;
	}
	
	/**
	 * Method used to get Service Now Notifications.
	 * 
	 * @param user
	 * @param model
	 * @return List<GetRecordsResult> 
	 */
	public List<GetRecordsResult> gerServiceNowNotifications(com.liferay.portal.model.User user, Model model) {
		LoggerUtil.debugLogger(logger, "Inside gerServiceNowNotifications in NotificationController");
		List<GetRecordsResult> recordsList;
		/*Reading web service properties file from catalina home - Start */
		Properties props = new Properties();
		String path = System.getProperty("catalina.home")+ File.separator + "web-service.properties";
		try {
			props.load(new FileInputStream(path));
		} catch (Exception e) {
			LoggerUtil.errorLogger(logger, "Caught Exception While loading web service properties",e);
		}
		/*Reading web service properties file from catalina home - End */
		
		final String userName = (String) props.get("coach.cip.notificationwebservice.username");
		final String passWord = (String) props.get("coach.cip.notificationwebservice.password");
		
		LoggerUtil.debugLogger(logger,"Notification Webservice userName------------>"+userName);
		LoggerUtil.debugLogger(logger,"Notification Webservice passWord------------>"+passWord);
		
		GetRecords getRecordsRequest = WS_CLIENT_FACTORY.createGetRecords();
		if(Validator.isNotNull(user) && Validator.isNotNull(user.getScreenName())){
			getRecordsRequest.setEncodedQuery("recipients="+user.getEmailAddress().trim()+"^"+"u_read_receipt=false");
			//getRecordsRequest.setEncodedQuery("recipients=mkakaraparthi@coach.com^u_read_receipt=false");
		}
		GetRecordsResponse getRecordsResponse = null;
		try{
			getRecordsResponse = (GetRecordsResponse) webServiceTemplate.marshalSendAndReceive(getRecordsRequest, new WebServiceMessageCallback() {
			public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
		        CommonsHttpConnection connection =(CommonsHttpConnection) TransportContextHolder.getTransportContext().getConnection();
		        PostMethod postMethod = connection.getPostMethod();
		        String authorization = new sun.misc.BASE64Encoder().encode((userName+":"+passWord).getBytes());
		        postMethod.setRequestHeader( "Authorization", "Basic " + authorization);
		        postMethod.addParameter("Accept", "application/xml");
		    }
		} );
		
		}catch(SoapFaultClientException ex){
			String lpart = null;
			SoapFaultClientException soapException = (SoapFaultClientException)ex;
			lpart = soapException.getFaultCode().getLocalPart().trim();
			if(lpart != null && lpart.equals("Server")){
				model.addAttribute("FaultCode", lpart);
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
			}
			if(lpart != null && lpart.equals("Client")){
				model.addAttribute("FaultCode", lpart);
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_CLIENT);
			}
			if(lpart != null && lpart.equals("MustUnderstand")){
				model.addAttribute("FaultCode", lpart);
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_UNDERSTAND);
			}
			if(lpart != null && lpart.equals("VersionMismatch")){
				model.addAttribute("FaultCode", lpart);
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_VERMISMATCH);
			}
			LoggerUtil.errorLogger(logger, "SoapFaultClientException caught while processing Notification Web Service in NotificationController.gerServiceNowNotifications()", ex);
		}
		catch(Exception ex){
			if(ex instanceof SocketTimeoutException){
				model.addAttribute("FaultMessage", "SocketTimeOutException Caught");
				LoggerUtil.errorLogger(logger, "SocketTimeOutException caught while processing Notification Web Service in NotificationController.gerServiceNowNotifications()", ex);
			} else if(ex instanceof ConnectException){
				model.addAttribute("FaultMessage", "ConnectException Caught");
				LoggerUtil.errorLogger(logger, "ConnectException caught while processing Notification Web Service in NotificationController.gerServiceNowNotifications()", ex);
			} else {
			LoggerUtil.errorLogger(logger, "Exception caught while processing Notification Web Service in NotificationController.gerServiceNowNotifications()", ex);
			}
		}
		if(getRecordsResponse != null && getRecordsResponse.getGetRecordsResult() != null){
			recordsList = getRecordsResponse.getGetRecordsResult();
		}else{
			recordsList = new ArrayList<GetRecordsResult>();
		}
		return recordsList;
	}
	
	/**
	 * Method to fetch HRTaskParents Notifications.
	 * 
	 * @param model
	 * @param user
	 * @return
	 */
	public String getHRTaskParentsNotification(Model model, com.liferay.portal.model.User user){
		LoggerUtil.debugLogger(logger, "Inside getHRTaskParentsNotification in NotificationController");
		
		String executeCaseResponse = null;
		//List<String> executeCaseResponseList = null;
		/*Reading web service properties file from catalina home - Start */
		Properties props = new Properties();
		String path = System.getProperty("catalina.home")+ File.separator + "web-service.properties";
		try {
			props.load(new FileInputStream(path));
		} catch (Exception e) {
			LoggerUtil.errorLogger(logger, "Caught Exception While loading web service properties",e);
		}
		/*Reading web service properties file from catalina home - End */
		
		final String userName = (String) props.get("coach.cip.notificationwebservice.username");
		final String passWord = (String) props.get("coach.cip.notificationwebservice.password");
		
		LoggerUtil.debugLogger(logger,"Notification Webservice userName------------>"+userName);
		LoggerUtil.debugLogger(logger,"Notification Webservice passWord------------>"+passWord);
		
		Execute caseRequest = WS_HRTASKPARENTS_CLIENT_FACTORY.createExecute();
		if(Validator.isNotNull(user) && Validator.isNotNull(user.getScreenName())){
			caseRequest.setCurrentUser(user.getScreenName());
			//caseRequest.setCurrentUser("mkakaraparthi");
		}
		ExecuteResponse caseResponse = null;
		try{
			caseResponse = (ExecuteResponse) hrtaskParentsWebServiceTemplate.marshalSendAndReceive(caseRequest, new WebServiceMessageCallback() {
			public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
		        CommonsHttpConnection connection =(CommonsHttpConnection) TransportContextHolder.getTransportContext().getConnection();
		        PostMethod postMethod = connection.getPostMethod();
		        String authorization = new sun.misc.BASE64Encoder().encode((userName+":"+passWord).getBytes());
		        postMethod.setRequestHeader( "Authorization", "Basic " + authorization);
		        postMethod.addParameter("Accept", "application/xml");
		    }
		} );
		
		}catch(SoapFaultClientException ex){
			String lpart = null;
			SoapFaultClientException soapException = (SoapFaultClientException)ex;
			lpart = soapException.getFaultCode().getLocalPart().trim();
			if(lpart != null && lpart.equals("Server")){
				model.addAttribute("FaultCode", lpart);
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
			}
			if(lpart != null && lpart.equals("Client")){
				model.addAttribute("FaultCode", lpart);
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_CLIENT);
			}
			if(lpart != null && lpart.equals("MustUnderstand")){
				model.addAttribute("FaultCode", lpart);
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_UNDERSTAND);
			}
			if(lpart != null && lpart.equals("VersionMismatch")){
				model.addAttribute("FaultCode", lpart);
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_VERMISMATCH);
			}
			LoggerUtil.errorLogger(logger, "SoapFaultClientException caught while processing Notification HRTaskParent Service in NotificationController.getHRTaskParentsNotification()", ex);
		}
		catch(Exception ex){
			if(ex instanceof SocketTimeoutException){
				model.addAttribute("FaultMessage", "SocketTimeOutException Caught");
				LoggerUtil.errorLogger(logger, "SocketTimeOutException caught while processing Notification HRTaskParent Service in NotificationController.getHRTaskParentsNotification()", ex);
			} else if(ex instanceof ConnectException){
				model.addAttribute("FaultMessage", "ConnectException Caught");
				LoggerUtil.errorLogger(logger, "ConnectException caught while processing Notification HRTaskParent Service in NotificationController.getHRTaskParentsNotification()", ex);
			} else {
			LoggerUtil.errorLogger(logger, "Exception caught while processing Notification HRTaskParent Service in NotificationController.getHRTaskParentsNotification()", ex);
			}
		}
		
		if(Validator.isNotNull(caseResponse) && Validator.isNotNull(caseResponse.getParents())){
			executeCaseResponse = caseResponse.getParents();
		}else{
			executeCaseResponse = new String("");
		}
		return executeCaseResponse;
	}
	
	/**
	 * Method used to get HRCase Notifications.
	 * 
	 * @param model
	 * @return List<com.coach.notifications.service.beans.GetRecordsResponse.GetRecordsResult>
	 */
	@SuppressWarnings("unused")
	private List<com.coach.notifications.hrcase.service.beans.GetRecordsResponse.GetRecordsResult> getHRCaseNotificationList(com.liferay.portal.model.User user,Model model){
		LoggerUtil.debugLogger(logger, "Inside getHRCaseNotificationList in NotificationController");
		List<com.coach.notifications.hrcase.service.beans.GetRecordsResponse.GetRecordsResult> notificationsList = null;
		
		com.coach.notifications.hrcase.service.beans.GetRecords getRecordsRequestForHRCase = WS_HRCASE_CLIENT_FACTORY.createGetRecords();
		if(Validator.isNotNull(user) && Validator.isNotNull(user.getScreenName())){
			getRecordsRequestForHRCase.setEncodedQuery("assigned_to.user_nameSTARTSWITH"+user.getScreenName().trim());
			//getRecordsRequestForHRCase.setEncodedQuery("assigned_to.user_nameSTARTSWITHmkakaraparthi");
		}
		com.coach.notifications.hrcase.service.beans.GetRecordsResponse getRecordsResponseForHRCase = null;
		
		/*Reading web service properties file from catalina home - Start */
		Properties props = new Properties();
		String path = System.getProperty("catalina.home")+ File.separator + "web-service.properties";
		try {
			props.load(new FileInputStream(path));
		} catch (Exception e) {
			LoggerUtil.errorLogger(logger, "Caught Exception While loading web service properties",e);
		}
		/*Reading web service properties file from catalina home - End */
		
		final String userName = (String) props.get("coach.cip.notificationwebservice.username");
		final String passWord = (String) props.get("coach.cip.notificationwebservice.password");
		Object object = null;
		try {
			object = hrCaseWebServiceTemplate.marshalSendAndReceive(getRecordsRequestForHRCase,new WebServiceMessageCallback() {
			public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
		        CommonsHttpConnection connection =(CommonsHttpConnection) TransportContextHolder.getTransportContext().getConnection();
		        PostMethod postMethod = connection.getPostMethod();
		        String authorization = new sun.misc.BASE64Encoder().encode((userName+":"+passWord).getBytes());
		        postMethod.setRequestHeader( "Authorization", "Basic " + authorization);
		        postMethod.addParameter("Accept", "application/xml");
		    }
		} );
		}catch(SoapFaultClientException ex){
			String lpart = null;
			SoapFaultClientException soapException = (SoapFaultClientException)ex;
			lpart = soapException.getFaultCode().getLocalPart().trim();
			if(lpart != null && lpart.equals("Server")){
				model.addAttribute("FaultCode", lpart);
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
			}
			if(lpart != null && lpart.equals("Client")){
				model.addAttribute("FaultCode", lpart);
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_CLIENT);
			}
			if(lpart != null && lpart.equals("MustUnderstand")){
				model.addAttribute("FaultCode", lpart);
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_UNDERSTAND);
			}
			if(lpart != null && lpart.equals("VersionMismatch")){
				model.addAttribute("FaultCode", lpart);
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_VERMISMATCH);
			}
			LoggerUtil.errorLogger(logger, "SoapFaultClientException caught while processing Notification Web Service in NotificationController.getHRCaseNotificationList()", ex);
		}
		catch(Exception ex){
			if(ex instanceof SocketTimeoutException){
				model.addAttribute("FaultMessage", "SocketTimeOutException Caught");
				LoggerUtil.errorLogger(logger, "SocketTimeOutException caught while processing Notification Web Service in NotificationController.getHRCaseNotificationList()", ex);
			} else if(ex instanceof ConnectException){
				model.addAttribute("FaultMessage", "ConnectException Caught");
				LoggerUtil.errorLogger(logger, "ConnectException caught while processing Notification Web Service in NotificationController.getHRCaseNotificationList()", ex);
			} else {
			LoggerUtil.errorLogger(logger, "Exception caught while processing Notification Web Service in NotificationController.getHRCaseNotificationList()", ex);
			}
		}
		
		LoggerUtil.debugLogger(logger, "object in Notification Controller--------->"+object);
		if(object instanceof com.coach.notifications.hrcase.service.beans.GetRecordsResponse){
			getRecordsResponseForHRCase = (com.coach.notifications.hrcase.service.beans.GetRecordsResponse)object;
			LoggerUtil.debugLogger(logger, "Notification Controller com.coach.notifications.hrcase.service.beans.GetRecordsResponse--------->"+getRecordsResponseForHRCase);
		}
		if(Validator.isNotNull(getRecordsResponseForHRCase) && Validator.isNotNull(getRecordsResponseForHRCase.getGetRecordsResult())){
			notificationsList = getRecordsResponseForHRCase.getGetRecordsResult();
			LoggerUtil.debugLogger(logger, "notificationsList Size in Notification Controller.getHRCaseNotificationList()--------->"+notificationsList.size());
		}else{
			notificationsList = new ArrayList<com.coach.notifications.hrcase.service.beans.GetRecordsResponse.GetRecordsResult>();
		}
		return notificationsList;
	}
		
	/**
	 * Model Object for populating Notification form data.
	 * @param portletRequest
	 * @return List<NotificationVO>
	 */

	@ModelAttribute(value = "notificaitonVoList")
	public List<NotificationVO> getNotification(PortletRequest portletRequest) {

		LoggerUtil.debugLogger(logger, "Command Object or Model Object for populating Notification form data.");
		List<NotificationVO> notificaitonVoList = null;
		List<CINotification> notificationDoList = null;
		try {
			notificationDoList = (List<CINotification>) serviceLocator.getNotificationService().getAllNotifications();
		}
		catch (Exception e) {
			LoggerUtil.errorLogger(logger, "NotificationController.getNotification()", e);
		}
		if (notificationDoList != null && !notificationDoList.isEmpty() && notificationDoList.size() != 0) {
			notificaitonVoList = convertNotificationDOToVOList(notificationDoList);
		}
		else {
			notificaitonVoList = new ArrayList<NotificationVO>();
		}
		LoggerUtil.debugLogger(logger,"notificaitonVoList.size()---------------------------->"+notificaitonVoList.size());
		portletRequest.setAttribute("notificaitonVoList", notificaitonVoList);
		return notificaitonVoList;
	}
	
	
	/**
	 * Method used to process notifications actions.
	 * 
	 * @param request
	 * @param respnse
	 * @throws IOException 
	 */
	@ActionMapping(params = "myaction=callTasksURL")
	public void processNotifications(ActionRequest request, ActionResponse response) throws IOException{
		LoggerUtil.debugLogger(logger, "ActionMapping For processNotifications");
		
		String localNotificationUrl = Validator.isNotNull(request.getParameter("localNotificationUrl")) ? (String)request.getParameter("localNotificationUrl") : "";
		String localNotificationTitle = Validator.isNotNull(request.getParameter("localNotificationTitle")) ? (String)request.getParameter("localNotificationTitle") : "";
		
		String title = Validator.isNotNull(request.getParameter("title")) ? (String)request.getParameter("title") : "";
		String sysid = Validator.isNotNull(request.getParameter("sysid")) ? (String)request.getParameter("sysid") : "";
		
		String caseTitle = Validator.isNotNull(request.getParameter("caseTitle")) ? (String)request.getParameter("caseTitle") : "";
		String caseSysid = Validator.isNotNull(request.getParameter("caseSysid")) ? (String)request.getParameter("caseSysid") : "";
		
		ThemeDisplay themeDisplay =WebUtil.getThemeDisplay(request);
		String redirectUrl =themeDisplay.getPathFriendlyURLPrivateGroup()+themeDisplay.getScopeGroup().getFriendlyURL()+"/notifications";
    	String redirectServiceUrl =themeDisplay.getPathFriendlyURLPrivateGroup()+themeDisplay.getScopeGroup().getFriendlyURL()+"/notificationdetails";
    	LoggerUtil.debugLogger(logger,"callTasksURL processAction in NotificationsController--"+redirectUrl);
    	
    	LoggerUtil.infoLogger(logger, "sysid---in NotificationController.processNotifications()-------------->"+sysid);
    	LoggerUtil.infoLogger(logger, "title---in NotificationController.processNotifications()-------------->"+title);
    	LoggerUtil.infoLogger(logger, "redirectServiceUrl---in NotificationController.processNotifications()-------------->"+redirectServiceUrl);
    	
    	LoggerUtil.infoLogger(logger, "caseTitle---in NotificationController.processNotifications()-------------->"+caseTitle);
    	LoggerUtil.infoLogger(logger, "caseSysid---in NotificationController.processNotifications()-------------->"+caseSysid);
    	
    	if(Validator.isNotNull(localNotificationUrl)){
    		HashMap<String, String> notificationsMap = new HashMap<String, String>();
    		notificationsMap.put("localNotificationUrl", localNotificationUrl);
    		notificationsMap.put("localNotificationtitle", localNotificationTitle);
        	QName qName=new QName("http://coach.com/events", "ipc.taskURL");
        	response.setEvent(qName,notificationsMap);
    		response.sendRedirect(redirectUrl);
    	}
    	if(Validator.isNotNull(sysid)){
    		LoggerUtil.infoLogger(logger, "sysid if check---in NotificationController.processNotifications()-------------->"+sysid);
    		HashMap<String, String> notificationDetailMap = new HashMap<String, String>();
    		notificationDetailMap.put("title", title);
    		notificationDetailMap.put("sysid", sysid);
        	QName qName=new QName("http://coach.com/events", "ipc.taskURL");
        	response.setEvent(qName,notificationDetailMap);
    		response.sendRedirect(redirectServiceUrl);
    	}
    	if(Validator.isNotNull(caseSysid)){
    		HashMap<String, String> caseNotificationMap = new HashMap<String, String>();
    		caseNotificationMap.put("caseTitle", caseTitle);
    		caseNotificationMap.put("caseSysid", caseSysid);
        	QName qName=new QName("http://coach.com/events", "ipc.taskURL");
        	response.setEvent(qName,caseNotificationMap);
    		response.sendRedirect(redirectUrl);
    	}
    	
	}
	
	
	/**
	 * Helper method for converting List of Notification Domain Object to
	 * Value Object.
	 * @param ciNotification
	 * @return List<NotificationVO>
	 */
	private List<NotificationVO> convertNotificationDOToVOList(List<CINotification> ciNotification) {
		List<NotificationVO>  notificaitonVoList = new ArrayList<NotificationVO>();
		for (CINotification notification : ciNotification) {
			notificaitonVoList.add(convertNotificationDOToVO(notification));
		}
		return notificaitonVoList;
	}

	/**
	 * Helper method for converting Notification Domain Object to Value
	 * Object.
	 * @param ciNotification
	 * @return NotificationVO
	 */
	private NotificationVO convertNotificationDOToVO(CINotification ciNotification) {

		NotificationVO notificaitonVo = new NotificationVO();
		Format formatter = new SimpleDateFormat("MM/dd/yyyy");
		notificaitonVo.setCreateDate(ciNotification.getCreateDate());
		notificaitonVo.setDisplayDate(formatter.format(ciNotification.getDisplayDate()));
		notificaitonVo.setExpirationDate(formatter.format(ciNotification.getExpirationDate()));
		notificaitonVo.setNotificationDescription(ciNotification.getNotificationDescription());
		notificaitonVo.setNotificationForLocations(convertNotificationForLocationDOToVO(ciNotification.getNotificationForLocation()));
		notificaitonVo.setNotificationForRoles(convertRoleDOToVO(ciNotification.getNotificationForRoles()));
		notificaitonVo.setNotificationForUsers(convertUserDOToVO(ciNotification.getNotificationForUsers()));		
		notificaitonVo.setNotificationId(ciNotification.getNotificationId());
		notificaitonVo.setNotificationType(ciNotification.getNotificationType());
		notificaitonVo.setModifiedDate(ciNotification.getModifiedDate());
		notificaitonVo.setNotificationSubject(ciNotification.getNotificationSubject());
		notificaitonVo.setUserByCreatedBy(convertUserDOToVO(ciNotification.getUserByCreatedBy()));
		notificaitonVo.setUserByModifiedBy(convertUserDOToVO(ciNotification.getUserByModifiedBy()));
		notificaitonVo.setNotificationUrl(ciNotification.getUrl());

		return notificaitonVo;
	}
	
	/**
	 * Helper method for converting Notification For Location Domain Object to Value
	 * Object.
	 * @param notificationForLocation
	 * @return Set<NotificationForLocationVO>
	 */
	private Set<NotificationForLocationVO> convertNotificationForLocationDOToVO(Set<CINotificationForLocation> notificationForLocation) {

		Set<NotificationForLocationVO> notificationForLocationVOSet = new HashSet<NotificationForLocationVO>();
		for (CINotificationForLocation ciNotificationForLocation : notificationForLocation) {
			NotificationForLocationVO notificationForLocationVO = new NotificationForLocationVO();
			notificationForLocationVO.setCinflId(ciNotificationForLocation.getCinflId());
			notificationForLocationVO.setLocationType(ciNotificationForLocation.getLocationType());
			notificationForLocationVO.setLocationName(ciNotificationForLocation.getLocationName());
			notificationForLocationVO.setState(ciNotificationForLocation.getState());
			notificationForLocationVO.setCity(ciNotificationForLocation.getCity());
			notificationForLocationVO.setOffice(ciNotificationForLocation.getOffice());
			notificationForLocationVOSet.add(notificationForLocationVO);
		}
		return notificationForLocationVOSet;
	}

	/**
	 * Helper method for converting Role Domain Object to Value Object.
	 * @param role
	 * @return Set<RoleVO>
	 */
	private Set<RoleVO> convertRoleDOToVO(Set<Role> role) {

		Set<RoleVO> roleVOSet = new HashSet<RoleVO>();
		for (Role rle : role) {
			RoleVO roleVO = new RoleVO();
			roleVO.setClassNameId(rle.getClassNameId());
			roleVO.setClassPk(rle.getClassPk());
			roleVO.setCompanyId(rle.getCompanyId());
			roleVO.setDescription(rle.getDescription());
			roleVO.setName(rle.getName());
			roleVO.setRoleId(rle.getRoleId());
			roleVO.setSubtype(rle.getSubtype());
			roleVO.setTitle(rle.getTitle());
			roleVO.setType(rle.getType());

			roleVOSet.add(roleVO);
		}
		return roleVOSet;
	}

	/**
	 * Helper method for converting User Domain Object to Value Object.
	 * @param user
	 * @return Set<UserVO>
	 */
	
	private Set<UserVO> convertUserDOToVO(Set<User> user) {

		Set<UserVO> userVOSet = new HashSet<UserVO>();
		for (User usr : user) {
			UserVO userVo = new UserVO();
			
			userVo.setCompanyId(usr.getCompanyId());
			userVo.setUserId(usr.getUserId());
			userVo.setFirstName(usr.getFirstName());
			userVo.setMiddleName(usr.getMiddleName());
			userVo.setLanguageId(usr.getLanguageId());
			userVo.setLastName(usr.getLastName());
			userVo.setJobTitle(usr.getJobTitle());
			userVo.setEmailAddress(usr.getEmailAddress());
			userVo.setScreenName(usr.getScreenName());
	
			
			userVOSet.add(userVo);
		}
		return userVOSet;
	}
	
	/**
	 * Helper method for converting User Domain Object to Value Object.
	 * @param user
	 * @return UserVO
	 */
	private UserVO convertUserDOToVO(User user) {

		UserVO userVO = new UserVO();

		userVO.setUserId(user.getUserId());
		userVO.setUuid(user.getUuid());
		userVO.setCompanyId(user.getCompanyId());
		userVO.setCreateDate(user.getCreateDate());
		userVO.setModifiedDate(user.getModifiedDate());
		userVO.setDefaultUser(user.getDefaultUser());
		userVO.setContactId(user.getContactId());
		userVO.setPassword(user.getPassword());
		userVO.setPasswordEncrypted(user.getPasswordEncrypted());
		userVO.setPasswordReset(user.getPasswordReset());
		userVO.setPasswordModifiedDate(user.getPasswordModifiedDate());
		userVO.setDigest(user.getDigest());
		userVO.setReminderQueryQuestion(user.getReminderQueryQuestion());
		userVO.setReminderQueryAnswer(user.getReminderQueryAnswer());
		userVO.setGraceLoginCount(user.getGraceLoginCount());
		userVO.setScreenName(user.getScreenName());
		userVO.setEmailAddress(user.getEmailAddress());
		userVO.setFacebookId(user.getFacebookId());
		userVO.setOpenId(user.getOpenId());
		userVO.setPortraitId(user.getPortraitId());
		userVO.setLanguageId(user.getLanguageId());
		userVO.setTimeZoneId(user.getTimeZoneId());
		userVO.setGreeting(user.getGreeting());
		userVO.setComments(user.getComments());
		userVO.setFirstName(user.getFirstName());
		userVO.setMiddleName(user.getMiddleName());
		userVO.setLastName(user.getLastName());
		userVO.setJobTitle(user.getJobTitle());
		userVO.setLoginDate(user.getLoginDate());
		userVO.setLoginIp(user.getLoginIp());
		userVO.setLastLoginDate(user.getLastLoginDate());
		userVO.setLastLoginIp(user.getLastLoginIp());
		userVO.setLastFailedLoginDate(user.getLastFailedLoginDate());
		userVO.setFailedLoginAttempts(user.getFailedLoginAttempts());
		userVO.setLockout(user.getLockout());
		userVO.setLockoutDate(user.getLockoutDate());
		userVO.setAgreedToTermsOfUse(user.getAgreedToTermsOfUse());
		userVO.setEmailAddressVerified(user.getEmailAddressVerified());
		userVO.setStatus(user.getStatus());
		userVO.setAdminAssistSet(user.getCIEmployeesForAdminAssistId());
		userVO.setUserMetricsSet(user.getCIUserMetrics());
		userVO.setQuickLinksSet(user.getCIQuickLinks());
		userVO.setNotificationsCreatorSet(user.getCINotificationForCreatedBy());
		userVO.setNotificationsModifierSet(user.getCINotificationForModifiedBy());
		userVO.setSupervisorSet(user.getCIEmployeesForSupervisorId());
		userVO.setStoreEmployeesSet(user.getCIStoreEmployees());
		userVO.setUserSet(user.getCIEmployeesForUserId());

		return userVO;
	}

}
