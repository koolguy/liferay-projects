
package com.coach.cip.common.dto;

import java.io.Serializable;

/**
 * Class is used to capture the characteristics of notificationForLocations. This is
 * a reusable class for notificationForLocations.
 * 
 * @author GalaxE.
 */
public class NotificationForLocationVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5499853124483683630L;
	private Long cinflId;
	private NotificationVO notification;
	private String locationType;
	private String locationName;
	private String state;
	private String city;
	private String office;

	/**
	 * Method used to get cinflId.
	 * @return cinflId.
	 */
	public Long getCinflId() {

		return cinflId;
	}

	/**
	 * Method used to set cinflId.
	 * @param cinflId
	 *            .
	 */
	public void setCinflId(Long cinflId) {

		this.cinflId = cinflId;
	}

	/**
	 * Method used to get notification.
	 * @return the notification.
	 */
	public NotificationVO getNotification() {

		return notification;
	}

	/**
	 * Method used to set notification.
	 * @param notification
	 *            .
	 */
	public void setNotification(NotificationVO notification) {

		this.notification = notification;
	}

	/**
	 * Method used to get locationType.
	 * @return the locationType.
	 */
	public String getLocationType() {

		return locationType;
	}

	/**
	 * Method used to set locationType.
	 * @param locationType
	 *            .
	 */
	public void setLocationType(String locationType) {

		this.locationType = locationType;
	}

	/**
	 * Method used to get locationName.
	 * @return locationName.
	 */
	public String getLocationName() {

		return locationName;
	}

	/**
	 * Method used to set locationName.
	 * @param locationName
	 *            .
	 */
	public void setLocationName(String locationName) {

		this.locationName = locationName;
	}

	/**
	 * Method used get state.
	 * @return state.
	 */
	public String getState() {

		return state;
	}

	/**
	 * Method used to set state.
	 * @param state
	 *            .
	 */
	public void setState(String state) {

		this.state = state;
	}

	/**
	 * Method used to get city.
	 * @return city.
	 */
	public String getCity() {

		return city;
	}

	/**
	 * Method used to set city.
	 * @param city
	 *            .
	 */
	public void setCity(String city) {

		this.city = city;
	}

	/**
	 * Method used to get office.
	 * @return office.
	 */
	public String getOffice() {

		return office;
	}

	/**
	 * Method used to set office.
	 * @param office
	 *            .
	 */
	public void setOffice(String office) {

		this.office = office;
	}

}
