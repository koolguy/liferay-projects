package com.coach.cip.portlets.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.xml.transform.TransformerException;
import org.apache.commons.httpclient.methods.PostMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;
import org.springframework.ws.transport.context.TransportContextHolder;
import org.springframework.ws.transport.http.CommonsHttpConnection;
import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;


/**
 * Class is used for tasks operations.
 * 
 * @author GalaxE.
 */

@Controller("tasksController")
@RequestMapping(value = "VIEW")
public class TasksController extends AbstractBaseController{

	/** The Constant LOG. */
	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(TasksController.class);
	private static final String TASKS = "tasks";
	
	@Autowired
	@Qualifier("notificationsHRTaskServiceTemplate")
	private WebServiceTemplate hrTaskwebServiceTemplate;
	
	private static final com.coach.notifications.hrtask.service.beans.ObjectFactory WS_HRTASK_CLIENT_FACTORY = new com.coach.notifications.hrtask.service.beans.ObjectFactory();
	/**
	 * Method used to render tasks view.
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RenderMapping
	public String showTasksView(RenderRequest request,RenderResponse response, Model model) {
		LoggerUtil.debugLogger(logger,"Render mapping for tasks view in TasksController");
		
		com.liferay.portal.model.User user = (com.liferay.portal.model.User)request.getAttribute(WebKeys.USER);
		LoggerUtil.debugLogger(logger, "user from notification Controller--------------->"+user.getEmailAddress()+"---"+user.getScreenName());
		
		PortletSession notificationsSession = request.getPortletSession();
		String localNotificationUrl = Validator.isNotNull(notificationsSession.getAttribute("localNotificationUrl")) ? (String)notificationsSession.getAttribute("localNotificationUrl") : "";
		String localNotificationtitle = Validator.isNotNull(notificationsSession.getAttribute("localNotificationtitle")) ? (String)notificationsSession.getAttribute("localNotificationtitle") : "";
		
		String caseTitle = Validator.isNotNull(notificationsSession.getAttribute("caseTitle")) ? (String) notificationsSession.getAttribute("caseTitle") : "";
		String caseSysid = Validator.isNotNull(notificationsSession.getAttribute("caseSysid")) ? (String) notificationsSession.getAttribute("caseSysid") : "";
		
		com.coach.notifications.hrtask.service.beans.UpdateResponse hrTaskUpatedResponse = Validator.isNotNull(notificationsSession.getAttribute("hrTaskUpatedResponse")) ? (com.coach.notifications.hrtask.service.beans.UpdateResponse) notificationsSession.getAttribute("hrTaskUpatedResponse") : null;
		
		model.addAttribute("localNotificationUrl", localNotificationUrl);
		model.addAttribute("localNotificationtitle", localNotificationtitle);
		model.addAttribute("caseTitle", caseTitle);
		model.addAttribute("caseSysid", caseSysid);
		model.addAttribute("hrTaskUpatedResponse", hrTaskUpatedResponse);
		
		List<com.coach.notifications.hrtask.service.beans.GetRecordsResponse.GetRecordsResult> hrTaskRecordslist = getHRTaskforCaseSysId(caseSysid, model, user);
		
		model.addAttribute("hrTaskRecordslist", hrTaskRecordslist);
		
		notificationsSession.setAttribute("localNotificationUrl", null);
		notificationsSession.setAttribute("localNotificationtitle", null);
		notificationsSession.setAttribute("caseTitle", null);
		notificationsSession.setAttribute("caseSysid", null);
		notificationsSession.setAttribute("hrTaskUpatedResponse", null);
		return TASKS;
	}
	
	/**
	 * Method to fetch hrTaskRecords.
	 * 
	 * @param caseSysid
	 * @param model
	 * @return List<com.coach.notifications.hrtask.service.beans.GetRecordsResponse.GetRecordsResult>
	 */
	private List<com.coach.notifications.hrtask.service.beans.GetRecordsResponse.GetRecordsResult> getHRTaskforCaseSysId(String caseSysid, Model model, com.liferay.portal.model.User user) {
		
		List<com.coach.notifications.hrtask.service.beans.GetRecordsResponse.GetRecordsResult> hrTaskRecordslist = null;
		/*Reading web service properties file from catalina home - Start */
		Properties props = new Properties();
		String path = System.getProperty("catalina.home")+ File.separator + "web-service.properties";
		try {
			props.load(new FileInputStream(path));
		} catch (Exception e) {
			LoggerUtil.errorLogger(logger, "Caught Exception While loading web service properties",e);
		}
		/*Reading web service properties file from catalina home - End */
		
		final String userName = (String) props.get("coach.cip.notificationwebservice.username");
		final String passWord = (String) props.get("coach.cip.notificationwebservice.password");
		
		com.coach.notifications.hrtask.service.beans.GetRecords hrTaskRecords= WS_HRTASK_CLIENT_FACTORY.createGetRecords();
		if(Validator.isNotNull(caseSysid) && Validator.isNotNull(user) && Validator.isNotNull(user.getScreenName())){
			hrTaskRecords.setEncodedQuery("parent="+caseSysid+"^"+"assigned_to.user_nameSTARTSWITH"+user.getScreenName().trim());
			//hrTaskRecords.setEncodedQuery("mkakaraparthi");
		}
		LoggerUtil.infoLogger(logger, "hrTaskRecords.getEncodedQuery() in TasksController.getHRTaskforCaseSysId()------->"+hrTaskRecords.getEncodedQuery());
		com.coach.notifications.hrtask.service.beans.GetRecordsResponse hrTaskRecordsResponse = null;
		Object object = null;
		try{
			object = hrTaskwebServiceTemplate.marshalSendAndReceive(hrTaskRecords, new WebServiceMessageCallback() {
			public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
		        CommonsHttpConnection connection =(CommonsHttpConnection) TransportContextHolder.getTransportContext().getConnection();
		        PostMethod postMethod = connection.getPostMethod();
		        String authorization = new sun.misc.BASE64Encoder().encode((userName+":"+passWord).getBytes());
		        postMethod.setRequestHeader( "Authorization", "Basic " + authorization);
		        postMethod.addParameter("Accept", "application/xml");
		    }
		} );
		}catch(SoapFaultClientException ex){
			String lpart = null;
			SoapFaultClientException soapException = (SoapFaultClientException)ex;
			lpart = soapException.getFaultCode().getLocalPart().trim();
			if(lpart != null && lpart.equals("Server")){
				model.addAttribute("FaultCode", lpart);
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
			}
			if(lpart != null && lpart.equals("Client")){
				model.addAttribute("FaultCode", lpart);
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_CLIENT);
			}
			if(lpart != null && lpart.equals("MustUnderstand")){
				model.addAttribute("FaultCode", lpart);
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_UNDERSTAND);
			}
			if(lpart != null && lpart.equals("VersionMismatch")){
				model.addAttribute("FaultCode", lpart);
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_VERMISMATCH);
			}
			LoggerUtil.errorLogger(logger, "SoapFaultClientException caught while processing Notification Web Service in TasksController.getHRTaskforCaseSysId()", ex);
		}
		catch(Exception ex){
			if(ex instanceof SocketTimeoutException){
				model.addAttribute("FaultMessage", "SocketTimeOutException Caught");
				LoggerUtil.errorLogger(logger, "SocketTimeOutException caught while processing Notification Web Service in TasksController.getHRTaskforCaseSysId()", ex);
			} else if(ex instanceof ConnectException){
				model.addAttribute("FaultMessage", "ConnectException Caught");
				LoggerUtil.errorLogger(logger, "ConnectException caught while processing Notification Web Service in TasksController.getHRTaskforCaseSysId()", ex);
			}  else {
			LoggerUtil.errorLogger(logger, "Exception caught while processing Notification Web Service in TasksController.getHRTaskforCaseSysId()", ex);
			}
		}
		if(Validator.isNotNull(object) && object instanceof com.coach.notifications.hrtask.service.beans.GetRecordsResponse){
			hrTaskRecordsResponse = (com.coach.notifications.hrtask.service.beans.GetRecordsResponse)object;
		}
		if(Validator.isNotNull(hrTaskRecordsResponse)&& Validator.isNotNull(hrTaskRecordsResponse.getGetRecordsResult())){
			hrTaskRecordslist = hrTaskRecordsResponse.getGetRecordsResult();
		} else {
			hrTaskRecordslist = new ArrayList<com.coach.notifications.hrtask.service.beans.GetRecordsResponse.GetRecordsResult>();
		}
		return hrTaskRecordslist;
	}

	/**
	 * Method used to process notifications events.
	 *  
	 * @param request
	 * @param response
	 */
	@SuppressWarnings("unchecked")
	@EventMapping(value="{http://coach.com/events}ipc.taskURL")
	public void processTasks(EventRequest request, EventResponse response){
		LoggerUtil.debugLogger(logger,"Event mapping for tasks to process notifications in TasksController");
		Event event= request.getEvent();
		PortletSession notificationsSession = request.getPortletSession();
		
		String localNotificationUrl = "";
		String localNotificationtitle = "";
		
		String caseTitle = "";
		String caseSysid = "";
		
    	HashMap<String, String> notificationsMap = (HashMap<String, String>)event.getValue();
    	if(Validator.isNotNull(notificationsMap)){
    		localNotificationUrl = Validator.isNotNull(notificationsMap.get("localNotificationUrl")) ? (String) notificationsMap.get("localNotificationUrl") : "";
    		localNotificationtitle = Validator.isNotNull(notificationsMap.get("localNotificationtitle")) ? (String) notificationsMap.get("localNotificationtitle") : "";
    		
    		LoggerUtil.debugLogger(logger,"localNotificationUrl -- Event mapping for tasks to process notifications in TasksController----"+localNotificationUrl);
    		LoggerUtil.debugLogger(logger,"localNotificationtitle -- Event mapping for tasks to process notifications in TasksController----"+localNotificationtitle);
    		
    		caseTitle = Validator.isNotNull(notificationsMap.get("caseTitle")) ? (String) notificationsMap.get("caseTitle") : "";
    		caseSysid = Validator.isNotNull(notificationsMap.get("caseSysid")) ? (String) notificationsMap.get("caseSysid") : "";
    		
    		LoggerUtil.debugLogger(logger,"caseSysid -- Event mapping for tasks to process notifications in TasksController----"+caseSysid);
    		LoggerUtil.debugLogger(logger,"caseTitle -- Event mapping for tasks to process notifications in TasksController----"+caseTitle);
    		
    	}
    	notificationsSession.setAttribute("localNotificationUrl", localNotificationUrl);
    	notificationsSession.setAttribute("localNotificationtitle", localNotificationtitle);
    	notificationsSession.setAttribute("caseTitle", caseTitle);
    	notificationsSession.setAttribute("caseSysid", caseSysid);
	}
	
	/**
	 * Action Mapping to process hr task completed links.
	 * 
	 * @param actionRequest
	 * @param actionResponse
	 */
	@ActionMapping(params = "myaction=callTasksCompletedURL")
	public void processTaskCompletedStatus(ActionRequest actionRequest, ActionResponse actionResponse, Model model){
		String caseSysid =  Validator.isNotNull(actionRequest.getParameter("caseSysid")) ? actionRequest.getParameter("caseSysid") : "";
		String caseTitle =  Validator.isNotNull(actionRequest.getParameter("caseTitle")) ? actionRequest.getParameter("caseTitle") : "";
		String taskSysId = Validator.isNotNull(actionRequest.getParameter("taskSysId")) ? actionRequest.getParameter("taskSysId") : "";
		com.coach.notifications.hrtask.service.beans.UpdateResponse hrTaskUpatedResponse = processHRTaskCompletedURL(model,taskSysId);
		PortletSession notificationsSession = actionRequest.getPortletSession();
		notificationsSession.setAttribute("caseSysid", caseSysid);
		notificationsSession.setAttribute("caseTitle", caseTitle);
		notificationsSession.setAttribute("hrTaskUpatedResponse", hrTaskUpatedResponse);
		
	}
	
	/**
	 * Helper Method to process HRTaskCompletedURL.
	 * 
	 * @param model
	 * @param caseSysid
	 */
	private com.coach.notifications.hrtask.service.beans.UpdateResponse processHRTaskCompletedURL(Model model, String taskSysId) {
		/*Reading web service properties file from catalina home - Start */
		Properties props = new Properties();
		String path = System.getProperty("catalina.home")+ File.separator + "web-service.properties";
		try {
			props.load(new FileInputStream(path));
		} catch (Exception e) {
			LoggerUtil.errorLogger(logger, "Caught Exception While loading web service properties",e);
		}
		/*Reading web service properties file from catalina home - End */
		
		final String userName = (String) props.get("coach.cip.notificationwebservice.username");
		final String passWord = (String) props.get("coach.cip.notificationwebservice.password");
		
		com.coach.notifications.hrtask.service.beans.Update hrTaskUpdate = WS_HRTASK_CLIENT_FACTORY.createUpdate();
		hrTaskUpdate.setSysId(taskSysId);
		com.coach.notifications.hrtask.service.beans.UpdateResponse hrTaskUpdateResponse = null;
		Object object = null;
		try{
			object = hrTaskwebServiceTemplate.marshalSendAndReceive(hrTaskUpdate, new WebServiceMessageCallback() {
			public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
		        CommonsHttpConnection connection =(CommonsHttpConnection) TransportContextHolder.getTransportContext().getConnection();
		        PostMethod postMethod = connection.getPostMethod();
		        String authorization = new sun.misc.BASE64Encoder().encode((userName+":"+passWord).getBytes());
		        postMethod.setRequestHeader( "Authorization", "Basic " + authorization);
		        postMethod.addParameter("Accept", "application/xml");
		    }
		} );
		}catch(SoapFaultClientException ex){
			String lpart = null;
			SoapFaultClientException soapException = (SoapFaultClientException)ex;
			lpart = soapException.getFaultCode().getLocalPart().trim();
			if(lpart != null && lpart.equals("Server")){
				model.addAttribute("FaultCode", lpart);
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
			}
			if(lpart != null && lpart.equals("Client")){
				model.addAttribute("FaultCode", lpart);
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_CLIENT);
			}
			if(lpart != null && lpart.equals("MustUnderstand")){
				model.addAttribute("FaultCode", lpart);
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_UNDERSTAND);
			}
			if(lpart != null && lpart.equals("VersionMismatch")){
				model.addAttribute("FaultCode", lpart);
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_VERMISMATCH);
			}
			LoggerUtil.errorLogger(logger, "SoapFaultClientException caught while processing Notification Web Service in TasksController.processHRTaskCompletedURL()", ex);
		}
		catch(Exception ex){
			if(ex instanceof SocketTimeoutException){
				model.addAttribute("FaultMessage", "SocketTimeOutException Caught");
				LoggerUtil.errorLogger(logger, "SocketTimeOutException caught while processing Notification Web Service in TasksController.processHRTaskCompletedURL()", ex);
			} else if(ex instanceof ConnectException){
				model.addAttribute("FaultMessage", "ConnectException Caught");
				LoggerUtil.errorLogger(logger, "ConnectException caught while processing Notification Web Service in TasksController.processHRTaskCompletedURL()", ex);
			} else {
			LoggerUtil.errorLogger(logger, "Exception caught while processing Notification Web Service in TasksController.processHRTaskCompletedURL()", ex);
			}
		}
		
		if(Validator.isNotNull(object) &&  object instanceof com.coach.notifications.hrtask.service.beans.UpdateResponse){
			hrTaskUpdateResponse = (com.coach.notifications.hrtask.service.beans.UpdateResponse)object;
		}
		return hrTaskUpdateResponse;
		
	}

}
