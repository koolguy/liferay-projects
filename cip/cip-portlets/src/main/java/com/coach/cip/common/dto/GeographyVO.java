package com.coach.cip.common.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Class is used to capture the characteristics of geographies.
 * 
 * @author GalaxE.
 *
 */
public class GeographyVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3238276700108390386L;
	private Long geographyId;
	private String geographyName;
	private Set<CountryVO> geographyCountries = new HashSet<CountryVO>(0);
	
	/**
	 * Method used to get geographyId.
	 * @return the geographyId
	 */
	public Long getGeographyId() {
		return geographyId;
	}
	/**
	 * Method used to set geographyId.
	 * @param geographyId.
	 */
	public void setGeographyId(Long geographyId) {
		this.geographyId = geographyId;
	}
	/**
	 * Method used to get geographyName.
	 * @return the geographyName.
	 */
	public String getGeographyName() {
		return geographyName;
	}
	/**
	 * Method used to set geographyName.
	 * @param geographyName.
	 */
	public void setGeographyName(String geographyName) {
		this.geographyName = geographyName;
	}
	
	/**
	 * Method used get geographyCountries.
	 * @return the geographyCountries Set.
	 */
	public Set<CountryVO> getGeographyCountries() {
		return geographyCountries;
	}
	/**
	 * Method used to set geographyCountries.
	 * @param geographyCountries Set.
	 */
	public void setGeographyCountries(Set<CountryVO> geographyCountries) {
		this.geographyCountries = geographyCountries;
	}

}
