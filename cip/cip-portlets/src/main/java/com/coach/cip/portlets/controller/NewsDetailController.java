
package com.coach.cip.portlets.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.WindowState;
import javax.portlet.WindowStateException;
import javax.xml.namespace.QName;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.coach.cip.util.WebUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.xml.Document;
import com.liferay.portal.kernel.xml.DocumentException;
import com.liferay.portal.kernel.xml.Node;
import com.liferay.portal.kernel.xml.SAXReaderUtil;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.asset.model.AssetCategory;
import com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;

/**
 * Controller class for NewsDetail.
 * @author Galaxe.
 */
@Controller("newsDetailController")
@RequestMapping(value = "VIEW")
public class NewsDetailController extends AbstractBaseController {

	/** The Constant logger. */
	private static final Log logger = LogFactoryUtil.getLog(NewsDetailController.class);

	/**
	 * Event Mapping Method for docMap type event. It receives doc map in event
	 * payload which has information about document which need to be rendered.
	 * @param eventRequest
	 * @param eventResponse
	 * @param model
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@EventMapping(value = "{http://coach.com/events}ipc.docMap")
	public void showOnlyDoc(EventRequest eventRequest, EventResponse eventResponse, Model model) throws IOException {

		LoggerUtil.debugLogger(logger, "Start of showOnlyDoc() method of NewsDetailController::");
		Event event = eventRequest.getEvent();
		HashMap<String, Object> docMap = (HashMap<String, Object>) event.getValue();
		if (Validator.isNotNull(docMap)) {

			String documentPath = (String) docMap.get("documentPath");
			String portletTitle = (String) docMap.get("portletTitle");
			PortletSession portletSession = eventRequest.getPortletSession();
			Map<String, Object> portletSessionMap = (Map<String, Object>) portletSession.getAttribute("portletSessionMap");
			portletSessionMap = Validator.isNotNull(portletSessionMap) ? portletSessionMap : new HashMap<String, Object>();

			portletSessionMap.put("documentPath", documentPath);
			portletSessionMap.put("portletTitle", portletTitle);
			portletSession.setAttribute("portletSessionMap", portletSessionMap);
			LoggerUtil.debugLogger(logger, "ipc.docMap event triggered");
		}
		LoggerUtil.debugLogger(logger, "End of showOnlyDoc() method of NewsDetailController::");
	}

	/**
	 * Default Render Mapping Method. If Specific Render mapping is not found,
	 * this method is invoked.
	 * @param renderRequest
	 * @param renderResponse
	 * @param model
	 * @return view name string
	 * @throws PortalException
	 * @throws SystemException
	 * @throws UnsupportedEncodingException 
	 */
	@SuppressWarnings("unchecked")
	@RenderMapping
	public String showNewsDetail(RenderRequest renderRequest, RenderResponse renderResponse, Model model) throws PortalException, SystemException, UnsupportedEncodingException {

		LoggerUtil.debugLogger(logger, "Start of showNewsDetail() method of NewsDetailController::");
		String urlName = WebUtil.getThemeDisplay(renderRequest).getLayout().getFriendlyURL();
		String pageName = WebUtil.getThemeDisplay(renderRequest).getLayout().getHTMLTitle(WebUtil.getThemeDisplay(renderRequest).getLocale());
		String vocabName = createPageName(renderRequest, urlName);
		/*
		 * if (pageName.contains("NEWS")) { pageName = pageName.replace("NEWS",
		 * ""); }
		 */
		LoggerUtil.debugLogger(logger, "vocabName is " + vocabName);
		LoggerUtil.debugLogger(logger, "current url is" + PortalUtil.getCurrentURL(renderRequest));
		boolean interPageIpcFlag = false;
		String defaultYear = new SimpleDateFormat("yyyy").format(new Date());
		String interPageParam = WebUtil.getHttpServletRequest(renderRequest).getParameter("interPage");
		PortletSession portletSession = renderRequest.getPortletSession();
		portletSession.setAttribute("vocabName", vocabName);
		Map<String, Object> portletSessionMap = (Map<String, Object>) portletSession.getAttribute("portletSessionMap");
		portletSessionMap = Validator.isNotNull(portletSessionMap) ? portletSessionMap : new HashMap<String, Object>();
		boolean showOnlyDocFlag = false;
		String showOnlyDoc = WebUtil.getHttpServletRequest(renderRequest).getParameter("showOnlyDoc");
		if (Validator.isNotNull(showOnlyDoc)) {
			try {
				showOnlyDocFlag = Boolean.parseBoolean(showOnlyDoc);
			}
			catch (NumberFormatException ne) {
				LoggerUtil.debugLogger(logger, "Parameter is not coming boolean from request, value is " + showOnlyDoc);
				showOnlyDocFlag = false;
			}
		}
		if (showOnlyDocFlag && Validator.isNotNull(portletSessionMap)) {
			String documentPath = (String) portletSessionMap.get("documentPath");
			String portletTitle = (String) portletSessionMap.get("portletTitle");
			model.addAttribute("documentPath", documentPath);
			renderResponse.setTitle(portletTitle.toUpperCase());
			return Constants.NEWS_ATTACHMENT_JSP;
		}
		if (Validator.isNotNull(interPageParam)) {
			try {
				interPageIpcFlag = Boolean.parseBoolean(interPageParam);
			}
			catch (NumberFormatException ne) {
				LoggerUtil.debugLogger(logger, "Parameter is not coming boolean from request, value is " + interPageParam);
				interPageIpcFlag = false;
			}
		}
		// IPC Event Request is coming from other page.
		if (interPageIpcFlag) {
			JournalArticle journalArticle = null;
			if (Validator.isNotNull(portletSessionMap) && Validator.isNotNull(portletSessionMap.get("newsListByInterPageEvent"))) {
				journalArticle = ((List<JournalArticle>) portletSessionMap.get("newsListByInterPageEvent")).get(0);
			}
			Map<Object, Object> articleDataMap = null;
			/*if current vocabulary is either of "EXECUTIVE CORNER" or "VISION AND VALUE" then image should not be displayed in detail section of news*/
			boolean imageDisplay = true;
			if(vocabName.equalsIgnoreCase(Constants.EXECUTIVE_CORNER) || vocabName.equalsIgnoreCase(Constants.VISION_AND_VALUES)){
				imageDisplay = false;
			}
			if (Validator.isNotNull(journalArticle)) {
				articleDataMap = getArticleStructureData(renderRequest, journalArticle);
				renderResponse.setTitle(journalArticle.getTitle(WebUtil.getThemeDisplay(renderRequest).getLanguageId()).toUpperCase());
				if (Validator.isNotNull(articleDataMap) &&
						((Validator.isNull(articleDataMap.get("imageUrl")) || !imageDisplay)&& Validator.isNull(articleDataMap.get("content")))) {
					List<String> docUrList = (List<String>) (articleDataMap.get("docUrList"));
					if (Validator.isNotNull(docUrList) && docUrList.size() != 0) {
						String documentPath = docUrList.get(0);
						if (Validator.isNotNull(documentPath)) {
							//documentPath = documentPath.replace("+", " ");
							documentPath = URLEncoder.encode(documentPath, "UTF-8");
						}
						model.addAttribute("documentPath", documentPath);
						model.addAttribute("redirect", (String) portletSessionMap.get("redirectUrl"));
						return Constants.NEWS_ATTACHMENT_JSP;
					}
				}
			}
			if (Validator.isNotNull(journalArticle)) {
				model.addAttribute("journalArticleId", String.valueOf(journalArticle.getId()));
				SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy", WebUtil.getThemeDisplay(renderRequest).getLocale());
				model.addAttribute("displayDateStr", sdf.format(journalArticle.getDisplayDate()));
				model.addAttribute("vocabName", vocabName);
				renderResponse.setTitle(journalArticle.getTitle(WebUtil.getThemeDisplay(renderRequest).getLanguageId()).toUpperCase());
			}
			return Constants.NEWS_FULL_DETAIL_JSP;
		}
		Boolean paginationTraversal = false;
		String curParamFrmPagination = renderRequest.getParameter("cur");
		String deltaParamFrmPagination = renderRequest.getParameter("delta");
		// checking for cur parameter presence in url
		if (Validator.isNotNull(curParamFrmPagination)) {
			LoggerUtil.debugLogger(logger, "cur value is " + curParamFrmPagination);
			try {
				if (Integer.parseInt(curParamFrmPagination) >= 1) {
					paginationTraversal = true;
				}
			}
			catch (NumberFormatException e) {
				LoggerUtil.debugLogger(logger, "curParamFrmPagination is not a number");
				paginationTraversal = false;
			}
		}
		// checking for delta parameter presence in url
		else if (Validator.isNotNull(deltaParamFrmPagination)) {
			LoggerUtil.debugLogger(logger, "delta value is " + deltaParamFrmPagination);
			try {
				if (Integer.parseInt(deltaParamFrmPagination) >= 0) {
					paginationTraversal = true;
				}
			}
			catch (NumberFormatException e) {

				LoggerUtil.errorLogger(logger, "deltaParamFrmPagination is not a number", e);
				paginationTraversal = false;
			}
		}
		List<JournalArticle> newsList = null;
		String portletTitle = "";
		String eventYearHighliter = "";
		String categoryNameHighliter = "";
		// if there is no news list created i.e. first time after login
		if (Validator.isNull(renderRequest.getPortletSession().getAttribute("newsList"))) {
			newsList = createDefaultNewsList(renderRequest, renderResponse, vocabName, defaultYear);
			if (Validator.isNull(renderRequest.getPortletSession().getAttribute("portletTitle"))) {
				portletTitle = createPortletTitle(defaultYear, pageName, "");
			}
			if (Validator.isNull(renderRequest.getPortletSession().getAttribute("eventYearHighliter"))) {
				eventYearHighliter = defaultYear;
			}
			if (Validator.isNull(renderRequest.getPortletSession().getAttribute("categoryNameHighliter"))) {
				categoryNameHighliter = "";
			}
		}
		// List is there but pagination is going on
		else if (paginationTraversal) {
			newsList = (List<JournalArticle>) renderRequest.getPortletSession().getAttribute("newsList");
			portletTitle = (String) renderRequest.getPortletSession().getAttribute("portletTitle");
			eventYearHighliter = (String) renderRequest.getPortletSession().getAttribute("eventYearHighliter");
			categoryNameHighliter = (String) renderRequest.getPortletSession().getAttribute("categoryNameHighliter");
		}
		// List is there but no pagination going on
		else {
			newsList = createDefaultNewsList(renderRequest, renderResponse, vocabName, defaultYear);
			renderRequest.getPortletSession().setAttribute("newsList", newsList);
			portletTitle = createPortletTitle(defaultYear, pageName, "");
			renderRequest.getPortletSession().setAttribute("portletTitle", portletTitle);
			eventYearHighliter = defaultYear;
			renderRequest.getPortletSession().setAttribute("eventYearHighliter", eventYearHighliter);
			categoryNameHighliter = "";
			renderRequest.getPortletSession().setAttribute("categoryNameHighliter", categoryNameHighliter);
		}
		String newsParamMapEventOccured = renderRequest.getParameter("newsParamMapEventOccured");
		// If Event is triggered then
		if (Validator.isNotNull(newsParamMapEventOccured) && newsParamMapEventOccured.equalsIgnoreCase("true")) {
			newsList = (List<JournalArticle>) renderRequest.getPortletSession().getAttribute("newsListByEvent");
			renderRequest.getPortletSession().setAttribute("newsList", newsList);
			portletTitle = (String) portletSession.getAttribute("portletTitleByEvent");
			renderRequest.getPortletSession().setAttribute("portletTitle", portletTitle);
			eventYearHighliter = renderRequest.getParameter("eventYear");
			renderRequest.getPortletSession().setAttribute("eventYearHighliter", eventYearHighliter);
			categoryNameHighliter = renderRequest.getParameter("eventCategory");
			renderRequest.getPortletSession().setAttribute("categoryNameHighliter", categoryNameHighliter);
		}
		portletSessionMap.put(vocabName, newsList);
		portletSession.setAttribute("portletSessionMap", portletSessionMap);
		renderResponse.setTitle(portletTitle);
		renderRequest.getPortletSession().setAttribute("eventYearHighliter", eventYearHighliter, PortletSession.APPLICATION_SCOPE);
		renderRequest.getPortletSession().setAttribute("categoryNameHighliter", categoryNameHighliter, PortletSession.APPLICATION_SCOPE);
		LoggerUtil.debugLogger(logger, "End of showNewsDetail() method of NewsDetailController::");
		return Constants.NEWS_DETAIL_JSP;
	}

	/**
	 * Helper Method which takes the vocabName string and defaultYear string and
	 * returns the list of journal articles which are assigned to vocabulary
	 * represented by vocabName string and have display year equal to
	 * defaultYear string.
	 * @param renderRequest
	 * @param renderResponse
	 * @param vocabName
	 * @param defaultYear
	 * @return List<JournalArticle>
	 */
	public List<JournalArticle> createDefaultNewsList(RenderRequest renderRequest, RenderResponse renderResponse, String vocabName, String defaultYear) {

		LoggerUtil.debugLogger(logger, "Start of createDefaultNewsList() method of NewsDetailController::");
		List<JournalArticle> vocabArticleList = WebUtil.getArticlesByCategory(renderRequest, renderResponse, vocabName);
		List<JournalArticle> defaultYearArticleList = null;
		if (Validator.isNotNull(vocabArticleList) && vocabArticleList.size() != 0) {
			LoggerUtil.debugLogger(logger, "default year is" + defaultYear);
			defaultYearArticleList = WebUtil.getArticlesByYear(vocabArticleList, defaultYear);
		}
		LoggerUtil.debugLogger(logger, "End of createDefaultNewsList() method of NewsDetailController::");
		return defaultYearArticleList;
	}

	/**
	 * Helper Method which takes the portletRequest and journalArticle objects
	 * and returns the map which contains key value pairs of article's structure
	 * element and data contained into the element.
	 * @param portletRequest
	 * @param journalArticle
	 * @return Map<Object, Object>
	 */
	public Map<Object, Object> getArticleStructureData(PortletRequest portletRequest, JournalArticle journalArticle) {

		LoggerUtil.debugLogger(logger, "Start of getArticleStructureData() method of NewsDetailController::");
		Map<Object, Object> articleDataMap = null;
		String imageUrl = null;
		String content = null;
		List<Node> docNodesList = null;
		List<String> docUrList = new ArrayList<String>();
		Document document;
		try {
			document = SAXReaderUtil.read(journalArticle.getContent());
			if (Validator.isNotNull(document)) {
				String xpathExpression = "";
				xpathExpression = Constants.DYNAMIC_ELEMENT_NAME + Constants.NEWS_IMAGE_URL + Constants.DYNAMIC_CONTENT;
				if (document.selectSingleNode(xpathExpression) != null)
					imageUrl = document.selectSingleNode(xpathExpression).getText();
				// multi lingual code starts
				List<Node> contentNodeList = null;
				contentNodeList = (document.selectNodes(Constants.DYNAMIC_ELEMENT_NAME + Constants.NEWS_CONTENT + "']"));
				for (Node contentNode : contentNodeList) {
					if (contentNode.selectNodes(Constants.NEWS_DC) != null) {
						List<Node> innerContentNodeList = contentNode.selectNodes(Constants.NEWS_DC);
						for (Node innerContent : innerContentNodeList) {
							String xmlStr = innerContent.asXML();
							Pattern pattern = Pattern.compile(Constants.LANGUAGE_ID_REGEX);
							Matcher matcher = pattern.matcher(xmlStr);
							if (matcher.find()) {
								String localString = matcher.group(1);
								if (WebUtil.getThemeDisplay(portletRequest).getLocale().toString().equalsIgnoreCase(localString)) {
									content = innerContent.getText();
									break;
								}
							}
							// no multilingual content
							else {
								content = innerContent.getText();
								break;
							}
						}
					}
				}
				// multi lingual code ends
				docNodesList = (document.selectNodes(Constants.DYNAMIC_ELEMENT_NAME + Constants.NEWS_DOCS + "']"));
				for (Node doc : docNodesList) {
					if (Validator.isNotNull(doc.selectSingleNode(Constants.NEWS_DC)) &&
						Validator.isNotNull(doc.selectSingleNode(Constants.NEWS_DC).getText())) {
						docUrList.add(doc.selectSingleNode(Constants.NEWS_DC).getText());
					}
				}
				articleDataMap = new HashMap<Object, Object>();
				articleDataMap.put("imageUrl", imageUrl);
				articleDataMap.put("content", content);
				articleDataMap.put("docUrList", docUrList);
			}
		}
		catch (DocumentException e) {

			LoggerUtil.errorLogger(logger, "Exception occured in NewsDetailController.getArticleStructureData(). DocumentException: ", e);
		}
		LoggerUtil.debugLogger(logger, "End of getArticleStructureData() method of NewsDetailController::");
		return articleDataMap;
	}

	/**
	 * Render Mapping Method. This Render Mapping is invoked for param myaction
	 * = renderNewsFullDetail. It displays journal article(News) in full details
	 * view.
	 * @param renderRequest
	 * @param renderResponse
	 * @param model
	 * @return view name string
	 * @throws NumberFormatException
	 * @throws PortalException
	 * @throws SystemException
	 * @throws UnsupportedEncodingException 
	 */

	@SuppressWarnings("unchecked")
	@RenderMapping(params = "myaction=renderNewsFullDetail")
	public String showNewsFullDetail(RenderRequest renderRequest, RenderResponse renderResponse, Model model) throws NumberFormatException, PortalException, SystemException, UnsupportedEncodingException {

		LoggerUtil.debugLogger(logger, "Start of showNewsFullDetail() method of NewsDetailController::");
		String articleId = renderRequest.getParameter("journalArticleId");
		String vocabName = "";
		if (Validator.isNotNull(articleId)) {
			JournalArticle journalArticle = JournalArticleLocalServiceUtil.getJournalArticle(Long.valueOf(articleId));
			Map<Object, Object> articleDataMap = null;
			/*if current vocabulary is either of "EXECUTIVE CORNER" or "VISION AND VALUE" then image should not be displayed in detail section of news*/
			boolean imageDisplay = true;
			vocabName = createPageName(renderRequest, WebUtil.getThemeDisplay(renderRequest).getLayout().getFriendlyURL());
			if(vocabName.equalsIgnoreCase(Constants.EXECUTIVE_CORNER) || vocabName.equalsIgnoreCase(Constants.VISION_AND_VALUES)){
				imageDisplay = false;
			}
			if (Validator.isNotNull(journalArticle)) {
				articleDataMap = getArticleStructureData(renderRequest, journalArticle);
				renderResponse.setTitle(journalArticle.getTitle(WebUtil.getThemeDisplay(renderRequest).getLocale()).toUpperCase());
				if (Validator.isNotNull(articleDataMap) &&
					((Validator.isNull(articleDataMap.get("imageUrl")) || !imageDisplay) && Validator.isNull(articleDataMap.get("content")))) {
					LoggerUtil.debugLogger(logger, "in renderNewsFullDetail and the article has only document");
					List<String> docUrList = (List<String>) (articleDataMap.get("docUrList"));
					if (Validator.isNotNull(docUrList) && docUrList.size() != 0) {
						String documentPath = docUrList.get(0);
						if (Validator.isNotNull(documentPath)) {
							//documentPath = documentPath.replace("+", " ");
							documentPath = URLEncoder.encode(documentPath, "UTF-8");
						}
						model.addAttribute("documentPath", documentPath);
						model.addAttribute("redirect", ParamUtil.getString(renderRequest, "newsDetailRedirect"));
						return Constants.NEWS_ATTACHMENT_JSP;
					}
				}
			}
		}
		model.addAttribute("journalArticleId", renderRequest.getParameter("journalArticleId"));
		model.addAttribute("displayDateStr", renderRequest.getParameter("displayDateStr"));
		String articleTitle = "";
		try {
			articleTitle =
				JournalArticleLocalServiceUtil.getArticle(Long.parseLong(renderRequest.getParameter("journalArticleId"))).getTitle(
					WebUtil.getThemeDisplay(renderRequest).getLocale());
		}
		catch (Exception e) {
			LoggerUtil.errorLogger(logger, "Could not get the title as not able to retrieve article id from request", e);
		}
		renderResponse.setTitle(articleTitle.toUpperCase());
		model.addAttribute("vocabName", vocabName);
		LoggerUtil.debugLogger(logger, "End of showNewsFullDetail() method of NewsDetailController::");
		return Constants.NEWS_FULL_DETAIL_JSP;
	}

	/**
	 * Event Mapping method for event having QName =
	 * "{http://coach.com/events}ipc.newsParamMap". It receives Map object in
	 * event payload which has information about which article is to be
	 * displayed in full details view.
	 * @param eventRequest
	 * @param eventResponse
	 * @param model
	 * @throws PortalException
	 * @throws SystemException
	 */
	@EventMapping(value = "{http://coach.com/events}ipc.newsParamMap")
	public void yearNCatProcessEvent(EventRequest eventRequest, EventResponse eventResponse, Model model) throws PortalException, SystemException {

		LoggerUtil.debugLogger(logger, "Start of yearNCatProcessEvent() method of NewsDetailController::");
		Event event = eventRequest.getEvent();
		@SuppressWarnings("unchecked")
		Map<String, Object> newsParamMap = (HashMap<String, Object>) event.getValue();
		String newsYear = (String) newsParamMap.get("newsYear");
		LoggerUtil.debugLogger(logger, "value of newsYear is " + newsYear);
		String pageName = (String) newsParamMap.get("pageName");
		LoggerUtil.debugLogger(logger, "value of pageName is " + pageName);
		String vocabNameFrmEvent = (String) newsParamMap.get("vocabName");
		AssetCategory[] categoryArray = (AssetCategory[]) newsParamMap.get("categoryArray");
		String vocabNameFrmSession = (String) eventRequest.getPortletSession().getAttribute("vocabName");
		List<JournalArticle> newsListByCategoriesNYear = null;
		Boolean catSelFlag = (Boolean) newsParamMap.get("catSelFlag");
		String portletTitle = "";
		String categoryName = "";
		if ((Validator.isNotNull(vocabNameFrmSession) && Validator.isNotNull(vocabNameFrmEvent)) &&
			vocabNameFrmSession.equalsIgnoreCase(vocabNameFrmEvent)) {

			newsListByCategoriesNYear = WebUtil.getArticlesByCategoriesNYear(eventRequest, eventResponse, newsYear, categoryArray);
			categoryName =
				catSelFlag ? AssetCategoryLocalServiceUtil.getCategory(categoryArray[0].getCategoryId()).getTitle(
					WebUtil.getThemeDisplay(eventRequest).getLocale()) : "";
			portletTitle = createPortletTitle(newsYear, pageName, categoryName);
			eventRequest.getPortletSession().setAttribute("newsListByEvent", newsListByCategoriesNYear);
			eventRequest.getPortletSession().setAttribute("portletTitleByEvent", portletTitle);

		}
		eventResponse.setRenderParameter("eventYear", newsYear);
		eventResponse.setRenderParameter("eventCategory", categoryName);
		eventResponse.setRenderParameter("newsParamMapEventOccured", "true");
		LoggerUtil.debugLogger(logger, "End of yearNCatProcessEvent() method of NewsDetailController::");
	}

	/**
	 * Event Mapping method for event having QName =
	 * "{http://coach.com/events}ipc.articleMap". It handles the event coming
	 * from different page and receives a Map object in event payload which has
	 * information about the article that need to be displayed in full details
	 * view.
	 * @param eventRequest
	 * @param eventResponse
	 * @param model
	 * @throws PortalException
	 * @throws IOException
	 * @throws SystemException
	 */
	@SuppressWarnings("unchecked")
	@EventMapping(value = "{http://coach.com/events}ipc.articleMap")
	public void articleIdProcessEvent(EventRequest eventRequest, EventResponse eventResponse, Model model) throws SystemException, PortalException, IOException {

		LoggerUtil.debugLogger(logger, "Start of articleIdProcessEvent() method of NewsDetailController::");
		Event event = eventRequest.getEvent();
		HashMap<String, Object> articleMap = (HashMap<String, Object>) event.getValue();
		if (Validator.isNotNull(articleMap)) {
			Long articleId = (Long) articleMap.get("articleId");
			String redirectUrl = (String) articleMap.get("redirectUrl");
			LoggerUtil.debugLogger(logger, "article id from news scroller portlet is " + articleId.toString());
			LoggerUtil.debugLogger(logger, "redirectUrl from news scroller portlet is " + redirectUrl);
			List<JournalArticle> news = new ArrayList<JournalArticle>();
			JournalArticle journalArticle = JournalArticleLocalServiceUtil.getArticle(Long.valueOf(articleId));
			PortletSession portletSession = eventRequest.getPortletSession();
			Map<String, Object> portletSessionMap = (Map<String, Object>) portletSession.getAttribute("portletSessionMap");
			portletSessionMap = Validator.isNotNull(portletSessionMap) ? portletSessionMap : new HashMap<String, Object>();
			if (Validator.isNotNull(journalArticle)) {
				news.add(journalArticle);
				portletSessionMap.put("newsListByInterPageEvent", news);
			}
			portletSessionMap.put("redirectUrl", redirectUrl);
			portletSession.setAttribute("portletSessionMap", portletSessionMap);
			LoggerUtil.debugLogger(logger, "event triggered");
		}
		LoggerUtil.debugLogger(logger, "End of articleIdProcessEvent() method of NewsDetailController::");
	}

	/**
	 * Action Mapping Method which sets the required parameters in response
	 * object to display the selected document in 70 % frame and triggers
	 * "http://coach.com/events","ipc.highlighterMap" type event to highlight
	 * the related journal article in News Portlet.
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 * @throws WindowStateException
	 * @throws NumberFormatException
	 * @throws PortalException
	 * @throws SystemException
	 * @throws UnsupportedEncodingException 
	 */
	@ActionMapping(params = "action=getNewsAttachment")
	public void getNewsAttachment(ActionRequest actionRequest, ActionResponse actionResponse, Model model) throws WindowStateException, NumberFormatException, PortalException, SystemException, UnsupportedEncodingException {

		LoggerUtil.debugLogger(logger, "Start of getNewsAttachment() method of NewsDetailController::");
		String redirect = actionRequest.getParameter("redirect");
		String journalArticleId = actionRequest.getParameter("journalArticleId");
		LoggerUtil.debugLogger(logger, "journalArticleId from request is" + journalArticleId);
		String documentPath = actionRequest.getParameter("documentPath");
		LoggerUtil.debugLogger(logger, "DocumentPath===========>" + documentPath);
		//documentPath = documentPath.replace("+", " ");
		documentPath = URLEncoder.encode(documentPath, "UTF-8");
		String portletTitle = actionRequest.getParameter("portletTitle");
		LoggerUtil.debugLogger(logger, "portletTitle===========>" + portletTitle);
		model.addAttribute("documentPath", documentPath);
		actionResponse.setWindowState(WindowState.MAXIMIZED);
		actionResponse.setRenderParameter("action", "renderAttachement");
		actionResponse.setRenderParameter("redirect", redirect);
		actionResponse.setRenderParameter("portletTitle", portletTitle);
		String articleCategory = "";
		String articleYear = "";
		JournalArticle journalArticle = null;
		if (Validator.isNotNull(journalArticleId)) {
			journalArticle = JournalArticleLocalServiceUtil.getArticle(Long.parseLong(journalArticleId));
			if (Validator.isNotNull(journalArticle)) {
				List<AssetCategory> catList =
					AssetCategoryLocalServiceUtil.getCategories(JournalArticle.class.getName(), journalArticle.getResourcePrimKey());
				if (catList != null && catList.size() != 0)
					articleCategory = catList.get(0).getName();
				articleYear = new SimpleDateFormat("yyyy").format(journalArticle.getDisplayDate());
			}
		}
		if (Validator.isNotNull(articleCategory) && Validator.isNotNull(articleYear)) {
			QName qname = new QName("http://coach.com/events", "ipc.highlighterMap");
			HashMap<String, String> highlighterMap = new HashMap<String, String>();
			String vocabName = createPageName(actionRequest, WebUtil.getThemeDisplay(actionRequest).getLayout().getFriendlyURL());
			highlighterMap.put("articleCategory", articleCategory);
			highlighterMap.put("articleYear", articleYear);
			highlighterMap.put("vocabName", vocabName);
			actionResponse.setEvent(qname, highlighterMap);
			// Auditing the action starts
			
			Locale locale = WebUtil.getThemeDisplay(actionRequest).getLocale();
			WebUtil.createNewsAuditRecord(this.getClass().getName(), vocabName, articleCategory, journalArticle.getTitle(locale));
			// Auditing the action ends
		}
		LoggerUtil.debugLogger(logger, "End of getNewsAttachment() method of NewsDetailController::");
	}

	/**
	 * Render Mapping Method which renders the document in 70% frame.
	 * @param renderRequest
	 * @param renderResponse
	 */
	@RenderMapping(params = "action=renderAttachement")
	public String renderAttachement(RenderRequest renderRequest, RenderResponse renderResponse) {

		LoggerUtil.debugLogger(logger, "Start of renderAttachement() method of NewsDetailController::");
		String portletTitle = ParamUtil.getString(renderRequest, "portletTitle");
		if (Validator.isNotNull(portletTitle))
			renderResponse.setTitle(portletTitle);
		LoggerUtil.debugLogger(logger, "End of renderAttachement() method of NewsDetailController::");
		return Constants.NEWS_ATTACHMENT_JSP;
	}

	/**
	 * Helper Method which takes newsYear string, pageName string and
	 * categoryName string and returns the portlet title string.
	 * @param newsYear
	 * @param pageName
	 * @param categoryName
	 * @return portlet title string
	 */
	public String createPortletTitle(String newsYear, String pageName, String categoryName) {

		LoggerUtil.debugLogger(logger, "Start of createPortletTitle() method of NewsDetailController::");
		StringBuilder portletTitle = new StringBuilder("");
		if (!newsYear.equalsIgnoreCase(""))
			portletTitle = portletTitle.append(newsYear + " ");
		portletTitle = portletTitle.append(pageName);
		if (!categoryName.equalsIgnoreCase("")) {
			portletTitle = portletTitle.append(" - ");
			portletTitle = portletTitle.append(categoryName);
		}
		LoggerUtil.debugLogger(logger, "portletTitle--->" + portletTitle);
		LoggerUtil.debugLogger(logger, "End of createPortletTitle() method of NewsDetailController::");
		return portletTitle.toString().toUpperCase();
	}

	/**
	 * Helper Method which takes portletRequest and urlName string and returns
	 * page name string.
	 * @param portletRequest
	 * @param urlName
	 * @return String
	 */
	public String createPageName(PortletRequest portletRequest, String urlName) {

		LoggerUtil.debugLogger(logger, "Start of createPageName() method of NewsDetailController::");
		String[] urlTokens = urlName.split("/");
		String pageName = "";
		if (Validator.isNotNull(urlTokens)) {
			int len = urlTokens.length;
			pageName = urlTokens[len - 1];
			pageName = pageName.toUpperCase(WebUtil.getThemeDisplay(portletRequest).getLocale()).replace("-", " ");
		}
		LoggerUtil.debugLogger(logger, "End of createPageName() method of NewsDetailController::");
		return pageName;
	}

	/**
	 * Action Mapping Method for param "action=showFulldetailsActionNews". It
	 * sets the required parameters in response object to display selected
	 * journal article in full details view and triggers
	 * "http://coach.com/events","ipc.highlighterMap" type event to highlight
	 * that journal article in News Portlet.
	 * @param actionRequest
	 * @param actionResponse
	 * @throws PortalException
	 * @throws SystemException
	 */
	@ActionMapping(params = "action=showFulldetailsAction")
	public void showFulldetailsAction(ActionRequest actionRequest, ActionResponse actionResponse, Model model) throws NumberFormatException, PortalException, SystemException {

		LoggerUtil.debugLogger(logger, "Start of showFulldetailsAction() method of NewsDetailController::");
		String journalArticleId = ParamUtil.getString(actionRequest, "journalArticleId");
		String displayDateStr = ParamUtil.getString(actionRequest, "displayDateStr");
		String newsDetailRedirect = ParamUtil.getString(actionRequest, "newsDetailRedirect");
		actionResponse.setRenderParameter("journalArticleId", journalArticleId);
		actionResponse.setRenderParameter("displayDateStr", displayDateStr);
		actionResponse.setRenderParameter("newsDetailRedirect", newsDetailRedirect);
		actionResponse.setRenderParameter("myaction", "renderNewsFullDetail");
		String articleCategory = "";
		String articleYear = "";
		JournalArticle journalArticle = null;
		if (Validator.isNotNull(journalArticleId)) {
			journalArticle = JournalArticleLocalServiceUtil.getArticle(Long.parseLong(journalArticleId));
			if (Validator.isNotNull(journalArticle)) {
				List<AssetCategory> catList =
					AssetCategoryLocalServiceUtil.getCategories(JournalArticle.class.getName(), journalArticle.getResourcePrimKey());
				if (catList != null && catList.size() != 0)
					articleCategory = catList.get(0).getName();
				articleYear = new SimpleDateFormat("yyyy").format(journalArticle.getDisplayDate());
			}
		}
		if (Validator.isNotNull(articleCategory) && Validator.isNotNull(articleYear)) {
			QName qname = new QName("http://coach.com/events", "ipc.highlighterMap");
			HashMap<String, String> highlighterMap = new HashMap<String, String>();
			String vocabName = createPageName(actionRequest, WebUtil.getThemeDisplay(actionRequest).getLayout().getFriendlyURL());
			highlighterMap.put("articleCategory", articleCategory);
			highlighterMap.put("articleYear", articleYear);
			highlighterMap.put("vocabName", vocabName);
			actionResponse.setEvent(qname, highlighterMap);
			// Auditing the action starts
			
			Locale locale = WebUtil.getThemeDisplay(actionRequest).getLocale();
			WebUtil.createNewsAuditRecord(this.getClass().getName(), vocabName, articleCategory, journalArticle.getTitle(locale));
			
			// Auditing the action ends
		}
		LoggerUtil.debugLogger(logger, "End of showFulldetailsAction() method of NewsDetailController::");
	}
}
