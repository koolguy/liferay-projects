
package com.coach.cip.portlets.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.xml.namespace.QName;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.coach.cip.util.WebUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portlet.asset.model.AssetCategory;
import com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;

/**
 * Controller class for ExecutiveCornerPortlet
 * @author GalaxE.
 */

@Controller("executiveCornerController")
@RequestMapping("VIEW")
public class ExecutiveCornerController extends AbstractBaseController {

	/** The Constant LOG. */
	private static final Log logger = LogFactoryUtil.getLog(ExecutiveCornerController.class);

	/**
	 * Default Render Mapping Method.
	 * @param renderRequest
	 * @param renderResponse
	 * @return view name string
	 */
	@RenderMapping
	public String showExecutiveCorner(RenderRequest renderRequest, RenderResponse renderResponse) {

		return Constants.EXECUTIVE_CORNER_JSP;
	}

	/**
	 * This method is used to to raise the event for showing full article.
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 * @throws IOException
	 * @throws SystemException
	 * @throws PortalException
	 * @throws NumberFormatException
	 */
	@ActionMapping(params = "myaction=showArticle")
	public void showArticle(ActionRequest actionRequest, ActionResponse actionResponse, Model model) throws IOException, NumberFormatException, PortalException, SystemException {

		LoggerUtil.debugLogger(logger, "Start of showArticle() method of ExecutiveCornerController::");
		String articleId = ParamUtil.getString(actionRequest, "articleId");
		QName qname = new QName("http://coach.com/events", "ipc.articleMap");
		HashMap<String, Object> articleMap = new HashMap<String, Object>();
		articleMap.put("articleId", Long.valueOf(articleId));
		articleMap.put("redirectUrl", WebUtil.getPortalURL(actionRequest) + Constants.HOME_URL);
		actionResponse.setEvent(qname, articleMap);
		// Auditing the action starts
		if (Validator.isNotNull(articleId)) {
			JournalArticle journalArticle = JournalArticleLocalServiceUtil.getArticle(Long.valueOf(articleId));
			AssetCategory articleCat = null;
			Locale locale = WebUtil.getThemeDisplay(actionRequest).getLocale();
			List<AssetCategory> catList =
				AssetCategoryLocalServiceUtil.getCategories(JournalArticle.class.getName(), journalArticle.getResourcePrimKey());
			if (Validator.isNotNull(catList))
				articleCat = catList.get(0);
			if(Validator.isNotNull(articleCat)){
				WebUtil.createNewsAuditRecord(
						this.getClass().getName(), Constants.EXECUTIVE_CORNER_NEWS_TYPE, articleCat.getTitle(locale), journalArticle.getTitle(locale));
			}
		}
		// Auditing the action ends
		actionResponse.sendRedirect(WebUtil.getPortalURL(actionRequest) + WebUtil.getPageUrl(actionRequest, Constants.EXECUTIVE_CORNER_URL)
						+ "?interPage=true&allNews=false");
		LoggerUtil.debugLogger(logger, "End of showArticle() method of ExecutiveCornerController::");
	}

	/**
	 * Action mapping method to trigger event for showing the document in 70%
	 * frame.
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 * @throws IOException
	 */
	@ActionMapping(params = "myaction=showOnlyDoc")
	public void showOnlyDoc(ActionRequest actionRequest, ActionResponse actionResponse, Model model) throws IOException {

		LoggerUtil.debugLogger(logger, "Start of showOnlyDoc() method of ExecutiveCornerController::");
		String documentPath = ParamUtil.getString(actionRequest, "documentPath");
		String portletTitle = ParamUtil.getString(actionRequest, "portletTitle");
		QName qname = new QName("http://coach.com/events", "ipc.docMap");
		HashMap<String, Object> docMap = new HashMap<String, Object>();
		docMap.put("documentPath", documentPath);
		docMap.put("portletTitle", portletTitle);
		actionResponse.setEvent(qname, docMap);
		actionResponse.sendRedirect(WebUtil.getPortalURL(actionRequest) + Constants.EXECUTIVE_CORNER_URL + "?showOnlyDoc=true");
		LoggerUtil.debugLogger(logger, "End of showOnlyDoc() method of ExecutiveCornerController::");
	}

	/**
	 * Render mapping method for myaction = showArticle.
	 * @param renderRequest
	 * @param renderResponse
	 * @return view name string
	 */
	@RenderMapping(params = "myaction=showArticle")
	public String showArticle(RenderRequest renderRequest, RenderResponse renderResponse) {

		return Constants.SHOW_ARTICLE_JSP;
	}

}
