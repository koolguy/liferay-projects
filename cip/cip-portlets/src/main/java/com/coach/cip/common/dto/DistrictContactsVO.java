package com.coach.cip.common.dto;

import java.io.Serializable;

import com.coach.cip.model.entity.CIContact;

/**
 * 
 * Class is used to capture the characteristics of DistrictContactsVO.
 * 
 * @author GalaxE.
 *
 */
public class DistrictContactsVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1872814297243646703L;
	// Fields    

    private Long contactDistrictId;
    public Long getContactDistrictId() {
		return contactDistrictId;
	}


	public void setContactDistrictId(Long contactDistrictId) {
		this.contactDistrictId = contactDistrictId;
	}


	public DistrictsVO getCIDistrict() {
		return CIDistrict;
	}


	public void setCIDistrict(DistrictsVO cIDistrict) {
		CIDistrict = cIDistrict;
	}


	public CIContact getCIContact() {
		return CIContact;
	}


	public void setCIContact(CIContact cIContact) {
		CIContact = cIContact;
	}


	private DistrictsVO CIDistrict;
    private CIContact CIContact;


   // Constructors

   /** default constructor */
   public DistrictContactsVO() {
   }

   
   /** full constructor */
   public DistrictContactsVO(Long contactDistrictId, DistrictsVO CIDistrict, CIContact CIContact) {
       this.contactDistrictId = contactDistrictId;
       this.CIDistrict = CIDistrict;
       this.CIContact = CIContact;
   }

}
