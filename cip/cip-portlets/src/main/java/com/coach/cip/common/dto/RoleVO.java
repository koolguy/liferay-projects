package com.coach.cip.common.dto;

import java.io.Serializable;

/**
 * Class is used to capture the characteristics of roleVO.
 * 
 * @author GalaxE.
 *
 */
public class RoleVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3991847512198063823L;
	// Fields
	private Long roleId;
	private Long companyId;
	private Long classNameId;
	private Long classPk;
	private String name;
	private String title;
	private String description;
	private Integer type;
	private String subtype;

	// Constructors

	/** default constructor */
	public RoleVO() {
	}

	/** minimal constructor */
	public RoleVO(Long roleId) {
		this.roleId = roleId;
	}

	/** full constructor */
	public RoleVO(Long roleId, Long companyId, Long classNameId, Long classPk,
			String name, String title, String description, Integer type,
			String subtype) {
		this.roleId = roleId;
		this.companyId = companyId;
		this.classNameId = classNameId;
		this.classPk = classPk;
		this.name = name;
		this.title = title;
		this.description = description;
		this.type = type;
		this.subtype = subtype;

	}
	
	/**
	 * Method used to roleId.
	 * @return roleId.
	 */
	public Long getRoleId() {
		return roleId;
	}
	/**
	 * Method used to set roleId.
	 * @param roleId
	 */
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}
	/**
	 * Method used to get companyId.
	 * @return companyId.
	 */
	public Long getCompanyId() {
		return companyId;
	}
	/**
	 * Method used to set companyId.
	 * @param companyId
	 */
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	/**
	 * Method used to get classNameId.
	 * @return classNameId.
	 */
	public Long getClassNameId() {
		return classNameId;
	}
	/**
	 * Method used to set classNameId.
	 * @param classNameId
	 */
	public void setClassNameId(Long classNameId) {
		this.classNameId = classNameId;
	}
	
	/**
	 * Method used to get classPK.
	 * @return classPk.
	 */
	public Long getClassPk() {
		return classPk;
	}
	/**
	 * This class is used to set classPK.
	 * @param classPk
	 */
	public void setClassPk(Long classPk) {
		this.classPk = classPk;
	}
	
	/**
	 * Method used to get name.
	 * @return name.
	 */
	public String getName() {
		return name;
	}
	/**
	 * Method used to set name.
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * Method used to get title.
	 * @return title.
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * Method used to set title.
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Method used to get description.
	 * @return description.
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Method used to set description.
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Method used to get type.
	 * @return type.
	 */
	public Integer getType() {
		return type;
	}

	/**
	 * Method used to set type.
	 * @param type
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * Method used to get subType.
	 * @return subtype.
	 */
	public String getSubtype() {
		return subtype;
	}

	/**
	 * Method used to set subType.
	 * @param subtype
	 */
	public void setSubtype(String subtype) {
		this.subtype = subtype;
	}


	/**
	 * Returns unique roleVO object based on hashCode.
	 */
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((roleId == null) ? 0 : roleId.hashCode());
		return result;
	}

	/**
	 * Returns equality of roleVO Objects.
	 */
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RoleVO other = (RoleVO) obj;
		if (roleId == null) {
			if (other.roleId != null)
				return false;
		} else if (!roleId.equals(other.roleId))
			return false;
		return true;
	}
}
