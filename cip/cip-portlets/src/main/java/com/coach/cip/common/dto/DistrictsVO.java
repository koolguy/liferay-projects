package com.coach.cip.common.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import com.coach.cip.model.entity.CIDistrictContact;
import com.coach.cip.model.entity.CIStore;

/**
 * 
 * Class is used to capture the characteristics of DistrictsVO.
 * 
 * @author GalaxE.
 *
 */
public class DistrictsVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8551991337911510891L;
	// Fields    

    private Long districtId;
    public Long getDistrictId() {
		return districtId;
	}

	public void setDistrictId(Long districtId) {
		this.districtId = districtId;
	}

	

	public Long getParentDistrictId() {
		return parentDistrictId;
	}

	public void setParentDistrictId(Long parentDistrictId) {
		this.parentDistrictId = parentDistrictId;
	}

	public String getDistrictName() {
		return districtName;
	}

	public void setDistrictName(String districtName) {
		this.districtName = districtName;
	}

	public Set<CIDistrictContact> getCIDistrictContacts() {
		return CIDistrictContacts;
	}

	public void setCIDistrictContacts(Set<CIDistrictContact> cIDistrictContacts) {
		CIDistrictContacts = cIDistrictContacts;
	}

	public Set<CIStore> getCIStores() {
		return CIStores;
	}

	public void setCIStores(Set<CIStore> cIStores) {
		CIStores = cIStores;
	}

	private StatesVO stateVO;
    private Long parentDistrictId;
    private String districtName;
    private Set<CIDistrictContact> CIDistrictContacts = new HashSet<CIDistrictContact>(0);
    private Set<CIStore> CIStores = new HashSet<CIStore>(0);


   // Constructors

   /** default constructor */
   public DistrictsVO() {
   }

	/** minimal constructor */
   public DistrictsVO(Long districtId, StatesVO stateVO) {
       this.districtId = districtId;
       this.stateVO = stateVO;
   }
   
   /** full constructor */
   public DistrictsVO(Long districtId, StatesVO stateVO, Long parentDistrictId, String districtName, Set<CIDistrictContact> CIDistrictContacts, Set<CIStore> CIStores) {
       this.districtId = districtId;
       this.stateVO = stateVO;
       this.parentDistrictId = parentDistrictId;
       this.districtName = districtName;
       this.CIDistrictContacts = CIDistrictContacts;
       this.CIStores = CIStores;
   }

public StatesVO getStateVO() {
	return stateVO;
}

public void setStateVO(StatesVO stateVO) {
	this.stateVO = stateVO;
}

}
