
package com.coach.cip.portlets.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.xml.namespace.QName;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.coach.cip.util.WebUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portlet.asset.model.AssetCategory;
import com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;

/**
 * Controller class for News.
 * @author Galaxe.
 */
@Controller("newsController")
@RequestMapping(value = "VIEW")
public class NewsController extends AbstractBaseController {

	/** The Constant logger. */
	private static final Log logger = LogFactoryUtil.getLog(NewsController.class);

	/**
	 * Default Render Mapping Method. If Specific Render mapping is not found,
	 * this method is invoked.
	 * @param renderRequest
	 * @param renderResponse
	 * @param model
	 * @return view name string
	 * @throws PortalException
	 * @throws SystemException
	 */
	@RenderMapping
	public String showNews(RenderRequest renderRequest, RenderResponse renderResponse, Model model) throws PortalException, SystemException {

		LoggerUtil.debugLogger(logger, "Start of showNews() method of NewsController::");
		String friendlyUrlName = WebUtil.getThemeDisplay(renderRequest).getLayout().getFriendlyURL();
		String[] urlTokens = friendlyUrlName.split("/");
		String pageName = urlTokens[urlTokens.length - 1];
		String vocabName = pageName.replace("-", " ").toUpperCase();
		LoggerUtil.debugLogger(logger, "vocabName is " + vocabName);
		renderRequest.getPortletSession().setAttribute("vocabName", vocabName);
		List<AssetCategory> categoryList = WebUtil.getAssetCategoryByVocabulary(renderRequest, renderResponse, vocabName);
		List<String> yearList = new ArrayList<String>();
		Date currDate = new Date();
		DateFormat df = new SimpleDateFormat("yyyy");
		int currYearVal = Integer.valueOf(df.format(currDate)).intValue();
		while (currYearVal >= Constants.COACH_STARTING_NEWS_YEAR) {
			yearList.add(String.valueOf(currYearVal));
			currYearVal--;
		}
		Map<Object, Object> yearCatMap = null;
		yearCatMap = getYearWisecat(renderRequest, renderResponse, categoryList, yearList);
		if (yearCatMap != null && yearCatMap.size() != 0) {
			renderRequest.getPortletSession().setAttribute("yearCatMap", yearCatMap);
			model.addAttribute("yearCatMap", yearCatMap);

		}
		model.addAttribute("catNameFrmEvent", (String) renderRequest.getPortletSession().getAttribute("catNameFrmEvent"));
		renderRequest.getPortletSession().removeAttribute("catNameFrmEvent");

		model.addAttribute("displayYearFrmEvent", (String) renderRequest.getPortletSession().getAttribute("displayYearFrmEvent"));
		renderRequest.getPortletSession().removeAttribute("displayYearFrmEvent");
		LoggerUtil.debugLogger(logger, "End of showNews() method of NewsController::");
		return Constants.NEWS_JSP;
	}

	/**
	 * Helper Method which takes the list of categories and list of year
	 * strings, and returns the Map<Object,Object> where key is year string and
	 * value is a list of categories in which only those categories are added
	 * which are assigned one or more journal article of that year.
	 * @param renderRequest
	 * @param renderResponse
	 * @param categoryList
	 * @param yearList
	 * @return Map<Object, Object>
	 * @throws PortalException
	 * @throws SystemException
	 */
	public Map<Object, Object> getYearWisecat(RenderRequest renderRequest, RenderResponse renderResponse, List<AssetCategory> categoryList, List<String> yearList) throws PortalException, SystemException {

		LoggerUtil.debugLogger(logger, "Start of getYearWisecat() method of NewsController::");
		Map<Object, Object> yearCatMap = null;
		if (Validator.isNotNull(yearList) && Validator.isNotNull(categoryList)) {
			yearCatMap = new HashMap<Object, Object>();

			for (String year : yearList) {
				List<AssetCategory> catList = new ArrayList<AssetCategory>();
				for (AssetCategory category : categoryList) {
					List<JournalArticle> newsListByCategoriesNYear =
						WebUtil.getArticlesByCategoriesNYear(renderRequest, renderResponse, year, category);
					if (Validator.isNotNull(newsListByCategoriesNYear) && newsListByCategoriesNYear.size() != 0)
						catList.add(category);
				}

				yearCatMap.put(year, catList);
			}

		}
		LoggerUtil.debugLogger(logger, "End of getYearWisecat() method of NewsController::");
		return yearCatMap;
	}

	/**
	 * Action Mapping Method for News. It creates the Event with selected
	 * newsType and sets the event in the ActionResponse object.
	 * @param actionRequest
	 * @param actionResponse
	 * @throws PortalException
	 * @throws SystemException
	 */
	@SuppressWarnings("unchecked")
	@ActionMapping("yearNCatAction")
	public void yearNCatProcessAction(ActionRequest actionRequest, ActionResponse actionResponse) throws PortalException, SystemException {

		LoggerUtil.debugLogger(logger, "Start of yearNCatProcessAction() method of NewsController::");
		String newsYear = actionRequest.getParameter("newsYear") != null ? actionRequest.getParameter("newsYear") : "";
		String newsCategoryId = actionRequest.getParameter("newsCategoryId") != null ? actionRequest.getParameter("newsCategoryId") : "";
		Boolean catSelFlag = false;
		LoggerUtil.debugLogger(logger, "newsYear-->" + newsYear + "newsCategoryId-->" + newsCategoryId);
		HashMap<String, Object> newsParamMap = new HashMap<String, Object>();
		AssetCategory[] categoryArray = null;
		AssetCategory category = null;
		if (newsCategoryId != "") {
			catSelFlag = true;
			category = AssetCategoryLocalServiceUtil.getAssetCategory(Long.parseLong(newsCategoryId));
			categoryArray = new AssetCategory[] {
				category
			};
			actionRequest.getPortletSession().setAttribute("catNameFrmEvent", category.getName().trim());
		}
		else {
			Map<Object, Object> yearCatMap = (HashMap<Object, Object>) actionRequest.getPortletSession().getAttribute("yearCatMap");
			if ((Validator.isNotNull(yearCatMap) && Validator.isNotNull(yearCatMap.get(newsYear))) &&
				(((List<AssetCategory>) yearCatMap.get(newsYear)).size() != 0)) {
				List<AssetCategory> categoryList = ((List<AssetCategory>) yearCatMap.get(newsYear));
				categoryArray = new AssetCategory[categoryList.size()];
				for (int i = 0; i < categoryList.size(); i++)
					categoryArray[i] = categoryList.get(i);
			}
			actionRequest.getPortletSession().setAttribute("catNameFrmEvent", "");
		}
		String pageName = WebUtil.getThemeDisplay(actionRequest).getLayout().getHTMLTitle(WebUtil.getThemeDisplay(actionRequest).getLocale());
		actionRequest.getPortletSession().setAttribute("displayYearFrmEvent", newsYear);
		newsParamMap.put("newsYear", newsYear);
		newsParamMap.put("pageName", pageName);
		String vocabName = "";
		vocabName = (String) actionRequest.getPortletSession().getAttribute("vocabName");
		newsParamMap.put("vocabName", vocabName);
		newsParamMap.put("categoryArray", categoryArray);
		newsParamMap.put("catSelFlag", catSelFlag);
		QName qname = new QName("http://coach.com/events", "ipc.newsParamMap");
		actionResponse.setEvent(qname, newsParamMap);
		// Auditing the action starts
		String newsCategory = "";
		Locale locale = WebUtil.getThemeDisplay(actionRequest).getLocale();
		// If category is selected
		if (catSelFlag) {
			newsCategory = category.getTitle(locale);
		}
		else {
			newsCategory = Constants.NOT_SELECTED;
		}
		WebUtil.createNewsAuditRecord(this.getClass().getName(), vocabName, newsCategory, Constants.ALL + newsYear);
		
		// Auditing the action ends
		LoggerUtil.debugLogger(logger, "End of yearNCatProcessAction() method of NewsController::");
	}

	/**
	 * Event Mapping Method for "{http://coach.com/events}ipc.articleMap" type
	 * event. It receives Map object in event payload which has information
	 * about article which needs to be highlighted.
	 * @param eventRequest
	 * @param eventResponse
	 * @param model
	 * @throws PortalException
	 * @throws IOException
	 * @throws SystemException
	 */
	@EventMapping(value = "{http://coach.com/events}ipc.articleMap")
	public void articleIdProcessEvent(EventRequest eventRequest, EventResponse eventResponse, Model model) throws PortalException, IOException, SystemException {

		LoggerUtil.debugLogger(logger, "Start of articleIdProcessEvent() method of NewsController::");
		Event event = eventRequest.getEvent();
		@SuppressWarnings("unchecked")
		HashMap<String, Object> articleMap = (HashMap<String, Object>) event.getValue();

		if (Validator.isNotNull(articleMap)) {
			Long articleId = (Long) articleMap.get("articleId");
			String redirectUrl = (String) articleMap.get("redirectUrl");
			LoggerUtil.debugLogger(logger, "article id from news scroller portlet is " + articleId.toString());
			LoggerUtil.debugLogger(logger, "redirectUrl from news scroller portlet is " + redirectUrl);
			JournalArticle journalArticle = JournalArticleLocalServiceUtil.getArticle(Long.valueOf(articleId));
			if (Validator.isNotNull(journalArticle)) {
				String displayYear = new SimpleDateFormat("yyyy").format(journalArticle.getDisplayDate());
				LoggerUtil.debugLogger(logger, "article displayDate in NewsController is " + displayYear);
				eventRequest.getPortletSession().setAttribute("displayYearFrmEvent", displayYear);
				AssetCategory articleCat = null;
				List<AssetCategory> catList =
					AssetCategoryLocalServiceUtil.getCategories(JournalArticle.class.getName(), journalArticle.getResourcePrimKey());
				if (catList != null && catList.size() != 0) {
					articleCat = catList.get(0);
					LoggerUtil.debugLogger(logger, "article category in NewsController is " + articleCat.getName());
					eventRequest.getPortletSession().setAttribute("catNameFrmEvent", articleCat.getName().trim());
				}
			}

			LoggerUtil.debugLogger(logger, "End of articleIdProcessEvent() method of NewsController::");
		}
	}

	/**
	 * Event Mapping Method for "{http://coach.com/events}ipc.highlighterMap"
	 * type event. It receives Map object in event payload which has information
	 * about which article need to be highlighted.
	 * @param eventRequest
	 * @param eventResponse
	 * @param model
	 * @throws PortalException
	 * @throws IOException
	 * @throws SystemException
	 */
	@SuppressWarnings("unchecked")
	@EventMapping(value = "{http://coach.com/events}ipc.highlighterMap")
	public void highilighterMapProcessEvent(EventRequest eventRequest, EventResponse eventResponse, Model model) throws PortalException, IOException, SystemException {

		LoggerUtil.debugLogger(logger, "Start of highilighterMapProcessEvent() method of NewsController::");
		Event event = eventRequest.getEvent();
		PortletSession portletSession = eventRequest.getPortletSession();
		HashMap<String, String> highlighterMap = (HashMap<String, String>) event.getValue();
		String vocabName = createPageName(eventRequest, WebUtil.getThemeDisplay(eventRequest).getLayout().getFriendlyURL());
		String vocabNameFrmEvent = "";
		if (Validator.isNotNull(highlighterMap) && Validator.isNotNull(highlighterMap.get("vocabName"))) {
			vocabNameFrmEvent = (String) highlighterMap.get("vocabName");
			if (Validator.isNotNull(vocabName) && vocabName.equalsIgnoreCase(vocabNameFrmEvent)) {
				String articleCategory =
					Validator.isNotNull(highlighterMap.get("articleCategory")) ? (String) highlighterMap.get("articleCategory") : "";
				String articleYear = Validator.isNotNull(highlighterMap.get("articleYear")) ? (String) highlighterMap.get("articleYear") : "";
				portletSession.setAttribute("catNameFrmEvent", articleCategory);
				portletSession.setAttribute("displayYearFrmEvent", articleYear);
			}
		}
		LoggerUtil.debugLogger(logger, "End of highilighterMapProcessEvent() method of NewsController::");
	}

	/**
	 * Helper Method which takes portletRequest and urlName string and returns
	 * page name string.
	 * @param portletRequest
	 * @param urlName
	 * @return String
	 */
	public String createPageName(PortletRequest portletRequest, String urlName) {

		LoggerUtil.debugLogger(logger, "Start of createPageName() method of NewsController::");
		String[] urlTokens = urlName.split("/");
		String pageName = "";
		if (Validator.isNotNull(urlTokens)) {
			int len = urlTokens.length;
			pageName = urlTokens[len - 1];
			pageName = pageName.toUpperCase(WebUtil.getThemeDisplay(portletRequest).getLocale()).replace("-", " ");
		}
		LoggerUtil.debugLogger(logger, "End of createPageName() method of NewsController::");
		return pageName;
	}
}
