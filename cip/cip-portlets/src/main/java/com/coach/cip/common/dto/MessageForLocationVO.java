
package com.coach.cip.common.dto;

import java.io.Serializable;

/**
 * Class is used to capture the characteristics of messageForLocations. This is
 * a reusable class for messageForLocations.
 * 
 * @author GalaxE.
 */
public class MessageForLocationVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3435597745052319514L;
	private Long cimflId;
	private PersonalMessageVO personalMessage;
	private String locationType;
	private String locationName;
	private String state;
	private String city;
	private String office;

	/**
	 * Method used to get cimflId.
	 * @return cimflId.
	 */
	public Long getCimflId() {

		return cimflId;
	}

	/**
	 * Method used to set cimflId.
	 * @param cimflId
	 *            .
	 */
	public void setCimflId(Long cimflId) {

		this.cimflId = cimflId;
	}

	/**
	 * Method used to get personalMessage.
	 * @return the personalMessage.
	 */
	public PersonalMessageVO getPersonalMessage() {

		return personalMessage;
	}

	/**
	 * Method used to set personalMessage.
	 * @param personalMessage
	 *            .
	 */
	public void setPersonalMessage(PersonalMessageVO personalMessage) {

		this.personalMessage = personalMessage;
	}

	/**
	 * Method used to get locationType.
	 * @return the locationType.
	 */
	public String getLocationType() {

		return locationType;
	}

	/**
	 * Method used to set locationType.
	 * @param locationType
	 *            .
	 */
	public void setLocationType(String locationType) {

		this.locationType = locationType;
	}

	/**
	 * Method used to get locationName.
	 * @return locationName.
	 */
	public String getLocationName() {

		return locationName;
	}

	/**
	 * Method used to set locationName.
	 * @param locationName
	 *            .
	 */
	public void setLocationName(String locationName) {

		this.locationName = locationName;
	}

	/**
	 * Method used get state.
	 * @return state.
	 */
	public String getState() {

		return state;
	}

	/**
	 * Method used to set state.
	 * @param state
	 *            .
	 */
	public void setState(String state) {

		this.state = state;
	}

	/**
	 * Method used to get city.
	 * @return city.
	 */
	public String getCity() {

		return city;
	}

	/**
	 * Method used to set city.
	 * @param city
	 *            .
	 */
	public void setCity(String city) {

		this.city = city;
	}

	/**
	 * Method used to get office.
	 * @return office.
	 */
	public String getOffice() {

		return office;
	}

	/**
	 * Method used to set office.
	 * @param office
	 *            .
	 */
	public void setOffice(String office) {

		this.office = office;
	}

}
