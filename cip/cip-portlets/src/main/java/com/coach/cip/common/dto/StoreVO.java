
package com.coach.cip.common.dto;

import java.io.Serializable;

/**
 * 
 * Class is used to capture the characteristics of stores.
 * 
 * @author GalaxE.
 *
 */
public class StoreVO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 107499160690897217L;

	//Default Constructor
	public StoreVO(){
		
	}
	//Fully Constructor
	public StoreVO(String storenumber, String storename, String storename2, String storetype, String storetypedescription,
      String storelat,    String storelongt,    String address,    String address2,    String city,      String vm,      String state,
      String zip,      String country,      String email,      String phone,      String fax,      String hours,      String holidayhours,
      String datetype,      String startdate,      String enddate,      String status,      String floor,      String regiondescription,
      String regioncode,      String regionmgr,      String regionmgrvm,      String districtdescription,      String districtmgr,      String districtmgrvm,
      String areamgr,      String generalmanager,      String manager1,      String manager2,      String associatemgr1,      String associatemgr2,
      String associatemgr3,      String associatemgr4,      String associatemgr5,      String associatemgr6,      String assistantmgr1,      String assistantmgr2,
      String assistantmgr3,      String assistantmgr4,      String assistantmgr5,      String assistantmgr6,      String assistantmgr7)
	{
		 this.storenumber = storenumber;
	     this.storename = storename;
	     this.storename2 = storename2;
	     this.storetype = storetype;
	     this.storetypedescription = storetypedescription;
	     this.storelat = storelat;
	     this.storelongt = storelongt;
	     this.address = address;
	     this.address2 = address2;
	     this.city = city;
	     this.vm = vm;
	     this.state = state;
	     this.zip = zip;
	     this.country = country;
	     this.email = email;
	     this.phone = phone;
	     this.fax = fax;
	     this.hours = hours;
	     this.holidayhours = holidayhours;
	     this.datetype = datetype;
	     this.startdate = startdate;
	     this.enddate = enddate;
	     this.status = status;
	     this.floor = floor;
	     this.regiondescription = regiondescription;
	     this.regioncode = regioncode;
	     this.regionmgr = regionmgr;
	     this.regionmgrvm = regionmgrvm;
	     this.districtdescription = districtdescription;
	     this.districtmgr = districtmgr;
	     this.districtmgrvm = districtmgrvm;
	     this.areamgr = areamgr;
	     this.generalmanager = generalmanager;
	     this.manager1 = manager1;
	     this.manager2 = manager2;
	     this.associatemgr1 = associatemgr1;
	     this.associatemgr2 = associatemgr2;
	     this.associatemgr3 = associatemgr3;
	     this.associatemgr4 = associatemgr4;
	     this.associatemgr5 = associatemgr5;
	     this.associatemgr6 = associatemgr6;
	     this.assistantmgr1 = assistantmgr1;
	     this.assistantmgr2 = assistantmgr2;
	     this.assistantmgr3 = assistantmgr3;
	     this.assistantmgr4 = assistantmgr4;
	     this.assistantmgr5 = assistantmgr5;
	     this.assistantmgr6 = assistantmgr6;
	     this.assistantmgr7 = assistantmgr7;
	}
	
	 private String storenumber;
     private String storename;
     private String storename2;
     private String storetype;
     private String storetypedescription;
     private String storelat;
     private String storelongt;
     private String address;
     private String address2;
     private String city;
     private String vm;
     private String state;
     private String zip;
     private String country;
     private String email;
     private String phone;
     private String fax;
     private String hours;
     private String holidayhours;
     private String datetype;
     private String startdate;
     private String enddate;
     private String status;
     private String floor;
     private String regiondescription;
     private String regioncode;
     private String regionmgr;
     private String regionmgrvm;
     private String districtdescription;
     private String districtmgr;
     private String districtmgrvm;
     private String areamgr;
     private String generalmanager;
     private String manager1;
     private String manager2;
     private String associatemgr1;
     private String associatemgr2;
     private String associatemgr3;
     private String associatemgr4;
     private String associatemgr5;
     private String associatemgr6;
     private String assistantmgr1;
     private String assistantmgr2;
     private String assistantmgr3;
     private String assistantmgr4;
     private String assistantmgr5;
     private String assistantmgr6;
     private String assistantmgr7;
     
	public String getStorenumber() {
		return storenumber;
	}
	public void setStorenumber(String storenumber) {
		this.storenumber = storenumber;
	}
	public String getStorename() {
		return storename;
	}
	public void setStorename(String storename) {
		this.storename = storename;
	}
	public String getStorename2() {
		return storename2;
	}
	public void setStorename2(String storename2) {
		this.storename2 = storename2;
	}
	public String getStoretype() {
		return storetype;
	}
	public void setStoretype(String storetype) {
		this.storetype = storetype;
	}
	public String getStoretypedescription() {
		return storetypedescription;
	}
	public void setStoretypedescription(String storetypedescription) {
		this.storetypedescription = storetypedescription;
	}
	public String getStorelat() {
		return storelat;
	}
	public void setStorelat(String storelat) {
		this.storelat = storelat;
	}
	public String getStorelongt() {
		return storelongt;
	}
	public void setStorelongt(String storelongt) {
		this.storelongt = storelongt;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getfullAddress() {
		String fullAddress ="";
		if(this.address != null){
		fullAddress = 	this.address;
		}
		if(this.address2 != null){
			if(!fullAddress.isEmpty()){
				fullAddress =fullAddress+ ",";
			}
			fullAddress =fullAddress+this.address2;
		}
		return fullAddress;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getVm() {
		return vm;
	}
	public void setVm(String vm) {
		this.vm = vm;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getFax() {
		return fax;
	}
	public void setFax(String fax) {
		this.fax = fax;
	}
	public String getHours() {
		return hours;
	}
	public void setHours(String hours) {
		this.hours = hours;
	}
	public String getHolidayhours() {
		return holidayhours;
	}
	public void setHolidayhours(String holidayhours) {
		this.holidayhours = holidayhours;
	}
	public String getDatetype() {
		return datetype;
	}
	public void setDatetype(String datetype) {
		this.datetype = datetype;
	}
	public String getStartdate() {
		return startdate;
	}
	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}
	public String getEnddate() {
		return enddate;
	}
	public void setEnddate(String enddate) {
		this.enddate = enddate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getFloor() {
		return floor;
	}
	public void setFloor(String floor) {
		this.floor = floor;
	}
	public String getRegiondescription() {
		return regiondescription;
	}
	public void setRegiondescription(String regiondescription) {
		this.regiondescription = regiondescription;
	}
	public String getRegioncode() {
		return regioncode;
	}
	public void setRegioncode(String regioncode) {
		this.regioncode = regioncode;
	}
	public String getRegionmgr() {
		return regionmgr;
	}
	public void setRegionmgr(String regionmgr) {
		this.regionmgr = regionmgr;
	}
	public String getRegionmgrvm() {
		return regionmgrvm;
	}
	public void setRegionmgrvm(String regionmgrvm) {
		this.regionmgrvm = regionmgrvm;
	}
	public String getDistrictdescription() {
		return districtdescription;
	}
	public void setDistrictdescription(String districtdescription) {
		this.districtdescription = districtdescription;
	}
	public String getDistrictmgr() {
		return districtmgr;
	}
	public void setDistrictmgr(String districtmgr) {
		this.districtmgr = districtmgr;
	}
	public String getDistrictmgrvm() {
		return districtmgrvm;
	}
	public void setDistrictmgrvm(String districtmgrvm) {
		this.districtmgrvm = districtmgrvm;
	}
	public String getAreamgr() {
		return areamgr;
	}
	public void setAreamgr(String areamgr) {
		this.areamgr = areamgr;
	}
	public String getGeneralmanager() {
		return generalmanager;
	}
	public void setGeneralmanager(String generalmanager) {
		this.generalmanager = generalmanager;
	}
	public String getManager1() {
		return manager1;
	}
	public void setManager1(String manager1) {
		this.manager1 = manager1;
	}
	public String getManager2() {
		return manager2;
	}
	public void setManager2(String manager2) {
		this.manager2 = manager2;
	}
	public String getAssociatemgr1() {
		return associatemgr1;
	}
	public void setAssociatemgr1(String associatemgr1) {
		this.associatemgr1 = associatemgr1;
	}
	public String getAssociatemgr2() {
		return associatemgr2;
	}
	public void setAssociatemgr2(String associatemgr2) {
		this.associatemgr2 = associatemgr2;
	}
	public String getAssociatemgr3() {
		return associatemgr3;
	}
	public void setAssociatemgr3(String associatemgr3) {
		this.associatemgr3 = associatemgr3;
	}
	public String getAssociatemgr4() {
		return associatemgr4;
	}
	public void setAssociatemgr4(String associatemgr4) {
		this.associatemgr4 = associatemgr4;
	}
	public String getAssociatemgr5() {
		return associatemgr5;
	}
	public void setAssociatemgr5(String associatemgr5) {
		this.associatemgr5 = associatemgr5;
	}
	public String getAssociatemgr6() {
		return associatemgr6;
	}
	public void setAssociatemgr6(String associatemgr6) {
		this.associatemgr6 = associatemgr6;
	}
	public String getAssistantmgr1() {
		return assistantmgr1;
	}
	public void setAssistantmgr1(String assistantmgr1) {
		this.assistantmgr1 = assistantmgr1;
	}
	public String getAssistantmgr2() {
		return assistantmgr2;
	}
	public void setAssistantmgr2(String assistantmgr2) {
		this.assistantmgr2 = assistantmgr2;
	}
	public String getAssistantmgr3() {
		return assistantmgr3;
	}
	public void setAssistantmgr3(String assistantmgr3) {
		this.assistantmgr3 = assistantmgr3;
	}
	public String getAssistantmgr4() {
		return assistantmgr4;
	}
	public void setAssistantmgr4(String assistantmgr4) {
		this.assistantmgr4 = assistantmgr4;
	}
	public String getAssistantmgr5() {
		return assistantmgr5;
	}
	public void setAssistantmgr5(String assistantmgr5) {
		this.assistantmgr5 = assistantmgr5;
	}
	public String getAssistantmgr6() {
		return assistantmgr6;
	}
	public void setAssistantmgr6(String assistantmgr6) {
		this.assistantmgr6 = assistantmgr6;
	}
	public String getAssistantmgr7() {
		return assistantmgr7;
	}
	public void setAssistantmgr7(String assistantmgr7) {
		this.assistantmgr7 = assistantmgr7;
	}
	
	private String storeDetailHeader;

	public String getStoreDetailHeader() {
		storeDetailHeader = getCity()+", "+getState()+" "+getZip();
		return storeDetailHeader;
	}
	public void setStoreDetailHeader(String storeDetailHeader) {
		this.storeDetailHeader = storeDetailHeader;
	}
	
}
