package com.coach.cip.portlets.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * Class is used to capture the characteristics of taskInstructions.
 * 
 * @author GalaxE.
 *
 */

@Controller("taskInstructionsController")
@RequestMapping(value = "VIEW")
public class TaskInstructionsController extends AbstractBaseController {

	/** The Constant LOG. */
	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(TaskInstructionsController.class);
	private static final String TASK_INSTRUCTIONS = "taskInstructions";
	
	/**
	 * Method used to render notifications view.
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	@RenderMapping
	public String showTaskInstructions(RenderRequest request,RenderResponse response) {
		LoggerUtil.debugLogger(logger,"Render mapping for TaskInstructions view in TaskInstructionsController");
		return TASK_INSTRUCTIONS;
	}
	
	/**
	 * Method serves as resource method to storeOffices.
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 * @throws SystemException
	 */
	@ResourceMapping("populateTasks")
	public ModelAndView populateTasks(ResourceRequest request, ResourceResponse response) throws IOException, SystemException {
		LoggerUtil.debugLogger(logger,"Inside populateTasks method in TaskInstructionsController");
		Map<String, String> modelMap = new HashMap<String, String>();
		String tempData = request.getParameter("tempData");
		modelMap.put("articleId", tempData);
		return new ModelAndView("tasksResource", modelMap);
	}
}
