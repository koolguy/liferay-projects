package com.coach.cip.common.dto;

import java.io.Serializable;

/**
 * 
 * Class is used to capture the characteristics of HRCaseServiceVO.
 * 
 * @author GalaxE.
 *
 */
public class HRCaseServiceVO implements Serializable{

	private static final long serialVersionUID = -912093456930409875L;
	
	/**
	 * default Constructor.
	 */
	public HRCaseServiceVO(){
		
	}
	
	/**
	 * @param caseSysId
	 * @param caseDescription
	 */
	public HRCaseServiceVO(String caseSysId, String caseDescription) {
		this.caseSysId = caseSysId;
		this.caseDescription = caseDescription;
	}
	
	private String caseSysId;
	private String caseDescription;
	
	public String getCaseSysId() {
		return caseSysId;
	}

	public void setCaseSysId(String caseSysId) {
		this.caseSysId = caseSysId;
	}

	public String getCaseDescription() {
		return caseDescription;
	}

	public void setCaseDescription(String caseDescription) {
		this.caseDescription = caseDescription;
	}
}
