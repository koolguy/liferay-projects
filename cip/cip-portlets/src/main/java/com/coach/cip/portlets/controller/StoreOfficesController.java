package com.coach.cip.portlets.controller;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;
import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * Class is used to capture the characteristics of offices.
 * 
 * @author GalaxE.
 *
 */

@Controller("storeOfficesController")
@RequestMapping(value = "VIEW")
public class StoreOfficesController extends AbstractBaseController {
	
	/** The Constant LOG. */
	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(StoreOfficesController.class);
	private static final String OFFICES_VIEW = "storeOffices";
	
	/**
	 * Renders storeOffices view.
	 * 
	 * @param renderresponse
	 * @return
	 */
	@RenderMapping
	public String showStoreOffices(RenderResponse renderresponse) {
		LoggerUtil.debugLogger(logger,"Default render mapping for showStoreOffices view in StoreOfficesController");
		return OFFICES_VIEW;
	}
	
	/**
	 * Method serves as resource method to storeOffices.
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 * @throws SystemException
	 */
	@ResourceMapping("populateStoreOffices")
	public ModelAndView populateStoreOffices(ResourceRequest request, ResourceResponse response) throws IOException, SystemException {
		LoggerUtil.debugLogger(logger,"Inside populateStoreOffices method in StoreOfficesController");
		Map<String, String> modelMap = new HashMap<String, String>();
		String tempData = request.getParameter("tempData");
		modelMap.put("articleId", tempData.split("::")[0]);
		modelMap.put("articleTitle", tempData.split("::")[1]);
		modelMap.put("brand", tempData.split("::")[2]);
		return new ModelAndView("officesResource", modelMap);
	}
	
	/**
	 * Mathod serves as a action method to search offices.
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 */
	@SuppressWarnings("unused")
	@ActionMapping(params="myaction=processOfficesWebContentURL")
	public void processOfficesWebContentURL(ActionRequest actionRequest, ActionResponse actionResponse, Model model){
		
		String fileTitle = actionRequest.getParameter("fileTitle") != null ? actionRequest.getParameter("fileTitle") : "";
		String fileURL = actionRequest.getParameter("fileURL") != null ? actionRequest.getParameter("fileURL") : "";
		String fileType = actionRequest.getParameter("fileType") != null ? actionRequest.getParameter("fileType") : "";
		String classPK = actionRequest.getParameter("classPK") != null ? actionRequest.getParameter("classPK") : "";
		String typeOfLink = actionRequest.getParameter("typeOfLink") != null ? actionRequest.getParameter("typeOfLink") : "";
		String mimeType = actionRequest.getParameter("mimeType") != null ? actionRequest.getParameter("mimeType") : "";
		String backButton = actionRequest.getParameter("backButton") != null ? actionRequest.getParameter("backButton") : "false";
		String redirectUrl = actionRequest.getParameter("redirectUrl") != null ? actionRequest.getParameter("redirectUrl") : "";
		String redirect = actionRequest.getParameter("redirect") != null ? actionRequest.getParameter("redirect") : "";
		
		model.addAttribute("fileTitle", fileTitle);
		model.addAttribute("fileURL", fileURL);
		model.addAttribute("backButton", backButton);
		model.addAttribute("redirectUrl", redirectUrl);
		model.addAttribute("redirect", redirect);
		
	}
}
