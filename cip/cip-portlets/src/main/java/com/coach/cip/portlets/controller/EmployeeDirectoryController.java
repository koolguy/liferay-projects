package com.coach.cip.portlets.controller;

import java.io.IOException;
import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.WindowStateException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.util.WebUtils;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.coach.cip.common.dto.DirectReportVO;
import com.coach.cip.common.dto.EmployeeDirectorySearchVO;
import com.coach.cip.common.dto.EmployeeProfileVO;
import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.coach.cip.util.WebUtil;
import com.coach.employees.service.beans.GetAllEmployeesDepartmentsFault;
import com.coach.employees.service.beans.GetAllEmployeesDepartmentsRequest;
import com.coach.employees.service.beans.GetAllEmployeesDepartmentsRequest.EMPDepartment;
import com.coach.employees.service.beans.GetAllEmployeesDepartmentsResponse;
import com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsRequest;
import com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsRequest.EMPByDetails;
import com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse;
import com.coach.employees.service.beans.GetEmployeesByIDFault;
import com.coach.employees.service.beans.GetEmployeesByIDRequest;
import com.coach.employees.service.beans.GetEmployeesByIDRequest.EMPByUserId;
import com.coach.employees.service.beans.GetEmployeesByIDResponse;
import com.coach.employees.service.beans.GetEmployeesByKeywordFault;
import com.coach.employees.service.beans.GetEmployeesByKeywordRequest;
import com.coach.employees.service.beans.GetEmployeesByKeywordRequest.EMPByKeyword;
import com.coach.employees.service.beans.GetEmployeesByKeywordResponse;
import com.coach.employees.service.beans.GetEmployeesByNameFault;
import com.coach.employees.service.beans.GetEmployeesByNameRequest;
import com.coach.employees.service.beans.GetEmployeesByNameRequest.EMPByName;
import com.coach.employees.service.beans.GetEmployeesByNameResponse;
import com.coach.employees.service.beans.GetEmployessByEmployeeDetailsFault;
import com.coach.employees.service.beans.ObjectFactory;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.theme.PortletDisplay;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;

/**
 * Class is used to search the employee directory.
 * 
 * @author GalaxE.
 */
@Controller("employeeDirectoryController")
public class EmployeeDirectoryController extends AbstractBaseController {

	/** The Log Constant */
	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil
			.getLog(EmployeeDirectoryController.class);
	List<EmployeeDirectorySearchVO> searchVoList = new ArrayList<EmployeeDirectorySearchVO>();

	@Autowired
	@Qualifier("employeeServiceTemplate")
	private WebServiceTemplate webServiceTemplate;
	private static final ObjectFactory WS_CLIENT_FACTORY = new ObjectFactory();

	/**
	 * Default Constructor.
	 */
	public EmployeeDirectoryController() {

	}

	/**
	 * Constructor which initializes webserviceTemplate.
	 * 
	 * @param webServiceTemplate
	 */
	public EmployeeDirectoryController(WebServiceTemplate webServiceTemplate) {

		LoggerUtil.debugLogger(logger,
				"Inside EmployeeDirectoryController construtor::");
		this.webServiceTemplate = webServiceTemplate;
	}

	/**
	 * Show Employee directory search by default and if it is redirected form
	 * default search then it should show the employee profile page.
	 * 
	 * @param renderResponse
	 *            the render response
	 * @return navigation string {employeeDirectorySearch}
	 * @throws SystemException
	 * @throws PortalException
	 */
	@RenderMapping
	public String showEmployeeDirectory(RenderRequest request,
			RenderResponse renderResponse, Model model) throws PortalException,
			SystemException {

		LoggerUtil
				.debugLogger(
						logger,
						"Start of render mapping  showEmployeeDirectory() method of EmpooyeeDirectoryController::");

		HttpServletRequest servletRequest = WebUtil
				.getHttpServletRequest(request);
		String interProfilePage = servletRequest
				.getParameter(Constants.INTER_PROFILE_PAGE);

		if (interProfilePage != null && interProfilePage.equals("true")) {
			EmployeeProfileVO employeeProfileVO = null;
			String empRedirectURL = Constants.EMPTY;
			String viewpage = Constants.EMPTY;
			String errorMessage = Constants.EMPTY;
			PortletSession session = request.getPortletSession();
			if (session.getAttribute(Constants.ERROR_MESSAGE) != null) {
				errorMessage = (String) session
						.getAttribute(Constants.ERROR_MESSAGE);
			}
			if (session.getAttribute(Constants.EMPLOYEE_PROFILE_VO) != null) {
				employeeProfileVO = (EmployeeProfileVO) session
						.getAttribute(Constants.EMPLOYEE_PROFILE_VO);
			}
			if (session.getAttribute(Constants.VIEWPAGE) != null) {
				viewpage = (String) session.getAttribute(Constants.VIEWPAGE);
			}
			if (session.getAttribute(Constants.EMPLOYEE_REDIRECT_URL) != null) {
				empRedirectURL = (String) session
						.getAttribute(Constants.EMPLOYEE_REDIRECT_URL);
			}
			if (employeeProfileVO != null) {
				model.addAttribute(Constants.SEARCHED_EMP_OBJECT,
						employeeProfileVO);
			} else {
				model.addAttribute(Constants.ERROR_MESSAGE, errorMessage);
			}
			if (empRedirectURL != null && !empRedirectURL.isEmpty()) {
				model.addAttribute(Constants.EMPLOYEE_REDIRECT_URL,
						empRedirectURL);
			}
			if (viewpage != null && !viewpage.isEmpty()) {
				model.addAttribute(Constants.VIEW_PAGE, viewpage);
			}

			LoggerUtil
					.debugLogger(
							logger,
							"End of render mapping  showEmployeeDirectory() method of EmpooyeeDirectoryController::");
			return Constants.EMPLOYEE_DIRECTORY_SEARCH_DETAIL;
		}
		LoggerUtil
				.debugLogger(
						logger,
						"End of render mapping  showEmployeeDirectory() method of EmpooyeeDirectoryController::");
		return Constants.EMPLOYEE_DIRECTORY_SEARCH;
	}

	/**
	 * Populate all Departments based by calling Web Service methods
	 * getAllEmployeesDepartments().
	 * 
	 * @param renderResponse
	 * @return employeeDirectorySearchVO
	 * @throws PortalException
	 * @throws SystemException
	 * @throws RemoteException
	 */
	@SuppressWarnings("unchecked")
	@ModelAttribute(value = "employeeDirectorySearchVO")
	public EmployeeDirectorySearchVO getValidValues(
			PortletRequest portletRequest, Model model) throws PortalException,
			SystemException, RemoteException {

		LoggerUtil
				.debugLogger(logger,
						"Start of model  getValidValues() method of EmpooyeeDirectoryController::");
		EmployeeDirectorySearchVO employeeDirectorySearchVO = new EmployeeDirectorySearchVO();
		List<String> deptList = null;
		HttpServletRequest httpReq = PortalUtil
				.getOriginalServletRequest(PortalUtil
						.getHttpServletRequest(portletRequest));
		String callNextTime = null;
		deptList = (List<String>) WebUtils.getSessionAttribute(httpReq,
				Constants.DEPARTMENT_LIST);
		callNextTime = (String) WebUtils.getSessionAttribute(httpReq,
				Constants.NEXT_CALL);

		
		if ((deptList == null || deptList.isEmpty())
				&& (callNextTime == null || callNextTime.isEmpty())) {

			//deptList = getAllEmployeesDepartment(model);
			WebUtils.setSessionAttribute(httpReq, Constants.DEPARTMENT_LIST,
					deptList);
			employeeDirectorySearchVO.setDeptList(deptList);
		} else {
			employeeDirectorySearchVO.setDeptList(deptList);
		}

		WebUtils.setSessionAttribute(httpReq, Constants.NEXT_CALL,
				Constants.YES);
		model.addAttribute(Constants.SEARCH_VO_LIST, searchVoList);
		LoggerUtil
				.debugLogger(logger,
						"End of model  getValidValues() method of EmpooyeeDirectoryController::");
		return employeeDirectorySearchVO;
	}

	/**
	 * Get all employees department by calling the webservice method
	 * GetAllEmployeesDepartment.
	 * 
	 * @param employeeDirectorySearchVO
	 */
	@SuppressWarnings("unused")
	private List<String> getAllEmployeesDepartment(Model model) {

		LoggerUtil
				.debugLogger(logger,
						"Start of getAllEmployeesDepartment() method of EmpooyeeDirectoryController::");
		List<String> deptList = null;
		GetAllEmployeesDepartmentsRequest allEmployeesDepartmentsRequest = WS_CLIENT_FACTORY
				.createGetAllEmployeesDepartmentsRequest();
		EMPDepartment department = new EMPDepartment();
		allEmployeesDepartmentsRequest.setEMPDepartment(department);
		try {
			WebUtil.trust();
			Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat(
					"E yyyy.MM.dd 'at' hh:mm:ss a zzz");

			//System.out.println("Current Date: " + ft.format(dNow));
			LoggerUtil.infoLogger(logger, ft.format(dNow));
			Object objectResponse = webServiceTemplate
					.marshalSendAndReceive(allEmployeesDepartmentsRequest);
			Date dNow1 = new Date();
			SimpleDateFormat ft1 = new SimpleDateFormat(
					"E yyyy.MM.dd 'at' hh:mm:ss a zzz");

			//System.out.println("Current Date: " + ft1.format(dNow1));
			LoggerUtil.infoLogger(logger, ft1.format(dNow1));
			if (objectResponse instanceof GetAllEmployeesDepartmentsResponse) {

				GetAllEmployeesDepartmentsResponse allEmployeesDepartmentsResponse = (GetAllEmployeesDepartmentsResponse) objectResponse;
				if (allEmployeesDepartmentsResponse != null
						&& allEmployeesDepartmentsResponse.getDepartment() != null
						&& !allEmployeesDepartmentsResponse.getDepartment()
								.isEmpty()) {

					deptList = allEmployeesDepartmentsResponse.getDepartment();

				}
			} else if (objectResponse instanceof GetAllEmployeesDepartmentsFault) {

				GetAllEmployeesDepartmentsFault departmentFault = (GetAllEmployeesDepartmentsFault) objectResponse;
				model.addAttribute(Constants.FAULT_MESSAGE,
						Constants.ERR_WEBSERVICE_PROCESSING);
				LoggerUtil.debugLogger(logger, departmentFault.getErrDesc());
			}
		} catch (SoapFaultClientException sfce) {

			LoggerUtil.errorLogger(logger, sfce.getMessage(), sfce);
			model.addAttribute(Constants.FAULT_MESSAGE,
					Constants.ERR_WEBSERVICE_PROCESSING);
		} catch (Exception ex) {

			LoggerUtil.errorLogger(logger, ex.getMessage(), ex);
			model.addAttribute(Constants.FAULT_MESSAGE,
					Constants.ERR_WEBSERVICE_PROCESSING);
		}
		LoggerUtil
				.debugLogger(logger,
						"End of getAllEmployeesDepartment() method of EmpooyeeDirectoryController::");
		return deptList;
	}

	/**
	 * Get the list of employees based on entered criteria (FirstName, LastName,
	 * Title, Geography, Department, Email and PhoneExtension) by calling the
	 * Web Service method getEmployeesByEmployeeDetails().
	 * 
	 * @param employeeDirectorySearchVO
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 *            contains the EmployeeSearchVoList.
	 */

	@ActionMapping(params = "myaction=getSearchResult")
	public void getSearchResult(ActionRequest actionRequest,
			ActionResponse actionResponse, Model model) throws RemoteException {
		
		System.out.println("EmployeeDirectoryController.getSearchResult() Before Thread Name -----"+Thread.currentThread( ).getName());
		LoggerUtil.infoLogger(logger, "EmployeeDirectoryController.getSearchResult() Before Thread Name -----"+Thread.currentThread( ).getName());
		System.out.println("EmployeeDirectoryController.getSearchResult() Before Thread State -----"+Thread.currentThread( ).getState());
		LoggerUtil.infoLogger(logger, "EmployeeDirectoryController.getSearchResult() Before Thread State -----"+Thread.currentThread( ).getState());
		WebUtil.getHeapSize();
		LoggerUtil
				.debugLogger(
						logger,
						"Inside the action mapping getSearchResult() method of EmpooyeeDirectoryController::");
		searchVoList = getEmployeeByEmployeeDetails(actionRequest, model);
		Collections.sort(searchVoList);
		try {
			model.addAttribute(Constants.SEARCH_VO_LIST, searchVoList);
			actionResponse.setRenderParameter(Constants.MY_ACTION,
					Constants.EMPLOYEE_DIRECTORY_RESULT_PAGE);
			//actionResponse.setWindowState(WindowState.MAXIMIZED);
			actionResponse.setRenderParameter("p_p_state", "maximized");
		} catch (Exception ex) {
			LoggerUtil.errorLogger(logger, ex.getMessage(), ex);
		}
		WebUtil.getHeapSize();
		System.out.println("EmployeeDirectoryController.getSearchResult() After Thread Name -----"+Thread.currentThread( ).getName());
		LoggerUtil.infoLogger(logger, "EmployeeDirectoryController.getSearchResult() After Thread Name -----"+Thread.currentThread( ).getName());
		System.out.println("EmployeeDirectoryController.getSearchResult() After Thread State -----"+Thread.currentThread( ).getState());
		LoggerUtil.infoLogger(logger, "EmployeeDirectoryController.getSearchResult() After Thread State -----"+Thread.currentThread( ).getState());

	}

	/**
	 * Redirect to Employee directory search page.
	 * 
	 * @return navigation string {employeeDirectorySearchResult}
	 */
	@RenderMapping(params = "myaction=employeeDirectoryResultPage")
	public String showSearchResult(RenderRequest request,
			RenderResponse response, Model model) {

		LoggerUtil
				.debugLogger(
						logger,
						"Inside the render mapping showSearchResult() method of EmpooyeeDirectoryController::");
		return Constants.EMPLOYEE_DIRECTORY_SEARCH_RESULT;
	}

	/**
	 * Get the Employee Details based on Employee ID by calling the Web Service
	 * method getEmployeeById().
	 * 
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 *            contains the Details of the Employee.
	 * @throws WindowStateException
	 * @throws IOException
	 */
	@ActionMapping(params = "myaction=getEmployeeDetail")
	public void getEmployeeDetail(ActionRequest actionRequest,
			ActionResponse actionResponse, Model model)
			throws WindowStateException, IOException {

		LoggerUtil
				.debugLogger(
						logger,
						"Inside the action mapping getEmployeeDetail() method of EmpooyeeDirectoryController::");
		String empId = actionRequest.getParameter(Constants.EMPLOYEE_ID);
		LoggerUtil.debugLogger(logger, "Employee ID ::" + empId);
		model.addAttribute(Constants.SEARCHED_EMP_OBJECT,
				getEmployeeByID(empId, model));
		model.addAttribute(Constants.VIEW_PAGE,
				actionRequest.getParameter(Constants.VIEW_PAGE));	
		actionResponse.setRenderParameter("p_p_state", "maximized");
		actionResponse.setRenderParameter(Constants.MY_ACTION,
				Constants.EMPLOYEE_DIRECTORY_DETAIL_PAGE);

	}

	/**
	 * Get the Employee Details based on Employee ID by calling the Web Service
	 * method getEmployeeProfile().
	 * 
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 *            contains the Details of the Employee.
	 * @throws WindowStateException
	 * @throws IOException
	 */
	@ActionMapping(params = "myaction=getEmployeeProfile")
	public void getEmployeeProfile(ActionRequest actionRequest,
			ActionResponse actionResponse, Model model,
			PortletDisplay portletDisplay) throws WindowStateException,
			IOException {

		LoggerUtil
				.debugLogger(
						logger,
						"Inside the action mapping getEmployeeProfile() method of EmpooyeeDirectoryController::");

		String empRedirectURL = Constants.EMPTY;
		String viewpage = Constants.EMPTY;
		String empId = actionRequest.getParameter(Constants.EMPLOYEE_ID);
		if (actionRequest.getParameter(Constants.EMPLOYEE_REDIRECT_URL) != null) {

			empRedirectURL = actionRequest
					.getParameter(Constants.EMPLOYEE_REDIRECT_URL);
		}
		if (actionRequest.getParameter(Constants.VIEW_PAGE) != null) {

			viewpage = actionRequest.getParameter(Constants.VIEW_PAGE);
		}
		HashMap<String, String> empProfileMap = new HashMap<String, String>();
		empProfileMap.put(Constants.EMPLOYEE_ID, empId);
		empProfileMap.put(Constants.EMPLOYEE_REDIRECT_URL, empRedirectURL);
		empProfileMap.put(Constants.VIEWPAGE, viewpage);
		QName qName = new QName(Constants.IPC_URL,
				Constants.EMPLOYEE_PROFILE_IPC);
		actionResponse.setEvent(qName, empProfileMap);
		ThemeDisplay themeDisplay = WebUtil.getThemeDisplay(actionRequest);
		String redirectUrl = "/"+themeDisplay.getLanguageId()+"/group"+ themeDisplay.getScopeGroup().getFriendlyURL()+"/employeeprofile?interProfilePage=true";
		actionResponse.sendRedirect(redirectUrl);

	}

	/**
	 * Event to get the employee profile page.
	 * 
	 * @param request
	 * @param response
	 * @param model
	 */
	@SuppressWarnings("unchecked")
	@EventMapping(value = "{http://coach.com/events}ipc.empProfile")
	public void getEmpProfileBySearch(EventRequest request,
			EventResponse response, Model model) {

		LoggerUtil
				.debugLogger(
						logger,
						"Inside the Event mapping getEmpProfileBySearch() method of EmpooyeeDirectoryController::");

		String empRedirectURL = Constants.EMPTY;
		String viewpage = Constants.EMPTY;
		String empId = Constants.EMPTY;
		PortletSession session = request.getPortletSession();
		Event event = request.getEvent();
		HashMap<String, String> empProfileMap = (HashMap<String, String>) event
				.getValue();
		empId = empProfileMap.get(Constants.EMPLOYEE_ID);
		viewpage = empProfileMap.get(Constants.VIEWPAGE);
		empRedirectURL = empProfileMap.get(Constants.EMPLOYEE_REDIRECT_URL);
		EmployeeProfileVO employeeProfileVO = null;
		employeeProfileVO = getEmployeeByID(empId, model);
		if (employeeProfileVO != null) {

			session.setAttribute(Constants.EMPLOYEE_PROFILE_VO,
					employeeProfileVO);
			session.setAttribute(Constants.EMPLOYEE_REDIRECT_URL,
					empRedirectURL);
			session.setAttribute(Constants.VIEWPAGE, viewpage);
			session.setAttribute(Constants.ERROR_MESSAGE, Constants.EMPTY);
		} else {

			session.setAttribute(Constants.ERROR_MESSAGE,
					Constants.TECHNICAL_ERROR_MESSAGE);
			session.setAttribute(Constants.EMPLOYEE_REDIRECT_URL,
					empRedirectURL);
			session.setAttribute(Constants.VIEWPAGE, viewpage);
			session.setAttribute(Constants.EMPLOYEE_PROFILE_VO, null);

		}
	}

	/**
	 * Navigate to Employee Directory Profile Page.
	 * 
	 * @return navigation string {employeeDirectorySearchDetail}
	 */
	@RenderMapping(params = "myaction=employeeDirectoryDetailPage")
	public String showSearchDetail(RenderRequest request,
			RenderResponse response, Model model) {

		LoggerUtil
				.debugLogger(
						logger,
						"Inside the render mapping showSearchDetail() method of EmpooyeeDirectoryController::");
		return Constants.EMPLOYEE_DIRECTORY_SEARCH_DETAIL;
	}

	/**
	 * Get list of employees based on firsName, LastName, Title, Department,
	 * Geography, Email & PhoneExtension.
	 * 
	 * @param actionRequest
	 * @param model
	 * @return list of EmployeeDirectorySearchVO
	 */
	public List<EmployeeDirectorySearchVO> getEmployeeByEmployeeDetails(
			ActionRequest actionRequest, Model model) {

		LoggerUtil.debugLogger(logger,"Start of getEmployeeByEmployeeDetails() method of EmpooyeeDirectoryController::");
		List<EmployeeDirectorySearchVO> list = new ArrayList<EmployeeDirectorySearchVO>();
		List<com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees.Employee> employeeList = null;
		GetEmployeesByEmployeeDetailsRequest employeesByEmployeeDetailsRequest = WS_CLIENT_FACTORY.createGetEmployeesByEmployeeDetailsRequest();
		EmployeeDirectorySearchVO employeeDataVO = null;
		EMPByDetails empDetails = new EMPByDetails();
		
		String firstName = null;
		String lastName = null;
		String title = null;
		String department = null;
		String geography = null;
		String email = null;
		String phoneExtn= null;
		
		String key = ParamUtil.getString(actionRequest,"key");
		
		if(key != null && key.equalsIgnoreCase("fromPortalNormal")){
			
			LoggerUtil.debugLogger(logger,"Request came from portalNormal employeeDirectorySearch:: Start of getEmployeeByEmployeeDetails() method of EmpooyeeDirectoryController::");
			
			String firstNameAndOrLastName = ParamUtil.getString(actionRequest,"firstNameAndOrLastName");
			StringTokenizer keywordString = new StringTokenizer(firstNameAndOrLastName);
			if (keywordString.hasMoreTokens()) {

				firstName = keywordString.nextToken();
			}
			if (keywordString.hasMoreTokens()) {

				lastName = keywordString.nextToken();
			}
			
			if (Validator.isNotNull(firstName)) {
				firstName = firstName + Constants.STAR;
			}
			if (Validator.isNotNull(lastName)) {
				lastName = lastName + Constants.STAR;
			}
			String keyword = null;
			
			/** Employee details search based on First Name and Last Name. */
			if(firstName != null && !firstName.isEmpty() && lastName != null && !lastName.isEmpty()) {
				
				list = getEmployeesByNameRequest(firstName, lastName,model);
				
			}		 	
			/** Employee details search based on keyword. */
			else if (firstName != null && !firstName.isEmpty() && (lastName == null || lastName.isEmpty())) {
	
				keyword = firstName;
				list = getEmployeesByKeywordRequest(keyword,model);
				
			}
					
			empDetails.setStrFirstName(firstName);
			empDetails.setStrLastName(lastName);
			employeesByEmployeeDetailsRequest.setEMPByDetails(empDetails);
			
		}else{
		
			 LoggerUtil.debugLogger(logger,"Request came from employeeDirectorySearch :: Start of getEmployeeByEmployeeDetails() method of EmpooyeeDirectoryController::");
			 firstName = ParamUtil.getString(actionRequest,Constants.FIRST_NAME, Constants.EMPTY);
			 lastName = ParamUtil.getString(actionRequest,Constants.LAST_NAME, Constants.EMPTY);
			 title = ParamUtil.getString(actionRequest, Constants.TITLE,Constants.EMPTY);
			 department = ParamUtil.getString(actionRequest,Constants.DEPARTMENT, Constants.EMPTY);
			 geography = ParamUtil.getString(actionRequest,Constants.GEOGRAPHY, Constants.EMPTY);
			 email = ParamUtil.getString(actionRequest, Constants.EMAIL,Constants.EMPTY);
			 phoneExtn = ParamUtil.getString(actionRequest,Constants.PHONE_EXTENSION, Constants.EMPTY);
			
			if (Validator.isNotNull(firstName)) {
			
				firstName = firstName + Constants.STAR;
			}
			if (Validator.isNotNull(lastName)) {
			
				lastName = lastName + Constants.STAR;
			}
			if (Validator.isNotNull(email)) {
			
				email = email + Constants.STAR;
			}		
			empDetails.setStrFirstName(firstName);
			empDetails.setStrLastName(lastName);
			empDetails.setStrTitle(title);
			empDetails.setStrDeptName(department);
			empDetails.setLocation(geography);
			empDetails.setStrEmail(email);
			empDetails.setStrPhone(phoneExtn);
			employeesByEmployeeDetailsRequest.setEMPByDetails(empDetails);
			
			try {
				WebUtil.trust();
				Date dNow = new Date();
				SimpleDateFormat ft = new SimpleDateFormat(
						"E yyyy.MM.dd 'at' hh:mm:ss a zzz");

				//System.out.println("Current Date: " + ft.format(dNow));
				LoggerUtil.infoLogger(logger, ft.format(dNow));
				
				System.out.println("EmployeeDirectoryController.getEmployeeByEmployeeDetails() Before Thread Name -----"+Thread.currentThread( ).getName());
				LoggerUtil.infoLogger(logger, "EmployeeDirectoryController.getEmployeeByEmployeeDetails() Before Thread Name -----"+Thread.currentThread( ).getName());
				System.out.println("EmployeeDirectoryController.getEmployeeByEmployeeDetails() Before Thread State -----"+Thread.currentThread( ).getState());
				LoggerUtil.infoLogger(logger, "EmployeeDirectoryController.getEmployeeByEmployeeDetails() Before Thread State -----"+Thread.currentThread( ).getState());
				
				Object responseObject = webServiceTemplate
						.marshalSendAndReceive(employeesByEmployeeDetailsRequest);
				
				System.out.println("EmployeeDirectoryController.getEmployeeByEmployeeDetails() After Thread Name -----"+Thread.currentThread( ).getName());
				LoggerUtil.infoLogger(logger, "EmployeeDirectoryController.getEmployeeByEmployeeDetails() After Thread Name -----"+Thread.currentThread( ).getName());
				System.out.println("EmployeeDirectoryController.getEmployeeByEmployeeDetails() After Thread State -----"+Thread.currentThread( ).getState());
				LoggerUtil.infoLogger(logger, "EmployeeDirectoryController.getEmployeeByEmployeeDetails() After Thread State -----"+Thread.currentThread( ).getState());
				
				Date dNow1 = new Date();
				SimpleDateFormat ft1 = new SimpleDateFormat(
						"E yyyy.MM.dd 'at' hh:mm:ss a zzz");

				//System.out.println("Current Date: " + ft1.format(dNow1));
				LoggerUtil.infoLogger(logger, ft1.format(dNow1));
				if (responseObject instanceof GetEmployeesByEmployeeDetailsResponse) {

					GetEmployeesByEmployeeDetailsResponse employeesByEmployeeDetailsResponse = (GetEmployeesByEmployeeDetailsResponse) responseObject;
					if (employeesByEmployeeDetailsResponse != null
							&& employeesByEmployeeDetailsResponse.getEmployees() != null
							&& employeesByEmployeeDetailsResponse.getEmployees()
									.getEmployee() != null
							&& employeesByEmployeeDetailsResponse.getEmployees()
									.getEmployee().size() > 0) {

						employeeList = employeesByEmployeeDetailsResponse
								.getEmployees().getEmployee();
						if (employeeList != null && !employeeList.isEmpty()
								&& employeeList.size() > 0) {
							for (com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees.Employee employee : employeeList) {

								employeeDataVO = new EmployeeDirectorySearchVO();
								employeeDataVO.setEmpName(employee.getFIRSTNAME()
										+ Constants.SPACE + employee.getLASTNAME());
								employeeDataVO.setEmpTitle(employee.getTITLE());
								employeeDataVO.setEmpDepartment(employee
										.getDEPARTMENT());
								employeeDataVO.setOffice(employee.getOFFICENAME());
								employeeDataVO.setEmpGeography(employee
										.getGEOGRAPHY());
								employeeDataVO.setEmpPhone(employee.getPHONE());
								employeeDataVO.setEmailAddress(employee.getEMAIL());
								employeeDataVO.setEmpId(employee.getUSERID());
								list.add(employeeDataVO);
							}
						}
					}
				} else if (responseObject instanceof GetEmployessByEmployeeDetailsFault) {

					GetEmployessByEmployeeDetailsFault employeeDetailsFault = (GetEmployessByEmployeeDetailsFault) responseObject;
					model.addAttribute(Constants.FAULT_MESSAGE,
							Constants.NO_RESULT_FOUND);
					LoggerUtil.debugLogger(logger,
							employeeDetailsFault.getErrDesc());
				}
			} catch (SoapFaultClientException sfce) {

				LoggerUtil.errorLogger(logger, sfce.getMessage(), sfce);
				model.addAttribute(Constants.FAULT_MESSAGE,
						Constants.ERR_WEBSERVICE_PROCESSING);
			} catch (Exception ex) {

				LoggerUtil.errorLogger(logger, ex.getMessage(), ex);
				model.addAttribute(Constants.FAULT_MESSAGE,
						Constants.ERR_WEBSERVICE_PROCESSING);
			}
			LoggerUtil.debugLogger(logger,"End of getEmployeeByEmployeeDetails() method of EmpooyeeDirectoryController::");
			
		}	
		
		return list;

	}

	private List<EmployeeDirectorySearchVO> getEmployeesByKeywordRequest(String keyword, Model model) {
		
		LoggerUtil.debugLogger(logger,"employeeDirectorySearch:: Start of getEmployeesByKeywordRequest() method of EmpooyeeDirectoryController::");
		
		List<EmployeeDirectorySearchVO> list = new ArrayList<EmployeeDirectorySearchVO>();
		List<com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees.Employee> employeeList = null;
		GetEmployeesByKeywordRequest getEmployeesByKeywordRequest = WS_CLIENT_FACTORY.createGetEmployeesByKeywordRequest();
		EmployeeDirectorySearchVO employeeDataVO = null;
		EMPByKeyword empByKeyword = new EMPByKeyword();
		
		empByKeyword.setStrKeyword(keyword);
		
		getEmployeesByKeywordRequest.setEMPByKeyword(empByKeyword);
		
		try {
			WebUtil.trust();
			Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");

			//System.out.println("Current Date: " + ft.format(dNow));
			LoggerUtil.infoLogger(logger, ft.format(dNow));
			Object responseObject = webServiceTemplate.marshalSendAndReceive(getEmployeesByKeywordRequest);
			Date dNow1 = new Date();
			SimpleDateFormat ft1 = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");

			LoggerUtil.infoLogger(logger, ft1.format(dNow1));
			if (responseObject instanceof GetEmployeesByKeywordResponse) {

				GetEmployeesByKeywordResponse getEmployeesByKeywordResponse = (GetEmployeesByKeywordResponse) responseObject;
				if (getEmployeesByKeywordResponse != null && getEmployeesByKeywordResponse.getEmployees() != null
						&& getEmployeesByKeywordResponse.getEmployees().getEmployee() != null
						&& getEmployeesByKeywordResponse.getEmployees().getEmployee().size() > 0) {

					employeeList = getEmployeesByKeywordResponse.getEmployees().getEmployee();
					if (employeeList != null && !employeeList.isEmpty()
							&& employeeList.size() > 0) {
						for (com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees.Employee employee : employeeList) {

							employeeDataVO = new EmployeeDirectorySearchVO();
							employeeDataVO.setEmpName(employee.getFIRSTNAME()+ Constants.SPACE + employee.getLASTNAME());
							employeeDataVO.setEmpTitle(employee.getTITLE());
							employeeDataVO.setEmpDepartment(employee.getDEPARTMENT());
							employeeDataVO.setOffice(employee.getOFFICENAME());
							employeeDataVO.setEmpGeography(employee.getGEOGRAPHY());
							employeeDataVO.setEmpPhone(employee.getPHONE());
							employeeDataVO.setEmailAddress(employee.getEMAIL());
							employeeDataVO.setEmpId(employee.getUSERID());
							list.add(employeeDataVO);
						}
					}
				}
			} else if (responseObject instanceof GetEmployeesByKeywordFault) {

				GetEmployeesByKeywordFault getEmployeesByKeywordFault = (GetEmployeesByKeywordFault) responseObject;
				model.addAttribute(Constants.FAULT_MESSAGE,Constants.NO_RESULT_FOUND);
				LoggerUtil.debugLogger(logger,getEmployeesByKeywordFault.getErrDesc());
			}
		} catch (SoapFaultClientException sfce) {

			LoggerUtil.errorLogger(logger, sfce.getMessage(), sfce);
			model.addAttribute(Constants.FAULT_MESSAGE,
					Constants.ERR_WEBSERVICE_PROCESSING);
		} catch (Exception ex) {

			LoggerUtil.errorLogger(logger, ex.getMessage(), ex);
			model.addAttribute(Constants.FAULT_MESSAGE,
					Constants.ERR_WEBSERVICE_PROCESSING);
		}
		LoggerUtil
				.debugLogger(logger,
						"End of getEmployeeByEmployeeDetails() method of EmpooyeeDirectoryController::");
		return list;
		
		
	}

	private List<EmployeeDirectorySearchVO> getEmployeesByNameRequest(String firstName, String lastName,Model model) {
		
		LoggerUtil.debugLogger(logger,"employeeDirectorySearch:: Start of getEmployeesByNameRequest() method of EmpooyeeDirectoryController::");
		
		List<EmployeeDirectorySearchVO> list = new ArrayList<EmployeeDirectorySearchVO>();
		List<com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees.Employee> employeeList = null;
		GetEmployeesByNameRequest getEmployeesByNameRequest = WS_CLIENT_FACTORY.createGetEmployeesByNameRequest();
		EmployeeDirectorySearchVO employeeDataVO = null;
		EMPByName empByName = new EMPByName();
		
		empByName.setStrFirstName(firstName);
		empByName.setStrLastName(lastName);
		
		getEmployeesByNameRequest.setEMPByName(empByName);
		
		try {
			WebUtil.trust();
			Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");

			//System.out.println("Current Date: " + ft.format(dNow));
			LoggerUtil.infoLogger(logger, ft.format(dNow));
			Object responseObject = webServiceTemplate.marshalSendAndReceive(getEmployeesByNameRequest);
			Date dNow1 = new Date();
			SimpleDateFormat ft1 = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss a zzz");

			LoggerUtil.infoLogger(logger, ft1.format(dNow1));
			if (responseObject instanceof GetEmployeesByNameResponse) {

				GetEmployeesByNameResponse getEmployeesByNameResponse = (GetEmployeesByNameResponse) responseObject;
				if (getEmployeesByNameResponse != null && getEmployeesByNameResponse.getEmployees() != null
						&& getEmployeesByNameResponse.getEmployees().getEmployee() != null
						&& getEmployeesByNameResponse.getEmployees().getEmployee().size() > 0) {

					employeeList = getEmployeesByNameResponse.getEmployees().getEmployee();
					if (employeeList != null && !employeeList.isEmpty()
							&& employeeList.size() > 0) {
						for (com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees.Employee employee : employeeList) {

							employeeDataVO = new EmployeeDirectorySearchVO();
							employeeDataVO.setEmpName(employee.getFIRSTNAME()+ Constants.SPACE + employee.getLASTNAME());
							employeeDataVO.setEmpTitle(employee.getTITLE());
							employeeDataVO.setEmpDepartment(employee.getDEPARTMENT());
							employeeDataVO.setOffice(employee.getOFFICENAME());
							employeeDataVO.setEmpGeography(employee.getGEOGRAPHY());
							employeeDataVO.setEmpPhone(employee.getPHONE());
							employeeDataVO.setEmailAddress(employee.getEMAIL());
							employeeDataVO.setEmpId(employee.getUSERID());
							list.add(employeeDataVO);
						}
					}
				}
			} else if (responseObject instanceof GetEmployeesByNameFault) {

				GetEmployeesByNameFault getEmployeesByNameFault = (GetEmployeesByNameFault) responseObject;
				model.addAttribute(Constants.FAULT_MESSAGE,Constants.NO_RESULT_FOUND);
				LoggerUtil.debugLogger(logger,getEmployeesByNameFault.getErrDesc());
			}
		} catch (SoapFaultClientException sfce) {

			LoggerUtil.errorLogger(logger, sfce.getMessage(), sfce);
			model.addAttribute(Constants.FAULT_MESSAGE,Constants.ERR_WEBSERVICE_PROCESSING);
		} catch (Exception ex) {

			LoggerUtil.errorLogger(logger, ex.getMessage(), ex);
			model.addAttribute(Constants.FAULT_MESSAGE,
					Constants.ERR_WEBSERVICE_PROCESSING);
		}
		LoggerUtil
				.debugLogger(logger,
						"End of getEmployeeByEmployeeDetails() method of EmpooyeeDirectoryController::");
		return list;
		
	}

	/**
	 * Get Employee details based on EmployeeId.
	 * 
	 * @param employeeID
	 * @param model
	 * @return EmployeeProfileVO object.
	 */
	public EmployeeProfileVO getEmployeeByID(String employeeID, Model model) {

		LoggerUtil
				.debugLogger(logger,
						"Start of getEmployeeByID() method of EmpooyeeDirectoryController::");
		GetEmployeesByIDRequest employeesByIDRequest = WS_CLIENT_FACTORY
				.createGetEmployeesByIDRequest();
		EMPByUserId userId = new EMPByUserId();
		DirectReportVO directReportVO = null;
		List<DirectReportVO> directReportVoList = new ArrayList<DirectReportVO>();
		EmployeeProfileVO employeeProfileVO = null;
		List<com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee> employeeDetailsList = null;
		userId.setStrADUserID(employeeID);
		employeesByIDRequest.setEMPByUserId(userId);
		try {
			WebUtil.trust();
			Date dNow = new Date();
			SimpleDateFormat ft = new SimpleDateFormat(
					"E yyyy.MM.dd 'at' hh:mm:ss a zzz");

			//System.out.println("Current Date: " + ft.format(dNow));
			LoggerUtil.infoLogger(logger, ft.format(dNow));
			Object responseObject = webServiceTemplate
					.marshalSendAndReceive(employeesByIDRequest);
			Date dNow1 = new Date();
			SimpleDateFormat ft1 = new SimpleDateFormat(
					"E yyyy.MM.dd 'at' hh:mm:ss a zzz");

			//System.out.println("Current Date: " + ft1.format(dNow1));
			LoggerUtil.infoLogger(logger, ft1.format(dNow1));

			if (responseObject instanceof GetEmployeesByIDResponse) {

				GetEmployeesByIDResponse employeeByIdResponse = (GetEmployeesByIDResponse) responseObject;
				if (employeeByIdResponse != null
						&& employeeByIdResponse.getEmployees() != null
						&& employeeByIdResponse.getEmployees().getEmployee() != null) {

					employeeDetailsList = employeeByIdResponse.getEmployees()
							.getEmployee();
					if (employeeDetailsList != null
							&& !employeeDetailsList.isEmpty()
							&& employeeDetailsList.size() > 0) {
						for (com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee employee : employeeDetailsList) {

							if (employeeID != null
									&& !employeeID.isEmpty()
									&& employee.getUSERID() != null
									&& !employee.getUSERID().isEmpty()
									&& employeeID.equalsIgnoreCase(employee
											.getUSERID())) {

								employeeProfileVO = new EmployeeProfileVO();
								employeeProfileVO.setEmpName(employee
										.getFIRSTNAME()
										+ Constants.SPACE
										+ employee.getLASTNAME());
								employeeProfileVO.setDesignation(employee
										.getTITLE());
								employeeProfileVO.setEmpPhone(employee
										.getPHONE());
								employeeProfileVO.setFax(employee.getFAX());
								employeeProfileVO.setEmailAddress(employee
										.getEMAIL());
								employeeProfileVO.setOffice(employee
										.getOFFICENAME());
								employeeProfileVO.setBuildingName(employee
										.getBUILDINGNAME());
								employeeProfileVO.setAddress1(employee
										.getADDRESS());
								employeeProfileVO.setAddress2(employee
										.getADDRESS2());
								employeeProfileVO.setAddress3(employee
										.getADDRESS3());
								employeeProfileVO.setFloor(employee.getFLOOR());
								employeeProfileVO.setCity(employee.getCITY());
								employeeProfileVO.setState(employee.getSTATE());
								employeeProfileVO.setCountry(employee
										.getCOUNTRY());
								employeeProfileVO.setZip(employee.getZIP());
								employeeProfileVO.setEmpDepartment(employee
										.getDEPARTMENT());
								employeeProfileVO.setEmpGeography(employee
										.getGEOGRAPHY());
								if (employee.getMANAGER() != null) {

									employeeProfileVO
											.setManagerFirstName(employee
													.getMANAGER()
													.getFIRSTNAME());
									employeeProfileVO
											.setManagerLastname(employee
													.getMANAGER().getLASTNAME());
									employeeProfileVO.setManagerId(employee
											.getMANAGER().getUSERID());
								}
								if (employee.getASSISTANT() != null) {

									employeeProfileVO
											.setAssistantFirstName(employee
													.getASSISTANT()
													.getFIRSTNAME());
									employeeProfileVO
											.setAssistantLastName(employee
													.getASSISTANT()
													.getLASTNAME());
									employeeProfileVO.setAssistantId(employee
											.getASSISTANT().getUSERID());
								}

								if (employee.getDIRECTREPORTS() != null
										&& employee.getDIRECTREPORTS()
												.getDIRECTREPORT() != null) {

									List<com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT> directReportsList = employee
											.getDIRECTREPORTS()
											.getDIRECTREPORT();

									for (com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT directReport : directReportsList) {
										directReportVO = new DirectReportVO();
										directReportVO
												.setDirectReportFirstName(directReport
														.getFIRSTNAME());
										directReportVO
												.setDirectReportLastName(directReport
														.getLASTNAME());
										directReportVO
												.setDirectReportId(directReport
														.getUSERID());
										directReportVoList.add(directReportVO);
									}
									employeeProfileVO
											.setDirectReports(directReportVoList);
								}

							}
						}
					}
				}
			} else if (responseObject instanceof GetEmployeesByIDFault) {

				GetEmployeesByIDFault employeeFault = (GetEmployeesByIDFault) responseObject;
				model.addAttribute(Constants.FAULT_MESSAGE,
						Constants.ERR_WEBSERVICE_PROCESSING);
				LoggerUtil.debugLogger(logger, employeeFault.getErrCode());
			}
		} catch (SoapFaultClientException sfce) {

			LoggerUtil.errorLogger(logger, sfce.getMessage(), sfce);
			model.addAttribute(Constants.FAULT_MESSAGE,
					Constants.ERR_WEBSERVICE_PROCESSING);
		} catch (Exception ex) {

			LoggerUtil.errorLogger(logger, ex.getMessage(), ex);
			model.addAttribute(Constants.FAULT_MESSAGE,
					Constants.ERR_WEBSERVICE_PROCESSING);
		}
		LoggerUtil
				.debugLogger(logger,
						"End of getEmployeeByID() method of EmpooyeeDirectoryController::");
		return employeeProfileVO;
	}

}
