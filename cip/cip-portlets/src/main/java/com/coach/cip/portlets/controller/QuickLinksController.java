
package com.coach.cip.portlets.controller;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.coach.cip.common.dto.QuickLinksVO;
import com.coach.cip.common.dto.UserVO;
import com.coach.cip.model.entity.CIQuickLink;
import com.coach.cip.model.entity.Layout;
import com.coach.cip.model.entity.User;
import com.coach.cip.services.QuickLinksService;
import com.coach.cip.util.CoachComparator;
import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.coach.cip.util.WebUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;

/**
 * The Class QuickLinksController, serves the request related to the Quick Links
 * operations like Adding, Updating, Deleting and populating Quick Links form
 * data.
 * @author GalaxE.
 */

@Controller("quickLinksController")
@Scope(value="globalSession", proxyMode=ScopedProxyMode.TARGET_CLASS)
@RequestMapping("VIEW")
public class QuickLinksController  extends AbstractBaseController implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2027592535075518206L;

	/** The Constant LOG. */
	private static final Log logger = LogFactoryUtil.getLog(QuickLinksController.class);

	/**
	 * List to hold the User Quick Links.
	 */
	private List<QuickLinksVO> quickLinksVOList = new ArrayList<QuickLinksVO>();
	
	
	/**
	 * List to hold the User Edit Quick Links.
	 */
	private List<QuickLinksVO> editQuickLinksVOList = new ArrayList<QuickLinksVO>();
	
	private ResourceBundle resourceBundle = ResourceBundle.getBundle("/content/Language");

	/**
	 * Holds the UserId for the previous request.
	 */
	private long previousUser;

	/**
	 * Holds the UserId for the current request.
	 */
	private long currentUser;

	/**
	 * Default Render Mapping Method. If Specific Render mapping is not found,
	 * this mapping is invoked.
	 * @param renderRequest
	 * @param renderResponse
	 * @return view name
	 */
	@RenderMapping
	public String showQuickLinks(RenderRequest renderRequest, RenderResponse renderResponse) {

		LoggerUtil.debugLogger(logger, "Inside default render mapping showQuickLinks() method of Quick Links Controller::");
		renderRequest.setAttribute("editOpenPopup", "false");
		return Constants.QUICKLINKS_JSP;
	}
	
	/**
	 * Render Mapping Method for Edit Quick Link Form. This Render Mapping is
	 * invoked in Edit Quick Links phase on click of update or delete buttons.
	 * @param renderRequest
	 * @return view name
	 */
	@RenderMapping(params = "myaction=editQuickLinksForm")
	public String showEditQuickLinks(RenderRequest renderRequest) {

		LoggerUtil.debugLogger(logger, "Inside edit render mapping showEditQuickLinks() method of Quick Links Controller::");
		renderRequest.setAttribute("editOpenPopup", "true");
		return Constants.QUICKLINKS_JSP;
	}
	
	/**
	 * Model Object for populating Quick Links form data.
	 * @param portletRequest
	 * @return List<QuickLinksVO>
	 */
	@ModelAttribute(value = "quickLinksVOList")
	public List<QuickLinksVO> getQuickLinks(PortletRequest portletRequest) {

	    LoggerUtil.debugLogger(logger, "Start of Model getQuickLinks() method of Quick Links Controller::");
		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		currentUser = themeDisplay.getUserId();
		if (previousUser != 0 && currentUser != previousUser) {
			quickLinksVOList = new ArrayList<QuickLinksVO>();
		}
		if (quickLinksVOList.size() == 0) {
			List<QuickLinksVO> internalQuickLinksList = null;
            /* Predefined Links : Start */
			try {
				String externalQuickLinks = resourceBundle.getString("external-quicklinks");
				String[] externalQLElement = externalQuickLinks.split(";");
				for(String externalQL : externalQLElement){
					QuickLinksVO quickLinksVO = new QuickLinksVO();
					String[] extQLArray = externalQL.split(",");
					quickLinksVO.setPageName(extQLArray[0]);
					quickLinksVO.setPageURL(extQLArray[1]);
					quickLinksVO.setAssetURL(extQLArray[1]);
					quickLinksVOList.add(quickLinksVO);
				}
				
				String internalQuickLinks = resourceBundle.getString("internal-quicklinks");
				String[] internalQLElement = internalQuickLinks.split(";");
				populatePredefinedInternalQL(internalQLElement, themeDisplay);

				CoachComparator.sort(quickLinksVOList, "getPageName", true,false);
			}
			catch (Exception e) {
				LoggerUtil.errorLogger(logger, "QuickLinksController.getExternalQuickLinks():::", e);
			}
			/* Predefined Links : End */
			/* User defined Links : Start */
			try {
				List<CIQuickLink> ciQuickLink = serviceLocator.getQuickLinksService().getQuickLinks(themeDisplay.getUserId());
				
				internalQuickLinksList = convertQuickLinksDOToVO(ciQuickLink, themeDisplay);
				if(Validator.isNull(internalQuickLinksList)){
					internalQuickLinksList = new ArrayList<QuickLinksVO>();
				}
				CoachComparator.sort(internalQuickLinksList, "getPageName", true, false);

				Collections.addAll(quickLinksVOList, internalQuickLinksList.toArray(new QuickLinksVO[internalQuickLinksList.size()]) );
			}
			catch (Exception e) {
				LoggerUtil.errorLogger(logger, "QuickLinksController.getQuickLinks():::", e);
			}
			/* User defined Links : End */
		}
		previousUser = currentUser;
	    LoggerUtil.debugLogger(logger, "End of Model getQuickLinks() method of Quick Links Controller::");
		
		return quickLinksVOList;
	}

	/**
	 * Model Object for populating Edit Quick Links form data.
	 * @param portletRequest
	 * @return List<QuickLinksVO>
	 */
	@ModelAttribute(value = "editQuickLinksVOList")
	public List<QuickLinksVO> editQuickLinksModel(PortletRequest portletRequest) {

		LoggerUtil.debugLogger(logger, "Start of Model editQuickLinksModel() method of Quick Links Controller::");
		ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
		editQuickLinksVOList = new ArrayList<QuickLinksVO>();
		for (QuickLinksVO quickLinksVO : quickLinksVOList) {
			if (quickLinksVO.getQuicklinkId() != 0 && quickLinksVO.getUser().getUserId().equals(themeDisplay.getUserId())) {
				QuickLinksVO editQuickLinksVO = new QuickLinksVO();
				String pgUrl = quickLinksVO.getPageURL();
				editQuickLinksVO.setQuicklinkId(quickLinksVO.getQuicklinkId());
				editQuickLinksVO.setPageName(quickLinksVO.getPageName());
				editQuickLinksVO.setPageURL(pgUrl.substring(pgUrl.lastIndexOf("/"), pgUrl.length()));
				editQuickLinksVO.setUser(quickLinksVO.getUser());
				editQuickLinksVO.setAssetURL(quickLinksVO.getAssetURL());
				editQuickLinksVOList.add(editQuickLinksVO);
			}
		}
		CoachComparator.sort(editQuickLinksVOList, "getPageName", true,false);
		portletRequest.setAttribute("editQuickLinksVOList", editQuickLinksVOList);
		LoggerUtil.debugLogger(logger, "End of Model editQuickLinksModel() method of Quick Links Controller::");
		return editQuickLinksVOList;
	}

	/**
	 * This method is used for updating the reference data or model data list
	 * with the updated values (while adding , editing or deleting Quick Links).
	 * @param actionRequest
	 * @param model
	 * @param themeDisplay
	 */
	public void updateQuickLinksModel(PortletRequest actionRequest, Model model, ThemeDisplay themeDisplay) {

		LoggerUtil.debugLogger(logger, "Start of Model updateQuickLinksModel() method of Quick Links Controller::");
		quickLinksVOList = new ArrayList<QuickLinksVO>();
		model.addAttribute("quickLinksVOList", getQuickLinks(actionRequest));
		model.addAttribute("editQuickLinksVOList", editQuickLinksModel(actionRequest));
		LoggerUtil.debugLogger(logger, "End of Model updateQuickLinksModel() method of Quick Links Controller::");
	}

	/**
	 * Action Mapping Method for Add Quick Link and redirecting to the
	 * respective Quick Links.
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 */
	@ActionMapping(params = "myaction=quickLinks")
	public void addQuickLinks(ActionRequest actionRequest, ActionResponse actionResponse, Model model) {

		LoggerUtil.debugLogger(logger, "Start of addQuickLinks() method of Quick Links Controller::");
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		QuickLinksService quickLinksService = serviceLocator.getQuickLinksService();
		if (actionRequest.getParameter("selcetedQuickLink").equals("addQuickLinks") && quickLinksVOList != null) {

			String pageName = actionRequest.getParameter("pageName")!=null ? actionRequest.getParameter("pageName").trim() : null;
			String assetURL = actionRequest.getParameter("selectedAssetURL")!=null ? actionRequest.getParameter("selectedAssetURL").trim() : null;
			if(Validator.isNotNull(assetURL)){
				if(assetURL.indexOf("_fileURL=") == -1 && assetURL.indexOf("?") >=0)
				 {
						 assetURL = assetURL.substring(0, assetURL.indexOf("?"));
				 }	 
				if (pageName == null || pageName.equals("")) {
					pageName = themeDisplay.getLayout().getName(themeDisplay.getLocale()).trim();
				}
				actionRequest.setAttribute("pageName", pageName);
				Layout layout = quickLinksService.getLayout(themeDisplay.getLayout().getPlid());
				User user = quickLinksService.getUser(themeDisplay.getUserId());
			     
					try {
						CIQuickLink ciQuickLink = new CIQuickLink();
						ciQuickLink.setLayout(layout);
						ciQuickLink.setPageName(pageName);
						ciQuickLink.setUser(user);
						ciQuickLink.setCreateDate(new Timestamp(new Date().getTime()));
						ciQuickLink.setAssetURL(assetURL);
						quickLinksService.addQuickLinks(ciQuickLink);
						SessionMessages.add(actionRequest, Constants.QUICKLINKS_SUCCESS_SAVE);
						model.addAttribute("message", Constants.QUICKLINKS_SUCCESS_SAVE);
						model.addAttribute("disableAdd", "true");
					}
					catch (Exception e) {
						LoggerUtil.errorLogger(logger, "QuickLinksController.addQuickLinks()", e);
						SessionErrors.add(actionRequest, Constants.QUICKLINKS_ERROR_SAVE);
						model.addAttribute("message", Constants.QUICKLINKS_ERROR_SAVE);
					}
						actionRequest.setAttribute("addOpenPopup", "true");
						actionRequest.setAttribute("addedAssetURL", assetURL);
						updateQuickLinksModel(actionRequest, model, themeDisplay);
			}
			
		}
		
		LoggerUtil.debugLogger(logger, "End of addQuickLinks() method of Quick Links Controller::");
	}

	/**
	 * Action Mapping Method for Updating & Deleting Quick Link and redirecting
	 * to the respective Quick Links.
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 */
	@ActionMapping(params = "myaction=editQuickLinks")
	public void editQuickLinks(ActionRequest actionRequest, ActionResponse actionResponse, Model model) {

		LoggerUtil.debugLogger(logger, "Start of editQuickLinks() method of Quick Links Controller::");
		if (actionRequest.getParameter("quiickLinksOperation") != null && !actionRequest.getParameter("quiickLinksOperation").equals("Close")) {
			actionRequest.setAttribute("pagination", actionRequest.getParameter("page"));
			String quickLinkIds[] = actionRequest.getParameterValues("quickLinkIds");
			if (quickLinkIds != null) {

				ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
				boolean deleteDuplicates = false;
				List<CIQuickLink> ciQuickLinksList = new ArrayList<CIQuickLink>();
				QuickLinksService quickLinksService = serviceLocator.getQuickLinksService();
				String quiickLinksOperation = actionRequest.getParameter("quiickLinksOperation");
				List<String> pageNames = Arrays.asList(actionRequest.getParameterValues("pgName"));
				String qlId = actionRequest.getParameter("qlPrimaryKeys");
				List<String> allQuickLinkIds = new ArrayList<String>();
				if(Validator.isNotNull(qlId)){
					allQuickLinkIds = Arrays.asList(qlId.split(","));
				}

				/*	for(int i=0 ;i<quickLinkIds.length ; i++){*/
				for (String selectedQLID : quickLinkIds) {
					int index = allQuickLinkIds.indexOf(selectedQLID);
					QuickLinksVO qlVO = searchList(selectedQLID);
					if(Validator.isNotNull(qlVO)){
					CIQuickLink ciQuickLink = new CIQuickLink();
					ciQuickLink.setQuicklinkId(qlVO.getQuicklinkId());
					ciQuickLink.setPageName(pageNames.get(index).trim());
					ciQuickLink.setCreateDate(new Timestamp(new Date().getTime()));
					ciQuickLinksList.add(ciQuickLink);
					}else{
						deleteDuplicates = true;
						SessionErrors.add(actionRequest, Constants.QUICKLINKS_ERROR_DELETE_DUPLICATE);
						model.addAttribute("message", Constants.QUICKLINKS_ERROR_DELETE_DUPLICATE);
					}
				}
				if (quiickLinksOperation.equals("Update") && ciQuickLinksList.size()>0 /*&& !duplicatePageName*/ ) {
					LoggerUtil.debugLogger(logger, "Updating Quick Links ::");
					try {
						quickLinksService.updateQuickLinks(ciQuickLinksList);
						if(deleteDuplicates){
							SessionErrors.add(actionRequest, Constants.QUICKLINKS_ERROR_UPDATE_DELETE_DUPLICATE);
							model.addAttribute("message", Constants.QUICKLINKS_ERROR_UPDATE_DELETE_DUPLICATE);
						}else{
							SessionMessages.add(actionRequest, Constants.QUICKLINKS_SUCCESS_EDIT_UPDATE);
							model.addAttribute("message", Constants.QUICKLINKS_SUCCESS_EDIT_UPDATE);
						}
						updateQuickLinksModel(actionRequest, model, themeDisplay);
					}
					catch (Exception e) {
						LoggerUtil.errorLogger(logger, "QuickLinksController.editQuickLinks() - Update ::", e);
						SessionErrors.add(actionRequest, Constants.QUICKLINKS_ERROR_EDIT_UPDATE);
						model.addAttribute("message", Constants.QUICKLINKS_ERROR_EDIT_UPDATE);
					}
				}
				else if (quiickLinksOperation.equals("Delete") && ciQuickLinksList.size()>0) {
					LoggerUtil.debugLogger(logger, "Deleting Quick Links ::");
					try {
						quickLinksService.deleteQuickLinks(ciQuickLinksList);
						if(deleteDuplicates){
							SessionErrors.add(actionRequest, Constants.QUICKLINKS_ERROR_UPDATE_DELETE_DUPLICATE);
							model.addAttribute("message", Constants.QUICKLINKS_ERROR_UPDATE_DELETE_DUPLICATE);
						}else{
							SessionMessages.add(actionRequest, Constants.QUICKLINKS_SUCCESS_EDIT_DELETE);
							model.addAttribute("message", Constants.QUICKLINKS_SUCCESS_EDIT_DELETE);
						}
						updateQuickLinksModel(actionRequest, model, themeDisplay);
					}
					catch (Exception e) {
						LoggerUtil.errorLogger(logger, "QuickLinksController.editQuickLinks() - Delete ::", e);
						SessionErrors.add(actionRequest, Constants.QUICKLINKS_ERROR_EDIT_DELETE);
						model.addAttribute("message", Constants.QUICKLINKS_ERROR_EDIT_DELETE);
					}
				}
				LoggerUtil.debugLogger(logger, "End of editQuickLinks() method of Quick Links Controller::");
				actionResponse.setRenderParameter("myaction", "editQuickLinksForm");
			}
		}
	}

	/**
	 * Helper method for converting Domain Object to Value Object for displaying
	 * the view layer form data.
	 * @param ciQuickLink
	 * @return List<QuickLinksVO>
	 */
	public List<QuickLinksVO> convertQuickLinksDOToVO(List<CIQuickLink> ciQuickLink, ThemeDisplay themeDisplay) {
		List<QuickLinksVO> userDefinedIntQLList = new ArrayList<QuickLinksVO>(); 
		for (CIQuickLink quickLinks : ciQuickLink) {
			QuickLinksVO quickLinksVO = new QuickLinksVO();
			StringBuffer url = new StringBuffer();
			url.append(themeDisplay.getPortalURL()
			// + themeDisplay.getPathFriendlyURLPublic()
				+
				themeDisplay.getPathFriendlyURLPrivateGroup() +
				themeDisplay.getParentGroup().getFriendlyURL() +
				quickLinks.getLayout().getFriendlyUrl());
			quickLinksVO.setQuicklinkId(quickLinks.getQuicklinkId());
			quickLinksVO.setPageName(quickLinks.getPageName());
			quickLinksVO.setPageURL(url.toString());
			quickLinksVO.setUser(convertUserDOToVO(quickLinks.getUser()));
			quickLinksVO.setAssetURL(quickLinks.getAssetURL());
			userDefinedIntQLList.add(quickLinksVO);
		}
		return userDefinedIntQLList;
	}

	/**
	 * Helper method to return Quick Links Object from the model attribute.
	 * @param quickLinksId
	 * @return
	 */
	public QuickLinksVO searchList(String quickLinksId) {

		QuickLinksVO quickLinksVO = null;
		for (QuickLinksVO qlVO : editQuickLinksVOList) {
			if (qlVO.getQuicklinkId() == Long.valueOf(quickLinksId))
			{	
				quickLinksVO = qlVO;
			    break;
			}
		}
		return quickLinksVO;

	}

	/**
	 * Helper method for converting User Domain Object to Value Object.
	 * @param user
	 * @return UserVO
	 */
	private UserVO convertUserDOToVO(User user) {

		UserVO userVO = new UserVO();

		userVO.setUserId(user.getUserId());
		userVO.setUuid(user.getUuid());
		userVO.setCompanyId(user.getCompanyId());
		userVO.setCreateDate(user.getCreateDate());
		userVO.setModifiedDate(user.getModifiedDate());
		userVO.setDefaultUser(user.getDefaultUser());
		userVO.setContactId(user.getContactId());
		userVO.setPassword(user.getPassword());
		userVO.setPasswordEncrypted(user.getPasswordEncrypted());
		userVO.setPasswordReset(user.getPasswordReset());
		userVO.setPasswordModifiedDate(user.getPasswordModifiedDate());
		userVO.setDigest(user.getDigest());
		userVO.setReminderQueryQuestion(user.getReminderQueryQuestion());
		userVO.setReminderQueryAnswer(user.getReminderQueryAnswer());
		userVO.setGraceLoginCount(user.getGraceLoginCount());
		userVO.setScreenName(user.getScreenName());
		userVO.setEmailAddress(user.getEmailAddress());
		userVO.setFacebookId(user.getFacebookId());
		userVO.setOpenId(user.getOpenId());
		userVO.setPortraitId(user.getPortraitId());
		userVO.setLanguageId(user.getLanguageId());
		userVO.setTimeZoneId(user.getTimeZoneId());
		userVO.setGreeting(user.getGreeting());
		userVO.setComments(user.getComments());
		userVO.setFirstName(user.getFirstName());
		userVO.setMiddleName(user.getMiddleName());
		userVO.setLastName(user.getLastName());
		userVO.setJobTitle(user.getJobTitle());
		userVO.setLoginDate(user.getLoginDate());
		userVO.setLoginIp(user.getLoginIp());
		userVO.setLastLoginDate(user.getLastLoginDate());
		userVO.setLastLoginIp(user.getLastLoginIp());
		userVO.setLastFailedLoginDate(user.getLastFailedLoginDate());
		userVO.setFailedLoginAttempts(user.getFailedLoginAttempts());
		userVO.setLockout(user.getLockout());
		userVO.setLockoutDate(user.getLockoutDate());
		userVO.setAgreedToTermsOfUse(user.getAgreedToTermsOfUse());
		userVO.setEmailAddressVerified(user.getEmailAddressVerified());
		userVO.setStatus(user.getStatus());
		userVO.setAdminAssistSet(user.getCIEmployeesForAdminAssistId());
		userVO.setUserMetricsSet(user.getCIUserMetrics());
		userVO.setQuickLinksSet(user.getCIQuickLinks());
		userVO.setPersonalMessagesCreatorSet(user.getCIPersonalMessagesForCreatedBy());
		userVO.setPersonalMessagesModifierSet(user.getCIPersonalMessagesForModifiedBy());
		userVO.setSupervisorSet(user.getCIEmployeesForSupervisorId());
		userVO.setStoreEmployeesSet(user.getCIStoreEmployees());
		userVO.setUserSet(user.getCIEmployeesForUserId());

		return userVO;
	}
	
	/**
	 * Helper method to populate quickLinksVOList with predefined Internal Links.
	 * @param internalQLElement
	 * @param themeDisplay
	 */
	private void populatePredefinedInternalQL(String[] internalQLElement, ThemeDisplay themeDisplay){
		
		for(String internalQL : internalQLElement){
			QuickLinksVO quickLinksVO = new QuickLinksVO();
			String[] intQLArray = internalQL.split(",");
			quickLinksVO.setPageName(intQLArray[0]);
			quickLinksVO.setPageURL(themeDisplay.getPortalURL()+intQLArray[1]);
			quickLinksVO.setAssetURL(intQLArray[1]);
			quickLinksVOList.add(quickLinksVO);
		}
	}
	
	@ResourceMapping("populateAddQuickLinkForm")
	public ModelAndView populateAddQuickLinkForm(ResourceRequest resourceRequest, ResourceResponse resourceResponse, Model model) throws IOException, SystemException 
	{
		LoggerUtil.debugLogger(logger, "Start of addQuickLinks() method of Quick Links Controller::");
		resourceResponse.setContentType("text/html; charset=utf-8");
		ThemeDisplay themeDisplay = WebUtil.getThemeDisplay( resourceRequest);
		QuickLinksService quickLinksService = serviceLocator.getQuickLinksService();
		Map<String, String> modelMap = new HashMap<String, String>();
		String pageName = resourceRequest.getParameter("pageName")!=null ? resourceRequest.getParameter("pageName").trim() : null;
		String assetURL = resourceRequest.getParameter("selectedAssetURL")!=null ? resourceRequest.getParameter("selectedAssetURL").trim() : null;
		if(Validator.isNotNull(assetURL)){
			if(assetURL.indexOf("_fileURL=") == -1 && assetURL.indexOf("?") >=0)
			 {
					 assetURL = assetURL.substring(0, assetURL.indexOf("?"));
			 }	 
			if (pageName == null || pageName.equals("")) {
				pageName = themeDisplay.getLayout().getName(themeDisplay.getLocale()).trim();
			}
			resourceRequest.setAttribute("pageName", pageName);
			Layout layout = quickLinksService.getLayout(themeDisplay.getLayout().getPlid());
			User user = quickLinksService.getUser(themeDisplay.getUserId());
			try {
				CIQuickLink ciQuickLink = new CIQuickLink();
				ciQuickLink.setLayout(layout);
				ciQuickLink.setPageName(pageName);
				ciQuickLink.setUser(user);
				ciQuickLink.setCreateDate(new Timestamp(new Date().getTime()));
				ciQuickLink.setAssetURL(assetURL);
				quickLinksService.addQuickLinks(ciQuickLink);
				SessionMessages.add(resourceRequest, Constants.QUICKLINKS_SUCCESS_SAVE);
				model.addAttribute("message", Constants.QUICKLINKS_SUCCESS_SAVE);
				model.addAttribute("disableAdd", "true");
			}
			catch (Exception e) {
				LoggerUtil.errorLogger(logger, "QuickLinksController.addQuickLinks()", e);
				SessionErrors.add(resourceRequest, Constants.QUICKLINKS_ERROR_SAVE);
				model.addAttribute("message", Constants.QUICKLINKS_ERROR_SAVE);
			}
				resourceRequest.setAttribute("addOpenPopup", "true");
				resourceRequest.setAttribute("addedAssetURL", assetURL);
	  }
			
		LoggerUtil.debugLogger(logger, "End of addQuickLinks() method of Quick Links Controller::");
		return new ModelAndView("populateAddQuickLinks", modelMap);
	}
	
	
	
	@ResourceMapping("refreshQuickLinks")
	public ModelAndView refreshQuickLinks(ResourceRequest resourceRequest, ResourceResponse resourceResponse, Model model) throws IOException, SystemException 
	{
		resourceResponse.setContentType("text/html; charset=utf-8");
		Map<String, String> modelMap = new HashMap<String, String>();
		ThemeDisplay themeDisplay = WebUtil.getThemeDisplay( resourceRequest);
		updateQuickLinksModel(resourceRequest, model, themeDisplay);
		return new ModelAndView("refreshQuickLinks", modelMap);
	}
	
	
	@ResourceMapping("populateEditQuickLinkForm")
	public ModelAndView populateEditQuickLinkForm(ResourceRequest actionRequest, ResourceResponse actionResponse, Model model) {
		
		LoggerUtil.debugLogger(logger, "Start of editQuickLinks() method of Quick Links Controller::");
		actionResponse.setContentType("text/html; charset=utf-8");
		Map<String, String> modelMap = new HashMap<String, String>();
		if (actionRequest.getParameter("quiickLinksOperation") != null && !actionRequest.getParameter("quiickLinksOperation").equals("Close")) {
			actionRequest.setAttribute("pagination", actionRequest.getParameter("page"));
			String quickLinkIds[] = actionRequest.getParameterValues("quickLinkIds");
			if (quickLinkIds != null) {

				ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
				boolean deleteDuplicates = false;
				List<CIQuickLink> ciQuickLinksList = new ArrayList<CIQuickLink>();
				QuickLinksService quickLinksService = serviceLocator.getQuickLinksService();
				String quiickLinksOperation = actionRequest.getParameter("quiickLinksOperation");
				List<String> pageNames = Arrays.asList(actionRequest.getParameterValues("pgName"));
				String qlId = actionRequest.getParameter("qlPrimaryKeys");
				List<String> allQuickLinkIds = new ArrayList<String>();
				if(Validator.isNotNull(qlId)){
					allQuickLinkIds = Arrays.asList(qlId.split(","));
				}
				/*	for(int i=0 ;i<quickLinkIds.length ; i++){*/
				for (String selectedQLID : quickLinkIds) {
					int index = allQuickLinkIds.indexOf(selectedQLID);
					QuickLinksVO qlVO = searchList(selectedQLID);
					if(Validator.isNotNull(qlVO)){
					CIQuickLink ciQuickLink = new CIQuickLink();
					ciQuickLink.setQuicklinkId(qlVO.getQuicklinkId());
					ciQuickLink.setPageName(pageNames.get(index).trim());
					ciQuickLink.setCreateDate(new Timestamp(new Date().getTime()));
					ciQuickLinksList.add(ciQuickLink);
					}else{
						deleteDuplicates = true;
						SessionErrors.add(actionRequest, Constants.QUICKLINKS_ERROR_DELETE_DUPLICATE);
						model.addAttribute("message", Constants.QUICKLINKS_ERROR_DELETE_DUPLICATE);
					}
				}
				if (quiickLinksOperation.equals("Update") && ciQuickLinksList.size()>0 /*&& !duplicatePageName*/ ) {
					LoggerUtil.debugLogger(logger, "Updating Quick Links ::");
					try {
						quickLinksService.updateQuickLinks(ciQuickLinksList);
						if(deleteDuplicates){
							SessionErrors.add(actionRequest, Constants.QUICKLINKS_ERROR_UPDATE_DELETE_DUPLICATE);
							model.addAttribute("message", Constants.QUICKLINKS_ERROR_UPDATE_DELETE_DUPLICATE);
						}else{
							SessionMessages.add(actionRequest, Constants.QUICKLINKS_SUCCESS_EDIT_UPDATE);
							model.addAttribute("message", Constants.QUICKLINKS_SUCCESS_EDIT_UPDATE);
						}
						updateQuickLinksModel(actionRequest, model, themeDisplay);
					}
					catch (Exception e) {
						LoggerUtil.errorLogger(logger, "QuickLinksController.editQuickLinks() - Update ::", e);
						SessionErrors.add(actionRequest, Constants.QUICKLINKS_ERROR_EDIT_UPDATE);
						model.addAttribute("message", Constants.QUICKLINKS_ERROR_EDIT_UPDATE);
					}
				}
				else if (quiickLinksOperation.equals("Delete") && ciQuickLinksList.size()>0) {
					LoggerUtil.debugLogger(logger, "Deleting Quick Links ::");
					try {
						quickLinksService.deleteQuickLinks(ciQuickLinksList);
						if(deleteDuplicates){
							SessionErrors.add(actionRequest, Constants.QUICKLINKS_ERROR_UPDATE_DELETE_DUPLICATE);
							model.addAttribute("message", Constants.QUICKLINKS_ERROR_UPDATE_DELETE_DUPLICATE);
						}else{
							SessionMessages.add(actionRequest, Constants.QUICKLINKS_SUCCESS_EDIT_DELETE);
							model.addAttribute("message", Constants.QUICKLINKS_SUCCESS_EDIT_DELETE);
						}
						updateQuickLinksModel(actionRequest, model, themeDisplay);
					}
					catch (Exception e) {
						LoggerUtil.errorLogger(logger, "QuickLinksController.editQuickLinks() - Delete ::", e);
						SessionErrors.add(actionRequest, Constants.QUICKLINKS_ERROR_EDIT_DELETE);
						model.addAttribute("message", Constants.QUICKLINKS_ERROR_EDIT_DELETE);
					}
				}
				LoggerUtil.debugLogger(logger, "End of editQuickLinks() method of Quick Links Controller::");
			}
		}
		return new ModelAndView("populateEditQuickLinks", modelMap);
		
	}
	
}
