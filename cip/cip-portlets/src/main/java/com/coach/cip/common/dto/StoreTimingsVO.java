package com.coach.cip.common.dto;

import java.io.Serializable;
import java.sql.Time;

/**
 * 
 * Class is used to capture the characteristics of StoreTimingsVO.
 * 
 * @author GalaxE.
 *
 */
public class StoreTimingsVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8055269558406477676L;
	// Fields    

    private Long storeTimingId;
    private StoresVO storesVO;
    private String weekday;
    private Time openingTime;
    private Time closingTime;


   // Constructors

   /** default constructor */
   public StoreTimingsVO() {
   }

	/** minimal constructor */
   public StoreTimingsVO(Long storeTimingId, StoresVO storesVO) {
       this.storeTimingId = storeTimingId;
       this.storesVO = storesVO;
   }
   
   /** full constructor */
   public StoreTimingsVO(Long storeTimingId, StoresVO storesVO, String weekday, Time openingTime, Time closingTime) {
       this.storeTimingId = storeTimingId;
       this.storesVO = storesVO;
       this.weekday = weekday;
       this.openingTime = openingTime;
       this.closingTime = closingTime;
   }

public Long getStoreTimingId() {
	return storeTimingId;
}

public void setStoreTimingId(Long storeTimingId) {
	this.storeTimingId = storeTimingId;
}

public StoresVO getStoresVO() {
	return storesVO;
}

public void setStoresVO(StoresVO storesVO) {
	this.storesVO = storesVO;
}

public String getWeekday() {
	return weekday;
}

public void setWeekday(String weekday) {
	this.weekday = weekday;
}

public Time getOpeningTime() {
	return openingTime;
}

public void setOpeningTime(Time openingTime) {
	this.openingTime = openingTime;
}

public Time getClosingTime() {
	return closingTime;
}

public void setClosingTime(Time closingTime) {
	this.closingTime = closingTime;
}
}
