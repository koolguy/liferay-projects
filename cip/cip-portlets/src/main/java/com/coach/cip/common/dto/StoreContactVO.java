package com.coach.cip.common.dto;

import java.io.Serializable;

/**
 * 
 * Class is used to capture the characteristics of StoreContactVO.
 * 
 * @author GalaxE.
 *
 */
public class StoreContactVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -61334437622824133L;
	// Fields

	private Long contactStoreId;
	private ContactVO contactVO;
	private StoresVO storesVO;

	// Constructors

	/** default constructor */
	public StoreContactVO() {
	}

	/** full constructor */
	public StoreContactVO(Long contactStoreId, ContactVO contactVO,
			StoresVO storesVO) {
		this.contactStoreId = contactStoreId;
		this.contactVO = contactVO;
		this.storesVO = storesVO;
	}

	public Long getContactStoreId() {
		return contactStoreId;
	}

	public void setContactStoreId(Long contactStoreId) {
		this.contactStoreId = contactStoreId;
	}

	public ContactVO getContactVO() {
		return contactVO;
	}

	public void setContactVO(ContactVO contactVO) {
		this.contactVO = contactVO;
	}

	public StoresVO getStoresVO() {
		return storesVO;
	}

	public void setStoresVO(StoresVO storesVO) {
		this.storesVO = storesVO;
	}

}
