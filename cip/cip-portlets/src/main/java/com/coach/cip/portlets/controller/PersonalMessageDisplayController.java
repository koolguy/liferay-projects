
package com.coach.cip.portlets.controller;

import java.io.IOException;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.xml.namespace.QName;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.coach.cip.common.dto.PersonalMessageVO;
import com.coach.cip.model.entity.CIPersonalMessage;
import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.coach.cip.util.WebUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Layout;
import com.liferay.portal.theme.ThemeDisplay;

/**
 * Class works as a controller for the personal Messages users action
 * @author GalaxE.
 */
@Controller("personalMessageDisplayController")
@RequestMapping(value = "VIEW")
public class PersonalMessageDisplayController extends AbstractBaseController {

	/** The Constant LOG. */
	private static final Log logger = LogFactoryUtil.getLog(PersonalMessageDisplayController.class);

	/**
	 * List to hold the Personal Messages.
	 */
	//List<PersonalMessageVO> personalMessageVOList;

	/**
	 * Default Render Mapping Method.
	 * @param renderRequest
	 * @param renderResponse
	 * @param model
	 * @return view name string
	 */
	@RenderMapping
	public String showPersonalMessage(RenderRequest renderRequest, RenderResponse renderResponse, Model model) {

		LoggerUtil.debugLogger(logger, "Start of showPersonalMessage() method of PersonalMessageDisplayController::");
		Layout currentLayout = WebUtil.getThemeDisplay(renderRequest).getLayout();
		Locale locale = WebUtil.getThemeDisplay(renderRequest).getLocale();
		String renderView = "default";
		boolean interPageIpcFlag = false;
		String interPageParam = WebUtil.getHttpServletRequest(renderRequest).getParameter("interPage");
		if (Validator.isNotNull(interPageParam)) {
			try {
				interPageIpcFlag = Boolean.parseBoolean(interPageParam);
			}
			catch (NumberFormatException ne) {
				LoggerUtil.errorLogger(logger, "Parameter is not coming boolean from request, value is " + interPageParam, ne);
				interPageIpcFlag = false;
			}
		}
		if ("Home".equalsIgnoreCase(currentLayout.getHTMLTitle(locale)) || "Home".equalsIgnoreCase(currentLayout.getName(locale)) ||
			currentLayout.getFriendlyURL().contains("home")) {
			renderView = "ticker";
		}
		else {
			renderView = "default";
		}
		if (renderView.equalsIgnoreCase("ticker")) {
			LoggerUtil.debugLogger(logger, "End of showPersonalMessage() method of PersonalMessageDisplayController::");
			return Constants.PERSONAL_MESSAGE_VIEW;
		}
		else {
			// coming from home page and showing personal message in detail view
			if (interPageIpcFlag) {
				String messageId = (String) renderRequest.getPortletSession().getAttribute("messageId");
				try {
					CIPersonalMessage ciPersonalMessage = serviceLocator.getPersonalMessageService().getPersonalMessage(Long.parseLong(messageId));
					PersonalMessageVO personalMessage = convertCIPerMsgToPerMsgVO(ciPersonalMessage);
					model.addAttribute("personalMessage", personalMessage);
				}
				catch (NumberFormatException e) {
					LoggerUtil.errorLogger(logger, "MessageId is not a number, it is " + messageId, e);
				}
				LoggerUtil.debugLogger(logger, "End of showPersonalMessage() method of PersonalMessageDisplayController::");
				return Constants.DETAIL_PERSONAL_MESSAGE_USER;
			}
			// listing all personal messages
			else {
				// Auditing the action starts
				WebUtil.createAuditRecord(this.getClass().getName(), Constants.PERSONAL_MESSAGE_ACTION_TYPE, Constants.ALL);
				// Auditing the action ends
				LoggerUtil.debugLogger(logger, "End of showPersonalMessage() method of PersonalMessageDisplayController::");
				return Constants.ALL_PERSONAL_MESSAGE_USER;
			}
		}
	}

	/**
	 * Event Mapping method for event having QName =
	 * "{http://coach.com/events}ipc.personalMessageMap". It receives Map object
	 * in event payload which has information about message which is to be
	 * displayed in full details view.
	 * @param eventRequest
	 * @param eventResponse
	 * @param model
	 */
	@SuppressWarnings("unchecked")
	@EventMapping(value = "{http://coach.com/events}ipc.personalMessageMap")
	public void personalMessageEvent(EventRequest eventRequest, EventResponse eventResponse, Model model) {

		LoggerUtil.debugLogger(logger, "Start of personalMessageEvent() method of PersonalMessageDisplayController::");
		Event event = eventRequest.getEvent();
		PortletSession portletSession = eventRequest.getPortletSession();
		HashMap<String, String> personalMessageMap = (HashMap<String, String>) event.getValue();
		String messageId = personalMessageMap.get("messageId");
		portletSession.setAttribute("messageId", messageId);
		LoggerUtil.debugLogger(logger, "End of personalMessageEvent() method of PersonalMessageDisplayController::");
	}

	/**
	 * Model object for populating personal messages.
	 * @param portletRequest
	 * @return List<PersonalMessageVO>
	 */
	@ModelAttribute(value = "personalMessageVOList")
	public List<PersonalMessageVO> getUsersPersonalMessage(PortletRequest portletRequest) {

		LoggerUtil.debugLogger(logger, "Start of getUsersPersonalMessage() method of PersonalMessageDisplayController::");
		List<CIPersonalMessage> personalMessageDOList = new ArrayList<CIPersonalMessage>();
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) portletRequest.getAttribute(WebKeys.THEME_DISPLAY);
			com.liferay.portal.model.User user = themeDisplay.getRealUser();
			personalMessageDOList = serviceLocator.getPersonalMessageService().getUserMessages(user.getUserId(), Constants.ALL_MESSAGES_FOR_USER);

		}
		catch (NumberFormatException nfe) {
			LoggerUtil.errorLogger(logger, "getUsersPersonalMessage() method NumberFormatException", nfe);
		}
		catch (Exception e) {
			LoggerUtil.errorLogger(logger, "getUsersPersonalMessage() method Exception", e);
		}
		List<PersonalMessageVO> personalMessageVOList  = new ArrayList<PersonalMessageVO>();
		if (personalMessageDOList != null && !personalMessageDOList.isEmpty() && personalMessageDOList.size() != 0) {
			for (CIPersonalMessage personalMessage : personalMessageDOList) {
				PersonalMessageVO personalMessageVO = convertCIPerMsgToPerMsgVO(personalMessage);
				personalMessageVOList.add(personalMessageVO);
			}
		}
		LoggerUtil.debugLogger(logger, "End of getUsersPersonalMessage() method of PersonalMessageDisplayController::");
		return personalMessageVOList;
	}

	/**
	 * Action mapping method for getting detailed personal message, and
	 * triggering "http://coach.com/events", "ipc.personalMessageMap" type event
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 * @throws IOException
	 * @throws SystemException
	 * @throws PortalException
	 */
	@ActionMapping(params = "myaction=detailPersonalMessage")
	public void detailPersonalMessage(ActionRequest actionRequest, ActionResponse actionResponse, Model model) throws IOException, PortalException, SystemException {

		LoggerUtil.debugLogger(logger, "Start of detailPersonalMessage() method of PersonalMessageDisplayController::");
		QName qname = new QName("http://coach.com/events", "ipc.personalMessageMap");
		HashMap<String, String> personalMessageMap = new HashMap<String, String>();
		personalMessageMap.put("messageId", actionRequest.getParameter("messageId"));
		actionResponse.setEvent(qname, personalMessageMap);
		// Auditing the action starts
		CIPersonalMessage ciPersonalMessage =
			serviceLocator.getPersonalMessageService().getPersonalMessage(Long.parseLong(actionRequest.getParameter("messageId")));
		PersonalMessageVO personalMessage = convertCIPerMsgToPerMsgVO(ciPersonalMessage);
		WebUtil.createAuditRecord(this.getClass().getName(), Constants.PERSONAL_MESSAGE_ACTION_TYPE, personalMessage.getSubject());
		
		// Auditing the action ends
		actionResponse.sendRedirect(Constants.PERSONAL_MESSAGE_VIEW_URL + "?interPage=true");
		LoggerUtil.debugLogger(logger, "End of detailPersonalMessage() method of PersonalMessageDisplayController::");
	}

	/**
	 * Action mapping method for showing personal message in details.
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 * @throws IOException
	 * @throws SystemException
	 * @throws PortalException
	 */
	@ActionMapping(params = "myaction=personalMessageDetails")
	public void personalMessageDetails(ActionRequest actionRequest, ActionResponse actionResponse, Model model) throws IOException, PortalException, SystemException {

		LoggerUtil.debugLogger(logger, "Start of personalMessageDetails() method of PersonalMessageDisplayController::");
		String messageId = actionRequest.getParameter("messageId");
		PersonalMessageVO personalMessage = null;
		try {
			CIPersonalMessage ciPersonalMessage = serviceLocator.getPersonalMessageService().getPersonalMessage(Long.parseLong(messageId));
			personalMessage = convertCIPerMsgToPerMsgVO(ciPersonalMessage);
			model.addAttribute("personalMessage", personalMessage);
			actionResponse.setRenderParameter("myaction", "renderPersonalMessageDetails");
			// Auditing the action starts
			WebUtil.createAuditRecord(this.getClass().getName(), Constants.PERSONAL_MESSAGE_ACTION_TYPE, personalMessage.getSubject());
			// Auditing the action ends
		}
		catch (NumberFormatException e) {
			LoggerUtil.errorLogger(logger, "MessageId is not a number, it is " + messageId, e);
		}
		LoggerUtil.debugLogger(logger, "End of personalMessageDetails() method of PersonalMessageDisplayController::");
	}

	/**
	 * Render Mapping Method for showing personal message in details.
	 * @param renderRequest
	 * @param renderResponse
	 * @param model
	 * @return view name string
	 */
	@RenderMapping(params = "myaction=renderPersonalMessageDetails")
	public String renderPersonalMessageDetails(RenderRequest renderRequest, RenderResponse renderResponse, Model model) {

		return Constants.DETAIL_PERSONAL_MESSAGE_USER;
	}

	/**
	 * Method to convert Domain Object to Value Object
	 * @param personalMessage
	 * @return PersonalMessageVO
	 */
	public PersonalMessageVO convertCIPerMsgToPerMsgVO(CIPersonalMessage personalMessage) {

		LoggerUtil.debugLogger(logger, "Start of convertCIPerMsgToPerMsgVO() method of PersonalMessageDisplayController::");
		PersonalMessageVO personalMessageVO = new PersonalMessageVO();
		Calendar defaultValueDate = Calendar.getInstance();
		Date displayDate = null;
		displayDate = (Date) personalMessage.getDisplayDate();
		if (Validator.isNull(displayDate)) {
			displayDate = new Date();
		}
		defaultValueDate.setTime(displayDate);
		personalMessageVO.setMessageId(personalMessage.getMessageId());
		personalMessageVO.setDisplayDate((new DateFormatSymbols().getMonths()[defaultValueDate.get(Calendar.MONTH)]) + Constants.SPACE +
			defaultValueDate.get(Calendar.DATE) + Constants.COMMA + Constants.SPACE + defaultValueDate.get(Calendar.YEAR));
		personalMessageVO.setMessage(personalMessage.getMessage());
		if (Constants.MESSAGE_LENGTH_TO_DISPLAY < personalMessage.getSubject().length()) {
			if (personalMessage.getSubject().indexOf(
				Constants.SPACE, ((personalMessage.getSubject().substring(0, Constants.MESSAGE_LENGTH_TO_DISPLAY).lastIndexOf(Constants.SPACE) + 1))) == Constants.MESSAGE_LENGTH_TO_DISPLAY) {
				personalMessageVO.setModifiedMessage((personalMessage.getSubject().substring(0, Constants.MESSAGE_LENGTH_TO_DISPLAY)) +
					Constants.JUNK_CHARS);
			}
			else {
				personalMessageVO.setModifiedMessage((personalMessage.getSubject().substring(0, Constants.MESSAGE_LENGTH_TO_DISPLAY)).substring(
					0, (personalMessage.getSubject().substring(0, Constants.MESSAGE_LENGTH_TO_DISPLAY)).lastIndexOf(Constants.SPACE) + 1) +
					Constants.JUNK_CHARS);
			}
		}
		else {
			personalMessageVO.setModifiedMessage(personalMessage.getSubject());
		}
		personalMessageVO.setSubject(personalMessage.getSubject());
		LoggerUtil.debugLogger(logger, "End of convertCIPerMsgToPerMsgVO() method of PersonalMessageDisplayController::");
		return personalMessageVO;
	}
}
