package com.coach.cip.portlets.controller;


import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.xml.namespace.QName;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.coach.cip.util.WebUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.asset.model.AssetCategory;
import com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;

/**
 * Controller class for Home News Portlet.
 * @author GalaxE
 */
@Controller("DocumentContentController")
@RequestMapping(value = "VIEW")
public class DocumentContentController extends AbstractBaseController {

	/** The Constant logger. */
	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(DocumentContentController.class);

	/**
	 * Default Render Mapping Method. this method is invoked.
	 * @param renderResponse
	 * @return string
	 */
	@RenderMapping
	public String showJournalArticles(RenderRequest renderRequest, RenderResponse renderResponse) {

		return Constants.DOCUMENTCONTENT_JSP;
	}

	/**
	 * Action mapping method for getting detail of all webcontent
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 * @throws IOException
	 */
	@ActionMapping(params = "myaction=detailAllWebContent")
	public void detailallwebcontent(ActionRequest actionRequest, ActionResponse actionResponse, Model model) throws IOException {

		ThemeDisplay themeDisplay = (ThemeDisplay)actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
		LoggerUtil.debugLogger(logger, "Start of detailallwebcontent() method----------------------------------------");
		WebUtil.createNewsAuditRecord(this.getClass().getName(), Constants.COACH_NEWS_TYPE, Constants.NOT_SELECTED, Constants.ALL);
		actionResponse.sendRedirect(WebUtil.getPortalURL(actionRequest) + WebUtil.getPageUrl(actionRequest, PropsUtil.get("ca.all.updates.page.url")));
		LoggerUtil.debugLogger(logger, "End of detailallwebcontent()-----------------------------------------------------");
	}
}
