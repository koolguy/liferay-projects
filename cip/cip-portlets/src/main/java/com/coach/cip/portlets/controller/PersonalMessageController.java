
package com.coach.cip.portlets.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.coach.cip.common.dto.CountryVO;
import com.coach.cip.common.dto.GeographyVO;
import com.coach.cip.common.dto.MessageForLocationVO;
import com.coach.cip.common.dto.PersonalMessageVO;
import com.coach.cip.common.dto.RoleVO;
import com.coach.cip.common.dto.UserVO;
import com.coach.cip.model.entity.Address;
import com.coach.cip.model.entity.CIGeography;
import com.coach.cip.model.entity.CIMessageForLocation;
import com.coach.cip.model.entity.CIPersonalMessage;
import com.coach.cip.model.entity.Country;
import com.coach.cip.model.entity.Region;
import com.coach.cip.model.entity.Role;
import com.coach.cip.model.entity.User;
import com.coach.cip.services.LocationService;
import com.coach.cip.services.PersonalMessageService;
import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;

/**
 * Class works as a controller for the personal Messages actions.
 * @author GalaxE.
 */
@SuppressWarnings("unused")
@Controller("personalMessageController")
@RequestMapping(value = "VIEW")
public class PersonalMessageController extends AbstractBaseController {

	/** The Constant LOG. */
	private static final Log logger = LogFactoryUtil.getLog(PersonalMessageController.class);

	/**
	 * List to hold the Personal Messages.
	 */
	List<PersonalMessageVO> personalMessageVOList;
	/**
	 * List to hold the Geographies.
	 */
	List<GeographyVO> geographyVOList;
	/**
	 * Hold the selected Personal Message.
	 */
	PersonalMessageVO personalMessage;

	/**
	 * Default Render Mapping Method. If Specific Render mapping is not found,
	 * this mapping is invoked.
	 * @param renderRequest
	 * @param renderResponse
	 * @return view name
	 */
	@RenderMapping
	public String showAllPersonalMessage(RenderResponse renderResponse) {

		LoggerUtil.debugLogger(logger, "Render Mapping method for Personal Message form.");
		return Constants.PERSONAL_MESSAGE_ADMIN;
	}

	/**
	 * Render Mapping Method for Add Personal Message Form. This Render Mapping
	 * is invoked after deletePersonalMessage() method on click of
	 * "Add Personal Message" buttons.
	 * @param renderRequest
	 * @return view name
	 */

	@RenderMapping(params = "myaction=addPersonalMessageForm")
	public String showAddPersonalMessage(RenderResponse renderResponse) {

		LoggerUtil.debugLogger(logger, "Render Mapping method for Add Personal Message form.");
		return Constants.ADD_PERSONAL_MESSAGE_ADMIN;
	}

	/**
	 * Render Mapping Method for Edit Personal Message Form. This Render Mapping
	 * is invoked after addPersonalMessage() method on click of
	 * "Add Personal Message" buttons.
	 * @param renderRequest
	 * @return view name
	 */

	@RenderMapping(params = "myaction=editPersonalMessageForm")
	public String showEditPersonalMessage() {

		LoggerUtil.debugLogger(logger, "Render Mapping method for Edit Personal Message form.");
		return Constants.EDIT_PERSONAL_MESSAGE_ADMIN;
	}

	/**
	 * Model Object for populating Personal Message form data.
	 * @param portletRequest
	 * @return List<PersonalMessageVO>
	 */

	@ModelAttribute(value = "personalMessageVOList")
	public List<PersonalMessageVO> getPersonalMessage(PortletRequest portletRequest) {

		LoggerUtil.debugLogger(logger, "Command Object or Model Object for populating Personal Message form data.");
		List<CIPersonalMessage> personalMessageDOList = null;
		try {
			personalMessageDOList = (List<CIPersonalMessage>) serviceLocator.getPersonalMessageService().getAllPersonalMessage();
		}
		catch (Exception e) {
			LoggerUtil.errorLogger(logger, "PersonalMessageController.getPersonalMessage()", e);
		}
		if (personalMessageDOList != null && !personalMessageDOList.isEmpty() && personalMessageDOList.size() != 0) {
			personalMessageVOList = convertPersonalMessageDOToVOList(personalMessageDOList);
		}
		else {
			personalMessageVOList = new ArrayList<PersonalMessageVO>();
		}
		portletRequest.setAttribute("personalMessageVOList", personalMessageVOList);
		return personalMessageVOList;
	}

	/**
	 * Method is used for updating the reference data or model data list with
	 * the updated values (while adding , editing or deleting Personal
	 * Messages).
	 * @param actionRequest
	 * @param model
	 */
	public void updatePersonalMessageModel(ActionRequest actionRequest, Model model) {

		model.addAttribute("personalMessageVOList", getPersonalMessage(actionRequest));
	}

	/**
	 * Model Object for populating Geography data for Locations.
	 * @param portletRequest
	 * @return List<GeographyVO>
	 */

	@SuppressWarnings("unchecked")
	@ModelAttribute(value = "geographyList")
	public List<GeographyVO> getAllGeographies(PortletRequest portletRequest) {

		List<CIGeography> geographyDOList = new ArrayList<CIGeography>();
		LocationService<CIGeography> geographyLocationService = serviceLocator.getLocationService();

		geographyVOList = new ArrayList<GeographyVO>();
		try {
			geographyDOList = geographyLocationService.getAllGeography(CIGeography.class);
		}
		catch (Exception e) {
			LoggerUtil.errorLogger(logger, "PersonalMessageController.getAllGeographies()", e);
		}
		if (geographyDOList != null && !geographyDOList.isEmpty() && geographyDOList.size() != 0) {
			geographyVOList = convertGeographyDOToVOList(geographyDOList);
		}
		portletRequest.setAttribute("geographyVOList", geographyVOList);
		return geographyVOList;
	}

	/**
	 * Action Mapping Method for Add Personal Message Form. This Action Mapping
	 * method is invoked in Add phase on click of "Submit" or "Cancel" button.
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 */
	@ActionMapping(params = "myaction=addPersonalMessage")
	public void addPersonalMessage(ActionRequest actionRequest, ActionResponse actionResponse, Model model) {

		LoggerUtil.debugLogger(logger, "Start of addPersonalMessage() method of Personal Message Controller::");
		if (actionRequest.getParameter("addPM").equals("Submit")) {
			LoggerUtil.debugLogger(logger, "Adding Personal Message.");
			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			Set<Role> roleDOSet = null;
			User user = null;
			CIPersonalMessage ciPersonalMessage = null;
			PersonalMessageVO personalMessageVO = new PersonalMessageVO();
			String locationType = actionRequest.getParameter("locationType");
			String messageType = actionRequest.getParameter("messageType");
			String displayDate = actionRequest.getParameter("displayDate");
			String expirationDate = actionRequest.getParameter("expirationDate");

			personalMessageVO.setSubject(actionRequest.getParameter("subject"));
			personalMessageVO.setMessage(actionRequest.getParameter("message"));
			personalMessageVO.setMessageType(actionRequest.getParameter("messageType"));
			personalMessageVO.setCreateDate(new Timestamp(new Date().getTime()));
			personalMessageVO.setModifiedDate(new Timestamp(new Date().getTime()));
			personalMessageVO.setDisplayDate(displayDate);
			personalMessageVO.setExpirationDate(expirationDate);
			try {
				if (messageType != null && messageType.equals("Regular")) {

					if (locationType != null && !locationType.isEmpty() && !locationType.equals("0")) {
						personalMessageVO.setMessageForLocations(populatingMessageForLocationVO(actionRequest));
					}

					String[] roleIds = actionRequest.getParameterValues("roles");
					Role role = null;
					if (roleIds != null) {
						roleDOSet = new HashSet<Role>();
						for (String s1 : roleIds) {
							role = serviceLocator.getPersonalMessageService().getRole(Long.valueOf(s1));
							roleDOSet.add(role);
						}
					}
				}

				ciPersonalMessage = convertPersonalMessageVOToDO(personalMessageVO);
				user = serviceLocator.getPersonalMessageService().getUser(themeDisplay.getUserId());
				ciPersonalMessage.setMessageForRoles(roleDOSet);
				ciPersonalMessage.setUserByModifiedBy(user);
				ciPersonalMessage.setUserByCreatedBy(user);
				serviceLocator.getPersonalMessageService().addPersonalMessage(ciPersonalMessage);
				// model.addAttribute("message",
				// "Succesfully saved the Message");
				SessionMessages.add(actionRequest, Constants.PERSONALMSG_ADMIN_SUCCESS_ADD_SAVE);
				model.addAttribute("message", Constants.PERSONALMSG_ADMIN_SUCCESS_ADD_SAVE);
			}
			catch (Exception ex) {
				LoggerUtil.errorLogger(logger, "PersonalMessageController.addPersonalMessage()", ex);
				// model.addAttribute("message",
				// "Error while Saving the Message");
				SessionErrors.add(actionRequest, Constants.PERSONALMSG_ADMIN_ERROR_ADD_SAVE);
				model.addAttribute("message", Constants.PERSONALMSG_ADMIN_ERROR_ADD_SAVE);
			}
			updatePersonalMessageModel(actionRequest, model);
		}
		else if (actionRequest.getParameter("addPM").equals("Reset")) {
			actionResponse.setRenderParameter("myaction", "addPersonalMessageForm");
		}
		LoggerUtil.debugLogger(logger, "End of addPersonalMessage() method of Personal Message Controller::");
	}

	/**
	 * Action Mapping Method for Edit Personal Message Form. This Action Mapping
	 * method is invoked on click of "Personal Message" to edit. Method
	 * populates the Edit form data.
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 */

	@SuppressWarnings("unchecked")
	@ActionMapping(params = "myaction=editPersonalMessage")
	public void editPersonalMessage(ActionRequest actionRequest, ActionResponse actionResponse, Model model) {

		LoggerUtil.debugLogger(logger, "Start of editPersonalMessage() method of Personal Message Controller::");
		LocationService<Address> addressLocationService = serviceLocator.getLocationService();
		LocationService<Region> stateLocationService = serviceLocator.getLocationService();
		LocationService<CIGeography> countryLocationService = serviceLocator.getLocationService();
		LocationService<CIGeography> geographyLocationService = serviceLocator.getLocationService();

		personalMessage = searchList(actionRequest.getParameter("messageId"));
		for (MessageForLocationVO messageForLocationVO : personalMessage.getMessageForLocations()) {
			model.addAttribute("messageForLocationId", messageForLocationVO.getCimflId());
			model.addAttribute("locationType", messageForLocationVO.getLocationType());

			if ((messageForLocationVO.getLocationType().equals("Building")) && messageForLocationVO.getState() != null &&
				messageForLocationVO.getCity() != null && messageForLocationVO.getOffice() != null) {
				String selectedOffice = messageForLocationVO.getOffice();
				List<String> buildingsList =
					addressLocationService.getAllDistinctAddressLocation(Address.class, "street2", "street3", messageForLocationVO.getOffice());
				List<String> officesList =
					addressLocationService.getAllDistinctAddressLocation(Address.class, "street3", "city", messageForLocationVO.getCity());
				List<String> cityList =
					addressLocationService.getAllDistinctAddressLocation(Address.class, "city", "regionId", messageForLocationVO.getState());
				List<Region> selectedCountryStateList =
					stateLocationService.getAllLocation(Region.class, "regionId", Long.valueOf(messageForLocationVO.getState()));
				List<Region> stateList =
					stateLocationService.getAllLocation(Region.class, "countryId", selectedCountryStateList.get(0).getCountryId());
				List<Object> geographyCountryId =
					geographyLocationService.getGeographyCountry("countryId", selectedCountryStateList.get(0).getCountryId());
				Object[] geographyCountry = (Object[]) geographyCountryId.get(0);
				String geographyId = geographyCountry[0].toString();
				String countryId = geographyCountry[1].toString();

				List<CIGeography> countryList = countryLocationService.getAllLocation(CIGeography.class, "geographyId", Long.valueOf(geographyId));

				model.addAttribute("selectedGeographyId", geographyId);
				model.addAttribute("countryList", countryList.get(0).getCiGeographycountries());
				model.addAttribute("selectedCountryId", selectedCountryStateList.get(0).getCountryId());
				model.addAttribute("stateList", stateList);
				model.addAttribute("selectedStateId", selectedCountryStateList.get(0).getRegionId());
				model.addAttribute("cityList", cityList);
				model.addAttribute("officesList", officesList);
				model.addAttribute("selectedOffice", selectedOffice);
				model.addAttribute("buildingsList", buildingsList);

			}
			else if ((messageForLocationVO.getLocationType().equals("Office")) && messageForLocationVO.getState() != null &&
				messageForLocationVO.getCity() != null) {

				List<String> officesList =
					addressLocationService.getAllDistinctAddressLocation(Address.class, "street3", "city", messageForLocationVO.getCity());
				List<String> cityList =
					addressLocationService.getAllDistinctAddressLocation(Address.class, "city", "regionId", messageForLocationVO.getState());
				List<Region> selectedCountryStateList =
					stateLocationService.getAllLocation(Region.class, "regionId", Long.valueOf(messageForLocationVO.getState()));
				List<Region> stateList =
					stateLocationService.getAllLocation(Region.class, "countryId", selectedCountryStateList.get(0).getCountryId());
				List<Object> geographyCountryId =
					geographyLocationService.getGeographyCountry("countryId", selectedCountryStateList.get(0).getCountryId());
				Object[] geographyCountry = (Object[]) geographyCountryId.get(0);
				String geographyId = geographyCountry[0].toString();
				String countryId = geographyCountry[1].toString();

				List<CIGeography> countryList = countryLocationService.getAllLocation(CIGeography.class, "geographyId", Long.valueOf(geographyId));

				model.addAttribute("selectedGeographyId", geographyId);
				model.addAttribute("countryList", countryList.get(0).getCiGeographycountries());
				model.addAttribute("selectedCountryId", selectedCountryStateList.get(0).getCountryId());
				model.addAttribute("stateList", stateList);
				model.addAttribute("selectedStateId", selectedCountryStateList.get(0).getRegionId());
				model.addAttribute("cityList", cityList);
				model.addAttribute("officesList", officesList);
				model.addAttribute("selectedOffice", messageForLocationVO.getLocationName());

			}
			else if ((messageForLocationVO.getLocationType().equals("City")) && messageForLocationVO.getState() != null) {

				List<String> cityList =
					addressLocationService.getAllDistinctAddressLocation(Address.class, "city", "regionId", messageForLocationVO.getState());
				List<Region> selectedCountryStateList =
					stateLocationService.getAllLocation(Region.class, "regionId", Long.valueOf(messageForLocationVO.getState()));
				List<Region> stateList =
					stateLocationService.getAllLocation(Region.class, "countryId", selectedCountryStateList.get(0).getCountryId());
				List<Object> geographyCountryId =
					geographyLocationService.getGeographyCountry("countryId", selectedCountryStateList.get(0).getCountryId());
				Object[] geographyCountry = (Object[]) geographyCountryId.get(0);
				String geographyId = geographyCountry[0].toString();
				String countryId = geographyCountry[1].toString();

				List<CIGeography> countryList = countryLocationService.getAllLocation(CIGeography.class, "geographyId", Long.valueOf(geographyId));
                if(Validator.isNull(messageForLocationVO.getCity())){
                	messageForLocationVO.setCity(messageForLocationVO.getLocationName());
                }
				model.addAttribute("selectedGeographyId", geographyId);
				model.addAttribute("countryList", countryList.get(0).getCiGeographycountries());
				model.addAttribute("selectedCountryId", selectedCountryStateList.get(0).getCountryId());
				model.addAttribute("stateList", stateList);
				model.addAttribute("selectedStateId", selectedCountryStateList.get(0).getRegionId());
				model.addAttribute("cityList", cityList);

			}
			else if (messageForLocationVO.getLocationType().equals("Region")) {

				List<Region> selectedCountryStateList =
					stateLocationService.getAllLocation(Region.class, "regionId", Long.valueOf(messageForLocationVO.getLocationName()));
				List<Region> stateList =
					stateLocationService.getAllLocation(Region.class, "countryId", selectedCountryStateList.get(0).getCountryId());
				List<Object> geographyCountryId =
					geographyLocationService.getGeographyCountry("countryId", selectedCountryStateList.get(0).getCountryId());
				Object[] geographyCountry = (Object[]) geographyCountryId.get(0);
				String geographyId = geographyCountry[0].toString();
				String countryId = geographyCountry[1].toString();

				List<CIGeography> countryList = countryLocationService.getAllLocation(CIGeography.class, "geographyId", Long.valueOf(geographyId));

				model.addAttribute("selectedGeographyId", geographyId);
				model.addAttribute("countryList", countryList.get(0).getCiGeographycountries());
				model.addAttribute("selectedCountryId", selectedCountryStateList.get(0).getCountryId());
				model.addAttribute("stateList", stateList);
				model.addAttribute("selectedStateId", selectedCountryStateList.get(0).getRegionId());
			}
			else if (messageForLocationVO.getLocationType().equals("Country")) {

				List<Object> geographyCountryId =
					geographyLocationService.getGeographyCountry("countryId", Long.valueOf(messageForLocationVO.getLocationName()));
				Object[] geographyCountry = (Object[]) geographyCountryId.get(0);
				String geographyId = geographyCountry[0].toString();
				String countryId = geographyCountry[1].toString();

				List<CIGeography> countryList = countryLocationService.getAllLocation(CIGeography.class, "geographyId", Long.valueOf(geographyId));

				model.addAttribute("selectedGeographyId", geographyId);
				model.addAttribute("countryList", countryList.get(0).getCiGeographycountries());
				model.addAttribute("selectedCountryId", messageForLocationVO.getLocationName());
			}
			else if (messageForLocationVO.getLocationType().equals("Geography")) {

				model.addAttribute("selectedGeographyId", messageForLocationVO.getLocationName());
			}
		}
		model.addAttribute("personalMessage", personalMessage);
		LoggerUtil.debugLogger(logger, "End of editPersonalMessage() method of Personal Message Controller::");
		actionResponse.setRenderParameter("myaction", "editPersonalMessageForm");
	}

	/**
	 * Action Mapping Method for Saving the edited Personal Message. This Action
	 * Mapping method is invoked on click of "Submit" to save the changes to the
	 * Message or "Cancel" to navigate to Personal Message.
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 */

	@ActionMapping(params = "myaction=updatePersonalMessage")
	public void updatePersonalMessage(ActionRequest actionRequest, ActionResponse actionResponse, Model model) {

		LoggerUtil.debugLogger(logger, "Start of updatePersonalMessage() method of Personal Message Controller::");
		if (actionRequest.getParameter("editPM").equals("Submit")) {

			ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(WebKeys.THEME_DISPLAY);
			Set<Role> roleDOSet = null;
			User modifiedUser = null;
			Set<CIMessageForLocation> messageForLocation = new HashSet<CIMessageForLocation>();
			String displayDate = actionRequest.getParameter("displayDate");
			String expirationDate = actionRequest.getParameter("expirationDate");
			String locationType = actionRequest.getParameter("locationType");
			String messageType = actionRequest.getParameter("messageType");

			try {
				if (messageType != null && messageType.equals("Regular")) {

					if (locationType != null && !locationType.isEmpty() && !locationType.equals("0")) {
						messageForLocation = convertMessageForLocationVOToDO(populatingMessageForLocationVO(actionRequest));
					}
					String[] roleIds = actionRequest.getParameterValues("roles");
					Role role = null;
					if (roleIds != null) {
						roleDOSet = new HashSet<Role>();
						for (String s1 : roleIds) {
							role = serviceLocator.getPersonalMessageService().getRole(Long.valueOf(s1));
							roleDOSet.add(role);
						}
					}
				}

				CIPersonalMessage ciPersonalMessage = serviceLocator.getPersonalMessageService().getPersonalMessage(personalMessage.getMessageId());
				ciPersonalMessage.setDisplayDate(dateFormatConversion(displayDate));
				ciPersonalMessage.setExpirationDate(dateFormatConversion(expirationDate));
				ciPersonalMessage.setSubject(actionRequest.getParameter("subject"));
				ciPersonalMessage.setMessage(actionRequest.getParameter("message"));
				ciPersonalMessage.setMessageType(messageType);
				ciPersonalMessage.setModifiedDate(new Timestamp(new Date().getTime()));
				ciPersonalMessage.setMessageForRoles(roleDOSet);
				ciPersonalMessage.setUserByModifiedBy(serviceLocator.getPersonalMessageService().getUser(themeDisplay.getUserId()));
				ciPersonalMessage.setUserByCreatedBy(serviceLocator.getPersonalMessageService().getUser(
					personalMessage.getUserByCreatedBy().getUserId()));
				if (ciPersonalMessage.getMessageType() != null &&
					(!ciPersonalMessage.getMessageType().equals("Regular") || locationType.isEmpty() || locationType.equals("0")) &&
					ciPersonalMessage.getMessageForLocations().size() > 0) {
					ciPersonalMessage.getMessageForLocations().clear();
				}
				else {
					ciPersonalMessage.setMessageForLocations(messageForLocation);

					/*
					 * Added to set the messageId value to ci_meesgaeforlocation
					 * table as we are unable to set through @ManytoOne
					 * annotations.
					 */

					for (CIMessageForLocation ciMessageForLocation : ciPersonalMessage.getMessageForLocations()) {
						ciMessageForLocation.setPersonalMessage(ciPersonalMessage);
					}
				}
				serviceLocator.getPersonalMessageService().updatePersonalMessage(ciPersonalMessage);
				SessionMessages.add(actionRequest, Constants.PERSONALMSG_ADMIN_SUCCESS_UPDATE);
				model.addAttribute("message", Constants.PERSONALMSG_ADMIN_SUCCESS_UPDATE);
			}
			catch (Exception ex) {
				LoggerUtil.errorLogger(logger, "PersonalMessageController.updatePersonalMessage():::", ex);
				SessionErrors.add(actionRequest, Constants.PERSONALMSG_ADMIN_ERROR_UPDATE);
				model.addAttribute("message", Constants.PERSONALMSG_ADMIN_ERROR_UPDATE);
			}
			updatePersonalMessageModel(actionRequest, model);
		}
		LoggerUtil.debugLogger(logger, "End of updatePersonalMessage() method of Personal Message Controller::");
	}

	/**
	 * Action Mapping Method for Delete Personal Message Form. This Action
	 * Mapping method is invoked in Delete phase on click of "Delete Message" or
	 * "Add Message" button.
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 */
	@ActionMapping(params = "myaction=deletPersonalMessageForm")
	public void deletePersonalMessage(ActionRequest actionRequest, ActionResponse actionResponse, Model model) {

		LoggerUtil.debugLogger(logger, "Start of deletePersonalMessage() method of Personal Message Controller::");
		if (actionRequest.getParameter("personalMessageOperation").equals("Add Messages")) {
			LoggerUtil.debugLogger(logger, "Redirecting to Add Personal Message Form.");
			actionResponse.setRenderParameter("myaction", "addPersonalMessageForm");
		}
		else if (actionRequest.getParameter("personalMessageOperation").equals("Delete Messages")) {
			PersonalMessageService personalMessageService = serviceLocator.getPersonalMessageService();
			String msgId[] = actionRequest.getParameterValues("rowIds");
			if (msgId != null) {
				for (String messageId : msgId) {
					try {
						personalMessageService.deletePersonalMessage(Long.valueOf(messageId));
						SessionMessages.add(actionRequest, Constants.PERSONALMSG_ADMIN_SUCCESS_DELETE);
						model.addAttribute("message", Constants.PERSONALMSG_ADMIN_SUCCESS_DELETE);
					}
					catch (Exception e) {
						LoggerUtil.errorLogger(logger, "PersonalMessageController.deletePersonalMessage():::", e);
						SessionErrors.add(actionRequest, Constants.PERSONALMSG_ADMIN_ERROR_DELETE);
						model.addAttribute("message", Constants.PERSONALMSG_ADMIN_ERROR_DELETE);
					}
				}
				updatePersonalMessageModel(actionRequest, model);
			}
			else {
				model.addAttribute("message", "Select the Message(s) to delete");
			}
		}
		LoggerUtil.debugLogger(logger, "End of deletePersonalMessage() method of Personal Message Controller::");
	}

	/**
	 * Helper method to convert String Date to Timestamp format.
	 * @param stringDate
	 * @return Timestamp
	 */
	public Timestamp dateFormatConversion(String stringDate) {

		Date date = null;

		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

		try {
			if (Validator.isNotNull(stringDate )) {
				date = (Date) formatter.parse(stringDate);
			}
			if(Validator.isNull(date)){
				date = new Date();
			}
		}
		catch (ParseException e1) {

			LoggerUtil.errorLogger(logger, "PersonalMessageController.dateFormatConversion() Parse Exception :::", e1);
		}

		return new Timestamp(date.getTime());
	}

	/**
	 * Helper method to return Personal Message Object from the model attribute.
	 * @param messageId
	 * @return PersonalMessageVO
	 */
	public PersonalMessageVO searchList(String messageId) {

		PersonalMessageVO personalMessageVO = null;
		for (PersonalMessageVO pmVO : personalMessageVOList) {
			if (pmVO.getMessageId().equals(Long.valueOf(messageId))) {
				personalMessageVO = pmVO;
			}
		}
		return personalMessageVO;
	}

	/**
	 * Helper method for converting List of Personal Message Domain Object to
	 * Value Object.
	 * @param ciPersonalMessage
	 * @return List<PersonalMessageVO>
	 */
	private List<PersonalMessageVO> convertPersonalMessageDOToVOList(List<CIPersonalMessage> ciPersonalMessage) {

		personalMessageVOList = new ArrayList<PersonalMessageVO>();

		for (CIPersonalMessage personalMessage : ciPersonalMessage) {
			personalMessageVOList.add(convertPersonalMessageDOToVO(personalMessage));
		}
		return personalMessageVOList;
	}

	/**
	 * Helper method for converting Personal Message Domain Object to Value
	 * Object.
	 * @param ciPersonalMessage
	 * @return PersonalMessageVO
	 */
	private PersonalMessageVO convertPersonalMessageDOToVO(CIPersonalMessage ciPersonalMessage) {

		PersonalMessageVO personalMessageVO = new PersonalMessageVO();
		Format formatter = new SimpleDateFormat("MM/dd/yyyy");
		personalMessageVO.setCreateDate(ciPersonalMessage.getCreateDate());
		personalMessageVO.setDisplayDate(formatter.format(ciPersonalMessage.getDisplayDate()));
		personalMessageVO.setExpirationDate(formatter.format(ciPersonalMessage.getExpirationDate()));
		personalMessageVO.setMessage(ciPersonalMessage.getMessage());
		personalMessageVO.setMessageForLocations(convertMessageForLocationDOToVO(ciPersonalMessage.getMessageForLocations()));
		personalMessageVO.setMessageForRoles(convertRoleDOToVO(ciPersonalMessage.getMessageForRoles()));
		personalMessageVO.setMessageId(ciPersonalMessage.getMessageId());
		personalMessageVO.setMessageType(ciPersonalMessage.getMessageType());
		personalMessageVO.setModifiedDate(ciPersonalMessage.getModifiedDate());
		personalMessageVO.setSubject(ciPersonalMessage.getSubject());
		personalMessageVO.setUserByCreatedBy(convertUserDOToVO(ciPersonalMessage.getUserByCreatedBy()));
		personalMessageVO.setUserByModifiedBy(convertUserDOToVO(ciPersonalMessage.getUserByModifiedBy()));

		return personalMessageVO;
	}

	/**
	 * Helper method for converting Message For Location Domain Object to Value
	 * Object.
	 * @param messageForLocation
	 * @return Set<MessageForLocationVO>
	 */
	private Set<MessageForLocationVO> convertMessageForLocationDOToVO(Set<CIMessageForLocation> messageForLocation) {

		Set<MessageForLocationVO> messageForLocationVOSet = new HashSet<MessageForLocationVO>();
		for (CIMessageForLocation ciMessageForLocation : messageForLocation) {
			MessageForLocationVO messageForLocationVO = new MessageForLocationVO();
			messageForLocationVO.setCimflId(ciMessageForLocation.getCimflId());
			messageForLocationVO.setLocationType(ciMessageForLocation.getLocationType());
			messageForLocationVO.setLocationName(ciMessageForLocation.getLocationName());
			messageForLocationVO.setState(ciMessageForLocation.getState());
			messageForLocationVO.setCity(ciMessageForLocation.getCity());
			messageForLocationVO.setOffice(ciMessageForLocation.getOffice());
			messageForLocationVOSet.add(messageForLocationVO);
		}
		return messageForLocationVOSet;
	}

	/**
	 * Helper method for converting Role Domain Object to Value Object.
	 * @param role
	 * @return Set<RoleVO>
	 */
	private Set<RoleVO> convertRoleDOToVO(Set<Role> role) {

		Set<RoleVO> roleVOSet = new HashSet<RoleVO>();
		for (Role rle : role) {
			RoleVO roleVO = new RoleVO();
			roleVO.setClassNameId(rle.getClassNameId());
			roleVO.setClassPk(rle.getClassPk());
			roleVO.setCompanyId(rle.getCompanyId());
			roleVO.setDescription(rle.getDescription());
			roleVO.setName(rle.getName());
			roleVO.setRoleId(rle.getRoleId());
			roleVO.setSubtype(rle.getSubtype());
			roleVO.setTitle(rle.getTitle());
			roleVO.setType(rle.getType());

			roleVOSet.add(roleVO);
		}
		return roleVOSet;
	}

	/**
	 * Helper method for converting User Domain Object to Value Object.
	 * @param user
	 * @return UserVO
	 */
	private UserVO convertUserDOToVO(User user) {

		UserVO userVO = new UserVO();

		userVO.setUserId(user.getUserId());
		userVO.setUuid(user.getUuid());
		userVO.setCompanyId(user.getCompanyId());
		userVO.setCreateDate(user.getCreateDate());
		userVO.setModifiedDate(user.getModifiedDate());
		userVO.setDefaultUser(user.getDefaultUser());
		userVO.setContactId(user.getContactId());
		userVO.setPassword(user.getPassword());
		userVO.setPasswordEncrypted(user.getPasswordEncrypted());
		userVO.setPasswordReset(user.getPasswordReset());
		userVO.setPasswordModifiedDate(user.getPasswordModifiedDate());
		userVO.setDigest(user.getDigest());
		userVO.setReminderQueryQuestion(user.getReminderQueryQuestion());
		userVO.setReminderQueryAnswer(user.getReminderQueryAnswer());
		userVO.setGraceLoginCount(user.getGraceLoginCount());
		userVO.setScreenName(user.getScreenName());
		userVO.setEmailAddress(user.getEmailAddress());
		userVO.setFacebookId(user.getFacebookId());
		userVO.setOpenId(user.getOpenId());
		userVO.setPortraitId(user.getPortraitId());
		userVO.setLanguageId(user.getLanguageId());
		userVO.setTimeZoneId(user.getTimeZoneId());
		userVO.setGreeting(user.getGreeting());
		userVO.setComments(user.getComments());
		userVO.setFirstName(user.getFirstName());
		userVO.setMiddleName(user.getMiddleName());
		userVO.setLastName(user.getLastName());
		userVO.setJobTitle(user.getJobTitle());
		userVO.setLoginDate(user.getLoginDate());
		userVO.setLoginIp(user.getLoginIp());
		userVO.setLastLoginDate(user.getLastLoginDate());
		userVO.setLastLoginIp(user.getLastLoginIp());
		userVO.setLastFailedLoginDate(user.getLastFailedLoginDate());
		userVO.setFailedLoginAttempts(user.getFailedLoginAttempts());
		userVO.setLockout(user.getLockout());
		userVO.setLockoutDate(user.getLockoutDate());
		userVO.setAgreedToTermsOfUse(user.getAgreedToTermsOfUse());
		userVO.setEmailAddressVerified(user.getEmailAddressVerified());
		userVO.setStatus(user.getStatus());
		userVO.setAdminAssistSet(user.getCIEmployeesForAdminAssistId());
		userVO.setUserMetricsSet(user.getCIUserMetrics());
		userVO.setQuickLinksSet(user.getCIQuickLinks());
		userVO.setPersonalMessagesCreatorSet(user.getCIPersonalMessagesForCreatedBy());
		userVO.setPersonalMessagesModifierSet(user.getCIPersonalMessagesForModifiedBy());
		userVO.setSupervisorSet(user.getCIEmployeesForSupervisorId());
		userVO.setStoreEmployeesSet(user.getCIStoreEmployees());
		userVO.setUserSet(user.getCIEmployeesForUserId());

		return userVO;
	}

	/**
	 * Helper method for converting List of Personal Message Value Object to
	 * Domain Object.
	 * @param personalMessageVO
	 * @return List<CIPersonalMessage>
	 */
	private List<CIPersonalMessage> convertPersonalMessageVOToDOList(List<PersonalMessageVO> personalMessageVO) {

		List<CIPersonalMessage> personalMessageDOList = new ArrayList<CIPersonalMessage>();

		for (PersonalMessageVO personalMessage : personalMessageVO) {
			personalMessageDOList.add(convertPersonalMessageVOToDO(personalMessage));
		}
		return personalMessageDOList;
	}

	/**
	 * Helper method for converting Personal Message Value Object to Domain
	 * Object.
	 * @param personalMessageVO
	 * @return CIPersonalMessage
	 */
	private CIPersonalMessage convertPersonalMessageVOToDO(PersonalMessageVO personalMessageVO) {

		Date displayDate = null;
		Date expirationDate = null;
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		CIPersonalMessage personalMessage = new CIPersonalMessage();

		try {
			displayDate = (Date) formatter.parse(personalMessageVO.getDisplayDate());
			expirationDate = (Date) formatter.parse(personalMessageVO.getExpirationDate());
		}
		catch (ParseException e1) {
			LoggerUtil.errorLogger(logger, "PersonalMessageController.convertPersonalMessageVOToDO():::", e1);
		}

		personalMessage.setCreateDate(personalMessageVO.getCreateDate());
		if (displayDate != null) {
			personalMessage.setDisplayDate(new Timestamp(displayDate.getTime()));
		}
		if (expirationDate != null) {
			personalMessage.setExpirationDate(new Timestamp(expirationDate.getTime()));
		}
		personalMessage.setMessage(personalMessageVO.getMessage());
		personalMessage.setMessageId(personalMessageVO.getMessageId());
		personalMessage.setMessageType(personalMessageVO.getMessageType());
		personalMessage.setModifiedDate(personalMessageVO.getModifiedDate());
		personalMessage.setSubject(personalMessageVO.getSubject());
		// personalMessage.setUserByCreatedBy(convertPersonalMessageUserVOToDO(personalMessageVO.getUserByCreatedBy()));
		// personalMessage.setUserByModifiedBy(convertPersonalMessageUserVOToDO(personalMessageVO.getUserByModifiedBy()));
		// personalMessage.setMessageForRoles(convertPersonalMessageRoleVOToDO(personalMessageVO.getMessageForRoles()));
		personalMessage.setMessageForLocations(convertMessageForLocationVOToDO(personalMessageVO.getMessageForLocations()));

		/*
		 * Added to set the messageId value to ci_meesgaeforlocation table as we
		 * are unable to set through @ManytoOne annotations.
		 */
		for (CIMessageForLocation ciMessageForLocation : personalMessage.getMessageForLocations()) {

			ciMessageForLocation.setPersonalMessage(personalMessage);
		}

		return personalMessage;
	}

	/**
	 * Helper method for converting Message for Location Value Object to Domain
	 * Object.
	 * @param messageForLocationVO
	 * @return Set<CIMessageForLocation>
	 */
	private Set<CIMessageForLocation> convertMessageForLocationVOToDO(Set<MessageForLocationVO> messageForLocationVO) {

		Set<CIMessageForLocation> messageForLocationDOSet = new HashSet<CIMessageForLocation>();
		for (MessageForLocationVO msgForLocationVO : messageForLocationVO) {
			CIMessageForLocation ciMessageForLocation = new CIMessageForLocation();
			ciMessageForLocation.setCimflId(msgForLocationVO.getCimflId());
			ciMessageForLocation.setLocationType(msgForLocationVO.getLocationType());
			ciMessageForLocation.setLocationName(msgForLocationVO.getLocationName());
			ciMessageForLocation.setState(msgForLocationVO.getState());
			ciMessageForLocation.setCity(msgForLocationVO.getCity());
			ciMessageForLocation.setOffice(msgForLocationVO.getOffice());

			messageForLocationDOSet.add(ciMessageForLocation);
		}
		return messageForLocationDOSet;
	}

	/**
	 * Helper method for converting Role Value Object to Domain Object.
	 * @param messageForRoles
	 * @return Set<Role>
	 */
	private Set<Role> convertPersonalMessageRoleVOToDO(Set<RoleVO> messageForRoles) {

		Set<Role> rolesSet = new HashSet<Role>();
		if (messageForRoles != null && !messageForRoles.isEmpty() && messageForRoles.size() != 0) {
			for (RoleVO role : messageForRoles) {
				Role tempRole = new Role();
				tempRole.setType(role.getType());
				tempRole.setTitle(role.getTitle());
				tempRole.setSubtype(role.getSubtype());
				tempRole.setRoleId(role.getRoleId());
				tempRole.setName(role.getName());
				tempRole.setDescription(role.getDescription());
				tempRole.setCompanyId(role.getCompanyId());
				tempRole.setClassPk(role.getClassPk());
				tempRole.setClassNameId(role.getClassNameId());
				rolesSet.add(tempRole);
			}
		}
		return rolesSet;
	}

	/**
	 * Helper method for converting User Value Object to Domain Object.
	 * @param userVO
	 * @return User
	 */
	private User convertPersonalMessageUserVOToDO(UserVO userVO) {

		User user = new User();
		if (userVO != null) {
			userVO.setUserId(userVO.getUserId());
			userVO.setUuid(userVO.getUuid());
			userVO.setCompanyId(userVO.getCompanyId());
			userVO.setCreateDate(userVO.getCreateDate());
			userVO.setModifiedDate(userVO.getModifiedDate());
			userVO.setDefaultUser(userVO.getDefaultUser());
			userVO.setContactId(userVO.getContactId());
			userVO.setPassword(userVO.getPassword());
			userVO.setPasswordEncrypted(userVO.getPasswordEncrypted());
			userVO.setPasswordReset(userVO.getPasswordReset());
			userVO.setPasswordModifiedDate(userVO.getPasswordModifiedDate());
			userVO.setDigest(userVO.getDigest());
			userVO.setReminderQueryQuestion(userVO.getReminderQueryQuestion());
			userVO.setReminderQueryAnswer(userVO.getReminderQueryAnswer());
			userVO.setGraceLoginCount(userVO.getGraceLoginCount());
			userVO.setScreenName(userVO.getScreenName());
			userVO.setEmailAddress(userVO.getEmailAddress());
			userVO.setFacebookId(userVO.getFacebookId());
			userVO.setOpenId(userVO.getOpenId());
			userVO.setPortraitId(userVO.getPortraitId());
			userVO.setLanguageId(userVO.getLanguageId());
			userVO.setTimeZoneId(userVO.getTimeZoneId());
			userVO.setGreeting(userVO.getGreeting());
			userVO.setComments(userVO.getComments());
			userVO.setFirstName(userVO.getFirstName());
			userVO.setMiddleName(userVO.getMiddleName());
			userVO.setLastName(userVO.getLastName());
			userVO.setJobTitle(userVO.getJobTitle());
			userVO.setLoginDate(userVO.getLoginDate());
			userVO.setLoginIp(userVO.getLoginIp());
			userVO.setLastLoginDate(userVO.getLastLoginDate());
			userVO.setLastLoginIp(userVO.getLastLoginIp());
			userVO.setLastFailedLoginDate(userVO.getLastFailedLoginDate());
			userVO.setFailedLoginAttempts(userVO.getFailedLoginAttempts());
			userVO.setLockout(userVO.getLockout());
			userVO.setLockoutDate(userVO.getLockoutDate());
			userVO.setAgreedToTermsOfUse(userVO.getAgreedToTermsOfUse());
			userVO.setEmailAddressVerified(userVO.getEmailAddressVerified());
			userVO.setStatus(userVO.getStatus());
			userVO.setAdminAssistSet(userVO.getAdminAssistSet());
			userVO.setUserMetricsSet(userVO.getUserMetricsSet());
			userVO.setQuickLinksSet(userVO.getQuickLinksSet());
			userVO.setPersonalMessagesCreatorSet(userVO.getPersonalMessagesCreatorSet());
			userVO.setPersonalMessagesModifierSet(userVO.getPersonalMessagesModifierSet());
			userVO.setSupervisorSet(userVO.getSupervisorSet());
			userVO.setStoreEmployeesSet(userVO.getStoreEmployeesSet());
			userVO.setUserSet(userVO.getUserSet());
		}
		return user;
	}

	/**
	 * Helper method for populating Location specific Personal Message to the
	 * Value Object from ActionRequest.
	 * @param actionRequest
	 * @return Set<MessageForLocationVO>
	 */
	public Set<MessageForLocationVO> populatingMessageForLocationVO(ActionRequest actionRequest) {

		LoggerUtil.debugLogger(logger, "Start of populatingMessageForLocation() method of Personal Message Controller::");

		String messageForLocationId = actionRequest.getParameter("messageForLocationId");
		String locationType = actionRequest.getParameter("locationType");
		String geographyId = actionRequest.getParameter("geographyId");
		String countryId = actionRequest.getParameter("countryId");
		String stateId = actionRequest.getParameter("stateId");
		String cityName = actionRequest.getParameter("cityName");
		String officeName = actionRequest.getParameter("officeName");
		String buildingName = actionRequest.getParameter("buildingName");
		Set<MessageForLocationVO> messageForLocationVOSet = new HashSet<MessageForLocationVO>();

		MessageForLocationVO msgForLocationVO = new MessageForLocationVO();
		if (null != messageForLocationId && !messageForLocationId.isEmpty()) {
			msgForLocationVO.setCimflId(Long.valueOf(messageForLocationId));
		}

		if (locationType.equals("Building") && buildingName != null && !buildingName.equals("0")) {
			msgForLocationVO.setLocationType(locationType);
			msgForLocationVO.setLocationName(buildingName);
			msgForLocationVO.setState(stateId);
			msgForLocationVO.setCity(cityName);
			msgForLocationVO.setOffice(officeName);
		}
		else if (locationType.equals("Office") && officeName != null && !officeName.equals("0")) {
			msgForLocationVO.setLocationType(locationType);
			msgForLocationVO.setLocationName(officeName);
			msgForLocationVO.setState(stateId);
			msgForLocationVO.setCity(cityName);
		}
		else if (locationType.equals("City") && cityName != null && !cityName.equals("0")) {
			msgForLocationVO.setLocationType(locationType);
			msgForLocationVO.setLocationName(cityName);
			msgForLocationVO.setState(stateId);
		}
		else if (locationType.equals("Region") && stateId != null && !stateId.equals("0")) {
			msgForLocationVO.setLocationType(locationType);
			msgForLocationVO.setLocationName(stateId);
		}
		else if (locationType.equals("Country") && countryId != null && !countryId.equals("0")) {
			msgForLocationVO.setLocationType(locationType);
			msgForLocationVO.setLocationName(countryId);
		}
		else if (locationType.equals("Geography") && geographyId != null && !geographyId.equals("0")) {
			msgForLocationVO.setLocationType(locationType);
			msgForLocationVO.setLocationName(geographyId);
		}

		messageForLocationVOSet.add(msgForLocationVO);

		LoggerUtil.debugLogger(logger, "End of populatingMessageForLocation() method of Personal Message Controller::");
		return messageForLocationVOSet;
	}

	/**
	 * Helper method for converting List of Geography Domain Object to Value
	 * Object.
	 * @param geographyDOList
	 * @return List<GeographyVO>
	 */
	private List<GeographyVO> convertGeographyDOToVOList(List<CIGeography> geographyDOList) {

		geographyVOList = new ArrayList<GeographyVO>();
		for (CIGeography ciGeography : geographyDOList) {
			geographyVOList.add(convertGeographyDOToVO(ciGeography));
		}
		return geographyVOList;
	}

	/**
	 * Helper method for converting Geography Domain Object to Value Object.
	 * @param ciGeography
	 * @return GeographyVO
	 */
	private GeographyVO convertGeographyDOToVO(CIGeography ciGeography) {

		GeographyVO geographyVO = new GeographyVO();
		geographyVO.setGeographyId(ciGeography.getGeographyId());
		geographyVO.setGeographyName(ciGeography.getGeographyName());

		return geographyVO;
	}

	/**
	 * Helper method for converting Country Domain Object to Value Object.
	 * @param ciGeographycountries
	 * @return Set<CountryVO>
	 */
	private Set<CountryVO> convertGeographyCountryDotoVO(Set<Country> ciGeographycountries) {

		Set<CountryVO> countryVOSet = new HashSet<CountryVO>();
		for (Country country : ciGeographycountries) {
			CountryVO countryVO = new CountryVO();
			countryVO.setCountryId(country.getCountryId());
			countryVO.setName(country.getName());
			countryVO.setNumber(country.getNumber());
			countryVO.setIdd(country.getIdd());
			countryVO.setActive(country.getActive());
			countryVO.setA2(country.getA2());
			countryVO.setA3(country.getA3());
			countryVO.setZipRequired(country.getZipRequired());
			countryVOSet.add(countryVO);
		}
		return countryVOSet;
	}

	/**
	 * Resource Mapping Method for populating list of Countries available for
	 * the selected Geography.
	 * @param request
	 * @param response
	 * @return ModelAndView
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@ResourceMapping("populateCountries")
	public ModelAndView populateCountries(ResourceRequest request, ResourceResponse response) throws IOException {

		response.setContentType("text/html; charset=utf-8");
		LocationService<CIGeography> countryLocationService = serviceLocator.getLocationService();
		String geoId = request.getParameter("geoId");
		List<CIGeography> list = countryLocationService.getAllLocation(CIGeography.class, "geographyId", Long.valueOf(geoId));
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("countryList", convertGeographyCountryDotoVO(list.get(0).getCiGeographycountries()));
		return new ModelAndView("countyList", model);
	}

	/**
	 * Resource Mapping Method for populating list of States available for the
	 * selected Country.
	 * @param request
	 * @param response
	 * @return ModelAndView
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@ResourceMapping("populateStates")
	public ModelAndView populateStates(ResourceRequest request, ResourceResponse response) throws IOException {

		response.setContentType("text/html; charset=utf-8");
		LocationService<Region> stateLocationService = serviceLocator.getLocationService();
		String countryId = request.getParameter("countryId");
		List<Region> list = stateLocationService.getAllLocation(Region.class, "countryId", Long.valueOf(countryId));
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("statesList", list);
		return new ModelAndView("StatesList", model);
	}

	/**
	 * Resource Mapping Method for populating list of Cities available for the
	 * selected State.
	 * @param request
	 * @param response
	 * @return ModelAndView
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@ResourceMapping("populateCities")
	public ModelAndView populateCities(ResourceRequest request, ResourceResponse response) throws IOException {

		response.setContentType("text/html; charset=utf-8");
		LocationService<Address> cityLocationService = serviceLocator.getLocationService();
		String stateId = request.getParameter("stateId");
		List<String> cityList = cityLocationService.getAllDistinctAddressLocation(Address.class, "city", "regionId", stateId);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("cityList", cityList);
		return new ModelAndView("CityList", model);
	}

	/**
	 * Resource Mapping Method for populating list of Offices available for the
	 * selected City.
	 * @param request
	 * @param response
	 * @return ModelAndView
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@ResourceMapping("populateOffices")
	public ModelAndView populateOffices(ResourceRequest request, ResourceResponse response) throws IOException {

		response.setContentType("text/html; charset=utf-8");
		LocationService<Address> officeLocationService = serviceLocator.getLocationService();
		String cityName = request.getParameter("cityId");
		List<String> officesList = officeLocationService.getAllDistinctAddressLocation(Address.class, "street3", "city", cityName);
		Map<String, Object> model = new HashMap<String, Object>();
		officesList.removeAll(Collections.singleton(null));
		officesList.removeAll(Collections.singleton(""));
		model.put("officesList", officesList);
		return new ModelAndView("OfficesList", model);
	}

	/**
	 * Resource Mapping Method for populating list of Buildings available for
	 * the selected Office.
	 * @param request
	 * @param response
	 * @return ModelAndView
	 * @throws IOException
	 */
	@SuppressWarnings("unchecked")
	@ResourceMapping("populateBuildings")
	public ModelAndView populateBuildings(ResourceRequest request, ResourceResponse response) throws IOException {

		response.setContentType("text/html; charset=utf-8");
		LocationService<Address> buildingLocationService = serviceLocator.getLocationService();
		String officeName = request.getParameter("officeId");
		List<String> buildingsList = buildingLocationService.getAllDistinctAddressLocation(Address.class, "street2", "street3", officeName);
		Map<String, Object> model = new HashMap<String, Object>();
		buildingsList.removeAll(Collections.singleton(null));
		buildingsList.removeAll(Collections.singleton(""));
		model.put("buildingsList", buildingsList);
		return new ModelAndView("BuildingList", model);
	}

}
