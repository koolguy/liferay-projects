package com.coach.cip.portlets.controller;


import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * Controller class for Home News Portlet.
 * @author GalaxE
 */
@Controller("AllDocumentContentController")
@RequestMapping(value = "VIEW")
public class AllDocumentContentController extends AbstractBaseController {

	/** The Constant logger. */
	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(AllDocumentContentController.class);

	/**
	 * Default Render Mapping Method. this method is invoked.
	 * @param renderResponse
	 * @return string
	 */
	@RenderMapping
	public String showJournalArticles(RenderRequest renderRequest, RenderResponse renderResponse) {

		return Constants.ALLDOCUMENTCONTENT_JSP;
	}
	
	/**
	 * To get vocabulary sent from DocumentContentNavigation portlet
	 * @param eventRequest
	 * @param eventResponse
	 * @param model
	*/
	@EventMapping(value = "{http://coach.com/events}ipc.vocabulory")
	public void showVocabContent(EventRequest eventRequest , EventResponse eventresponse, Model model){
		LoggerUtil.debugLogger(logger, "Start of showVocabContent() method of AllDocumentContentController::");
		Event event = eventRequest.getEvent();
		String categoryId = (String) event.getValue();
		eventresponse.setRenderParameter("categoryId", categoryId);
		LoggerUtil.debugLogger(logger, "End of showVocabContent() method of AllDocumentContentController::");
	}
}
