package com.coach.cip.portlets.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.coach.cip.util.WebUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portlet.asset.model.AssetCategory;

/**
 * The Class CalendarNavigationController, serves the request related to
 * Calendar navigation, like populating the form data.
 * 
 * @author GalaxE.
 */

@Controller("calendarNavigationController")
@RequestMapping(value = "VIEW")
public class CalendarNavigationController extends AbstractBaseController {

	/** The Constant LOG. */
	private static final Log logger = LogFactoryUtil
			.getLog(CalendarNavigationController.class);

	/**
	 * Default Render Mapping Method. If Specific Render mapping is not found,
	 * this mapping is invoked.
	 * 
	 * @param renderResponse
	 * @return
	 */
	@RenderMapping
	public String showCalendarNavigation(RenderResponse renderResponse) {
		LoggerUtil
				.debugLogger(
						logger,
						"Inside default render mapping showCalendarNavigation() method of Calendar Navigation Controller::");
		return Constants.CALENDARNAVIGATION;

	}

	/**
	 * Model Object for displaying Asset Categories.
	 * 
	 * @param portletRequest
	 * @param portletResponse
	 * @return
	 * @throws SystemException
	 */
	@ModelAttribute(value = "assetCategoryMap")
	public Map<Long, List<AssetCategory>> getAssetCategoryMap(
			PortletRequest portletRequest, PortletResponse portletResponse)
			throws SystemException {
		LoggerUtil
				.debugLogger(
						logger,
						"Start of Model getAssetCategoryMap() method of Calendar Navigation Controller::");
		List<AssetCategory> assetCategoryList = WebUtil
				.getAllAssetCategoriesByVocabulary(portletRequest,
						portletResponse, Constants.CALENDARVOCABULARY);
		Map<Long, List<AssetCategory>> assetCategoryMap = new HashMap<Long, List<AssetCategory>>();
		for (AssetCategory assetCategory : assetCategoryList) {
			if (assetCategoryMap.containsKey(assetCategory
					.getParentCategoryId())) {
				assetCategoryMap.get(assetCategory.getParentCategoryId()).add(
						assetCategory);
			} else {
				List<AssetCategory> categoryList = new ArrayList<AssetCategory>();
				categoryList.add(assetCategory);
				assetCategoryMap.put(assetCategory.getParentCategoryId(),
						categoryList);
			}
		}
		LoggerUtil
				.debugLogger(logger,
						"End of Model getAssetCategoryMap() method of Calendar Navigation Controller::");
		return assetCategoryMap;
	}

}
