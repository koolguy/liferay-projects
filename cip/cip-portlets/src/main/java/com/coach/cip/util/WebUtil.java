
package com.coach.cip.util;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.portlet.ActionRequest;
import javax.portlet.PortletPreferences;
import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.swing.text.Document;
import javax.swing.text.EditorKit;
import javax.swing.text.html.HTMLEditorKit;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.springframework.web.util.WebUtils;

import com.coach.cip.audit.util.Attribute;
import com.coach.cip.audit.util.AuditMessageBuilder;
import com.coach.cip.model.entity.CIEmployee;
import com.coach.cip.model.entity.CIPersonalMessage;
import com.coach.cip.model.entity.User;
import com.coach.cip.services.ServiceLocator;
import com.liferay.portal.kernel.audit.AuditMessage;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.FileVersion;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.LocalizationUtil;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.Company;
import com.liferay.portal.model.Contact;
import com.liferay.portal.model.Layout;
import com.liferay.portal.model.Role;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.service.ContactLocalServiceUtil;
import com.liferay.portal.service.LayoutLocalServiceUtil;
import com.liferay.portal.service.ResourcePermissionLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.RoleServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.asset.AssetRendererFactoryRegistryUtil;
import com.liferay.portlet.asset.model.AssetCategory;
import com.liferay.portlet.asset.model.AssetEntry;
import com.liferay.portlet.asset.model.AssetRenderer;
import com.liferay.portlet.asset.model.AssetRendererFactory;
import com.liferay.portlet.asset.model.AssetVocabulary;
import com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetVocabularyServiceUtil;
import com.liferay.portlet.asset.service.persistence.AssetEntryQuery;
import com.liferay.portlet.documentlibrary.model.DLFileEntryConstants;
import com.liferay.portlet.documentlibrary.model.DLFileShortcut;
import com.liferay.portlet.documentlibrary.util.DLUtil;
import com.liferay.portlet.dynamicdatalists.model.DDLRecord;
import com.liferay.portlet.dynamicdatalists.model.DDLRecordSet;
import com.liferay.portlet.dynamicdatalists.model.DDLRecordVersion;
import com.liferay.portlet.dynamicdatalists.service.DDLRecordLocalServiceUtil;
import com.liferay.portlet.dynamicdatalists.service.DDLRecordSetLocalServiceUtil;
import com.liferay.portlet.dynamicdatamapping.model.DDMStructure;
import com.liferay.portlet.dynamicdatamapping.storage.FieldConstants;
import com.liferay.portlet.dynamicdatamapping.storage.Fields;
import com.liferay.portlet.dynamicdatamapping.storage.StorageEngineUtil;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portlet.journal.util.comparator.ArticleDisplayDateComparator;
import com.liferay.portlet.journal.util.comparator.ArticleModifiedDateComparator;
import com.liferay.portlet.journal.util.comparator.ArticleTitleComparator;

/**
 * The Web utility class, has helper methods for web layer.
 * @author GalaxE
 */
public final class WebUtil {

	/** The Constant LOG. */
	private static final Log logger = LogFactoryUtil.getLog(WebUtil.class);

	/** The application context */
	private static ApplicationContext ctx = null;

	/**
	 * Gets the user.
	 * @param portletRequest
	 * @return User
	 */

	public static User getUser(PortletRequest portletRequest) {

		return getUser(PortalUtil.getHttpServletRequest(portletRequest));
	}

	/**
	 * Gets the user list.
	 * @param portletRequest
	 * @return List<User>
	 */
	public static List<User> getUserList(PortletRequest portletRequest) {

		return getUserList(PortalUtil.getHttpServletRequest(portletRequest));
	}

	/**
	 * Gets the employee list.
	 * @param portletRequest
	 * @return List<CIEmployee>
	 */
	public static List<CIEmployee> getEmployeeList(PortletRequest portletRequest) {

		return getEmployeeList(PortalUtil.getHttpServletRequest(portletRequest));
	}

	/**
	 * Gets the user object
	 * @param request
	 * @return User
	 */
	public static User getUser(HttpServletRequest request) {

		LoggerUtil.debugLogger(logger, "Start of getUser() method of WebUtil::");
		User user = null;
		Object userObj = WebUtils.getSessionAttribute(request, Constants.COACH_USER);
		if (userObj == null) {
			long userId = PortalUtil.getUserId(request);
			// As session user object is null so getting from DB
			ServiceLocator serviceLocator = getContext(request).getBean(Constants.SERVICE_LOCATOR, ServiceLocator.class);
			user = serviceLocator.getUserService().findUserByUserId(userId);
			WebUtils.setSessionAttribute(request, Constants.COACH_USER, user);
		}
		else {
			user = (User) userObj;
		}
		LoggerUtil.debugLogger(logger, "End of getUser() method of WebUtil::");
		return user;
	}

	/**
	 * Gets the user list.
	 * @param request
	 * @return List<User>
	 */
	@SuppressWarnings("unchecked")
	public static List<User> getUserList(HttpServletRequest request) {

		LoggerUtil.debugLogger(logger, "Start of getUserList() method of WebUtil::");
		List<User> userList = null;
		Object userListObj = WebUtils.getSessionAttribute(request, Constants.COACH_USER_LIST);
		if (userListObj == null) {

			// As session user list object is null so getting from DB
			ServiceLocator serviceLocator = getContext(request).getBean(Constants.SERVICE_LOCATOR, ServiceLocator.class);
			userList = serviceLocator.getUserService().getUsers();
			WebUtils.setSessionAttribute(request, Constants.COACH_USER_LIST, userList);
		}
		else {
			userList = (List<User>) userListObj;
		}
		LoggerUtil.debugLogger(logger, "End of getUserList() method of WebUtil::");
		return userList;
	}

	/**
	 * Gets the employee list.
	 * @param request
	 * @return List<CIEmployee>
	 */
	@SuppressWarnings("unchecked")
	public static List<CIEmployee> getEmployeeList(HttpServletRequest request) {

		LoggerUtil.debugLogger(logger, "Start of getEmployeeList() method of WebUtil::");
		List<CIEmployee> employeeList = null;
		Object employeeListObj = WebUtils.getSessionAttribute(request, Constants.COACH_EMPLOYEE_LIST);
		if (employeeListObj == null) {
			// As session employee list object is null so getting from DB
			ServiceLocator serviceLocator = getContext(request).getBean(Constants.SERVICE_LOCATOR, ServiceLocator.class);
			employeeList = serviceLocator.getEmployeeService().getEmployees();
			WebUtils.setSessionAttribute(request, Constants.COACH_EMPLOYEE_LIST, employeeList);
		}
		else {
			employeeList = (List<CIEmployee>) employeeListObj;
		}
		LoggerUtil.debugLogger(logger, "End of getEmployeeList() method of WebUtil::");
		return employeeList;
	}

	/**
	 * Gets the Application context
	 * @param request
	 * @return ApplicationContext
	 */
	public static ApplicationContext getContext(HttpServletRequest request) {

		LoggerUtil.debugLogger(logger, "Start of getContext() method of WebUtil::");
		if (ctx == null) {
			ServletContext servletContext = request.getSession().getServletContext();
			ctx = WebApplicationContextUtils.getWebApplicationContext(servletContext);
		}
		LoggerUtil.debugLogger(logger, "End of getContext() method of WebUtil::");
		return ctx;
	}

	/**
	 * Returns the company associated with the current portal instance.
	 * @return Company
	 * @throws PortalException
	 * @throws SystemException
	 */
	@Deprecated
	public static Company getCompany() throws PortalException, SystemException {

		return PortalUtil.getCompany(getPortletRequest());
	}

	/**
	 * Gets the path context
	 * @return Path context string
	 * @throws PortalException
	 * @throws SystemException
	 */
	public static String getPathContext() throws PortalException, SystemException {

		return PortalUtil.getPathContext();
	}

	/**
	 * Returns the company Id associated with the current portal instance.
	 * @return Company Id
	 * @throws PortalException
	 * @throws SystemException
	 */
	@Deprecated
	public static long getCompanyId() throws PortalException, SystemException {

		return PortalUtil.getCompanyId(getPortletRequest());
	}

	/**
	 * Gets the Contact object.
	 * @param actionRequest
	 * @return Contact
	 * @throws PortalException
	 * @throws SystemException
	 */
	public static Contact getContact(ActionRequest actionRequest) throws PortalException, SystemException {

		return ContactLocalServiceUtil.getContact(getContactId(actionRequest));
	}

	/**
	 * Returns the contact Id associated with the current portal instance.
	 * @param actionRequest
	 * @return Contact Id
	 * @throws PortalException
	 * @throws SystemException
	 */
	public static long getContactId(ActionRequest actionRequest) throws PortalException, SystemException {

		return getLoggedInUser(actionRequest).getContactId();
	}

	/**
	 * Takes PortletRequest object and returns the HttpServletRequest object.
	 * @param actionRequest
	 * @return HttpServletRequest
	 */
	public static HttpServletRequest getHttpServletRequest(PortletRequest actionRequest) {

		HttpServletRequest convertReq = PortalUtil.getHttpServletRequest(actionRequest);
		HttpServletRequest originalReq = PortalUtil.getOriginalServletRequest(convertReq);
		return originalReq;
	}

	/**
	 * Gets the liferay user.
	 * @return User
	 * @throws PortalException
	 * @throws SystemException
	 */
	@Deprecated
	public static User getLiferayUser() throws PortalException, SystemException {

		return null;
	}

	/**
	 * Returns the currently logged In User.
	 * @param actionRequest
	 * @return the logged in user
	 * @throws PortalException
	 * @throws SystemException
	 */
	@Deprecated
	public static com.liferay.portal.model.User getLoggedInUser(ActionRequest actionRequest) throws PortalException, SystemException {

		return PortalUtil.getUser(actionRequest);
	}

	/**
	 * Returns the currently logged in user's email address.
	 * @param actionRequest
	 * @return Email address string
	 * @throws PortalException
	 * @throws SystemException
	 */
	@Deprecated
	public static String getLoggedInUserEmailAddress(ActionRequest actionRequest) throws PortalException, SystemException {

		return getLoggedInUser(actionRequest).getDisplayEmailAddress();
	}

	/**
	 * Gets the path friendly url private group.
	 * @param actionRequest
	 * @return Path friendly url private group string
	 */
	public static String getPathFriendlyURLPrivateGroup(ActionRequest actionRequest) {

		return getThemeDisplay(actionRequest).getPathFriendlyURLPrivateGroup();
	}

	/**
	 * Gets the path friendly url private user.
	 * @return Path friendly url private user string
	 */
	public static String getPathFriendlyURLPrivateUser() {

		return PortalUtil.getPathFriendlyURLPrivateUser();
	}

	/**
	 * Gets the path friendly url public.
	 * @return Path friendly url public string
	 */
	public static String getPathFriendlyURLPublic() {

		return PortalUtil.getPathFriendlyURLPublic();
	}

	/**
	 * Returns the portal url
	 * @param actionRequest
	 * @return Url string
	 */
	public static String getPortalURL(ActionRequest actionRequest) {

		return getThemeDisplay(actionRequest).getPortalURL();
	}

	/**
	 * Returns the portlet preferences object
	 * @return PortletPreferences
	 */
	@Deprecated
	public static PortletPreferences getPortletPrefs() {

		return getPortletRequest().getPreferences();
	}

	/**
	 * Returns the portlet request object
	 * @return PortletRequest
	 */
	@Deprecated
	public static PortletRequest getPortletRequest() {

		return null;
	}

	/**
	 * Returns the portlet response object
	 * @return PortletResponse
	 */
	@Deprecated
	public static PortletResponse getPortletResponse() {

		return null;
	}

	/**
	 * Returns the servlet context object.
	 * @return ServletContext
	 */
	@Deprecated
	public static ServletContext getServletContext() {

		return null;
	}

	/**
	 * Returns the ThemeDisplay object
	 * @param actionRequest
	 * @return ThemeDisplay
	 */
	public static ThemeDisplay getThemeDisplay(PortletRequest actionRequest) {

		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(com.liferay.portal.kernel.util.WebKeys.THEME_DISPLAY);
		return themeDisplay;
	}

	/**
	 * Returns the user id
	 * @param portletRequest
	 * @return User id
	 */
	public static long getUserId(PortletRequest portletRequest) {

		return PortalUtil.getUserId(portletRequest);
	}

	/**
	 * Method redirects to the given URL.
	 * @param url
	 * @throws IOException
	 */
	public static void redirect(final String url) throws IOException {

	}

	/**
	 * Default Constructor
	 */

	private WebUtil() {

	}

	/**
	 * Sets the session attribute.
	 * @param renderRequest
	 * @param name
	 * @param value
	 */
	public static void setSessionAttribute(PortletRequest portletRequest, String name, String value) {

		HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(portletRequest);
		WebUtils.setSessionAttribute(httpRequest, name, value);
	}

	/**
	 * Gets the session attribute.
	 * @param actionRequest
	 * @param name
	 * @return Session attribute object
	 */
	public static Object getSessionAttribute(ActionRequest actionRequest, String name) {

		HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(actionRequest);
		return WebUtils.getSessionAttribute(httpRequest, name);
	}

	/**
	 * Gets the session attribute.
	 * @param portletRequest
	 * @param name
	 * @return Session attribute object
	 */
	public static Object getSessionAttribute(PortletRequest portletRequest, String name) {

		HttpServletRequest httpRequest = PortalUtil.getHttpServletRequest(portletRequest);
		return WebUtils.getSessionAttribute(httpRequest, name);
	}

	/**
	 * Returns the list of personal messages
	 * @param portletRequest
	 * @return List<CIPersonalMessage>
	 */
	public static List<CIPersonalMessage> getPersonalMessageList(PortletRequest portletRequest) {

		return getPersonalMessageList(PortalUtil.getHttpServletRequest(portletRequest));
	}

	/**
	 * Returns the list of personal messages
	 * @param request
	 * @return List<CIPersonalMessage>
	 */
	@SuppressWarnings("unchecked")
	public static List<CIPersonalMessage> getPersonalMessageList(HttpServletRequest request) {

		LoggerUtil.debugLogger(logger, "Start of getPersonalMessageList() method of WebUtil::");
		List<CIPersonalMessage> personalMessageList = null;
		Object personalMessageListObj = WebUtils.getSessionAttribute(request, Constants.PERSONAL_MESSAGE_LIST);
		LoggerUtil.debugLogger(logger, "Retrieved Caoch PERSONAL_MESSAGE_LIST  from session:" + personalMessageListObj);
		if (personalMessageListObj == null) {
			// As session personalMessageListObj is null so getting from DB
			ServiceLocator serviceLocator = getContext(request).getBean(Constants.SERVICE_LOCATOR, ServiceLocator.class);
			personalMessageList = serviceLocator.getPersonalMessageService().getAllPersonalMessage();
			WebUtils.setSessionAttribute(request, Constants.PERSONAL_MESSAGE_LIST, personalMessageList);
		}
		else {
			personalMessageList = (List<CIPersonalMessage>) personalMessageListObj;
		}
		LoggerUtil.debugLogger(logger, "End of getPersonalMessageList() method of WebUtil::");
		return personalMessageList;
	}

	/**
	 * Returns personal message object
	 * @param portletRequest
	 * @return CIPersonalMessage
	 */
	public static CIPersonalMessage getPersonalMessage(PortletRequest portletRequest) {

		return getPersonalMessage(PortalUtil.getHttpServletRequest(portletRequest));
	}

	/**
	 * Returns personal message object
	 * @param request
	 * @return CIPersonalMessage
	 */
	public static CIPersonalMessage getPersonalMessage(HttpServletRequest request) {

		LoggerUtil.debugLogger(logger, "Start of getPersonalMessage() method of WebUtil::");
		CIPersonalMessage personalMessage = null;
		Object personalMessageObj = WebUtils.getSessionAttribute(request, Constants.PERSONAL_MESSAGE);
		LoggerUtil.debugLogger(logger, "Retrieved Caoch PERSONAL_MESSAGE  from session:" + personalMessageObj);
		if (personalMessageObj == null) {
			// As session personalMessageListObj is null so getting from DB
			ServiceLocator serviceLocator = getContext(request).getBean(Constants.SERVICE_LOCATOR, ServiceLocator.class);
			personalMessage = serviceLocator.getPersonalMessageService().getPersonalMessage(Long.parseLong(request.getParameter("messageId")));
			WebUtils.setSessionAttribute(request, Constants.PERSONAL_MESSAGE, personalMessage);
		}
		else {
			personalMessage = (CIPersonalMessage) personalMessageObj;
		}
		LoggerUtil.debugLogger(logger, "End of getPersonalMessage() method of WebUtil::");
		return personalMessage;
	}

	/**
	 * Returns the feedback topics list.
	 * @param renderRequest
	 * @return List<String[]>
	 */
	/*public static List<String[]> getFeedbackTopic(PortletRequest renderRequest) {

		LoggerUtil.debugLogger(logger, "Start of getFeedbackTopic() method of WebUtil::");
		List<String[]> lstTopics = new ArrayList<String[]>();
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			List<DDLRecordSet> ddlRecordSet = DDLRecordSetLocalServiceUtil.getRecordSets(themeDisplay.getScopeGroupId());
			for (DDLRecordSet recordSet : ddlRecordSet) {
				if (recordSet.getName().contains(Constants.SHARE_YOUR_FEEDBACK_TOPIC)) {
					DDMStructure ddmStructure = recordSet.getDDMStructure();
					String languageId = LanguageUtil.getLanguageId(renderRequest);
					Map<String, Map<String, String>> fieldsMap = ddmStructure.getFieldsMap(languageId);
					List<DDLRecord> ddlRecords = DDLRecordLocalServiceUtil.getRecords(recordSet.getRecordSetId());
					for (DDLRecord ddlRecord : ddlRecords) {
						DDLRecordVersion recordVersion = ddlRecord.getRecordVersion();
						Fields fieldsModel = StorageEngineUtil.getFields(recordVersion.getDDMStorageId());
						// Columns
						String[] fieldMapValues = new String[fieldsMap.values().size()];
						int counter = 0;
						for (Map<String, String> fields : fieldsMap.values()) {
							String name = fields.get(FieldConstants.NAME);
							String value = null;
							if (fieldsModel.contains(name)) {
								com.liferay.portlet.dynamicdatamapping.storage.Field field = fieldsModel.get(name);
								value = field.getRenderedValue(themeDisplay);
							}
							else {
								value = StringPool.BLANK;
							}
							fieldMapValues[counter] = value;
							counter++;
						}
						lstTopics.add(fieldMapValues);
					}
				}
			}
		}
		catch (Exception e) {
			LoggerUtil.errorLogger(logger, "Exception occured while getting the feedback topics list: " + e.getMessage(), e);
		}
		LoggerUtil.debugLogger(logger, "End of getFeedbackTopic() method of WebUtil::");
		return lstTopics;
	}*/

	/**
	 * Method used to convert html to Text.
	 * @param htmText
	 * @return plainText string
	 */
	public static String convertHtmlToString(String htmText) {

		LoggerUtil.debugLogger(logger, "Start of convertHtmlToString() method of WebUtil::");
		EditorKit kit = new HTMLEditorKit();
		Document doc = kit.createDefaultDocument();
		doc.putProperty("IgnoreCharsetDirective", Boolean.TRUE);
		String plainText = "";
		try {
			Reader reader = new StringReader(htmText);
			kit.read(reader, doc, 0);
			plainText = doc.getText(0, doc.getLength());
		}
		catch (Exception e) {
			LoggerUtil.errorLogger(logger, "Exception occured while converting html to string : " + e.getMessage(), e);
		}
		LoggerUtil.debugLogger(logger, "End of convertHtmlToString() method of WebUtil::");
		return plainText;
	}

	/**
	 * Method used to call web service
	 * @throws SOAPException
	 */
	@SuppressWarnings("restriction")
	public static void callWebService() throws SOAPException {

		LoggerUtil.debugLogger(logger, "Start of callWebService() method of WebUtil::");
		SOAPMessage soapMessage = MessageFactory.newInstance().createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();
		SOAPEnvelope soapEnvelope = soapPart.getEnvelope();
		SOAPHeader soapHeader = soapEnvelope.getHeader();
		SOAPElement security =
			soapHeader.addChildElement("Security", "wsse", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");
		SOAPElement usernameToken = security.addChildElement("UsernameToken", "wsse");
		usernameToken.addAttribute(new QName("xmlns:wsu"), "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
		SOAPElement username = usernameToken.addChildElement("Username", "wsse");
		username.addTextNode("SVCEcvision");
		SOAPElement password = usernameToken.addChildElement("Password", "wsse");
		password.setAttribute("Type", "http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
		password.addTextNode("t1m34bbQ!#");
		SOAPBody soapBody = soapEnvelope.getBody();
		soapBody.addAttribute(soapEnvelope.createName("id", "xsi", "http://www.w3.org/2001/XMLSchema-instance"), "Body");
		soapBody.addAttribute(soapEnvelope.createName("id", "xsd", "http://www.w3.org/2001/XMLSchema"), "Body");
		Name bodyName = soapEnvelope.createName("getEmployeesByDepartmentRequest", "", "http://coach.com/esb/employeeWebService/v1.0");
		SOAPBodyElement gltp = soapBody.addBodyElement(bodyName);
		Name childName = soapEnvelope.createName("getDepartmentRequesst", "", "http://coach.com/esb/employeeWebService/v1.0");
		SOAPElement childElement = gltp.addChildElement(childName);
		Name departmentName = soapEnvelope.createName("Department");
		SOAPElement childChildElement = childElement.addChildElement(departmentName);
		Name stateName = soapEnvelope.createName("STATE");
		SOAPElement stateElement = childChildElement.addChildElement(stateName);
		stateElement.setValue("NY");
		try {
			soapMessage.writeTo(System.out);
		}
		catch (IOException e) {
			LoggerUtil.errorLogger(logger, "Exception occured while calling web service : " + e.getMessage(), e);
		}
		LoggerUtil.debugLogger(logger, "End of callWebService() method of WebUtil::");
	}

	/**
	 * Returns root asset categories of given Vocabulary name.
	 * @param request
	 * @param response
	 * @param vocabularyName
	 * @return List<AssetCategory>
	 * @throws SystemException
	 */
	public static List<AssetCategory> getAssetCategoryByVocabulary(PortletRequest request, PortletResponse response, String vocabularyName) throws SystemException {

		LoggerUtil.debugLogger(logger, "Start of getAssetCategoryByVocabulary() method of WebUtil::");
		ThemeDisplay themeDisplay = getThemeDisplay(request);
		List<AssetVocabulary> vocabularies = null;
		List<AssetCategory> assetCategoryList = null;
		vocabularies = AssetVocabularyServiceUtil.getGroupVocabularies(themeDisplay.getLayout().getGroupId(), vocabularyName, -1, -1, null);
		if (vocabularies != null && vocabularies.size() > 0) {
			// get the first and only vocabulary
			AssetVocabulary vocabulary = vocabularies.get(0);
			assetCategoryList = AssetCategoryLocalServiceUtil.getVocabularyRootCategories(vocabulary.getVocabularyId(), -1, -1, null);
		}
		LoggerUtil.debugLogger(logger, "End of getAssetCategoryByVocabulary() method of WebUtil::");
		return assetCategoryList;
	}

	/**
	 * Removes duplicate strings and returns list of unique strings
	 * @param list
	 * @return List<String>
	 */
	public static List<String> removeDuplicates(List<String> list) {

		LoggerUtil.debugLogger(logger, "Start of removeDuplicates() method of WebUtil::");
		Set<String> set = new HashSet<String>();
		set.addAll(list);
		list = new ArrayList<String>();
		list.addAll(set);
		LoggerUtil.debugLogger(logger, "End of removeDuplicates() method of WebUtil::");
		return list;
	}

	/**
	 * Returns the list of JournalArticles based on Article type
	 * @param companyId
	 * @param groupId
	 * @param type
	 * @param limit
	 * @return List<JournalArticle>
	 * @throws PortalException
	 * @throws SystemException
	 */
	public static List<JournalArticle> getArticlesByType(long companyId, long groupId, String type, int limit) throws PortalException, SystemException {

		LoggerUtil.debugLogger(logger, "Start of getArticlesByType() method of WebUtil::");
		List<JournalArticle> results = Collections.synchronizedList(new ArrayList<JournalArticle>());
		String articleId = null;
		Double version = null;
		String title = null;
		String description = null;
		String content = null;
		String[] structureIds = null;
		String[] templateIds = null;
		Date displayDateGT = null;
		Date displayDateLT = null;
		Date reviewDate = null;
		boolean andOperator = true;
		int start = 0;
		int end = limit;
		String orderByType = "";
		long classNameId = 0;
		int status = 0;
		boolean orderByAsc = orderByType.equals("asc");
		OrderByComparator obc = new ArticleDisplayDateComparator(orderByAsc);
		try {
			results =
				JournalArticleLocalServiceUtil.search(
					companyId, groupId, classNameId, articleId, version, title, description, content, type, structureIds, templateIds, displayDateGT,
					displayDateLT, status, reviewDate, andOperator, start, end, obc);
		}
		catch (Exception e) {

			LoggerUtil.errorLogger(logger, "Exception occured on search of Article", e);

		}
		results = filterByCurrentDate(results);
		LoggerUtil.debugLogger(logger, "End of getArticlesByType() method of WebUtil::");
		return results;
	}

	/**
	 * Method returns articles list based on categories and display year.
	 * @param request
	 * @param response
	 * @param year
	 * @param categoryArr
	 * @return List<JournalArticle>
	 * @throws PortalException
	 * @throws SystemException
	 */

	public static List<JournalArticle> getArticlesByCategoriesNYear(PortletRequest request, PortletResponse response, String year, AssetCategory... categoryArr) throws PortalException, SystemException {

		LoggerUtil.debugLogger(logger, "Start of getArticlesByCategoriesNYear() method of WebUtil::");
		List<JournalArticle> articlesList = null;
		List<JournalArticle> articlesListByCategories = null;
		List<JournalArticle> articlesListByCategoriesNYear = null;
		articlesList = JournalArticleLocalServiceUtil.getArticles();
		if (articlesList != null && articlesList.size() != 0) {
			articlesListByCategories = getArticlesByCategories(request, response, articlesList, categoryArr);
		}
		if (articlesListByCategories != null && articlesListByCategories.size() != 0) {
			articlesListByCategoriesNYear = getArticlesByYear(articlesListByCategories, year);
		}
		LoggerUtil.debugLogger(logger, "End of getArticlesByCategoriesNYear() method of WebUtil::");
		return articlesListByCategoriesNYear;
	}

	/**
	 * Method returns articles list based on categories.
	 * @param request
	 * @param response
	 * @param articlesList
	 * @param categoryIdArr
	 * @return List<JournalArticle>
	 * @throws PortalException
	 * @throws SystemException
	 */
	@SuppressWarnings("unchecked")
	public static List<JournalArticle> getArticlesByCategories(PortletRequest request, PortletResponse response, List<JournalArticle> articlesList, AssetCategory... categoryArr) throws PortalException, SystemException {

		LoggerUtil.debugLogger(logger, "Start of getArticlesByCategories() method of WebUtil::");
		String orderByType = "";
		boolean ORDER_BY_DESC = orderByType.equals("DESC");
		OrderByComparator obc = new ArticleDisplayDateComparator(ORDER_BY_DESC);
		AssetEntryQuery assetEntryQuery = new AssetEntryQuery();
		List<AssetEntry> assetEntriesList = null;
		long[] categoryIdArr = null;
		List<JournalArticle> articlesListByCategories = new ArrayList<JournalArticle>();
		long[] classNameIds = {
			PortalUtil.getClassNameId(JournalArticle.class)
		};
		if (categoryArr != null && categoryArr.length != 0) {
			categoryIdArr = new long[categoryArr.length];
			for (int i = 0; i < categoryArr.length; i++) {
				categoryIdArr[i] = categoryArr[i].getCategoryId();
			}
		}
		ThemeDisplay themeDisplay =  getThemeDisplay(request);
		long[] groupIds = {themeDisplay.getScopeGroupId()};
		if (categoryIdArr != null && categoryArr.length != 0) {
			assetEntryQuery.setClassNameIds(classNameIds);
			assetEntryQuery.setAnyCategoryIds(categoryIdArr);
			assetEntryQuery.setGroupIds(groupIds);
			assetEntriesList = AssetEntryLocalServiceUtil.getEntries(assetEntryQuery);
		}
		if (assetEntriesList != null && assetEntriesList.size() != 0) {
			PermissionChecker permissionChecker = WebUtil.getPermissionChecker(request);
			for (AssetEntry assetEntry : assetEntriesList) {
				AssetRendererFactory assetRendererFact = AssetRendererFactoryRegistryUtil.getAssetRendererFactoryByClassName(JournalArticle.class.getName());
				AssetRenderer assetRender = assetRendererFact.getAssetRenderer(assetEntry.getClassPK());
				if(assetRender.hasViewPermission(permissionChecker) ){
					articlesListByCategories.add(JournalArticleLocalServiceUtil.getLatestArticle(assetEntry.getClassPK()));
				}
			}
		}
		Collections.sort(articlesListByCategories, obc);
		articlesListByCategories = filterByCurrentDate(articlesListByCategories);
		LoggerUtil.debugLogger(logger, "End of getArticlesByCategories() method of WebUtil::");
		return articlesListByCategories;
	}

	/**
	 * Method returns latest articles list based on categories.
	 * @param request
	 * @param response
	 * @param articlesList
	 * @param categoryIdArr
	 * @return List<JournalArticle>
	 * @throws PortalException
	 * @throws SystemException
	 */
	@SuppressWarnings("unchecked")
	public static List<JournalArticle> getArticlesLatestByCategories(PortletRequest request, PortletResponse response, List<JournalArticle> articlesList, AssetCategory... categoryArr) throws PortalException, SystemException {

		LoggerUtil.debugLogger(logger, "Start of getArticlesByCategories() method of WebUtil::");
		String orderByType = "ASC";
		boolean ORDER_BY_ASC = orderByType.equals("ASC");
		OrderByComparator obc = new ArticleModifiedDateComparator(ORDER_BY_ASC);
		AssetEntryQuery assetEntryQuery = new AssetEntryQuery();
		List<AssetEntry> assetEntriesList = null;
		long[] categoryIdArr = null;
		List<JournalArticle> articlesListByCategories = new ArrayList<JournalArticle>();
		long[] classNameIds = {
			PortalUtil.getClassNameId(JournalArticle.class)
		};
		if (categoryArr != null && categoryArr.length != 0) {
			categoryIdArr = new long[categoryArr.length];
			for (int i = 0; i < categoryArr.length; i++) {
				categoryIdArr[i] = categoryArr[i].getCategoryId();
			}
		}
		ThemeDisplay themeDisplay =  getThemeDisplay(request);
		long[] groupIds = {themeDisplay.getScopeGroupId()};
		if (categoryIdArr != null && categoryArr.length != 0) {
			assetEntryQuery.setClassNameIds(classNameIds);
			assetEntryQuery.setAnyCategoryIds(categoryIdArr);
			assetEntryQuery.setGroupIds(groupIds);
			assetEntriesList = AssetEntryLocalServiceUtil.getEntries(assetEntryQuery);
		}
		if (assetEntriesList != null && assetEntriesList.size() != 0) {
			PermissionChecker permissionChecker = WebUtil.getPermissionChecker(request);
			for (AssetEntry assetEntry : assetEntriesList) {
				AssetRendererFactory assetRendererFact = AssetRendererFactoryRegistryUtil.getAssetRendererFactoryByClassName(JournalArticle.class.getName());
				AssetRenderer assetRender = assetRendererFact.getAssetRenderer(assetEntry.getClassPK());
				if(assetRender.hasViewPermission(permissionChecker) ){
					articlesListByCategories.add(JournalArticleLocalServiceUtil.getLatestArticle(assetEntry.getClassPK()));
				}
			}
		}
		Collections.sort(articlesListByCategories, obc);
		articlesListByCategories = filterByCurrentDate(articlesListByCategories);
		LoggerUtil.debugLogger(logger, "End of getArticlesByCategories() method of WebUtil::");
		return articlesListByCategories;
	}
	
	/**
	 * Method takes the list of articles and returns the list of articles which
	 * have display time on or before current time
	 * @param journalArticlesList
	 * @return List<JournalArticle>
	 */
	public static List<JournalArticle> filterByCurrentDate(List<JournalArticle> journalArticlesList) {

		LoggerUtil.debugLogger(logger, "Start of filterByCurrentDate() method of WebUtil::");
		Date currDate = new Date();
		List<JournalArticle> listByCurrDate = new ArrayList<JournalArticle>();
		for (JournalArticle journalArticle : journalArticlesList) {
			Date articleDate = journalArticle.getDisplayDate();
			if ((DateUtil.compareTo(articleDate, currDate) <= 0)) {
				listByCurrDate.add(journalArticle);
			}
		}
		LoggerUtil.debugLogger(logger, "End of filterByCurrentDate() method of WebUtil::");
		return listByCurrDate;
	}

	/**
	 * Method takes the list of articles and year string. Returns the list of
	 * articles which have display year same as given year.
	 * @param articlesList
	 * @param year
	 * @return List<JournalArticle>
	 */
	public static List<JournalArticle> getArticlesByYear(List<JournalArticle> journalArticlesList, String year) {

		LoggerUtil.debugLogger(logger, "Start of getArticlesByYear() method of WebUtil::");
		List<JournalArticle> articlesListByYear = new ArrayList<JournalArticle>();
		DateFormat df = new SimpleDateFormat("yyyy");
		String displayYear = "";
		for (JournalArticle journalArticle : journalArticlesList) {
			displayYear = df.format(journalArticle.getDisplayDate());
			if (displayYear.equals(year)) {
				articlesListByYear.add(journalArticle);
			}
		}
		LoggerUtil.debugLogger(logger, "End of getArticlesByYear() method of WebUtil::");
		return articlesListByYear;
	}

	/**
	 * Method takes list of Journal Articles and creates the set of Resource
	 * Primary keys of journal articles. Even for a journal article having
	 * multiple versions, only one r.p. key will be stored in set as multiple
	 * versions of same article have same resource primary key. Then based on
	 * set values latest journal articles list is created and returned.
	 * @param articlesList
	 * @return List<JournalArticle>
	 * @throws PortalException
	 * @throws SystemException
	 */

	public static List<JournalArticle> getLatestArticles(List<JournalArticle> articlesList) throws PortalException, SystemException {

		LoggerUtil.debugLogger(logger, "Start of getLatestArticles() method of WebUtil::");
		List<JournalArticle> latestArticlesList = new ArrayList<JournalArticle>();
		Set<Long> articleResPrimKeySet = new HashSet<Long>();
		for (JournalArticle journalArticle : latestArticlesList) {
			articleResPrimKeySet.add(journalArticle.getResourcePrimKey());
		}
		for (Long resourcePrimKey : articleResPrimKeySet) {
			latestArticlesList.add(JournalArticleLocalServiceUtil.getLatestArticle(resourcePrimKey));
		}
		LoggerUtil.debugLogger(logger, "End of getLatestArticles() method of WebUtil::");
		return latestArticlesList;
	}

	/**
	 * Returns the list of JournalArticles based on Article type
	 * @param type
	 * @return List<JournalArticle>
	 * @throws PortalException
	 * @throws SystemException
	 */
	public static List<JournalArticle> getArticlesByType(String type) throws PortalException, SystemException {

		LoggerUtil.debugLogger(logger, "Start of getArticlesByType() method of WebUtil::");
		List<JournalArticle> journalArticleList = null;
		List<JournalArticle> journalArticleListByType = new ArrayList<JournalArticle>();
		try {
			journalArticleList = JournalArticleLocalServiceUtil.getArticles();
		}
		catch (SystemException e) {
			LoggerUtil.errorLogger(logger, "Error occured while getting the artciles by type", e);
		}
		for (JournalArticle journalArticle : journalArticleList) {
			if (journalArticle.getType().equalsIgnoreCase(type)) {
				journalArticleListByType.add(journalArticle);
			}
		}
		LoggerUtil.debugLogger(logger, "End of getArticlesByType() method of WebUtil::");
		return getLatestArticles(journalArticleListByType);
	}

	/**
	 * Returns the list of Asset Entries for the given Asset Category.
	 * @param assetCategory
	 * @param portletRequest
	 * @return List<AssetEntry>
	 */
	public static List<AssetEntry> fetchAssetEntries(AssetCategory assetCategory, PortletRequest portletRequest) {

		LoggerUtil.debugLogger(logger, "Start of fetchAssetEntries(AssetCategory,PortletRequest) method of WebUtil::");
		List<AssetEntry> assetEntryList = new ArrayList<AssetEntry>();
		long[] allCateogyIds = {
			assetCategory.getCategoryId()
		};
		long[] classNameIds =
			{
				PortalUtil.getClassNameId(DLFileEntryConstants.getClassName()), PortalUtil.getClassNameId(DLFileShortcut.class.getName()),
				PortalUtil.getClassNameId(JournalArticle.class)
			};
		AssetEntryQuery assetEntryQuery = new AssetEntryQuery();
		assetEntryQuery.setClassNameIds(classNameIds);
		assetEntryQuery.setStart(0);
		assetEntryQuery.setEnd(50);
		assetEntryQuery.setAllCategoryIds(allCateogyIds);
		try {
			List<AssetEntry> allAssetEntryList = AssetEntryLocalServiceUtil.getEntries(assetEntryQuery);
			for (AssetEntry assetEntry : allAssetEntryList) {
				AssetRendererFactory assetRendererFactory =
					AssetRendererFactoryRegistryUtil.getAssetRendererFactoryByClassName(assetEntry.getClassName());
				AssetRenderer assetRenderer = assetRendererFactory.getAssetRenderer(assetEntry.getClassPK());
				List<AssetCategory> categoryList = assetEntry.getCategories();
				for (AssetCategory category : categoryList) {
					if (category.getCategoryId() == allCateogyIds[0] && assetRenderer.hasViewPermission(getPermissionChecker(portletRequest)) &&
						!category.getName().equalsIgnoreCase(cropTitle(assetEntry.getTitleCurrentValue()))) {
						assetEntryList.add(assetEntry);
					}
				}
			}
		}
		catch (SystemException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.fetchAssetEntries(AssetCategory,PortletRequest) SystemException :::", e);
		}
		catch (Exception e) {
			LoggerUtil.errorLogger(logger, "WebUtil.fetchAssetEntries(AssetCategory,PortletRequest) Exception :::", e);
		}
		LoggerUtil.debugLogger(logger, "End of fetchAssetEntries(AssetCategory,PortletRequest) method of WebUtil::");
		return assetEntryList;
	}

	/**
	 * Returns the list of Asset Entries for the given Asset Category.
	 * @param assetCategory
	 * @param Set
	 *            <AssetEntry>
	 * @return List<AssetEntry>
	 */
	public static List<AssetEntry> fetchAssetEntries(AssetCategory assetCategory, Set<AssetEntry> allAssetEntryList) {

		LoggerUtil.debugLogger(logger, "Start of fetchAssetEntries(AssetCategory,Set<AssetEntry>) method of WebUtil::");
		List<AssetEntry> assetEntryList = new ArrayList<AssetEntry>();
		if (Validator.isNotNull(allAssetEntryList) && allAssetEntryList.size() > 0) {/*
			try {
				for (AssetEntry assetEntry : allAssetEntryList) {
					List<AssetCategory> categoryList = assetEntry.getCategories();
					if (Validator.isNotNull(categoryList) && categoryList.size() > 0) {
						for (AssetCategory category : categoryList) {
							if (category.getCategoryId() == assetCategory.getCategoryId() &&
								!category.getName().equalsIgnoreCase(cropTitle(assetEntry.getTitleCurrentValue()))) {
								assetEntryList.add(assetEntry);
							}
						}
					}
				}
			}
			catch (SystemException e) {
				LoggerUtil.errorLogger(logger, "WebUtil.fetchAssetEntries(AssetCategory,Set<AssetEntry>) SystemException :::", e);
			}
			catch (Exception e) {
				LoggerUtil.errorLogger(logger, "WebUtil.fetchAssetEntries(AssetCategory,Set<AssetEntry>) Exception :::", e);
			}
		*/
			
			assetEntryList = new ArrayList<AssetEntry>(allAssetEntryList);
		
		
		}
		LoggerUtil.debugLogger(logger, "End of fetchAssetEntries(AssetCategory,Set<AssetEntry>) method of WebUtil::");
		return assetEntryList;
	}

	/**
	 * Returns the Asset Entry URL.
	 * @param classPK
	 * @param renderRequest
	 * @param renderResponse
	 * @param themeDisplay
	 * @return Url String
	 */
	public static String displayFileURL(long classPK, RenderRequest renderRequest, RenderResponse renderResponse, ThemeDisplay themeDisplay) {

		LoggerUtil.debugLogger(logger, "Start of displayFileURL() method of WebUtil::");
		String fileURL = "";
		try {
			FileEntry fileEntry = getFileEntry(classPK, renderRequest, renderResponse);
			fileURL = DLUtil.getPreviewURL(fileEntry, fileEntry.getFileVersion(), themeDisplay, StringPool.BLANK);
		}
		catch (SystemException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.displayFileURL() SystemException :::", e);
		}
		catch (Exception e) {
			LoggerUtil.errorLogger(logger, "WebUtil.displayFileURL() Exception :::", e);
		}
		LoggerUtil.debugLogger(logger, "End of displayFileURL() method of WebUtil::");
		return fileURL;
	}

	/**
	 * Returns the FileEntry Object based on ClassPK.
	 * @param classPK
	 * @param renderRequest
	 * @param renderResponse
	 * @return FileEntry
	 * @throws Exception
	 */
	public static FileEntry getFileEntry(long classPK, RenderRequest renderRequest, RenderResponse renderResponse) throws Exception {

		LoggerUtil.debugLogger(logger, "Start of getFileEntry() method of WebUtil::");
		AssetRendererFactory assetRendererFactory =
			AssetRendererFactoryRegistryUtil.getAssetRendererFactoryByClassName(DLFileEntryConstants.getClassName());
		FileEntry fileEntry = null;
		AssetRenderer assetRenderer;
		try {
			assetRenderer = assetRendererFactory.getAssetRenderer(classPK);
			assetRenderer.render(renderRequest, renderResponse, AssetRenderer.TEMPLATE_FULL_CONTENT);
			FileVersion fileVersion = (FileVersion) renderRequest.getAttribute("DOCUMENT_LIBRARY_FILE_VERSION");
			fileEntry = fileVersion.getFileEntry();
		}
		catch (PortalException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.getFileEntry() PortalException :::", e);
		}
		catch (SystemException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.getFileEntry() SystemException :::", e);
		}
		LoggerUtil.debugLogger(logger, "End of getFileEntry() method of WebUtil::");
		return fileEntry;
	}

	/**
	 * Returns Asset Category based on Asset Category Name.
	 * @param portletRequest
	 * @param childName
	 * @param parentName
	 * @param locale
	 * @return List<AssetCategory>
	 */
	@SuppressWarnings("unchecked")
	public static List<AssetCategory> fetchAssetCategoryByName(PortletRequest portletRequest, String childName, String parentName, Locale locale) {

		LoggerUtil.debugLogger(logger, "Start of fetchAssetCategoryByName() method of WebUtil::");
		List<AssetCategory> allCategoryList = null;
		List<AssetCategory> categoryList = new ArrayList<AssetCategory>();
		try {
			DynamicQuery query = DynamicQueryFactoryUtil.forClass(AssetCategory.class, PortalClassLoaderUtil.getClassLoader());
			Criterion criterion = RestrictionsFactoryUtil.eq("name", childName);
			query.add(criterion);
			allCategoryList = (List<AssetCategory>) AssetCategoryLocalServiceUtil.dynamicQuery(query);
			if (parentName != null && !parentName.isEmpty()) {
				for (AssetCategory assetCategory : allCategoryList) {
					try {
						if (hasCategoryAccess(portletRequest, assetCategory, ActionKeys.VIEW)) {
							if (assetCategory.getParentCategoryId() != 0 &&
								AssetCategoryLocalServiceUtil.getCategory(assetCategory.getParentCategoryId()).getTitle(locale).equals(parentName)) {
								categoryList.add(assetCategory);
							}
							else if (assetCategory.getParentCategoryId() == 0 &&
								AssetVocabularyLocalServiceUtil.getAssetVocabulary(assetCategory.getVocabularyId()).getTitle(locale).equals(
									parentName)) {
								categoryList.add(assetCategory);
							}
						}
					}
					catch (PortalException e) {
						LoggerUtil.errorLogger(logger, "WebUtil.fetchAssetCategoryByName() PortalException :::", e);
					}
				}
			}
		}
		catch (SystemException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.fetchAssetCategoryByName() SystemException :::", e);
		}
		LoggerUtil.debugLogger(logger, "End of fetchAssetCategoryByName() method of WebUtil::");
		return categoryList;
	}

	/**
	 * Gets Articles based on given Vocab name.
	 * @param portletRequest
	 * @param portletResponse
	 * @param vocabName
	 * @return List<JournalArticle>
	 */
	public static List<JournalArticle> getArticlesByCategory(PortletRequest portletRequest, PortletResponse portletResponse, String vocabName) {

		LoggerUtil.debugLogger(logger, "Start of getArticlesByCategory() method of WebUtil::");
		List<JournalArticle> articlesList = null;
		try {
			List<AssetCategory> assetCategoryList = WebUtil.getAssetCategoryByVocabulary(portletRequest, portletResponse, vocabName);
			if (null != assetCategoryList && assetCategoryList.size() > 0) {
				AssetCategory[] categoryIdArr = new AssetCategory[assetCategoryList.size()];
				for (int i = 0; i < assetCategoryList.size(); i++) {
					categoryIdArr[i] = assetCategoryList.get(i);
				}
				articlesList = JournalArticleLocalServiceUtil.getArticles();
				try {
					articlesList = WebUtil.getArticlesByCategories(portletRequest, portletResponse, articlesList, categoryIdArr);
				}
				catch (PortalException e) {
					LoggerUtil.errorLogger(logger, "WebUtil.getArticlesByCategory() PortalException :::", e);
				}
			}
		}
		catch (SystemException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.getArticlesByCategory() SystemException :::", e);
		}
		LoggerUtil.debugLogger(logger, "End of getArticlesByCategory() method of WebUtil::");
		return articlesList;
	}

	/**
	 * Gets latest Articles based on given Vocab name.
	 * @param portletRequest
	 * @param portletResponse
	 * @param vocabName
	 * @return List<JournalArticle>
	 */
	public static List<JournalArticle> getArticlesLatestByCategory(PortletRequest portletRequest, PortletResponse portletResponse, String vocabName) {

		LoggerUtil.debugLogger(logger, "Start of getArticlesByCategory() method of WebUtil::");
		List<JournalArticle> articlesList = null;
		try {
			List<AssetCategory> assetCategoryList = WebUtil.getAssetCategoryByVocabulary(portletRequest, portletResponse, vocabName);
			if (null != assetCategoryList && assetCategoryList.size() > 0) {
				AssetCategory[] categoryIdArr = new AssetCategory[assetCategoryList.size()];
				for (int i = 0; i < assetCategoryList.size(); i++) {
					categoryIdArr[i] = assetCategoryList.get(i);
				}
				articlesList = JournalArticleLocalServiceUtil.getArticles();
				try {
					articlesList = WebUtil.getArticlesLatestByCategories(portletRequest, portletResponse, articlesList, categoryIdArr);
				}
				catch (PortalException e) {
					LoggerUtil.errorLogger(logger, "WebUtil.getArticlesByCategory() PortalException :::", e);
				}
			}
		}
		catch (SystemException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.getArticlesByCategory() SystemException :::", e);
		}
		LoggerUtil.debugLogger(logger, "End of getArticlesByCategory() method of WebUtil::");
		return articlesList;
	}
	
	/**
	 * Gets user roles
	 * @param portletRequest
	 * @return List<Role>
	 */
	public static List<Role> getUserRoles(PortletRequest portletRequest) {

		LoggerUtil.debugLogger(logger, "Start of getUserRoles() method of WebUtil::");
		List<Role> roles = null;
		try {
			roles = RoleServiceUtil.getUserRoles(getUserId(portletRequest));
		}
		catch (PortalException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.getUserRoles() PortalException :::", e);
		}
		catch (SystemException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.getUserRoles() SystemException :::", e);
		}
		LoggerUtil.debugLogger(logger, "End of getUserRoles() method of WebUtil::");
		return roles;
	}

	/**
	 * Checks if user has given role
	 * @param portletRequest
	 * @param role
	 * @return boolean
	 */
	public static boolean checkRole(PortletRequest portletRequest, String role) {

		LoggerUtil.debugLogger(logger, "Start of checkRole() method of WebUtil::");
		boolean isRole = false;
		try {
			com.liferay.portal.model.User user = UserLocalServiceUtil.getUserById(getUserId(portletRequest));
			isRole = RoleLocalServiceUtil.hasUserRole(getUserId(portletRequest), user.getCompanyId(), role, true);
		}
		catch (Exception e) {
			LoggerUtil.errorLogger(logger, "WebUtil.checkRole() Exception :::", e);
		}
		LoggerUtil.debugLogger(logger, "End of checkRole() method of WebUtil::");
		return isRole;
	}

	/**
	 * Checks if file has view permission for the currently logged in user
	 * @param portletRequest
	 * @param fileEntry
	 * @return boolean
	 */
	public static boolean hasPermission(PortletRequest portletRequest, FileEntry fileEntry) {

		LoggerUtil.debugLogger(logger, "Start of hasPermission() method of WebUtil::");
		boolean hasPermission = false;
		try {
			hasPermission = fileEntry.containsPermission(getThemeDisplay(portletRequest).getPermissionChecker(), ActionKeys.VIEW);
		}
		catch (PortalException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.hasPermission() PortalException :::", e);
		}
		catch (SystemException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.hasPermission() SystemException :::", e);
		}
		LoggerUtil.debugLogger(logger, "hasPermission............................ " + hasPermission);
		LoggerUtil.debugLogger(logger, "End of hasPermission() method of WebUtil::");
		return hasPermission;
	}

	/**
	 * Returns PermissionChecker object
	 * @param portletRequest
	 * @return PermissionChecker
	 */
	public static PermissionChecker getPermissionChecker(PortletRequest portletRequest) {

		return getThemeDisplay(portletRequest).getPermissionChecker();
	}

	/**
	 * Gets Asset Categories based on Vocabulary Name.
	 * @param portletRequest
	 * @param portletResponse
	 * @param vocabularyName
	 * @return List<AssetCategory>
	 * @throws SystemException
	 */
	public static List<AssetCategory> getAllAssetCategoriesByVocabulary(PortletRequest portletRequest, PortletResponse portletResponse, String vocabularyName) throws SystemException {

		LoggerUtil.debugLogger(logger, "Start of getAllAssetCategoriesByVocabulary() method of WebUtil::");
		ThemeDisplay themeDisplay = getThemeDisplay(portletRequest);
		List<AssetVocabulary> vocabularies = null;
		List<AssetCategory> assetCategoryList = null;
		vocabularies = AssetVocabularyServiceUtil.getGroupVocabularies(themeDisplay.getLayout().getGroupId(), vocabularyName, -1, -1, null);
		if (vocabularies != null && vocabularies.size() > 0) {
			// get the first and only vocabulary
			AssetVocabulary vocabulary = vocabularies.get(0);
			assetCategoryList = AssetCategoryLocalServiceUtil.getVocabularyCategories(vocabulary.getVocabularyId(), -1, -1, null);
		}
		LoggerUtil.debugLogger(logger, "End of getAllAssetCategoriesByVocabulary() method of WebUtil::");
		return assetCategoryList;
	}

	/**
	 * Fetches list of Asset Entries based on category name
	 * @param title
	 * @return List<AssetEntry>
	 */
	@SuppressWarnings("unchecked")
	public static List<AssetEntry> fetchAssetEntryByName(String title, PortletRequest portletRequest) {

		LoggerUtil.debugLogger(logger, "Start of fetchAssetEntryByName() method of WebUtil::");
		ThemeDisplay themeDisplay = WebUtil.getThemeDisplay(portletRequest);
		List<AssetEntry> assetEntryList = new ArrayList<AssetEntry>();
		List<AssetEntry> tempAssetEntryList = null;
		String[] layoutTitle = null;
		if (title.contains(StringPool.POUND)) {
			layoutTitle = title.split(StringPool.POUND);
		}
		else {
			layoutTitle = new String[] {
				title
			};
		}
		
		try {
			DynamicQuery query = DynamicQueryFactoryUtil.forClass(AssetEntry.class, PortalClassLoaderUtil.getClassLoader());
			Criterion criterion = RestrictionsFactoryUtil.like("title", StringPool.PERCENT + layoutTitle[0].replaceAll(StringPool.AMPERSAND, StringPool.AMPERSAND_ENCODED) + StringPool.PERCENT);
			query.add(criterion);
			tempAssetEntryList = (List<AssetEntry>) AssetEntryLocalServiceUtil.dynamicQuery(query);
			for (AssetEntry assetEntry : tempAssetEntryList) {
				List<AssetCategory> categoryList = assetEntry.getCategories();
				for (AssetCategory category : categoryList) {
					if (category.getName().equals(layoutTitle[0]) && assetEntry.getGroupId() == themeDisplay.getScopeGroupId() &&
						layoutTitle[0].equalsIgnoreCase(cropTitle(assetEntry.getTitleCurrentValue().trim()))) {

						if (layoutTitle.length >= 2 &&  category.getParentCategoryId() != 0 ) {
							AssetCategory parentAssetCategory = AssetCategoryLocalServiceUtil.getCategory(category.getParentCategoryId());
							if(parentAssetCategory.getName().equalsIgnoreCase(layoutTitle[1])){
									assetEntryList.add(assetEntry);
							}
						}
						else if (layoutTitle.length >= 2 && category.getParentCategoryId() == 0 ) {
							AssetVocabulary parentAssetVocabulary = AssetVocabularyLocalServiceUtil.getAssetVocabulary(category.getVocabularyId());
							if(parentAssetVocabulary.getName().equalsIgnoreCase(layoutTitle[1])){
									assetEntryList.add(assetEntry);
							}
						}else if(layoutTitle.length == 1){
							assetEntryList.add(assetEntry);
						}
					}
				}
			}
		}
		catch (SystemException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.fetchAssetCategoryByName() SystemException :::", e);
		}catch (PortalException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.fetchAssetCategoryByName() PortalException :::", e);
		}
		catch (Exception e) {
			LoggerUtil.errorLogger(logger, "WebUtil.fetchAssetCategoryByName() Exception :::", e);
		}
		LoggerUtil.debugLogger(logger, "End of fetchAssetEntryByName() method of WebUtil::");
		return assetEntryList;
	}
	
	
	/**
	 * Fetches list of Asset Entries based on category name
	 * @param title
	 * @return List<AssetEntry>
	 */
	public static List<AssetEntry> fetchAssetEntryByName(String title, Set<AssetEntry> tempAssetEntryList , PortletRequest portletRequest) {

		LoggerUtil.debugLogger(logger, "Start of fetchAssetEntryByName(String,Set<AssetEntry>) method of WebUtil::");
		ThemeDisplay themeDisplay = WebUtil.getThemeDisplay(portletRequest);
		List<AssetEntry> assetEntryList = new ArrayList<AssetEntry>();
		if(Validator.isNotNull(tempAssetEntryList) && tempAssetEntryList.size()>0 ){
		String[] layoutTitle = null;
		if (title.contains(StringPool.POUND)) {
			layoutTitle = title.split(StringPool.POUND);
		}
		else {
			layoutTitle = new String[] {
				title
			};
		}
		
		try {
			for (AssetEntry assetEntry : tempAssetEntryList) {
				List<AssetCategory> categoryList = assetEntry.getCategories();
				for (AssetCategory category : categoryList) {
					if (category.getName().equals(layoutTitle[0]) && assetEntry.getGroupId() == themeDisplay.getScopeGroupId() && 
						layoutTitle[0].equalsIgnoreCase(cropTitle(assetEntry.getTitleCurrentValue().trim()))) {
						assetEntryList.add(assetEntry);
					}
				}
			}
		}
		catch (SystemException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.fetchAssetCategoryByName(String,Set<AssetEntry>) SystemException :::", e);
		}
		}
		LoggerUtil.debugLogger(logger, "End of fetchAssetEntryByName(String,Set<AssetEntry>) method of WebUtil::");
		return assetEntryList;
	}
	
	

	/**
	 * Removes the Content after "."
	 * @param assetEntryTitle
	 * @return title string
	 */
	public static String cropTitle(String assetEntryTitle) {

		LoggerUtil.debugLogger(logger, "Start of cropTitle() method of WebUtil::");
		String title = "";
		String escapeExtensons = PropsUtil.get("sideNavigation.remove.extensions").toLowerCase();
		if (assetEntryTitle.contains(".")) {
			if (escapeExtensons.contains(assetEntryTitle.substring(assetEntryTitle.lastIndexOf("."), assetEntryTitle.length()).toLowerCase())) {
				title = assetEntryTitle.substring(0, assetEntryTitle.lastIndexOf("."));
			}
			else {
				title = assetEntryTitle;
			}
		}
		else {
			title = assetEntryTitle;
		}
		//title = converCamelCase(title);
		LoggerUtil.debugLogger(logger, "End of cropTitle() method of WebUtil::");
		return title;
	}

	/**
	 * Helper Method to convert text to CamelCaseText.
	 * @param text
	 * @return CamelCaseText string
	 */
	public static String converCamelCase(String text) {

		LoggerUtil.debugLogger(logger, "Start of converCamelCase() method of WebUtil::");
		StringBuffer strbufCamelCase = new StringBuffer();
		StringTokenizer st = new StringTokenizer(text);
		if (null == text || text.length() == 0) {
			return "";
		}
		while (st.hasMoreTokens()) {
			String strWord = st.nextToken();
			strbufCamelCase.append(strWord.substring(0, 1).toUpperCase());
			if (strWord.length() > 1) {
				strbufCamelCase.append(strWord.substring(1).toLowerCase());
			}
			strbufCamelCase.append(" ");
		}
		LoggerUtil.debugLogger(logger, "End of converCamelCase() method of WebUtil::");
		return strbufCamelCase.toString().trim();
	}

	/**
	 * Returns true if the user is accessing from outside the coach network or
	 * false if accessing from inside the network.
	 * @param portletRequest
	 * @return status string
	 */
	public static boolean isExternalNetwork(PortletRequest portletRequest) {
		LoggerUtil.debugLogger(logger, "Start of isExternalNetwork() method of WebUtil::");
		String ipAddress  = PortalUtil.getHttpServletRequest(portletRequest).getRemoteAddr();
		String[] internalIPS = PropsUtil.getArray(Constants.INTERNAL_NETWORK_IPS);
		boolean isExternal = true;
		for(String internalIP : internalIPS)
		{
			if(Validator.isNotNull(ipAddress) && ipAddress.startsWith(internalIP))
			{
				isExternal = false;
				break;
			}
		}
		LoggerUtil.debugLogger(logger, "isExternalNetwork Value:::::::" + isExternal);
		LoggerUtil.debugLogger(logger, "End of isExternalNetwork() method of WebUtil::");
		return isExternal;
	}
	
	/**
	 * Returns true if the user has the writer role.
	 * @param portletRequest
	 * @return
	 */
	public static boolean hasWriterRole(PortletRequest portletRequest) {
		LoggerUtil.debugLogger(logger, "Start of hasWriterRole() method of WebUtil::");
		Set<String> userRoles = new HashSet<String>();
		ThemeDisplay themeDisplay = getThemeDisplay(portletRequest);
		 boolean hasPermission = false;
		 String[] writerRoles = PropsUtil.getArray(Constants.PDF_VIEW_URL);
		 try {
			hasPermission = RoleLocalServiceUtil.hasUserRoles(themeDisplay.getUserId(), themeDisplay.getCompanyId(), writerRoles, true);
		} catch (PortalException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.hasWriterRole() PortalException :::", e);
		} catch (SystemException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.hasWriterRole() SystemException :::", e);
		}
		/*try {
			roleList = themeDisplay.getUser().getRoles();
			 for(Role role : roleList)
			    {
				 userRoles.add(role.getName().toLowerCase());
			    }
			
			 for(String writerRole : writerRoles)
			 {
				 writerRolesSet.add(writerRole.toLowerCase());
			 }
			 
			 userRoles.retainAll(writerRolesSet);
		} catch (SystemException e) {
			
			LoggerUtil.errorLogger(logger, "WebUtil.hasWriterRole() SystemException :::", e);
		}*/
		
		LoggerUtil.debugLogger(logger, "has writer role :::::::" + userRoles.size());
		LoggerUtil.debugLogger(logger, "End of hasWriterRole() method of WebUtil::");
		return hasPermission;
	}

	/**
	 * Returns "true" if the Asset can be accessed from outside the network or
	 * "false" otherwise.
	 * @param companyId
	 * @param name
	 * @param scope
	 * @param primKey
	 * @param roleId
	 * @param actionId
	 * @return boolean
	 * @throws PortalException
	 * @throws SystemException
	 */
	public static boolean hasAssetExternalAccess(long companyId, String name, int scope, String primKey, long roleId, String actionId) throws PortalException, SystemException {

		LoggerUtil.debugLogger(logger, "Start of hasAssetExternalAccess() method of WebUtil::");
		if (roleId == 0 && internalAccesRoleId == 0) {
			Role role = RoleLocalServiceUtil.getRole(companyId, Constants.INTERNAL_ACCESS_ROLE);
			internalAccesRoleId = role.getRoleId();
		}

		if (roleId == 0) {
			roleId = internalAccesRoleId;
		}

		boolean assetExternalAccess = !ResourcePermissionLocalServiceUtil.hasResourcePermission(companyId, name, scope, primKey, roleId, actionId);
		LoggerUtil.debugLogger(logger, "End of hasAssetExternalAccess() method of WebUtil::");
		return assetExternalAccess;
	}

	/**
	 * Returns "true" if the Category can be viewed or "false" otherwise.
	 * @param portalRequest
	 * @param category
	 * @param actionId
	 * @return boolean
	 */
	public static boolean hasCategoryAccess(PortletRequest portalRequest, AssetCategory category, String actionId) {

		LoggerUtil.debugLogger(logger, "Start of hasCategoryAccess() method of WebUtil::");
		PermissionChecker permissionChecker = getPermissionChecker(portalRequest);
		if (permissionChecker.hasOwnerPermission(
			category.getCompanyId(), AssetCategory.class.getName(), category.getCategoryId(), category.getUserId(), actionId)) {
			return true;
		}
		LoggerUtil.debugLogger(logger, "End of hasCategoryAccess() method of WebUtil::");
		return permissionChecker.hasPermission(category.getGroupId(), AssetCategory.class.getName(), category.getCategoryId(), actionId);
	}

	/**
	 * Returns all the child categories which has View Permissions for this
	 * parent category by Querying DB.
	 * @param AssetCategory
	 * @param PortletRequest
	 * @return List of child Asset Categories.
	 */
	public static List<AssetCategory> getChildCategories(AssetCategory category, PortletRequest portletRequest) {

		LoggerUtil.debugLogger(logger, "Start of getChildCategories() method of WebUtil::");
		List<AssetCategory> allCategoryList = null;
		List<AssetCategory> childCategoryList = new ArrayList<AssetCategory>();
		try {
			allCategoryList = AssetCategoryLocalServiceUtil.getChildCategories(category.getCategoryId());
			for (AssetCategory assetCategory : allCategoryList) {
				if (hasCategoryAccess(portletRequest, assetCategory, ActionKeys.VIEW)) {
					childCategoryList.add(assetCategory);
				}
			}
		}
		catch (SystemException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.getChildCategories() SystemException :::", e);
		}
		LoggerUtil.debugLogger(logger, "End of getChildCategories() method of WebUtil::");
		return childCategoryList;
	}

	/**
	 * Returns all the child categories which has View Permissions from the
	 * list.
	 * @param List
	 *            <AssetCategory>
	 * @param PortletRequest
	 * @return List of child Asset Categories.
	 */
	public static List<AssetCategory> getChildCategories(List<AssetCategory> categoryList, PortletRequest portletRequest) {

		LoggerUtil.debugLogger(logger, "Start of getChildCategories() method of WebUtil::");
		List<AssetCategory> childCategoryList = new ArrayList<AssetCategory>();
		if (Validator.isNotNull(categoryList) && categoryList.size() > 0) {
			for (AssetCategory assetCategory : categoryList) {
				if (hasCategoryAccess(portletRequest, assetCategory, ActionKeys.VIEW)) {
					childCategoryList.add(assetCategory);
				}
			}
		}
		LoggerUtil.debugLogger(logger, "End of getChildCategories() method of WebUtil::");
		return childCategoryList;
	}

	// Class Members
	private static long internalAccesRoleId;

	/**
	 * Helper method to trust certificate.
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 */
	public static void trust() throws NoSuchAlgorithmException, KeyManagementException {

		LoggerUtil.debugLogger(logger, "trust() Method Starts in WebUtil");
		Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		TrustManager[] trustAllCerts = new TrustManager[] {
			new X509TrustManager() {

				public X509Certificate[] getAcceptedIssuers() {

					return null;
				}

				public void checkServerTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {

					// return ;

				}

				public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {

					// return;
				}
			}
		};
		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		HostnameVerifier hv = new HostnameVerifier() {

			public boolean verify(String urlHostName, SSLSession session) {

				if (!urlHostName.equalsIgnoreCase(session.getPeerHost())) {
					LoggerUtil.debugLogger(
						logger, "Warning: URL host '" + urlHostName + "' is different to SSLSession host '" + session.getPeerHost() + "'.");
				}
				return true;
			}
		};
		HttpsURLConnection.setDefaultHostnameVerifier(hv);
		LoggerUtil.debugLogger(logger, "trust() Method Ends in WebUtil");
	}

	/**
	 * Returns dynamic data list topics.
	 * @param renderRequest
	 * @return List<String[]>
	 */
	public static List<String[]> getDynamicDataListRecords(PortletRequest renderRequest, String keyValue) {

		LoggerUtil.debugLogger(logger, "Start of getFeedbackTopic() method of WebUtil::");
		List<String[]> lstTopics = new ArrayList<String[]>();
		try {
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			List<DDLRecordSet> ddlRecordSet = DDLRecordSetLocalServiceUtil.getRecordSets(themeDisplay.getScopeGroupId());
			for (DDLRecordSet recordSet : ddlRecordSet) {
				if (recordSet.getName().contains(keyValue)) {
					DDMStructure ddmStructure = recordSet.getDDMStructure();
					String languageId = LanguageUtil.getLanguageId(renderRequest);
					Map<String, Map<String, String>> fieldsMap = ddmStructure.getFieldsMap(languageId);
					List<DDLRecord> ddlRecords = DDLRecordLocalServiceUtil.getRecords(recordSet.getRecordSetId());
					for (DDLRecord ddlRecord : ddlRecords) {
						DDLRecordVersion recordVersion = ddlRecord.getRecordVersion();
						Fields fieldsModel = StorageEngineUtil.getFields(recordVersion.getDDMStorageId());
						// Columns
						String[] fieldMapValues = new String[fieldsMap.values().size()];
						int counter = 0;
						for (Map<String, String> fields : fieldsMap.values()) {
							String name = fields.get(FieldConstants.NAME);
							String value = null;
							if (fieldsModel.contains(name)) {
								com.liferay.portlet.dynamicdatamapping.storage.Field field = fieldsModel.get(name);
								value = field.getRenderedValue(themeDisplay);
							}
							else {
								value = StringPool.BLANK;
							}
							fieldMapValues[counter] = value;
							counter++;
						}
						lstTopics.add(fieldMapValues);
					}
				}
			}
		}
		catch (Exception e) {
			LoggerUtil.errorLogger(logger, "Exception occured while getting the feedback topics list: " + e.getMessage(), e);
		}
		LoggerUtil.debugLogger(logger, "End of getFeedbackTopic() method of WebUtil::");
		return lstTopics;
	}

	/**
	 * Creates audit record for news actions. Returns true if record is created
	 * successfully, false otherwise.
	 * @param className
	 * @param newsType
	 * @param newsCategory
	 * @param newsTitle
	 * @return boolean
	 */
	public static void createNewsAuditRecord(String className, String newsType, String newsCategory, String newsTitle) {

		if (Validator.isNotNull(className) && Validator.isNotNull(newsType) && Validator.isNotNull(newsCategory) && Validator.isNotNull(newsTitle)) {
			Attribute newsTypeAttr = new Attribute("NEWS_TYPE", newsType);
			Attribute newsCategoryAttr = new Attribute("NEWS_CATEGORY", newsCategory);
			Attribute newsTitleAttr = new Attribute("NEWS_TITLE", newsTitle);
			List<Attribute> attributeList = new ArrayList<Attribute>();
			attributeList.add(newsTypeAttr);
			attributeList.add(newsCategoryAttr);
			attributeList.add(newsTitleAttr);
			AuditMessage auditMessage = AuditMessageBuilder.buildAuditMessage(newsType, className, 0l, attributeList);
			CoachAudit.route(auditMessage);
		}
	}

	/**
	 * Creates audit record for general actions. Returns true if record is
	 * created successfully, false otherwise.
	 * @param className
	 * @param actionType
	 * @param itemTitle
	 * @return boolean
	 */
	public static void createAuditRecord(String className, String actionType, String itemTitle) {

//		boolean result = false;
		if (Validator.isNotNull(className) && Validator.isNotNull(actionType) && Validator.isNotNull(itemTitle)) 
		{
			Attribute actionTypeAttr = new Attribute("ACTION_TYPE", actionType);
			Attribute itemTitleAttr = new Attribute("ITEM_TITLE", itemTitle);
			List<Attribute> attributeList = new ArrayList<Attribute>();
			attributeList.add(actionTypeAttr);
			attributeList.add(itemTitleAttr);
			AuditMessage auditMessage = AuditMessageBuilder.buildAuditMessage(actionType, className, 0l, attributeList);
			CoachAudit.route(auditMessage);
		}
	}
	
	/**
	 * get current vocabulary name by passing request.
	 * @param portletRequest
	 * @return
	 */
	public static String getCurrentVocabulary(PortletRequest portletRequest){
		ThemeDisplay themeDisplay = getThemeDisplay(portletRequest);
		List<Layout> parentNavs = null;
		String currentVocab = null;
		try {
			parentNavs = themeDisplay.getLayout().getAncestors();
			if (parentNavs.size() == 2) {
				currentVocab = parentNavs.get(parentNavs.size() - 1)
						.getHTMLTitle(themeDisplay.getLocale());
			} else if (parentNavs.size() == 1) {
				currentVocab = parentNavs.get(parentNavs.size() - 1)
						.getHTMLTitle(themeDisplay.getLocale());
			}
		} catch (PortalException e) {
			LoggerUtil.errorLogger(logger, "Caught Exception in getCurrentVocabulary() in WebUtil", e);
		} catch (SystemException e) {
			LoggerUtil.errorLogger(logger, "Caught Exception in getCurrentVocabulary() in WebUtil", e);
		}
		return currentVocab;
	}
	
	/**
	 * Returns the List of Categories for the current GroupId. Does not checks  for the permission on Asset Vocabulary.  
	 * @param portletRequest
	 * @return
	 * @throws PortalException
	 * @throws SystemException
	 */
	public static List<AssetCategory> getAllAssetCategoriesByGroupId(PortletRequest portletRequest) throws PortalException, SystemException{
		LoggerUtil.debugLogger(logger, "Start of getAllAssetCategoriesByVocabulary() method of WebUtil::");
		ThemeDisplay themeDisplay = getThemeDisplay(portletRequest);
		List<AssetVocabulary> vocabularyList = null;
		List<AssetCategory> assetCategoryList = new ArrayList<AssetCategory>();
		String[] ignoreVocabularies = PropsUtil.getArray(Constants.CA_IGNORE_VOCAB_LIST);
		long[] groupIds = {themeDisplay.getScopeGroupId()};
		//vocabularyList = AssetVocabularyServiceUtil.getGroupsVocabularies(groupIds);
		/* Does not checks for the permission on Asset Vocabulary. */
		vocabularyList = AssetVocabularyLocalServiceUtil.getGroupsVocabularies(groupIds);
		
		for(AssetVocabulary assetVocabulary : vocabularyList)
		{
			if(ArrayUtil.contains(ignoreVocabularies, assetVocabulary.getName())){ //for Coach ASIA Side Navigation
				continue;
			}
			if(themeDisplay.getLocale().getLanguage().equalsIgnoreCase("en") && !assetVocabulary.getName().contains("_"))
			{
			   assetCategoryList.addAll(AssetCategoryLocalServiceUtil.getVocabularyCategories(assetVocabulary.getVocabularyId(),-1,-1,null));
			}else if (assetVocabulary.getName().contains("_") && assetVocabulary.getName().split("_")[1].equalsIgnoreCase(themeDisplay.getLocale().getCountry())){
				assetCategoryList.addAll(AssetCategoryLocalServiceUtil.getVocabularyCategories(assetVocabulary.getVocabularyId(),-1,-1,null));
			}
		}
		LoggerUtil.debugLogger(logger, "End of getAllAssetCategoriesByVocabulary() method of WebUtil::");
		return assetCategoryList;
	}
	
	
	/**
	 * Method used to get all LayOuts.
	 * @param groupId
	 * @return Map<String, Layout>
	 */
	public static Map<String, Layout> getLayoutsByGroupId(ThemeDisplay themeDisplay) {
		LoggerUtil.debugLogger(logger,"getLayouts() Method Starts in WebServiceUtil");
		List<Layout> layoutList = null;
		Map<String, Layout> layoutMap = new HashMap<String, Layout>();
		try {
				layoutList = new ArrayList<Layout>(LayoutLocalServiceUtil.getLayouts(themeDisplay.getScopeGroupId(), true));
				for (Layout layout : layoutList) {
					layoutMap.put(layout.getName(themeDisplay.getLocale()), layout);
				}
			}
			catch (SystemException e) {
				LoggerUtil.errorLogger(logger,"SystemException Caught while Processing getlayOuts() Method in WebServiceUtil Class.", e);
			}
			catch (Exception e) {
				LoggerUtil.errorLogger(logger, "Exception Caught while Processing getlayOuts() Method in WebServiceUtil Class.", e);
			}
		LoggerUtil.debugLogger(logger,"getLayouts() Method Ends in WebServiceUtil");
		return layoutMap;
	}
	
	/**
	 * Returns the list of JournalArticles based on Article type
	 * @param companyId
	 * @param groupId
	 * @param type
	 * @param limit
	 * @return List<JournalArticle>
	 * @throws PortalException
	 * @throws SystemException
	 */
	public static List<JournalArticle> getArticlesByTypeForOffices(long companyId, long groupId, String type, String title, int limit) throws PortalException, SystemException {

		LoggerUtil.debugLogger(logger, "Start of getArticlesByTypeForOffices() method of WebUtil::");
		List<JournalArticle> results = Collections.synchronizedList(new ArrayList<JournalArticle>());
		String articleId = null;
		Double version = null;
	//	String title = null;
		String description = null;
		String content = null;
		String[] structureIds = null;
		String[] templateIds = null;
		Date displayDateGT = null;
		Date displayDateLT = null;
		Date reviewDate = null;
		boolean andOperator = true;
		int start = 0;
		int end = limit;
		String orderByType = "asc";
		long classNameId = 0;
		int status = 0;
		boolean orderByAsc = orderByType.equals("asc");
		//OrderByComparator obc = new ArticleDisplayDateComparator(orderByAsc);
		OrderByComparator obcByArticleTitle = new ArticleTitleComparator(orderByAsc);
		try {
			results =
				JournalArticleLocalServiceUtil.search(
					companyId, groupId, classNameId, articleId, version, title, description, content, type, structureIds, templateIds, displayDateGT,
					displayDateLT, status, reviewDate, andOperator, start, end, obcByArticleTitle);
		}
		catch (Exception e) {

			LoggerUtil.errorLogger(logger, "Exception occured on search of Article", e);

		}
		LoggerUtil.debugLogger(logger, "End of getArticlesByTypeForOffices() method of WebUtil::");
		return results;
	}

	public static String getPortletInstanceId(PortletRequest request,String column)
	{
		String storePortletURL ="";
		Layout storeLayout = null;
		try {
			storeLayout = LayoutLocalServiceUtil.getFriendlyURLLayout(getThemeDisplay(request).getLayout().getGroupId(), true, getThemeDisplay(request).getLayout().getFriendlyURL());
		} catch (PortalException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.getPortletInstanceId()", e);
		} catch (SystemException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.getPortletInstanceId()", e);
		}
		if(storeLayout != null){
			 storePortletURL = storeLayout.getTypeSettingsProperty(column);
		}
		
		return storePortletURL;
	}
	
	/**
	 * Returns the Page URL.
	 * @param actionRequest
	 * @param LayoutUrl
	 * @return Page URL
	 */
	public static String getPageUrl(PortletRequest portletRequest, String layoutUrl){
		ThemeDisplay themeDisplay = getThemeDisplay(portletRequest);
		
		String pageUrl = themeDisplay.getPathFriendlyURLPrivateGroup()+themeDisplay.getScopeGroup().getFriendlyURL()+layoutUrl;
		return pageUrl;
		
	}
	
	
	/**
	 * Fetches list of Asset Entries based on title.
	 * @param title
	 * @return List<AssetEntry>
	 */
	@SuppressWarnings("unchecked")
	public static List<AssetEntry> fetchQandAAssetEntry(String title,Locale locale) {

		LoggerUtil.debugLogger(logger, "Start of fetchQandAAssetEntry() method of WebUtil::");
		List<AssetEntry> assetEntryList = new ArrayList<AssetEntry>();
		List<AssetEntry> tempAssetEntryList = null;
		try {
			DynamicQuery query = DynamicQueryFactoryUtil.forClass(AssetEntry.class, PortalClassLoaderUtil.getClassLoader());
			Criterion criterion = RestrictionsFactoryUtil.like("title", StringPool.PERCENT + title.replaceAll(StringPool.AMPERSAND, StringPool.AMPERSAND_ENCODED) + StringPool.PERCENT);
			query.add(criterion);
			tempAssetEntryList = (List<AssetEntry>) AssetEntryLocalServiceUtil.dynamicQuery(query);
			
			if(tempAssetEntryList.size()>1)
			{
				for (AssetEntry assetEntry : tempAssetEntryList) {
				   if(title.equalsIgnoreCase(assetEntry.getTitle(locale)))	
					 assetEntryList.add(assetEntry);
				     break;
				}
			}else{
				assetEntryList = tempAssetEntryList;
			}
		}
		catch (SystemException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.fetchQandAAssetEntry() SystemException :::", e);
		}
		catch (Exception e) {
			LoggerUtil.errorLogger(logger, "WebUtil.fetchQandAAssetEntry() Exception :::", e);
		}
		LoggerUtil.debugLogger(logger, "End of fetchQandAAssetEntry() method of WebUtil::");
		return assetEntryList;
	}
	
	
	
	/**
	 * Returns true if the user has the access to Questions And Answers or Instructions Portlet.
	 * @param portletRequest
	 * @return
	 */
	public static boolean hasQandAPortletAccess(PortletRequest portletRequest) {
		LoggerUtil.debugLogger(logger, "Start of hasWriterRole() method of WebUtil::");
		Set<String> userRoles = new HashSet<String>();
		ThemeDisplay themeDisplay = getThemeDisplay(portletRequest);
		boolean hasPermission = false;
		String[] qandAPortletAccessRoles = PropsUtil.getArray(Constants.QANDA_PORTLET_ACCESS);
		try {
			hasPermission = RoleLocalServiceUtil.hasUserRoles(themeDisplay.getUserId(), themeDisplay.getCompanyId(), qandAPortletAccessRoles, true);
		} catch (PortalException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.hasWriterRole() PortalException :::", e);
		} catch (SystemException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.hasWriterRole() SystemException :::", e);
		}
		LoggerUtil.debugLogger(logger, "has writer role :::::::" + userRoles.size());
		LoggerUtil.debugLogger(logger, "End of hasWriterRole() method of WebUtil::");
		return hasPermission;
	}
	
	/**
	 * Returns true if the user has the access to Questions And Answers or Instructions Portlet.
	 * @param portletRequest
	 * @return
	 */
	public static boolean hasUserHRServiceCenterRole(PortletRequest portletRequest) {
		LoggerUtil.debugLogger(logger, "Start of hasUserHRServiceCenterRole() method of WebUtil::");
		Set<String> userRoles = new HashSet<String>();
		ThemeDisplay themeDisplay = getThemeDisplay(portletRequest);
		boolean hasPermission = false;
		String[] qandAPortletAccessRoles = PropsUtil.getArray(Constants.QANDA_PORTLET_ACCESS);
		try {
			for (String role : qandAPortletAccessRoles) {
				 if(role.equalsIgnoreCase("App-MCW-HR-Service-Center")) {
					 //isRole = RoleLocalServiceUtil.hasUserRole(getUserId(portletRequest), user.getCompanyId(), role, true);
					 hasPermission = RoleLocalServiceUtil.hasUserRole(themeDisplay.getUserId(), themeDisplay.getCompanyId(), role, true);
				 }
			}
			
		} catch (PortalException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.hasUserHRServiceCenterRole() PortalException :::", e);
		} catch (SystemException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.hasUserHRServiceCenterRole() SystemException :::", e);
		}
		LoggerUtil.debugLogger(logger, "has HRServiceCenterRole role :::::::" + userRoles.size());
		LoggerUtil.debugLogger(logger, "End of hasUserHRServiceCenterRole() method of WebUtil::");
		return hasPermission;
	}
	
	
	/*public static String getXMLLocaleContent(String content,Locale locale) {
		String languageId = LocaleUtil.toLanguageId(locale);

		return LocalizationUtil.getLocalization(content, languageId);
	}*/
	
	
	public static void getHeapSize() {
	  int mb = 1024*1024;
      
      //Getting the runtime reference from system
      Runtime runtime = Runtime.getRuntime();
       
      System.out.println("##### Heap utilization statistics [MB] #####");
      LoggerUtil.debugLogger(logger, "##### Heap utilization statistics [MB] #####");
      //Print used memory
      System.out.println("Used Memory:" + (runtime.totalMemory() - runtime.freeMemory()) / mb);
      LoggerUtil.debugLogger(logger, "Used Memory:" + (runtime.totalMemory() - runtime.freeMemory()) / mb);
      //Print free memory
      System.out.println("Free Memory:"+ runtime.freeMemory() / mb);
      LoggerUtil.debugLogger(logger, "Free Memory:"+ runtime.freeMemory() / mb);
      //Print total available memory
      System.out.println("Total Memory:" + runtime.totalMemory() / mb);
      LoggerUtil.debugLogger(logger, "Total Memory:" + runtime.totalMemory() / mb);
      //Print Maximum available memory
      System.out.println("Max Memory:" + runtime.maxMemory() / mb);
      LoggerUtil.debugLogger(logger, "Max Memory:" + runtime.maxMemory() / mb);
	}
}
