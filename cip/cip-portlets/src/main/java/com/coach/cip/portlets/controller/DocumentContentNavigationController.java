package com.coach.cip.portlets.controller;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.xml.namespace.QName;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.coach.cip.util.WebUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.asset.model.AssetCategory;
import com.liferay.portlet.asset.model.AssetVocabulary;
import com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil;

/**
 * Controller class for DocumentContentNavigation Portlet.
 * @author GalaxE
 */
@Controller("DocumentContentNavigationController")
@RequestMapping(value = "VIEW")
public class DocumentContentNavigationController extends AbstractBaseController {

	/** The Constant logger. */
	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(DocumentContentNavigationController.class);

	/**
	 * Default Render Mapping Method. this method is invoked.
	 * @param renderResponse
	 * @return string
	 * @throws SystemException 
	 * @throws PortalException 
	 */
	@RenderMapping
	public String showJournalArticles(RenderRequest renderRequest, RenderResponse renderResponse) throws PortalException, SystemException {

		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
		List<AssetVocabulary> vocabList = AssetVocabularyLocalServiceUtil.getGroupVocabularies(themeDisplay.getScopeGroupId());
		List<AssetVocabulary> localeVocabList = new ArrayList<AssetVocabulary>();
		String[] ignoreVocabularies = PropsUtil.getArray(Constants.CA_IGNORE_VOCAB_LIST);
		for(AssetVocabulary assetVocabulary : vocabList)
		{
			if(ArrayUtil.contains(ignoreVocabularies, assetVocabulary.getName())){ //for Coach ASIA Side Navigation
				continue;
			}
			if(themeDisplay.getLocale().getLanguage().equalsIgnoreCase("en") && !assetVocabulary.getName().contains("_"))
			{
				localeVocabList.add(assetVocabulary);
			}else if (assetVocabulary.getName().contains("_") && assetVocabulary.getName().split("_")[1].equalsIgnoreCase(themeDisplay.getLocale().getCountry())){
				localeVocabList.add(assetVocabulary);
			}
		}
		
		renderRequest.setAttribute("vocabList",localeVocabList);
		
		Map<Long, List<AssetCategory>> categoryMap = new HashMap<Long, List<AssetCategory>>();
		if(localeVocabList != null && localeVocabList.size()>0){
			for(AssetVocabulary vocabulory : localeVocabList){
				long vocabuloryId = vocabulory.getVocabularyId();
				List<AssetCategory> listCats = AssetCategoryLocalServiceUtil.getVocabularyCategories(vocabuloryId,0,AssetCategoryLocalServiceUtil.getVocabularyCategoriesCount(vocabuloryId),null);
				List<AssetCategory> listRootCats = new ArrayList<AssetCategory>();
				if(listCats != null && listCats.size()>0){
					for(AssetCategory cat : listCats){
						if(cat.getParentCategoryId()==0){
							listRootCats.add(cat);
						}
					}
				categoryMap.put(vocabuloryId, listRootCats);
				}
			}
		}
		renderRequest.setAttribute("categoryMap", categoryMap);
		
		return Constants.DOCUMENTCONTENTNAVIGATION_JSP;
	}

	/**
	 * To send vocabulary to AlldocumentContent Portlet
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 * @throws IOException
	 */
	@ActionMapping(params = "myaction=listAllCatContent")
	public void listAllCatContent(ActionRequest actionRequest, ActionResponse actionResponse, Model model) throws IOException {

		LoggerUtil.debugLogger(logger, "Start of listAllCatContent() method----------------------------------------");
		String categoryId = actionRequest.getParameter("catId");
		actionResponse.setRenderParameter("catId", categoryId);
		QName qname = new QName("http://coach.com/events","ipc.vocabulory");
		actionResponse.setEvent(qname, categoryId);
		LoggerUtil.debugLogger(logger, "End of detailallwebcontent()-----------------------------------------------------");
	}
}
