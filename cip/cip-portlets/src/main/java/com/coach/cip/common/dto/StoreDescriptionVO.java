package com.coach.cip.common.dto;

import java.io.Serializable;

/**
 * 
 * Class is used to capture the characteristics of storeDescriptionVO.
 * 
 * @author GalaxE.
 *
 */
public class StoreDescriptionVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3201774331363586723L;
	//Default Constructor
	public StoreDescriptionVO(){
		
	}
	
	//Fully Constructor
	public StoreDescriptionVO(String storenumber, String storename, String city, String state, String country, String vm,
    String geography,  String phone, String districtDescription, String floor){
		
		 this.storenumber = storenumber;
	     this.storename = storename;
	     this.city = city;
	     this.state = state;
	     this.country = country;
	     this.vm = vm;
	     this.geography = geography;
	     this.phone = phone;
	     this.districtDescription = districtDescription;
	     this.floor = floor;
		
	}
	 private String storenumber;
     private String storename;
     private String city;
     private String state;
     private String country;
     private String vm;
     private String geography;
     private String phone;
     private String districtDescription;
     private String floor;
	public String getStorenumber() {
		return storenumber;
	}

	public void setStorenumber(String storenumber) {
		this.storenumber = storenumber;
	}

	public String getStorename() {
		return storename;
	}

	public void setStorename(String storename) {
		this.storename = storename;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getVm() {
		return vm;
	}

	public void setVm(String vm) {
		this.vm = vm;
	}

	public String getGeography() {
		return geography;
	}

	public void setGeography(String geography) {
		this.geography = geography;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getDistrictDescription() {
		return districtDescription;
	}

	public void setDistrictDescription(String districtDescription) {
		this.districtDescription = districtDescription;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}
}
