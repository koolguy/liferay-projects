package com.coach.cip.portlets.controller;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.PortletSession;
import javax.portlet.ReadOnlyException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.ValidatorException;
import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;
import com.coach.cip.common.dto.StoreVO;
import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.coach.cip.util.WebUtil;
import com.coach.stores.service.beans.GetAllOpenStoreDescriptionResponse.StoresDesc.StoreDesc;
import com.coach.stores.service.beans.GetOpenStoreByGeographyRequest;
import com.coach.stores.service.beans.GetOpenStoreByGeographyResponse;
import com.coach.stores.service.beans.GetOpenStoreByStoreNumberRequest;
import com.coach.stores.service.beans.GetOpenStoreByStoreNumberResponse;
import com.coach.stores.service.beans.GetOpenStoreByStoreNumberResponse.Stores.Store;
import com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyRequest;
import com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyRequest.Geography;
import com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyResponse;
import com.coach.stores.service.beans.ObjectFactory;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.theme.ThemeDisplay;

/**
 * Class is used for store Address operations.
 * 
 * @author GalaxE.
 */

@Controller("storeAddressController")
@RequestMapping(value = "VIEW")
public class StoreAddressController extends AbstractBaseController {

	/** The Constant LOG. */
	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(StoreAddressController.class);
	private static final String STOREADDRESS = "storeAddress";
	private static final String STOREDETAILSLIST = "storeDetailsList";
	
	@Autowired
	@Qualifier("storeServiceTemplate")
	private WebServiceTemplate webServiceTemplate;
	private static final ObjectFactory WS_CLIENT_FACTORY = new ObjectFactory();
	
	List<StoreDesc> openStoresDescList = new ArrayList<StoreDesc>();
	List<com.coach.stores.service.beans.GetOpenStoreByGeographyResponse.Stores.Store> storeDesclist =
					new ArrayList<com.coach.stores.service.beans.GetOpenStoreByGeographyResponse.Stores.Store>();

	List<com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyResponse.StoresDesc.StoreDesc> openStoreDesclist =
		new ArrayList<com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyResponse.StoresDesc.StoreDesc>();
	
	/**
	 * Default Constructor.
	 */
	public StoreAddressController() {

	}

	/**
	 * Constructor which initializes webserviceTemplate.
	 * @param webServiceTemplate
	 */
	public StoreAddressController(WebServiceTemplate webServiceTemplate) {
		this.webServiceTemplate = webServiceTemplate;
	}
	
	private String geoName;
	private String brand;
	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getGeoName() {
		return geoName;
	}
	public void setGeoName(String geoName) {
		this.geoName = geoName;
	}
	
	/**
	 * Renders storeAddress view.
	 * 
	 * @param request
	 * @param renderresponse
	 * @return 
	 * @throws IOException 
	 * @throws ValidatorException 
	 * @throws ReadOnlyException 
	 */
	@SuppressWarnings({"unchecked" })
	@RenderMapping
	public String showStoreAddress(RenderRequest request,RenderResponse response, Model model) throws ReadOnlyException, ValidatorException, IOException {
		LoggerUtil.debugLogger(logger,"Default render mapping for storeAddress view in StoreAddressController");
		HttpServletRequest httpServletRequest = WebUtil.getHttpServletRequest(request);
		ThemeDisplay themeDisplay = WebUtil.getThemeDisplay(request);
		LoggerUtil.debugLogger(logger,"Default render mapping for storeAddress view in StoreAddressController---"+themeDisplay.getLayout().getHTMLTitle(themeDisplay.getLocale()));
		String eventGeographyName = null;
		String defaultGeographyName = null;
		String defaultBrand = null;
		String brand = null;
		String interStorePage =null;
		PortletSession session = request.getPortletSession();
		if(httpServletRequest.getParameter("interStorePage") != null){
		interStorePage = (String) httpServletRequest.getParameter("interStorePage");
		}
		eventGeographyName = (String) session.getAttribute("eventGeographyName");
		brand = (String) session.getAttribute("brand");
			if(eventGeographyName == null){
				//fetching geographyName from  storeDetailList action mapping
				eventGeographyName = (String) request.getAttribute("geographyName");
			}
			if(brand == null){
				//fetching brand from  storeDetailList action mapping
				brand = (String) request.getAttribute("brand");
			}
			//fetching eventgeographyName from eventMapping
			if(eventGeographyName != null){
				populateGeoList(request, response, eventGeographyName, model, brand);
				model.addAttribute("geographyName", eventGeographyName);
				session.removeAttribute("eventGeographyName");
				session.removeAttribute("brand");
			}
			if(interStorePage == null && eventGeographyName == null && themeDisplay.getLayout().getHTMLTitle(themeDisplay.getLocale()).equalsIgnoreCase(Constants.STORES)){
				defaultGeographyName = themeDisplay.getLayout().getHTMLTitle(themeDisplay.getLocale());
				defaultBrand = "COACH";
				if(defaultGeographyName.equalsIgnoreCase("STORES")){
					defaultGeographyName = Constants.STORES_NA;
				}
				LoggerUtil.debugLogger(logger,"Default render mapping for storeAddress view in StoreAddressController---"+defaultGeographyName);
				populateGeoList(request, response, defaultGeographyName, model,defaultBrand);
			}
			if(interStorePage != null && interStorePage.equalsIgnoreCase("true")) {
				List<StoreVO> storeList = null;
				String geographyName= "";
				String redirectUrl ="";
				String searchRedirectString = "";
				String faultCode = "";
				String faultMessage = "";
				
				storeList = (List<StoreVO>) session.getAttribute("storeList");
				geographyName = (String) session.getAttribute("geographyName");
				redirectUrl = (String) session.getAttribute("redirectUrl");
				searchRedirectString = (String) session.getAttribute("searchRedirectString");
				faultCode = (String) session.getAttribute("FaultCode");
				faultMessage = (String)session.getAttribute("FaultMessage");
				
				model.addAttribute("storeList", storeList);
				model.addAttribute("geographyName", geographyName);
				model.addAttribute("redirectUrl", redirectUrl);
				model.addAttribute("searchRedirectString", searchRedirectString);
				model.addAttribute("FaultCode", faultCode);
				model.addAttribute("FaultMessage", faultMessage);
				
				return STOREDETAILSLIST;
				}
			
		return STOREADDRESS;
	}
	
	/**
	 * Renders storeDetailsList view.
	 * 
	 * @param request
	 * @param renderresponse
	 * @return
	 */
	
	@RenderMapping(params="myaction=displayStoreAddressDetailsForm")
	public String showStoreAddressDetails(RenderRequest request,RenderResponse renderresponse,Model model) {
		LoggerUtil.debugLogger(logger,"Render mapping for storeAddressDetail view in StoreAddressConttroller");
		/*PortletSession session = request.getPortletSession();
		if(session.getAttribute("FromSearchEvent") != null) {
		List<StoreVO> storeList = null;
		String geographyName= "";
		String redirectUrl ="";
		String searchRedirectString = "";
		String faultCode = "";
		String faultMessage = "";
		
		storeList = (List<StoreVO>) session.getAttribute("storeList");
		geographyName = (String) session.getAttribute("geographyName");
		redirectUrl = (String) session.getAttribute("redirectUrl");
		searchRedirectString = (String) session.getAttribute("searchRedirectString");
		faultCode = (String) session.getAttribute("FaultCode");
		faultMessage = (String)session.getAttribute("FaultMessage");
		
		model.addAttribute("storeList", storeList);
		model.addAttribute("geographyName", geographyName);
		model.addAttribute("redirectUrl", redirectUrl);
		model.addAttribute("searchRedirectString", searchRedirectString);
		model.addAttribute("FaultCode", faultCode);
		model.addAttribute("FaultMessage", faultMessage);
		}*/
		return STOREDETAILSLIST;
	}
	
	/**
	 * Renders storeAddress view.
	 * @param request
	 * @param renderresponse
	 * @return
	 */
	@RenderMapping(params="myaction=displayStoreDetailsForm")
	public String showStoreDetails(RenderRequest request,RenderResponse response) {
		LoggerUtil.debugLogger(logger,"Render mapping for storeDetail view in StoreAddressConttroller");
		return STOREADDRESS;
	}
	
	/**
	 * Method catches location event and processing event.
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @throws ReadOnlyException
	 * @throws ValidatorException
	 * @throws IOException
	 */
	
	@SuppressWarnings("unchecked")
	@EventMapping(value="{http://coach.com/events}ipc.region")
    public void catchRegions(EventRequest request, EventResponse response, Model model) throws ReadOnlyException, ValidatorException, IOException{
		LoggerUtil.debugLogger(logger,"catchRegions process event in StoreAddressController");
		PortletSession eventSession = request.getPortletSession();	
    	Event event= request.getEvent();
    	HashMap<String, String> regionMap = (HashMap<String, String>)event.getValue();
    	if(regionMap != null){
    	String geographyName = (String) regionMap.get("regionId");
    	String brand = (String) regionMap.get("brand");
    	LoggerUtil.debugLogger(logger,"regionId from catchRegions in StoreAddressController----"+geographyName);
    	LoggerUtil.debugLogger(logger,"brand from catchRegions in StoreAddressController----"+brand);
    	if (geographyName != null && event.getName().equals("ipc.region")) {
    		//populateGeoList(request, response, geographyName, model);
    		eventSession.setAttribute("eventGeographyName", geographyName);
    		eventSession.setAttribute("brand", brand);
		}
    	}
    }

	/**
	 * Helper method for event mapping.
	 * 
	 * @param request
	 * @param response
	 * @param geographyName
	 * @throws ReadOnlyException
	 * @throws IOException
	 * @throws ValidatorException
	 */
	public void populateGeoList(PortletRequest request, PortletResponse response, String geographyName, Model model, String brand) throws ReadOnlyException, IOException,
			ValidatorException {
		LoggerUtil.debugLogger(logger,"Helper method for event mapping for catchRegions---"+geographyName);
		this.setGeoName(geographyName);
		this.setBrand(brand);
		model.addAttribute("geographyName", geographyName);
		model.addAttribute("brand", brand);
		if(geographyName != null){
		//storeDesclist = getAllOpenStorebyGeography(geographyName, model);
		openStoreDesclist = getOpenStoreDescriptionByGeography(geographyName, model, brand);
		}
	//	model.addAttribute("storeDesclist", storeDesclist);
		model.addAttribute("openStoreDesclist", openStoreDesclist);
	}
	
	/**
	 * Helper Method to fetch stores list by geography.
	 * 
	 * @param geographyName
	 * @return list of stores.
	 */
	public List<com.coach.stores.service.beans.GetOpenStoreByGeographyResponse.Stores.Store> getAllOpenStorebyGeography(String geographyName, Model model){
		LoggerUtil.debugLogger(logger,"storeGeographyList from getAllOpenStorebyGeography--Start---"+geographyName);
		List<com.coach.stores.service.beans.GetOpenStoreByGeographyResponse.Stores.Store> storeGeographyList =
			new ArrayList<com.coach.stores.service.beans.GetOpenStoreByGeographyResponse.Stores.Store>();
		GetOpenStoreByGeographyResponse getOpenStoreByGeographyResponse = null;
		GetOpenStoreByGeographyRequest getOpenStoreByGeographyRequest = WS_CLIENT_FACTORY.createGetOpenStoreByGeographyRequest();
		if(getOpenStoreByGeographyRequest != null && webServiceTemplate != null){
			getOpenStoreByGeographyRequest.setGeographyName(geographyName);
			try{
				long entryTime = System.currentTimeMillis();
				LoggerUtil.debugLogger(logger, "entryTime in getAllOpenStorebyGeography---"+entryTime);
				getOpenStoreByGeographyResponse = (GetOpenStoreByGeographyResponse) webServiceTemplate
						.marshalSendAndReceive(getOpenStoreByGeographyRequest);
				long exitTime = System.currentTimeMillis();
				LoggerUtil.debugLogger(logger, "exitTime in getAllOpenStorebyGeography---"+exitTime);
				LoggerUtil.debugLogger(logger, "Total Time in MilliSeconds in getAllOpenStorebyGeography---"+(exitTime-entryTime));
				LoggerUtil.debugLogger(logger, "Total Time in Seconds in getAllOpenStorebyGeography---"+((exitTime-entryTime)/1000));
			}catch(SoapFaultClientException ex){
				String lpart = null;
				SoapFaultClientException soapException = (SoapFaultClientException)ex;
				lpart = soapException.getFaultCode().getLocalPart().trim();
				if(lpart != null && lpart.equals("Server")){
					model.addAttribute("FaultCode", lpart);
					model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
				}
				if(lpart != null && lpart.equals("Client")){
					model.addAttribute("FaultCode", lpart);
					model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_CLIENT);
				}
				if(lpart != null && lpart.equals("MustUnderstand")){
					model.addAttribute("FaultCode", lpart);
					model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_UNDERSTAND);
				}
				if(lpart != null && lpart.equals("VersionMismatch")){
					model.addAttribute("FaultCode", lpart);
					model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_VERMISMATCH);
				}
				LoggerUtil.errorLogger(logger, "SoapFaultClientException caught while processing Stores Web Service in StoreAddressController.getAllOpenStorebyGeography()", ex);
			}
			catch(WebServiceIOException ex){
				model.addAttribute("FaultCode", "WebserviceIOE");
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
				LoggerUtil.errorLogger(logger, "WebServiceIOException caught while processing Stores Web Service in StoreAddressController.getAllOpenStorebyGeography()", ex);
			}
			catch(Exception ex){
				if(ex instanceof SocketTimeoutException){
					model.addAttribute("FaultMessage", "SocketTimeOutException Caught");
					LoggerUtil.errorLogger(logger, "SocketTimeOutException caught while processing Stores Web Service in StoreAddressController.getAllOpenStorebyGeography()", ex);
				} else if(ex instanceof ConnectException){
					model.addAttribute("FaultMessage", "ConnectException Caught");
					LoggerUtil.errorLogger(logger, "ConnectException caught while processing Stores Web Service in StoreAddressController.getAllOpenStorebyGeography()", ex);
				} else {
				model.addAttribute("FaultCode", "WebserviceIOE");
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
				LoggerUtil.errorLogger(logger, "WebServiceIOException caught while processing Stores Web Service in StoreAddressController.getAllOpenStorebyGeography()", ex);
				}
			}
		}
		if(getOpenStoreByGeographyResponse != null && getOpenStoreByGeographyResponse.getStores() != null){
			model.addAttribute("FaultCode", null);
			storeGeographyList = getOpenStoreByGeographyResponse.getStores().getStore();
			LoggerUtil.debugLogger(logger,"storeGeographyList from getAllOpenStorebyGeography-----"+storeGeographyList.size());
		}
		return storeGeographyList;
	}
	
	/**
	 * Helper Method to fetch stores list by geography.
	 * 
	 * @param geographyName
	 * @return list of stores.
	 */
	public List<com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyResponse.StoresDesc.StoreDesc> getOpenStoreDescriptionByGeography(String geographyName, Model model, String brand){
		LoggerUtil.debugLogger(logger,"storeGeographyList from getOpenStoreDescriptionByGeography--Start---"+geographyName);
		List<com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyResponse.StoresDesc.StoreDesc> storeGeographyList =
			new ArrayList<com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyResponse.StoresDesc.StoreDesc>();
		GetOpenStoreDescriptionByGeographyResponse response = null;
		GetOpenStoreDescriptionByGeographyRequest request = WS_CLIENT_FACTORY.createGetOpenStoreDescriptionByGeographyRequest();
		if(request != null && webServiceTemplate != null){
			Geography geography = new Geography();
			geography.setGEOGRAPHY(geographyName);
			geography.setSTORETYPE(brand);
			request.setGeography(geography);
			try{
				long entryTime = System.currentTimeMillis();
				LoggerUtil.debugLogger(logger, "entryTime in getOpenStoreDescriptionByGeography---"+entryTime);
				response = (GetOpenStoreDescriptionByGeographyResponse) webServiceTemplate
						.marshalSendAndReceive(request);
				long exitTime = System.currentTimeMillis();
				LoggerUtil.debugLogger(logger, "exitTime in getOpenStoreDescriptionByGeography---"+exitTime);
				LoggerUtil.debugLogger(logger, "Total Time in MilliSeconds in getOpenStoreDescriptionByGeography---"+(exitTime-entryTime));
				LoggerUtil.debugLogger(logger, "Total Time in Seconds in getOpenStoreDescriptionByGeography---"+((exitTime-entryTime)/1000));
			}catch(SoapFaultClientException ex){
				String lpart = null;
				SoapFaultClientException soapException = (SoapFaultClientException)ex;
				lpart = soapException.getFaultCode().getLocalPart().trim();
				if(lpart != null && lpart.equals("Server")){
					model.addAttribute("FaultCode", lpart);
					model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
				}
				if(lpart != null && lpart.equals("Client")){
					model.addAttribute("FaultCode", lpart);
					model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_CLIENT);
				}
				if(lpart != null && lpart.equals("MustUnderstand")){
					model.addAttribute("FaultCode", lpart);
					model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_UNDERSTAND);
				}
				if(lpart != null && lpart.equals("VersionMismatch")){
					model.addAttribute("FaultCode", lpart);
					model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_VERMISMATCH);
				}
				LoggerUtil.errorLogger(logger, "SoapFaultClientException caught while processing Stores Web Service in StoreAddressController.getOpenStoreDescriptionByGeography()", ex);
			}
			catch(WebServiceIOException ex){
				model.addAttribute("FaultCode", "WebserviceIOE");
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
				LoggerUtil.errorLogger(logger, "WebServiceIOException caught while processing Stores Web Service in StoreAddressController.getOpenStoreDescriptionByGeography()", ex);
			}
			catch(Exception ex){
				if(ex instanceof SocketTimeoutException){
					model.addAttribute("FaultMessage", "SocketTimeOutException Caught");
					LoggerUtil.errorLogger(logger, "SocketTimeOutException caught while processing Stores Web Service in StoreAddressController.getOpenStoreDescriptionByGeography()", ex);
				} else if(ex instanceof ConnectException){
					model.addAttribute("FaultMessage", "ConnectException Caught");
					LoggerUtil.errorLogger(logger, "ConnectException caught while processing Stores Web Service in StoreAddressController.getOpenStoreDescriptionByGeography()", ex);
				} else {
				model.addAttribute("FaultCode", "WebserviceIOE");
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
				LoggerUtil.errorLogger(logger, "WebServiceIOException caught while processing Stores Web Service in StoreAddressController.getOpenStoreDescriptionByGeography()", ex);
				}
			}
		}
		if(response != null && response.getStoresDesc() != null){
			model.addAttribute("FaultCode", null);
			storeGeographyList = response.getStoresDesc().getStoreDesc();
			LoggerUtil.debugLogger(logger,"storeGeographyList from getOpenStoreDescriptionByGeography-----"+storeGeographyList.size());
		}
		return storeGeographyList;
	}
	
	/**
	 * Method used to populate states.
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@ResourceMapping("populateStates")
	public ModelAndView populateStates(ResourceRequest request, ResourceResponse response) throws IOException {
		LoggerUtil.debugLogger(logger,"Inside populateStates method in storeAddressController");
		String countryId = request.getParameter("countryId");
		LoggerUtil.debugLogger(logger,"inside populateStates -- countryId----"+countryId);
		
		String cityId = request.getParameter("cityId");
		LoggerUtil.debugLogger(logger,"inside populateStates -- cityId----"+cityId);
		
		Map<String, Object> model = new HashMap<String, Object>();
		List<String> stateList = new ArrayList<String>();
		List<String> cityStateList = new ArrayList<String>();
		
		String selectedValue = "";
		String selectedCountry = "";
		
		LoggerUtil.debugLogger(logger,"Inside populateStates method -- storeDesclist size---" + openStoreDesclist.size());
		if(openStoreDesclist.size()>0){	
			for (com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyResponse.StoresDesc.StoreDesc storedesc : openStoreDesclist) {
				if (Validator.isNotNull(storedesc)  && Validator.isNotNull(countryId) && Validator.isNotNull(storedesc.getCOUNTRY()) 
						&& storedesc.getCOUNTRY().trim().equalsIgnoreCase(countryId.trim()) && Validator.isNotNull(storedesc.getSTATE())){
					stateList.add(storedesc.getSTATE().trim());
				}
			
				if (Validator.isNotNull(storedesc) && Validator.isNotNull(cityId) && Validator.isNotNull(storedesc.getCITY()) 
						&& storedesc.getCITY().trim().equalsIgnoreCase(cityId.trim()) && Validator.isNotNull(storedesc.getSTATE())) {
					selectedCountry = Validator.isNotNull(storedesc.getCOUNTRY()) ? storedesc.getCOUNTRY().trim() : "";
					selectedValue = Validator.isNotNull(storedesc.getSTATE())? storedesc.getSTATE().trim() : "";
				}
			}
			
			LoggerUtil.debugLogger(logger,"inside populateStates ------------- selectedCountry ----"+selectedCountry);
			for (com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyResponse.StoresDesc.StoreDesc storedesc : openStoreDesclist) {
				if(Validator.isNotNull(storedesc.getCOUNTRY()) && Validator.isNotNull(selectedCountry) 
						&& storedesc.getCOUNTRY().trim().equalsIgnoreCase(selectedCountry.trim()) && Validator.isNotNull(storedesc.getSTATE())){
					cityStateList.add(storedesc.getSTATE().trim());
				}
			}
		}
		
		if(countryId != null && stateList != null && stateList.size() > 0){
			stateList =  WebUtil.removeDuplicates(stateList);
			Collections.sort(stateList);
			model.put("statesList", stateList);
			LoggerUtil.debugLogger(logger,"StateStoresList.size()--in StoreAddressController----"+stateList.size());
			return new ModelAndView("StoreStatesList", model);
		}
		else{
			if(cityId != null && cityStateList != null && cityStateList.size() > 0){
			cityStateList =  WebUtil.removeDuplicates(cityStateList);
			Collections.sort(cityStateList);
			model.put("statesList", cityStateList);
			model.put("selectedValue", selectedValue);
			LoggerUtil.debugLogger(logger,"inside populateStates -- cityStateList.size()----"+cityStateList.size());
			}
			return new ModelAndView("StoreStatesListResource", model);
			
		}
	}
	
	/**
	 * Method used to populate countries.
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@ResourceMapping("populateCountries")
	public ModelAndView populateCountries(ResourceRequest request, ResourceResponse response)throws IOException{
		LoggerUtil.debugLogger(logger,"inside populateCountries method in storeAddressController");
		String stateId = request.getParameter("stateId");
		LoggerUtil.debugLogger(logger,"inside populateCountries -- stateId----"+stateId);
		String cityName = request.getParameter("cityId");
		LoggerUtil.debugLogger(logger,"inside populateCountries -- cityName----"+cityName);
		
		String selectedValue="";
		Map<String, Object> model = new HashMap<String, Object>();
		List<String> stateCountriesList = new ArrayList<String>();
		List<String> cityCountriesList = new ArrayList<String>();
		if(openStoreDesclist != null && !openStoreDesclist.isEmpty()){
		LoggerUtil.debugLogger(logger,"Inside populateCountries method -- storeDesclist size---"+openStoreDesclist.size());
		
		for (com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyResponse.StoresDesc.StoreDesc storedesc : openStoreDesclist) {
			
			if (Validator.isNotNull(storedesc) && Validator.isNotNull(stateId) && Validator.isNotNull(storedesc.getSTATE())
					&& storedesc.getSTATE().trim().equalsIgnoreCase(stateId.trim()) && Validator.isNotNull(storedesc.getCOUNTRY()))	{
				stateCountriesList.add(WebUtil.converCamelCase(storedesc.getCOUNTRY().trim()));
				selectedValue = storedesc.getCOUNTRY().trim();
			}else{
				stateCountriesList.add(WebUtil.converCamelCase(storedesc.getCOUNTRY().trim()));
			}
			
			if (Validator.isNotNull(storedesc) && Validator.isNotNull(cityName) && Validator.isNotNull(storedesc.getCITY())
					&& storedesc.getCITY().trim().equalsIgnoreCase(cityName.trim()) && Validator.isNotNull(storedesc.getCOUNTRY())) {
				cityCountriesList.add(WebUtil.converCamelCase(storedesc.getCOUNTRY().trim()));
				selectedValue = storedesc.getCOUNTRY().trim();
			}else{
				cityCountriesList.add(WebUtil.converCamelCase(storedesc.getCOUNTRY().trim()));
			}
		}
		}
		
		if(stateCountriesList != null && !stateCountriesList.isEmpty() && stateCountriesList.size() > 0){
			stateCountriesList = WebUtil.removeDuplicates(stateCountriesList);
			Collections.sort(stateCountriesList);
			model.put("countriesList", stateCountriesList);
		}
		if(cityCountriesList != null && !cityCountriesList.isEmpty() && cityCountriesList.size() > 0){
			cityCountriesList = WebUtil.removeDuplicates(cityCountriesList);
			Collections.sort(cityCountriesList);
			model.put("countriesList", cityCountriesList);
		}
		if(selectedValue != null && !selectedValue.isEmpty()){
			model.put("selectedValue", selectedValue);
		}
		
		LoggerUtil.debugLogger(logger,"populateCountries stateCountriesList.size()--in StoreAddressController-----"+stateCountriesList.size());
		LoggerUtil.debugLogger(logger,"populateCountries cityCountriesList.size()--in StoreAddressController-----"+cityCountriesList.size());
		return new ModelAndView("storeCountriesList", model);
	}
	/**
	 * Method used to populate cities.
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@ResourceMapping("populateCities")
	public ModelAndView populateCities(ResourceRequest request, ResourceResponse response) throws IOException {
		LoggerUtil.debugLogger(logger,"inside populateCities method in storeAddressController");
		String stateId = request.getParameter("stateId");
		LoggerUtil.debugLogger(logger,"inside populateCities -- stateId----"+stateId);
		String countryId = request.getParameter("countryId");
		LoggerUtil.debugLogger(logger,"inside populateCities -- countryId----"+countryId);
		Map<String, Object> model = new HashMap<String, Object>();
		List<String> cityList = new ArrayList<String>();
		if(openStoreDesclist != null && !openStoreDesclist.isEmpty()){
		LoggerUtil.debugLogger(logger,"Inside populateCities method -- storeDesclist size---"+openStoreDesclist.size());
		
		for (com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyResponse.StoresDesc.StoreDesc storedesc : openStoreDesclist) {
			
			if (Validator.isNotNull(storedesc) && Validator.isNotNull(stateId) && Validator.isNotNull(storedesc.getSTATE())
					&& storedesc.getSTATE().trim().equalsIgnoreCase(stateId.trim()) && Validator.isNotNull(storedesc.getCITY())) {
				cityList.add(WebUtil.converCamelCase(storedesc.getCITY().trim()));
			}
		
			if (Validator.isNotNull(storedesc) && Validator.isNotNull(countryId) && Validator.isNotNull(storedesc.getCOUNTRY())
					&& storedesc.getCOUNTRY().trim().equalsIgnoreCase(countryId.trim())&& Validator.isNotNull(storedesc.getCITY())) {
				cityList.add(WebUtil.converCamelCase(storedesc.getCITY().trim()));
			}
			
		}
		}
		cityList = WebUtil.removeDuplicates(cityList);
		Collections.sort(cityList);
		LoggerUtil.debugLogger(logger,"cityStoresList.size()--in StoreAddressController-----"+cityList.size());
		model.put("cityList", cityList);
		return new ModelAndView("storeCityList", model);
	}
	
	/**
	 * Method used to populate stores.
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@ResourceMapping("populateStores")
	public ModelAndView populateStores(ResourceRequest request, ResourceResponse response) throws IOException {
		LoggerUtil.debugLogger(logger,"inside populateStores method in storeAddressController");
		String cityName = request.getParameter("cityId");
		LoggerUtil.debugLogger(logger,"inside populateStores -- cityName----"+cityName);
		String stateId = request.getParameter("stateId");
		LoggerUtil.debugLogger(logger,"stateId---inside populateStores--------->"+stateId);
		String countryId = request.getParameter("countryId");
		LoggerUtil.debugLogger(logger,"countryId---inside populateStores------->"+countryId);
		Map<String, Object> model = new HashMap<String, Object>();
		List<String> storesList = new ArrayList<String>();
		if(openStoreDesclist != null && !openStoreDesclist.isEmpty()){
		LoggerUtil.debugLogger(logger,"Inside populateStores method -- storeDesclist size---"+openStoreDesclist.size());
		
		for (com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyResponse.StoresDesc.StoreDesc storedesc : openStoreDesclist) {
			if (Validator.isNotNull(storedesc) && Validator.isNotNull(cityName) && Validator.isNotNull(storedesc.getCITY())
					&& storedesc.getCITY().trim().equalsIgnoreCase(cityName.trim()) && Validator.isNotNull(storedesc.getSTORENAME())) {
				storesList.add(WebUtil.converCamelCase(storedesc.getSTORENAME().trim()));
			}
			if(Validator.isNotNull(storedesc) && Validator.isNotNull(stateId) && Validator.isNotNull(storedesc.getSTATE())
					&& storedesc.getSTATE().trim().equalsIgnoreCase(stateId.trim()) && Validator.isNotNull(storedesc.getSTORENAME())){
					storesList.add(WebUtil.converCamelCase(storedesc.getSTORENAME().trim()));
			}
			if(Validator.isNotNull(storedesc) && Validator.isNotNull(countryId) && Validator.isNotNull(storedesc.getCOUNTRY())
					&& storedesc.getCOUNTRY().trim().equalsIgnoreCase(countryId.trim()) && Validator.isNotNull(storedesc.getSTORENAME())){
					storesList.add(WebUtil.converCamelCase(storedesc.getSTORENAME().trim()));
			}
		}
		}
		LoggerUtil.debugLogger(logger,"storesList.size()--inside populateStores---"+storesList.size());
		storesList = WebUtil.removeDuplicates(storesList);
		Collections.sort(storesList);
		LoggerUtil.debugLogger(logger,"storesList.size()--inside populateStores--After removing Duplicates----"+storesList.size());
		model.put("storesList", storesList);
		
		return new ModelAndView("storesList", model);
	}
	
	/**
	 * ActionMapping to render storeDetails view.
	 * 
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 */
	@ActionMapping(params = "myaction=displayStoreDetails")
	public void displayStoreDetails(ActionRequest actionRequest, ActionResponse actionResponse, Model model) {
		LoggerUtil.debugLogger(logger,"Inside displayStoreDetails method to display storesDetailView in StoreAddressController");
		LoggerUtil.debugLogger(logger,"Inside displayStoreDetails -- countryId----"+actionRequest.getParameter("countryId"));
		LoggerUtil.debugLogger(logger,"Inside displayStoreDetails -- stateId------"+actionRequest.getParameter("stateId"));
		LoggerUtil.debugLogger(logger,"Inside displayStoreDetails -- cityName-----"+actionRequest.getParameter("cityName"));
		LoggerUtil.debugLogger(logger,"Inside displayStoreDetails -- storeName----"+actionRequest.getParameter("storeName"));
		
		PortletSession session = actionRequest.getPortletSession();
		session.setAttribute("FromSearchEvent", null);
		StoreVO storeVO = null;
		List<StoreVO> storeList = new ArrayList<StoreVO>();
		List<Store> storeNumberlist = null;
		String storeName = actionRequest.getParameter("storeName");
		String storeNumber = "";
		/*if(storeDesclist != null && !storeDesclist.isEmpty()){
		LoggerUtil.debugLogger(logger,"Inside displayStoreDetails method -- storeDesclist size----"+storeDesclist.size());
		for (com.coach.stores.service.beans.GetOpenStoreByGeographyResponse.Stores.Store store : storeDesclist) {
			if (store.getSTORENAME().equals(storeName)) {
				storeNumber = store.getSTORENUMBER();
			}
		}
		}*/
		if(openStoreDesclist != null && !openStoreDesclist.isEmpty()){
		for (com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyResponse.StoresDesc.StoreDesc store : openStoreDesclist) {
			if (store.getSTORENAME().equals(storeName)) {
				storeNumber = store.getSTORENUMBER();
			}
		}
		}
		LoggerUtil.debugLogger(logger,"Inside displayStoreDetails -- storeNumber----"+storeNumber);
		GetOpenStoreByStoreNumberRequest getOpenStoreByStoreNumberRequest = WS_CLIENT_FACTORY.createGetOpenStoreByStoreNumberRequest();
		if(storeNumber != null && !storeNumber.isEmpty()){
			LoggerUtil.debugLogger(logger,"Inside displayStoreDetails -- storeNumber----"+storeNumber);
			getOpenStoreByStoreNumberRequest.setStrStoreNumber(storeNumber);
		}
		GetOpenStoreByStoreNumberResponse getOpenStoreByStoreNumberResponse = null;
		if(webServiceTemplate != null && getOpenStoreByStoreNumberRequest != null){
			try{
				long entryTime = System.currentTimeMillis();
				LoggerUtil.debugLogger(logger, "entryTime in getOpenStoreByStoreNumberRequest---"+entryTime);
				getOpenStoreByStoreNumberResponse = (GetOpenStoreByStoreNumberResponse)webServiceTemplate
					.marshalSendAndReceive(getOpenStoreByStoreNumberRequest);
				long exitTime = System.currentTimeMillis();
				LoggerUtil.debugLogger(logger, "exitTime in getOpenStoreByStoreNumberRequest---"+exitTime);
				LoggerUtil.debugLogger(logger, "Total Time in MilliSeconds in getOpenStoreByStoreNumberRequest---"+(exitTime-entryTime));
				LoggerUtil.debugLogger(logger, "Total Time in Seconds in getOpenStoreByStoreNumberRequest---"+((exitTime-entryTime)/1000));
			}
			catch(SoapFaultClientException ex){
				String lpart = null;
				SoapFaultClientException soapException = (SoapFaultClientException)ex;
				lpart = soapException.getFaultCode().getLocalPart().trim();
				if(lpart != null && lpart.equals("Server")){
					model.addAttribute("FaultCode", lpart);
					model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
				}
				if(lpart != null && lpart.equals("Client")){
					model.addAttribute("FaultCode", lpart);
					model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_CLIENT);
				}
				if(lpart != null && lpart.equals("MustUnderstand")){
					model.addAttribute("FaultCode", lpart);
					model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_UNDERSTAND);
				}
				if(lpart != null && lpart.equals("VersionMismatch")){
					model.addAttribute("FaultCode", lpart);
					model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_VERMISMATCH);
				}
				LoggerUtil.errorLogger(logger, "SoapFaultClientException caught while processing Stores Web Service in StoreAddressController.displayStoreDetails()", ex);
			}
			catch(WebServiceIOException ex){
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
				LoggerUtil.errorLogger(logger, "WebServiceIOException caught while processing Stores Web Service in StoreAddressController.displayStoreDetails()", ex);
			}
			catch(Exception ex){
				if(ex instanceof SocketTimeoutException){
					model.addAttribute("FaultMessage", "SocketTimeOutException Caught");
					LoggerUtil.errorLogger(logger, "SocketTimeOutException caught while processing Stores Web Service in StoreAddressController.displayStoreDetails()", ex);
				} else if(ex instanceof ConnectException){
					model.addAttribute("FaultMessage", "ConnectException Caught");
					LoggerUtil.errorLogger(logger, "ConnectException caught while processing Stores Web Service in StoreAddressController.displayStoreDetails()", ex);
				} else {
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
				LoggerUtil.errorLogger(logger, "WebServiceIOException caught while processing Stores Web Service in StoreAddressController.displayStoreDetails()", ex);
				}
			}
		}
		
		if(getOpenStoreByStoreNumberResponse != null && getOpenStoreByStoreNumberResponse.getStores() != null){
		 storeNumberlist = 	getOpenStoreByStoreNumberResponse.getStores().getStore();
		 LoggerUtil.debugLogger(logger,"Inside displayStoreDetails -- storeNumberlist size----"+storeNumberlist.size());
		}
		if (storeNumberlist != null && !storeNumberlist.isEmpty()) {
			for (Store store : storeNumberlist) {
				storeVO = new StoreVO();
				storeVO.setStorenumber(store.getSTORENUMBER());
				storeVO.setStorename(store.getSTORENAME());
				storeVO.setStorename2(store.getSTORENAME2());
				storeVO.setStoretype(store.getSTORETYPE());
				storeVO.setStoretypedescription(store.getSTORETYPEDESCRIPTION());
				storeVO.setStorelongt(store.getSTORELONGT());
				storeVO.setStorelat(store.getSTORELAT());
				storeVO.setVm(store.getVM());
				storeVO.setStatus(store.getSTATUS());
				storeVO.setAddress(store.getADDRESS());
				storeVO.setAddress2(store.getADDRESS2());
				storeVO.setZip(store.getZIP());
				storeVO.setAreamgr(store.getAREAMGR());
				storeVO.setManager1(store.getMANAGER1());
				storeVO.setManager2(store.getMANAGER2());
				storeVO.setAssistantmgr1(store.getASSISTANTMGR1());
				storeVO.setAssistantmgr2(store.getASSISTANTMGR2());
				storeVO.setAssistantmgr3(store.getASSISTANTMGR3());
				storeVO.setAssistantmgr4(store.getASSISTANTMGR4());
				storeVO.setAssistantmgr5(store.getASSISTANTMGR5());
				storeVO.setAssistantmgr6(store.getASSISTANTMGR6());
				storeVO.setAssistantmgr7(store.getASSISTANTMGR7());
				storeVO.setAssociatemgr1(store.getASSOCIATEMGR1());
				storeVO.setAssociatemgr2(store.getASSOCIATEMGR2());
				storeVO.setAssociatemgr3(store.getASSOCIATEMGR3());
				storeVO.setAssociatemgr4(store.getASSOCIATEMGR4());
				storeVO.setAssociatemgr5(store.getASSOCIATEMGR5());
				storeVO.setAssociatemgr6(store.getASSOCIATEMGR6());
				storeVO.setCity(store.getCITY());
				storeVO.setCountry(store.getCOUNTRY());
				storeVO.setDatetype(store.getLASTREMODELLEDDATE());
				storeVO.setDistrictdescription(store.getDISTRICTDESCRIPTION());
				storeVO.setDistrictmgr(store.getDISTRICTMGR());
				storeVO.setDistrictmgrvm(store.getDISTRICTMGRVM());
				storeVO.setEmail(store.getEMAIL());
				storeVO.setPhone(store.getPHONE());
				storeVO.setEnddate(store.getCLOSEDATE());
				storeVO.setStartdate(store.getOPENDATE());
				storeVO.setState(store.getSTATE());
				storeVO.setFax(store.getFAX());
				storeVO.setFloor(store.getFLOOR());
				storeVO.setGeneralmanager(store.getGENERALMANAGER());
				storeVO.setHolidayhours(store.getHOURS());
				storeVO.setHours(store.getHOURS());
				storeVO.setRegioncode(store.getREGIONCODE());
				storeVO.setRegiondescription(store.getREGIONDESCRIPTION());
				storeVO.setRegionmgr(store.getREGIONMGR());
				storeVO.setRegionmgrvm(store.getREGIONMGRVM());
				String storeDetailHeader = "";
				storeDetailHeader = store.getSTORENUMBER()+" "+store.getSTORENAME()+" "+store.getSTATE();
				storeVO.setStoreDetailHeader(storeDetailHeader);		
				storeList.add(storeVO);
			}
		}
		if(storeList  != null && !storeList.isEmpty() && storeList.size() > 0){
		LoggerUtil.debugLogger(logger,"Inside displayStoreDetails -- storeList size----"+storeList.size());
		} else {
			model.addAttribute("NOSTOREDETAILFOUND", Constants.ERROR_STORES_WS_EXC_SERVER);
		}
		model.addAttribute("storeList", storeList);
		model.addAttribute("geographyName", this.getGeoName());
		model.addAttribute("brand", this.getBrand());
		actionResponse.setRenderParameter("myaction", "displayStoreAddressDetailsForm");
	}
	
	/**
	 * Action Mapping for the storeAddress view.
	 * 
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 */
	@ActionMapping(params = "myaction=displayStoreAddressDetails")
	public void displayStoreAddressDetails(ActionRequest actionRequest, ActionResponse actionResponse, Model model) {
		LoggerUtil.debugLogger(logger,"Inside displayStoreAddressDetails method in StoreAddressController");
		actionRequest.setAttribute("geographyName", actionRequest.getParameter("geographyName"));
		actionRequest.setAttribute("brand", actionRequest.getParameter("brand"));
		//actionResponse.setRenderParameter("myaction", "displayStoreDetailsForm");
	}
	
	/**
	 * Method to get openStoresbyStoreNumber from Search Portlet.
	 * 
	 * @param request
	 * @param response
	 * @param model
	 * @throws IOException 
	 * @throws PortletException 
	 */
	@ActionMapping(params = "myaction=storeNumberActionURLFromSearch")
	public void getOpenStoreDescriptionByKeyWord(ActionRequest request, ActionResponse response, Model model) throws IOException, PortletException{
		LoggerUtil.debugLogger(logger,"Inside StoreNumberActionURL-----storeNumberFromSearch--------"+request.getParameter("storeNumberFromSearch"));
		LoggerUtil.debugLogger(logger,"Inside StoreNumberActionURL-----redirectUrl------------------"+request.getParameter("redirectUrl"));
		LoggerUtil.debugLogger(logger,"Inside StoreNumberActionURL-----geographyName----------------"+request.getParameter("geographyName"));
		LoggerUtil.debugLogger(logger,"Inside StoreNumberActionURL-----searchRedirectString---------"+request.getParameter("searchRedirectString"));
		
		ThemeDisplay themeDisplay =WebUtil.getThemeDisplay(request);
		String geographyName = "";
		String storeNumber = "";
		String redirectUrl = "";
		String searchRedirectString = "";
		String storeType = "";
		
		if(request.getParameter("geographyName") != null){
			geographyName = request.getParameter("geographyName");
		}
		if(request.getParameter("storeNumberFromSearch")!= null){
			storeNumber = request.getParameter("storeNumberFromSearch");
		}
		if(request.getParameter("redirectUrl")!= null){
			redirectUrl = request.getParameter("redirectUrl");
		}
		if(request.getParameter("searchRedirectString")!= null){
			searchRedirectString = request.getParameter("searchRedirectString");
		}
		if(request.getParameter("storeType") != null){
			storeType = request.getParameter("storeType");
		}
		 String geoURL = "/stores";
		 HashMap<String, String> searchStoresMap = new HashMap<String, String>();
		 searchStoresMap.put("geographyName", geographyName);
		 searchStoresMap.put("storeNumber", storeNumber);
		 searchStoresMap.put("redirectUrl", redirectUrl);
		 searchStoresMap.put("searchRedirectString", searchRedirectString);
		 searchStoresMap.put("storeType", storeType);
		 QName qName=new QName("http://coach.com/events", "ipc.searchStores");
		 response.setEvent(qName,searchStoresMap);
		// response.setRenderParameter("myaction", "displayStoreAddressDetailsForm");
		 StringBuffer url = new StringBuffer();
		 url.append(themeDisplay.getPathFriendlyURLPrivateGroup()+themeDisplay.getScopeGroup().getFriendlyURL());
		// url.append("/group/coach");
		 url.append(geoURL);
		 url.append("?");
		 url.append("interStorePage");
		 url.append("=");
		 url.append("true");
		 LoggerUtil.debugLogger(logger,"End of the seach action----------------------------------------"+url.toString());
		 response.sendRedirect(url.toString());
		 
		/* String geo = "";
		if(geographyName.equals("North America")){
			geo = Constants.NORTH_AMERICA1;
		}
		if(geographyName.equals("Asia")){
			geo = Constants.ASIA1;
		}
		if(geographyName.equals("Japan")){
			geo = Constants.JAPAN1;
		}
		if(geographyName.equals("China")){
			geo = Constants.CHINA1;
		}
		if(geographyName.equals("Western Europe")){
			geo = Constants.WESTERN_EUROPE1;
		}
		if(geographyName.equals("Other")){
			geo = Constants.OTHER1;
		}
		StringBuffer url = new StringBuffer();
		url.append("/group/coach");
		url.append(geo);
		url.append("?");
		url.append("interStorePage");
		url.append("=");
		url.append("true");
		LoggerUtil.debugLogger(logger,"End of the seach action----------------------------------------"+url.toString());
		response.sendRedirect(url.toString());*/
	}
	
	/**
	 * Method to catch searchStoresEvent and processing event.
	 * 
	 * @param request
	 * @param response
	 */
	@SuppressWarnings({ "unchecked", "unused" })
	@EventMapping(value="{http://coach.com/events}ipc.searchStores")
	public void searchStoresEvent(EventRequest request, EventResponse response){
		LoggerUtil.debugLogger(logger,"Event mapping triggered for getOpenStoreDescriptionByKeyWord from StoreAddressController----");
		String geographyName = "";
		String storeNumber = "";
		String redirectUrl = "";
		String searchRedirectString = "";
		String storeType = "";
		
		List<Store> searchStoreNumberlist = null;
		List<StoreVO> searchStoresList = new ArrayList<StoreVO>();
		StoreVO storeVO = null;
		
		Event event = request.getEvent();
		HashMap<String, String> map = (HashMap<String, String>)event.getValue();
		geographyName = map.get("geographyName");
		storeNumber = map.get("storeNumber");
		redirectUrl = map.get("redirectUrl");
		searchRedirectString = map.get("searchRedirectString");
		storeType = map.get("storeType");
		
		PortletSession session = request.getPortletSession();
		GetOpenStoreByStoreNumberRequest getOpenStoreByStoreNumberRequest = WS_CLIENT_FACTORY.createGetOpenStoreByStoreNumberRequest();
		if(storeNumber != null && !storeNumber.isEmpty()){
			LoggerUtil.debugLogger(logger,"Inside displayStoreDetails -- storeNumber----"+storeNumber);
			getOpenStoreByStoreNumberRequest.setStrStoreNumber(storeNumber);
		}
		GetOpenStoreByStoreNumberResponse getOpenStoreByStoreNumberResponse = null;
		if(webServiceTemplate != null && getOpenStoreByStoreNumberRequest != null){
			try{
				long entryTime = System.currentTimeMillis();
				LoggerUtil.debugLogger(logger, "entryTime in getOpenStoreByStoreNumberRequest in searchStoresEvent Method---"+entryTime);
				getOpenStoreByStoreNumberResponse = (GetOpenStoreByStoreNumberResponse)webServiceTemplate
					.marshalSendAndReceive(getOpenStoreByStoreNumberRequest);
				long exitTime = System.currentTimeMillis();
				LoggerUtil.debugLogger(logger, "exitTime in getOpenStoreByStoreNumberRequest in searchStoresEvent Method---"+exitTime);
				LoggerUtil.debugLogger(logger, "Total Time in MilliSeconds in getOpenStoreByStoreNumberRequest in searchStoresEvent Method---"+(exitTime-entryTime));
				LoggerUtil.debugLogger(logger, "Total Time in Seconds in getOpenStoreByStoreNumberRequest in searchStoresEvent Method---"+((exitTime-entryTime)/1000));
			}
			catch(SoapFaultClientException ex){
				String lpart = null;
				SoapFaultClientException soapException = (SoapFaultClientException)ex;
				lpart = soapException.getFaultCode().getLocalPart().trim();
				if(lpart != null && lpart.equals("Server")){
					session.setAttribute("FaultCode", lpart);
					session.setAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
				}
				if(lpart != null && lpart.equals("Client")){
					session.setAttribute("FaultCode", lpart);
					session.setAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_CLIENT);
				}
				if(lpart != null && lpart.equals("MustUnderstand")){
					session.setAttribute("FaultCode", lpart);
					session.setAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_UNDERSTAND);
				}
				if(lpart != null && lpart.equals("VersionMismatch")){
					session.setAttribute("FaultCode", lpart);
					session.setAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_VERMISMATCH);
				}
				LoggerUtil.errorLogger(logger, "SoapFaultClientException caught while processing Stores Web Service in StoreAddressController.searchStoresEvent()", ex);
			}
			catch(WebServiceIOException ex){
				session.setAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
				LoggerUtil.errorLogger(logger, "WebServiceIOException caught while processing Stores Web Service in StoreAddressController.searchStoresEvent()", ex);
			}
			catch(Exception ex){
				if(ex instanceof SocketTimeoutException){
					session.setAttribute("FaultMessage", "SocketTimeOutException Caught");
					LoggerUtil.errorLogger(logger, "SocketTimeoutException caught while processing Stores Web Service in StoreAddressController.searchStoresEvent()", ex);
				} else if(ex instanceof ConnectException){
					session.setAttribute("FaultMessage", "ConnectException Caught");
					LoggerUtil.errorLogger(logger, "ConnectException caught while processing Stores Web Service in StoreAddressController.searchStoresEvent()", ex);
				} else {
				session.setAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
				LoggerUtil.errorLogger(logger, "WebServiceIOException caught while processing Stores Web Service in StoreAddressController.searchStoresEvent()", ex);
				}
			}
		}
		
		if(getOpenStoreByStoreNumberResponse != null && getOpenStoreByStoreNumberResponse.getStores() != null){
			searchStoreNumberlist = getOpenStoreByStoreNumberResponse.getStores().getStore();
			LoggerUtil.debugLogger(logger,"Inside getOpenStoreDescriptionByKeyWord -- searchStoreNumberlist size----"+searchStoreNumberlist.size());
		}
		
		if (searchStoreNumberlist != null && !searchStoreNumberlist.isEmpty()) {
			for (Store store : searchStoreNumberlist) {
				storeVO = new StoreVO();
				storeVO.setStorenumber(store.getSTORENUMBER());
				storeVO.setStorename(store.getSTORENAME());
				storeVO.setStorename2(store.getSTORENAME2());
				storeVO.setStoretype(store.getSTORETYPE());
				storeVO.setStoretypedescription(store.getSTORETYPEDESCRIPTION());
				storeVO.setStorelongt(store.getSTORELONGT());
				storeVO.setStorelat(store.getSTORELAT());
				storeVO.setVm(store.getVM());
				storeVO.setStatus(store.getSTATUS());
				storeVO.setAddress(store.getADDRESS());
				storeVO.setAddress2(store.getADDRESS2());
				storeVO.setZip(store.getZIP());
				storeVO.setAreamgr(store.getAREAMGR());
				storeVO.setManager1(store.getMANAGER1());
				storeVO.setManager2(store.getMANAGER2());
				storeVO.setAssistantmgr1(store.getASSISTANTMGR1());
				storeVO.setAssistantmgr2(store.getASSISTANTMGR2());
				storeVO.setAssistantmgr3(store.getASSISTANTMGR3());
				storeVO.setAssistantmgr4(store.getASSISTANTMGR4());
				storeVO.setAssistantmgr5(store.getASSISTANTMGR5());
				storeVO.setAssistantmgr6(store.getASSISTANTMGR6());
				storeVO.setAssistantmgr7(store.getASSISTANTMGR7());
				storeVO.setAssociatemgr1(store.getASSOCIATEMGR1());
				storeVO.setAssociatemgr2(store.getASSOCIATEMGR2());
				storeVO.setAssociatemgr3(store.getASSOCIATEMGR3());
				storeVO.setAssociatemgr4(store.getASSOCIATEMGR4());
				storeVO.setAssociatemgr5(store.getASSOCIATEMGR5());
				storeVO.setAssociatemgr6(store.getASSOCIATEMGR6());
				storeVO.setCity(store.getCITY());
				storeVO.setCountry(store.getCOUNTRY());
				storeVO.setDatetype(store.getLASTREMODELLEDDATE());
				storeVO.setDistrictdescription(store.getDISTRICTDESCRIPTION());
				storeVO.setDistrictmgr(store.getDISTRICTMGR());
				storeVO.setDistrictmgrvm(store.getDISTRICTMGRVM());
				storeVO.setEmail(store.getEMAIL());
				storeVO.setPhone(store.getPHONE());
				storeVO.setEnddate(store.getCLOSEDATE());
				storeVO.setStartdate(store.getOPENDATE());
				storeVO.setState(store.getSTATE());
				storeVO.setFax(store.getFAX());
				storeVO.setFloor(store.getFLOOR());
				storeVO.setGeneralmanager(store.getGENERALMANAGER());
				storeVO.setHolidayhours(store.getHOURS());
				storeVO.setHours(store.getHOURS());
				storeVO.setRegioncode(store.getREGIONCODE());
				storeVO.setRegiondescription(store.getREGIONDESCRIPTION());
				storeVO.setRegionmgr(store.getREGIONMGR());
				storeVO.setRegionmgrvm(store.getREGIONMGRVM());
				String storeDetailHeader = "";
				storeDetailHeader = store.getSTORENUMBER()+" "+store.getSTORENAME()+" "+store.getSTATE();
				storeVO.setStoreDetailHeader(storeDetailHeader);		
				searchStoresList.add(storeVO);
			}
		}
		LoggerUtil.debugLogger(logger,"Inside getOpenStoreDescriptionByKeyWord -- searchStoresList size----"+searchStoresList.size());
		session.setAttribute("FromSearchEvent", "FromSearchEvent");
		session.setAttribute("storeList", searchStoresList);
		session.setAttribute("geographyName", geographyName);
		session.setAttribute("redirectUrl", redirectUrl);
		session.setAttribute("searchRedirectString", searchRedirectString);
	}
	
	/**
	 * Method serves as resource method to storeOffices.
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 * @throws SystemException
	 */
	@ResourceMapping("populateStoreOffices")
	public ModelAndView populateStoreOffices(ResourceRequest request, ResourceResponse response) throws IOException, SystemException {
		LoggerUtil.debugLogger(logger,"Inside populateStoreOffices method in StoreOfficesController");
		Map<String, String> modelMap = new HashMap<String, String>();
		String tempData = request.getParameter("tempData");
		modelMap.put("articleId", tempData.split("::")[0]);
		modelMap.put("articleTitle", tempData.split("::")[1]);
		modelMap.put("brand", tempData.split("::")[2]);
		return new ModelAndView("officesResource", modelMap);
	}
}
