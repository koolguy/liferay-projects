package com.coach.cip.common.dto;

import java.io.Serializable;

import com.coach.cip.model.entity.User;

/**
 * 
 * Class is used to capture the characteristics of StoreEmployeeVO.
 * 
 * @author GalaxE.
 *
 */
public class StoreEmployeeVO implements Serializable{
	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -5824474714451360374L;
	private Long storeEmployeeId;
	private StoresVO storesVO;
	private User user;
	private String employeeType;

	// Constructors

	/** default constructor */
	public StoreEmployeeVO() {
	}

	/** minimal constructor */
	public StoreEmployeeVO(Long storeEmployeeId, StoresVO storesVO, User user) {
		this.storeEmployeeId = storeEmployeeId;
		this.storesVO = storesVO;
		this.user = user;
	}

	/** full constructor */
	public StoreEmployeeVO(Long storeEmployeeId, StoresVO storesVO, User user,
			String employeeType) {
		this.storeEmployeeId = storeEmployeeId;
		this.storesVO = storesVO;
		this.user = user;
		this.employeeType = employeeType;
	}

	public Long getStoreEmployeeId() {
		return storeEmployeeId;
	}

	public void setStoreEmployeeId(Long storeEmployeeId) {
		this.storeEmployeeId = storeEmployeeId;
	}

	public StoresVO getStoresVO() {
		return storesVO;
	}

	public void setStoresVO(StoresVO storesVO) {
		this.storesVO = storesVO;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getEmployeeType() {
		return employeeType;
	}

	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}

}
