package com.coach.cip.common.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * Class is used to capture the characteristics of StoresVO.
 * 
 * @author GalaxE.
 *
 */
public class StoresVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7098689493412243257L;

	public DistrictsVO getDistrictVO() {
		return districtVO;
	}

	public void setDistrictVO(DistrictsVO districtVO) {
		this.districtVO = districtVO;
	}

	public Set<StoreTimingsVO> getStoreTimings() {
		return storeTimings;
	}

	public void setStoreTimings(Set<StoreTimingsVO> storeTimings) {
		this.storeTimings = storeTimings;
	}

	public Set<AddressVO> getAddresses() {
		return addresses;
	}

	public void setAddresses(Set<AddressVO> addresses) {
		this.addresses = addresses;
	}

	public Set<StoreEmployeeVO> getStoreEmployees() {
		return storeEmployees;
	}

	public void setStoreEmployees(Set<StoreEmployeeVO> storeEmployees) {
		this.storeEmployees = storeEmployees;
	}

	public Set<StoreContactVO> getStoreContacts() {
		return storeContacts;
	}

	public void setStoreContacts(Set<StoreContactVO> storeContacts) {
		this.storeContacts = storeContacts;
	}

	public Long getStoreId() {
		return storeId;
	}

	public void setStoreId(Long storeId) {
		this.storeId = storeId;
	}

	public String getStoreNumber() {
		return storeNumber;
	}

	public void setStoreNumber(String storeNumber) {
		this.storeNumber = storeNumber;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	public String getVm() {
		return vm;
	}

	public void setVm(String vm) {
		this.vm = vm;
	}

	public String getStoreType() {
		return storeType;
	}

	public void setStoreType(String storeType) {
		this.storeType = storeType;
	}

	private Long storeId;
	private DistrictsVO districtVO;
	private String storeNumber;
	private String storeName;
	private String vm;
	private String storeType;
	private Set<StoreTimingsVO> storeTimings = new HashSet<StoreTimingsVO>(0);
	private Set<AddressVO> addresses = new HashSet<AddressVO>(0);
	private Set<StoreEmployeeVO> storeEmployees = new HashSet<StoreEmployeeVO>(0);
	private Set<StoreContactVO> storeContacts = new HashSet<StoreContactVO>(0);

	// Constructors

	/** default constructor */
	public StoresVO() {
	}

	/** minimal constructor */
	public StoresVO(Long storeId, DistrictsVO districtVO) {
		this.storeId = storeId;
		this.districtVO = districtVO;
	}

	/** full constructor */
	public StoresVO(Long storeId, DistrictsVO districtVO, String storeNumber,
			String storeName, String vm, String storeType,
			Set<StoreTimingsVO> storeTimings, Set<AddressVO> addressVO,
			Set<StoreEmployeeVO> storeEmployeesVO,
			Set<StoreContactVO> storeContactsVO) {
		this.storeId = storeId;
		this.districtVO = districtVO;
		this.storeNumber = storeNumber;
		this.storeName = storeName;
		this.vm = vm;
		this.storeType = storeType;
		this.storeTimings = storeTimings;
		this.addresses = addressVO;
		this.storeEmployees = storeEmployeesVO;
		this.storeContacts = storeContactsVO;
	}

}
