
package com.coach.cip.portlets.controller;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * Controller class for links portlet
 * @author GalaxE.
 */
@Controller("linksPortletController")
@RequestMapping(value = "VIEW")
public class LinksPortletController extends AbstractBaseController {

	/** The Constant LOG. */
	private static final Log logger = LogFactoryUtil.getLog(LinksPortletController.class);

	/**
	 * Default Render Mapping Method.
	 * @param renderRequest
	 * @param renderResponse
	 * @param model
	 * @return view name string
	 */
	@RenderMapping
	public String showLinks(RenderRequest renderRequest, RenderResponse renderResponse, Model model) {

		LoggerUtil.debugLogger(logger, "Start of showLinks() method of LinksPortletController::");
		renderResponse.setContentType("text/html");
		LoggerUtil.debugLogger(logger, "End of showLinks() method of LinksPortletController::");
		return Constants.LINKS_PORTLET_VIEW_JSP;
	}

	/**
	 * Action Mapping Method.
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 */
	@ActionMapping(params = "myaction=linksAction")
	public void linksAction(ActionRequest actionRequest, ActionResponse actionResponse, Model model) {

	}
}
