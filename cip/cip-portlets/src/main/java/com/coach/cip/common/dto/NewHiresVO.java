package com.coach.cip.common.dto;

import java.io.Serializable;
import java.sql.Timestamp;


/**
 * Class is used to capture the characteristics of NewHiresVO.
 * 
 * @author GalaxE.
 *
 */
public class NewHiresVO implements Comparable<NewHiresVO>, Serializable{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1477663025217848242L;
	private String firstName;	
	private String lastName;
	private String designation;
	private Timestamp date;
	private String documentPath;
	private String employeeId;
	private String document;
	
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
	
		return firstName;
	}
	
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
	
		this.firstName = firstName;
	}
	
	/**
	 * @return the lastName
	 */
	public String getLastName() {
	
		return lastName;
	}
	
	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
	
		this.lastName = lastName;
	}
	
	/**
	 * @return the designation
	 */
	public String getDesignation() {
	
		return designation;
	}
	
	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
	
		this.designation = designation;
	}
	
	
	public Timestamp getDate() {
		return date;
	}

	public void setDate(Timestamp date) {
		this.date = date;
	}

	/**
	 * @return the documentPath
	 */
	public String getDocumentPath() {
	
		return documentPath;
	}

	
	/**
	 * @param documentPath the documentPath to set
	 */
	public void setDocumentPath(String documentPath) {
	
		this.documentPath = documentPath;
	}

	
	/**
	 * @return the employeeId
	 */
	public String getEmployeeId() {
	
		return employeeId;
	}

	
	/**
	 * @param employeeId the employeeId to set
	 */
	public void setEmployeeId(String employeeId) {
	
		this.employeeId = employeeId;
	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewHiresVO other = (NewHiresVO) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		}
		else if (!date.equals(other.date))
			return false;
		return true;
	}
	
	
	public int compareTo(NewHiresVO otherNewHiresVo) {

		Timestamp joiningDate = ((NewHiresVO) otherNewHiresVo).getDate();

		if (this.date != null && joiningDate != null && this.date.before(joiningDate))
			return 1;
		else if (this.date != null && joiningDate != null && this.date.after(joiningDate))
			return -1;
		else
			return 0;

	}

	public String getDocument() {
		return document;
	}

	public void setDocument(String document) {
		this.document = document;
	}

	
	

}
