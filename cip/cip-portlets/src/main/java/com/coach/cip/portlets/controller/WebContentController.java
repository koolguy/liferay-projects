
package com.coach.cip.portlets.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.xml.namespace.QName;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.coach.cip.util.WebUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portlet.asset.model.AssetCategory;
import com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;

/**
 * Controller class for All Coach News Portlet.
 * @author GalaxE
 */
@Controller("WebContentController")
@RequestMapping(value = "VIEW")
public class WebContentController extends AbstractBaseController {

	/** The Constant logger. */
	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(WebContentController.class);

	/**
	 * Default Render Mapping Method. this method is invoked.
	 * @param renderResponse
	 * @return string
	 */
	@RenderMapping
	public String showJournalArticles(RenderRequest renderRequest, RenderResponse renderResponse) {

		return Constants.WEBCONTENT_JSP;
	}

	/**
	 * Action mapping method for getting detailed webcontent
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 * @throws IOException
	 * @throws SystemException
	 * @throws PortalException
	 * @throws NumberFormatException
	 */
	@ActionMapping(params = "myaction=showArticle")
	public void detailwebcontent(ActionRequest actionRequest, ActionResponse actionResponse, Model model) throws IOException, NumberFormatException, PortalException, SystemException {

		LoggerUtil.debugLogger(logger, "Start of detailwebcontent() method----------------------------------------");
		String articleId = ParamUtil.getString(actionRequest, "articleId");
		QName qname = new QName("http://coach.com/events", "ipc.articleMap");
		HashMap<String, Object> articleMap = new HashMap<String, Object>();
		articleMap.put("articleId", Long.valueOf(articleId));
		articleMap.put("redirectUrl", WebUtil.getPortalURL(actionRequest) + Constants.HOME_URL);
		actionResponse.setEvent(qname, articleMap);
		// Auditing the action starts
		if (Validator.isNotNull(articleId)) {
			JournalArticle journalArticle = JournalArticleLocalServiceUtil.getArticle(Long.valueOf(articleId));
			AssetCategory articleCat = null;
			Locale locale = WebUtil.getThemeDisplay(actionRequest).getLocale();
			List<AssetCategory> catList =
				AssetCategoryLocalServiceUtil.getCategories(JournalArticle.class.getName(), journalArticle.getResourcePrimKey());
			if (Validator.isNotNull(catList))
				articleCat = catList.get(0);
			if(Validator.isNotNull(articleCat)){
					WebUtil.createNewsAuditRecord(
							this.getClass().getName(), Constants.COACH_NEWS_TYPE, articleCat.getTitle(locale), journalArticle.getTitle(locale));
			}
		}
		// Auditing the action ends
		actionResponse.sendRedirect(WebUtil.getPortalURL(actionRequest) + Constants.COACH_NEWS_URL + "?interPage=true&allNews=false");
		LoggerUtil.debugLogger(logger, "End of detailwebcontent()-----------------------------------------------------");
	}

	/**
	 * Action mapping method to display documents in coach news
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 * @throws IOException
	 */
	@ActionMapping(params = "myaction=showOnlyDoc")
	public void showOnlyDoc(ActionRequest actionRequest, ActionResponse actionResponse, Model model) throws IOException {

		LoggerUtil.debugLogger(logger, "Start of showOnlyDoc() method----------------------------------------");
		String documentPath = ParamUtil.getString(actionRequest, "documentPath");
		String portletTitle = ParamUtil.getString(actionRequest, "portletTitle");
		QName qname = new QName("http://coach.com/events", "ipc.docMap");
		HashMap<String, Object> docMap = new HashMap<String, Object>();

		docMap.put("documentPath", documentPath);
		docMap.put("portletTitle", portletTitle);
		actionResponse.setEvent(qname, docMap);
		actionResponse.sendRedirect(WebUtil.getPortalURL(actionRequest) + Constants.COACH_NEWS_URL + "?showOnlyDoc=true");
		LoggerUtil.debugLogger(logger, "End of showOnlyDoc()-----------------------------------------------------");
	}

	/**
	 * Action mapping method for getting detail of all webcontent
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 * @throws IOException
	 */
	@ActionMapping(params = "myaction=detailAllWebContent")
	public void detailallwebcontent(ActionRequest actionRequest, ActionResponse actionResponse, Model model) throws IOException {

		LoggerUtil.debugLogger(logger, "Start of detailallwebcontent() method----------------------------------------");
		WebUtil.createNewsAuditRecord(this.getClass().getName(), Constants.COACH_NEWS_TYPE, Constants.NOT_SELECTED, Constants.ALL);
		actionResponse.sendRedirect(WebUtil.getPortalURL(actionRequest) + Constants.COACH_NEWS_URL);
		LoggerUtil.debugLogger(logger, "End of detailallwebcontent()-----------------------------------------------------");
	}
}
