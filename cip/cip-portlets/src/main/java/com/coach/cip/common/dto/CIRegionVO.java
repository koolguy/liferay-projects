package com.coach.cip.common.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * Class is used to capture the characteristics of CIRegionVO.
 * 
 * @author GalaxE.
 *
 */
public class CIRegionVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1399898618905773771L;

	/** default constructor */
	public CIRegionVO() {
	}

	/** minimal constructor */
	public CIRegionVO(Long ciRegionId, CountryVO countryVO) {
		this.ciRegionId = ciRegionId;
		this.countryVO = countryVO;
	}

	/** full constructor */
	public CIRegionVO(Long ciRegionId, CountryVO countryVO, String regionName,
			Set<StatesVO> StatesVO) {
		this.ciRegionId = ciRegionId;
		this.countryVO = countryVO;
		this.regionName = regionName;

		this.statesVO = StatesVO;
	}
	// Fields

	private Long ciRegionId;

	/*public Long getRegionId() {
		return regionId;
	}

	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}*/

	public Long getCiRegionId() {
		return ciRegionId;
	}

	public void setCiRegionId(Long ciRegionId) {
		this.ciRegionId = ciRegionId;
	}

	public String getRegionName() {
		return regionName;
	}

	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	private CountryVO countryVO;

	private String regionName;

	private Set<StatesVO> statesVO = new HashSet<StatesVO>(0);

	// Constructors

	public Set<StatesVO> getStatesVO() {
		return statesVO;
	}

	public void setStatesVO(Set<StatesVO> statesVO) {
		this.statesVO = statesVO;
	}


	public CountryVO getCountryVO() {
		return countryVO;
	}

	public void setCountryVO(CountryVO countryVO) {
		this.countryVO = countryVO;
	}
}
