
package com.coach.cip.portlets.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.User;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;

/**
 * The Class QuickLinksController, serves the request related to the Quick Links
 * operations like Adding, Updating, Deleting and populating Quick Links form
 * data.
 * @author GalaxE.
 */

@Controller("orgSwitchController")
@RequestMapping("VIEW")
public class OrganizationSwitchController extends AbstractBaseController {

	/** The Constant LOG. */
	private static final Log logger = LogFactoryUtil.getLog(OrganizationSwitchController.class);
	private static final String CA_ORG_SWITCH_VIEW = "ca-orgSwitch-view";
	/**
	 * Default Render Mapping Method. If Specific Render mapping is not found,
	 * this mapping is invoked.
	 * @param renderRequest
	 * @param renderResponse
	 * @return view name
	 */
	@RenderMapping
	public String getOrganizations(RenderRequest renderRequest, RenderResponse renderResponse) {

		PortletSession session = renderRequest.getPortletSession();
		Map<String, String> orgMap  = (Map<String, String>) session.getAttribute("orgMap");
		if(orgMap == null){
			orgMap	= new HashMap<String, String>();
			try {
				User user = PortalUtil.getUser(renderRequest);
				List<Organization> orgList = OrganizationLocalServiceUtil.getUserOrganizations(user.getUserId());
				if (!orgList.isEmpty()) {
					// and such community has a private page
					for (Organization org : orgList) {
						if (org.hasPrivateLayouts()) {
							// go there instead
							String orgFriendlyURL = org.getGroup().getFriendlyURL();
							String language = GetterUtil.get(PropsUtil.get("default.language.["+ org.getName().replaceAll("&", "-").replaceAll(" ", "") + "]"), "en_US");
							//language = user.getLocale().getLanguage();
							String url = null;
							if(orgFriendlyURL.contains("coach")){
							//	url = "/" + language + "/group" + orgFriendlyURL + "/home";
							//	orgMap.put(LanguageUtil.get(WebUtil.getThemeDisplay(renderRequest).getLocale(), "coach-web"), url);
							//	orgMap.put(LanguageUtil.get( LocaleUtil.fromLanguageId("en_US"),"coach-web"), url);  //only English as of now
							} else{
								url = "/" + language + "/group" + orgFriendlyURL + "/home";
								orgMap.put(org.getName(), url);
							}
						}
					}
				}
				session.setAttribute("orgMap", orgMap);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return CA_ORG_SWITCH_VIEW;

	}
	
}
