package com.coach.cip.common.dto;

import java.io.Serializable;

/**
 * 
 * Class is used to capture the characteristics of AddressVO.
 * 
 * @author GalaxE.
 *
 */
public class AddressVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3978074752022471365L;
	// Fields

	private Long addressId;
	private StoresVO storesVO;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private String zip;
	private Long stateId;

	// Constructors

	/** default constructor */
	public AddressVO() {
	}

	/** minimal constructor */
	public AddressVO(Long addressId, StoresVO storesVO, String addressLine1,
			String city, String zip, Long stateId) {
		this.addressId = addressId;
		this.storesVO = storesVO;
		this.addressLine1 = addressLine1;
		this.city = city;
		this.zip = zip;
		this.stateId = stateId;
	}

	/** full constructor */
	public AddressVO(Long addressId, StoresVO storesVO, String addressLine1,
			String addressLine2, String city, String zip, Long stateId) {
		this.addressId = addressId;
		this.storesVO = storesVO;
		this.addressLine1 = addressLine1;
		this.addressLine2 = addressLine2;
		this.city = city;
		this.zip = zip;
		this.stateId = stateId;
	}

	public Long getAddressId() {
		return addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	public StoresVO getStoresVO() {
		return storesVO;
	}

	public void setStoresVO(StoresVO storesVO) {
		this.storesVO = storesVO;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}

}
