package com.coach.cip.common.dto;

import java.io.Serializable;
import java.util.List;


/**
 * Class is used to capture the characteristics of EmployeeProfileVO.
 * 
 * @author GalaxE.
 *
 */
public class EmployeeProfileVO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5414167698510635809L;
	private String empName;
	private String designation;
	private String empPhone;	
	private String fax;
	private String emailAddress;	
	private String office;
	private String buildingName;
	private String address1;
	private String address2;
	private String address3;
	private String floor;
	private String city;
	private String country;
	private String zip;
	private String empDepartment;	
	private String empGeography;	
	private String manager;
	private List<DirectReportVO> directReports;
	private String assistant;
	private String state;
	private String managerId;
	private String assistantId;	
	private String managerFirstName;
	private String managerLastname;
	private String assistantFirstName;
	private String assistantLastName;
	/**	 * @return the empName
	 */
	public String getEmpName() {
	
		return empName;
	}
	
	/**
	 * @param empName the empName to set
	 */
	public void setEmpName(String empName) {
	
		this.empName = empName;
	}
	
	/**
	 * @return the designation
	 */
	public String getDesignation() {
	
		return designation;
	}
	
	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
	
		this.designation = designation;
	}
	
	/**
	 * @return the empPhone
	 */
	public String getEmpPhone() {
	
		return empPhone;
	}
	
	/**
	 * @param empPhone the empPhone to set
	 */
	public void setEmpPhone(String empPhone) {
	
		this.empPhone = empPhone;
	}
	
	/**
	 * @return the fax
	 */
	public String getFax() {
	
		return fax;
	}
	
	/**
	 * @param fax the fax to set
	 */
	public void setFax(String fax) {
	
		this.fax = fax;
	}
	
	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {
	
		return emailAddress;
	}
	
	/**
	 * @param emailAddress the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {
	
		this.emailAddress = emailAddress;
	}
	
	/**
	 * @return the office
	 */
	public String getOffice() {
	
		return office;
	}
	
	/**
	 * @param office the office to set
	 */
	public void setOffice(String office) {
	
		this.office = office;
	}
	
	/**
	 * @return the buildingName
	 */
	public String getBuildingName() {
	
		return buildingName;
	}
	
	/**
	 * @param buildingName the buildingName to set
	 */
	public void setBuildingName(String buildingName) {
	
		this.buildingName = buildingName;
	}
	
	/**
	 * @return the address1
	 */
	public String getAddress1() {
	
		return address1;
	}
	
	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress1(String address1) {
	
		this.address1 = address1;
	}
	
	/**
	 * @return the address2
	 */
	public String getAddress2() {
	
		return address2;
	}
	
	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2) {
	
		this.address2 = address2;
	}
	
	/**
	 * @return the address3
	 */
	public String getAddress3() {
	
		return address3;
	}
	
	/**
	 * @param address3 the address3 to set
	 */
	public void setAddress3(String address3) {
	
		this.address3 = address3;
	}
	
	/**
	 * @return the floor
	 */
	public String getFloor() {
	
		return floor;
	}
	
	/**
	 * @param floor the floor to set
	 */
	public void setFloor(String floor) {
	
		this.floor = floor;
	}
	
	/**
	 * @return the city
	 */
	public String getCity() {
	
		return city;
	}
	
	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
	
		this.city = city;
	}
	
	/**
	 * @return the country
	 */
	public String getCountry() {
	
		return country;
	}
	
	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
	
		this.country = country;
	}
	
	/**
	 * @return the zip
	 */
	public String getZip() {
	
		return zip;
	}
	
	/**
	 * @param zip the zip to set
	 */
	public void setZip(String zip) {
	
		this.zip = zip;
	}
	
	/**
	 * @return the empDepartment
	 */
	public String getEmpDepartment() {
	
		return empDepartment;
	}
	
	/**
	 * @param empDepartment the empDepartment to set
	 */
	public void setEmpDepartment(String empDepartment) {
	
		this.empDepartment = empDepartment;
	}
	
	/**
	 * @return the empGeography
	 */
	public String getEmpGeography() {
	
		return empGeography;
	}
	
	/**
	 * @param empGeography the empGeography to set
	 */
	public void setEmpGeography(String empGeography) {
	
		this.empGeography = empGeography;
	}
	
	/**
	 * @return the manager
	 */
	public String getManager() {
	
		return manager;
	}
	
	/**
	 * @param manager the manager to set
	 */
	public void setManager(String manager) {
	
		this.manager = manager;
	}
	
		
	

	/**
	 * @return the assistant
	 */
	public String getAssistant() {
	
		return assistant;
	}
	
	/**
	 * @param assistant the assistant to set
	 */
	public void setAssistant(String assistant) {
	
		this.assistant = assistant;
	}
	
	public List<DirectReportVO> getDirectReports() {
		return directReports;
	}

	public void setDirectReports(List<DirectReportVO> directReports) {
		this.directReports = directReports;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getManagerId() {
		return managerId;
	}

	public void setManagerId(String managerId) {
		this.managerId = managerId;
	}

	public String getAssistantId() {
		return assistantId;
	}

	public void setAssistantId(String assistantId) {
		this.assistantId = assistantId;
	}

	public String getManagerFirstName() {
		return managerFirstName;
	}

	public void setManagerFirstName(String managerFirstName) {
		this.managerFirstName = managerFirstName;
	}

	public String getManagerLastname() {
		return managerLastname;
	}

	public void setManagerLastname(String managerLastname) {
		this.managerLastname = managerLastname;
	}

	public String getAssistantFirstName() {
		return assistantFirstName;
	}

	public void setAssistantFirstName(String assistantFirstName) {
		this.assistantFirstName = assistantFirstName;
	}

	public String getAssistantLastName() {
		return assistantLastName;
	}

	public void setAssistantLastName(String assistantLastName) {
		this.assistantLastName = assistantLastName;
	}	
	
	
	

}
