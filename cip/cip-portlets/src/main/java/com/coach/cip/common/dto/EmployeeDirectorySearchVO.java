package com.coach.cip.common.dto;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

/**
 * Class is used to capture the characteristics of EmployeeDirectorySearchVO.
 * 
 * @author GalaxE.
 *
 */
public class EmployeeDirectorySearchVO implements Comparable<EmployeeDirectorySearchVO>, Serializable{
	
	private static final long serialVersionUID = -1017025426891394303L;
	private String firstName;
	private String lastName;
	private String department;
	private String geography;
	private String title;
	private String email;
	private List<String> deptList;
	private List<String> geographyList;
	private Set<String> addressList;
	private String empFirstName;
	private String empLastName;
	private String phoneExtension;
	private String empDepartment;
	private String empLocation;
	private String emailAddress;
	private String empTitle;
	private String empId;
	private String office;
	private String empGeography;
	private String empPhone;
	private String designation;
	private String fax;
	private String manager;
	private String directReports;
	private String assistant;
	private String empName;
	private String buildingName;
	private String address1;
	private String address2;
	private String address3;
	private String floor;
	private String city;
	private String country;
	private String zip;

	/**
	 * @return the firstName
	 */
	public String getFirstName() {

		return firstName;
	}

	/**
	 * @param firstName
	 *            the firstName to set
	 */
	public void setFirstName(String firstName) {

		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {

		return lastName;
	}

	/**
	 * @param lastName
	 *            the lastName to set
	 */
	public void setLastName(String lastName) {

		this.lastName = lastName;
	}

	/**
	 * @return the department
	 */
	public String getDepartment() {

		return department;
	}

	/**
	 * @param department
	 *            the department to set
	 */
	public void setDepartment(String department) {

		this.department = department;
	}

	/**
	 * @return the empFirstName
	 */
	public String getEmpFirstName() {

		return empFirstName;
	}

	/**
	 * @param empFirstName
	 *            the empFirstName to set
	 */
	public void setEmpFirstName(String empFirstName) {

		this.empFirstName = empFirstName;
	}

	/**
	 * @return the empLastName
	 */
	public String getEmpLastName() {

		return empLastName;
	}

	/**
	 * @param empLastName
	 *            the empLastName to set
	 */
	public void setEmpLastName(String empLastName) {

		this.empLastName = empLastName;
	}

	/**
	 * @return the empDepartment
	 */
	public String getEmpDepartment() {

		return empDepartment;
	}

	/**
	 * @param empDepartment
	 *            the empDepartment to set
	 */
	public void setEmpDepartment(String empDepartment) {

		this.empDepartment = empDepartment;
	}

	/**
	 * @return the empLocation
	 */
	public String getEmpLocation() {

		return empLocation;
	}

	/**
	 * @param empLocation
	 *            the empLocation to set
	 */
	public void setEmpLocation(String empLocation) {

		this.empLocation = empLocation;
	}

	/**
	 * @return the emailAddress
	 */
	public String getEmailAddress() {

		return emailAddress;
	}

	/**
	 * @param emailAddress
	 *            the emailAddress to set
	 */
	public void setEmailAddress(String emailAddress) {

		this.emailAddress = emailAddress;
	}

	/**
	 * @return the geography
	 */
	public String getGeography() {

		return geography;
	}

	/**
	 * @param geography
	 *            the geography to set
	 */
	public void setGeography(String geography) {

		this.geography = geography;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {

		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title) {

		this.title = title;
	}

	/**
	 * @return the phoneExtension
	 */
	public String getPhoneExtension() {

		return phoneExtension;
	}

	/**
	 * @param phoneExtension
	 *            the phoneExtension to set
	 */
	public void setPhoneExtension(String phoneExtension) {

		this.phoneExtension = phoneExtension;
	}

	/**
	 * @return the empTitle
	 */
	public String getEmpTitle() {

		return empTitle;
	}

	/**
	 * @param empTitle
	 *            the empTitle to set
	 */
	public void setEmpTitle(String empTitle) {

		this.empTitle = empTitle;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {

		return email;
	}

	/**
	 * @param email
	 *            the email to set
	 */
	public void setEmail(String email) {

		this.email = email;
	}

	/**
	 * @return the empId
	 */
	public String getEmpId() {

		return empId;
	}

	/**
	 * @param empId
	 *            the empId to set
	 */
	public void setEmpId(String empId) {

		this.empId = empId;
	}

	/**
	 * @return the office
	 */
	public String getOffice() {

		return office;
	}

	/**
	 * @param office
	 *            the office to set
	 */
	public void setOffice(String office) {

		this.office = office;
	}

	/**
	 * @return the empGeography
	 */
	public String getEmpGeography() {

		return empGeography;
	}

	/**
	 * @param empGeography
	 *            the empGeography to set
	 */
	public void setEmpGeography(String empGeography) {

		this.empGeography = empGeography;
	}

	/**
	 * @return the empPhone
	 */
	public String getEmpPhone() {

		return empPhone;
	}

	/**
	 * @param empPhone
	 *            the empPhone to set
	 */
	public void setEmpPhone(String empPhone) {

		this.empPhone = empPhone;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {

		return designation;
	}

	/**
	 * @param designation
	 *            the designation to set
	 */
	public void setDesignation(String designation) {

		this.designation = designation;
	}

	/**
	 * @return the fax
	 */
	public String getFax() {

		return fax;
	}

	/**
	 * @param fax
	 *            the fax to set
	 */
	public void setFax(String fax) {

		this.fax = fax;
	}

	/**
	 * @return the manager
	 */
	public String getManager() {

		return manager;
	}

	/**
	 * @param manager
	 *            the manager to set
	 */
	public void setManager(String manager) {

		this.manager = manager;
	}

	/**
	 * @return the directReports
	 */
	public String getDirectReports() {

		return directReports;
	}

	/**
	 * @param directReports
	 *            the directReports to set
	 */
	public void setDirectReports(String directReports) {

		this.directReports = directReports;
	}

	/**
	 * @return the assistant
	 */
	public String getAssistant() {

		return assistant;
	}

	/**
	 * @param assistant
	 *            the assistant to set
	 */
	public void setAssistant(String assistant) {

		this.assistant = assistant;
	}

	/**
	 * @return the empName
	 */
	public String getEmpName() {

		return empName;
	}

	/**
	 * @param empName
	 *            the empName to set
	 */
	public void setEmpName(String empName) {

		this.empName = empName;
	}

	/**
	 * @return the addressList
	 */
	public Set<String> getAddressList() {

		return addressList;
	}

	/**
	 * @param addressList
	 *            the addressList to set
	 */
	public void setAddressList(Set<String> addressList) {

		this.addressList = addressList;
	}

	/**
	 * @return the buildingName
	 */
	public String getBuildingName() {

		return buildingName;
	}

	/**
	 * @param buildingName
	 *            the buildingName to set
	 */
	public void setBuildingName(String buildingName) {

		this.buildingName = buildingName;
	}

	/**
	 * @return the address1
	 */
	public String getAddress1() {

		return address1;
	}

	/**
	 * @param address1
	 *            the address1 to set
	 */
	public void setAddress1(String address1) {

		this.address1 = address1;
	}

	/**
	 * @return the address2
	 */
	public String getAddress2() {

		return address2;
	}

	/**
	 * @param address2
	 *            the address2 to set
	 */
	public void setAddress2(String address2) {

		this.address2 = address2;
	}

	/**
	 * @return the address3
	 */
	public String getAddress3() {

		return address3;
	}

	/**
	 * @param address3
	 *            the address3 to set
	 */
	public void setAddress3(String address3) {

		this.address3 = address3;
	}

	/**
	 * @return the floor
	 */
	public String getFloor() {

		return floor;
	}

	/**
	 * @param floor
	 *            the floor to set
	 */
	public void setFloor(String floor) {

		this.floor = floor;
	}

	/**
	 * @return the city
	 */
	public String getCity() {

		return city;
	}

	/**
	 * @param city
	 *            the city to set
	 */
	public void setCity(String city) {

		this.city = city;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {

		return country;
	}

	/**
	 * @param country
	 *            the country to set
	 */
	public void setCountry(String country) {

		this.country = country;
	}

	/**
	 * @return the zip
	 */
	public String getZip() {

		return zip;
	}

	/**
	 * @param zip
	 *            the zip to set
	 */
	public void setZip(String zip) {

		this.zip = zip;
	}

	
	/**
	 * @return the deptList
	 */
	public List<String> getDeptList() {
	
		return deptList;
	}

	
	/**
	 * @param deptList the deptList to set
	 */
	public void setDeptList(List<String> deptList) {
	
		this.deptList = deptList;
	}

	
	/**
	 * @return the geographyList
	 */
	public List<String> getGeographyList() {
	
		return geographyList;
	}

	
	/**
	 * @param geographyList the geographyList to set
	 */
	public void setGeographyList(List<String> geographyList) {
	
		this.geographyList = geographyList;
	}

	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + ((empName == null) ? 0 : empName.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeDirectorySearchVO other = (EmployeeDirectorySearchVO) obj;
		if (empName == null) {
			if (other.empName != null)
				return false;
		}
		else if (!empName.equals(other.empName))
			return false;
		return true;
	}
		
	public int compareTo(EmployeeDirectorySearchVO otherEmployeeDirectoryVO) {
		
        return this.empName.compareTo(otherEmployeeDirectoryVO.getEmpName());
	}
	
}
