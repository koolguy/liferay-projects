
package com.coach.cip.util;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

import org.springframework.context.i18n.LocaleContextHolder;

/**
 * Utility Class MessageBundleLoader
 * 
 * @author GalaxE.
 */

public class MessageBundleLoader {

	/** The Constant MESSAGE_PATH. */
	public static final String MESSAGE_PATH = "content/messages";

	/** The messages. */
	private static ResourceBundle messages;

	/**
	 * Initialize internationalization.
	 */
	private static void init() {

		Locale locale = LocaleContextHolder.getLocale();
		// assign a default locale if the faces context has none, shouldn't happen
		if (locale == null) {
			locale = Locale.ENGLISH;
		}
		messages = ResourceBundle.getBundle(MESSAGE_PATH, locale);
	}

	/**
	 * Gets a string for the given key from this resource bundle or one of its
	 * parents.
	 * @param key
	 *            the key for the desired string
	 * @return the string for the given key. If the key string value is not
	 *         found the key itself is returned.
	 */
	public static String getMessage(String key) {

		try {
			if (messages == null) {
				init();
			}
			return messages.getString(key);
		}
		// on any failure we just return the key, which should aid in debugging.
		catch (Exception e) {
			return key;
		}
	}

	/**
	 * Gets the message.
	 * @param key
	 *            the key
	 * @param messageArguments
	 *            the message arguments
	 * @return the message
	 */
	public static String getMessage(String key, Object[] messageArguments) {

		Locale locale = LocaleContextHolder.getLocale();
		// assign a default locale if the faces context has none, shouldn't happen
		if (locale == null) {
			locale = Locale.ENGLISH;
		}
		MessageFormat formatter = new MessageFormat("");
		formatter.setLocale(locale);
		formatter.applyPattern(getMessage(key));
		return formatter.format(messageArguments);
	}
}
