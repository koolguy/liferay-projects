package com.coach.cip.portlets.controller;

import java.util.HashMap;
import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;


/**
 * Class is used to capture the characteristics of notificationDetails.
 * 
 * @author GalaxE.
 *
 */

@Controller("notificationDetailController")
@RequestMapping(value = "VIEW")
public class NotificationDetailController extends AbstractBaseController {

	/** The Constant LOG. */
	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(NotificationDetailController.class);
	private static final String NOTIFICATIONS_DETAIL = "notificationDetail";
	
	/**
	 * Method used to render notifications detail view.
	 * 
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception 
	 */
	@RenderMapping
	public String showNotificationDetail(RenderRequest request,RenderResponse response, Model model) throws Exception {
		LoggerUtil.debugLogger(logger,"Render mapping for Notifications Detail view in NotificationDetailController");
		
		PortletSession notificationsSession = request.getPortletSession();
		String sysid = Validator.isNotNull(notificationsSession.getAttribute("sysid")) ? (String)notificationsSession.getAttribute("sysid") : "";
		String title = Validator.isNotNull(notificationsSession.getAttribute("title")) ? (String)notificationsSession.getAttribute("title") : "NotificationDetails";
		
		LoggerUtil.infoLogger(logger,"title -- Render mapping for notification Detail to process showNotificationDetail in NotificationDetailController----"+title);
		LoggerUtil.infoLogger(logger,"sysid -- Event mapping for notification Detail to process showNotificationDetail in NotificationDetailController----"+sysid);
		
		model.addAttribute("title", title);
		model.addAttribute("sysid", sysid);
		
		notificationsSession.setAttribute("title", null);
		notificationsSession.setAttribute("sysid", null);
		
		return NOTIFICATIONS_DETAIL;
	}
	
	/**
	 * Method used to process notifications events.
	 *  
	 * @param request
	 * @param response
	 */
	@SuppressWarnings("unchecked")
	@EventMapping(value="{http://coach.com/events}ipc.taskURL")
	public void processTasks(EventRequest request, EventResponse response){
		LoggerUtil.debugLogger(logger,"Event mapping for tasks to process notificationDetails in NotificationDetailController");
		Event event= request.getEvent();
		PortletSession notificationsSession = request.getPortletSession();
		String title = "";
		String sysid = "";
    	HashMap<String, String> notificationsMap = (HashMap<String, String>)event.getValue();
    	if(notificationsMap != null){
    		title = (String) notificationsMap.get("title");
    		sysid = (String) notificationsMap.get("sysid");
    		LoggerUtil.infoLogger(logger,"title -- Event mapping for notification Detail to process notifications in NotificationDetailController----"+title);
    		LoggerUtil.infoLogger(logger,"sysid -- Event mapping for notification Detail to process notifications in NotificationDetailController----"+sysid);
    	}
    	notificationsSession.setAttribute("title", title);
    	notificationsSession.setAttribute("sysid", sysid);
	}
}
