package com.coach.cip.common.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * Class is used to capture the characteristics of StatesVO.
 * 
 * @author GalaxE.
 *
 */
public class StatesVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5318799095033526018L;
	// Fields

	private Long stateId;

	public Long getStateId() {
		return stateId;
	}

	public void setStateId(Long stateId) {
		this.stateId = stateId;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	

	private RegionVO regionVO;
	private String stateName;

	public Set<DistrictsVO> getDistrictsSet() {
		return districtsSet;
	}

	public void setDistrictsSet(Set<DistrictsVO> districtsSet) {
		this.districtsSet = districtsSet;
	}

	private Set<DistrictsVO> districtsSet = new HashSet<DistrictsVO>(0);

	// Constructors

	/** default constructor */
	public StatesVO() {
	}

	/** minimal constructor */
	public StatesVO(Long stateId, RegionVO regionVO) {
		this.stateId = stateId;
		this.regionVO = regionVO;
	}

	/** full constructor */
	public StatesVO(Long stateId, RegionVO regionVO, String stateName,
			Set<DistrictsVO> districtsVO) {
		this.stateId = stateId;
		this.regionVO = regionVO;
		this.stateName = stateName;
		this.districtsSet = districtsVO;
	}

	public RegionVO getRegionVO() {
		return regionVO;
	}

	public void setRegionVO(RegionVO regionVO) {
		this.regionVO = regionVO;
	}
}
