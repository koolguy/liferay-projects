
package com.coach.cip.portlets.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.coach.cip.services.ServiceLocator;

/**
 * The Class AbstractBaseController, is the base class for all the coach
 * controllers. This will hold the common functionality used across the
 * portlets.
 * 
 * @author GalaxE.
 */
public class AbstractBaseController {

	/** The service locator. */
	@Autowired
	@Qualifier("serviceLocator")
	protected ServiceLocator serviceLocator;

	/**
	 * Gets the service locator.
	 * 
	 * @return the service locator
	 */
	public ServiceLocator getServiceLocator() {
		return serviceLocator;
	}

	/**
	 * Sets the service locator.
	 * 
	 * @param serviceLocator
	 *            the new service locator
	 */
	public void setServiceLocator(ServiceLocator serviceLocator) {
		this.serviceLocator = serviceLocator;
	}
}
