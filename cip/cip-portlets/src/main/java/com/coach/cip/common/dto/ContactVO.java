package com.coach.cip.common.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * Class is used to capture the characteristics of ContactVO.
 * 
 * @author GalaxE.
 *
 */
public class ContactVO implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5339868373981806181L;
	// Fields

	private Long contactId;
	private String contactType;
	private String contactNo;
	private Set<StoreContactVO> storeContactsVO = new HashSet<StoreContactVO>(0);
	private Set<DistrictContactsVO> districtContactsVO = new HashSet<DistrictContactsVO>(
			0);

	// Constructors

	/** default constructor */
	public ContactVO() {
	}

	/** minimal constructor */
	public ContactVO(Long contactId, String contactType, String contactNo) {
		this.contactId = contactId;
		this.contactType = contactType;
		this.contactNo = contactNo;
	}

	/** full constructor */
	public ContactVO(Long contactId, String contactType, String contactNo,
			Set<StoreContactVO> storeContactsVO,
			Set<DistrictContactsVO> districtContactsVO) {
		this.contactId = contactId;
		this.contactType = contactType;
		this.contactNo = contactNo;
		this.storeContactsVO = storeContactsVO;
		this.districtContactsVO = districtContactsVO;
	}

	public Long getContactId() {
		return contactId;
	}

	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}

	public String getContactType() {
		return contactType;
	}

	public void setContactType(String contactType) {
		this.contactType = contactType;
	}

	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public Set<StoreContactVO> getStoreContactsVO() {
		return storeContactsVO;
	}

	public void setStoreContactsVO(Set<StoreContactVO> storeContactsVO) {
		this.storeContactsVO = storeContactsVO;
	}

	public Set<DistrictContactsVO> getDistrictContactsVO() {
		return districtContactsVO;
	}

	public void setDistrictContactsVO(Set<DistrictContactsVO> districtContactsVO) {
		this.districtContactsVO = districtContactsVO;
	}

}
