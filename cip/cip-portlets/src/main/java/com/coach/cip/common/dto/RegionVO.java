package com.coach.cip.common.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * 
 * Class is used to capture the characteristics of RegionVO.
 * 
 * @author GalaxE.
 *
 */
public class RegionVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8444731170905351928L;
	private Long regionId;
	private Long countryId;
	private String regionCode;
	private String name;
	private Short active;
	private CountryVO countryVO;
	private String regionName;
	private Set<StatesVO> statesVO = new HashSet<StatesVO>(0);

	
	
	/** default constructor */
	public RegionVO() {
	}

	/** minimal constructor */
	public RegionVO(Long regionId, CountryVO countryVO) {
		this.regionId = regionId;
		this.countryVO = countryVO;
	}

	/** full constructor */
	public RegionVO(Long regionId, CountryVO countryVO, String regionName,
			Set<StatesVO> StatesVO) {
		this.regionId = regionId;
		this.countryVO = countryVO;
		this.regionName = regionName;
		this.statesVO = StatesVO;
	}

	/**
	 * @return the regionId
	 */
	public Long getRegionId() {
		return regionId;
	}

	/**
	 * @param regionId the regionId to set
	 */
	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}

	/**
	 * @return the countryId
	 */
	public Long getCountryId() {
		return countryId;
	}

	/**
	 * @param countryId the countryId to set
	 */
	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	/**
	 * @return the regionCode
	 */
	public String getRegionCode() {
		return regionCode;
	}

	/**
	 * @param regionCode the regionCode to set
	 */
	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the active
	 */
	public Short getActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(Short active) {
		this.active = active;
	}

	/**
	 * @return the countryVO
	 */
	public CountryVO getCountryVO() {
		return countryVO;
	}

	/**
	 * @param countryVO the countryVO to set
	 */
	public void setCountryVO(CountryVO countryVO) {
		this.countryVO = countryVO;
	}

	/**
	 * @return the regionName
	 */
	public String getRegionName() {
		return regionName;
	}

	/**
	 * @param regionName the regionName to set
	 */
	public void setRegionName(String regionName) {
		this.regionName = regionName;
	}

	/**
	 * @return the statesVO
	 */
	public Set<StatesVO> getStatesVO() {
		return statesVO;
	}

	/**
	 * @param statesVO the statesVO to set
	 */
	public void setStatesVO(Set<StatesVO> statesVO) {
		this.statesVO = statesVO;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((regionId == null) ? 0 : regionId.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RegionVO other = (RegionVO) obj;
		if (regionId == null) {
			if (other.regionId != null)
				return false;
		} else if (!regionId.equals(other.regionId))
			return false;
		return true;
	}
}
