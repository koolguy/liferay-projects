package com.coach.cip.portlets.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.portlet.Event;
import javax.portlet.EventRequest;
import javax.portlet.EventResponse;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.bind.annotation.EventMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.portlet.bind.annotation.ResourceMapping;

import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.coach.cip.util.WebUtil;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.ResourceConstants;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.asset.model.AssetEntry;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;

/**
 * The Class PDFPortletController, serves the request related to the Assets
 * operations like rendering the page, generating events to open the Assets (
 * Webcontent, PDF, Doc files etc).
 * 
 * @author GalaxE.
 */

@Controller("instructionsController")
@RequestMapping(value = "VIEW")
public class InstructionsController extends AbstractBaseController {

	/** The Constant LOG. */
	private static final Log logger = LogFactoryUtil
			.getLog(InstructionsController.class);

	/**
	 * Default Render Mapping Method. If Specific Render mapping is not found,
	 * this mapping is invoked.
	 * 
	 * @param renderResponse
	 * @return
	 */
	@RenderMapping
	public String showInstructionsPortlet(RenderResponse renderResponse) {
		return Constants.INSTRUCTIONS_JSP;
	}
	
	
	/**
	 * Event mapping to process SideNavigation event in
	 * PDFPortletController.
	 * 
	 * @param eventRequest
	 * @param eventResponse
	 */
	@SuppressWarnings("rawtypes")
	@EventMapping(value = "{http://coach.com/events}ipc.fileURL")
	public void processInstructionEvent(EventRequest eventRequest,
			EventResponse eventResponse) {
		LoggerUtil.debugLogger(logger, "Start of processSideNavigationEvent() method of PDF Portlet Controller::");
		Event event = eventRequest.getEvent();
		HashMap fileMap = (HashMap) event.getValue();

		String fileURL = "";
		String fileType = "";
		String classPK = "";
		String mimeType = "";
		String title = fileMap.get("fileTitle").toString();
		ThemeDisplay themeDisplay = WebUtil.getThemeDisplay( eventRequest);
		ResourceBundle resourceBundle = ResourceBundle.getBundle("content/Language", themeDisplay.getLocale());
		title = title+" - "+resourceBundle.getString("label.instructions.partialtitle");
		if(WebUtil.hasQandAPortletAccess(eventRequest)){
			JournalArticle journalArticle = null;
			List<AssetEntry> assetEntryList = WebUtil.fetchQandAAssetEntry(title, themeDisplay.getLocale());
			AssetEntry assetEntry = null ;
			if(null != assetEntryList && assetEntryList.size()>0)
			{
				assetEntry = assetEntryList.get(0);
				
				if (assetEntry.getClassName().endsWith("JournalArticle")) {
					try {
						/* For Webcontent */
						journalArticle = JournalArticleLocalServiceUtil.getLatestArticle(assetEntry.getClassPK());
	                        
						title = WebUtil.cropTitle(journalArticle.getTitleCurrentValue());
						fileURL = journalArticle.getArticleId();
						fileType = assetEntry.getClassName();
						classPK = String.valueOf(assetEntry.getClassPK());
						mimeType = assetEntry.getMimeType().trim();
					}
					catch (Exception e) {
						LoggerUtil.errorLogger(logger, "SideNavigation jsp displayAssetEntries() - Journal Article Exception ",e);
					}
				}/* For Document & Media. */
				/*else {
					fileURL = WebUtil.displayFileURL(assetEntry.getClassPK(), renderRequest, renderResponse, themeDisplay);
					title = WebUtil.cropTitle(assetEntry.getTitleCurrentValue());
				}*/
			}
			/* Checking the Asset accesibility oustide the network : Start */
			if(WebUtil.isExternalNetwork(eventRequest) && null != assetEntry)
			{	
			  try{ 	
				boolean assetExternalAccess = WebUtil.hasAssetExternalAccess(themeDisplay.getCompanyId(),assetEntry.getClassName(),
						ResourceConstants.SCOPE_INDIVIDUAL,String.valueOf(assetEntry.getClassPK()),0,ActionKeys.VIEW);
				if(!assetExternalAccess){
					fileURL = Constants.NO_USER_PRIVILEGE;
				}
			  }catch(Exception e) {
				  LoggerUtil.errorLogger(logger, "PDFPortlet jsp - Internal Access only Exception ",e);
				}
			} 
			/* Checking the Asset accesibility oustide the network : End */
		}
		
		eventResponse.setRenderParameter("title", title);
		eventResponse.setRenderParameter("fileURL", fileURL);
		eventResponse.setRenderParameter("fileType", fileType);
		eventResponse.setRenderParameter("classPK",classPK );
		eventResponse.setRenderParameter("mimeType",mimeType );
		
		LoggerUtil.debugLogger(logger, "End of processSideNavigationEvent() method of PDF Portlet Controller::");
	}
	
	
	@ResourceMapping("populateInstructions")
	public ModelAndView populateInstructions(ResourceRequest request, ResourceResponse response) throws IOException, SystemException 
	{
		LoggerUtil.debugLogger(logger, "Start of processSideNavigationEvent() method of PDF Portlet Controller::");
		String fileURL = "";
		String fileType = "";
		String classPK = "";
		String mimeType = "";
		String title = request.getParameter("title");
		if(WebUtil.hasQandAPortletAccess(request)){
			JournalArticle journalArticle = null;
			ThemeDisplay themeDisplay = WebUtil.getThemeDisplay( request);
			List<AssetEntry> assetEntryList = WebUtil.fetchQandAAssetEntry(title, themeDisplay.getLocale());
			AssetEntry assetEntry = null ;
			if(null != assetEntryList && assetEntryList.size()>0)
			{
				assetEntry = assetEntryList.get(0);
				
				if (assetEntry.getClassName().endsWith("JournalArticle")) {
					try {
						/* For Webcontent */
						journalArticle = JournalArticleLocalServiceUtil.getLatestArticle(assetEntry.getClassPK());
	                        
						title = WebUtil.cropTitle(journalArticle.getTitleCurrentValue());
						fileURL = journalArticle.getArticleId();
						fileType = assetEntry.getClassName();
						classPK = String.valueOf(assetEntry.getClassPK());
						mimeType = assetEntry.getMimeType().trim();
					}
					catch (Exception e) {
						LoggerUtil.errorLogger(logger, "SideNavigation jsp displayAssetEntries() - Journal Article Exception ",e);
					}
				}/* For Document & Media. */
				/*else {
					fileURL = WebUtil.displayFileURL(assetEntry.getClassPK(), renderRequest, renderResponse, themeDisplay);
					title = WebUtil.cropTitle(assetEntry.getTitleCurrentValue());
				}*/
			}
			/* Checking the Asset accesibility oustide the network : Start */
			if(WebUtil.isExternalNetwork(request) && null != assetEntry)
			{	
			  try{ 	
				boolean assetExternalAccess = WebUtil.hasAssetExternalAccess(themeDisplay.getCompanyId(),assetEntry.getClassName(),
						ResourceConstants.SCOPE_INDIVIDUAL,String.valueOf(assetEntry.getClassPK()),0,ActionKeys.VIEW);
				if(!assetExternalAccess){
					fileURL = Constants.NO_USER_PRIVILEGE;
				}
			  }catch(Exception e) {
				  LoggerUtil.errorLogger(logger, "PDFPortlet jsp - Internal Access only Exception ",e);
				}
			} 
			/* Checking the Asset accesibility oustide the network : End */
		}
		Map<String, String> modelMap = new HashMap<String, String>();
		modelMap.put("title", title);
		modelMap.put("fileURL", fileURL);
		modelMap.put("fileType",fileType);
		modelMap.put("classPK", classPK);
		modelMap.put("mimeType", mimeType);
		return new ModelAndView("populateInstructions", modelMap);
	}

}
