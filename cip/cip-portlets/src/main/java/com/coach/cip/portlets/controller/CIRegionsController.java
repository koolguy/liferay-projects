package com.coach.cip.portlets.controller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderResponse;
import javax.xml.namespace.QName;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.coach.cip.util.WebUtil;
import com.coach.stores.service.beans.GetGeographyMasterRequest;
import com.coach.stores.service.beans.GetGeographyMasterResponse;
import com.coach.stores.service.beans.GetGeographyMasterResponse.Geographies.GeographyT;
import com.coach.stores.service.beans.ObjectFactory;
/*
 * Class is used for Regions operations.
 * 
 * @author GalaxE.
 */
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.theme.ThemeDisplay;

@Controller("ciRegionsController")
@RequestMapping(value = "VIEW")
public class CIRegionsController extends AbstractBaseController {

	/** The Constant LOG. */
	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(CIRegionsController.class);
	private static final String REGIONS = "regions";
	
	@Autowired
	@Qualifier("storeServiceTemplate")
	private WebServiceTemplate webServiceTemplate;
	private static final ObjectFactory WS_CLIENT_FACTORY = new ObjectFactory();
	/**
	 * Default Constructor.
	 */
	public CIRegionsController() {

	}
	
	/**
	 * Renders regions view.
	 * 
	 * @param renderresponse
	 * @return
	 */
	@RenderMapping
	public String showRegions(RenderResponse renderresponse) {
		LoggerUtil.debugLogger(logger,"Default render mapping for showRegions view in CIRegionsController");
		return REGIONS;
	}
	
	/**
	 * Action Mapping to set and process the event.
	 * 
	 * @param request
	 * @param response
	 * @param status
	 * @throws PortletException
	 * @throws IOException
	 */
	
	@ActionMapping(value="callofficcesURL")
    public void processAction(ActionRequest request, ActionResponse response,SessionStatus status)
    		throws PortletException, IOException {
		LoggerUtil.debugLogger(logger,"callofficcesURL processAction in CIRegionsController--"+request.getParameter("regionId"));
		ThemeDisplay themeDisplay =WebUtil.getThemeDisplay(request);
    	String regionId= (String)request.getParameter("regionId");
    	String brand = (String)request.getParameter("brand");
    	HashMap<String, String> regionsMap = new HashMap<String, String>();
    	regionsMap.put("regionId", regionId);
    	regionsMap.put("brand", brand);
    	QName qName=new QName("http://coach.com/events", "ipc.region");
    	response.setEvent(qName,regionsMap);
    	String redirectUrl =themeDisplay.getPathFriendlyURLPrivateGroup()+themeDisplay.getScopeGroup().getFriendlyURL()+"/stores";
    	LoggerUtil.debugLogger(logger,"callofficcesURL processAction in CIRegionsController--"+redirectUrl);
    	response.sendRedirect(redirectUrl);
    }
		
	/**
	 * get All Geographies by brand coach.
	 * 
	 * @param model
	 */
	@ModelAttribute(value="coachGeographiesList")
	public void getGeographyMasterByBrandCoach(Model model){
		LoggerUtil.debugLogger(logger, "getGeographyMasterByBrandCoach Method Start in CIRegionController");
		List<GeographyT> coachGeograpyList =  new ArrayList<GeographyT>();
		List<String> coachGeographiesList =  new ArrayList<String>();
		GetGeographyMasterRequest getGeographyMasterRequest = WS_CLIENT_FACTORY.createGetGeographyMasterRequest();
		getGeographyMasterRequest.setBrandName(Constants.STORE_BRAND_COACH);
		GetGeographyMasterResponse getGeographyMasterResponse = null;
		if(getGeographyMasterRequest != null && getGeographyMasterRequest.getBrandName() != null && webServiceTemplate != null){
			try {
				getGeographyMasterResponse = (GetGeographyMasterResponse) webServiceTemplate.marshalSendAndReceive(getGeographyMasterRequest);
			} catch(SoapFaultClientException ex){
				String lpart = null;
				SoapFaultClientException soapException = (SoapFaultClientException)ex;
				lpart = soapException.getFaultCode().getLocalPart().trim();
				if(lpart != null && lpart.equals("Server")){
					model.addAttribute("FaultCode", lpart);
					model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
				}
				if(lpart != null && lpart.equals("Client")){
					model.addAttribute("FaultCode", lpart);
					model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_CLIENT);
				}
				if(lpart != null && lpart.equals("MustUnderstand")){
					model.addAttribute("FaultCode", lpart);
					model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_UNDERSTAND);
				}
				if(lpart != null && lpart.equals("VersionMismatch")){
					model.addAttribute("FaultCode", lpart);
					model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_VERMISMATCH);
				}
				LoggerUtil.errorLogger(logger, "SoapFaultClientException caught while processing Stores Web Service in CIRegionController", ex);
			}
			catch(WebServiceIOException ex){
				model.addAttribute("FaultCode", "WebserviceIOE");
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
				LoggerUtil.errorLogger(logger, "WebServiceIOException caught while processing Stores Web Service in CIRegionController", ex);
			}
			catch(Exception ex){
				model.addAttribute("FaultCode", "WebserviceIOE");
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
				LoggerUtil.errorLogger(logger, "WebServiceIOException caught while processing Stores Web Service in CIRegionController", ex);
			}
			
			if(getGeographyMasterResponse != null && getGeographyMasterResponse.getGeographies() != null){
				coachGeograpyList = getGeographyMasterResponse.getGeographies().getGeographyT();
				LoggerUtil.debugLogger(logger, "coachGeograpyList size in getGeographyMasterByBrand mathod in CIRegionController----"+coachGeograpyList.size());
			}
			if(coachGeograpyList != null && !coachGeograpyList.isEmpty() && coachGeograpyList.size() > 0){
				for (GeographyT geographyT : coachGeograpyList) {
					coachGeographiesList.add(geographyT.getGeography());
				}
				model.addAttribute("coachGeographiesList", coachGeographiesList);
				LoggerUtil.debugLogger(logger, "coachGeographiesList size in getGeographyMasterByBrand mathod in CIRegionController----"+coachGeographiesList.size());
			}
		}
		LoggerUtil.debugLogger(logger, "getGeographyMasterByBrandCoach Method End in CIRegionController");
	}
	
	/**
	 * get All Geographies by brand RK.
	 * 
	 * @param model
	 */
	@ModelAttribute(value="rkGeographiesList")
	public void getGeographyMasterByBrandRK(Model model){
		LoggerUtil.debugLogger(logger, "getGeographyMasterByBrandRK Method Start in CIRegionController");
		List<GeographyT> rkGeographyList =  new ArrayList<GeographyT>();
		List<String> rkGeographiesList =  new ArrayList<String>();
		GetGeographyMasterRequest getGeographyMasterRequest = WS_CLIENT_FACTORY.createGetGeographyMasterRequest();
		getGeographyMasterRequest.setBrandName(Constants.STORE_BRAND_RK);
		GetGeographyMasterResponse getGeographyMasterResponse = null;
		if(getGeographyMasterRequest != null && getGeographyMasterRequest.getBrandName() != null && webServiceTemplate != null){
			try {
				getGeographyMasterResponse = (GetGeographyMasterResponse) webServiceTemplate.marshalSendAndReceive(getGeographyMasterRequest);
			} catch(SoapFaultClientException ex){
				String lpart = null;
				SoapFaultClientException soapException = (SoapFaultClientException)ex;
				lpart = soapException.getFaultCode().getLocalPart().trim();
				if(lpart != null && lpart.equals("Server")){
					model.addAttribute("FaultCode", lpart);
					model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
				}
				if(lpart != null && lpart.equals("Client")){
					model.addAttribute("FaultCode", lpart);
					model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_CLIENT);
				}
				if(lpart != null && lpart.equals("MustUnderstand")){
					model.addAttribute("FaultCode", lpart);
					model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_UNDERSTAND);
				}
				if(lpart != null && lpart.equals("VersionMismatch")){
					model.addAttribute("FaultCode", lpart);
					model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_VERMISMATCH);
				}
				LoggerUtil.errorLogger(logger, "SoapFaultClientException caught while processing Stores Web Service in CIRegionController", ex);
			}
			catch(WebServiceIOException ex){
				model.addAttribute("FaultCode", "WebserviceIOE");
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
				LoggerUtil.errorLogger(logger, "WebServiceIOException caught while processing Stores Web Service in CIRegionController", ex);
			}
			catch(Exception ex){
				model.addAttribute("FaultCode", "WebserviceIOE");
				model.addAttribute("FaultMessage", Constants.ERROR_STORES_WS_EXC_SERVER);
				LoggerUtil.errorLogger(logger, "WebServiceIOException caught while processing Stores Web Service in CIRegionController", ex);
			}
			
			if(getGeographyMasterResponse != null && getGeographyMasterResponse.getGeographies() != null){
				rkGeographyList = getGeographyMasterResponse.getGeographies().getGeographyT();
				LoggerUtil.debugLogger(logger, "rkGeographyList size in getGeographyMasterByBrandRK mathod in CIRegionController----"+rkGeographyList.size());
			}
			if(rkGeographyList != null && !rkGeographyList.isEmpty() && rkGeographyList.size() > 0){
				for (GeographyT geographyT : rkGeographyList) {
					rkGeographiesList.add(geographyT.getGeography());
				}
				model.addAttribute("rkGeographiesList", rkGeographiesList);
				LoggerUtil.debugLogger(logger, "rkGeographiesList size in getGeographyMasterByRK mathod in CIRegionController----"+rkGeographiesList.size());
			}
		}
		LoggerUtil.debugLogger(logger, "getGeographyMasterByBrandRK Method End in CIRegionController");
	}
}
