
package com.coach.cip.common.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Class is used to capture the characteristics of personal messages and
 * associated entities. This is a reusable class for the personal messages.
 * @author GalaxE.
 */
public class PersonalMessageVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -180435144815482248L;
	private Long messageId;
	private UserVO userByModifiedBy;
	private UserVO userByCreatedBy;
	private Timestamp createDate;
	private Timestamp modifiedDate;
	private String subject;
	private String message;
	private String displayDate;
	private String expirationDate;
	private String messageType;
	private Set<MessageForLocationVO> messageForLocations = new HashSet<MessageForLocationVO>(0);
	private Set<RoleVO> messageForRoles = new HashSet<RoleVO>(0);
	private String displayDay;
	private String displayMonth;
	private String displayYear;
	private String expiryDay;
	private String expiryMonth;
	private String expiryYear;
	private boolean readMore;
	private String modifiedMessage;

	/**
	 * Method used to get the messageId.
	 * @return messageId.
	 */
	public Long getMessageId() {

		return messageId;
	}

	/**
	 * Method used to set the messageId.
	 * @param messageId
	 *            .
	 */
	public void setMessageId(Long messageId) {

		this.messageId = messageId;
	}

	/**
	 * Method used to get the userVO object.
	 * @return userVO Object.
	 */
	public UserVO getUserByModifiedBy() {

		return userByModifiedBy;
	}

	/**
	 * Method used to set the userVo object.
	 * @param userByModifiedBy
	 *            .
	 */
	public void setUserByModifiedBy(UserVO userByModifiedBy) {

		this.userByModifiedBy = userByModifiedBy;
	}

	/**
	 * Method used to get the userVO object.
	 * @return userVO Object.
	 */
	public UserVO getUserByCreatedBy() {

		return userByCreatedBy;
	}

	/**
	 * Method used to set the userVo object.
	 * @param userByCreatedBy
	 */
	public void setUserByCreatedBy(UserVO userByCreatedBy) {

		this.userByCreatedBy = userByCreatedBy;
	}

	/**
	 * Method used to get the time stamp.
	 * @return time stamp for createDate.
	 */
	public Timestamp getCreateDate() {

		return createDate;
	}

	/**
	 * Method used to set the time stamp.
	 * @param createDate
	 *            .
	 */
	public void setCreateDate(Timestamp createDate) {

		this.createDate = createDate;
	}

	/**
	 * Method used to get the time stamp.
	 * @return time stamp for modifiedDate
	 */
	public Timestamp getModifiedDate() {

		return modifiedDate;
	}

	/**
	 * Method used to set the time stamp.
	 * @param modifiedDate
	 *            .
	 */
	public void setModifiedDate(Timestamp modifiedDate) {

		this.modifiedDate = modifiedDate;
	}

	/**
	 * Method used to get the subject.
	 * @return subject.
	 */
	public String getSubject() {

		return subject;
	}

	/**
	 * Method used to set the subject.
	 * @param subject
	 */
	public void setSubject(String subject) {

		this.subject = subject;
	}

	/**
	 * Method used to get the message.
	 * @return message.
	 */
	public String getMessage() {

		return message;
	}

	/**
	 * Method used to set the message.
	 * @param message
	 *            .
	 */
	public void setMessage(String message) {

		this.message = message;
	}

	/**
	 * Method used to get the displayDate.
	 * @return displayDate.
	 */
	public String getDisplayDate() {

		return displayDate;
	}

	/**
	 * Method used to set the displayDate.
	 * @param displayDate
	 *            .
	 */
	public void setDisplayDate(String displayDate) {

		this.displayDate = displayDate;
	}

	/**
	 * Method used to get expirationDate.
	 * @return expirationDate.
	 */
	public String getExpirationDate() {

		return expirationDate;
	}

	/**
	 * Method used to set the expirationDate.
	 * @param expirationDate
	 *            .
	 */
	public void setExpirationDate(String expirationDate) {

		this.expirationDate = expirationDate;
	}

	/**
	 * Method used to get the messageType.
	 * @return messageType.
	 */
	public String getMessageType() {

		return messageType;
	}

	/**
	 * Method used to set the messageType.
	 * @param messageType
	 */
	public void setMessageType(String messageType) {

		this.messageType = messageType;
	}

	/**
	 * Method used to get the messageForLocaions.
	 * @return the messageForLocations Set.
	 */
	public Set<MessageForLocationVO> getMessageForLocations() {

		return messageForLocations;
	}

	/**
	 * Method used to set messageForLocations.
	 * @param messageForLocations
	 *            .
	 */
	public void setMessageForLocations(Set<MessageForLocationVO> messageForLocations) {

		this.messageForLocations = messageForLocations;
	}

	/**
	 * Method to get the messageForRoles.
	 * @return the messageForRoles Set.
	 */
	public Set<RoleVO> getMessageForRoles() {

		return messageForRoles;
	}

	/**
	 * Method used to set the messageForRoles.
	 * @param the
	 *            messageForRoles Set
	 */
	public void setMessageForRoles(Set<RoleVO> messageForRoles) {

		this.messageForRoles = messageForRoles;
	}

	/**
	 * Method used to get displayDay.
	 * @return displayDay.
	 */
	public String getDisplayDay() {

		return displayDay;
	}

	/**
	 * Method used to set displayDay.
	 * @param displayDay
	 */
	public void setDisplayDay(String displayDay) {

		this.displayDay = displayDay;
	}

	/**
	 * Method used to get displayMonth.
	 * @return displayMonth.
	 */
	public String getDisplayMonth() {

		return displayMonth;
	}

	/**
	 * Method used to set displayMonth.
	 * @param displayMonth
	 */
	public void setDisplayMonth(String displayMonth) {

		this.displayMonth = displayMonth;
	}

	/**
	 * Method used to get displayYear.
	 * @return displayYear.
	 */
	public String getDisplayYear() {

		return displayYear;
	}

	/**
	 * Method used to set displayYear.
	 * @param displayYear
	 */
	public void setDisplayYear(String displayYear) {

		this.displayYear = displayYear;
	}

	/**
	 * Method used to get expiryDay.
	 * @return expiryDay.
	 */
	public String getExpiryDay() {

		return expiryDay;
	}

	/**
	 * Method used to set expiryDay.
	 * @param expiryDay
	 */
	public void setExpiryDay(String expiryDay) {

		this.expiryDay = expiryDay;
	}

	/**
	 * Method used to get the expiryMonth.
	 * @return expiryMonth.
	 */
	public String getExpiryMonth() {

		return expiryMonth;
	}

	/**
	 * Method used to set the expiryMonth.
	 * @param expiryMonth
	 */
	public void setExpiryMonth(String expiryMonth) {

		this.expiryMonth = expiryMonth;
	}

	/**
	 * Method used to get the expiryYear.
	 * @return expiryYear.
	 */
	public String getExpiryYear() {

		return expiryYear;
	}

	/**
	 * Method used to set the expiryYear.
	 * @param expiryYear
	 */
	public void setExpiryYear(String expiryYear) {

		this.expiryYear = expiryYear;
	}

	/**
	 * Method used to get the readMore option.
	 * @return readMore.
	 */
	public boolean isReadMore() {

		return readMore;
	}

	/**
	 * Method used to set the readMore option.
	 * @param readMore
	 */
	public void setReadMore(boolean readMore) {

		this.readMore = readMore;
	}

	/**
	 * Method used to get ModifiedMessage
	 * @return modifiedMessage
	 */
	public String getModifiedMessage() {

		return modifiedMessage;
	}

	/**
	 * Method used to set the ModifiedMessage
	 * @param modifiedMessage
	 */
	public void setModifiedMessage(String modifiedMessage) {

		this.modifiedMessage = modifiedMessage;
	}

}
