
package com.coach.cip.common.dto;

import java.io.Serializable;

/**
 * Class is used to capture the characteristics of Quick Links and associated
 * entities. This is a reusable class for the Quick Links.
 * @author GalaxE.
 */
public class QuickLinksVO implements Comparable<QuickLinksVO>, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3009070774981790808L;

	/**
	 * Holds the Quick Links Id.
	 */
	private long quicklinkId;

	/**
	 * The pageName is the alias name for the page.
	 */
	private String pageName;

	/**
	 * The pageURL is the URL for the page.
	 */
	private String pageURL;

	/**
	 * Associated user for this page.
	 */
	private UserVO user;
	
	/**
	 * The assetURL is the URL for the asset.
	 */
	private String assetURL;

	/**
	 * Method used to get the quicklinkId.
	 * @return the quicklinkId
	 */
	public long getQuicklinkId() {

		return quicklinkId;
	}

	/**
	 * Method used to set the quicklinkId.
	 * @param quicklinkId
	 *            the quicklinkId to set
	 */
	public void setQuicklinkId(long quicklinkId) {

		this.quicklinkId = quicklinkId;
	}

	/**
	 * Method used to get the pageName.
	 * @return the pageName
	 */
	public String getPageName() {

		return pageName;
	}

	/**
	 * Method used to set the pageName.
	 * @param pageName
	 *            the pageName to set
	 */
	public void setPageName(String pageName) {

		this.pageName = pageName;
	}

	/**
	 * Method used to get the pageURL.
	 * @return the pageURL
	 */
	public String getPageURL() {

		return pageURL;
	}

	/**
	 * Method used to set the pageURL.
	 * @param pageURL
	 *            the pageURL to set
	 */
	public void setPageURL(String pageURL) {

		this.pageURL = pageURL;
	}

	/**
	 * Method used to get the user object.
	 * @return the user
	 */
	public UserVO getUser() {

		return user;
	}

	/**
	 * Method used to set the user object.
	 * @param user
	 *            the user to set
	 */
	public void setUser(UserVO user) {

		this.user = user;
	}

	public String getAssetURL() {
		return assetURL;
	}

	public void setAssetURL(String assetURL) {
		this.assetURL = assetURL;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {

		final int prime = 31;
		int result = 1;
		result = prime * result + ((pageURL == null) ? 0 : pageURL.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {

		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QuickLinksVO other = (QuickLinksVO) obj;
		if (pageURL == null) {
			if (other.pageURL != null)
				return false;
		}
		else if (!pageURL.equals(other.pageURL))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(QuickLinksVO o) {

		return this.pageURL.compareTo(o.getPageURL());
	}

}
