package com.coach.cip.portlets.controller;

import java.util.ArrayList;
import java.util.List;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.coach.cip.common.dto.EmployeeDirectorySearchVO;
import com.coach.cip.util.Constants;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * Controller class for Employee Search.
 *
 * @author GalaxE.
 *
 */

@Controller("employeeSearchController")
@RequestMapping("VIEW")
public class EmployeeSearchController extends AbstractBaseController {

	/** The Log Constant */
	private static final Log logger = LogFactoryUtil.getLog(EmployeeSearchController.class);
	
	List<EmployeeDirectorySearchVO> searchVoList = new ArrayList<EmployeeDirectorySearchVO>();
	
	/**
	 * Default Constructor.
	 */
	public EmployeeSearchController() {

	}

	/**
	 * Showing Employee Search.
	 * 
	 * @param renderResponse the render response
	 * @return navigation string {employeeSearch}
	 * @throws SystemException
	 * @throws PortalException
	 */
	@RenderMapping
	public String showEmployeeDirectory(RenderRequest request,RenderResponse renderResponse, Model model) throws PortalException,SystemException 
	{
			return Constants.EMPLOYEE_SEARCH;
	}
	
}
