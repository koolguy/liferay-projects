
package com.coach.cip.portlets.controller;

import java.io.IOException;

import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * Controller class for Static Video Portlet
 * @author GalaxE.
 */

@Controller("staticVideoController")
@RequestMapping(value = "VIEW")
public class StaticVideoController extends AbstractBaseController {

	/** The Constant LOG. */
	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(StaticVideoController.class);

	/**
	 * Renders Video Player View.
	 * @param request
	 * @param response
	 * @return
	 * @throws IOException
	 */
	@RenderMapping
	public String showVideo(RenderRequest request, RenderResponse response) throws IOException {

		LoggerUtil.debugLogger(logger, "Start of showVideo() method of StaticVideoController");
		String jspPage = request.getParameter("jspPage");
		if (jspPage != null && jspPage.equals(Constants.VIDEO_JSP)) {
			return Constants.VIDEO_JSP;
		}
		else {
			return Constants.STATIC_VIDEO_VIEW_JSP;
		}

	}
}
