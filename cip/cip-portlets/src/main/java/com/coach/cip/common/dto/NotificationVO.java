
package com.coach.cip.common.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

/**
 * Class is used to capture the characteristics of notifications and
 * associated entities. This is a reusable class for the notifications.
 * @author GalaxE.
 */
public class NotificationVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5864742289642404755L;
	private Long notificationId;
	private UserVO userByModifiedBy;
	private UserVO userByCreatedBy;
	private Timestamp createDate;
	private Timestamp modifiedDate;
	private String notificationSubject;
	private String notificationDescription;
	private String displayDate;
	private String expirationDate;
	private String notificationType;
	private Set<NotificationForLocationVO> notificationForLocations = new HashSet<NotificationForLocationVO>(0);
	private Set<RoleVO> notificationForRoles = new HashSet<RoleVO>(0);
	private Set<UserVO> NotificationForUsers = new HashSet<UserVO>(0);
	private String displayDay;
	private String displayMonth;
	private String displayYear;
	private String expiryDay;
	private String expiryMonth;
	private String expiryYear;
	private boolean readMore;
	private String modifiedNotification;
	private String notificationUrl;

	/**
	 * Method used to get the notificationId.
	 * @return notificationId.
	 */
	public Long getNotificationId() {

		return notificationId;
	}

	/**
	 * Method used to set the notificationId.
	 * @param notificationId
	 *            .
	 */
	public void setNotificationId(Long notificationId) {

		this.notificationId = notificationId;
	}

	/**
	 * Method used to get the userVO object.
	 * @return userVO Object.
	 */
	public UserVO getUserByModifiedBy() {

		return userByModifiedBy;
	}

	/**
	 * Method used to set the userVo object.
	 * @param userByModifiedBy
	 *            .
	 */
	public void setUserByModifiedBy(UserVO userByModifiedBy) {

		this.userByModifiedBy = userByModifiedBy;
	}

	/**
	 * Method used to get the userVO object.
	 * @return userVO Object.
	 */
	public UserVO getUserByCreatedBy() {

		return userByCreatedBy;
	}

	/**
	 * Method used to set the userVo object.
	 * @param userByCreatedBy
	 */
	public void setUserByCreatedBy(UserVO userByCreatedBy) {

		this.userByCreatedBy = userByCreatedBy;
	}

	/**
	 * Method used to get the time stamp.
	 * @return time stamp for createDate.
	 */
	public Timestamp getCreateDate() {

		return createDate;
	}

	/**
	 * Method used to set the time stamp.
	 * @param createDate
	 *            .
	 */
	public void setCreateDate(Timestamp createDate) {

		this.createDate = createDate;
	}

	/**
	 * Method used to get the time stamp.
	 * @return time stamp for modifiedDate
	 */
	public Timestamp getModifiedDate() {

		return modifiedDate;
	}

	/**
	 * Method used to set the time stamp.
	 * @param modifiedDate
	 *            .
	 */
	public void setModifiedDate(Timestamp modifiedDate) {

		this.modifiedDate = modifiedDate;
	}

	/**
	 * Method used to get the notificationSubject.
	 * @return notificationSubject.
	 */
	public String getNotificationSubject() {

		return notificationSubject;
	}

	/**
	 * Method used to set the notificationSubject.
	 * @param notificationSubject
	 */
	public void setNotificationSubject(String notificationSubject) {

		this.notificationSubject = notificationSubject;
	}

	/**
	 * Method used to get the notificationDescription.
	 * @return notificationDescription.
	 */
	public String getNotificationDescription() {

		return notificationDescription;
	}

	/**
	 * Method used to set the notificationDescription.
	 * @param notificationDescription
	 *            .
	 */
	public void setNotificationDescription(String notificationDescription) {

		this.notificationDescription = notificationDescription;
	}

	/**
	 * Method used to get the displayDate.
	 * @return displayDate.
	 */
	public String getDisplayDate() {

		return displayDate;
	}

	/**
	 * Method used to set the displayDate.
	 * @param displayDate
	 *            .
	 */
	public void setDisplayDate(String displayDate) {

		this.displayDate = displayDate;
	}

	/**
	 * Method used to get expirationDate.
	 * @return expirationDate.
	 */
	public String getExpirationDate() {

		return expirationDate;
	}

	/**
	 * Method used to set the expirationDate.
	 * @param expirationDate
	 *            .
	 */
	public void setExpirationDate(String expirationDate) {

		this.expirationDate = expirationDate;
	}

	/**
	 * Method used to get the notificationType.
	 * @return notificationType.
	 */
	public String getNotificationType() {

		return notificationType;
	}

	/**
	 * Method used to set the notificationType.
	 * @param notificationType
	 */
	public void setNotificationType(String notificationType) {

		this.notificationType = notificationType;
	}

	/**
	 * Method used to get the notificationForLocations.
	 * @return the notificationForLocation Set.
	 */
	public Set<NotificationForLocationVO> getNotificationForLocations() {

		return notificationForLocations;
	}

	/**
	 * Method used to set notificationForLocations.
	 * @param notificationForLocations
	 *            .
	 */
	public void setNotificationForLocations(Set<NotificationForLocationVO> notificationForLocations) {

		this.notificationForLocations = notificationForLocations;
	}

	/**
	 * Method to get the notificationForRoles.
	 * @return the notificationForRoles Set.
	 */
	public Set<RoleVO> getNotificationForRoles() {

		return notificationForRoles;
	}

	/**
	 * Method used to set the notificationForRoles.
	 * @param the
	 *            notificationForRoles Set
	 */
	public void setNotificationForRoles(Set<RoleVO> notificationForRoles) {

		this.notificationForRoles = notificationForRoles;
	}

	/**
	 * Method used to get displayDay.
	 * @return displayDay.
	 */
	public String getDisplayDay() {

		return displayDay;
	}

	/**
	 * Method used to set displayDay.
	 * @param displayDay
	 */
	public void setDisplayDay(String displayDay) {

		this.displayDay = displayDay;
	}

	/**
	 * Method used to get displayMonth.
	 * @return displayMonth.
	 */
	public String getDisplayMonth() {

		return displayMonth;
	}

	/**
	 * Method used to set displayMonth.
	 * @param displayMonth
	 */
	public void setDisplayMonth(String displayMonth) {

		this.displayMonth = displayMonth;
	}

	/**
	 * Method used to get displayYear.
	 * @return displayYear.
	 */
	public String getDisplayYear() {

		return displayYear;
	}

	/**
	 * Method used to set displayYear.
	 * @param displayYear
	 */
	public void setDisplayYear(String displayYear) {

		this.displayYear = displayYear;
	}

	/**
	 * Method used to get expiryDay.
	 * @return expiryDay.
	 */
	public String getExpiryDay() {

		return expiryDay;
	}

	/**
	 * Method used to set expiryDay.
	 * @param expiryDay
	 */
	public void setExpiryDay(String expiryDay) {

		this.expiryDay = expiryDay;
	}

	/**
	 * Method used to get the expiryMonth.
	 * @return expiryMonth.
	 */
	public String getExpiryMonth() {

		return expiryMonth;
	}

	/**
	 * Method used to set the expiryMonth.
	 * @param expiryMonth
	 */
	public void setExpiryMonth(String expiryMonth) {

		this.expiryMonth = expiryMonth;
	}

	/**
	 * Method used to get the expiryYear.
	 * @return expiryYear.
	 */
	public String getExpiryYear() {

		return expiryYear;
	}

	/**
	 * Method used to set the expiryYear.
	 * @param expiryYear
	 */
	public void setExpiryYear(String expiryYear) {

		this.expiryYear = expiryYear;
	}

	/**
	 * Method used to get the readMore option.
	 * @return readMore.
	 */
	public boolean isReadMore() {

		return readMore;
	}

	/**
	 * Method used to set the readMore option.
	 * @param readMore
	 */
	public void setReadMore(boolean readMore) {

		this.readMore = readMore;
	}

	/**
	 * Method used to get modifiedNotification
	 * @return modifiedNotification
	 */
	public String getModifiedNotification() {

		return modifiedNotification;
	}

	/**
	 * Method used to set the modifiedNotification
	 * @param modifiedNotification
	 */
	public void setModifiedNotification(String modifiedNotification) {

		this.modifiedNotification = modifiedNotification;
	}

	/**
	 * @return the notificationUrl
	 */
	public String getNotificationUrl() {
		return notificationUrl;
	}

	/**
	 * @param notificationUrl the notificationUrl to set
	 */
	public void setNotificationUrl(String notificationUrl) {
		this.notificationUrl = notificationUrl;
	}

	/**
	 * @return the notificationForUsers
	 */
	public Set<UserVO> getNotificationForUsers() {
		return NotificationForUsers;
	}

	/**
	 * @param notificationForUsers the notificationForUsers to set
	 */
	public void setNotificationForUsers(Set<UserVO> notificationForUsers) {
		NotificationForUsers = notificationForUsers;
	}

	
	
}
