package com.coach.cip.portlets.controller;

import java.io.IOException;
import java.io.StringWriter;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;

import javax.mail.internet.InternetAddress;
import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;
import org.springframework.web.util.WebUtils;

import com.coach.cip.model.entity.CIFeedback;
import com.coach.cip.services.FeedbackService;
import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.coach.cip.util.WebUtil;
import com.liferay.mail.service.MailServiceUtil;
import com.liferay.portal.kernel.language.LanguageUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.ParamUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;

/**
 * Controller class for Share your feedback and Report a problem.
 *
 *
 */

@Controller("feedbackController")
@RequestMapping("VIEW")
public class FeedbackController extends AbstractBaseController {

	/** The Constant LOG. */

	private static final Log logger = LogFactoryUtil
			.getLog(FeedbackController.class);

	/**
	 * Default Render Mapping Method.
	 *
	 * @param renderRequest
	 * @param renderResponse
	 * @return view name
	 */
	@RenderMapping
	public String showFeedback(RenderRequest renderRequest,
			RenderResponse renderResponse, Model model) {
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		String keyValue = "";
		ResourceBundle resourceBundle = ResourceBundle.getBundle(
				"content/Language", themeDisplay.getLocale());
		HttpServletRequest httpReq = PortalUtil
				.getOriginalServletRequest(PortalUtil
						.getHttpServletRequest(renderRequest));
		if (httpReq.getParameter("key") != null) {
			keyValue = httpReq.getParameter("key");
			WebUtils.setSessionAttribute(httpReq, "key", keyValue);
		} else {
			String currentURL =  PortalUtil.getCurrentURL(renderRequest).toLowerCase();
			if(currentURL.contains("report"))
			{
				keyValue = "report";
				WebUtils.setSessionAttribute(httpReq, "key", keyValue);
			}else if(currentURL.contains("share"))
			{
				keyValue = "share";
				WebUtils.setSessionAttribute(httpReq, "key", keyValue);
			}else
			{	
				keyValue = (String) WebUtils.getSessionAttribute(httpReq, "key");
			}	
		}

		if (keyValue != null && (!(keyValue.equals("")))
				&& keyValue.equals("share")) {
			renderResponse.setTitle(resourceBundle
					.getString("share-your-feedback"));
			model.addAttribute("title", "share-your-feedback");
		} else if (keyValue != null && (!(keyValue.equals("")))
				&& keyValue.equals("report")) {
			renderResponse.setTitle(resourceBundle
					.getString("report-a-problem"));
			model.addAttribute("title", "report-a-problem");
			
			
		}
		return Constants.FEEDBACK_JSP;
	}

	/**
	 * Action Mapping Method for adding feedback and redirecting to the
	 * respective feedback view.
	 *
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 * @throws IOException
	 * @throws PortletException
	 */
	@ActionMapping(params = "myaction=submitFeedback")
	public void addFeedback(ActionRequest actionRequest,
			ActionResponse actionResponse, Model model) throws IOException,
			PortletException {
		
		
		LoggerUtil.debugLogger(logger, "From addFeedback()");
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest
				.getAttribute(WebKeys.THEME_DISPLAY);
		
		ResourceBundle resourceBundle = ResourceBundle.getBundle(
				"content/Language", themeDisplay.getLocale());
		String firstName = ParamUtil.getString(actionRequest, "firstName");
		String lastName = ParamUtil.getString(actionRequest, "lastName");
		String emailAddress = ParamUtil
				.getString(actionRequest, "emailAddress");
		String phoneNumber = ParamUtil.getString(actionRequest, "phoneNumber");
		String comments = ParamUtil.getString(actionRequest, "comments");
		String topic = ParamUtil.getString(actionRequest, "feedbackTopic");
		String messageSubject = "";
		String keyValue = "";
		LoggerUtil.infoLogger(logger, "comments from addFeedback() method in Feedback controller---------->"+comments);
		LoggerUtil.infoLogger(logger, "fromFirstName from getMailBody() method in Feedback controller---------->"+firstName);
		try {
			FeedbackService feedbackService = serviceLocator
					.getFeedbackService();
			CIFeedback cifeedback = new CIFeedback();
			cifeedback.setFirstName(firstName);
			cifeedback.setLastName(lastName);
			cifeedback.setEmailAddress(emailAddress);
			cifeedback.setPhoneNumber(phoneNumber);
			cifeedback.setComments(comments);
			cifeedback.setTopic(topic);
			feedbackService.addFeedback(cifeedback);

			HttpServletRequest httpReq = PortalUtil
					.getOriginalServletRequest(PortalUtil
							.getHttpServletRequest(actionRequest));
			keyValue = (String) WebUtils.getSessionAttribute(httpReq, "key");

			if (keyValue != null && (!(keyValue.equals("")))
					&& keyValue.equals("share")) {
				LoggerUtil.debugLogger(logger,
						"User feedback added successfully.");
				messageSubject = resourceBundle
						.getString("share-your-feedback");
				SessionMessages.add(actionRequest, "success-feedback-key");
				model.addAttribute("message", "success-feedback-key");
			} else if (keyValue != null && (!(keyValue.equals("")))
					&& keyValue.equals("report")) {
				LoggerUtil.debugLogger(logger,
						"User report added successfully.");
				messageSubject = resourceBundle.getString("report-a-problem");
				SessionMessages.add(actionRequest, "success-report-key");
				model.addAttribute("message", "success-report-key");
			} else {

				messageSubject = resourceBundle
						.getString("share-your-feedback");
				SessionMessages.add(actionRequest, "success-feedback-key");
				model.addAttribute("message", "success-feedback-key");
			}

		} catch (Exception e) {
			LoggerUtil.errorLogger(logger,
					"Error while saving feedback data: ", e);
			SessionErrors.add(actionRequest, "error-saving-data");
			model.addAttribute("message", "error-saving-data");
		}
		Map<String, String> fieldsMap = new HashMap<String, String>();
		fieldsMap.put("fromEmailAddress", emailAddress);
		fieldsMap.put("fromFirstName", firstName);
		fieldsMap.put("fromLastName", lastName);
		fieldsMap.put("fromEmailAddressName", firstName + " " + lastName);
		fieldsMap.put("subjectValue", messageSubject);
		fieldsMap.put("topic", topic);
		fieldsMap.put("comments", comments);
		fieldsMap.put("fromPhoneNumber", phoneNumber);
		fieldsMap.put("keyValue", keyValue);

		sendEmail(actionRequest, resourceBundle, model,
				themeDisplay.getCompanyId(), fieldsMap);

	}

	/**
	 * Method used to send the e-mail based on the Configured toEmail addresses
	 * and fromEmail addresses.
	 *
	 * @param actionRequest
	 * @param resourceBundle
	 * @param model
	 * @param companyId
	 * @param fieldsMap
	 * @return
	 */
	protected boolean sendEmail(ActionRequest actionRequest,
			ResourceBundle resourceBundle, Model model, long companyId,
			Map<String, String> fieldsMap) {

		try {
			String toEmailAddresses = "";
			//Fix to report a problem to send email.
			List<String[]> fields = null;
			String keyValue = fieldsMap.get("keyValue");
			String type = null;
			if (keyValue != null && (!(keyValue.equals("")))
					&& keyValue.equals("share")) {
				type = resourceBundle.getString("shared-feedback");
				fields = WebUtil.getDynamicDataListRecords(actionRequest, Constants.SHARE_YOUR_FEEDBACK_TOPIC);
			} else if (keyValue != null && (!(keyValue.equals("")))
					&& keyValue.equals("report")) {
				type = resourceBundle.getString("reported-a-problem");
				fields = WebUtil.getDynamicDataListRecords(actionRequest, Constants.REPORT_A_PROBLEM_TOPIC);
			} else {
				type = resourceBundle.getString("shared-feedback");
			}
			
			String topic = fieldsMap.get("topic");
			LoggerUtil.infoLogger(logger, "topic from sendEmail in Feedback Controller-------------->"+topic);
			for (String[] field : fields) {
				LoggerUtil.infoLogger(logger, "filed[0]---field[1] from sendEmail in Feedback Controller-------------->"+field[0]+"---------------"+field[1]);
				if (topic.equals(field[0])) {
					toEmailAddresses = field[1];
					break;
				}
			}
			if (Validator.isNull(toEmailAddresses)) {
				logger.error("The FeedBackController email cannot be sent because no email address is configured");
				SessionErrors.add(actionRequest,
						"error-no-email-address-configured");
				model.addAttribute("message",
						"error-no-email-address-configured");
				return false;
			}
			InternetAddress fromAddress = null;
			String fromEmailAddress = fieldsMap.get("fromEmailAddress");
			if (fromEmailAddress != null && (!(fromEmailAddress.equals("")))) {
				fromAddress = new InternetAddress(
						fieldsMap.get("fromEmailAddress"),
						fieldsMap.get("fromEmailAddressName"));
			} else {
				fromAddress = new InternetAddress(
						Constants.GENERIC_EMAIL_ADDRESS, Constants.GENERIC_NAME);
			}
			if (keyValue != null && (!(keyValue.equals("")))
					&& keyValue.equals("share")) {
				type = resourceBundle.getString("shared-feedback");
			} else if (keyValue != null && (!(keyValue.equals("")))
					&& keyValue.equals("report")) {
				type = resourceBundle.getString("reported-a-problem");
			} else {
				type = resourceBundle.getString("shared-feedback");
			}

			StringBuilder sbSubject = new StringBuilder();
			sbSubject.append(type);
			sbSubject.append(" - ");
			sbSubject.append(topic);
			String subject = sbSubject.toString();

			String body = getMailBody(actionRequest, resourceBundle, fieldsMap);

			MailMessage mailMessage = new MailMessage(fromAddress, subject,
					body, false);
			InternetAddress[] toInternetAddresses = InternetAddress
					.parse(toEmailAddresses);
			mailMessage.setTo(toInternetAddresses);
			mailMessage.setHTMLFormat(true);
			MailServiceUtil.sendEmail(mailMessage);
			LoggerUtil.debugLogger(logger,
					"Feedback comments send via email successfully.");
			
			LoggerUtil.infoLogger(logger, "comments from sendEmail() method in Feedback controller---------->"+fieldsMap.get("comments"));
			LoggerUtil.infoLogger(logger, "fromFirstName from sendEmail() method in Feedback controller---------->"+fieldsMap.get("fromFirstName"));
			
			return true;
		} catch (Exception e) {
			LoggerUtil.errorLogger(logger,
					"The sendEmail -> email could not be sent", e);
			SessionErrors.add(actionRequest, "error-sending-email");
			model.addAttribute("message", "error-sending-email");
			return false;
		}
	}

	/**
	 * Gets the EMail body for Share your feedback and Report a Problem portlet.
	 *
	 * @param actionRequest
	 * @param resourceBundle
	 * @param fieldsMap
	 * @return
	 */
	protected String getMailBody(ActionRequest actionRequest,
			ResourceBundle resourceBundle, Map<String, String> fieldsMap) {
		/**
		 * Code for getting Email body from Velocity Template.
		 */
		
		String formattedMailBody = null;
		String messageBodyTemplate = resourceBundle.getString("feedback.mail.template");
		MessageFormat messageFormat = new MessageFormat(messageBodyTemplate);
		Object[] objectArray = new Object[6];
		
		objectArray[0] = fieldsMap.get("comments");
		objectArray[1] = resourceBundle.getString("feedback-mail-contact-information");
		objectArray[2] = fieldsMap.get("fromFirstName");
		objectArray[3] = fieldsMap.get("fromLastName");
				
		if(fieldsMap.get("fromEmailAddress").equals(""))
			objectArray[4] = resourceBundle.getString("feedback-none-available");
		else
			objectArray[4] = fieldsMap.get("fromEmailAddress");
		
		if(fieldsMap.get("fromPhoneNumber").equals(""))
			objectArray[5] = resourceBundle.getString("feedback-none-available");
		else
			objectArray[5] = fieldsMap.get("fromPhoneNumber");
		try{
			formattedMailBody = messageFormat.format(objectArray);
	    }catch (Exception e) {
	    	LoggerUtil.errorLogger(logger,"Exception from feedback - getMailBody() while formatting the string for mailBody :", e);
		}
		return formattedMailBody;
	    
		/*StringWriter writer = new StringWriter();
		Properties velocityProperties = new Properties();
		velocityProperties.put("resource.loader", "class");
		velocityProperties.put("class.resource.loader.description","Velocity Classpath Resource Loader");
		velocityProperties.put("class.resource.loader.class","org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		VelocityEngine ve = new VelocityEngine();
		try {
			LoggerUtil.infoLogger(logger, "comments from getMailBody() method in Feedback controller---------->"+fieldsMap.get("comments"));
			LoggerUtil.infoLogger(logger, "fromFirstName from getMailBody() method in Feedback controller---------->"+fieldsMap.get("fromFirstName"));
			ve.init(velocityProperties);
			Template feedbackTemplate = null;
			feedbackTemplate = ve.getTemplate(Constants.FEEDBACK_TEMPLATE);
			VelocityContext context = new VelocityContext();
			context.put("COMMENTS", fieldsMap.get("comments"));
			context.put("FROM_FIRST_NAME", fieldsMap.get("fromFirstName"));
			context.put("FROM_LAST_NAME", fieldsMap.get("fromLastName"));
			context.put("FROM_EMAIL_ADDRESS", fieldsMap.get("fromEmailAddress"));
			context.put("FROM_EMAIL_ADDRESS_NAME",fieldsMap.get("fromEmailAddressName"));
			context.put("FROM_PHONE_NUMBER", fieldsMap.get("fromPhoneNumber"));
			context.put("FROM_CONTACT_INFORMATION", resourceBundle.getString("feedback-mail-contact-information"));
			context.put("NONE_AVAILABLE",resourceBundle.getString("feedback-none-available"));
			feedbackTemplate.merge(context, writer);
		} catch (ResourceNotFoundException rnfe) {
			LoggerUtil.errorLogger(logger,
					"Exeception raised while loading the feedback template: ",
					rnfe);
		} catch (ParseErrorException e) {
			LoggerUtil.errorLogger(logger, "ParseErrorException raised", e);
		} catch (Exception e) {
			LoggerUtil.errorLogger(logger, "Exception raised", e);
		}
		LoggerUtil.infoLogger(logger, "writer.toString() from getMailBody() method in Feedback controller---------->"+writer.toString());
		return writer.toString();*/
	}

}
