package com.coach.cip.common.dto;

import java.io.Serializable;

/**
 * Class is used to capture the characteristics of countries.
 * 
 * @author GalaxE.
 *
 */
public class CountryVO implements Serializable{
	
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 973436067987582447L;
	private Long countryId;
    private String name;
    private String a2;
    private String a3;
    private String number;
    private String idd;
    private Short zipRequired;
    private Short active;
    
    /**
     * Method used to get countryId.
     * @return countryId.
     */
	public Long getCountryId() {
		return countryId;
	}
	/**
	 * Method used to set countryId.
	 * @param countryId
	 */
	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}
	/**
	 * Method used to get name.
	 * @return name.
	 */
	public String getName() {
		return name;
	}
	/**
	 * Method used to set name.
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Method used to get a2.
	 * @return a2.
	 */
	public String getA2() {
		return a2;
	}
	/**
	 * Method used to set a2.
	 * @param a2
	 */
	public void setA2(String a2) {
		this.a2 = a2;
	}
	/**
	 * Method used to get a3.
	 * @return a3.
	 */
	public String getA3() {
		return a3;
	}
	/**
	 * Method used to set a3.
	 * @param a3
	 */
	public void setA3(String a3) {
		this.a3 = a3;
	}
	/**
	 * Method used to get number.
	 * @return number.
	 */
	public String getNumber() {
		return number;
	}
	/**
	 * Method used to set number.
	 * @param number
	 */
	public void setNumber(String number) {
		this.number = number;
	}
	/**
	 * Method used to get idd.
	 * @return idd.
	 */
	public String getIdd() {
		return idd;
	}
	/**
	 * Method used to set idd.
	 * @param idd
	 */
	public void setIdd(String idd) {
		this.idd = idd;
	}
	/**
	 * Method used to get zipRequired.
	 * @return zipRequired.
	 */
	public Short getZipRequired() {
		return zipRequired;
	}
	/**
	 * Method used to set zipRequired.
	 * @param zipRequired
	 */
	public void setZipRequired(Short zipRequired) {
		this.zipRequired = zipRequired;
	}
	/**
	 * Method used to get active.
	 * @return active.
	 */
	public Short getActive() {
		return active;
	}
	/**
	 * Method used to set active.
	 * @param active
	 */
	public void setActive(Short active) {
		this.active = active;
	}
}
