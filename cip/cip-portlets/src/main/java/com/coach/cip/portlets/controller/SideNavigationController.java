
package com.coach.cip.portlets.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.RenderResponse;
import javax.servlet.http.HttpServletRequest;
import javax.xml.namespace.QName;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.coach.cip.audit.util.Attribute;
import com.coach.cip.audit.util.AuditMessageBuilder;
import com.coach.cip.util.CoachAudit;
import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.coach.cip.util.WebUtil;
import com.liferay.portal.kernel.audit.AuditMessage;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Layout;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.asset.AssetRendererFactoryRegistryUtil;
import com.liferay.portlet.asset.model.AssetCategory;
import com.liferay.portlet.asset.model.AssetEntry;
import com.liferay.portlet.asset.model.AssetRenderer;
import com.liferay.portlet.asset.model.AssetRendererFactory;
import com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil;
import com.liferay.portlet.asset.service.persistence.AssetEntryQuery;
import com.liferay.portlet.documentlibrary.model.DLFileEntryConstants;
import com.liferay.portlet.documentlibrary.model.DLFileShortcut;
import com.liferay.portlet.journal.model.JournalArticle;

/**
 * The Class SideNavigationController, serves the request related to the Side Navigation
 * operations like rendering the page, generating events to open the Assets ( Webcontent, 
 * PDF, Doc files etc).
 * 
 * @author GalaxE.
 */

@Controller("sideNavigationController")
@Scope(value="globalSession", proxyMode=ScopedProxyMode.TARGET_CLASS)
@RequestMapping(value = "VIEW")
public class SideNavigationController extends AbstractBaseController implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 952015912924712790L;

	/** The Constant LOG. */
	private static final Log logger = LogFactoryUtil.getLog(SideNavigationController.class);

	/** Map to hold Asset Categories. */
	private Map<Long, List<AssetCategory>> assetCategoryMap = new HashMap<Long, List<AssetCategory>>();
	
	/** Map to hold Asset Entries. */
	private Map<Long, Set<AssetEntry>> assetEntryMap = new HashMap<Long, Set<AssetEntry>>();
	
	/** Map to hold Category related Asset Entries . */
	private Map<String, Set<AssetEntry>> assetCategoryEntryMap = new HashMap<String, Set<AssetEntry>>();
	
	/** List to hold Asset Categories */
	private List<AssetCategory> assetCategoryList;
	
	/** Variable to hold last request Vocabulary. */
	private String previousVocab;
	
	/** Variable to hold current request Vocabulary. */
	private String currentVocab;

	/**
	 * Default Render Mapping Method. If Specific Render mapping is not found,
	 * this mapping is invoked.
	 * 
	 * @param renderResponse
	 * @return
	 */
	@RenderMapping
	public String showSideNavigation(RenderResponse renderResponse) {
		LoggerUtil
		.debugLogger(
				logger,
				"Inside default render mapping showSideNavigation() method of Side Navigation Controller::");
		return Constants.SIDENAVIGATIONCONTROLLER;
	}
	
	
	/**
	 * Model Object for displaying Asset Categories.
	 * 
	 * @param portletRequest
	 * @param portletResponse
	 * @return Map<Long, List<AssetCategory>>
	 * @throws SystemException
	 */
	@ModelAttribute(value = "assetCategoryMap")
	public Map<Long, List<AssetCategory>> getAssetCategoryMap(PortletRequest portletRequest,
							PortletResponse portletResponse)throws SystemException {
		LoggerUtil.debugLogger(logger,"Start of Model getAssetCategoryMap() method of Side Navigation Controller::");
		ThemeDisplay themeDisplay = WebUtil.getThemeDisplay(portletRequest);
		List<Layout> parentNavs = null;
		try {
			parentNavs = themeDisplay.getLayout().getAncestors();
			currentVocab = themeDisplay.getLayout().getHTMLTitle(themeDisplay.getLocale());
			if (parentNavs.size() == 2) {
				currentVocab = parentNavs.get(parentNavs.size() - 1)
						.getHTMLTitle(themeDisplay.getLocale());
			} else if (parentNavs.size() == 1) {
				currentVocab = parentNavs.get(parentNavs.size() - 1)
						.getHTMLTitle(themeDisplay.getLocale());
			}
		} catch (PortalException e) {
			LoggerUtil.errorLogger(logger,"SideNavigationController.getAssetEntryMap() PortalException :::",e);
		}
		
	
		if (!(Validator.isNotNull(previousVocab) && currentVocab.equalsIgnoreCase(previousVocab)) || !isCIPOrganization(themeDisplay)) {
			assetCategoryMap = new HashMap<Long, List<AssetCategory>>();
			assetCategoryList = new ArrayList<AssetCategory>();
			assetEntryMap = new HashMap<Long, Set<AssetEntry>>();
			assetCategoryEntryMap = new HashMap<String, Set<AssetEntry>>();
		}
	

		if (assetCategoryMap.size() == 0) {
			
			assetCategoryList = getOrganizationAssetCategories(themeDisplay,portletRequest,portletResponse);
			
			
			if (Validator.isNull(assetCategoryList)) {
				assetCategoryList = new ArrayList<AssetCategory>();
			}
			if (assetCategoryList.size() > 0) {
				for (AssetCategory assetCategory : assetCategoryList) {
					if (assetCategoryMap.containsKey(assetCategory
							.getParentCategoryId())) {
						assetCategoryMap.get(
								assetCategory.getParentCategoryId()).add(
								assetCategory);
					} else {
						List<AssetCategory> categoryList = new ArrayList<AssetCategory>();
						categoryList.add(assetCategory);
						assetCategoryMap.put(
								assetCategory.getParentCategoryId(),
								categoryList);
					}
				}
			}
		}
		previousVocab = currentVocab;
		LoggerUtil.debugLogger(logger,"End of Model getAssetCategoryMap() method of Side Navigation Controller::");
		return assetCategoryMap;
	}
	

	/**
	 * Model Object for displaying Asset Entries.
	 * 
	 * @param portletRequest
	 * @param portletResponse
	 * @return Map<Long, Set<AssetEntry>>
	 * @throws SystemException
	 */
	@ModelAttribute(value = "assetEntriesMap")
	public Map<Long, Set<AssetEntry>> getAssetEntryMap(
			PortletRequest portletRequest, PortletResponse portletResponse)
			throws SystemException {

		LoggerUtil.debugLogger(logger,"Start of getAssetEntryMap() method of SideNavigationController::");
		ThemeDisplay themeDisplay = WebUtil.getThemeDisplay(portletRequest);
		if (assetEntryMap.size() == 0) {
			if (Validator.isNull(assetCategoryList)) {
				assetCategoryList = getOrganizationAssetCategories(themeDisplay,portletRequest,portletResponse);
			}
			int i = assetCategoryList.size() / 200;
			int j = assetCategoryList.size() % 200;
			if (j != 0) {
				i = i + 1;
			}
			for (int k = 0; k < i; k++) {
				int start = (k) * 200;
				int end = 0;
				if (k == i - 1 && assetCategoryList.size() % 200 != 0) {
					end = start + assetCategoryList.size() % 200;
				} else {
					end = start + 200;
				}
				List<AssetCategory> assetCategorySubList = assetCategoryList
						.subList(start, end);
				long[] allCateogyIds = null;
				if (Validator.isNotNull(assetCategorySubList)) {
					allCateogyIds = new long[assetCategorySubList.size()];
					int l = 0;
					for (AssetCategory assetCategory : assetCategorySubList) {
						allCateogyIds[l] = assetCategory.getCategoryId();
						l++;
					}
				}

				long[] classNameIds = {
						PortalUtil.getClassNameId(DLFileEntryConstants.getClassName()),
						PortalUtil.getClassNameId(DLFileShortcut.class.getName()),
						PortalUtil.getClassNameId(JournalArticle.class) };
				AssetEntryQuery assetEntryQuery = new AssetEntryQuery();
				assetEntryQuery.setClassNameIds(classNameIds);
				assetEntryQuery.setStart(0);
				assetEntryQuery.setEnd(9000);
				assetEntryQuery.setAnyCategoryIds(allCateogyIds);

				try {
					List<AssetEntry> allAssetEntryList = AssetEntryLocalServiceUtil
							.getEntries(assetEntryQuery);
					for (AssetEntry assetEntry : allAssetEntryList) {
						AssetRendererFactory assetRendererFactory = AssetRendererFactoryRegistryUtil
								.getAssetRendererFactoryByClassName(assetEntry
										.getClassName());
						AssetRenderer assetRenderer = assetRendererFactory
								.getAssetRenderer(assetEntry.getClassPK());
						List<AssetCategory> categoryList = assetEntry
								.getCategories();
						if (assetRenderer.hasViewPermission(WebUtil
								.getPermissionChecker(portletRequest))) {
						for (AssetCategory category : categoryList) {

								/* Populating Asset Entries which are having same title as the  Category Name : Start */
								if(category.getName().equalsIgnoreCase(WebUtil.cropTitle(assetEntry.getTitleCurrentValue()))){
									
									if (assetCategoryEntryMap.containsKey(category.getName())) {
										assetCategoryEntryMap.get(category.getCategoryId()+"-"+category.getName()).add(assetEntry);
									} else {
										Set<AssetEntry> assetEntrySet = new HashSet<AssetEntry>();
										assetEntrySet.add(assetEntry);
										assetCategoryEntryMap.put(category.getCategoryId()+"-"+category.getName(),
												assetEntrySet);
									}
								}
								/* Populating Asset Entries which are having same title as the  Category Name : End */
								
								/* Populating Asset Entries for the Category Id : Start */
								else{
									if (assetEntryMap.containsKey(category
											.getCategoryId())) {
										assetEntryMap.get(category.getCategoryId())
												.add(assetEntry);
									} else {
										Set<AssetEntry> assetEntrySet = new HashSet<AssetEntry>();
										assetEntrySet.add(assetEntry);
										assetEntryMap.put(category.getCategoryId(),
												assetEntrySet);
									}
								}	
								/* Populating Asset Entries for the Category Id : End */
							}
						}
					}
				} catch (SystemException e) {
					LoggerUtil.errorLogger(logger,"SideNavigationController.getAssetEntryMap() SystemException :::",e);
				} catch (Exception e) {
					LoggerUtil.errorLogger(logger,"SideNavigationController.getAssetEntryMap()  Exception :::",e);
				}
			}
		}
		LoggerUtil.debugLogger(logger,"End of getAssetEntryMap()  method of SideNavigationController::");
		return assetEntryMap;
	}
	
	
	/**
	 * Model Object for displaying Asset Category Entries.
	 * 
	 * @param portletRequest
	 * @param portletResponse
	 * @return Map<Long, Set<AssetEntry>>
	 * @throws SystemException
	 */
	@ModelAttribute(value = "assetCategoryEntryMap")
	public Map<String, Set<AssetEntry>> getAssetCategoryEntryMap(
			PortletRequest portletRequest, PortletResponse portletResponse){
		assetCategoryList = null;
		return assetCategoryEntryMap;
	}
	
	
	/**
	 * Fetches the Asset Categories Specific to the organization.
	 * @param themeDisplay
	 * @param portletRequest
	 * @param portletResponse
	 * @return
	 */
	public List<AssetCategory> getOrganizationAssetCategories(ThemeDisplay themeDisplay,PortletRequest portletRequest, PortletResponse portletResponse){
		try {
				if(isCIPOrganization(themeDisplay))
				{
					return WebUtil.getAllAssetCategoriesByVocabulary(portletRequest,
								portletResponse, currentVocab);
				}else
				{
					return WebUtil.getAllAssetCategoriesByGroupId(portletRequest);
				}
			} catch (PortalException e) {
				LoggerUtil.errorLogger(logger,"SideNavigationController.getOrganizationAssetCategories() PortalException :::",e);
			} catch (SystemException e) {
				LoggerUtil.errorLogger(logger,"SideNavigationController.getOrganizationAssetCategories() SystemException :::",e);
			}
		
		return Collections.EMPTY_LIST;
	}
	
	/**
	 * Helper Method to check whether the user belongs to CIP organization or not. Returns true if the user belongs to CIP organization.
	 * @param themeDisplay
	 * @return
	 */
	public boolean isCIPOrganization(ThemeDisplay themeDisplay){
		String OrganizationName = PropsUtil.get(Constants.COACH_INTRANET_PORTAL_ORGANIZATION_NAME);
		try {
			if(Validator.isNotNull(OrganizationName) &&
					  themeDisplay.getScopeGroupName().equalsIgnoreCase(OrganizationName))
			{
				return true;
			}else{
				return false;
			}
		} catch (PortalException e) {
			LoggerUtil.errorLogger(logger,"SideNavigationController.isCIPOrganization() PortalException :::",e);
		} catch (SystemException e) {
			LoggerUtil.errorLogger(logger,"SideNavigationController.isCIPOrganization() SystemException :::",e);
		}
		return false;
	}
	
	
	/**
	 * Action Mapping Method for Side Navigation to generate Events for the Assets.
	 * 
	 * @param actionRequest
	 * @param actionResponse
	 * @param model
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ActionMapping(params = "myaction=processDocumentURL")
	public void processDocumentURL(ActionRequest actionRequest, ActionResponse actionResponse, Model model) {
		LoggerUtil.debugLogger(logger, "Start of processDocumentURL() method of Side Navigation Controller::");
		
		HttpServletRequest servletRequest = WebUtil.getHttpServletRequest(actionRequest);
		servletRequest.setAttribute("level5Parent", actionRequest.getParameter("level5Parent"));
		servletRequest.setAttribute("level6Parent", actionRequest.getParameter("level6Parent"));
		servletRequest.setAttribute("level7Parent", actionRequest.getParameter("level7Parent"));
		servletRequest.setAttribute("level8Parent", actionRequest.getParameter("level8Parent"));
		servletRequest.setAttribute("fileName", actionRequest.getParameter("fileTitle") != null ? actionRequest.getParameter("fileTitle") : "");

		model.addAttribute("level5Parent", actionRequest.getParameter("level5Parent"));
		model.addAttribute("level6Parent", actionRequest.getParameter("level6Parent"));
		model.addAttribute("level7Parent", actionRequest.getParameter("level7Parent"));
		model.addAttribute("level8Parent", actionRequest.getParameter("level8Parent"));
		model.addAttribute("fileName", actionRequest.getParameter("fileTitle") != null ? actionRequest.getParameter("fileTitle") : "");
		
		//Audit start
		ThemeDisplay themeDisplay = WebUtil.getThemeDisplay(actionRequest);
		String fileTitle = actionRequest.getParameter("fileTitle");
		String fileType = actionRequest.getParameter("fileType");
		String renderURL = themeDisplay.getPortalURL()+PortalUtil.getCurrentURL(actionRequest);
		
		HashMap fileMap = new HashMap();
		fileMap.put("fileTitle", actionRequest.getParameter("fileTitle") != null ? actionRequest.getParameter("fileTitle") : "");
		fileMap.put("fileURL", actionRequest.getParameter("fileURL") != null ? actionRequest.getParameter("fileURL") : "");
		fileMap.put("fileType", actionRequest.getParameter("fileType") != null ? actionRequest.getParameter("fileType") : "");
		fileMap.put("classPK", actionRequest.getParameter("classPK") != null ? actionRequest.getParameter("classPK") : "");
		fileMap.put("typeOfLink", actionRequest.getParameter("typeOfLink") != null ? actionRequest.getParameter("typeOfLink") : "");
		fileMap.put("mimeType", actionRequest.getParameter("mimeType") != null ? actionRequest.getParameter("mimeType") : "");
		fileMap.put("backButton", actionRequest.getParameter("backButton") != null ? actionRequest.getParameter("backButton") : "false");
		QName qName = new QName("http://coach.com/events", "ipc.fileURL");
		actionResponse.setEvent(qName, fileMap);
		LoggerUtil.debugLogger(logger, "End of processDocumentURL() method of Side Navigation Controller::");
		
		Attribute attributeFileTitle = new Attribute("FILE_TITLE",fileTitle,"");
		Attribute attributeFileURL = new Attribute("RENDER_URL",renderURL,"");
		Attribute attributeFileType = new Attribute("FILE_TYPE",fileType,"");
		Attribute attributeVocabName = new Attribute("VOCABULARY_NAME",currentVocab,"");
		
		List<Attribute> attributeslist = new ArrayList<Attribute>();
		attributeslist.add(attributeFileTitle);
		attributeslist.add(attributeFileURL);
		attributeslist.add(attributeFileType);
		attributeslist.add(attributeVocabName);
		
		
		
		AuditMessage auditMessage = AuditMessageBuilder.buildAuditMessage(fileType.substring(fileType.lastIndexOf(".")+1, fileType.length()), currentVocab, 0l, attributeslist);
		CoachAudit.route(auditMessage);
//		try {
//		     AuditRouterUtil.route(auditMessage);
//		    } catch (AuditException e) {
//		    	LoggerUtil.errorLogger(logger,"SideNavigationController.processDocumentURL() AuditException :::",e);
//		    }
	}
}
