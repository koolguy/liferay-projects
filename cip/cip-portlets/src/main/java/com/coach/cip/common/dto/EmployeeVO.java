
package com.coach.cip.common.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import com.coach.cip.model.entity.User;

/**
 * DTO class for Employee Domain Object.
 * @author GalaxE.
 */
public class EmployeeVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1663195254118707378L;
	/**
	 * Employee Id of Employee.
	 */
	private Long empId;
	/**
	 * Supervisor of Employee.
	 */
	private User supervisor;
	/**
	 * Administration Assistant for Employee.
	 */
	private User adminAssist;
	/**
	 * User Object for Employee.
	 */
	private User user;
	/**
	 * Type of Employee.
	 */
	private String employeeType;
	/**
	 * Employment type of Employee.
	 */
	private String employmentType;
	/**
	 * Status of Employee.
	 */
	private String employeeStatus;
	/**
	 * Leadership Indicator for Employee.
	 */
	private Boolean leadershipIndicator;
	/**
	 * Enrollment Benefit Indicator for Employee.
	 */
	private Boolean enrollmentBenefitIndicator;
	/**
	 * Department of Employee.
	 */
	private String department;
	/**
	 * Hire Date of Employee.
	 */
	private Timestamp hireDate;

	/**
	 * Getter method for empId
	 * @return empId of type Long
	 */
	public Long getEmpId() {

		return this.empId;
	}

	/**
	 * Setter method for empId
	 * @param empId
	 */
	public void setEmpId(Long empId) {

		this.empId = empId;
	}

	/**
	 * Getter method for supervisor
	 * @return supervisor of type User
	 */
	public User getSupervisor() {

		return this.supervisor;
	}

	/**
	 * Setter method for supervisor
	 * @param supervisor
	 */
	public void setSupervisor(User supervisor) {

		this.supervisor = supervisor;
	}

	/**
	 * Getter method for adminAssist
	 * @return adminAssist of Type User
	 */
	public User getAdminAssist() {

		return this.adminAssist;
	}

	/**
	 * Setter method for adminAssist
	 * @param adminAssist
	 */
	public void setAdminAssist(User adminAssist) {

		this.adminAssist = adminAssist;
	}

	/**
	 * Getter method for user
	 * @return User
	 */
	public User getUser() {

		return this.user;
	}

	/**
	 * Setter method for user
	 * @param user
	 */
	public void setUser(User user) {

		this.user = user;
	}

	/**
	 * Getter method for employeeType
	 * @return employeeType String
	 */
	public String getEmployeeType() {

		return this.employeeType;
	}

	/**
	 * Setter method for employeeType
	 * @param employeeType
	 */
	public void setEmployeeType(String employeeType) {

		this.employeeType = employeeType;
	}

	/**
	 * Getter method for employmentType
	 * @return employmentType String
	 */
	public String getEmploymentType() {

		return this.employmentType;
	}

	/**
	 * Setter method for employmentType
	 * @param employmentType
	 */
	public void setEmploymentType(String employmentType) {

		this.employmentType = employmentType;
	}

	/**
	 * Getter method for employeeStatus
	 * @return employeeStatus String
	 */
	public String getEmployeeStatus() {

		return this.employeeStatus;
	}

	/**
	 * Setter method for employeeStatus
	 * @param employeeStatus
	 */
	public void setEmployeeStatus(String employeeStatus) {

		this.employeeStatus = employeeStatus;
	}

	/**
	 * Getter method for leadershipIndicator
	 * @return leadershipIndicator
	 */
	public Boolean getLeadershipIndicator() {

		return this.leadershipIndicator;
	}

	/**
	 * Setter method for leadershipIndicator
	 * @param leadershipIndicator
	 */
	public void setLeadershipIndicator(Boolean leadershipIndicator) {

		this.leadershipIndicator = leadershipIndicator;
	}

	/**
	 * Getter method for enrollmentBenefitIndicator
	 * @return enrollmentBenefitIndicator
	 */
	public Boolean getEnrollmentBenefitIndicator() {

		return this.enrollmentBenefitIndicator;
	}

	/**
	 * Setter method for enrollmentBenefitIndicator
	 * @param enrollmentBenefitIndicator
	 */
	public void setEnrollmentBenefitIndicator(Boolean enrollmentBenefitIndicator) {

		this.enrollmentBenefitIndicator = enrollmentBenefitIndicator;
	}

	/**
	 * Getter method for department
	 * @return department String
	 */
	public String getDepartment() {

		return this.department;
	}

	/**
	 * Setter method for department
	 * @param department
	 */
	public void setDepartment(String department) {

		this.department = department;
	}

	/**
	 * Getter method for hireDate
	 * @return hireDate of type Timestamp
	 */
	public Timestamp getHireDate() {

		return this.hireDate;
	}

	/**
	 * Setter method for hireDate
	 * @param hireDate
	 */
	public void setHireDate(Timestamp hireDate) {

		this.hireDate = hireDate;
	}

}
