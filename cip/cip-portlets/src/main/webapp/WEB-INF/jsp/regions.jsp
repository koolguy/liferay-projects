<%@page import="com.coach.cip.util.WebUtil"%>
<%@page import="org.apache.velocity.runtime.directive.Foreach"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticle"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.liferay.portal.kernel.util.LocaleUtil"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.ResourceBundle"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.coach.cip.util.Constants"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<style>
.selectedgeo {
	font-weight: bold;
	color: #E8317D;
}
.coachTitle {
	margin-top: 4px;
	float: left;
}
.rkTitle {
	margin-top: 4px;
	float: left;
}
.regionsStyle {
	font-size: 11px;
	font-family: Verdana;
	border: 1px solid black; 
	margin-bottom: 7px;
}
</style>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<script type="text/javascript">
<%
			String title = themeDisplay.getLayout().getName(
					themeDisplay.getLocale());
			if (title != null && !title.isEmpty()) {
				title = title.toUpperCase();
			}%>

       $(document).ready(function() {

    		 function undohighlight() { 
    			 
					  $("table#offTable tr").each(function(){
						 if($(this).find('a').hasClass('selectedgeo')) {
					 	 $(this).find('a').removeClass('selectedgeo');
							} 
					  });
					
                     $("table#geoTable tr").each(function(){
					 	if($(this).find('a').hasClass('selectedgeo')) {
					 	$(this).find('a').removeClass('selectedgeo');
							} 
					 });

                    $("table#rkoffTable tr").each(function(){
						 if($(this).find('a').hasClass('selectedgeo')) {
					 	 $(this).find('a').removeClass('selectedgeo');
							} 
					});

					$("table#rkgeoTable tr").each(function(){
					 	if($(this).find('a').hasClass('selectedgeo')) {
					  	$(this).find('a').removeClass('selectedgeo');
						} 
					});
	   }

             $('a.officesAnchor').click(function(event) {
            	
                       undohighlight(); 
                       $(this).addClass('selectedgeo');
				       var value = $(this).attr('href');	
              		   Liferay.fire('retrieveValue', value);
              		   return false;
            }
          );

           var officeName = '<%=title%>';
           if(officeName=="<spring:message code='label.title.offices'/>"){
 			  officeName="<spring:message code='label.offices.northamerica'/>";
 			  $('#offTable tr td').each(function() {
 				var name = $(this).text();
 				if ($.trim(name) == officeName) {
 				$(this).find("a").addClass("selectedgeo");
 			    }
 			});
 		   }
           //Highlighting office when you come from search.
           $("#offTable").find("tr").find("a").removeClass("selectedgeo");
			var offName=$("span.portlet-title-text").text().split("-")[1];
			var selectedOffName=$("span.portlet-title-text").text().split("-")[2];
			 if($.trim(offName)=="COACH"){
					$('#offTable tr').each(function() {
						var name = $(this).text();
						if ($.trim(name) == $.trim(selectedOffName)) {
							$(this).find("a").addClass("selectedgeo");
						}
					  });
				 }
			  else{
					$('#rkoffTable tr').each(function() {
					var name = $(this).text();
					if ($.trim(name) == $.trim(selectedOffName)) {
					$(this).find("a").addClass("selectedgeo");
					}
				});
			 } 
			 
			 

		  $(".portlet-layout").find("#column-1").find("h1:first").append('<input  type="image" src="<%=themeDisplay.getPathThemeImages()%>/leftarrow.png"  id="collapseButton" title="Hide" style="float:right"/>')
			$("#collapseButton").click(function(){

				var $columnOne=$(this).parents(".portlet-layout").find("#column-1");
			    var $columnTwo=$(this).parents(".portlet-layout").find("#column-2");

           	    $($columnOne).hide();
                var totalWidth=$($columnTwo).width()+$($columnOne).width()+"px";
					
				$($columnTwo).css({"width":$.trim(totalWidth)});
				
				//$(this).parents(".portlet-layout").find("#column-2").css({"width":$("#wrapper").css("width")+"px"});
				$($columnOne).parent().prepend('<input  type="image" src="<%=themeDisplay.getPathThemeImages()%>/rightarrow.png"   id="expndButton" title="Show" />');
								
			});
			
			$("#expndButton").live("click",function(){
               
				var $columnOne=$(this).parents(".portlet-layout").find("#column-1");
			    var $columnTwo=$(this).parents(".portlet-layout").find("#column-2");

               var totalWidth=(0.7 * $($columnTwo).width())+"px";
				$($columnTwo).css({"width":$.trim(totalWidth)});
			   $($columnOne).show();
				
			   $(this).remove();	
									
			});
	});

	
</script>

<%
	List<JournalArticle> journalArticleList = null;
	List<JournalArticle> rkjournalArticleList = null;
	journalArticleList = WebUtil.getArticlesByTypeForOffices(themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId(), "coach-offices", null, -1);
	rkjournalArticleList = WebUtil.getArticlesByTypeForOffices(themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId(), "rk-offices", null, -1);
	
	ResourceBundle rb = ResourceBundle.getBundle("/content/Language", locale);
	String greaterSymbol= new String(">");	
%>
<div class="regionsStyle">
	<header class="portlet-storeDetail-topper">
	<h1 class="portlet-title">
		<span class="portlet-title-text"><spring:message
				code='label.title.regions' /> </span>
	</h1>
	</header>
	
	<div style="margin-left: 10px; margin-top: 10px; color: red; font-weight: bold;">
		<c:if test="${FaultCode ne null}">
		<spring:message code='${FaultMessage}'/>
		</c:if>
	</div>

	<div>
	<table class="coachTitle" style="margin-top: 9px;">
		<tr>
			<td style="font-weight: bold;"><img class="coachTitle" src="<%=themeDisplay.getPathThemeImages()+"/sidenav-arrow.png"%>"/><spring:message 
					code='label.title.brand.coach' /></td>
		</tr>
	</table>
	<table class="coachTitle" style="margin-top: 0px; margin-left: 22px;">
		<tr>
			<td style="font-weight: bold;"><img class="coachTitle" src="<%=themeDisplay.getPathThemeImages()+"/sidenav-arrow.png"%>"/><spring:message
					code='label.title.offices' /></td>
		</tr>
	</table>

	<table style="margin-left: 30px;" id="offTable">
	
		<%
			if(journalArticleList != null && journalArticleList.size() > 0){
				for(JournalArticle journalArticle : journalArticleList){
					
		%>
	
		<tr>
			<td> <a class='officesAnchor' style="text-decoration: none" href="<%=journalArticle.getArticleId()%>::<%=journalArticle.getTitle(themeDisplay.getLocale())%>::<spring:message code='label.title.brand.coach'/>">
				 <%=journalArticle.getTitle(themeDisplay.getLocale())%>
				 </a>
			</td>
		</tr>
		
		<%}} %>

	</table>


	<table class="coachTitle" style="margin-left: 22px;">
		<tr>
			<td style="font-weight: bold;"><img class="coachTitle" src="<%=themeDisplay.getPathThemeImages()+"/sidenav-arrow.png"%>"/><spring:message
					code='label.title.stores' /></td>
		</tr>
	</table>
	<table style="margin-left: 30px;" id="geoTable">
		<c:if test="${not empty coachGeographiesList && coachGeographiesList ne null}">
		<c:forEach var="coachgeography" items="${coachGeographiesList}">
		<tr>
			<td>
			<a style="text-decoration: none"
				href="<portlet:actionURL name="callofficcesURL">
				<portlet:param name="regionId" value="${coachgeography.toUpperCase()}"/>
				<portlet:param name="brand" value="<%=Constants.STORE_BRAND_COACH%>"/>
				</portlet:actionURL>"><c:out value="${coachgeography.toUpperCase()}"></c:out> </a>
			</td>
		</tr>
		</c:forEach>
		</c:if>
		
	</table>
</div>

<div>
	<table class="rkTitle" style="margin-top: 9px;">
		<tr>
			<td style="font-weight: bold;"><img class="rkTitle" src="<%=themeDisplay.getPathThemeImages()+"/sidenav-arrow.png"%>"/><spring:message
					code='label.title.brand.rk' /></td>
		</tr>
	</table>
	<table  class="rkTitle" style="margin-top: 0px; margin-left: 22px;">
		<tr>
			<td style="font-weight: bold;"><img class="rkTitle" src="<%=themeDisplay.getPathThemeImages()+"/sidenav-arrow.png"%>"/><spring:message
					code='label.title.offices' /></td>
		</tr>
	</table>

	<table style="margin-left: 30px;" id="rkoffTable">
		<%
			if(rkjournalArticleList != null && rkjournalArticleList.size() > 0){
				for(JournalArticle journalArticle : rkjournalArticleList){
		%>
	
		<tr>
			<td> <a class='officesAnchor' style="text-decoration: none" href="<%=journalArticle.getArticleId()%>::<%=journalArticle.getTitle(themeDisplay.getLocale())%>::<spring:message code='label.title.brand.rk'/>">
				 <%=journalArticle.getTitle(themeDisplay.getLocale())%>
				 </a>
			</td>
		</tr>
		
		<%}} %>

	</table>


	<table  class="rkTitle" style="margin-left: 22px;">
		<tr>
			<td style="font-weight: bold;"><img class="rkTitle" src="<%=themeDisplay.getPathThemeImages()+"/sidenav-arrow.png"%>"/><spring:message
					code='label.title.stores' /></td>
		</tr>
	</table>

	<table style="margin-left: 30px;" id="rkgeoTable">
	<tr>
		<td>
		<c:if test="${not empty rkGeographiesList && rkGeographiesList ne null}">
		<c:forEach var="rkgeography" items="${rkGeographiesList}">
		<tr>
			<td>
			<a style="text-decoration: none"
				href="<portlet:actionURL name="callofficcesURL">
				<portlet:param name="regionId" value="${rkgeography.toUpperCase()}"/>
				<portlet:param name="brand" value="<%=Constants.STORE_BRAND_RK%>"/>
				</portlet:actionURL>"><c:out value="${rkgeography.toUpperCase()}"></c:out> </a>
			</td>
		</tr>
		</c:forEach>
		</c:if>
		</td>
	</tr>
	</table> 
	
</div>
	
</div>
