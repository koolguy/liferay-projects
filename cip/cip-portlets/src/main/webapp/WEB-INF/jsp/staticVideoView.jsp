<%@page import="com.coach.cip.util.Constants"%>
<%@page import="javax.portlet.PortletSession"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.util.DateUtil"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="com.liferay.portal.kernel.util.DateFormatFactoryUtil"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@page import="com.liferay.portal.kernel.util.PropsUtil"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="liferay-portlet" uri="http://liferay.com/tld/portlet"%>
<%@ taglib prefix="aui" uri="http://alloy.liferay.com/tld/aui"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />
<%
	PortletURL viewVideoURL = renderResponse.createRenderURL();
	viewVideoURL.setWindowState(LiferayWindowState.POP_UP);
	viewVideoURL.setParameter("jspPage", Constants.VIDEO_JSP);
	viewVideoURL.setParameter("showHeader", "false");
	String popup = "javascript:popup('" + viewVideoURL.toString()
			+ "');";
%>

<input type="hidden" id="viewVideoURL"
	value="<%=viewVideoURL.toString()%>">

<%
	DateFormat formatter = DateFormatFactoryUtil
			.getSimpleDateFormat("yyyy/MM/dd");
	String prodDateStr = PropsUtil.get("com.coach.production.date");
	Date prodDate = null;
	Date currDate = new Date();
	if (Validator.isNotNull(prodDateStr)) {
		prodDate = formatter.parse(prodDateStr);
	}
	boolean displayVideo = false;
	if (DateUtil
			.compareTo(user.getLoginDate(), user.getLastLoginDate()) == 0) {
		displayVideo = true;
	} else if ((DateUtil.compareTo(currDate, prodDate) > 0)
			&& (DateUtil.compareTo(user.getLastLoginDate(), prodDate) < 0)) {
		displayVideo = true;
	} else {
		displayVideo = false;
	}
	Boolean shownOnceFlag = false;
	if (Validator.isNotNull(portletSession.getAttribute("shownOnce"))) {
		shownOnceFlag = (Boolean) portletSession
				.getAttribute("shownOnce");
	} else {
		portletSession.setAttribute("shownOnce", true);
	}
%>
<script type="text/javascript">
	$(document).ready(function() {
<%if (displayVideo && !shownOnceFlag) {%>
	popup(document.getElementById('viewVideoURL').value);
<%}%>
	});
</script>
<aui:script>
    Liferay.provide(
        window,'popup',
        function(url) {
            var A = AUI();
            var data = {};
            var dialog = new A.Dialog(
                {
                    centered: true,
                    destroyOnClose: true,
                    draggable: true,
                    height: 400,
                    width: 700,
                    resizable: true,
                    modal: true,
                    title: 'Video'
                }
                ).render();
                    dialog.plug(
                        A.Plugin.IO,
                        {
                            data: data,
                            uri: url
                        }
                    );
            },
        ['aui-dialog', 'aui-io']
    );
</aui:script>
<%
	if (!(displayVideo && !shownOnceFlag)) {
%>
<style>
#p_p_id_StaticVideoPortlet_WAR_cipportlet_ {
	display: none;
}
</style>
<%
	}
%>
