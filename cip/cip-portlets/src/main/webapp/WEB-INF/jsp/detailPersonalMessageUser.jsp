<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="com.liferay.portal.service.RoleServiceUtil"%>
<%@ page import="java.util.List"%>
<%@ page import="com.liferay.portal.model.Role"%>
<%@ page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import="com.liferay.portal.model.User"%>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>


<portlet:defineObjects />
<liferay-theme:defineObjects />
<script language="javascript" type="text/javascript">
	/* function backRedirect() {
		window.location.href = "${redirect}";

	} */
</script>
<%-- <div id="aui_3_4_0_1_622" style="width: 930px; overflow: auto">
	<P id="aui_3_4_0_1_621" style="color: #d76c03; font-weight: bold">
		<B><c:out value="${personalMessage.displayDate}"></c:out> </B>
	</P>
	<P id="aui_3_4_0_1_715"
		style="font-family: 'myriad-web-pro'; font-size: 12px; font-weight: bold">
		<c:out value="${personalMessage.subject}"></c:out>

	</P>
	<P id="aui_3_4_0_1_715" style="">
		<c:out value="${personalMessage.message}"></c:out>

	</P>

	


</div> --%>

<div>
	<p id="aui_3_4_0_1_621" style="color: #EB3528; ">
		<c:out value="${personalMessage.displayDate.toString().toUpperCase(locale)}"></c:out>
	</p>
	
	<p id="aui_3_4_0_1_715"
		style="font-family: 'myriad-web-pro'; font-size: 12px; font-weight: bold">
		<c:out value="${personalMessage.subject}"></c:out>
	</p>
	<p id="aui_3_4_0_1_715" style="">
		<c:out value="${personalMessage.message}"></c:out>
	</p>
</div>
<div style="margin-top:4px;" id="aui_3_4_0_1_622">
	<!-- <input type="button" id="back" onclick="javascript:backRedirect()" value="Back" class="submitClass" /> -->
	<input type="button" id="fullDetailsBack"
						value="<spring:message code='label.back.button'/>" class="submitClass" onClick="history.go(-1);" />
</div>