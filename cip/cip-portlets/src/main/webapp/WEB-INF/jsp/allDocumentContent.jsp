<%@page import="com.liferay.portlet.asset.service.AssetEntryServiceUtil"%>
<%@page import="com.liferay.portal.service.ClassNameLocalServiceUtil"%>
<%@page import="com.liferay.portlet.asset.service.persistence.AssetEntryQuery"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@page import="com.liferay.portal.kernel.dao.search.SearchContainer"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.liferay.portal.kernel.util.PropsUtil"%>
<%@page import="com.liferay.portal.security.permission.ActionKeys"%>
<%@page import="com.liferay.portal.security.permission.PermissionCheckerFactoryUtil"%>
<%@page import="com.liferay.portal.security.permission.PermissionChecker"%>
<%@page import="com.liferay.portal.kernel.xml.SAXReaderUtil"%>
<%@page import="com.liferay.portlet.journal.service.JournalTemplateLocalServiceUtil"%>
<%@page import="com.liferay.portlet.journal.model.JournalTemplate"%>
<%@page import="com.liferay.portlet.asset.model.AssetEntry"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="javax.portlet.PortletRequest"%>
<%@page import="com.liferay.portlet.PortletURLFactoryUtil"%>
<%@page import="javax.portlet.RenderRequest"%>
<%@page import="com.liferay.portal.kernel.exception.SystemException"%>
<%@page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@page import="com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="com.coach.cip.util.WebServiceUtil"%>
<%@page import="com.liferay.portal.model.Layout"%>
<%@page import="com.liferay.portlet.asset.model.AssetVocabulary"%>
<%@page import="com.liferay.portal.service.LayoutLocalServiceUtil"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFileEntryModel"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticleModel"%>
<%@page import="com.liferay.portlet.documentlibrary.util.DLUtil"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFileEntry"%>
<%@page import="com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil"%>
<%@page import="com.liferay.portlet.asset.model.AssetCategory"%>
<%@page import="com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.coach.cip.util.Constants"%>
<%@ page import="com.liferay.portal.kernel.util.StringUtil"%>
<%@ page import="com.coach.cip.util.WebUtil"%>
<%@ page import="com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil"%>
<%@ page import="com.liferay.portlet.journal.service.persistence.JournalArticleFinderUtil"%>
<%@ page import="com.liferay.portlet.journalcontent.util.JournalContentUtil"%>
<%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import="com.liferay.portlet.journal.model.JournalArticle"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.*" %>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="javax.portlet.PortletURL"%>
<%@ page import="com.liferay.portlet.PortletURLUtil"%>
<%@ page import="com.liferay.portal.util.PortalUtil"%>
<%@ page import="com.liferay.portal.kernel.util.HttpUtil"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.liferay.portal.kernel.util.OrderByComparator" %>
<%@ page import="com.liferay.portlet.journal.util.comparator.ArticleModifiedDateComparator" %>
<%@ page import="com.liferay.portlet.journal.util.comparator.ArticleDisplayDateComparator" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<%!private String getAssetEntryPage(AssetCategory childAssetCategory, Map<String,Layout> layoutMap, List<String> categoryNamesList, RenderRequest renderRequest) {
	  String categoryKey = "";
	  categoryNamesList.add(childAssetCategory.getTitle(renderRequest.getLocale()));
	  String tempCategoryKey = getCategory(childAssetCategory);
	  if(layoutMap.containsKey(tempCategoryKey)){
		  categoryKey = tempCategoryKey;
		  renderRequest.setAttribute("categoryNamesList",categoryNamesList);
		  return categoryKey;
	  }else{
		  try{	
			  if(childAssetCategory.getParentCategoryId() != 0){
		  childAssetCategory = AssetCategoryLocalServiceUtil.getCategory(childAssetCategory.getParentCategoryId());
		  categoryKey = getAssetEntryPage(childAssetCategory,layoutMap, categoryNamesList,renderRequest);
			  }
		  }catch(Exception e){
				e.getMessage();
				
			}
		  return categoryKey;
	  }
	  
  }%>
  
  <!-- /**
	 * Helper method to form the "child-parent" category to match with the "child-parent" page. 
	 * 
	 * @param AssetCategory : Child Category. 
	 * @return String :  Returns "child-parent" name.   
	 */
	 --> 
  <%!private String getCategory(AssetCategory childAssetCategory) {
	  AssetCategory parentAssetCategory = null;
	  AssetVocabulary assetVocabulary = null;
	  StringBuilder categorySB = new StringBuilder();
	  if(childAssetCategory.getParentCategoryId() != 0){
		try{	parentAssetCategory = AssetCategoryLocalServiceUtil.getCategory(childAssetCategory.getParentCategoryId());
		    categorySB.append(childAssetCategory.getName().trim());
			categorySB.append("-");
			categorySB.append(parentAssetCategory.getName().trim());
		}catch(Exception e){
			e.getMessage();
		}
		}else{
			try{
		//	assetVocabulary = AssetVocabularyLocalServiceUtil.getAssetVocabulary(childAssetCategory.getVocabularyId());
			categorySB.append(childAssetCategory.getName().trim());
			/* categorySB.append("-");
			categorySB.append(assetVocabulary.getName().trim()); */
			}catch(Exception e){
				e.getMessage();
			}
		}

	    return categorySB.toString().toUpperCase();
	  
  }%>
  <%!private String cropTitle(String assetEntryTitle) {

		String title = "";
		if(assetEntryTitle.contains("."))
		{
			title = assetEntryTitle.split("\\.")[0];
			 
		}else{
			title = assetEntryTitle;
		}
		return title.toUpperCase();
	}%>
<%
long companyId = themeDisplay.getCompanyId();
long groupId = themeDisplay.getScopeGroupId();
Map<String,Layout> layoutMap = WebServiceUtil.getLayouts(themeDisplay.getScopeGroupId());
List<Object> mixedLst = new ArrayList<Object>();
String categoryId = renderRequest.getParameter("categoryId");
if(categoryId !=null && !categoryId.equalsIgnoreCase("all-news") ){
		long groupIds[] = new long[1];
		AssetCategory cat =  AssetCategoryLocalServiceUtil.getAssetCategory(Long.parseLong(categoryId));
		groupIds[0] = themeDisplay.getScopeGroupId();
		List<AssetCategory> childCats = AssetCategoryLocalServiceUtil.getVocabularyCategories(cat.getVocabularyId(),0,AssetCategoryLocalServiceUtil.getVocabularyCategoriesCount(cat.getVocabularyId()),null);
		long categoryIds[] = new long[childCats.size()];
		if(childCats !=null && childCats.size()>0){
			for(int a=0;a<childCats.size();a++){
				categoryIds[a] = childCats.get(a).getCategoryId();
			}
		}
		AssetEntryQuery assetEntryQuery = new AssetEntryQuery();
		assetEntryQuery.setAnyCategoryIds(categoryIds);
		long classNameIds[] = new long[2];
		classNameIds[0] = ClassNameLocalServiceUtil.getClassNameId("com.liferay.portlet.journal.model.JournalArticle");
		classNameIds[1] = ClassNameLocalServiceUtil.getClassNameId("com.liferay.portlet.documentlibrary.model.DLFileEntry");
		assetEntryQuery.setClassNameIds(classNameIds);
		assetEntryQuery.setGroupIds(groupIds);
		List<AssetEntry> assetByCategories = AssetEntryServiceUtil.getEntries(assetEntryQuery);
		for(AssetEntry entry : assetByCategories){
			if(entry.getClassName().equalsIgnoreCase("com.liferay.portlet.journal.model.JournalArticle")){
				if(permissionChecker.hasPermission(groupId,JournalArticle.class.getName(),entry.getClassPK(),ActionKeys.VIEW)){
					mixedLst.add(JournalArticleLocalServiceUtil.getLatestArticle(entry.getClassPK()));
				}
			}
			else{
				if(permissionChecker.hasPermission(groupId,DLFileEntry.class.getName(),entry.getClassPK(),ActionKeys.VIEW)){
					mixedLst.add(DLFileEntryLocalServiceUtil.getDLFileEntry(entry.getClassPK()));
				}	
			}
		}
}
else{
    
	List<DLFileEntry> latestDocumentsAll = new ArrayList<DLFileEntry>(); 
    List<DLFileEntry> latestDocuments = new ArrayList<DLFileEntry>();
    List<JournalArticle> latestarticlesAll = new ArrayList<JournalArticle>();
    List<JournalArticle> latestarticles = new ArrayList<JournalArticle>();
    
    AssetEntryQuery assetEntryQuery = new AssetEntryQuery();
    assetEntryQuery.setGroupIds(new long[]{themeDisplay.getScopeGroupId()});
    long classNameIds[] = new long[2];
	classNameIds[0] = ClassNameLocalServiceUtil.getClassNameId("com.liferay.portlet.journal.model.JournalArticle");
	classNameIds[1] = ClassNameLocalServiceUtil.getClassNameId("com.liferay.portlet.documentlibrary.model.DLFileEntry");
	assetEntryQuery.setClassNameIds(classNameIds);
    assetEntryQuery.setOrderByCol1("modifiedDate");
    List<AssetCategory> localeCategories = WebUtil.getAllAssetCategoriesByGroupId(renderRequest);
    long catIds[] = new long[localeCategories.size()];
    for(int k=0;k<localeCategories.size();k++){
    	catIds[k] = localeCategories.get(k).getCategoryId();
    }
    assetEntryQuery.setAnyCategoryIds(catIds);
    List<AssetEntry> assetByCategories = AssetEntryServiceUtil.getEntries(assetEntryQuery);
    for(AssetEntry entry :assetByCategories){
    	if(entry.getClassName().equalsIgnoreCase("com.liferay.portlet.journal.model.JournalArticle")){
    		latestarticlesAll.add(JournalArticleLocalServiceUtil.getLatestArticle(entry.getClassPK()));
    	}
    	else{
    		latestDocumentsAll.add(DLFileEntryLocalServiceUtil.getDLFileEntry(entry.getClassPK()));
    	}
    }	
    
    for(DLFileEntry dlFile : latestDocumentsAll){
    	List<AssetCategory> hasDocumentCategories = AssetEntryLocalServiceUtil.getEntry("com.liferay.portlet.documentlibrary.model.DLFileEntry",dlFile.getFileEntryId()).getCategories();
		if(null !=hasDocumentCategories && hasDocumentCategories.size()>0){
			for(int s=0;s<hasDocumentCategories.size();s++){
				String categoryPage = "";
				categoryPage = getAssetEntryPage(hasDocumentCategories.get(s), layoutMap, new ArrayList<String>(), renderRequest);
				if(!categoryPage.equalsIgnoreCase("")){
			       	if(permissionChecker.hasPermission(groupId,DLFileEntry.class.getName(),dlFile.getFileEntryId(),ActionKeys.VIEW)){
			       		latestDocuments.add(dlFile);
			       		break;
			       	}
				}
			}	
		}	
    }
    
   	for(JournalArticle article : latestarticlesAll){
   		List<AssetCategory> hasJornalCategories = AssetEntryLocalServiceUtil.getEntry("com.liferay.portlet.journal.model.JournalArticle",article.getResourcePrimKey()).getCategories();
		if(null !=hasJornalCategories && hasJornalCategories.size()>0){
			for(int q=0;q<hasJornalCategories.size();q++){
				String categoryPage = "";
				categoryPage = getAssetEntryPage(hasJornalCategories.get(q), layoutMap, new ArrayList<String>(), renderRequest);
				if(!categoryPage.equalsIgnoreCase("")){
			   		if(permissionChecker.hasPermission(groupId,JournalArticle.class.getName(),article.getResourcePrimKey(),ActionKeys.VIEW)){
			   			 if(WebServiceUtil.isTransmittal(article)){
                            if(WebServiceUtil.hasTransmittalPermission(article, themeDisplay.getUserId())){
                                  latestarticles.add(article);
                                  break;
                            }
                     	 }else{
                          	  latestarticles.add(article);
                          	  break;
                     	 }  
			   		}
				}
			}	
		}	
   	}
    
    for(JournalArticle article : latestarticles){
    	mixedLst.add((Object)article);
    }
    for(DLFileEntry document : latestDocuments){
    	mixedLst.add((Object)document);
    }
}  
    Collections.sort(mixedLst,new Comparator<Object>(){
    	public int compare(Object o1, Object o2) {
    		Date d1 = null;
    		Date d2 = null;
       			if(o1 instanceof JournalArticle)
       				d1 = ((JournalArticleModel)o1).getModifiedDate();
       			else
       				d1 = ((DLFileEntryModel)o1).getModifiedDate();
       			
       			if(o2 instanceof JournalArticle )
       				d2 = ((JournalArticleModel)o2).getModifiedDate();
       			else
       				d2 = ((DLFileEntryModel)o2).getModifiedDate();
       			
       			return d2.compareTo(d1);
       				
    	}
	});
%>

<c:choose>
	<c:when test="<%= (mixedLst != null) && (mixedLst.size() > 0) %>">
				<%
					DLFileEntry document = null;
					JournalArticle article = null;
					String className = null;
				    AssetCategory assetCategory = null;
				    String summary = null;
				    String fileTitle = null;
			        PortletURL assetURL = null;
			        AssetEntry assetEntry = null;
			        String mimeType = null;
			        String categoryFileURL = null;
			        String typeOfLink = "";
	
				    Date displayDate = null;
				    SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy",locale);
				    String displayDateStr = null;
					PortletURL portletURL = renderResponse.createRenderURL();
					if(categoryId !=null)
						portletURL.setParameter("categoryId",categoryId);
					SearchContainer searchContainer = new SearchContainer(renderRequest, null, null, SearchContainer.DEFAULT_CUR_PARAM, 5, portletURL, null, "there-are-no-events-on-this-day");
					searchContainer.setHover(false);
					searchContainer.setTotal(mixedLst.size());
					List<Object> results= ListUtil.subList(mixedLst,searchContainer.getStart(),searchContainer.getEnd());
					searchContainer.setResults(results);
					List resultRows = searchContainer.getResultRows();
					for (int i = 0; i < results.size(); i++) {
						Object obj = results.get(i);
						ResultRow row = null;
						List<AssetCategory> assetCategories = new ArrayList<AssetCategory>();
							if(obj instanceof JournalArticle){
								article = (JournalArticle)obj;
								summary = article.getTitle(locale);
								className = JournalArticle.class.getName();
								assetCategories = AssetEntryLocalServiceUtil.getEntry(className,article.getResourcePrimKey()).getCategories();
								assetEntry = AssetEntryLocalServiceUtil.getEntry(className,article.getResourcePrimKey());
								categoryFileURL = article.getArticleId();
								displayDate = article.getDisplayDate();
								row = new ResultRow(article,article.getResourcePrimKey(),i);
								if (article != null && article.getTemplateId() != null && !article.getTemplateId().isEmpty()) 
								{
									JournalTemplate journalTemplate =
										JournalTemplateLocalServiceUtil.getTemplate(article.getGroupId(), article.getTemplateId());
									if (journalTemplate != null && journalTemplate.getNameCurrentValue().equalsIgnoreCase("COACH_LINK_TEMPLATE")) 
									{
										com.liferay.portal.kernel.xml.Document xmlDocument = SAXReaderUtil.read(article.getContent());
										categoryFileURL = xmlDocument.selectSingleNode(com.coach.cip.util.Constants.DYNAMIC_ELEMENT_NAME + "url" + com.coach.cip.util.Constants.DYNAMIC_CONTENT).getText().toLowerCase();
										typeOfLink = xmlDocument.selectSingleNode(com.coach.cip.util.Constants.DYNAMIC_ELEMENT_NAME + "type" + com.coach.cip.util.Constants.DYNAMIC_CONTENT).getText();
									}
								}
							}
							else{
								document = (DLFileEntry)obj;
								summary = document.getTitle();
								className = DLFileEntry.class.getName();
								assetCategories = AssetEntryLocalServiceUtil.getEntry(className,document.getFileEntryId()).getCategories();
								assetEntry = AssetEntryLocalServiceUtil.getEntry(className,document.getFileEntryId());
								categoryFileURL = WebServiceUtil.displayFileURL(assetEntry.getClassPK(), renderRequest, renderResponse, themeDisplay);
								displayDate = document.getModifiedDate();
								row = new ResultRow(document,document.getFileEntryId(),i);
							}
							if(assetCategories !=null && assetCategories.size()>0){
								fileTitle = cropTitle(assetEntry.getTitleCurrentValue());
								mimeType = assetEntry.getMimeType().trim();
								String catepage = "";
								for(AssetCategory cat : assetCategories){
									catepage = getAssetEntryPage(cat, layoutMap, new ArrayList<String>(), renderRequest);
									if(!catepage.equalsIgnoreCase("")){
										assetCategory = cat;
										break;
									}
								}
								String categoryKey = "";
								List<String> categoryNamesList = new ArrayList<String>();
								
								categoryKey = getAssetEntryPage(assetCategory, layoutMap, categoryNamesList, renderRequest);
								categoryNamesList = (List<String>)renderRequest.getAttribute("categoryNamesList");
								assetURL = PortletURLFactoryUtil.create(request,"SideNavigation_WAR_cipportlet",layoutMap.get(categoryKey).getPlid(),PortletRequest.ACTION_PHASE);
								 assetURL.setParameter("myaction", "processDocumentURL");
								 assetURL.setParameter("fileURL", categoryFileURL);
								 assetURL.setParameter("fileTitle", fileTitle);
								 assetURL.setParameter("fileType", className);
								 assetURL.setParameter("classPK", String.valueOf(assetEntry.getClassPK()));
								 assetURL.setParameter("typeOfLink", typeOfLink);
								 assetURL.setParameter("mimeType", mimeType);
								 assetURL.setParameter("backButton", "true");
								 assetURL.setWindowState(WindowState.NORMAL);
								 if(Validator.isNotNull(categoryNamesList)){
									 Collections.reverse(categoryNamesList);
									 for (int k = 1; k <categoryNamesList.size() ; k++) {
										 if(k==1){
											 assetURL.setParameter("level5Parent", categoryNamesList.get(k));
										 }
										 if(k==2){
											 assetURL.setParameter("level6Parent", categoryNamesList.get(k));
										 }
										 if(k==3){
											 assetURL.setParameter("level7Parent", categoryNamesList.get(k));
										 }
										 if(k==4){
											 assetURL.setParameter("level8Parent", categoryNamesList.get(k));
										 }
									 }
								 }
							}
						summary = StringUtil.shorten(summary,Constants.NEWS_ABSTRACT_LENGTH);
						displayDateStr = sdf.format(displayDate);
						String moreLink = LanguageUtil.get(pageContext,"hyperlink-more");
						row.addText("<table><tr><td style='padding: 1px;color:#EB3528'>"+displayDateStr+"</td></tr><tr><td style='padding: 0px;''>"+summary+"</td></tr>");
						row.addText("<tr><td style='padding: 1px;'><a style='font-weight: bold; color: black;' href='"+assetURL+" '><span class='learnMoreBtn'>"+moreLink+" &gt;&gt;</span></a></td></tr></table>");
						resultRows.add(row);
					}
				%>	
				<liferay-ui:search-iterator searchContainer="<%= searchContainer %>" />
	</c:when>
	<c:otherwise>
		<div style="margin-left: 10px; margin-right: 10px;">
			<b><spring:message code='validation.message' />
			</b>
		</div>
		<br/>
	</c:otherwise>


</c:choose>
    