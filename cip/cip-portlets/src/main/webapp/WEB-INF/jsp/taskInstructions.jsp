<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.coach.cip.util.Constants"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<script type="text/javascript">

$(document).ready(function() {
			Liferay.on(
		           'retrieveValue',
		           function(event, data) {
		        	   popup(data);
					}
				);
			
			function popup(tempData){
				var url = '<portlet:resourceURL id="populateTasks"></portlet:resourceURL>'+ '?fakeId=345&tempData='+ tempData;
				var xhr = new XMLHttpRequest();
				xhr.onreadystatechange = function() {
				if (xhr.readyState == 4 && xhr.status==200) {
					var responseText = document.getElementById("taskInstructionsId");
					responseText.innerHTML = xhr.responseText;
					}
				};
				xhr.open("GET", url, true);
				xhr.send();
			}
			
});
</script>


<div id="taskInstructionsId">
Please Click On Instructions Which are On Tasks Portlet.

</div>