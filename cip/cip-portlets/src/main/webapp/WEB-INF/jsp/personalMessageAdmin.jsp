<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@ page import="java.util.List"%>
<%@ page import="com.coach.cip.common.dto.PersonalMessageVO"%>
<%@ page import="com.coach.cip.portlets.controller.QuickLinksController"%>
<%@ page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<%@ page import="javax.portlet.PortletURL"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />


<script type="text/javascript"
      src="<%= themeDisplay.getPathThemeCss()%>/../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
      src="<%= themeDisplay.getPathThemeCss()%>/../js/jquery-ui-1.8.19.custom.min.js"></script>

<script>
	$(document).ready(function() {

		$("#deleteMessage").click(
				function() {
					$("#pmActionName").val("Delete Messages");

					if ($("#personalMessageForm").find(
							"input:checkbox").is(':checked')) {

						if (confirm($('#pmAlertDeleteConfirm')
								.val())) {

							$("#personalMessageForm").submit();
						}
					} else {
						alert($('#pmAlertDelete').val())
					}
		});
		
		$("#createMessage").click(function() {
	           
			$("#pmActionName").val("Add Messages");
			$("#personalMessageForm").submit();
		});
	});
</script>


<portlet:actionURL var="personalMessageUrl">
	<portlet:param name="myaction" value="deletPersonalMessageForm" />
</portlet:actionURL>

<jsp:include page="/WEB-INF/jsp/message.jsp" />

<form:form name="personalMessageForm" method="post"
	action="${personalMessageUrl}" id="personalMessageForm">
	<liferay-ui:search-container delta="10"
		emptyResultsMessage="no-messages-were-found"
		rowChecker="<%= new RowChecker(renderResponse) %>">

		<%
			int counter = 0;
					String str;
		%>

		<liferay-ui:search-container-results>
			<%
				List<PersonalMessageVO> tempResults = (List<PersonalMessageVO>) request.getAttribute("personalMessageVOList");
							if (tempResults != null) {
								results = ListUtil.subList(tempResults, searchContainer.getStart(), searchContainer.getEnd());
								total = tempResults.size();
								pageContext.setAttribute("results", results);
								pageContext.setAttribute("total", total);
							}
			%>
		</liferay-ui:search-container-results>

		<liferay-ui:search-container-row
			className="com.coach.cip.common.dto.PersonalMessageVO"
			modelVar="personalMessageVO" keyProperty="messageId">

			<%-- 	<liferay-ui:search-container-column-text name="Id">
			  <c:out value="<%= (counter = counter+1) %>"></c:out>
			</liferay-ui:search-container-column-text> --%>

			<liferay-ui:search-container-column-text name="label.personalmessageadmin.messagesubject">
				<a
					href="<portlet:actionURL>
							<portlet:param name="myaction" value="editPersonalMessage" />
							<portlet:param name="messageId" value="${personalMessageVO.messageId}" />
						</portlet:actionURL>">
						
					<c:choose>
								<c:when test="${fn:length(personalMessageVO.subject) > 25}">
									<c:set var="msgSubjectSubString"
										value="${fn:substring(personalMessageVO.subject, 0, 25)}" />
									<c:out value="${msgSubjectSubString}..."></c:out>
								</c:when>
								<c:otherwise>
									<c:out value="${personalMessageVO.subject}"></c:out>
								</c:otherwise>
							</c:choose>	
					 </a>
			</liferay-ui:search-container-column-text>

			<liferay-ui:search-container-column-text name="label.personalmessageadmin.messagetype"
				property="messageType">

			</liferay-ui:search-container-column-text>



			<liferay-ui:search-container-column-text name="label.personalmessageadmin.displaydate"
				property="displayDate">

			</liferay-ui:search-container-column-text>

			<liferay-ui:search-container-column-text name="label.personalmessageadmin.expirationdate"
				property="expirationDate">

			</liferay-ui:search-container-column-text>


		</liferay-ui:search-container-row>

		<liferay-ui:search-iterator searchContainer="<%= searchContainer %>"
			paginate="<%= true %>" />

	</liferay-ui:search-container>
    <div>
    <% 
    List<PersonalMessageVO> temResults = (List<PersonalMessageVO>) request.getAttribute("personalMessageVOList");
    if(temResults.size() >0 ){ %>
	<input class="submitClass" style="margin-top: 13px; margin-bottom: 10px; width: 145px;"
				type="button" value="<spring:message code='label.personalmessageadmin.deletemessage'/>"
				name="personalMessageOperation"	id="deleteMessage" />
	<%	}  %>
    <input class="submitClass" style="margin-top: 13px; margin-bottom: 10px; width: 110px;"
				type="button" value="<spring:message code='label.personalmessageadmin.addmessage'/>" name="personalMessageOperation"
				id="createMessage"/ >
	<input type="hidden" value="" id="pmActionName" name="personalMessageOperation" />
	</div>
	
	<input type="hidden"
		value="<spring:message code='label.personalmessageadmin.alert.deleteconfirm'/>"
		id="pmAlertDeleteConfirm" />
	<input type="hidden"
		value="<spring:message code='label.personalmessageadmin.alert.delete'/>"
		id="pmAlertDelete" />	
</form:form>

