


<%@page import="com.coach.cip.util.WebUtil"%>
<%@page import="com.liferay.portlet.asset.model.AssetCategory"%>
<%@page import="com.liferay.portlet.asset.model.AssetEntry"%>

			<%			
						/* String pdfPortletNameSpace = StringPool.UNDERLINE + WebUtil.getPortletInstanceId(renderRequest, "column-2")+StringPool.UNDERLINE;
						pdfPortletNameSpace = pdfPortletNameSpace.replaceAll(",",""); */
			
						/* Displaying Level 3 child (or Level 4) Documents or Categories Start. */
						if((sub3AssetEntryList!=null && sub3AssetEntryList.size()>0) || (sub3CategoryList!=null && sub3CategoryList.size()>0))
						{
							/* level3child ul start. */
							out.println(" <ul id ='level3child' style='padding-left: 10px;'>");
							%>
								<!-- Displaying Level 3 child (or Level 4) Asset Entries Documents Start. -->
								<%=displayAssetEntries(sub3AssetEntryList,targetLayout,false,"",fileName, renderRequest, renderResponse, themeDisplay) %>
								<!-- Displaying Level 3 child (or Level 4) Asset Entries Documents End. -->
							<% 
							
							 /* Level 3 child (or Level 4) Categories Start. */ 
							for(AssetCategory sub3Category : sub3CategoryList){
								%>
								<!-- Level 3 child (or Level 4) Categories li Start.  --> 
								<c:set var="level5Parent" value='${level5Parent}'></c:set>
							    
								<%
							/* 	HttpServletRequest servletRequest = WebUtil.getHttpServletRequest(renderRequest); */
									String level5Parent = (String) pageContext.getAttribute("level5Parent");
								if(Validator.isNull(level5Parent))
									{
										level5Parent = Validator.isNotNull(renderRequest.getAttribute("level5Parent")) ? (String)renderRequest.getAttribute("level5Parent") 
												 : servletRequest.getParameter(pdfPortletNameSpace+"level5Parent"); 
										if(Validator.isNotNull(level5Parent))
											level5Parent = level5Parent.replaceAll("\\+", " ");
										
									}
							
								 if (level5Parent != null && level5Parent !="" && level5Parent.equals(sub3Category.getTitle(locale)))
								    {
								    	out.println("<li class='dcjq-current-parent'>");
								    	
								    }else{
								    	
								    	out.println("<li>");
								    }
								%>								
								<!-- Dispalying Level 3 child (or Level 4) Categories : Start. -->
								<%
								//List<AssetEntry> level4CategoryAssetEntryList = WebUtil.fetchAssetEntryByName(sub3Category.getName());
								List<AssetEntry> level4CategoryAssetEntryList = WebUtil.fetchAssetEntryByName(sub3Category.getName(),assetCategoryEntryMap.get(sub3Category.getCategoryId()+"-"+sub3Category.getName()),renderRequest);
								if(level4CategoryAssetEntryList.size()>0)	{
									out.println(displayAssetEntries(level4CategoryAssetEntryList.subList(0,1),targetLayout,true,"level5Parent,"+sub3Category.getTitle(locale),fileName, renderRequest, renderResponse, themeDisplay)); 
								}else{
									out.println("<a>"
										+ sub3Category.getTitle(locale)
										+ "</a>");
								}
								%>		
								<%-- <a ><%= sub3Category.getName() %></a> --%>
								<!-- Dispalying Level 3 child (or Level 4) Categories : End. -->
								<%
								/* Displaying Level 4 child (or Level 5) Documents or Categories Start. */
							//	List<AssetEntry> sub4AssetEntryList = WebUtil.fetchAssetEntries(sub3Category,renderRequest);
								List<AssetEntry> sub4AssetEntryList = WebUtil.fetchAssetEntries(sub3Category,assetEntriesMap.get(sub3Category.getCategoryId()));
							//	List<AssetCategory> sub4CategoryList = WebUtil.getChildCategories(sub3Category,renderRequest);
								List<AssetCategory> sub4CategoryList = WebUtil.getChildCategories(assetCategoryMap.get(sub3Category.getCategoryId()),renderRequest);
								if((sub4AssetEntryList!=null && sub4AssetEntryList.size()>0) || (sub4CategoryList!=null && sub4CategoryList.size()>0))
								{
									/* level4child ul Start. */
									out.println(" <ul id ='level4child' style='padding-left: 10px;'>");	
									%>
										<!-- Displaying Level 4 child (or Level 5) Asset Entries Documents Start. -->
										<%=displayAssetEntries(sub4AssetEntryList,targetLayout,false, "level5Parent,"+sub3Category.getTitle(locale),fileName, renderRequest, renderResponse, themeDisplay) %>
										<!-- Displaying Level 4 child (or Level 5) Asset Entries Documents End. -->
									<% 
									 /* Level 4 child (or Level 5) Categories Start. */ 
									for(AssetCategory sub4Category : sub4CategoryList){
										%>
										<!-- Level 4 child (or Level 5) Categories li Start.  --> 
										<c:set var="level6Parent" value='${level6Parent}'></c:set>
							
										<%
											String level6Parent = (String) pageContext.getAttribute("level6Parent");
											if(Validator.isNull(level6Parent))
											{
												//level6Parent = (String)renderRequest.getAttribute("level6Parent");
												level6Parent = Validator.isNotNull(renderRequest.getAttribute("level6Parent")) ? (String)renderRequest.getAttribute("level6Parent") 
														 : servletRequest.getParameter(pdfPortletNameSpace+"level6Parent"); 
												if(Validator.isNotNull(level6Parent))
													level6Parent = level6Parent.replaceAll("\\+", " ");
											}
										    if (level6Parent != null && level6Parent !="" && level6Parent.equals(sub4Category.getTitle(locale)))
										    {
										    	out.println("<li class='dcjq-current-parent'>");
										    	
										    }else{
										    	
										    	out.println("<li>");
										    }
										%>	
										<!-- Dispalying Level 4 child (or Level 5) Categories. -->
										<%
										//	List<AssetEntry> level5CategoryAssetEntryList = WebUtil.fetchAssetEntryByName(sub4Category.getName());
										List<AssetEntry> level5CategoryAssetEntryList = WebUtil.fetchAssetEntryByName(sub4Category.getName(),assetCategoryEntryMap.get(sub4Category.getCategoryId()+"-"+sub4Category.getName()),renderRequest);
											if(level5CategoryAssetEntryList.size()>0)	{
												out.println(displayAssetEntries(level5CategoryAssetEntryList.subList(0,1),targetLayout,true,"level5Parent,"+sub3Category.getTitle(locale)+";level6Parent,"+sub4Category.getTitle(locale),fileName, renderRequest, renderResponse, themeDisplay)); 
											}else{
												out.println("<a>"
													+ sub4Category.getTitle(locale) 
													+ "</a>");
											}
										%>
										<%-- <a ><%= sub4Category.getName() %></a>  --%>
										<%
										/* Displaying Level 5 child (or Level 6) Documents or Categories Start. */
									//	List<AssetEntry> sub5AssetEntryList = WebUtil.fetchAssetEntries(sub4Category,renderRequest);
										List<AssetEntry> sub5AssetEntryList = WebUtil.fetchAssetEntries(sub4Category,assetEntriesMap.get(sub4Category.getCategoryId()));
									//  List<AssetCategory> sub5CategoryList = WebUtil.getChildCategories(sub4Category,renderRequest);
										List<AssetCategory> sub5CategoryList = WebUtil.getChildCategories(assetCategoryMap.get(sub4Category.getCategoryId()),renderRequest);
										if((sub5AssetEntryList!=null && sub5AssetEntryList.size()>0) || (sub5CategoryList!=null && sub5CategoryList.size()>0))
										{
											/* level5child ul Start. */
											out.println(" <ul id ='level5child' style='padding-left: 10px;'>");	
											%>
											
												<!-- Displaying Level 5 child (or Level 6) Asset Entries Documents Start. -->
												<%=displayAssetEntries(sub5AssetEntryList,targetLayout,false,"level5Parent,"+sub3Category.getTitle(locale)+";level6Parent,"+sub4Category.getTitle(locale),fileName, renderRequest, renderResponse, themeDisplay) %>
												<!-- Displaying Level 5 child (or Level 6) Asset Entries Documents End. -->
											
											<%
											
											/* Level 5 child (or Level 6) Categories Start. */ 
										for(AssetCategory sub5Category : sub5CategoryList){
										%>
										<!-- Level 5 child (or Level 6) Categories li Start.  --> 
										<c:set var="level7Parent" value='${level7Parent}'></c:set>
							
										<%
											String level7Parent = (String) pageContext.getAttribute("level7Parent");
											if(Validator.isNull(level7Parent))
											{
												//level7Parent = (String)renderRequest.getAttribute("level7Parent");
												level7Parent = Validator.isNotNull(renderRequest.getAttribute("level7Parent")) ? (String)renderRequest.getAttribute("level7Parent") 
														 : servletRequest.getParameter(pdfPortletNameSpace+"level7Parent"); 
												if(Validator.isNotNull(level7Parent))
												level7Parent = level7Parent.replaceAll("\\+", " ");
											}
										    if (level7Parent != null && level7Parent !="" && level7Parent.equals(sub5Category.getTitle(locale)))
										    {
										    	out.println("<li class='dcjq-current-parent'>");
										    	
										    }else{
										    	
										    	out.println("<li>");
										    }
										%>	
										<!-- Dispalying Level 5 child (or Level 6) Categories. -->
										<%
										//	List<AssetEntry> level5CategoryAssetEntryList = WebUtil.fetchAssetEntryByName(sub5Category.getName());
										List<AssetEntry> level6CategoryAssetEntryList = WebUtil.fetchAssetEntryByName(sub5Category.getName(),assetCategoryEntryMap.get(sub5Category.getCategoryId()+"-"+sub5Category.getName()),renderRequest);
											if(level6CategoryAssetEntryList.size()>0)	{
												out.println(displayAssetEntries(level6CategoryAssetEntryList.subList(0,1),targetLayout,true,"level5Parent,"+sub3Category.getTitle(locale)+";level6Parent,"+sub4Category.getTitle(locale)
														+";level7Parent,"+sub5Category.getTitle(locale),fileName, renderRequest, renderResponse, themeDisplay)); 
											}else{
												out.println("<a>"
													+ sub5Category.getTitle(locale)
													+ "</a>");
											}
										%>
										    <%-- <a ><%= sub5Category.getName() %></a> --%>
										    <%
										/* Displaying Level 6 child (or Level 7) Documents or Categories Start. */
									//	List<AssetEntry> sub6AssetEntryList = WebUtil.fetchAssetEntries(sub5Category,renderRequest);
										List<AssetEntry> sub6AssetEntryList = WebUtil.fetchAssetEntries(sub5Category,assetEntriesMap.get(sub5Category.getCategoryId()));    
									//	List<AssetCategory> sub6CategoryList = WebUtil.getChildCategories(sub5Category,renderRequest);
									    List<AssetCategory> sub6CategoryList = WebUtil.getChildCategories(assetCategoryMap.get(sub5Category.getCategoryId()),renderRequest);
										if((sub6AssetEntryList!=null && sub6AssetEntryList.size()>0) || (sub6CategoryList!=null && sub6CategoryList.size()>0))
										{
											/* level6child ul Start. */
											out.println(" <ul id ='level6child' style='padding-left: 10px;'>");	
											%>
											
											<!-- Displaying Level 6 child (or Level 7) Asset Entries Documents Start. -->
											<%=displayAssetEntries(sub6AssetEntryList,targetLayout,false,"level5Parent,"+sub3Category.getTitle(locale)+";level6Parent,"+sub4Category.getTitle(locale)
												+";level7Parent,"+sub5Category.getTitle(locale),fileName, renderRequest, renderResponse, themeDisplay) %>
											<!-- Displaying Level 6 child (or Level 7) Asset Entries Documents End. -->
											
										<%
										
										/* Level 6 child (or Level 7) Categories Start. */ 
										for(AssetCategory sub6Category : sub6CategoryList){
											%>		
										<!-- Level 6 child (or Level 7) Categories li Start.  --> 
											<c:set var="level8Parent" value='${level8Parent}'></c:set>
							
											<%
											String level8Parent = (String) pageContext.getAttribute("level8Parent");
											if(Validator.isNull(level8Parent))
											{
												//level8Parent = (String)renderRequest.getAttribute("level8Parent");
												level8Parent = Validator.isNotNull(renderRequest.getAttribute("level8Parent")) ? (String)renderRequest.getAttribute("level8Parent") 
														 : servletRequest.getParameter(pdfPortletNameSpace+"level8Parent");
												if(Validator.isNotNull(level8Parent))
												level8Parent = level8Parent.replaceAll("\\+", " ");
											}
										    if (level8Parent != null && level8Parent !="" && level8Parent.equals(sub6Category.getTitle(locale)))
										    {
										    	out.println("<li class='dcjq-current-parent'>");
										    	
										    }else{
										    	
										    	out.println("<li>");
										    }
										%>	
											<!-- Dispalying Level 5 child (or Level 6) Categories. -->
										    <a ><%= sub6Category.getTitle(locale) %></a>
											<%
											/* Displaying Level 7 child (or Level 8) Documents or Categories Start. */
										//	List<AssetEntry> sub7AssetEntryList = WebUtil.fetchAssetEntries(sub6Category,renderRequest);
											List<AssetEntry> sub7AssetEntryList = WebUtil.fetchAssetEntries(sub6Category,assetEntriesMap.get(sub6Category.getCategoryId()));
										//	List<AssetCategory> sub7CategoryList = WebUtil.getChildCategories(sub6Category,renderRequest);
										    List<AssetCategory> sub7CategoryList = WebUtil.getChildCategories(assetCategoryMap.get(sub6Category.getCategoryId()),renderRequest);
											if((sub7AssetEntryList!=null && sub7AssetEntryList.size()>0) || (sub7CategoryList!=null && sub7CategoryList.size()>0))
											{
												
												/* level7child ul Start. */
												out.println(" <ul id ='level7child' style='padding-left: 10px;'>");	
												%>
												<!-- Displaying Level 7 child (or Level 8) Asset Entries Documents Start. -->
											<%=displayAssetEntries(sub7AssetEntryList,targetLayout,false,"level5Parent,"+sub3Category.getTitle(locale)+";level6Parent,"+sub4Category.getTitle(locale)
												+";level7Parent,"+sub5Category.getTitle(locale)+";level8Parent,"+sub6Category.getTitle(locale),fileName, renderRequest, renderResponse, themeDisplay) %>
											<!-- Displaying Level 7 child (or Level 8) Asset Entries Documents End. -->
											
												<%
											// Need to write logic to display Level 7 child (or Level 8) categories.
											
												out.println(" </ul>");
												/* level7child ul End. */
											}
											
											
											out.println("</li>");
											/*  Level 7 child (or Level 8) Categories li End. */  
										}
										/* Level 6 child (or Level 7) Categories End. */
										

										out.println(" </ul>"); 
											/* level6child ul End. */
										} 
										%>
										
										</li>
										<!-- Level 5 child (or Level 6) Categories li End.  --> 
										<%
									}
											 /* Level 5 child (or Level 6) Categories End. */ 		
											
											out.println(" </ul>"); 
											/* level5child ul End. */
										}
										/* Displaying Level 5 child (or Level 6) Documents or Categories End. */
										
										%></li>
										<!-- Level 4 child (or Level 5) Categories li End.  --> 
										<% 
										
									}
									 /* Level 4 child (or Level 5) Categories End. */ 
									
									out.println(" </ul>"); 
									/* level4child ul End. */
								}
								
								/* Displaying Level 4 child (or Level 5) Documents or Categories End. */
								
								%></li>
								<!-- Level 3 child (or Level 4) Categories li End.  --> 
								<% 
							}
							/* Level 3 child (or Level 4) Categories End. */ 
							out.println(" </ul>"); 
							/* level3child ul End. */
						}/* Displaying Level 3 child (or Level 4) Documents or Categories End. */	
                         %>