<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.coach.cip.util.Constants"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="aui" uri="http://alloy.liferay.com/tld/aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import="com.liferay.portal.model.User"%>
<%@ page import="com.coach.cip.util.WebUtil"%>
<%@ page import="com.liferay.portal.model.Phone"%>
<%@ page import="com.liferay.portal.service.PhoneLocalServiceUtil"%>
<%@ page import="com.liferay.portal.model.Contact"%>
<%@ page import="java.util.List"%>


<portlet:defineObjects />
<liferay-theme:defineObjects />

<!--<script src="http://jquery.bassistance.de/validate/jquery.validate.js" type="text/javascript"></script> -->

<script	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.validate.feedback.js"	type="text/javascript"></script>

<script	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.selectbox-0.5_style_2.js"	type="text/javascript"></script>
<script type="text/javascript">

$(document).ready(function() {
	
	$('.styledselect_form_1').selectbox({ inputClass: "styledselect_form_1" });
	
	var feedbackTopicStatus = false;

	$("#feedbackForm").validate({
		
		submitHandler: function(form) {
			if(feedbackTopicStatus){
				form.submit();
			}
		},
		messages: {
			comments:"<spring:message code='validation.feedback.msg.comments'/>",
			firstName: "<spring:message code='validation.feedback.msg.firstname'/>",
			lastName:"<spring:message code='validation.feedback.msg.lastname'/>",
			emailAddress:"<spring:message code='validation.feedback.msg.emailaddress'/>",
			//phoneNumber:"<spring:message code='validation.feedback.msg.phonenumber'/>"
		}
	});

	$("#submitId").click(function(){
		$(".red-left").fadeOut("slow");
		$(".red-right").fadeOut("slow");
		$(".green-left").fadeOut("slow");
		$(".green-right").fadeOut("slow");
		var feedbackTopic = jQuery.trim($("#feedbackTopic_input").val());
		$("#feedbackTopic > option").each(function() {
		  if (feedbackTopic==this.text)
		  {
			if(this.value == 0){									
				$("#feedbackTopic_errors").show();
				feedbackTopicStatus=false;
			 }
			 else{									
				$("#feedbackTopic_errors").hide();
				feedbackTopicStatus=true;
			}								
		  }
		});	
		
	});
	
	$("#feedbackTopic_container").find("ul").find("li").live('click',function() {
		var feedbackTopic = jQuery.trim($("#feedbackTopic_input").val());
		$("#feedbackTopic > option").each(function() {
		  if (feedbackTopic==this.text)
		  {
			if(this.value == 0){									
				$("#feedbackTopic_errors").show();
				feedbackTopicStatus=false;
			 }
			 else{									
				 $("#feedbackTopic_errors").hide();
				 feedbackTopicStatus=true;
			}								
		  }
		});	

	                                              
	});
	
	
});

 function cancelBack()   
 {  
     if(event.keyCode==8)
     {
        return false;
     }
 } 

</script>

<style type="text/css">
form.feedbackForm label.feedback_error {
	color: red; 
}

form.feedbackForm {
	font-size: 1.0em;
	color: #333;
}

form.feedbackForm label {
	color: #333;
}

form.feedbackForm label {
	width: 100px;
}

input {
	border: 1px solid black;
}

input:focus {
	border: 1px dotted black;
}

form.feedbackForm .gray * {
	color: gray;
}

textarea {
	margin: 0;
	padding: 1px;
	font-size: 100%;
	font-family: inherit;
}

.inp-formOne {
	width: 205px;
	padding: 2px 5px;
	float: left;
	background: white;
}

.submitBtn{
	background-color: black;
color: white;
margin-left: 4px;
height: 29px;
width: 85px;
border: none;
font-family: inherit;
font-size: 12px;
margin-top: 3px;
font-weight: bold;
}

.cancelBtn{
	background-color: black;
color: white;
margin-left: 4px;
height: 29px;
width: 85px;
border: none;
font-family: inherit;
font-size: 13px;
margin-top: 3px;
font-weight: bold;
}

#feedbackTopic_input{
	margin-left:-2px;
}


</style>


<%
	String userId = renderRequest.getRemoteUser();

	String firstName = "";
	String lastName = "";
	String emailAddress = "";
	String phoneNumber = "";
	String readOnly = "";
	String lastNameReadOnly = "";
	String onKeyDown="";
	String onKeyDownLastName="";

	User user1 = null;
	if (userId != null && userId.length() > 0) {
		readOnly = "readOnly";
		onKeyDown = "onKeyDown=\"javascript: return cancelBack()\";";
		user1 = UserLocalServiceUtil.getUserById(Long.valueOf(userId));
		firstName = user1.getFirstName();
		lastName = user1.getLastName();
		if (lastName != null && (!(lastName.equals("")))) {
			lastNameReadOnly = "readOnly";
			onKeyDownLastName = "onKeyDown=\"javascript: return cancelBack()\";";
		}
		emailAddress = user1.getEmailAddress();
		List<Phone> personalPhones = PhoneLocalServiceUtil.getPhones(
				user1.getCompanyId(), Contact.class.getName(),
				user1.getContactId());
		for (Phone phone : personalPhones) {
			if (phone.isPrimary()) {
				phoneNumber = phone.getNumber();
			}
		}

	}
	List<String[]> lstTopics = WebUtil.getDynamicDataListRecords(renderRequest, Constants.SHARE_YOUR_FEEDBACK_TOPIC);
	List<String[]> reportProblemTopicList = WebUtil.getDynamicDataListRecords(renderRequest, Constants.REPORT_A_PROBLEM_TOPIC);
	String title = (String)request.getAttribute("title");
	renderResponse.setTitle(LanguageUtil.get(locale, title));
	
%>

<portlet:actionURL var="feedbackActionURL">
	<portlet:param name="myaction" value="submitFeedback" />
</portlet:actionURL>

<form name="<portlet:namespace/>feedbackForm" class="feedbackForm" id="feedbackForm" method="POST" autocomplete="off" action="<%=feedbackActionURL.toString()%>">
	<div>
		<jsp:include page="/WEB-INF/jsp/message.jsp" />

		<!-- Setting redirectURL in hidden field  -->
		<input type="hidden" name="<portlet:namespace/>redirectURL"
			value="<%=renderResponse.createRenderURL().toString()%>" />

		<div style="margin-left: 10px">
			<table width="560px">
				<tr>
					<td colspan="3"><label><b><spring:message code='label.feedback.comments' /> <font color="red">*</font></b></label><br>
						<textarea rows="12" cols="107" name="comments" id="comments" class="required" style="resize: none;"></textarea>
					</td>
				</tr>

				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				
				<tr>
					<td width="220px"><label><b><spring:message code='label.feedback.firstname' /> <font color="red">*</font></b></label>
					</td>
					<td width="120px">&nbsp;</td>
					<td width="220px"><label><b><spring:message	code='label.feedback.lastname' /> <font color="red">*</font></b></label>
					</td>
				</tr>

				<tr>
					<td width="220px" valign="top">
						<input type="text" name="firstName" id="firstName" class="required inp-formOne" value="<%=firstName%>" <%=readOnly%> <%=onKeyDown%> />
					</td>
					<td width="120px" valign="top">&nbsp;</td>
					<td width="220px" valign="top">
						<input type="text" name="lastName" id="lastName" class="required inp-formOne" value="<%=lastName%>" <%=lastNameReadOnly%> <%=onKeyDownLastName%> /></td>
				</tr>

				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>

				<tr>
					<td width="220px"><label><b><spring:message	code='label.feedback.emailaddress' /></b></label>
					</td>
					<td width="120px">&nbsp;</td>
					<td width="220px"><label><b><spring:message	code='label.feedback.phonenumber' /></b> </label>
				</tr>

				<tr>
					<td width="220px" valign="top"> 
					<input type="text" name="emailAddress" id="emailAddress" class="email inp-formOne" value="<%=emailAddress%>" <%=readOnly%> <%=onKeyDown%> />
					</td>
					<td width="120px" valign="top">&nbsp;</td>
					<td width="220px" valign="top">
					<input type="text" name="phoneNumber" id="phoneNumber"	class="inp-formOne" value="<%=phoneNumber%>" /></td>
				</tr>

				<tr>
					<td colspan="3">&nbsp;</td>

				</tr>

				<tr>
					<td colspan="2"><label><b><spring:message code='label.feedback.topic' /> <font color="red">*</font></b></label> </td>
					<td>
					</td>
				</tr>

				<tr>
					<td colspan="2" style="height:58px;" valign="top">
						<select class="styledselect_form_1" name="feedbackTopic" id="feedbackTopic">
							<%if(title.equals("report-a-problem")) {%>
							<option value="">--	<spring:message code='select.feedback.please.select.topic' /> --</option>
							<%
								for (String[] topic : reportProblemTopicList) {
							%>
							<option value="<%=topic[0]%>"><%=topic[0]%></option>
							<%
								} }
							%>
							
							<%if(title.equals("share-your-feedback")) {%>
							<option value="">--	<spring:message code='select.feedback.please.select.topic' /> --</option>
							<%
								for (String[] topic : lstTopics) {
							%>
							<option value="<%=topic[0]%>"><%=topic[0]%></option>
							<%
								}}
							%>
							
					</select>
						<div id="feedbackTopic_errors"
							style="display: none; color: red; margin-left: 5px">
							<spring:message code='validation.feedback.msg.topic' />
						</div></td>
					<td valign="top" width="220px">
					<div align="right">
						<span align="left">
						<input style = "border-radius:0px;" type="submit" id="submitId" class="submitBtn" value="<spring:message code='button.feedback.submit'/>" />
						</span>
						
						<span align="right">
						<% 
						String currentTheme = themeDisplay.getThemeSetting("active-theme");
						 StringBuffer homeURL = new StringBuffer();
						if(currentTheme != null && currentTheme.equals("cip-theme")){
							homeURL.append("/").append(themeDisplay.getLanguageId()).append("/group").append(themeDisplay.getScopeGroup().getFriendlyURL()).append("/home");
						} else{
							homeURL.append("/").append(themeDisplay.getLanguageId()).append("/group").append(themeDisplay.getScopeGroup().getFriendlyURL()).append("/home");
							
						}
						%>
						<input class="submitBtn" style="border-radius:0px;" type="button" value="<spring:message code='button.feedback.cancel'/>" onClick="javascript:location.href = '<%=homeURL.toString() %>'" />
						</span>
					</div>
					</td>
				</tr>

				<tr>
					<td colspan="3">&nbsp;</td>

				</tr>

				<tr>
					<td colspan="3"><label><b><font color="red">*</font>
								<spring:message code='label.feedback.requiredfields' /></b></label></td>

				</tr>

			</table>
		</div>
	</div>
</form>

