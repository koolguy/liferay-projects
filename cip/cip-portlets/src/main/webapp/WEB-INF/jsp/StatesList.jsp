<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui" %>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="com.coach.cip.common.dto.StatesVO"%>
<%@ page import="com.coach.cip.model.entity.Region"%>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="org.apache.velocity.runtime.directive.Foreach"%>


<liferay-theme:defineObjects />
<portlet:defineObjects />

<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery-ui-1.8.19.custom.min.js"></script>

	<select name="stateId" id="target3" class="styledselect_form_1">
		<c:choose>
			<c:when test="${statesList.size() eq 0}">
				<c:set var="message">
					<spring:message
						code='label.addeditpersonalmessageadmin.message.nostatesfound' />
				</c:set>
				<option value="0"><spring:message
						code='label.addeditpersonalmessageadmin.select.nostatesfound' />
				</option>
			</c:when>
			<c:otherwise>
				<c:set var="message">
					<spring:message code='label.error.requiredfeild.blank' />
				</c:set>
				<option value="0"><spring:message
						code='label.addeditpersonalmessageadmin.select.state' />
				</option>
			</c:otherwise>
		</c:choose>
		<c:forEach var="region" items="${statesList}">

			<option value="${region.getRegionId()}">${region.getName()}</option>

		</c:forEach>
	</select>
	<div id="state_errors" class="error_message" style="display: none">${message}</div>
