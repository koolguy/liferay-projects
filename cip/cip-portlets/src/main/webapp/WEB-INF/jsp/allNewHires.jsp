<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@ page import="java.util.List"%>
<%@ page import="com.coach.cip.common.dto.NewHiresVO"%>
<%@ page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<portlet:actionURL var="allNewHiresUrl">
	<portlet:param name="myaction" value="allNewHiresPage"></portlet:param>
</portlet:actionURL>

<head>
<style type="text/css">
tr.results-header th.only {
	display: none;
}
</style>
</head>
<%
	PortletURL changePageURL = renderResponse.createRenderURL();
	changePageURL.setParameter("myaction", "allNewHiresPage");
%>
<c:set var="allNewHiresList" value='${allNewHiresList}'></c:set>

<%
	List<NewHiresVO> tempResults = (List<NewHiresVO>) pageContext.getAttribute("allNewHiresList");
%>

<fmt:bundle basename="content/Language">



	<form:form name="allNewHiresForm" method="post" commandName="newHires"
		id="allNewHiresFormId" action="${allNewHiresUrl}">

		<c:set var="allNewHiresList" value='${allNewHiresList}'></c:set>
		<div id="form_container_newHires">
			<header class="portlet-topper" style="background-color: #EB3528;">
			<h1 class="portlet-title">
				<fmt:message key='label.new.hires.header' />
			</h1>
			</header>
			<table class="tableEmployee">

				<tr>
					<td>
						<div>
							<table style="width: 96%; margin: 16px;">

								<tr>
									<td><liferay-ui:search-container delta="5"
											iteratorURL="<%=changePageURL %>">

											<%
												int counter = 0;
											%>

											<liferay-ui:search-container-results>
												<%
													if (tempResults != null) {
																		results = ListUtil.subList(tempResults,
																				searchContainer.getStart(),
																				searchContainer.getEnd());
																		total = tempResults.size();
																		pageContext.setAttribute("results", results);
																		pageContext.setAttribute("total", total);
																	}
												%>
											</liferay-ui:search-container-results>

											<liferay-ui:search-container-row
												className="com.coach.cip.common.dto.NewHiresVO"
												modelVar="newHires" keyProperty="employeeId">

												<liferay-ui:search-container-column-jsp
													path="/WEB-INF/jsp/newHiresItem.jsp" />


											</liferay-ui:search-container-row>
											<liferay-ui:search-iterator
												searchContainer="<%= searchContainer %>"
												paginate="<%= true %>" />
										</liferay-ui:search-container>
									</td>
								</tr>


							</table>
						</div></td>
				</tr>

				<tr>
					<td style="padding: 10px 0px 10px 10px;"><a
						href="/group/coach/employeedirectory"> <span
							style="background-color: black; color: white; padding: 7px 20px 7px 20px; -webkit-border-radius: 4px; margin-left: 4px; border: none; font-family: Arial; font-size: 13px; width: 120px; cursor: default;">
								BACK </span> </a>
					</td>
				</tr>


			</table>
		</div>
	</form:form>
</fmt:bundle>
