<%@ page contentType="text/html; charset=UTF-8"%>
<%@page import="org.apache.velocity.runtime.directive.Foreach"%>
<%@ page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page import="java.util.Set"%>
<%@ page import="com.liferay.portal.model.User"%>
<%@ page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import="java.util.ArrayList"%>
<%@page import="com.liferay.portal.kernel.dao.orm.QueryUtil"%>
<liferay-theme:defineObjects />
<portlet:defineObjects />


	<select multiple="multiple" class="notify_users" id="users"
		name="users" style="width: 330px; margin-left: -4px;">
		<%
			Set<User> lstUsers = new HashSet<User>();

			if (request.getAttribute("userList") != null) {
				lstUsers = (Set<User>) request.getAttribute("userList");				

				for (User usr : lstUsers) {
					
					if (usr.getFirstName() != null
							&& !usr.getFirstName().isEmpty()) {
		%>

		<option value="<%=usr.getUserId()%>"><%=usr.getFirstName()%></option>

		<%
			}
				}
			}
		%>



	</select>

