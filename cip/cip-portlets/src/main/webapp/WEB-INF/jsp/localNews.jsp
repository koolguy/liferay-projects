<%@page import="java.util.Collections"%>
<%@page import="java.util.Collection"%>
<%@page import="com.liferay.portlet.journal.service.JournalStructureLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.PropsUtil"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="com.liferay.portlet.journal.model.JournalArticle"%>
<%@ page import="java.util.List"%>
<%@ page import="java.text.DateFormat" %>
<%@ page import="com.coach.cip.util.WebUtil"%>
<%@ page import="com.coach.cip.util.Constants" %>
<%@ page import="com.liferay.portal.kernel.xml.SAXReaderUtil" %>
<%@ page import="com.liferay.portal.kernel.xml.Document" %>
<%@ page import="com.liferay.portal.kernel.util.StringUtil" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>


<portlet:defineObjects />
<liferay-theme:defineObjects />

<div id="localNewsDiv">
		
<%
		ResourceBundle resourceBundle = ResourceBundle.getBundle("content/Language", themeDisplay.getLocale());
		String portletTitle  = null;		
		String currVocabName = Constants.LOCAL_NEWS_ARTICLE_TYPE;
		String currOrgName = PropsUtil.get("coach.cip.organization.name");
		if(Validator.isNotNull(currOrgName) && themeDisplay.getScopeGroupName().equalsIgnoreCase(PropsUtil.get("coach.cip.organization.name"))){
			portletTitle = resourceBundle.getString("title.local.news");
		}else{
			portletTitle = resourceBundle.getString("title.dept.news");
			currVocabName = Constants.DEPARTMENT_NEWS_ARTICLE_TYPE;
		}
		renderResponse.setTitle(portletTitle);
		
		long groupId = themeDisplay.getScopeGroupId();
		String redirect = renderResponse.createRenderURL().toString();
		String articleId = null;
		long id = 0l;
		String displayDate = null;
		String docUrl = null;
		String abstractTitle = null;
		JournalArticle journalArticle = null;
		String structName = null;
		int count = 0;
		List<JournalArticle> journalArticlesListByType = WebUtil.getArticlesLatestByCategory(renderRequest, renderResponse, currVocabName);
		
		
		if(Validator.isNotNull(journalArticlesListByType) && journalArticlesListByType.size() > 0)
		{
			Collections.reverse(journalArticlesListByType);
	%>		
<div class="articleDiv">	
	<div class="journal-content-article"> 
			
	<%         
	String localNewsDisplayNo = PropsUtil.get("news.local.display.number");
	int localNewsSize = 2;
	if(Validator.isNotNull(localNewsDisplayNo)){
		localNewsSize = Integer.parseInt(localNewsDisplayNo);
	}
	for(int i=0;  i < journalArticlesListByType.size(); i++)
			{
				boolean renderArticle = false;
				if(count==2)
					break;
				journalArticle=	journalArticlesListByType.get(i);
				try{
					structName = JournalStructureLocalServiceUtil.getStructure(themeDisplay.getScopeGroupId(),journalArticle.getStructureId()).getNameCurrentValue();
					if(structName!= null && structName.equalsIgnoreCase(PropsUtil.get("portlet.executive-vision-local.structure.name"))){
						renderArticle = true;	
					}
				}	
				catch(Exception e){
					e.getMessage();
				}	
					
				if(renderArticle){	
						count++;
						id = journalArticle.getId();
						Document document = SAXReaderUtil.read(journalArticle.getContent());
						docUrl = document.selectSingleNode(Constants.DYNAMIC_ELEMENT_NAME + Constants.DOC_URL + Constants.DYNAMIC_CONTENT).getText();
						//contentUrl = document.selectSingleNode(Constants.DYNAMIC_ELEMENT_NAME + Constants.CONTENT_URL + Constants.DYNAMIC_CONTENT).getText();
						//displayDate = (DateFormat.getDateInstance(DateFormat.LONG).format(journalArticle.getDisplayDate())).toString();
						SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy",locale);
						displayDate = sdf.format(journalArticle.getDisplayDate());
						//abstractTitle = journalArticle.getDescriptionCurrentValue();
						abstractTitle = journalArticle.getTitle(locale);
						abstractTitle = StringUtil.shorten(abstractTitle, 40);
%>
	

<div style="margin-bottom:5px">
	<div>
		<p> 
			<span class="displayDate"><%=displayDate.toUpperCase()%></span>
		</p> 
	</div>
	<div>
		<p class="abstractTitle"> <%=abstractTitle.toUpperCase()%> </p> 
	</div>
	<div>

		<p> 
			<strong>
				<a href="<portlet:actionURL>
										<portlet:param name="myaction" value="showArticle" />
										<portlet:param name="articleId" value="<%=String.valueOf(id)%>" />
										<portlet:param name="groupId" value="<%=String.valueOf(groupId)%>" />
										<portlet:param name="redirect" value="<%=redirect%>" />
								 </portlet:actionURL>" >
								 <span class="learnMoreBtn" >
								 <%if(themeDisplay.getScopeGroupName().equalsIgnoreCase(PropsUtil.get("coach.cip.organization.name"))){
									 %>
									  <spring:message code='hyperlink-learn-more'/>
									 <%}else{%>
									 <spring:message code='hyperlink-more'/>
								 <%} %>
								  <img src="<%=themeDisplay.getPathThemeCss()%>/../images/right_arrow_small.png"  alt=">>" ></span></a> 
			</strong>
		</p>
	</div>
	
</div>

<%	
				}
		}	
		if(count>0){
%>
</div>
<div>
			<p
				style="float: left; color: #fff; position: absolute; top: 18px; left: 194px">
				
			 <%if(themeDisplay.getScopeGroupName().equalsIgnoreCase(PropsUtil.get("coach.cip.organization.name"))){
									 %>
									 <a style="color: white; cursor: pointer;"
					href="<%= WebUtil.getPageUrl(renderRequest, Constants.LOCAL_NEWS_URL)%>">
									 	<spring:message code='button-all-local-news' /> &raquo; </a>
									 <%}else{%>
									 <a style="color: white; cursor: pointer;padding-left: 45px;"
					href="<%= WebUtil.getPageUrl(renderRequest, Constants.DEPARTMENT_NEWS_URL)%>">
										<spring:message code='button-all-dept-news' /> &raquo; </a>
								 <%} %>
				
			</p>
		</div>

</div>

<%
		}	
		else{%>
			<div class="validationMsg">
			<b><spring:message code='validation.message'/></b>
			</div>
			<br>
		
<%		
		}
	}	
		else
		{	
%>
			<div class="validationMsg">
			<b><spring:message code='validation.message'/></b>
			</div>
			<br>
<%
		}
%>

</div>

<style type="text/css">

#localNewsDiv{
	height:155px;
}

#localNewsDiv .articleDiv{
	margin-left:5px;
	margin-right:5px;
	height:149px;
	overflow:auto;
}

#localNewsDiv .displayDate{
	color: #EB3528;
	font-weight: bold;
}

#localNewsDiv .abstractTitle{
	font-family: Verdana;
	font-size: 12px;
}

#localNewsDiv a{
	text-decoration: none;
	color:black;
}

#localNewsDiv .learnMoreBtn{
	font-family: Verdana;
	font-size: 12px;
	font-weight:bold;
	margin-right:4px;
}

#localNewsDiv .validationMsg{
	margin-left:10px;
	margin-right:10px;
}

</style>