<%@page import="com.liferay.portlet.asset.service.AssetEntryServiceUtil"%>
<%@page import="com.liferay.portal.service.ClassNameLocalServiceUtil"%>
<%@page import="com.liferay.portlet.asset.service.persistence.AssetEntryQuery"%>
<%@page import="com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.dao.orm.DynamicQuery"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.liferay.portal.kernel.util.PropsUtil"%>
<%@page import="com.liferay.portal.security.permission.ActionKeys"%>
<%@page import="com.liferay.portal.security.permission.PermissionCheckerFactoryUtil"%>
<%@page import="com.liferay.portal.security.permission.PermissionChecker"%>
<%@page import="com.liferay.portal.kernel.xml.SAXReaderUtil"%>
<%@page import="com.liferay.portlet.journal.service.JournalTemplateLocalServiceUtil"%>
<%@page import="com.liferay.portlet.journal.model.JournalTemplate"%>
<%@page import="com.liferay.portlet.asset.model.AssetEntry"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="javax.portlet.PortletRequest"%>
<%@page import="com.liferay.portlet.PortletURLFactoryUtil"%>
<%@page import="javax.portlet.RenderRequest"%>
<%@page import="com.liferay.portal.kernel.exception.SystemException"%>
<%@page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@page import="com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="com.coach.cip.util.WebServiceUtil"%>
<%@page import="com.liferay.portal.model.Layout"%>
<%@page import="com.liferay.portlet.asset.model.AssetVocabulary"%>
<%@page import="com.liferay.portal.service.LayoutLocalServiceUtil"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFileEntryModel"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticleModel"%>
<%@page import="com.liferay.portlet.documentlibrary.util.DLUtil"%>
<%@page import="com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil"%>
<%@page import="com.liferay.portlet.documentlibrary.model.DLFileEntry"%>
<%@page import="com.liferay.portlet.asset.service.AssetEntryLocalServiceUtil"%>
<%@page import="com.liferay.portlet.asset.model.AssetCategory"%>
<%@page import="com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.coach.cip.util.Constants"%>
<%@ page import="com.liferay.portal.kernel.util.StringUtil"%>
<%@ page import="com.coach.cip.util.WebUtil"%>
<%@ page import="com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil"%>
<%@ page import="com.liferay.portlet.journal.service.persistence.JournalArticleFinderUtil"%>
<%@ page import="com.liferay.portlet.journalcontent.util.JournalContentUtil"%>
<%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import="com.liferay.portlet.journal.model.JournalArticle"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.*" %>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="javax.portlet.PortletURL"%>
<%@ page import="com.liferay.portlet.PortletURLUtil"%>
<%@ page import="com.liferay.portal.util.PortalUtil"%>
<%@ page import="com.liferay.portal.kernel.util.HttpUtil"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.liferay.portal.kernel.util.OrderByComparator" %>
<%@ page import="com.liferay.portlet.journal.util.comparator.ArticleModifiedDateComparator" %>
<%@ page import="com.liferay.portlet.journal.util.comparator.ArticleDisplayDateComparator" %>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<link rel="stylesheet"
	href="<%=themeDisplay.getPathThemeCss()%>/allCoachNews.css"
	type="text/css" media="screen" title="default" />
<script src="<%=themeDisplay.getPathThemeCss()%>/../js/slider.js"
	type="text/javascript"></script>

<div style="background: #000; height: 30px; width: 100%;">
	<span style="float: left; color: #fff; font-size: 18px;padding: 2px 2px 2px 5px;">
	<spring:message code='label.ca.links.title'/>
	</span> 
	<span style="float: right; color: #fff; font-size: 12px; padding-top: 14px;padding-right: 5px;">
	<a style="color: #fff;"	href="<portlet:actionURL><portlet:param name="myaction" value="detailAllWebContent" /></portlet:actionURL>">
			<span class="learnMoreBtn">
			<spring:message	code='button.all.ca.news'/> &raquo;</span>
	</a>
	</span>
</div>
<%!private String getAssetEntryPage(AssetCategory childAssetCategory, Map<String,Layout> layoutMap, List<String> categoryNamesList, RenderRequest renderRequest) {
	  String categoryKey = "";
	  categoryNamesList.add(childAssetCategory.getTitle(renderRequest.getLocale()));
	  String tempCategoryKey = getCategory(childAssetCategory);
	  if(layoutMap.containsKey(tempCategoryKey)){
		  categoryKey = tempCategoryKey;
		  renderRequest.setAttribute("categoryNamesList",categoryNamesList);
		  return categoryKey;
	  }else{
		  try{	
			  if(childAssetCategory.getParentCategoryId() != 0){
		  childAssetCategory = AssetCategoryLocalServiceUtil.getCategory(childAssetCategory.getParentCategoryId());
		  categoryKey = getAssetEntryPage(childAssetCategory,layoutMap, categoryNamesList,renderRequest);
			  }
		  }catch(Exception e){
				e.getMessage();
				
			}
		  return categoryKey;
	  }
	  
  }%>
  
  <!-- /**
	 * Helper method to form the "child-parent" category to match with the "child-parent" page. 
	 * 
	 * @param AssetCategory : Child Category. 
	 * @return String :  Returns "child-parent" name.   
	 */
	 --> 
  <%!private String getCategory(AssetCategory childAssetCategory) {
	  AssetCategory parentAssetCategory = null;
	  AssetVocabulary assetVocabulary = null;
	  StringBuilder categorySB = new StringBuilder();
	  if(childAssetCategory.getParentCategoryId() != 0){
		try{	parentAssetCategory = AssetCategoryLocalServiceUtil.getCategory(childAssetCategory.getParentCategoryId());
		    categorySB.append(childAssetCategory.getName().trim());
			categorySB.append("-");
			categorySB.append(parentAssetCategory.getName().trim());
		}catch(Exception e){
			e.getMessage();
		}
		}else{
			try{
		//	assetVocabulary = AssetVocabularyLocalServiceUtil.getAssetVocabulary(childAssetCategory.getVocabularyId());
			categorySB.append(childAssetCategory.getName().trim());
			/* categorySB.append("-");
			categorySB.append(assetVocabulary.getName().trim()); */
			}catch(Exception e){
				e.getMessage();
			}
		}

	    return categorySB.toString().toUpperCase();
	  
  }%>
  <%!private String cropTitle(String assetEntryTitle) {

		String title = "";
		if(assetEntryTitle.contains("."))
		{
			title = assetEntryTitle.split("\\.")[0];
			 
		}else{
			title = assetEntryTitle;
		}
		return title.toUpperCase();
	}%>
<div id="journal-article-import" style="border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; height: 90px;">
	<%
		String redirect = PortalUtil.getCurrentURL(renderRequest);
        long companyId = themeDisplay.getCompanyId();
        long groupId = themeDisplay.getScopeGroupId();
        String fileTitle = null;
        Map<String,Layout> layoutMap = WebServiceUtil.getLayouts(themeDisplay.getScopeGroupId());
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy",locale);
        String displayDate = null;
        double contentVersion = 1.0;
        long plid1 = 0;
        PortletURL assetURL = null;
        AssetEntry assetEntry = null;
        String mimeType = null;
        String categoryFileURL = null;
        String typeOfLink = "";

        List<JournalArticle> latestarticlesAll = new ArrayList<JournalArticle>();
        List<JournalArticle> latestarticles = new ArrayList<JournalArticle>();
        List<DLFileEntry> latestDocumentsAll = new ArrayList<DLFileEntry>(); 
        List<DLFileEntry> latestDocuments = new ArrayList<DLFileEntry>();

        AssetEntryQuery assetEntryQuery = new AssetEntryQuery();
        assetEntryQuery.setGroupIds(new long[]{themeDisplay.getScopeGroupId()});
        long classNameIds[] = new long[2];
		classNameIds[0] = ClassNameLocalServiceUtil.getClassNameId("com.liferay.portlet.journal.model.JournalArticle");
		classNameIds[1] = ClassNameLocalServiceUtil.getClassNameId("com.liferay.portlet.documentlibrary.model.DLFileEntry");
		assetEntryQuery.setClassNameIds(classNameIds);
        assetEntryQuery.setOrderByCol1("modifiedDate");
        List<AssetCategory> localeCategories = WebUtil.getAllAssetCategoriesByGroupId(renderRequest);
        long catIds[] = new long[localeCategories.size()];
        for(int k=0;k<localeCategories.size();k++){
        	catIds[k] = localeCategories.get(k).getCategoryId();
        }
        assetEntryQuery.setAnyCategoryIds(catIds);
        List<AssetEntry> assetByCategories = AssetEntryServiceUtil.getEntries(assetEntryQuery);
        for(AssetEntry entry :assetByCategories){
        	if(entry.getClassName().equalsIgnoreCase("com.liferay.portlet.journal.model.JournalArticle")){
        		latestarticlesAll.add(JournalArticleLocalServiceUtil.getLatestArticle(entry.getClassPK()));
        	}
        	else{
        		latestDocumentsAll.add(DLFileEntryLocalServiceUtil.getDLFileEntry(entry.getClassPK()));
        	}
        }	
        
        for(DLFileEntry dlFile : latestDocumentsAll){
        	if(latestDocuments.size()<16){
        		List<AssetCategory> hasDocumentCategories = AssetEntryLocalServiceUtil.getEntry("com.liferay.portlet.documentlibrary.model.DLFileEntry",dlFile.getFileEntryId()).getCategories();
        		if(null !=hasDocumentCategories && hasDocumentCategories.size()>0){
        			for(int s=0;s<hasDocumentCategories.size();s++){
	        			String categoryPage = "";
	        			categoryPage = getAssetEntryPage(hasDocumentCategories.get(s), layoutMap, new ArrayList<String>(), renderRequest);
	        			if(!categoryPage.equalsIgnoreCase("")){
				        	if(permissionChecker.hasPermission(groupId,DLFileEntry.class.getName(),dlFile.getFileEntryId(),ActionKeys.VIEW)){
				        		latestDocuments.add(dlFile);
				        		break;
				        	}
	        			}
        			}	
        		}	
        	}
        }
        
        
       	for(JournalArticle article : latestarticlesAll){
       		if(latestarticles.size()<16){
       			List<AssetCategory> hasJornalCategories = AssetEntryLocalServiceUtil.getEntry("com.liferay.portlet.journal.model.JournalArticle",article.getResourcePrimKey()).getCategories();
    			if(null !=hasJornalCategories && hasJornalCategories.size()>0){
    				for(int q=0;q<hasJornalCategories.size();q++){
	    				String categoryPage = "";
	        			categoryPage = getAssetEntryPage(hasJornalCategories.get(q), layoutMap, new ArrayList<String>(), renderRequest);
	        			if(!categoryPage.equalsIgnoreCase("")){
			        		if(permissionChecker.hasPermission(groupId,JournalArticle.class.getName(),article.getResourcePrimKey(),ActionKeys.VIEW)){
			        			 if(WebServiceUtil.isTransmittal(article)){
                                    if(WebServiceUtil.hasTransmittalPermission(article, themeDisplay.getUserId())){
                                          latestarticles.add(article);
                                          break;
                                    }
                             	 }else{
                                  	  latestarticles.add(article);
                                  	  break;
                             	 }  
			        		}
	        			}
    				}	
    			}	
       		}
       	}
       
        List<Object> mixedLst = new ArrayList<Object>();
        for(JournalArticle article : latestarticles){
        	mixedLst.add((Object)article);
        }
        for(DLFileEntry document : latestDocuments){
        	mixedLst.add((Object)document);
        }
        
        Collections.sort(mixedLst,new Comparator<Object>(){
        	public int compare(Object o1, Object o2) {
        		Date d1 = null;
        		Date d2 = null;
           			if(o1 instanceof JournalArticle)
           				d1 = ((JournalArticleModel)o1).getModifiedDate();
           			else
           				d1 = ((DLFileEntryModel)o1).getModifiedDate();
           			
           			if(o2 instanceof JournalArticle )
           				d2 = ((JournalArticleModel)o2).getModifiedDate();
           			else
           				d2 = ((DLFileEntryModel)o2).getModifiedDate();
           			
           			return d2.compareTo(d1);
           				
        	}
		});
        int listSize = 10;
        if(null !=PropsUtil.get("document.content.size")){
        	listSize = Integer.parseInt(PropsUtil.get("document.content.size"));
        }
        mixedLst = ListUtil.subList(mixedLst,0,listSize);

        if (null != mixedLst && mixedLst.size() > 0) {
		int size = mixedLst.size();
		if (size < 5) {
	%>
	<div id="loopedSlider" style="width: 100%">
		<table style="width: 105%;">
			<tr>
				<td><div class="container">
						<ul class="slides">
							<li>
								<table>
									<tr>
										<%
											int i = 0;
											int j = 0;
											int counter = 0;
											for (i = 0; i < size; i++) {
												String summary = null;
												boolean isNew = false;
												String categories = "";
												String className = null;
												String content  = null;
												String languageId = LanguageUtil.getLanguageId(renderRequest);
												AssetCategory assetCategory = null;
												List<AssetCategory> assetCategories = new ArrayList<AssetCategory>();
												DLFileEntry oneDocument = null;
												JournalArticle onearticle = null;	
												Object obj = mixedLst.get(counter++);
												if(obj instanceof JournalArticle){
													onearticle = (JournalArticle)obj;
													summary = onearticle.getTitle(locale);
													if(contentVersion == onearticle.getVersion())
														isNew =true;
													className = JournalArticle.class.getName();
													displayDate = sdf.format(onearticle.getDisplayDate());
													assetCategories = AssetEntryLocalServiceUtil.getEntry(className,onearticle.getResourcePrimKey()).getCategories();
													assetEntry = AssetEntryLocalServiceUtil.getEntry(className,onearticle.getResourcePrimKey());
													categoryFileURL = onearticle.getArticleId();
													if (onearticle != null && onearticle.getTemplateId() != null && !onearticle.getTemplateId().isEmpty()) 
													{
														JournalTemplate journalTemplate =
															JournalTemplateLocalServiceUtil.getTemplate(onearticle.getGroupId(), onearticle.getTemplateId());
														if (journalTemplate != null && journalTemplate.getNameCurrentValue().equalsIgnoreCase("COACH_LINK_TEMPLATE")) 
														{
															com.liferay.portal.kernel.xml.Document xmlDocument = SAXReaderUtil.read(onearticle.getContent());
															categoryFileURL = xmlDocument.selectSingleNode(com.coach.cip.util.Constants.DYNAMIC_ELEMENT_NAME + "url" + com.coach.cip.util.Constants.DYNAMIC_CONTENT).getText().toLowerCase();
															typeOfLink = xmlDocument.selectSingleNode(com.coach.cip.util.Constants.DYNAMIC_ELEMENT_NAME + "type" + com.coach.cip.util.Constants.DYNAMIC_CONTENT).getText();
									                         
														}
													}
													
													if (onearticle != null && onearticle.getStructureId() != null && onearticle.getStructureId().equalsIgnoreCase("TRANSMITTAL"))
													{
														typeOfLink = "TRANSMITTAL";
														categoryFileURL = ""+onearticle.getId();
												    }
												}
												else{
													oneDocument = (DLFileEntry)obj;
													summary = oneDocument.getTitle();
													if(contentVersion == Double.parseDouble(oneDocument.getVersion()))
														isNew =true;
													className = DLFileEntry.class.getName();
													displayDate = sdf.format(oneDocument.getModifiedDate());
													assetCategories = AssetEntryLocalServiceUtil.getEntry(className,oneDocument.getFileEntryId()).getCategories();
													assetEntry = AssetEntryLocalServiceUtil.getEntry(className,oneDocument.getFileEntryId());
													categoryFileURL = WebServiceUtil.displayFileURL(assetEntry.getClassPK(), renderRequest, renderResponse, themeDisplay);
												}
												
												if(assetCategories !=null && assetCategories.size()>0){
													fileTitle = cropTitle(assetEntry.getTitleCurrentValue());
													mimeType = assetEntry.getMimeType().trim();
													String catepage = "";
													for(AssetCategory cat : assetCategories){
														catepage = getAssetEntryPage(cat, layoutMap, new ArrayList<String>(), renderRequest);
														if(!catepage.equalsIgnoreCase("")){
															assetCategory = cat;
															break;
														}
													}
													
													//AssetCategory rootCategory = getParentCategory(assetCategory);
													String categoryKey = "";
													List<String> categoryNamesList = new ArrayList<String>();
													
													
													categoryKey = getAssetEntryPage(assetCategory, layoutMap, categoryNamesList, renderRequest);
													categoryNamesList = (List<String>)renderRequest.getAttribute("categoryNamesList");
													
													assetURL = PortletURLFactoryUtil.create(request,"SideNavigation_WAR_cipportlet",layoutMap.get(categoryKey).getPlid(),PortletRequest.ACTION_PHASE);
													 assetURL.setParameter("myaction", "processDocumentURL");
													 assetURL.setParameter("fileURL", categoryFileURL);
													 assetURL.setParameter("fileTitle", fileTitle);
													 assetURL.setParameter("fileType", className);
													 assetURL.setParameter("classPK", String.valueOf(assetEntry.getClassPK()));
													 assetURL.setParameter("typeOfLink", typeOfLink);
													 assetURL.setParameter("mimeType", mimeType);
													 assetURL.setParameter("backButton", "true");
													 assetURL.setWindowState(WindowState.NORMAL);
													 if(Validator.isNotNull(categoryNamesList)){
														 Collections.reverse(categoryNamesList);
														 for (int k = 1; k <categoryNamesList.size() ; k++) {
															 if(k==1){
																 assetURL.setParameter("level5Parent", categoryNamesList.get(k));
															 }
															 if(k==2){
																 assetURL.setParameter("level6Parent", categoryNamesList.get(k));
															 }
															 if(k==3){
																 assetURL.setParameter("level7Parent", categoryNamesList.get(k));
															 }
															 if(k==4){
																 assetURL.setParameter("level8Parent", categoryNamesList.get(k));
															 }
														 }
													 }
												}
												String shortsummary = StringUtil.shorten(summary,40);
											%>
										<td><div class="box">
												<span
													style="color: #EB3528; font-weight: bold; cursor: default;">
													<%=displayDate.toUpperCase(locale)%><%if(isNew){ %><img src="<%=themeDisplay.getPathThemeImages()%>/new-icon-sm.gif" ><%}else{ %><img src="<%=themeDisplay.getPathThemeImages()%>/updated_icon.gif" ><%} %> </span><br>
												<div style="width: 200px; height: 30px; cursor: default; word-wrap: break-word;"><%=shortsummary.toUpperCase(locale)%> </div>
												<br>
												<P>
													<a href="<%= assetURL%>">
														<span class="learnMoreBtn"><spring:message code='hyperlink-more'/> <img src="<%=themeDisplay.getPathThemeCss()%>/../images/right_arrow_small.png"  alt=">>" ></span>
													</a>
												</P>
											</div></td>
										<%
											}
										%>
									</tr>
								</table></li>
						</ul>
					</div></td>
			</tr>
		</table>
	</div>
	<%
		} else {
	%>
	<div id="loopedSlider" style="width: 100%;">
		<table class="webcontentTable">
			<tr>
				<td style="padding-left: 2px;"><a href="#" class="previous"> <img
						src="<%=themeDisplay.getPathThemeCss()%>/../images/left_arrow.png" /> </a></td>
				<td class="NewssecondColumn">
					<div class="container" style="width: 100%;">
						<ul class="slides" style="width: 100%;">
							<%
								int i = 0;
								int j = 0;
								int nextLoop = size / 4;
								int remaining = size % 4;
								int counter = 0;
								for (i = 0; i < nextLoop; i++) {
							%>
							<li>
								<table>
									<tr>
										<%
											for (j = 0; j < 4; j++) {
												String summary = null;
												boolean isNew = false;
												String categories = "";
												String className = null;
												String content  = null;
												String languageId = LanguageUtil.getLanguageId(renderRequest);
												AssetCategory assetCategory = null;
												List<AssetCategory> assetCategories = new ArrayList<AssetCategory>();
												DLFileEntry oneDocument = null;
												JournalArticle onearticle = null;	
												Object obj = mixedLst.get(counter++);
												if(obj instanceof JournalArticle){
													onearticle = (JournalArticle)obj;
													summary = onearticle.getTitle(locale);
													if(contentVersion == onearticle.getVersion())
														isNew =true;
													className = JournalArticle.class.getName();
													displayDate = sdf.format(onearticle.getDisplayDate());
													assetCategories = AssetEntryLocalServiceUtil.getEntry(className,onearticle.getResourcePrimKey()).getCategories();
													assetEntry = AssetEntryLocalServiceUtil.getEntry(className,onearticle.getResourcePrimKey());
													categoryFileURL = onearticle.getArticleId();
													if (onearticle != null && onearticle.getTemplateId() != null && !onearticle.getTemplateId().isEmpty()) 
													{
														JournalTemplate journalTemplate =
															JournalTemplateLocalServiceUtil.getTemplate(onearticle.getGroupId(), onearticle.getTemplateId());
														if (journalTemplate != null && journalTemplate.getNameCurrentValue().equalsIgnoreCase("COACH_LINK_TEMPLATE")) 
														{

															com.liferay.portal.kernel.xml.Document xmlDocument = SAXReaderUtil.read(onearticle.getContent());
															categoryFileURL = xmlDocument.selectSingleNode(com.coach.cip.util.Constants.DYNAMIC_ELEMENT_NAME + "url" + com.coach.cip.util.Constants.DYNAMIC_CONTENT).getText().toLowerCase();
															typeOfLink = xmlDocument.selectSingleNode(com.coach.cip.util.Constants.DYNAMIC_ELEMENT_NAME + "type" + com.coach.cip.util.Constants.DYNAMIC_CONTENT).getText();
									                         
														}
													}
													if (onearticle != null && onearticle.getStructureId() != null && onearticle.getStructureId().equalsIgnoreCase("TRANSMITTAL"))
													{
														typeOfLink = "TRANSMITTAL";
														categoryFileURL = ""+onearticle.getId();
												    }
												}
												else{
													oneDocument = (DLFileEntry)obj;
													summary = oneDocument.getTitle();
													if(contentVersion == Double.parseDouble(oneDocument.getVersion()))
														isNew =true;
													className = DLFileEntry.class.getName();
													displayDate = sdf.format(oneDocument.getModifiedDate());
													assetCategories = AssetEntryLocalServiceUtil.getEntry(className,oneDocument.getFileEntryId()).getCategories();
													assetEntry = AssetEntryLocalServiceUtil.getEntry(className,oneDocument.getFileEntryId());
													categoryFileURL = WebServiceUtil.displayFileURL(assetEntry.getClassPK(), renderRequest, renderResponse, themeDisplay);
												}
												
												if(assetCategories !=null && assetCategories.size()>0){
													fileTitle = cropTitle(assetEntry.getTitleCurrentValue());
													mimeType = assetEntry.getMimeType().trim();
													categories = assetCategories.get(0).getName();
													String catepage = "";
													for(AssetCategory cat : assetCategories){
														catepage = getAssetEntryPage(cat, layoutMap, new ArrayList<String>(), renderRequest);
														if(!catepage.equalsIgnoreCase("")){
															assetCategory = cat;
															break;
														}
													}
													String categoryKey = "";
													List<String> categoryNamesList = new ArrayList<String>();
													
													categoryKey = getAssetEntryPage(assetCategory, layoutMap, categoryNamesList, renderRequest);
													categoryNamesList = (List<String>)renderRequest.getAttribute("categoryNamesList");
													
													assetURL = PortletURLFactoryUtil.create(request,"SideNavigation_WAR_cipportlet",layoutMap.get(categoryKey).getPlid(),PortletRequest.ACTION_PHASE);
													 assetURL.setParameter("myaction", "processDocumentURL");
													 assetURL.setParameter("fileURL", categoryFileURL);
													 assetURL.setParameter("fileTitle", fileTitle);
													 assetURL.setParameter("fileType", className);
													 assetURL.setParameter("classPK", String.valueOf(assetEntry.getClassPK()));
													 assetURL.setParameter("typeOfLink", typeOfLink);
													 assetURL.setParameter("mimeType", mimeType);
													 assetURL.setParameter("backButton", "true");
													 assetURL.setWindowState(WindowState.NORMAL);
													 if(Validator.isNotNull(categoryNamesList)){
														 Collections.reverse(categoryNamesList);
														 for (int k = 1; k <categoryNamesList.size() ; k++) {
															 if(k==1){
																 assetURL.setParameter("level5Parent", categoryNamesList.get(k));
															 }
															 if(k==2){
																 assetURL.setParameter("level6Parent", categoryNamesList.get(k));
															 }
															 if(k==3){
																 assetURL.setParameter("level7Parent", categoryNamesList.get(k));
															 }
															 if(k==4){
																 assetURL.setParameter("level8Parent", categoryNamesList.get(k));
															 }
														 }
													 }
												}
												//categories.substring(0,categories.lastIndexOf(","));
												String shortsummary = StringUtil.shorten(summary,40);
											%>
										<td><div class="box">
												<span style="color: #EB3528; font-weight: bold"> <%=displayDate.toUpperCase(locale)%><%if(isNew){ %><img src="<%=themeDisplay.getPathThemeImages()%>/new-icon-sm.gif" ><%}else{ %><img src="<%=themeDisplay.getPathThemeImages()%>/updated_icon.gif" ><%} %>
												</span><br>
												<div style="width: 200px; height: 30px; cursor: default; word-wrap: break-word;"><%=shortsummary.toUpperCase(locale)%> </div>
												<br>
												<P>
														<a href="<%= assetURL%>">
															<span class="learnMoreBtn"><spring:message code='hyperlink-more'/> <img src="<%=themeDisplay.getPathThemeCss()%>/../images/right_arrow_small.png"  alt=">>" ></span>
														</a>
												</P>
											</div></td>
										<%
											}
										%>
									</tr>
								</table></li>
							<%
								}
							%>
							<%
								if (remaining > 0) {
							%>
							<li>
								<table>
									<tr>
										<%
											for (j = 0; j < 4; j++) {
												String summary = null;
												boolean isNew = false;
												String categories = "";
												String className = null;
												String content  = null;
												String languageId = LanguageUtil.getLanguageId(renderRequest);
												AssetCategory assetCategory = null;
												List<AssetCategory> assetCategories = new ArrayList<AssetCategory>();
												DLFileEntry oneDocument = null;
												JournalArticle onearticle = null;	
												Object obj = mixedLst.get((counter++ % size));
												if(obj instanceof JournalArticle){
													onearticle = (JournalArticle)obj;
													summary = onearticle.getTitle(locale);
													if(contentVersion == onearticle.getVersion())
														isNew =true;
													className = JournalArticle.class.getName();
													displayDate = sdf.format(onearticle.getDisplayDate());
													assetCategories = AssetEntryLocalServiceUtil.getEntry(className,onearticle.getResourcePrimKey()).getCategories();
													assetEntry = AssetEntryLocalServiceUtil.getEntry(className,onearticle.getResourcePrimKey());
													categoryFileURL = onearticle.getArticleId();
													if (onearticle != null && onearticle.getTemplateId() != null && !onearticle.getTemplateId().isEmpty()) 
													{
														JournalTemplate journalTemplate =
															JournalTemplateLocalServiceUtil.getTemplate(onearticle.getGroupId(), onearticle.getTemplateId());
														if (journalTemplate != null && journalTemplate.getNameCurrentValue().equalsIgnoreCase("COACH_LINK_TEMPLATE")) 
														{

															com.liferay.portal.kernel.xml.Document xmlDocument = SAXReaderUtil.read(onearticle.getContent());
															categoryFileURL = xmlDocument.selectSingleNode(com.coach.cip.util.Constants.DYNAMIC_ELEMENT_NAME + "url" + com.coach.cip.util.Constants.DYNAMIC_CONTENT).getText().toLowerCase();
															typeOfLink = xmlDocument.selectSingleNode(com.coach.cip.util.Constants.DYNAMIC_ELEMENT_NAME + "type" + com.coach.cip.util.Constants.DYNAMIC_CONTENT).getText();
									                         
														}
													}
													if (onearticle != null && onearticle.getStructureId() != null && onearticle.getStructureId().equalsIgnoreCase("TRANSMITTAL"))
													{
														typeOfLink = "TRANSMITTAL";
														categoryFileURL = ""+onearticle.getId();
												    }
												}
												else{
													oneDocument = (DLFileEntry)obj;
													summary = oneDocument.getTitle();
													if(contentVersion == Double.parseDouble(oneDocument.getVersion()))
														isNew =true;
													className = DLFileEntry.class.getName();
													displayDate = sdf.format(oneDocument.getModifiedDate());
													assetCategories = AssetEntryLocalServiceUtil.getEntry(className,oneDocument.getFileEntryId()).getCategories();
													assetEntry = AssetEntryLocalServiceUtil.getEntry(className,oneDocument.getFileEntryId());
													categoryFileURL = WebServiceUtil.displayFileURL(assetEntry.getClassPK(), renderRequest, renderResponse, themeDisplay);
												}
												
												if(assetCategories !=null && assetCategories.size()>0){
													fileTitle = cropTitle(assetEntry.getTitleCurrentValue());
													mimeType = assetEntry.getMimeType().trim();
													categories = assetCategories.get(0).getName();
													String catepage = "";
													for(AssetCategory cat : assetCategories){
														catepage = getAssetEntryPage(cat, layoutMap, new ArrayList<String>(), renderRequest);
														if(!catepage.equalsIgnoreCase("")){
															assetCategory = cat;
															break;
														}
													}
													String categoryKey = "";
													List<String> categoryNamesList = new ArrayList<String>();
													
													categoryKey = getAssetEntryPage(assetCategory, layoutMap, categoryNamesList, renderRequest);
													categoryNamesList = (List<String>)renderRequest.getAttribute("categoryNamesList");
													
													assetURL = PortletURLFactoryUtil.create(request,"SideNavigation_WAR_cipportlet",layoutMap.get(categoryKey).getPlid(),PortletRequest.ACTION_PHASE);
													 assetURL.setParameter("myaction", "processDocumentURL");
													 assetURL.setParameter("fileURL", categoryFileURL);
													 assetURL.setParameter("fileTitle", fileTitle);
													 assetURL.setParameter("fileType", className);
													 assetURL.setParameter("classPK", String.valueOf(assetEntry.getClassPK()));
													 assetURL.setParameter("typeOfLink", typeOfLink);
													 assetURL.setParameter("mimeType", mimeType);
													 assetURL.setParameter("backButton", "true");
													 assetURL.setWindowState(WindowState.NORMAL);
													 if(Validator.isNotNull(categoryNamesList)){
														 Collections.reverse(categoryNamesList);
														 for (int k = 1; k <categoryNamesList.size() ; k++) {
															 if(k==1){
																 assetURL.setParameter("level5Parent", categoryNamesList.get(k));
															 }
															 if(k==2){
																 assetURL.setParameter("level6Parent", categoryNamesList.get(k));
															 }
															 if(k==3){
																 assetURL.setParameter("level7Parent", categoryNamesList.get(k));
															 }
															 if(k==4){
																 assetURL.setParameter("level8Parent", categoryNamesList.get(k));
															 }
														 }
													 }
												}
												String shortsummary = StringUtil.shorten(summary,40);
										%>
										<td>
											<div class="box">
												<span style="color: #EB3528; font-weight: bold;"> <%=displayDate.toUpperCase(locale)%><%if(isNew){ %><img src="<%=themeDisplay.getPathThemeImages()%>/new-icon-sm.gif" ><%}else{ %><img src="<%=themeDisplay.getPathThemeImages()%>/updated_icon.gif" ><%} %>
												</span><br>
												<div style="width: 200px; height: 30px; cursor: default; word-wrap: break-word;"><%=shortsummary.toUpperCase(locale)%> </div>
												<br>
												<P>
													<a href="<%= assetURL%>">
														<span class="learnMoreBtn"><spring:message code='hyperlink-more'/> <img src="<%=themeDisplay.getPathThemeCss()%>/../images/right_arrow_small.png"  alt=">>" ></span>
													</a>
												</P>
											</div></td>
										<%
											}
										%>
									</tr>
								</table></li>
							<%
								}
							%>
						</ul>
					</div></td>

				<td><a href="#" class="next" style="padding-left:30px"> <img src="<%=themeDisplay.getPathThemeCss()%>/../images/right_arrow.png" /> </a></td>
			</tr>
		</table>
	</div>
	<%
		}
	%>

	<%
		} else {
	%>
	<div style="margin-left: 10px; margin-right: 10px;">
		<b><spring:message code='validation.message' /></b>
	</div>
	<br>
	<%
		}
	%>
	<br>
	<div style="clear: both;"></div>
</div>

  
<script type="text/javascript" charset="utf-8">
	$(function() {
		$('#loopedSlider').loopedSlider({
			autoHeight : 800
		});
	});
</script>