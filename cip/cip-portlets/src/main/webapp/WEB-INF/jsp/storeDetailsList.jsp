<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page import="java.util.ArrayList"%>
<%@ page import="com.liferay.portal.service.RegionServiceUtil"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ page import="java.util.List"%>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticle"%>
<%@page import="com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil"%>
<%@page import="com.liferay.portlet.journalcontent.util.JournalContentUtil"%>
<%@page import="com.coach.cip.util.WebUtil"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.coach.cip.util.Constants"%>



<liferay-theme:defineObjects />
<portlet:defineObjects />

<!--  jquery core -->
<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery-ui-1.8.19.custom.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$("#cancelId").click(function() {
			$("#storeDetailsForm").submit();
		});
	});
</script>


<portlet:actionURL var="storeAddressDetailsURL">
	<portlet:param name="myaction" value="displayStoreAddressDetails"></portlet:param>
</portlet:actionURL>
<portlet:renderURL var="storeAddressDetailsURLForm">
	<portlet:param name="myaction" value="displayStoreAddressDetailsForm"></portlet:param>
</portlet:renderURL>

<c:set var="geographyName" value="${geographyName}" />
<c:set var="redirectUrl" value="${redirectUrl}" />
<c:set var="searchRedirectString" value="${searchRedirectString}" />
<c:set var="brand" value="${brand}" />
<c:set var="nostoreDeailfound" value="${NOSTOREDETAILFOUND}" />


<%
	String articleContent = "";
	String geographyName = "";
	String redirectUrl = "";
	String searchRedirectString = "";
	String faultMessage = "";
	String brand = "";
	if(pageContext.getAttribute("geographyName") != null){
		geographyName = (String) pageContext.getAttribute("geographyName");
	}
	if(pageContext.getAttribute("redirectUrl") != null){
		redirectUrl = (String) pageContext.getAttribute("redirectUrl");
	}
	if(pageContext.getAttribute("searchRedirectString") != null){
	 	searchRedirectString = (String) pageContext.getAttribute("searchRedirectString");
	}
	if(pageContext.getAttribute("FaultMessage") != null){
		faultMessage = (String)pageContext.getAttribute("faultMessage");
	}
	if(pageContext.getAttribute("brand") != null){
		brand = (String)pageContext.getAttribute("brand");
	}
%>

<style>
.selectedgeo {
	font-weight: bold;
	color: #E8317D;
}
#mainform {
	font-size: 11px;
	font-family: Verdana;
}
#storeDetailView{
	padding: 0px 0px 0px 30px;
}
</style>
<script type="text/javascript">
<%
String title = themeDisplay.getLayout().getName(themeDisplay.getLocale());
if(title.equalsIgnoreCase("stores")){
	title = geographyName;
}
if(title != null && !title.isEmpty()){
	title = title.toUpperCase();
}

if(geographyName != null && !geographyName.isEmpty()){
	geographyName = geographyName.toUpperCase();
}
%>

$(document).ready(function() {
	  $("#geoTable").find("tr").find("a").removeClass("selectedgeo");
				var storeName=$("span.portlet-title-text").text().split("-")[1];
				if($.trim(storeName)=="COACH"){
					$('#geoTable tr').each(function() {
						var name = $(this).text();
						if ($.trim(name) == $("#geoName").val()) {
							$(this).find("a").addClass("selectedgeo");
						}
					  });
				 }
				if($.trim(storeName)=="RK"){
					$('#rkgeoTable tr').each(function() {
					var name = $(this).text();
					if ($.trim(name) == $("#geoName").val()) {
					 $(this).find("a").addClass("selectedgeo");
					}
				});
			 } 

	});

	 $('#offTable').find('a.officesAnchor').click(function(){
	     $("#storeDetailsForm").parent().hide();
		     $("#officesId").show();

			 Liferay.on(
		           'retrieveValue',
		           function(event, data) {
		        	   popup(data);
					}
				);
			
			function popup(tempData){
				var url = '<portlet:resourceURL id="populateStoreOffices"></portlet:resourceURL>'+ '?fakeId=345&tempData='+ tempData;
				var xhr = new XMLHttpRequest();
				xhr.onreadystatechange = function() {
				if (xhr.readyState == 4 && xhr.status==200) {
					var responseText = document.getElementById("officesId");
					responseText.innerHTML = xhr.responseText;
					}
				};
				xhr.open("GET", url, true);
				xhr.send();
			}
  });


   $('#rkoffTable').find('a.officesAnchor').click(function(){
     $("#officesId").show();
			 Liferay.on(
		           'retrieveValue',
		           function(event, data) {
		        	   popup(data);
					}
				);
			
			function popup(tempData){
				var url = '<portlet:resourceURL id="populateStoreOffices"></portlet:resourceURL>'+ '?fakeId=345&tempData='+ tempData;
				var xhr = new XMLHttpRequest();
				xhr.onreadystatechange = function() {
				if (xhr.readyState == 4 && xhr.status==200) {
					var responseText = document.getElementById("officesId");
					responseText.innerHTML = xhr.responseText;
					}
				};
				xhr.open("GET", url, true);
				xhr.send();
			}
	     $("#storeDetailsForm").parent().hide();
  });
</script>


<div id="mainform">
<form id="storeDetailsForm" method="post" autocomplete="off"
	action="${storeAddressDetailsURL}">
	<input type="hidden" id="geoName" name="geographyName" value="<%=geographyName%>" />
	<input type="hidden" name="brand" value="<%=brand%>" />
	<div style="border: 1px solid black; margin-bottom: 7px;">
		<header class="portlet-storeDetail-topper">
		<h1 class="portlet-title">
		<c:if test="${brand ne null }">
			<span class="portlet-title-text"><spring:message
					code='label.title.stores' /> - <%=brand%> - <%=geographyName%></span>
		</c:if>
		<c:if test="${brand eq null }">
			<span class="portlet-title-text"><spring:message
					code='label.title.stores' /> - <%=geographyName%></span>
		</c:if>
		</h1>
		</header>
		<div style="margin-left: 10px; margin-top: 10px; color: red; font-weight: bold;">
		<c:if test="${faultMessage ne null || not empty faultMessage}">
		<spring:message code='${faultMessage}'/>
		</c:if>
		</div>
		<div style="margin-left: 10px; margin-top: 10px; color: red; font-weight: bold;">
		<c:if test="${nostoreDeailfound ne null || not empty nostoreDeailfound}">
		<spring:message code='${nostoreDeailfound}'/>
		</c:if>
		</div>
		
		<c:forEach var="storesVO" items="${storeList}">
			<table style="margin-left: 9px; font-size: 12px; font-family: Verdana;">
				<tr>
					<td style="font-weight: bold;"><c:out
							value='${storesVO.storename}' /></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td>Store # <c:out value='${storesVO.storenumber}' />
					</td>
				</tr>
				<%-- <tr>
					<td><c:out value='${storesVO.storename}' /></td>
				</tr> --%>
				<tr>
					<td><c:out value='${storesVO.storetype}' /></td>
				</tr>
				<tr>
					<td><c:out value='${storesVO.districtdescription}' /></td>
				</tr>
				<tr>
					<td><c:out value='${storesVO.address}' /></td>
				</tr>
				<tr>
					<td><c:out value='${storesVO.address2}' /></td>
				</tr>
				<tr>
					<td><c:out value='${storesVO.storeDetailHeader}' /></td>
				</tr>
				
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td><spring:message code='label.stores.phone' /> </td>
					<td id="storeDetailView"><c:out value='${storesVO.phone}'></c:out></td>
				</tr>
				<tr>
					<td><spring:message code='label.store.fax' /> </td>
					<td id="storeDetailView"><c:out value='${storesVO.fax}'></c:out>	</td>
				</tr>
				<tr>
					<td><spring:message code='label.store.extension' /> </td>
					<td id="storeDetailView"><c:out value='${storesVO.vm}'></c:out>	</td>
				</tr>
				<tr>
					<td><spring:message code='label.stores.hours' /> </td>
					<td id="storeDetailView"><c:out value='${storesVO.hours}'></c:out></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<c:if test="${storesVO.generalmanager.length() != 0}">
				<tr>
					<td><spring:message code='lable.stores.gm' /> </td>
					<td id="storeDetailView"><c:out value='${storesVO.generalmanager}'></c:out></td>
				</tr>
				</c:if>
				<c:if test="${storesVO.manager1.length() != 0}">
				<tr>
					<td><spring:message code='label.stores.sm' /> </td>
					<td id="storeDetailView"><c:out value='${storesVO.manager1}'></c:out> </td>
				</tr>
				</c:if>
				<c:if test="${storesVO.associatemgr1.length() != 0}">
				<tr>
					<td><spring:message code='label.stores.assmgr' /> </td>
					<td id="storeDetailView"><c:out value='${storesVO.associatemgr1}'></c:out></td>
				</tr>
				</c:if>
				<c:if test="${storesVO.associatemgr2.length() != 0}">
				<tr>
					<td><spring:message code='label.stores.assmgr' /> </td>
					<td id="storeDetailView"><c:out value='${storesVO.associatemgr2}'></c:out></td>
				</tr>
				</c:if>
				<c:if test="${storesVO.associatemgr3.length() != 0}">
				<tr>
					<td><spring:message code='label.stores.assmgr' /> </td>
					<td id="storeDetailView"><c:out value='${storesVO.associatemgr3}'></c:out></td>
				</tr>
				</c:if>
				<c:if test="${storesVO.associatemgr4.length() != 0}">
				<tr>
					<td><spring:message code='label.stores.assmgr' /> </td>
					<td id="storeDetailView"><c:out value='${storesVO.associatemgr4}'></c:out></td>
				</tr>
				</c:if>
				<c:if test="${storesVO.assistantmgr1.length() != 0}">
				<tr>
					<td><spring:message code='label.stores.asstmgr' /> </td>
					<td id="storeDetailView"><c:out value='${storesVO.assistantmgr1}'></c:out></td>
				</tr>
				</c:if>
				<c:if test="${storesVO.assistantmgr2.length() != 0}">
				<tr>
					<td><spring:message code='label.stores.asstmgr' /> </td>
					<td id="storeDetailView"><c:out value='${storesVO.assistantmgr2}'></c:out></td>
				</tr>
				</c:if>
				<c:if test="${storesVO.assistantmgr3.length() != 0}">
				<tr>
					<td><spring:message code='label.stores.asstmgr' /> </td>
					<td id="storeDetailView"><c:out value='${storesVO.assistantmgr3}'></c:out></td>
				</tr>
				</c:if>
				<c:if test="${storesVO.assistantmgr4.length() != 0}">
				<tr>
					<td><spring:message code='label.stores.asstmgr' /> </td>
					<td id="storeDetailView"><c:out value='${storesVO.assistantmgr4}'></c:out></td>
				</tr>
				</c:if>
				<c:if test="${storesVO.assistantmgr5.length() != 0}">
				<tr>
					<td><spring:message code='label.stores.asstmgr' /> </td>
					<td id="storeDetailView"><c:out value='${storesVO.assistantmgr5}'></c:out></td>
				</tr>
				</c:if>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<c:if test="${storesVO.areamgr.length() != 0}">
				<tr>
					<td><spring:message code='label.stores.am' /> </td>
					<td id="storeDetailView"><c:out value='${storesVO.areamgr}'></c:out>	</td>
				</tr>
				</c:if>
				<c:if test="${storesVO.districtmgr.length() != 0}">
				<tr>
					<td><spring:message code='label.stores.dm' /> </td>
					<td id="storeDetailView"><c:out value='${storesVO.districtmgr}'></c:out></td>
				</tr>
				</c:if>
				<c:if test="${storesVO.regionmgr.length() != 0}">
				<tr>
					<td><spring:message code='label.stores.rm' /> </td>
				    <td id="storeDetailView"><c:out value='${storesVO.regionmgr}'></c:out></td>
				</tr>
				</c:if>
			</table>
		</c:forEach>

		<table>
		<c:if test="${searchRedirectString ne null || not empty searchRedirectString}">
			<%-- <td>
			<a style="cursor:default;" href="${redirectUrl}">
			<input type="button"  value="BACK"				
				style="background-color: black; color: white; padding: 1px; margin-left: 9px; margin-bottom: 5px; height: 34px; width: 94px; border: none;">
			</input></a>
			</td> --%>
			
			<td>
			<input style="cursor:default;background-color: black; color: white; padding: 1px; margin-left: 9px; margin-bottom: 5px; 
			height: 34px; width: 94px; border: none;" type="button" value="BACK" onClick="javascript:location.href = '${redirectUrl}'" />
			</td>
			
		</c:if>
		<c:if test="${searchRedirectString eq null || empty searchRedirectString}">
			<td><input type="submit" id="cancelId" value="Cancel"
				name="storeDetailCancelSubmit"
				style="background-color: black; color: white; padding: 1px; margin-left: 9px; margin-bottom: 11px; margin-top:10px; height: 34px; width: 94px; border: none;">
				</input>
			</td>
		</c:if>
		</table>
	</div>
</form>
</div>

<div id="officesId" style="border: 1px solid black; margin-bottom: 10px;display:none">
					<header class="portlet-storeDetail-topper">
					<h1 class="portlet-title">
						<span class="portlet-title-text"><spring:message
								code='label.title.offices' /> - <spring:message	code='label.title.brand.coach'/> - <spring:message code='label.offices.northamerica'/></span>
								
					</h1>
					</header>
<div id="jacontent" style="padding:10px;"><%=articleContent %></div>
</div>