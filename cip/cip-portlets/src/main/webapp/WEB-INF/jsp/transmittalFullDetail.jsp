<%@page import="com.coach.cip.util.LoggerUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.coach.cip.portlets.controller.PDFPortletController"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.coach.cip.util.WebUtil"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="com.liferay.portal.kernel.xml.Element"%>
<%@page import="javax.portlet.PortletRequestDispatcher"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="com.liferay.portal.kernel.xml.Document"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.liferay.portal.kernel.xml.Node"%>
<%@page import="com.liferay.portal.kernel.xml.DocumentException"%>
<%@page import="com.coach.cip.util.Constants"%>
<%@page import="com.liferay.portal.kernel.xml.SAXReaderUtil"%>
<%@page
	import="com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil"%>
<%@page import="com.liferay.portlet.asset.model.AssetCategory"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticleDisplay"%>
<%@page
	import="com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil"%>
<%@page import="org.apache.velocity.runtime.directive.Foreach"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticle"%>
<%@page import="com.coach.ca.service.TransmittalLocalServiceUtil"%>
<%@page import="com.coach.ca.model.Transmittal"%>
<%@page import="com.liferay.portal.kernel.util.ArrayUtil"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<%
	final Log loggerJsp = LogFactoryUtil.getLog(PDFPortletController.class);
	String homeRedirect = ParamUtil.getString(request, "homeRedirect");
	String redirect = PortalUtil.getCurrentURL(renderRequest);
%>
<%
	String level5Parent = "";
	String level6Parent = "";
	String level7Parent = "";
	String level8Parent = "";
	redirect = URLDecoder.decode(redirect);
	String[] highlightSideNavigation = redirect.split("&");
	for (int i = 0; i < highlightSideNavigation.length; i++) {
		String val = highlightSideNavigation[i];
		if (val.contains("level5Parent") && val.split("level5Parent=").length == 2) {
			level5Parent = Validator.isNotNull(val.split("level5Parent=")[1]) ? val.split("level5Parent=")[1] : "";
		}
		if (val.contains("level6Parent") && val.split("level6Parent=").length == 2) {
			level6Parent = Validator.isNotNull(val.split("level6Parent=")[1]) ? val.split("level6Parent=")[1] : "";
		}
		if (val.contains("level7Parent") && val.split("level7Parent=").length == 2) {
			level7Parent = Validator.isNotNull(val.split("level7Parent=")[1]) ? val.split("level7Parent=")[1] : "";
		}
		if (val.contains("level8Parent") && val.split("level8Parent=").length == 2) {
			level8Parent = Validator.isNotNull(val.split("level8Parent=")[1]) ? val.split("level8Parent=")[1] : "";
		}
	}
%>

<portlet:actionURL var="getTransmittalAttachment">
	<portlet:param name="action" value="getTransmittalAttachment" />
	<portlet:param name="documentPath" value="${docUrl}" />
	<portlet:param name="redirect" value="<%=redirect %>" />
	<portlet:param name="title" value="<%=title %>"/>
	<portlet:param name="level5Parent" value="<%=level5Parent %>" />
	<portlet:param name="level6Parent" value="<%=level6Parent %>" />
	<portlet:param name="level7Parent" value="<%=level7Parent %>" />
	<portlet:param name="level8Parent" value="<%=level8Parent %>" />
</portlet:actionURL>
<c:set var="journalArticleId" value="${journalArticleId}" />
<%
	String imageUrl = null;
	String content = null;
	List<Node> docNodesList = null;
	List<String> docUrList = new ArrayList<String>();
	JournalArticleDisplay journalArticleDisplay = null;
	JournalArticle journalArticle = null;
	//AssetCategory articleCat = null;
	boolean onlyDocAttachment = false;
	String journalArticleId = (String) pageContext.getAttribute("journalArticleId");
	boolean templateBasedArticle = false;
	boolean hasTransmittalPermission = false;
	if (Validator.isNotNull(journalArticleId)) {
		journalArticle = JournalArticleLocalServiceUtil.getArticle(Long.valueOf(journalArticleId));
		String articleId = journalArticle.getArticleId();
		try{
		Transmittal transmittal = TransmittalLocalServiceUtil.getTransmittal(articleId);
		String[] storeUserIds = transmittal.getStoreUsers().split(",");
		String[] corpUserIds = transmittal.getCorporateUsers().split(",");
		if(ArrayUtil.contains(storeUserIds, "" + themeDisplay.getUserId()) || ArrayUtil.contains(corpUserIds, "" + themeDisplay.getUserId())){
			hasTransmittalPermission = true;
		}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	if (Validator.isNotNull(journalArticle) && hasTransmittalPermission) {
		/*List<AssetCategory> catList =
			AssetCategoryLocalServiceUtil.getCategories(JournalArticle.class.getName(), journalArticle.getResourcePrimKey());
		if (catList != null && catList.size() != 0)
			articleCat = catList.get(0);*/
		if (Validator.isNotNull(journalArticle.getTemplateId())) {
			try {
				Document document = SAXReaderUtil.read(journalArticle.getContent());
				if (document != null) {
					/*String xpathExpression = "";
					xpathExpression = Constants.DYNAMIC_ELEMENT_NAME + Constants.NEWS_IMAGE_URL + Constants.DYNAMIC_CONTENT;
					if (document.selectSingleNode(xpathExpression) != null)
						imageUrl = document.selectSingleNode(xpathExpression).getText();
					*/
					//multi lingual start
					List<Node> contentNodeList = null;
					contentNodeList = (document.selectNodes(Constants.DYNAMIC_ELEMENT_NAME + "content" + "']"));
					for (Node contentNode : contentNodeList) {
						if (contentNode.selectNodes(Constants.NEWS_DC) != null) {
							List<Node> innerContentNodeList = contentNode.selectNodes(Constants.NEWS_DC);
							for (Node innerContent : innerContentNodeList) {
								String xmlStr = innerContent.asXML();
								Pattern pattern = Pattern.compile("language-id=\"(.*?)\"");
								Matcher matcher = pattern.matcher(xmlStr);
								if (matcher.find()) {
									String localString = matcher.group(1);
									if (locale.toString().equalsIgnoreCase(localString)) {
										content = innerContent.getText();
										break;
									}
								}
								//no multilingual content 
								else {
									content = innerContent.getText();;
									break;
								}
							}
						}
					}
					//end
					docNodesList = (document.selectNodes(Constants.DYNAMIC_ELEMENT_NAME + Constants.NEWS_DOCS + "']"));
					if (Validator.isNull(imageUrl) && Validator.isNull(content)) {
						onlyDocAttachment = true;
					}
					for (Node doc : docNodesList) {
						if (doc.selectNodes(Constants.NEWS_DC) != null) {
							List<Node> innerDocNodeList = doc.selectNodes(Constants.NEWS_DC);
							for (Node innerDoc : innerDocNodeList) {
								String xmlStr = innerDoc.asXML();
								Pattern pattern = Pattern.compile("language-id=\"(.*?)\"");
								Matcher matcher = pattern.matcher(xmlStr);
								if (matcher.find()) {
									String localString = matcher.group(1);
									if (locale.toString().equalsIgnoreCase(localString)) {
										docUrList.add(innerDoc.getText());
										break;
									}
								}
								//no multilingual doc urls
								else {
									if (Validator.isNotNull(doc.selectSingleNode(Constants.NEWS_DC).getText())) {
										docUrList.add(doc.selectSingleNode(Constants.NEWS_DC).getText());
									}
									break;
								}
							}
						}
					}
				}
				templateBasedArticle = true;
				
			}
			catch (DocumentException e) {
				LoggerUtil.errorLogger(loggerJsp, "Exception occured in transmittalFullDetails.jsp ", e);
			}
		}
		else {
			try {
				journalArticleDisplay =
					JournalArticleLocalServiceUtil.getArticleDisplay(
						journalArticle, journalArticle.getTemplateId(), null, themeDisplay.getLanguageId(), 1, null, themeDisplay);
				templateBasedArticle = false;
			}
			catch (Exception e) {
			}
		}
	String portletTitle = PortalUtil.getPortletTitle(renderResponse);
%>
<%--
<div>
	<p>
		<span style="color: #EB3528"><c:out
				value="<%= displayDateStr.toUpperCase(locale)+\" | \"+articleCat.getTitle(locale).toUpperCase()  %>"></c:out>
		</span>
	</p>
</div>
 --%>
<%
	if (templateBasedArticle) {
%>
<div style="min-height: 175px;">
	<table>
		<tr>

			<td><span
				style="font-family: 'myriad-web-pro'; font-size: 12px;"> <%
 	if (content != null && content.length() != 0)
 			out.println(content);
 %> </span></td>
		</tr>
	</table>
</div>
<div>
<% int counter = 0; %>
	<c:forEach var="docUrl" items="<%=docUrList %>">
		<c:set var="docUrl" value="${docUrl}"></c:set>
		<%
		 	counter++;
			String docUrl = (String) pageContext.getAttribute("docUrl");
			//String docName = "Attachment " + ++counter;//journalArticle.getTitle(locale);
		// disable this for performance issue
			/*try{
			DLFileEntry dlFileEntry = DLFileEntryLocalServiceUtil.getDLFileEntryByUuidAndGroupId(docUrl.substring(docUrl.lastIndexOf("/")+1), themeDisplay.getScopeGroupId());
			docName = dlFileEntry.getTitle();
		}catch(Exception e){
			LoggerUtil.errorLogger(logger, "Fetching File Entry Exception===========>",e);
		}*/
		%>
		<p>
			<span
				style="font-family: 'myriad-web-pro'; font-size: 12px; font-weight: bold">
				<a
				href="<portlet:actionURL>
									<portlet:param name="action" value="getTransmittalAttachment" />
									 <portlet:param name="documentPath" value="${docUrl}" />
									 <portlet:param name="redirect" value="<%=redirect %>"/> 
									 <portlet:param name="portletTitle" value="<%=title %>"/>
									 <portlet:param name="journalArticleId" value="<%=journalArticleId %>"/>
									 <portlet:param name="level5Parent" value="<%=level5Parent %>" />
									 <portlet:param name="level6Parent" value="<%=level6Parent %>" />
									 <portlet:param name="level7Parent" value="<%=level7Parent %>" />
									 <portlet:param name="level8Parent" value="<%=level8Parent %>" />
									 </portlet:actionURL>">
					<spring:message code='label.attachment' /> <%=" "+counter %></a> </span>
		</p>
	</c:forEach>
</div>
<br></br>
<%
	}
	else {
%>
<c:set var="journalArticleDisplay" value="<%=journalArticleDisplay%>"></c:set>
<c:if test="${journalArticleDisplay ne null}">
	<table>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td style="padding: 1px;"><%=journalArticleDisplay.getContent()%></td>
		</tr>

		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>
</c:if>
<%
		}
	}else{
%>

<div>
	 <h3><spring:message code='transmittal.you.dont.have.permission' /></h3>
</div>

<%
}
%>