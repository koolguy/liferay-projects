<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletURL"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.security.auth.AuthTokenUtil"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.liferay.portal.kernel.util.PropsUtil"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="aui" uri="http://alloy.liferay.com/tld/aui"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="java.util.List"%>
<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@ page import="org.springframework.ui.Model"%>
<%@ page import="java.util.ResourceBundle"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery-ui-1.8.19.custom.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		
		
					var dialogOpts = {
						title : $('#qlAddTitle').val(),
						bgiframe : true,
						autoOpen : false,
						modal : true,
						width : "auto"
					};

					var editdialogOpts = {
						title : $('#qlEditTitle').val(),
						bgiframe : true,
						autoOpen : false,
						modal : true,
						width : "auto",
						height : "auto"
					};

					$('#addQuickLinks').dialog(dialogOpts);

					$('#editQuickLinks').dialog(editdialogOpts);

					$('.addQuickLinks').live('click',function(event) {
						$("#pdfid").parent().hide(); 
						var showAddForm = true;
						var selectedQLAssetURL = $.trim($('#selectedAssetURLId').val());
						if(null == selectedQLAssetURL || selectedQLAssetURL =="")
						{
							selectedQLAssetURL = $('#qlCurrentPageURL').val();
						}	
						$(".quick-links").find("ul").find("li").find('a[href*="/group/coach/"]').each(function() {
							 var existingQLAssetURL = $.trim($(this).attr("href"));
							 var htmlButton = '<br/><div class="element" align="right">	<input type="button" id="popupCancel" value="<spring:message code='label.close'/>" class="submitClass" /><input type="hidden" id="selectedAssetURLId" value='+selectedQLAssetURL+' />';
							 /* For all Assets in the landing pages : Start */
							 if((selectedQLAssetURL.indexOf("p_auth=") >= 0 && existingQLAssetURL.indexOf("p_auth=") >=0)
									|| (selectedQLAssetURL.indexOf("_fileURL=") >= 0 && existingQLAssetURL.indexOf("_fileURL=") >=0))
							 {
								 var  selectedFileURL = $.trim(selectedQLAssetURL.split("_classPK=")[1].split('&')[0]);
								 var  existingFileURL = $.trim(existingQLAssetURL.split("_classPK=")[1].split('&')[0]);
								 if(selectedFileURL == existingFileURL)
								 {
									 showAddForm = false;
									 $('#addQuickLinks').dialog('open');
									return $('#addQuickLinksForm').text($('#qlAddDuplicate').val()+'"'+$(this).text()+'".') +$('#addQuickLinksForm').append(htmlButton);
									 
								 }
							 } 
							 /* For all Assets in the landing pages : End */
							 
							 /* For landing pages : Start */
							 if(selectedQLAssetURL.indexOf("_fileURL=") == -1 && existingQLAssetURL.indexOf("_fileURL=") == -1)
							 {
								 if(selectedQLAssetURL.indexOf("?") >= 0)
								 {
									 selectedQLAssetURL = selectedQLAssetURL.split("?")[0];
								 }	 

								 if(selectedQLAssetURL.indexOf(existingQLAssetURL)>=0 || selectedQLAssetURL == existingQLAssetURL)
								 {
									  showAddForm = false;
									  $('#addQuickLinks').dialog('open');
									   return $('#addQuickLinksForm').text($('#qlAddDuplicate').val()+'"'+$(this).text()+'".') +$('#addQuickLinksForm').append(htmlButton);
								 }
								 
							 }
							 /* For landing pages : End */
						});
						
						if(showAddForm)
						{
							$.ajax({
								  url: '<portlet:resourceURL id="populateAddQuickLinkForm"></portlet:resourceURL>',
					              type: "POST",
					              dataType: "html",
				                  success: function(data, textStatus, xhr) {
				                       $("#addQuickLinks-Wrapper").html( $.trim(data) );
				                       if(selectedQLAssetURL != null && selectedQLAssetURL.length > 0){
											$('#addQuickLinksForm').append("<input id='selectedAssetURLId' name='selectedAssetURL' type='hidden' value='"+selectedQLAssetURL+"'>");
										}
				                       var pageName = $.trim($('div[id*="p_p_id_PDFPortlet_WAR_cipportlet"]').find('h1').text());
				                       if(pageName != null && pageName != "")
				                       {
				                       		$('#addQLPageName').val(pageName);
				                       }
				                       $('#addQuickLinks').dialog('open');
				                   },
				                   error: function(xhr, textStatus, errorThrown) {
				                	   $("#addQuickLinksForm").html("<h5>Error while loading the Add Quick Link.</h5>");
				                	   $('#addQuickLinks').dialog('open');
				                   }
					            });
						
						}
						
					});
					
					 $(document).keyup(function(e) {
			             if (e.keyCode == 27) {  $("#pdfid").parent().show();  }   /* esc */
			          });

					$('.editQuickLinks').click(function() {
						 /* populateEditQL();  */
						 <%
							LiferayPortletURL editQLFormURL = (LiferayPortletURL)renderResponse.createResourceURL();
						    editQLFormURL.setCopyCurrentRenderParameters(false);
						    editQLFormURL.setResourceID("populateEditQuickLinkForm");
						%>
						var url = "<%=editQLFormURL%>";
						$.ajax({
							  url: url,
				              type: "POST",
				              dataType: "html",
			                  success: function(data, textStatus, xhr) {
			                       $("#editQuickLinks-Wrapper").html( $.trim(data) );
			                       $('#editQuickLinks').dialog('open');
			                   },
			                   error: function(xhr, textStatus, errorThrown) {
			                	   $("#editQuickLinks").html("<h5>Error while loading the Form.</h5>");
			                	   $('#editQuickLinks').dialog('open');
			                   }
				            });
						
						 $("#pdfid").parent().hide();
					});


					if ($("#editOpenPopup").val() == "true") {
						$("#editOpenPopup").val("false");
						$('#editQuickLinks').dialog('open');
						$("#pdfid").parent().hide();
					}
					if ($("#addOpenPopup").val() == "true") {
						$("#addOpenPopup").val("false");
						$('#addQuickLinks').dialog('open');
						$("#pdfid").parent().hide();
					}

					$("#popupSubmit").live('click',function(event) {
			              var addQLPageName =jQuery.trim($("#addQLPageName").val());
							if (addQLPageName == "") {
								alert($('#qlAlertAliasNameBlank').val());
							}else if(addQLPageName != ""){
									var checkboxStatus = true;
								$(".quick-links").find("ul").find("li").each(function() {
									if ($.trim($(this).text().toLowerCase())== addQLPageName.toLowerCase()){
										checkboxStatus = false;
										alert("\""+addQLPageName +"\" Alias Name already exist.");
									}
								});
	
								if (checkboxStatus) {
									/* $("#addQuickLinksForm").submit(); */
									populateAddQL();
								} else {
									return false;
								}
							} 									
							else if (jQuery.trim($("#addQLPageName").val()).length > 100) {
								alert($('#qlPageNameMaxlength').val());
							} else {
								/* $("#addQuickLinksForm").submit(); */
								populateAddQL();
							}
					});
					
					function populateAddQL(){
						var pageName = $('#addQLPageName').val();
						var selectedAssetURL = $('#selectedAssetURLId').val();
						<%
							LiferayPortletURL resourceURL = (LiferayPortletURL)renderResponse.createResourceURL();
						    resourceURL.setCopyCurrentRenderParameters(false);
						    resourceURL.setResourceID("populateAddQuickLinkForm");
						%>
						var url = "<%=resourceURL%>";
						$.ajax({
							  url: url,
				              type: "POST",
				              data: {pageName:pageName ,selectedAssetURL : selectedAssetURL }, 
				              dataType: "html",
			                  success: function(data, textStatus, xhr) {
			                       $("#addQuickLinks-Wrapper").html( $.trim(data) );
			                       refreshQL();
			                   },
			                   error: function(xhr, textStatus, errorThrown) {
			                	   $("#addQuickLinks").html("<h5>Error while loading the Form.</h5>");
			                   }
				            });
					}	
					
					function refreshQL(){
						<%
							LiferayPortletURL refreshQLURL = (LiferayPortletURL)renderResponse.createResourceURL();
						    refreshQLURL.setCopyCurrentRenderParameters(false);
						    refreshQLURL.setResourceID("refreshQuickLinks");
						%>
						var url = "<%=refreshQLURL%>";
						$.ajax({
							  url: url,
				              type: "POST",
				              dataType: "html",
			                  success: function(data, textStatus, xhr) {
			                       $("#refreshQLId").html( $.trim(data) );
			                   },
			                   error: function(xhr, textStatus, errorThrown) {
			                	   $("#refreshQLId").html("<h5>Error while loading the Form.</h5>");
			                   }
				            });
					}

					$("span.ui-icon.ui-icon-closethick,#popupCancel").live('click',function(event) { 
						closeQL();
					});
					
					 $("span.ui-icon.ui-icon-closethick").click(function() {
						 closeQL();
					 }); 
					 
					 function closeQL(){
							$('#addQuickLinks').dialog('close');
							$('.quick-links').find('dt').find('a').text('QUICK LINKS');
							$("#pdfid").parent().show(); 
					 }

					
					$("#editclose").live('click',function(event) {
						/* $("#actionName").val("Close"); */
						$('#editQuickLinks').dialog('close');
						$('.quick-links').find('dt').find('a').text('QUICK LINKS');
						$("#pdfid").parent().show();
					});
					
					$("#editupdate").live('click',function(event) {
							$("#actionName").val("Update");
							$('#editQuickLinksForm').append('<input type="hidden" name="page" value='+$.trim($('#pagedropdown').val())+' />')
							if (!$("#editQuickLinksForm").find("input:checkbox").is(':checked')) {
								alert($('#qlAlertUpdate').val());
								return false;
							} else if ($("#editQuickLinksForm").find("input:checkbox")) {
								var checkboxStatus = true;
								$("#editQuickLinksForm").find("td input:checkbox:checked").each(function() {
									var changedPageName =$(this).parent().parent().find("input:text").val(); 
									
									$(".quick-links").find("ul").find("li").each(function() {
										if ($.trim($(this).text().toLowerCase())== changedPageName.toLowerCase()){
											checkboxStatus = false;
											alert("\""+changedPageName +"\" Page Name already exist.");
										}
									});
									if ($(this).parent().parent().find("input:text").val() == "") {
										checkboxStatus = false;
										alert($('#qlAlertPageNameBlank').val());
									}
									
									if ($(this).parent().parent().find("input:text").val() == "") {
										checkboxStatus = false;
										alert($('#qlAlertPageNameBlank').val());
									}
								});
								if (checkboxStatus) {
									 populateEditQL(); 
								} else {
									return false;
								}
							} else if (jQuery.trim($("#editQuickLinksForm").find("input:checked").parent().parent().find("input:text")
																				.val()).length > 100) {
								alert($('#qlPageNameMaxlength').val());
								return false;
							} else {
								/*  $("#editQuickLinksForm").submit();  */
								 populateEditQL(); 
							}
					});
					
					
					function populateEditQL(){
						<%
							LiferayPortletURL editQLURL = (LiferayPortletURL)renderResponse.createResourceURL();
						    editQLURL.setCopyCurrentRenderParameters(false);
						    editQLURL.setResourceID("populateEditQuickLinkForm");
						%>
						var url = "<%=editQLURL%>";
						$.ajax({
							  url: url,
				              type: "POST",
				              data: $("#editQuickLinksForm").serialize(), 
				              dataType: "html",
			                  success: function(data, textStatus, xhr) {
			                       $("#editQuickLinks-Wrapper").html( $.trim(data) );
			                       var pageNum =  $("#qlPageNumber").val();
			                       sorter.tinygoto(pageNum);
			                       $("#pagedropdown").val(pageNum); 
			                       refreshQL();
			                   },
			                   error: function(xhr, textStatus, errorThrown) {
			                	   $("#editQuickLinks").html("<h5>Error while loading the Form.</h5>");
			                   }
				            });
					}	

					$("#editdelete").live('click',function(event) {
							$("#actionName").val("Delete");
							$('#editQuickLinksForm').append('<input type="hidden" name="page" value='+$.trim($('#pagedropdown').val())+' />');
							if ($("#editQuickLinksForm").find("input:checkbox").is(':checked')) {

								if (confirm($('#qlAlertDeleteConfirm').val())) {

									/* $("#editQuickLinksForm").submit(); */
									populateEditQL();
								}
							} else {
								alert($('#qlAlertDelete').val());
							}
					});

					$(".quick-links").find('a[target="_blank"]').click(function() {
								$("#quickLinksForm dl dt a").text($('#qlSelectOption').val());
					});
					
					$('#addQuickLinksForm').live('submit',function() {
					    $('input[type=button]', this).attr('disabled', 'disabled');
					});
					
					var qlAuthValue= $('#qlPauthId').val();
				    $('.quick-links').find('a[href*="/group/coach/"]').live('mousedown',function(event) {
					    var currentHREFValue = $(this).attr("href");
					    var editedHREFValue = "";
					    if (event.which === 1 || event.which === 3) {
					    if(currentHREFValue != null){
				 	    	 if(currentHREFValue.indexOf("p_auth=") >= 0){
				 	    		 var beforePauth = currentHREFValue.split("p_auth=")[0];
				 	    		 var afterPauth = currentHREFValue.split("p_auth=")[1];
				 	    		  editedHREFValue = beforePauth+"p_auth="+qlAuthValue+afterPauth.replace(afterPauth.split('&')[0],'');
				 	    	  }
				 	    	   else if (currentHREFValue.indexOf("?") >=0){
				 	    		  editedHREFValue = currentHREFValue+"&p_auth="+qlAuthValue;
				 	    	  } else if(currentHREFValue.indexOf("?") == -1){
				 	    		  editedHREFValue = currentHREFValue;
				 	    	  }
					    	}
					    }
					    $(this).attr("href",editedHREFValue);
				    });
				    
				    
				    $("#checkQuickLinkIds").live('click',function() {
						if($(this).is(':checked'))
						{
							$('#table').find('tbody').find('tr').each(function() {
								if($(this).attr('style') == null || $(this).attr('style') == ""){
									$(this).find('input[name=quickLinkIds]').attr('checked', true);	
								}
							});
						}else{
							$('#table').find('tbody').find('input:checked').each(function() {
								$(this).attr('checked', false);	
							});
						}
					});
				    
				    $("#pagedropdown").live('change',function() {
				    	$("#checkQuickLinkIds").attr('checked', false);
				    	$('input[name=quickLinkIds]').attr('checked', false);
				    });
						
				    $('img[onclick*="sorter.move"]').live('click',function() {
				    	  $("#pagedropdown").val($("#currentpage").text()); 
				    }); 

	});
	
</script>

<portlet:actionURL var="quickLinksUrl">
	<portlet:param name="myaction" value="quickLinks" />
</portlet:actionURL>
<portlet:actionURL var="addQuickLinksUrl">
	<portlet:param name="myaction" value="quickLinks" />
	<portlet:param name="selcetedQuickLink" value="addQuickLinks" />
</portlet:actionURL>
<portlet:actionURL var="editQuickLinksUrl">
	<portlet:param name="myaction" value="editQuickLinks" />
</portlet:actionURL> 

<portlet:renderURL var="pageRefreshUrl">
    
</portlet:renderURL>

<fmt:bundle basename="content/Language">

	<form:form method="post" action="${quickLinksUrl}" id="quickLinksForm">
	   <input type="hidden" value="<%=AuthTokenUtil.getToken(request) %>" id="qlPauthId" />
		<dl id="sample" class="quick-links">
			<dt>
				<a
					style="text-decoration: none; font-size: 12px; font-weight:bold; border: 1px solid black;"
					href="javascript:void(0)"><span><spring:message
							code="label.quicklinks.select.option" /> </span> </a>
			</dt>
			<dd> 
			    
				<ul>
				<div id='refreshQLId'>
					<c:if test="${not empty quickLinksVOList}">
						<c:forEach var="quickLinks" items="${quickLinksVOList}">
						<c:set var="pageURL"
									value="${quickLinks.assetURL}" />
						<c:set var="portalURL"
									value="<%=(\"/group\"+themeDisplay.getParentGroup().getFriendlyURL())%>" />
						<c:choose>
							<c:when test="${fn:contains(pageURL, '/group/coach/')}">
						      <li><a href="${pageURL}">${quickLinks.pageName}</a>
									</li>
							</c:when>
							<c:otherwise>
									<li><a href="${pageURL}" target="_blank">${quickLinks.pageName}</a>
									</li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
					</c:if>
				</div>	
					------------------------
					<li><a class="editQuickLinks"><spring:message
								code="label.quicklinks.select.editoption" /> </a>
					</li>
					<li><a class="addQuickLinks"><spring:message
								code="label.quicklinks.select.addoption" /> </a>
					</li>
				</ul>
			</dd>
		</dl>
	</form:form>
	

	<div id="addQuickLinks" style="display: none;">
	    
		<div id="addQuickLinks-Wrapper">
			
				
			<%
				String addPopup = (String) request.getAttribute("addOpenPopup");
				String addedAssetURL = (String) request.getAttribute("addedAssetURL");
					if (addPopup != null && addPopup.equals("true")) {
			%>
			<input type="hidden" value="<%=addPopup%>" id="addOpenPopup" name="addOpenPopup" />
			<%
				}
			
			if (Validator.isNotNull(addedAssetURL)) {
				%>
					<input type="hidden" value="<%=addedAssetURL%>" id="qlAddedAssetURLId" name="qlAddedAssetURLId" />
				<%
					}
				%>
			<br />
			<form:form name="addQuickLinks" method="post"
				action="${addQuickLinksUrl}" id="addQuickLinksForm">
			    
				
			</form:form>
		</div>
	</div>

	<form:form action="${pageRefreshUrl}" id="pageRefreshForm">

	</form:form>
	
	<jsp:include page="/WEB-INF/jsp/editQuickLinks.jsp" />
    <input type="hidden" value="<%=("/group"+themeDisplay.getParentGroup().getFriendlyURL() +
		themeDisplay.getLayout().getFriendlyURL() )%>" id="qlCurrentPageURL" />
	<input type="hidden"
		value="<spring:message code='label.quicklinks.alert.update'/>"
		id="qlAlertUpdate" />
	<input type="hidden"
		value="<spring:message code='label.quicklinks.alert.pagename.blank'/>"
		id="qlAlertPageNameBlank" />
	<input type="hidden"
		value="<spring:message code='label.quicklinks.alert.delete'/>"
		id="qlAlertDelete" />
	<input type="hidden"
		value="<spring:message code='label.quicklinks.alert.deleteconfirm'/>"
		id="qlAlertDeleteConfirm" />
	<input type="hidden"
		value="<spring:message code='label.quicklinks.add.title'/>"
		id="qlAddTitle" />
	<input type="hidden"
		value="<spring:message code='label.quicklinks.edit.title'/>"
		id="qlEditTitle" />
	<input type="hidden"
		value="<spring:message code='label.quicklinks.alert.pagename.maxlength'/>"
		id="qlPageNameMaxlength" />
	<input type="hidden"
		value="<spring:message code='label.error.requiredfeild.blank'/>"
		id="qlAlertAliasNameBlank" />
	<input type="hidden"
		value="<spring:message code='label.quicklinks.select.option'/>"
		id="qlSelectOption" />
	<input type="hidden"
		value="<spring:message code='error-quicklinks-add-duplicate'/>"
		id="qlAddDuplicate" />	
</fmt:bundle>


