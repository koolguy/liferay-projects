<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.coach.cip.util.LoggerUtil"%>
<%@page import="javax.portlet.WindowStateException"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="javax.portlet.PortletRequest"%>
<%@page import="com.liferay.portlet.PortletURLFactoryUtil"%>
<%@page import="java.net.URLDecoder"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.liferay.portal.kernel.xml.Document"%>
<%@page import="com.liferay.portal.kernel.xml.SAXReaderUtil"%>
<%@page import="com.liferay.portlet.journal.service.JournalTemplateLocalServiceUtil"%>
<%@page import="com.liferay.portlet.journal.model.JournalTemplate"%>
<%@page import="com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticle"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.coach.cip.util.Constants"%>
<%@page import="com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="com.liferay.portlet.asset.model.AssetVocabulary"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.liferay.portal.kernel.util.PropsKeys"%>
<%@page import="com.liferay.portal.kernel.util.PrefsPropsUtil"%>
<%@page import="com.liferay.portal.kernel.util.LocaleUtil"%>
<%@page import="java.util.Locale"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="javax.portlet.RenderResponse"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@page import="javax.portlet.RenderRequest"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>	
<%@page import="com.liferay.portal.model.Layout"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portlet.asset.model.AssetCategory"%>
<%@page import="com.coach.cip.util.WebUtil"%>
<%@page import="com.liferay.portlet.asset.model.AssetEntry"%>
<%@page	import="com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil"%>
<%@page	import="com.coach.cip.util.CoachComparator"%>


<liferay-theme:defineObjects />
<portlet:defineObjects />
<style>
.srl {
	color:#FF9454 !important;
	}
#SRLlevel3child li a:hover {
	text-decoration: none; 
	color:blue !important;
	}
</style>


<script>

$(document).ready(function($){
	
	$("a.srllink").click(function(event) {
		$('a.srllink').each(function(){							 						
		  	$(this).removeClass('srl');						 
		});
	    $(this).addClass('srl');
	    var currentURL = $(location).attr('href');		
		var value = $(this).attr("href");
	 	if(value != null)
		{
			Liferay.fire('sideRLValue', value);
			return false;
		} 
	});  
});
</script>

<c:set var="fileTitle" value="${fileTitle}"/>
<%
	ResourceBundle rb = ResourceBundle.getBundle("/content/Language",locale);
	String fileTitle ="";
	if(pageContext.getAttribute("fileTitle") != null){
		fileTitle = (String)pageContext.getAttribute("fileTitle");
	}
	//Added
	Locale defaultLocale = LocaleUtil.getDefault();
    List<AssetEntry> assetEntryList = null;
	List<Layout> parentNavs = layout.getAncestors();
	List<Layout> childNavs = null;
	List<Layout> subChildNavs = null;
	String currentPage = layout.getHTMLTitle(defaultLocale);
	String currentParentPage = null;
	int counter = 0;
	//renderResponse.setTitle(parentNavs.get(0).getHTMLTitle(locale));
	
	if(parentNavs.size() == 2)
	{
		counter=1;
	//	renderResponse.setTitle(parentNavs.get(1).getHTMLTitle(locale));
		currentParentPage = parentNavs.get(0).getHTMLTitle(defaultLocale);
	}else if (parentNavs.size() == 1) {
	//	renderResponse.setTitle(parentNavs.get(0).getHTMLTitle(locale));
		counter=0;
		currentParentPage = parentNavs.get(0).getHTMLTitle(defaultLocale);
	}
	
	//Added to set title.
	renderResponse.setTitle(rb.getString(Constants.SIDE_RELATED_LINKS_TITLE));
	
	if(currentPage != null){
		currentPage = currentPage+"-"+"RELATED LINKS";
	}
	List<AssetCategory> assetCategoryList = new ArrayList<AssetCategory>();
	List<AssetCategory> chinaCategoryList = new ArrayList<AssetCategory>();
	String currentLocale = "";
	assetCategoryList = WebUtil.fetchAssetCategoryByName(renderRequest, currentPage, currentParentPage, defaultLocale);
	if(assetCategoryList != null && assetCategoryList.size() != 0){
		
		//ADDED
		String locales = PrefsPropsUtil.getString(themeDisplay.getCompanyId(), PropsKeys.LOCALES);
		String[] splitLocales = locales.split(",");
		for(int s=0;s<splitLocales.length;s++){
			if(locale.toString().equals(splitLocales[s])){
				currentLocale = splitLocales[s];
			}
		}
		/* Fetching Asset Category based on locale and vocabulary. */
		if(!(locale.toString().equals("en_US"))){
			for(int n=0;n<assetCategoryList.size();n++){
				AssetVocabulary assetVocabulary = AssetVocabularyLocalServiceUtil.getAssetVocabulary(assetCategoryList.get(n).getVocabularyId());
				if(locale.toString().equals(currentLocale) && assetVocabulary.getName()!= null && assetVocabulary.getName().contains(currentLocale.split("_")[1])){
					chinaCategoryList.add(assetCategoryList.get(n));
				}
			}
			assetCategoryList.clear();
			assetCategoryList.addAll(chinaCategoryList);
		}			
		
		//Method definitions modified to check permissions.
		if(assetCategoryList != null && assetCategoryList.size() > 0){
		assetEntryList = WebUtil.fetchAssetEntries(assetCategoryList.get(0), renderRequest);
		}
		}
		if(assetEntryList!=null && assetEntryList.size()>0)
		{
		//Added to sort sideRelatedLinks.
		CoachComparator.sort(assetEntryList, "getTitleCurrentValue", true, false);
		out.println(" <ul id ='SRLlevel3child' style='padding-left: 10px;overflow-y: scroll;height: 173px;padding-bottom: 10px;'>"); %>
		<%=displayAssetEntries(assetEntryList, renderRequest, renderResponse, themeDisplay,fileTitle) %>
	<%	out.println(" </ul>");
    } %>
	
   <%!private String cropTitle(String assetEntryTitle) {

		String title = "";
		if(assetEntryTitle.contains("."))
		{
			title = assetEntryTitle.split("\\.")[0];
			 
		}else{
			title = assetEntryTitle;
		}
		return title.toUpperCase();
	}%>

<%!
	final Log logger = LogFactoryUtil.getLog(this.getClass());
	private String displayAssetEntries(List<AssetEntry> assetEntryList, RenderRequest renderRequest,
	                                RenderResponse renderResponse, ThemeDisplay themeDisplay, String fileTitle) {
	    JournalArticle journalArticle = null;
		StringBuilder assetEntryStringBuffer = new StringBuilder();
		for(AssetEntry assetEntry : assetEntryList){
			String title = "";
			String categoryFileURL = "";
			String typeOfLink = "";
			String mimeType = assetEntry.getMimeType().trim();
			
			/* For Webcontent, Internal & External Link. */
			if (assetEntry.getClassName().endsWith("JournalArticle")) {
				try {
					/* For Webcontent */
					journalArticle = JournalArticleLocalServiceUtil.getLatestArticle(assetEntry.getClassPK());
                        
					title = WebUtil.cropTitle(journalArticle.getTitleCurrentValue());
					categoryFileURL = journalArticle.getArticleId();
					/* For Internal & External Link (Structures & Templates) . */
					if (journalArticle != null && journalArticle.getTemplateId() != null && !journalArticle.getTemplateId().isEmpty()) {
						JournalTemplate journalTemplate =
							JournalTemplateLocalServiceUtil.getTemplate(journalArticle.getGroupId(), journalArticle.getTemplateId());
						if (journalTemplate != null && journalTemplate.getNameCurrentValue().equalsIgnoreCase("COACH_LINK_TEMPLATE")) {

							Document document = SAXReaderUtil.read(journalArticle.getContent());
							categoryFileURL = document.selectSingleNode(Constants.DYNAMIC_ELEMENT_NAME + "url" + Constants.DYNAMIC_CONTENT).getText();
							typeOfLink = document.selectSingleNode(Constants.DYNAMIC_ELEMENT_NAME + "type" + Constants.DYNAMIC_CONTENT).getText();

						}
					}
				}
				catch (Exception e) {
					LoggerUtil.errorLogger(logger, "SideRelatedLinks jsp displayAssetEntries() - Journal Article Exception ",e);
				}
			}/* For Document & Media. */
			else {
				categoryFileURL = WebUtil.displayFileURL(assetEntry.getClassPK(), renderRequest, renderResponse, themeDisplay);
				title = WebUtil.cropTitle(assetEntry.getTitleCurrentValue());
			}
			
			if (null != categoryFileURL && !categoryFileURL.isEmpty()) {

				PortletURL actionURL = renderResponse.createActionURL();
				
				actionURL = PortletURLFactoryUtil.create(renderRequest,"SideRelatedLinks_WAR_cipportlet",themeDisplay.getPlid(),PortletRequest.ACTION_PHASE);
				try{
					actionURL.setWindowState(WindowState.NORMAL);
				}catch( WindowStateException e)
				{
					LoggerUtil.errorLogger(logger, "SideRelatedLinks jsp displayAssetEntries() - WindowStateException",e);
				}
				actionURL.setParameter("myaction", "publishSideRelatedlinkURL");
				actionURL.setParameter("fileURL", categoryFileURL);
				actionURL.setParameter("fileTitle", title);
				actionURL.setParameter("fileType", assetEntry.getClassName());
				actionURL.setParameter("classPK", String.valueOf(assetEntry.getClassPK()));
				actionURL.setParameter("typeOfLink", typeOfLink);
				actionURL.setParameter("mimeType", mimeType);
				
				if(fileTitle != null && title != null && fileTitle.trim().equalsIgnoreCase(title.trim())){
					assetEntryStringBuffer.append("<li class='srl' style='padding-bottom: 4px;list-style: none;'>");
				}else{
					assetEntryStringBuffer.append("<li style='padding-bottom: 4px;list-style: none;'>");
				}
				assetEntryStringBuffer.append("<a class='srllink' style='color:black;' href='");
				assetEntryStringBuffer.append(actionURL.toString());
				assetEntryStringBuffer.append("::" + title);
				assetEntryStringBuffer.append("'>");
				assetEntryStringBuffer.append(title);
				assetEntryStringBuffer.append("</a>");
				assetEntryStringBuffer.append("</li>");
			}
		}
		return assetEntryStringBuffer.toString();
	}%>	
