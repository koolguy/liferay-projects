<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>

<%@page import="com.coach.cip.common.dto.PersonalMessageVO"%>
<%@ page import="java.util.List"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />


<link href="<%=themeDisplay.getPathThemeCss()%>/ticker-style.css"
	rel="stylesheet" type="text/css" />
<script src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.ticker.js"
	type="text/javascript"></script>
<script src="<%=themeDisplay.getPathThemeCss()%>/../js/site.js"
	type="text/javascript"></script>

<%
	String redirect = PortalUtil.getCurrentURL(renderRequest);
%>

<%-- <%
	String redirect = PortalUtil.getCurrentURL(renderRequest);
%>

<c:if test="${not empty personalMessageVOList}">
<div style="height:140px;">
	<c:forEach var="personalMessage" items="${personalMessageVOList}">
		<div id="aui_3_4_0_1_622">
			<P id="aui_3_4_0_1_621" style="color: #d76c03; font-weight: bold">
				<B><c:out value="${personalMessage.displayDate}"></c:out> </B>
			</P>
			<P id="aui_3_4_0_1_715"
				style="font-family: 'myriad-web-pro'; font-size: 12px;">
				<c:out value="${personalMessage.modifiedMessage}"></c:out>

			</P>

			<P id="aui_3_4_0_1_715" style="color:#7fb2ca;font-weight:bold">
				<a style="text-decoration: none;color:black"
					href="<portlet:actionURL>
							<portlet:param name="myaction" value="detailPersonalMessage" />
							<portlet:param name="messageId" value="${personalMessage.messageId}" />
							<portlet:param name="redirect" value="<%= redirect %>" />
						</portlet:actionURL>">
					LEARN MORE >></a>
			</P>

		</div>
	</c:forEach>
</div>

	<P style="float:right;background:#000;color:#fff; margin: -7px -7px 0px 0px;padding: 2px 2px 2px 2px;">
		<a style="text-decoration: none;color:white;"
			href="<portlet:actionURL>
							<portlet:param name="myaction" value="allPersonalMessage" />	
							<portlet:param name="redirect" value="<%= redirect %>" />						
						</portlet:actionURL>">
			ALL LOCAL NEWS</a>
	</P>

</c:if> --%>

<c:set var="personalMessageVOList" value="${personalMessageVOList}"></c:set>
<%
	List<PersonalMessageVO> personalMessageVOList = (List<PersonalMessageVO>) pageContext.getAttribute("personalMessageVOList");
	
%>
<c:if test="${not empty personalMessageVOList}">

	<ul id="js-news" class="js-hidden">
		<c:forEach var="personalMessage" items="${personalMessageVOList}">
			<li class="news-item">
			
				<a style="text-decoration: none;color:black"
						href="<portlet:actionURL>
								<portlet:param name="myaction" value="detailPersonalMessage" />
								<portlet:param name="messageId" value="${personalMessage.messageId}" />
								<portlet:param name="redirect" value="<%= redirect %>" />
							</portlet:actionURL>">
						<c:out value="${personalMessage.subject}"></c:out>
				</a>
			
			
			
			</li>
		</c:forEach>
	</ul>
	<noscript>
		<h2>Latest News</h2>
		<ul id="no-js-news">
			<c:forEach var="personalMessage" items="${personalMessageVOList}">
				<li class="news-item">
				<a style="text-decoration: none;color:black"
						href="<portlet:actionURL>
								<portlet:param name="myaction" value="detailPersonalMessage" />
								<portlet:param name="messageId" value="${personalMessage.messageId}" />
								<portlet:param name="redirect" value="<%= redirect %>" />
							</portlet:actionURL>"><c:out value="${personalMessage.subject}"></c:out></a>
				</li>
			</c:forEach>
		</ul>
	</noscript>
</c:if>

<c:if test="${empty personalMessageVOList}">

<style>
#p_p_id_personalMessage_WAR_cipportlet_{
	display: none;
}
</style>

	<P id="aui_3_4_0_1_715"
		style="font-family: 'myriad-web-pro'; font-size: 12px; font-weight: bold; display: none;">
		<c:out value="No message available"></c:out>
	</P>
</c:if>

<c:if test="<%=personalMessageVOList.size() == 1 %>">

<style>
.ticker-controls{
	display: none;
}
</style>

</c:if>

