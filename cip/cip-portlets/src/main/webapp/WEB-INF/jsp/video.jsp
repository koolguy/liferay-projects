<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.PropsUtil"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="liferay-portlet" uri="http://liferay.com/tld/portlet"%>
<liferay-theme:defineObjects />
<portlet:defineObjects />
<%
	String videoPath  = PropsUtil.get("com.coach.cip.video.path");
	videoPath = Validator.isNotNull(videoPath)? videoPath : "";
%>
<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/flowplayer-3.2.11.min.js"></script>
<div>
	<a style="display: block; width: 640px; height: 300px;" id="player">
	</a>
	<script> 
		$f("player", "<%=themeDisplay.getPathThemeCss()%>/../player/flowplayer-3.2.15.swf",
				{
					playlist : [ {
						url : '<%=videoPath%>'
					} ]
				});
	</script>
</div>