<%@page import="java.util.ResourceBundle"%>
<%@page import="org.springframework.web.portlet.context.PortletApplicationContextUtils"%>
<%@page import="javax.portlet.PortletContext"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%@page import="org.springframework.context.support.ReloadableResourceBundleMessageSource"%>
<%@page import="org.springframework.web.portlet.context.XmlPortletApplicationContext"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletURL"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletURL"%>
<%@ include file="init.jsp"%>

<script type="text/javascript">
	$(document).ready(function($){
		
		var nameSpace = "#p_p_id" + '<portlet:namespace />';
		
		Liferay.on('QandA',function(event, title) {
			$(nameSpace).hide();
	    	var loadingImg=document.getElementById("QandAId");
	    	loadingImg.innerHTML='<div id="pdf-placeholder" style="width: 100%;height:100%;"><img src="<%=themeDisplay.getPathThemeCss()%>/../images/loadinganimation.gif"/></div>';
			
            popupQandA(title)

		});
		
		function popupQandA(title){
			title = title+" - "+$('#qandaPartialTitle').val();
			$(nameSpace).find('.portlet-title').html(title);
			<%
				LiferayPortletURL resourceURL = (LiferayPortletURL)renderResponse.createResourceURL();
			    resourceURL.setCopyCurrentRenderParameters(false);
			    resourceURL.setResourceID("populateQandA");
			%>
			
			$.ajax({
	              url: "<%=resourceURL%>"+ "&title="+title ,
	              type: "POST",
	              dataType: "html",
                  success: function(data, textStatus, xhr) {
                	  if($.trim(data) != null && $.trim(data) != "")
                	  {
                        $("#QandAId").html( $.trim(data) );
                        $(nameSpace).show();
                	  }	
                   },
                   error: function(xhr, textStatus, errorThrown) {
                	   $("#QandAId").html("<h5>Error while loading the Document.</h5>");
                   }
	            });
		}	

	});
</script>
       <%
		  if(WebUtil.hasUserHRServiceCenterRole(renderRequest))
		  {		  
		%>
        <div id="qandAHRCenterCSSWrapper">
        <% } else { %>
		<div id="qandAIdCSSWrapper" >
	    <% } %>
       
       <div id="QandAId" height="600" width="100%">
       
       <%
       		String fileType = renderRequest.getParameter("fileType");
            String fileURL = renderRequest.getParameter("fileURL"); 
       		String title = renderRequest.getParameter("title");
       		String classPK = renderRequest.getParameter("classPK");
       		String mimeType = renderRequest.getParameter("mimeType");
     		String renderURL = themeDisplay.getPortalURL()+PortalUtil.getCurrentURL(renderRequest);
     		final Log logger = LogFactoryUtil.getLog(this.getClass());
       		/* Dispalying Content on landing page for First request: Start */
       		if(Validator.isNull(fileType) && Validator.isNull(fileURL) && Validator.isNull(title) && WebUtil.hasQandAPortletAccess(renderRequest)){
       			String parentLayout = "";
       			List<Layout> parentNavs = layout.getAncestors();
       			List<AssetEntry> pageNameAssetEntryList;
       			/* PortletContext pc = renderRequest.getPortletSession().getPortletContext();
       			ApplicationContext context = PortletApplicationContextUtils.getWebApplicationContext(pc);
       			ReloadableResourceBundleMessageSource resourceBundle =(ReloadableResourceBundleMessageSource)context.getBean("messageSource") ; */ 
       			ResourceBundle resourceBundle = ResourceBundle.getBundle("content/Language", themeDisplay.getLocale());
       			String QandAConstant = " - "+resourceBundle.getString("label.qanda.partialtitle");
   				pageNameAssetEntryList	= WebUtil.fetchQandAAssetEntry(layout.getHTMLTitle(locale).trim()+QandAConstant,locale);
       			
       			if(pageNameAssetEntryList.size()>0)	{
					JournalArticle journalArticle = null;
					AssetEntry pageNameAssetEntry = pageNameAssetEntryList.get(0);
					fileType = pageNameAssetEntry.getClassName();
					mimeType = pageNameAssetEntry.getMimeType();
					if (pageNameAssetEntry.getClassName().endsWith("JournalArticle")) {
						try {
							/* For Webcontent */
							journalArticle = JournalArticleLocalServiceUtil.getLatestArticle(pageNameAssetEntry.getClassPK());
							title = WebUtil.cropTitle(journalArticle.getTitleCurrentValue());
							fileURL = journalArticle.getArticleId();
														
							if(!journalArticle.isApproved())
							{
								fileURL = "";
							} 
						}
						catch (Exception e) {
							LoggerUtil.errorLogger(logger, "PDFPortlet jsp  - Journal Article Exception ",e);
						}
					}/* For Document & Media. */
					else {
						fileURL = WebUtil.displayFileURL(pageNameAssetEntry.getClassPK(), renderRequest, renderResponse, themeDisplay);
						title = WebUtil.cropTitle(pageNameAssetEntry.getTitleCurrentValue());
					}
					
				}
       		}
       		/* Dispalying Content on landing page for First request: End */
       		
       		
       		/* Checking the Asset accesibility oustide the network : Start */
			if(Validator.isNotNull(fileType) && WebUtil.isExternalNetwork(renderRequest)){	
			  try{ 	
				boolean assetExternalAccess = WebUtil.hasAssetExternalAccess(themeDisplay.getCompanyId(),fileType,
						ResourceConstants.SCOPE_INDIVIDUAL,classPK,0,ActionKeys.VIEW);
				if(!assetExternalAccess){
					fileURL = Constants.NO_USER_PRIVILEGE;
				}
			  }catch(Exception e) {
				  LoggerUtil.errorLogger(logger, "PDFPortlet jsp - Internal Access only Exception ",e);
				}
			} 
			/* Checking the Asset accesibility oustide the network : End */
       		
			/* Removing the Domain Name or IP Address from the URL for DLFileEntry : Start. */ 
       		 if(Validator.isNotNull(fileType) && fileType.endsWith("DLFileEntry") && !fileURL.equals(Constants.NO_USER_PRIVILEGE)){
       			
       			fileURL = StringUtils.getChomp(fileURL,"/documents");
       			if(!locale.getLanguage().equalsIgnoreCase("en") ){
         		      fileURL = URLDecoder.decode(fileURL,"UTF-8");
         		}	
       			if(fileURL.contains("%26")){
       				fileURL = fileURL.replaceAll("%26","&");
       			}
       			if(fileURL.contains(" ")){
       				fileURL = fileURL.replaceAll(" ","+");
       			}
       		} 
       		/* Removing the Domain Name or IP Address from the URL for DLFileEntry : End. */
       		
       	%>

      <%--   <input type="hidden" value="<%=pauthId%>" id="pauthId" /> --%>
        
		<c:choose>
			<c:when test="<%=title != null && !title.isEmpty() %>">
				<%
					/* renderResponse.setTitle(title.toUpperCase()); */
					renderResponse.setTitle(title);
				%>
			</c:when>
			<c:otherwise>
				<%
					renderResponse.setTitle(layout.getHTMLTitle(locale));
				%>
			</c:otherwise>
		</c:choose>

		<c:choose>
			<c:when test="<%= (Validator.isNotNull(fileURL) && !fileURL.contains(Constants.NO_USER_PRIVILEGE))%>">
				<c:choose>
					<c:when
						test="<%=fileType != null && !fileType.isEmpty() && fileType.endsWith(\"JournalArticle\") && fileURL.matches(\"[0-9]*\") %>">
						<liferay-ui:journal-article articleId="<%=fileURL%>"
							groupId="<%= themeDisplay.getLayout().getGroupId() %>" />
					</c:when>
					<c:otherwise>
						 <c:if test="<%= Validator.isNotNull(mimeType) && !mimeType.equalsIgnoreCase(\"application/pdf\") && !mimeType.equalsIgnoreCase(\"text/html\")%>">
						      <iframe style="display:none"  src="<%=fileURL%>" width="0%" height="0"></iframe> 
						    <h3><spring:message code='label.pdfPortlet.statement.microsoft'/></h3>
						 </c:if>
						 <c:if test="<%= Validator.isNotNull(mimeType) && mimeType.equalsIgnoreCase(\"application/pdf\") %>">
							  <div id="pdf-placeholder" style="width: 100%;height:100%;">
							     <img  src="<%=themeDisplay.getPathThemeCss()%>/../images/loadinganimation.gif"/>
							  </div> 
						      <object id="pdfobject" data="<%=fileURL%>" type="application/pdf" height="600" width="100%" style="display:none"></object>
						 </c:if>
						  <c:if test="<%= Validator.isNotNull(mimeType) && mimeType.equalsIgnoreCase(\"text/html\")%>">
						      <iframe src="<%=fileURL%>" height="600" width="100%"></iframe> 
						 </c:if>
					</c:otherwise>
				</c:choose>
			</c:when>
		
			<c:when	test="<%= (fileURL != null && fileURL.contains(Constants.NO_USER_PRIVILEGE)) %>">
				<h5><spring:message code='label.pdfPortlet.statement.information'/>
                <br/><br/> 
				<spring:message code='label.pdfPortlet.statement.network'/></h5>
			</c:when>
		
			<c:otherwise>
				<script type="text/javascript">
	               $(document).ready(function($){
	            	   var nameSpace = "#p_p_id" + '<portlet:namespace />';
	            	   $(nameSpace).hide();
	               });
	            </script>
			</c:otherwise>
		
		</c:choose>
		
        </div>
       </div> 
	<input type="hidden" value="<spring:message code='label.qanda.partialtitle'/>" id="qandaPartialTitle" />