

<%@page import="java.util.Set"%>
<%@page import="java.util.Map"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="aui" uri="http://alloy.liferay.com/tld/aui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import="com.liferay.portal.model.User"%>
<%@ page import="com.coach.cip.util.WebUtil"%>
<%@ page import="com.liferay.portal.model.Phone"%>
<%@ page import="com.liferay.portal.service.PhoneLocalServiceUtil"%>
<%@ page import="com.liferay.portal.model.Contact"%>
<%@ page import="java.util.List"%>


<portlet:defineObjects />
<liferay-theme:defineObjects />

<%-- <%
	Map<String, String> orgMap = (Map<String, String>) portletSession.getAttribute("orgMap");
	Set<String> orgNames = orgMap.keySet();
	if (!orgNames.isEmpty()) {
		String switchImage = themeDisplay.getPathThemeImages() +"/globe.png";
%>
<span id="orgList"><a><img src="<%=switchImage %>"/></a></span>
	<div id="orgListContainer" style="display:none">
		<%
			for (String orgName : orgNames) {
		%>

		<div>
			<a href="<%=orgMap.get(orgName)%>"><%=orgName%></a>
		</div>

		<%
			}
		%>
	</div> <%
 	}
 %> --%>
<%
	Map<String, String> orgMap = (Map<String, String>) portletSession.getAttribute("orgMap");
	Set<String> orgNames = orgMap.keySet();
%>
<dl id="sample" class="organizations-links"> 
	<dt>
		<a style="text-decoration: none; font-size: 12px; font-weight:bold; border: 1px solid black;" href="javascript:void(0)"><span><%=themeDisplay.getScopeGroupName()%></span></a>
	</dt>
	<dd> 
		<ul> 
			<%
			for (String orgName : orgNames) {
			%>
			<li>
				<a href="<%=orgMap.get(orgName)%>"><%=orgName%></a> 
			</li> 
			<%
				}
			%>
		</ul> 
	</dd> 
</dl>
 
<!--  <script>
$('#orgList').parent().parent().hover(function(){
$("#content").find("#pdfid").css({"visibility":"hidden"});
$('#orgListContainer').slideToggle(50);
},function(){$('#orgListContainer').hide();
$("#content").find("#pdfid").css({"visibility":"visible"});
});
</script> -->