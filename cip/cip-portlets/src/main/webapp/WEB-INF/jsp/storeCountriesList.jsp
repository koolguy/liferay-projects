<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="org.apache.velocity.runtime.directive.Foreach"%>
<%@ page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ page import="com.coach.cip.common.dto.StatesVO"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<liferay-theme:defineObjects />
<portlet:defineObjects />

<html>

<head>
<script type="text/javascript"
	src="<%= themeDisplay.getPathThemeCss()%>/../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%= themeDisplay.getPathThemeCss()%>/../js/jquery-ui-1.8.19.custom.min.js"></script>
</head>

<body>
	<c:set var="selectedValue" value='${selectedValue}'></c:set>
	<c:set var="countriesList" value='${countriesList}'></c:set>
	
	<%
	String selectedCountry = "";
	List<String> countriesList1 = new ArrayList<String>();
	if(pageContext.getAttribute("selectedValue") != null){
		selectedCountry = (String)pageContext.getAttribute("selectedValue");
	}
	if(pageContext.getAttribute("countriesList") != null){
		countriesList1 = (List<String>)pageContext.getAttribute("countriesList");
	}
	%>
	
	<select name="countryId" id="target1" class="styledselect_form_1">
	<c:if test="${countriesList ne null}">
	<c:choose>
	<c:when test="${countriesList.size() eq 0 || countriesList.isEmpty()}">
	<c:set var="message"><spring:message code='label.stores.country.zero'/></c:set>
	<option value="0"><spring:message code='label.stores.choose.country'/></option>
	</c:when>
	<c:otherwise>
	<%-- <option value="0"><spring:message code='label.stores.choose.country'/></option> --%>
	<%-- <c:forEach  var="country" items="${countriesList}">
		<option value="${country}">${country}</option>
	</c:forEach> --%>
	
	<%
	for(String country : countriesList1){
		if(country.equals(selectedCountry)){
	%>
		<option selected="true" value="<%=country%>"><%=country%></option>
	<%}else{ %>
		<option value="<%=country%>"><%=country%></option>
	<% }} %>
	
	</c:otherwise>
	</c:choose>
	</c:if>
	</select>
	<%-- <div id="state_errors" class="error_message">${message}</div> --%>
</body>

</html>