<%@ page contentType="text/html; charset=UTF-8" %>

<%@page import="org.apache.cxf.wsdl.http.UrlEncoded"%>
<%@page import="org.apache.commons.lang.StringUtils"%>

<%@page import="com.liferay.portal.util.PortalUtil"%>

<%@page import="com.liferay.portal.security.permission.ActionKeys"%>
<%@page import="com.liferay.portal.kernel.servlet.URLEncoder"%>
<%@page import="com.liferay.portal.model.Layout"%>
<%@page import="com.liferay.portal.model.ResourceConstants"%>
<%@page import="com.liferay.portlet.asset.model.AssetEntry"%>
<%@page import="com.liferay.portlet.journal.service.JournalTemplateLocalServiceUtil"%>
<%@page import="com.liferay.portlet.journal.model.JournalTemplate"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticle"%>
<%@page import="com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil"%>


<%@page import="com.liferay.portal.kernel.xml.Document"%>
<%@page import="com.liferay.portal.kernel.xml.SAXReaderUtil"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.portal.kernel.audit.AuditException"%>
<%@page import="com.liferay.portal.kernel.audit.AuditRouterUtil"%>
<%@page import="com.liferay.portal.kernel.util.PropsUtil"%>
<%@page import="com.liferay.portal.kernel.audit.AuditMessage"%>

<%@page import="java.net.URL"%>
<%@page import="java.net.URLDecoder"%>

<%@page import="javax.portlet.PortletRequest"%>
<%@page import="javax.portlet.RenderRequest"%>
<%@page import="javax.portlet.WindowStateException"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="javax.portlet.RenderResponse"%>

<%@page import="com.liferay.portlet.PortletURLFactoryUtil"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@page import="com.liferay.portal.kernel.util.PropsKeys"%>
<%@page import="com.liferay.portal.kernel.util.PrefsPropsUtil"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portal.service.permission.LayoutPermissionUtil"%>
<%@page import="com.liferay.portal.service.LayoutLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.LocaleUtil"%>

<%@page import="com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil"%>
<%@page import="com.liferay.portlet.asset.model.AssetVocabulary"%>

<%@page import="com.coach.cip.audit.util.Attribute"%>
<%@page import="com.coach.cip.util.Constants"%>
<%@page import="com.coach.cip.util.WebServiceUtil"%>
<%@page import="com.coach.cip.audit.util.AuditMessageBuilder"%>
<%@page import="com.coach.cip.util.CoachComparator"%>
<%@page import="com.coach.cip.util.LoggerUtil"%>
<%@page import="com.coach.cip.util.WebUtil"%>
<%@page import="com.coach.cip.portlets.controller.PDFPortletController"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Set"%>
<%@page import="java.util.Map"%>

<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />