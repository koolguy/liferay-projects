<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.liferay.portal.kernel.util.PropsUtil"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="aui" uri="http://alloy.liferay.com/tld/aui"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ page import="java.util.List"%>
<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@ page import="org.springframework.ui.Model"%>
<%@ page import="java.util.ResourceBundle"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery-ui-1.8.19.custom.min.js"></script>

<script type="text/javascript">
	$(document)
			.ready(
					function() {
						<%--	var dialogOpts = {
							title : $('#qlAddTitle').val(),
							bgiframe : true,
							autoOpen : false,
							modal : true,
							width : "350px"
						};

						var editdialogOpts = {
							title : $('#qlEditTitle').val(),
							bgiframe : true,
							autoOpen : false,
							modal : true,
							width : "730px"
						};

						$('#addQuickLinks').dialog(dialogOpts);

						$('#editQuickLinks').dialog(editdialogOpts);

						$('.addQuickLinks').click(function() {
							$('#addQuickLinks').dialog('open');
						});

						$('.editQuickLinks').click(function() {
							$('#editQuickLinks').dialog('open');
						});

						/* $("#popupCancel").click(function() {
							$('#addQuickLinks').dialog('close');
							window.location.reload();
						}); */

						if ($("#editOpenPopup").val() == "true") {
							$("#editOpenPopup").val("false");
							$('#editQuickLinks').dialog('open');
						}
						if ($("#addOpenPopup").val() == "true") {
							$("#addOpenPopup").val("false");
							$('#addQuickLinks').dialog('open');
						}

						$("#popupSubmit")
								.click(
										function() {
							              var addQLPageName =jQuery.trim($("#addQLPageName").val());
											if (addQLPageName == "") {
												alert($(
														'#qlAlertAliasNameBlank')
														.val())
											}else if(addQLPageName != ""){
													var checkboxStatus = true;
												$(".quick-links").find("ul").find("li").each(function() {
													if ($.trim($(this).text().toLowerCase())== addQLPageName.toLowerCase()){
														checkboxStatus = false;
														alert("\""+addQLPageName +"\" Alias Name already exist.");
													}
												});

												if (checkboxStatus) {
													$("#addQuickLinksForm").submit();
												} else {
													return false;
												}
											} 									
											else if (jQuery.trim($(
													"#addQLPageName").val()).length > 100) {
												alert($('#qlPageNameMaxlength')
														.val());
											} else {
												$("#addQuickLinksForm")
														.submit();
											}
										});

						$("span.ui-icon.ui-icon-closethick,#popupCancel")
								.click(function() {
									$("#pageRefreshForm").submit();
								});

							$("#editclose").click(function() {
							$("#actionName").val("Close");
						});
						$("#editupdate")
								.click(
										function() {
											$("#actionName").val("Update");

											if (!$("#editQuickLinksForm").find(
													"input:checkbox").is(
													':checked')) {
												alert($('#qlAlertUpdate').val())
												return false;
											} else if ($("#editQuickLinksForm")
													.find("input:checkbox")) {
												var checkboxStatus = true;
												$("#editQuickLinksForm")
														.find("td input:checkbox:checked")
														.each(function() {
															var changedPageName =$(this).parent().parent().find("input:text").val() 
															
															$(".quick-links").find("ul").find("li").each(function() {
																if ($.trim($(this).text().toLowerCase())== changedPageName.toLowerCase()){
																	checkboxStatus = false;
																	alert("\""+changedPageName +"\" Page Name already exist.");
																}
															});
															if ($(this).parent().parent()
																	.find("input:text").val() == "") {
																checkboxStatus = false;
																alert($('#qlAlertPageNameBlank').val())
															}
															
															if ($(this).parent().parent()
																	.find("input:text").val() == "") {
																checkboxStatus = false;
																alert($('#qlAlertPageNameBlank').val())
															}
														});
												if (checkboxStatus) {
													$("#editQuickLinksForm").submit();
												} else {
													return false;
												}
											} else if (jQuery
													.trim($(
															"#editQuickLinksForm")
															.find(
																	"input:checked")
															.parent().parent()
															.find("input:text")
															.val()).length > 100) {
												alert($('#qlPageNameMaxlength')
														.val());
												return false;
											} else {
												$("#editQuickLinksForm")
														.submit();
											}
										});

						$("#editdelete").click(
								function() {
									$("#actionName").val("Delete");

									if ($("#editQuickLinksForm").find(
											"input:checkbox").is(':checked')) {

										if (confirm($('#qlAlertDeleteConfirm')
												.val())) {

											$("#editQuickLinksForm").submit();
										}
									} else {
										alert($('#qlAlertDelete').val())
									}
								});
								--%>
						$('a[target="_blank"]').click(
								function() {
									$("#quickLinksForm dl dt a").text($('#qlSelectOption').val());
						    });

					}); 

	/* 	function details(){

	 detailsBox.innerHTML=document.getElementById('user_name').value;

	 } */
</script>

<portlet:actionURL var="quickLinksUrl">
	<portlet:param name="myaction" value="quickLinks" />
</portlet:actionURL>
<%--
<portlet:actionURL var="addQuickLinksUrl">
	<portlet:param name="myaction" value="quickLinks" />
	<portlet:param name="selcetedQuickLink" value="addQuickLinks" />
</portlet:actionURL>
<portlet:actionURL var="editQuickLinksUrl">
	<portlet:param name="myaction" value="editQuickLinks" />
</portlet:actionURL>
<portlet:renderURL var="pageRefreshUrl">
</portlet:renderURL>
 --%>
<fmt:bundle basename="content/Language">

	<form:form method="post" action="${quickLinksUrl}" id="quickLinksForm">
		<dl id="sample" class="quick-links">
			<dt>
				<a
					style="text-decoration: none; font-size: 12px; font-weight:bold; border: 1px solid black;"
					href="javascript:void(0)"><span><spring:message
							code="label.quicklinks.select.option" /> </span> </a>
			</dt>
			<dd>
				<ul>
					<c:if test="${not empty quickLinksVOList}">
						<c:forEach var="quickLinks" items="${quickLinksVOList}">
						<c:set var="pageURL"
									value="${quickLinks.pageURL}" />
						<c:set var="portalURL"
									value="<%=themeDisplay.getPortalURL() %>" />
						<c:choose>
							<c:when test="${fn:contains(pageURL, portalURL)}">
						      <li><a href="${pageURL}"><spring:message
							code="${quickLinks.pageName}" /></a>
									</li>
							</c:when>
							<c:otherwise>
									<li><a href="${pageURL}" target="_blank"><spring:message
							code="${quickLinks.pageName}" /></a>
									</li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
					</c:if>
					<%--
					------------------------
					<li><a class="editQuickLinks"><spring:message
								code="label.quicklinks.select.editoption" /> </a>
					</li>
					<li><a class="addQuickLinks"><spring:message
								code="label.quicklinks.select.addoption" /> </a>
					</li>
					 --%>
				</ul>
			</dd>
		</dl>
	</form:form>

	<%-- 
	<div id="addQuickLinks" style="display: none;">
		<div id="addQuickLinks-Wrapper">
			<jsp:include page="/WEB-INF/jsp/message.jsp" />
			<%
				String addPopup = (String) request.getAttribute("addOpenPopup");
					if (addPopup != null && addPopup.equals("true")) {
			%>
			<input type="hidden" value="<%=addPopup%>" id="addOpenPopup"
				name="addOpenPopup" />
			<%
				}
					else {
			%>
			<input type="hidden" value="<%=addPopup%>" id="addOpenPopup"
				name="addOpenPopup" />
			<%
				}
			%>
			<br />
			<form:form name="addQuickLinks" method="post"
				action="${addQuickLinksUrl}" id="addQuickLinksForm">
				<c:set var="quickLinksVOList" value='${quickLinksVOList}'></c:set>
				<%
					List<QuickLinksVO> quickLinksVOList = (List<QuickLinksVO>) pageContext.getAttribute("quickLinksVOList");
							StringBuilder url = new StringBuilder();
							url.append(themeDisplay.getPortalURL()
							// + themeDisplay.getPathFriendlyURLPublic()
								+
								themeDisplay.getPathFriendlyURLPrivateGroup() +
								themeDisplay.getParentGroup().getFriendlyURL() +
								themeDisplay.getLayout().getFriendlyURL());
							QuickLinksVO quickLinksVO = new QuickLinksVO();
							quickLinksVO.setPageURL(url.toString());
							if (quickLinksVOList.contains(quickLinksVO) && !"true".equals(addPopup)) {
				%>
				<liferay-ui:message key="error-quicklinks-add-duplicate"
					arguments="<%=quickLinksVOList.get(quickLinksVOList.indexOf(quickLinksVO)).getPageName() %>"></liferay-ui:message>
				<br />
				<br />
				<div class="element" align="right">
					<input type="button" id="popupCancel"
						value="<spring:message code='label.close'/>" class="submitClass" />
				</div>
				<%
					}
							else {
				%>
				<div class="element">
					<%
						String pageName = (String) request.getAttribute("pageName");
					%>
					<c:if test="<%= pageName == null %>">
						<span class="mandetory">*</span>
						<spring:message code="label.quicklinks.aliasname" />
						<c:choose>
							<c:when test="${disableAdd ne null && disableAdd eq true}">
								<input type="text" disabled="disabled" id="addQLPageName" maxlength="75"
									value="<%=themeDisplay.getLayout().getName(themeDisplay.getLocale())%>"
									name="pageName" class="required text">
							</c:when>
							<c:otherwise>
								<input type="text" id="addQLPageName" maxlength="75"
									value="<%=themeDisplay.getLayout().getName(themeDisplay.getLocale())%>"
									name="pageName" class="required text">
							</c:otherwise>
						</c:choose>
					</c:if>
					<c:if test="<%= pageName != null %>">
						<span class="mandetory">*</span>
						<spring:message code="label.quicklinks.aliasname" />
						<c:choose>
							<c:when test="${disableAdd ne null && disableAdd eq true}">
								<input type="text" value="<%=request.getAttribute("pageName")%>" maxlength="75"
									disabled="disabled" name="pageName" class="required text"
									id="addQLPageName">
							</c:when>
							<c:otherwise>
								<input type="text" value="<%=request.getAttribute("pageName")%>" maxlength="75"
									name="pageName" class="required text" id="addQLPageName">
							</c:otherwise>
						</c:choose>
					</c:if>
				</div>
				<br />
				<div class="element" align="right">
					<c:if
						test="${disableAdd eq null ||  disableAdd ne null && disableAdd ne true }">
						<input type="button" id="popupSubmit"
							value="<spring:message code='label.add'/>" class="submitClass" />
					</c:if>
					&nbsp; <input type="button" id="popupCancel"
						value="<spring:message code='label.close'/>" class="submitClass" />
				</div>
				<%
					}
				%>
			</form:form>
		</div>
	</div>

	<form:form action="${pageRefreshUrl}" id="pageRefreshForm">

	</form:form>


	<%@include file="/WEB-INF/jsp/editQuickLinks.jsp"%>

	<input type="hidden"
		value="<spring:message code='label.quicklinks.alert.update'/>"
		id="qlAlertUpdate" />
	<input type="hidden"
		value="<spring:message code='label.quicklinks.alert.pagename.blank'/>"
		id="qlAlertPageNameBlank" />
	<input type="hidden"
		value="<spring:message code='label.quicklinks.alert.delete'/>"
		id="qlAlertDelete" />
	<input type="hidden"
		value="<spring:message code='label.quicklinks.alert.deleteconfirm'/>"
		id="qlAlertDeleteConfirm" />
	<input type="hidden"
		value="<spring:message code='label.quicklinks.add.title'/>"
		id="qlAddTitle" />
	<input type="hidden"
		value="<spring:message code='label.quicklinks.edit.title'/>"
		id="qlEditTitle" />
	<input type="hidden"
		value="<spring:message code='label.quicklinks.alert.pagename.maxlength'/>"
		id="qlPageNameMaxlength" />
	<input type="hidden"
		value="<spring:message code='label.error.requiredfeild.blank'/>"
		id="qlAlertAliasNameBlank" />
--%>
	<input type="hidden"
		value="<spring:message code='label.quicklinks.select.option'/>"
		id="qlSelectOption" />

</fmt:bundle>
