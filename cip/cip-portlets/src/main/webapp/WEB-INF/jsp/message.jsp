<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="aui" uri="http://alloy.liferay.com/tld/aui"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<link rel="stylesheet"
	href="<%= themeDisplay.getPathThemeCss() %>/screen.css" type="text/css"
	media="screen" title="default" />
<%-- <link href="<%= themeDisplay.getPathThemeCss() %>/style.css"
	rel="stylesheet" type="text/css" /> --%>
	<script type="text/javascript">
	$(document).ready(function() {

		$(".close-yellow").click(function() {
			
			$(".yellow-left").fadeOut("slow");
			$(".yellow-right").fadeOut("slow");
		});
		$(".close-red").click(function() {
			$(".red-left").fadeOut("slow");
			$(".red-right").fadeOut("slow");
		});
		$(".close-blue").click(function() {
			$(".blue-left").fadeOut("slow");
			$(".blue-right").fadeOut("slow");
		});
		$(".close-green").click(function() {

			$(".green-left").fadeOut("slow");
			$(".green-right").fadeOut("slow");
		});
	});
</script>


   <c:set var="message" value='${message}'></c:set>
	<%
		String message = (String) pageContext.getAttribute("message");
	%>	
	
		
	<c:if test="${not empty message}">
			<!--  start message-red -->
		<c:if test='<%= SessionErrors.contains(renderRequest, message) %>'>
			<c:if test="${message.startsWith('error')}">
				<div id="message-red">
					<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="red-left"><liferay-ui:error key="${message}" message="${message}"></liferay-ui:error>
						<%-- 	<c:out value="${message}" /> --%>
							</td>
							<td class="red-right"><a class="close-red"><img
									src="<%= themeDisplay.getPathThemeImages() %>/table/icon_close_red.gif"
									alt="" /> </a></td>
						</tr>
					</table>
				</div>
			</c:if>
		</c:if>
		<c:if test='<%= SessionMessages.contains(renderRequest, message) %>'>		
			<c:if test="${message.startsWith('success')}">
				<div id="message-green">
					<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td class="green-left"><liferay-ui:success key="${message}" message="${message}"></liferay-ui:success>
							<%-- <c:out value="${message}" /> --%>
							</td>
							<td class="green-right"><a class="close-green"><img
									src="<%= themeDisplay.getPathThemeImages() %>/table/icon_close_green.gif"
									alt="" /> </a></td>
						</tr>
					</table>
				</div>
			</c:if>
		</c:if>
	</c:if>
