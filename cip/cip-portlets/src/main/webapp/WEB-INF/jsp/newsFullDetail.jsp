<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="java.io.UnsupportedEncodingException"%>
<%@page import="java.net.URLDecoder"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.coach.cip.util.LoggerUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.coach.cip.portlets.controller.NewsDetailController"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.coach.cip.util.WebUtil"%>
<%@page import="java.util.regex.Matcher"%>
<%@page import="java.util.regex.Pattern"%>
<%@page import="com.liferay.portal.kernel.xml.Element"%>
<%@page import="javax.portlet.PortletRequestDispatcher"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="com.liferay.portal.kernel.xml.Document"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.liferay.portal.kernel.xml.Node"%>
<%@page import="com.liferay.portal.kernel.xml.DocumentException"%>
<%@page import="com.coach.cip.util.Constants"%>
<%@page import="com.liferay.portal.kernel.xml.SAXReaderUtil"%>
<%@page
	import="com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil"%>
<%@page import="com.liferay.portlet.asset.model.AssetCategory"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticleDisplay"%>
<%@page
	import="com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil"%>
<%@page import="org.apache.velocity.runtime.directive.Foreach"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticle"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<script>

$(document).ready(function($){	
	$("#viewURL").click(function() {
				var fileURL = $(location).attr('href');
				if(fileURL != null && fileURL.length > 0){
					var url = "/group"+fileURL.split("/group")[1];
		    		var d=document.getElementById("assetURLDiv");
		    		if(d.innerHTML){$(d).toggle();}
		       	    d.innerHTML ="";
		   		    d.appendChild(document.createTextNode('URL:'));
		   			var newInput = document.createElement('input');
		   			newInput.type = 'text';
		   			
		   			newInput.readOnly=true;
		   			
		   			newInput.className = 'assetURlDivCSS';
		   			newInput.value = url;
		   			
		   			d.appendChild(newInput);
		   	    	$(d).find('input:text').focus();
		   	    	$(d).find('input:text').select();
		   			}
				
		});

});
</script>

<%
	final Log loggerJsp = LogFactoryUtil.getLog(NewsDetailController.class);
	String homeRedirect = ParamUtil.getString(request, "homeRedirect");
	String redirect = PortalUtil.getCurrentURL(renderRequest);
%>

<portlet:actionURL var="getNewsAttachment">
	<portlet:param name="action" value="getNewsAttachment" />
	<portlet:param name="documentPath" value="${docUrl}" />
	<portlet:param name="redirect" value="<%=redirect %>" />
</portlet:actionURL>
<c:set var="journalArticleId" value="${journalArticleId}" />
<c:set var="displayDateStr" value="${displayDateStr}" />
<%
	String imageUrl = null;
	String content = null;
	List<Node> docNodesList = null;
	List<String> docUrList = new ArrayList<String>();
	JournalArticleDisplay journalArticleDisplay = null;
	JournalArticle journalArticle = null;
	AssetCategory articleCat = null;
	boolean onlyDocAttachment = false;
	String displayDateStr = (String) pageContext.getAttribute("displayDateStr");
	String journalArticleId = (String) pageContext.getAttribute("journalArticleId");
	boolean templateBasedArticle = false;
	if (Validator.isNotNull(journalArticleId)) {
		journalArticle = JournalArticleLocalServiceUtil.getArticle(Long.valueOf(journalArticleId));
	}
	if (Validator.isNotNull(journalArticle)) {
		List<AssetCategory> catList =
			AssetCategoryLocalServiceUtil.getCategories(JournalArticle.class.getName(), journalArticle.getResourcePrimKey());
		if (catList != null && catList.size() != 0)
			articleCat = catList.get(0);
		if (Validator.isNotNull(journalArticle.getTemplateId())) {
			try {
				Document document = SAXReaderUtil.read(journalArticle.getContent());
				if (document != null) {
					String xpathExpression = "";
					xpathExpression = Constants.DYNAMIC_ELEMENT_NAME + Constants.NEWS_IMAGE_URL + Constants.DYNAMIC_CONTENT;
					if (document.selectSingleNode(xpathExpression) != null)
						imageUrl = document.selectSingleNode(xpathExpression).getText();
					//multi lingual start
					List<Node> contentNodeList = null;
					contentNodeList = (document.selectNodes(Constants.DYNAMIC_ELEMENT_NAME + "content" + "']"));
					for (Node contentNode : contentNodeList) {
						if (contentNode.selectNodes(Constants.NEWS_DC) != null) {
							List<Node> innerContentNodeList = contentNode.selectNodes(Constants.NEWS_DC);
							for (Node innerContent : innerContentNodeList) {
								String xmlStr = innerContent.asXML();
								Pattern pattern = Pattern.compile("language-id=\"(.*?)\"");
								Matcher matcher = pattern.matcher(xmlStr);
								if (matcher.find()) {
									String localString = matcher.group(1);
									if (locale.toString().equalsIgnoreCase(localString)) {
										content = innerContent.getText();
										break;
									}
								}
								//no multilingual content 
								else {
									content = innerContent.getText();;
									break;
								}
							}
						}
					}
					//end
					docNodesList = (document.selectNodes(Constants.DYNAMIC_ELEMENT_NAME + Constants.NEWS_DOCS + "']"));
					if (Validator.isNull(imageUrl) && Validator.isNull(content)) {
						onlyDocAttachment = true;
					}
					for (Node doc : docNodesList) {
						if (doc.selectNodes(Constants.NEWS_DC) != null) {
							List<Node> innerDocNodeList = doc.selectNodes(Constants.NEWS_DC);
							for (Node innerDoc : innerDocNodeList) {
								String xmlStr = innerDoc.asXML();
								Pattern pattern = Pattern.compile("language-id=\"(.*?)\"");
								Matcher matcher = pattern.matcher(xmlStr);
								if (matcher.find()) {
									String localString = matcher.group(1);
									if (locale.toString().equalsIgnoreCase(localString)) {
										docUrList.add(innerDoc.getText());
										break;
									}
								}
								//no multilingual doc urls
								else {
									if (Validator.isNotNull(doc.selectSingleNode(Constants.NEWS_DC).getText())) {
										docUrList.add(doc.selectSingleNode(Constants.NEWS_DC).getText());
									}
									break;
								}
							}
						}
					}
				}
				templateBasedArticle = true;
				if (onlyDocAttachment && !(Validator.isNull(docUrList) || docUrList.size() == 0)) {
					PortletURL actUrl = renderResponse.createActionURL();
					actUrl.setParameter("action", getNewsAttachment);
					actUrl.setParameter("documentPath", getNewsAttachment);
					actUrl.setParameter("redirect", redirect);
				}
			}
			catch (DocumentException e) {
				LoggerUtil.errorLogger(loggerJsp, "Exception occured in newsFullDetails.jsp ", e);
			}
		}
		else {
			try {
				journalArticleDisplay =
					JournalArticleLocalServiceUtil.getArticleDisplay(
						journalArticle, journalArticle.getTemplateId(), null, themeDisplay.getLanguageId(), 1, null, themeDisplay);
				templateBasedArticle = false;
			}
			catch (Exception e) {
			}
		}
	}
%>
<%
	String portletTitle = PortalUtil.getPortletTitle(renderResponse);
%>
<div>
	<p>
		<span style="color: #EB3528">
			<%
				displayDateStr = (null != displayDateStr) ? displayDateStr.toUpperCase(locale) : displayDateStr;
				String articleTitle = articleCat.getTitle(locale);
				articleTitle = (null != articleTitle) ? articleTitle.toUpperCase(locale) : articleTitle;
			%>
			<c:out value="<%=displayDateStr+\" | \"+articleTitle  %>"></c:out>
		</span>
	</p>
</div>
<%
	if (templateBasedArticle) {
%>
<c:set var="vocabName" value="${vocabName}"></c:set>
<%
/*if current vocabulary is either of "EXECUTIVE CORNER" or "VISION AND VALUE" then image should not be displayed in detail section of news*/
boolean imageDisplay = true;
String vocabName = (String)pageContext.getAttribute("vocabName");
if(vocabName.equalsIgnoreCase(Constants.EXECUTIVE_CORNER) || vocabName.equalsIgnoreCase(Constants.VISION_AND_VALUES)){
	imageDisplay = false;
}
%>
<div style="min-height: 175px;">
	<table>
		<tr>
			<%
				if (imageDisplay && Validator.isNotNull(imageUrl)) {
				
			%>
			<td><img alt="image" src="<%=imageUrl%>"
				style="width: 105px; height: 140px; float: left; padding-right: 5px;" />
			</td>
			<%
				}
			%>
			<td><span
				style="font-family: 'myriad-web-pro'; font-size: 12px;"> <%
 	if (content != null && content.length() != 0)
 			out.println(content);
 %> </span>
			</td>
		</tr>
	</table>
</div>
<div>
	<c:forEach var="docUrl" items="<%=docUrList %>">
		<c:set var="docUrl" value="${docUrl}"></c:set>
		<%
			String docUrl = (String) pageContext.getAttribute("docUrl");
					String docName = "";
					try {
						//docUrl is encoded by default by liferay while selecting the doc in web content, so decoding it to get the real value of doc url
						docUrl = URLDecoder.decode(docUrl, Constants.CHARACTER_ENCODING);
						String[] docUrlTokenArr = docUrl.split(StringPool.FORWARD_SLASH);
						if(Validator.isNotNull(docUrlTokenArr) && docUrlTokenArr.length > 1 ){
							//taking the name of dcoument
							docName = docUrlTokenArr[docUrlTokenArr.length - 2];
						}
					}
					catch (UnsupportedEncodingException e) {
						LoggerUtil.errorLogger(loggerJsp, "UTF-8 Decoding is not supported by "+docUrl, e);
						//Assigning web content title to docName if document name could not be retrieved above 
						docName = journalArticle.getTitle(locale);
					}
		%>
		<p>
			<span
				style="font-family: 'myriad-web-pro'; font-size: 12px; font-weight: bold">
				<a
				href="<portlet:actionURL>
									 <portlet:param name="action" value="getNewsAttachment" />
									 <portlet:param name="documentPath" value="${docUrl}" />
									 <portlet:param name="redirect" value="<%=redirect %>"/>
									 <portlet:param name="portletTitle" value="<%=portletTitle %>"/>
									 <portlet:param name="journalArticleId" value="<%=journalArticleId %>"/>
									 </portlet:actionURL>">
					<%=docName%> </a> </span>
		</p>
	</c:forEach>
</div>
<br></br>
<%
	}
	else {
%>
<c:set var="journalArticleDisplay" value="<%=journalArticleDisplay%>"></c:set>
<c:if test="${journalArticleDisplay ne null}">
	<table>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td style="padding: 1px;"><%=journalArticleDisplay.getContent()%></td>
		</tr>

		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>
</c:if>
<%
	}
%>
<div style="display:inline;">
<span style="float:left;margin-bottom:-5px;">

		<input type="button" id="fullDetailsBack"
			value="<spring:message code='label.back.button'/>"
			class="submitClass" onClick="history.go(-1);" />
        </span>


<%
if(WebUtil.hasWriterRole(renderRequest))
{%>
	<input type="image" src = "<%=request.getContextPath() %>/images/internet_url.jpg" value="Get URL" id="viewURL"/>
	
<% } %>
<span id="assetURLDiv" ></span>
</div>