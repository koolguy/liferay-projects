<%@page import="com.liferay.portal.service.RoleLocalServiceUtil"%>
<%@ include file="init.jsp"%>

<link rel="stylesheet" type="text/css" href="<%=themeDisplay.getPathThemeCss()%>/sidenav.css" />
<link rel="stylesheet" type="text/css" href="<%=themeDisplay.getPathThemeCss()%>/slidingpanel.css" />
<script type='text/javascript' src='<%=themeDisplay.getPathThemeCss()%>/../js/jquery.cookie.js'></script>
<script type='text/javascript' src='<%=themeDisplay.getPathThemeCss()%>/../js/jquery.hoverIntent.minified.js'></script>
<script type='text/javascript' src='<%=themeDisplay.getPathThemeCss()%>/../js/jquery.dcjqaccordion.2.7.min.js'></script>

<script type="text/javascript">
/*!
* jQuery blockUI plugin
*/

;(function() {

                function setup($) {
                                if (/1\.(0|1|2)\.(0|1|2)/.test($.fn.jquery) || /^1.1/.test($.fn.jquery)) {
                                                alert('blockUI requires jQuery v1.2.3 or later!  You are using v' + $.fn.jquery);
                                                return;
                                }

                                $.fn._fadeIn = $.fn.fadeIn;

                                var noOp = function() {};

                                // this bit is to ensure we don't call setExpression when we shouldn't (with extra muscle to handle
                                // retarded userAgent strings on Vista)
                                var mode = document.documentMode || 0;
                                var setExpr = $.browser.msie && (($.browser.version < 8 && !mode) || mode < 8);
                                var ie6 = $.browser.msie && /MSIE 6.0/.test(navigator.userAgent) && !mode;

                                // global $ methods for blocking/unblocking the entire page
                                $.blockUI   = function(opts) { install(window, opts); };
                                $.unblockUI = function(opts) { remove(window, opts); };

                                // convenience method for quick growl-like notifications  
                                $.growlUI = function(title, message, timeout, onClose) {
                                                var $m = $('<div class="growlUI"></div>');
                                                if (title) $m.append('<h1>'+title+'</h1>');
                                                if (message) $m.append('<h2>'+message+'</h2>');
                                                if (timeout == undefined) timeout = 3000;
                                                $.blockUI({
                                                                message: $m, fadeIn: 700, fadeOut: 1000, centerY: false,
                                                                timeout: timeout, showOverlay: false,
                                                                onUnblock: onClose,
                                                                css: $.blockUI.defaults.growlCSS
                                                });
                                };

                                // plugin method for blocking element content
                                $.fn.block = function(opts) {
                                                var fullOpts = $.extend({}, $.blockUI.defaults, opts || {});
                                                this.each(function() {
                                                                var $el = $(this);
                                                                if (fullOpts.ignoreIfBlocked && $el.data('blockUI.isBlocked'))
                                                                                return;
                                                                $el.unblock({ fadeOut: 0 });
                                                });

                                                return this.each(function() {
                                                                if ($.css(this,'position') == 'static')
                                                                                this.style.position = 'relative';
                                                                if ($.browser.msie)
                                                                                this.style.zoom = 1; // force 'hasLayout'
                                                                install(this, opts);
                                                });
                                };

                                // plugin method for unblocking element content
                                $.fn.unblock = function(opts) {
                                                return this.each(function() {
                                                                remove(this, opts);
                                                });
                                };

                                $.blockUI.version = 2.42; 

                                // override these in your code to change the default behavior and style
                                $.blockUI.defaults = {
                                                // message displayed when blocking (use null for no message)
                                                message:  '',

                                                title: null,               // title string; only used when theme == true
                                                draggable: true,  // only used when theme == true (requires jquery-ui.js to be loaded)

                                                theme: false, // set to true to use with jQuery UI themes

                                                // styles for the message when blocking; if you wish to disable
                                                // these and use an external stylesheet then do this in your code:
                                                // $.blockUI.defaults.css = {};
                                                css: {
                                                                padding:              0,
                                                                margin:                 0,
                                                                width:                   '30%',
                                                                top:                        '40%',
                                                                left:                        '35%',
                                                                textAlign:            'center',
                                                                color:                     '#000',
                                                                border:                 '3px solid #aaa',
                                                                backgroundColor:'#fff',
                                                                cursor:                  'wait'
                                                },

                                                // minimal style set used when themes are used
                                                themedCSS: {
                                                                width:   '30%',
                                                                top:        '40%',
                                                                left:        '35%'
                                                },

                                                // styles for the overlay
                                                overlayCSS:  {
                                                                backgroundColor: '#000',
                                                                opacity:                                 0.6,
                                                                cursor:                                   'wait'
                                                },

                                                // styles applied when using $.growlUI
                                                growlCSS: {
                                                                width:  '350px',
                                                                top:                        '10px',
                                                                left:        '',
                                                                right:     '10px',
                                                                border:                 'none',
                                                                padding:              '5px',
                                                                opacity:                0.6,
                                                                cursor: 'default',
                                                                color:                     '#fff',
                                                                backgroundColor: '#000',
                                                                '-webkit-border-radius': '10px',
                                                                '-moz-border-radius':     '10px',
                                                                'border-radius':                                  '10px'
                                                },

                                                // IE issues: 'about:blank' fails on HTTPS and javascript:false is s-l-o-w
                                                // (hat tip to Jorge H. N. de Vasconcelos)
                                                iframeSrc: /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank',

                                                // force usage of iframe in non-IE browsers (handy for blocking applets)
                                                forceIframe: false,

                                                // z-index for the blocking overlay
                                                baseZ: 1000,

                                                // set these to true to have the message automatically centered
                                                centerX: true, // <-- only effects element blocking (page block controlled via css above)
                                                centerY: true,

                                                // allow body element to be stetched in ie6; this makes blocking look better
                                                // on "short" pages.  disable if you wish to prevent changes to the body height
                                                allowBodyStretch: true,

                                                // enable if you want key and mouse events to be disabled for content that is blocked
                                                bindEvents: true,

                                                // be default blockUI will supress tab navigation from leaving blocking content
                                                // (if bindEvents is true)
                                                constrainTabKey: true,

                                                // fadeIn time in millis; set to 0 to disable fadeIn on block
                                                fadeIn:  200,

                                                // fadeOut time in millis; set to 0 to disable fadeOut on unblock
                                                fadeOut:  400,

                                                // time in millis to wait before auto-unblocking; set to 0 to disable auto-unblock
                                                timeout: 0,

                                                // disable if you don't want to show the overlay
                                                showOverlay: true,

                                                // if true, focus will be placed in the first available input field when
                                                // page blocking
                                                focusInput: true,

                                                // suppresses the use of overlay styles on FF/Linux (due to performance issues with opacity)
                                                applyPlatformOpacityRules: true,

                                                // callback method invoked when fadeIn has completed and blocking message is visible
                                                onBlock: null,

                                                // callback method invoked when unblocking has completed; the callback is
                                                // passed the element that has been unblocked (which is the window object for page
                                                // blocks) and the options that were passed to the unblock call:
                                                //            onUnblock(element, options)
                                                onUnblock: null,

                                                // don't ask; if you really must know: http://groups.google.com/group/jquery-en/browse_thread/thread/36640a8730503595/2f6a79a77a78e493#2f6a79a77a78e493
                                                quirksmodeOffsetHack: 4,

                                                // class name of the message block
                                                blockMsgClass: 'blockMsg',

                                                // if it is already blocked, then ignore it (don't unblock and reblock)
                                                ignoreIfBlocked: false
                                };

                                // private data and functions follow...

                                var pageBlock = null;
                                var pageBlockEls = [];

                                function install(el, opts) {
                                                var css, themedCSS;
                                                var full = (el == window);
                                                var msg = (opts && opts.message !== undefined ? opts.message : undefined);
                                                opts = $.extend({}, $.blockUI.defaults, opts || {});

                                                if (opts.ignoreIfBlocked && $(el).data('blockUI.isBlocked'))
                                                                return;

                                                opts.overlayCSS = $.extend({}, $.blockUI.defaults.overlayCSS, opts.overlayCSS || {});
                                                css = $.extend({}, $.blockUI.defaults.css, opts.css || {});
                                                themedCSS = $.extend({}, $.blockUI.defaults.themedCSS, opts.themedCSS || {});
                                                msg = msg === undefined ? opts.message : msg;

                                                // remove the current block (if there is one)
                                                if (full && pageBlock)
                                                                remove(window, {fadeOut:0});

                                                // if an existing element is being used as the blocking content then we capture
                                                // its current place in the DOM (and current display style) so we can restore
                                                // it when we unblock
                                                if (msg && typeof msg != 'string' && (msg.parentNode || msg.jquery)) {
                                                                var node = msg.jquery ? msg[0] : msg;
                                                                var data = {};
                                                                $(el).data('blockUI.history', data);
                                                                data.el = node;
                                                                data.parent = node.parentNode;
                                                                data.display = node.style.display;
                                                                data.position = node.style.position;
                                                                if (data.parent)
                                                                                data.parent.removeChild(node);
                                                }

                                                $(el).data('blockUI.onUnblock', opts.onUnblock);
                                                var z = opts.baseZ;

                                                // blockUI uses 3 layers for blocking, for simplicity they are all used on every platform;
                                                // layer1 is the iframe layer which is used to supress bleed through of underlying content
                                                // layer2 is the overlay layer which has opacity and a wait cursor (by default)
                                                // layer3 is the message content that is displayed while blocking

                                                var lyr1 = ($.browser.msie || opts.forceIframe)
                                                                ? $('<iframe class="blockUI" style="z-index:'+ (z++) +';display:none;border:none;margin:0;padding:0;position:absolute;width:100%;height:100%;top:0;left:0" src="'+opts.iframeSrc+'"></iframe>')
                                                                : $('<div class="blockUI" style="display:none"></div>');

                                                var lyr2 = opts.theme
                                                                ? $('<div class="blockUI blockOverlay ui-widget-overlay" style="z-index:'+ (z++) +';display:none"></div>')
                                                                : $('<div class="blockUI blockOverlay" style="z-index:'+ (z++) +';display:none;border:none;margin:0;padding:0;width:100%;height:100%;top:0;left:0"></div>');

                                                var lyr3, s;
                                                if (opts.theme && full) {
                                                                s = '<div class="blockUI ' + opts.blockMsgClass + ' blockPage ui-dialog ui-widget ui-corner-all" style="z-index:'+(z+10)+';display:none;position:fixed">' +
                                                                                                '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">'+(opts.title || '&nbsp;')+'</div>' +
                                                                                                '<div class="ui-widget-content ui-dialog-content"></div>' +
                                                                                '</div>';
                                                }
                                                else if (opts.theme) {
                                                                s = '<div class="blockUI ' + opts.blockMsgClass + ' blockElement ui-dialog ui-widget ui-corner-all" style="z-index:'+(z+10)+';display:none;position:absolute">' +
                                                                                                '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">'+(opts.title || '&nbsp;')+'</div>' +
                                                                                                '<div class="ui-widget-content ui-dialog-content"></div>' +
                                                                                '</div>';
                                                }
                                                else if (full) {
                                                                s = '<div class="blockUI ' + opts.blockMsgClass + ' blockPage" style="z-index:'+(z+10)+';display:none;position:fixed"></div>';
                                                }
                                                else {
                                                                s = '<div class="blockUI ' + opts.blockMsgClass + ' blockElement" style="z-index:'+(z+10)+';display:none;position:absolute"></div>';
                                                }
                                                lyr3 = $(s);

                                                // if we have a message, style it
                                                if (msg) {
                                                                if (opts.theme) {
                                                                                lyr3.css(themedCSS);
                                                                                lyr3.addClass('ui-widget-content');
                                                                }
                                                                else
                                                                                lyr3.css(css);
                                                }

                                                // style the overlay
                                                if (!opts.theme && (!opts.applyPlatformOpacityRules || !($.browser.mozilla && /Linux/.test(navigator.platform))))
                                                                lyr2.css(opts.overlayCSS);
                                                lyr2.css('position', full ? 'fixed' : 'absolute');

                                                // make iframe layer transparent in IE
                                                if ($.browser.msie || opts.forceIframe)
                                                                lyr1.css('opacity',0.0);

                                                //$([lyr1[0],lyr2[0],lyr3[0]]).appendTo(full ? 'body' : el);
                                                var layers = [lyr1,lyr2,lyr3], $par = full ? $('body') : $(el);
                                                $.each(layers, function() {
                                                                this.appendTo($par);
                                                });

                                                if (opts.theme && opts.draggable && $.fn.draggable) {
                                                                lyr3.draggable({
                                                                                handle: '.ui-dialog-titlebar',
                                                                                cancel: 'li'
                                                                });
                                                }

                                                // ie7 must use absolute positioning in quirks mode and to account for activex issues (when scrolling)
                                                var expr = setExpr && (!$.boxModel || $('object,embed', full ? null : el).length > 0);
                                                if (ie6 || expr) {
                                                                // give body 100% height
                                                                if (full && opts.allowBodyStretch && $.boxModel)
                                                                                $('html,body').css('height','100%');

                                                                // fix ie6 issue when blocked element has a border width
                                                                if ((ie6 || !$.boxModel) && !full) {
                                                                                var t = sz(el,'borderTopWidth'), l = sz(el,'borderLeftWidth');
                                                                                var fixT = t ? '(0 - '+t+')' : 0;
                                                                                var fixL = l ? '(0 - '+l+')' : 0;
                                                                }

                                                                // simulate fixed position
                                                                $.each([lyr1,lyr2,lyr3], function(i,o) {
                                                                                var s = o[0].style;
                                                                                s.position = 'absolute';
                                                                                if (i < 2) {
                                                                                                full ? s.setExpression('height','Math.max(document.body.scrollHeight, document.body.offsetHeight) - (jQuery.boxModel?0:'+opts.quirksmodeOffsetHack+') + "px"')
                                                                                                                : s.setExpression('height','this.parentNode.offsetHeight + "px"');
                                                                                                full ? s.setExpression('width','jQuery.boxModel && document.documentElement.clientWidth || document.body.clientWidth + "px"')
                                                                                                                : s.setExpression('width','this.parentNode.offsetWidth + "px"');
                                                                                                if (fixL) s.setExpression('left', fixL);
                                                                                                if (fixT) s.setExpression('top', fixT);
                                                                                }
                                                                                else if (opts.centerY) {
                                                                                                if (full) s.setExpression('top','(document.documentElement.clientHeight || document.body.clientHeight) / 2 - (this.offsetHeight / 2) + (blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "px"');
                                                                                                s.marginTop = 0;
                                                                                }
                                                                                else if (!opts.centerY && full) {
                                                                                                var top = (opts.css && opts.css.top) ? parseInt(opts.css.top) : 0;
                                                                                                var expression = '((document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + '+top+') + "px"';
                                                                                                s.setExpression('top',expression);
                                                                                }
                                                                });
                                                }

                                                // show the message
                                                if (msg) {
                                                                if (opts.theme)
                                                                                lyr3.find('.ui-widget-content').append(msg);
                                                                else
                                                                                lyr3.append(msg);
                                                                if (msg.jquery || msg.nodeType)
                                                                                $(msg).show();
                                                }

                                                if (($.browser.msie || opts.forceIframe) && opts.showOverlay)
                                                                lyr1.show(); // opacity is zero
                                                if (opts.fadeIn) {
                                                                var cb = opts.onBlock ? opts.onBlock : noOp;
                                                                var cb1 = (opts.showOverlay && !msg) ? cb : noOp;
                                                                var cb2 = msg ? cb : noOp;
                                                                if (opts.showOverlay)
                                                                                lyr2._fadeIn(opts.fadeIn, cb1);
                                                                if (msg)
                                                                                lyr3._fadeIn(opts.fadeIn, cb2);
                                                }
                                                else {
                                                                if (opts.showOverlay)
                                                                                lyr2.show();
                                                                if (msg)
                                                                                lyr3.show();
                                                                if (opts.onBlock)
                                                                                opts.onBlock();
                                                }

                                                // bind key and mouse events
                                                bind(1, el, opts);

                                                if (full) {
                                                                pageBlock = lyr3[0];
                                                                pageBlockEls = $(':input:enabled:visible',pageBlock);
                                                                if (opts.focusInput)
                                                                                setTimeout(focus, 20);
                                                }
                                                else
                                                                center(lyr3[0], opts.centerX, opts.centerY);

                                                if (opts.timeout) {
                                                                // auto-unblock
                                                                var to = setTimeout(function() {
                                                                                full ? $.unblockUI(opts) : $(el).unblock(opts);
                                                                }, opts.timeout);
                                                                $(el).data('blockUI.timeout', to);
                                                }
                                };

                                // remove the block
                                function remove(el, opts) {
                                                var full = (el == window);
                                                var $el = $(el);
                                                var data = $el.data('blockUI.history');
                                                var to = $el.data('blockUI.timeout');
                                                if (to) {
                                                                clearTimeout(to);
                                                                $el.removeData('blockUI.timeout');
                                                }
                                                opts = $.extend({}, $.blockUI.defaults, opts || {});
                                                bind(0, el, opts); // unbind events

                                                if (opts.onUnblock === null) {
                                                                opts.onUnblock = $el.data('blockUI.onUnblock');
                                                                $el.removeData('blockUI.onUnblock');
                                                }

                                                var els;
                                                if (full) // crazy selector to handle odd field errors in ie6/7
                                                                els = $('body').children().filter('.blockUI').add('body > .blockUI');
                                                else
                                                                els = $('.blockUI', el);

                                                if (full)
                                                                pageBlock = pageBlockEls = null;

                                                if (opts.fadeOut) {
                                                                els.fadeOut(opts.fadeOut);
                                                                setTimeout(function() { reset(els,data,opts,el); }, opts.fadeOut);
                                                }
                                                else
                                                                reset(els, data, opts, el);
                                };

                                // move blocking element back into the DOM where it started
                                function reset(els,data,opts,el) {
                                                els.each(function(i,o) {
                                                                // remove via DOM calls so we don't lose event handlers
                                                                if (this.parentNode)
                                                                                this.parentNode.removeChild(this);
                                                });

                                                if (data && data.el) {
                                                                data.el.style.display = data.display;
                                                                data.el.style.position = data.position;
                                                                if (data.parent)
                                                                                data.parent.appendChild(data.el);
                                                                $(el).removeData('blockUI.history');
                                                }

                                                if (typeof opts.onUnblock == 'function')
                                                                opts.onUnblock(el,opts);
                                };

                                // bind/unbind the handler
                                function bind(b, el, opts) {
                                                var full = el == window, $el = $(el);

                                                // don't bother unbinding if there is nothing to unbind
                                                if (!b && (full && !pageBlock || !full && !$el.data('blockUI.isBlocked')))
                                                                return;

                                                $el.data('blockUI.isBlocked', b);

                                                // don't bind events when overlay is not in use or if bindEvents is false
                                                if (!opts.bindEvents || (b && !opts.showOverlay))
                                                                return;

                                                // bind anchors and inputs for mouse and key events
                                                var events = 'mousedown mouseup keydown keypress';
                                                b ? $(document).bind(events, opts, handler) : $(document).unbind(events, handler);

                                // former impl...
                                //               var $e = $('a,:input');
                                //               b ? $e.bind(events, opts, handler) : $e.unbind(events, handler);
                                };

                                // event handler to suppress keyboard/mouse events when blocking
                                function handler(e) {
                                                // allow tab navigation (conditionally)
                                                if (e.keyCode && e.keyCode == 9) {
                                                                if (pageBlock && e.data.constrainTabKey) {
                                                                                var els = pageBlockEls;
                                                                                var fwd = !e.shiftKey && e.target === els[els.length-1];
                                                                                var back = e.shiftKey && e.target === els[0];
                                                                                if (fwd || back) {
                                                                                                setTimeout(function(){focus(back)},10);
                                                                                                return false;
                                                                                }
                                                                }
                                                }
                                                var opts = e.data;
                                                // allow events within the message content
                                                if ($(e.target).parents('div.' + opts.blockMsgClass).length > 0)
                                                                return true;

                                                // allow events for content that is not being blocked
                                                return $(e.target).parents().children().filter('div.blockUI').length == 0;
                                };

                                function focus(back) {
                                                if (!pageBlockEls)
                                                                return;
                                                var e = pageBlockEls[back===true ? pageBlockEls.length-1 : 0];
                                                if (e)
                                                                e.focus();
                                };

                                function center(el, x, y) {
                                                var p = el.parentNode, s = el.style;
                                                var l = ((p.offsetWidth - el.offsetWidth)/2) - sz(p,'borderLeftWidth');
                                                var t = ((p.offsetHeight - el.offsetHeight)/2) - sz(p,'borderTopWidth');
                                                if (x) s.left = l > 0 ? (l+'px') : '0';
                                                if (y) s.top  = t > 0 ? (t+'px') : '0';
                                };

                                function sz(el, p) {
                                                return parseInt($.css(el,p))||0;
                                };

                };


                if (typeof define === 'function' && define.amd && define.amd.jQuery) {
                                define(['jquery'], setup);
                } else {
                                setup(jQuery);
                }

})();
$(document).ready(function($){
	
$(".portlet-layout").find("#column-1").find("h1:first").append('<input  type="image" src="<%=themeDisplay.getPathThemeImages()%>/leftarrow.png"  id="collapseButton" title="Hide" style="float:right;margin-right:14px;"/>')
                                
                                $('.bodyBlock').remove();
				                $('#accordion-2').dcAccordion({
				                                eventType: 'click',
				                                autoClose: true,
				                                saveState: false,
				                                disableLink: false,
				                                speed: 'fast',
				                                classActive: 'test',
				                                showCount: false,
				                                autoExpand: true,
				                                classExpand : 'dcjq-current-parent'
				                 });

		                /* var url=document.URL.split("%23")[1];
		                if(url=="true"){
		             		   $(".portlet-boundary_PDFPortlet_WAR_cipportlet_").show();
		                }
		                else{
		              		  $(".portlet-boundary_56_").show();
		                } 

		
		   				$('#accordion-2 a').click(function(){
		            		
		                	if(!   (($.trim($(this).attr('href'))=="") || ($.trim($(this).attr('href'))=="undefind") || ($.trim($(this).attr('target'))=="_blank"))  ){
		                		  
		                          $.blockUI.defaults.overlayCSS = {backgroundColor: ''}; 
		                        $.blockUI({ css: { background: 'transperent', color: 'none',width:'0px',height:'0px' } });
		                	}
		                        }); 
                        */

						$("#collapseButton").click(function(){

							var $columnOne=$(this).parents(".portlet-layout").find("#column-1");
						    var $columnTwo=$(this).parents(".portlet-layout").find("#column-2");

                       	    $($columnOne).hide();
                            var totalWidth=$($columnTwo).width()+$($columnOne).width()+"px";
								
							$($columnTwo).css({"width":$.trim(totalWidth)});
							
							//$(this).parents(".portlet-layout").find("#column-2").css({"width":$("#wrapper").css("width")+"px"});
							$($columnOne).parent().prepend('<input  type="image" src="<%=themeDisplay.getPathThemeImages()%>/rightarrow.png"   id="expndButton" title="Show"  alt="image not loading"/>');
											
						});
						
						$("#expndButton").live("click",function(){
                           
							var $columnOne=$(this).parents(".portlet-layout").find("#column-1");
						    var $columnTwo=$(this).parents(".portlet-layout").find("#column-2");
	
                           var totalWidth=(0.7 * $($columnTwo).width())+"px";
							$($columnTwo).css({"width":$.trim(totalWidth)});
						   $($columnOne).show();
						   $(this).remove();	
						});
});

</script>

<script>

$(document).ready(function($){
	
	$("#sideNavigationDiv").find("a").click(function(event) {
		$('a').each(function(){							 						
		  	$(this).removeClass('test');						 
		});
	    $(this).addClass('test');
	    
	    var currentURL = $(location).attr('href');
	    currentURL = currentURL.split("#")[0];
	    currentURL = currentURL.split("?")[0];
	    
		var value = $(this).attr("href");
		var target = $(this).attr("target");
		var name = $(this).attr("name");
		if(target != null && target.search('_blank') >= 0)
		{
			Liferay.fire('sideNavValue', name);
		}
	 	if(value != null && value.search(currentURL) >= 0)
		{
			Liferay.fire('sideNavValue', value);
			Liferay.fire('QandA', $.trim(value.split("::")[1]));
			return false;
		} 
	 	if(value == null || value =="")
	 	{
	 		return false;
	 		
	 	}
	});  
});
</script>

<style>

.test{
color:#000 !important;
font-weight: bold;
}

.grey .accordion a{

cursor:pointer;

}

</style>

	<%
		/* 	Level 1 : MY COACH EXPERIENCE
		Level 2 : MY CAREER , MY PAY, MY TOOLS, etc under MY COACH EXPERIENCE.
		Level 3 : HR ONLINE, EAP.COM, POLICIES, FORMS, etc under MY TOOLS.
		Level 4 : DOCUMENTS OR COMMUNICATION, EMPLOYMENT, FINANCIAL , etc under POLICIES. 
		Level 5 : DOCUMENTS OR PUBLIC COMMUNICATION POLICY, SOCIAL COMMUNICATION POLICY, etc under COMMUNICATION. */
		long entryTime = System.currentTimeMillis(); 
		List<Layout> parentNavs = layout.getAncestors();
		List<Layout> childNavs = null;
		List<Layout> subChildNavs = null;
		String currentPage = layout.getName(locale);
		String currentParentPage = null;
		int counter = 0;
		renderResponse.setTitle(currentPage);
		HttpServletRequest servletRequest = WebUtil.getHttpServletRequest(renderRequest);
		String pdfPortletNameSpace = StringPool.UNDERLINE + WebUtil.getPortletInstanceId(renderRequest, "column-2")+StringPool.UNDERLINE;
		pdfPortletNameSpace = pdfPortletNameSpace.replaceAll(",","");
		//Added
		Locale defaultLocale = LocaleUtil.getDefault();
		
		if(parentNavs.size() == 2)
		{
			counter=1;
			renderResponse.setTitle(parentNavs.get(1).getName(locale));
			currentParentPage = parentNavs.get(0).getName(locale);
		}else if (parentNavs.size() == 1) {
			renderResponse.setTitle(parentNavs.get(0).getName(locale));
			counter=0;
			currentParentPage = currentPage;
		}/* else if(parentNavs.size() == 0 || Validator.isNull(parentNavs)){
			parentNavs.add(layout);
			renderResponse.setTitle(currentPage);
		} */
		%>
		<c:set var="assetCategoryMap" value='${assetCategoryMap}'></c:set>
		<c:set var="assetEntriesMap" value='${assetEntriesMap}'></c:set>
		<c:set var="fileName" value='${fileName}'></c:set>
		<c:set var="assetCategoryEntryMap" value='${assetCategoryEntryMap}'></c:set>
		<%
		Map<Long,List<AssetCategory>> assetCategoryMap = (Map<Long,List<AssetCategory>> )pageContext.getAttribute("assetCategoryMap");
		Map<Long,Set<AssetEntry>> assetEntriesMap = (Map<Long,Set<AssetEntry>> )pageContext.getAttribute("assetEntriesMap");
		Map<String, Set<AssetEntry>> assetCategoryEntryMap = (Map<String, Set<AssetEntry>>)pageContext.getAttribute("assetCategoryEntryMap");
		Layout targetLayout = null;
		String fileName = "";
		fileName = (String) pageContext.getAttribute("fileName");
		if(Validator.isNull(fileName))
		{
			/* fileName = (String)renderRequest.getAttribute("fileName"); */
			fileName = Validator.isNotNull(renderRequest.getAttribute("fileName")) ? (String)renderRequest.getAttribute("fileName") 
					 : servletRequest.getParameter(pdfPortletNameSpace+"portletTitle"); 
		}
		
		/* Coach Asia Side Navigation : Start */
		if(parentNavs.size()==0){
			if(assetCategoryMap.containsKey(new Long(0))){
				//	List<AssetCategory> level1CategoryList = assetCategoryMap.get(new Long(0));
					List<AssetCategory> level1CategoryList = WebUtil.getChildCategories(assetCategoryMap.get(new Long(0)),renderRequest);
					/* UL Main Start */
		        	out.println("<div id='sideNavigationDiv' class='grey demo-container'> <ul class='accordion'  id='accordion-2'>");
		        	for(AssetCategory level1AssetCategory : level1CategoryList){
		        		/*Level 1 Category li Start. */
		        		%>
		        		
						<c:set var="level2Parent" value='${level2Parent}'></c:set>
									    
					<%
						String level2Parent = 	currentPage;
						Map<String, Layout> layoutMap = WebUtil.getLayoutsByGroupId(themeDisplay);
						String friendlyURL = layoutMap.containsKey(level1AssetCategory.getTitle(locale)) ? layoutMap.get(level1AssetCategory.getTitle(locale)).getFriendlyURL():
								layout.getFriendlyURL();
						targetLayout = layoutMap.containsKey(level1AssetCategory.getTitle(locale)) ? layoutMap.get(level1AssetCategory.getTitle(locale)): null;
					 if (level2Parent != null && level2Parent !="" && level2Parent.equalsIgnoreCase(level1AssetCategory.getTitle(locale)))
					    {
					    	out.println("<li class='dcjq-current-parent'><a  id='level1'  href='"
					    			+ themeDisplay.getPathFriendlyURLPrivateGroup()
									+ layout.getGroup().getFriendlyURL()
									+ friendlyURL + " '>"
									+ level1AssetCategory.getTitle(locale)
									+ "</a>");
					    	
					    	
					    }else{
					    	
					    	out.println("<li><a  id='level1' href='"
									+ themeDisplay.getPathFriendlyURLPrivateGroup()
									+ layout.getGroup().getFriendlyURL()
									+ friendlyURL + " '>"
									+ level1AssetCategory.getTitle(locale)
									+ "</a>");
					    }
					 
						//List<AssetEntry> level4CategoryAssetEntryList = WebUtil.fetchAssetEntryByName(sub3Category.getName());
						/* List<AssetEntry> level1CategoryAssetEntryList = WebUtil.fetchAssetEntryByName(level1AssetCategory.getName(),assetCategoryEntryMap.get(level1AssetCategory.getCategoryId()+"-"+level1AssetCategory.getName()));
						if(level1CategoryAssetEntryList.size()>0)	{
							out.println(displayAssetEntries(level1CategoryAssetEntryList.subList(0,1),true,"level2Parent,"+level1AssetCategory.getName(),fileName, renderRequest, renderResponse, themeDisplay)); 
						}else{
							out.println("<a>"
			   						+ level1AssetCategory.getTitle(themeDisplay.getLanguageId())
			   						+ "</a>");
						} */
		        		List<AssetEntry> sub3AssetEntryList = WebUtil.fetchAssetEntries(level1AssetCategory,assetEntriesMap.get(level1AssetCategory.getCategoryId()));
		    			
					    List<AssetCategory> sub3CategoryList = WebUtil.getChildCategories(assetCategoryMap.get(level1AssetCategory.getCategoryId()),renderRequest);
						%>
						<%@include file="/WEB-INF/jsp/sideNavigationCategory.jsp"%>
						
						
						<%
						out.println("</li>");
		        		/*Level 1 Category li End. */
		        	}
		    		//coach asia admin page -- start
				      try{
			    		Layout adminLayout = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getScopeGroupId(),true,"/admin");
			    		if(LayoutPermissionUtil.contains(themeDisplay.getPermissionChecker(), adminLayout, ActionKeys.VIEW)){
			    			 out.println("<li> <a  id='level1'  href='"
			    					+ themeDisplay.getPathFriendlyURLPrivateGroup()
			    					+ themeDisplay.getScopeGroup().getFriendlyURL()
			    					+ themeDisplay.getLayout().getFriendlyURL()+"'>"
			 						+ adminLayout.getName(locale)
			 						+ "</a>");
			    					List<Layout> kids = adminLayout.getAllChildren();
			    					if(kids != null && kids.size() > 0) {
			    			 			out.println("<ul>");
			    						out.println("<li>");
			    					     //todo
			    				 	    for(Layout kid : kids){
			    				 	    	out.println("<a  id='level2'  href='"
			    			 		    			+ themeDisplay.getPathFriendlyURLPrivateGroup()
			    			 						+ kid.getGroup().getFriendlyURL()
			    			 						+ kid.getFriendlyURL() + "'>"
			    			 						+ kid.getName(locale)
			    			 						+ "</a>");
			    				 	    }
			    						out.println("</li>");
			    						out.println("</ul>");
			    					}
			    				out.println("</li>");
			    			}
			    		}catch(Exception e){
			    				System.out.println("Error while rendering admin page :" + e.getMessage());
			    		}
			    		//coach asia admin page -- End
				        	out.println("</ul></div>");
				        	/* UL Main End */
		        	
				}
			
			
		}
		/* Coach Asia Side Navigation : End */
		/* Coach Intarnet Side Navigation : Start */
		if(parentNavs.size()>0){
		  Map<String, Layout> layoutMap = WebServiceUtil.getLayouts(themeDisplay.getScopeGroupId());
    	  for (int i=counter;i < parentNavs.size(); i++) {
			childNavs = parentNavs.get(i).getChildren();
			out.println("<div id='sideNavigationDiv' class='grey demo-container'> <ul class='accordion'  id='accordion-2'>");
			/* Level 2 links Start. */
			for (int j = 0; j < childNavs.size(); j++) {
				if(LayoutPermissionUtil.contains(permissionChecker, childNavs.get(j), ActionKeys.VIEW) ){
				/* out.println("<li><a href='"
							+ themeDisplay.getPathFriendlyURLPrivateGroup()
							+ layout.getGroup().getFriendlyURL()
							+ childNavs.get(j).getFriendlyURL() + " '>"
							+ childNavs.get(j).getHTMLTitle(locale)
							+ "</a>"); */
		         
				/* Setting URL for Level 2 Start. */			
				String level2URL = "";
				if (childNavs.get(j).getChildren().size() == 0) 
				{
					level2URL = "href='"+themeDisplay.getPathFriendlyURLPrivateGroup()
					+ layout.getGroup().getFriendlyURL()
					+ childNavs.get(j).getFriendlyURL()+"'";
					
					targetLayout = layoutMap.containsKey(childNavs.get(j).getNameCurrentValue()+"-"+parentNavs.get(i).getNameCurrentValue()) ? 
							layoutMap.get(childNavs.get(j).getNameCurrentValue()+"-"+parentNavs.get(i).getNameCurrentValue()): null;
						
				}
				/* Setting URL for Level 2 End. */
				/* Logic to display Level 2 links Start. */				
				if(currentParentPage!=null && currentParentPage.equalsIgnoreCase(childNavs.get(j).getName(locale))){			
				out.println("<li class='dcjq-current-parent'><a "+level2URL+">"
					+ childNavs.get(j).getName(locale)
					+ "</a>");
				}else{
					out.println("<li><a "+level2URL+">"
						+ childNavs.get(j).getName(locale)
						+ "</a>");
				}
				
				
				/* Logic to display Level 2 links End. */
				/* Code to displaying Level 2 child (or Level 3) Asset Entries Documents Start. */
				String subChildName = childNavs.get(j).getName(defaultLocale);
				String subParentName = parentNavs.get(i).getName(defaultLocale);
				List<AssetCategory>	assetCategoryList = WebUtil.fetchAssetCategoryByName(renderRequest, subChildName, subParentName, defaultLocale);
				if(assetCategoryList!=null && assetCategoryList.size()>0 && Validator.isNotNull(assetCategoryMap) && 
						assetCategoryMap.size()>0 && childNavs.get(j).getChildren().size() == 0){
				//	List<AssetEntry> sub3AssetEntryList = WebUtil.fetchAssetEntries(assetCategoryList.get(0),renderRequest);
					List<AssetEntry> sub3AssetEntryList = WebUtil.fetchAssetEntries(assetCategoryList.get(0),assetEntriesMap.get(assetCategoryList.get(0).getCategoryId()));
				//	List<AssetCategory> sub3CategoryList = WebUtil.getChildCategories(assetCategoryList.get(0),renderRequest);
				    List<AssetCategory> sub3CategoryList = WebUtil.getChildCategories(assetCategoryMap.get(assetCategoryList.get(0).getCategoryId()),renderRequest);	
				
					%>
					<%@include file="/WEB-INF/jsp/sideNavigationCategory.jsp"%>
					
					
					<%	
				}		
				
				/* Code to displaying Level 2 child (or Level 3) Asset Entries Documents End. */
							
					subChildNavs = childNavs.get(j).getChildren();
					/* Level 3 links Start. */
					if(subChildNavs!=null && subChildNavs.size()>0){
						out.println("<ul style='padding-left: 10px;'>");
					for (int k = 0; k < subChildNavs.size(); k++) {
						if(LayoutPermissionUtil.contains(permissionChecker, subChildNavs.get(k), ActionKeys.VIEW) ){
						/* Logic to display Level 3 links. */
						if(currentPage!=null && currentPage.equalsIgnoreCase(subChildNavs.get(k).getName(locale))){
							out.println("<li class='dcjq-current-parent'><a  id='level3'  href='"
								+ themeDisplay.getPathFriendlyURLPrivateGroup()
								+ layout.getGroup().getFriendlyURL()
								+ subChildNavs.get(k).getFriendlyURL() + " '>"
								+ subChildNavs.get(k).getName(locale)
								+ "</a>");
						}else{
							
							out.println("<li><a  id='level3' href='"
								+ themeDisplay.getPathFriendlyURLPrivateGroup()
								+ layout.getGroup().getFriendlyURL()
								+ subChildNavs.get(k).getFriendlyURL() + " '>"
								+ subChildNavs.get(k).getName(locale)
								+ "</a>");
						}
						
						
						/* Fetching Asset Category based on Asset Name Start. */
						String childName = subChildNavs.get(k).getName(defaultLocale);
						String parentName = childNavs.get(j).getName(defaultLocale);	
						List<AssetCategory> categoryList = new ArrayList<AssetCategory>();
						List<AssetCategory> tempCategoryList = new ArrayList<AssetCategory>();
						String currentLocale = "";
						
						categoryList = WebUtil.fetchAssetCategoryByName(renderRequest ,childName, parentName, defaultLocale);
						
						/* Fetching locales and filtering with current locale. */
						String locales = PrefsPropsUtil.getString(themeDisplay.getCompanyId(), PropsKeys.LOCALES);
						String[] splitLocales = locales.split(",");
						for(int s=0;s<splitLocales.length;s++){
							if(locale.toString().equals(splitLocales[s])){
								currentLocale = splitLocales[s];
							}
						}
						/* Fetching Asset Category based on locale and vocabulary. */
						if(!(locale.toString().equals("en_US"))){
							for(int n=0;n<categoryList.size();n++){
								AssetVocabulary assetVocabulary = AssetVocabularyLocalServiceUtil.getAssetVocabulary(categoryList.get(n).getVocabularyId());
								if(locale.toString().equals(currentLocale) && assetVocabulary.getName()!= null && assetVocabulary.getName().contains(currentLocale.split("_")[1])){
									tempCategoryList.add(categoryList.get(n));
								}
							}
							categoryList.clear();
							categoryList.addAll(tempCategoryList);
						}						
						targetLayout = layoutMap.containsKey(subChildNavs.get(k).getNameCurrentValue()+"-"+childNavs.get(j).getNameCurrentValue()) ? 
								layoutMap.get(subChildNavs.get(k).getNameCurrentValue()+"-"+childNavs.get(j).getNameCurrentValue()): null;
						
						if(categoryList!=null && categoryList.size()>0 && Validator.isNotNull(assetCategoryMap) && assetCategoryMap.size()>0 &&
							AssetCategoryLocalServiceUtil.getCategory(categoryList.get(0).getParentCategoryId()).getName().equals(parentName)){
					//	List<AssetEntry> sub3AssetEntryList = WebUtil.fetchAssetEntries(categoryList.get(0),renderRequest);
						List<AssetEntry> sub3AssetEntryList = WebUtil.fetchAssetEntries(categoryList.get(0),assetEntriesMap.get(categoryList.get(0).getCategoryId()));
					//	List<AssetCategory> sub3CategoryList = WebUtil.getChildCategories(categoryList.get(0),renderRequest);
					    List<AssetCategory> sub3CategoryList = WebUtil.getChildCategories(assetCategoryMap.get(categoryList.get(0).getCategoryId()),renderRequest);
						%>
						<%@include file="/WEB-INF/jsp/sideNavigationCategory.jsp"%>
						
						
						<%
                        }//if end.
						out.println("</li>");
					}
					}// k end
					out.println("</ul>");
					}//if end. Level 3 links End.
					out.println("</li>");
			  }
			}/*  j end. Level 2 links End. */
			out.println("</ul></div>");
		}// i end
		}
		/* Coach Intarnet Side Navigation : End */
		long exitTime = System.currentTimeMillis(); 
		long totalTime = exitTime-entryTime; 
	%>
	<input type="hidden" value='<%=totalTime%>' id="TotalTime" name="TotalTime">
	 <!--  /**
			 * Dispalying Asset Entries.
			 * @param assetEntryList : List of Asset Entries.
			 * @param category : If it is Category value will be "true" or "false" for Asset.
			 * @param catOrEntryName : Holds the Level 5,6,7,8 Category or Asset Entry name for opening the node.  
			 * @param fileName : Holds the  File name to be highlighted. 
			 * @param renderRequest : Render Request.
			 * @param renderResponse : Render Response. 
			 * @param themeDisplay : Theme Display. 
			 * @return
			 */
	-->
	
   <%!
   final Log logger = LogFactoryUtil.getLog(this.getClass());
   private String displayAssetEntries(List<AssetEntry> assetEntryList,Layout targetLayout, boolean isCategory, String catOrEntryName,String fileName, RenderRequest renderRequest,
	                                RenderResponse renderResponse, ThemeDisplay themeDisplay) {

		StringBuilder assetEntryStringBuffer = new StringBuilder();
		String level5Parent = "";
		String level6Parent = "";
		String level7Parent = "";
		String level8Parent = "";
		JournalArticle journalArticle = null;
	
		CoachComparator.sort(assetEntryList, "getTitleCurrentValue", true, false);
		for (AssetEntry assetEntry : assetEntryList) {
			String title = "";
			String categoryFileURL = "";
			String typeOfLink = "";
			String mimeType = assetEntry.getMimeType().trim();

			/* For Webcontent, Internal & External Link. */
			if (assetEntry.getClassName().endsWith("JournalArticle")) {
				try {
					/* For Webcontent */
					journalArticle = JournalArticleLocalServiceUtil.getLatestArticle(assetEntry.getClassPK());
                        
					title = WebUtil.cropTitle(journalArticle.getTitleCurrentValue());
					categoryFileURL = journalArticle.getArticleId();
					/* For Internal & External Link (Structures & Templates) . */
					if (journalArticle != null && journalArticle.getTemplateId() != null && !journalArticle.getTemplateId().isEmpty()) 
					{
						JournalTemplate journalTemplate =
							JournalTemplateLocalServiceUtil.getTemplate(journalArticle.getGroupId(), journalArticle.getTemplateId());
						if (journalTemplate != null && journalTemplate.getNameCurrentValue().equalsIgnoreCase("COACH_LINK_TEMPLATE")) 
						{

							Document document = SAXReaderUtil.read(journalArticle.getContent());
							categoryFileURL = document.selectSingleNode(Constants.DYNAMIC_ELEMENT_NAME + "url" + Constants.DYNAMIC_CONTENT).getText();
							typeOfLink = document.selectSingleNode(Constants.DYNAMIC_ELEMENT_NAME + "type" + Constants.DYNAMIC_CONTENT).getText();
                             
						}
					}
					// Transmittal Check
					if (journalArticle != null && journalArticle.getStructureId() != null && journalArticle.getStructureId().equalsIgnoreCase("TRANSMITTAL"))
					{
						typeOfLink = "TRANSMITTAL";
						categoryFileURL = ""+journalArticle.getId();
				    }
				}
				catch (Exception e) {
					LoggerUtil.errorLogger(logger, "SideNavigation jsp displayAssetEntries() - Journal Article Exception ",e);
				}
			}/* For Document & Media. */
			else {
				categoryFileURL = WebUtil.displayFileURL(assetEntry.getClassPK(), renderRequest, renderResponse, themeDisplay);
				title = WebUtil.cropTitle(assetEntry.getTitleCurrentValue());
			}

			if (null != categoryFileURL && !categoryFileURL.isEmpty()) {
				if (catOrEntryName != null && !catOrEntryName.isEmpty()) {
					if (catOrEntryName.contains(";")) {
						String[] level5level6level7level8Array = catOrEntryName.split(";");
						String[] level5Array = new String[1];
						String[] level6Array = new String[1];
						String[] level7Array = new String[1];
						String[] level8Array = new String[1];

						for (int i = 0; i < level5level6level7level8Array.length; i++) {
							if (i == 0) {
								level5Array = level5level6level7level8Array[i].split(",");
								if (level5Array[0].equals("level5Parent")) {
									level5Parent = level5Array[1];
								}
							}

							if (i == 1) {
								level6Array = level5level6level7level8Array[i].split(",");
								if (level6Array[0].equals("level6Parent")) {
									level6Parent = level6Array[1];
								}
							}

							if (i == 2) {
								level7Array = level5level6level7level8Array[i].split(",");
								if (level7Array[0].equals("level7Parent")) {
									level7Parent = level7Array[1];
								}
							}

							if (i == 3) {
								level8Array = level5level6level7level8Array[i].split(",");
								if (level8Array[0].equals("level8Parent")) {
									level8Parent = level8Array[1];
								}
							}
						}

					}
					else {
						String[] level5Array = catOrEntryName.split(",");

						if (level5Array[0].equals("level5Parent")) {
							level5Parent = level5Array[1];
						}
					}
				}
				PortletURL actionURL = null;
                if(Validator.isNull(targetLayout))
                {
					actionURL = renderResponse.createActionURL();
                }else{
					actionURL = PortletURLFactoryUtil.create(renderRequest,"SideNavigation_WAR_cipportlet",targetLayout.getPlid(),PortletRequest.ACTION_PHASE);
					try{
						actionURL.setWindowState(WindowState.NORMAL);
					}catch( WindowStateException e)
					{
						LoggerUtil.errorLogger(logger, "SideNavigation jsp displayAssetEntries() -  WindowStateException Exception ",e);
					}
                } 
				actionURL.setParameter("myaction", "processDocumentURL");
				actionURL.setParameter("fileURL", categoryFileURL);
				actionURL.setParameter("fileTitle", title);
				actionURL.setParameter("fileType", assetEntry.getClassName());
				actionURL.setParameter("classPK", String.valueOf(assetEntry.getClassPK()));
				actionURL.setParameter("typeOfLink", typeOfLink);
				actionURL.setParameter("mimeType", mimeType);
				actionURL.setParameter("level5Parent", level5Parent);
				actionURL.setParameter("level6Parent", level6Parent);
				actionURL.setParameter("level7Parent", level7Parent);
				actionURL.setParameter("level8Parent", level8Parent);

				/* Link to display Asset with the same Category name. This Asset is not visible under the Category.*/ 
				if(isCategory){
					
					
					assetEntryStringBuffer.append("<a href='");
					assetEntryStringBuffer.append(actionURL.toString());
					assetEntryStringBuffer.append("::" + title);
					assetEntryStringBuffer.append("'>");
					assetEntryStringBuffer.append(title.toUpperCase());
					assetEntryStringBuffer.append("</a>");

				}
				/* Link to display Assets under the Category. */
				else{				
				
					if (fileName != null && !fileName.isEmpty() && fileName.equalsIgnoreCase(title)) {
						assetEntryStringBuffer.append("<li class='dcjq-current-parent'>");
					}
					else {
						assetEntryStringBuffer.append("<li>");
					}
					
					
					assetEntryStringBuffer.append("<a href='");
					if(typeOfLink.contains("external"))
					{
						assetEntryStringBuffer.append(categoryFileURL);
						assetEntryStringBuffer.append("' target='_blank'");
						assetEntryStringBuffer.append(" name='");
						assetEntryStringBuffer.append(actionURL.toString());
						assetEntryStringBuffer.append("::" + title);
						
					}else
					{
						assetEntryStringBuffer.append(actionURL.toString());
						assetEntryStringBuffer.append("::" + title);
					}
						assetEntryStringBuffer.append("'");
					/* assetEntryStringBuffer.append(" name='");
					assetEntryStringBuffer.append(actionURL.toString());
					assetEntryStringBuffer.append("::" + title); */
					assetEntryStringBuffer.append(">");
					assetEntryStringBuffer.append(title);
					assetEntryStringBuffer.append("</a>");
					assetEntryStringBuffer.append("</li>");
				
				}

			}

		}
		return assetEntryStringBuffer.toString();
	}%>	
