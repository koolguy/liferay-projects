<%@page import="com.coach.cip.util.LoggerUtil"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.coach.cip.util.Constants"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>
<%@page import="com.liferay.portlet.journalcontent.util.JournalContentUtil"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticle"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<%!
public final boolean containsDigit(final String s){  
    boolean containsDigit = false;

    if(s != null){
        for(char c : s.toCharArray()){
            if(Character.isDigit(c)){
                containsDigit = true;
                break;
            }
        }
    }
    return containsDigit;
}

%>

<%
final Log logger = LogFactoryUtil.getLog(this.getClass());
String articleId = "";
String articleContent = "";
if(request.getAttribute("articleId") != null){
	articleId = (String)request.getAttribute("articleId");
	String articleName = "";
	boolean chkVariable = containsDigit(articleId);
	JournalArticle article = null;
	if(!chkVariable) {
	if(Validator.isNotNull(articleId) && articleId.contains(" ")){
		articleName = articleId.trim().replaceAll(" ","-");
	}
	if(Validator.isNotNull(articleName) && articleName.endsWith(".")){
		articleName = articleName.replace(".","-");
	}
	if(Validator.isNotNull(articleName)){
		try {
				article =  JournalArticleLocalServiceUtil.getLatestArticleByUrlTitle(themeDisplay.getScopeGroupId(), articleName.trim(),-1);
		}catch(Exception e){
			LoggerUtil.errorLogger(logger,"Exception Caught in TaskResource JSP for a getLatestArticleByUrlTitle while processing jouranl article",e);
		}
	}
	if(Validator.isNotNull(article) && Validator.isNotNull(article.getArticleId())){
		articleName = article.getArticleId();
	}
	} else {
		articleName = articleId;
	}
	
	try {
	//JournalArticle article =  JournalArticleLocalServiceUtil.getLatestArticleByUrlTitle(themeDisplay.getScopeGroupId(), articleName.trim(),-1);
	
	if(Validator.isNotNull(articleName)){
	articleContent = JournalContentUtil.getContent(themeDisplay.getScopeGroupId(),articleName, null, null, themeDisplay.getLanguageId(), themeDisplay);
	}
	}catch(Exception e){
		LoggerUtil.errorLogger(logger,"Exception Caught in TaskResource JSP while processing jouranl article",e);
	}
}
%>

<div id="taskInstructionsId" style="padding:10px;">				
<%=articleContent %>
</div>