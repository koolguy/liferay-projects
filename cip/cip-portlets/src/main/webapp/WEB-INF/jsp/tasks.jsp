<%@page import="com.coach.cip.util.LoggerUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.coach.cip.portlets.controller.TasksController"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="java.util.List"%>
<%@page import="javax.portlet.PortletMode"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="org.springframework.ws.client.core.WebServiceTemplate"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticle"%>
<%@page import="com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.coach.notifications.hrtask.service.beans.GetRecordsResponse.GetRecordsResult"%>
<%@page import="com.coach.notifications.hrtask.service.beans.UpdateResponse"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.coach.cip.util.Constants"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<style>
.tdstyle {
	vertical-align:left;
}
</style>

<c:set var="localNotificationUrl" value="${localNotificationUrl}"/>
<c:set var="localNotificationtitle" value="${localNotificationtitle}"/>

<c:set var="caseTitle" value="${caseTitle}"/>
<c:set var="caseSysid" value="${caseSysid}"/>

<c:set var="hrTaskUpatedResponse" value="${hrTaskUpatedResponse}"/>
<c:set var="hrTaskRecordslist" value="${hrTaskRecordslist}"/>
<%
	final Log logger = LogFactoryUtil.getLog(this.getClass());
	String localNotificationUrl = Validator.isNotNull(pageContext.getAttribute("localNotificationUrl")) ? (String)pageContext.getAttribute("localNotificationUrl") : "";
	String localNotificationtitle = Validator.isNotNull(pageContext.getAttribute("localNotificationtitle")) ? (String)pageContext.getAttribute("localNotificationtitle") : null;
	String caseTitle = Validator.isNotNull(pageContext.getAttribute("caseTitle")) ? (String)pageContext.getAttribute("caseTitle") : "";
	String caseSysid = Validator.isNotNull(pageContext.getAttribute("caseSysid")) ? (String)pageContext.getAttribute("caseSysid") : "";
	com.coach.notifications.hrtask.service.beans.UpdateResponse hrTaskUpatedResponse = 
		Validator.isNotNull(pageContext.getAttribute("hrTaskUpatedResponse")) ? (com.coach.notifications.hrtask.service.beans.UpdateResponse)pageContext.getAttribute("hrTaskUpatedResponse") : null;
	List<com.coach.notifications.hrtask.service.beans.GetRecordsResponse.GetRecordsResult> hrTaskRecordslist = 
		Validator.isNotNull(pageContext.getAttribute("hrTaskRecordslist")) ? (List<com.coach.notifications.hrtask.service.beans.GetRecordsResponse.GetRecordsResult>)pageContext.getAttribute("hrTaskRecordslist") : null;
		
		
	if(Validator.isNotNull(localNotificationtitle)){
		renderResponse.setTitle(localNotificationtitle);
	} else if(Validator.isNotNull(caseTitle)){
		renderResponse.setTitle(caseTitle);
	}else{
		renderResponse.setTitle("Tasks");
	}
	
%>

<script type="text/javascript">

$(document).ready(function() {
	
	$('a.tasksAnchor').click(function(event) {
	    var value = $(this).attr('href');	
		   Liferay.fire('retrieveValue', value);
		   return false;
	});
});

</script>

<%if(Validator.isNotNull(hrTaskUpatedResponse)) { 
%>
<div style="font-weight:bold;color:red;">Task Status Updated.</div></br>
<%} %>
<div>

<% if (Validator.isNotNull(localNotificationUrl)) { %>
<div>
	<table>
	<tr>
	<td width="80%">
	<%=localNotificationUrl %>
	</td>
	<td width="5%">&nbsp;</td>
	</table>
</div>
<%} %>

<%
try{
	if(Validator.isNotNull(caseTitle) && caseTitle.contains(" ")){
	caseTitle = caseTitle.trim().replaceAll(" ","-");
	LoggerUtil.infoLogger(logger,"caseTitle from tasks jsp to display web content-------->"+caseTitle);
	JournalArticle article =  JournalArticleLocalServiceUtil.getLatestArticleByUrlTitle(themeDisplay.getScopeGroupId(), caseTitle.trim(),-1);	
	if(Validator.isNotNull(article) && Validator.isNotNull(article.getArticleId())){
%>
<div>
	<liferay-ui:journal-article articleId="<%=article.getArticleId()%>"
								groupId="<%=themeDisplay.getScopeGroupId()%>" />
</div>	
<% }}} catch(Exception e){
	LoggerUtil.infoLogger(logger,"No JournalArticle found with case title.");
	//LoggerUtil.errorLogger(logger,"Exception Caught in Tasks JSP while processing jouranl article",e); 
	}%>
<br></br>

<div id="updateTaskCompleteStatusPopup" style="display: none;"></div>
<div class="taskdivstyle">
	<table>
	<% if(Validator.isNotNull(hrTaskRecordslist)) {
		for(com.coach.notifications.hrtask.service.beans.GetRecordsResponse.GetRecordsResult taskRecord:hrTaskRecordslist) { 
			PortletURL taskCompletedActionURL = renderResponse.createActionURL();
			taskCompletedActionURL.setParameter("myaction","callTasksCompletedURL");
			taskCompletedActionURL.setParameter("caseSysid",caseSysid);
			taskCompletedActionURL.setParameter("caseTitle",caseTitle);
			taskCompletedActionURL.setParameter("taskSysId",taskRecord.getSysId());
			taskCompletedActionURL.setWindowState(WindowState.NORMAL);
	%>
	<%if(Validator.isNotNull(taskRecord) && Validator.isNotNull(taskRecord.getShortDescription())) { %>
	<tr>
	<td width="80%"><img class="coachTitle" src="<%=themeDisplay.getPathThemeImages()+"/sidenav-arrow.png"%>"/>
	<a class="tasksAnchor" href="<%=taskRecord.getShortDescription()%>"><%=taskRecord.getShortDescription()%></a>
	</td>
	<td width="5%">&nbsp;</td>
	<td class="tdstyle" width="15%"><a id="updateTaskCompleteStatus" href="<%=taskCompletedActionURL%>">Completed</a></td>
	<%}}} %>
	</tr>
	</table>
</div>	


</div>		
							