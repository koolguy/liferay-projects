<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="org.apache.velocity.runtime.directive.Foreach"%>
<%@ page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<liferay-theme:defineObjects />
<portlet:defineObjects />

<html>

<head>
<script type="text/javascript"
	src="<%= themeDisplay.getPathThemeCss()%>/../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%= themeDisplay.getPathThemeCss()%>/../js/jquery-ui-1.8.19.custom.min.js"></script>
</head>

<body>
	<c:set var="storesList" value='${storesList}'></c:set>
	<select name="storeName" id="target4" class="styledselect_form_1">
	<c:if test="${storesList ne null}">
	<c:choose>
	<c:when test="${storesList.size() eq 0 || storesList.isEmpty()}">
	<c:set var="storeMessage"><spring:message code='label.stores.store.notfound'/></c:set>
	<option value="0"><spring:message code='label.stores.choose.store'/></option>
	</c:when>
	<c:otherwise>
	 <option value="0"><spring:message code='label.stores.choose.store'/></option> 
	<c:forEach  var="store" items="${storesList}">
		<option value="${store}">${store}</option>
	</c:forEach> 
	</c:otherwise>
	</c:choose>
	</c:if>
	</select>
	<div id="storelist_erros" style="color:red;position: absolute;top: 315px;margin: 15px 0px 0px 4px;font-weight:bold;">
		<c:if test="${storeMessage ne null }">
		<c:out value="${storeMessage}"></c:out>
		</c:if>				
	</div>
	<%-- <div id="store_errors" class="error_message">${message}</div> --%>
</body>

</html>