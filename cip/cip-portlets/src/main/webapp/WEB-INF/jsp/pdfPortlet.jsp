<%@page import="com.coach.cip.util.CoachAudit"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletURL"%>
<%@ include file="init.jsp"%>

     
       <div id="pdfid">
	   
	   <div id="pdfCSSWrapperDiv" >
       
       <%
       		String fileType = renderRequest.getParameter("fileType");
            String fileURL = renderRequest.getParameter("fileURL"); 
       		String title = renderRequest.getParameter("title");
       		String classPK = renderRequest.getParameter("classPK");
       		String typeOfLink = renderRequest.getParameter("typeOfLink");
       		String mimeType = renderRequest.getParameter("mimeType");
       		String backButton = renderRequest.getParameter("backButton");
       		String currentVocab = "";
     		String renderURL = themeDisplay.getPortalURL()+PortalUtil.getCurrentURL(renderRequest);
     		currentVocab = WebUtil.getCurrentVocabulary(renderRequest);
     		final Log logger = LogFactoryUtil.getLog(this.getClass());

     		/* Generating the Current Session p_auth : Start */ 
       		String actionURL = renderResponse.createActionURL().toString();
     		String pauthId = StringUtils.substringBetween(actionURL,"p_auth=","&");
     		/* Generating the Current Session p_auth : End */
     		
       		/* Dispalying Content on landing page for First request: Start */
       		if(Validator.isNull(fileType) && Validator.isNull(fileURL) && Validator.isNull(title)){
       			String parentLayout = "";
       			List<Layout> parentNavs = layout.getAncestors();
       			List<AssetEntry> pageNameAssetEntryList;
       			if(parentNavs.size() > 0){
	       			pageNameAssetEntryList	= WebUtil.fetchAssetEntryByName(layout.getHTMLTitle(locale).trim()+StringPool.POUND+layout.getAncestors().get(0).getHTMLTitle(locale),renderRequest);
       			}else{
       				pageNameAssetEntryList	= WebUtil.fetchAssetEntryByName(layout.getHTMLTitle(locale).trim(),renderRequest);
       			}
       			if(pageNameAssetEntryList.size()>0)	{
					JournalArticle journalArticle = null;
					AssetEntry pageNameAssetEntry = pageNameAssetEntryList.get(0);
					fileType = pageNameAssetEntry.getClassName();
					mimeType = pageNameAssetEntry.getMimeType();
					if (pageNameAssetEntry.getClassName().endsWith("JournalArticle")) {
						try {
							/* For Webcontent */
							journalArticle = JournalArticleLocalServiceUtil.getLatestArticle(pageNameAssetEntry.getClassPK());
							title = WebUtil.cropTitle(journalArticle.getTitleCurrentValue());
							fileURL = journalArticle.getArticleId();
							/* For Internal & External Link (Structures & Templates) : Start . */
							if (journalArticle != null && journalArticle.getTemplateId() != null && !journalArticle.getTemplateId().isEmpty()) {
								/* String typeOfLink =""; */
								JournalTemplate journalTemplate =
									JournalTemplateLocalServiceUtil.getTemplate(journalArticle.getGroupId(), journalArticle.getTemplateId());
								if (journalTemplate != null && journalTemplate.getNameCurrentValue().equalsIgnoreCase("COACH_LINK_TEMPLATE")) {

									Document document = SAXReaderUtil.read(journalArticle.getContent());
									fileURL = document.selectSingleNode(Constants.DYNAMIC_ELEMENT_NAME + "url" + Constants.DYNAMIC_CONTENT).getText();
									typeOfLink = document.selectSingleNode(Constants.DYNAMIC_ELEMENT_NAME + "type" + Constants.DYNAMIC_CONTENT).getText();
								}
							}
							/* For Internal & External Link (Structures & Templates) : End . */
							
							if(!journalArticle.isApproved())
							{
								fileURL = "";
							} 
						}
						catch (Exception e) {
							LoggerUtil.errorLogger(logger, "PDFPortlet jsp  - Journal Article Exception ",e);
						}
					}/* For Document & Media. */
					else {
						fileURL = WebUtil.displayFileURL(pageNameAssetEntry.getClassPK(), renderRequest, renderResponse, themeDisplay);
						title = WebUtil.cropTitle(pageNameAssetEntry.getTitleCurrentValue());
					}
					
				}
       			
				/* Web Content Auditing. */
       			
       			Attribute attributeFileTitle = new Attribute("FILE_TITLE",title,"");
				Attribute attributeFileURL = new Attribute("RENDER_URL",renderURL,"");
				Attribute attributeFileType = new Attribute("FILE_TYPE",fileType,"");
				Attribute attributeVocabName = new Attribute("VOCABULARY_NAME",currentVocab,"");
				
				List<Attribute> attributeslist = new ArrayList<Attribute>();
				attributeslist.add(attributeFileTitle);
				attributeslist.add(attributeFileURL);
				attributeslist.add(attributeFileType);
				attributeslist.add(attributeVocabName);
				AuditMessage auditMessage = null;
				if(Validator.isNotNull(fileType)){
					auditMessage = AuditMessageBuilder.buildAuditMessage(fileType.substring(fileType.lastIndexOf(".")+1, fileType.length()), currentVocab, 0l, attributeslist);
				}
				else{
					auditMessage = AuditMessageBuilder.buildAuditMessage("Web Content", currentVocab, 0l, attributeslist);
				}
				
				CoachAudit.route(auditMessage);
				//AuditRouterUtil.route(auditMessage);
				
       		}
       		/* Dispalying Content on landing page for First request: End */
       		
       		
       		/* Adding request parameter for COACHWEB Internal Links : Start */
			if(Validator.isNotNull(typeOfLink) && typeOfLink.equalsIgnoreCase("internal") && fileURL.toLowerCase().contains("coachweb"))
			{
			  if(fileURL.endsWith("/"))
			  {
				  fileURL = fileURL.substring(0, fileURL.length() - 1)+PropsUtil.get(Constants.INTERNAL_LINK_REQUEST_PARAMETER);
			  }else
			  {
				  fileURL = fileURL+PropsUtil.get(Constants.INTERNAL_LINK_REQUEST_PARAMETER);
			  }
			}  
			/* Adding request parameter for COACHWEB Internal Links : End */
       		
       		/* Checking the Asset accesibility oustide the network : Start */
			if(WebUtil.isExternalNetwork(renderRequest)){	
			  try{ 	
				boolean assetExternalAccess = WebUtil.hasAssetExternalAccess(themeDisplay.getCompanyId(),fileType,
						ResourceConstants.SCOPE_INDIVIDUAL,classPK,0,ActionKeys.VIEW);
				if(!assetExternalAccess){
					fileURL = Constants.NO_USER_PRIVILEGE;
				}
			  }catch(Exception e) {
				  LoggerUtil.errorLogger(logger, "PDFPortlet jsp - Internal Access only Exception ",e);
				}
			} 
			/* Checking the Asset accesibility oustide the network : End */
       		
			/* Removing the Domain Name or IP Address from the URL for DLFileEntry : Start. */ 
       		 if(Validator.isNotNull(fileType) && fileType.endsWith("DLFileEntry") && !fileURL.equals(Constants.NO_USER_PRIVILEGE)){
       			
       			fileURL = StringUtils.getChomp(fileURL,"/documents");
       			if(!locale.getLanguage().equalsIgnoreCase("en") ){
         		      fileURL = URLDecoder.decode(fileURL,"UTF-8");
         		}	
       			if(fileURL.contains("%26")){
       				fileURL = fileURL.replaceAll("%26","&");
       			}
       			if(fileURL.contains(" ")){
       				fileURL = fileURL.replaceAll(" ","+");
       			}
       		} 
       		/* Removing the Domain Name or IP Address from the URL for DLFileEntry : End. */
       		
       	%>

        <input type="hidden" value="<%=pauthId%>" id="pauthId" />
        
		<c:choose>
			<c:when test="<%=title != null && !title.isEmpty() %>">
				<script>	
					var nameSpace = "#p_p_id" + '<portlet:namespace />';
				    $(nameSpace).find('.portlet-title').html("<%=title%>");
				</script>
			</c:when>
			<c:otherwise>
				<script>	
					var nameSpace = "#p_p_id" + '<portlet:namespace />';
				    $(nameSpace).find('.portlet-title').html("<%=layout.getHTMLTitle(locale)%>");
				</script>
			</c:otherwise>
		</c:choose>

		<c:choose>
			<c:when
				test="<%= (Validator.isNotNull(fileURL) && !fileURL.contains(\"no-user-privilege\") && (Validator.isNull(typeOfLink) ||(Validator.isNotNull(typeOfLink) && typeOfLink.equalsIgnoreCase(\"internal\"))))%>">
				<c:choose>
					<c:when
						test="<%=fileType != null && !fileType.isEmpty() && fileType.endsWith(\"JournalArticle\") && fileURL.matches(\"[0-9]*\") %>">
						<liferay-ui:journal-article articleId="<%=fileURL%>"
							groupId="<%= themeDisplay.getLayout().getGroupId() %>" />
					</c:when>
					<c:otherwise>
						 <c:if test="<%= Validator.isNotNull(mimeType) && !mimeType.equalsIgnoreCase(\"application/pdf\") && !mimeType.equalsIgnoreCase(\"text/html\")%>">
						      <iframe style="display:none"  src="<%=fileURL%>" width="0%" height="0"></iframe> 
						    <h3><spring:message code='label.pdfPortlet.statement.microsoft'/></h3>
						 </c:if>
						 <c:if test="<%= Validator.isNotNull(mimeType) && mimeType.equalsIgnoreCase(\"application/pdf\") %>">
						     <%--  <iframe src="<%=fileURL%>" width="100%" height="600"></iframe>  --%>
							  <div id="pdf-placeholder" style="width: 100%;height:100%;">
							     <img  src="<%=themeDisplay.getPathThemeCss()%>/../images/loadinganimation.gif"/>
							  </div> 
						      <object id="pdfobject" data="<%=fileURL%>" type="application/pdf"  width="100%" style="display:none"></object>
						 </c:if>
						  <c:if test="<%= Validator.isNotNull(mimeType) && mimeType.equalsIgnoreCase(\"text/html\")%>">
						      <iframe src="<%=fileURL%>" height="600" width="100%"></iframe> 
						 </c:if>
					</c:otherwise>
				</c:choose>
			</c:when>
			<c:when
				test="<%= (Validator.isNotNull(fileURL) && !fileURL.contains(\"no-user-privilege\") && (Validator.isNotNull(typeOfLink) && typeOfLink.equalsIgnoreCase(\"TRANSMITTAL\")))%>">
				 <%--   <jsp:include page="/WEB-INF/jsp/transmittalFullDetail.jsp" /> --%>
				   <%@include file="/WEB-INF/jsp/transmittalFullDetail.jsp"%>
			</c:when>
			<c:when
				test="<%= (Validator.isNotNull(fileURL) && !fileURL.contains(\"no-user-privilege\") && (Validator.isNotNull(typeOfLink) && typeOfLink.equalsIgnoreCase(\"TRANSMITTAL_ATTACHMENT\")))%>">
				 <%--   <jsp:include page="/WEB-INF/jsp/transmittalFullDetail.jsp" /> --%>
				   <%@include file="/WEB-INF/jsp/transmittalAttachement.jsp"%>
			</c:when>
			<c:when
				test="<%= (Validator.isNotNull(typeOfLink) && typeOfLink.equalsIgnoreCase(\"external\")) %>">
				 						<script>
										      var url = "<%=fileURL%>";
										      window.open(url);
									   </script>
				<h3><spring:message code='label.pdfPortlet.statement.window'/></h3>									   
			</c:when>
			<c:when
				test="<%= (fileURL != null && fileURL.contains(Constants.NO_USER_PRIVILEGE)) %>">
				<h5><spring:message code='label.pdfPortlet.statement.information'/>
                <br/><br/> 
				<spring:message code='label.pdfPortlet.statement.network'/></h5>
				
			</c:when>
			<c:otherwise>
				<div style="min-height: 200px;">
					<h3><spring:message code='label.pdfPortlet.statement.selection'/></h3>
				</div>
			</c:otherwise>
		
		</c:choose>
				
        </div>
        </div>
        
	    <div style="display:inline;">
	     <%
		  if(WebUtil.hasWriterRole(renderRequest))
		  {		  
		%>
		
		<c:if test="<%=backButton!= null && backButton.equals(\"true\") %>">
		    
            <span style="float:left;margin-bottom:-5px;">	 		
			<input type="button" class="submitClass" value="<spring:message code='label.back.button'/>"
			onClick="history.go(-1);" />
			</span>
        </c:if>
        <input id="fileURLId"  type="hidden" value="<%=fileURL%>">
		
        <input type="image" src = "<%=request.getContextPath() %>/images/internet_url.jpg" value="Get URL" id="viewURL"/>
        <% } %>
		<span id="assetURLDiv" ></span>
		     
		 </div>
<script>

$(document).ready(function($){	
		var data1;
		var hrFlag = "<%= WebUtil.hasUserHRServiceCenterRole(renderRequest) %>";
		var fileType ="<%= fileType %>";
        var mimeType = "<%= mimeType %>";

		if(hrFlag == 'true' && (fileType.indexOf("DLFileEntry") >= 0) && mimeType == "application/pdf") {
			$('#pdfCSSWrapperDiv').css('height','150px');
			$('#pdfobject').css('height','148px');
			$('#pdfCSSWrapperDiv').addClass('pdfCSSHRCenterWrapper');
		}
		else if(hrFlag == 'false' && (fileType.indexOf("DLFileEntry") >= 0) && mimeType == "application/pdf") {
			$('#pdfCSSWrapperDiv').css('height','600');
			
			$('#pdfobject').css('height','595');
			$('#pdfCSSWrapperDiv').addClass('pdfCSSHRCenterWrapper');
		}
		else if (hrFlag == 'true' && (fileType.indexOf("JournalArticle") >= 0) && mimeType == "text/html") {
		   $('#pdfCSSWrapperDiv').css('height','150px');
		   $('#pdfid').css('height','150px');
		   $('#pdfCSSWrapperDiv').css('overflow','auto');
		   $('#pdfCSSWrapperDiv').addClass('jounalArticleCSSWrapperDiv');
		}
		else if(hrFlag == 'false' && (fileType.indexOf("JournalArticle") >= 0) && mimeType == "text/html"){
		   $('#pdfCSSWrapperDiv').css('height','100%');
		   $('#pdfCSSWrapperDiv').css('overflow','auto');
		   $('#pdfCSSWrapperDiv').addClass('jounalArticleCSSWrapperDiv');
		}
		else {
		   $('#pdfobject').height(600);
		   $('#pdfCSSWrapperDiv').addClass('pdfCSSWrapper');
		}
		$(window).load( function() {
			$('#pdfid').append("<input id='selectedAssetURLId' name='selectedAssetURL' type='hidden' value='"+displayAssetURL()+"'>");
		    $('#pdf-placeholder').hide();
			$('#pdfobject').show();
		});


		/* Script to handle Webcontent links pointing to another Asset : Start. */ 
	      
		//  var userAuthValue=document.URL.split("p_auth=")[1].split("&")[0];
		var userAuthValue= $('#pauthId').val();
	    $('#pdfid').find('a').live('mousedown',function(event) {
		    var currentHREFValue = $(this).attr("href");
		    var editedHREFValue = "";
		    if (event.which === 1 || event.which === 3) {
		    if(currentHREFValue != null){
	 	    	 if(currentHREFValue.indexOf("p_auth=") >= 0){
	 	    		 var beforePauth = currentHREFValue.split("p_auth=")[0];
	 	    		 var afterPauth = currentHREFValue.split("p_auth=")[1];
	 	    		//  editedHREFValue = currentHREFValue.split("p_auth=")[0]+currentHREFValue.split("p_auth=")[1].split(/&(.+)?/)[1]+"&p_auth="+userAuthValue;
	 	    		  editedHREFValue = beforePauth+"p_auth="+userAuthValue+afterPauth.replace(afterPauth.split('&')[0],'');
	 	    	  }
	 	    	   else if (currentHREFValue.indexOf("?") >=0){
	 	    		  editedHREFValue = currentHREFValue+"&p_auth="+userAuthValue;
	 	    	  } else if(currentHREFValue.indexOf("?") == -1){
	 	    		  editedHREFValue = currentHREFValue;
	 	    	  }
		    	}
		    }
		    $(this).attr("href",editedHREFValue);
	    });

	   /* Script to handle Webcontent links pointing to another Asset : End. */
		
		Liferay.on('sideNavValue',function(event, filedata) {
			var data = filedata.split("::");
			data1 = data[0];
			var title = data[1];
			
			var assetURL=document.getElementById("assetURLDiv");
	    	assetURL.innerHTML="";
	    	
	    	var loadingImg=document.getElementById("pdfid");
	    	loadingImg.innerHTML='<div id="pdf-placeholder" style="width: 100%;height:100%;"><img src="<%=themeDisplay.getPathThemeCss()%>/../images/loadinganimation.gif"/></div>';
			
            popup(data1, title)
		});
		
		Liferay.on('sideRLValue',function(event, filedata) {
			var d=document.getElementById("assetURLDiv");
	    	d.innerHTML="";
    		var data = filedata.split("::");
			data1 = data[0];
			var title = data[1];
			popupSRL(data1, title)

		});
			
		
		function popup(tempData, title){
			var nameSpace = "#p_p_id" + '<portlet:namespace />';
			//nameSpace = nameSpace.substring(0, nameSpace.length - 1); 
			$(nameSpace).find('.portlet-title').html(title);
			<%
				LiferayPortletURL resourceURL = (LiferayPortletURL)renderResponse.createResourceURL();
			    resourceURL.setCopyCurrentRenderParameters(false);
			    resourceURL.setResourceID("populateSideNavigation");
			%>
			var url = "<%=resourceURL%>"+ "&"+ tempData.split("?")[1];
			$.ajax({
				  url: url,
	              type: "POST",
	              data: {redirect:tempData.split("?")[1]}, 
	              dataType: "html",
                  success: function(data, textStatus, xhr) {
                       $("#pdfid").html( $.trim(data) );
                       $('#pdfid').append("<input id='selectedAssetURLId' name='selectedAssetURL' type='hidden' value='"+displayAssetURL()+"'>");
                   },
                   error: function(xhr, textStatus, errorThrown) {
                	   $("#pdfid").html("<h5>Error while loading the Document.</h5>");
                   }
	            });
		}	
		
		
		function popupSRL(tempData, title){
			var nameSpace = "#p_p_id" + '<portlet:namespace />';
			$(nameSpace).find('.portlet-title').html(title);

			var url = '<portlet:resourceURL id="populateSideRelatedLinks"></portlet:resourceURL>'+ '?fakeId=345&'+ tempData.split("?")[1];
			var xhr = new XMLHttpRequest();
			xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status==200) {
				var responseText = document.getElementById("pdfid");
				responseText.innerHTML = xhr.responseText;
				}
			};
			xhr.open("GET", url, true);
			xhr.send();
		}	
		
		$("#viewURL").click(function() {
    		var d = document.getElementById("assetURLDiv");
    	   if(d.innerHTML){$(d).toggle();}
    	    d.innerHTML ="";
		    d.appendChild(document.createTextNode('URL:'));
			var newInput = document.createElement('input');
			newInput.type = 'text';
			
			newInput.readOnly=true;
			
			newInput.className = 'assetURlDivCSS';
			newInput.value = displayAssetURL();
			
			d.appendChild(newInput);
	    	$(d).find('input:text').focus();
	    	$(d).find('input:text').select();
		});
		
		function displayAssetURL()
		{
			var docURL;
			if(data1 != null && data1.length > 0){
				docURL	= "/group"+data1.split("/group")[1];
				
	    	}else{
				var fileURL = $(location).attr('href');
				if(fileURL != null && fileURL.length > 0){
					docURL = "/group"+fileURL.split("/group")[1];
				}
			}
			if(docURL.indexOf('p_auth') > -1){
				var uri = new URI(docURL);
				uri.removeQuery('p_auth');
				uri.addQuery('transAuth','false');
				uri.addQuery('wcType','trans');
				docURL = uri.toString();
			}
			
			return docURL ; 
		} 
});
</script>


