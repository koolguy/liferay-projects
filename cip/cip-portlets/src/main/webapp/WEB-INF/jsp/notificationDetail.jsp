<%@page import="com.coach.cip.util.LoggerUtil"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.PropsUtil"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.coach.cip.util.Constants"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui" %>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<c:set var="title" value="${title}"/>
<c:set var="sysid" value="${sysid}"/>

<%
	final Log logger = LogFactoryUtil.getLog(this.getClass());
	String sysid = Validator.isNotNull(pageContext.getAttribute("sysid")) ? (String)pageContext.getAttribute("sysid") : null;
	String title = Validator.isNotNull(pageContext.getAttribute("title")) ? (String)pageContext.getAttribute("title") : "NotificationDetail";
	if(title != null && !title.isEmpty()){
		renderResponse.setTitle(title);
	}else{
		renderResponse.setTitle("NotificationDetail");
	}
	LoggerUtil.infoLogger(logger,"sysid from Notification Detail JSP---------->"+sysid);
	LoggerUtil.infoLogger(logger,"title from Notification Detail JSP---------->"+title);
%>

<% if(Validator.isNotNull(sysid)) {
	String notificationDetailURL = PropsUtil.get("service.now.notificationdetail.url")+sysid;
	LoggerUtil.infoLogger(logger,"notificationDetailURL from Notification Detail JSP---------->"+notificationDetailURL);
	%>
<div>
	<iframe src="<%=notificationDetailURL%>" height="500" width="100%" scrolling="no"></iframe> 
</div>
<%}%>
	
