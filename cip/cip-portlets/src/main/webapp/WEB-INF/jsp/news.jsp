<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="javax.portlet.PortletSession"%>
<%@page import="java.util.Date"%>
<%@page import="com.coach.cip.portlets.controller.NewsController"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.coach.cip.util.LoggerUtil"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="javax.portlet.PortletRequest"%>
<%@page import="com.coach.cip.util.WebUtil"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticle"%>
<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<portlet:actionURL name="yearNCatAction" var="yearNCatAction">
</portlet:actionURL>
<head>
<style>
.test {
	color: #ff9454 !important;
}
</style>
<link rel="stylesheet" type="text/css"
	href="<%=themeDisplay.getPathThemeCss()%>/sidenav.css" />
<title>Insert title here</title>
</head>
<script type='text/javascript'
	src='<%=themeDisplay.getPathThemeCss()%>/../js/jquery.cookie.js'></script>
<script type='text/javascript'
	src='<%=themeDisplay.getPathThemeCss()%>/../js/jquery.hoverIntent.minified.js'></script>
<script type='text/javascript'
	src='<%=themeDisplay.getPathThemeCss()%>/../js/jquery.dcjqaccordion.2.7.min.js'></script>
<script type="text/javascript">
	$(document).ready(function($) {
		
		$(".portlet-layout").find("#column-1").find("h1:first").append('<input  type="image" src="<%=themeDisplay.getPathThemeImages()%>/leftarrow.png"  id="collapseButton"  style="float:right;margin-right:12px;" title="Hide"/>');		
		
		$('#accordion-news').dcAccordion({
			eventType : 'click',
			autoClose : true,
			saveState : false,
			disableLink : false,
			speed : 'fast',
			classActive : 'test',
			showCount : false,
			autoExpand : true,
			classExpand : 'dcjq-current-parent'
		});
		
		
		$("#collapseButton").click(function(){

			var $columnOne=$(this).parents(".portlet-layout").find("#column-1");
		    var $columnTwo=$(this).parents(".portlet-layout").find("#column-2");

       	    $($columnOne).hide();
            var totalWidth=$($columnTwo).width()+$($columnOne).width()+"px";
				
			$($columnTwo).css({"width":$.trim(totalWidth)});
			
			//$(this).parents(".portlet-layout").find("#column-2").css({"width":$("#wrapper").css("width")+"px"});
			$($columnOne).parent().prepend('<input type="image" src="<%=themeDisplay.getPathThemeImages()%>/rightarrow.png"   id="expndButton" title="Show"/>');
							
		});
		
		
		$("#expndButton").live("click",function(){
           
			var $columnOne=$(this).parents(".portlet-layout").find("#column-1");
		    var $columnTwo=$(this).parents(".portlet-layout").find("#column-2");

           var totalWidth=(0.7 * $($columnTwo).width())+"px";
			$($columnTwo).css({"width":$.trim(totalWidth)});
		   $($columnOne).show();
			
		   $(this).remove();	
								
		});
	});
	function setYearNCat(newsYear, categoryId) {
		document.getElementById("newsYear").value = newsYear;
		document.getElementById("newsCategoryId").value = categoryId;
		$("#newsForm").submit();
	}
	function setYear(newsYear) {
		document.getElementById("newsYear").value = newsYear;
		document.getElementById("newsCategoryId").value = "";
		$("#newsForm").submit();
	}
</script>
<%
	String friendlyUrlName = themeDisplay.getLayout().getFriendlyURL();
	String[] urlTokens = friendlyUrlName.split("/");
	String pageName = urlTokens[urlTokens.length - 1];
	pageName = pageName.toUpperCase(themeDisplay.getLocale()).replace("-", " ");
	if (pageName.contains("NEWS")) {
		pageName = pageName.replaceAll("NEWS", "");
	}
%>
<c:set var="displayYearFrmEvent" value="${displayYearFrmEvent}"></c:set>
<c:set var="catNameFrmEvent" value="${catNameFrmEvent}"></c:set>
<c:set var="yearCatMap" value="${yearCatMap}"></c:set>
<%! final Log newsJspLogger = LogFactoryUtil.getLog(NewsController.class);
	public String createPageName(PortletRequest portletRequest, String urlName) {
		String[] urlTokens = urlName.split("/");
		String pageName = "";
		if (Validator.isNotNull(urlTokens)) {
			int len = urlTokens.length;
			pageName = urlTokens[len - 1];
			pageName = pageName.toUpperCase(WebUtil.getThemeDisplay(portletRequest).getLocale()).replace("-", " ");
		}
	return pageName;
	}
%>

<%

	String displayYearFrmEvent = (String) pageContext.getAttribute("displayYearFrmEvent");
	String catNameFrmEvent = (String) pageContext.getAttribute("catNameFrmEvent");
	String displayYear= "";
	String catName = "";
	Boolean paginationTraversal = false;
	paginationTraversal = PortalUtil.getCurrentURL(renderRequest).contains("cur") || PortalUtil.getCurrentURL(renderRequest).contains("delta");
	if (Validator.isNull(displayYearFrmEvent) && Validator.isNull(catNameFrmEvent)) {
		String defaultYear = new SimpleDateFormat("yyyy").format(new Date());
		displayYear = defaultYear;
		
	}
	if (paginationTraversal) {
		displayYear = (String)renderRequest.getPortletSession().getAttribute("eventYearHighliter", PortletSession.APPLICATION_SCOPE);
		catName = (String)renderRequest.getPortletSession().getAttribute("categoryNameHighliter", PortletSession.APPLICATION_SCOPE);
	}
	if(Validator.isNotNull(displayYearFrmEvent))
		displayYear = displayYearFrmEvent;
	if(Validator.isNotNull(catNameFrmEvent))
		catName = catNameFrmEvent;
%>

<c:set var="displayYearFrmEvent" value="<%=displayYear %>"></c:set>
<c:set var="catNameFrmEvent" value="<%=catName %>"></c:set>
<form id="newsForm" method="post"  autocomplete="off" action="${yearNCatAction}">

	<c:if test="${yearCatMap ne null}">
		<div class='grey demo-container'>
			<ul class='accordion' id='accordion-news'>
				<c:forEach var="newsYear" items="${yearCatMap.keySet()}">
					<c:choose>
						<c:when
							test="${fn:trim(displayYearFrmEvent) eq fn:trim(newsYear)}">
							<li class='dcjq-current-parent'>
						</c:when>
						<c:otherwise>
							<li>
						</c:otherwise>
					</c:choose>
					<a onclick="setYear('${newsYear}')" id="yearUrl" href="#"> 
					<c:out value="${newsYear} " /> 
					
					 <c:out value="<%=themeDisplay.getLayout().getHTMLTitle(locale).toUpperCase(locale) %>"></c:out>
					 </a>
					<c:if test="${not empty yearCatMap.get(newsYear)}">
						<ul style="padding-left: 10px;">
							<c:forEach var="category" items="${yearCatMap.get(newsYear)}">
								<c:choose>
									<c:when
										test="${(fn:trim(displayYearFrmEvent) eq fn:trim(newsYear)) && (fn:trim(catNameFrmEvent) eq fn:trim(category.name))}">
										<li class='dcjq-current-parent'>
									</c:when>
									<c:otherwise>
										<li>
									</c:otherwise>
								</c:choose>
								
								<a onclick="setYearNCat('${newsYear}','${category.categoryId}')"
									id="catUrl" href="#"><c:out value="${category.getTitle(locale)}"></c:out>
								</a>
								</li>

							</c:forEach>
						</ul>
					</c:if>
					</li>

				</c:forEach>
			</ul>
		</div>
	</c:if>

	<input type="hidden" id="newsYear" name="newsYear" value="" /> <input
		type="hidden" id="newsCategoryId" name="newsCategoryId" value="" />

</form>