<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="aui" uri="http://liferay.com/tld/aui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="com.coach.cip.common.dto.CountryVO"%>
<%@ page import="org.apache.velocity.runtime.directive.Foreach"%>

<portlet:defineObjects />
	<select name="countryId" id="target2" class="styledselect_form_1">
		<c:choose>
			<c:when test="${countryList.size() eq 0}">
				<c:set var="message">
					<spring:message
						code='label.addeditpersonalmessageadmin.message.nocountryfound' />
				</c:set>
				<option value="0"><spring:message code='label.addeditpersonalmessageadmin.select.nocountryfound' />
				</option>
			</c:when>
			<c:otherwise>
				<c:set var="message">
					<spring:message code='label.error.requiredfeild.blank' />
				</c:set>
				<option value="0"><spring:message code='label.addeditpersonalmessageadmin.select.country' />
				</option>
			</c:otherwise>
		</c:choose>

		<c:forEach var="countryVO" items="${countryList}">

			<option value="${countryVO.getCountryId()}">${countryVO.getName()}</option>

		</c:forEach>
	</select>
	<div id="country_errors" class="error_message" style="display: none">${message}</div>
