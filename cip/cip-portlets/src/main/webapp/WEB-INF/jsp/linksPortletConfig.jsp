<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="liferay-portlet" uri="http://liferay.com/tld/portlet"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ page import="com.liferay.portal.kernel.util.Validator"%>
<%@ page import="com.liferay.portlet.PortletPreferencesFactoryUtil"%>
<%@ page import="javax.portlet.PortletPreferences"%>
<%@ page import="javax.portlet.RenderRequest"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<form action="<liferay-portlet:actionURL portletConfiguration='true' />" autocomplete="off"
	method="post" name="<portlet:namespace/>fm">

	<input type="hidden" name="actionType" id="actionType" value="">
	<input type="hidden" name="deleteIndex" id="deleteIndex" value="">

	<%
		String portletResource = ParamUtil.getString(request,
				"portletResource");
		PortletPreferences preferences = PortletPreferencesFactoryUtil
				.getPortletSetup(request, portletResource);
		String count = preferences.getValue("rowCount", "");
		String[] linkNamesArr = preferences.getValues("linkNamesArr",
				new String[]{});
		String[] linkUrlsArr = preferences.getValues("linkUrlsArr",
				new String[]{});
	%>
	<table>
		<%
			if (linkNamesArr.length > 0) {
		%>
		<tr>
			<td><label style="margin-left: 5px;"><b>Link Name</b> </label></td>
			<td><label style="margin-left: 5px;"><b>Link Url</b> </label></td>
		</tr>
		<%
			}
		%>
		<%
			for (int index = 0; index < linkNamesArr.length; index++) {
				if (Validator.isNotNull(linkNamesArr[index])) {
		%>
		<tr>
			<td style="padding: 3px;"><input style="padding: 4px;"
				type="text" name="linkName<%=index%>"
				value="<%=linkNamesArr[index]%>">
			</td>
			<td style="padding: 3px;"><input style="padding: 4px;"
				type="text" name="linkUrl<%=index%>" value="<%=linkUrlsArr[index]%>">
			</td>
			<td><input class="aui-button-input" type="submit"
				name="deleteRow" onclick="setActionType('deleteRow','<%=index%>');"
				value="<spring:message code='label.delete'  />">
			</td>
		</tr>
		<%
			}
			}
		%>
		<tr>
			<td><br>
			</td>
		</tr>
	</table>
	<input class="aui-button-input" type="submit" name="save"
		value="<spring:message code='label.save'  />"
		onclick="setActionType('save');" /> 
	<input class="aui-button-input"
		type="submit" name="addRow"
		value="<spring:message code='label.add-row' />"
		onclick="setActionType('addRow');" />

</form>

<script type="text/javascript">
	function setActionType(actionType, deleteIndex) {
		document.getElementById('actionType').value = actionType;
		if ('deleteRow' == actionType) {
			document.getElementById('deleteIndex').value = deleteIndex;

		}
	}
</script>

