<%@page import="com.coach.cip.util.WebUtil"%>
<%@page import="com.coach.cip.util.LoggerUtil"%>
<%@page import="com.coach.cip.portlets.controller.NewsDetailController"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="java.io.UnsupportedEncodingException"%>
<%@page import="java.net.URLDecoder"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@ page import="java.util.List"%>
<%@ page import="com.coach.cip.common.dto.NewHiresVO"%>
<%@ page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<script>
$(document).ready(function($){	
		
		$("#viewURL").click(function() {
				var fileURL = $(location).attr('href');
				if(fileURL != null && fileURL.length > 0){
					var url = "/group"+fileURL.split("/group")[1];
		    		var d=document.getElementById("assetURLDiv");
		    		if(d.innerHTML){$(d).toggle();}
		       	    d.innerHTML ="";
		   		    d.appendChild(document.createTextNode('URL:'));
		   			var newInput = document.createElement('input');
		   			newInput.type = 'text';
		   			
		   			newInput.readOnly=true;
		   			
		   			newInput.className = 'assetURlDivCSS';
		   			newInput.value = url;
		   			
		   			d.appendChild(newInput);
		   	    	$(d).find('input:text').focus();
		   	    	$(d).find('input:text').select();
		   			
				}
		});

});
</script>

<portlet:actionURL var="attachmentUrl">
	<portlet:param name="action" value="attachmentPage"></portlet:param>
</portlet:actionURL>
<c:set var="documentPath" value="${documentPath}"></c:set>
<%
	//String redirect = ParamUtil.getString(request, "redirect");
	final Log loggerJsp = LogFactoryUtil.getLog(NewsDetailController.class);

	String documentPath = (String) pageContext.getAttribute("documentPath");
	try {
		documentPath = URLDecoder.decode(documentPath, "UTF-8");
	}
	catch (UnsupportedEncodingException e) {
		LoggerUtil.errorLogger(loggerJsp, "UTF-8 Decoding is not supported by "+documentPath, e);
	}
%>


<%-- <liferay-ui:header backURL="<%= redirect %>" title="" /> --%>
<%-- <liferay-ui:header backURL="javascript:history.back()" title="" /> --%>


<fmt:bundle basename="content/Language">


	<form:form name="attachmentForm" method="post"
		commandName="documentPath" action="${attachmentUrl}">

		<div id="pdfid">


			<table class="tableEmployee">
				<tr>
					<td><iframe src="<%=documentPath %>" width="100%" height="600"></iframe>
					</td>
				</tr>
			</table>
			
        </div>
		
	</form:form>
	<br/>
	<div style="display:inline;">
		<span style="float:left;margin-bottom:-5px;">
			<input type="button" id="aui_3_4_0_1_498" value="<spring:message code='label.back.button'/>" class="submitClass" onClick="history.go(-1);" />
        </span>
		<%
		if(WebUtil.hasWriterRole(renderRequest))
		{%>
			<input type="image" src = "<%=request.getContextPath() %>/images/internet_url.jpg" value="Get URL" id="viewURL"/>
			
		<% } %>
		<span id="assetURLDiv" ></span>
	</div>

</fmt:bundle>
