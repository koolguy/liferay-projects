<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="org.apache.velocity.runtime.directive.Foreach"%>
<%@ page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ page import="com.coach.cip.common.dto.StatesVO"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<liferay-theme:defineObjects />
<portlet:defineObjects />

<html>

<head>
<script type="text/javascript"
	src="<%= themeDisplay.getPathThemeCss()%>/../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%= themeDisplay.getPathThemeCss()%>/../js/jquery-ui-1.8.19.custom.min.js"></script>
</head>

<body>
	<c:set var="selectedValue" value='${selectedValue}'></c:set>
	<c:set var="statesList" value='${statesList}'></c:set>
	<%
	String selectedState = "";
	List<String> statesList1 = new ArrayList<String>();
	if(pageContext.getAttribute("selectedValue") != null){
		 selectedState = (String)pageContext.getAttribute("selectedValue");
	}
	if(pageContext.getAttribute("statesList") != null){
		statesList1 = (List<String>)pageContext.getAttribute("statesList");
	}
	%>
	
	<select name="stateId" id="target2" class="styledselect_form_1">
	<%
	if(Validator.isNotNull(statesList1) && statesList1.size() > 0){
	%>
	<c:choose>
	<c:when test="${statesList1.size() eq 0 || statesList1.isEmpty()}">
	<c:set var="message"><spring:message code='label.stores.state.notfound'/></c:set>
	<option value="0"><spring:message code='label.stores.choose.state'/></option>
	</c:when>
	<c:otherwise>
	<%
	for(String state : statesList1){
		if(state.equals(selectedState)){
	%>
		<option selected="true" value="<%=state%>"><%=state%></option>
	<%}else{ %>
		<option value="<%=state%>"><%=state%></option>
	<% }} %>
	
	</c:otherwise>
	</c:choose>
	<%} else {%>
	<option value="0"><spring:message code='label.stores.state.zero'/></option>
	<%} %>
	</select>
	<%-- <div id="state_errors" class="error_message">${message}</div> --%>
</body>

</html>