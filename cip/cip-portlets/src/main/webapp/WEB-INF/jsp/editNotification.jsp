<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="java.util.List"%>
<%@ page import="com.liferay.portal.kernel.util.CalendarFactoryUtil"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="com.liferay.portal.service.RoleLocalServiceUtil"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="com.liferay.portal.model.Role"%>
<%@ page import="com.liferay.portal.util.PortalUtil"%>
<%@ page import="com.coach.cip.model.entity.Region"%>
<%@ page import="com.coach.cip.common.dto.RegionVO"%>
<%@ page import="com.coach.cip.common.dto.RoleVO"%>
<%@ page import="com.coach.cip.common.dto.UserVO"%>
<%@ page import="com.coach.cip.common.dto.NotificationVO"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.coach.cip.model.entity.Country"%>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="org.apache.velocity.runtime.directive.Foreach"%>
<%@ page import="com.coach.cip.common.dto.GeographyVO"%>
<%@ page import="com.coach.cip.services.ServiceLocator"%>
<%@ page import="com.liferay.portal.kernel.util.UnicodeFormatter"%>
<%@ page import="com.liferay.portal.service.RegionServiceUtil"%>
<%@ page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import="com.liferay.portal.model.User"%>
<%@page import="com.liferay.portal.kernel.dao.orm.QueryUtil"%>


<liferay-theme:defineObjects />
<portlet:defineObjects />

<link rel="stylesheet"
	href="<%=themeDisplay.getPathThemeCss()%>/screen.css" type="text/css"
	media="screen" title="default" />

<link rel="stylesheet" type="text/css"
	href="<%=themeDisplay.getPathThemeCss()%>/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css"
	href="<%=themeDisplay.getPathThemeCss()%>/jquery.multiselect.filter.css" />

<link rel="stylesheet" type="text/css"
	href="<%=themeDisplay.getPathThemeCss()%>/jquery-ui.css" />

<!--  date picker script -->
<link rel="stylesheet"
	href="<%=themeDisplay.getPathThemeCss()%>/datePicker.css"
	type="text/css" />

<style>
tr.hasselect td input {
	margin-left: -4px
}
</style>

<!--  jquery core -->
<script type="text/javascript"
	src="<%= themeDisplay.getPathThemeCss()%>/../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%= themeDisplay.getPathThemeCss()%>/../js/jquery-ui-1.8.19.custom.min.js"></script>


<script>
	$(function() {
		$( "#datepicker1" ).datepicker({
			showOn: "button",
			minDate: 0,
			buttonImage: "<%=themeDisplay.getPathThemeCss()%>/../images/icon_calendar.jpg",
			buttonImageOnly: true
		});
	});
	function textCounter( field, countfield, maxlimit ) {
	    if ( field.value.length > maxlimit )
	    {
	    field.value = field.value.substring( 0, maxlimit-1 );
	    alert( 'Description is greater than 2000 characters in length.' );
	    return false;
	   }
	   else
	   {
	    countfield.value = maxlimit - field.value.length;
	   }
	   }
	
	$(function() {
		$( "#datepicker2" ).datepicker({
			showOn: "button",
			minDate: 0,
			buttonImage: "<%=themeDisplay.getPathThemeCss()%>/../images/icon_calendar.jpg",
			buttonImageOnly: true
		});
	});
	</script>

<!--  checkbox styling script -->
<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.multiselect.js"></script>
<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.multiselect.filter.js"></script>
<script src="<%=themeDisplay.getPathThemeCss()%>/../js/ui.core.js"
	type="text/javascript"></script>
<script src="<%=themeDisplay.getPathThemeCss()%>/../js/ui.checkbox.js"
	type="text/javascript"></script>
<script src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.bind.js"
	type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

  $("#roles").multiselect({
	      classes : 'notify_roles',
		  close: function(event, ui){
				var selectedRoleIds = [];
				$('.notify_roles').find('ul').find('li').find('input[aria-selected="true"]').each(function(){
					//alert(this.title + " : " + this.value);
					selectedRoleIds.push(this.value);
				});
				//alert(selectedRoleIds);
				var xmlHttpRequest;
				
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
					xmlHttpRequest = new XMLHttpRequest();
				}
				else
				{// code for IE6, IE5
					xmlHttpRequest = new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xmlHttpRequest.onreadystatechange = function() {
					if (xmlHttpRequest.readyState == 4 && xmlHttpRequest.status==200) {
						var messageText = document
								.getElementById("<portlet:namespace/>userText");
						messageText.innerHTML = xmlHttpRequest.responseText;
						$("#users").multiselect().multiselectfilter();
						$("#roles").focus();
					}
				};
				var url = '<portlet:resourceURL id="populateUsers"></portlet:resourceURL>'+ '&notify_roleIds='+ selectedRoleIds.join(',');
				/* var jqxhr = $.ajax(url)
			    .done(function() { alert(jqxhr.statusText + " : " + jqxhr.status  + " : " +jqxhr.responseXML ); })
			    .fail(function() { alert("error"); }); */
			    xmlHttpRequest.open("GET", url, true);
			    xmlHttpRequest.send();
			}}).multiselectfilter();

$("#users").multiselect().multiselectfilter();
});
</script>

<script type="text/javascript">
$(function(){
	$('input').checkBox();
	$('#toggle-all').click(function(){
 	$('#toggle-all').toggleClass('toggle-checked');
	$('#mainform input[type=checkbox]').checkBox('toggle');
	return false;
	});
});
</script>


<![if !IE 7]>

<!--  styled select box script version 1 -->
<script
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.selectbox-0.5_style_2.js"
	type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('.styledselect').selectbox({ inputClass: "selectbox_styled" });
});
</script>


<![endif]>


<!--  styled select box script version 2 -->
<script
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.selectbox-0.5_style_2.js"
	type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#target2").live('focus',function(){$(this).selectbox({ inputClass: "styledselect_form_1" });});
	$("#target3").live('focus',function(){$(this).selectbox({ inputClass: "styledselect_form_1" });});
	$("#target4").live('focus',function(){$(this).selectbox({ inputClass: "styledselect_form_1" });});
	$("#target5").live('focus',function(){$(this).selectbox({ inputClass: "styledselect_form_1" });});
	$("#target6").live('focus',function(){$(this).selectbox({ inputClass: "styledselect_form_1" });});
	$('.styledselect_form_1').selectbox({ inputClass: "styledselect_form_1" });
    $('.styledselect_form_2').selectbox({ inputClass: "styledselect_form_2" });
});
</script>

<!--  styled select box script version 3 -->
<script
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.selectbox-0.5_style_2.js"
	type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$('.styledselect_pages').selectbox({ inputClass: "styledselect_pages" });
});
</script>



<!--  date picker script -->

<script src="<%=themeDisplay.getPathThemeCss()%>/../js/date.js"
	type="text/javascript"></script>
<script
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.datePicker.js"
	type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
        $(function()
{

// initialise the "Select date" link
$('#date-pick')
	.datePicker(
		// associate the link with a date picker
		{
			createButton:false,
			startDate:'01/01/2005',
			endDate:'31/12/2020'
		}
	).bind(
		// when the link is clicked display the date picker
		'click',
		function()
		{
			updateSelects($(this).dpGetSelected()[0]);
			$(this).dpDisplay();
			return false;
		}
	).bind(
		// when a date is selected update the SELECTs
		'dateSelected',
		function(e, selectedDate, $td, state)
		{
			updateSelects(selectedDate);
		}
	).bind(
		'dpClosed',
		function(e, selected)
		{
			updateSelects(selected[0]);
		}
	);
	
var updateSelects = function (selectedDate)
{
	var selectedDate = new Date(selectedDate);
	$('#d option[value=' + selectedDate.getDate() + ']').attr('selected', 'selected');
	$('#m option[value=' + (selectedDate.getMonth()+1) + ']').attr('selected', 'selected');
	$('#y option[value=' + (selectedDate.getFullYear()) + ']').attr('selected', 'selected');
}
// listen for when the selects are changed and update the picker
$('#d, #m, #y')
	.bind(
		'change',
		function()
		{
			var d = new Date(
						$('#y').val(),
						$('#m').val()-1,
						$('#d').val()
					);
			$('#date-pick').dpSetSelected(d.asString());
		}
	);

// default the position of the selects to today
var today = new Date();
updateSelects(today.getTime());

// and update the datePicker to reflect it...
$('#d').trigger('change');
});
</script>






<script type="text/javascript">
	$(document).ready(function() {     
		
						$('textarea[maxlength]').live('keyup change', function() {
						    var str = $(this).val()
							var mx = parseInt($(this).attr('maxlength')-5)
						    if (str.length > mx) {
							   $(this).val(str.substr(0, mx))
						       return false;
						    }
					    });
		
						$("#geographyRow").hide();
                        $("#countryRow").hide();
						$("#stateRow").hide();
						$("#cityRow").hide();
						$("#officeRow").hide();
					    $("#buildingRow").hide();	
						$("#rolesRow").hide();
						$("#locationTypeRow").hide();
						
						<!-- Updated with validations-start->
						
						$("#cancelId").click(function(){
							$("#pmEditActionName").val("Cancel");
                  	      	$("#editFormID").submit();
                        }); 
						

						$("#submitId").click(function(){

							var status = false;	
							var messageTypestatus = false;
							var messageStatus = false;
							var subjectStatus = false;
							var datepicker1Status = false;
							var datepicker2Status = false;
							var rolesStatus = false;
							var geographyStatus = false;
							var countryStatus = false;
							var	stateStatus = false;
							var	cityStatus = false;
							var officeStatus = false;
							var buildingStatus = false;
							var datepicker3Status = false;
							
							$("#pmEditActionName").val("Submit");
							var notificationType = $("#notificationType_input").val();
							
							$("#notificationType > option").each(function() {                            
							 
							  if (notificationType==this.text)
                              {
								
								if(this.value == 0){										
									$("#messageType_errors").show();
									messageTypestatus = false;
								 }
								 else {										  
									 $("#messageType_errors").hide();
									 messageTypestatus = true;								 
									 }								
                              }
                           
                            });	
																				
                            				
							var notificationSubject=jQuery.trim($("#notificationSubject").val());							
							if(notificationSubject == "") {									
								$("#subject_errors").show();
								subjectStatus = false;
							}
							else {								
								$("#subject_errors").hide();
								subjectStatus = true;
							}

							var notificationDescription= jQuery.trim($("#notificationDescription").val());						
							if(notificationDescription == "") {								
								$("#message_errors").show();
								messageStatus = false;
							}
							else {								
								$("#message_errors").hide();
								messageStatus = true;
							}

							var datepicker1= $("#datepicker1").val();							
							if(datepicker1 == "") {								
								$("#datepicker1_errors").show();
								datepicker1Status = false;
							}
							else {								
								$("#datepicker1_errors").hide();
								datepicker1Status = true;
							}


							var datepicker2= $("#datepicker2").val();							
							if(datepicker2 == "") {								
								$("#datepicker2_errors").show();
								datepicker2Status = false;
							}
							else {								
								$("#datepicker2_errors").hide();
								datepicker2Status = true;
							}
							var date1 = new Date(datepicker1);
							var date2 = new Date(datepicker2);
							if (date2<date1)
							{
								$("#datepicker3_errors").show();
								datepicker3Status = false;
							}else{								
								$("#datepicker3_errors").hide();
								datepicker3Status = true;
							}	
							
							
							var locationType= $("#locationType").val();	
							if(locationType == "") {								
								$("#locationType_errors").show();
								locationTypeStatus = false;
							}
							else {												
                                $("#locationType_errors").hide();
								locationTypeStatus = true;
							}

							var roles= $("#roles").val();
							if(roles == "" || roles == null){								
								$("#roles_errors").show();
								rolesStatus = false;
							}
							else {								
								$("#roles_errors").hide();                               
								rolesStatus = true;
							}
							
								

							if(locationTypeStatus  || rolesStatus){
								$("#roles_errors").hide();
                                $("#locationType_errors").hide();
							}

							<!--functions to check geography & siblings	 Start-->	
							
							var geography= $("#target1").val();							
							if(geography == 0) {	
								$("#geography_errors").show();
								geographyStatus = false;
							}
							else {		
								$("#geography_errors").hide();
								geographyStatus = true;
							}
							
	
							var country= $("#target2").val();		
							if(country == 0) {								
								$("#country_errors").show();
								countryStatus = false;
							}
							else {								
								$("#country_errors").hide();
								countryStatus = true;
							}
							
							
								var state= $("#target3").val();							
								if(state == 0) {								
									$("#state_errors").show();
									stateStatus = false;
								}
								else {								
									$("#state_errors").hide();
									stateStatus = true;
								}
						
							
							
								var city= $("#target4").val();	
								
								if(city == 0) {								
									$("#city_errors").show();
									cityStatus = false;
								}
								else {								
									$("#city_errors").hide();
									cityStatus = true;
								}	
							
							
							
								var office= $("#target5").val();							
								if(office == 0) {								
									$("#office_errors").show();
									officeStatus = false;
								}
								else {								
									$("#office_errors").hide();
									officeStatus = true;
								}
												
							
							
								var building= $("#target6").val();							
								if(building == 0) {								
									$("#building_errors").show();
									buildingStatus = false;
								}
								else {								
									$("#building_errors").hide();
									buildingStatus = true;
								}
							
							
							<!--functions to check geography & siblings	END-->	
                         
							if(notificationType == "New Hire" || notificationType == "US Benefits Eligible") 
							{								
								if(messageStatus && subjectStatus && messageTypestatus && datepicker1Status && datepicker2Status && datepicker3Status)
								{
									status = true;
								}
								else{
									status = false;
								}
								
							}
							
							if(notificationType == "Regular") 								
							{
							
								if(messageStatus && subjectStatus && messageTypestatus && datepicker1Status && datepicker2Status && datepicker3Status){
									if(rolesStatus || locationTypeStatus) {
										if(locationTypeStatus){
											if(locationType == "Building"){
                                              if(buildingStatus && officeStatus && cityStatus && stateStatus && countryStatus && geographyStatus){
                                                status = true;
											  }else{
												  status = false;
											  }
											}
											else if(locationType == "Office"){
                                              if(officeStatus && cityStatus && stateStatus && countryStatus && geographyStatus){
                                                status = true;
											  }else{
												  status = false;
											  }
											}
											else if(locationType == "City"){
                                              if(cityStatus && stateStatus && countryStatus && geographyStatus){
                                                status = true;
											  }else{
												  status = false;
											  }
											}
											else if(locationType == "Region"){
                                              if(stateStatus && countryStatus && geographyStatus){
                                                status = true;
											  }else{
												  status = false;
											  }
											}
											else if(locationType == "Country"){
                                              if(countryStatus && geographyStatus){
                                                status = true;
											  }else{
												  status = false;
											  }
											}
											else if(locationType == "Geography"){
                                              if(geographyStatus){
                                                status = true;
											  }else{
												  status = false;
											  }
											}
											else{
												 status = false;
											}
										}
										 
						if(rolesStatus && status == false && locationType == ""){
											
											status = true;
										}
									}
								}
							}
						if(status){
							 $("#editFormID").submit();
						 }
						 else{
								 return status;
						 }
	
});
<!-- Updated with validations - end-->
				 
				 var showMessageType =$("#messageTypeId").val();  
					   if(showMessageType != " ") 
	                  {		
					      if(showMessageType == "New Hire"){                                   
								   $("#rolesRow").hide();
								   $("#locationTypeRow").hide();
							   }
							   if(showMessageType=="US Benefits Eligible"){
								   $("#rolesRow").hide();
								   $("#locationTypeRow").hide();
							   }
							   if(showMessageType=="Regular"){
								   $("#rolesRow").show();
								   $("#locationTypeRow").show();
							   }
					   }

					   var showLocationType = $("#locType").val(); 
					   
			           if(showLocationType != " ") 
	                  {		 
						
					         if (showLocationType=="Country")
	                              {
									  $("#geographyRow").show();
									  $("#countryRow").show();									 
	                            	  $("#stateRow").hide();
									  $("#cityRow").hide();
									  $("#officeRow").hide();
									  $("#buildingRow").hide();									
	                              }
								  else if(showLocationType=="Geography"){
									   $("#geographyRow").show();
									  $("#countryRow").hide();
									  $("#stateRow").hide();
									  $("#cityRow").hide();
									  $("#officeRow").hide();
									  $("#buildingRow").hide();	
								  }
								  else if(showLocationType=="Region"){
									   $("#geographyRow").show();
									  $("#countryRow").show();
									  $("#stateRow").show();
									  $("#cityRow").hide();
									  $("#officeRow").hide();
									  $("#buildingRow").hide();	
								  }
								  else if(showLocationType=="City"){
									   $("#geographyRow").show();
									  $("#countryRow").show();
									  $("#stateRow").show();
									  $("#cityRow").show();										  
									  $("#officeRow").hide();
									  $("#buildingRow").hide();	
								  }
								   else if(showLocationType=="Office"){	
									  $("#geographyRow").show();
									  $("#countryRow").show();
									  $("#stateRow").show();
									  $("#cityRow").show();	
									   $("#officeRow").show();
									  $("#buildingRow").hide();	
								  }
								   else if(showLocationType=="Building"){
									  $("#geographyRow").show();
									  $("#countryRow").show();
									  $("#stateRow").show();
									  $("#cityRow").show();	
									  $("#officeRow").show();
									  $("#buildingRow").show();	
								  }
								  else {
									  $("#geographyRow").hide();
									  $("#countryRow").hide();
									  $("#stateRow").hide();
									  $("#cityRow").hide();
									  $("#officeRow").hide();
									  $("#buildingRow").hide();	
								  }
					  }
						
				$("#target1_container").find("ul").find("li").click(function(){

						$("#target3").remove();  
						$("#target3_container").remove(); 
						$("#target3_input").val($("#stateIdMessage").val()); 
						
						$("#target4").remove(); 
						$("#target4_container").remove();
						$("#target4_input").val($("#cityIdMessage").val()); 
	
						$("#target5").remove(); 
						$("#target5_container").remove();
						$("#target5_input").val($("#officeIdMessage").val()); 
	
						$("#target6").remove();
						$("#target6_container").remove();
						$("#target6_input").val($("#buildingIdMessage").val());
					
                           var geographyId;
                           var geoName= $(this).text();

							$("#target1 > option").each(function() {
                              if (geoName==$(this).text())
                              {
								  geographyId=this.value;
                              }
                            
                            });						             
									   if(geographyId !=0 ){
											var xhr = new XMLHttpRequest();										
											xhr.onreadystatechange = function() {
												if(xhr.readyState == 4) {						
													var messageText = document.getElementById("<portlet:namespace/>countryText");				
													messageText.innerHTML = xhr.responseText;
													$("#target2").focus();
												}
											};
											var url = '<portlet:resourceURL id="populateCountries"></portlet:resourceURL>' + '?fakeId=345&geoId='+ geographyId ;
											xhr.open("GET", url, true);
											xhr.send();		
									   }
						
						}); 
					

					
						$("#target2_container").find("ul").find("li").live('click',function() {
							
							$("#target4").remove(); 
							$("#target4_container").remove();
							$("#target4_input").val($("#cityIdMessage").val()); 

							$("#target5").remove(); 
							$("#target5_container").remove();
							$("#target5_input").val($("#officeIdMessage").val()); 

							$("#target6").remove();
							$("#target6_container").remove();
							$("#target6_input").val($("#buildingIdMessage").val());
							
							 var countryId;
	                           var countryName= $(this).text();

								$("#target2 > option").each(function() {
	                              if (countryName==$(this).text())
	                              {
	                            	  countryId=this.value;
	                              }
	                            
	                            });
						               if(countryId !=0 ){
										var xhr1 = new XMLHttpRequest();										
										xhr1.onreadystatechange = function() {
										if(xhr1.readyState == 4) {						
											var messageText1 = document.getElementById("<portlet:namespace/>stateText");				
											messageText1.innerHTML = xhr1.responseText;
											$("#target3").focus();
										}
										};
										var url1 = '<portlet:resourceURL id="populateStates"></portlet:resourceURL>' + '?fakeId=345&countryId='+ countryId ;
										xhr1.open("GET", url1, true);
										xhr1.send();	
									}
                             });						
				
						$("#target3_container").find("ul").find("li").live('click',function() {
							
							$("#target5").remove(); 
							$("#target5_container").remove();
							$("#target5_input").val($("#officeIdMessage").val()); 

							$("#target6").remove();
							$("#target6_container").remove();
							$("#target6_input").val($("#buildingIdMessage").val());
							
							 var stateId;
	                           var stateName= $(this).text();

								$("#target3 > option").each(function() {
	                              if (stateName==$(this).text())
	                              {
	                            	  stateId=this.value;
	                              }
	                            
	                            });
						
							      if(stateId !=0 ){
									var xhr2 = new XMLHttpRequest();
									
									xhr2.onreadystatechange = function() {
									if(xhr2.readyState == 4) {						
										var messageText2 = document.getElementById("<portlet:namespace/>cityText");				
										messageText2.innerHTML = xhr2.responseText;
										$("#target4").focus();
									}
									};
									var url2 = '<portlet:resourceURL id="populateCities"></portlet:resourceURL>' + '?fakeId=345&stateId='+ stateId ;
									xhr2.open("GET", url2, true);
									xhr2.send();
								}
                        });	
						
						$("#target4_container").find("ul").find("li").live('click',function() {
							
							$("#target6").remove();
							$("#target6_container").remove();
							$("#target6_input").val($("#buildingIdMessage").val());
							
							   var cityId;
	                           var cityName= $(this).text();
							   $("#target4 > option").each(function() {
	                              if (cityName==$(this).text())
	                              {
	                            	  cityId=this.value;
	                              }
	                            
	                            });
							   if(cityId != 0 ){
							   var xhr3 = new XMLHttpRequest();									
								xhr3.onreadystatechange = function() {
									if(xhr3.readyState == 4) {						
										var messageText3 = document.getElementById("<portlet:namespace/>officeText");				
										messageText3.innerHTML = xhr3.responseText;
										$("#target5").focus();
									}
								};
								var url3 = '<portlet:resourceURL id="populateOffices"></portlet:resourceURL>' + '?fakeId=345&cityId='+ cityName ;
								xhr3.open("GET", url3, true);
								xhr3.send();
								}
                        });	
						
						$("#target5_container").find("ul").find("li").live('click',function() {
							   var officeId;
	                           var officeName= $(this).text();
							   $("#target5 > option").each(function() {
	                              if (officeName==$(this).text())
	                              {
	                            	  officeId=this.value;
	                              }
	                            
	                            });
							   if(officeId != 0 ){
							   var xhr4 = new XMLHttpRequest();									
							   xhr4.onreadystatechange = function() {
									if(xhr4.readyState == 4) {						
										var messageText4 = document.getElementById("<portlet:namespace/>buildingText");				
										messageText4.innerHTML = xhr4.responseText;
										$("#target6").focus();
									}
								};
								var url4 = '<portlet:resourceURL id="populateBuildings"></portlet:resourceURL>' + '?fakeId=345&officeId='+ officeName ;
								xhr4.open("GET", url4, true);
								xhr4.send();	
							  }
                        });	


						$("#locationType_container").find("ul").find("li").live('click',function() {
							
	                           var locationType= $(this).text();
							      
							    if (locationType=="Country")
	                              {
									  $("#geographyRow").show();
									  $("#countryRow").show();									 
	                            	  $("#stateRow").hide();
									  $("#cityRow").hide();
									  $("#officeRow").hide();
									  $("#buildingRow").hide();									
	                              }
								  else if(locationType=="Geography"){
									   $("#geographyRow").show();
									  $("#countryRow").hide();
									  $("#stateRow").hide();
									  $("#cityRow").hide();
									  $("#officeRow").hide();
									  $("#buildingRow").hide();	
								  }
								  else if(locationType=="State"){
									   $("#geographyRow").show();
									  $("#countryRow").show();
									  $("#stateRow").show();
									  $("#cityRow").hide();
									  $("#officeRow").hide();
									  $("#buildingRow").hide();	
								  }
								  else if(locationType=="City"){
									   $("#geographyRow").show();
									  $("#countryRow").show();
									  $("#stateRow").show();
									  $("#cityRow").show();										  
									  $("#officeRow").hide();
									  $("#buildingRow").hide();	
								  }
								   else if(locationType=="Office"){	
									    $("#geographyRow").show();
									  $("#countryRow").show();
									  $("#stateRow").show();
									  $("#cityRow").show();	
									   $("#officeRow").show();
									  $("#buildingRow").hide();	
								  }
								   else if(locationType=="Building"){
									  $("#geographyRow").show();
									  $("#countryRow").show();
									  $("#stateRow").show();
									  $("#cityRow").show();	
									  $("#officeRow").show();
									  $("#buildingRow").show();	
								  }
								  else {
									  $("#geographyRow").hide();
									  $("#countryRow").hide();
									  $("#stateRow").hide();
									  $("#cityRow").hide();
									  $("#officeRow").hide();
									  $("#buildingRow").hide();	
								  }
																
                        });	


						$("#notificationType_container").find("ul").find("li").live('click',function() {
							
	                           var notificationType=$(this).text();	
							  
							   if(notificationType == "New Hire"){                                
								   $("#rolesRow").hide();
								   $("#locationTypeRow").hide();
								    $("#geographyRow").hide();
									  $("#countryRow").hide();
									  $("#stateRow").hide();
									  $("#cityRow").hide();
									  $("#officeRow").hide();
									  $("#buildingRow").hide();	
							   }
							   if(notificationType=="US Benefits Eligible"){
								   $("#rolesRow").hide();
								   $("#locationTypeRow").hide();
								    $("#geographyRow").hide();
									  $("#countryRow").hide();
									  $("#stateRow").hide();
									  $("#cityRow").hide();
									  $("#officeRow").hide();
									  $("#buildingRow").hide();	
							   }
							   if(notificationType=="Regular"){
								   $("#rolesRow").show();
								   $("#locationTypeRow").show();


								 var locationType= $("#locationType_input").val();
							     							    
							    if (locationType=="Country")
	                              {
									  $("#geographyRow").show();
									  $("#countryRow").show();									 
	                            	  $("#stateRow").hide();
									  $("#cityRow").hide();
									  $("#officeRow").hide();
									  $("#buildingRow").hide();									
	                              }
								  else if(locationType=="Geography"){
									   $("#geographyRow").show();
									  $("#countryRow").hide();
									  $("#stateRow").hide();
									  $("#cityRow").hide();
									  $("#officeRow").hide();
									  $("#buildingRow").hide();	
								  }
								  else if(locationType=="State"){
									   $("#geographyRow").show();
									  $("#countryRow").show();
									  $("#stateRow").show();
									  $("#cityRow").hide();
									  $("#officeRow").hide();
									  $("#buildingRow").hide();	
								  }
								  else if(locationType=="City"){
									   $("#geographyRow").show();
									  $("#countryRow").show();
									  $("#stateRow").show();
									  $("#cityRow").show();										  
									  $("#officeRow").hide();
									  $("#buildingRow").hide();	
								  }
								   else if(locationType=="Office"){	
									    $("#geographyRow").show();
									  $("#countryRow").show();
									  $("#stateRow").show();
									  $("#cityRow").show();	
									   $("#officeRow").show();
									  $("#buildingRow").hide();	
								  }
								   else if(locationType=="Building"){
									  $("#geographyRow").show();
									  $("#countryRow").show();
									  $("#stateRow").show();
									  $("#cityRow").show();	
									  $("#officeRow").show();
									  $("#buildingRow").show();	
								  }
								  else {
									  $("#geographyRow").hide();
									  $("#countryRow").hide();
									  $("#stateRow").hide();
									  $("#cityRow").hide();
									  $("#officeRow").hide();
									  $("#buildingRow").hide();	
								  }

							   }
							 	
                        });	
						
					});
</script>
<script type="text/javascript">
$('document').load('ajax/countyList.jsp', function() {
});
</script>

<portlet:actionURL var="updateNotificationURL">
	<portlet:param name="myaction" value="updateNotification"></portlet:param>
</portlet:actionURL>
<portlet:renderURL var="updateNotificationRenderURL">
	<portlet:param name="myaction" value="editNotificationURL"></portlet:param>
</portlet:renderURL>

<liferay-theme:defineObjects />
<form id="editFormID" method="post" action="${updateNotificationURL}"
	autocomplete="off"
	onsubmit="<portlet:namespace />extractCodeFromEditor()"
	name="<portlet:namespace />fm">

	<div class="aui-datepicker aui-helper-clearfix"
		id="#<portlet:namespace />startDatePicker">
		<input type="hidden" name="startDate"
			id="<portlet:namespace />startDate" size="30" />
	</div>

	<c:set var="geographyList" value='${geographyList}'></c:set>
	<c:set var="notificationVO" value='${notification}'></c:set>
	<c:set var="notificationForLocationsVOSet"
		value='${notificationVO.notificationForLocations}'></c:set>
	<%
		NotificationVO notification = (NotificationVO) pageContext
				.getAttribute("notificationVO");
		List<GeographyVO> geographyList = (List<GeographyVO>) pageContext
				.getAttribute("geographyList");
		String content = UnicodeFormatter.toString(notification
				.getNotificationDescription());
	%>

	<aui:input type="hidden" name="notificationId"
		value='${notification.notificationId}' />
	<aui:input type="hidden" name="notificationForLocationId"
		value='${notificationForLocationId}' />
	<div id="form_container" style="margin-left: 20px;">
		<table border="0" cellpadding="0" cellspacing="0" id="id-form">


			<tr>
				<th valign="top"><span class="mandetory"></span></th>
				<td><div id="1" class="error_message">
						<spring:message code="label.addeditnotification.info" />
					</div>
				</td>
				<td></td>
			</tr>
			<tr>
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.addeditnotification.notificationsubject" /></th>
				<td><input type="text"
					value='${notification.notificationSubject}' class="inp-form"
					name="notificationSubject" id="notificationSubject" maxlength="200" />
					<div id="subject_errors" class="error_message"
						style="display: none">
						<spring:message code="label.error.requiredfeild.blank" />
					</div>
				</td>
				<td></td>
			</tr>
			<tr>
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.addeditnotification.notificationdescription" /></th>
				<td><textarea id="notificationDescription" rows="" cols=""
						class="form-textarea" maxlength="1955"
						name="notificationDescription"><%=notification.getNotificationDescription()%></textarea>
					<div id="message_errors" class="error_message"
						style="display: none">
						<spring:message code="label.error.requiredfeild.blank" />
					</div>
				</td>

				<td>&nbsp;</td>
			</tr>

			<tr class="hasselect">
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.notification.notificationtype" />:</th>
				<td><input type="hidden"
					value="<%=notification.getNotificationType()%>" id="messageTypeId" />

					<select class="styledselect_form_1" name="notificationType"
					id="notificationType">
						<option value="0">
							<spring:message
								code="label.addeditnotification.select.notificationtype" />
						</option>
						<option
							<%=notification.getNotificationType().equals("Regular") ? "Selected"
					: ""%>
							value="Regular">Regular</option>
						<option
							<%=notification.getNotificationType().equals("New Hire") ? "Selected"
					: ""%>
							value="New Hire">New Hire</option>
						<option
							<%=notification.getNotificationType().equals(
					"US Benefits Eligible") ? "Selected" : ""%>
							value="US Benefits Eligible">US Benefits Eligible</option>
				</select>
					<div id="messageType_errors" class="error_message"
						style="display: none">
						<spring:message code="label.error.requiredfeild.blank" />
					</div></td>
				<td></td>
			</tr>

			<tr id="rolesRow">
				<th valign="top"><span class="mandetory"><spring:message
							code="label.doublemandatory" /> </span> <spring:message
						code="label.addeditnotification.roles" /></th>
				<td><select multiple="multiple" class="notify_roles"
					name="roles" id="roles" style="width: 330px; margin-left: -4px;">
						<%
							List<Role> lstRoles = new ArrayList<Role>();
							lstRoles = RoleLocalServiceUtil.getRoles(1, "");
							Set<RoleVO> roleVOSet = notification.getNotificationForRoles();
							for (Role role : lstRoles) {
								boolean isRolePresent = isRoleInList(roleVOSet, role);
								if (isRolePresent) {
						%>

						<option selected="true" value="<%=role.getRoleId()%>"><%=role.getName()%></option>

						<%
							} else {
						%>
						<option value="<%=role.getRoleId()%>"><%=role.getName()%></option>
						<%
							}
							}
						%>
				</select>
					<div id="roles_errors" class="error_message" style="display: none">
						<spring:message
							code="label.addeditnotification.error.rolelocationselection" />
					</div></td>
				<td></td>
			</tr>
			<tr id="usersRow">
				<th valign="top"><span class="mandetory"><spring:message
							code="label.doublemandatory" /> </span> <spring:message
						code="label.addeditnotification.users" /></th>
				<td>
					<div id="<portlet:namespace/>userText">
						<select multiple="multiple" class="notify_users" name="users"
							id="users" style="width: 330px; margin-left: -4px;">
							
							<%
							Set<UserVO> userVOSet = notification.getNotificationForUsers();
										for (UserVO userVO : userVOSet) {
											if (userVO.getFirstName() != null
													&& !userVO.getFirstName().isEmpty()) {
							%>
							<option selected="true" value="<%=userVO.getUserId()%>"><%=userVO.getFirstName()%></option>

							
							<%
							}
								}
							
						%>
						</select>
					</div> <%-- <div id="roles_errors" class="error_message" style="display: none">
						<spring:message
							code="label.addeditnotification.error.rolelocationselection" />
					</div> --%></td>
				<td></td>
			</tr>

			<%!private boolean isRoleInList(Set<RoleVO> roleVOSet, Role role) {
		boolean roleSelected = false;
		for (RoleVO roleVO : roleVOSet) {
			if (roleVO.getRoleId().equals(Long.valueOf(role.getRoleId()))) {
				roleSelected = true;
				break;
			}
		}
		return roleSelected;
	}%>




			<tr id="locationTypeRow" class="hasselect">
				<th valign="top"><span class="mandetory"><spring:message
							code="label.doublemandatory" /> </span> <spring:message
						code="label.addeditnotification.locationtype" /></th>
				<td><c:if test="${empty locationType}">
						<select class="styledselect_form_1" name="locationType"
							id="locationType">
							<option value="">
								<spring:message
									code="label.addeditnotification.select.locationtype" />
							</option>
							<option value="Geography">Geography</option>
							<option value="Country">Country</option>
							<option value="Region">State</option>
							<option value="City">City</option>
							<option value="Office">Office</option>
							<option value="Building">Building</option>
						</select>
						<div id="locationType_errors" class="error_message"
							style="display: none">
							<spring:message
								code="label.addeditnotification.error.rolelocationselection" />
						</div>
					</c:if> <c:if test="${not empty locationType}">
						<c:set var="locationType" value='${locationType}'></c:set>
						<%
							String locationType = (String) pageContext
										.getAttribute("locationType");
						%>
						<input type="hidden" value=<%=locationType%> id="locType" />
						<select class="styledselect_form_1" name="locationType"
							id="locationType">
							<option value="">
								<spring:message
									code="label.addeditnotification.select.locationtype" />
							</option>
							<option <%=locationType.equals("Geography") ? "Selected" : ""%>
								value="Geography">Geography</option>
							<option <%=locationType.equals("Country") ? "Selected" : ""%>
								value="Country">Country</option>
							<option <%=locationType.equals("Region") ? "Selected" : ""%>
								value="Region">State</option>
							<option <%=locationType.equals("City") ? "Selected" : ""%>
								value="City">City</option>
							<option <%=locationType.equals("Office") ? "Selected" : ""%>
								value="Office">Office</option>
							<option <%=locationType.equals("Building") ? "Selected" : ""%>
								value="Building">Building</option>
						</select>
						<div id="locationType_errors" class="error_message"
							style="display: none">
							<spring:message code="label.error.requiredfeild.blank" />
						</div>
					</c:if>
				</td>
				<td></td>
			</tr>

			<tr id="geographyRow" class="hasselect">
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.addeditnotification.geography" /></th>
				<td><c:if test="${not empty geographyList}">
						<select name="geographyId" id="target1"
							class="styledselect_form_1">
							<option value="0">
								<spring:message
									code="label.addeditnotification.select.geographylist" />
							</option>
							<c:forEach var="geography" items="${geographyList}">
								<c:set var="geography" value='${geography}'></c:set>

								<%
									GeographyVO geographyVO = (GeographyVO) pageContext
													.getAttribute("geography");
								%>
								<c:if test="${geography.geographyId eq selectedGeographyId}">
									<option selected="true"
										value="<%=geographyVO.getGeographyId()%>"><%=geographyVO.getGeographyName()%>
									</option>
								</c:if>
								<c:if test="${geography.geographyId ne selectedGeographyId}">
									<option value="<%=geographyVO.getGeographyId()%>"><%=geographyVO.getGeographyName()%>
									</option>
								</c:if>
							</c:forEach>
						</select>
						<div id="geography_errors" class="error_message"
							style="display: none">
							<spring:message code="label.error.requiredfeild.blank" />
						</div>
					</c:if></td>
			</tr>

			<tr id="countryRow" class="hasselect">
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.addeditnotification.country" /></th>
				<td>
					<div id="<portlet:namespace/>countryText">
						<c:if test="${empty countryList}">
							<Select name="countryId" id="target2" class="styledselect_form_1">
								<option value="0">
									<spring:message
										code="label.addeditnotification.select.countrylist" />
								</option>
							</Select>
							<div id="country_errors" class="error_message"
								style="display: none">
								<spring:message code="label.error.requiredfeild.blank" />
							</div>
						</c:if>
						<c:if test="${not empty countryList}">
							<Select name="countryId" id="target2" class="styledselect_form_1">
								<option value="0">
									<spring:message code="label.addeditnotification.select.country" />
								</option>
								<c:forEach var="country" items="${countryList}">
									<c:set var="country" value='${country}'></c:set>
									<%
										Country country = (Country) pageContext
														.getAttribute("country");
									%>
									<c:if test="${country.countryId eq selectedCountryId}">
										<option selected="true" value="<%=country.getCountryId()%>"><%=country.getName()%>
										</option>
									</c:if>
									<c:if test="${country.countryId ne selectedCountryId}">
										<option value="<%=country.getCountryId()%>"><%=country.getName()%>
										</option>
									</c:if>
								</c:forEach>
							</Select>
							<div id="country_errors" class="error_message"
								style="display: none">
								<spring:message code="label.error.requiredfeild.blank" />
							</div>
						</c:if>

					</div>
				</td>
			</tr>

			<tr id="stateRow" class="hasselect">
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.addeditnotification.state" /></th>
				<td>
					<div id="<portlet:namespace/>stateText">

						<c:if test="${empty stateList}">
							<Select name="stateId" id="target3" class="styledselect_form_1">
								<option value="0">
									<spring:message
										code="label.addeditnotification.select.statelist" />
								</option>
							</Select>
							<div id="state_errors" class="error_message"
								style="display: none">
								<spring:message code="label.error.requiredfeild.blank" />
							</div>
						</c:if>
						<c:if test="${not empty stateList}">
							<Select name="stateId" id="target3" class="styledselect_form_1">
								<option value="">
									<spring:message code="label.addeditnotification.select.state" />
								</option>
								<c:forEach var="region" items="${stateList}">
									<c:set var="region" value='${region}'></c:set>

									<%
										Region region = (Region) pageContext.getAttribute("region");
									%>
									<c:if test="${region.regionId eq selectedStateId}">
										<option selected="true" value="<%=region.getRegionId()%>"><%=region.getName()%>
										</option>
									</c:if>
									<c:if test="${region.regionId ne selectedStateId}">
										<option value="<%=region.getRegionId()%>"><%=region.getName()%>
										</option>
									</c:if>
								</c:forEach>
							</Select>
						</c:if>

					</div>
					<div id="state_errors" class="error_message" style="display: none">
						<spring:message code="label.error.requiredfeild.blank" />
					</div></td>
			</tr>

			<tr id="cityRow" class="hasselect">
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.addeditnotification.city" /></th>
				<td>
					<div id="<portlet:namespace/>cityText">

						<c:if test="${empty cityList}">
							<Select name="cityName" id="target4" class="styledselect_form_1">
								<option value="0">
									<spring:message
										code="label.addeditnotification.select.citylist" />
								</option>
							</Select>
							<div id="city_errors" class="error_message" style="display: none">
								<spring:message code="label.error.requiredfeild.blank" />
							</div>
						</c:if>
						<c:if test="${not empty cityList}">
							<Select name="cityName" id="target4" class="styledselect_form_1">
								<option value="">
									<spring:message code="label.addeditnotification.select.city" />
								</option>
								<c:forEach var="city" items="${cityList}">
									<c:set var="city" value='${city}'></c:set>
									<%
										String city = (String) pageContext.getAttribute("city");
									%>
									<c:forEach var="messageForLocationsVO"
										items="${notificationForLocationsVOSet}">
										<c:if test="${city eq messageForLocationsVO.city}">
											<option selected="true" value="<%=city%>"><%=city%>
											</option>
										</c:if>
										<c:if test="${city ne messageForLocationsVO.city}">
											<option value="<%=city%>"><%=city%>
											</option>
										</c:if>
									</c:forEach>
								</c:forEach>
							</Select>
						</c:if>

					</div>
					<div id="city_errors" class="error_message" style="display: none">
						<spring:message code="label.error.requiredfeild.blank" />
					</div></td>
			</tr>

			<tr id="officeRow" class="hasselect">
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.addeditnotification.office" /></th>
				<td>
					<div id="<portlet:namespace/>officeText">

						<c:if test="${empty officesList}">
							<Select name="officeName" id="target5"
								class="styledselect_form_1">
								<option value="0">
									<spring:message
										code="label.addeditnotification.select.officelist" />
								</option>
							</Select>
							<div id="office_errors" class="error_message"
								style="display: none">
								<spring:message code="label.error.requiredfeild.blank" />
							</div>
						</c:if>
						<c:if test="${not empty officesList}">
							<Select name="officeName" id="target5"
								class="styledselect_form_1">
								<option value="">
									<spring:message code="label.addeditnotification.select.office" />
								</option>
								<c:forEach var="office" items="${officesList}">
									<c:set var="office" value='${office}'></c:set>
									<%
										String office = (String) pageContext.getAttribute("office");
									%>
									<c:if test="${office eq selectedOffice}">
										<option selected="true" value="<%=office%>"><%=office%>
										</option>
									</c:if>
									<c:if test="${office ne selectedOffice}">
										<option value="<%=office%>"><%=office%>
										</option>
									</c:if>

								</c:forEach>
							</Select>
						</c:if>

					</div>
					<div id="office_errors" class="error_message" style="display: none">
						<spring:message code="label.error.requiredfeild.blank" />
					</div></td>
			</tr>

			<tr id="buildingRow" class="hasselect">
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.addeditnotification.building" /></th>
				<td>
					<div id="<portlet:namespace/>buildingText">

						<c:if test="${empty buildingsList}">
							<Select name="buildingName" id="target6"
								class="styledselect_form_1">
								<option value="0">
									<spring:message
										code="label.addeditnotification.select.buildinglist" />
								</option>
							</Select>
							<div id="building_errors" class="error_message"
								style="display: none">
								<spring:message code="label.error.requiredfeild.blank" />
							</div>
						</c:if>
						<c:if test="${not empty buildingsList}">
							<Select name="buildingName" id="target6"
								class="styledselect_form_1">
								<option value="">
									<spring:message
										code="label.addeditnotification.select.building" />
								</option>
								<c:forEach var="building" items="${buildingsList}">
									<c:set var="building" value='${building}'></c:set>
									<%
										String building = (String) pageContext
														.getAttribute("building");
									%>
									<c:forEach var="messageForLocationsVO"
										items="${messageForLocationsVOSet}">
										<c:if
											test="${building eq notificationForLocationsVO.locationName}">
											<option selected="true" value="<%=building%>"><%=building%>
											</option>
										</c:if>
										<c:if
											test="${building ne notificationForLocationsVO.locationName}">
											<option value="<%=building%>"><%=building%>
											</option>
										</c:if>
									</c:forEach>
								</c:forEach>
							</Select>
						</c:if>

					</div>
					<div id="building_errors" class="error_message"
						style="display: none">
						<spring:message code="label.error.requiredfeild.blank" />
					</div></td>
			</tr>

			<tr>
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.addeditnotification.url" /></th>
				<td><input type="text" value='${notification.notificationUrl}'
					class="inp-form" name="notificationUrl" id="notificationUrl"
					maxlength="200" />
					<div id="url_errors" class="error_message" style="display: none">
						<spring:message code="label.error.requiredfeild.blank" />
					</div>
				</td>
				<td></td>
			</tr>


			<%-- <%
						DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
						Date displayDate = new Date();
						Calendar defaultValueDate = Calendar.getInstance();
						if (personalMessage.getDisplayDate() != null) {
							displayDate = (Date) formatter.parse(personalMessage
									.getDisplayDate());
						}
						defaultValueDate.setTime(displayDate);
					%> Date : <%=defaultValueDate.get(Calendar.DATE)%> Month : <%=defaultValueDate.get(Calendar.MONTH)%>
					Year : <%=defaultValueDate.get(Calendar.YEAR)%> <c:out
						value="<%= (defaultValueDate.get(Calendar.DATE) != 0) %>"></c:out> --%>


			<tr>
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.notification.displaydate" />:</th>
				<td class="noheight">

					<div class="demo">

						<p>
							<input type="text" value="<%=notification.getDisplayDate()%>"
								name="displayDate" id="datepicker1" class="inp-form-calendar"
								style="margin-right: 4px;" readonly="readonly" />
						<div id="datepicker1_errors" class="error_message"
							style="display: none">
							<spring:message code="label.error.requiredfeild.blank" />
						</div>

						</p>

					</div></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.notification.expirationdate" />:</th>
				<td class="noheight">

					<div class="demo">

						<p>
							<input type="text" value="<%=notification.getExpirationDate()%>"
								name="expirationDate" id="datepicker2" class="inp-form-calendar"
								style="margin-right: 4px;" readonly="readonly">
						<div id="datepicker2_errors" class="error_message"
							style="display: none">
							<spring:message code="label.error.requiredfeild.blank" />
						</div>
						<div id="datepicker3_errors" class="error_message"
							style="display: none">
							<spring:message code="label.addeditnotification.error.exiprydate" />
						</div>
						</p>
					</div></td>
				<td></td>
			</tr>
		</table>
		<br /> <input type="hidden" value="" id="pmEditActionName"
			name="editNotification" />
		<table width="100%">

			<tr>
				<td valign="top"><input type="submit" id="submitId"
					value="<spring:message code="label.submit" />"
					name="editNotification"
					style="background-color: black; color: white; padding: 1px; margin-left: 4px; height: 34px; width: 94px; border: none;" />
					<input type="submit" id="cancelId"
					value="<spring:message code="label.cancel" />"
					name="editNotification"
					style="background-color: black; color: white; padding: 1px; margin-left: 4px; height: 34px; width: 94px; border: none;" />
				</td>
			</tr>
		</table>
		<input type="hidden"
			value="<spring:message code='label.addeditnotification.select.statelist'/>"
			id="stateIdMessage" /> <input type="hidden"
			value="<spring:message code='label.addeditnotification.select.citylist'/>"
			id="cityIdMessage" /> <input type="hidden"
			value="<spring:message code='label.addeditnotification.select.officelist'/>"
			id="officeIdMessage" /> <input type="hidden"
			value="<spring:message code='label.addeditnotification.select.buildinglist'/>"
			id="buildingIdMessage" />
</form>



