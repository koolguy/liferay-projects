<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@ page import="java.util.List"%>
<%@ page import="com.coach.cip.common.dto.EmployeeProfileVO"%>
<%@ page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>





<portlet:defineObjects />
<liferay-theme:defineObjects />

<portlet:actionURL var="promotionsEmployeeProfileUrl">
	<portlet:param name="myaction" value="promotionsEmployeeProfilePage"></portlet:param>
</portlet:actionURL>


<fmt:bundle basename="content/Language">
	<form:form name="promotionsEmployeeProfileForm" method="post"
		id="promotionsEmployeeProfileFormId" commandName="employeeProfileVO"
		action="${promotionsEmployeeProfileUrl}">

		<c:set var="employeeProfileVO" value="${searchedEmpObject}"></c:set>
		<c:set var="FaultMessage" value='${FaultMessage}' />
		<c:set var="empRedirectURL" value='${empRedirectURL}' />
		<div id="form_container2">
			<header class="portlet-topper" style="background-color: #EB3528;">
			<h1 class="portlet-title">
				<fmt:message key='label.employee.directory.search.detail.header' />
			</h1>
			</header>
			<c:if test="${not empty employeeProfileVO}">
				<table class="tableEmployee">

					<tr>
						<td>&nbsp;</td>
					</tr>
					<tr style="vertical-align: top;">
						<td style="padding-left: 10px;" width="35%">
							<table>
								<tr>
									<th style="font-size: 16px;" colspan="2" valign="top"><c:out
											value="${employeeProfileVO.empName}"></c:out>
									</th>
								</tr>
								<tr>
									<td colspan="2"><c:out
											value="${employeeProfileVO.designation}"></c:out></td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
								<tr>
									<td>
										<table>
											<tr>
												<th valign="top" style="padding-right: 13px;"><fmt:message
														key='label.employee.directory.search.detail.phone' />
												</th>
												<td><c:out value="${employeeProfileVO.empPhone}"></c:out>
												</td>
											</tr>
										</table>
									</td>
								</tr>

								<tr>
									<td>
										<table>
											<tr>
												<th valign="top" style="padding-right: 17px;"><fmt:message
														key='label.employee.directory.search.detail.email' />
												</th>
												<td><a
													href='mailto:<c:out	value="${employeeProfileVO.emailAddress}"></c:out>'><c:out
															value="${employeeProfileVO.emailAddress}"></c:out> </a></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td colspan="2">&nbsp;</td>
								</tr>
								<tr>
									<th colspan="2" valign="top"><fmt:message
											key='label.employee.directory.search.detail.location' />
									</th>
								</tr>
								<tr>
									<td colspan="2"><c:out value="${employeeProfileVO.office}" />
										<c:if
											test="${not empty employeeProfileVO.office && not empty employeeProfileVO.buildingName}">
											<c:out value="-" />
										</c:if> <c:out value="${employeeProfileVO.buildingName}" /> <c:if
											test="${not empty employeeProfileVO.office || not empty employeeProfileVO.buildingName}">
											<br />
										</c:if> <c:out value="${employeeProfileVO.address1}" /> <c:if
											test="${not empty employeeProfileVO.address2}">
											<br />
										</c:if> <c:out value="${employeeProfileVO.address2}" /> <c:if
											test="${not empty employeeProfileVO.address3}">
											<br />
										</c:if> <c:out value="${employeeProfileVO.address3}" /> <c:if
											test="${not empty employeeProfileVO.floor}">
											<br />
										</c:if> <c:out value="${employeeProfileVO.floor}" /> <c:if
											test="${not empty employeeProfileVO.city ||not empty employeeProfileVO.state ||not empty employeeProfileVO.zip}">
											<br />
										</c:if> <c:out value="${employeeProfileVO.city}" /> <c:if
											test="${not empty employeeProfileVO.state}">
											<c:out value=", " />
										</c:if> <c:out value="${employeeProfileVO.state}" /> <c:if
											test="${not empty employeeProfileVO.zip}">
											<c:out value=" " />
										</c:if> <c:out value="${employeeProfileVO.zip}" />
									</td>
								</tr>
								<tr>
									<td colspan="2"><c:out value=""></c:out></td>
								</tr>


							</table></td>
						<td width="20%">
							<table>
								<tr>
									<th valign="top"><fmt:message
											key='label.employee.directory.search.detail.department' />
									</th>
								</tr>
								<tr>
									<td><c:out value="${employeeProfileVO.empDepartment}"></c:out>
									</td>
								</tr>

								<tr>
									<td>&nbsp;</td>
								</tr>

								<tr>
									<th valign="top"><fmt:message
											key='label.employee.directory.search.detail.manager' />
									</th>
								</tr>
								<tr>
									<td><c:if test="${not empty employeeProfileVO.managerId}">
											<a
												href="<portlet:actionURL>
											<portlet:param name="myaction" value="getEmployeeProfile" />
											<portlet:param name="empId" value="${employeeProfileVO.managerId}" />
										 </portlet:actionURL>">
												<c:out value="${employeeProfileVO.managerFirstName}" /> <c:out
													value="${employeeProfileVO.managerLastname}" /> </a>
										</c:if> <c:if test="${empty employeeProfileVO.managerId}">
											<c:out value="${employeeProfileVO.managerFirstName}" />
											<c:out value="${employeeProfileVO.managerLastname}" />
										</c:if></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<th valign="top"><fmt:message
											key='label.employee.directory.search.detail.direct.reports' />
									</th>
								</tr>

								<tr>
									<td><c:forEach var="directReport"
											items="${employeeProfileVO.directReports}">
											<c:if test="${not empty directReport.directReportId}">
												<a
													href="<portlet:actionURL>
											<portlet:param name="myaction" value="getEmployeeProfile" />
											<portlet:param name="empId" value="${directReport.directReportId}" />
										 </portlet:actionURL>">
													<c:out value="${directReport.directReportFirstName}" /> <c:out
														value="${directReport.directReportLastName}" /> </a>
												<br />
											</c:if>

											<c:if test="${empty directReport.directReportId}">
												<c:out value="${directReport.directReportFirstName}" />
												<c:out value="${directReport.directReportLastName}" />
												<br />
											</c:if>

										</c:forEach></td>
								</tr>

							</table></td>

						<td width="45%">
							<table>
								<tr>
									<th valign="top"><c:out value=" "></c:out></th>
								</tr>
							</table></td>


					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>

					<tr>
						<td colspan="3" style="padding: 10px 0px 10px 10px;"><input
							type="button" class="submitClass" value="BACK"
							onClick="history.go(-1);" /></td>
					</tr>

					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>
				</table>
			</c:if>
			<c:if test="${not empty FaultMessage}">
				<table height="200">
					<tr>
						<td class="employee-inp-form-td"
							style="font-size: 18px; color: red;"><spring:message
								code='${FaultMessage}' />
						</td>
					</tr>
					<tr>
						<td class="employee-inp-form-td"><input type="button"
							class="submitClass" value="BACK" onClick="history.go(-1);" />
						</td>
					</tr>
				</table>
			</c:if>
		</div>
	</form:form>
</fmt:bundle>