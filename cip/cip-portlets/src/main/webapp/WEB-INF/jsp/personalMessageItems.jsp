<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="java.util.Map"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@page import="com.coach.cip.common.dto.PersonalMessageVO"%>
<%@page import="com.coach.cip.util.Constants"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.kernel.util.StringUtil"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="com.liferay.portal.webserver.WebServerServletTokenUtil"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticleDisplay"%>
<%@page
	import="com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	PersonalMessageVO personalMessage = (PersonalMessageVO) row.getObject();
	
%>

<table>
	<tr>
		<td style="padding: 1px;color:#EB3528"><c:out value="<%=personalMessage.getDisplayDate().toUpperCase() %>"></c:out>
		</td>
	</tr>

	<tr>
		<td style="padding: 0px;"><%=personalMessage.getModifiedMessage()%></td>
	</tr>
	<tr>
		<td style="padding: 1px;"><a
			style="font-weight: bold; color: black;"
			href="<portlet:actionURL>
							<portlet:param name="myaction" value="personalMessageDetails"/>
							<portlet:param name="messageId" value="<%=personalMessage.getMessageId().toString() %>"/>
				</portlet:actionURL>"><span class="learnMoreBtn"><spring:message code='hyperlink-learn-more'/> &gt;&gt;</span></a></td>
	</tr>
</table>

