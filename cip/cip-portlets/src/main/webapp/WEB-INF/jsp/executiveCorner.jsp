<%@page import="com.liferay.portlet.journal.service.JournalStructureLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.PropsUtil"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="com.liferay.portlet.journal.model.JournalArticle"%>
<%@ page import="java.util.List"%>
<%@ page import="com.coach.cip.util.WebUtil"%>
<%@ page import="com.coach.cip.util.Constants" %>
<%@ page import="com.liferay.portal.kernel.xml.SAXReaderUtil" %>
<%@ page import="com.liferay.portal.kernel.xml.Document" %>
<%@ page import="com.liferay.portal.kernel.util.StringUtil" %>
<%@ page import="java.util.ResourceBundle" %>
<%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<div id="executiveCornerDiv">
	
	<%
		ResourceBundle resourceBundle = ResourceBundle.getBundle("content/Language", themeDisplay.getLocale());
		String portletTitle = resourceBundle.getString("title.executive.corner");
		renderResponse.setTitle(portletTitle);
		
		long groupId = themeDisplay.getScopeGroupId();
		String redirect = renderResponse.createRenderURL().toString();
		String articleId = null;
		long id = 0l;
		String imageUrl = null;
		String docUrl = null;
		String contentUrl = null;
		String title = null;
		String abstractTitle = null;
		String structName = null;
		boolean renderArticle = false;	
		JournalArticle journalArticle = null;
		List<JournalArticle> journalArticlesListByType = WebUtil.getArticlesLatestByCategory(renderRequest, renderResponse, Constants.EXECUTIVE_CORNER_ARTICLE_TYPE);
		if(Validator.isNotNull(journalArticlesListByType) && journalArticlesListByType.size() > 0)
		{
			for(JournalArticle  journalArticleTemp: journalArticlesListByType){
				try{
					structName = JournalStructureLocalServiceUtil.getStructure(themeDisplay.getScopeGroupId(),journalArticleTemp.getStructureId()).getNameCurrentValue();
					if(structName!= null && structName.equalsIgnoreCase(PropsUtil.get("portlet.executive-vision-local.structure.name"))){
						journalArticle = journalArticleTemp;	
					}
				}
				catch(Exception e){
					e.getMessage();
				}
			}
		  }
		if(journalArticle != null){	
					//articleId = journalArticle.getArticleId();
					id = journalArticle.getId();
					Document document = SAXReaderUtil.read(journalArticle.getContent());
					imageUrl = document.selectSingleNode(Constants.DYNAMIC_ELEMENT_NAME + Constants.IMAGE_URL + Constants.DYNAMIC_CONTENT).getText();
					docUrl = document.selectSingleNode(Constants.DYNAMIC_ELEMENT_NAME + Constants.DOC_URL + Constants.DYNAMIC_CONTENT).getText();
					contentUrl = document.selectSingleNode(Constants.DYNAMIC_ELEMENT_NAME + Constants.CONTENT_URL + Constants.DYNAMIC_CONTENT).getText();
					title = journalArticle.getTitle(locale);
					abstractTitle = journalArticle.getDescription(locale);
					abstractTitle = StringUtil.shorten(abstractTitle, 130);
			
%>
<div class="articleDiv">
	<div class="journal-content-article"> 
		<div> 
			<% if(imageUrl !=null && (!imageUrl.equals(""))){ %>
			<p> 
				<img alt="" src="<%=imageUrl%>" class="image"> 
			</p> 
			<div style="margin-left: 117px;">
			<%}else{%>
			<div>
			<%}%>
			
				<div style="margin-bottom:6px;">
					<span class="title"> 
						<%=title.toUpperCase()%> 
					</span>  
				</div>
				<div style="margin-bottom:6px;">
				<p> 
					<span class="abstractTitle"> 
						<%=abstractTitle.toUpperCase()%>
					</span>
				</p> 
				</div>
				<div>
					<a href="<portlet:actionURL>
										<portlet:param name="myaction" value="showArticle" />
										<portlet:param name="articleId" value="<%=String.valueOf(id)%>" />
										<portlet:param name="groupId" value="<%=String.valueOf(groupId)%>" />
										<portlet:param name="redirect" value="<%=redirect%>" />
								 </portlet:actionURL>" >
								<span class="learnMoreBtn">
							 		<%if(themeDisplay.getScopeGroupName().equalsIgnoreCase(PropsUtil.get("coach.cip.organization.name"))){
									 %>
									  <spring:message code='hyperlink-learn-more'/>
									 <%}else{%>
									 <spring:message code='hyperlink-more'/>
									 <%} %>
									 <img src="<%=themeDisplay.getPathThemeCss()%>/../images/right_arrow_small.png"  alt=">>" ></span></a>
					<br/>
				</div>
			</div> 
		</div> 
	</div>
</div>
<br/>

<%		
		}
		else
		{	
%>
			<div class="validationMsg">
			<b><spring:message code='validation.message'/></b>
			</div>
			<br>
<%
		}
%>

</div>

<style type="text/css">

#executiveCornerDiv {
height:155px;
}

#executiveCornerDiv .articleDiv{
	margin-left:5px;
	margin-right:5px;
	height:155px;
	overflow:auto;
}

#executiveCornerDiv .image{
	
	float:left;
}

#executiveCornerDiv .title{
	font-family: Verdana;
	font-size: 14px;
	font-weight:bold;
}

#executiveCornerDiv .abstractTitle{
	font-family: Verdana;
	font-size: 12px;
}

#executiveCornerDiv a{
	text-decoration: none;
	color:black;
}

#executiveCornerDiv .learnMoreBtn{
	font-family: Verdana;
	font-size: 12px;
	font-weight:bold;
	margin-right:4px;
}

#executiveCornerDiv .validationMsg{
	margin-left:10px;
	margin-right:10px;
}

</style>
	

