<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="aui" uri="http://liferay.com/tld/aui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="java.util.List"%>
<%@ page import="com.liferay.portal.kernel.util.CalendarFactoryUtil"%>
<%@ page import="java.util.Calendar"%>
<%@ page import="com.liferay.portal.service.RoleLocalServiceUtil"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="com.liferay.portal.model.Role"%>
<%@ page import="com.liferay.portal.util.PortalUtil"%>
<%@ page import="com.liferay.portal.model.Region"%>
<%@ page import="com.coach.cip.common.dto.RegionVO"%>
<%@ page import="com.coach.cip.common.dto.RoleVO"%>
<%@ page import="com.coach.cip.common.dto.PersonalMessageVO"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.coach.cip.common.dto.GeographyVO"%>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.service.RegionServiceUtil"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<link rel="stylesheet"
	href="<%=themeDisplay.getPathThemeCss()%>/screen.css" type="text/css"
	media="screen" title="default" />

<link rel="stylesheet" type="text/css"
	href="<%=themeDisplay.getPathThemeCss()%>/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css"
	href="<%=themeDisplay.getPathThemeCss()%>/jquery.multiselect.filter.css" />

<link rel="stylesheet" type="text/css"
	href="<%=themeDisplay.getPathThemeCss()%>/jquery-ui.css" />
	
<!--  date picker script -->
<link rel="stylesheet"
	href="<%=themeDisplay.getPathThemeCss()%>/datePicker.css" type="text/css" />

<style>
tr.hasselect td input{
margin-left: -4px
}
</style>
<!--  jquery core -->
<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery-ui-1.8.19.custom.min.js"></script>

<script>
	$(function() {
		$( "#datepicker1" ).datepicker({
			showOn: "button",
			minDate: 0,
			buttonImage:"<%=themeDisplay.getPathThemeCss()%>/../images/icon_calendar.jpg",
			buttonImageOnly: true
		});
	});
	
	$(function() {
		$( "#datepicker2" ).datepicker({
			showOn: "button",
			minDate: 0,
			buttonImage: "<%=themeDisplay.getPathThemeCss()%>/../images/icon_calendar.jpg",
			buttonImageOnly: true
		});
	});

</script>
<!--  checkbox styling script -->
<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.multiselect.js"></script>
<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.multiselect.filter.js"></script>
<script src="<%=themeDisplay.getPathThemeCss()%>/../js/ui.core.js"
	type="text/javascript"></script>
<script src="<%=themeDisplay.getPathThemeCss()%>/../js/ui.checkbox.js"
	type="text/javascript"></script>
<script src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.bind.js"
	type="text/javascript"></script>

<script type="text/javascript">
$(document).ready(function(){

  $(".multiSelect").multiselect().multiselectfilter();
});
</script>

<script type="text/javascript">
$(function(){
	$('input').checkBox();
	$('#toggle-all').click(function(){
 	$('#toggle-all').toggleClass('toggle-checked');
	$('#mainform input[type=checkbox]').checkBox('toggle');
	return false;
	});
});
</script>





<!--  styled select box script version 2 -->
<script
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.selectbox-0.5_style_2.js"
	type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	$("#target2").live('focus',function(){$(this).selectbox({ inputClass: "styledselect_form_1" });});
	$("#target3").live('focus',function(){$(this).selectbox({ inputClass: "styledselect_form_1" });});
	$("#target4").live('focus',function(){$(this).selectbox({ inputClass: "styledselect_form_1" });});
	$("#target5").live('focus',function(){$(this).selectbox({ inputClass: "styledselect_form_1" });});
	$("#target6").live('focus',function(){$(this).selectbox({ inputClass: "styledselect_form_1" });});
	$('.styledselect_form_1').selectbox({ inputClass: "styledselect_form_1" });
    $('.styledselect_form_2').selectbox({ inputClass: "styledselect_form_2" });
});
</script>





<script src="<%=themeDisplay.getPathThemeCss()%>/../js/date.js"
	type="text/javascript"></script>
<script
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.datePicker.js"
	type="text/javascript"></script>
<script type="text/javascript" charset="utf-8">
        $(function()
{

// initialise the "Select date" link
$('#date-pick')
	.datePicker(
		// associate the link with a date picker
		{
			createButton:false,
			startDate:'01/01/2005',
			endDate:'31/12/2020'
		}
	).bind(
		// when the link is clicked display the date picker
		'click',
		function()
		{
			updateSelects($(this).dpGetSelected()[0]);
			$(this).dpDisplay();
			return false;
		}
	).bind(
		// when a date is selected update the SELECTs
		'dateSelected',
		function(e, selectedDate, $td, state)
		{
			updateSelects(selectedDate);
		}
	).bind(
		'dpClosed',
		function(e, selected)
		{
			updateSelects(selected[0]);
		}
	);
	
var updateSelects = function (selectedDate)
{
	var selectedDate = new Date(selectedDate);
	$('#d option[value=' + selectedDate.getDate() + ']').attr('selected', 'selected');
	$('#m option[value=' + (selectedDate.getMonth()+1) + ']').attr('selected', 'selected');
	$('#y option[value=' + (selectedDate.getFullYear()) + ']').attr('selected', 'selected');
}
// listen for when the selects are changed and update the picker
$('#d, #m, #y')
	.bind(
		'change',
		function()
		{
			var d = new Date(
						$('#y').val(),
						$('#m').val()-1,
						$('#d').val()
					);
			$('#date-pick').dpSetSelected(d.asString());
		}
	);

// default the position of the selects to today
var today = new Date();
updateSelects(today.getTime());

// and update the datePicker to reflect it...
$('#d').trigger('change');
});
</script>

<!-- MUST BE THE LAST SCRIPT IN <HEAD></HEAD></HEAD> png fix -->




<script type="text/javascript">
	$(document).ready(function() {
		 	
						$('textarea[maxlength]').live('keyup change', function() {
						    var str = $(this).val()
							var mx = parseInt($(this).attr('maxlength')-5)
						    if (str.length > mx) {
							   $(this).val(str.substr(0, mx))
						       return false;
						    }
					    });
		 
						$("#geographyRow").hide();
                        $("#countryRow").hide();
						$("#stateRow").hide();
						$("#cityRow").hide();
						$("#officeRow").hide();
					    $("#buildingRow").hide();	
						$("#rolesRow").hide();
						$("#locationTypeRow").hide();


						$("#cancelId").click(function(){
							$("#pmAddActionName").val("Cancel");
                      	    $("#formID").submit();
                        }); 
						
						$("#resetId").click(function(){
							$("#pmAddActionName").val("Reset");
							$("#formID").submit();
	                        }); 
						/* Start of validation on click of Submit button */
						$("#submitId").click(function(){
                            
				            var status = false;	
							var messageTypestatus = false;
							var messageStatus = false;
							var subjectStatus = false;
							var datepicker1Status = false;
							var datepicker2Status = false;
							var rolesStatus = false;
							var geographyStatus = false;
							var countryStatus = false;
							var	stateStatus = false;
							var	cityStatus = false;
							var officeStatus = false;
							var buildingStatus = false;
							var datepicker3Status = false;
                           
							$("#pmAddActionName").val("Submit");
							
							var messageType = jQuery.trim($("#messageType_input").val());
							$("#messageType > option").each(function() {
                              if (messageType==this.text)
                              {
								if(this.value == 0){									
									$("#messageType_errors").show();
									messageTypestatus = false;
								 }
								 else{									
									 $("#messageType_errors").hide();
									 messageTypestatus = true;								 
									 }								
                              }
                           
                            });	
							var subject=jQuery.trim($("#subject").val());
							if(subject == "") {									
								$("#subject_errors").show();
								subjectStatus = false;
							}
							else {								
								$("#subject_errors").hide();
								subjectStatus = true;
							}

							  
							var message= jQuery.trim($("#message").val());								
							if(message == "") {								
								$("#message_errors").show();
								messageStatus = false;
							}
							else {								
								$("#message_errors").hide();
								messageStatus = true;
							}

							var datepicker1= $("#datepicker1").val();							
							if(datepicker1 == "") {								
								$("#datepicker1_errors").show();
								datepicker1Status = false;
							}
							else {								
								$("#datepicker1_errors").hide();
								datepicker1Status = true;
							}


							var datepicker2= $("#datepicker2").val();							
							if(datepicker2 == "") {								
								$("#datepicker2_errors").show();
								datepicker2Status = false;
							}
							else {								
								$("#datepicker2_errors").hide();
								datepicker2Status = true;
							}

							var date1 = new Date(datepicker1);
							var date2 = new Date(datepicker2);
							if (date2<date1)
							{
								$("#datepicker3_errors").show();
								datepicker3Status = false;
							}else{								
								$("#datepicker3_errors").hide();
								datepicker3Status = true;
							}	
							
							<!--Location type check -->				
							
														
								var locationType= $("#locationType").val();	
							
								if(locationType == "") {								
									$("#locationType_errors").show();
									locationTypeStatus = false;
								}
								else {									
									$("#locationType_errors").hide();
									locationTypeStatus = true;
								}
							
								var roles= $("#roles").val();							
								if(roles == "" || roles == null){								
									$("#roles_errors").show();
									rolesStatus = false;
								}
								else {								
									$("#roles_errors").hide();
									rolesStatus = true;
								}
							
								if(locationTypeStatus  || rolesStatus){
									$("#roles_errors").hide();
	                                $("#locationType_errors").hide();
								}

							<!--functions to check geography & siblings	 Start-->	
							
							var geography= $("#target1").val();							
							if(geography == 0) {	
								$("#geography_errors").show();
								geographyStatus = false;
							}
							else {		
								$("#geography_errors").hide();
								geographyStatus = true;
							}
						
							var country= $("#target2").val();							
							if(country == 0) {								
								$("#country_errors").show();
								countryStatus = false;
							}
							else {								
								$("#country_errors").hide();
								countryStatus = true;
							}
							
							var state= $("#target3").val();							
							if(state == 0) {								
								$("#state_errors").show();
								stateStatus = false;
							}
							else {								
								$("#state_errors").hide();
								stateStatus = true;
							}
					
						
						
							var city= $("#target4").val();							
							if(city == 0) {								
								$("#city_errors").show();
								cityStatus = false;
							}
							else {								
								$("#city_errors").hide();
								cityStatus = true;
							}	
						
						
						
							var office= $("#target5").val();							
							if(office == 0) {								
								$("#office_errors").show();
								officeStatus = false;
							}
							else {								
								$("#office_errors").hide();
								officeStatus = true;
							}
											
						
						
							var building= $("#target6").val();							
							if(building == 0) {								
								$("#building_errors").show();
								buildingStatus = false;
							}
							else {								
								$("#building_errors").hide();
								buildingStatus = true;
							}
						
						
						<!--functions to check geography & siblings	END-->	
                     
						if(messageType == "New Hire" || messageType == "US Benefits Eligible") 
						{								
							if(messageStatus && subjectStatus && messageTypestatus && datepicker1Status && datepicker2Status && datepicker3Status)
							{
								status = true;
							}
							else {
								status = false;
							}
							
						}
						
						if(messageType == "Regular") 
						{
							if(messageStatus && subjectStatus && messageTypestatus && datepicker1Status && datepicker2Status && datepicker3Status){
								if(rolesStatus || locationTypeStatus) {
									if(locationTypeStatus){
										if(locationType == "Building"){
                                             if(buildingStatus && officeStatus && cityStatus && stateStatus && countryStatus && geographyStatus){
                                               status = true;
										  }else{
											  status = false;
										  }
										}
										else if(locationType == "Office"){
                                             if(officeStatus && cityStatus && stateStatus && countryStatus && geographyStatus){
                                               status = true;
										  }else{
											  status = false;
										  }
										}
										else if(locationType == "City"){
                                             if(cityStatus && stateStatus && countryStatus && geographyStatus){
                                               status = true;
										  }else{
											  status = false;
										  }
										}
										else if(locationType == "Region"){
                                             if(stateStatus && countryStatus && geographyStatus){
                                               status = true;
										  }else{
											  status = false;
										  }
										}
										else if(locationType == "Country"){
                                             if(countryStatus && geographyStatus){
                                               status = true;
										  }else{
											  status = false;
										  }
										}
										else if(locationType == "Geography"){
										
                                             if(geographyStatus){
                                               status = true;
										  }else{
											  status = false;
										  }
										}
										else{
											 status = false;
										}
									}
									
								if(rolesStatus && status == false && locationType == ""){
										
										status = true;
									}
								}
							}
						}
						
					     if(status){
							 $("#formID").submit();
						 }
						 else{
							 return status;
						 }
	
					});
				/* End of validation on click of Submit button */
				
						$("#target1_container").find("ul").find("li").click(function(){
							
							$("#target3").remove();  
							$("#target3_container").remove(); 
							$("#target3_input").val($("#stateIdMessage").val()); 
							
							$("#target4").remove(); 
							$("#target4_container").remove();
							$("#target4_input").val($("#cityIdMessage").val()); 

							$("#target5").remove(); 
							$("#target5_container").remove();
							$("#target5_input").val($("#officeIdMessage").val()); 

							$("#target6").remove();
							$("#target6_container").remove();
							$("#target6_input").val($("#buildingIdMessage").val());
 
							
                           var geographyId;
                           var geoName= $(this).text();                          
							$("#target1 > option").each(function() {								
                              if(geoName == $(this).text()){                            	
								  geographyId=this.value;						  
                              }
                            });
							if(geographyId !=0 ){
											var xhr = new XMLHttpRequest();
											xhr.onreadystatechange = function() {
											if(xhr.readyState == 4) {						
												var messageText = document.getElementById("<portlet:namespace/>countryText");				
												messageText.innerHTML = xhr.responseText;												
												$("#target2").focus();
											}
										};
										var url = '<portlet:resourceURL id="populateCountries"></portlet:resourceURL>' + '?fakeId=345&geoId='+ geographyId ;
										xhr.open("GET", url, true);
										xhr.send();	
							}
						
						}); 
					
						$("#target2_container").find("ul").find("li").live('click',function() {
							
							$("#target4").remove(); 
							$("#target4_container").remove();
							$("#target4_input").val($("#cityIdMessage").val()); 

							$("#target5").remove(); 
							$("#target5_container").remove();
							$("#target5_input").val($("#officeIdMessage").val()); 

							$("#target6").remove();
							$("#target6_container").remove();
							$("#target6_input").val($("#buildingIdMessage").val());
							
							 var countryId;
	                           var countryName= $(this).text();

								$("#target2 > option").each(function() {
	                              if (countryName==this.text)
	                              {
	                            	  countryId=this.value;
	                              }
	                            
	                            });
								if(countryId != 0){
										var xhr1 = new XMLHttpRequest();										
										xhr1.onreadystatechange = function() {
										if(xhr1.readyState == 4) {						
											var messageText1 = document.getElementById("<portlet:namespace/>stateText");				
											messageText1.innerHTML = xhr1.responseText;
											
											$("#target3").focus();
										}
									};
									var url1 = '<portlet:resourceURL id="populateStates"></portlet:resourceURL>' + '?fakeId=345&countryId='+ countryId ;
									xhr1.open("GET", url1, true);
									xhr1.send();
								}
                             });						
				
						$("#target3_container").find("ul").find("li").live('click',function() {
							
							$("#target5").remove(); 
							$("#target5_container").remove();
							$("#target5_input").val($("#officeIdMessage").val()); 

							$("#target6").remove();
							$("#target6_container").remove();
							$("#target6_input").val($("#buildingIdMessage").val());
							
							 var stateId;
	                           var stateName= $(this).text();

								$("#target3 > option").each(function() {
								if ($.trim(stateName)==$.trim(this.text))
	                            {
	                            	  stateId=this.value;
	                            }
	                            
	                            });
						
								if(stateId != 0){
									var xhr2 = new XMLHttpRequest();
									
									xhr2.onreadystatechange = function() {
									if(xhr2.readyState == 4) {						
										var messageText2 = document.getElementById("<portlet:namespace/>cityText");				
										messageText2.innerHTML = xhr2.responseText;
										$("#target4").focus();
									}
								};
								var url2 = '<portlet:resourceURL id="populateCities"></portlet:resourceURL>' + '?fakeId=345&stateId='+ stateId ;
								xhr2.open("GET", url2, true);
								xhr2.send();	
								}
                        });	
						
						$("#target4_container").find("ul").find("li").live('click',function() {

							$("#target6").remove();
							$("#target6_container").remove();
							$("#target6_input").val($("#buildingIdMessage").val());
							
	                           var cityId;
	                           var cityName= $(this).text();
							   $("#target4 > option").each(function() {
	                              if (cityName==$(this).text())
	                              {
	                            	  cityId=this.value;
	                              }
	                            
	                            });
							   if(cityId != 0 ){
							   var xhr3 = new XMLHttpRequest();									
								xhr3.onreadystatechange = function() {
									if(xhr3.readyState == 4) {						
										var messageText3 = document.getElementById("<portlet:namespace/>officeText");				
										messageText3.innerHTML = xhr3.responseText;
										$("#target5").focus();
									}
								};
								var url3 = '<portlet:resourceURL id="populateOffices"></portlet:resourceURL>' + '?fakeId=345&cityId='+ cityName ;
								xhr3.open("GET", url3, true);
								xhr3.send();
							   }
                        });	
						
						$("#target5_container").find("ul").find("li").live('click',function() {
							var officeId;
							var officeName= $(this).text();
							   $("#target5 > option").each(function() {
	                              if (officeName==$(this).text())
	                              {
	                            	  officeId=this.value;
	                              }
	                            
	                            });
							   if(officeId != 0 ){								
							   var xhr4 = new XMLHttpRequest();									
							   xhr4.onreadystatechange = function() {
									if(xhr4.readyState == 4) {						
										var messageText4 = document.getElementById("<portlet:namespace/>buildingText");				
										messageText4.innerHTML = xhr4.responseText;
										$("#target6").focus();
									}
								};
								var url4 = '<portlet:resourceURL id="populateBuildings"></portlet:resourceURL>' + '?fakeId=345&officeId='+ officeName ;
								xhr4.open("GET", url4, true);
								xhr4.send();
							   }
                        });	
						
						$("#locationType_container").find("ul").find("li").live('click',function() {
							
	                           var locationType= $(this).text();	
	                             if (locationType=="Country")
	                              {
									  $("#geographyRow").show();
									  $("#countryRow").show();									 
	                            	  $("#stateRow").hide();
									  $("#cityRow").hide();
									  $("#officeRow").hide();
									  $("#buildingRow").hide();									
	                              }
								  else if(locationType=="Geography"){
									   $("#geographyRow").show();
									  $("#countryRow").hide();
									  $("#stateRow").hide();
									  $("#cityRow").hide();
									  $("#officeRow").hide();
									  $("#buildingRow").hide();	
								  }
								  else if(locationType=="State"){
									   $("#geographyRow").show();
									  $("#countryRow").show();
									  $("#stateRow").show();
									  $("#cityRow").hide();
									  $("#officeRow").hide();
									  $("#buildingRow").hide();	
								  }
								  else if(locationType=="City"){
									   $("#geographyRow").show();
									  $("#countryRow").show();
									  $("#stateRow").show();
									  $("#cityRow").show();										  
									  $("#officeRow").hide();
									  $("#buildingRow").hide();	
								  }
								   else if(locationType=="Office"){	
									    $("#geographyRow").show();
									  $("#countryRow").show();
									  $("#stateRow").show();
									  $("#cityRow").show();	
									   $("#officeRow").show();
									  $("#buildingRow").hide();	
								  }
								   else if(locationType=="Building"){
									  $("#geographyRow").show();
									  $("#countryRow").show();
									  $("#stateRow").show();
									  $("#cityRow").show();	
									  $("#officeRow").show();
									  $("#buildingRow").show();	
								  }
								  else {
									  $("#geographyRow").hide();
									  $("#countryRow").hide();
									  $("#stateRow").hide();
									  $("#cityRow").hide();
									  $("#officeRow").hide();
									  $("#buildingRow").hide();	
								  }
																
                        });	
						$("#messageType_container").find("ul").find("li").live('click',function() {
							
	                        var messageType=$(this).text();	
							$("#messageType > option").each(function() {
                              if (messageType==this.text)
                              {
								if(this.value == 0){									
									$("#messageType_errors").show();								
								 }
								else{									
									 $("#messageType_errors").hide();									 
								}								
                              }                           
                            });


							   if(messageType == "New Hire"){                                   
								   $("#rolesRow").hide();
								   $("#locationTypeRow").hide();
								   $("#geographyRow").hide();
									  $("#countryRow").hide();
									  $("#stateRow").hide();
									  $("#cityRow").hide();
									  $("#officeRow").hide();
									  $("#buildingRow").hide();	
							   }
							   if(messageType=="US Benefits Eligible"){
								   $("#rolesRow").hide();
								   $("#locationTypeRow").hide();
								   $("#geographyRow").hide();
									  $("#countryRow").hide();
									  $("#stateRow").hide();
									  $("#cityRow").hide();
									  $("#officeRow").hide();
									  $("#buildingRow").hide();	
							   }
							   if(messageType=="Regular"){
								   $(".ui-multiselect-none").click();
								   $("#locationType_input_").click();
								   $("#rolesRow").show();
								   $("#locationTypeRow").show();
								   
								   $("#target1_input").val($("#geographyIdMessage").val()); 
								   $("#target2_input").val($("#countryIdMessage").val()); 
								   $("#target3_input").val($("#stateIdMessage").val()); 
								   $("#target4_input").val($("#cityIdMessage").val()); 
								   $("#target5_input").val($("#officeIdMessage").val()); 
								   $("#target6_input").val($("#buildingIdMessage").val());
							   }
							 	
                        });	
						
					});


</script>
<script type="text/javascript">
$('document').load('ajax/countyList.jsp', function() {
  
});
</script>

<portlet:actionURL var="addPersonalMessageURL">
	<portlet:param name="myaction" value="addPersonalMessage"></portlet:param>
</portlet:actionURL>

<form id="formID" method="post" action="${addPersonalMessageURL}" autocomplete="off"
	onsubmit="<portlet:namespace />extractCodeFromEditor()"
	name="<portlet:namespace />fm">

	<div class="aui-datepicker aui-helper-clearfix"
		id="#<portlet:namespace />startDatePicker">
		<input type="hidden" name="startDate"
			id="<portlet:namespace />startDate" size="30" />
	</div>

	<c:set var="geographyList" value='${geographyList}'></c:set>

	<%
		List<GeographyVO> geographyList = (List<GeographyVO>) pageContext.getAttribute("geographyList");
	%>

	<div id="form_container" style="margin-left: 20px;">

		<table border="0" cellpadding="0" cellspacing="0" id="id-form">

			<tr>
				<th valign="top"><span class="mandetory"></span></th>
				<td><div id="1" class="error_message">
						<spring:message code="label.addeditpersonalmessageadmin.info" />
					</div>
				</td>
				<td></td>
			</tr>
			<tr>
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.addeditpersonalmessageadmin.subject" /></th>
				<td><input type="text" class="inp-form" name="subject"
					maxlength="200" id="subject" />
					<div id="subject_errors" class="error_message"
						style="display: none">
						<spring:message code="label.error.requiredfeild.blank" />
					</div></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.addeditpersonalmessageadmin.description" /></th>
				<td><textarea maxlength="1955" rows="" cols=""
						class="form-textarea" name="message" id="message"></textarea>
					<div id="message_errors" class="error_message"
						style="display: none">
						<spring:message code="label.error.requiredfeild.blank" />
					</div>
				</td>
				<td></td>
			</tr>
			<tr>

				<td>
			<tr>

				<td>
			<tr class="hasselect">
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.personalmessageadmin.messagetype" />:</th>
				<td><select class="styledselect_form_1" name="messageType"
					id="messageType">
						<option value="0">
							<spring:message
								code="label.addeditpersonalmessageadmin.select.messagetype" />
						</option>
						<option value="Regular">Regular</option>
						<option value="New Hire">New Hire</option>
						<option value="US Benefits Eligible">US Benefits Eligible</option>
				</select>
					<div id="messageType_errors" class="error_message"
						style="display: none">
						<spring:message code="label.error.requiredfeild.blank" />
					</div></td>
				<td></td>
			</tr>
			<tr id="rolesRow" class="hasselect">
				<th valign="top"><span class="mandetory"><spring:message
							code="label.doublemandatory" /> </span> <spring:message
						code="label.addeditpersonalmessageadmin.roles" /></th>
				<td><select multiple="multiple" class="multiSelect" id="roles"
					name="roles" style="width:330px;margin-left:-4px;">
						<%
							List<Role> lstRoles = new ArrayList<Role>();
							lstRoles = RoleLocalServiceUtil.getRoles(1, "");
							for (Role role : lstRoles) {
						%>

						<option value="<%=role.getRoleId()%>"><%=role.getName()%></option>

						<%
							}
						%>
				</select>
					<div id="roles_errors" class="error_message" style="display: none">
						<spring:message code="label.addeditpersonalmessageadmin.error.rolelocationselection" />
					</div></td>
				<td></td>
			</tr>
			<tr>
				<td>
			<tr id="locationTypeRow" class="hasselect">
				<th valign="top"><span class="mandetory"><spring:message
							code="label.doublemandatory" /> </span> <spring:message
						code="label.addeditpersonalmessageadmin.locationtype" /></th>
				<td><select class="styledselect_form_1" name="locationType"
					id="locationType">
						<option value="">
							<spring:message
								code="label.addeditpersonalmessageadmin.select.locationtype" />
						</option>
						<option value="Geography">Geography</option>
						<option value="Country">Country</option>
						<option value="Region">State</option>
						<option value="City">City</option>
						<option value="Office">Office</option>
						<option value="Building">Building</option>
				</select>
					<div id="locationType_errors" class="error_message"
						style="display: none"><spring:message code="label.addeditpersonalmessageadmin.error.rolelocationselection" />
						</div></td>
				<td></td>
			</tr>
			<tr id="geographyRow" class="hasselect">
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.addeditpersonalmessageadmin.geography" /></th>
				<td><select name="geographyId" id="target1"
					class="styledselect_form_1">
						<option value="0">
							<spring:message
								code="label.addeditpersonalmessageadmin.select.geographylist" />
						</option>
						<%
							for (GeographyVO geographyVO : geographyList) {
						%>
						<option value="<%=geographyVO.getGeographyId()%>"><%=geographyVO.getGeographyName()%>
						</option>
						<%
							}
						%>

				</select>
					<div id="geography_errors" class="error_message"
						style="display: none">
						<spring:message code="label.error.requiredfeild.blank" />
					</div></td>
			</tr>

			<tr id="countryRow" class="hasselect">
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.addeditpersonalmessageadmin.country" /></th>
				<td>
					<div id="<portlet:namespace/>countryText">
						<Select name="countryId" id="target2" class="styledselect_form_1">
							<option value="0">
								<spring:message
									code="label.addeditpersonalmessageadmin.select.countrylist" />
							</option>
						</Select>
						<div id="country_errors" class="error_message"
							style="display: none">
							<spring:message code="label.error.requiredfeild.blank" />
						</div>
					</div></td>
			</tr>

			<tr id="stateRow" class="hasselect">
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.addeditpersonalmessageadmin.state" /></th>
				<td>
					<div id="<portlet:namespace/>stateText">
						<Select name="stateId" id="target3" class="styledselect_form_1">
							<option value="0">
								<spring:message
									code="label.addeditpersonalmessageadmin.select.statelist" />
							</option>
						</Select>
						<div id="state_errors" class="error_message" style="display: none">
							<spring:message code="label.error.requiredfeild.blank" />
						</div>
					</div></td>
			</tr>

			<tr id="cityRow" class="hasselect">
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.addeditpersonalmessageadmin.city" /></th>
				<td>
					<div id="<portlet:namespace/>cityText">
						<Select name="cityName" id="target4" class="styledselect_form_1">
							<option value="0">
								<spring:message
									code="label.addeditpersonalmessageadmin.select.citylist" />
							</option>
						</Select>
						<div id="city_errors" class="error_message" style="display: none">
							<spring:message code="label.error.requiredfeild.blank" />
						</div>
					</div></td>
			</tr>

			<tr id="officeRow" class="hasselect">
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.addeditpersonalmessageadmin.office" /></th>
				<td>
					<div id="<portlet:namespace/>officeText">
						<Select name="officeName" id="target5" class="styledselect_form_1">
							<option value="0">
								<spring:message
									code="label.addeditpersonalmessageadmin.select.officelist" />
							</option>
						</Select>
						<div id="office_errors" class="error_message"
							style="display: none">
							<spring:message code="label.error.requiredfeild.blank" />
						</div>
					</div></td>
			</tr>

			<tr id="buildingRow" class="hasselect">
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.addeditpersonalmessageadmin.building" /></th>
				<td>
					<div id="<portlet:namespace/>buildingText">
						<Select name="buildingName" id="target6"
							class="styledselect_form_1">
							<option value="0">
								<spring:message
									code="label.addeditpersonalmessageadmin.select.buildinglist" />
							</option>
						</Select>
						<div id="building_errors" class="error_message"
							style="display: none">
							<spring:message code="label.error.requiredfeild.blank" />
						</div>
					</div></td>
			</tr>

			<tr>
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.personalmessageadmin.displaydate" />:</th>
				<td class="noheight">
					<div class="demo">
						<p>
							<input type="text" name="displayDate" id="datepicker1"
								class="inp-form-calendar" style="margin-right: 4px;"
								readonly="readonly" />
						<div id="datepicker1_errors" class="error_message"
							style="display: none">
							<spring:message code="label.error.requiredfeild.blank" />
						</div>
						</p>
					</div></td>
				<td></td>
			</tr>
			<tr>
				<th valign="top"><span class="mandetory"><spring:message
							code="label.mandatory" /> </span> <spring:message
						code="label.personalmessageadmin.expirationdate" />:</th>
				<td class="noheight">
					<div class="demo">
						<p>
							<input type="text" name="expirationDate" id="datepicker2"
								class="inp-form-calendar" style="margin-right: 4px;"
								readonly="readonly">
						<div id="datepicker2_errors" class="error_message"
							style="display: none">
							<spring:message code="label.error.requiredfeild.blank" />
						</div>
						<div id="datepicker3_errors" class="error_message"
							style="display: none">
							<spring:message
								code="label.addeditpersonalmessageadmin.error.exiprydate" />
						</div>
						</p>
					</div></td>
				<td></td>
			</tr>
		</table>
		<br />
 			<input type="hidden" value="" id="pmAddActionName" name="addPM" />
		<table width = "100%">
			
			<tr>
			<td valign="top"><input type="submit" id="submitId"
				value="<spring:message code="label.submit" />" name="addPM"
				style="background-color: black; color: white; padding: 1px; margin-left: 4px; height: 34px; width: 94px; border: none;" />
				<input type="submit" id="resetId" value="<spring:message code="label.reset" />" name="addPM"
				style="background-color: black; color: white; padding: 1px; margin-left: 1px; height: 34px; width: 94px; border: none;" />
				<input type="submit" id="cancelId" value="<spring:message code="label.cancel" />"
				name="addPM"
				style="background-color: black; color: white; padding: 1px; margin-left: 4px; height: 34px; width: 94px; border: none;" />
				
			</td>
			</tr>
		</table>
	</div>
	
	<input type="hidden"
		value="<spring:message code="label.addeditpersonalmessageadmin.select.geographylist" />"
		id="geographyIdMessage" />
	<input type="hidden"
		value="<spring:message code="label.addeditpersonalmessageadmin.select.countrylist" />"
		id="countryIdMessage" />
	<input type="hidden"
		value="<spring:message code='label.addeditpersonalmessageadmin.select.statelist'/>"
		id="stateIdMessage" />
	<input type="hidden"
		value="<spring:message code='label.addeditpersonalmessageadmin.select.citylist'/>"
		id="cityIdMessage" />
	<input type="hidden"
		value="<spring:message code='label.addeditpersonalmessageadmin.select.officelist'/>"
		id="officeIdMessage" />
	<input type="hidden"
		value="<spring:message code='label.addeditpersonalmessageadmin.select.buildinglist'/>"
		id="buildingIdMessage" />
</form>