<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.liferay.portlet.journal.model.JournalArticle"%>
<%@ page import="java.util.List"%>
<%@ page import="com.coach.cip.util.WebUtil"%>
<%@ page import="com.coach.cip.util.Constants" %>


<portlet:defineObjects />
<liferay-theme:defineObjects />

<div style="border: 1px solid black;">
		<header class="portlet-topper" style="background-color: #395A08;">
			<h1 class="portlet-title">
				<span class="portlet-title-text"><%=portletDisplay.getTitle()%></span>
			</h1>
		</header>
<div style="margin-left:10px;margin-right:10px;">
	<div align="right" >
		</br>
		<a style="text-decoration: none;color:black" href="<%=renderRequest.getParameter("redirect")%>" > 
		<span style="font-family: 'myriad-web-pro';font-size: 12px;font-weight:bold" >&lt;&lt;Back </span>
		</a>
	</div>

	<div class="journal-content-article"> 
		<div> 
			<% if(renderRequest.getParameter(Constants.IMAGE_URL) != null && (!renderRequest.getParameter(Constants.IMAGE_URL).equals(""))){ %>
			<p> 
				<img alt="" src="<%=renderRequest.getParameter(Constants.IMAGE_URL)%>" style="width: 105px; height: 140px; float:left">  
			</p> 
			
			<%}else{%>
			 <p> 
				<img alt="" src="<%=request.getContextPath()%>/images/lew-image.PNG" style="width: 105px; height: 140px; float:left"> 
			</p>
			<%}%>
			<div style="margin-left: 117px;">
				<div style="margin-bottom:6px;">
					<span style="font-family: 'myriad-web-pro';font-size: 14px;font-weight:bold"> 
						<%=renderRequest.getParameter("title")%> 
					</span>  
				</div>
				<div style="margin-bottom:6px;">
				<p> 
					<span style="font-family:'myriad-web-pro';font-size: 12px;"> 
						<%=renderRequest.getParameter(Constants.CONTENT_URL)%>
					</span>
				</p> 
				</div>
				<div>
					<%=renderRequest.getParameter(Constants.DOC_URL)%>
				</div>
			</div> 
		</div> 
	</div>
<br/>
</div>
</div>
<br/>

