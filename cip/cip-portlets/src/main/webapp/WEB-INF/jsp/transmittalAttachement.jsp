<%@page import="java.net.URLDecoder"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@ page import="java.util.List"%>
<%@ page import="com.coach.cip.common.dto.NewHiresVO"%>
<%@ page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>





<portlet:defineObjects />
<liferay-theme:defineObjects />

<portlet:actionURL var="attachmentUrl">
	<portlet:param name="action" value="attachmentPage"></portlet:param>
</portlet:actionURL>

<%
	String redirect = ParamUtil.getString(renderRequest, "redirect");
	redirect = URLDecoder.decode(redirect);
%>

<%-- <liferay-ui:header backURL="javascript:history.back()" title="" /> --%>


<fmt:bundle basename="content/Language">


	<form:form name="attachmentForm" method="post"
		commandName="documentPath" action="${attachmentUrl}">

		<div>
            <c:set var="dlFileEntry" value='${dlFileEntry}'></c:set>

			<table class="tableEmployee">
				<tr>
				      <c:choose>
					<c:when
						test="${dlFileEntry ne null && dlFileEntry.extension eq 'pdf'}">
						
						     <td><iframe src="${fn:replace(documentPath,' ', '+')}" width="100%" height="600"></iframe>
								</td>
						    
					</c:when>
					<c:otherwise>
							<iframe style="display:none"  src="${fn:replace(documentPath,' ', '+')}" width="0%" height="0"></iframe> 
						    <h3><spring:message code='label.document.download'/></h3>
				        </c:otherwise>
				     </c:choose>   
				</tr>
			</table>
			
			<input type="button" id="aui_3_4_0_1_498"
						value="<spring:message code='label.back.button'/>" class="submitClass" onClick="location.href='<%=redirect %>'" />
			
		</div>
	</form:form>


</fmt:bundle>
