<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@ page import="java.util.List"%>
<%@ page import="com.coach.cip.common.dto.EmployeeDirectorySearchVO"%>
<%@ page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />


<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery-ui-1.8.19.custom.min.js"></script>

<script
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.selectbox-0.5_style_2.js"
	type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.styledselect_form_1').selectbox({
			inputClass : "styledselect_form_1"
		});
	});
</script>

<script>
	$(document)
			.ready(
					function() {

						$("#searchId")
								.click(
										function() {

											var firstName = jQuery.trim($(
													"#firstName").val());
											var lastName = jQuery.trim($(
													"#lastName").val());
											var title = jQuery.trim($("#title")
													.val());
											var department = $("#department")
													.val();
											var geography = $("#geography")
													.val();
											var email = jQuery.trim($("#email")
													.val());
											var phoneExtension = jQuery.trim($(
													"#phoneExtension").val());

											if (firstName == ""
													&& lastName == ""
													&& title == ""
													&& (department == ""
															|| department == null || department == 0)
													&& (geography == ""
															|| geography == null || geography == 0)
													&& email == ""
													&& phoneExtension == "") {

												$("#search_error").show();
												return false;
											} else {

												$("#search_error").hide();
											}

										});

					});
</script>

<portlet:actionURL var="employeeDirectoryUrl">
	<portlet:param name="myaction" value="getSearchResult"></portlet:param>
</portlet:actionURL>


<fmt:bundle basename="content/Language">

	<form:form name="employeeDirectoryForm" method="post"
		commandName="employeeDirectorySearchVO" id="employeeDirectoryFormId"
		action="${employeeDirectoryUrl}">

		<div id="form_container1">
			<header class="portlet-topper" style="background-color: #EB3528;">
			<h1 class="portlet-title">
				<fmt:message key='label.employee.directory.search.header' />
			</h1>
			</header>
			
			<table class="tableEmployee" id="empDirTable">
				<!-- 
				<tr>
					<th class="employee-inp-form-td" valign="top" style="color:red;">
						<c:if test="${empty employeeDirectorySearchVO.deptList}">
							<spring:message code="message.employee.directory.webservice" />
						</c:if>
					</th>
				</tr> -->
				<tr>
					<th class="employee-inp-form-td" valign="top"><fmt:message
							key='label.employee.directory.search.message' /></th>
				</tr>
				<tr>
					<th class="employee-inp-form-td" valign="top"><fmt:message
							key='label.employee.directory.firstname' /></th>
				</tr>
				<tr>
					<td class="tableEmployee-td"><form:input
							class="employee-inp-form" maxlength="200" id="firstName"
							path="firstName" />
					</td>
				</tr>
				<tr>
					<th valign="top" class="employee-inp-form-td"><fmt:message
							key='label.employee.directory.lastname' /></th>
				</tr>
				<tr>

					<td class="tableEmployee-td"><form:input
							class="employee-inp-form" maxlength="200" id="lastName"
							path="lastName" />
					</td>

				</tr>
				<!-- 
			<tr>
				<th valign="top" class="employee-inp-form-td">				
				<fmt:message   key='label.employee.directory.title' />		
				</th>
			</tr>
			<tr>
				<td class="tableEmployee-td"><form:input class="employee-inp-form" maxlength="200" id="title"
						path="title" /></td>
			</tr> 
				<tr>
					<th valign="top" class="employee-inp-form-td"><fmt:message
							key='label.employee.directory.department' /></th>
				</tr>
				<tr>
					<td class="tableEmployee-td"><select name="department"
						id="department" class="styledselect_form_1">
							<option Value="0">
								<fmt:message key='label.employee.directory.select.dept' />
							</option>
							<c:if test="${not empty employeeDirectorySearchVO.deptList}">
								<c:forEach var="department"
									items="${employeeDirectorySearchVO.deptList}">
									<option value="${department}">${department}</option>
								</c:forEach>
							</c:if>
					</select></td>
				</tr>
				
				<tr>
					<th valign="top" class="employee-inp-form-td"><fmt:message
							key='label.employee.directory.geography' /></th>
				</tr>
				<tr>
					<td class="tableEmployee-td"><select name="geography"
						id="geography" class="styledselect_form_1">
							<option Value="0">
								<fmt:message key='label.employee.directory.select.geography' />
							</option>
							<c:if test="${not empty employeeDirectorySearchVO.geographyList}">
								<c:forEach var="geography"
									items="${employeeDirectorySearchVO.geographyList}">
									<option value="${geography}">${geography}</option>
								</c:forEach>
							</c:if>
					</select>
					</td>
				</tr>-->
				<tr>
					<th valign="top" class="employee-inp-form-td"><fmt:message
							key='label.employee.directory.email' />
					</th>
				</tr>
				<tr>
					<td class="tableEmployee-td"><form:input
							class="employee-inp-form" maxlength="200" id="email" path="email" />
					</td>
				</tr>
				<tr>
					<th valign="top" class="employee-inp-form-td"><fmt:message
							key='label.employee.directory.phone.extension' />
					</th>
				</tr>
				<tr>
					<td class="tableEmployee-td"><form:input
							class="employee-inp-form" maxlength="4" id="phoneExtension"
							path="phoneExtension" />
					</td>
				</tr>
				<tr>
					<td class="tableEmployee-td">
						<div id="search_error" class="error_message" style="display: none">
							<fmt:message key='message.employee.directory.search' />
						</div>
					</td>
				</tr>


				<tr>
					<td class="tableEmployee-td"><input type="submit" class="button" 
						value="<fmt:message   key='label.employee.directory.search' />"
						id="searchId"
						style="background-color: black; color: white; padding: 1px; margin-top: 8px; height: 30px; width: 94px; border: none;" />
					</td>

				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
			</table>
		</div>
	</form:form>
</fmt:bundle>