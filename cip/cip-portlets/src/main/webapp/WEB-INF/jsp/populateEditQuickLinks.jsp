<%@page import="java.util.List"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="javax.portlet.PortletResponse"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="aui" uri="http://alloy.liferay.com/tld/aui"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="javax.portlet.ActionRequest"%>
<%@ page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@ page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@ page import="com.coach.cip.common.dto.QuickLinksVO"%>
<%@ page import="com.coach.cip.portlets.controller.QuickLinksController"%>
<%@ page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<%@ page import="javax.portlet.PortletURL"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<portlet:defineObjects />
<liferay-theme:defineObjects />

<link rel="stylesheet"
	href="<%= themeDisplay.getPathThemeCss() %>/screen.css" type="text/css"
	media="screen" title="default" />
	<script type="text/javascript">
	$(document).ready(function() {

		$(".close-yellow").click(function() {
			
			$(".yellow-left").fadeOut("slow");
			$(".yellow-right").fadeOut("slow");
		});
		$(".close-red").click(function() {
			$(".red-left").fadeOut("slow");
			$(".red-right").fadeOut("slow");
		});
		$(".close-blue").click(function() {
			$(".blue-left").fadeOut("slow");
			$(".blue-right").fadeOut("slow");
		});
		$(".close-green").click(function() {

			$(".green-left").fadeOut("slow");
			$(".green-right").fadeOut("slow");
		});
		
		
	});
</script>


        <div id="editQuickParentDiv">
		<c:set var="message" value='${message}'></c:set>
		<%
			String message = (String) pageContext.getAttribute("message");
		%>	
		
		
		<c:if test="${not empty message}">
		<!--  start message-red -->
		<c:if test='<%= SessionErrors.contains(resourceRequest, message) %>'>
		<c:if test="${message.startsWith('error')}">
			<div id="message-red">
				<table border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td class="red-left"><liferay-ui:error key="${message}" message="${message}"></liferay-ui:error>
					<%-- 	<c:out value="${message}" /> --%>
						</td>
						<td class="red-right"><a class="close-red"><img
								src="<%= themeDisplay.getPathThemeImages() %>/table/icon_close_red.gif"
								alt="" /> </a></td>
					</tr>
				</table>
			</div>
		</c:if>
		</c:if>
		<c:if test='<%= SessionMessages.contains(resourceRequest, message) %>'>		
		<c:if test="${message.startsWith('success')}">
			<div id="message-green">
				<table border="0"  cellpadding="0" cellspacing="0">
					<tr>
						<td class="green-left"><liferay-ui:success key="${message}" message="${message}"></liferay-ui:success>
						<%-- <c:out value="${message}" /> --%>
						</td>
						<td class="green-right"><a class="close-green"><img
								src="<%= themeDisplay.getPathThemeImages() %>/table/icon_close_green.gif"
								alt="" /> </a></td>
					</tr>
				</table>
			</div>
		</c:if>
		</c:if>
		</c:if>

		<form:form name="editQuickLinksForm" method="post"
			action="${editQuickLinksUrl}" id="editQuickLinksForm">
		<%
            PortletResponse   portletResponse = null;
			PortletURL changePageURL = null;
		    if(Validator.isNotNull(renderResponse))
		    {
		    	portletResponse = renderResponse;
		    	changePageURL = renderResponse.createRenderURL();
		    }
		    if(Validator.isNotNull(resourceResponse))
		    {
		    	portletResponse = resourceResponse;
		    	changePageURL = resourceResponse.createRenderURL();
		    }
			changePageURL.setParameter("myaction", "editQuickLinksForm");
			String pgNo = (String) request.getAttribute("pagination");
			int deltaValue = 5;
			List<QuickLinksVO> tempResults = (List<QuickLinksVO>) request.getAttribute("editQuickLinksVOList");
		%>
		<c:choose>
		  <c:when test="<%=  tempResults.size()>0 %>">
		  	
			<input type="hidden" value="<%=pgNo %>" id="qlPageNumber"
				name="qlPageNumber" />
			<input type="hidden" value="Close" id="actionName"
				name="quiickLinksOperation" />
			<%
				String str = (String) request.getAttribute("editOpenPopup");
				if (str != null && str.equals("true")) {
			%>
			<input type="hidden" value="<%=str%>" id="editOpenPopup"
				name="editOpenPopup" />
			<%
				}
					else {
			%>
			<input type="hidden" value="<%=str%>" id="editOpenPopup"
				name="editOpenPopup" />
			<%
				}
			%>
			
			<br />
		
				<div id="tablewrapper">
					<!-- <div id="tableheader">
						 <div class="search">
				                <select id="columns" onchange="sorter.search('query')"></select>
				                <input type="text" id="query" onkeyup="sorter.search('query')" />
				            </div> 
						 <span class="details">	<div>Showing <span id="startrecord"></span> - <span id="endrecord"></span> of <span id="totalrecords"></span> results.</div> 
							<div id="resetDiv">
								<a href="javascript:sorter.reset()">reset</a>
							</div> </span>
					</div>  -->
					<table cellpadding="0" cellspacing="0" border="0" id="table"
						class="tinytable">
						<thead class="resultHeader">
							<tr height="32px">
								<th class="nosort"><h3><input type="checkbox" id="checkQuickLinkIds" ></h3>
								</th>
								<th class="nosort"><h3>Page Name</h3>
								</th>
								<th class="nosort"><h3>Page URL</h3>
								</th>
							</tr>
						</thead>
						<tbody>
							<%
								StringBuilder qlPrimaryKeys = new StringBuilder();
									for (QuickLinksVO results : tempResults) {
										qlPrimaryKeys.append(results.getQuicklinkId() + ",");
							%>
							<tr>
								<td><input type="checkbox" name="quickLinkIds"
									value="<%=results.getQuicklinkId()%>"></td>
								<td><input type="text" name="pgName" maxlength="75"
									value="<%=results.getPageName()%>" />
								</td>
								<td><input type="text" class="pageUrlWidth" readonly="readonly" name="pgURL" value="<%=results.getAssetURL()%>" /></td>
							</tr>
							<%
								}
							%>
						</tbody>
					</table>
					<input type="hidden" id="qlPrimaryKeys" name="qlPrimaryKeys"
						value="<%=Validator.isNotNull(qlPrimaryKeys) ? qlPrimaryKeys
									.substring(0, qlPrimaryKeys.length() - 1) : ""%>">
					   
						<div class="details" style="float: left">
							<div>
								Showing <span id="startrecord"></span> - <span id="endrecord"></span>
								of <span id="totalrecords"></span> results.
							</div> <!-- 	<div><a href="javascript:sorter.reset()">reset</a></div> -->
						</div>
					
						<div id="tablefooter" style="display:inline;">
						<div id="tablelocation" style="float:left;">
							<div>
								<span>Items Per Page <select id="itemPerPage"
									onchange="sorter.size(this.value)">
									<option value="5" selected="selected">5</option>
									<option value="10">10</option>
									<option value="20">20</option>
									<option value="50">50</option>
									<option value="100">100</option>
								</select>
			                     </span>
							
							<span>
								Page <select id="pagedropdown"></select> of <span id="totalpages"></span>
								<input type="hidden" id="currentpage">
							</span>
							</div>
							<!--   <div class="page">Page <span id="currentpage"></span> of <span id="totalpages"></span></div> -->
						</div>
						<div id="tablenav" style="float:right;">
			
							<!-- <div>
								Page <select id="pagedropdown"></select> of <span id="totalpages"></span>
							</div> -->
			
							<span>
								<img src="<%=themeDisplay.getPathThemeImages()%>/first.gif"
									width="16" height="16" alt="First Page"
									onclick="sorter.move(-1,true)" /> First<img
									src="<%=themeDisplay.getPathThemeImages()%>/previous.gif"
									width="16" height="16" alt="Previus Page" onclick="sorter.move(-1,false)" />Previus
								<img src="<%=themeDisplay.getPathThemeImages()%>/next.gif"
									width="16" height="16" alt="Next Page" onclick="sorter.move(1,false)" />Next
								<img src="<%=themeDisplay.getPathThemeImages()%>/last.gif"
									width="16" height="16" alt="Last Page"
									onclick="sorter.move(1,true)" >Last</img>
							</span>
							<!-- <div>
			                	<a href="javascript:sorter.showall()">view all</a>
			                </div> -->
						</div>
			
					</div>
				</div>
				
					<script type="text/javascript" src="<%=themeDisplay.getPathThemeJavaScript()%>/tiny.table.js"></script>
	<script type="text/javascript">
	var sorter = new TINY.table.sorter('sorter','table',{
		headclass:'head',
		ascclass:'asc',
		descclass:'desc',
		evenclass:'evenrow',
		oddclass:'oddrow',
		evenselclass:'evenselected',
		oddselclass:'oddselected',
		paginate:true,
		size:5,
		/* colddid:'columns', */
		currentid:'currentpage',
		totalid:'totalpages',
		startingrecid:'startrecord',
		endingrecid:'endrecord',
		totalrecid:'totalrecords',
		hoverid:'selectedrow',
		pageddid:'pagedropdown',
		navid:'tablenav',
		sortcolumn:1,
		sortdir:1,
		/* sum:[8],
		avg:[6,7,8,9], */
		columns:[{index:7, format:'%', decimals:1},{index:8, format:'$', decimals:0}],
		init:true
	});
	
  </script>
				
				
	    </c:when>
		<c:otherwise>
		         <spring:message code='no-quicklinks-for-user'/>
		         <input type="hidden" value="true" id="editOpenPopup"
				name="editOpenPopup" />
		</c:otherwise>
		
	  </c:choose>	 
		<table align="right" style="margin-top:5px;">
			<tr>
				<c:if test="<%=  tempResults.size()>0 %>">
				 <td><input type="button" value="<spring:message code='label.update'/>"  
					name="quiickLinksOperation" id="editupdate" class="submitClass" />
					&nbsp;</td>
				<td><input type="button" value="<spring:message code='label.delete'/>" name="quiickLinksOperation" id="editdelete" class="submitClass"/ >
					&nbsp;</td> 
				</c:if>	
				<td><input type="button"
					value="<spring:message code='label.close'/>"
					name="quiickLinksOperation" id="editclose" class="submitClass"/ >
				</td>
			</tr>
		</table>
	
		</form:form>
		
	</div>	
	
