<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletURL"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.security.auth.AuthTokenUtil"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.liferay.portal.kernel.util.PropsUtil"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="aui" uri="http://alloy.liferay.com/tld/aui"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="java.util.List"%>
<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@ page import="org.springframework.ui.Model"%>
<%@ page import="java.util.ResourceBundle"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<script type="text/javascript">
	$(document).ready(function() {
		
		$(".quick-links").find("dd").find("ul").find("li").find("a").hover(function () {
				 $(this).addClass("quickHover");
				},
			function () {
			 $(this).removeClass("quickHover");
		 }
		); 
	});
</script>
<portlet:actionURL var="quickLinksUrl">
	<portlet:param name="myaction" value="quickLinks" />
</portlet:actionURL>

	
					<c:if test="${not empty quickLinksVOList}">
						<c:forEach var="quickLinks" items="${quickLinksVOList}">
						<c:set var="pageURL"
									value="${quickLinks.assetURL}" />
						<c:set var="portalURL"
									value="<%=(\"/group\"+themeDisplay.getParentGroup().getFriendlyURL())%>" />
						<c:choose>
							<c:when test="${fn:contains(pageURL, '/group/coach/')}">
						      <li><a href="${pageURL}">${quickLinks.pageName}</a>
									</li>
							</c:when>
							<c:otherwise>
									<li><a href="${pageURL}" target="_blank">${quickLinks.pageName}</a>
									</li>
							</c:otherwise>
						</c:choose>
					</c:forEach>
					</c:if>
					
