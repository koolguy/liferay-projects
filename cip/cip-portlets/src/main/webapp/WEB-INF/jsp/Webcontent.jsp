<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.coach.cip.util.Constants"%>
<%@ page import="com.liferay.portal.kernel.util.StringUtil"%>
<%@ page import="com.coach.cip.util.WebUtil"%>
<%@ page import="com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil"%>
<%@ page import="com.liferay.portlet.journal.service.persistence.JournalArticleFinderUtil"%>
<%@ page import="com.liferay.portlet.journalcontent.util.JournalContentUtil"%>
<%@ page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import="com.liferay.portlet.journal.model.JournalArticle"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="javax.portlet.PortletURL"%>
<%@ page import="com.liferay.portlet.PortletURLUtil"%>
<%@ page import="com.liferay.portal.util.PortalUtil"%>
<%@ page import="com.liferay.portal.kernel.util.HttpUtil"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<link rel="stylesheet"
	href="<%=themeDisplay.getPathThemeCss()%>/allCoachNews.css"
	type="text/css" media="screen" title="default" />
<script src="<%=themeDisplay.getPathThemeCss()%>/../js/slider.js"
	type="text/javascript"></script>

<div style="background: #000; height: 30px; width: 100%;">
	<span style="float: left; color: #fff; font-size: 18px;padding: 2px 2px 2px 5px;">
	<spring:message code='label.coachnews.links.title'/>
	</span> 
	<span style="float: right; color: #fff; font-size: 12px; padding-top: 14px;padding-right: 5px;">
	<a style="color: #fff;"
		href="<portlet:actionURL>
								<portlet:param name="myaction" value="detailAllWebContent" />
			</portlet:actionURL>">
			<span class="learnMoreBtn">
			<spring:message	code='button.all.coach.news'/> &raquo;</span>
	</a>
	</span>
</div>
<div id="journal-article-import" style="border-bottom: 1px solid black; border-right: 1px solid black; border-left: 1px solid black; height: 90px;">
	<%
		String redirect = PortalUtil.getCurrentURL(renderRequest);
		List<JournalArticle> latestarticles = WebUtil.getArticlesByCategory(renderRequest, renderResponse,Constants.COACH_NEWS_ARTICLE_TYPE);
		String vocabname = Constants.COACH_NEWS_ARTICLE_TYPE;
		if (null != latestarticles && latestarticles.size() > 0) {
		Date latestArticledDate =  latestarticles.get(0).getDisplayDate();
		DateFormat df  = new SimpleDateFormat("yyyy");
		String dateString = df.format(latestArticledDate);
		int size = latestarticles.size();
		if (size < 5) {
	%>
	<div id="loopedSlider" style="width: 100%">
		<table style="width: 105%;">
			<tr>
				<td><div class="container">
						<ul class="slides">
							<li>
								<table>
									<tr>
										<%
											int i = 0;
																int j = 0;
																int counter = 0;
																for (i = 0; i < size; i++) {
										%>

										<%
											JournalArticle onearticle = latestarticles.get(counter++);
																String languageId = LanguageUtil.getLanguageId(renderRequest);
																//String summary = onearticle.getDescription(locale);
																String summary = onearticle.getTitle(locale);
																SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy",locale);
																String displayDate = sdf.format(onearticle.getDisplayDate());
																String shortsummary = StringUtil.shorten(summary,60);
																String content = JournalContentUtil.getContent(themeDisplay.getParentGroupId(),onearticle.getArticleId(), null, languageId,themeDisplay);
										%>
										<td><div class="box">
												<span
													style="color: #EB3528; font-weight: bold; cursor: default;">
													<%=displayDate.toUpperCase(locale)%> </span><br>
												<div style="width: 200px; height: 30px; cursor: default;"><%=shortsummary.toUpperCase(locale)%></div>
												<br>
												<P>
													<a
														href="<portlet:actionURL>
														<portlet:param name="myaction" value="showArticle" />
														<portlet:param name="title" value="<%=onearticle.getTitleCurrentValue()%>" />
														<portlet:param name="articleId" value="<%=String.valueOf(onearticle.getId())%>" />
														<portlet:param name="redirect" value="<%=redirect%>" />
													</portlet:actionURL>">
														<span class="learnMoreBtn"><spring:message code='hyperlink-learn-more'/> <img src="<%=themeDisplay.getPathThemeCss()%>/../images/right_arrow_small.png"  alt=">>" ></span></a>
												</P>
											</div></td>
										<%
											}
										%>
									</tr>
								</table></li>
						</ul>
					</div></td>
			</tr>
		</table>
	</div>
	<%
		} else {
	%>
	<div id="loopedSlider" style="width: 100%;">
		<table class="webcontentTable">
			<tr>
				<td style="padding: 2px 2px 2px 2px;"><a href="#" style="padding: 2px 2px 2px 2px;" class="previous"> <img style="padding: 2px 2px 2px 2px;"
						src="<%=themeDisplay.getPathThemeCss()%>/../images/left_arrow.png" /> </a></td>
				<td class="NewssecondColumn">
					<div class="container" style="width: 100%;">
						<ul class="slides" style="width: 100%;">
							<%
								int i = 0;
													int j = 0;
													int nextLoop = size / 4;
													int remaining = size % 4;
													int counter = 0;
													for (i = 0; i < nextLoop; i++) {
							%>
							<li>
								<table>
									<tr>
										<%
											for (j = 0; j < 4; j++) {
																		JournalArticle onearticle = latestarticles.get(counter++);
																		String languageId = LanguageUtil.getLanguageId(renderRequest);
																		//String summary = onearticle.getDescription(locale);
																		String summary = onearticle.getTitle(locale);
																		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy",locale);
																		String displayDate = sdf.format(onearticle.getDisplayDate());
																		String shortsummary = StringUtil.shorten(summary,60);
																		String content = JournalContentUtil.getContent(themeDisplay.getParentGroupId(),onearticle.getArticleId(), null,languageId, themeDisplay);
										%>
										<td><div class="box">
												<span style="color: #EB3528; font-weight: bold"> <%=displayDate.toUpperCase(locale)%>
												</span><br>
												<div style="width: 200px; height: 30px"><%=shortsummary.toUpperCase(locale)%></div>
												<br>
												<P>
													<a
														href="<portlet:actionURL>
														<portlet:param name="myaction" value="showArticle" />
														<portlet:param name="title" value="<%=onearticle.getTitleCurrentValue()%>" />
														<portlet:param name="articleId" value="<%=String.valueOf(onearticle.getId())%>" />
														<portlet:param name="redirect" value="<%=redirect%>" />
														</portlet:actionURL>">
														<span class="learnMoreBtn"><spring:message code='hyperlink-learn-more'/> <img src="<%=themeDisplay.getPathThemeCss()%>/../images/right_arrow_small.png"  alt=">>" ></span></a>
												</P>
											</div></td>
										<%
											}
										%>
									</tr>
								</table></li>
							<%
								}
							%>
							<%
								if (remaining > 0) {
							%>
							<li>
								<table>
									<tr>
										<%
											for (j = 0; j < 4; j++) {
																	JournalArticle onearticle = latestarticles.get((counter++ % size));
																	String languageId = LanguageUtil.getLanguageId(renderRequest);
																	//String summary = onearticle.getDescription(locale);
																	String summary = onearticle.getTitle(locale);
																	SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy",locale);
																	String displayDate = sdf.format(onearticle.getDisplayDate());
																	String shortsummary = StringUtil.shorten(summary,60);
																	String content = JournalContentUtil.getContent(themeDisplay.getParentGroupId(),onearticle.getArticleId(), null,
																										languageId, themeDisplay);
										%>
										<td>
											<div class="box">
												<span style="color: #EB3528; font-weight: bold;"> <%=displayDate.toUpperCase(locale)%>
												</span><br>
												<div style="width: 200px; height: 30px"><%=shortsummary.toUpperCase(locale)%></div>
												<br>
												<P>
													<a
														href="
														<portlet:actionURL>
															<portlet:param name="myaction" value="showArticle" />
															<portlet:param name="title" value="<%=onearticle.getTitleCurrentValue()%>" />
															<portlet:param name="articleId" value="<%=String.valueOf(onearticle.getId())%>" />
															<portlet:param name="redirect" value="<%=redirect%>" />
														</portlet:actionURL>">
														<span class="learnMoreBtn"><spring:message code='hyperlink-learn-more'/> <img src="<%=themeDisplay.getPathThemeCss()%>/../images/right_arrow_small.png"  alt=">>" ></span></a>
												</P>
											</div></td>
										<%
											}
										%>
									</tr>
								</table></li>
							<%
								}
							%>
						</ul>
					</div></td>

				<td><a href="#" class="next" style="padding: 2px 2px 30px 2px;"> <img src="<%=themeDisplay.getPathThemeCss()%>/../images/right_arrow.png" /> </a></td>
			</tr>
		</table>
	</div>
	<%
		}
	%>

	<%
		} else {
	%>
	<div style="margin-left: 10px; margin-right: 10px;">
		<b><spring:message code='validation.message' /></b>
	</div>
	<br>
	<%
		}
	%>
	<br>
	<div style="clear: both;"></div>
</div>
<script type="text/javascript" charset="utf-8">
	$(function() {
		$('#loopedSlider').loopedSlider({
			autoHeight : 800
		});
	});
</script>