<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="java.util.Map"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticle"%>
<%@page import="com.coach.cip.util.Constants"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.kernel.util.StringUtil"%>
<%@page import="com.liferay.portal.kernel.util.HtmlUtil"%>
<%@page import="com.liferay.portal.webserver.WebServerServletTokenUtil"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticleDisplay"%>
<%@page
	import="com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>

<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<%
	String newsDetailRedirect = PortalUtil.getCurrentURL(renderRequest);
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);
	JournalArticle journalArticle = (JournalArticle) row.getObject();
	String journalArticleId = String.valueOf(journalArticle.getId());
	Date displayDate = journalArticle.getDisplayDate();
	SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy",locale);
	String displayDateStr = sdf.format(displayDate);
	String languageId = LanguageUtil.getLanguageId(renderRequest);
	Map<String,Object> velocityVarMap = (Map<String, Object>)request.getAttribute(WebKeys.VM_VARIABLES);

	/* JournalArticleDisplay journalArticleDisplay = JournalArticleLocalServiceUtil
			.getArticleDisplay(journalArticle.getGroupId(),
					journalArticle.getArticleId(), null,
					languageId, themeDisplay); */
	JournalArticleDisplay journalArticleDisplay =
		JournalArticleLocalServiceUtil.getArticleDisplay(
			journalArticle, journalArticle.getTemplateId(), null, languageId, 1, null, themeDisplay);
%>
<table>
	<tr>
		<td style="padding: 1px;color:#EB3528"><c:out value="<%=displayDateStr.toUpperCase() %>"></c:out>
		</td>
	</tr>
	<c:if test="<%= journalArticleDisplay.isSmallImage() %>">
		<%
			String src = StringPool.BLANK;
				if (Validator.isNotNull(journalArticleDisplay.getSmallImageURL())) {
					src = journalArticleDisplay.getSmallImageURL();
				}
				else {
					src =
						themeDisplay.getPathImage() + "/journal/article?img_id=" + journalArticleDisplay.getSmallImageId() + "&t=" +
							WebServerServletTokenUtil.getToken(journalArticleDisplay.getSmallImageId());
				}
		%>
		<div class="news-asset-small-image">
			<img alt="" class="news-asset-small-image"
				src="<%=HtmlUtil.escape(src)%>" width="150" />
		</div>
	</c:if>
	<%
		//String summary = journalArticleDisplay.getDescription();
		String summary = journalArticle.getTitle(locale);
		if (Validator.isNull(summary)) {
			summary = StringUtil.shorten(HtmlUtil.stripHtml(journalArticleDisplay.getContent()), Constants.NEWS_ABSTRACT_LENGTH);
		}
	%>
	<tr>
		<td style="padding: 0px;"><%=summary%></td>
	</tr>
	<tr>
		<td style="padding: 1px;"><a
			style="font-weight: bold; color: black;"
			href="<portlet:actionURL>
							<portlet:param name="action" value="showFulldetailsAction"/>
							<portlet:param name="journalArticleId" value="<%=journalArticleId %>"/>
							<portlet:param name="displayDateStr" value="<%=displayDateStr %>"/>
							<portlet:param name="newsDetailRedirect" value="<%=newsDetailRedirect %>"/>
				  </portlet:actionURL>"><span class="learnMoreBtn"><spring:message code='hyperlink-learn-more'/> &gt;&gt;</span></a></td>
	</tr>
</table>

