<%@page import="java.util.Map"%>
<%@page import="com.liferay.portlet.asset.model.AssetCategory"%>
<%@page import="com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@page import="com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="com.liferay.portlet.asset.model.AssetVocabulary"%>
<%@page import="java.util.List"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<style>
.test {
	color: #ff9454 !important;
}
</style>
<link rel="stylesheet" type="text/css"
	href="<%=themeDisplay.getPathThemeCss()%>/sidenav.css" />
	
<script type='text/javascript'
	src='<%=themeDisplay.getPathThemeCss()%>/../js/jquery.cookie.js'></script>
<script type='text/javascript'
	src='<%=themeDisplay.getPathThemeCss()%>/../js/jquery.hoverIntent.minified.js'></script>
<script type='text/javascript'
	src='<%=themeDisplay.getPathThemeCss()%>/../js/jquery.dcjqaccordion.2.7.min.js'></script>
	
<script type="text/javascript">
	$(document).ready(function($) {
	
		$('#accordion-vocabulory').dcAccordion({
			eventType : 'click',
			autoClose : true,
			saveState : false,
			disableLink : false,
			speed : 'fast',
			classActive : 'test',
			showCount : false,
			autoExpand : true,
			classExpand : 'dcnv-current-parent'
		});
	});
</script>

<% 
List<AssetVocabulary> listVocab = (List<AssetVocabulary>)request.getAttribute("vocabList");
Map<Long, List<AssetCategory>> catMap = (Map<Long, List<AssetCategory>>)request.getAttribute("categoryMap");
String categoryId = renderRequest.getParameter("catId");
%>
<div class='grey demo-container'>
<ul class='accordion' id='accordion-vocabulory'>
<portlet:actionURL var="allUrl" >
	<portlet:param name="myaction" value="listAllCatContent" />
	<portlet:param name="catId" value="all-news" />
</portlet:actionURL>
<%if(categoryId == null || categoryId.equalsIgnoreCase("all-news")){ %>

	<li class="dcnv-current-parent">
<%}
else{%>
<li>
<%} %>	
	
	<a href="<%=allUrl%>"><spring:message code='label.ca.allcategories'/></a></li>
	
<%
if(listVocab !=null && listVocab.size()>0){%>

	<%for(AssetVocabulary vocab : listVocab){
			List<AssetCategory> lstCat = catMap.get(vocab.getVocabularyId());
			if(lstCat !=null && lstCat.size()>0){ 
					for(AssetCategory category : lstCat){
						if(categoryId !=null && categoryId.equalsIgnoreCase(String.valueOf(category.getCategoryId()))){%>
							<li class="dcnv-current-parent">	
						<%}
						else{%>
							<li>
						<%}%>
						<a href="<portlet:actionURL>
									<portlet:param name="myaction" value="listAllCatContent" />
									<portlet:param name="catId" value="<%= String.valueOf(category.getCategoryId()) %>" />
								</portlet:actionURL>" ><%= category.getTitle(locale)%></a></li>
					<%}
			} 		
	}		
		
}	
%>
</ul>	
</div>	


