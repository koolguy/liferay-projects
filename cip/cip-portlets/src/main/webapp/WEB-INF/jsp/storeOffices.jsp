<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="com.coach.cip.util.Constants"%>
<%@page import="com.coach.cip.util.WebUtil"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticle"%>
<%@page import="com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil"%>
<%@page import="com.liferay.portlet.journalcontent.util.JournalContentUtil"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<script>
			Liferay.on(
		           'retrieveValue',
		           function(event, data) {
		        	   popup(data);
					}
				);
			
			function popup(tempData){
				var url = '<portlet:resourceURL id="populateStoreOffices"></portlet:resourceURL>'+ '?fakeId=345&tempData='+ tempData;
				var xhr = new XMLHttpRequest();
				xhr.onreadystatechange = function() {
				if (xhr.readyState == 4 && xhr.status==200) {
					var responseText = document.getElementById("officesId");
					responseText.innerHTML = xhr.responseText;
					}
				};
				xhr.open("GET", url, true);
				xhr.send();
			}
			
</script>
<style>
#officesId {
font-family: verdana;
font-size: 11px;
}
</style>

<c:set var="fileTitle" value="${fileTitle}"/>
<c:set var="fileURL" value="${fileURL}"/>
<c:set var="backButton" value="${backButton}"/>
<c:set var="redirectUrl" value="${redirectUrl}"/>
<c:set var="redirect" value="${redirect}"/>


<%
	ResourceBundle rb = ResourceBundle.getBundle("/content/Language", locale);
	String defaultOffice = "";
	String articleContent = "";
	String articleType = "";
	String fileURL = pageContext.getAttribute("fileURL") != null ? (String)pageContext.getAttribute("fileURL") : "";
	String fileTitle = pageContext.getAttribute("fileTitle") != null ? (String)pageContext.getAttribute("fileTitle") : "";
	String backButton = pageContext.getAttribute("backButton") != null ? (String)pageContext.getAttribute("backButton") : "";
	
	if(backButton.equalsIgnoreCase("true")){
		defaultOffice = fileTitle;
		JournalArticle journalArticle = JournalArticleLocalServiceUtil.getArticle(themeDisplay.getScopeGroupId(), fileURL);
		articleType = journalArticle.getType();
		articleType = articleType.split("-")[0];
		articleContent = JournalContentUtil.getContent(themeDisplay.getScopeGroupId(),fileURL, null, null, themeDisplay.getLanguageId(), themeDisplay);
	}else {
		defaultOffice = rb.getString(Constants.OFFICES_NA);
   	    JournalArticle journalArticle = WebUtil.getArticlesByTypeForOffices(themeDisplay.getCompanyId(), themeDisplay.getScopeGroupId(), "coach-offices", defaultOffice, -1).get(0);
	    articleContent = JournalContentUtil.getContent(themeDisplay.getScopeGroupId(),journalArticle.getArticleId(), null, null, themeDisplay.getLanguageId(), themeDisplay);
	    articleType = journalArticle.getType();
	    articleType = articleType.split("-")[0];
	}
	
%>
<div id="officesId" style="border: 1px solid black; margin-bottom: 10px;">
					<header class="portlet-storeDetail-topper">
					<h1 class="portlet-title">
						<span class="portlet-title-text"><spring:message
								code='label.title.offices' /> - <%=articleType.toUpperCase() %> - <%=defaultOffice %></span>
								
					</h1>
					</header>
<div id="jacontent" style="padding:10px;"><%=articleContent %></div>

<div>
	<%
	if(backButton.equalsIgnoreCase("true")){
	%>
			<div>
			<input style="cursor:default;background-color: black; color: white; padding: 1px; margin-left: 9px; margin-bottom: 5px; 
			height: 34px; width: 94px; border: none;" type="button" value="BACK" onClick="javascript:location.href = '${redirect}'" />
			</div>
	<%} %>
</div>
</div>

