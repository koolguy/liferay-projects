<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.coach.cip.util.Constants"%>
<%@page import="java.util.ResourceBundle"%>
<%@page import="java.util.Collections"%>
<%@page import="javax.portlet.PortletContext"%>
<%@page import="org.springframework.ui.Model"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.liferay.portal.service.RegionServiceUtil"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ page import="java.util.List"%>
<%@ page import="javax.portlet.PortletPreferences"%>
<%@ page import="javax.portlet.ActionRequest"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="com.coach.stores.service.beans.GetOpenStoreByGeographyResponse.Stores.Store"%>
<%@page import="com.coach.cip.util.WebUtil"%>
<%@page import="javax.portlet.PortletSession"%>
<%@page import="org.springframework.ws.client.core.WebServiceTemplate"%>
<%@page import="com.coach.stores.service.beans.GetOpenStoreByGeographyRequest"%>
<%@page import="com.coach.stores.service.beans.GetOpenStoreByGeographyResponse"%>
<%@page import="com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyResponse.StoresDesc.StoreDesc"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticle"%>
<%@page import="com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil"%>
<%@page import="com.liferay.portlet.journalcontent.util.JournalContentUtil"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<!--  jquery core -->
<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery-ui-1.8.19.custom.min.js"></script>

<!--  styled select box script version 1 -->
<script
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.selectbox-0.5.js"
	type="text/javascript">
</script>
<script type="text/javascript">
		$(document).ready(function() {
		$('.styledselect').selectbox({
			inputClass : "selectbox_styled"
		});

		Liferay.on(
		           'retrieveValue',
		           function(event, data) {
		        	   popup(data);
					}
				);
			
			function popup(tempData){
				
				var url = '<portlet:resourceURL id="populateStoreOffices"></portlet:resourceURL>'+ '?fakeId=345&tempData='+ tempData;
				var xhr;
				
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
					xhr=new XMLHttpRequest();
				}
				else
				{// code for IE6, IE5
					xhr=new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xhr.onreadystatechange = function() {
				if (xhr.readyState == 4 && xhr.status==200) {
					var responseText = document.getElementById("officesId");
					responseText.innerHTML = xhr.responseText;
					}
				};
				xhr.open("GET", url, true);
				xhr.send();
			}
	
       var currUrl=document.location.href;
	   if($("currUrl:conatins('stores')")){
              $("#storeAddressForm").parent().show();

	   }
	   
	   $("#target1").live('focus', function() {
			$(this).selectbox({
				inputClass : "styledselect_form_1"
			});
		});

		$("#target2").live('focus', function() {
			$(this).selectbox({
				inputClass : "styledselect_form_1"
			});
		});
		$("#target3").live('focus', function() {
			$(this).selectbox({
				inputClass : "styledselect_form_1"
			});
		});
		$("#target4").live('focus', function() {
			$(this).selectbox({
				inputClass : "styledselect_form_1"
			});
		});
		$('.styledselect_form_1').selectbox({
			inputClass : "styledselect_form_1"
		});
        
		
		$("#geoTable").find("tr").find("a").removeClass("selectedgeo");
			var storeName=$("span.portlet-title-text").text().split("-")[1];
		    if($.trim(storeName)=="COACH"){
				$('#geoTable tr').each(function() {
					var name = $(this).text();
					if ($.trim(name) == $("#geoName").val()) {
						$(this).find("a").addClass("selectedgeo");
					}
				  });
			 }
		  else{
				$('#rkgeoTable tr').each(function() {
				var name = $(this).text();
				if ($.trim(name) == $("#geoName").val()) {
				$(this).find("a").addClass("selectedgeo");
				}
			});
		 } 

  $('#offTable').find('a.officesAnchor').click(function(){

	     $("#storeAddressForm").parent().hide();
		     $("#officesId").show();
			 Liferay.on(
		           'retrieveValue',
		           function(event, data) {
		        	   popup(data);
					}
				);
			
			function popup(tempData){
				var url = '<portlet:resourceURL id="populateStoreOffices"></portlet:resourceURL>'+ '?fakeId=345&tempData='+ tempData;
				var xhr;
				
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
					xhr=new XMLHttpRequest();
				}
				else
				{// code for IE6, IE5
					xhr=new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xhr.onreadystatechange = function() {
				if (xhr.readyState == 4 && xhr.status==200) {
					var responseText = document.getElementById("officesId");
					responseText.innerHTML = xhr.responseText;
					}
				};
				xhr.open("GET", url, true);
				xhr.send();
			}
  });


   $('#rkoffTable').find('a.officesAnchor').click(function(){
            
     $("#officesId").show();
			 Liferay.on(
		           'retrieveValue',
		           function(event, data) {
		        	   popup(data);
					}
				);
			
			function popup(tempData){
				var url = '<portlet:resourceURL id="populateStoreOffices"></portlet:resourceURL>'+ '?fakeId=345&tempData='+ tempData;
				var xhr;
				
				if (window.XMLHttpRequest)
				  {// code for IE7+, Firefox, Chrome, Opera, Safari
					xhr=new XMLHttpRequest();
				}
				else
				{// code for IE6, IE5
					xhr=new ActiveXObject("Microsoft.XMLHTTP");
				}
				
				xhr.onreadystatechange = function() {
				if (xhr.readyState == 4 && xhr.status==200) {
					var responseText = document.getElementById("officesId");
					responseText.innerHTML = xhr.responseText;
					}
				};
				xhr.open("GET", url, true);
				xhr.send();
			}

	     $("#storeAddressForm").parent().hide();
  });

	});
</script>

<!--  styled select box script version 3 -->
<script
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.selectbox-0.5_style_2.js"
	type="text/javascript">
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.styledselect_pages').selectbox({
			inputClass : "styledselect_pages"
		});
	});
</script>

<style>
.selectedgeo{font-weight:bold;color: #E8317D;}
	#storeAddressForm {
	font-family: verdana;
	font-size: 11px;
}
#officesId {
	font-family: verdana;
	font-size: 11px;
}

.styledselect_form_1 {
	background: url(<%=themeDisplay.getPathThemeCss()%>/../images/select2.JPG) left no-repeat !important;
	border: none;
	border-left: none;
	color: #393939;
	cursor: pointer;
	display: block;
	font-family: Arial;
	font-size: 12px;
	height: 20px !important;
	margin: 0px 0px 0px 0px;
	padding: 7px 0 4px 6px;
	text-align: left;
	width: 215px !important;
	}
</style>

<script type="text/javascript">
	$(document)
			.ready(
					function() {

						$("#target1_container")
								.find("ul")
								.find("li")
								.live(
										'click',
										function() {
											var time = new Date().getTime();
											var status = true;
											var countryId;
											var countryName = $(this).text();
											if($.trim(countryName) == '<spring:message code='label.stores.choose.country'/>'){
												status = false;
											}
											if(status){
											$("#target1 > option")
													.each(
															function() {
																if (countryName == this.text) {
																	countryId = this.value;
																}

															});
											if (countryId != 0) {
												var xhr1;
												
												if (window.XMLHttpRequest)
												  {// code for IE7+, Firefox, Chrome, Opera, Safari
													xhr1=new XMLHttpRequest();
												}
												else
												{// code for IE6, IE5
													xhr1=new ActiveXObject("Microsoft.XMLHTTP");
												}
												
												xhr1.onreadystatechange = function() {
													if (xhr1.readyState == 4 && xhr1.status==200) {
														var messageText1 = document
																.getElementById("<portlet:namespace/>stateText");
														messageText1.innerHTML = xhr1.responseText;

														$("#target2").focus();
													}
												};
												//Added time parameter to fix ajax cache issue(Alternate: use post method instead of GET to fix cache issue).
												var url1 = '<portlet:resourceURL id="populateStates"></portlet:resourceURL>'
														+ '?fakeId='+time+'&countryId='
														+ countryId;
												xhr1.open("GET", url1, true);
												xhr1.send();
												
												//Populating cities for the city level
												var xhr2;
												
												if (window.XMLHttpRequest)
												  {// code for IE7+, Firefox, Chrome, Opera, Safari
													xhr2=new XMLHttpRequest();
												}
												else
												{// code for IE6, IE5
													xhr2=new ActiveXObject("Microsoft.XMLHTTP");
												}
												xhr2.onreadystatechange = function() {
													if (xhr2.readyState == 4 && xhr2.status==200) {
														var messageText2 = document
																.getElementById("<portlet:namespace/>cityText");
														messageText2.innerHTML = xhr2.responseText;
														$("#target3").focus();
													}
												};
												var url2 = '<portlet:resourceURL id="populateCities"></portlet:resourceURL>'
														+ '?fakeId='+time+'&countryId='
														+ countryId;
												xhr2.open("GET", url2, true);
												xhr2.send();
												
												//Populating stores for the country level dropdown
												var xhr3;
												
												if (window.XMLHttpRequest)
												  {// code for IE7+, Firefox, Chrome, Opera, Safari
													xhr3=new XMLHttpRequest();
												}
												else
												{// code for IE6, IE5
													xhr3=new ActiveXObject("Microsoft.XMLHTTP");
												}
												
												xhr3.onreadystatechange = function() {
													if (xhr3.readyState == 4 && xhr3.status==200) {
														var messageText3 = document
																.getElementById("<portlet:namespace/>storeText");
														messageText3.innerHTML = xhr3.responseText;

														$("#target4").focus();
													}
												};
												var url3 = '<portlet:resourceURL id="populateStores"></portlet:resourceURL>'
														+ '?fakeId='+time+'&countryId='
														+ countryId;
												xhr3.open("GET", url3, true);
												xhr3.send();
											}}
										});

						$("#target2_container")
								.find("ul")
								.find("li")
								.live(
										'click',
										function() {
											var time = new Date().getTime();
											var status = true;
											var stateId;
											var stateName = $(this).text();
											if($.trim(stateName) == '<spring:message code='label.stores.choose.state'/>'){
												status = false;
											}
											if(status){
											$("#target2 > option")
													.each(
															function() {
																if (stateName == this.text) {
																	stateId = this.value;
																}
															});
											if (stateId != 0) {
												var xhr2;
												
												if (window.XMLHttpRequest)
												  {// code for IE7+, Firefox, Chrome, Opera, Safari
													xhr2=new XMLHttpRequest();
												}
												else
												{// code for IE6, IE5
													xhr2=new ActiveXObject("Microsoft.XMLHTTP");
												}
												
												xhr2.onreadystatechange = function() {
													if (xhr2.readyState == 4 && xhr2.status==200) {
														var messageText2 = document
																.getElementById("<portlet:namespace/>cityText");
														messageText2.innerHTML = xhr2.responseText;
														$("#target3").focus();
													}
												};
												var url2 = '<portlet:resourceURL id="populateCities"></portlet:resourceURL>'
														+ '?fakeId='+time+'&stateId='
														+ stateId;
												xhr2.open("GET", url2, true);
												xhr2.send();
												
												//Populating stores for the state level dropdown
												var xhr3;
												
												if (window.XMLHttpRequest)
												  {// code for IE7+, Firefox, Chrome, Opera, Safari
													xhr3=new XMLHttpRequest();
												}
												else
												{// code for IE6, IE5
													xhr3=new ActiveXObject("Microsoft.XMLHTTP");
												}
												
												xhr3.onreadystatechange = function() {
													if (xhr3.readyState == 4 && xhr3.status==200) {
														var messageText3 = document
																.getElementById("<portlet:namespace/>storeText");
														messageText3.innerHTML = xhr3.responseText;
														$("#target4").focus();
													}
												};
												var url3 = '<portlet:resourceURL id="populateStores"></portlet:resourceURL>'
														+ '?fakeId='+time+'&stateId='
														+ stateId;
												xhr3.open("GET", url3, true);
												xhr3.send();
												
												
												var xhr5;
												if (window.XMLHttpRequest)
												  {// code for IE7+, Firefox, Chrome, Opera, Safari
													xhr5=new XMLHttpRequest();
												}
												else
												{// code for IE6, IE5
													xhr5=new ActiveXObject("Microsoft.XMLHTTP");
												}
												
												xhr5.onreadystatechange = function() {
													if (xhr5.readyState == 4 && xhr5.status==200) {
														var messageText5 = document
																.getElementById("<portlet:namespace/>countryText");
														messageText5.innerHTML = xhr5.responseText;
														$("#target1").focus();
													}
												};
												var url5 = '<portlet:resourceURL id="populateCountries"></portlet:resourceURL>'
														+ '?fakeId='+time+'&stateId='
														+ stateId;
												xhr5.open("GET", url5, true);
												xhr5.send();  
												
												
											}}
										});

						$("#target3_container")
								.find("ul")
								.find("li")
								.live(
										'click',
										function() {
											var time = new Date().getTime();
											var status = true;
											var cityId;
											var cityName = $(this).text();
											if($.trim(cityName) == '<spring:message code='label.stores.choose.city'/>'){
												status = false;
											}
											if(status){
											$("#target3 > option")
													.each(
															function() {
																if (cityName == $(
																		this)
																		.text()) {
																	cityId = this.value;
																}

															});
											if (cityId != 0) {
												$("#city_errors").hide();
												var xhr3;
												
												if (window.XMLHttpRequest)
												  {// code for IE7+, Firefox, Chrome, Opera, Safari
													xhr3=new XMLHttpRequest();
												}
												else
												{// code for IE6, IE5
													xhr3=new ActiveXObject("Microsoft.XMLHTTP");
												}
												
												xhr3.onreadystatechange = function() {
													if (xhr3.readyState == 4 && xhr3.status==200) {
														var messageText3 = document
																.getElementById("<portlet:namespace/>storeText");
														messageText3.innerHTML = xhr3.responseText;
														$("#target4").focus();
													}
												};
												var url3 = '<portlet:resourceURL id="populateStores"></portlet:resourceURL>'
														+ '?fakeId='+time+'&cityId='
														+ cityName;
												xhr3.open("GET", url3, true);
												xhr3.send();
												
												
												var xhr5;
												if (window.XMLHttpRequest)
												  {// code for IE7+, Firefox, Chrome, Opera, Safari
													xhr5=new XMLHttpRequest();
												}
												else
												{// code for IE6, IE5
													xhr5=new ActiveXObject("Microsoft.XMLHTTP");
												}
												
												xhr5.onreadystatechange = function() {
													if (xhr5.readyState == 4 && xhr5.status==200) {
														var messageText5 = document
																.getElementById("<portlet:namespace/>countryText");
														messageText5.innerHTML = xhr5.responseText;
														$("#target1").focus();
													}
												};
												var url5 = '<portlet:resourceURL id="populateCountries"></portlet:resourceURL>'
														+ '?fakeId='+time+'&cityId='
														+ cityName;
												xhr5.open("GET", url5, true);
												xhr5.send();  
												
												
												
												
												 var xhr4;
												if (window.XMLHttpRequest)
												  {// code for IE7+, Firefox, Chrome, Opera, Safari
													xhr4=new XMLHttpRequest();
												}
												else
												{// code for IE6, IE5
													xhr4=new ActiveXObject("Microsoft.XMLHTTP");
												}
												
												xhr4.onreadystatechange = function() {
													if (xhr4.readyState == 4 && xhr4.status==200) {
														var messageText4 = document
																.getElementById("<portlet:namespace/>stateText");
														messageText4.innerHTML = xhr4.responseText;
														$("#target2").focus();
													}
												};
												var url4 = '<portlet:resourceURL id="populateStates"></portlet:resourceURL>'
														+ '?fakeId='+time+'&cityId='
														+ cityName;
												xhr4.open("GET", url4, true);
												xhr4.send(); 
											
											}}
										});

						$("#target4_container").find("ul").find("li").live(
								'click',
								function() {
									var status = true;
									var storeid;
									var storeName = $(this).text();
									if($.trim(storeName) == '<spring:message code='label.stores.choose.store'/>'){
										status = false;
									}
									if(status){
									$("#storeAddressForm").submit();
									}

								});

						$("#submitId").click(
								function() {

									var status = false;
									var countryStatus = false;
									var stateStatus = false;
									var cityStatus = false;
									var storeStatus = false;

									var country = $("#target1").val();
									if (country == 0) {
										$("#country_errors").show();
										countryStatus = false;
									} else {
										$("#country_errors").hide();
										countryStatus = true;
									}

									var state = $("#target2").val();
									if (state == 0) {
										$("#state_errors").show();
										stateStatus = false;
									} else {
										$("#state_errors").hide();
										stateStatus = true;
									}

									var city = $("#target3").val();
									if (city == 0) {
										$("#city_errors").show();
										cityStatus = false;
									} else {
										$("#city_errors").hide();
										cityStatus = true;
									}

									var store = $("#target4").val();
									if (store == 0) {
										$("#store_errors").show();
										storeStatus = false;
									} else {
										$("#store_errors").hide();
										storeStatus = true;
									}

									if (countryStatus && stateStatus
											&& cityStatus && storeStatus) {
										status = true;
									} else {
										status = false;
									}
									if (status) {
										$("#storeAddressForm").submit();
									} else {
										return status;
									}

								});

					});
</script>

	<portlet:renderURL var="storeAddressURLForm">
		<portlet:param name="myaction" value="displayStoreDetailsForm"></portlet:param>
	</portlet:renderURL>
	
	<portlet:actionURL var="storeAddressURL">
		<portlet:param name="myaction" value="displayStoreDetails"></portlet:param>
	</portlet:actionURL>
	
	<c:set var="geographyName" value="${geographyName}"/>
	<c:set var="storeDesclist" value="${storeDesclist}"/>
	<c:set var="openStoreDesclist" value="${openStoreDesclist}"/>
	<c:set var="brand" value="${brand}"/>
	
	<%
	ResourceBundle rb = ResourceBundle.getBundle("/content/Language",locale);
	String articleContent = "";
	String geographyName = "";
	String brand = "";
	String message = null;
	String storeMessage = null;
	List<StoreDesc> tempStoreslist = new ArrayList<StoreDesc>();
	List<String> countriesList = new ArrayList<String>();
	List<String> countryStateList = new ArrayList<String>();
	List<String> countryCityList = new ArrayList<String>();
	List<String> countryStoresList = new ArrayList<String>();
	
	if(pageContext.getAttribute("brand") != null){
		brand = (String)pageContext.getAttribute("brand");
	}
		
	if(pageContext.getAttribute("geographyName") != null){
		geographyName = (String)pageContext.getAttribute("geographyName");
	}
	if(pageContext.getAttribute("openStoreDesclist") != null){
		tempStoreslist = (List<StoreDesc>)pageContext.getAttribute("openStoreDesclist");
	}
	if(tempStoreslist != null && !tempStoreslist.isEmpty()){
	for(StoreDesc store : tempStoreslist){
		if(store.getCOUNTRY() != null || !store.getCOUNTRY().isEmpty()){
			countriesList.add(store.getCOUNTRY().trim());
		}
		if(store.getSTORENAME() != null && !store.getSTORENAME().isEmpty()){
			countryStoresList.add(store.getSTORENAME().trim());
		}
		if(store.getSTATE() != null && !store.getSTATE().isEmpty()){
			countryStateList.add(store.getSTATE().trim());
		}
		if(store.getCITY() != null && !store.getCITY().isEmpty()){
			countryCityList.add(store.getCITY().trim());
		}
		
	}
	}
	if(countriesList != null && !countriesList.isEmpty()){
		countriesList = WebUtil.removeDuplicates(countriesList);
		Collections.sort(countriesList);
	}
	if(countryStoresList != null && !countryStoresList.isEmpty()){
		countryStoresList = WebUtil.removeDuplicates(countryStoresList);
		Collections.sort(countryStoresList);
	}
	if(countryStateList != null && !countryStateList.isEmpty()){
		countryStateList = WebUtil.removeDuplicates(countryStateList);
		Collections.sort(countryStateList);
	}
	if(countryCityList != null && !countryCityList.isEmpty()){
		countryCityList = WebUtil.removeDuplicates(countryCityList);
		Collections.sort(countryCityList);
	}
	%>
	
<div style="border: 1px solid black; margin-bottom: 10px;display:none">
<form id="storeAddressForm" method="post" action="${storeAddressURL}" autocomplete="off">
	
	<div>
	<%  boolean menuNavigation = false; 
	 	boolean sideNavigation = false;
	 	String navName = null;
	 	String menuName = null;
	 	String geoName = null;
	%>
		<c:choose>
			<c:when
				test="<%= themeDisplay.getLayout().getHTMLTitle(locale).equalsIgnoreCase(\"stores\")%>">
				<div>
					<header class="portlet-storeDetail-topper">
					<h1 class="portlet-title">
						<span class="portlet-title-text"><spring:message
								code='label.title.stores' /> - <%=brand%> - <%=geographyName%></span>
								
								<% 
								if(geographyName != null && !geographyName.isEmpty()){
								   navName = geographyName.toUpperCase();
								}
								   menuName = null;
								    menuNavigation = true;
									
								%>
					</h1>
					</header>
				</div>
			</c:when>
			<c:otherwise>
				<div>
					<header class="portlet-storeDetail-topper">
					<h1 class="portlet-title">
						<span class="portlet-title-text"><spring:message
								code='label.title.stores' /> - <%=themeDisplay.getLayout().getName().toUpperCase()%></span>
								<% List<Store> list = new ArrayList<Store>();
								   String geoName1 =themeDisplay.getLayout().getHTMLTitle(locale);
								   if(geoName1 != null && !geoName1.isEmpty()){
								   menuName = geoName1.toUpperCase();
								   }
								   navName = null;
								   sideNavigation = true;
								%>
							
					</h1>
					</header>
				</div>
			</c:otherwise>
		</c:choose>
		</div>
		<div style="margin-left: 10px; margin-top: 10px; color: red; font-weight: bold;">
		<c:if test="${FaultCode ne null}">
		<spring:message code='${FaultMessage}'/>
		</c:if>
		</div>
		<%
			if (countryStoresList.size() == 0 || countryStoresList.isEmpty()) {
				storeMessage = rb.getString(Constants.NO_STORES_FOUND);
			}
		%>
		
		<div style="width:675px; padding-left:15px;">
		<div class="stores-intro-message">
			<spring:message code='label.stores.intro.new.message' />
		</div>
		<div class="stores-intro-message1">
			<spring:message code='label.stores.intro.new.message1' />
		</div>
		</div>
		
		<div>&nbsp;</div>
		<c:if test="${FaultCode eq null}">
		<div id="store_erros" style="margin-left:8px;color:red;margin-bottom: 8px;font-weight:bold;margin-top: -10px;">
			<%if(storeMessage != null) { %>
				<%=storeMessage%>
			<%} %>
		
		</div>
		</c:if>
		<table style="margin-left: 7px; padding-left: 15px;">
		
			<tr id="storeRow">
				<td colspan="2" style="height:58px;" valign="top">
					
			<div id="rightdiv" style="border: 2px solid #1296E0;padding: 11px 10px 12px 10px;width: 270px;float: left;">
					<div id="<portlet:namespace/>countryText" style="margin-bottom: 20px;margin-top: 10px;margin-left:25px;">
						<Select name="countryId" id="target1" class="styledselect_form_1">
							<%
								if (countriesList.size() == 0 || countriesList.isEmpty()) {
							%>
							<option value="0">
								<spring:message code='label.stores.choose.country' />
							</option>
							<%
								} else {
							%>
							<option value="0">
								<spring:message code='label.stores.choose.country' />
							</option>
							<%
								for (String country : countriesList) {
							%>
							<option value="<%=country%>"><%=country%></option>
							<%
								}}
							%>
						</Select>
					</div>
					<div id="<portlet:namespace/>stateText" style="margin-bottom:20px;margin-left:25px;">
						<Select name="stateId" id="target2" class="styledselect_form_1">
						<%
								if (countryStateList.size() == 0 || countryStateList.isEmpty()) {
						%>
							<option value="0">
								<spring:message code='label.stores.choose.state' />
							</option>
						<%} else { %>	
							<option value="0">
								<spring:message code='label.stores.choose.state' />
							</option>
							<%
								for (String state : countryStateList) {
							%>
							<option value="<%=state%>"><%=state%></option>
							<%
								}}
							%>
						</Select>
					</div>
					<div id="<portlet:namespace/>cityText" style="margin-bottom:20px;margin-left:25px;">
						<Select name="cityName" id="target3" class="styledselect_form_1">
						<%
								if (countryCityList.size() == 0 || countryCityList.isEmpty()) {
						%>
							<option value="0">
								<spring:message code='label.stores.choose.city' />
							</option>
						<%} else { %>	
							<option value="0">
								<spring:message code='label.stores.choose.city' />
							</option>
							<%
								for (String city : countryCityList) {
							%>
							<option value="<%=city%>"><%=city%></option>
							<%
								}}
							%>
						</Select>
					</div>
			</div>
			
				<div id="leftdiv" style="border: 2px solid #1296E0;padding: 20px 10px 80px 10px;width: 270px;float: left;margin-left:30px;height:85px;">
					<div id="<portlet:namespace/>storeText" style="margin-left:25px;">
						<Select name="storeName" id="target4" class="styledselect_form_1">
							<%
								if (countryStoresList.size() == 0 || countryStoresList.isEmpty()) {
							%>
							<option value="0">
								<spring:message code='label.stores.choose.store' />
							</option>
							<%
								} else {
							%>
							<option value="0">
								<spring:message code='label.stores.choose.store' />
							</option>
						
							<%
								for (String store : countryStoresList) {
							%>
							<option value="<%=store%>"><%=store%></option>
							<%
								}}
							%>
						</Select>
					</div>
				</div>
			</td>
			</tr>
			
			<tr>
				<td>&nbsp;</td>
			</tr>
		</table>

		<!-- <table>

		<tr>
			<td valign="bottom"><input type="submit" id="submitId"
				value="Submit" name="storeDetailsSubmit"
				style="background-color: black; color: white; padding: 1px; margin-left: 4px; height: 34px; width: 94px; border: none;" />
			</td>
		<tr> 
	</table> -->

	<% 
		geoName = null;
	if(menuNavigation){
		geoName = navName ;
	}else if(sideNavigation){
		geoName = menuName;
		
	}else {
		geoName = null;
	}
	
	%>
<input type="hidden" id="geoName" value="<%=geoName%>">

</form>
</div>

<div id="officesId" style="border: 1px solid black; margin-bottom: 10px;display:none">
					<header class="portlet-storeDetail-topper">
					<h1 class="portlet-title">
						<span class="portlet-title-text"><spring:message
								code='label.title.offices' /> - <spring:message	code='label.title.brand.coach'/> - <spring:message code='label.offices.northamerica'/></span>
								
					</h1>
					</header>
<div id="jacontent" style="padding:10px;"><%=articleContent %></div>
</div>