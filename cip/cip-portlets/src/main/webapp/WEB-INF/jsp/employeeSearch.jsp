<%@page import="org.apache.bcel.classfile.Constant"%>
<%@page import="com.liferay.portal.service.LayoutLocalServiceUtil"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@ page import="java.util.List"%>
<%@ page import="com.coach.cip.common.dto.EmployeeDirectorySearchVO"%>
<%@ page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>

<%@ page import="com.liferay.portlet.PortletURLFactoryUtil"%>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayPortletURL"%>
<%@ page import="javax.portlet.PortletRequest"%>
<%@ page import="javax.portlet.PortletURL"%>
<%@ page import="javax.portlet.PortletMode"%>

<%@ page import="javax.servlet.http.HttpServletRequest"%>
<%@ page import="com.liferay.portal.model.Layout" %>
<%@ page import="com.coach.cip.util.Constants" %>

<%@ page import="java.util.StringTokenizer" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>




<portlet:defineObjects />
<liferay-theme:defineObjects />

<script>
	$(document).ready(function() {
		
		$('.default-value').each(function() {
			var default_value = this.value;
			$(this).focus(function() {
				if(this.value == default_value) {
					this.value = '';
				}
			});
			$(this).blur(function() {
				if(this.value == '') {
					this.value = default_value;
				}
			});
		});	
		
		 $("#employeeSearchId").click(function() {
			var default_value =jQuery.trim("<spring:message code='textbox.employee.search.name'/>");
			var firstNameAndOrLastName = jQuery.trim($("#firstNameAndOrLastName").val());
			if (firstNameAndOrLastName == "" || firstNameAndOrLastName == default_value){
				alert("Please enter FirstName And/Or LastName.");
				$("#firstNameAndOrLastName").focus();
				return false;
			} 
		}); 
		
	});
	
</script>





<%
	String friendlyURL = "/"+Constants.EMPLOYEE_DIRECTORY_FRIENDLY_URL;
	Layout employeeSearchLayout = LayoutLocalServiceUtil.getFriendlyURLLayout(themeDisplay.getLayout().getGroupId(), true, friendlyURL);
	String employeeSearchPortletURL = employeeSearchLayout.getTypeSettingsProperty("column-1");
	String portletId = null;
	StringTokenizer st = new StringTokenizer(employeeSearchPortletURL,",");
	if (st.hasMoreTokens()) {
		portletId = st.nextToken();
	}
	PortletURL employeeSearchUrl = PortletURLFactoryUtil.create(request,portletId,employeeSearchLayout.getPlid(),PortletRequest.ACTION_PHASE);
	employeeSearchUrl.setParameter("myaction","getSearchResult");
	employeeSearchUrl.setParameter("key","fromPortalNormal");
	employeeSearchUrl.setWindowState(WindowState.NORMAL);
	
	
%>

<fmt:bundle basename="content/Language">

	<form:form name="employeeSearchForm" method="post" commandName="employeeDirectorySearchVO" id="employeeSearchFormId" 
																										action="<%=employeeSearchUrl.toString()%>">
		<div id="employeeSearch">
			<table id="empDirTable">
				<tr>
					<td style="vertical-align: top;">
						<input type="text" class="default-value firstNameAndOrLastNameTxtBx" maxlength="200" id="firstNameAndOrLastName" value="<fmt:message key='textbox.employee.search.name' />"
							name="firstNameAndOrLastName" />
					</td>
					<td style="vertical-align: top;">
						<input type="submit" class="button employeeSearchBtn"  value="<fmt:message   key='label.employee.search' />" id="employeeSearchId"/>
						
					</td>
					<td style="padding-left:5px;text-align:center; margin-top: 2px; width:90px;font-size:6px;">
					
							<a href="/<%=themeDisplay.getLanguageId()%>/group<%=themeDisplay.getScopeGroup().getFriendlyURL()%>/employeedirectory" 
							class="advancedSearchLink">
								<fmt:message   key='label.employee.search.advancedSearch' />
							</a>
						
					</td>
				</tr>
			</table>
		</div>
	</form:form>

</fmt:bundle>

