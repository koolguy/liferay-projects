<%@page import="com.coach.cip.common.dto.QuickLinksVO"%>
<%@page import="com.liferay.portal.kernel.portlet.LiferayPortletURL"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.security.auth.AuthTokenUtil"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.liferay.portal.kernel.util.PropsUtil"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="aui" uri="http://alloy.liferay.com/tld/aui"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="java.util.List"%>
<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors"%>
<%@ page import="org.springframework.ui.Model"%>
<%@ page import="java.util.ResourceBundle"%>
<%@page import="com.liferay.portal.kernel.servlet.SessionMessages"%>
<%@ page import="com.liferay.portal.kernel.servlet.SessionErrors"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<link rel="stylesheet"
	href="<%= themeDisplay.getPathThemeCss() %>/screen.css" type="text/css"
	media="screen" title="default" />
<%-- <link href="<%= themeDisplay.getPathThemeCss() %>/style.css"
	rel="stylesheet" type="text/css" /> --%>
	<script type="text/javascript">
	$(document).ready(function() {

		$(".close-yellow").click(function() {
			
			$(".yellow-left").fadeOut("slow");
			$(".yellow-right").fadeOut("slow");
		});
		$(".close-red").click(function() {
			$(".red-left").fadeOut("slow");
			$(".red-right").fadeOut("slow");
		});
		$(".close-blue").click(function() {
			$(".blue-left").fadeOut("slow");
			$(".blue-right").fadeOut("slow");
		});
		$(".close-green").click(function() {

			$(".green-left").fadeOut("slow");
			$(".green-right").fadeOut("slow");
		});
	});
</script>



<portlet:actionURL var="addQuickLinksUrl">
	<portlet:param name="myaction" value="quickLinks" />
	<portlet:param name="selcetedQuickLink" value="addQuickLinks" />
</portlet:actionURL>


	<%
		String addPopup = (String) request.getAttribute("addOpenPopup");
		String addedAssetURL = (String) request.getAttribute("addedAssetURL");
			if (addPopup != null && addPopup.equals("true")) {
	%>
	<input type="hidden" value="<%=addPopup%>" id="addOpenPopup" name="addOpenPopup" />
	<%
		}
	
	if (Validator.isNotNull(addedAssetURL)) {
		%>
			<input type="hidden" value="<%=addedAssetURL%>" id="qlAddedAssetURLId" name="qlAddedAssetURLId" />
		<%
			}
		%>
	<br />
	<form:form name="addQuickLinks" method="post"
		action="${addQuickLinksUrl}" id="addQuickLinksForm">
	
		<c:set var="message" value='${message}'></c:set>
		<%
			String message = (String) pageContext.getAttribute("message");
		%>	
		
		
		<c:if test="${not empty message}">
		<!--  start message-red -->
		<c:if test='<%= SessionErrors.contains(resourceRequest, message) %>'>
		<c:if test="${message.startsWith('error')}">
			<div id="message-red">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="red-left"><liferay-ui:error key="${message}" message="${message}"></liferay-ui:error>
					<%-- 	<c:out value="${message}" /> --%>
						</td>
						<td class="red-right"><a class="close-red"><img
								src="<%= themeDisplay.getPathThemeImages() %>/table/icon_close_red.gif"
								alt="" /> </a></td>
					</tr>
				</table>
			</div>
		</c:if>
		</c:if>
		<c:if test='<%= SessionMessages.contains(resourceRequest, message) %>'>		
		<c:if test="${message.startsWith('success')}">
			<div id="message-green">
				<table border="0" width="100%" cellpadding="0" cellspacing="0">
					<tr>
						<td class="green-left"><liferay-ui:success key="${message}" message="${message}"></liferay-ui:success>
						<%-- <c:out value="${message}" /> --%>
						</td>
						<td class="green-right"><a class="close-green"><img
								src="<%= themeDisplay.getPathThemeImages() %>/table/icon_close_green.gif"
								alt="" /> </a></td>
					</tr>
				</table>
			</div>
		</c:if>
		</c:if>
		</c:if>
	
		<div class="element">
			<%
				String pageName = (String) request.getAttribute("pageName");
			%>
			<c:if test="<%= pageName == null %>">
				<span class="mandetory">*</span>
				<spring:message code="label.quicklinks.aliasname" />
				<c:choose>
					<c:when test="${disableAdd ne null && disableAdd eq true}">
						<input type="text" disabled="disabled" id="addQLPageName" maxlength="75" name="pageName" class="required text"	value="<%=themeDisplay.getLayout().getName(themeDisplay.getLocale())%>"/>
					</c:when>
					<c:otherwise>
						<input type="text" id="addQLPageName" maxlength="75"  name="pageName" class="required text" value="<%=themeDisplay.getLayout().getName(themeDisplay.getLocale())%>"/>
					</c:otherwise>
				</c:choose>
			</c:if>
			<c:if test="<%= pageName != null %>">
				<span class="mandetory">*</span>
				<spring:message code="label.quicklinks.aliasname" />
				<c:choose>
					<c:when test="${disableAdd ne null && disableAdd eq true}">
						<input type="text" value="<%=request.getAttribute("pageName")%>" maxlength="75"
							disabled="disabled" name="pageName" class="required text"
							id="addQLPageName">
					</c:when>
					<c:otherwise>
						<input type="text" value="<%=request.getAttribute("pageName")%>" maxlength="75"
							name="pageName" class="required text" id="addQLPageName">
					</c:otherwise>
				</c:choose>
			</c:if>
		</div>
		<br />
		<div class="element" align="right">
			<c:if
				test="${disableAdd eq null ||  disableAdd ne null && disableAdd ne true }">
				<input type="button" id="popupSubmit"
					value="<spring:message code='label.add'/>" class="submitClass" />
			</c:if>
			&nbsp; <input type="button" id="popupCancel"
				value="<spring:message code='label.close'/>" class="submitClass" />
		</div>
		
	</form:form>


