<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="com.coach.cip.common.dto.PersonalMessageVO"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="javax.portlet.PortletURL"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ page import="com.liferay.portal.service.RoleServiceUtil"%>
<%@ page import="java.util.List"%>
<%@ page import="com.liferay.portal.model.Role"%>
<%@ page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@ page import="com.liferay.portal.model.User"%>
<%@ page import="com.liferay.portal.util.PortalUtil"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />
<head>
<style type="text/css">
tr.results-header th.only {
	display: none;
}
</style>
</head>
<c:set var="personalMessageVOList" value="${personalMessageVOList}"></c:set>
<%
	String redirect = PortalUtil.getCurrentURL(renderRequest);
	PortletURL personalMessageURL = renderResponse.createRenderURL();
	List<PersonalMessageVO> personalMessageVOList = (List<PersonalMessageVO>)pageContext.getAttribute("personalMessageVOList");
%>
<script language="javascript" type="text/javascript">
	function backRedirect() {
		window.location.href = "${redirect}";

	}
</script>
<%-- <c:if test="${not empty personalMessageVOList}">

	<c:forEach var="personalMessage" items="${personalMessageVOList}">
		<div id="aui_3_4_0_1_622">
			<P id="aui_3_4_0_1_621" style="color: #d76c03; font-weight: bold">
				<B><c:out value="${personalMessage.displayDate}"></c:out> </B>
			</P>
			<P id="aui_3_4_0_1_715"
				style="font-family: 'myriad-web-pro'; font-size: 12px;">
				<c:out value="${personalMessage.modifiedMessage}"></c:out>

			</P>

			<P id="aui_3_4_0_1_715" style="color: #7fb2ca; font-weight: bold">
				<a style="text-decoration: none; color: black"
					href="<portlet:actionURL>
							<portlet:param name="myaction" value="detailPersonalMessage" />
							<portlet:param name="messageId" value="${personalMessage.messageId}" />
							<portlet:param name="redirect" value="<%= redirect %>" />
						</portlet:actionURL>">
					<span class="learnMoreBtn"><spring:message code='hyperlink-learn-more'/> &gt;&gt;</span></a>
			</P>

		</div>
	</c:forEach>


</c:if>
<c:if test="${empty personalMessageVOList}">
	<P id="aui_3_4_0_1_715"
		style="font-family: 'myriad-web-pro'; font-size: 12px; font-weight: bold">
		<c:out value="No message available"></c:out>
	</P>
</c:if> 

<input type="button" id="back" onclick="javascript:backRedirect()"
	value="Back" class="submitClass" />
--%>

<c:choose>
	<c:when
		test="<%= (personalMessageVOList != null) && (personalMessageVOList.size() > 0) %>">
		<liferay-ui:search-container delta="5" hover="false"
			emptyResultsMessage="no personal message found"
			iteratorURL="<%=personalMessageURL %>">
			<liferay-ui:search-container-results>
				<%
					results = ListUtil.subList(personalMessageVOList, searchContainer.getStart(), searchContainer.getEnd());
									total = personalMessageVOList.size();
									pageContext.setAttribute("results", results);
									pageContext.setAttribute("total", total);
				%>
			</liferay-ui:search-container-results>
			<liferay-ui:search-container-row
				className="com.coach.cip.common.dto.PersonalMessageVO"
				modelVar="personalMessage" keyProperty="messageId">
				<liferay-ui:search-container-column-jsp
					path="/WEB-INF/jsp/personalMessageItems.jsp" />
			</liferay-ui:search-container-row>
			<liferay-ui:search-iterator searchContainer="<%= searchContainer %>"
				paginate="<%=true %>" />
		</liferay-ui:search-container>
	</c:when>
	<c:otherwise>
		<div style="margin-left: 10px; margin-right: 10px;">
			<b><spring:message code='validation.message' /> </b>
		</div>
		<br>
	</c:otherwise>
</c:choose>


