<%@page import="com.liferay.portlet.journalcontent.util.JournalContentUtil"%>
<%@page import="com.liferay.portlet.journal.service.persistence.JournalArticleUtil"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticle"%>
<%@page import="com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>


<liferay-theme:defineObjects />
<portlet:defineObjects />

<%
String articleId = "";
String articleTitle = "";
String brand = "";
String articleContent = null;
if(request.getAttribute("articleId") != null){
	articleId = (String)request.getAttribute("articleId");
	articleContent = JournalContentUtil.getContent(themeDisplay.getScopeGroupId(),articleId, null, null, themeDisplay.getLanguageId(), themeDisplay);
}
if(request.getAttribute("articleTitle") != null){
	articleTitle = (String)request.getAttribute("articleTitle");
}
if(request.getAttribute("brand") != null){
	brand = (String)request.getAttribute("brand");
}
%>


				<div>
					<header class="portlet-storeDetail-topper">
					<h1 class="portlet-title">
						<span class="portlet-title-text"><spring:message
								code='label.title.offices' /> - <%=brand%> - <%=articleTitle%></span>
								
					</h1>
					</header>
<div id="jacontent" style="padding:10px;">				
<%=articleContent %>
</div>
</div>