<%@ include file="init.jsp"%>

       <%
       		String fileType = (String)request.getAttribute("fileType");
            String fileURL = (String)request.getAttribute("fileURL"); 
       		String title = (String)request.getAttribute("title");
       		String classPK = (String)request.getAttribute("classPK");
       		String typeOfLink = (String)request.getAttribute("typeOfLink");
       		String mimeType = (String)request.getAttribute("mimeType");

       		
       		
			/* Removing the Domain Name or IP Address from the URL for DLFileEntry : Start. */ 
       		 if(Validator.isNotNull(fileType) && fileType.endsWith("DLFileEntry") && !fileURL.equals(Constants.NO_USER_PRIVILEGE)){
       			
       			fileURL = StringUtils.getChomp(fileURL,"/documents");
       			
       			if(!locale.getLanguage().equalsIgnoreCase("en") ){
         		      fileURL = URLDecoder.decode(fileURL,"UTF-8");
         		}	
       			if(fileURL.contains("%26")){
       				fileURL = fileURL.replaceAll("%26","&");
       			}
       			if(fileURL.contains(" ")){
       				fileURL = fileURL.replaceAll(" ","+");
       			}
       		} 
       		/* Removing the Domain Name or IP Address from the URL for DLFileEntry : End. */
       		
       		
       	%>

    	<c:choose>
			<c:when test="<%= (Validator.isNotNull(fileURL) && !fileURL.contains(\"no-user-privilege\") )%>">
				<c:choose>
					<c:when
						test="<%=fileType != null && !fileType.isEmpty() && fileType.endsWith(\"JournalArticle\") && fileURL.matches(\"[0-9]*\") %>">
						<liferay-ui:journal-article articleId="<%=fileURL%>"
							groupId="<%= themeDisplay.getLayout().getGroupId() %>" />
					</c:when>
					<c:otherwise>
						 <c:if test="<%= Validator.isNotNull(mimeType) && !mimeType.equalsIgnoreCase(\"application/pdf\") && !mimeType.equalsIgnoreCase(\"text/html\")%>">
						      <iframe style="display:none"  src="<%=fileURL%>" width="0%" height="0"></iframe> 
						    <h3><spring:message code='label.pdfPortlet.statement.microsoft'/></h3>
						 </c:if>
						 <c:if test="<%= Validator.isNotNull(mimeType) && mimeType.equalsIgnoreCase(\"application/pdf\") %>">
							  <div id="pdf-placeholder" style="width: 100%;height:100%;">
							     <img  src="<%=themeDisplay.getPathThemeCss()%>/../images/loadinganimation.gif"/>
							  </div> 
						      <object id="pdfobject" data="<%=fileURL%>" type="application/pdf" height="600" width="100%" style="display:none"></object>
						 </c:if>
						  <c:if test="<%= Validator.isNotNull(mimeType) && mimeType.equalsIgnoreCase(\"text/html\")%>">
						      <iframe src="<%=fileURL%>" height="600" width="100%"></iframe> 
						 </c:if>
					</c:otherwise>
				</c:choose>
			</c:when>
		
			<c:when	test="<%= (fileURL != null && fileURL.contains(Constants.NO_USER_PRIVILEGE)) %>">
				<h5><spring:message code='label.pdfPortlet.statement.information'/>
                <br/><br/> 
				<spring:message code='label.pdfPortlet.statement.network'/></h5>
			</c:when>
		
			<c:otherwise>
				
			</c:otherwise>
		
		</c:choose>
        
