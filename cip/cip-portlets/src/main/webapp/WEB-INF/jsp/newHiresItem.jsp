<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page import="com.liferay.portal.kernel.dao.search.ResultRow"%>
<%@ page import="com.coach.cip.common.dto.NewHiresVO"%>
<%@ page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@page import="java.sql.Timestamp"%>
<%@page import="java.text.SimpleDateFormat"%>
<portlet:defineObjects />
<liferay-theme:defineObjects />

<%
	ResultRow row = (ResultRow) request.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);	
	NewHiresVO newHires = (NewHiresVO) row.getObject();	
	Timestamp joiningDate = newHires.getDate();
	String date = null;
	SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy",locale);
	if(joiningDate != null) {
		 date = sdf.format(joiningDate);
	}
	
%>
<c:set var="newHires" value="<%=newHires %>" />
<fmt:bundle basename="content/Language">
	<table>
		<tr>
			<td style="padding: 1px; color: #EB3528;">
			<%if(date != null){ %>
			<c:out value="<%=date.toUpperCase() %>"></c:out>
			<%} %>
			</td>
		</tr>
		<tr>
			<td style="padding: 1px; font-weight: bold; color: black;"><c:if test="${not empty newHires.employeeId}">
					<a
						href="<portlet:actionURL>
									  <portlet:param name="myaction" value="getEmployeeProfile" />
									 <portlet:param name="empId" value="${newHires.employeeId}" />
									 <portlet:param name="viewPage" value="0" />
									 </portlet:actionURL>">
						<c:out value="${newHires.firstName}"></c:out> <c:out value=" "></c:out>
						<c:out value="${newHires.lastName}"></c:out> </a>
					<c:out value=", "></c:out>
				</c:if> <c:if test="${empty newHires.employeeId}">

					<c:out value="${newHires.firstName}" ></c:out>
					<c:out value=" "></c:out>
					<c:out value="${newHires.lastName}"></c:out>
					<c:out value=", "></c:out>
				</c:if> <c:out value="${newHires.designation}" /></td>
		</tr>

		<tr>
			<td style="padding: 1px;"><c:if test="${not empty newHires.document}">
					<a style="font-weight: bold; color: black;"
						href="<portlet:actionURL>
									  <portlet:param name="myaction" value="getAttachment" />
									 <portlet:param name="employeeId" value="${newHires.employeeId}" />
									 </portlet:actionURL>"><span
						class="learnMoreBtn"><spring:message
								code='hyperlink-learn-more' /> &gt;&gt;</span> </a>
				</c:if> <c:if
					test="${not empty newHires.documentPath && empty newHires.document}">
					<a style="font-weight: bold; color: black;"
						href="<portlet:actionURL>
									  <portlet:param name="myaction" value="getAttachment" />
									 <portlet:param name="documentPath" value="${newHires.documentPath}" />
									 </portlet:actionURL>"><span
						class="learnMoreBtn"><spring:message
								code='hyperlink-learn-more' /> &gt;&gt;</span> </a>
				</c:if></td>
		</tr>
	</table>
</fmt:bundle>
