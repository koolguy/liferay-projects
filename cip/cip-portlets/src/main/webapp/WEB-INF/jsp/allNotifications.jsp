<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@ page import="java.util.List"%>
<%@ page import="com.coach.cip.common.dto.NotificationVO"%>
<%@ page import="com.coach.cip.portlets.controller.QuickLinksController"%>
<%@ page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<%@ page import="javax.portlet.PortletURL"%>
<%@ page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ page import="com.liferay.portal.theme.ThemeDisplay"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />


<script type="text/javascript"
      src="<%= themeDisplay.getPathThemeCss()%>/../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
      src="<%= themeDisplay.getPathThemeCss()%>/../js/jquery-ui-1.8.19.custom.min.js"></script>

<script>
	$(document).ready(function() {

		$("#deleteNotification").click(
				function() {
					$("#notificationActionName").val("Delete Notifications");

					if ($("#notificationForm").find(
							"input:checkbox").is(':checked')) {

						if (confirm($('#notificationAlertDeleteConfirm')
								.val())) {

							$("#notificationForm").submit();
						}
					} else {
						alert($('#notificationAlertDelete').val())
					}
		});
		
		$("#createNotification").click(function() {
	           
			$("#notificationActionName").val("Add Notifications");
			$("#notificationForm").submit();
		});
	});
</script>


<portlet:actionURL var="notificationUrl">
	<portlet:param name="myaction" value="deletNotificationForm" />
</portlet:actionURL>

<jsp:include page="/WEB-INF/jsp/message.jsp" />

<form:form name="notificationForm" method="post"
	action="${notificationUrl}" id="notificationForm">
	<liferay-ui:search-container delta="10"
		emptyResultsMessage="no-messages-were-found"
		rowChecker="<%= new RowChecker(renderResponse) %>">

		<%
			int counter = 0;
					String str;
		%>

		<liferay-ui:search-container-results>
			<%
				List<NotificationVO> tempResults = (List<NotificationVO>) request.getAttribute("notificaitonVoList");
							if (tempResults != null) {
								results = ListUtil.subList(tempResults, searchContainer.getStart(), searchContainer.getEnd());
								total = tempResults.size();
								pageContext.setAttribute("results", results);
								pageContext.setAttribute("total", total);
							}
			%>
		</liferay-ui:search-container-results>

		<liferay-ui:search-container-row
			className="com.coach.cip.common.dto.NotificationVO"
			modelVar="notificationVO" keyProperty="notificationId">

			<%-- 	<liferay-ui:search-container-column-text name="Id">
			  <c:out value="<%= (counter = counter+1) %>"></c:out>
			</liferay-ui:search-container-column-text> --%>

			<liferay-ui:search-container-column-text name="label.notification.notificationsubject">
				<a
					href="<portlet:actionURL>
							<portlet:param name="myaction" value="editNotification" />
							<portlet:param name="notificationId" value="${notificationVO.notificationId}" />
						</portlet:actionURL>">
						
					<c:choose>
								<c:when test="${fn:length(notificationVO.notificationSubject) > 25}">
									<c:set var="msgSubjectSubString"
										value="${fn:substring(notificationVO.notificationSubject, 0, 25)}" />
									<c:out value="${msgSubjectSubString}..."></c:out>
								</c:when>
								<c:otherwise>
									<c:out value="${notificationVO.notificationSubject}"></c:out>
								</c:otherwise>
							</c:choose>	
					 </a>
			</liferay-ui:search-container-column-text>

			<liferay-ui:search-container-column-text name="label.notification.notificationtype"
				property="notificationType">

			</liferay-ui:search-container-column-text>



			<liferay-ui:search-container-column-text name="label.notification.displaydate"
				property="displayDate">

			</liferay-ui:search-container-column-text>

			<liferay-ui:search-container-column-text name="label.notification.expirationdate"
				property="expirationDate">

			</liferay-ui:search-container-column-text>


		</liferay-ui:search-container-row>

		<liferay-ui:search-iterator searchContainer="<%= searchContainer %>"
			paginate="<%= true %>" />

	</liferay-ui:search-container>
    <div>
    <% 
    List<NotificationVO> temResults = (List<NotificationVO>) request.getAttribute("notificaitonVoList");
    if(temResults.size() >0 ){ %>
	<input class="submitClass" style="margin-top: 13px; margin-bottom: 10px; width: 160px;"
				type="button" value="<spring:message code='label.notification.deletenotification'/>"
				name="notificationOperation"	id="deleteNotification" />
	<%	}  %>
    <input class="submitClass" style="margin-top: 13px; margin-bottom: 10px; width: 135px;"
				type="button" value="<spring:message code='label.notification.addnotification'/>" name="notificationOperation"
				id="createNotification"/ >
	<input type="hidden" value="" id="notificationActionName" name="notificationOperation" />
	</div>
	
	<input type="hidden"
		value="<spring:message code='label.notification.alert.deleteconfirm'/>"
		id="notificationAlertDeleteConfirm" />
	<input type="hidden"
		value="<spring:message code='label.notification.alert.delete'/>"
		id="notificationAlertDelete" />	
</form:form>

