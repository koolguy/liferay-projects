<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@ page import="java.util.List"%>
<%@ page import="com.coach.cip.common.dto.EmployeeDirectorySearchVO"%>
<%@ page import="com.coach.cip.portlets.controller.EmployeeDirectoryController"%>
<%@ page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>




<portlet:defineObjects />
<liferay-theme:defineObjects />

<portlet:actionURL var="employeeDirectoryResultUrl">
	<portlet:param name="myaction" value="employeeDirectoryResultPage"></portlet:param>
</portlet:actionURL>


<%
	PortletURL changePageURL = renderResponse.createRenderURL();
	changePageURL.setParameter("myaction", "employeeDirectoryResultPage");
%>

<c:set var="searchVoList" value='${searchVoList}'></c:set>

<fmt:bundle basename="content/Language">

	<form:form name="employeeDirectoryResultForm" method="post"
		id="employeeDirectoryResultFormId"
		commandName="employeeDirectorySearchVO"
		action="${employeeDirectoryResultUrl}">
	<c:set var="FaultMessage" value='${FaultMessage}' />
		<div id="form_container_empDir">
			<header class="portlet-topper" style="background-color: #EB3528;">
			<h1 class="portlet-title">
				<fmt:message key='label.employee.directory.search.result.header' />
			</h1>
			</header>
			<c:if test="${empty FaultMessage}">
				<table class="tableEmployee">

					<tr>
						<td>
							<table style="width: 96%; border: 1px solid #CCC; margin: 16px;">
								<tr>
									<td><liferay-ui:search-container delta="20"
											iteratorURL="<%=changePageURL %>">

											<%
												int counter = 0;
											%>

											<liferay-ui:search-container-results>
												<%
													List<EmployeeDirectorySearchVO> tempResults = (List<EmployeeDirectorySearchVO>) request.getAttribute("searchVoList");
																		if (tempResults != null) {
																			results = ListUtil.subList(tempResults, searchContainer.getStart(), searchContainer.getEnd());
																			total = tempResults.size();
																			pageContext.setAttribute("results", results);
																			pageContext.setAttribute("total", total);
																		}
												%>
											</liferay-ui:search-container-results>

											<liferay-ui:search-container-row
												className="com.coach.cip.common.dto.EmployeeDirectorySearchVO"
												modelVar="empDirectorySearchVO" keyProperty="empId">


												<liferay-ui:search-container-column-text
													name="label.employee.directory.name">
													<c:if test="${not empty empDirectorySearchVO.empId}">
													<a style="text-decoration: underline;"
														href="<portlet:actionURL>
															<portlet:param name="myaction" value="getEmployeeDetail" />
															<portlet:param name="empId" value="${empDirectorySearchVO.empId}" />
															<portlet:param name="viewPage" value="1" />	</portlet:actionURL>">
															<c:out value="${empDirectorySearchVO.empName}"></c:out> 
													</a></c:if>
													
													<c:if test="${empty empDirectorySearchVO.empId}">
														<c:out value="${empDirectorySearchVO.empName}"></c:out> 
													</c:if>
													
												</liferay-ui:search-container-column-text>												
												
												<liferay-ui:search-container-column-text
													name="label.employee.directory.title"
													property="empTitle" />
												<liferay-ui:search-container-column-text
													name="label.employee.directory.dept"
													property="empDepartment" />

												<liferay-ui:search-container-column-text
													name="label.employee.directory.office" property="office" />												

												<liferay-ui:search-container-column-text
													name="label.employee.directory.phone" property="empPhone" />
												<liferay-ui:search-container-column-text
													href='<%= "mailto:" + empDirectorySearchVO.getEmailAddress()%>'
													name="label.employee.directory.email"
													property="emailAddress" />

											</liferay-ui:search-container-row>



											<liferay-ui:search-iterator
												searchContainer="<%= searchContainer %>"
												paginate="<%= true %>" />

										</liferay-ui:search-container>
									</td>
								</tr>
							</table>
						</td>

					</tr>
					<tr>
						<td>
						<%
							StringBuffer employeeDirectoryURL = new StringBuffer();
							employeeDirectoryURL.append("/").append(themeDisplay.getLanguageId()).append("/group").append(themeDisplay.getScopeGroup().getFriendlyURL()).append("/employeedirectory");
							%>
						<a href="<%= employeeDirectoryURL.toString()%>">
							<span style="background-color: black; color: white; padding: 7px 20px 7px 20px; -webkit-border-radius: 4px; margin-left: 15px; border: none; font-family: Arial; font-size: 13px;width:176px;cursor:default;">
							<fmt:message key='label.employee.directory.result.button' />
							</span></a>
						</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
					</tr>

				</table>
			</c:if>
			<c:if test="${not empty FaultMessage}">
				<table height="200">
					<tr >					
						<td class="employee-inp-form-td" style="font-size: 18px;color: red;">
							<spring:message code='${FaultMessage}' />	
						</td>
					</tr>
					<tr>
						<td class="employee-inp-form-td" >
							<input type="button" class="submitClass" value="BACK" onClick="history.go(-1);"/>
						</td>
					</tr>
				</table>
			</c:if>
		</div>
	</form:form>
</fmt:bundle>

