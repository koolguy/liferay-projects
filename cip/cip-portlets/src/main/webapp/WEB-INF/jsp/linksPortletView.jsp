<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="liferay-portlet" uri="http://liferay.com/tld/portlet"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@ page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@ page import="com.liferay.portal.kernel.util.Validator"%>
<%@ page import="com.liferay.portlet.PortletPreferencesFactoryUtil"%>
<%@ page import="javax.portlet.PortletPreferences"%>
<%@ page import="javax.portlet.RenderRequest"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<head>
<style>
.test {
	color: #ff9454 !important;
}
</style>
<link rel="stylesheet" type="text/css"
	href="<%=themeDisplay.getPathThemeCss()%>/sidenav.css" />
<title>linkportlet</title>
</head>
<%
	PortletPreferences preferences = renderRequest.getPreferences();
	String portletResource = ParamUtil.getString(request, "portletResource");
	if (Validator.isNotNull(portletResource)) {
		preferences = PortletPreferencesFactoryUtil.getPortletSetup(request, portletResource);
	}
	String[] linkNamesArr = preferences.getValues("linkNamesArr", new String[] {
		""
	});

	String[] linkUrlsArr = preferences.getValues("linkUrlsArr", new String[] {
		""
	});
	String currentContext = PortalUtil.getCurrentURL(renderRequest);
%>
<div>
	<ul style="list-style: none;">
		<%
			for (int index = 0; index < linkNamesArr.length; index++) {
				if (Validator.isNotNull(linkNamesArr[index])) {
					if (currentContext.contains(linkUrlsArr[index])) {
		%>
		<li><a class="test" href="<%=linkUrlsArr[index]%>"><img
				alt=""
				src="<%=themeDisplay.getPathThemeCss()%>/../images/arrow_grey_right.png">
				<span style="position: relative; top: -6px;"> <%=linkNamesArr[index]%></span>
		</a></li>
		<%
			}
					else {
		%>
		<li><a class="" href="<%=linkUrlsArr[index]%>"><img alt=""
				src="<%=themeDisplay.getPathThemeCss()%>/../images/arrow_grey_right.png">
				<span style="position: relative; top: -6px;"> <%=linkNamesArr[index]%></span>
		</a></li>
		<%
			}
				}
			}
		%>
	</ul>
</div>