<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@ page import="java.util.List"%>
<%@ page import="com.coach.cip.common.dto.NewHiresVO"%>
<%@ page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>





<portlet:defineObjects />
<liferay-theme:defineObjects />

<portlet:actionURL var="newHiresUrl">
	<portlet:param name="myaction" value="getAllNewHiresList"></portlet:param>
</portlet:actionURL>

<fmt:bundle basename="content/Language">
	<form:form name="newHiresForm" method="post" commandName="newHiresList" autocomplete="off"
		id="newHiresFormId" action="${newHiresUrl}">
		<c:set var="NoNewHires" value='${NoNewHires}' />
		<div id="form_container1">
			<header class="portlet-topper" style="background-color: #EB3528;">
			<h1 class="portlet-title">
				<fmt:message key='label.new.hires.header' />
			</h1>
			</header>
		
			<table class="tableEmployee" id="newHiresTable">
				<c:if test="${not empty NoNewHires}">
					<tr align="center" height="150">
						<td style="color:red;"><spring:message code='${NoNewHires}'/></td>
					</tr>
				</c:if>
				
				<c:if test="${not empty newHiresList && empty NoNewHires}">


					<c:forEach var="newHires" items="${newHiresList}">

						<tr>
							<td class="fistLineFont-td">
							
								<c:if test="${not empty newHires.employeeId}">
									<a	href="<portlet:actionURL>
									<portlet:param name="myaction" value="getEmployeeProfile" />
									<portlet:param name="empId" value="${newHires.employeeId}" />
									<portlet:param name="viewPage" value="0" />
									</portlet:actionURL>">
									<c:out value="${newHires.firstName}"></c:out> <c:out value=" "></c:out>
									<c:out value="${newHires.lastName}"></c:out> </a>
								</c:if>
								<c:if test="${empty newHires.employeeId}">
									<c:out value="${newHires.firstName}"></c:out> 
									<c:out value=" "></c:out>
									<c:out value="${newHires.lastName}"></c:out>								
								</c:if>
								 <c:out value=", "></c:out>
								<c:out value="${newHires.designation}"></c:out>
							</td>
						</tr>
						<tr>
							<td class="secondLineFont-td" style="color: #EB3528;">					
							
							<fmt:formatDate value="${newHires.date}" pattern="dd MMM yyyy"/> 
							</td>
						</tr>					
						

					</c:forEach>
					<tr>
						<td style="padding: 0px 10px 10px 0px;" align="right"><input
							type="submit"
							value="<fmt:message   key='label.newHires.button' />"
							id="allNewHiresId"
							style="background-color: black; color: white; padding: 1px; margin-top: 8px; height: 30px; width: 134px; border: none; " />
						</td>
					</tr>
				</c:if>
			</table>
		</div>
	</form:form>
</fmt:bundle>
