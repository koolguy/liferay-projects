<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="org.apache.velocity.runtime.directive.Foreach"%>
<%@ page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/ui" prefix="liferay-ui"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ page import="com.coach.cip.common.dto.StatesVO"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<liferay-theme:defineObjects />
<portlet:defineObjects />

<html>

<head>
<script type="text/javascript"
	src="<%= themeDisplay.getPathThemeCss()%>/../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%= themeDisplay.getPathThemeCss()%>/../js/jquery-ui-1.8.19.custom.min.js"></script>
</head>

<body>
	<c:set var="cityList" value='${cityList}'></c:set>
	<select name="cityName" id="target3" class="styledselect_form_1">
	<c:if test="${cityList ne null}">
	<c:choose>
	<c:when test="${cityList.size() eq 0 || cityList.isEmpty()}">
	<c:set var="message"><spring:message code='label.stores.city.notfound'/></c:set>
	<option value="0"><spring:message code='label.stores.choose.city'/></option>
	</c:when>
	<c:otherwise>
	 <option value="0"><spring:message code='label.stores.choose.city'/></option> 
	<c:forEach  var="city" items="${cityList}">
		<option value="${city}">${city}</option>
	</c:forEach>
	</c:otherwise>
	</c:choose>
	</c:if>
	</select>
	<%-- <div id="city_errors" class="error_message">${message}</div> --%>
</body>

</html>