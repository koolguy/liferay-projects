<%@page import="com.coach.cip.common.dto.NotificationVO"%>
<%@page import="java.io.IOException"%>
<%@page import="com.coach.notifications.service.beans.GetRecordsResponse"%>
<%@page import="com.coach.notifications.service.beans.GetRecords"%>
<%@page import="com.coach.notifications.service.beans.GetRecordsResponse.GetRecordsResult"%>
<%@page import="com.coach.cip.util.LoggerUtil"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticle"%>
<%@page import="com.coach.cip.util.WebUtil"%>
<%@page import="com.coach.cip.common.dto.HRCaseServiceVO"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@ page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>

<liferay-theme:defineObjects />
<portlet:defineObjects />

<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery-ui-1.8.19.custom.min.js"></script>
	
<c:set var="recordsList" value="${recordsList}"></c:set>
<c:set var="hrCaseNotificationsList" value="${hrCaseNotificationsList}"></c:set>
<c:set var="hrCaseServiceVOList" value="${hrCaseServiceVOList}"></c:set>
<%
	List<GetRecordsResult> recordsList = 
	Validator.isNotNull(pageContext.getAttribute("recordsList")) ? (List<GetRecordsResult>)pageContext.getAttribute("recordsList") : new ArrayList<GetRecordsResult>();
	List<com.coach.notifications.hrcase.service.beans.GetRecordsResponse.GetRecordsResult> hrCaseNotificationsList = 
		Validator.isNotNull(pageContext.getAttribute("hrCaseNotificationsList")) ? (List<com.coach.notifications.hrcase.service.beans.GetRecordsResponse.GetRecordsResult>)pageContext.getAttribute("hrCaseNotificationsList") : new ArrayList<com.coach.notifications.hrcase.service.beans.GetRecordsResponse.GetRecordsResult>();
	List<com.coach.notifications.hrtask.service.beans.GetRecordsResponse.GetRecordsResult> hrTaskNotificationsList = null;
	List<NotificationVO> tempNotificationList = (List<NotificationVO>) request.getAttribute("notificaitonVoList");
	List<HRCaseServiceVO> hrCaseServiceVOList = Validator.isNotNull(pageContext.getAttribute("hrCaseServiceVOList")) ? (List<HRCaseServiceVO>)pageContext.getAttribute("hrCaseServiceVOList") : new ArrayList<HRCaseServiceVO>();
	
	int allNotificationsListSize = 0;
	if(Validator.isNotNull(recordsList) || Validator.isNotNull(hrCaseServiceVOList)
			/* || Validator.isNotNull(hrCaseNotificationsList)|| (tempNotificationList != null && tempNotificationList.size() > 0) */ ) {
		allNotificationsListSize = recordsList.size() + hrCaseServiceVOList.size()/*  + tempNotificationList.size() + hrCaseNotificationsList.size()  */;
	}
%>

<script type="text/javascript">
$(document).ready(function(){
	$("#top-panel").hide();
	var notiSize = document.getElementById('notiSize').value;
	// Lets make the top panel toggle based on the click of the show/hide link	
	$("#sub-panel").mouseover(function(e){
		// Toggle the bar up 
		 e.stopPropagation();
		 if(notiSize != '0') {
			  $("#top-panel").slideToggle();
			}
			});// end on DOM
	});			
		
	
	//close div on mouse click outside div
	document.onclick=check;
	function check(e){
		var target = (e && e.target) || (event && event.srcElement);
		var obj = document.getElementById('top-panel');
		checkParent(target)?obj.style.display='none':null;
	}
	
	function checkParent(t){
		while(t.parentNode){
		if(t==document.getElementById('top-panel')||t==document.getElementById('click')){
		return false
		}
		t=t.parentNode
		}
		return true
	}
	
	//close div when mouseout
	function domouseout(e) {
		if (!e) var e = window.event;
		var tg = (window.event) ? e.srcElement : e.target;
		if (tg.nodeName != 'DIV') return;
		var reltg = (e.relatedTarget) ? e.relatedTarget : e.toElement;
		while (reltg != tg && reltg.nodeName != 'BODY')
			reltg= reltg.parentNode
		if (reltg== tg) {
	      return;
	}
	    $("#top-panel").hide();
	// Mouseout took place when mouse actually left layer
	// Handle event
	}

</script>

<div id="parentNotificationDiv" style="">

<span id="sub-panel">
	<a href="#" id="toggle"><%=allNotificationsListSize%></a> 
</span>

<input type="hidden" id="notiSize" value="<%=allNotificationsListSize%>" />
<div id="top-panel" onmouseout="domouseout(event);return true;" style="display:none;">
	<table class="notification">
		<%
		if(recordsList != null && recordsList.size() > 0){
			for(GetRecordsResult record : recordsList){
		%>
		<tr>
			<td><img class="coachTitle" src="<%=themeDisplay.getPathThemeImages()+"/sidenav-arrow.png"%>"/>
				<a class="highlightNotification" style="text-decoration: none;color: black;"
				href="<portlet:actionURL>
				<portlet:param name="myaction" value="callTasksURL"/>
				<portlet:param name="title" value="<%=record.getSubject()%>"/>
				<portlet:param name="sysid" value="<%=record.getSysId()%>"/>
				</portlet:actionURL>"><c:out value="<%=record.getSubject()%>"></c:out> </a>
			</td>
		</tr>
		<%
			}}
		%>
		
		<% if(Validator.isNotNull(hrCaseServiceVOList)){ 
				for(HRCaseServiceVO hrCaseServiceVO : hrCaseServiceVOList){
		%>
			<tr>
			<td><img class="coachTitle" src="<%=themeDisplay.getPathThemeImages()+"/sidenav-arrow.png"%>"/>
				<a class="highlightNotification" style="text-decoration: none;color: black;"
				href="<portlet:actionURL>
				<portlet:param name="myaction" value="callTasksURL"/>
				<portlet:param name="caseTitle" value="<%=hrCaseServiceVO.getCaseDescription()%>"/>
				<portlet:param name="caseSysid" value="<%=hrCaseServiceVO.getCaseSysId()%>"/>
				</portlet:actionURL>"><c:out value="<%=hrCaseServiceVO.getCaseDescription()%>"></c:out> </a>
			</td>
		</tr>
		<%} }%>
		<%-- <%
			if(Validator.isNotNull(hrCaseNotificationsList)){
				for(com.coach.notifications.hrcase.service.beans.GetRecordsResponse.GetRecordsResult hrCaseRecord : hrCaseNotificationsList){
					if(Validator.isNotNull(hrCaseRecord) && Validator.isNotNull(hrCaseRecord.getShortDescription())){
					String description = hrCaseRecord.getShortDescription();
					description = description.replaceAll(" ","-");
					hrCaseRecord.setShortDescription(description.trim());
					}
		%>
		<tr>
			<td><img class="coachTitle" src="<%=themeDisplay.getPathThemeImages()+"/sidenav-arrow.png"%>"/>
				<a class="highlightNotification" style="text-decoration: none;color: black;"
				href="<portlet:actionURL>
				<portlet:param name="myaction" value="callTasksURL"/>
				<portlet:param name="caseRecord" value="<%=hrCaseRecord.getShortDescription()%>"/>
				<portlet:param name="caseTitle" value="<%=hrCaseRecord.getShortDescription()%>"/>
				<portlet:param name="caseSysid" value="<%=hrCaseRecord.getSysId()%>"/>
				</portlet:actionURL>"><c:out value="<%=hrCaseRecord.getShortDescription()%>"></c:out> </a>
			</td>
		</tr>
		
		<%
			}}
		%> --%>
	</table>
	
	<%-- <table class="notification">
		<%
		if(tempNotificationList != null && tempNotificationList.size() > 0){
			for(NotificationVO notification : tempNotificationList){
		%>
		<tr>
			<td><img class="coachTitle" src="<%=themeDisplay.getPathThemeImages()+"/sidenav-arrow.png"%>"/>
				<a class="highlightNotification" style="text-decoration: none;color: black;"
				href="<portlet:actionURL>
				<portlet:param name="myaction" value="callTasksURL"/>
				<portlet:param name="localNotificationUrl" value="<%=notification.getNotificationUrl()%>"/>
				<portlet:param name="localNotificationTitle" value="<%=notification.getNotificationSubject()%>"/>
				</portlet:actionURL>"><c:out value="<%=notification.getNotificationSubject()%>"></c:out> </a>
			</td>
		</tr>
		<%
			}}
		%>
	</table> --%>
	</div>
</div>





