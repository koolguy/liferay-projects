<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="java.util.HashMap"%>
<%@page import="com.coach.cip.portlets.controller.NewsDetailController"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.coach.cip.util.LoggerUtil"%>
<%@page import="com.coach.cip.util.WebUtil"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="java.util.Map"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="java.util.List"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticle"%>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<portlet:defineObjects />
<liferay-theme:defineObjects />

<head>
<style type="text/css">
tr.results-header th.only {
	display: none;
}
</style>
</head>


<%
	final Log loggerJsp = LogFactoryUtil.getLog(NewsDetailController.class);
	PortletURL newsURL = renderResponse.createRenderURL();

	String friendlyUrlName = WebUtil.getThemeDisplay(renderRequest).getLayout().getFriendlyURL();
	String[] urlTokens = friendlyUrlName.split("/");
	String pageName = urlTokens[urlTokens.length - 1];
	String vocabName = pageName.replace("-", " ").toUpperCase();

	Map<String, Object> portletSessionMap = (Map<String, Object>) portletSession.getAttribute("portletSessionMap");
	portletSessionMap = Validator.isNotNull(portletSessionMap) ? portletSessionMap : new HashMap<String, Object>();

	List<JournalArticle> newsList = null;
	if (Validator.isNotNull(vocabName)) {
		if (Validator.isNotNull(portletSessionMap)) {
			if (Validator.isNotNull(portletSessionMap.get(vocabName))) {
				newsList = (List<JournalArticle>) portletSessionMap.get(vocabName);
			}
		}
	}
	else
		LoggerUtil.debugLogger(loggerJsp, "Vocab name could not be retrieved");
%>


<c:choose>
	<c:when test="<%= (newsList != null) && (newsList.size() > 0) %>">
		<liferay-ui:search-container delta="5" 
			hover="false" emptyResultsMessage="no news found"
			iteratorURL="<%=newsURL %>">
			<liferay-ui:search-container-results>
				<%
					results = ListUtil.subList(newsList, searchContainer.getStart(), searchContainer.getEnd());
									total = newsList.size();
									pageContext.setAttribute("results", results);
									pageContext.setAttribute("total", total);
				%>
			</liferay-ui:search-container-results>
			<liferay-ui:search-container-row
				className="com.liferay.portlet.journal.model.JournalArticle"
				modelVar="journalArticle" keyProperty="id">
				<liferay-ui:search-container-column-jsp
					path="/WEB-INF/jsp/newsItems.jsp" />
			</liferay-ui:search-container-row>
			<liferay-ui:search-iterator searchContainer="<%= searchContainer %>"
				paginate="<%=true %>" />
		</liferay-ui:search-container>
	</c:when>
	<c:otherwise>
		<div style="margin-left: 10px; margin-right: 10px;">
			<b><spring:message code='validation.message' />
			</b>
		</div>
		<br>
	</c:otherwise>


</c:choose>
