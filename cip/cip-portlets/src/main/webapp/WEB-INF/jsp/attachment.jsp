<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://liferay.com/tld/theme" prefix="liferay-theme"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@ page import="java.util.List"%>
<%@ page import="com.coach.cip.common.dto.NewHiresVO"%>
<%@ page import="com.liferay.portal.kernel.dao.search.RowChecker"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.kernel.util.WebKeys"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>

<link rel="stylesheet"
	href="<%=request.getContextPath()%>/css/screen.css" type="text/css"
	media="screen" title="default" />
<link href="<%=request.getContextPath()%>/style/style.css"
	rel="stylesheet" type="text/css" />

<portlet:defineObjects />
<liferay-theme:defineObjects />

<portlet:actionURL var="attachmentUrl">
	<portlet:param name="myaction" value="attachmentPage"></portlet:param>
</portlet:actionURL>

<c:set var="attachmentHeader" value="${attachmentHeader}"></c:set>
<c:set var="document" value="${document}"></c:set>

<fmt:bundle basename="content/Language">

	<form:form name="attachmentForm" method="post"
		commandName="documentPath" id="attachmentFormId"
		action="${attachmentUrl}">
		<div>

			<header class="portlet-topper" style="background-color: #EB3528;">
			<h1 class="portlet-title">
				<c:out value="${attachmentHeader}"></c:out>
			</h1>
			</header>

			<table class="tableEmployee" >
				<c:if test="${not empty documentPath}">
					<tr>
						<td><iframe src="${documentPath}" width="675px"
								height="500px"></iframe>
						</td>
					</tr>
				</c:if>

				<c:if test="${not empty document}">
					<tr>
						<td><div
								style='height: 500px; width: 675px; overflow-x: scroll; overflow-y: scroll'>
								<table style="width: 99%;text-align: justify;font-size: 12px;font-weight: normal;font-family: verdana;">
									<tr>

										<td style="padding:6px;"><c:out value="${document}" escapeXml="false" /></td>
									</tr>
								</table>
							</div></td>
					</tr>
				</c:if>

				<tr>
					<td style="padding: 10px 0px 10px 10px;"><input type="button"
						class="submitClass" value="BACK" onClick="history.go(-1);" />
					</td>
				</tr>



			</table>
		</div>
	</form:form>
</fmt:bundle>
