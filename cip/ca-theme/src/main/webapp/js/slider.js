		/*
		 *      loopedSlider 0.5.1 - jQuery plugin
		 *      written by Nathan Searles       
		 *      http://nathansearles.com/loopedslider/
		 *
		 *      Copyright (c) 2009 Nathan Searles (http://nathansearles.com/)
		 *      Dual licensed under the MIT (MIT-LICENSE.txt)
		 *      and GPL (GPL-LICENSE.txt) licenses.
		 *
		 *      Built for jQuery library
		 *      http://jquery.com
		 *
		 */

		/*
		 *      markup example for $("#loopedSlider").loopedSlider();
		 *
		 *      <div id="loopedSlider"> 
		 *              <div class="container">
		 *                      <div class="slides">
		 *                              <div><img src="01.jpg" alt="" /></div>
		 *                              <div><img src="02.jpg" alt="" /></div>
		 *                              <div><img src="03.jpg" alt="" /></div>
		 *                              <div><img src="04.jpg" alt="" /></div>
		 *                      </div>
		 *              </div>
		 *              <a href="#" class="previous">previous</a>
		 *              <a href="#" class="next">next</a>
		 *              <ul class="pagination">
		 *                      <li><a href="#">1</a></li>
		 *                      <li><a href="#">2</a></li>
		 *                      <li><a href="#">3</a></li>
		 *                      <li><a href="#">4</a></li>
		 *              </ul>   
		 *      </div>
		 *
		 */

		(function($) {
			$.fn.loopedSlider = function(options) {

				$(".link").click(function() {
					//$("#loopSlider").undbind("click");
					window.location.replace($(this).attr('href'));
					return false;
				})

				$("h3.trig").click(
						function() {

							if ($(this).next(".tar").css('display') == 'none') {
								$(this).next(".tar").show()
								$("div.container").height(
										$("div.container").height()
												+ $(this).next(".tar").height()
												+ 50)
							} else {
								$("div.container").height(
										$("div.container").height()
												- $(this).next(".tar").height()
												- 50)
								$(this).next(".tar").hide();
							}
							return false;

						})

				//              $("#controlPanel").css({ 'height': '100%'})

				//              /* remove hard coded spacer shim from first li */

				$("#spacer").remove();

				var w = Math.round(($("#mainBody").width())
						- ($("#mainBody").width() / 4) - 24)
				//alert( w )

				/* required styles */

				$(".container").css({
					width : w + 'px'
				}); // ,  'height':'375px', 'overflow':'hidden', 'position':'relative', 'cursor':'pointer' });

				//
				$(".slides").css({
					width : w + 'px'
				});// position:'absolute',  top:'0', left:'0' } );
				$(".slides li").css({
					width : w + 'px'
				}); //,  position:'absolute', 'border':'0px solid red', top:'0', display:'none', padding:'0', margin:'0' } );

				/* add slider navigation */

				var defaults = {
					container : '.container',
					slides : '.slides',
					pagination : '.pagination',
					containerClick : false, // Click container for next slide
					autoStart : 0, // Set to positive number for auto interval and interval time
					slidespeed : 800, // Speed of slide animation
					fadespeed : 1000, // Speed of fade animation
					autoHeight : false
				// Set to positive number for auto height and animation speed
				};

				this.each(function() {

					var obj = $(this);
					var o = $.extend(defaults, options);
					var pagination = $(o.pagination + ' li a', obj);
					var m = 0;
					var t = 1;
					var s = $(o.slides, obj).children().size();
					var w = $(o.slides, obj).children().outerWidth();
					var p = 0;
					var u = false;
					var n = 0;

					$(o.slides, obj).css({
						width : (s * w)
					});
						
					$(o.slides, obj).children().each(function() {
						$(this).css({
							position : 'absolute',
							left : p,
							display : 'block'/*,'color':'red'*/
						});// tb mod - added color red
						//$(this).prepend('<img src="shim.gif" border="1" width="100" height="250" class="shim" alt="shim pix" />');// tb mod - add spacer gif to top li

						p = p + w;
					});

					$(pagination, obj).each(function() {
						n = n + 1;
						$(this).attr('rel', n);
						$(pagination.eq(0), obj).parent().addClass('active');

					});
					//alert(s)
					if(s == 1){
					$(o.slides, obj).children(':eq(' + (s-1) + ')').css({
						position : 'absolute',
						left : 0
					});
					}else{
					$(o.slides, obj).children(':eq(' + (s - 1) + ')').css({
						position : 'absolute',
						left : -w
					});
}
					if (o.autoHeight) {
						$(this).css('padding-right', '6px'); // tb - mod currently animated item
						autoHeight(t);
					}

					$('.next', obj).click(function() {
                 if(s>2){
						if (u === false) {
							animate('next', true);
							if (o.autoStart) {
								clearInterval(sliderIntervalID);
							}
						}
						}
						if(s == 2){
							if (u === false) {
							animate('prev', true);
							if (o.autoStart) {
								clearInterval(sliderIntervalID);
							}
						}
						
						}
						return false;
					});

					$('.previous', obj).click(function() {
					if(s!=1){
						if (u === false) {
							animate('prev', true);
							if (o.autoStart) {
								clearInterval(sliderIntervalID);
							}
						}
						}
						return false;
					});

					if (o.containerClick) {
						$(o.container, obj).click(function() {
							if (u === false) {
								animate('next', true);
								if (o.autoStart) {
									clearInterval(sliderIntervalID);
								}
							}
							return false;
						});
					}

					$(pagination, obj).click(
							function() {
								if ($(this).parent().hasClass('active')) {
									return false;
								} else {
									t = $(this).attr('rel');
									$(pagination, obj).parent().siblings()
											.removeClass('active');
									$(this).parent().addClass('active');
									animate('fade', t);
									if (o.autoStart) {
										clearInterval(sliderIntervalID);
									}
								}
								return false;
							});

					if (o.autoStart) {
						sliderIntervalID = setInterval(function() {
							if (u === false) {
								animate('next', true);
							}
						}, o.autoStart);
					}

					function current(t) {
						if (t === s + 1) {
							t = 1;
						}
						if (t === 0) {
							t = s;
						}
						$(pagination, obj).parent().siblings().removeClass(
								'active');

						$(pagination + '[rel="' + (t) + '"]', obj).parent()
								.addClass('active');
					}
					;

					function autoHeight(t) {
						if (t === s + 1) {
							t = 1;
						}
						if (t === 0) {
							t = s;
						}
						var getHeight = $(o.slides, obj).children(
								':eq(' + (t - 1) + ')', obj).outerHeight();
						$(o.container, obj).animate({
							height : getHeight
						}, o.autoHeight);
					}
					;

					function animate(dir, clicked) {
						u = true;
						switch (dir) {
						case 'next':
						//alert("next")
							t = t + 1;
							m = (-(t * w - w));
							current(t);
							if (o.autoHeight) {
								autoHeight(t);
							}
							$(o.slides, obj).animate(
									{
										left : m
									},
									o.slidespeed,
									function() {
										if (t === s + 1) {
											t = 1;
											$(o.slides, obj).css({
												left : 0
											}, function() {
												$(o.slides, obj).animate({
													left : m
												})
											});
											$(o.slides, obj).children(':eq(0)')
													.css({
														left : 0
													});
											$(o.slides, obj).children(
													':eq(' + (s - 1) + ')')
													.css({
														position : 'absolute',
														left : -w
													});
										}
										if (t === s)
											$(o.slides, obj).children(':eq(0)')
													.css({
														left : (s * w)
													});
										if (t === s - 1)
											$(o.slides, obj).children(
													':eq(' + (s - 1) + ')')
													.css({
														left : s * w - w
													});
										u = false;
									});
							break;
						case 'prev':
						//alert("prev")
							t = t - 1;
							m = (-(t * w - w));
							current(t);
							if (o.autoHeight) {
								autoHeight(t);
							}
							$(o.slides, obj).animate(
									{
										left : m
									},
									o.slidespeed,
									function() {
										if (t === 0) {
											t = s;
											$(o.slides, obj).children(
													':eq(' + (s - 1) + ')')
													.css({
														position : 'absolute',
														left : (s * w - w)
													});
											$(o.slides, obj).css({
												left : -(s * w - w)
											});
											$(o.slides, obj).children(':eq(0)')
													.css({
														left : (s * w)
													});
										}
										if (t === 2)
											$(o.slides, obj).children(':eq(0)')
													.css({
														position : 'absolute',
														left : 0
													});
										if (t === 1)
											$(o.slides, obj).children(
													':eq(' + (s - 1) + ')')
													.css({
														position : 'absolute',
														left : -w
													});
										u = false;
									});
							break;
						case 'fade':
						//alert("fade")
							t = [ t ] * 1;
							m = (-(t * w - w));
							current(t);
							if (o.autoHeight) {
								autoHeight(t);
							}
							$(o.slides, obj).children().fadeOut(
									o.fadespeed,
									function() {
										$(o.slides, obj).css({
											left : m
										});

										$(o.slides, obj).children(
												':eq(' + (s - 1) + ')').css({
											left : s * w - w /*,'color':'green'*/
										});/* last slide div */
										/** tb - first slide FADE IN */
										$(o.slides, obj).children(':eq(0)')
												.css({
													left : 0
												});
										if (t === s) {
											$(o.slides, obj).children(':eq(0)')
													.css({
														left : (s * w)
													});
										}
										if (t === 1) {
											$(o.slides, obj).children(
													':eq(' + (s - 1) + ')')
													.css({
														position : 'absolute',
														left : -w
													});
										}

										/* tb mod: control currently displayed slide's properties ( width parameter ) */
										$(o.slides, obj).children().fadeIn(
												o.fadespeed);//.css( {'color':'red',  'border':'1px solid blue', 'margin-right':'25%', 'display':'block', 'right':'20%', 'background-color':'#ddd' });
										u = false;
									});
							break;
						default:
						//alert("default")
							break;
						}
					}
					;
				});
			};
		})(jQuery);
	