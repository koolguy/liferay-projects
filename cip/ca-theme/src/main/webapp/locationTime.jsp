<%@page trimDirectiveWhitespaces="true"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>


<%!//returns list of local time of tomezones specified in timeZoneIdArr array
	public List<String> getLocalTimeArr(Date date, String[] timeZoneIdArr) {

		List<String> list = new ArrayList<String>();
		StringBuilder resultStr = null;
		for (String timeZoneId : timeZoneIdArr) {
			resultStr = new StringBuilder("");
			String timeZoneAttr[] = timeZoneId.split("-");
			String cityName = timeZoneAttr[0].trim();
			String timeZoneName = timeZoneAttr[1].trim();
			TimeZone tz = TimeZone.getTimeZone(timeZoneName);
			DateFormat gmtOffset = new SimpleDateFormat("Z");
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy hh:mm a");
			dateFormat.setTimeZone(tz);
			gmtOffset.setTimeZone(tz);
			resultStr = resultStr.append("<zone>");
			resultStr = resultStr.append("<city>" + cityName + "</city>");
			resultStr = resultStr.append("<time>" + dateFormat.format(date) + "</time>");
			resultStr = resultStr.append("<offset>GMT" + gmtOffset.format(date)+"</offset>");
			resultStr = resultStr.append("</zone>");
			list.add(resultStr.toString());

		}

		return list;
	}%>
<%
	response.setContentType("text/xml;charset=UTF-8");
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("pragma", "no-cache");
%>

<%
	String[] timeZoneIdArr =
		{
			"Beijing-Asia/Shanghai", "Chicago-America/Chicago", "Denver-America/Denver", "Dongguan-Asia/Shanghai",
			"Ho Chi Minh City-Asia/Saigon", "Hong Kong-Asia/Hong_Kong", "Honolulu-Pacific/Honolulu", "Jacksonville-America/New_York",
			"Kuala Lumpur-Asia/Kuala_Lumpur", "London-Europe/London", "Los Angeles-America/Los_Angeles", "Manila-Asia/Manila",
			"New York-America/New_York", "Paris-Europe/Paris", "Seoul-Asia/Seoul", "Shanghai-Asia/Shanghai", "Singapore-Asia/Singapore",
			"Taipei-Asia/Taipei", "Tokyo-Asia/Tokyo"
		};
	List<String> localTimeList = null;
	Date date = new Date();
	localTimeList = getLocalTimeArr(date, timeZoneIdArr);
	StringBuilder responseXML = new StringBuilder("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?><Root>");
	for (String currStr : localTimeList) {
		responseXML = responseXML.append(currStr.trim());
	}
	responseXML = responseXML.append("</Root>");
%>
<%
	PrintWriter printWriter = response.getWriter();
	printWriter.print(responseXML.toString().trim());
	printWriter.flush();
%>