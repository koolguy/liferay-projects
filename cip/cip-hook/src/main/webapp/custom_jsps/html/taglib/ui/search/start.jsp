<%--
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>

<%@page import="com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@page import="com.liferay.portlet.asset.service.AssetVocabularyServiceUtil"%>
<%@page import="com.liferay.portlet.asset.model.AssetVocabulary"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<liferay-theme:defineObjects />
	<%/* CIP Code Change : Start 
		 * 
		 * Description: Adding prepopulated keyword for type assist,
		 * Supporting code for "Search customization",
		 * 
		* */ %>
<script type="text/javascript">
	function resetDateFacet(){
		if(null != document.getElementById("modified")){
			document.getElementById("modified").value = null;
		}
		if(null != document.getElementById("modifiedselection")){
			document.getElementById("modifiedselection").value = "0";
		}	
		if(null != document.getElementById("modifiedfrom")){
			document.getElementById("modifiedfrom").value = null;
		}
		if(null != document.getElementById("modifiedto")){
			document.getElementById("modifiedto").value = null;
		}
  		
  		
	}
	
	function checkForEnterKey(e){
		var keynum;
		if(window.event) // IE8 and earlier
		{
			keynum = e.keyCode;
		}
		else if(e.which) // IE9/Firefox/Chrome/Opera/Safari
		{
			keynum = e.which;
		}
		if(keynum == 13){
			document.getElementById('mainSearchButton').click();
		}
		
	}

</script>


<link rel="stylesheet" type="text/css"
	href="<%=themeDisplay.getPathThemeCss()%>/jquery-ui.css" />


<style>
#ui-active-menuitem{
background: #DAD9D7;
}
</style>

<script>

$(document).ready(function() {
	
	$('.default-value').each(function() {
		var default_value = this.value;
		$(this).focus(function() {
			if(this.value == default_value) {
				this.value = '';
			}
		});
		$(this).blur(function() {
			if(this.value == '') {
				this.value = default_value;
			}
		});
	});	

	$(function() {
		var availableTags = [
			"ActionScript",
			"AppleScript",
			"Asp",
			"BASIC",
			"Coach",
			"Collection",
			"Checklist",
			"COBOL",
			"ColdFusion",
			"Evaluation",
			"Facebook",
			"February",
			"International",
			"Internship",
			"Jacksonville",
			"JavaScript",
			"Legacy",
			"North America",
			"Orientation",
			"Processes",
			"Program",
			"Registration",
			"Scala",
			"Scheme",
			"University",
			"wwd",
			"Western Europe"
		];
		$( "#mainSearchId" ).autocomplete({
			source: '/html/taglib/ui/search/autocomplete.jsp?fl=content&limit=10&prefix=true',
			open: function(){ $("#content").find("#pdfid").css({"visibility":"hidden"});},
			close: function(){ $("#content").find("#pdfid").css({"visibility":"visible"});}
		});
	});
	/*source: '/SolrAutoComplete/autocomplete?fl=content&limit=10&prefix=true',*/
	
	$("#mainSearchButton").click(function() {
		var selectedText = $.trim($(".search-links ").find("span").text());
		$(".search-links").find("ul").find("li").each(function() {
			if ($.trim($(this).text())== selectedText){
				$("#searchIn").val($(this).text());
				$("#searchInId").val($(this).find("a").attr("name"));
			}
		});
	});  
	
	if(!$("#searchIn").val() == "" )
	{
		if($("#searchIn").val().indexOf("_")>0)
		{
			 $(".search-links ").find("dt").find("a").find("span").html($("#searchIn").val().split("_")[0]);
		}else{
				$(".search-links ").find("dt").find("a").find("span").html($("#searchIn").val());
		}
	} 
	
	var refineSearchCategory = $.trim($("#refineSearchId").find('li[class*="current-term"]').text());
	if(refineSearchCategory != null && refineSearchCategory == $.trim($("#allCategoryId").text())){
		$(".search-links ").find("dt").find("a").find("span").html(refineSearchCategory);
	}
	
});



</script>

<%@ include file="/html/taglib/ui/search/init.jsp"%>

<%
	long groupId = ParamUtil.getLong(request, namespace + "groupId");

	Group group = themeDisplay.getScopeGroup();

	String keywords = ParamUtil.getString(request, namespace
			+ "keywords");
	
	
	PortletURL portletURL = null;

	if (portletResponse != null) {
		LiferayPortletResponse liferayPortletResponse = (LiferayPortletResponse) portletResponse;

		portletURL = liferayPortletResponse.createLiferayPortletURL(
				PortletKeys.SEARCH, PortletRequest.RENDER_PHASE);
	} else {
		portletURL = new PortletURLImpl(request, PortletKeys.SEARCH,
				plid, PortletRequest.RENDER_PHASE);
	}
	portletURL.setWindowState(WindowState.MAXIMIZED);
	portletURL.setPortletMode(PortletMode.VIEW);
	portletURL.setParameter("struts_action", "/search/search");
	/* Commented to Solve Bad request as the redirect parameter was adding to current URL on click of Search/search result */
	//portletURL.setParameter("redirect", currentURL);
	portletURL.setParameter("sortByDate", "false");
	pageContext.setAttribute("portletURL", portletURL);
	
	/* List<AssetVocabulary> assetVocabularyList = AssetVocabularyServiceUtil.getGroupVocabularies(themeDisplay.getScopeGroupId());
	pageContext.setAttribute("assetVocabularyList", assetVocabularyList); */
	
	
	long[] groupIds = {themeDisplay.getScopeGroupId()};
	List<AssetVocabulary> tempAssetVocabularyList = AssetVocabularyLocalServiceUtil.getGroupsVocabularies(groupIds);
	List<AssetVocabulary> assetVocabularyList = new ArrayList<AssetVocabulary>();
	String OrganizationName = PropsUtil.get(com.coach.cip.util.Constants.COACH_INTRANET_PORTAL_ORGANIZATION_NAME);
	if(Validator.isNotNull(OrganizationName) &&
			  themeDisplay.getScopeGroupName().equalsIgnoreCase(OrganizationName))
	{
		pageContext.setAttribute("assetVocabularyList", tempAssetVocabularyList);
		
	}else
	{
		for(AssetVocabulary assetVocabulary : tempAssetVocabularyList){
			if(locale.getLanguage().equalsIgnoreCase("en") && !assetVocabulary.getName().contains("_"))
			{
				assetVocabularyList.add(assetVocabulary);
			}else if (assetVocabulary.getName().contains("_") && assetVocabulary.getName().split("_")[1].equalsIgnoreCase(locale.getCountry())){
				assetVocabularyList.add(assetVocabulary);
			}
		}
		pageContext.setAttribute("assetVocabularyList", assetVocabularyList);
	}
	
	
	
%>
   <!-- CIP Code Change : Start 
      
      Code to display SearchIn select box.
   -->

   <div style="float: left;height: 23px;margin-left: 4px;">
        				<dl id="sample" class="search-links" style="display:none;">
				        <dt><a style="text-decoration:none;font-size:12px;font-weight:bold;border:1px solid black;text-align:left;" href="javascript:void(0)"><span><liferay-ui:message key="com.coach.search.search-in" />:</span></a></dt>
				        <dd>
				            <ul>
				             <li><a href="#" id="allCategoryId"><liferay-ui:message key="com.coach.search.all-categories" /></a></li>
					            <c:if test="${not empty assetVocabularyList}">
									<c:forEach var="assetVocabulary" items="${assetVocabularyList}">
									    <%
									       AssetVocabulary assetVocabulary  =(AssetVocabulary) pageContext.getAttribute("assetVocabulary");
									       if(assetVocabulary.getName().toUpperCase().contains("DEPT"))
 									       { %>
									    	   <li><a href="#" name="<%=assetVocabulary.getVocabularyId()%>"><%=assetVocabulary.getTitle(locale).toUpperCase().split("DEPT")[0].trim()%></a>
												</li>
									    <%
 									       }else{									    
									    %>
										         <li><a href="#" name="<%=assetVocabulary.getVocabularyId()%>"><%=assetVocabulary.getTitle(locale).toUpperCase()%></a>
												</li>
										<% } %>		
									</c:forEach>
								</c:if>
								 <li><a href="#"><liferay-ui:message key="com.coach.search.employee-directory"/></a></li>
				             <!-- <li><a href="#" name="0"><liferay-ui:message key="com.coach.search.employee-directory"/></a></li> -->
								<!-- <li><a href="#">LOCATIONS</a>
												</li> -->
				            </ul>
				        </dd>
						</dl>
					</div> 

 <!-- CIP Code Change : End --> 	 

<form id="mainSearchForm" action="<%=portletURL.toString()%>" method="get"
	name="<%=randomNamespace%><%=namespace%>fm" 
	onSubmit="<%=randomNamespace%><%=namespace%>search(); return false;">
	<liferay-portlet:renderURLParams varImpl="portletURL" />
	
<!-- 	<input id="_3_assetCategoryNames" name="_3_assetCategoryNames" type="hidden" value=""> -->
	<% String searchIn = request.getParameter("_3_searchIn") ;
	  String searchInId = request.getParameter("_3_searchInId"); 
	  if(searchIn== null){
		   searchIn = ""; 
	   }
	   if(Validator.isNotNull(request.getParameter("selectedVocabName"))){
		   searchIn = request.getParameter("selectedVocabName");
	   }
	%>
	<% 
	   String modified = request.getParameter("_3_modified") ;
	   String modifiedselection = request.getParameter("_3_modifiedselection") ;
	   String modifiedfrom = request.getParameter("_3_modifiedfrom") ;
	   String modifiedto = request.getParameter("_3_modifiedto") ;	  
	%>
	
	<% if(null != modified){ %>
		<input type="hidden" value="<%= modified %>" id="modified"  name="<%=namespace%>modified"/>
	<% } 
	   if(null != modifiedselection){ %>
		<input type="hidden" value="<%= modifiedselection %>" id="modifiedselection"  name="<%=namespace%>modifiedselection"/>
	<% } 
	   if(null != modifiedfrom){ %>
		<input type="hidden" value="<%= modifiedfrom %>" id="modifiedfrom"  name="<%=namespace%>modifiedfrom"/>
	<% } 
	   if(null != modifiedto){ %>
			<input type="hidden" value="<%= modifiedto %>" id="modifiedto"  name="<%=namespace%>modifiedto"/>
	<% } %>
	
	
	
	<input type="hidden" value="<%= searchInId %>" id="searchInId"  name="<%=namespace%>searchInId"/>
	<input type="hidden" value="<%= searchIn %>" id="searchIn"  name="<%=namespace%>searchIn"/>

<!-- <div id="demo"></div> -->

<%
String defaultKeyword = LanguageUtil.get(themeDisplay.getLocale(), "enter.name.or.topic.to.search");
if(keywords == null || keywords==""){
	keywords = defaultKeyword;
}

%>
    <div style="margin-top:0px;display:inline;">
	<span>
	<input class="default-value" style="margin-top: 3px;height:17px;border:1px solid #000000;margin-left:2px;border-radius: 0px;" name="<%=namespace%>keywords" id="mainSearchId" size="30" type="text"
		value="<%=HtmlUtil.escapeAttribute(keywords)%>" onkeydown="return checkForEnterKey(event)" onclick="if(this.value == '<%=defaultKeyword %>') this.value='';"/>
	</span>
	<%/* CIP Code Change : End */ %>
	<%/* CIP Code Change : Start 
		 * 
		 * Description: Changes for hiding dropdown, search and clear icons
		 * 
		* */ %>
	<%-- 
	<select name="<%= namespace %>groupId">
	<option value="0" <%= (groupId == 0) ? "selected" : "" %>><liferay-ui:message key="everything" /></option>
	<option value="<%= group.getGroupId() %>" <%= (groupId != 0) ? "selected" : "" %>><liferay-ui:message key='<%= "this-" + (group.isOrganization() ? "organization" : "site") %>' /></option>
</select> --%>

	<aui:input name="groupId" type="hidden" value="<%=themeDisplay.getScopeGroupId() %>" />
	
	
	<%-- 
	<input align="absmiddle" border="0"
		src="<%=themeDisplay.getPathThemeImages()%>/common/search.png"
		title="<liferay-ui:message key="search" />" type="image" /> --%>
		
	<span><input style="height:19px;padding-bottom:0px;line-height: 19px;vertical-align: middle;background:#000;color:#fff;width:120px;border-radius:0px;font-weight:bold;border: 0px;padding-top: 0px;"
		
		title="<liferay-ui:message key="search" />" id="mainSearchButton" type="submit" value="<liferay-ui:message key="com.coach.search.search" />" onclick="resetDateFacet()" />
</span>
	<%/* CIP Code Change : End */ %>
	</div>
	<aui:script>
	function <%=randomNamespace%><%=namespace%>search() {
		var keywords = document.<%=randomNamespace%><%=namespace%>fm.<%=namespace%>keywords.value;

		keywords = keywords.replace(/^\s+|\s+$/, '');

		if (keywords != '') {
		  if(keywords == "<%=defaultKeyword %>"){
		  	alert("<liferay-ui:message key="com.coach.search.pls.enter.search.keyword" />");
		  	$("#mainSearchId").focus();
		  }else{
		  	
				document.<%=randomNamespace%><%=namespace%>fm.submit();
			}
		}
		else
        {
           alert("<liferay-ui:message key="com.coach.search.pls.enter.search.keyword" />");
           $("#mainSearchId").focus();
        }
	}
	</aui:script>
	
