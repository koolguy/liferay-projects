<%@page import="com.liferay.portal.kernel.util.PropsUtil"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@ page import="java.io.*" %>
<%@ page import="java.net.*" %>
<%@ page import="org.apache.solr.client.solrj.*" %>
<%@ page import="org.apache.solr.client.solrj.impl.*" %>
<%@ page import="org.apache.solr.client.solrj.response.*" %>
<%@ page import="org.apache.solr.common.util.*" %>
<%@ page import="org.apache.commons.lang.*" %>
<%@ page import="java.net.*" %>

<%
	
	
	String solrUrl = PropsUtil.get("solr.url");
	String queryParamName = PropsUtil.get("solr.query.paramname");
	
	String fl = request.getParameter("fl");
	String limit = request.getParameter("limit");
	String infix = request.getParameter("infix");
	String prefix = request.getParameter("prefix");
	String q = request.getParameter(queryParamName);
	if(Validator.isNotNull(q)){
		q = q.toLowerCase();
		String[] multiTerms = q.split(" ");
		if(multiTerms.length > 1){
			q = multiTerms[multiTerms.length - 1];
		}
	}
	CommonsHttpSolrServer solrServer = null;

	try {
		solrServer = new CommonsHttpSolrServer(solrUrl);
		SolrQuery query = new SolrQuery();
		if (null != fl)
			query.setParam("terms.fl", fl);
		else
			query.setParam("terms.fl", "content");
		if (null != limit)
			query.setParam("terms.limit", limit);
		else
			query.setParam("terms.limit", "20");
		if (null != infix)
			query.add("terms.infix", q);
		else if (null != prefix)
			query.setParam("terms.prefix", q);

		query.setParam("omitHeader", "true");
		query.setParam("wt", "json");
		query.setQueryType("/terms");

		QueryResponse qr = solrServer.query(query);
		NamedList<Object> solrResp = qr.getResponse();
		@SuppressWarnings("unchecked")
		NamedList<Object> termsList = (NamedList<Object>) solrResp.get("terms");
		NamedList<Object> contentList = null;
		//JSONObject json = new JSONObject();
		//JSONArray jsonList = new JSONArray();
		response.setContentType("application/json");
		StringBuffer buf = new StringBuffer(300);
		if (null != termsList & termsList.size() > 0) {
			contentList = (NamedList<Object>) termsList.get("content");
			int size = contentList.size();
			if (null != contentList && size > 0) {
				buf.append("[");
				for (int i = 0; i < size; i++) {
					//jsonList.add(contentList.getName(i));
					buf.append("\"").append(contentList.getName(i)).append("\"");
					if (i < size - 1) {
						buf.append(",");
					}
				}
				buf.append("]");
				out.println(buf.toString());
			}
			//json.put("query", "test");
			//json.put("suggestions", jsonList);
		}
	} catch (MalformedURLException e) {
		e.getMessage();
	} catch (SolrServerException e) {
		e.getMessage();
	} catch (IOException e) {
		e.getMessage();
	}
%>