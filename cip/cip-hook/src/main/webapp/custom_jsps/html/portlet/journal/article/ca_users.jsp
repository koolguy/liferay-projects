<%@page import="java.util.Collections"%>
<%@page import="com.liferay.portal.model.UserGroup"%>
<%@page import="java.util.HashSet"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Set"%>
<%@page import="com.liferay.portal.theme.ThemeDisplay"%>
<%@page import="com.liferay.portal.kernel.util.StringPool"%>
<%@page import="com.liferay.portlet.dynamicdatamapping.storage.FieldConstants"%>
<%@page import="com.liferay.portlet.dynamicdatamapping.storage.StorageEngineUtil"%>
<%@page import="com.liferay.portlet.dynamicdatamapping.storage.Fields"%>
<%@page import="com.liferay.portlet.dynamicdatalists.model.DDLRecordVersion"%>
<%@page import="com.liferay.portlet.dynamicdatalists.service.DDLRecordLocalServiceUtil"%>
<%@page import="com.liferay.portlet.dynamicdatalists.model.DDLRecord"%>
<%@page import="java.util.Map"%>
<%@page import="com.liferay.portlet.dynamicdatamapping.model.DDMStructure"%>
<%@page import="com.liferay.portlet.dynamicdatalists.service.DDLRecordSetLocalServiceUtil"%>
<%@page import="com.liferay.portlet.dynamicdatalists.model.DDLRecordSet"%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="com.liferay.portal.kernel.language.LanguageUtil"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.liferay.portal.kernel.util.PropsUtil"%>
<%@page import="com.liferay.portal.kernel.util.GetterUtil"%>
<%@page import="com.liferay.portal.service.UserGroupLocalServiceUtil"%>
<%@page import="com.liferay.portal.service.GroupLocalServiceUtil"%>
<%@page import="java.util.Arrays"%>
<%@page import="java.lang.reflect.Array"%>
<%@page import="com.liferay.portal.kernel.util.ArrayUtil"%>
<%@page import="com.coach.ca.model.Transmittal"%>
<%@page import="com.coach.ca.service.TransmittalLocalServiceUtil"%>
<%@ page import="com.liferay.portal.util.WebKeys"%>
<%@ page import="com.liferay.portlet.journal.model.JournalArticle"%>

<%@ page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@ page import="com.liferay.portal.kernel.exception.SystemException"%>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="liferay-ui" uri="http://liferay.com/tld/ui"%>
<%@ taglib prefix="liferay-theme" uri="http://liferay.com/tld/theme"%>
<%@ taglib prefix="aui" uri="http://liferay.com/tld/aui"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ page import="com.liferay.portal.kernel.bean.BeanParamUtil"%>
<%@page import="com.liferay.portal.service.UserLocalServiceUtil"%>
<%@page import="com.liferay.portal.kernel.util.ListUtil"%>
<%@page import="com.liferay.portal.model.User"%>
<%@page import="java.util.List"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portal.service.OrganizationLocalServiceUtil"%>
<%@page import="com.liferay.portal.service.UserGroupLocalServiceUtil"%>


<portlet:defineObjects />
<liferay-theme:defineObjects />


<link rel="stylesheet"
	href="<%=themeDisplay.getPathThemeCss()%>/screen.css" type="text/css"
	media="screen" title="default" />

<link rel="stylesheet" type="text/css"
	href="<%=themeDisplay.getPathThemeCss()%>/jquery.multiselect.css" />
<link rel="stylesheet" type="text/css"
	href="<%=themeDisplay.getPathThemeCss()%>/jquery.multiselect.filter.css" />

<link rel="stylesheet" type="text/css"
	href="<%=themeDisplay.getPathThemeCss()%>/jquery-ui.css" />



<style>
tr.hasselect td input {
	margin-left: -4px
}
</style>
<!--  jquery core -->
<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery-1.7.2.min.js"></script>
<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery-ui-1.8.19.custom.min.js"></script>


<!--  checkbox styling script -->
<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.multiselect.js"></script>
<script type="text/javascript"
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.multiselect.filter.js"></script>
<script src="<%=themeDisplay.getPathThemeCss()%>/../js/ui.core.js"
	type="text/javascript"></script>
<script src="<%=themeDisplay.getPathThemeCss()%>/../js/ui.checkbox.js"
	type="text/javascript"></script>
<script src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.bind.js"
	type="text/javascript"></script>

<script type="text/javascript">
	$(document).ready(function() {

		$(".multiSelect").multiselect().multiselectfilter();
	});
</script>

<script type="text/javascript">
	$(function() {
		$('input').checkBox();
		$('#toggle-all').click(function() {
			$('#toggle-all').toggleClass('toggle-checked');
			$('#mainform input[type=checkbox]').checkBox('toggle');
			return false;
		});
	});
</script>

<!--  styled select box script version 2 -->
<script
	src="<%=themeDisplay.getPathThemeCss()%>/../js/jquery.selectbox-0.5_style_2.js"
	type="text/javascript"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('.styledselect_form_1').selectbox({
			inputClass : "styledselect_form_1"
		});
		$('.styledselect_form_2').selectbox({
			inputClass : "styledselect_form_2"
		});
	});
</script>


<%




long orgId = OrganizationLocalServiceUtil.getOrganizationId(themeDisplay.getCompanyId(), themeDisplay.getScopeGroupName());
Map<String, Set<User>> usersListMap =  getUsers(themeDisplay, request, response);
Set<User> corpUserList = usersListMap.get("corp_users");
Set<User> storeUserList = usersListMap.get("store_users");
	
JournalArticle article = (JournalArticle) request.getAttribute(WebKeys.JOURNAL_ARTICLE);

	String articleId = BeanParamUtil.getString(article, request,"articleId");

	
	String[] storeUsers = new String[0];
	String[] corporateUsers = new String[0];
	Transmittal transmittal = null;
	try {
		transmittal = TransmittalLocalServiceUtil.getTransmittal(articleId);

		storeUsers = transmittal.getStoreUsers().split(",");
		corporateUsers = transmittal.getCorporateUsers().split(",");
	} catch (Exception e) {
		System.out.println("Error fetching Transmittal CA Users ID :" + transmittal + "\n" + e);
	}
%>


<table>

	<tr id="rolesRow" class="hasselect">
		<br />
		<th valign="top">Store Users :</th>
		<td><select multiple="multiple" class="multiSelect" id="storeUsers"
			name="store_users" style="width: 330px; margin-left: -4px;">
				<%
					for (User storeUserObj : storeUserList) {
						
						if (ArrayUtil.contains(storeUsers, "" + storeUserObj.getUserId())) {

							if (storeUserObj.getFirstName() != null	&& !storeUserObj.getFirstName().isEmpty()) {
				%>

				<option selected="selected" value="<%=storeUserObj.getUserId()%>"><%=storeUserObj.getFirstName()%><%=" "%><%=storeUserObj.getLastName()%></option>
				<%
					} else {
				%>
				<option selected="selected" value="<%=storeUserObj.getUserId()%>"><%=storeUserObj.getScreenName()%></option>
				<%
					}
						} else {
							if (storeUserObj.getFirstName() != null
									&& !storeUserObj.getFirstName().isEmpty()) {
				%>
				<option value="<%=storeUserObj.getUserId()%>"><%=storeUserObj.getFirstName()%><%=" "%><%=storeUserObj.getLastName()%></option>
				<%
					} else {
				%>

				<option value="<%=storeUserObj.getUserId()%>"><%=storeUserObj.getScreenName()%></option>


				<%
					}
						}
					}
				%>

		</select>
		</td>

	</tr>

	<tr id="rolesRow" class="hasselect">
		<br />

		<th valign="top">Corporate Users :</th>
		<td><select multiple="multiple" class="multiSelect" id="corporateUsers"
			name="corporate_users" style="width: 330px; margin-left: -4px;">
				<%
					for (User corporateUsersObj : corpUserList) {
						if (ArrayUtil.contains(corporateUsers, "" + corporateUsersObj.getUserId())) {

							if (corporateUsersObj.getFirstName() != null && !corporateUsersObj.getFirstName().isEmpty()) {
				%>

				<option selected="selected" value="<%=corporateUsersObj.getUserId()%>"><%=corporateUsersObj.getFirstName()%><%=" "%><%=corporateUsersObj.getLastName()%></option>
				<%
					} else {
				%>
				<option selected="selected" value="<%=corporateUsersObj.getUserId()%>"><%=corporateUsersObj.getScreenName()%></option>
				<%
					}
						} else {
							if (corporateUsersObj.getFirstName() != null && !corporateUsersObj.getFirstName().isEmpty()) {
				%>
				<option value="<%=corporateUsersObj.getUserId()%>"><%=corporateUsersObj.getFirstName()%><%=" "%><%=corporateUsersObj.getLastName()%></option>
				<%
					} else {
				%>

				<option value="<%=corporateUsersObj.getUserId()%>"><%=corporateUsersObj.getScreenName()%></option>


				<%
					}
				}
			}
				%>

		</select>
		</td>

	</tr>

</table>



<%!

//Group Ids
private Map<String, Set<User>> getUsers(ThemeDisplay themeDisplay,  HttpServletRequest request,
		HttpServletResponse response){
	Map<String, Set<User>> usersMap = new HashMap<String, Set<User>>();
	Set<User> corpUsers = Collections.emptySet();
	Set<User> storeUsers = Collections.emptySet();
	try{
	List<String[]> orgGroupMap = buildGroupMap(request, response);
	String orgName = themeDisplay.getScopeGroupName();
	for(String[] orgGroupVals : orgGroupMap ){
		if(orgName!= null && orgGroupVals!= null && orgGroupVals.length == 3 && orgName.equalsIgnoreCase(orgGroupVals[0])){
			String[] store_group_names = orgGroupVals[1].split(",");
			String[] corp_group_names = orgGroupVals[2].split(",");
		
			corpUsers = getUsersFromGroups(themeDisplay, corp_group_names);
			storeUsers = getUsersFromGroups(themeDisplay, store_group_names);
			break;
		}
	}
	}catch(Exception e){
		System.out.println(e.getMessage());
	}
		usersMap.put("corp_users", corpUsers);
		usersMap.put("store_users", storeUsers);
	return usersMap;
}

private Set<User> getUsersFromGroups(ThemeDisplay themeDisplay, String[] groupNames) throws PortalException,  SystemException{
	Set<User> users = new HashSet<User>();
	for(String groupName : groupNames ){
		try{
		UserGroup userGroup = 	UserGroupLocalServiceUtil.getUserGroup(themeDisplay.getCompanyId(), groupName);
		List<User> groupUsers = UserLocalServiceUtil.getUserGroupUsers(userGroup.getUserGroupId());
		users.addAll(groupUsers);
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
	}
		return users;
}

private List<String[]> buildGroupMap(HttpServletRequest request,
			HttpServletResponse response) throws PortalException,  SystemException {
		
		String ORGANIZATION_GROUP_DDL_NAME = GetterUtil.getString(PropsUtil.get("org.group.ddl.ids"));
		List<String[]> lstTopics = new ArrayList<String[]>();
		String languageId = LanguageUtil.getLanguageId(request);
		try {
			long companyGroupId = PortalUtil.getCompany(request).getGroup().getGroupId();
			List<DDLRecordSet> ddlRecordSet = DDLRecordSetLocalServiceUtil.getRecordSets(companyGroupId);
			for (DDLRecordSet recordSet : ddlRecordSet) {
				if (recordSet.getName(languageId).equalsIgnoreCase(ORGANIZATION_GROUP_DDL_NAME)) {
					DDMStructure ddmStructure = recordSet.getDDMStructure();
					Map<String, Map<String, String>> fieldsMap = ddmStructure.getFieldsMap(languageId);
					List<DDLRecord> ddlRecords = DDLRecordLocalServiceUtil.getRecords(recordSet.getRecordSetId());
					for (DDLRecord ddlRecord : ddlRecords) {
						DDLRecordVersion recordVersion = ddlRecord.getRecordVersion();
						Fields fieldsModel = StorageEngineUtil.getFields(recordVersion.getDDMStorageId());
						// Columns
						String[] fieldMapValues = new String[fieldsMap.values().size()];
						int counter = 0;
						for (Map<String, String> fields : fieldsMap.values()) {
							String name = fields.get(FieldConstants.NAME);
							String value = null;
							if (fieldsModel.contains(name)) {
								com.liferay.portlet.dynamicdatamapping.storage.Field field = fieldsModel.get(name);
								value = (String) field.getValue();
							}
							else {
								value = StringPool.BLANK;
							}
							fieldMapValues[counter] = value;
							counter++;
						}
						lstTopics.add(fieldMapValues);
					}
				}
			}
		}
		catch (Exception e) {
			
			System.out.println(e.getMessage());
		}
		return lstTopics;
	}

		
	
//Group Ids
%>
