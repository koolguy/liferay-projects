<%--
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>
<%@page import="com.liferay.portal.util.PortalUtil"%>
<%@page import="com.liferay.portal.kernel.xml.SAXReaderUtil"%>
<%@page import="com.liferay.portlet.journal.service.JournalTemplateLocalServiceUtil"%>
<%@page import="com.liferay.portlet.journal.model.JournalTemplate"%>
<%@page import="com.coach.cip.util.CoachComparator"%>
<%@page import="com.liferay.portal.kernel.exception.PortalException"%>
<%@page import="com.liferay.portal.kernel.exception.SystemException"%>
<%@page import="com.liferay.portal.kernel.log.LogFactoryUtil"%>
<%@page import="com.liferay.portal.kernel.log.Log"%>
<%@page import="com.liferay.portlet.asset.NoSuchEntryException"%>
<%@page import="java.net.URLEncoder"%>
<%@page import="com.liferay.portal.kernel.repository.model.FileEntry"%>
<%@page import="java.util.HashMap"%>
<%@page import="com.liferay.portlet.asset.service.AssetVocabularyServiceUtil"%>
<%@page import="java.util.Collections"%>
<%@page import="javax.portlet.RenderRequest"%>
<%@page import="java.util.Map"%>
<%@page import="javax.portlet.PortletRequest"%>
<%@page import="com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="com.liferay.portlet.asset.model.AssetVocabulary"%>
<%@page import="javax.portlet.WindowState"%>
<%@page import="com.liferay.portlet.PortletURLFactoryUtil"%>
<%@page import="com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil"%>
<%@page import="com.liferay.portlet.journal.model.JournalArticle"%>
<%@page import="javax.portlet.PortletURL"%>
<%@page import="com.liferay.portlet.asset.model.AssetCategory"%>
<%@page import="com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil"%>
<%@page import="com.coach.cip.util.WebServiceUtil"%>
<%@page import="com.liferay.portal.service.LayoutLocalServiceUtil"%>
<%@page import="com.liferay.portal.model.Layout"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.util.StringTokenizer"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.text.DateFormat"%>
<%@ include file="/html/portlet/search/init.jsp"%>

<%

/* CIP Code Change : Start 
 * 
 * Description: Supporting code for "Displaying vocabs instead of categories Adding Employee directory and Locations functionality",
 * Search results display changes, highlight feature changes
 
* */ 
/* CIP Code Change : Start 
 * 
 * Fetching Layouts & Creating Map for required Vocabularies.
* */ 
final Log loggerJsp = LogFactoryUtil.getLog(this.getClass());
//Map<String,Layout> layoutMap = WebServiceUtil.getLayouts(themeDisplay.getScopeGroupId());
Map<String,Layout> layoutMap = (Map<String,Layout>)session.getAttribute("layoutMap");
Map<String,String> vocabularyPortletNameMap = new HashMap<String,String>();
vocabularyPortletNameMap.put("COACH NEWS","Webcontent_WAR_cipportlet");
vocabularyPortletNameMap.put("EXECUTIVE CORNER","executiveCorner_WAR_cipportlet");
vocabularyPortletNameMap.put("VISION AND VALUES","visionAndValues_WAR_cipportlet");
vocabularyPortletNameMap.put("LOCAL NEWS","localNews_WAR_cipportlet");

StringBuffer url = new StringBuffer();
JournalArticle journalArticle = null;
AssetCategory assetCategory = null;
AssetVocabulary vocabulary = null;
PortletURL assetURL = null;

/* CIP Code Change : End */

	ResultRow row = (ResultRow) request
			.getAttribute(WebKeys.SEARCH_CONTAINER_RESULT_ROW);

	Document document = (Document) row.getObject();

	String className = document.get(Field.ENTRY_CLASS_NAME);

	AssetRendererFactory assetRendererFactory = AssetRendererFactoryRegistryUtil
			.getAssetRendererFactoryByClassName(className);

	AssetRenderer assetRenderer = null;

	PortletURL viewFullContentURL = null;

	String assetCreateDateString = null;
	
	String title = null;
	String categoryFileURL = null;
	String typeOfLink = "";
	String mimeType = null;
	if (assetRendererFactory != null) {
		
		long classPK = GetterUtil.getLong(document
				.get(Field.ENTRY_CLASS_PK));

		long resourcePrimKey = GetterUtil.getLong(document
				.get(Field.ROOT_ENTRY_CLASS_PK));

		if (resourcePrimKey > 0) {
			classPK = resourcePrimKey;
		}
		AssetEntry assetEntry = null;
		try{
			assetEntry = AssetEntryLocalServiceUtil.getEntry(
				className, classPK);
		}
		catch(NoSuchEntryException e){
			loggerJsp.error("No Asset Entry exist with the className="+className+" and classPK="+classPK);
			return;
		}
		
		
		/* CIP Code Change : Start 
		 * 
		 * Creating URL to open the search results in their respective pages.
		* */ 
		 mimeType = assetEntry.getMimeType().trim();
		 if(document.get(Field.ENTRY_CLASS_NAME).endsWith("JournalArticle"))
			{
				journalArticle = JournalArticleLocalServiceUtil.getLatestArticle(Long.parseLong(document.get(Field.ENTRY_CLASS_PK)));
				title = cropTitle(journalArticle.getTitleCurrentValue());
				categoryFileURL = journalArticle.getArticleId();
				
				/* For Internal & External Link (Structures & Templates) . */
				if (journalArticle != null && journalArticle.getTemplateId() != null && !journalArticle.getTemplateId().isEmpty()) 
				{
					JournalTemplate journalTemplate =
						JournalTemplateLocalServiceUtil.getTemplate(journalArticle.getGroupId(), journalArticle.getTemplateId());
					if (journalTemplate != null && journalTemplate.getNameCurrentValue().equalsIgnoreCase("COACH_LINK_TEMPLATE")) 
					{

						com.liferay.portal.kernel.xml.Document xmlDocument = SAXReaderUtil.read(journalArticle.getContent());
						categoryFileURL = xmlDocument.selectSingleNode(com.coach.cip.util.Constants.DYNAMIC_ELEMENT_NAME + "url" + com.coach.cip.util.Constants.DYNAMIC_CONTENT).getText().toLowerCase();
						typeOfLink = xmlDocument.selectSingleNode(com.coach.cip.util.Constants.DYNAMIC_ELEMENT_NAME + "type" + com.coach.cip.util.Constants.DYNAMIC_CONTENT).getText();
                         
					}
				}
			
			}
		 if(document.get(Field.ENTRY_CLASS_NAME).endsWith("DLFileEntry"))
			{
				title = cropTitle(assetEntry.getTitleCurrentValue());
				categoryFileURL = WebServiceUtil.displayFileURL(assetEntry.getClassPK(), renderRequest, renderResponse, themeDisplay);
			
			
			}
			Map<String, Long> matchingDocCatMap = (Map<String, Long>)session.getAttribute("matchingDocCatMap");
			if(Validator.isNull(matchingDocCatMap) )
			{
				matchingDocCatMap = new HashMap<String, Long>();
			}
		long matchingDocCatId = matchingDocCatMap.containsKey(document.get(Field.ENTRY_CLASS_PK)) ? matchingDocCatMap.get(document.get(Field.ENTRY_CLASS_PK)) 
				        : Long.parseLong(document.get(Field.ASSET_CATEGORY_IDS));	
		 if(Validator.isNotNull(matchingDocCatId)){
			 assetCategory = AssetCategoryLocalServiceUtil.getCategory(matchingDocCatId);
			 vocabulary = AssetVocabularyLocalServiceUtil.getVocabulary(assetCategory.getVocabularyId());
		 }
		 
		 if(Validator.isNotNull(vocabulary) && vocabularyPortletNameMap.containsKey(vocabulary.getName())){
			if(document.get(Field.ENTRY_CLASS_NAME).endsWith("JournalArticle")){ 
				 categoryFileURL = String.valueOf(journalArticle.getId());
				 assetURL = PortletURLFactoryUtil.create(request,vocabularyPortletNameMap.get(vocabulary.getName()),
					 			layoutMap.get("HOME").getPlid(),PortletRequest.ACTION_PHASE);
				 assetURL.setParameter("myaction", "showArticle");
				 assetURL.setParameter("articleId", categoryFileURL);
				 assetURL.setWindowState(WindowState.NORMAL);
			} else if(document.get(Field.ENTRY_CLASS_NAME).endsWith("DLFileEntry")){
				assetURL = PortletURLFactoryUtil.create(request,vocabularyPortletNameMap.get(vocabulary.getName()),
		 			layoutMap.get("HOME").getPlid(),PortletRequest.ACTION_PHASE);
				assetURL.setParameter("myaction", "showOnlyDoc");
				FileEntry fileEntry = WebServiceUtil.getFileEntry(classPK, renderRequest, renderResponse);
				String fileTitle = URLEncoder.encode(fileEntry.getTitle(), "UTF-8");
				String docUrl = ("/documents/" + fileEntry.getGroupId() + "/" + fileEntry.getFolderId() + "/" + 
								fileTitle + "/" + fileEntry.getUuid());
				docUrl = URLEncoder.encode(docUrl,"UTF-8");
				//docUrl = docUrl.replace('+',' ');
				assetURL.setParameter("documentPath", docUrl);
				assetURL.setParameter("portletTitle",title);
				assetURL.setWindowState(WindowState.NORMAL);
			} 
		 }else{
			 
			String categoryKey = "";
			List<String> categoryNamesList = new ArrayList<String>();
			 if(Validator.isNotNull(matchingDocCatId) &&  WebServiceUtil.isCIPOrganization(themeDisplay)){
				categoryKey = getAssetEntryPage(assetCategory, layoutMap, categoryNamesList, renderRequest);
				categoryNamesList = (List<String>)renderRequest.getAttribute("categoryNamesList");
			 }else{
				 categoryKey = vocabulary.getName().toUpperCase();
				 if(categoryKey.contains("DEPT"))
				 {
					 categoryKey =  categoryKey.split("DEPT")[0].trim();
				 }
				 List<AssetCategory> categoryList = new ArrayList<AssetCategory>();
				 WebServiceUtil.fetchRecursiveParentCategories(assetCategory,categoryList);
				 for(AssetCategory assetCat : categoryList){
					 categoryNamesList.add(assetCat.getTitle(locale));
				 }
			 }
		
		if(layoutMap.containsKey(categoryKey) && !vocabulary.getName().equalsIgnoreCase(LanguageUtil.get(locale,"com.coach.search.locations"))){
			 
			 assetURL = PortletURLFactoryUtil.create(request,"SideNavigation_WAR_cipportlet",layoutMap.get(categoryKey).getPlid(),PortletRequest.ACTION_PHASE);
			 assetURL.setParameter("myaction", "processDocumentURL");
			 assetURL.setParameter("fileURL", categoryFileURL);
			 assetURL.setParameter("fileTitle", title);
			 assetURL.setParameter("fileType", document.get(Field.ENTRY_CLASS_NAME));
			 assetURL.setParameter("classPK", document.get(Field.ENTRY_CLASS_PK));
			 assetURL.setParameter("typeOfLink", typeOfLink);
			 assetURL.setParameter("mimeType", mimeType);
			 assetURL.setParameter("backButton", "true");
			 assetURL.setParameter("showPDFPortlet", "#true#");
			 assetURL.setWindowState(WindowState.NORMAL);
			 if(Validator.isNotNull(categoryNamesList)){
				 Collections.reverse(categoryNamesList);
				 for (int i = 1; i <categoryNamesList.size() ; i++) {
					 if(i==1){
						 assetURL.setParameter("level5Parent", categoryNamesList.get(i));
					 }
					 if(i==2){
						 assetURL.setParameter("level6Parent", categoryNamesList.get(i));
					 }
					 if(i==3){
						 assetURL.setParameter("level7Parent", categoryNamesList.get(i));
					 }
					 if(i==4){
						 assetURL.setParameter("level8Parent", categoryNamesList.get(i));
					 }
				 }
			 }
				
		}else if(layoutMap.containsKey(categoryKey) && vocabulary.getName().equalsIgnoreCase(LanguageUtil.get(locale,"com.coach.search.locations"))){
			/* assetURL = PortletURLFactoryUtil.create(request,"",layoutMap.get(categoryKey).getPlid(),PortletRequest.ACTION_PHASE);
			 assetURL.setWindowState(WindowState.NORMAL); */
			 
			 //constructing assetURL to navigate to offices.
			 assetURL = PortletURLFactoryUtil.create(request,"StoreOffices_WAR_cipportlet",layoutMap.get(categoryKey).getPlid(),PortletRequest.ACTION_PHASE);
			 assetURL.setParameter("myaction", "processOfficesWebContentURL");
			 assetURL.setParameter("fileURL", categoryFileURL);
			 assetURL.setParameter("fileTitle", title);
			 assetURL.setParameter("fileType", document.get(Field.ENTRY_CLASS_NAME));
			 assetURL.setParameter("classPK", document.get(Field.ENTRY_CLASS_PK));
			 assetURL.setParameter("typeOfLink", typeOfLink);
			 assetURL.setParameter("mimeType", mimeType);
			// assetURL.setParameter("redirectUrl", PortalUtil.getCurrentURL(renderRequest).toString());
			 assetURL.setParameter("backButton", "true");
			 assetURL.setWindowState(WindowState.NORMAL);
		}
	 }
		
		 /* CIP Code Change : End */	 
		 
		Date assetCreateDate = assetEntry.getModifiedDate();
		Format formatter = new SimpleDateFormat("MM/dd/yyyy");
		assetCreateDateString = formatter.format(assetCreateDate);
	

		assetRenderer = assetRendererFactory.getAssetRenderer(classPK);
 
		viewFullContentURL = _getViewFullContentURL(request,
				themeDisplay, PortletKeys.ASSET_PUBLISHER, document);

		viewFullContentURL.setParameter("struts_action",
				"/asset_publisher/view_content");
		viewFullContentURL.setParameter("assetEntryId",
				String.valueOf(assetEntry.getEntryId()));
		viewFullContentURL.setParameter("type",
				assetRendererFactory.getType());

		if (Validator.isNotNull(assetRenderer.getUrlTitle())) {
			if ((assetRenderer.getGroupId() > 0)
					&& (assetRenderer.getGroupId() != scopeGroupId)) {
				viewFullContentURL.setParameter("groupId",
						String.valueOf(assetRenderer.getGroupId()));
			}

			viewFullContentURL.setParameter("urlTitle",
					assetRenderer.getUrlTitle());
		}
	} else {
		String portletId = document.get(Field.PORTLET_ID);

		viewFullContentURL = _getViewFullContentURL(request,
				themeDisplay, portletId, document);
	}
	if(Validator.isNotNull(assetURL)){
		viewFullContentURL = assetURL;
	} 
	String viewURL = null;
	if (viewInContext) {
		String viewFullContentURLString = viewFullContentURL.toString();

		viewFullContentURLString = HttpUtil.setParameter(
				viewFullContentURLString, "redirect", currentURL);
		if(Validator.isNull(assetURL)){
		viewURL = assetRenderer.getURLViewInContext(
				liferayPortletRequest, liferayPortletResponse,
				viewFullContentURLString);}
		//	viewURL = assetURL.toString();
	} else {
		viewURL = viewFullContentURL.toString();
	  //	viewURL = assetURL.toString();
	}

	if (Validator.isNull(viewURL)) {
		viewURL = viewFullContentURL.toString();
	 //	viewURL = assetURL.toString();
	}

	viewURL = _checkViewURL(themeDisplay, viewURL, currentURL);
	String delimiter = "=";
	String displayUrl[] = viewURL.split(delimiter);

	String entryTitle = null;
	String entrySummary = null;

	String[] queryTerms = (String[]) request
			.getAttribute("search.jsp-queryTerms");
		
	if (assetRenderer != null) {
		entryTitle = assetRenderer.getTitle(locale);
		/*cip changes starts here */
		//entrySummary = StringUtil.shorten(assetRenderer.getSummary(locale), 200);
		//entrySummary = com.coach.cip.util.CoachStringUtil.getSummary(document.get(Field.CONTENT), queryTerms, locale);
		entrySummary = StringUtil.shorten(document.get(Field.CONTENT),200);
		/*cip changes ends here */
	} else {
		Indexer indexer = IndexerRegistryUtil.getIndexer(className);

		String snippet = document.get(Field.SNIPPET);

		Summary summary = indexer.getSummary(document, locale, snippet,
				viewFullContentURL);

		entryTitle = summary.getTitle();
		
		/*cip changes starts here */
		//entrySummary = StringUtil.shorten(summary.getContent(), 200);
		entrySummary = com.coach.cip.util.CoachStringUtil.getSummary(document.get(Field.CONTENT), queryTerms, locale);
		/*cip changes ends here */
	}

	PortletURL portletURL = (PortletURL) request
			.getAttribute("search.jsp-portletURL");
%>

<span class="asset-entry"> 
<%-- <span class="asset-entry-type">
		<%=ResourceActionsUtil.getModelResource(
					themeDisplay.getLocale(), className)%>
</span>  --%>

<span class="asset-entry-title"> <%-- <a href="<%=viewURL%>"> --%> <c:if
				test="<%= assetRendererFactory != null %>">
				<%-- <img alt="\"
					src="<%=assetRendererFactory.getIconPath(renderRequest)%>" /> --%>
			</c:if> 
			
			
			
			<%
			String fullSearchKeywordFrmSession = (String)session.getAttribute("searchKeywordName");
			String[] cutomQuerytermsArr = null;
			List<String> termList = new ArrayList<String>();
			if(Validator.isNotNull(fullSearchKeywordFrmSession)){
				String spacedStr = fullSearchKeywordFrmSession.replace(':',' ');
				spacedStr = spacedStr.replace('"',' ');
				spacedStr = spacedStr.replace('\'',' ');
				spacedStr = spacedStr.replace('*',' ');
				StringTokenizer st = new StringTokenizer(spacedStr," ");
				while(st.hasMoreTokens()){
					String token = st.nextToken().trim();
					termList.add(token);
				}
				
			}
			
			cutomQuerytermsArr = termList.toArray(new String[termList.size()]);
			
			
		%>
		<%-- <%=	StringUtil.highlight(HtmlUtil.escape(entryTitle),queryTerms)%> --%>
		   <a href="<%=viewURL%>" style="color:red;font-weight:bold;">
		   		<%=	StringUtil.highlight(HtmlUtil.escape(WebServiceUtil.convertCamelCase(entryTitle)),cutomQuerytermsArr)%> 
		   	</a>
			<%-- <%=	StringUtil.highlight(HtmlUtil.escape(entryTitle),cutomQuerytermsArr)%> --%>
			 <!-- </a>  -->
</span> 
<c:if
		test="<%= Validator.isNotNull(entrySummary) %>">
		<span class="asset-entry-summary"> 
		<%-- <%=StringUtil.highlight(HtmlUtil.escape(entrySummary),queryTerms)%> --%>
		<%String assetSummary = StringUtil.highlight(HtmlUtil.escape(entrySummary),cutomQuerytermsArr);
		if(Validator.isNotNull(assetSummary)){
			assetSummary =  assetSummary.replace("+"," ");
		}
		%>
		<%=assetSummary %>
		 </span>
</c:if>
 <%-- <a href="<%=viewURL%>">${layoutURL}  <%=displayUrl[0]%> </a> --%> 
 <%--  <a href="<%=viewURL%>" style="color:red;font-weight:bold;"><%=displayUrl[0]%> </a> --%>
 <p style="font-size: 12px;"><%=assetCreateDateString%></p>
<%--   <%
 	String[] assetTagNames = document.getValues(Field.ASSET_TAG_NAMES);
 %> 
 <c:if test="<%= Validator.isNotNull(assetTagNames[0]) %>">
		<div class="asset-entry-tags">

			<%
				for (int i = 0; i < assetTagNames.length; i++) {
						String assetTagName = assetTagNames[i].trim();

						PortletURL tagURL = PortletURLUtil.clone(portletURL,
								renderResponse);

						tagURL.setParameter(Field.ASSET_TAG_NAMES, assetTagName);
			%>

			<c:if test="<%= i == 0 %>">
				<div class="taglib-asset-tags-summary">
			</c:if>

			<a class="tag" href="<%=tagURL.toString()%>"><%=assetTagName%></a>

			<c:if test="<%= (i + 1) == assetTagNames.length %>">
		</div>
	</c:if> <%
 	}
 %>

	 <%
 	String[] assetCategoryIds = document
 			.getValues(Field.ASSET_CATEGORY_IDS);
 %> <c:if test="<%= Validator.isNotNull(assetCategoryIds[0]) %>">
		<div class="asset-entry-categories">
			<!-- <h1>alfhasdhfkasdfhkasf</h1> -->
			<%
				for (int i = 0; i < assetCategoryIds.length; i++) {
						long assetCategoryId = GetterUtil
								.getLong(assetCategoryIds[i]);

						AssetCategory assetCategory = null;

						try {
							assetCategory = AssetCategoryLocalServiceUtil
									.getCategory(assetCategoryId);
						} catch (NoSuchCategoryException nsce) {
						}

						if (assetCategory == null) {
							continue;
						}

						AssetVocabulary assetVocabulary = AssetVocabularyLocalServiceUtil
								.getVocabulary(assetCategory.getVocabularyId());

						PortletURL categoryURL = PortletURLUtil.clone(portletURL,
								renderResponse);

						categoryURL.setParameter(Field.ASSET_CATEGORY_NAMES,
								assetCategory.getName());
			%>

			<c:if test="<%= i == 0 %>">
				<div class="taglib-asset-categories-summary">
					<span class="asset-vocabulary"> <%=HtmlUtil.escape(assetVocabulary
								.getTitle(locale))%>: </span>
			</c:if>

			<a class="asset-category" href="<%=categoryURL.toString()%>"> <%=_buildAssetCategoryPath(assetCategory, locale)%>
			</a>

			<c:if test="<%= (i + 1) == assetCategoryIds.length %>">
		</div>
	</c:if> <%
 	}
 %> --%>

	 </span>
	 
<%/* CIP Code Change : Start */ %>

	<!-- /**
	 * Helper method to fetch Asset Entry landing page from the Asset Category. This is a recursive method and calls until the 
	 * Parent Category Id is equal to "0" for the Asset Category. 
	 * 
	 * @param AssetCategory : Child Category for which document is mapped. 
	 * @param Map<String,Layout> : Layout map.
	 * @param List<String> : To hold the all the child category levels and used in Side Navigation for highlighting the links. 
	 * @param RenderRequest : Render Request.
	 * @return String :  Holds the Child page and the Parent page name, where the Asset Entry should be displayed. Ex : "child page-parent page"   
	 */
	 --> 
	 
<%!private String getAssetEntryPage(AssetCategory childAssetCategory, Map<String,Layout> layoutMap, List<String> categoryNamesList, RenderRequest renderRequest) {
	  String categoryKey = "";
	  categoryNamesList.add(childAssetCategory.getName());
	  String tempCategoryKey = getCategory(childAssetCategory);
	  if(layoutMap.containsKey(tempCategoryKey)){
		  categoryKey = tempCategoryKey;
		  renderRequest.setAttribute("categoryNamesList",categoryNamesList);
		  return categoryKey;
	  }else{
		  try{	
			  if(childAssetCategory.getParentCategoryId() != 0){
		  childAssetCategory = AssetCategoryLocalServiceUtil.getCategory(childAssetCategory.getParentCategoryId());
		  categoryKey = getAssetEntryPage(childAssetCategory,layoutMap, categoryNamesList,renderRequest);
			  }
		  }catch(Exception e){
				e.getMessage();
				
			}
		  return categoryKey;
	  }
	  
  }%>
  
  <!-- /**
	 * Helper method to form the "child-parent" category to match with the "child-parent" page. 
	 * 
	 * @param AssetCategory : Child Category. 
	 * @return String :  Returns "child-parent" name.   
	 */
	 --> 
  <%!private String getCategory(AssetCategory childAssetCategory) {
	  AssetCategory parentAssetCategory = null;
	  AssetVocabulary assetVocabulary = null;
	  StringBuilder categorySB = new StringBuilder();
	  if(childAssetCategory.getParentCategoryId() != 0){
		try{	parentAssetCategory = AssetCategoryLocalServiceUtil.getCategory(childAssetCategory.getParentCategoryId());
		    categorySB.append(childAssetCategory.getName().trim());
			categorySB.append("-");
			categorySB.append(parentAssetCategory.getName().trim());
		}catch(Exception e){
			e.getMessage();
		}
		}else{
			try{
			assetVocabulary = AssetVocabularyLocalServiceUtil.getAssetVocabulary(childAssetCategory.getVocabularyId());
			categorySB.append(childAssetCategory.getName().trim());
			categorySB.append("-");
			categorySB.append(assetVocabulary.getName().trim());
			}catch(Exception e){
				e.getMessage();
			}
		}

	    return categorySB.toString().toUpperCase();
	  
  }%>
	
		<!-- /**
	 /**
	 * Removes the Content after "."
	 * @param assetEntryTitle
	 * @return title string
	 */
	 -->
		
   <%!private String cropTitle(String assetEntryTitle) {

	   String title = "";
		String escapeExtensons = PropsUtil.get("sideNavigation.remove.extensions").toLowerCase();
		if (assetEntryTitle.contains(".")) {
			if (escapeExtensons.contains(assetEntryTitle.substring(assetEntryTitle.lastIndexOf("."), assetEntryTitle.length()).toLowerCase())) {
				title = assetEntryTitle.substring(0, assetEntryTitle.lastIndexOf("."));
			}
			else {
				title = assetEntryTitle;
			}
		}
		else {
			title = assetEntryTitle;
		}
		return title;
	}%>
	 	
<%/* CIP Code Change : End */ %>
