<%--
/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */
--%>
<script type="text/javascript">
	function setVocabName(vocabName) {
		document.getElementById("selectedVocabName").value = vocabName;
		document.getElementById("asset_tags_searchIn").value = vocabName;
	}

</script>
<%@page import="java.util.Map"%>
<%@page import="com.liferay.portlet.asset.model.AssetVocabulary"%>
<%@page import="com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil"%>
<%@page import="java.util.Set"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@ include file="/html/portlet/search/facets/init.jsp" %>
<input type="hidden" value="" id="asset_tags_searchIn"  name="_3_searchIn"/>
<%

Map<String,String> vocabCatMap = (Map<String,String>) request.getAttribute("asset_vocabs.jsp-vocabCatMap");
if (vocabCatMap.isEmpty()) {
	return;
}
/* CIP Code Change : Start 
 * 
 * Description: Displaying vocabs instead of categories, custom look and feel, Adding Employee directory and Locations functionality
* */ 

/* if (termCollectors.isEmpty()) {
	return;
} */

String displayStyle = dataJSONObject.getString("displayStyle", "cloud");
int frequencyThreshold = dataJSONObject.getInt("frequencyThreshold");
int maxTerms = dataJSONObject.getInt("maxTerms", 10);
boolean showAssetCount = dataJSONObject.getBoolean("showAssetCount", true);
%>

<div class="<%= cssClass %>" id="<%= randomNamespace %>facet">

	<aui:input name="<%= facet.getFieldName() %>" type="hidden" value="<%= fieldParam %>" />

	<aui:field-wrapper cssClass='<%= randomNamespace + "asset-tags asset-tags" %>' label="" name="assetTags">
	<!-- <div class="refine-search">REFINE SEARCH</div> -->
		<ul id="refineSearchId" class="<%= (showAssetCount && displayStyle.equals("cloud")) ? "tag-cloud" : "tag-list" %>">
			<li class="facet-value default">
				<span class="results-header-span" style="margin-left:-10px;color:#000;"><img style="margin-right: 2px;top: 0px;" src="<%= themeDisplay.getPathThemeImages() %>/arrows/04_right.png"/><liferay-ui:message key="com.coach.search.category" /></span>
			</li>
			<li class="facet-value default <%= (Validator.isNull(request.getParameter("selectedVocabName"))  &&  Validator.isNull(request.getParameter("searchIn")))   ? "current-term" 
				: (Validator.isNotNull(request.getParameter("searchIn")) && request.getParameter("searchIn").contains(LanguageUtil.get(locale,"com.coach.search.all-categories"))) ? "current-term" : StringPool.BLANK %>">
				<a href="#" data-value="">
				<%-- <img alt="" src="<%= themeDisplay.getPathThemeImages() %>/common/<%= facetConfiguration.getLabel() %>.png" /> --%>
				<%-- <liferay-ui:message key="any" /> <liferay-ui:message key="<%= facetConfiguration.getLabel() %>" /> --%>
				<liferay-ui:message key='com.coach.search.all-categories' /></a>
			</li>
			<%
				String vocabNameString = request.getParameter("selectedVocabName");
				String searchInString = request.getParameter("searchIn");
				if(Validator.isNotNull(searchInString) && searchInString.contains("_"))
				{	
					searchInString = searchInString.split("_")[0].trim();
				}
				if(Validator.isNotNull(vocabNameString) && vocabNameString.contains("_"))
				{	
					vocabNameString = vocabNameString.split("_")[0].trim();
				}
				String currentTerm =StringPool.BLANK;
				
				if(/* !(vocabCatMap.containsKey("EMPLOYEE DIRECTORY")) */ true) 
				{
						Iterator iterVocab = vocabCatMap.keySet().iterator();
						while(iterVocab.hasNext()){
						String vocabName = (String)iterVocab.next();
						String vocabNameDisplay = null;
						if(vocabName.contains("_"))
						{	
							vocabNameDisplay = vocabName.split("_")[0].trim();
						}
						if(!vocabName.equals(LanguageUtil.get(locale,"com.coach.search.employee-directory"))) 
						{
							if(Validator.isNotNull(vocabNameString)&& vocabNameString.trim().equalsIgnoreCase(vocabNameDisplay)){
								currentTerm = "current-term";
							}
							else if(Validator.isNotNull(searchInString)&& searchInString.trim().equalsIgnoreCase(vocabNameDisplay)){
								currentTerm = "current-term";
							}
							%>
							             
			      			<li class="facet-value <%=currentTerm %>" >
			                                <a onclick="setVocabName('<%= vocabName.trim() %>')"  href="#" data-value="<%=Validator.isNotNull(vocabCatMap.get(vocabName)) ? vocabCatMap.get(vocabName) + "," + vocabCatMap.get(vocabName).toLowerCase() :"" %>"><%=Validator.isNotNull(vocabNameDisplay)? vocabNameDisplay : vocabName %></a>
			
			                </li>
							<%
						}else{
							if(Validator.isNotNull(searchInString) && searchInString.contains(LanguageUtil.get(locale,"com.coach.search.employee-directory"))){
					        	currentTerm = "current-term";
					        }
						   %>  
						   <li class="facet-value <%= currentTerm %>" >
		                  	 <a href="<%= vocabCatMap.get(LanguageUtil.get(locale,"com.coach.search.employee-directory"))%>"><%=LanguageUtil.get(locale,"com.coach.search.employee-directory")%></a>
		     				</li> 
		     			<%
						}
					}
				}
				 
			
			/* CIP Code Change : End */
			%> 
			
		</ul>
	</aui:field-wrapper>
</div>

<aui:script position="inline" use="aui-base">
	var container = A.one('<%= cssClassSelector %> .<%= randomNamespace %>asset-tags');

	if (container) {
		container.delegate(
			'click',
			function(event) {
				var term = event.currentTarget;

				var wasSelfSelected = false;

				var field = document.<portlet:namespace />fm['<portlet:namespace /><%= facet.getFieldName() %>'];

				var currentTerms = A.all('<%= cssClassSelector %> .<%= randomNamespace %>asset-tags .facet-value.current-term a');

				if (currentTerms) {
					currentTerms.each(
						function(item, index, collection) {
							item.ancestor('.facet-value').removeClass('current-term');
							
							if (item == term) {
								wasSelfSelected = true;
							}
						}
					);

					field.value = '';
					//field.value = term.attr('data-value');
				}

				if (!wasSelfSelected) {
					term.ancestor('.facet-value').addClass('current-term');

					//field.value = term.attr('data-value');
					field.value = '';
				}
				
				submitForm(document.<portlet:namespace />fm);
			},
			'.facet-value a'
		);
	}

	Liferay.provide(
		window,
		'<portlet:namespace /><%= facet.getFieldName() %>clearFacet',
		function() {
			document.<portlet:namespace />fm['<portlet:namespace /><%= facet.getFieldName() %>'].value = '';
			
			submitForm(document.<portlet:namespace />fm);
		},
		['aui-base']
	);
</aui:script>
