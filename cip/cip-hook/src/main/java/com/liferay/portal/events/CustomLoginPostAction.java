package com.liferay.portal.events;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.struts.LastPath;
import com.liferay.portal.kernel.util.WebKeys;

public class CustomLoginPostAction extends Action {
	private static Map<String, String> map = new HashMap<String, String>();

	public CustomLoginPostAction() {
		super();
	}

	public void run(HttpServletRequest request, HttpServletResponse response)
			throws ActionException {
		try {
			debug("CustomLoginPostAction for User: " + request.getRemoteUser());

			HttpSession session = request.getSession();
			LastPath lastPath = null;

			// Look for roles
			debug("Looking for associated roles");
			lastPath = new LastPath(request.getContextPath(),"/group/coach/home");

			if (lastPath != null) {
				debug("lastPath = " + lastPath.toString());
				session.setAttribute(WebKeys.LAST_PATH, lastPath);
			}
		} catch (Exception e) {
			throw new ActionException(e);
		}
	}

	/**
	 * Helper Method to create debug log messages
	 * 
	 * @param msg
	 *            Log message as String
	 */
	private void debug(String msg) {
		if (_log.isDebugEnabled()) {
			_log.debug(msg);
		}
	}
	private static Log _log = LogFactoryUtil.getLog(CustomLoginPostAction.class);

}