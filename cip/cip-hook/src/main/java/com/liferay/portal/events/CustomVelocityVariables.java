
package com.liferay.portal.events;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.liferay.portal.kernel.events.Action;
import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
//import com.coach.cip.util.Constants;

public class CustomVelocityVariables extends Action {

	private static Log logger = LogFactoryUtil.getLog(CustomVelocityVariables.class);
	/*static Map<String, String> portletTopperMap = new HashMap<String, String>(6);
	static List<NavItem> navItems;
	static {
		Properties pros = PropsUtil.getProperties("portlet-topper", false);
		for(String key : pros.stringPropertyNames()){
			String[] values = pros.getProperty(key).split(",");
			for(String value : values){
				portletTopperMap.put(value, key);
			}
		}
		
	}*/

	public void run(HttpServletRequest request, HttpServletResponse response) throws ActionException {
		
		
		 HttpSession session = request.getSession(false);
		 long creationTime  =	session.getCreationTime();
		 int  sessionLength    = session.getMaxInactiveInterval();
         long now              = new java.util.Date().getTime();
         long lastAccessed     = (now - session.getLastAccessedTime()) / 1000;
         long lengthInactive   = (now - session.getLastAccessedTime());
         long minutesRemaining = sessionLength - (lengthInactive / 1000);
         
         StringBuffer buf = new StringBuffer(200);
         buf.append("creationTime :").append(creationTime).append("\n");
         buf.append("sessionLength :").append(sessionLength).append("\n");
         buf.append("now :").append(now).append("\n");
         buf.append("lastAccessed :").append(lastAccessed).append("\n");
         buf.append("lengthInactive :").append(lengthInactive).append("\n");
         buf.append("minutesRemaining :").append(minutesRemaining).append("\n");
         /*

		Map<String, Object> vars = new HashMap<String, Object>();
		boolean isWriter = false;
		String portletTopperId = getPortletTopperId(request);
		if (portletTopperId == null) {
			portletTopperId = "portlet-topper";
		}
		vars.put("portletTopperId", portletTopperId);
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		long userId = themeDisplay.getUserId();
		vars.put("isWriter", checkRole(isWriter, themeDisplay, userId, "Writer"));
		vars.put("isDocumentLibrary", checkRole(isWriter, themeDisplay, userId, "Document Library Role"));
		
		String[] qandAPortletAccessRoles = PropsUtil.getArray(Constants.QANDA_PORTLET_ACCESS);
		boolean faqHRAccessRole = false;
		for (String role : qandAPortletAccessRoles) {
			 
			 if(role.equals("App-MCW-HR-Service-Center")){
				 faqHRAccessRole = checkRole(faqHRAccessRole, themeDisplay, userId, role);//true;
			 }
		} 
		vars.put("isQandAHRServicesAccessRoles",faqHRAccessRole);
		request.setAttribute(WebKeys.VM_VARIABLES, vars);
	*/}

	/*private boolean checkRole(boolean isWriter, ThemeDisplay themeDisplay, long userId, String role) {

		if (themeDisplay.isSignedIn()) {
			try {
				User user = UserLocalServiceUtil.getUserById(userId);
				isWriter = RoleLocalServiceUtil.hasUserRole(userId, user.getCompanyId(), role, true);
			}
			catch (Exception e) {
				logger.error(e.getMessage());
			}

		}
		return isWriter;
	}
	
	private String getPortletTopperId(HttpServletRequest request) {

		String portletTopperId = null;
		ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
		if (themeDisplay == null) {
			return null;
		}
		String currentPageURL =
				themeDisplay.getPortalURL() + themeDisplay.getPathFriendlyURLPrivateGroup() + themeDisplay.getScopeGroup().getFriendlyURL() +
				themeDisplay.getLayout().getFriendlyURL();
		Layout layout = themeDisplay.getLayout();
		List<Layout> layouts = themeDisplay.getLayouts();
		String pageURL = null;
		if (layout != null) {
			try {
				RequestVars requestVars = new RequestVars(request, themeDisplay, layout.getAncestorPlid(), layout.getAncestorLayoutId());
				navItems = NavItem.fromLayouts(requestVars, layouts);
				for (NavItem nav_item : navItems) {
					if (currentPageURL.equalsIgnoreCase(nav_item.getURL())) {
						pageURL = nav_item.getLayout().getFriendlyURL();
					}
					if (nav_item.hasChildren()) {
						for (NavItem nav_child : nav_item.getChildren()) {
							if (currentPageURL.equalsIgnoreCase(nav_child.getURL())) {
								pageURL = nav_item.getLayout().getFriendlyURL();
							}
							if (nav_child.hasChildren()) {
								for (NavItem nav_child_child : nav_child.getChildren()) {
									if (currentPageURL.equalsIgnoreCase(nav_child_child.getURL())) {
										pageURL = nav_item.getLayout().getFriendlyURL();
									}
								}
							}
						}
					}
				}

			}
			catch (Exception e) {
				logger.warn("Error while traversing the navigation to decide Portlet-topper class name. The default class will be returned", e);
			}
		}
		portletTopperId = portletTopperMap.get(pageURL);
		logger.debug("Portlet Bordr coloring Class  ---> " + pageURL +" : " + portletTopperId);
		return portletTopperId;
	}*/
}
