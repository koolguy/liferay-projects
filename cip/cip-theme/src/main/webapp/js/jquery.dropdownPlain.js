$(function(){

/*menu one Script starts here*/
$("ul.dropdown li.menuOne").hover(function(){
        $(this).css({"background":"#6673B9"});
        $(this).addClass("hover");
        $('ul:first',this).css('visibility', 'visible');
         
}, function(){
        $(this).css({"background":"#000"});
        $(this).removeClass("hover");
        $('ul:first',this).css('visibility', 'hidden');
     
 });
$("ul.dropdown li.menuOne ul.sub_menu li").hover(function(){
        $(this).css({"background":"#6673B9"});
}, function(){
        $(this).css({"background":"#FFF"});
});
$('ul.menuOne li').hover(function(){
		$(this).css({"background":"#848EC3"});	
		$(this).find("a").css({"color":"#6673B9"});
  }, function(){
		$(this).find("a").css({"color":"#FFF"});	
        $(this).css({"background":"#6673B9"});
		
});
/*menu one Script ends here*/
/*menu Two Script starts here*/
$("ul.dropdown li.menuTwo").hover(function(){
        $(this).css({"background":"#D466AD"});
        $(this).addClass("hover");
        $('ul:first',this).css('visibility', 'visible');
}, function(){
        $(this).css({"background":"#000"});
        $(this).removeClass("hover");
        $('ul:first',this).css('visibility', 'hidden');
    });
$("ul.dropdown li.menuTwo ul.sub_menu li").hover(function(){
        $(this).css({"background":"#D466AD"});
         
}, function(){
        $(this).css({"background":"#FFF"});
    });
$('ul.menuTwo li').hover(function(){
		$(this).css({"background":"#DF9CC8"});	
		$(this).find("a").css({"color":"#D466AD"});
  }, function(){
	    $(this).find("a").css({"color":"#FFF"});
        $(this).css({"background":"#D466AD"});
});
/*menu Two Script ends here*/
/*menu Three Script starts here*/
$("ul.dropdown li.menuThree").hover(function(){
        $(this).css({"background":"#789457"});
        $(this).addClass("hover");
        $('ul:first',this).css('visibility', 'visible');
}, function(){
        $(this).css({"background":"#000"});
        $(this).removeClass("hover");
        $('ul:first',this).css('visibility', 'hidden');
    });
$("ul.dropdown li.menuThree ul.sub_menu li").hover(function(){
        $(this).css({"background":"#789457"});
         
}, function(){
        $(this).css({"background":"#FFF"});
    });
$('ul.menuThree li').hover(function(){
		$(this).css({"background":"#95BB68"});
		$(this).find("a").css({"color":"#789457"});
  }, function(){
	    $(this).find("a").css({"color":"#FFF"});
        $(this).css({"background":"#789457"});
});
/*menu Three Script ends here*/
/*menu four Script starts here*/
$("ul.dropdown li.menuFour").hover(function(){
        $(this).css({"background":"#EB9C4F"});
        $(this).addClass("hover");
        $('ul:first',this).css('visibility','visible');
}, function(){
        $(this).css({"background":"#000"});
        $(this).removeClass("hover");
        $('ul:first',this).css('visibility', 'hidden');
    });
$("ul.dropdown li.menuFour ul.sub_menu li").hover(function(){
        $(this).css({"background":"#EB9C4F"});
         
}, function(){
        $(this).css({"background":"#FFF"});
    });
$('ul.menuFour li').hover(function(){
		$(this).css({"background":"#FDB771"});	
		$(this).find("a").css({"color":"#EB9C4F"});
  }, function(){
	  	$(this).find("a").css({"color":"#FFF"});
        $(this).css({"background":"#EB9C4F"});
});
/*menu five Script ends here*/
$("ul.dropdown li.menuFive").hover(function(){
        $(this).css({"background":"#945E3C"});
        $(this).addClass("hover");
        $('ul:first',this).css('visibility','visible');
}, function(){
        $(this).css({"background":"#000"});
        $(this).removeClass("hover");
        $('ul:first',this).css('visibility', 'hidden');
    });
$("ul.dropdown li.menuFive ul.sub_menu li").hover(function(){
        $(this).css({"background":"#945E3C"});
         
}, function(){
        $(this).css({"background":"#FFF"});
    });
$('ul.menuFive li').hover(function(){
		$(this).css({"background":"#9C7A5F"});	
		$(this).find("a").css({"color":"#945E3C"});
  }, function(){
	    $(this).find("a").css({"color":"#FFF"});
        $(this).css({"background":"#945E3C"});
});
/*menu five Script ends here*/
/*menu six Script ends here*/
$("ul.dropdown li.menuSix").hover(function(){
        $(this).css({"background":"#73B6D0"});
        $(this).addClass("hover");
        $('ul:first',this).css('visibility','visible');
}, function(){
        $(this).css({"background":"#000"});
        $(this).removeClass("hover");
        $('ul:first',this).css('visibility', 'hidden');
    });
$("ul.dropdown li.menuSix ul.sub_menu li").hover(function(){
        $(this).css({"background":"#73B6D0"});
         
}, function(){
        $(this).css({"background":"#FFF"});
    });
$('ul.menuSix li').hover(function(){
		$(this).css({"background":"#A0D1E2"});
		$(this).find("a").css({"color":"#73B6D0"});
  }, function(){
	    $(this).find("a").css({"color":"#FFF"});
        $(this).css({"background":"#73B6D0"});
});
/*menu six Script ends here*/
/*menu seven Script ends here*/
$("ul.dropdown li.menuSeven").hover(function(){
        $(this).css({"background":"#C0B5E0"});
        $(this).addClass("hover");
        $('ul:first',this).css('visibility','visible');
}, function(){
        $(this).css({"background":"#000"});
        $(this).removeClass("hover");
        $('ul:first',this).css('visibility', 'hidden');
    });
$("ul.dropdown li.menuSeven ul.sub_menu li").hover(function(){
        $(this).css({"background":"#9C8AD6"});
         
}, function(){
        $(this).css({"background":"#FFF"});
    });
$('ul.menuSeven li').hover(function(){
		$(this).css({"background":"#C0B5E0"});	
		$(this).find("a").css({"color":"#9C8AD6"});
  }, function(){
	    $(this).find("a").css({"color":"#FFF"});
        $(this).css({"background":"#9C8AD6"});
});
/*menu seven Script ends here*/
});