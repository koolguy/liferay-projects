<%@page import="com.liferay.portal.kernel.util.PropsUtil"%>
<%@page import="com.liferay.portal.kernel.util.DateUtil"%>
<%@page import="com.liferay.portal.kernel.util.Validator"%>
<%@page trimDirectiveWhitespaces="true"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.TimeZone"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.io.IOException"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.io.OutputStreamWriter"%>
<%@page import="java.net.URL"%>
<%@page import="java.net.URLConnection"%>



<%!String lastFetchedStockResult;
	long lastFetchedTime;%>

<%
	response.setContentType("text/xml;charset=UTF-8");
	response.setHeader("Cache-Control", "no-cache");
	response.setHeader("pragma", "no-cache");
%>

<%
	
	String result = "";
	long currTime = DateUtil.newDate().getTime();
	String stockCallIntervalStr = PropsUtil.get("stock.call.interval");
	long stockCallInterval = 60000;
	
	if(Validator.isNotNull(stockCallIntervalStr)){
		stockCallInterval = Integer.parseInt(stockCallIntervalStr);
	}
	if (Validator.isNotNull(this.lastFetchedStockResult)
			&& (currTime - this.lastFetchedTime < stockCallInterval)) {
		result = this.lastFetchedStockResult;
	} else {
		InputStream inputStream = null;
		InputStreamReader reader = null;
		BufferedReader bufferReader = null;

		try {

			String urlStr = "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22COH%22)%0A%09%09&&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys";
			URL url = new URL(urlStr.toString());
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			inputStream = conn.getInputStream();
			reader = new InputStreamReader(inputStream);
			bufferReader = new BufferedReader(reader);
			result = bufferReader.readLine();
			if (Validator.isNotNull(result)) {
				this.lastFetchedStockResult = result;
			}
			this.lastFetchedTime = DateUtil.newDate().getTime();
			bufferReader.close();
			reader.close();
			inputStream.close();
		} catch (IOException e) {

		} catch (Exception e) {
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
				}
			}
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
				}
			}
			if (bufferReader != null) {
				try {
					bufferReader.close();
				} catch (IOException e) {
				}
			}
		}
	}

	PrintWriter printWriter = response.getWriter();
	if (Validator.isNotNull(result)) {
		printWriter.print(result);
	} else {
		printWriter.print("");
	}
	printWriter.flush();
%>

