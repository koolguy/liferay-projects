package com.coach.cip.model.dao;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * Interface used to perform DML Basic Operations.
 * 
 *
 */
public interface GenericDAO<T> {
	
	/**
	 * Persist the given transient instance
	 * @param pojo
	 * @return Id
	 */
	public Serializable save(T pojo);
	
	/**
	 * Update the persistent instance
	 * @param pojo
	 */
	public void update(T pojo);
	
	/**
	 * Either save(Object) or update(Object) the given instance, 
	 * depending upon resolution of the unsaved-value checks 
	 * @param pojo
	 */
	public void saveOrUpdate(T pojo);
	
	/**
	 * Remove a persistent instance from the data store
	 * @param pojo
	 */
	public void delete(T pojo);
	
	/**
	 * Deletes the persistent instance of the given entity class with the given identifier
	 * @param classObject
	 * @param Id
	 */
	public void deleteById(Class<T> classObject, Serializable Id);
	
	/**
	 * Return the persistent instance of the given entity class 
	 * with the given identifier, or null if there is no such persistent instance.
	 * @param classObject
	 * @param Id
	 * @return object
	 */
	public T findById(Class<T> classObject, Serializable Id);
	
	/**
	 * Return the list of persistent instances of the given entity class
	 * which matches the given pojo object
	 * @param classObject
	 * @param pojo
	 * @param strict false if pattern matching, true if exact matching
	 * @return list of objects
	 */
	public List<T> findByExample(T pojo);
	
	/**
	 * Return the list of all persistent instances of the given entity class
	 * @param classObject
	 * @return
	 */
	public List<T> findAll(Class<T> classObject);
}
