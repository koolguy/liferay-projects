
package com.coach.cip.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * Time Utility Class
 * @author GalaxE.
 */

public class TimeUtil {

	/** The Constant logger. */
	private static final Log logger = LogFactoryUtil.getLog(TimeUtil.class);

	/**
	 * Takes date object and array of "time zone id" strings and returns the
	 * list of respective local time strings.
	 * @param date
	 * @param timeZoneIdArr
	 * @return List<String>
	 */
	public List<String> getLocalTimeArr(Date date, String[] timeZoneIdArr) {

		LoggerUtil.debugLogger(logger, "Start of getLocalTimeArr() method of TimeUtil::");
		List<String> list = new ArrayList<String>();
		String resultStr = null;
		for (String timeZoneId : timeZoneIdArr) {
			String timeZoneAttr[] = timeZoneId.split("-");
			String cityName = timeZoneAttr[0].trim();
			String timeZoneName = timeZoneAttr[1].trim();
			TimeZone tz = TimeZone.getTimeZone(timeZoneName);
			DateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, yyyy HH:mm a");
			dateFormat.setTimeZone(tz);
			resultStr = cityName + ": " + dateFormat.format(date);
			list.add(resultStr);
		}
		LoggerUtil.debugLogger(logger, "End of getLocalTimeArr() method of TimeUtil::");
		return list;
	}
}
