package com.coach.cip.model.dao;

import java.util.List;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.coach.cip.model.entity.Audit;
import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * 
 * Class used to perform AuditOperations.
 * 
 * @author GalaxE.
 */

@Repository(value="auditDAO")
@Transactional
public class AuditDAOImpl extends AbstractDAO<Audit> implements AuditDAO {

	/** The Constant LOG. */
	private static final Log logger = LogFactoryUtil.getLog(AuditDAOImpl.class);
	
	/**
	 * Method to save audit.
	 * 
	 * @param audit
	 * @throws Exception
	 */
	public void createAudit(Audit audit) throws Exception {
		LoggerUtil.debugLogger(logger, "Start of createAudit() method in AuditDAOImpl");
		long maxAuditEventId = 0l;
		try {
			maxAuditEventId = getMaxAuditEventIdFromAuditEntity();
			LoggerUtil.debugLogger(logger, "maxAuditEventId from SideNavigationcontroller------->"+maxAuditEventId);
			audit.setAuditEventId(maxAuditEventId+1);
			save(audit);
		} catch (Exception ex) {
			LoggerUtil.errorLogger(logger, "Exception Caught While Saving Audit Object in createAudit() Method in AuditDAOImpl Class.", ex);
		}
		LoggerUtil.debugLogger(logger, "End of createAudit() method in AuditDAOImpl");
	}

	/**
	 * Method to get max auditEventId.
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public long getMaxAuditEventIdFromAuditEntity() throws Exception {
		LoggerUtil.debugLogger(logger, "Start of getMaxAuditEventIdFromAuditEntity() method in AuditDAOImpl");
			long maxAuditEventId = 0l;
		try{
			StringBuffer query = new StringBuffer();
			query.append("select max(audit.auditEventId) from Audit audit");
			List list = getSession().createQuery(query.toString()).list();
			maxAuditEventId =  Long.valueOf(list.get(0).toString());
			LoggerUtil.debugLogger(logger, "maxAuditEventId from getMaxAuditEventIdFromAuditEntity()------->"+maxAuditEventId);
		}catch(Exception ex){
			LoggerUtil.errorLogger(logger, "Exception Caught While processing getMaxAuditEventIdFromAuditEntity() Method in AuditDAOImpl to get MaxAuditEventId.", ex);
		}
		LoggerUtil.debugLogger(logger, "End of getMaxAuditEventIdFromAuditEntity() method in AuditDAOImpl");
		return maxAuditEventId;
	}

}
