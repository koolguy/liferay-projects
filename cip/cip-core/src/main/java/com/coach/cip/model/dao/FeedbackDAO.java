package com.coach.cip.model.dao;

import com.coach.cip.model.entity.CIFeedback;

/**
 * Interface FeedbackDAO declares DML operations on ci_feedback table.
 *
 * @author GalaxE.
 */
public interface FeedbackDAO {
	/**
	 * This method is used to add the feedback to database.
	 *
	 * @param cifeedback
	 */
	void addFeedback(CIFeedback cifeedback);
}
