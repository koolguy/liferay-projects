package com.coach.cip.model.dao;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;
import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * 
 * Class Used to perform Hibernate Basic CRUD Operations.
 * 
 *
 */
public abstract class AbstractDAO<T> implements GenericDAO<T> {
	
	@SuppressWarnings("unused")
	private SessionFactory sessionFactory;
	protected HibernateTemplate hibernateTemplate;
	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(AbstractDAO.class);
	
	/**
	 * Persist the given transient instance
	 * @param pojo
	 * @return Id
	 */
	public Serializable save(T pojo) {
		LoggerUtil.debugLogger(logger, "save() Method Starts in AbstractDAO");
		Serializable Id = null;
		try {
			Id = hibernateTemplate.save(pojo);
			LoggerUtil.debugLogger(logger, "Save Successful in save() Method in AbstractDAO Class");
		} catch (HibernateException e) {
			logger.error("save failed", e);
			LoggerUtil.errorLogger(logger, "Exception Caught While Saving Persistance Object in AbstractDAO", e);
			throw e;
		}
		LoggerUtil.debugLogger(logger, "save() Method Ends in AbstractDAO");
		return Id;
	}
	
	/**
	 * Update the persistent instance
	 * @param pojo
	 */
	public void update(T pojo) {
		LoggerUtil.debugLogger(logger, "update() Method Starts in AbstractDAO");
		try {			
			hibernateTemplate.update(pojo);	
			LoggerUtil.debugLogger(logger, "update successful in update() Method in AbstractDAO");
		} catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "Exception Caught While Updating Persistance Object in AbstractDAO", e);
			throw e;
		}		
		LoggerUtil.debugLogger(logger, "update() Method Ends in AbstractDAO");
	}
	
	/**
	 * Either save(Object) or update(Object) the given instance, 
	 * depending upon resolution of the unsaved-value checks 
	 * @param pojo
	 */
	public void saveOrUpdate(T pojo) {
		LoggerUtil.debugLogger(logger, "saveOrUpdate() Method Starts in AbstractDAO");
		try {
			hibernateTemplate.saveOrUpdate(pojo);
			LoggerUtil.debugLogger(logger, "saveOrUpdate successful in saveOrUpdate() Method in AbstractDAO");
		} catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "Exception Caught While Updating Persistance Object in AbstractDAO", e);
			throw e;
		}	
		LoggerUtil.debugLogger(logger, "saveOrUpdate() Method Ends in AbstractDAO");
	}
	
	/**
	 * Remove a persistent instance from the data store
	 * @param pojo
	 */
	public void delete(T pojo) {
		LoggerUtil.debugLogger(logger, "delete() Method Starts in AbstractDAO");
		try {
			hibernateTemplate.delete(pojo);
			LoggerUtil.debugLogger(logger, "delete successful in delete() Method in AbstractDAO");
		} catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "Exception Caught While Deleting Persistance Object in AbstractDAO", e);
			throw e;
		}	
		LoggerUtil.debugLogger(logger, "delete() Method Ends in AbstractDAO");
	}
	
	/**
	 * Deletes the persistent instance of the given entity class with the given identifier
	 * @param classObject
	 * @param Id
	 */
	public void deleteById(Class<T> classObject, Serializable Id) {
		LoggerUtil.debugLogger(logger, "deleteById() Method Starts in AbstractDAO");
		try {
		T pojo = findById(classObject, Id);
		LoggerUtil.debugLogger(logger, "POJO Class in deleteById() Method  in AbstractDAO----"+pojo);
		if(pojo != null){
			delete(pojo);
		}
		} catch(HibernateException e){
			LoggerUtil.errorLogger(logger, "Exception Caught While Deleting Persistance ObjectById() in AbstractDAO", e);
			throw e;
		}
		LoggerUtil.debugLogger(logger, "deleteById() Method Ends in AbstractDAO");
	}
	
	/**
	 * Return the persistent instance of the given entity class 
	 * with the given identifier, or null if there is no such persistent instance.
	 * @param classObject
	 * @param Id
	 * @return object
	 */
	public T findById(Class<T> classObject, Serializable Id) {
		LoggerUtil.debugLogger(logger, "findById() Method Starts in AbstractDAO");
		T pojo = null;
		try {
			pojo = (T)hibernateTemplate.get(classObject, Id);
			LoggerUtil.debugLogger(logger, "findById() Method successful in AbstractDAO");
		} catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "Exception Caught While Processing findById() in AbstractDAO", e);
			throw e;
		}	
		LoggerUtil.debugLogger(logger, "findById() Method Ends in AbstractDAO");
		return pojo;
	}
	
	/**
	 * Return the list of persistent instances of the given entity class
	 * which matches the given pojo object
	 * @param classObject
	 * @param pojo
	 * @param strict false if pattern matching, true if exact matching
	 * @return list of objects
	 */
	@SuppressWarnings("unchecked")
	public List<T> findByExample(T pojo) {
		LoggerUtil.debugLogger(logger, "findByExample() Method Starts in AbstractDAO");
		List<T> list = null;
		try {
			list = (List<T>) hibernateTemplate.findByExample(pojo);
			LoggerUtil.debugLogger(logger, "findByExample() Successful in AbstractDAO");
		} catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "Exception Caught While Processing findByExample() in AbstractDAO", e);
			throw e;
		}	
		LoggerUtil.debugLogger(logger, "findByExample() Method Ends in AbstractDAO");
		return list;
	}
	
	/**
	 * Return the list of all persistent instances of the given entity class
	 * @param classObject
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<T> findAll(Class<T> classObject) {
		LoggerUtil.debugLogger(logger, "findAll() Method Starts in AbstractDAO");
		List<T> list;
		try {
			list = (List<T>)hibernateTemplate.findByCriteria(DetachedCriteria.forClass(classObject));
			LoggerUtil.debugLogger(logger, "findAll() Method Successful in AbstractDAO");
		} catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "Exception Caught While Processing findAll() Method in AbstractDAO", e);
			throw e;
		}	
		LoggerUtil.debugLogger(logger, "findAll() Method Ends in AbstractDAO");
		return list;
	}
	
	/**
	 * Method used to set sessionFactory.
	 * @param sessionFactory
	 */
	@Autowired
	public void setSessionFactory(SessionFactory sessionFactory) {
		LoggerUtil.debugLogger(logger, "setSessionFactory() Method Starts in AbstractDAO");
		this.sessionFactory = sessionFactory;
		if (this.hibernateTemplate == null || sessionFactory != this.hibernateTemplate.getSessionFactory()) {
			this.hibernateTemplate = new HibernateTemplate(sessionFactory);
		}
		LoggerUtil.debugLogger(logger, "setSessionFactory() Method Ends in AbstractDAO");
	}
	
	/**
	 * Method used to return list of entities based on the property.
	 * @param classObject
	 * @param propertyName
	 * @param value
	 * @return List<T>
	 */
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<T> findByProperty(Class<T> classObject, String propertyName,
			final Object value) {
		LoggerUtil.debugLogger(logger, "findByProperty() Method Starts in AbstractDAO");
		List<T> list = null;
		try {
			final String queryString = "from " + classObject.getName()
					+ " as model where model." + propertyName + "= ?";
			HibernateCallback hc = new HibernateCallback() {
				public List<T> doInHibernate(Session session)
						throws HibernateException, SQLException {
					return session.createQuery(queryString)
							.setParameter(0, value).list();
				}
			};
			list =  (List<T>) hibernateTemplate.execute(hc);
		} catch (RuntimeException re) {
			LoggerUtil.errorLogger(logger, "Exception Caught while processing findByproperty() method in AbstractDAO",re);
			throw re;
		}
		LoggerUtil.debugLogger(logger, "findByProperty() Method Ends in AbstractDAO");
		return list;
	}
	
	/**
	 * Method used to return list of Strings based on the property.
	 * @param classObject
	 * @param returnColumnName
	 * @param propertyName
	 * @param value
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<String> findDistinctByProperty(Class<T> classObject,String returnColumnName, String propertyName,
			final Object value) {
		LoggerUtil.debugLogger(logger, "findDistinctByProperty() Method Starts in AbstractDAO");
		List<String> list = null;
		try {
			final String queryString = "select distinct model."+returnColumnName+" from " + classObject.getName()
					+ " as model where model." + propertyName + "= ?";
			HibernateCallback hc = new HibernateCallback() {
				public List<T> doInHibernate(Session session)
						throws HibernateException, SQLException {
					return session.createQuery(queryString)
							.setParameter(0, value).list();
				}
			};
			list =  (List<String>) hibernateTemplate.execute(hc);
		} catch (RuntimeException re) {
			LoggerUtil.errorLogger(logger, "Exception Caught while processing findDistinctByProperty() method in AbstractDAO",re);
			throw re;
		}
		LoggerUtil.debugLogger(logger, "findDistinctByProperty() Method Ends in AbstractDAO");
		return list;
	}
	
	/**
	 * Returns contextual session 
	 * @return current or contextual session
	 */
	public Session getSession() {
		LoggerUtil.debugLogger(logger, "getSession() Method Starts in AbstractDAO");
		Session session = null;
		if(hibernateTemplate != null && hibernateTemplate.getSessionFactory() != null){
			session = hibernateTemplate.getSessionFactory().getCurrentSession();
		}
		LoggerUtil.debugLogger(logger, "getSession() Method Ends in AbstractDAO");
		return session;
	}
	
}
