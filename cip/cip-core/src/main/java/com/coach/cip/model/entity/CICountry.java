package com.coach.cip.model.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Class is used to capture the characteristics of ci_country. 
 * This is reusable class for ci_country.
 * 
 * @author GalaxE.
 * 
 */
@Entity
@Table(name = "ci_country")
public class CICountry implements java.io.Serializable {

	private static final long serialVersionUID = 3307112695787918643L;
	// Fields	
	private Long countryId;
	private String countryName;
	private Set<CIRegion> CIRegions = new HashSet<CIRegion>(0);

	// Constructors

	/** default constructor */
	public CICountry() {
	}

	/** minimal constructor */
	public CICountry(Long countryId, String countryName) {
		this.countryId = countryId;
		this.countryName = countryName;
	}

	/** full constructor */
	public CICountry(Long countryId, String countryName, Set<CIRegion> CIRegions) {
		this.countryId = countryId;
		this.countryName = countryName;
		this.CIRegions = CIRegions;
	}
	/**
	 * Method used to get countryId.
	 * @return countryId.
	 */

	@Id
	@Column(name = "countryId", unique = true, nullable = false)
	public Long getCountryId() {
		return this.countryId;
	}

	/**
	 * Method used to set countryId.
	 * @param countryId
	 */
	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	/**
	 * Method used to get countryName.
	 * @return countryName.
	 */
	@Column(name = "countryName", nullable = false, length = 100)
	public String getCountryName() {
		return this.countryName;
	}

	/**
	 * Method used to set countryName.
	 * @param countryName
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}

	/**
	 * Method used to get ciRegions.
	 * @return CIRegions Set.
	 */
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "CICountry")
	public Set<CIRegion> getCIRegions() {
		return this.CIRegions;
	}

	/**
	 * Method used to set ciRegions.
	 * @param CIRegions
	 */
	public void setCIRegions(Set<CIRegion> CIRegions) {
		this.CIRegions = CIRegions;
	}

}