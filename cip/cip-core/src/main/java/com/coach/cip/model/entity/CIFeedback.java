package com.coach.cip.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Class is used to capture the characteristics of ci_feedback. 
 * This is reusable class for ci_feedback.
 *
 * @author GalaxE.
 */
@Entity
@Table(name = "ci_feedback")
public class CIFeedback implements java.io.Serializable {

	
	private static final long serialVersionUID = -4452923042286893864L;
	// Fields
	/**
	 * feedbackId for the feedack submitted by the User.
	 */
	private Long feedbackId;
	/**
	 * firstName of the user.
	 */
	private String firstName;
	/**
	 * lastName of the user.
	 */
	private String lastName;
	/**
	 * emailAddress of the user.
	 */
	private String emailAddress;
	/**
	 * phoneNumber of the user.
	 */
	private String phoneNumber;
	/**
	 * comments submitted by user.
	 */
	private String comments;
	/**
	 * topic selected by the user.
	 */
	private String topic;

	// Constructors
	/**
	 * Default constructor.
	 */
	public CIFeedback() {
	}

	/**
	 * Full constructor.
	 *
	 * @param feedbackId
	 * @param firstName
	 * @param lastName
	 * @param emailAddress
	 * @param phoneNumber
	 * @param comments
	 * @param topic
	 */
	public CIFeedback(Long feedbackId, String firstName, String lastName,
			String emailAddress, String phoneNumber, String comments,
			String topic) {
		this.feedbackId = feedbackId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailAddress = emailAddress;
		this.phoneNumber = phoneNumber;
		this.comments = comments;
		this.topic = topic;
	}

	// Property accessors
	/**
	 * Getter method for feedbackId.
	 *
	 * @return feedbackId
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "feedbackId", unique = true, nullable = false)
	public Long getFeedbackId() {
		return this.feedbackId;
	}

	/**
	 * Setter method for feedbackId.
	 *
	 * @param feedbackId
	 */
	public void setFeedbackId(Long feedbackId) {
		this.feedbackId = feedbackId;
	}

	/**
	 * Getter method for firstName.
	 *
	 * @return firstName.
	 */
	@Column(name = "firstName", length = 75, nullable = false)
	public String getFirstName() {
		return this.firstName;
	}

	/**
	 * Setter method for firstName.
	 *
	 * @param firstName
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Getter method for lastName.
	 *
	 * @return lastName
	 */
	@Column(name = "lastName", length = 75, nullable = false)
	public String getLastName() {
		return this.lastName;
	}

	/**
	 * Setter method for lastName.
	 *
	 * @param lastName
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Getter method for emailAddress.
	 *
	 * @return emailAddress
	 */
	@Column(name = "emailAddress", length = 75)
	public String getEmailAddress() {
		return this.emailAddress;
	}

	/**
	 * Setter method for emailAddress.
	 *
	 * @param emailAddress
	 */
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	/**
	 * Getter method for phoneNumber.
	 *
	 * @return phoneNumber
	 */
	@Column(name = "phoneNumber", length = 75)
	public String getPhoneNumber() {
		return this.phoneNumber;
	}

	/**
	 * Setter method for phoneNumber.
	 *
	 * @param phoneNumber
	 */
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	/**
	 * Getter method for comments.
	 *
	 * @return comments
	 */
	@Column(name = "comments", length = 2000, nullable = false)
	public String getComments() {
		return this.comments;
	}

	/**
	 * Setter method for comments.
	 *
	 * @param comments
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * Getter method for topic.
	 *
	 * @return topic
	 */
	@Column(name = "topic", length = 500, nullable = false)
	public String getTopic() {
		return this.topic;
	}

	/**
	 * Setter method for topic.
	 *
	 * @param topic
	 */
	public void setTopic(String topic) {
		this.topic = topic;
	}
}
