package com.coach.cip.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 
 * Class Used to locate the Service Objects.
 * 
 * @author GalaxE.
 *
 */
@SuppressWarnings("rawtypes")
@Component(value = "serviceLocator")
public class ServiceLocatorImpl implements ServiceLocator {

	private static ServiceLocatorImpl ourInstance = new ServiceLocatorImpl();

	public static ServiceLocatorImpl getInstance() {
		return ourInstance;
	}

	private ServiceLocatorImpl() {

	}

	@Autowired
	private UserService userService;


	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private QuickLinksService quickLinksService;

	@Autowired
	private PersonalMessageService personalMessageService;

	
	@Autowired
	private LocationService locationService;

	@Autowired
	private FeedbackService feedbackService;

	@Autowired
	private AuditService auditService;
	
	@Autowired
	private NotificationService notificationService;

	/**
	 * returns auditService.
	 */
	public AuditService getAuditService() {
		return auditService;
	}

	public void setAuditService(AuditService auditService) {
		this.auditService = auditService;
	}

	public QuickLinksService getQuickLinksService() {
		return quickLinksService;
	}

	public void setQuickLinksService(QuickLinksService quickLinksService) {
		this.quickLinksService = quickLinksService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	public EmployeeService getEmployeeService() {
		return employeeService;
	}

	public void setEmployeeService(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}

	public PersonalMessageService getPersonalMessageService() {
		return personalMessageService;
	}

	public void setPersonalMessageService(
			PersonalMessageService personalMessageService) {
		this.personalMessageService = personalMessageService;
	}

	public LocationService getLocationService() {
		return locationService;
	}

	public void setLocationService(LocationService locationService) {
		this.locationService = locationService;
	}

	/**
	 * @return feedbackService
	 */
	public FeedbackService getFeedbackService() {
		return feedbackService;
	}

	/**
	 * @param feedbackService
	 */
	public void setFeedbackService(FeedbackService feedbackService) {
		this.feedbackService = feedbackService;
	}

	public NotificationService getNotificationService() {
		return notificationService;
	}

	public void setNotificationService(NotificationService notificationService) {
		this.notificationService = notificationService;
	}
	
	
}