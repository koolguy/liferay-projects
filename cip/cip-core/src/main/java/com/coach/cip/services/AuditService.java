package com.coach.cip.services;

import org.springframework.stereotype.Component;
import com.coach.cip.model.entity.Audit;

/**
 * 
 * Class used as a AuditSericeComponent .
 * 
 * @author GalaxE.
 */
@Component
public interface AuditService {
	
	/**
	 * Method to save audit.
	 * 
	 * @param audit
	 * @throws Exception
	 */
	public void createAudit(Audit audit) throws Exception;
	
	/**
	 * Method to get max auditEventId.
	 * @return
	 * @throws Exception
	 */
	public long getMaxAuditEventIdFromAuditEntity() throws Exception;

}
