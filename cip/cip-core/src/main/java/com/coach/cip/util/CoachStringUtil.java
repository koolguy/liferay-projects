
package com.coach.cip.util;

import java.text.BreakIterator;
import java.util.Locale;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;

/**
 * String utility class
 * @author GalaxE.
 */

public class CoachStringUtil {

	/** The Constant logger. */
	private static final Log logger = LogFactoryUtil.getLog(CoachStringUtil.class);

	/**
	 * Returns the sub sentence string
	 * @param sentence
	 * @param startQueryIndex
	 * @param endQueryIndex
	 * @param radius
	 * @param appender
	 * @return Sub sentence String
	 */
	private static String getSubSentence(String sentence, int startQueryIndex, int endQueryIndex, int radius, String appender) {

		LoggerUtil.debugLogger(logger, "Start of getSubSentence() method of CoachStringUtil::");
		int startIndex = 0;
		int i = 0;
		int endIndex = sentence.length() - 1;
		if ((startQueryIndex - radius) > 0) {
			i = sentence.indexOf(' ', startQueryIndex - radius);
			if (i > -1 && i < startQueryIndex) {
				startIndex = i + 1;
			}
		}
		if ((endQueryIndex + radius) < endIndex) {
			i = sentence.indexOf(' ', endQueryIndex + radius);
			if (i > -1) {
				endIndex = i - 1;
			}
		}
		String str = sentence;
		if (startIndex <= endIndex && endIndex < sentence.length()) {
			str = sentence.substring(startIndex, endIndex + 1);
			if (startIndex > 0) {
				str = appender + str;
			}
			if (endIndex < sentence.length() - 1) {
				str = str + appender;
			}
		}
		str = str + " ";
		LoggerUtil.debugLogger(logger, "End of getSubSentence() method of CoachStringUtil::");
		return str;

	}

	/**
	 * Returns the Summary string
	 * @param sentence
	 * @param queryWords
	 * @return Summary String
	 */
	private static String getSentenceSummary(String sentence, String[] queryWords) {

		LoggerUtil.debugLogger(logger, "Start of getSentenceSummary() method of CoachStringUtil::");
		int radius = 25;
		String appender = "...";
		int startQueryWordIndex = -1;
		int endQueryWordIndex = -1;
		String sentenceLower = sentence.toLowerCase();
		for (String word : queryWords) {
			String wordLower = word.toLowerCase();
			if (sentenceLower.indexOf(wordLower) >= 0) {
				if (sentence.length() <= (radius * 2)) {
					return sentence;
				}
				else {
					int start = sentenceLower.indexOf(wordLower);
					if (startQueryWordIndex == -1) {
						startQueryWordIndex = start;
					}
					else if (start < startQueryWordIndex) {
						startQueryWordIndex = start;
					}
					int end = sentenceLower.lastIndexOf(wordLower);
					end = end + word.length();
					if (endQueryWordIndex == -1) {
						endQueryWordIndex = end;
					}
					else if (end > endQueryWordIndex) {
						endQueryWordIndex = end;
					}
				}
			}
		}
		// Query word not found in the sentence
		if (startQueryWordIndex == -1) {
			LoggerUtil.debugLogger(logger, "End of getSentenceSummary() method of CoachStringUtil::");
			return null;
		}
		else {
			// Query word found
			LoggerUtil.debugLogger(logger, "End of getSentenceSummary() method of CoachStringUtil::");
			return getSubSentence(sentence, startQueryWordIndex, endQueryWordIndex, radius, appender);
		}

	}

	/**
	 * Returns the Summary string
	 * @param stringToExamine
	 * @param queryWords
	 * @param locale
	 * @return Summary String
	 */
	public static String getSummary(String stringToExamine, String[] queryWords, Locale locale) {
		LoggerUtil.debugLogger(logger, "Start of getSummary() method of CoachStringUtil::");
		BreakIterator sentenceIterator = BreakIterator.getSentenceInstance(locale);
		// remove all the control characters
		stringToExamine = stringToExamine.replaceAll("[\u0002\u001f\u0016\u000f]", "");
		sentenceIterator.setText(stringToExamine);
		int start = sentenceIterator.first();
		int end = sentenceIterator.next();
		StringBuilder returnString = new StringBuilder("");
		String sentence = null;
		for (; end != BreakIterator.DONE; end = sentenceIterator.next()) {
			sentence = stringToExamine.substring(start, end - 1);
			if (Validator.isNotNull(sentence)) {
				sentence = getSentenceSummary(sentence, queryWords);
				if (sentence != null && sentence.length() > 0) {
					returnString.append(sentence);
				}
			}
			start = end;
		}
		LoggerUtil.debugLogger(logger, "End of getSummary() method of CoachStringUtil::");
		return returnString.toString();
	}
}
