
package com.coach.cip.model.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

/**
 * Class is used to capture the characteristics of personalMessages and
 * associated entities. This is a reusable class for personal messages.
 * @author GalaxE.
 */

@NamedNativeQueries({
	@NamedNativeQuery(name = "callPersonalMessageUsersProcedure", query = "CALL GetMessageForGivenUserId(:userId,:resultCount)", resultClass = CIPersonalMessage.class)
})
@Entity
@Table(name = "ci_personalmessage")
public class CIPersonalMessage implements java.io.Serializable {
	
	
	private static final long serialVersionUID = 7128075942431649299L;
	// Fields

	private Long messageId;
	private User userByModifiedBy;
	private User userByCreatedBy;
	private Timestamp createDate;
	private Timestamp modifiedDate;
	private String subject;
	private String message;
	private Timestamp displayDate;
	private Timestamp expirationDate;
	private String messageType;
	// private Set<Region> messageForLocations = new HashSet<Region>(0);
	private Set<CIMessageForLocation> messageForLocations = new HashSet<CIMessageForLocation>(0);
	private Set<Role> messageForRoles = new HashSet<Role>(0);

	// Constructors

	/** default constructor. */
	public CIPersonalMessage() {

	}

	/** minimal constructor. */
	public CIPersonalMessage(Long messageId, User userByModifiedBy, User userByCreatedBy) {

		this.messageId = messageId;
		this.userByModifiedBy = userByModifiedBy;
		this.userByCreatedBy = userByCreatedBy;
	}

	/** full constructor. */
	public CIPersonalMessage(
		Long messageId, User userByModifiedBy, User userByCreatedBy, Timestamp createDate, Timestamp modifiedDate, String subject, String message,
		Timestamp displayDate, Timestamp expirationDate, String messageType, Set<CIMessageForLocation> messageForLocations, Set<Role> messageForRoles) {

		this.messageId = messageId;
		this.userByModifiedBy = userByModifiedBy;
		this.userByCreatedBy = userByCreatedBy;
		this.createDate = createDate;
		this.modifiedDate = modifiedDate;
		this.subject = subject;
		this.message = message;
		this.displayDate = displayDate;
		this.expirationDate = expirationDate;
		this.messageType = messageType;
		this.messageForLocations = messageForLocations;
		this.messageForRoles = messageForRoles;
	}

	/**
	 * Method used to get messageId.
	 * @return messageId.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "messageId", unique = true, nullable = false)
	public Long getMessageId() {

		return this.messageId;
	}

	/**
	 * Method used to set messageId.
	 * @param messageId
	 */
	public void setMessageId(Long messageId) {

		this.messageId = messageId;
	}

	/**
	 * Method used to get user object.
	 * @return userByModifiedBy.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modifiedBy", nullable = false)
	public User getUserByModifiedBy() {

		return this.userByModifiedBy;
	}

	/**
	 * Method used to set user object.
	 * @param userByModifiedBy
	 */
	public void setUserByModifiedBy(User userByModifiedBy) {

		this.userByModifiedBy = userByModifiedBy;
	}

	/**
	 * Method used to get user object.
	 * @return userByCreatedBy.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "createdBy", nullable = false)
	public User getUserByCreatedBy() {

		return this.userByCreatedBy;
	}

	/**
	 * Method used to set user object.
	 * @param userByCreatedBy
	 */
	public void setUserByCreatedBy(User userByCreatedBy) {

		this.userByCreatedBy = userByCreatedBy;
	}

	/**
	 * Method used to get time stamp for createDate.
	 * @return createDate
	 */
	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {

		return this.createDate;
	}

	/**
	 * Method used to set time stamp for createDate.
	 * @param createDate
	 */
	public void setCreateDate(Timestamp createDate) {

		this.createDate = createDate;
	}

	/**
	 * Method used to get time stamp for modifiedDate.
	 * @return modifiedDate.
	 */
	@Column(name = "modifiedDate", length = 0)
	public Timestamp getModifiedDate() {

		return this.modifiedDate;
	}

	/**
	 * Method used to set time stamp for modifiedDate.
	 * @param modifiedDate
	 */
	public void setModifiedDate(Timestamp modifiedDate) {

		this.modifiedDate = modifiedDate;
	}

	/**
	 * Method used to get subject.
	 * @return subject.
	 */
	@Column(name = "subject", length = 200)
	public String getSubject() {

		return this.subject;
	}

	/**
	 * Method used to set subject.
	 * @param subject
	 */
	public void setSubject(String subject) {

		this.subject = subject;
	}

	/**
	 * Method used to get message.
	 * @return message.
	 */
	@Column(name = "message", length = 2000)
	public String getMessage() {

		return this.message;
	}

	/**
	 * Method used to set message.
	 * @param message
	 */
	public void setMessage(String message) {

		this.message = message;
	}

	/**
	 * Method used to get displayDate.
	 * @return displayDate.
	 */
	@Column(name = "displayDate", length = 0)
	public Timestamp getDisplayDate() {

		return this.displayDate;
	}

	/**
	 * Method used to set displayDate.
	 * @param displayDate
	 */
	public void setDisplayDate(Timestamp displayDate) {

		this.displayDate = displayDate;
	}

	/**
	 * Method used to get expirationDate.
	 * @return expirationDate.
	 */
	@Column(name = "expirationDate", length = 0)
	public Timestamp getExpirationDate() {

		return this.expirationDate;
	}

	/**
	 * Method used to set expirationDate.
	 * @param expirationDate
	 */
	public void setExpirationDate(Timestamp expirationDate) {

		this.expirationDate = expirationDate;
	}

	/**
	 * Method used to get messageType.
	 * @return messageType.
	 */
	@Column(name = "messageType", length = 20)
	public String getMessageType() {

		return this.messageType;
	}

	/**
	 * Method used to set messageType.
	 * @param messageType
	 */
	public void setMessageType(String messageType) {

		this.messageType = messageType;
	}

	/*
	 * cascade=CascadeType.ALL is deleting the child objects i.e "regionId" from
	 * "region" table are also getting deleted when a "message" is deleted from
	 * "CI_PersonalMessages". Do not use CascadeType.ALL & CascadeType.REMOVE.
	 */

	/**
	 * Method used to set messageForLocations.
	 * @return messageForLocations Set.
	 */
	
	@SuppressWarnings("deprecation")
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "personalMessage")
	@Cascade({
		org.hibernate.annotations.CascadeType.DELETE_ORPHAN
	})
	public Set<CIMessageForLocation> getMessageForLocations() {

		return messageForLocations;
	}

	/**
	 * Method used to set messageForLocations.
	 * @param messageForLocations
	 */
	public void setMessageForLocations(Set<CIMessageForLocation> messageForLocations) {

		this.messageForLocations = messageForLocations;
	}

	/*
	 * cascade=CascadeType.ALL is deleting the child objects i.e "roleid" from
	 * "role_" table are also getting deleted when a "message" is deleted from
	 * "CI_PersonalMessages". Do not use CascadeType.ALL & CascadeType.REMOVE.
	 */

	/**
	 * Method used to get messageForRoles.
	 * @return messageForRoles Set.
	 */

	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "ci_messageforrole", joinColumns = @JoinColumn(name = "messageId"), inverseJoinColumns = @JoinColumn(name = "roleId"))
	public Set<Role> getMessageForRoles() {

		return messageForRoles;
	}

	/**
	 * Method used to set messageForRoles.
	 * @param messageForRoles
	 *            the messageForRoles to set
	 */
	public void setMessageForRoles(Set<Role> messageForRoles) {

		this.messageForRoles = messageForRoles;
	}

}
