package com.coach.cip.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.StringTokenizer;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.xml.namespace.QName;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.soap.SOAPException;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.coach.ca.model.Transmittal;
import com.coach.ca.service.TransmittalLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.Criterion;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.FileVersion;
import com.liferay.portal.kernel.util.ArrayUtil;
import com.liferay.portal.kernel.util.LocaleUtil;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.Layout;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.service.LayoutLocalServiceUtil;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.asset.AssetRendererFactoryRegistryUtil;
import com.liferay.portlet.asset.model.AssetCategory;
import com.liferay.portlet.asset.model.AssetRenderer;
import com.liferay.portlet.asset.model.AssetRendererFactory;
import com.liferay.portlet.asset.service.AssetCategoryLocalServiceUtil;
import com.liferay.portlet.asset.service.AssetVocabularyLocalServiceUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntryConstants;
import com.liferay.portlet.documentlibrary.util.DLUtil;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;

import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Security;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * Utility Class used for web service operations.
 *
 * @author GalaxE.
 *
 */

public class WebServiceUtil {

	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(WebServiceUtil.class);

	/**
	 * Helper Method to search the stores by keyword.
	 *
	 * @param keywords
	 * @return
	 * @throws Exception
	 */
	public static List<StoreDetailsVO> getOpenStoreByKeyWord(String keywords)throws Exception {
		Properties prop = new Properties();
		String storesUrl = null;
		String wsuserName = null;
		String wspassword = null;
		String employeesUrl = null;
		try {
			String catalinaHome = System.getProperty("catalina.home");
			String webServiceFileName = PropsUtil.get("com.coach.cip.webservice.properties.filename");
			prop.load(new FileInputStream(catalinaHome+"/"+webServiceFileName));
			storesUrl = prop.getProperty("coach.cip.webservice.stores.url");
			wsuserName = prop.getProperty("coach.cip.webservice.username");
			wspassword = prop.getProperty("coach.cip.webservice.password");
			employeesUrl = prop.getProperty("coach.cip.webservice.employees.url");
			/*System.out.println("storesUrl: " + storesUrl);
			System.out.println("userName: " + wsuserName);
			System.out.println("password: " + wspassword);
			System.out.println("employeesUrl: " + employeesUrl);
     		 */		
			} catch (IOException e) {
			LoggerUtil.errorLogger(logger, "Caughet Exception While Reading WebService Properties in getOpenStoreByKeyWord method in WebServiceUtil Class.", e);
		}

		LoggerUtil.debugLogger(logger,"GetOpenStoreByKeyWord() Method Starts in WebServiceUtil");
		List<StoreDetailsVO> storeDetaillist = new ArrayList<StoreDetailsVO>();
		SOAPConnectionFactory sfc = SOAPConnectionFactory.newInstance();
		SOAPConnection connection = sfc.createConnection();
		trust();
		SOAPMessage soapMessage = MessageFactory.newInstance().createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		SOAPEnvelope soapEnvelope = soapPart.getEnvelope();

		SOAPHeader soapHeader = soapEnvelope.getHeader();

		SOAPElement security = soapHeader.addChildElement("Security","wsse",
		"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");

		SOAPElement usernameToken = security.addChildElement("UsernameToken","wsse");
		usernameToken.addAttribute(new QName("xmlns:wsu"),
		"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");

		SOAPElement username = usernameToken.addChildElement("Username", "wsse");
		username.addTextNode(wsuserName);

		SOAPElement password = usernameToken.addChildElement("Password", "wsse");
		password.setAttribute("Type","http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
		password.addTextNode(wspassword);
		SOAPBody soapBody = soapEnvelope.getBody();
		Name bodyName = soapEnvelope.createName("GetOpenStoreDescriptionByKeywordRequest", "ns1","http://coach.com/esb/storewebservice/v1.0");

		SOAPBodyElement gltp = soapBody.addBodyElement(bodyName);

		Name storeNum = soapEnvelope.createName("strKeyword");
		SOAPElement childElement = gltp.addChildElement(storeNum);
		childElement.setValue(keywords);
		LoggerUtil.debugLogger(logger,"keywords from WebServiceUtil-----"+keywords);
		LoggerUtil.debugLogger(logger, "StoresWebServiceCall Start in WebServiceUtil-----------");
		URL endpoint = new URL(storesUrl);
		SOAPMessage response = null;
		try{
			response = connection.call(soapMessage, endpoint);
		}catch(Exception ex){
			LoggerUtil.errorLogger(logger, "Exception Caught While Processing GetopenStoreDescriptionByKeyWord() Method in WebServiceUtil", ex);
		}
		if(response != null){
			LoggerUtil.debugLogger(logger, "StoresWebServiceCall End in WebServiceUtil--------"+response);
			storeDetaillist = parseStoreResponseXML(response);
			LoggerUtil.debugLogger(logger, "storeDetaillist.size() from WebServiceUtil-------End--------"+storeDetaillist.size());
		}
		LoggerUtil.debugLogger(logger,"GetOpenStoreByKeyWord() Method Ends in WebServiceUtil");
		return storeDetaillist;
	}
	/**
	 * Helper method to trust certificate.
	 * 
	 * @throws NoSuchAlgorithmException
	 * @throws KeyManagementException
	 */
	public static void trust() throws NoSuchAlgorithmException, KeyManagementException{
		LoggerUtil.debugLogger(logger,"trust() Method Starts in WebServiceUtil");
		Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		TrustManager[] trustAllCerts = new TrustManager[]{
				new X509TrustManager() {
					public X509Certificate[] getAcceptedIssuers() {
						return null;
					}

					public void checkServerTrusted(X509Certificate[] arg0, String arg1)
					throws CertificateException {
						//return;

					}

					public void checkClientTrusted(X509Certificate[] arg0, String arg1)
					throws CertificateException {
						//	return;
					}
				}
		};
		SSLContext sc = SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, new SecureRandom());
		HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());	
		HostnameVerifier hv = new HostnameVerifier(){
			public boolean verify(String urlHostName, SSLSession session) {
				if (!urlHostName.equalsIgnoreCase(session.getPeerHost())) {
					LoggerUtil.debugLogger(logger,"Warning: URL host '" + urlHostName + "' is different to SSLSession host '" + session.getPeerHost() + "'.");
				}
				return true;
			}
		};
		HttpsURLConnection.setDefaultHostnameVerifier(hv);
		LoggerUtil.debugLogger(logger,"trust() Method Ends in WebServiceUtil");
	}

	/**
	 * Helper Method to parse storeResponseXML.
	 *
	 * @param response
	 * @return List<StoreDetailsVO>
	 * @throws Exception
	 */
	private static List<StoreDetailsVO> parseStoreResponseXML(
			SOAPMessage response) throws Exception {
		LoggerUtil.debugLogger(logger,"parseStoreResponseXML() Method Starts in WebServiceUtil");
		List<StoreDetailsVO> storesList = new ArrayList<StoreDetailsVO>();
		StoreDetailsVO storeDetailsVO = null;
		NodeList nodeList = response.getSOAPBody().getFirstChild().getChildNodes();
		if(!(response.getSOAPBody().getFirstChild().getLocalName()).equalsIgnoreCase("Fault") && nodeList != null && nodeList.getLength() > 0){
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				NodeList childNodeList = node.getChildNodes();
				if(childNodeList != null && childNodeList.getLength() > 0){
					boolean add = false;
					for (int j = 0; j < childNodeList.getLength(); j++) {
						storeDetailsVO = new StoreDetailsVO();
						Node childNode = childNodeList.item(j);
						NodeList subChildNodeList = childNode.getChildNodes();
						for (int k = 0; k < subChildNodeList.getLength(); k++) {
							boolean isStoreNumber = false;
							boolean isStoreName = false;
							boolean isGeography = false;
							boolean isStoreType = false;

							boolean isCity = false;
							boolean isState = false;
							boolean isCountry = false;
							boolean isPhone = false;
							boolean isVm = false;
							boolean isDistrictDescription = false;
							boolean isFloor = false;

							Node subChildNode = subChildNodeList.item(k);
							if (null != subChildNode.getNodeName()&& "STORE_NUMBER".equalsIgnoreCase(subChildNode.getNodeName())
									&& subChildNode.getTextContent() != null && !subChildNode.getTextContent().isEmpty()) {
								storeDetailsVO.setStoreNumber(subChildNode.getTextContent());
								isStoreNumber = true;
							}
							if (null != subChildNode.getNodeName()&& "STORE_NAME".equalsIgnoreCase(subChildNode.getNodeName()) 
									&& subChildNode.getTextContent() != null && !subChildNode.getTextContent().isEmpty()) {
								storeDetailsVO.setStoreName(subChildNode.getTextContent());
								isStoreName = true;
							}
							if (null != subChildNode.getNodeName()&& "GEOGRAPHY".equalsIgnoreCase(subChildNode.getNodeName()) 
									&& subChildNode.getTextContent() != null && !subChildNode.getTextContent().isEmpty()) {
								storeDetailsVO.setGeographyName(subChildNode.getTextContent());
								isGeography = true;
							}
							if (null != subChildNode.getNodeName()&& "STORE_TYPE".equalsIgnoreCase(subChildNode.getNodeName()) 
									&& subChildNode.getTextContent() != null && !subChildNode.getTextContent().isEmpty()) {
								storeDetailsVO.setStoreType(subChildNode.getTextContent());
								isStoreType = true;
							}

							if (null != subChildNode.getNodeName()&& "CITY".equalsIgnoreCase(subChildNode.getNodeName()) 
									&& subChildNode.getTextContent() != null && !subChildNode.getTextContent().isEmpty()) {
								storeDetailsVO.setCity(subChildNode.getTextContent());
								isCity = true;
							}
							if (null != subChildNode.getNodeName()&& "STATE".equalsIgnoreCase(subChildNode.getNodeName()) 
									&& subChildNode.getTextContent() != null && !subChildNode.getTextContent().isEmpty()) {
								storeDetailsVO.setState(subChildNode.getTextContent());
								isState = true;
							}
							if (null != subChildNode.getNodeName()&& "COUNTRY".equalsIgnoreCase(subChildNode.getNodeName()) 
									&& subChildNode.getTextContent() != null && !subChildNode.getTextContent().isEmpty()) {
								storeDetailsVO.setCountry(subChildNode.getTextContent());
								isCountry = true;
							}
							if (null != subChildNode.getNodeName()&& "PHONE".equalsIgnoreCase(subChildNode.getNodeName()) 
									&& subChildNode.getTextContent() != null && !subChildNode.getTextContent().isEmpty()) {
								storeDetailsVO.setPhone(subChildNode.getTextContent());
								isPhone = true;
							}
							if (null != subChildNode.getNodeName()&& "VM".equalsIgnoreCase(subChildNode.getNodeName()) 
									&& subChildNode.getTextContent() != null && !subChildNode.getTextContent().isEmpty()) {
								storeDetailsVO.setVm(subChildNode.getTextContent());
								isVm = true;
							}
							if (null != subChildNode.getNodeName()&& "DISTRICT_DESCRIPTION".equalsIgnoreCase(subChildNode.getNodeName()) 
									&& subChildNode.getTextContent() != null && !subChildNode.getTextContent().isEmpty()) {
								storeDetailsVO.setDistrictDescription(subChildNode.getTextContent());
								isDistrictDescription = true;
							}
							if (null != subChildNode.getNodeName()&& "FLOOR".equalsIgnoreCase(subChildNode.getNodeName()) 
									&& subChildNode.getTextContent() != null && !subChildNode.getTextContent().isEmpty()) {
								storeDetailsVO.setFloor(subChildNode.getTextContent());
								isFloor = true;
							}

							if(isStoreNumber || isStoreName || isGeography || isStoreType || isCity || isState ||isCountry || isPhone || isVm || isDistrictDescription || isFloor){
								add = true;
							}
						}
						if(add){
							storesList.add(storeDetailsVO);
						}
					}
				}
			}
		}
		if((response.getSOAPBody().getFirstChild().getLocalName()).equalsIgnoreCase("Fault")){
			throw new Exception();
		}
		LoggerUtil.debugLogger(logger,"parseStoreResponseXML() Method Ends in WebServiceUtil");
		return storesList;
	}


	/**
	 * Helper method to search the employees based on keyword.
	 *
	 * @param keywords
	 * @return List<EmployeeSearchVO>
	 * @throws Exception 
	 */
	@SuppressWarnings("restriction")
	public static List<EmployeeSearchVO> getEmployeesByKeyWord(String keyword)
	throws Exception {
		LoggerUtil.debugLogger(logger,"getEmployeesByKeyWord() Method Starts in WebServiceUtil");

		Properties prop = new Properties();
		String storesUrl = null;
		String wsuserName = null;
		String wspassword = null;
		String employeesUrl = null;
		try {
			String catalinaHome = System.getProperty("catalina.home");
			String webServiceFileName = PropsUtil.get("com.coach.cip.webservice.properties.filename");
			prop.load(new FileInputStream(catalinaHome+"/"+webServiceFileName));
			storesUrl = prop.getProperty("coach.cip.webservice.stores.url");
			wsuserName = prop.getProperty("coach.cip.webservice.username");
			wspassword = prop.getProperty("coach.cip.webservice.password");
			employeesUrl = prop.getProperty("coach.cip.webservice.employees.url");
			/*System.out.println("storesUrl: " + storesUrl);
			System.out.println("userName: " + wsuserName);
			System.out.println("password: " + wspassword);
			System.out.println("employeesUrl: " + employeesUrl);*/
		} catch (IOException e) {
			LoggerUtil.errorLogger(logger, "Caughet Exception While Reading WebService Properties in getEmployeesByKeyWord method in WebServiceUtil Class.", e);
		}

		SOAPConnectionFactory sfc = SOAPConnectionFactory.newInstance();
		SOAPConnection connection = sfc.createConnection();
		trust();
		SOAPMessage soapMessage = MessageFactory.newInstance().createMessage();
		SOAPPart soapPart = soapMessage.getSOAPPart();

		SOAPEnvelope soapEnvelope = soapPart.getEnvelope();

		SOAPHeader soapHeader = soapEnvelope.getHeader();

		SOAPElement security = soapHeader
		.addChildElement(
				"Security",
				"wsse",
		"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd");

		SOAPElement usernameToken = security.addChildElement("UsernameToken",
		"wsse");
		usernameToken
		.addAttribute(
				new QName("xmlns:wsu"),
		"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");

		SOAPElement username = usernameToken
		.addChildElement("Username", "wsse");
		username.addTextNode(wsuserName);

		SOAPElement password = usernameToken
		.addChildElement("Password", "wsse");
		password.setAttribute(
				"Type",
		"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
		password.addTextNode(wspassword);

		URL endpoint = new URL(employeesUrl);		

		String firstName = Constants.EMPTY;
		String lastName = Constants.EMPTY;

		StringTokenizer keywordString = new StringTokenizer(keyword);
		if(keywordString.hasMoreTokens()) {

			firstName = keywordString.nextToken();	
		}
		if(keywordString.hasMoreTokens()) {

			lastName = keywordString.nextToken();
		}

		SOAPMessage response = null;

		/** Employee details search based on First Name and Last Name. */
		if(firstName != null && !firstName.isEmpty() && lastName != null && !lastName.isEmpty()) {

			response = getEmployeeDetailsByNameSearch(firstName, lastName,
					connection, soapMessage, soapEnvelope, endpoint);
		}		 	
		/** Employee details search based on keyword. */
		else if(firstName != null && !firstName.isEmpty() && (lastName == null || lastName.isEmpty())) {

			keyword = firstName;
			response = getEmployeeDetailsByKeywordSearch(keyword,
					connection, soapMessage, soapEnvelope, endpoint);
		}else {

			LoggerUtil.infoLogger(logger,"Please enter a search keyword.");
		}
		List<EmployeeSearchVO> employeeDetailsList = null;

		if(response != null) {

			employeeDetailsList = parseEmployeesResponseXML(response);
			Collections.sort(employeeDetailsList);
		}

		LoggerUtil.debugLogger(logger,"getEmployeesByKeyWord() Method Ends in WebServiceUtil");
		return employeeDetailsList;

	}

	/**
	 * Get the employee details based on keyword search.
	 * @param keyword
	 * @param connection
	 * @param soapMessage
	 * @param soapEnvelope
	 * @param endpoint 
	 * @return
	 * @throws SOAPException
	 */
	@SuppressWarnings("restriction")
	private static SOAPMessage getEmployeeDetailsByKeywordSearch(
			String keyword, SOAPConnection connection, SOAPMessage soapMessage,
			SOAPEnvelope soapEnvelope, URL endpoint)
	throws SOAPException {

		SOAPBody soapBody = soapEnvelope.getBody();		
		Name bodyName = soapEnvelope.createName("GetEmployeesByKeywordRequest",
				"ns2", "http://coach.com/esb/employeeWebService/v1.0");
		SOAPBodyElement gltp = soapBody.addBodyElement(bodyName);
		Name childName = soapEnvelope.createName("EMP_ByKeyword", "",
		"http://coach.com/esb/employeeWebService/v1.0");
		SOAPElement childElement = gltp.addChildElement(childName);
		Name userID = soapEnvelope.createName("strKeyword");
		SOAPElement childChildElement = childElement.addChildElement(userID);
		keyword = keyword + "*";
		childChildElement.setValue(keyword);		
		SOAPMessage response = null;
		try {

			response = connection.call(soapMessage, endpoint);
		} catch (Exception ex) {
			LoggerUtil.errorLogger(logger, ex.getMessage(), ex);
		}
		connection.close();
		return response;
	}

	/**
	 * Get the employee details based on name search.
	 * @param keyword
	 * @param connection
	 * @param soapMessage
	 * @param soapEnvelope
	 * @param endpoint 
	 * @return
	 * @throws SOAPException
	 */
	@SuppressWarnings("restriction")
	private static SOAPMessage getEmployeeDetailsByNameSearch(
			String firstName, String lastName, SOAPConnection connection, SOAPMessage soapMessage,
			SOAPEnvelope soapEnvelope, URL endpoint)
	throws SOAPException {

		SOAPBody soapBody = soapEnvelope.getBody();
		Name bodyName = soapEnvelope.createName(
				"GetEmployeesByNameRequest", "ns2",
		"http://coach.com/esb/employeeWebService/v1.0");
		SOAPBodyElement gltp = soapBody.addBodyElement(bodyName);		
		Name childName = soapEnvelope.createName("EMP_ByName", "",
		"http://coach.com/esb/employeeWebService/v1.0");
		SOAPElement childElement = gltp.addChildElement(childName);		
		firstName = firstName + "*";
		lastName = lastName + "*";
		Name firstNameId = soapEnvelope.createName("strFirstName");		
		SOAPElement fistChildChildElement = childElement.addChildElement(firstNameId);		
		fistChildChildElement.setValue(firstName);
		Name lastNameId = soapEnvelope.createName("strLastName");		
		SOAPElement secondChildElement = childElement.addChildElement(lastNameId);
		secondChildElement.setValue(lastName);		
		SOAPMessage response = null;
		try {

			response = connection.call(soapMessage, endpoint);
		} catch (Exception ex) {
			LoggerUtil.errorLogger(logger, ex.getMessage(), ex);
		}
		connection.close();
		return response;
	}

	/**
	 * Helper method to  parse Employees XML Response. 
	 * @param response
	 * @return
	 * @throws Exception
	 */
	private static List<EmployeeSearchVO> parseEmployeesResponseXML(
			SOAPMessage response) throws Exception {
		LoggerUtil.debugLogger(logger,"parseEmployeesResponseXML() Method Starts in WebServiceUtil");
		List<EmployeeSearchVO> employeeList = new ArrayList<EmployeeSearchVO>();
		EmployeeSearchVO employeeSearchVO = null;
		NodeList nodeList = response.getSOAPBody().getFirstChild().getChildNodes();
		if(!(response.getSOAPBody().getFirstChild().getLocalName()).equalsIgnoreCase("Fault") && nodeList != null && nodeList.getLength() > 0){
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);

				NodeList childNodeList = node.getChildNodes();
				if(childNodeList != null && childNodeList.getLength() > 0){					
					for (int j = 0; j < childNodeList.getLength(); j++) {
						employeeSearchVO = new EmployeeSearchVO();
						Node childNode = childNodeList.item(j);
						NodeList subChildNodeList = childNode.getChildNodes();
						if(subChildNodeList != null && subChildNodeList.getLength() > 0) {

							for (int k = 0; k < subChildNodeList.getLength(); k++) {

								Node subChildNode = subChildNodeList.item(k);
								if (null != subChildNode.getNodeName()&& "FIRST_NAME".equalsIgnoreCase(subChildNode.getNodeName()) 
										&& subChildNode.getTextContent() != null && !subChildNode.getTextContent().isEmpty()) {
									employeeSearchVO.setFirstName(subChildNode.getTextContent());
								}
								if (null != subChildNode.getNodeName()&& "LAST_NAME".equalsIgnoreCase(subChildNode.getNodeName()) 
										&& subChildNode.getTextContent() != null && !subChildNode.getTextContent().isEmpty()) {
									employeeSearchVO.setLastName(subChildNode.getTextContent());
								}
								if (null != subChildNode.getNodeName()&& "USERID".equalsIgnoreCase(subChildNode.getNodeName()) 
										&& subChildNode.getTextContent() != null && !subChildNode.getTextContent().isEmpty()) {
									employeeSearchVO.setEmpId(subChildNode.getTextContent());
								}
								if (null != subChildNode.getNodeName()&& "DEPARTMENT".equalsIgnoreCase(subChildNode.getNodeName()) 
										&& subChildNode.getTextContent() != null && !subChildNode.getTextContent().isEmpty()) {
									employeeSearchVO.setDepartment(subChildNode.getTextContent());	
								}		

								if (null != subChildNode.getNodeName()&& "TITLE".equalsIgnoreCase(subChildNode.getNodeName()) 
										&& subChildNode.getTextContent() != null && !subChildNode.getTextContent().isEmpty()) {
									employeeSearchVO.setTitle(subChildNode.getTextContent());	
								}	
								if (null != subChildNode.getNodeName()&& "PHONE".equalsIgnoreCase(subChildNode.getNodeName()) 
										&& subChildNode.getTextContent() != null && !subChildNode.getTextContent().isEmpty()) {
									employeeSearchVO.setPhone(subChildNode.getTextContent());	
								}	
								if (null != subChildNode.getNodeName()&& "EMAIL".equalsIgnoreCase(subChildNode.getNodeName()) 
										&& subChildNode.getTextContent() != null && !subChildNode.getTextContent().isEmpty()) {
									employeeSearchVO.setEmail(subChildNode.getTextContent());	
								}	
								if (null != subChildNode.getNodeName()&& "OFFICE_NAME".equalsIgnoreCase(subChildNode.getNodeName()) 
										&& subChildNode.getTextContent() != null && !subChildNode.getTextContent().isEmpty()) {
									employeeSearchVO.setOffice(subChildNode.getTextContent());	
								}	
								if (null != subChildNode.getNodeName()&& "PHONE_EXTN".equalsIgnoreCase(subChildNode.getNodeName()) 
										&& subChildNode.getTextContent() != null && !subChildNode.getTextContent().isEmpty()) {
									employeeSearchVO.setPhoneextn(subChildNode.getTextContent());	
								}	

							}						

							employeeList.add(employeeSearchVO);

						}
					}
				}
			}
		}

		if((response.getSOAPBody().getFirstChild().getLocalName()).equalsIgnoreCase("Fault")){
			throw new Exception();
		}
		//employeeList = new ArrayList<EmployeeSearchVO>(employeeSet);
		LoggerUtil.debugLogger(logger,"parseEmployeesResponseXML() Method Ends in WebServiceUtil");
		return employeeList;
	}

	/**
	 * Method used to get all LayOuts.
	 * @param groupId
	 * @return Map<String, Layout>
	 */
	public static Map<String, Layout> getLayouts(long groupId) {
		LoggerUtil.debugLogger(logger,"getLayouts() Method Starts in WebServiceUtil");
		List<Layout> layoutList = null;
		Map<Long, Layout> layoutIdMap = null;
		Map<String, Layout> layoutMap = new HashMap<String, Layout>();
		try 
		{
			layoutList = new ArrayList<Layout>(LayoutLocalServiceUtil.getLayouts(groupId, true));
			CoachComparator.sort(layoutList, "getLayoutId", true);
			//layoutMap = new HashMap<String, Layout>();
			layoutIdMap = new HashMap<Long, Layout>();
			for (Layout layout : layoutList) {
				layoutIdMap.put(layout.getLayoutId(), layout);
			}
			Locale defaultLocale = LocaleUtil.getDefault();
			for (Layout layout : layoutList) {
				StringBuilder layoutSB = new StringBuilder();
				layoutSB.append(layout.getName(defaultLocale).trim());
				if (layout.getParentLayoutId() == 0) {
					layoutMap.put(layoutSB.toString().trim().toUpperCase(), layout);
				}
				else if (layoutIdMap.containsKey(layout.getParentLayoutId())) {
					layoutSB.append("-");
					layoutSB.append(layoutIdMap.get(layout.getParentLayoutId()).getName(defaultLocale).trim());
					layoutMap.put(layoutSB.toString().trim().toUpperCase(), layout);
				}
			}
		}
		catch (SystemException e) {
			LoggerUtil.errorLogger(logger,"SystemException Caught while Processing getlayOuts() Method in WebServiceUtil Class.", e);
		}
		catch (Exception e) {
			LoggerUtil.errorLogger(logger, "Exception Caught while Processing getlayOuts() Method in WebServiceUtil Class.", e);
		}

		LoggerUtil.debugLogger(logger,"getLayouts() Method Ends in WebServiceUtil");
		return layoutMap;
	}

	/**
	 * Returns the Asset Entry URL.
	 * @param classPK
	 * @param renderRequest
	 * @param renderResponse
	 * @param themeDisplay
	 * @return String
	 */
	public static String displayFileURL(long classPK, RenderRequest renderRequest, RenderResponse renderResponse, ThemeDisplay themeDisplay) {
		LoggerUtil.debugLogger(logger,"displayFileURL() Method Starts in WebServiceUtil");
		String fileURL = "";
		try {

			FileEntry fileEntry = getFileEntry(classPK, renderRequest, renderResponse);
			if (hasPermission(renderRequest, fileEntry)) {
				fileURL = DLUtil.getPreviewURL(fileEntry, fileEntry.getFileVersion(), themeDisplay, StringPool.BLANK);
			}
			else {
				fileURL = "no-user-privilege";
			}

		}
		catch (SystemException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.displayFileURL() SystemException :::", e);
		}
		catch (Exception e) {
			LoggerUtil.errorLogger(logger, "WebUtil.displayFileURL() Exception :::", e);
		}
		LoggerUtil.debugLogger(logger,"displayFileURL() Method Ends in WebServiceUtil");
		return fileURL;
	}

	/**
	 * Returns the FileEntry Object based on ClassPK.
	 * @param classPK
	 * @param request
	 * @param response
	 * @return
	 * @throws Exception
	 */
	public static FileEntry getFileEntry(long classPK, RenderRequest request, RenderResponse response) throws Exception {
		LoggerUtil.debugLogger(logger,"getFileEntry() Method Starts in WebServiceUtil");
		AssetRendererFactory assetRendererFactory =
			AssetRendererFactoryRegistryUtil.getAssetRendererFactoryByClassName(DLFileEntryConstants.getClassName());
		FileEntry fileEntry = null;
		AssetRenderer assetRenderer;
		try {
			assetRenderer = assetRendererFactory.getAssetRenderer(classPK);
			assetRenderer.render(request, response, AssetRenderer.TEMPLATE_FULL_CONTENT);

			FileVersion fileVersion = (FileVersion) request.getAttribute("DOCUMENT_LIBRARY_FILE_VERSION");
			fileEntry = fileVersion.getFileEntry();
		}
		catch (PortalException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.getFileEntry() PortalException :::", e);
		}
		catch (SystemException e) {
			LoggerUtil.errorLogger(logger, "WebUtil.getFileEntry() SystemException :::", e);
		}
		LoggerUtil.debugLogger(logger,"getFileEntry() Method Ends in WebServiceUtil");
		return fileEntry;
	}

	public static boolean hasPermission(PortletRequest request, FileEntry fileEntry) {
		LoggerUtil.debugLogger(logger,"hasPermission() Method Starts in WebServiceUtil");
		boolean hasPermission = false;
		try {
			hasPermission = fileEntry.containsPermission(getThemeDisplay(request).getPermissionChecker(), ActionKeys.VIEW);
		}
		catch (PortalException e) {
			logger.error(e);
		}
		catch (SystemException e) {
			logger.error(e);
		}
		LoggerUtil.debugLogger(logger,"hasPermission() Method Ends in WebServiceUtil"+hasPermission);

		return hasPermission;
	}

	/**
	 * Returns the ThemeDisplay associated with the community that is hosting
	 * the portlet associated with the current JSF FacesContext.
	 * @param actionRequest
	 *            the action request
	 * @return the theme display
	 */
	public static ThemeDisplay getThemeDisplay(PortletRequest actionRequest) {
		LoggerUtil.debugLogger(logger,"ThemeDisplay() Method Starts in WebServiceUtil");
		ThemeDisplay themeDisplay = (ThemeDisplay) actionRequest.getAttribute(com.liferay.portal.kernel.util.WebKeys.THEME_DISPLAY);
		LoggerUtil.debugLogger(logger,"ThemeDisplay() Method Ends in WebServiceUtil---"+themeDisplay);
		return themeDisplay;
	}

	/**
	 * Fetching Asset Category based on Asset Category Name.
	 * 
	 * @param name
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static List<AssetCategory> fetchAssetCategoryByName(
			String childName, String parentName, Locale locale) {
		LoggerUtil.debugLogger(logger,"fetchAssetCategoryByName() Method Starts in WebServiceUtil");
		List<AssetCategory> allCategoryList = null;
		List<AssetCategory> categoryList = new ArrayList<AssetCategory>();
		try {
			DynamicQuery query = DynamicQueryFactoryUtil
			.forClass(AssetCategory.class,
					PortalClassLoaderUtil.getClassLoader());
			Criterion criterion = RestrictionsFactoryUtil.eq("name", childName);
			query.add(criterion);
			allCategoryList = (List<AssetCategory>) AssetCategoryLocalServiceUtil
			.dynamicQuery(query);
			if (parentName != null && !parentName.isEmpty()) {
				for (AssetCategory assetCategory : allCategoryList) {
					try {
						if (assetCategory.getParentCategoryId() != 0 && 
								AssetCategoryLocalServiceUtil.getCategory(assetCategory.getParentCategoryId()).getTitle(locale).equals(parentName)) {
							categoryList.add(assetCategory);
						} else if (assetCategory.getParentCategoryId() == 0 && 
								AssetVocabularyLocalServiceUtil.getAssetVocabulary(assetCategory.getVocabularyId()).getTitle(locale).equals(parentName)) {
							categoryList.add(assetCategory);
						}
					} catch (PortalException e) {
						LoggerUtil
						.errorLogger(
								logger,
								"WebUtil.fetchAssetCategoryByName() PortalException :::",
								e);
					}
				}
			}
		} catch (SystemException e) {
			LoggerUtil
			.errorLogger(
					logger,
					"WebUtil.fetchAssetCategoryByName() SystemException :::",
					e);
		}
		LoggerUtil.debugLogger(logger,"fetchAssetCategoryByName() Method Starts in WebServiceUtil");
		return categoryList;
	}


	/**
	 * Fetching Child Asset Categories based on Parent Category Id.
	 * 
	 * @param parentCategoryId
	 * @return
	 * @throws SystemException
	 */
	public static List<AssetCategory> getChildAssetCategories(
			long parentCategoryId) throws SystemException {
		LoggerUtil.debugLogger(logger,"getChildAssetCategories() Method Starts in WebServiceUtil");
		List<AssetCategory> childCategoryList = null;
		childCategoryList = AssetCategoryLocalServiceUtil
		.getChildCategories(parentCategoryId);
		LoggerUtil.debugLogger(logger,"getChildAssetCategories() Method Ends in WebServiceUtil");
		return childCategoryList;
	}


	/**
	 * Helper Method to convert text to CamelCaseText.
	 * @param text
	 * @return CamelCaseText string
	 */
	public static String convertCamelCase(String text) {

		LoggerUtil.debugLogger(logger, "Start of converCamelCase() method of WebServiceUtil::");
		StringBuffer strbufCamelCase = new StringBuffer();
		StringTokenizer st = new StringTokenizer(text);
		if (null == text || text.length() == 0) {
			return "";
		}
		while (st.hasMoreTokens()) {
			String strWord = st.nextToken();
			strbufCamelCase.append(strWord.substring(0, 1).toUpperCase());
			if (strWord.length() > 1) {
				strbufCamelCase.append(strWord.substring(1).toLowerCase());
			}
			strbufCamelCase.append(" ");
		}
		LoggerUtil.debugLogger(logger, "End of converCamelCase() method of WebServiceUtil::");
		return strbufCamelCase.toString().trim();
	}

	/**
	 * Helper Method to check whether the user belongs to CIP or not.
	 * @param themeDisplay
	 * @return
	 */
	public static boolean isCIPOrganization(ThemeDisplay themeDisplay){
		String OrganizationName = PropsUtil.get(Constants.COACH_INTRANET_PORTAL_ORGANIZATION_NAME);
		try 
		{
			LoggerUtil.debugLogger(logger,"WebServiceUtil.isCIPOrganization() OrganizationName from Properties :::"+OrganizationName);
			LoggerUtil.debugLogger(logger,"WebServiceUtil.isCIPOrganization() Current Organization :::"+themeDisplay.getScopeGroupName());
			if(Validator.isNotNull(OrganizationName) &&
					themeDisplay.getScopeGroupName().equalsIgnoreCase(OrganizationName))
			{
				return true;
			}else{
				return false;
			}
		} catch (PortalException e) {
			LoggerUtil.errorLogger(logger,"SideNavigationController.isCIPOrganization() PortalException :::",e);
		} catch (SystemException e) {
			LoggerUtil.errorLogger(logger,"SideNavigationController.isCIPOrganization() SystemException :::",e);
		}
		return false;
	}	

	/**
	 * Helper Method to check whether the user has privilege to view the Category. 
	 * @param permissionChecker
	 * @param category
	 * @param actionId
	 * @return
	 */
	public static boolean hasCategoryAccess(PermissionChecker permissionChecker, AssetCategory category, String actionId) {

		//PermissionChecker permissionChecker = getPermissionChecker(portalRequest);
		List<AssetCategory> categoryList = new ArrayList<AssetCategory>();
		fetchRecursiveParentCategories(category, categoryList);
		boolean isCategoryAccess = false;
		for(AssetCategory assetCategory: categoryList)	{ 
			isCategoryAccess = false;

			if (permissionChecker.hasOwnerPermission(
					assetCategory.getCompanyId(), AssetCategory.class.getName(), assetCategory.getCategoryId(), assetCategory.getUserId(), actionId)) {
				isCategoryAccess = true;
			}
			//LoggerUtil.debugLogger(logger, "End of hasCategoryAccess() method of WebUtil::");
			if( permissionChecker.hasPermission(assetCategory.getGroupId(), AssetCategory.class.getName(), assetCategory.getCategoryId(), actionId)){
				isCategoryAccess = true;
			}
			if(!isCategoryAccess)
			{
				return false;
			}
		}	
		return true;
	}

	/**
	 * Helper method to fetch all the parent Categories for an Assest.
	 * @param selectedAssetCategory
	 * @param categoryList
	 */
	public static void  fetchRecursiveParentCategories(AssetCategory selectedAssetCategory, List<AssetCategory> categoryList){
		AssetCategory assetCategory = null;
		//List<AssetCategory> categoryList = new ArrayList<AssetCategory>();
		if(!categoryList.contains(selectedAssetCategory))
		{
			categoryList.add(selectedAssetCategory);
		}
		try {
			if(selectedAssetCategory.getParentCategoryId() != 0)
			{
				assetCategory = AssetCategoryLocalServiceUtil.getAssetCategory(selectedAssetCategory.getParentCategoryId());
				categoryList.add(assetCategory);
				if(assetCategory.getParentCategoryId() != 0){
					fetchRecursiveParentCategories(assetCategory,categoryList);
				}
			} 
		} 
		catch (PortalException e) {
			e.printStackTrace();
		}
		catch (SystemException e) {
			e.printStackTrace();
		}
		//return categoryList;
	}

	/**
	 * web service test method.
	 * @param keywords
	 * @return
	 * @throws Exception
	 */
	public static List<StoreDetailsVO> getOpenStoreByKeyWordExample(String keywords)throws Exception {

		List<StoreDetailsVO> storesList = new ArrayList<StoreDetailsVO>();
		StoreDetailsVO store1 = new StoreDetailsVO();
		store1.setStoreName("storename1");
		store1.setStoreNumber("storenumber1");
		store1.setStoreType("storetype1");
		store1.setCountry("country1");
		store1.setState("state1");
		store1.setCity("city1");
		store1.setGeographyName("geography1");
		store1.setDistrictDescription("district desc1");
		store1.setFloor("floor1");
		store1.setPhone("121212121");

		StoreDetailsVO store2 = new StoreDetailsVO();
		store2.setStoreName("storename2");
		store2.setStoreNumber("storenumber2");
		store2.setStoreType("storetype2");
		store2.setCountry("country2");
		store2.setState("state2");
		store2.setCity("city2");
		store2.setGeographyName("geography2");
		store2.setDistrictDescription("district desc2");
		store2.setFloor("floor2");
		store2.setPhone("3434343434");

		storesList.add(store1);
		storesList.add(store2);
		return storesList;

	}


	public static boolean isTransmittal(JournalArticle journalArticle ){

		if (journalArticle != null && journalArticle.getStructureId() != null && journalArticle.getStructureId().equalsIgnoreCase("TRANSMITTAL"))
		{
			return true;
		}
		return false;

	}
	
	public static boolean hasTransmittalPermission(JournalArticle journalArticle , long userId){
		boolean hasTransmittalPermission = false;
		try{
			String articleId = journalArticle.getArticleId();
			Transmittal transmittal = TransmittalLocalServiceUtil.getTransmittal(articleId);
			String[] storeUserIds = transmittal.getStoreUsers().split(",");
			String[] corpUserIds = transmittal.getCorporateUsers().split(",");
			if(ArrayUtil.contains(storeUserIds, "" + userId) || ArrayUtil.contains(corpUserIds, "" + userId)){
				hasTransmittalPermission = true;
			}
			}catch(Exception e){
				e.printStackTrace();
			}
		return hasTransmittalPermission;

	}
	
}
