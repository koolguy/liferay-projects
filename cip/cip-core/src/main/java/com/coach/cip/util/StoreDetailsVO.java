package com.coach.cip.util;

/**
 * Class is used to capture the characteristics of StoreDetailVO.
 * 
 * @author GalaxE.
 *
 */
public class StoreDetailsVO {

	private String storeNumber;
	private String storeName;
	private String geographyName;
	private String storeType;
	private String city;
	private String state;
	private String country;
	private String phone;
	private String vm;
	private String districtDescription;
	private String floor;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getVm() {
		return vm;
	}

	public void setVm(String vm) {
		this.vm = vm;
	}

	public String getDistrictDescription() {
		return districtDescription;
	}

	public void setDistrictDescription(String districtDescription) {
		this.districtDescription = districtDescription;
	}

	public String getFloor() {
		return floor;
	}

	public void setFloor(String floor) {
		this.floor = floor;
	}

	public String getStoreType() {
		return storeType;
	}

	public void setStoreType(String storeType) {
		this.storeType = storeType;
	}

	public String getGeographyName() {
		return geographyName;
	}

	public void setGeographyName(String geographyName) {
		this.geographyName = geographyName;
	}

	public String getStoreNumber() {
		return storeNumber;
	}

	public void setStoreNumber(String storeNumber) {
		this.storeNumber = storeNumber;
	}

	public String getStoreName() {
		return storeName;
	}

	public void setStoreName(String storeName) {
		this.storeName = storeName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime
				* result
				+ ((districtDescription == null) ? 0 : districtDescription
						.hashCode());
		result = prime * result + ((floor == null) ? 0 : floor.hashCode());
		result = prime * result
				+ ((geographyName == null) ? 0 : geographyName.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result
				+ ((storeName == null) ? 0 : storeName.hashCode());
		result = prime * result
				+ ((storeNumber == null) ? 0 : storeNumber.hashCode());
		result = prime * result
				+ ((storeType == null) ? 0 : storeType.hashCode());
		result = prime * result + ((vm == null) ? 0 : vm.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StoreDetailsVO other = (StoreDetailsVO) obj;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (districtDescription == null) {
			if (other.districtDescription != null)
				return false;
		} else if (!districtDescription.equals(other.districtDescription))
			return false;
		if (floor == null) {
			if (other.floor != null)
				return false;
		} else if (!floor.equals(other.floor))
			return false;
		if (geographyName == null) {
			if (other.geographyName != null)
				return false;
		} else if (!geographyName.equals(other.geographyName))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (storeName == null) {
			if (other.storeName != null)
				return false;
		} else if (!storeName.equals(other.storeName))
			return false;
		if (storeNumber == null) {
			if (other.storeNumber != null)
				return false;
		} else if (!storeNumber.equals(other.storeNumber))
			return false;
		if (storeType == null) {
			if (other.storeType != null)
				return false;
		} else if (!storeType.equals(other.storeType))
			return false;
		if (vm == null) {
			if (other.vm != null)
				return false;
		} else if (!vm.equals(other.vm))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "StoreDetailsVO [storeNumber=" + storeNumber + ", storeName="
				+ storeName + ", geographyName=" + geographyName
				+ ", storeType=" + storeType + ", city=" + city + ", state="
				+ state + ", country=" + country + ", phone=" + phone + ", vm="
				+ vm + ", districtDescription=" + districtDescription
				+ ", floor=" + floor + "]";
	}

	/*@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((geographyName == null) ? 0 : geographyName.hashCode());
		result = prime * result
				+ ((storeName == null) ? 0 : storeName.hashCode());
		result = prime * result
				+ ((storeNumber == null) ? 0 : storeNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StoreDetailsVO other = (StoreDetailsVO) obj;
		if (geographyName == null) {
			if (other.geographyName != null)
				return false;
		} else if (!geographyName.equals(other.geographyName))
			return false;
		if (storeName == null) {
			if (other.storeName != null)
				return false;
		} else if (!storeName.equals(other.storeName))
			return false;
		if (storeNumber == null) {
			if (other.storeNumber != null)
				return false;
		} else if (!storeNumber.equals(other.storeNumber))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "StoreDetailsVO [storeNumber=" + storeNumber + ", storeName="
				+ storeName + ", geographyName=" + geographyName + "]";
	}*/
}
