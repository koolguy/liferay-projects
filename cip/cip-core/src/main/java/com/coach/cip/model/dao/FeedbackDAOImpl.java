package com.coach.cip.model.dao;

import org.springframework.stereotype.Repository;

import com.coach.cip.model.entity.CIFeedback;
import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * Class provides the implementations for FeedbackDAO's to perform DML
 * operations.
 *
 * @author GalaxE.
 */
@Repository("feedbackDAO")
public class FeedbackDAOImpl extends AbstractDAO<CIFeedback> implements
		FeedbackDAO {

	/**
	 * The log instance for FeedbackDAOImpl.
	 */
	private static final Log logger = LogFactoryUtil
			.getLog(FeedbackDAOImpl.class);

	/**
	 * This method is used to add the feedback.
	 *
	 * @param cifeedback
	 */
	public void addFeedback(CIFeedback cifeedback) {
		try {
			saveOrUpdate(cifeedback);
			LoggerUtil.debugLogger(logger, "Successfully added the feedback.");
		} catch (Exception e) {
			LoggerUtil.errorLogger(logger,
					"Exception raised while adding the feedback: ", e);
		}
	}

}
