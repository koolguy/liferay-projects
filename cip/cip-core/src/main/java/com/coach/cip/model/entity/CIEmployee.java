
package com.coach.cip.model.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Class is used to capture the characteristics of ci_employee. 
 * This is reusable class for ci_employee.
 * @author GalaxE.
 */
@Entity
@Table(name = "ci_employee")
public class CIEmployee implements java.io.Serializable {

		
	private static final long serialVersionUID = -2828861827483771607L;
	// Fields

	private Long empId;
	private User userBySupervisorId;
	private User userByAdminAssistId;
	private User userByUserId;
	private String employeeType;
	private String employmentType;
	private String employeeStatus;
	private Boolean leadershipIndicator;
	private Boolean enrollmentBenefitIndicator;
	private String department;
	private Timestamp hireDate;

	// Constructors

	/** default constructor */
	public CIEmployee() {

	}

	/** minimal constructor */
	public CIEmployee(
		Long empId, User userByUserId, String employmentType, String employeeStatus, Boolean leadershipIndicator, Boolean enrollmentBenefitIndicator,
		String department) {

		this.empId = empId;
		this.userByUserId = userByUserId;
		this.employmentType = employmentType;
		this.employeeStatus = employeeStatus;
		this.leadershipIndicator = leadershipIndicator;
		this.enrollmentBenefitIndicator = enrollmentBenefitIndicator;
		this.department = department;
	}

	/** full constructor */
	public CIEmployee(
		Long empId, User userBySupervisorId, User userByAdminAssistId, User userByUserId, String employeeType, String employmentType,
		String employeeStatus, Boolean leadershipIndicator, Boolean enrollmentBenefitIndicator, String department, Timestamp hireDate) {

		this.empId = empId;
		this.userBySupervisorId = userBySupervisorId;
		this.userByAdminAssistId = userByAdminAssistId;
		this.userByUserId = userByUserId;
		this.employeeType = employeeType;
		this.employmentType = employmentType;
		this.employeeStatus = employeeStatus;
		this.leadershipIndicator = leadershipIndicator;
		this.enrollmentBenefitIndicator = enrollmentBenefitIndicator;
		this.department = department;
		this.hireDate = hireDate;
	}

	// Property accessors
	/**
	 * Getter method for empId
	 * @return empId
	 */
	@Id
	@Column(name = "empId", unique = true, nullable = false)
	public Long getEmpId() {

		return this.empId;
	}

	/**
	 * Setter method for empId
	 * @param empId
	 */
	public void setEmpId(Long empId) {

		this.empId = empId;
	}

	/**
	 * Getter method for UserBySupervisorId
	 * @return userBySupervisorId of type User
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "supervisorId")
	public User getUserBySupervisorId() {

		return this.userBySupervisorId;
	}

	/**
	 * Setter method for userBySupervisorId
	 * @param userBySupervisorId
	 */
	public void setUserBySupervisorId(User userBySupervisorId) {

		this.userBySupervisorId = userBySupervisorId;
	}

	/**
	 * Getter method for userByAdminAssistId
	 * @return userByAdminAssistId of type User
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "adminAssistId")
	public User getUserByAdminAssistId() {

		return this.userByAdminAssistId;
	}

	/**
	 * Setter method for userByAdminAssistId
	 * @param userByAdminAssistId
	 */
	public void setUserByAdminAssistId(User userByAdminAssistId) {

		this.userByAdminAssistId = userByAdminAssistId;
	}

	/**
	 * Getter method for userByUserId
	 * @return userByUserId of type User
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "userId", nullable = false)
	public User getUserByUserId() {

		return this.userByUserId;
	}

	/**
	 * Setter method for userByUserId
	 * @param userByUserId
	 */
	public void setUserByUserId(User userByUserId) {

		this.userByUserId = userByUserId;
	}

	/**
	 * Getter method for employeeType
	 * @return employeeType String
	 */
	@Column(name = "employeeType", length = 25)
	public String getEmployeeType() {

		return this.employeeType;
	}

	/**
	 * Setter method for employeeType
	 * @param employeeType
	 */
	public void setEmployeeType(String employeeType) {

		this.employeeType = employeeType;
	}

	/**
	 * Getter method for employmentType
	 * @return employmentType String
	 */
	@Column(name = "employmentType", nullable = false, length = 50)
	public String getEmploymentType() {

		return this.employmentType;
	}

	/**
	 * Setter method for employmentType
	 * @param employmentType
	 */
	public void setEmploymentType(String employmentType) {

		this.employmentType = employmentType;
	}

	/**
	 * Getter method for employeeStatus
	 * @return employeeStatus String
	 */
	@Column(name = "employeeStatus", nullable = false, length = 25)
	public String getEmployeeStatus() {

		return this.employeeStatus;
	}

	/**
	 * Setter method for employeeStatus
	 * @param employeeStatus
	 */
	public void setEmployeeStatus(String employeeStatus) {

		this.employeeStatus = employeeStatus;
	}

	/**
	 * Getter method for leadershipIndicator
	 * @return leadershipIndicator
	 */
	@Column(name = "leadershipIndicator", nullable = false)
	public Boolean getLeadershipIndicator() {

		return this.leadershipIndicator;
	}

	/**
	 * Setter method for leadershipIndicator
	 * @param leadershipIndicator
	 */
	public void setLeadershipIndicator(Boolean leadershipIndicator) {

		this.leadershipIndicator = leadershipIndicator;
	}

	/**
	 * Getter method for enrollmentBenefitIndicator
	 * @return enrollmentBenefitIndicator
	 */
	@Column(name = "enrollmentBenefitIndicator", nullable = false)
	public Boolean getEnrollmentBenefitIndicator() {

		return this.enrollmentBenefitIndicator;
	}

	/**
	 * Setter method for enrollmentBenefitIndicator
	 * @param enrollmentBenefitIndicator
	 */
	public void setEnrollmentBenefitIndicator(Boolean enrollmentBenefitIndicator) {

		this.enrollmentBenefitIndicator = enrollmentBenefitIndicator;
	}

	/**
	 * Getter method for department
	 * @return department String
	 */
	@Column(name = "department", nullable = false, length = 100)
	public String getDepartment() {

		return this.department;
	}

	/**
	 * Setter method for department
	 * @param department
	 */
	public void setDepartment(String department) {

		this.department = department;
	}

	/**
	 * Getter method for hireDate
	 * @return hireDate of type Timestamp
	 */
	@Column(name = "hireDate", length = 0)
	public Timestamp getHireDate() {

		return this.hireDate;
	}

	/**
	 * Setter method for hireDate
	 * @param hireDate
	 */
	public void setHireDate(Timestamp hireDate) {

		this.hireDate = hireDate;
	}

}
