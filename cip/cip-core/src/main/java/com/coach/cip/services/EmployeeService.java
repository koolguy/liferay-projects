
package com.coach.cip.services;

import java.util.List;
import com.coach.cip.model.entity.CIEmployee;

/**
 * Service interface for the Employee.
 * @author GalaxE.
 */
public interface EmployeeService {

	/**
	 * Adds Employee
	 * @param employee
	 * @return Employee which is added by the method
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 */
	public CIEmployee addEmployee(CIEmployee employee) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	 * Creates Employee
	 * @param userId
	 * @return Employee which is created by the method
	 */
	public CIEmployee createEmployee(long userId);

	/**
	 * Deletes Employee
	 * @param employee
	 * @return Employee which is deleted by the method
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 */
	public CIEmployee deleteEmployee(CIEmployee employee) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	 * Deletes Employee by userID
	 * @param userId
	 * @return Employee which is deleted by the method
	 * @throws com.liferay.portal.kernel.exception.PortalException
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 */
	public CIEmployee deleteEmployee(long userId) throws com.liferay.portal.kernel.exception.PortalException, com.liferay.portal.kernel.exception.SystemException;

	/**
	 * Updates Employee
	 * @param employee
	 * @return Employee which is updated by the method
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 */
	public CIEmployee updateEmployee(CIEmployee employee) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	 * Retrieves Employees List
	 * @return List<CIEmployee>
	 */
	public List<CIEmployee> getEmployees();

	/**
	 * Finds Employee by userId
	 * @param userId
	 * @return Employee
	 */
	public CIEmployee findEmployeeByUserId(long userId);

}
