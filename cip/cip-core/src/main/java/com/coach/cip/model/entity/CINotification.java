
package com.coach.cip.model.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

/**
 * Class is used to capture the characteristics of Notifications and
 * associated entities. This is a reusable class for notification.
 * @author GalaxE.
 */

@NamedNativeQueries({
	@NamedNativeQuery(name = "callNotificationUsersProcedure", query = "CALL GetNotificationForGivenUserId(:userId,:resultCount)", resultClass = CINotification.class)
})
@Entity
@Table(name="ci_notification")
public class CINotification implements java.io.Serializable{
	
	
	private static final long serialVersionUID = 7128075942431649299L;
	// Fields

	private Long notificationId;
	private User userByModifiedBy;
	private User userByCreatedBy;
	private Timestamp createDate;
	private Timestamp modifiedDate;
	private String notificationSubject;
	private String notificationDescription;
	private Timestamp displayDate;
	private Timestamp expirationDate;
	private String notificationType;
	private String url;
	private Set<CINotificationForLocation> notificationForLocation = new HashSet<CINotificationForLocation>(0);
	private Set<Role> notificationForRoles = new HashSet<Role>(0);
	private Set<User> notificationForUsers = new HashSet<User>(0);

	// Constructors

	/** default constructor. */
	public CINotification() {

	}

	/** minimal constructor. */
	public CINotification(Long notificationId, User userByModifiedBy, User userByCreatedBy) {

		this.notificationId = notificationId;
		this.userByModifiedBy = userByModifiedBy;
		this.userByCreatedBy = userByCreatedBy;
	}

	/** full constructor. */
	public CINotification(
		Long notificationId, User userByModifiedBy, User userByCreatedBy, Timestamp createDate, Timestamp modifiedDate, String notificationSubject, String notificationDescription,
		Timestamp displayDate, Timestamp expirationDate, String notificationType, Set<CINotificationForLocation> notificationForLocation, Set<Role> notificationForRoles, Set<User> notificationForUsers) {

		this.notificationId = notificationId;
		this.userByModifiedBy = userByModifiedBy;
		this.userByCreatedBy = userByCreatedBy;
		this.createDate = createDate;
		this.modifiedDate = modifiedDate;
		this.notificationSubject = notificationSubject;
		this.notificationDescription = notificationDescription;
		this.displayDate = displayDate;
		this.expirationDate = expirationDate;
		this.notificationType = notificationType;
		this.notificationForLocation = notificationForLocation;
		this.notificationForRoles = notificationForRoles;
		this.notificationForUsers = notificationForUsers;
	}

	/**
	 * Method used to get notificationId.
	 * @return notificationId.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "notificationId", unique = true, nullable = false)
	public Long getNotificationId() {

		return this.notificationId;
	}

	/**
	 * Method used to set notificationId.
	 * @param notificationId
	 */
	public void setNotificationId(Long notificationId) {

		this.notificationId = notificationId;
	}

	/**
	 * Method used to get user object.
	 * @return userByModifiedBy.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "modifiedBy", nullable = false)
	public User getUserByModifiedBy() {

		return this.userByModifiedBy;
	}

	/**
	 * Method used to set user object.
	 * @param userByModifiedBy
	 */
	public void setUserByModifiedBy(User userByModifiedBy) {

		this.userByModifiedBy = userByModifiedBy;
	}

	/**
	 * Method used to get user object.
	 * @return userByCreatedBy.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "createdBy", nullable = false)
	public User getUserByCreatedBy() {

		return this.userByCreatedBy;
	}

	/**
	 * Method used to set user object.
	 * @param userByCreatedBy
	 */
	public void setUserByCreatedBy(User userByCreatedBy) {

		this.userByCreatedBy = userByCreatedBy;
	}

	/**
	 * Method used to get time stamp for createDate.
	 * @return createDate
	 */
	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {

		return this.createDate;
	}

	/**
	 * Method used to set time stamp for createDate.
	 * @param createDate
	 */
	public void setCreateDate(Timestamp createDate) {

		this.createDate = createDate;
	}

	/**
	 * Method used to get time stamp for modifiedDate.
	 * @return modifiedDate.
	 */
	@Column(name = "modifiedDate", length = 0)
	public Timestamp getModifiedDate() {

		return this.modifiedDate;
	}

	/**
	 * Method used to set time stamp for modifiedDate.
	 * @param modifiedDate
	 */
	public void setModifiedDate(Timestamp modifiedDate) {

		this.modifiedDate = modifiedDate;
	}

	/**
	 * Method used to get notificationSubject.
	 * @return notificationSubject.
	 */
	@Column(name = "notificationSubject", length = 200)
	public String getNotificationSubject() {

		return this.notificationSubject;
	}

	/**
	 * Method used to set notificationSubject.
	 * @param notificationSubject
	 */
	public void setNotificationSubject(String notificationSubject) {

		this.notificationSubject = notificationSubject;
	}

	/**
	 * Method used to get notificationDescription.
	 * @return notificationDescription.
	 */
	@Column(name = "notificationDescription", length = 2000)
	public String getNotificationDescription() {

		return this.notificationDescription;
	}

	/**
	 * Method used to set notificationDescription.
	 * @param notificationDescription
	 */
	public void setNotificationDescription(String notificationDescription) {

		this.notificationDescription = notificationDescription;
	}

	/**
	 * Method used to get displayDate.
	 * @return displayDate.
	 */
	@Column(name = "displayDate", length = 0)
	public Timestamp getDisplayDate() {

		return this.displayDate;
	}

	/**
	 * Method used to set displayDate.
	 * @param displayDate
	 */
	public void setDisplayDate(Timestamp displayDate) {

		this.displayDate = displayDate;
	}

	/**
	 * Method used to get expirationDate.
	 * @return expirationDate.
	 */
	@Column(name = "expirationDate", length = 0)
	public Timestamp getExpirationDate() {

		return this.expirationDate;
	}

	/**
	 * Method used to set expirationDate.
	 * @param expirationDate
	 */
	public void setExpirationDate(Timestamp expirationDate) {

		this.expirationDate = expirationDate;
	}

	/**
	 * Method used to get notificationType.
	 * @return notificationType.
	 */
	@Column(name = "notificationType", length = 20)
	public String getNotificationType() {

		return this.notificationType;
	}

	/**
	 * Method used to set notificationType.
	 * @param notificationType
	 */
	public void setNotificationType(String notificationType) {

		this.notificationType = notificationType;
	}

	/*
	 * cascade=CascadeType.ALL is deleting the child objects i.e "regionId" from
	 * "region" table are also getting deleted when a "notificationDescription" is deleted from
	 * "CI_Notification". Do not use CascadeType.ALL & CascadeType.REMOVE.
	 */

	/**
	 * Method used to set notificationForLocation.
	 * @return notificationForLocation Set.
	 */
	
	@SuppressWarnings("deprecation")
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "notification")
	@Cascade({
		org.hibernate.annotations.CascadeType.DELETE_ORPHAN
	})
	public Set<CINotificationForLocation> getNotificationForLocation() {

		return notificationForLocation;
	}

	/**
	 * Method used to set notificationForLocation.
	 * @param notificationForLocation
	 */
	public void setNotificationForLocation(Set<CINotificationForLocation> notificationForLocation) {

		this.notificationForLocation = notificationForLocation;
	}

	/*
	 * cascade=CascadeType.ALL is deleting the child objects i.e "roleid" from
	 * "role_" table are also getting deleted when a "notificationDescription" is deleted from
	 * "CI_Notification". Do not use CascadeType.ALL & CascadeType.REMOVE.
	 */

	/**
	 * Method used to get notificationForRoles.
	 * @return notificationForRoles Set.
	 */

	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "ci_notificationforrole", joinColumns = @JoinColumn(name = "notificationId"), inverseJoinColumns = @JoinColumn(name = "roleId"))
	public Set<Role> getNotificationForRoles() {

		return notificationForRoles;
	}

	/**
	 * Method used to set notificationForRoles.
	 * @param notificationForRoles
	 *            the notificationForRoles to set
	 */
	public void setNotificationForRoles(Set<Role> notificationForRoles) {

		this.notificationForRoles = notificationForRoles;
	}

	/**
	 * @return the url
	 */
	@Column(name = "url", length = 200)
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	
	
	/*
	 * cascade=CascadeType.ALL is deleting the child objects i.e "userid" from
	 * "user_" table are also getting deleted when a "notificationDescription" is deleted from
	 * "CI_Notification". Do not use CascadeType.ALL & CascadeType.REMOVE.
	 */

	/**
	 * Method used to get notificationForUsers.
	 * @return notificationForUsers Set.
	 */

	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "ci_notificationforuser", joinColumns = @JoinColumn(name = "notificationId"), inverseJoinColumns = @JoinColumn(name = "userId"))
	public Set<User> getNotificationForUsers() {

		return notificationForUsers;
	}

	/**
	 * Method used to set notificationForUsers.
	 * @param notificationForUsers
	 *            the notificationForUsers to set
	 */
	public void setNotificationForUsers(Set<User> notificationForUsers) {

		this.notificationForUsers = notificationForUsers;
	}

}
