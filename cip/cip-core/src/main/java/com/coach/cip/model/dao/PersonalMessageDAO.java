
package com.coach.cip.model.dao;

import java.util.List;
import org.springframework.stereotype.Component;
import com.coach.cip.model.entity.CIPersonalMessage;
import com.coach.cip.model.entity.Role;
import com.coach.cip.model.entity.User;

/**
 * Interface personalMessageDAO declares DML operations on personalMessages
 * tables.
 * 
 * @author GalaxE.
 */
@Component
public interface PersonalMessageDAO extends GenericDAO<CIPersonalMessage> {

	/**
	 * retrieves personalMessages.
	 * @return list of CIPersonalMessages.
	 */
	List<CIPersonalMessage> getAllPersonalMessage();

	/**
	 * retrieves personalMessage based on messageId.
	 * @param messageId
	 * @return CIPersonalMessage.
	 */
	CIPersonalMessage getPersonalMessage(Long messageId);

	/**
	 * deletes personalMessage based on messageId.
	 * @param messageId
	 */
	void deletePersonalMessage(Long messageId);

	/**
	 * Adds a Personal Message.
	 * @param personalMessage
	 */
	void addPersonalMessage(CIPersonalMessage personalMessage);

	/**
	 * retrieving personalMessages based on usedId.
	 * @param userId
	 *            , resultCount
	 * @return list of CIPersonalMessages.
	 */
	List<CIPersonalMessage> getUserMessages(Long userId, int resultCount);

	/**
	 * retrieves roles based on roleId.
	 * @param roleId
	 * @return role.
	 */
	Role getRole(Long roleId);

	/**
	 * retrieves user based on userId.
	 * @param userId
	 * @return user.
	 */
	User getUser(Long userId);

	/**
	 * updates personalMessage.
	 * @param personalMessage
	 */
	void updatePersonalMessage(CIPersonalMessage personalMessage);

}
