package com.coach.cip.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * Class is used to capture the characteristics of region.
 * This is a reusable class for regions. 
 * @author GalaxE.
 */
@Entity
@Table(name="Region")

public class Region  implements java.io.Serializable {


	 private static final long serialVersionUID = 1710057983647112950L;
     // Fields  
	
	 private Long regionId;
     private Long countryId;
     private String regionCode;
     private String name;
     private Short active;
   


    // Constructors

    /** default constructor */
    public Region() {
    }

	/** minimal constructor */
    public Region(Long regionId) {
        this.regionId = regionId;
    }
    
    /** full constructor */
    public Region(Long regionId, Long countryId, String regionCode, String name, Short active) {
        this.regionId = regionId;
        this.countryId = countryId;
        this.regionCode = regionCode;
        this.name = name;
        this.active = active;
       
    }

   
    // Property accessors
    @Id 
    
    @Column(name="regionId", unique=true, nullable=false)

    public Long getRegionId() {
        return this.regionId;
    }
    
    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }
    
    @Column(name="countryId")

    public Long getCountryId() {
        return this.countryId;
    }
    
    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }
    
    @Column(name="regionCode", length=75)

    public String getRegionCode() {
        return this.regionCode;
    }
    
    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }
    
    @Column(name="name", length=75)

    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    @Column(name="active_")

    public Short getActive() {
        return this.active;
    }
    
    public void setActive(Short active) {
        this.active = active;
    }

}