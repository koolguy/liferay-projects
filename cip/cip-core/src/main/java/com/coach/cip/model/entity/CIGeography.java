package com.coach.cip.model.entity;

import static javax.persistence.GenerationType.IDENTITY;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Class is used to capture the characteristics of ci_geography.
 * This is a reusable class for ci_geography.
 * 
 * @author GalaxE.
 *
 */
@Entity
@Table(name = "ci_geography")
public class CIGeography implements java.io.Serializable {
	
	
	private static final long serialVersionUID = 5541396609669517620L;
	// Fields

	private Long geographyId;
	private String geographyName;
	private Set<Country> ciGeographycountries = new HashSet<Country>(0);

	// Constructors

	/** default constructor */
	public CIGeography() {
	}

	/** minimal constructor */
	public CIGeography(String geographyName) {
		this.geographyName = geographyName;
	}

	/** full constructor */
	public CIGeography(String geographyName, Set<Country> ciGeographycountries) {
		this.geographyName = geographyName;
		this.ciGeographycountries = ciGeographycountries;
	}

		/**
		 * Method used to get geographyId.
		 * @return geographyId.
		 */
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "geographyId", unique = true, nullable = false)
	public Long getGeographyId() {
		return this.geographyId;
	}

	/**
	 * Method used to set geographyId.
	 * @param geographyId
	 */
	public void setGeographyId(Long geographyId) {
		this.geographyId = geographyId;
	}

	/**
	 * Method used to get geographyName.
	 * @return geographyName.
	 */
	@Column(name = "geographyName", nullable = false, length = 75)
	public String getGeographyName() {
		return this.geographyName;
	}

	/**
	 * Method used to set geographyName.
	 * @param geographyName
	 */
	public void setGeographyName(String geographyName) {
		this.geographyName = geographyName;
	}
	
	/**
	 * Method used to get ciGeographycountries.
	 * @return ciGeographycountries.
	 */
	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "ci_geographycountry", joinColumns = @JoinColumn(name = "geographyId"), inverseJoinColumns = @JoinColumn(name = "countryId"))
	public Set<Country> getCiGeographycountries() {
		return this.ciGeographycountries;
	}

	/**
	 * Method is used to set ciGeographycountries.
	 * @param ciGeographycountries
	 */
	public void setCiGeographycountries(Set<Country> ciGeographycountries) {
		this.ciGeographycountries = ciGeographycountries;
	}

}