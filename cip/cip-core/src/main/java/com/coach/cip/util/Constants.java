
package com.coach.cip.util;

/**
 * Class is used to capture the constants of the application. 
 * @author GalaxE
 *
 */
public interface Constants {

	String COACH_USER = "Coach_User";
	String COACH_USER_LIST = "Coach_User_List";
	String COACH_EMPLOYEE_LIST = "Coach_Employee_List";
	String PERSONAL_MESSAGE_LIST = "Personal_Message_List";
	String USERS_PERSONAL_MESSAGE_LIST = "Users_Personal_Message_List";
	String QUICKLINKS_JSP = "quickLinks";
	String PERSONAL_MESSAGE = "Personal_Message";
	String SPACE = " ";
	String COMMA = ",";
	String DATE_FORMAT = "MM/dd/yyyy";
	String SERVICE_LOCATOR = "serviceLocator";
	String CALL_STORED_PROCEDURE_TO_GET_USERS_MESSAGES = "callPersonalMessageUsersProcedure";
	String EMPLOYEES_JSP = "employees";
	String NEWS_JSP = "news";
	String NEWS_DETAIL_JSP = "newsDetail";
	String NEWS_FULL_DETAIL_JSP = "newsFullDetail";
	String NEWS_ATTACHMENT_JSP = "newsAttachement";
	int MESSAGE_LENGTH_TO_DISPLAY = 50;
	String NOT_SELECTED = "Not Selected";
	String ALL = "All";
	String HOME_URL = "/group/coach/home";
	String STATIC_VIDEO_VIEW_JSP = "staticVideoView";
	String VIDEO_JSP = "video";
	/**
	 * Personal Message Constants.
	 */
	String PERSONAL_MESSAGE_VIEW_URL = "/group/coach/personal-message-view";
	String PERSONAL_MESSAGE_ADMIN = "personalMessageAdmin";
	String EDIT_PERSONAL_MESSAGE_ADMIN = "editPersonalMessageAdmin";
	String ADD_PERSONAL_MESSAGE_ADMIN = "addPersonalMessageAdmin";
	String DETAIL_PERSONAL_MESSAGE_USER = "detailPersonalMessageUser";
	String PERSONAL_MESSAGE_VIEW = "viewPersonalMessage";
	String ALL_PERSONAL_MESSAGE_USER = "allPersonalMessageUser";
	int LIMITED_MESSAGES_FOR_USER = 3;
	int ALL_MESSAGES_FOR_USER = 2147483647;
	String JUNK_CHARS = ".....";
	String PERSONAL_MESSAGE_ACTION_TYPE = "Personal Message";
	String PERSONALMSG_ADMIN_SUCCESS_ADD_SAVE = "success-personalmessageadmin-add-save";
	String PERSONALMSG_ADMIN_SUCCESS_DELETE = "success-personalmessageadmin-delete";
	String PERSONALMSG_ADMIN_SUCCESS_UPDATE = "success-personalmessageadmin-update";

	String PERSONALMSG_ADMIN_ERROR_ADD_SAVE = "error-personalmessageadmin-add-save";
	String PERSONALMSG_ADMIN_ERROR_DELETE = "error-personalmessageadmin-delete";
	String PERSONALMSG_ADMIN_ERROR_UPDATE = "error-personalmessageadmin-update";

	/**
	 * Webcontent.
	 */
	String WEBCONTENT_JSP = "Webcontent";
	String WEBCONTENT_DETAIL_JSP = "Webcontentdetail";
	public static final String COACH_NEWS_ARTICLE_TYPE = "COACH NEWS";
	String COACH_NEWS_TYPE = "Coach News";
	String COACH_NEWS_URL = "/group/coach/coach-news";
	/**
	 * Newsslider.
	 */
	String NEWSSLIDER_JSP = "Newsslider";
		
	/**
	 * Newsslidermobile.
	 */
	String NEWSSLIDERMOBILE_JSP = "Newsslidermobile";
	
	
	/**
	 * Documentcontent.
	 */
	String DOCUMENTCONTENT_JSP = "documentContent";
	
	/**
	 * Documentcontentnavigation.
	 */
	String DOCUMENTCONTENTNAVIGATION_JSP = "documentContentNavigation";
	
	/**
	 * AllDocumentcontent.
	 */
	String ALLDOCUMENTCONTENT_JSP = "allDocumentContent";
	
	/**
	 * Feedback.
	 */
	String FEEDBACK_JSP = "feedback";
	String GENERIC_EMAIL_ADDRESS = "Coachweb_Feedback@coach.com";
	String GENERIC_NAME = "Coachweb Feedback";
	String SHARE_YOUR_FEEDBACK_TOPIC = "SHAREYOURFEEDBACKTOPIC";
	String FEEDBACK_TEMPLATE = "templates/feedback.vm";
	String REPORT_A_PROBLEM_TOPIC = "REPORTAPROBLEMTOPIC";

	/**
	 * Coach News Links Constants.
	 */
	String COACH_NEWS_LINKS_JSP = "newsLinks/coachNewsLinks";
	String COACH_NEWS_JSP = "coachNews";

	
	/**
	 * Employee Search Constants.
	 */
	public static final String EMPLOYEE_SEARCH = "employeeSearch";
	
	/**
	 * Employee Directory Search Constants.
	 */

	public static final String EMPLOYEE_DIRECTORY_SEARCH = "employeeDirectorySearch";
	public static final String DEPARTMENT = "department";
	public static final String SEARCH_VO_LIST = "searchVoList";
	public static final String EMPLOYEE_DIRECTORY_SEARCH_RESULT = "employeeDirectorySearchResult";
	public static final String EMPLOYEE_DIRECTORY_SEARCH_DETAIL = "employeeDirectorySearchDetail";
	public static final String EMPLOYEE_DIRECTORY_RESULT_PAGE = "employeeDirectoryResultPage";
	public static final String EMPLOYEE_DIRECTORY_DETAIL_PAGE = "employeeDirectoryDetailPage";
	public static final String MY_ACTION = "myaction";
	public static final String SEARCHED_EMP_OBJECT = "searchedEmpObject";
	public static final String EMPLOYEE_ID = "empId";
	public static final String VIEW_PAGE = "viewPage";
	public static final String EMPLOYEE_LIST_SIZE = "empListSize";
	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	public static final String TITLE = "title";
	public static final String GEOGRAPHY = "geography";
	public static final String EMAIL = "email";
	public static final String PHONE_EXTENSION = "phoneExtension";
	public static final String FAULT_CODE = "FaultCode";
	public static final String FAULT_MESSAGE = "FaultMessage";
	public static final String EMPTY = "";
	public static final String SERVER = "Server";
	public static final String CLIENT = "Client";
	public static final String MUST_UNDERSTAND  = "MustUnderstand";
	public static final String VERSION_MISMATCH = "VersionMismatch";
	public static final String DEPARTMENT_LIST = "departmentList";
	public static final String GEOGRAPHY_LIST = "geographyList";
	public static final String NEXT_CALL = "callNextTime";
	public static final String YES = "Yes";
	public static final String NO_RESULT_FOUND = "message.employee.directory.noresult";
	public static final String  EMPLOYEE_REDIRECT_URL = "empRedirectURL";
	public static final String  EMPLOYEE_PROFILE_IPC = "ipc.empProfile";
	public static final String  IPC_URL = "http://coach.com/events";
	public static final String  EMPLOYEE_PROFILE_REDIRECT_URL = "/group/coach/employeeprofile?interProfilePage=true";
	public static final String  EMPLOYEE_PROFILE_VO = "employeeProfileVO";	
	public static final String INTER_PROFILE_PAGE = "interProfilePage";
	public static final String ERROR_MESSAGE = "errorMessage";
	public static final String VIEWPAGE = "viewpage";
	public static final String TECHNICAL_ERROR_MESSAGE = "Due to technical issue system was unable to fetch the data. Try again later or please contact system administrator.";
	public static final String STAR = "*";
	String CHARACTER_ENCODING = "UTF-8";

	/**
	 * Quick Links Constants.
	 */
	String QUICKLINKS_SUCCESS_SAVE = "success-quicklinks-save";
	String QUICKLINKS_SUCCESS_EDIT_UPDATE = "success-quicklinks-edit-update";
	String QUICKLINKS_SUCCESS_EDIT_DELETE = "success-quicklinks-edit-delete";

	String QUICKLINKS_ERROR_SAVE = "error-quicklinks-save";
	String QUICKLINKS_ERROR_EDIT_UPDATE = "error-quicklinks-edit-update";
	String QUICKLINKS_ERROR_EDIT_DELETE = "error-quicklinks-edit-delete";
	String QUICKLINKS_ERROR_ADD_DUPLICATE = "error-quicklinks-add-duplicate-click";
	String QUICKLINKS_ERROR_DELETE_DUPLICATE = "error-quicklinks-delete-duplicate-click";
	String QUICKLINKS_ERROR_UPDATE_DELETE_DUPLICATE = "error-quicklinks-update-delete-duplicate-click";

	String USER_ID = "user.userId";
	String PLID = "plid";
	String QUICKLINKSID = "quicklinkId";

	/**
	 * News and NewsDetail Constants.
	 */
	int NEWS_ABSTRACT_LENGTH = 100;
	String COACH_NEWS_VOCAB = "coach_news";
	int COACH_STARTING_NEWS_YEAR = 2011;
	String NEWS_IMAGE_URL = "imageUrl";
	String NEWS_CONTENT = "content";
	String NEWS_DOCS = "docs";
	String NEWS_DC = "./dynamic-content";
	String LANGUAGE_ID_REGEX = "language-id=\"(.*?)\"";
	/**
	 * ExecutiveCorner Portlet.
	 */

	public static final String EXECUTIVE_CORNER_JSP = "executiveCorner";
	public static final String SHOW_ARTICLE_JSP = "showArticle";
	public static final String EXECUTIVE_CORNER_ARTICLE_TYPE = "EXECUTIVE CORNER";
	String EXECUTIVE_CORNER_NEWS_TYPE = "Executive Corner News";
	String EXECUTIVE_CORNER_URL = "/executive-corner";
	String EXECUTIVE_CORNER = "EXECUTIVE CORNER";

	/**
	 * VisionAndValues Portlet.
	 */

	public static final String VISION_AND_VALUES_JSP = "visionAndValues";
	public static final String VISION_AND_VALUES_ARTICLE_TYPE = "VISION AND VALUES";
	public static final String VISION_AND_VALUES_SHOW_ARTICLE_JSP = "visionAndValuesShowArticle";
	String VISION_AND_VALUES_NEWS_TYPE = "Vision and Value News";
	String VISION_AND_VALUES_URL = "/vision-and-values";
	String VISION_AND_VALUES = "VISION AND VALUES";

	/**
	 * LocalNews Portlet.
	 */

	public static final String LOCAL_NEWS_JSP = "localNews";
	String LOCAL_NEWS_ARTICLE_TYPE = "LOCAL NEWS";
	String DEPARTMENT_NEWS_ARTICLE_TYPE = "DEPARTMENT NEWS";
	String LOCAL_NEWS_TYPE = "Local News";
	String LOCAL_NEWS_URL = "/local-news";
	String DEPARTMENT_NEWS_URL = "/department-news";
	String DEPARTMENT_NEWS_TYPE = "Department News";
	/**
	 * New Hires & Promotions
	 */
	String LASTNAME = "LastName";
	String DESIGNATION = "Designation";
	String DATE = "Date";
	String FIRSTNAME = "FirstName";
	String PROMOTIONDETAILS = "PromotionDetails";
	String NEWS_TICKER_JSP = "newsTicker";
	public static final String NEW_HIRES = "newHires";
	public static final String NEWHIRES_ARTICLE_TYPE = "new-hires";
	public static final String DYNAMIC_ELEMENT_NAME = "/root/dynamic-element[@name='";
	public static final String DYNAMIC_CONTENT = "']/dynamic-content";
	public static final String ALL_NEW_HIRES_PAGE = "allNewHiresPage";
	public static final String ALL_NEW_HIRES = "allNewHires";
	public static final String PROMOTIONS = "promotions";
	public static final String ALL_PROMOTIONS_LIST = "allPromotionsList";
	public static final String ALL_PROMOTIONS_PAGE = "allPromotionsPage";
	public static final String ALL_PROMOTIONS = "allPromotions";
	public static final String PROMOTION_ARTICLE_TYPE = "promotions";
	public static final String ALL_NEW_HIRES_LIST = "allNewHiresList";
	public static final String EMPLOYEE_PROFILE_PAGE ="employeeProfilePage";
	public static final String EMPLOYEE_PROFILE_NEWHIRES = "newHiresEmployeeProfile";
	public static final String EMPLOYEE_PROFILE_PROMOTIONS = "promotionsEmployeeProfile";
	public static final String DOCUMENT = "Document";
	public static final String EMPLOYEEID = "EmployeeID";
	public static final String DOCUMENT_PATH = "documentPath";
	public static final String ATTACHMENTH_HEADER = "attachmentHeader";
	public static final String ATTACHMENT_PAGE = "attachmentPage";
	public static final String ATTACHMENT = "attachment";
	public static final String NEW_HIRES_HEADER = "NEW HIRES";
	public static final String PROMOTIONS_HEADER = "PROMOTIONS";	
	public static final String IMAGE_URL = "imageUrl";
	public static final String CONTENT_URL = "content";
	public static final String DOC_URL = "docs";
	public static final String ERR_WEBSERVICE_PROCESSING = "message.employee.directory.webservice";
	public static final String MSG_NO_NEW_HIRES = "message.no.newhires";
	public static final String MSG_NO_PROMOTIONS = "message.no.promotions";
	public static final String NO_NEW_HIRES = "NoNewHires";
	public static final String DELIMETER = "[,]+";
	public static final String SEMICOLON = ";";
	public static final String NO_PROMOTIONS = "NoPromotions";
	public static final String DOCUMENT_DOC = "DocumentDoc";
	public static final String EMP_ID = "employeeId";
	public static final String WEBCONTENT_DOCUMENT = "document";
	public static final String TAMPLATE_NAME = "Name";
	public static final String NEWHIRES_TEMPLATE_NAME = "NewHires";
	public static final String PRMOTIONS_TEMPLATE_NAME = "Promotions";
	public static final String PROMOTIONS_EMPLOYEE_PROFILE_PAGE = "promotionsEmployeeProfilePage";
	public static final String NEWHIRES_EMPLOYEE_PROFILE_PAGE = "newHiresEmployeeProfilePage";
	public static final String EMPLOYEE_DIRECTORY_FRIENDLY_URL = "employeedirectory";
	public static final String EMPLOYEE = "employee";
	public static final String DIRECTORY = "directory";
	public static final String EMPLOYEE_DIRECTORY_HTML_TITLE = "EMPLOYEE DIRECTORY";
	public static final String ALL_NEW_HIRES_URL = "/group/coach/all-new-hires";
	public static final String ALL_PROMOTIONS_URL = "/group/coach/all-promotions";
	String NEW_HIRES_ACTION_TYPE = "New Hires";
	String NEW_HIRES_DETAILS = "New Hire Details";
	String PROMOTIONS_ACTION_TYPE = "Promotions";
	String PROMOTIONS_DETAILS = "Promotions Details";
	/**
	 * Stores Constants
	 */

	String ERROR_STORES_WS_EXC_SERVER = "error_stores_webservice_exception_server";
	String ERROR_STORES_WS_EXC_CLIENT = "error-stores-webservice-exception-client";
	String ERROR_STORES_WS_EXC_UNDERSTAND = "error_stores_webservice_exception_mustunderstand";
	String ERROR_STORES_WS_EXC_VERMISMATCH = "error_stores_webservice_exception_versionmismatch";
	String STORES = "stores";
	String NORTH_AMERICA1 = "/north-america1";
	String CHINA1 = "/china1";
	String JAPAN1 = "/japan1";
	String ASIA1 = "/asia1";
	String WESTERN_EUROPE1 = "/western-europe1";
	String OTHER1 = "/other1";
	String ERROR_STOREDETAIL_STORENUM_EXC = "error_storesdetail_storenumber_exc";
	String NO_STORES_FOUND = "no.stores.found";
	
	String OFFICES_NA = "label.offices.northamerica";
	String OFFICES_CHINA = "label.offices.china";
	String OFFICES_JAPAN = "label.offices.japan";
	String OFFICES_ASIA = "label.offices.asia";
	String OFFICES_WESTERNEUROPE = "label.offices.westerneurope";
	String OFFICES_OTHER = "label.offices.other";
	String STORES_NA = "NORTH AMERICA";
	String BRAND_COACH = "label.title.brand.coach";
	String BRAND_RK = "label.title.brand.rk";
	String STORE_BRAND_COACH = "COACH";
	String STORE_BRAND_RK = "RK";
	
	/**
	 * SideRelatedLinks
	 */

	String SIDE_RELATED_LINKS_TITLE = "label.siderelated.links.title";
	/**
	 * Coach Configuration Portlet
	 */
	public static final String COACH_CONFIG_VIEW_JSP = "coachConfiguration/view";
	
	/**
	 * Calendar Navigation Portlet.
	 */

	String CALENDARNAVIGATION = "calendarNavigation";
	String CALENDARVOCABULARY = "CALENDAR";
	
	/**
	 * Side Navigation.
	 */
	
	String SIDENAVIGATIONCONTROLLER = "sideNavigation";
	String INTERNAL_ACCESS_ROLE = "Internal Access Only";
	String INTERNAL_NETWORK_IPS = "com.coach.cip.network.ips";
	String PDF_VIEW_URL = "com.coach.cip.pdf.viewurl.roles";
	String COACH_INTRANET_PORTAL_ORGANIZATION_NAME = "coach.cip.organization.name";
	String NO_USER_PRIVILEGE = "no-user-privilege";
	String CA_IGNORE_VOCAB_LIST = "ca.ignore.navigation.vocab.list";
	/**
	 * PDF Portlet.
	 */
	String PDFPORTLET = "pdfPortlet";
	String INTERNAL_LINK_REQUEST_PARAMETER = "com.coach.cip.internallink.request.parameter";
	
	/**
	 * Links Portlet
	 */
	String LINKS_PORTLET_CONFIG_JSP = "linksPortletConfig";
	String LINKS_PORTLET_VIEW_JSP = "linksPortletView";

	
	/**
	 * Transmittal
	 */
	String TRANSMITTAL_FULL_DETAIL_JSP = "transmittalFullDetail";
	String TRANSMITTAL_ATTACHMENT_JSP = "transmittalAttachement";
	
	
	/**
	 * Questions And Answers.
	 */
	String QANDA_JSP = "questionsAndAnswers";
	String QANDA_PORTLET_ACCESS = "com.coach.cip.qanda.view.roles";
	
	/**
	 * Instructions.
	 */
	String INSTRUCTIONS_JSP = "instructions";
	
}
