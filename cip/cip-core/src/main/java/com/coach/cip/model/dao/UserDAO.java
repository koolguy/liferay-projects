
package com.coach.cip.model.dao;

import java.util.List;
import com.coach.cip.model.entity.User;

/**
 * Interface UserDAO declares DML operations on User table.
 * 
 * @author GalaxE.
 */
public interface UserDAO extends GenericDAO<User> {

	/**
	 * Retrieves List of User
	 * @return List<User>
	 */
	public List<User> getUsers();

	/**
	 * Finds User by userId
	 * @param userId
	 * @return User
	 */
	public User findUserByUserId(long userId);
}
