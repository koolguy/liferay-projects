package com.coach.cip.services;

import java.util.List;

import org.springframework.stereotype.Component;

@Component
public interface LocationService<T> {

	public List<T> getAllLocation(Class<T> classObject, String columnName, Long id);
	
	public List<T> getAllLocation(Class<T> classObject, String columnName, String value);
	
	public List<String> getAllDistinctAddressLocation(Class<T> classObject, String returnColumnName,String columnName, String value);
	
	public List<T> getAllGeography(Class<T> classObject);
	
	public List<Object> getGeographyCountry(String columnName, Long id);
	
}
