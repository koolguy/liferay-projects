package com.coach.cip.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Class is used to capture the characteristics of ci_messageforlocation.
 * This is a reusable class for ci_messageforlocation.
 * 
 * @author GalaxE
 *
 */

@Entity
@Table(name = "ci_messageforlocation")
public class CIMessageForLocation implements java.io.Serializable {

	
	private static final long serialVersionUID = 5596659900738286261L;
	// Fields
	
	private Long cimflId;
	private CIPersonalMessage personalMessage;
	private String locationType;
	private String locationName;
	private String state;
	private String city;
	private String office;

	// Constructors

	/** default constructor */
	public CIMessageForLocation() {
	}

	/** full constructor */
	public CIMessageForLocation(Long cimflId,
			CIPersonalMessage ciPersonalMessage, String locationType,
			String locationName, String state, String city) {
		this.cimflId = cimflId;
		this.personalMessage = ciPersonalMessage;
		this.locationType = locationType;
		this.locationName = locationName;
		this.state = state;
		this.city = city;
	}

	/**
	 * Method used to get cimflId.
	 * @return cimflId.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cimflId", unique = true, nullable = false)
	public Long getCimflId() {
		return this.cimflId;
	}

	/**
	 * Method used to set cimflId.
	 * @param cimflId.
	 */
	public void setCimflId(Long cimflId) {
		this.cimflId = cimflId;
	}

	/**
	 * Method used to get personalMessage.
	 * @return personalMessage.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "messageId")
	public CIPersonalMessage getPersonalMessage() {
		return this.personalMessage;
	}

	/**
	 * Method used to set personalMessage.
	 * @param ciPersonalMessage
	 */
	public void setPersonalMessage(CIPersonalMessage ciPersonalMessage) {
		this.personalMessage = ciPersonalMessage;
	}

	/**
	 * Method used to get locationType.
	 * @return locationType.
	 */
	@Column(name = "locationType", nullable = false, length = 20)
	public String getLocationType() {
		return this.locationType;
	}

	/**
	 * Method used to set locationType.
	 * @param locationType
	 */
	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	/**
	 * Method used to get locationName.
	 * @return locationName.
	 */
	@Column(name = "locationName", nullable = false, length = 75)
	public String getLocationName() {
		return this.locationName;
	}

	/**
	 * Method used to set locationName.
	 * @param locationName
	 */
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	/**
	 * Method used to get state.
	 * @return state.
	 */
	@Column(name = "state", nullable = true, length = 75)
	public String getState() {
		return this.state;
	}

	/**
	 * Method used to set state.
	 * @param state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Method used to get city.
	 * @return city.
	 */
	@Column(name = "city", nullable = true, length = 75)
	public String getCity() {
		return this.city;
	}

	/**
	 * Method used to set city.
	 * @param city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Method used to get office.
	 * @return the office.
	 */
	public String getOffice() {
		return office;
	}

	/**
	 * Method used to set office.
	 * @param office.
	 */
	public void setOffice(String office) {
		this.office = office;
	}

}