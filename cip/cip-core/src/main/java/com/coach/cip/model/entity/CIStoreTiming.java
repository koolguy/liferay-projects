package com.coach.cip.model.entity;

import java.sql.Time;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * Class is used to capture the characteristics of store timing.
 * This is a reusable class for store timing. 
 * @author GalaxE.
 */
@Entity
@Table(name="ci_storetiming")

public class CIStoreTiming  implements java.io.Serializable {


	private static final long serialVersionUID = 3786096843306193186L;
	// Fields    

     private Long storeTimingId;
     private CIStore CIStore;
     private String weekday;
     private Time openingTime;
     private Time closingTime;


    // Constructors

    /** default constructor */
    public CIStoreTiming() {
    }

	/** minimal constructor */
    public CIStoreTiming(Long storeTimingId, CIStore CIStore) {
        this.storeTimingId = storeTimingId;
        this.CIStore = CIStore;
    }
    
    /** full constructor */
    public CIStoreTiming(Long storeTimingId, CIStore CIStore, String weekday, Time openingTime, Time closingTime) {
        this.storeTimingId = storeTimingId;
        this.CIStore = CIStore;
        this.weekday = weekday;
        this.openingTime = openingTime;
        this.closingTime = closingTime;
    }

   
    // Property accessors
    @Id 
    
    @Column(name="storeTimingId", unique=true, nullable=false)

    public Long getStoreTimingId() {
        return this.storeTimingId;
    }
    
    public void setStoreTimingId(Long storeTimingId) {
        this.storeTimingId = storeTimingId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="storeId", nullable=false)

    public CIStore getCIStore() {
        return this.CIStore;
    }
    
    public void setCIStore(CIStore CIStore) {
        this.CIStore = CIStore;
    }
    
    @Column(name="weekday", length=3)

    public String getWeekday() {
        return this.weekday;
    }
    
    public void setWeekday(String weekday) {
        this.weekday = weekday;
    }
    
    @Column(name="openingTime", length=0)

    public Time getOpeningTime() {
        return this.openingTime;
    }
    
    public void setOpeningTime(Time openingTime) {
        this.openingTime = openingTime;
    }
    
    @Column(name="closingTime", length=0)

    public Time getClosingTime() {
        return this.closingTime;
    }
    
    public void setClosingTime(Time closingTime) {
        this.closingTime = closingTime;
    }
   








}