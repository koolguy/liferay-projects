
package com.coach.cip.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.coach.cip.model.dao.EmployeeDAO;
import com.coach.cip.model.entity.CIEmployee;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * Service implementation class for the EmployeeService Interface.
 * @author GalaxE.
 */
@Service("employeeService")
public class EmployeeServiceImpl implements EmployeeService {

	/**
	 * Instance variable for employeeDao.
	 */
	@Autowired
	EmployeeDAO employeeDao;

	/**
	 * Getter method for EmployeeDAO
	 * @return EmployeeDao
	 */
	public EmployeeDAO getEmployeeDao() {

		return employeeDao;
	}

	/**
	 * Setter method for EmployeeDAO
	 * @param employeeDao
	 */
	public void setEmployeeDao(EmployeeDAO employeeDao) {

		this.employeeDao = employeeDao;
	}

	/**
	 * Adds Employee
	 * @param employee
	 * @return Employee which is added by the method
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 */
	public CIEmployee addEmployee(CIEmployee employee) throws SystemException {

		return null;
	}

	/**
	 * Creates Employee
	 * @param userId
	 * @return Employee which is created by the method
	 */
	public CIEmployee createEmployee(long userId) {

		return null;
	}

	/**
	 * Deletes Employee
	 * @param employee
	 * @return Employee which is deleted by the method
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 */
	public CIEmployee deleteEmployee(CIEmployee employee) throws SystemException {

		return null;
	}

	/**
	 * Deletes Employee by userID
	 * @param userId
	 * @return Employee which is deleted by the method
	 * @throws com.liferay.portal.kernel.exception.PortalException
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 */
	public CIEmployee deleteEmployee(long userId) throws PortalException, SystemException {

		return null;
	}

	/**
	 * Updates Employee
	 * @param employee
	 * @return Employee which is updated by the method
	 * @throws com.liferay.portal.kernel.exception.SystemException
	 */
	public CIEmployee updateEmployee(CIEmployee employee) throws SystemException {

		return null;
	}

	/**
	 * Retrieves Employees List
	 * @return List<CIEmployee>
	 */
	public List<CIEmployee> getEmployees() {

		return employeeDao.getEmployees();
	}

	/**
	 * Finds Employee by userId
	 * @param userId
	 * @return Employee
	 */
	public CIEmployee findEmployeeByUserId(long userId) {

		return employeeDao.findEmployeeByUserId(userId);
	}

	/*
	 * public User addEmployee(long companyId, boolean autoPassword, String
	 * password1, String password2, boolean autoScreenName, String screenName,
	 * String emailAddress, long facebookId, String openId, Locale locale,
	 * String firstName, String middleName, String lastName, int prefixId, int
	 * suffixId, boolean male, int birthdayMonth, int birthdayDay, int
	 * birthdayYear, String jobTitle, long[] groupIds, long[] organizationIds,
	 * long[] roleIds, long[] userGroupIds, boolean sendEmail, ServiceContext
	 * serviceContext, long empId, long adminAssistId, long supervisorId, String
	 * employeeType, String employeeStatus, String employmentType, boolean
	 * leadershipIndicator, boolean enrollmentBenefitIndicator, Date hireDate)
	 * throws PortalException, SystemException { User user =
	 * UserServiceUtil.addUser(companyId, autoPassword, password1, password2,
	 * autoScreenName, screenName, emailAddress, facebookId, openId, locale,
	 * firstName, middleName, lastName, prefixId, suffixId, male, birthdayMonth,
	 * birthdayDay, birthdayYear, jobTitle, groupIds, organizationIds, roleIds,
	 * userGroupIds, sendEmail, serviceContext); CIEmployee employee = new
	 * CIEmployee(); employee.setEmpId(empId);
	 * employee.setUserId(user.getUserId());
	 * employee.setAdminAssistId(adminAssistId);
	 * employee.setSupervisorId(supervisorId);
	 * employee.setEmployeeType(employeeType);
	 * employee.setEmployeeStatus(employeeStatus);
	 * employee.setEmploymentType(employmentType);
	 * employee.setEnrollmentBenefitIndicator(enrollmentBenefitIndicator);
	 * employee.setLeadershipIndicator(leadershipIndicator);
	 * employee.setHireDate(hireDate); employeeDao.save(employee); return user;
	 * }
	 */
}
