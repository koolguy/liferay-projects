
package com.coach.cip.services;

import java.util.List;
import com.coach.cip.model.entity.User;

/**
 * Service interface for the User.
 * @author GalaxE.
 */
public interface UserService {

	/**
	 * Finds User by userId
	 * @param userId
	 * @return User
	 */
	public User findUserByUserId(long userId);

	/**
	 * Retrieves Users List
	 * @return List<User>
	 */
	public List<User> getUsers();

}
