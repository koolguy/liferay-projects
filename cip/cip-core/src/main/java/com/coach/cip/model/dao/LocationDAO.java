package com.coach.cip.model.dao;

import java.util.List;
import org.springframework.stereotype.Component;

/**
 * 
 * Interface used to perform the location operations.
 * 
 * @author GalaxE.
 *
 */
@Component
public interface LocationDAO<T> extends GenericDAO<T> {
	
	/**
	 * returns all locations based on id.
	 * @param classObject
	 * @param columnName
	 * @param id
	 * @return
	 */
	public List<T> getAllLocation(Class<T> classObject, String columnName, Long id);
	
	/**
	 * returns all locations based on name.
	 * @param classObject
	 * @param columnName
	 * @param name
	 * @return
	 */
	public List<T> getAllLocation(Class<T> classObject, String columnName, String name);

	/**
	 * returns all distinct address locations.
	 * @param classObject
	 * @param returnColumnName
	 * @param columnName
	 * @param value
	 * @return
	 */
	public List<String> getAllDistinctAddressLocation(Class<T> classObject, String returnColumnName,String columnName, String value);
	
	
	/**
	 * return list of geographies.
	 * @param classObject
	 * @return List<T> 
	 */
	public List<T> getAllGeography(Class<T> classObject);
	
	/**
	 * return list of objects.
	 * @param columnName
	 * @param id
	 * @return
	 */
	public List<Object> getGeographyCountry(String columnName, Long id);

}
