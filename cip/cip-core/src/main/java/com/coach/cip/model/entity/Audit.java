package com.coach.cip.model.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Class used to capture the characteristics of Audit.
 * 
 * @author GalaxE.
 * 
 */

@Entity
@Table(name = "audit_auditevent")
public class Audit implements Serializable {
	private static final long serialVersionUID = -3520167478482665982L;

	// Default Constructor
	public Audit() {

	}

	// Parameterized Constructor
	/**
	 * @param auditEventId
	 * @param companyId
	 * @param userId
	 */
	public Audit(Long auditEventId, Long companyId, Long userId) {
		this.auditEventId = auditEventId;
		this.companyId = companyId;
		this.userId = userId;
	}

	private Long auditEventId;
	private Long companyId;
	private Long userId;
	private String userName;
	private Timestamp createDate;
	private String eventType;
	private String className;
	private String classPK;
	private String message;
	private String clientHost;
	private String clientIP;
	private String serverName;
	private Integer serverPort;
	private String sessionID;
	private String additionalInfo;
	
	@Id
	@Column(name = "auditEventId", unique = true, nullable = false)
	public Long getAuditEventId() {
		return auditEventId;
	}

	public void setAuditEventId(Long auditEventId) {
		this.auditEventId = auditEventId;
	}

	@Column(name = "companyId", nullable = false)
	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	@Column(name = "userId", nullable = false)
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Column(name = "userName", nullable = true)
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "createDate", nullable = true)
	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	@Column(name = "eventType", nullable = true)
	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	@Column(name = "className", nullable = true)
	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	@Column(name = "classPK", nullable = true)
	public String getClassPK() {
		return classPK;
	}

	public void setClassPK(String classPK) {
		this.classPK = classPK;
	}

	@Column(name = "message", nullable = true)
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Column(name = "clientHost", nullable = true)
	public String getClientHost() {
		return clientHost;
	}

	public void setClientHost(String clientHost) {
		this.clientHost = clientHost;
	}

	@Column(name = "clientIP", nullable = true)
	public String getClientIP() {
		return clientIP;
	}

	public void setClientIP(String clientIP) {
		this.clientIP = clientIP;
	}

	@Column(name = "serverName", nullable = true)
	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	@Column(name = "serverPort", nullable = true)
	public Integer getServerPort() {
		return serverPort;
	}

	public void setServerPort(Integer serverPort) {
		this.serverPort = serverPort;
	}

	@Column(name = "sessionID", nullable = true)
	public String getSessionID() {
		return sessionID;
	}

	public void setSessionID(String sessionID) {
		this.sessionID = sessionID;
	}

	@Column(name = "additionalInfo", nullable = true)
	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}
}
