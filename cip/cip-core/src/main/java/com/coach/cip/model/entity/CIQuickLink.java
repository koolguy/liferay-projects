
package com.coach.cip.model.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Class is used to capture the characteristics of Quick Links and associated
 * entities. This is a reusable class for Quick Links.
 * @author GalaxE.
 */
@Entity
@Table(name = "ci_quicklink")
public class CIQuickLink implements java.io.Serializable {

	
	private static final long serialVersionUID = 6784442744372744417L;
	// Fields

	private Long quicklinkId;
	private Layout layout;
	private User user;
	private String pageName;
	private Timestamp createDate;
	private String assetURL;
	// Constructors

	/** default constructor. */
	public CIQuickLink() {

	}

	/** minimal constructor. */
	public CIQuickLink(Long quicklinkId, Layout layout, User user) {

		this.quicklinkId = quicklinkId;
		this.layout = layout;
		this.user = user;
	}

	/** full constructor. */
	public CIQuickLink(Long quicklinkId, Layout layout, User user, String pageName, Timestamp createDate,String assetURL) {

		this.quicklinkId = quicklinkId;
		this.layout = layout;
		this.user = user;
		this.pageName = pageName;
		this.createDate = createDate;
		this.assetURL = assetURL;
	}

	// Property accessors
	/**
	 * Getter Method for quicklinkId.
	 * @return quicklinkId
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "quicklinkId", unique = true, nullable = false)
	public Long getQuicklinkId() {

		return this.quicklinkId;
	}

	/**
	 * Setter Method for quicklinkId.
	 * @param quicklinkId
	 */
	public void setQuicklinkId(Long quicklinkId) {

		this.quicklinkId = quicklinkId;
	}

	/**
	 * Getter Method for plid.
	 * @return Layout
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "plid", nullable = false)
	public Layout getLayout() {

		return this.layout;
	}

	/**
	 * Setter Method for plid.
	 * @param layout
	 */
	public void setLayout(Layout layout) {

		this.layout = layout;
	}

	/**
	 * Getter Method for userId.
	 * @return User
	 */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "userId", nullable = false)
	public User getUser() {

		return this.user;
	}

	/**
	 * Setter Method for userId.
	 * @param user
	 */
	public void setUser(User user) {

		this.user = user;
	}

	/**
	 * Getter Method for pageName.
	 * @return pageName
	 */
	@Column(name = "pageName", length = 100)
	public String getPageName() {

		return this.pageName;
	}

	/**
	 * Setter Method for pageName.
	 * @param pageName
	 */
	public void setPageName(String pageName) {

		this.pageName = pageName;
	}

	/**
	 * Getter Method for createDate.
	 * @return createDate
	 */
	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {

		return this.createDate;
	}

	/**
	 * Setter Method for createDate.
	 * @param createDate
	 */
	public void setCreateDate(Timestamp createDate) {

		this.createDate = createDate;
	}

	/**
	 * Getter Method for assetURL.
	 * @return assetURL
	 */
	@Column(name = "assetURL")
	public String getAssetURL() {
		return assetURL;
	}

	/**
	 * Setter Method for assetURL.
	 * @param assetURL
	 */
	public void setAssetURL(String assetURL) {
		this.assetURL = assetURL;
	}

}
