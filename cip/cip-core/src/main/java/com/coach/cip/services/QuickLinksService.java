
package com.coach.cip.services;

import java.util.List;

import com.coach.cip.model.entity.CIQuickLink;
import com.coach.cip.model.entity.Layout;
import com.coach.cip.model.entity.User;

/**
 * Service interface for the Quick Links.
 * @author GalaxE.
 */
public interface QuickLinksService {

	/**
	 * Retrieves Quick Links for the User.
	 * @param userId
	 * @return List<CIQuickLink>
	 */
	List<CIQuickLink> getQuickLinks(long userId);

	/**
	 * Retrieves the Layout based on Page Layout Id.
	 * @param plId
	 * @return Layout
	 */
	Layout getLayout(long plId);

	/**
	 * Add Quick Link
	 * @param ciQuickLink
	 */
	 void addQuickLinks(CIQuickLink ciQuickLink);

	/**
	 * Updates Quick Links.
	 * @param ciQuickLinksList
	 */
	 void updateQuickLinks(List<CIQuickLink> ciQuickLinksList);

	/**
	 * Delete Quick Links.
	 * @param ciQuickLinksList
	 */
	 void deleteQuickLinks(List<CIQuickLink> ciQuickLinksList);

	/**
	 * Retrieves the User based on User Id.
	 * @param userId
	 * @return User
	 */
	 User getUser(long userId);

}
