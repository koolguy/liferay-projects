
package com.coach.cip.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.coach.cip.model.dao.UserDAO;
import com.coach.cip.model.entity.User;

/**
 * Service implementation class for the UserService Interface.
 * @author GalaxE.
 */
@Service(value = "userService")
public class UserServiceImpl implements UserService {

	/**
	 * Instance variable for userDao.
	 */
	@Autowired()
	UserDAO userDao;

	/**
	 * Finds User by userId
	 * @param userId
	 * @return User
	 */
	public User findUserByUserId(long userId) {

		return userDao.findUserByUserId(userId);
	}

	/**
	 * Retrieves Users List
	 * @return List<User>
	 */
	public List<User> getUsers() {

		return userDao.getUsers();

	}

}
