package com.coach.cip.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * Class is used to capture the characteristics of role entity.
 * This is a reusable class for roles.
 * 
 * @author GalaxE.
 *
 */
@Entity
@Table(name = "Role_", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "companyId", "classNameId", "classPK" }),
		@UniqueConstraint(columnNames = { "companyId", "name" }) })
public class Role implements java.io.Serializable {

	
	private static final long serialVersionUID = -5252193340999093915L;
	// Fields

	private Long roleId;
	private Long companyId;
	private Long classNameId;
	private Long classPk;
	private String name;
	private String title;
	private String description;
	private Integer type;
	private String subtype;

	// Constructors

	/** default constructor */
	public Role() {
	}

	/** minimal constructor */
	public Role(Long roleId) {
		this.roleId = roleId;
	}

	/** full constructor */
	public Role(Long roleId, Long companyId, Long classNameId, Long classPk,
			String name, String title, String description, Integer type,
			String subtype) {
		this.roleId = roleId;
		this.companyId = companyId;
		this.classNameId = classNameId;
		this.classPk = classPk;
		this.name = name;
		this.title = title;
		this.description = description;
		this.type = type;
		this.subtype = subtype;

	}

	/**
	 * Method used to get roleId.
	 * @return roleId.
	 */
	@Id
	@Column(name = "roleId", unique = true, nullable = false)
	public Long getRoleId() {
		return this.roleId;
	}

	/**
	 * Method used to set roleId.
	 * @param roleId
	 */
	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	/**
	 * Method used to get companyId
	 * @return companyId.
	 */
	@Column(name = "companyId")
	public Long getCompanyId() {
		return this.companyId;
	}

	/**
	 * Method used to set companyId.
	 * @param companyId
	 */
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	/**
	 * Method used to get classNameId.
	 * @return classNameId.
	 */
	@Column(name = "classNameId")
	public Long getClassNameId() {
		return this.classNameId;
	}

	/**
	 * Method used to set classNameId.
	 * @param classNameId
	 */
	public void setClassNameId(Long classNameId) {
		this.classNameId = classNameId;
	}

	/**
	 * Method used to get classPK.
	 * @return classPk.
	 */
	@Column(name = "classPK")
	public Long getClassPk() {
		return this.classPk;
	}

	/**
	 * Method used to set classPK.
	 * @param classPk
	 */
	public void setClassPk(Long classPk) {
		this.classPk = classPk;
	}

	/**
	 * Method used to get name.
	 * @return name.
	 */
	@Column(name = "name", length = 75)
	public String getName() {
		return this.name;
	}

	/**
	 * Method used to set name.
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Method used to get title.
	 * @return title.
	 */
	@Column(name = "title")
	public String getTitle() {
		return this.title;
	}

	/**
	 * Method used to set title.
	 * @param title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Method used to get description.
	 * @return description.
	 */
	@Column(name = "description")
	public String getDescription() {
		return this.description;
	}

	/**
	 * Method used to set description.
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Method used to get type.
	 * @return type.
	 */
	@Column(name = "type_")
	public Integer getType() {
		return this.type;
	}

	/**
	 * Method used to set type.
	 * @param type
	 */
	public void setType(Integer type) {
		this.type = type;
	}

	/**
	 * Method used to get subType.
	 * @return subtype.
	 */
	@Column(name = "subtype", length = 75)
	public String getSubtype() {
		return this.subtype;
	}

	/**
	 * Method used to set subType.
	 * @param subtype
	 */
	public void setSubtype(String subtype) {
		this.subtype = subtype;
	}

}