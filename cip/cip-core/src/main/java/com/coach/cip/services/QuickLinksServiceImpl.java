
package com.coach.cip.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coach.cip.model.dao.QuickLinksDAO;
import com.coach.cip.model.entity.CIQuickLink;
import com.coach.cip.model.entity.Layout;
import com.coach.cip.model.entity.User;

/**
 * Service implementation class for the QuickLinksService.
 * @author GalaxE.
 */
@Service(value = "quickLinksService")
public class QuickLinksServiceImpl implements QuickLinksService {

	/**
	 * Instance variable for QuickLinksDAO.
	 */
	@Autowired
	private QuickLinksDAO quickLinksDAO;

	/**
	 * Getter method for quickLinksDAO.
	 * @return QuickLinksDAO
	 */
	public QuickLinksDAO getQuickLinksDAO() {

		return quickLinksDAO;
	}

	/**
	 * Setter method for quickLinksDAO.
	 * @param quickLinksDAO
	 */
	public void setQuickLinksDAO(QuickLinksDAO quickLinksDAO) {

		this.quickLinksDAO = quickLinksDAO;
	}

	/**
	 * Retrieves Quick Links for the User.
	 * @param userId
	 * @return List<CIQuickLink>
	 */
	public List<CIQuickLink> getQuickLinks(long userId) {

		return quickLinksDAO.getQuickLinks(userId);
	}

	/**
	 * Retrieves the Layout based on Page Layout Id.
	 * @param plId
	 * @return Layout
	 */
	public Layout getLayout(long plId) {

		return quickLinksDAO.getLayout(plId);
	}

	/**
	 * Add Quick Link.
	 * @param ciQuickLink
	 */
	public void addQuickLinks(CIQuickLink ciQuickLink) {

		quickLinksDAO.addQuickLinks(ciQuickLink);
	}

	/**
	 * Updates Quick Links.
	 * @param ciQuickLinksList
	 */
	public void updateQuickLinks(List<CIQuickLink> ciQuickLinksList) {

		quickLinksDAO.updateQuickLinks(ciQuickLinksList);

	}

	/**
	 * Delete Quick Links.
	 * @param ciQuickLinksList
	 */
	public void deleteQuickLinks(List<CIQuickLink> ciQuickLinksList) {

		quickLinksDAO.deleteQuickLinks(ciQuickLinksList);

	}

	/**
	 * Retrieves the User based on User Id.
	 * @param userId
	 * @return User
	 */
	public User getUser(long userId) {

		return quickLinksDAO.getUser(userId);
	}

}
