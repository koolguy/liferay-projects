package com.coach.cip.services;

import com.coach.cip.model.entity.CIFeedback;

/**
 * Service interface for the Feedback.
 *
 * @author GalaxE.
 *
 */
public interface FeedbackService {

	/**
	 * This method is used for adding the feedback.
	 *
	 * @param cifeedback
	 */
	void addFeedback(CIFeedback cifeedback);

}
