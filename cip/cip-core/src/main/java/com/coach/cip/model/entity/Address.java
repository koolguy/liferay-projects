package com.coach.cip.model.entity;

import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Class is used to capture the characteristics of address. 
 * This is reusable class for address.
 * @author GalaxE.
 */
@Entity
@Table(name="Address")

public class Address  implements java.io.Serializable {
	
	private static final long serialVersionUID = 5439668925734980947L;

	/** default constructor */
	public Address() {
	}

	/** minimal constructor */
	public Address(Long addressId) {
		this.addressId = addressId;
	}

	/** full constructor */
	public Address(Long addressId, Long companyId, Long userId,
			String userName, Timestamp createDate, Timestamp modifiedDate,
			Long classNameId, Long classPk, String street1, String street2,
			String street3, String city, String zip, Long regionId,
			Long countryId, Integer typeId, Short mailing, Short primary) {
		this.addressId = addressId;
		this.companyId = companyId;
		this.userId = userId;
		this.userName = userName;
		this.createDate = createDate;
		this.modifiedDate = modifiedDate;
		this.classNameId = classNameId;
		this.classPk = classPk;
		this.street1 = street1;
		this.street2 = street2;
		this.street3 = street3;
		this.city = city;
		this.zip = zip;
		this.regionId = regionId;
		this.countryId = countryId;
		this.typeId = typeId;
		this.mailing = mailing;
		this.primary = primary;

	}

	private Long addressId;
	private Long companyId;
	private Long userId;
	private String userName;
	private Timestamp createDate;
	private Timestamp modifiedDate;
	private Long classNameId;
	private Long classPk;
	private String street1;
	private String street2;
	private String street3;
	private String city;
	private String zip;
	private Long regionId;
	private Long countryId;
	private Integer typeId;
	private Short mailing;
	private Short primary;

	
	// Property accessors
	@Id
	@Column(name = "addressId", unique = true, nullable = false)
	public Long getAddressId() {
		return this.addressId;
	}

	public void setAddressId(Long addressId) {
		this.addressId = addressId;
	}

	@Column(name = "companyId")
	public Long getCompanyId() {
		return this.companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

	@Column(name = "userId")
	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Column(name = "userName", length = 75)
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "createDate", length = 0)
	public Timestamp getCreateDate() {
		return this.createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	@Column(name = "modifiedDate", length = 0)
	public Timestamp getModifiedDate() {
		return this.modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	@Column(name = "classNameId")
	public Long getClassNameId() {
		return this.classNameId;
	}

	public void setClassNameId(Long classNameId) {
		this.classNameId = classNameId;
	}

	@Column(name = "classPK")
	public Long getClassPk() {
		return this.classPk;
	}

	public void setClassPk(Long classPk) {
		this.classPk = classPk;
	}

	@Column(name = "street1", length = 75)
	public String getStreet1() {
		return this.street1;
	}

	public void setStreet1(String street1) {
		this.street1 = street1;
	}

	@Column(name = "street2", length = 75)
	public String getStreet2() {
		return this.street2;
	}

	public void setStreet2(String street2) {
		this.street2 = street2;
	}

	@Column(name = "street3", length = 75)
	public String getStreet3() {
		return this.street3;
	}

	public void setStreet3(String street3) {
		this.street3 = street3;
	}

	@Column(name = "city", length = 75)
	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	@Column(name = "zip", length = 75)
	public String getZip() {
		return this.zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	@Column(name = "regionId")
	public Long getRegionId() {
		return this.regionId;
	}

	public void setRegionId(Long regionId) {
		this.regionId = regionId;
	}

	@Column(name = "countryId")
	public Long getCountryId() {
		return this.countryId;
	}

	public void setCountryId(Long countryId) {
		this.countryId = countryId;
	}

	@Column(name = "typeId")
	public Integer getTypeId() {
		return this.typeId;
	}

	public void setTypeId(Integer typeId) {
		this.typeId = typeId;
	}

	@Column(name = "mailing")
	public Short getMailing() {
		return this.mailing;
	}

	public void setMailing(Short mailing) {
		this.mailing = mailing;
	}

	@Column(name = "primary_")
	public Short getPrimary() {
		return this.primary;
	}

	public void setPrimary(Short primary) {
		this.primary = primary;
	}


}