
package com.coach.cip.model.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.coach.cip.model.entity.CIPersonalMessage;
import com.coach.cip.model.entity.Role;
import com.coach.cip.model.entity.User;
import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;

/**
 * Class provides the implementations of personalMessages DAO's needed to
 * perform DML operations.
 * 
 * @author GalaxE.
 */
@Repository(value = "personalMessageDAO")
@Transactional
public class PersonalMessageDAOImpl extends AbstractDAO<CIPersonalMessage> implements PersonalMessageDAO {

	private static final Log logger = LogFactoryUtil.getLog(PersonalMessageDAOImpl.class);

	/**
	 * retrieves personalMessages.
	 * @return list of CIPersonalMessages.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<CIPersonalMessage> getAllPersonalMessage() {

		LoggerUtil.debugLogger(logger, "Start of getAllPersonalMessage() method of PersonalMessageDAOImpl:: ");
		List<CIPersonalMessage> list = null;
		list = getSession().createCriteria(CIPersonalMessage.class).list();
		Iterator it = list.iterator();
		while (it.hasNext()) {
			CIPersonalMessage pmsg = (CIPersonalMessage) it.next();
			Hibernate.initialize(pmsg.getUserByCreatedBy());
			Hibernate.initialize(pmsg.getUserByModifiedBy());
			Hibernate.initialize(pmsg.getMessageForLocations());
			Hibernate.initialize(pmsg.getMessageForRoles());
		}
		LoggerUtil.debugLogger(logger, "End of getAllPersonalMessage() method of PersonalMessageDAOImpl:: ");
		return list;
	}

	/**
	 * retrieves personalMessage based on messageId.
	 * @param messageId
	 * @return CIPersonalMessage.
	 */
	public CIPersonalMessage getPersonalMessage(Long messageId) {

		LoggerUtil.debugLogger(logger, "Start of getPersonalMessage() method of PersonalMessageDAOImpl:: ");
		CIPersonalMessage personalMessage = findById(CIPersonalMessage.class, messageId);
		Hibernate.initialize(personalMessage.getMessageForLocations());
		Hibernate.initialize(personalMessage.getMessageForRoles());
		LoggerUtil.debugLogger(logger, "End of getPersonalMessage() method of PersonalMessageDAOImpl:: ");
		return personalMessage;
	}

	/**
	 * deletes personalMessage based on messageId.
	 * @param messageId
	 */
	public void deletePersonalMessage(Long messageId) {

		LoggerUtil.debugLogger(logger, "Start of deletePersonalMessage() method of PersonalMessageDAOImpl:: ");
		deleteById(CIPersonalMessage.class, messageId);
		LoggerUtil.debugLogger(logger, "End of deletePersonalMessage() method of PersonalMessageDAOImpl:: ");
	}

	/**
	 * Adds a Personal Message
	 * @param personalMessage
	 */
	public void addPersonalMessage(CIPersonalMessage personalMessage) {

		LoggerUtil.debugLogger(logger, "Start of addPersonalMessage() method of PersonalMessageDAOImpl:: ");
		Session session = getSession();
		session.saveOrUpdate(personalMessage);
		session.flush();
		LoggerUtil.debugLogger(logger, "End of addPersonalMessage() method of PersonalMessageDAOImpl:: ");
	}

	/**
	 * retrieving personalMessages based on usedId.
	 * @param userId
	 *            , resultCount
	 * @return list of CIPersonalMessages.
	 */
	@SuppressWarnings("unchecked")
	public List<CIPersonalMessage> getUserMessages(Long userId, int resultCount) {

		LoggerUtil.debugLogger(logger, "Start of getUserMessages() method of PersonalMessageDAOImpl:: ");
		List<CIPersonalMessage> perosnalMessageList = null;

		try {
			Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			perosnalMessageList =
				session.getNamedQuery("callPersonalMessageUsersProcedure").setParameter("userId", userId).setParameter("resultCount", resultCount).list();
		    if(Validator.isNull(perosnalMessageList)){
		    	perosnalMessageList = new ArrayList<CIPersonalMessage>();
		    }
		}
		catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "PersonalMessageDAOImpl.getUserMessages() HibernateException ::", e);
			throw e;
		}
		LoggerUtil.debugLogger(logger, "End of getUserMessages() method of PersonalMessageDAOImpl:: ");
		return perosnalMessageList;
	}

	/**
	 * retrieves roles based on roleId.
	 * @param roleId
	 * @return role.
	 */
	public Role getRole(Long roleId) {

		LoggerUtil.debugLogger(logger, "Start of getRole() method of PersonalMessageDAOImpl:: ");
		Role role = null;
		try {
			role = (Role) hibernateTemplate.get(Role.class, roleId);

		}
		catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "PersonalMessageDAOImpl.getRole() HibernateException ::", e);
			throw e;
		}
		LoggerUtil.debugLogger(logger, "End of getRole() method of PersonalMessageDAOImpl:: ");
		return role;

	}

	/**
	 * retrieves user based on userId.
	 * @param userId
	 * @return user.
	 */
	public User getUser(Long userId) {

		LoggerUtil.debugLogger(logger, "Start of getUser() method of PersonalMessageDAOImpl:: ");
		User user = null;
		try {
			user = (User) hibernateTemplate.get(User.class, userId);

		}
		catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "PersonalMessageDAOImpl.getUser() HibernateException ::", e);
			throw e;
		}
		LoggerUtil.debugLogger(logger, "End of getUser() method of PersonalMessageDAOImpl:: ");
		return user;
	}

	/**
	 * updates personalMessage.
	 * @param personalMessage
	 */
	public void updatePersonalMessage(CIPersonalMessage personalMessage) {

		LoggerUtil.debugLogger(logger, "Start of updatePersonalMessage() method of PersonalMessageDAOImpl:: ");
		if (personalMessage != null) {
			hibernateTemplate.update(personalMessage);
		}
		LoggerUtil.debugLogger(logger, "End of updatePersonalMessage() method of PersonalMessageDAOImpl:: ");
	}

}
