
package com.coach.cip.model.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;

import com.coach.cip.model.entity.CIQuickLink;
import com.coach.cip.model.entity.Layout;
import com.coach.cip.model.entity.User;
import com.coach.cip.util.Constants;
import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * Class provides the implementations of QuickLinksDAO's needed to perform DML
 * operations.
 * @author GalaxE.
 */
@Repository("quickLinksDAO")
public class QuickLinksDAOImpl extends AbstractDAO<CIQuickLink> implements QuickLinksDAO {

	/**
	 * Constants
	 */
	private static final Log logger = LogFactoryUtil.getLog(QuickLinksDAOImpl.class);

	/**
	 * Retrieves Quick Links for the User.
	 * @param userId
	 * @return List<CIQuickLink>
	 */
	@SuppressWarnings("unchecked")
	public List<CIQuickLink> getQuickLinks(long userId) {

		LoggerUtil.debugLogger(logger, "Start of getQuickLinks() method of Quick Links DAOImpl:: ");
		List<CIQuickLink> ciQuickLinkList = null;
		final StringBuilder queryString = new StringBuilder();
		try {
			queryString.append("from CIQuickLink as model where (model.");
			queryString.append(Constants.USER_ID);
			queryString.append("= ?");
			queryString.append(" or model.");
			queryString.append(Constants.USER_ID);
			queryString.append("= ?");
			queryString.append(") and model.layout.plid in (select plid from Layout)");
			queryString.append(" order by ");
			queryString.append(Constants.USER_ID );

			ciQuickLinkList = hibernateTemplate.find(queryString.toString(), 0l, userId);

		}
		catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "QuickLinksDAOImpl.getQuickLinks() HibernateException ::", e);
			throw e;
		}

		LoggerUtil.debugLogger(logger, "End of getQuickLinks() method of Quick Links DAOImpl:: ");
		return ciQuickLinkList;
	}

	/**
	 * Retrieves the Layout based on Page Layout Id.
	 * @param plId
	 * @return Layout
	 */
	public Layout getLayout(long plId) {

		LoggerUtil.debugLogger(logger, "Start of getLayout() method of Quick Links DAOImpl:: ");
		Layout layout = null;
		try {
			layout = hibernateTemplate.get(Layout.class, plId);
		}
		catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "Find by Id failed", e);
		}
		LoggerUtil.debugLogger(logger, "End of getLayout() method of Quick Links DAOImpl:: ");
		return layout;

	}

	/**
	 * Add Quick Link.
	 * @param ciQuickLink
	 */
	public void addQuickLinks(CIQuickLink ciQuickLink) {

		LoggerUtil.debugLogger(logger, "Start of addQuickLinks() method of Quick Links DAOImpl:: ");
		saveOrUpdate(ciQuickLink);
		LoggerUtil.debugLogger(logger, "End of addQuickLinks() method of Quick Links DAOImpl:: ");
	}

	/**
	 * Updates Quick Links.
	 * @param ciQuickLinksList
	 */
	public void updateQuickLinks(List<CIQuickLink> ciQuickLinksList) {

		LoggerUtil.debugLogger(logger, "Start of updateQuickLinks() method of Quick Links DAOImpl:: ");
		for (CIQuickLink ciQuickLink : ciQuickLinksList) {
				StringBuilder queryString = new StringBuilder();
				queryString.append("update CIQuickLink ");
				queryString.append("set pageName= '");
				queryString.append(ciQuickLink.getPageName());
				queryString.append("', createDate='");
				queryString.append(ciQuickLink.getCreateDate());
				queryString.append("' where ");
				queryString.append(Constants.QUICKLINKSID);
				queryString.append("=");
				queryString.append(ciQuickLink.getQuicklinkId());
			try {
				hibernateTemplate.bulkUpdate(queryString.toString());
			}
			catch (HibernateException e) {
				LoggerUtil.errorLogger(logger, "QuickLinksDAOImpl.updateQuickLinks() HibernateException ::", e);
				throw e;
			}

		}
		LoggerUtil.debugLogger(logger, "End of updateQuickLinks() method of Quick Links DAOImpl:: ");

	}

	/**
	 * Delete Quick Links.
	 * @param ciQuickLinksList
	 */
	public void deleteQuickLinks(List<CIQuickLink> ciQuickLinksList) {

		LoggerUtil.debugLogger(logger, "Start of deleteQuickLinks() method of Quick Links DAOImpl:: ");
		for (CIQuickLink ciQuickLink : ciQuickLinksList) {
				StringBuilder queryString = new StringBuilder();
				queryString.append("delete CIQuickLink ");
				queryString.append("where ");
				queryString.append(Constants.QUICKLINKSID);
				queryString.append("=");
				queryString.append(ciQuickLink.getQuicklinkId());
			try {
				hibernateTemplate.bulkUpdate(queryString.toString());
			}
			catch (HibernateException e) {
				LoggerUtil.errorLogger(logger, "QuickLinksDAOImpl.updateQuickLinks() HibernateException ::", e);
				throw e;
			}
		}
		LoggerUtil.debugLogger(logger, "End of deleteQuickLinks() method of Quick Links DAOImpl:: ");
	}

	/**
	 * Retrieves the User based on User Id.
	 * @param userId
	 * @return User
	 */
	public User getUser(long userId) {

		LoggerUtil.debugLogger(logger, "Start of getUser() method of Quick Links DAOImpl:: ");
		User user = null;
		try {
			user = hibernateTemplate.get(User.class, userId);
		}
		catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "QuickLinksDAOImpl.getUser() HibernateException ::", e);
		}
		LoggerUtil.debugLogger(logger, "End of getUser() method of Quick Links DAOImpl:: ");
		return user;

	}

}
