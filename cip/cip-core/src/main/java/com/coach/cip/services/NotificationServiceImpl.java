package com.coach.cip.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coach.cip.model.dao.NotificationDAO;
import com.coach.cip.model.entity.CINotification;
import com.coach.cip.model.entity.Role;
import com.coach.cip.model.entity.User;

/**
 * Service implementation class for the notification.
 * 
 * @author GalaxE.
 * 
 */
@Service(value = "notificationService")
public class NotificationServiceImpl implements NotificationService {

	@Autowired
	private NotificationDAO notificationDAO;

	public NotificationDAO getNotificationDAO() {
		return notificationDAO;
	}

	public void setNotificationDAO(NotificationDAO notificationDAO) {
		this.notificationDAO = notificationDAO;
	}

	/**
	 * retrieves notifications.
	 * 
	 * @return list of CINotifications.
	 */
	public List<CINotification> getAllNotifications() {
		return getNotificationDAO().getAllNotifications();
	}

	/**
	 * retrieves notification based on notificationId.
	 * 
	 * @param notificationId
	 * @return CINotification.
	 */
	public CINotification getNotification(Long notificationId) {
		return getNotificationDAO().getNotification(notificationId);
	}

	/**
	 * deletes notification based on notificationId.
	 * 
	 * @param notificationId
	 */
	public void deleteNotification(Long notificationId) {
		getNotificationDAO().deleteNotification(notificationId);
	}

	/**
	 * Adds a Notification
	 * 
	 * @param notification
	 */
	public void addNotification(CINotification notification) {
		getNotificationDAO().addNotification(notification);
	}

	/**
	 * retrieving notification based on usedId.
	 * 
	 * @param userId, resultCount
	 * @return list of CINotifications.
	 */
	public List<CINotification> getUserMessages(Long userId, int resultCount) {

		return getNotificationDAO().getUserMessages(userId, resultCount);

	}

	/**
	 * retrieves roles based on roleId.
	 * 
	 * @param roleId
	 * @return role.
	 */
	public Role getRole(Long roleId) {
		return getNotificationDAO().getRole(roleId);

	}

	/**
	 * retrieves user based on userId.
	 * 
	 * @param userId
	 * @return user.
	 */
	public User getUser(Long userId) {

		return getNotificationDAO().getUser(userId);
	}

	/**
	 * updates notification.
	 * 
	 * @param notification
	 */
	public void updateNotification(CINotification notification) {
		getNotificationDAO().updateNotification(notification);
	}

}
