package com.coach.cip.services;

/**
 * @author GalaxE.
 * 
 */
@SuppressWarnings("rawtypes")
public interface ServiceLocator {

	UserService getUserService();

	QuickLinksService getQuickLinksService();


	EmployeeService getEmployeeService();

	PersonalMessageService getPersonalMessageService();

	LocationService getLocationService();

	/**
	 * @return feedbackService
	 */
	FeedbackService getFeedbackService();

	/**
	 * 
	 * @return auditService.
	 */
	AuditService getAuditService();
	
	/**
	 * 
	 * @return notificationService
	 */
	NotificationService getNotificationService();

}
