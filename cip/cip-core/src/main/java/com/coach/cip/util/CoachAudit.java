package com.coach.cip.util;

import com.liferay.portal.kernel.audit.AuditException;
import com.liferay.portal.kernel.audit.AuditMessage;
import com.liferay.portal.kernel.audit.AuditRouterUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.model.User;

/**
 * 
 * Class is used to capture the characteristics of .
 * @author GalaxE.
 *
 */
public class CoachAudit 
{
	private static final Log logger = LogFactoryUtil.getLog(CoachAudit.class);
	
	public static void audit(String eventType, User user)
	{
		AuditMessage auditMessage = new AuditMessage(eventType,
				user.getCompanyId(), user.getUserId(), user.getFullName(),
				user.getModelClassName(), String.valueOf(user.getUserId()));
		route(auditMessage);
	}
	
	public static void route(AuditMessage auditMessage)
	{
		try 
		{
			if(GetterUtil.getBoolean(PropsUtil.get("com.liferay.portal.servlet.filters.audit.AuditFilter")))
			{
				AuditRouterUtil.route(auditMessage);
			}
			else
			{
				//TODO: Log it in the log file
			}
		}
		catch (AuditException e) {
	    	LoggerUtil.errorLogger(logger,"CoachAudit.route() AuditException :::",e);
	    	e.printStackTrace();
		}
	}
}
