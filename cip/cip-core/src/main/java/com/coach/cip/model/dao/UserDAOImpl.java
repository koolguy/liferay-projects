
package com.coach.cip.model.dao;

import java.util.List;
import org.springframework.stereotype.Repository;
import com.coach.cip.model.entity.User;
import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * Class provides the implementations of UserDAO Interface to perform DML
 * operations.
 * 
 * @author GalaxE.
 */
@Repository
public class UserDAOImpl extends AbstractDAO<User> implements UserDAO {

	private static final Log logger = LogFactoryUtil.getLog(PersonalMessageDAOImpl.class);
	
	/**
	 * Retrieves List of User
	 * @return List<User>
	 */
	public List<User> getUsers() {
		LoggerUtil.debugLogger(logger, "getUsers() Method Starts in UserDAOImpl Class.");
		return findAll(User.class);
	}

	/**
	 * Finds User by userId
	 * @param userId
	 * @return User
	 */
	public User findUserByUserId(long userId) {
		LoggerUtil.debugLogger(logger, "findUserByUserId() Method Starts in UserDAOImpl Class.");
		return findById(User.class, userId);
	}

}
