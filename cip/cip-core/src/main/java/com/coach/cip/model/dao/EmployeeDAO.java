
package com.coach.cip.model.dao;

import java.util.List;
import org.springframework.stereotype.Component;
import com.coach.cip.model.entity.CIEmployee;

/**
 * Interface EmployeeDAO declares DML operations on Employee table.
 * 
 * @author GalaxE.
 */
@Component
public interface EmployeeDAO extends GenericDAO<CIEmployee> {

	/**
	 * Retrieves List of Employees
	 * @return List<CIEmployee>
	 */
	public List<CIEmployee> getEmployees();

	/**
	 * Finds Employee by userId.
	 * @param userId
	 * @return CIEmployee
	 */
	public CIEmployee findEmployeeByUserId(long userId);
}
