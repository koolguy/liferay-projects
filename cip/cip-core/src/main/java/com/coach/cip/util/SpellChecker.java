
package com.coach.cip.util;

/**
 * Utility interface, has spell checker methods.
 * 
 * @author GalaxE.
 */
public interface SpellChecker {

	/**
	 * Returns the correct spelled word if the wordStr does not represent
	 * correct word for the language represented by langStr, otherwise empty
	 * string.
	 * @param wordStr
	 * @param langStr
	 * @return Correct spelled word or empty string
	 */
	String spellCheck(String wordStr, String langStr);
}
