
package com.coach.cip.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;

/**
 * The Class SpellCheckerUsingJavaHTTPClientAPI, provides the spellChecker
 * method.
 * @author GalaxE.
 */

public class SpellCheckerUsingJavaHTTPClientAPI implements SpellChecker {

	/** The Constant logger. */
	private static final Log logger = LogFactoryUtil.getLog(SpellCheckerUsingJavaHTTPClientAPI.class);

	/**
	 * Returns the correct spelled word if the wordStr does not represent
	 * correct word for the language represented by langStr, otherwise empty
	 * string.
	 * @param wordStr
	 * @param langStr
	 * @return Correct spelled word or empty string
	 */
	public String spellCheck(String wordStr, String langStr) {

		LoggerUtil.debugLogger(logger, "Start of spellCheck() method of SpellCheckerUsingJavaHTTPClientAPI::");
		String result = "";
		String response = "";
		String defaultLang = "US";
		InputStream inputStream = null;
		InputStreamReader reader = null;
		BufferedReader bufferReader = null;
		try {
			StringBuffer requestXML = new StringBuffer();
			requestXML.append("<spellrequest textalreadyclipped=\"0\" ignoredups=\"1\" ignoredigits=\"1\" ignoreallcaps=\"0\"><text>");
			requestXML.append(wordStr);
			requestXML.append("</text></spellrequest>");
			LoggerUtil.debugLogger(logger, "Rquest xml is " + requestXML);
			StringBuffer urlStr = new StringBuffer("https://www.google.com/tbproxy/spell?lang=");
			if (Validator.isNull(langStr)) {
				urlStr = urlStr.append(defaultLang);
				urlStr = urlStr.append("&hl=");
				urlStr = urlStr.append(defaultLang);
			}
			else {
				urlStr = urlStr.append(langStr);
				urlStr = urlStr.append("&hl=");
				urlStr = urlStr.append(langStr);
			}
			LoggerUtil.debugLogger(logger, "urlStr" + urlStr);
			URL url = new URL(urlStr.toString());
			URLConnection conn = url.openConnection();
			conn.setDoOutput(true);
			OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
			out.write(requestXML.toString());
			out.close();
			inputStream = conn.getInputStream();
			reader = new InputStreamReader(inputStream);
			bufferReader = new BufferedReader(reader);
			while ((response = bufferReader.readLine()) != null) {
				LoggerUtil.debugLogger(logger, "\nresponse is " + response);
				String regexPattern = "<(.|\n)*?>";
				response = response.replaceAll(regexPattern, "");
				result = response.split("\t")[0];
				LoggerUtil.debugLogger(logger, "Displaying first suggestion " + result);
			}
			bufferReader.close();
			reader.close();
			inputStream.close();

		}
		catch (IOException e) {
			LoggerUtil.errorLogger(logger, "Exception occured in  spellCheck IOException:", e);
		}
		catch (Exception e) {
			LoggerUtil.errorLogger(logger, "Exception occured in  spellCheck Exception:", e);
		}
		finally {
			if (Validator.isNotNull(inputStream)) {
				try {
					inputStream.close();
				}
				catch (IOException e) {
					LoggerUtil.errorLogger(logger, "Exception in closing the InputStream", e);
				}
			}
			if (Validator.isNotNull(reader)) {
				try {
					reader.close();
				}
				catch (IOException e) {
					LoggerUtil.errorLogger(logger, "Exception in closing the InputStreamReader", e);
				}
			}
			if (Validator.isNotNull(bufferReader)) {
				try {
					bufferReader.close();
				}
				catch (IOException e) {
					LoggerUtil.errorLogger(logger, "Exception in closing the BufferedReader", e);
				}
			}
		}
		LoggerUtil.debugLogger(logger, "End of spellCheck() method of SpellCheckerUsingJavaHTTPClientAPI::");
		return result;
	}
}
