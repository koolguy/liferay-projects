package com.coach.cip.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * Class is used to capture the characteristics of store contacts.
 * This is a reusable class for store contacts.
 * @author GalaxE.
 */
@Entity
@Table(name="ci_storecontact")

public class CIStoreContact  implements java.io.Serializable {


	private static final long serialVersionUID = -3288328905295391145L;
	// Fields    

     private Long contactStoreId;
     private CIContact CIContact;
     private CIStore CIStore;


    // Constructors

    /** default constructor */
    public CIStoreContact() {
    }

    
    /** full constructor */
    public CIStoreContact(Long contactStoreId, CIContact CIContact, CIStore CIStore) {
        this.contactStoreId = contactStoreId;
        this.CIContact = CIContact;
        this.CIStore = CIStore;
    }

   
    // Property accessors
    @Id 
    
    @Column(name="contactStoreId", unique=true, nullable=false)

    public Long getContactStoreId() {
        return this.contactStoreId;
    }
    
    public void setContactStoreId(Long contactStoreId) {
        this.contactStoreId = contactStoreId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="contactId", nullable=false)

    public CIContact getCIContact() {
        return this.CIContact;
    }
    
    public void setCIContact(CIContact CIContact) {
        this.CIContact = CIContact;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="storeId", nullable=false)

    public CIStore getCIStore() {
        return this.CIStore;
    }
    
    public void setCIStore(CIStore CIStore) {
        this.CIStore = CIStore;
    }
   








}