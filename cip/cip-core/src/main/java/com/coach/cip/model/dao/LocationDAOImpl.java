package com.coach.cip.model.dao;

import java.util.List;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.coach.cip.model.entity.CIGeography;
import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * 
 * Class used to perform the location operations.
 * 
 * @author GalaxE.
 *
 */
@Repository(value = "locationDAO")
@Transactional
public class LocationDAOImpl<T> extends AbstractDAO<T> implements
		LocationDAO<T> {

	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(EmployeeDAOImpl.class);
	
	/**
	 * returns all locations based on id.
	 * @param classObject
	 * @param columnName
	 * @param id
	 * @return List<T>
	 */
	public List<T> getAllLocation(Class<T> classObject, String columnName,
			Long id) {
		LoggerUtil.debugLogger(logger, "getAllLocation()By Id Method starts in LocationDAOImpl Class.");
		List<T> list = findByProperty(classObject, columnName, id);
		for (T t : list) {
			Hibernate.initialize(t.getClass());
			if (t.getClass().getName() != null
					&& t.getClass().getName().endsWith("CIGeography")) {
				Hibernate.initialize(((CIGeography) t)
						.getCiGeographycountries());
			}
		}
		LoggerUtil.debugLogger(logger, "getAllLocation() Method Ends in LocationDAOImpl Class.");
		return list;
	}

	/**
	 * returns all locations based on name.
	 * @param classObject
	 * @param columnName
	 * @param name
	 * @return List<T>
	 */
	public List<T> getAllLocation(Class<T> classObject, String columnName,
			String name) {
		LoggerUtil.debugLogger(logger, "getAllLocation() By Name Method starts in LocationDAOImpl Class.");
		return findByProperty(classObject, columnName, name);
	}

	/**
	 * return list of geographies.
	 * @param classObject
	 * @return List<T> 
	 */
	
	public List<T> getAllGeography(Class<T> classObject) {
		LoggerUtil.debugLogger(logger, "getAllGeography() Method starts in LocationDAOImpl Class.");
		return findAll(classObject);
	}

	/**
	 * returns all distinct address locations.
	 * @param classObject
	 * @param returnColumnName
	 * @param columnName
	 * @param value
	 * @return List<String>
	 */
	public List<String> getAllDistinctAddressLocation(Class<T> classObject,String returnColumnName,
			String columnName, String value) {
		LoggerUtil.debugLogger(logger, "getAllDistinctAddressLocation() Method starts in LocationDAOImpl Class.");
		if(columnName.equals("regionId")){
			return findDistinctByProperty(classObject,returnColumnName,columnName,Long.valueOf(value));	
		}else{
			return findDistinctByProperty(classObject,returnColumnName,columnName,value);
		}
	}

	/**
	 * return list of objects.
	 * @param columnName
	 * @param id
	 * @return List<Object>
	 */
	@SuppressWarnings("unchecked")
	public List<Object> getGeographyCountry(String columnName, Long id) {
		LoggerUtil.debugLogger(logger, "getGeographyCountry() Method starts in LocationDAOImpl Class.");
		StringBuffer geographyCountryQuery = new StringBuffer();
		geographyCountryQuery.append("Select * from ci_geographycountry where "+columnName+"=?");
		Query query = getSession().createSQLQuery(geographyCountryQuery.toString());
		query.setParameter(0, id);
		List<Object> obj = query.list();
		LoggerUtil.debugLogger(logger, "getGeographyCountry() Method Ends in LocationDAOImpl Class.");
		return obj;
	}

}
