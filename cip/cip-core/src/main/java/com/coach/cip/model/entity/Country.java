package com.coach.cip.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


/**
 * Class is used to capture the characteristics of country.
 * This is a reusable class for countries. 
 * @author GalaxE.
 */
@Entity
@Table(name="Country", uniqueConstraints = {@UniqueConstraint(columnNames="name"), @UniqueConstraint(columnNames="a2"), @UniqueConstraint(columnNames="a3")}
)

public class Country  implements java.io.Serializable {


	 private static final long serialVersionUID = -6605351928903790880L;
	 // Fields    

     private Long countryId;
     private String name;
     private String a2;
     private String a3;
     private String number;
     private String idd;
     private Short zipRequired;
     private Short active;


    // Constructors

    /** default constructor */
    public Country() {
    }

	/** minimal constructor */
    public Country(Long countryId) {
        this.countryId = countryId;
    }
    
    /** full constructor */
    public Country(Long countryId, String name, String a2, String a3, String number, String idd, Short zipRequired, Short active) {
        this.countryId = countryId;
        this.name = name;
        this.a2 = a2;
        this.a3 = a3;
        this.number = number;
        this.idd = idd;
        this.zipRequired = zipRequired;
        this.active = active;
    }

   
    // Property accessors
    @Id 
    
    @Column(name="countryId", unique=true, nullable=false)

    public Long getCountryId() {
        return this.countryId;
    }
    
    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }
    
    @Column(name="name", unique=true, length=75)

    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    @Column(name="a2", unique=true, length=75)

    public String getA2() {
        return this.a2;
    }
    
    public void setA2(String a2) {
        this.a2 = a2;
    }
    
    @Column(name="a3", unique=true, length=75)

    public String getA3() {
        return this.a3;
    }
    
    public void setA3(String a3) {
        this.a3 = a3;
    }
    
    @Column(name="number_", length=75)

    public String getNumber() {
        return this.number;
    }
    
    public void setNumber(String number) {
        this.number = number;
    }
    
    @Column(name="idd_", length=75)

    public String getIdd() {
        return this.idd;
    }
    
    public void setIdd(String idd) {
        this.idd = idd;
    }
    
    @Column(name="zipRequired")

    public Short getZipRequired() {
        return this.zipRequired;
    }
    
    public void setZipRequired(Short zipRequired) {
        this.zipRequired = zipRequired;
    }
    
    @Column(name="active_")

    public Short getActive() {
        return this.active;
    }
    
    public void setActive(Short active) {
        this.active = active;
    }
   








}