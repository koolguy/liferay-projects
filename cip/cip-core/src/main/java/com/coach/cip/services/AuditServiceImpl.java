package com.coach.cip.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coach.cip.model.dao.AuditDAO;
import com.coach.cip.model.entity.Audit;
import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * Class used as a AuditSericeComponent.
 * 
 * @author GalaxE.
 */
@Service(value="auditService")
public class AuditServiceImpl implements AuditService{

	/** The Constant LOG. */
	private static final Log logger = LogFactoryUtil.getLog(AuditServiceImpl.class);
	
	@Autowired
	private AuditDAO auditDAO;
	
	/**
	 * Method to save audit.
	 * 
	 * @param audit
	 * @throws Exception
	 */
	public void createAudit(Audit audit) throws Exception {
		LoggerUtil.debugLogger(logger, "Start of createAudit() method of AuditServiceImpl");
		try{
			auditDAO.createAudit(audit);
		}catch(Exception ex){
			LoggerUtil.errorLogger(logger, "Exception Caught While Saving Audit Object in createAudit() Method in AuditServiceimpl Class.", ex);
		}
		LoggerUtil.debugLogger(logger, "End of createAudit() method of AuditServiceImpl");
	}

	/**
	 * Method to get max auditEventId.
	 * @return
	 * @throws Exception
	 */
	public long getMaxAuditEventIdFromAuditEntity() throws Exception {
		LoggerUtil.debugLogger(logger, "Start of getMaxAuditEventIdFromAuditEntity() method of AuditServiceImpl");
		long maxAuditEventId =  0l;
		try{
			maxAuditEventId = auditDAO.getMaxAuditEventIdFromAuditEntity();
		}catch(Exception ex){
			LoggerUtil.errorLogger(logger, "Exception Caught While Saving Audit Object in getMaxAuditEventIdFromAuditEntity() Method in AuditServiceimpl Class.", ex);
		}
		LoggerUtil.debugLogger(logger, "End of getMaxAuditEventIdFromAuditEntity() method of AuditServiceImpl");
		return maxAuditEventId;
	}

}
