package com.coach.cip.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.coach.cip.model.dao.PersonalMessageDAO;
import com.coach.cip.model.entity.CIPersonalMessage;
import com.coach.cip.model.entity.Role;
import com.coach.cip.model.entity.User;

/**
 * Service implementation class for the personalMessagesService.
 * 
 * @author GalaxE.
 * 
 */
@Service(value = "personalMessageService")
public class PersonalMessageServiceImpl implements PersonalMessageService {

	@Autowired
	private PersonalMessageDAO personalMessageDAO;

	public PersonalMessageDAO getPersonalMessageDAO() {
		return personalMessageDAO;
	}

	public void setPersonalMessageDAO(PersonalMessageDAO personalMessageDAO) {
		this.personalMessageDAO = personalMessageDAO;
	}

	/**
	 * retrieves personalMessages.
	 * 
	 * @return list of CIPersonalMessages.
	 */
	public List<CIPersonalMessage> getAllPersonalMessage() {
		return getPersonalMessageDAO().getAllPersonalMessage();
	}

	/**
	 * retrieves personalMessage based on messageId.
	 * 
	 * @param messageId
	 * @return CIPersonalMessage.
	 */
	public CIPersonalMessage getPersonalMessage(Long messageId) {
		return getPersonalMessageDAO().getPersonalMessage(messageId);
	}

	/**
	 * deletes personalMessage based on messageId.
	 * 
	 * @param messageId
	 */
	public void deletePersonalMessage(Long messageId) {
		getPersonalMessageDAO().deletePersonalMessage(messageId);
	}

	/**
	 * Adds a Personal Message
	 * 
	 * @param personalMessage
	 */
	public void addPersonalMessage(CIPersonalMessage personalMessage) {
		getPersonalMessageDAO().addPersonalMessage(personalMessage);
	}

	/**
	 * retrieving personalMessages based on usedId.
	 * 
	 * @param userId, resultCount
	 * @return list of CIPersonalMessages.
	 */
	public List<CIPersonalMessage> getUserMessages(Long userId, int resultCount) {

		return getPersonalMessageDAO().getUserMessages(userId, resultCount);

	}

	/**
	 * retrieves roles based on roleId.
	 * 
	 * @param roleId
	 * @return role.
	 */
	public Role getRole(Long roleId) {
		return getPersonalMessageDAO().getRole(roleId);

	}

	/**
	 * retrieves user based on userId.
	 * 
	 * @param userId
	 * @return user.
	 */
	public User getUser(Long userId) {

		return getPersonalMessageDAO().getUser(userId);
	}

	/**
	 * updates personalMessage.
	 * 
	 * @param personalMessage
	 */
	public void updatePersonalMessage(CIPersonalMessage personalMessage) {
		getPersonalMessageDAO().updatePersonalMessage(personalMessage);
	}

}
