package com.coach.cip.model.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * Class is used to capture the characteristics of ci_state.
 * This is a reusable class for ci_state. 
 * @author GalaxE
 */
@Entity
@Table(name="ci_state")

public class CIState  implements java.io.Serializable {

	
	private static final long serialVersionUID = 5208440375897271275L;
	// Fields    

     private Long stateId;
     private CIRegion CIRegion;
     private String stateName;
     private Set<CIDistrict> CIDistricts = new HashSet<CIDistrict>(0);


    // Constructors

    /** default constructor */
    public CIState() {
    }

	/** minimal constructor */
    public CIState(Long stateId, CIRegion CIRegion) {
        this.stateId = stateId;
        this.CIRegion = CIRegion;
    }
    
    /** full constructor */
    public CIState(Long stateId, CIRegion CIRegion, String stateName, Set<CIDistrict> CIDistricts) {
        this.stateId = stateId;
        this.CIRegion = CIRegion;
        this.stateName = stateName;
        this.CIDistricts = CIDistricts;
    }

   
    // Property accessors
    @Id 
    
    @Column(name="stateId", unique=true, nullable=false)

    public Long getStateId() {
        return this.stateId;
    }
    
    public void setStateId(Long stateId) {
        this.stateId = stateId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="regionId", nullable=false)

    public CIRegion getCIRegion() {
        return this.CIRegion;
    }
    
    public void setCIRegion(CIRegion CIRegion) {
        this.CIRegion = CIRegion;
    }
    
    @Column(name="stateName", length=100)

    public String getStateName() {
        return this.stateName;
    }
    
    public void setStateName(String stateName) {
        this.stateName = stateName;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER, mappedBy="CIState")

    public Set<CIDistrict> getCIDistricts() {
        return this.CIDistricts;
    }
    
    public void setCIDistricts(Set<CIDistrict> CIDistricts) {
        this.CIDistricts = CIDistricts;
    }
   








}