package com.coach.cip.model.entity;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


/**
 * Class is used to capture the characteristics of layout.
 * This is a reusable class for layouts. 
 * @author GalaxE.
 */
@Entity
@Table(name="Layout", uniqueConstraints = {@UniqueConstraint(columnNames={"uuid_", "groupId"}), @UniqueConstraint(columnNames={"groupId", "privateLayout", "layoutId"}), @UniqueConstraint(columnNames={"groupId", "privateLayout", "friendlyURL"})}
)

public class Layout  implements java.io.Serializable {


	 private static final long serialVersionUID = -8339763772201384008L;
	 // Fields    

     private Long plid;
     private String uuid;
     private Long groupId;
     private Long companyId;
     private Timestamp createDate;
     private Timestamp modifiedDate;
     private Short privateLayout;
     private Long layoutId;
     private Long parentLayoutId;
     private String name;
     private String title;
     private String description;
     private String keywords;
     private String robots;
     private String type;
     private String typeSettings;
     private Short hidden;
     private String friendlyUrl;
     private Short iconImage;
     private Long iconImageId;
     private String themeId;
     private String colorSchemeId;
     private String wapThemeId;
     private String wapColorSchemeId;
     private String css;
     private Integer priority;
     private String layoutPrototypeUuid;
     private Short layoutPrototypeLinkEnabled;
     private String sourcePrototypeLayoutUuid;
     private Set<CIQuickLink> CIQuickLinks = new HashSet<CIQuickLink>(0);


    // Constructors

    /** default constructor */
    public Layout() {
    }

	/** minimal constructor */
    public Layout(Long plid) {
        this.plid = plid;
    }
    
    /** full constructor */
    public Layout(Long plid, String uuid, Long groupId, Long companyId, Timestamp createDate, Timestamp modifiedDate, Short privateLayout, Long layoutId, Long parentLayoutId, String name, String title, String description, String keywords, String robots, String type, String typeSettings, Short hidden, String friendlyUrl, Short iconImage, Long iconImageId, String themeId, String colorSchemeId, String wapThemeId, String wapColorSchemeId, String css, Integer priority, String layoutPrototypeUuid, Short layoutPrototypeLinkEnabled, String sourcePrototypeLayoutUuid, Set<CIQuickLink> CIQuickLinks) {
        this.plid = plid;
        this.uuid = uuid;
        this.groupId = groupId;
        this.companyId = companyId;
        this.createDate = createDate;
        this.modifiedDate = modifiedDate;
        this.privateLayout = privateLayout;
        this.layoutId = layoutId;
        this.parentLayoutId = parentLayoutId;
        this.name = name;
        this.title = title;
        this.description = description;
        this.keywords = keywords;
        this.robots = robots;
        this.type = type;
        this.typeSettings = typeSettings;
        this.hidden = hidden;
        this.friendlyUrl = friendlyUrl;
        this.iconImage = iconImage;
        this.iconImageId = iconImageId;
        this.themeId = themeId;
        this.colorSchemeId = colorSchemeId;
        this.wapThemeId = wapThemeId;
        this.wapColorSchemeId = wapColorSchemeId;
        this.css = css;
        this.priority = priority;
        this.layoutPrototypeUuid = layoutPrototypeUuid;
        this.layoutPrototypeLinkEnabled = layoutPrototypeLinkEnabled;
        this.sourcePrototypeLayoutUuid = sourcePrototypeLayoutUuid;
        this.CIQuickLinks = CIQuickLinks;
    }

   
    // Property accessors
    @Id 
    
    @Column(name="plid", unique=true, nullable=false)

    public Long getPlid() {
        return this.plid;
    }
    
    public void setPlid(Long plid) {
        this.plid = plid;
    }
    
    @Column(name="uuid_", length=75)

    public String getUuid() {
        return this.uuid;
    }
    
    public void setUuid(String uuid) {
        this.uuid = uuid;
    }
    
    @Column(name="groupId")

    public Long getGroupId() {
        return this.groupId;
    }
    
    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }
    
    @Column(name="companyId")

    public Long getCompanyId() {
        return this.companyId;
    }
    
    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }
    
    @Column(name="createDate", length=0)

    public Timestamp getCreateDate() {
        return this.createDate;
    }
    
    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
    
    @Column(name="modifiedDate", length=0)

    public Timestamp getModifiedDate() {
        return this.modifiedDate;
    }
    
    public void setModifiedDate(Timestamp modifiedDate) {
        this.modifiedDate = modifiedDate;
    }
    
    @Column(name="privateLayout")

    public Short getPrivateLayout() {
        return this.privateLayout;
    }
    
    public void setPrivateLayout(Short privateLayout) {
        this.privateLayout = privateLayout;
    }
    
    @Column(name="layoutId")

    public Long getLayoutId() {
        return this.layoutId;
    }
    
    public void setLayoutId(Long layoutId) {
        this.layoutId = layoutId;
    }
    
    @Column(name="parentLayoutId")

    public Long getParentLayoutId() {
        return this.parentLayoutId;
    }
    
    public void setParentLayoutId(Long parentLayoutId) {
        this.parentLayoutId = parentLayoutId;
    }
    
    @Column(name="name")

    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    @Column(name="title")

    public String getTitle() {
        return this.title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    @Column(name="description")

    public String getDescription() {
        return this.description;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    @Column(name="keywords")

    public String getKeywords() {
        return this.keywords;
    }
    
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }
    
    @Column(name="robots")

    public String getRobots() {
        return this.robots;
    }
    
    public void setRobots(String robots) {
        this.robots = robots;
    }
    
    @Column(name="type_", length=75)

    public String getType() {
        return this.type;
    }
    
    public void setType(String type) {
        this.type = type;
    }
    
    @Column(name="typeSettings")

    public String getTypeSettings() {
        return this.typeSettings;
    }
    
    public void setTypeSettings(String typeSettings) {
        this.typeSettings = typeSettings;
    }
    
    @Column(name="hidden_")

    public Short getHidden() {
        return this.hidden;
    }
    
    public void setHidden(Short hidden) {
        this.hidden = hidden;
    }
    
    @Column(name="friendlyURL")

    public String getFriendlyUrl() {
        return this.friendlyUrl;
    }
    
    public void setFriendlyUrl(String friendlyUrl) {
        this.friendlyUrl = friendlyUrl;
    }
    
    @Column(name="iconImage")

    public Short getIconImage() {
        return this.iconImage;
    }
    
    public void setIconImage(Short iconImage) {
        this.iconImage = iconImage;
    }
    
    @Column(name="iconImageId")

    public Long getIconImageId() {
        return this.iconImageId;
    }
    
    public void setIconImageId(Long iconImageId) {
        this.iconImageId = iconImageId;
    }
    
    @Column(name="themeId", length=75)

    public String getThemeId() {
        return this.themeId;
    }
    
    public void setThemeId(String themeId) {
        this.themeId = themeId;
    }
    
    @Column(name="colorSchemeId", length=75)

    public String getColorSchemeId() {
        return this.colorSchemeId;
    }
    
    public void setColorSchemeId(String colorSchemeId) {
        this.colorSchemeId = colorSchemeId;
    }
    
    @Column(name="wapThemeId", length=75)

    public String getWapThemeId() {
        return this.wapThemeId;
    }
    
    public void setWapThemeId(String wapThemeId) {
        this.wapThemeId = wapThemeId;
    }
    
    @Column(name="wapColorSchemeId", length=75)

    public String getWapColorSchemeId() {
        return this.wapColorSchemeId;
    }
    
    public void setWapColorSchemeId(String wapColorSchemeId) {
        this.wapColorSchemeId = wapColorSchemeId;
    }
    
    @Column(name="css")

    public String getCss() {
        return this.css;
    }
    
    public void setCss(String css) {
        this.css = css;
    }
    
    @Column(name="priority")

    public Integer getPriority() {
        return this.priority;
    }
    
    public void setPriority(Integer priority) {
        this.priority = priority;
    }
    
    @Column(name="layoutPrototypeUuid", length=75)

    public String getLayoutPrototypeUuid() {
        return this.layoutPrototypeUuid;
    }
    
    public void setLayoutPrototypeUuid(String layoutPrototypeUuid) {
        this.layoutPrototypeUuid = layoutPrototypeUuid;
    }
    
    @Column(name="layoutPrototypeLinkEnabled")

    public Short getLayoutPrototypeLinkEnabled() {
        return this.layoutPrototypeLinkEnabled;
    }
    
    public void setLayoutPrototypeLinkEnabled(Short layoutPrototypeLinkEnabled) {
        this.layoutPrototypeLinkEnabled = layoutPrototypeLinkEnabled;
    }
    
    @Column(name="sourcePrototypeLayoutUuid", length=75)

    public String getSourcePrototypeLayoutUuid() {
        return this.sourcePrototypeLayoutUuid;
    }
    
    public void setSourcePrototypeLayoutUuid(String sourcePrototypeLayoutUuid) {
        this.sourcePrototypeLayoutUuid = sourcePrototypeLayoutUuid;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="layout")

    public Set<CIQuickLink> getCIQuickLinks() {
        return this.CIQuickLinks;
    }
    
    public void setCIQuickLinks(Set<CIQuickLink> CIQuickLinks) {
        this.CIQuickLinks = CIQuickLinks;
    }
   








}