package com.coach.cip.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Class is used to capture the characteristics of ci_notificationforlocation.
 * This is a reusable class for ci_notificationforlocation.
 * 
 * @author GalaxE
 *
 */

@Entity
@Table(name = "ci_notificationforlocation")
public class CINotificationForLocation implements java.io.Serializable {

	
	
	
	private static final long serialVersionUID = 4956234623601606713L;
	// Fields
	
	private Long cinflId;
	private CINotification notification;
	private String locationType;
	private String locationName;
	private String state;
	private String city;
	private String office;

	// Constructors

	/** default constructor */
	public CINotificationForLocation() {
	}

	/** full constructor */
	public CINotificationForLocation(Long cinflId,
			CINotification notification, String locationType,
			String locationName, String state, String city) {
		this.cinflId = cinflId;
		this.notification = notification;
		this.locationType = locationType;
		this.locationName = locationName;
		this.state = state;
		this.city = city;
	}

	/**
	 * Method used to get cinflId.
	 * @return cinflId.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "cinflId", unique = true, nullable = false)
	public Long getCinflId() {
		return this.cinflId;
	}

	/**
	 * Method used to set cinflId.
	 * @param cinflId.
	 */
	public void setCinflId(Long cinflId) {
		this.cinflId = cinflId;
	}

	/**
	 * Method used to get notification.
	 * @return notification.
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "notificationId")
	public CINotification getNotification() {
		return this.notification;
	}

	/**
	 * Method used to set notification.
	 * @param notification
	 */
	public void setNotification(CINotification notification) {
		this.notification = notification;
	}

	/**
	 * Method used to get locationType.
	 * @return locationType.
	 */
	@Column(name = "locationType", nullable = false, length = 20)
	public String getLocationType() {
		return this.locationType;
	}

	/**
	 * Method used to set locationType.
	 * @param locationType
	 */
	public void setLocationType(String locationType) {
		this.locationType = locationType;
	}

	/**
	 * Method used to get locationName.
	 * @return locationName.
	 */
	@Column(name = "locationName", nullable = false, length = 75)
	public String getLocationName() {
		return this.locationName;
	}

	/**
	 * Method used to set locationName.
	 * @param locationName
	 */
	public void setLocationName(String locationName) {
		this.locationName = locationName;
	}

	/**
	 * Method used to get state.
	 * @return state.
	 */
	@Column(name = "state", nullable = true, length = 75)
	public String getState() {
		return this.state;
	}

	/**
	 * Method used to set state.
	 * @param state
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * Method used to get city.
	 * @return city.
	 */
	@Column(name = "city", nullable = true, length = 75)
	public String getCity() {
		return this.city;
	}

	/**
	 * Method used to set city.
	 * @param city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Method used to get office.
	 * @return the office.
	 */
	public String getOffice() {
		return office;
	}

	/**
	 * Method used to set office.
	 * @param office.
	 */
	public void setOffice(String office) {
		this.office = office;
	}

}