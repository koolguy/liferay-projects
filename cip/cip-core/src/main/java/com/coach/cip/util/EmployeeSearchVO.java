
package com.coach.cip.util;


/**
 * Class is used to capture the characteristics of employeeSearchVO.
 * This is a reusable class for employee search. 
 * @author GalaxE.
 *
 */
public class EmployeeSearchVO implements Comparable<EmployeeSearchVO>{
	
	private String empId;
	private String firstName;
	private String lastName;
	private String department;
	private String email;
	private String title;
	private String office;
	private String phone;
	private String phoneextn;
	
	public String getPhoneextn() {
		return phoneextn;
	}
	public void setPhoneextn(String phoneextn) {
		this.phoneextn = phoneextn;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getOffice() {
		return office;
	}
	public void setOffice(String office) {
		this.office = office;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	
	
	public int compareTo(EmployeeSearchVO otherEmployeeSearchVO) {

		 return  this.firstName.compareTo(otherEmployeeSearchVO.getFirstName());

	}

}
