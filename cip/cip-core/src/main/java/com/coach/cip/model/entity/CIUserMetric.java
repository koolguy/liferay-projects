package com.coach.cip.model.entity;

import java.sql.Time;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * Class is used to capture the characteristics of user metric.
 * This is a reusable class for user metric. 
 * @author GalaxE.
 */
@Entity
@Table(name="ci_usermetric")

public class CIUserMetric  implements java.io.Serializable {


	 private static final long serialVersionUID = -3411272564672310223L;
	 // Fields    

     private Long usermetricId;
     private User user;
     private String url;
     private Time startTime;


    // Constructors

    /** default constructor */
    public CIUserMetric() {
    }

    
    /** full constructor */
    public CIUserMetric(Long usermetricId, User user, String url, Time startTime) {
        this.usermetricId = usermetricId;
        this.user = user;
        this.url = url;
        this.startTime = startTime;
    }

   
    // Property accessors
    @Id 
    
    @Column(name="usermetricId", unique=true, nullable=false)

    public Long getUsermetricId() {
        return this.usermetricId;
    }
    
    public void setUsermetricId(Long usermetricId) {
        this.usermetricId = usermetricId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="userId", nullable=false)

    public User getUser() {
        return this.user;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    
    @Column(name="url", nullable=false, length=512)

    public String getUrl() {
        return this.url;
    }
    
    public void setUrl(String url) {
        this.url = url;
    }
    
    @Column(name="startTime", nullable=false, length=0)

    public Time getStartTime() {
        return this.startTime;
    }
    
    public void setStartTime(Time startTime) {
        this.startTime = startTime;
    }
   








}