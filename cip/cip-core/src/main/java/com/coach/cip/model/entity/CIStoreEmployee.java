package com.coach.cip.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * Class is used to capture the characteristics of store employee.
 * This is a reusable class for store employee. 
 * @author GalaxE.
 */
@Entity
@Table(name="ci_storeemployee")

public class CIStoreEmployee  implements java.io.Serializable {


	private static final long serialVersionUID = -4019219607257299352L;
	// Fields    

     private Long storeEmployeeId;
     private CIStore CIStore;
     private User user;
     private String employeeType;


    // Constructors

    /** default constructor */
    public CIStoreEmployee() {
    }

	/** minimal constructor */
    public CIStoreEmployee(Long storeEmployeeId, CIStore CIStore, User user) {
        this.storeEmployeeId = storeEmployeeId;
        this.CIStore = CIStore;
        this.user = user;
    }
    
    /** full constructor */
    public CIStoreEmployee(Long storeEmployeeId, CIStore CIStore, User user, String employeeType) {
        this.storeEmployeeId = storeEmployeeId;
        this.CIStore = CIStore;
        this.user = user;
        this.employeeType = employeeType;
    }

   
    // Property accessors
    @Id 
    
    @Column(name="storeEmployeeId", unique=true, nullable=false)

    public Long getStoreEmployeeId() {
        return this.storeEmployeeId;
    }
    
    public void setStoreEmployeeId(Long storeEmployeeId) {
        this.storeEmployeeId = storeEmployeeId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="storeId", nullable=false)

    public CIStore getCIStore() {
        return this.CIStore;
    }
    
    public void setCIStore(CIStore CIStore) {
        this.CIStore = CIStore;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="employeeId", nullable=false)

    public User getUser() {
        return this.user;
    }
    
    public void setUser(User user) {
        this.user = user;
    }
    
    @Column(name="employeeType", length=20)

    public String getEmployeeType() {
        return this.employeeType;
    }
    
    public void setEmployeeType(String employeeType) {
        this.employeeType = employeeType;
    }
   








}