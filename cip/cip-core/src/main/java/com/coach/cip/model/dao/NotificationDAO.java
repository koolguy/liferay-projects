
package com.coach.cip.model.dao;

import java.util.List;
import org.springframework.stereotype.Component;

import com.coach.cip.model.entity.CINotification;
import com.coach.cip.model.entity.Role;
import com.coach.cip.model.entity.User;

/**
 * Interface NotificationDAO declares DML operations on notification
 * tables.
 * 
 * @author GalaxE.
 */
@Component
public interface NotificationDAO extends GenericDAO<CINotification> {

	/**
	 * retrieves notification.
	 * @return list of CINotifications.
	 */
	List<CINotification> getAllNotifications();

	/**
	 * retrieves notification based on notificationId.
	 * @param notificationId
	 * @return CINotification.
	 */
	CINotification getNotification(Long notificationId);

	/**
	 * deletes notification based on notificationId.
	 * @param notificationId
	 */
	void deleteNotification(Long notificationId);

	/**
	 * Adds a notification.
	 * @param notification
	 */
	void addNotification(CINotification notification);

	/**
	 * retrieving notification based on usedId.
	 * @param userId
	 *            , resultCount
	 * @return list of CINotifications.
	 */
	List<CINotification> getUserMessages(Long userId, int resultCount);

	/**
	 * retrieves roles based on roleId.
	 * @param roleId
	 * @return role.
	 */
	Role getRole(Long roleId);

	/**
	 * retrieves user based on userId.
	 * @param userId
	 * @return user.
	 */
	User getUser(Long userId);

	/**
	 * updates notification.
	 * @param notification
	 */
	void updateNotification(CINotification notification);

}
