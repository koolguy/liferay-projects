package com.coach.cip.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coach.cip.model.dao.FeedbackDAO;
import com.coach.cip.model.entity.CIFeedback;

/**
 * Service implementation class for the FeedbackService.
 *
 * @author GalaxE.
 *
 */
@Service(value = "feedbackService")
public class FeedbackServiceImpl implements FeedbackService {

	/**
	 * Instance variable for FeedbackDAO.
	 */
	@Autowired
	private FeedbackDAO feedbackDAO;

	/**
	 * This method is used for adding the feedback.
	 *
	 * @param cifeedback
	 */
	public void addFeedback(CIFeedback cifeedback) {

		feedbackDAO.addFeedback(cifeedback);
	}
}
