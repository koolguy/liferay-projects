package com.coach.cip.services;
	import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.coach.cip.model.dao.LocationDAO;

	@Service(value="locationService")
	public class LocationServiceImpl<T> implements LocationService<T> {
		
		@Autowired
	private LocationDAO<T> locationDAO;

		public LocationDAO<T> getLocationDAO() {
			return locationDAO;
		}

		public void setLocationDAO(LocationDAO<T> locationDAO) {
			this.locationDAO = locationDAO;
		}
		
		public List<T> getAllLocation(Class<T> classObject, String columnName, Long id){
			return locationDAO.getAllLocation(classObject, columnName, id);
			
		}
		
		public List<T> getAllLocation(Class<T> classObject, String columnName, String name){
			return locationDAO.getAllLocation(classObject, columnName, name);
			
		}

		public List<T> getAllGeography(Class<T> classObject) {
			
			return locationDAO.getAllGeography(classObject);
		}

		public List<String> getAllDistinctAddressLocation(Class<T> classObject, String returnColumnName,
				String columnName, String value) {
			// TODO Auto-generated method stub
			return locationDAO.getAllDistinctAddressLocation(classObject,returnColumnName, columnName,value);
		}

		public List<Object> getGeographyCountry(String columnName, Long id) {
			// TODO Auto-generated method stub
			return locationDAO.getGeographyCountry(columnName,id);
		}

	}


