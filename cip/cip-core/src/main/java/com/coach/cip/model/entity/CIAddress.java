package com.coach.cip.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * Class is used to capture the characteristics of ci_address. 
 * This is reusable class for ci_address. 
 * @author GalaxE.
 */
@Entity
@Table(name="ci_address")

public class CIAddress  implements java.io.Serializable {	
	
	 private static final long serialVersionUID = -8883736338826801165L;
	 //Field
	 private Long addressId;
     private CIStore CIStore;
     private String addressLine1;
     private String addressLine2;
     private String city;
     private String zip;
     private Long stateId;


    // Constructors

    /** default constructor */
    public CIAddress() {
    }

	/** minimal constructor */
    public CIAddress(Long addressId, CIStore CIStore, String addressLine1, String city, String zip, Long stateId) {
        this.addressId = addressId;
        this.CIStore = CIStore;
        this.addressLine1 = addressLine1;
        this.city = city;
        this.zip = zip;
        this.stateId = stateId;
    }
    
    /** full constructor */
    public CIAddress(Long addressId, CIStore CIStore, String addressLine1, String addressLine2, String city, String zip, Long stateId) {
        this.addressId = addressId;
        this.CIStore = CIStore;
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.city = city;
        this.zip = zip;
        this.stateId = stateId;
    }

   
    // Property accessors
    @Id 
    
    @Column(name="addressId", unique=true, nullable=false)

    public Long getAddressId() {
        return this.addressId;
    }
    
    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="storeId", nullable=false)

    public CIStore getCIStore() {
        return this.CIStore;
    }
    
    public void setCIStore(CIStore CIStore) {
        this.CIStore = CIStore;
    }
    
    @Column(name="addressLine1", nullable=false, length=100)

    public String getAddressLine1() {
        return this.addressLine1;
    }
    
    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }
    
    @Column(name="addressLine2", length=100)

    public String getAddressLine2() {
        return this.addressLine2;
    }
    
    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }
    
    @Column(name="city", nullable=false, length=50)

    public String getCity() {
        return this.city;
    }
    
    public void setCity(String city) {
        this.city = city;
    }
    
    @Column(name="zip", nullable=false, length=10)

    public String getZip() {
        return this.zip;
    }
    
    public void setZip(String zip) {
        this.zip = zip;
    }
    
    @Column(name="StateId", nullable=false)

    public Long getStateId() {
        return this.stateId;
    }
    
    public void setStateId(Long stateId) {
        this.stateId = stateId;
    }
   
}