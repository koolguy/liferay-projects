
package com.coach.cip.util;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * Comparator utility class
 * @author GalaxE.
 */

@SuppressWarnings("rawtypes")
public final class CoachComparator implements Comparator {

	/** The Constant logger. */
	private static final Log logger = LogFactoryUtil.getLog(CoachComparator.class);

	// Class Members
	public static final int EQUAL = 0;

	public static final int LESS_THAN = -1;

	public static final int GREATER_THAN = 1;

	// Object Members
	private Collection collection;

	private String methodName;

	private boolean sortAsc;

	private boolean caseSensitve;

	private CoachComparator(Collection collection, String methodName, boolean sortAsc, boolean caseSensitve) {

		this.collection = collection;
		this.methodName = methodName;
		this.sortAsc = sortAsc;
		this.caseSensitve = caseSensitve;
	}

	/**
	 * Sort method to sort list in ascending order. Include option for case
	 * sensitivity.
	 * @param list
	 * @param methodName
	 * @param sortAsc
	 * @param caseSensitve
	 */
	@SuppressWarnings("unchecked")
	public static void sort(List list, String methodName, boolean sortAsc, boolean caseSensitve) {
		LoggerUtil.debugLogger(logger, "Start of sort() method of CoachComparator::");
		Collections.sort(list, new CoachComparator(list, methodName, sortAsc, caseSensitve));
		LoggerUtil.debugLogger(logger, "End of sort() method of CoachComparator::");
	}

	/**
	 * Sort method to sort list in ascending order.
	 * @param list
	 * @param methodName
	 * @param sortAsc
	 */
	public static void sort(List list, String methodName, boolean sortAsc) {

		LoggerUtil.debugLogger(logger, "Start of sort() method of CoachComparator::");
		sort(list, methodName, sortAsc, true);
		LoggerUtil.debugLogger(logger, "End of sort() method of CoachComparator::");
	}

	/**
	 * Overridden Compare method for Collection.sort().
	 * @param o1
	 * @param o2
	 * @return int
	 */
	public int compare(Object o1, Object o2) {

		int rv = 0;
		try {
			// Determine the return type
			Method method = o1.getClass().getMethod(methodName, null);
			Class c = method.getReturnType();
			// Invoke method to gather two comparable objects
			java.lang.Object result1 = method.invoke(o1, null);
			java.lang.Object result2 = method.invoke(o2, null);
			if (c.isAssignableFrom(Class.forName("java.util.Comparator"))) {
				java.util.Comparator c1 = (java.util.Comparator) result1;
				java.util.Comparator c2 = (java.util.Comparator) result2;
				rv = c1.compare(c1, c2);
			}
			else if (Class.forName("java.lang.Comparable").isAssignableFrom(c)) {
				java.lang.Comparable c1;
				java.lang.Comparable c2;
				if (!caseSensitve) {
					c1 = (java.lang.Comparable) result1.toString().toUpperCase();
					c2 = (java.lang.Comparable) result2.toString().toUpperCase();
				}
				else {
					c1 = (java.lang.Comparable) result1;
					c2 = (java.lang.Comparable) result2;
				}
				rv = c1.compareTo(c2);
			}
			else if (c.isPrimitive()) {
				long f1 = ((Number) result1).longValue();
				long f2 = ((Number) result2).longValue();
				if (f1 == f2)
					rv = EQUAL;
				else if (f1 < f2)
					rv = LESS_THAN;
				else if (f1 > f2)
					rv = GREATER_THAN;
			}
			else {
				throw new RuntimeException("CoachComparator does not currently support ''" + c.getName() + "''!");
			}
		}
		catch (Exception e) {

			LoggerUtil.errorLogger(logger, "Error occured in compare() of CoachComparator", e);
		}
		return rv * getSortOrder();
	}

	/**
	 * Helper method to get the sort order.
	 * @return int
	 */
	private int getSortOrder() {

		return sortAsc ? 1 : -1;
	}
}
