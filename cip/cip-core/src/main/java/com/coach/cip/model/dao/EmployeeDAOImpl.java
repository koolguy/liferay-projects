
package com.coach.cip.model.dao;

import java.util.List;
import org.springframework.stereotype.Repository;
import com.coach.cip.model.entity.CIEmployee;
import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * Class provides the implementations of EmployeeDAO Interface to perform DML
 * operations.
 * 
 * @author GalaxE.
 */
@Repository(value = "employeeDao")
public class EmployeeDAOImpl extends AbstractDAO<CIEmployee> implements EmployeeDAO {

	private static final com.liferay.portal.kernel.log.Log logger = LogFactoryUtil.getLog(EmployeeDAOImpl.class);
	
	/**
	 * Retrieves List of Employees
	 * @return List<CIEmployee>
	 */
	public List<CIEmployee> getEmployees() {
		LoggerUtil.debugLogger(logger, "getEmployees() Method Starts in EmployeeDAOImpl Class.");
		List<CIEmployee> ciEmployee = null;
		ciEmployee = findAll(CIEmployee.class);
		LoggerUtil.debugLogger(logger, "getEmployees() Method Ends in EmployeeDAOImpl Class.");
		return ciEmployee;

	}

	/**
	 * Finds Employee by userId.
	 * @param userId
	 * @return CIEmployee
	 */
	public CIEmployee findEmployeeByUserId(long userId) {
		LoggerUtil.debugLogger(logger, "findEmployeeByUserId() Method Starts in EmployeeDAOImpl Class.");
		CIEmployee ciEmployee = null;
		ciEmployee = findById(CIEmployee.class, userId);
		LoggerUtil.debugLogger(logger, "findEmployeeByUserId() Method Ends in EmployeeDAOImpl Class.");
		return ciEmployee;
	}

}
