
package com.coach.cip.audit.util;

import java.util.Iterator;
import java.util.List;

import com.liferay.portal.kernel.audit.AuditMessage;
import com.liferay.portal.kernel.audit.AuditRequestThreadLocal;
import com.liferay.portal.kernel.json.JSONArray;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.security.auth.CompanyThreadLocal;
import com.liferay.portal.security.auth.PrincipalThreadLocal;
import com.liferay.portal.util.PortalUtil;

/**
 * Class for AttributeMessageBuilder.
 * @author GalaxE.
 */

public class AuditMessageBuilder {

	public AuditMessageBuilder() {

	}

	/**
	 * Builds Audit Message.
	 * @param eventType
	 * @param className
	 * @param classPK
	 * @param attributes
	 * @return AuditMessage
	 */
	@SuppressWarnings("rawtypes")
	public static AuditMessage buildAuditMessage(String eventType, String className, long classPK, List attributes) {

		long companyId = CompanyThreadLocal.getCompanyId().longValue();
		long userId = 0L;
		if (PrincipalThreadLocal.getName() != null) {
			userId = GetterUtil.getLong(PrincipalThreadLocal.getName());
		}
		AuditRequestThreadLocal auditRequestThreadLocal = AuditRequestThreadLocal.getAuditThreadLocal();
		long realUserId = auditRequestThreadLocal.getRealUserId();
		String realUserName = PortalUtil.getUserName(realUserId, "");
		JSONObject additionalInfo = JSONFactoryUtil.createJSONObject();
		if (realUserId > 0L && userId != realUserId) {
			additionalInfo.put("doAsUserId", String.valueOf(userId));
			additionalInfo.put("doAsUserName", PortalUtil.getUserName(userId, ""));
		}
		if (attributes != null) {
			additionalInfo.put("attributes", _getAttributesJSON(attributes));
		}
		return new AuditMessage(eventType, companyId, realUserId, realUserName, className, String.valueOf(classPK), null, additionalInfo);
	}

	/**
	 * Helper method that takes list of attributes and after processing list
	 * elements it returns JSONArray object
	 * @param eventType
	 * @param className
	 * @param classPK
	 * @param attributes
	 * @return AuditMessage
	 */
	@SuppressWarnings("rawtypes")
	private static JSONArray _getAttributesJSON(List attributes) {

		JSONArray jsonArray = JSONFactoryUtil.createJSONArray();
		JSONObject attributeJSONObject;
		for (Iterator iterator = attributes.iterator(); iterator.hasNext(); jsonArray.put(attributeJSONObject)) {
			Attribute attribute = (Attribute) iterator.next();
			attributeJSONObject = JSONFactoryUtil.createJSONObject();
			attributeJSONObject.put("Name", attribute.getName());
			attributeJSONObject.put("Value", attribute.getNewValue());
		}
		return jsonArray;
	}
}