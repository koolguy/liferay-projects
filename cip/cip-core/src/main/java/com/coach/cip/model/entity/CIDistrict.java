package com.coach.cip.model.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * Class is used to capture the characteristics of ci_district. 
 * This is reusable class for ci_district.
 * @author GalaxE
 */
@Entity
@Table(name="ci_district")

public class CIDistrict  implements java.io.Serializable {


	private static final long serialVersionUID = -7548541367606920331L;
    // Fields    
    
	 private Long districtId;
     private CIState CIState;
     private Long parentDistrictId;
     private String districtName;
     private Set<CIStore> CIStores = new HashSet<CIStore>(0);
     private Set<CIDistrictContact> CIDistrictContacts = new HashSet<CIDistrictContact>(0);


    // Constructors

    /** default constructor */
    public CIDistrict() {
    }

	/** minimal constructor */
    public CIDistrict(Long districtId, CIState CIState) {
        this.districtId = districtId;
        this.CIState = CIState;
    }
    
    /** full constructor */
    public CIDistrict(Long districtId, CIState CIState, Long parentDistrictId, String districtName, Set<CIStore> CIStores, Set<CIDistrictContact> CIDistrictContacts) {
        this.districtId = districtId;
        this.CIState = CIState;
        this.parentDistrictId = parentDistrictId;
        this.districtName = districtName;
        this.CIStores = CIStores;
        this.CIDistrictContacts = CIDistrictContacts;
    }

   
    // Property accessors
    @Id 
    
    @Column(name="districtId", unique=true, nullable=false)

    public Long getDistrictId() {
        return this.districtId;
    }
    
    public void setDistrictId(Long districtId) {
        this.districtId = districtId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="stateId", nullable=false)

    public CIState getCIState() {
        return this.CIState;
    }
    
    public void setCIState(CIState CIState) {
        this.CIState = CIState;
    }
    
    @Column(name="parentDistrictId")

    public Long getParentDistrictId() {
        return this.parentDistrictId;
    }
    
    public void setParentDistrictId(Long parentDistrictId) {
        this.parentDistrictId = parentDistrictId;
    }
    
    @Column(name="districtName", length=100)

    public String getDistrictName() {
        return this.districtName;
    }
    
    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER, mappedBy="CIDistrict")

    public Set<CIStore> getCIStores() {
        return this.CIStores;
    }
    
    public void setCIStores(Set<CIStore> CIStores) {
        this.CIStores = CIStores;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER, mappedBy="CIDistrict")

    public Set<CIDistrictContact> getCIDistrictContacts() {
        return this.CIDistrictContacts;
    }
    
    public void setCIDistrictContacts(Set<CIDistrictContact> CIDistrictContacts) {
        this.CIDistrictContacts = CIDistrictContacts;
    }
   








}