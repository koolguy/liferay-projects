package com.coach.cip.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


/**
 * Class is used to capture the characteristics of ci_districtcontact. 
 * This is reusable class for ci_districtcontact. 
 * @author GalaxE
 */
@Entity
@Table(name="ci_districtcontact")

public class CIDistrictContact  implements java.io.Serializable {


	 private static final long serialVersionUID = 7717471589492577296L;
    // Fields  
     
	 private Long contactDistrictId;
     private CIDistrict CIDistrict;
     private CIContact CIContact;


    // Constructors

    /** default constructor */
    public CIDistrictContact() {
    }

    
    /** full constructor */
    public CIDistrictContact(Long contactDistrictId, CIDistrict CIDistrict, CIContact CIContact) {
        this.contactDistrictId = contactDistrictId;
        this.CIDistrict = CIDistrict;
        this.CIContact = CIContact;
    }

   
    // Property accessors
    @Id 
    
    @Column(name="contactDistrictId", unique=true, nullable=false)

    public Long getContactDistrictId() {
        return this.contactDistrictId;
    }
    
    public void setContactDistrictId(Long contactDistrictId) {
        this.contactDistrictId = contactDistrictId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="districtId", nullable=false)

    public CIDistrict getCIDistrict() {
        return this.CIDistrict;
    }
    
    public void setCIDistrict(CIDistrict CIDistrict) {
        this.CIDistrict = CIDistrict;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="contactId", nullable=false)

    public CIContact getCIContact() {
        return this.CIContact;
    }
    
    public void setCIContact(CIContact CIContact) {
        this.CIContact = CIContact;
    }
   








}