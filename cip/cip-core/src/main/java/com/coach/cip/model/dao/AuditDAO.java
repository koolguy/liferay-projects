package com.coach.cip.model.dao;

import org.springframework.stereotype.Component;
import com.coach.cip.model.entity.Audit;

/**
 * 
 * Class used to perform AuditOperations.
 * 
 * @author GalaxE.
 */

@Component
public interface AuditDAO extends GenericDAO<Audit> {

	/**
	 * Method to save audit.
	 * 
	 * @param audit
	 * @throws Exception
	 */
	public void createAudit(Audit audit) throws Exception;
	
	/**
	 * Method to get max auditEventId.
	 * @return
	 * @throws Exception
	 */
	public long getMaxAuditEventIdFromAuditEntity() throws Exception;
}
