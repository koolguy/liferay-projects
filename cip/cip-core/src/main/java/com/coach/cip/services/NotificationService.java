package com.coach.cip.services;

import java.util.List;
import com.coach.cip.model.entity.CINotification;
import com.coach.cip.model.entity.Role;
import com.coach.cip.model.entity.User;

/**
 * Service interface for the notifications.
 * 
 * @author GalaxE.
 * 
 */
public interface NotificationService {

	/**
	 * retrieves notification.
	 * 
	 * @return list of CINotification.
	 */
	public List<CINotification> getAllNotifications();

	/**
	 * retrieves notification based on notificationId.
	 * 
	 * @param notificationId
	 * @return CINotification.
	 */
	public CINotification getNotification(Long notificationId);

	/**
	 * deletes notification based on notificationId.
	 * 
	 * @param notificationId
	 */
	public void deleteNotification(Long notificationId);

	/**
	 * Adds a notification
	 * 
	 * @param notification
	 */
	public void addNotification(CINotification notification);

	/**
	 * retrieving notification based on usedId.
	 * 
	 * @param userId, resultCount
	 * @return list of CINotifications.
	 */
	public List<CINotification> getUserMessages(Long userId, int resultCount);

	/**
	 * retrieves roles based on roleId.
	 * 
	 * @param roleId
	 * @return role.
	 */
	public Role getRole(Long roleId);

	/**
	 * retrieves user based on userId.
	 * 
	 * @param userId
	 * @return user.
	 */
	public User getUser(Long userId);

	/**
	 * updates notification.
	 * 
	 * @param notification
	 */
	public void updateNotification(CINotification notification);

}
