package com.coach.cip.model.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * Class is used to capture the characteristics of ci_region.
 * This is a reusable class for ci_region.
 * @author GalaxE
 */
@Entity
@Table(name="ci_region")

public class CIRegion  implements java.io.Serializable {

	
	private static final long serialVersionUID = 3855556093080676264L;
    // Fields  
	
	 private Long ciRegionId;
     private CICountry CICountry;
     private String regionName;
     private Set<CIState> CIStates = new HashSet<CIState>(0);


    // Constructors

    /** default constructor */
    public CIRegion() {
    }

	/** minimal constructor */
    public CIRegion(Long ciRegionId, CICountry CICountry) {
        this.ciRegionId = ciRegionId;
        this.CICountry = CICountry;
    }
    
    /** full constructor */
    public CIRegion(Long ciRegionId, CICountry CICountry, String regionName, Set<CIState> CIStates) {
        this.ciRegionId = ciRegionId;
        this.CICountry = CICountry;
        this.regionName = regionName;
        this.CIStates = CIStates;
    }
    
    @Id 
    @Column(name="ciRegionId", unique=true, nullable=false)
    public Long getCiRegionId() {
		return ciRegionId;
	}

	public void setCiRegionId(Long ciRegionId) {
		this.ciRegionId = ciRegionId;
	}
   
   
    // Property accessors
    /*@Id 
    
    @Column(name="regionId", unique=true, nullable=false)

    public Long getRegionId() {
        return this.regionId;
    }
    
    public void setRegionId(Long regionId) {
        this.regionId = regionId;
    }*/
	@ManyToOne(fetch=FetchType.EAGER)
        @JoinColumn(name="countryId", nullable=false)

    public CICountry getCICountry() {
        return this.CICountry;
    }
    
    public void setCICountry(CICountry CICountry) {
        this.CICountry = CICountry;
    }
    
    @Column(name="regionName", length=100)

    public String getRegionName() {
        return this.regionName;
    }
    
    public void setRegionName(String regionName) {
        this.regionName = regionName;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER, mappedBy="CIRegion")

    public Set<CIState> getCIStates() {
        return this.CIStates;
    }
    
    public void setCIStates(Set<CIState> CIStates) {
        this.CIStates = CIStates;
    }

}