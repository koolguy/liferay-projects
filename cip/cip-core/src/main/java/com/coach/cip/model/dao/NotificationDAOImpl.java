
package com.coach.cip.model.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.coach.cip.model.entity.CINotification;
import com.coach.cip.model.entity.Role;
import com.coach.cip.model.entity.User;
import com.coach.cip.util.LoggerUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.Validator;

/**
 * Class provides the implementations of notification DAO's needed to
 * perform DML operations.
 * 
 * @author GalaxE.
 */
@Repository(value = "notificationDAO")
@Transactional
public class NotificationDAOImpl extends AbstractDAO<CINotification> implements NotificationDAO {

	private static final Log logger = LogFactoryUtil.getLog(NotificationDAOImpl.class);

	/**
	 * retrieves notification.
	 * @return list of CINotifications.
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<CINotification> getAllNotifications() {

		LoggerUtil.debugLogger(logger, "Start of getAllNotifications() method of NotificationDAOImpl:: ");
		List<CINotification> list = null;
		list = getSession().createCriteria(CINotification.class).list();
		Iterator it = list.iterator();
		while (it.hasNext()) {
			CINotification notification = (CINotification) it.next();
			Hibernate.initialize(notification.getUserByCreatedBy());
			Hibernate.initialize(notification.getUserByModifiedBy());
			Hibernate.initialize(notification.getNotificationForLocation());
			Hibernate.initialize(notification.getNotificationForRoles());
			Hibernate.initialize(notification.getNotificationForUsers());
		}
		LoggerUtil.debugLogger(logger, "End of getAllNotifications() method of NotificationDAOImpl:: ");
		return list;
	}

	/**
	 * retrieves notification based on messageId.
	 * @param notificationId
	 * @return CINotification.
	 */
	public CINotification getNotification(Long notificationId) {

		LoggerUtil.debugLogger(logger, "Start of getNotification() method of NotificationDAOImpl:: ");
		CINotification notification = findById(CINotification.class, notificationId);
		Hibernate.initialize(notification.getNotificationForLocation());
		Hibernate.initialize(notification.getNotificationForRoles());
		LoggerUtil.debugLogger(logger, "End of getNotification() method of NotificationDAOImpl:: ");
		return notification;
	}

	/**
	 * deletes notification based on notificationId.
	 * @param notificationId
	 */
	public void deleteNotification(Long notificationId) {

		LoggerUtil.debugLogger(logger, "Start of deleteNotification() method of NotificationDAOImpl:: ");
		deleteById(CINotification.class, notificationId);
		LoggerUtil.debugLogger(logger, "End of deleteNotification() method of NotificationDAOImpl:: ");
	}

	/**
	 * Adds a notification
	 * @param notification
	 */
	public void addNotification(CINotification notification) {

		LoggerUtil.debugLogger(logger, "Start of addNotification() method of NotificationDAOImpl:: ");
		Session session = getSession();
		session.saveOrUpdate(notification);
		session.flush();
		LoggerUtil.debugLogger(logger, "End of addNotification() method of NotificationDAOImpl:: ");
	}

	/**
	 * retrieving notification based on usedId.
	 * @param userId
	 *            , resultCount
	 * @return list of CINotifications.
	 */
	@SuppressWarnings("unchecked")
	public List<CINotification> getUserMessages(Long userId, int resultCount) {

		LoggerUtil.debugLogger(logger, "Start of getUserMessages() method of NotificationDAOImpl:: ");
		List<CINotification> notificationList = null;

		try {
			Session session = hibernateTemplate.getSessionFactory().getCurrentSession();
			notificationList =
				session.getNamedQuery("callPersonalMessageUsersProcedure").setParameter("userId", userId).setParameter("resultCount", resultCount).list();
		    if(Validator.isNull(notificationList)){
		    	notificationList = new ArrayList<CINotification>();
		    }
		}
		catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "NotificationDAOImpl.getUserMessages() HibernateException ::", e);
			throw e;
		}
		LoggerUtil.debugLogger(logger, "End of getUserMessages() method of NotificationDAOImpl:: ");
		return notificationList;
	}

	/**
	 * retrieves roles based on roleId.
	 * @param roleId
	 * @return role.
	 */
	public Role getRole(Long roleId) {

		LoggerUtil.debugLogger(logger, "Start of getRole() method of NotificationDAOImpl:: ");
		Role role = null;
		try {
			role = (Role) hibernateTemplate.get(Role.class, roleId);

		}
		catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "NotificationDAOImpl.getRole() HibernateException ::", e);
			throw e;
		}
		LoggerUtil.debugLogger(logger, "End of getRole() method of NotificationDAOImpl:: ");
		return role;

	}

	/**
	 * retrieves user based on userId.
	 * @param userId
	 * @return user.
	 */
	public User getUser(Long userId) {

		LoggerUtil.debugLogger(logger, "Start of getUser() method of NotificationDAOImpl:: ");
		User user = null;
		try {
			user = (User) hibernateTemplate.get(User.class, userId);

		}
		catch (HibernateException e) {
			LoggerUtil.errorLogger(logger, "NotificationDAOImpl.getUser() HibernateException ::", e);
			throw e;
		}
		LoggerUtil.debugLogger(logger, "End of getUser() method of NotificationDAOImpl:: ");
		return user;
	}

	/**
	 * updates notification.
	 * @param notification
	 */
	public void updateNotification(CINotification notification) {

		LoggerUtil.debugLogger(logger, "Start of updateNotification() method of NotificationDAOImpl:: ");
		if (notification != null) {
			hibernateTemplate.update(notification);
		}
		LoggerUtil.debugLogger(logger, "End of updateNotification() method of NotificationDAOImpl:: ");
	}

}
