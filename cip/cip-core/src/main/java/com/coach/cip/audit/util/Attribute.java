
package com.coach.cip.audit.util;

/**
 * Attribute Class for audit purpose
 * @author GalaxE.
 */
public class Attribute {

	private String _name;
	private String _newValue;
	private String _oldValue;

	public Attribute(String name) {

		this(name, "", "");
	}

	public Attribute(String name, String newValue, String oldValue) {

		_name = name;
		_newValue = newValue;
		_oldValue = oldValue;
	}

	public Attribute(String name, String newValue) {

		_name = name;
		_newValue = newValue;
	}

	public String getName() {

		return _name;
	}

	public String getNewValue() {

		return _newValue;
	}

	public String getOldValue() {

		return _oldValue;
	}
}
