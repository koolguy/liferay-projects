package com.coach.cip.model.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * Class is used to capture the characteristics of ci_store.
 * This is a reusable class for ci_store. 
 * @author GalaxE.
 */
@Entity
@Table(name="ci_store")

public class CIStore  implements java.io.Serializable {

	
	 private static final long serialVersionUID = -8361952644209535304L;
	// Fields    

     private Long storeId;
     private CIDistrict CIDistrict;
     private String storeNumber;
     private String storeName;
     private String vm;
     private String storeType;
     private Set<CIAddress> CIAddresses = new HashSet<CIAddress>(0);
     private Set<CIStoreTiming> CIStoreTimings = new HashSet<CIStoreTiming>(0);
     private Set<CIStoreEmployee> CIStoreEmployees = new HashSet<CIStoreEmployee>(0);
     private Set<CIStoreContact> CIStoreContacts = new HashSet<CIStoreContact>(0);


    // Constructors

    /** default constructor */
    public CIStore() {
    }

	/** minimal constructor */
    public CIStore(Long storeId, CIDistrict CIDistrict) {
        this.storeId = storeId;
        this.CIDistrict = CIDistrict;
    }
    
    /** full constructor */
    public CIStore(Long storeId, CIDistrict CIDistrict, String storeNumber, String storeName, String vm, String storeType, Set<CIAddress> CIAddresses, Set<CIStoreTiming> CIStoreTimings, Set<CIStoreEmployee> CIStoreEmployees, Set<CIStoreContact> CIStoreContacts) {
        this.storeId = storeId;
        this.CIDistrict = CIDistrict;
        this.storeNumber = storeNumber;
        this.storeName = storeName;
        this.vm = vm;
        this.storeType = storeType;
        this.CIAddresses = CIAddresses;
        this.CIStoreTimings = CIStoreTimings;
        this.CIStoreEmployees = CIStoreEmployees;
        this.CIStoreContacts = CIStoreContacts;
    }

   
    // Property accessors
    @Id 
    
    @Column(name="storeId", unique=true, nullable=false)

    public Long getStoreId() {
        return this.storeId;
    }
    
    public void setStoreId(Long storeId) {
        this.storeId = storeId;
    }
	@ManyToOne(fetch=FetchType.LAZY)
        @JoinColumn(name="districtId", nullable=false)

    public CIDistrict getCIDistrict() {
        return this.CIDistrict;
    }
    
    public void setCIDistrict(CIDistrict CIDistrict) {
        this.CIDistrict = CIDistrict;
    }
    
    @Column(name="storeNumber", length=10)

    public String getStoreNumber() {
        return this.storeNumber;
    }
    
    public void setStoreNumber(String storeNumber) {
        this.storeNumber = storeNumber;
    }
    
    @Column(name="storeName", length=100)

    public String getStoreName() {
        return this.storeName;
    }
    
    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }
    
    @Column(name="vm", length=10)

    public String getVm() {
        return this.vm;
    }
    
    public void setVm(String vm) {
        this.vm = vm;
    }
    
    @Column(name="storeType", length=15)

    public String getStoreType() {
        return this.storeType;
    }
    
    public void setStoreType(String storeType) {
        this.storeType = storeType;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER, mappedBy="CIStore")

    public Set<CIAddress> getCIAddresses() {
        return this.CIAddresses;
    }
    
    public void setCIAddresses(Set<CIAddress> CIAddresses) {
        this.CIAddresses = CIAddresses;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER, mappedBy="CIStore")

    public Set<CIStoreTiming> getCIStoreTimings() {
        return this.CIStoreTimings;
    }
    
    public void setCIStoreTimings(Set<CIStoreTiming> CIStoreTimings) {
        this.CIStoreTimings = CIStoreTimings;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER, mappedBy="CIStore")

    public Set<CIStoreEmployee> getCIStoreEmployees() {
        return this.CIStoreEmployees;
    }
    
    public void setCIStoreEmployees(Set<CIStoreEmployee> CIStoreEmployees) {
        this.CIStoreEmployees = CIStoreEmployees;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.EAGER, mappedBy="CIStore")

    public Set<CIStoreContact> getCIStoreContacts() {
        return this.CIStoreContacts;
    }
    
    public void setCIStoreContacts(Set<CIStoreContact> CIStoreContacts) {
        this.CIStoreContacts = CIStoreContacts;
    }
   








}