package com.coach.cip.services;

import java.util.List;
import com.coach.cip.model.entity.CIPersonalMessage;
import com.coach.cip.model.entity.Role;
import com.coach.cip.model.entity.User;

/**
 * Service interface for the personalMessages.
 * 
 * @author GalaxE.
 * 
 */
public interface PersonalMessageService {

	/**
	 * retrieves personalMessages.
	 * 
	 * @return list of CIPersonalMessages.
	 */
	public List<CIPersonalMessage> getAllPersonalMessage();

	/**
	 * retrieves personalMessage based on messageId.
	 * 
	 * @param messageId
	 * @return CIPersonalMessage.
	 */
	public CIPersonalMessage getPersonalMessage(Long messageId);

	/**
	 * deletes personalMessage based on messageId.
	 * 
	 * @param messageId
	 */
	public void deletePersonalMessage(Long messageId);

	/**
	 * Adds a Personal Message
	 * 
	 * @param personalMessage
	 */
	public void addPersonalMessage(CIPersonalMessage personalMessage);

	/**
	 * retrieving personalMessages based on usedId.
	 * 
	 * @param userId, resultCount
	 * @return list of CIPersonalMessages.
	 */
	public List<CIPersonalMessage> getUserMessages(Long userId, int resultCount);

	/**
	 * retrieves roles based on roleId.
	 * 
	 * @param roleId
	 * @return role.
	 */
	public Role getRole(Long roleId);

	/**
	 * retrieves user based on userId.
	 * 
	 * @param userId
	 * @return user.
	 */
	public User getUser(Long userId);

	/**
	 * updates personalMessage.
	 * 
	 * @param personalMessage
	 */
	public void updatePersonalMessage(CIPersonalMessage personalMessage);

}
