package com.coach.cip.model.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


/**
 * Class is used to capture the characteristics of ci_contact. 
 * This is reusable class for ci_contact. 
 * @author GalaxE.
 */
@Entity
@Table(name="ci_contact")

public class CIContact  implements java.io.Serializable {
	 
	 private static final long serialVersionUID = -768766591565957216L;
	 //Field
	 private Long contactId;
     private String contactType;
     private String contactNo;
     private Set<CIDistrictContact> CIDistrictContacts = new HashSet<CIDistrictContact>(0);
     private Set<CIStoreContact> CIStoreContacts = new HashSet<CIStoreContact>(0);


    // Constructors

    /** default constructor */
    public CIContact() {
    }

	/** minimal constructor */
    public CIContact(Long contactId, String contactType, String contactNo) {
        this.contactId = contactId;
        this.contactType = contactType;
        this.contactNo = contactNo;
    }
    
    /** full constructor */
    public CIContact(Long contactId, String contactType, String contactNo, Set<CIDistrictContact> CIDistrictContacts, Set<CIStoreContact> CIStoreContacts) {
        this.contactId = contactId;
        this.contactType = contactType;
        this.contactNo = contactNo;
        this.CIDistrictContacts = CIDistrictContacts;
        this.CIStoreContacts = CIStoreContacts;
    }

   
    // Property accessors
    @Id 
    
    @Column(name="contactId", unique=true, nullable=false)

    public Long getContactId() {
        return this.contactId;
    }
    
    public void setContactId(Long contactId) {
        this.contactId = contactId;
    }
    
    @Column(name="contactType", nullable=false, length=15)

    public String getContactType() {
        return this.contactType;
    }
    
    public void setContactType(String contactType) {
        this.contactType = contactType;
    }
    
    @Column(name="contactNo", nullable=false, length=30)

    public String getContactNo() {
        return this.contactNo;
    }
    
    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="CIContact")

    public Set<CIDistrictContact> getCIDistrictContacts() {
        return this.CIDistrictContacts;
    }
    
    public void setCIDistrictContacts(Set<CIDistrictContact> CIDistrictContacts) {
        this.CIDistrictContacts = CIDistrictContacts;
    }
@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="CIContact")

    public Set<CIStoreContact> getCIStoreContacts() {
        return this.CIStoreContacts;
    }
    
    public void setCIStoreContacts(Set<CIStoreContact> CIStoreContacts) {
        this.CIStoreContacts = CIStoreContacts;
    }
   








}