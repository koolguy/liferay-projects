package com.coach.stores.service.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getOpenStoreByGeographyRequest element declaration.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;element name="getOpenStoreByGeographyRequest">
 *   &lt;complexType>
 *     &lt;complexContent>
 *       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *         &lt;sequence>
 *           &lt;element name="GeographyName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;/sequence>
 *       &lt;/restriction>
 *     &lt;/complexContent>
 *   &lt;/complexType>
 * &lt;/element>
 * </pre>
 * 
 * 
 */

/**
 * @author GalaxE.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "geographyName"
})
@XmlRootElement(name = "getOpenStoreByGeographyRequest")
public class GetOpenStoreByGeographyRequest {

    @XmlElement(name = "GeographyName", required = true)
    protected String geographyName;

    /**
     * Gets the value of the geographyName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeographyName() {
        return geographyName;
    }

    /**
     * Sets the value of the geographyName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeographyName(String value) {
        this.geographyName = value;
    }

}
