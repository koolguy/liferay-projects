package com.coach.stores.service.beans;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/*import com.coach.stores.service.beans.GetStoreDateHistoryResponse.StoresHistory;
import com.coach.stores.service.beans.GetStoreDateHistoryResponse.StoresHistory.StoreHistory;*/


/**
 * <p>Java class for getStoreDateHistoryResponse element declaration.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;element name="getStoreDateHistoryResponse">
 *   &lt;complexType>
 *     &lt;complexContent>
 *       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *         &lt;sequence>
 *           &lt;element name="Stores_History">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="Store_History" maxOccurs="unbounded">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="STORE_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="STORE_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="DATE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="START_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="END_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/restriction>
 *     &lt;/complexContent>
 *   &lt;/complexType>
 * &lt;/element>
 * </pre>
 * 
 * 
 */

/**
 * @author GalaxE.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "storesHistory"
})
@XmlRootElement(name = "getStoreDateHistoryResponse")
public class GetStoreDateHistoryResponse {

    @XmlElement(name = "Stores_History", required = true)
    protected StoresHistory storesHistory;

    /**
     * Gets the value of the storesHistory property.
     * 
     * @return
     *     possible object is
     *     {@link StoresHistory }
     *     
     */
    public StoresHistory getStoresHistory() {
        return storesHistory;
    }

    /**
     * Sets the value of the storesHistory property.
     * 
     * @param value
     *     allowed object is
     *     {@link StoresHistory }
     *     
     */
    public void setStoresHistory(StoresHistory value) {
        this.storesHistory = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Store_History" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="STORE_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="STORE_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="START_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="END_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "storeHistory"
    })
    public static class StoresHistory {

        @XmlElement(name = "Store_History", required = true)
        protected List<StoreHistory> storeHistory;

        /**
         * Gets the value of the storeHistory property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the storeHistory property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getStoreHistory().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link StoreHistory }
         * 
         * 
         */
        public List<StoreHistory> getStoreHistory() {
            if (storeHistory == null) {
                storeHistory = new ArrayList<StoreHistory>();
            }
            return this.storeHistory;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="STORE_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="STORE_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="START_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="END_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "storenumber",
            "storename",
            "city",
            "datetype",
            "startdate",
            "enddate"
        })
        public static class StoreHistory {

            @XmlElement(name = "STORE_NUMBER", required = true)
            protected String storenumber;
            @XmlElement(name = "STORE_NAME", required = true)
            protected String storename;
            @XmlElement(name = "CITY", required = true)
            protected String city;
            @XmlElement(name = "DATE_TYPE", required = true)
            protected String datetype;
            @XmlElement(name = "START_DATE", required = true)
            protected String startdate;
            @XmlElement(name = "END_DATE", required = true)
            protected String enddate;

            /**
             * Gets the value of the storenumber property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSTORENUMBER() {
                return storenumber;
            }

            /**
             * Sets the value of the storenumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSTORENUMBER(String value) {
                this.storenumber = value;
            }

            /**
             * Gets the value of the storename property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSTORENAME() {
                return storename;
            }

            /**
             * Sets the value of the storename property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSTORENAME(String value) {
                this.storename = value;
            }

            /**
             * Gets the value of the city property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCITY() {
                return city;
            }

            /**
             * Sets the value of the city property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCITY(String value) {
                this.city = value;
            }

            /**
             * Gets the value of the datetype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATETYPE() {
                return datetype;
            }

            /**
             * Sets the value of the datetype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATETYPE(String value) {
                this.datetype = value;
            }

            /**
             * Gets the value of the startdate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSTARTDATE() {
                return startdate;
            }

            /**
             * Sets the value of the startdate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSTARTDATE(String value) {
                this.startdate = value;
            }

            /**
             * Gets the value of the enddate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getENDDATE() {
                return enddate;
            }

            /**
             * Sets the value of the enddate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setENDDATE(String value) {
                this.enddate = value;
            }

        }

    }

}
