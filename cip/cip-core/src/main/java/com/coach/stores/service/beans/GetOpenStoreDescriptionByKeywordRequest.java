package com.coach.stores.service.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getOpenStoreDescriptionByKeywordRequest element declaration.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;element name="getOpenStoreDescriptionByKeywordRequest">
 *   &lt;complexType>
 *     &lt;complexContent>
 *       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *         &lt;sequence>
 *           &lt;element name="strKeyword" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;/sequence>
 *       &lt;/restriction>
 *     &lt;/complexContent>
 *   &lt;/complexType>
 * &lt;/element>
 * </pre>
 * 
 * 
 */

/**
 * @author GalaxE.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "strKeyword"
})
@XmlRootElement(name = "getOpenStoreDescriptionByKeywordRequest")
public class GetOpenStoreDescriptionByKeywordRequest {

    @XmlElement(required = true)
    protected String strKeyword;

    /**
     * Gets the value of the strKeyword property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStrKeyword() {
        return strKeyword;
    }

    /**
     * Sets the value of the strKeyword property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStrKeyword(String value) {
        this.strKeyword = value;
    }

}
