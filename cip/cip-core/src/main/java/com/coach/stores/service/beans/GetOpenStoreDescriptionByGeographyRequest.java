package com.coach.stores.service.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/*import com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyRequest.Geography;*/


/**
 * <p>Java class for getOpenStoreDescriptionByGeographyRequest element declaration.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;element name="getOpenStoreDescriptionByGeographyRequest">
 *   &lt;complexType>
 *     &lt;complexContent>
 *       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *         &lt;sequence>
 *           &lt;element name="Geography">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="GEOGRAPHY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="PHONE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="VM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="EMAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="STORE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/restriction>
 *     &lt;/complexContent>
 *   &lt;/complexType>
 * &lt;/element>
 * </pre>
 * 
 * 
 */

/**
 * @author GalaxE.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "geography"
})
@XmlRootElement(name = "getOpenStoreDescriptionByGeographyRequest")
public class GetOpenStoreDescriptionByGeographyRequest {

    @XmlElement(name = "Geography", required = true)
    protected Geography geography;

    /**
     * Gets the value of the geography property.
     * 
     * @return
     *     possible object is
     *     {@link Geography }
     *     
     */
    public Geography getGeography() {
        return geography;
    }

    /**
     * Sets the value of the geography property.
     * 
     * @param value
     *     allowed object is
     *     {@link Geography }
     *     
     */
    public void setGeography(Geography value) {
        this.geography = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="GEOGRAPHY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="PHONE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="VM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="EMAIL" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="STORE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "geography",
        "country",
        "state",
        "city",
        "phone",
        "vm",
        "email",
        "storetype"
    })
    public static class Geography {

        @XmlElement(name = "GEOGRAPHY")
        protected String geography;
        @XmlElement(name = "COUNTRY")
        protected String country;
        @XmlElement(name = "STATE")
        protected String state;
        @XmlElement(name = "CITY")
        protected String city;
        @XmlElement(name = "PHONE")
        protected String phone;
        @XmlElement(name = "VM")
        protected String vm;
        @XmlElement(name = "EMAIL")
        protected String email;
        @XmlElement(name = "STORE_TYPE", required = true)
        protected String storetype;

        /**
         * Gets the value of the geography property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGEOGRAPHY() {
            return geography;
        }

        /**
         * Sets the value of the geography property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGEOGRAPHY(String value) {
            this.geography = value;
        }

        /**
         * Gets the value of the country property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCOUNTRY() {
            return country;
        }

        /**
         * Sets the value of the country property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCOUNTRY(String value) {
            this.country = value;
        }

        /**
         * Gets the value of the state property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSTATE() {
            return state;
        }

        /**
         * Sets the value of the state property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSTATE(String value) {
            this.state = value;
        }

        /**
         * Gets the value of the city property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCITY() {
            return city;
        }

        /**
         * Sets the value of the city property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCITY(String value) {
            this.city = value;
        }

        /**
         * Gets the value of the phone property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPHONE() {
            return phone;
        }

        /**
         * Sets the value of the phone property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPHONE(String value) {
            this.phone = value;
        }

        /**
         * Gets the value of the vm property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVM() {
            return vm;
        }

        /**
         * Sets the value of the vm property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVM(String value) {
            this.vm = value;
        }

        /**
         * Gets the value of the email property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEMAIL() {
            return email;
        }

        /**
         * Sets the value of the email property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEMAIL(String value) {
            this.email = value;
        }

        /**
         * Gets the value of the storetype property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSTORETYPE() {
            return storetype;
        }

        /**
         * Sets the value of the storetype property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSTORETYPE(String value) {
            this.storetype = value;
        }

    }

}
