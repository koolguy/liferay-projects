package com.coach.stores.service.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for getStoreDateHistoryRequest element declaration.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;element name="getStoreDateHistoryRequest">
 *   &lt;complexType>
 *     &lt;complexContent>
 *       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *         &lt;sequence>
 *           &lt;element name="intStoreID" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;/sequence>
 *       &lt;/restriction>
 *     &lt;/complexContent>
 *   &lt;/complexType>
 * &lt;/element>
 * </pre>
 * 
 * 
 */

/**
 * @author GalaxE.
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "intStoreID"
})
@XmlRootElement(name = "getStoreDateHistoryRequest")
public class GetStoreDateHistoryRequest {

    protected int intStoreID;

    /**
     * Gets the value of the intStoreID property.
     * 
     */
    public int getIntStoreID() {
        return intStoreID;
    }

    /**
     * Sets the value of the intStoreID property.
     * 
     */
    public void setIntStoreID(int value) {
        this.intStoreID = value;
    }

}
