package com.coach.stores.service.beans;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/*import com.coach.stores.service.beans.GetOpenStoreByStoreNumberResponse.Stores;
import com.coach.stores.service.beans.GetOpenStoreByStoreNumberResponse.Stores.Store;*/


/**
 * <p>Java class for getOpenStoreByStoreNumberResponse element declaration.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;element name="getOpenStoreByStoreNumberResponse">
 *   &lt;complexType>
 *     &lt;complexContent>
 *       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *         &lt;sequence>
 *           &lt;element name="Stores">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="Store" maxOccurs="unbounded" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="STORE_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="STORE_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="STORE_NAME2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="STORE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="STORE_TYPE_DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="STORE_LAT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="STORE_LONGT" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="ADDRESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="ADDRESS2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="ZIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="EMAIL" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="PHONE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="FAX" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="VM" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="HOURS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="HOLIDAY_HOURS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="OPEN_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="CLOSE_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="LAST_REMODELLED_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="STATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="FLOOR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="REGION_DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="REGION_CODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="REGION_MGR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="REGION_MGR_VM" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="DISTRICT_DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="DISTRICT_MGR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="DISTRICT_MGR_VM" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="AREA_MGR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="GENERAL_MANAGER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="MANAGER_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="MANAGER_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="ASSOCIATE_MGR_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="ASSOCIATE_MGR_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="ASSOCIATE_MGR_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="ASSOCIATE_MGR_4" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="ASSOCIATE_MGR_5" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="ASSOCIATE_MGR_6" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="ASSISTANT_MGR_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="ASSISTANT_MGR_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="ASSISTANT_MGR_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="ASSISTANT_MGR_4" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="ASSISTANT_MGR_5" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="ASSISTANT_MGR_6" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="ASSISTANT_MGR_7" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/restriction>
 *     &lt;/complexContent>
 *   &lt;/complexType>
 * &lt;/element>
 * </pre>
 * 
 * 
 */

/**
 * @author GalaxE.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "stores"
})
@XmlRootElement(name = "getOpenStoreByStoreNumberResponse")
public class GetOpenStoreByStoreNumberResponse {

    @XmlElement(name = "Stores", required = true)
    protected Stores stores;

    /**
     * Gets the value of the stores property.
     * 
     * @return
     *     possible object is
     *     {@link Stores }
     *     
     */
    public Stores getStores() {
        return stores;
    }

    /**
     * Sets the value of the stores property.
     * 
     * @param value
     *     allowed object is
     *     {@link Stores }
     *     
     */
    public void setStores(Stores value) {
        this.stores = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Store" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="STORE_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="STORE_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="STORE_NAME2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="STORE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="STORE_TYPE_DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="STORE_LAT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="STORE_LONGT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ADDRESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ADDRESS2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ZIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="EMAIL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PHONE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FAX" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="VM" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="HOURS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="HOLIDAY_HOURS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OPEN_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CLOSE_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LAST_REMODELLED_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="STATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FLOOR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="REGION_DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="REGION_CODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="REGION_MGR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="REGION_MGR_VM" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DISTRICT_DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DISTRICT_MGR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DISTRICT_MGR_VM" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="AREA_MGR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="GENERAL_MANAGER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MANAGER_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MANAGER_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ASSOCIATE_MGR_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ASSOCIATE_MGR_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ASSOCIATE_MGR_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ASSOCIATE_MGR_4" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ASSOCIATE_MGR_5" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ASSOCIATE_MGR_6" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ASSISTANT_MGR_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ASSISTANT_MGR_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ASSISTANT_MGR_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ASSISTANT_MGR_4" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ASSISTANT_MGR_5" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ASSISTANT_MGR_6" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ASSISTANT_MGR_7" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "store"
    })
    public static class Stores {

        @XmlElement(name = "Store", required = true)
        protected List<Store> store;

        /**
         * Gets the value of the store property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the store property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getStore().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Store }
         * 
         * 
         */
        public List<Store> getStore() {
            if (store == null) {
                store = new ArrayList<Store>();
            }
            return this.store;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="STORE_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="STORE_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="STORE_NAME2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="STORE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="STORE_TYPE_DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="STORE_LAT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="STORE_LONGT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ADDRESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ADDRESS2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ZIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="EMAIL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PHONE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FAX" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="VM" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="HOURS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="HOLIDAY_HOURS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OPEN_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CLOSE_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LAST_REMODELLED_DATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="STATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FLOOR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="REGION_DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="REGION_CODE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="REGION_MGR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="REGION_MGR_VM" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DISTRICT_DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DISTRICT_MGR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DISTRICT_MGR_VM" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="AREA_MGR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="GENERAL_MANAGER" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MANAGER_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MANAGER_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ASSOCIATE_MGR_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ASSOCIATE_MGR_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ASSOCIATE_MGR_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ASSOCIATE_MGR_4" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ASSOCIATE_MGR_5" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ASSOCIATE_MGR_6" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ASSISTANT_MGR_1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ASSISTANT_MGR_2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ASSISTANT_MGR_3" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ASSISTANT_MGR_4" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ASSISTANT_MGR_5" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ASSISTANT_MGR_6" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ASSISTANT_MGR_7" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "storenumber",
            "storename",
            "storename2",
            "storetype",
            "storetypedescription",
            "storelat",
            "storelongt",
            "address",
            "address2",
            "city",
            "state",
            "zip",
            "country",
            "email",
            "phone",
            "fax",
            "vm",
            "hours",
            "holidayhours",
            "opendate",
            "closedate",
            "lastremodelleddate",
            "status",
            "floor",
            "regiondescription",
            "regioncode",
            "regionmgr",
            "regionmgrvm",
            "districtdescription",
            "districtmgr",
            "districtmgrvm",
            "areamgr",
            "generalmanager",
            "manager1",
            "manager2",
            "associatemgr1",
            "associatemgr2",
            "associatemgr3",
            "associatemgr4",
            "associatemgr5",
            "associatemgr6",
            "assistantmgr1",
            "assistantmgr2",
            "assistantmgr3",
            "assistantmgr4",
            "assistantmgr5",
            "assistantmgr6",
            "assistantmgr7"
        })
        public static class Store {

            @XmlElement(name = "STORE_NUMBER", required = true)
            protected String storenumber;
            @XmlElement(name = "STORE_NAME", required = true)
            protected String storename;
            @XmlElement(name = "STORE_NAME2", required = true)
            protected String storename2;
            @XmlElement(name = "STORE_TYPE", required = true)
            protected String storetype;
            @XmlElement(name = "STORE_TYPE_DESCRIPTION", required = true)
            protected String storetypedescription;
            @XmlElement(name = "STORE_LAT", required = true)
            protected String storelat;
            @XmlElement(name = "STORE_LONGT", required = true)
            protected String storelongt;
            @XmlElement(name = "ADDRESS", required = true)
            protected String address;
            @XmlElement(name = "ADDRESS2", required = true)
            protected String address2;
            @XmlElement(name = "CITY", required = true)
            protected String city;
            @XmlElement(name = "STATE", required = true)
            protected String state;
            @XmlElement(name = "ZIP", required = true)
            protected String zip;
            @XmlElement(name = "COUNTRY", required = true)
            protected String country;
            @XmlElement(name = "EMAIL", required = true)
            protected String email;
            @XmlElement(name = "PHONE", required = true)
            protected String phone;
            @XmlElement(name = "FAX", required = true)
            protected String fax;
            @XmlElement(name = "VM", required = true)
            protected String vm;
            @XmlElement(name = "HOURS", required = true)
            protected String hours;
            @XmlElement(name = "HOLIDAY_HOURS", required = true)
            protected String holidayhours;
            @XmlElement(name = "OPEN_DATE", required = true)
            protected String opendate;
            @XmlElement(name = "CLOSE_DATE", required = true)
            protected String closedate;
            @XmlElement(name = "LAST_REMODELLED_DATE", required = true)
            protected String lastremodelleddate;
            @XmlElement(name = "STATUS", required = true)
            protected String status;
            @XmlElement(name = "FLOOR", required = true)
            protected String floor;
            @XmlElement(name = "REGION_DESCRIPTION", required = true)
            protected String regiondescription;
            @XmlElement(name = "REGION_CODE", required = true)
            protected String regioncode;
            @XmlElement(name = "REGION_MGR", required = true)
            protected String regionmgr;
            @XmlElement(name = "REGION_MGR_VM", required = true)
            protected String regionmgrvm;
            @XmlElement(name = "DISTRICT_DESCRIPTION", required = true)
            protected String districtdescription;
            @XmlElement(name = "DISTRICT_MGR", required = true)
            protected String districtmgr;
            @XmlElement(name = "DISTRICT_MGR_VM", required = true)
            protected String districtmgrvm;
            @XmlElement(name = "AREA_MGR", required = true)
            protected String areamgr;
            @XmlElement(name = "GENERAL_MANAGER", required = true)
            protected String generalmanager;
            @XmlElement(name = "MANAGER_1", required = true)
            protected String manager1;
            @XmlElement(name = "MANAGER_2", required = true)
            protected String manager2;
            @XmlElement(name = "ASSOCIATE_MGR_1", required = true)
            protected String associatemgr1;
            @XmlElement(name = "ASSOCIATE_MGR_2", required = true)
            protected String associatemgr2;
            @XmlElement(name = "ASSOCIATE_MGR_3", required = true)
            protected String associatemgr3;
            @XmlElement(name = "ASSOCIATE_MGR_4", required = true)
            protected String associatemgr4;
            @XmlElement(name = "ASSOCIATE_MGR_5", required = true)
            protected String associatemgr5;
            @XmlElement(name = "ASSOCIATE_MGR_6", required = true)
            protected String associatemgr6;
            @XmlElement(name = "ASSISTANT_MGR_1", required = true)
            protected String assistantmgr1;
            @XmlElement(name = "ASSISTANT_MGR_2", required = true)
            protected String assistantmgr2;
            @XmlElement(name = "ASSISTANT_MGR_3", required = true)
            protected String assistantmgr3;
            @XmlElement(name = "ASSISTANT_MGR_4", required = true)
            protected String assistantmgr4;
            @XmlElement(name = "ASSISTANT_MGR_5", required = true)
            protected String assistantmgr5;
            @XmlElement(name = "ASSISTANT_MGR_6", required = true)
            protected String assistantmgr6;
            @XmlElement(name = "ASSISTANT_MGR_7", required = true)
            protected String assistantmgr7;

            /**
             * Gets the value of the storenumber property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSTORENUMBER() {
                return storenumber;
            }

            /**
             * Sets the value of the storenumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSTORENUMBER(String value) {
                this.storenumber = value;
            }

            /**
             * Gets the value of the storename property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSTORENAME() {
                return storename;
            }

            /**
             * Sets the value of the storename property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSTORENAME(String value) {
                this.storename = value;
            }

            /**
             * Gets the value of the storename2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSTORENAME2() {
                return storename2;
            }

            /**
             * Sets the value of the storename2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSTORENAME2(String value) {
                this.storename2 = value;
            }

            /**
             * Gets the value of the storetype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSTORETYPE() {
                return storetype;
            }

            /**
             * Sets the value of the storetype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSTORETYPE(String value) {
                this.storetype = value;
            }

            /**
             * Gets the value of the storetypedescription property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSTORETYPEDESCRIPTION() {
                return storetypedescription;
            }

            /**
             * Sets the value of the storetypedescription property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSTORETYPEDESCRIPTION(String value) {
                this.storetypedescription = value;
            }

            /**
             * Gets the value of the storelat property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSTORELAT() {
                return storelat;
            }

            /**
             * Sets the value of the storelat property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSTORELAT(String value) {
                this.storelat = value;
            }

            /**
             * Gets the value of the storelongt property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSTORELONGT() {
                return storelongt;
            }

            /**
             * Sets the value of the storelongt property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSTORELONGT(String value) {
                this.storelongt = value;
            }

            /**
             * Gets the value of the address property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getADDRESS() {
                return address;
            }

            /**
             * Sets the value of the address property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setADDRESS(String value) {
                this.address = value;
            }

            /**
             * Gets the value of the address2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getADDRESS2() {
                return address2;
            }

            /**
             * Sets the value of the address2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setADDRESS2(String value) {
                this.address2 = value;
            }

            /**
             * Gets the value of the city property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCITY() {
                return city;
            }

            /**
             * Sets the value of the city property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCITY(String value) {
                this.city = value;
            }

            /**
             * Gets the value of the state property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSTATE() {
                return state;
            }

            /**
             * Sets the value of the state property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSTATE(String value) {
                this.state = value;
            }

            /**
             * Gets the value of the zip property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getZIP() {
                return zip;
            }

            /**
             * Sets the value of the zip property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setZIP(String value) {
                this.zip = value;
            }

            /**
             * Gets the value of the country property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCOUNTRY() {
                return country;
            }

            /**
             * Sets the value of the country property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCOUNTRY(String value) {
                this.country = value;
            }

            /**
             * Gets the value of the email property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEMAIL() {
                return email;
            }

            /**
             * Sets the value of the email property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEMAIL(String value) {
                this.email = value;
            }

            /**
             * Gets the value of the phone property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPHONE() {
                return phone;
            }

            /**
             * Sets the value of the phone property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPHONE(String value) {
                this.phone = value;
            }

            /**
             * Gets the value of the fax property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFAX() {
                return fax;
            }

            /**
             * Sets the value of the fax property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFAX(String value) {
                this.fax = value;
            }

            /**
             * Gets the value of the vm property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getVM() {
                return vm;
            }

            /**
             * Sets the value of the vm property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setVM(String value) {
                this.vm = value;
            }

            /**
             * Gets the value of the hours property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHOURS() {
                return hours;
            }

            /**
             * Sets the value of the hours property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHOURS(String value) {
                this.hours = value;
            }

            /**
             * Gets the value of the holidayhours property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHOLIDAYHOURS() {
                return holidayhours;
            }

            /**
             * Sets the value of the holidayhours property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHOLIDAYHOURS(String value) {
                this.holidayhours = value;
            }

            /**
             * Gets the value of the opendate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOPENDATE() {
                return opendate;
            }

            /**
             * Sets the value of the opendate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOPENDATE(String value) {
                this.opendate = value;
            }

            /**
             * Gets the value of the closedate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCLOSEDATE() {
                return closedate;
            }

            /**
             * Sets the value of the closedate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCLOSEDATE(String value) {
                this.closedate = value;
            }

            /**
             * Gets the value of the lastremodelleddate property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLASTREMODELLEDDATE() {
                return lastremodelleddate;
            }

            /**
             * Sets the value of the lastremodelleddate property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLASTREMODELLEDDATE(String value) {
                this.lastremodelleddate = value;
            }

            /**
             * Gets the value of the status property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSTATUS() {
                return status;
            }

            /**
             * Sets the value of the status property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSTATUS(String value) {
                this.status = value;
            }

            /**
             * Gets the value of the floor property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFLOOR() {
                return floor;
            }

            /**
             * Sets the value of the floor property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFLOOR(String value) {
                this.floor = value;
            }

            /**
             * Gets the value of the regiondescription property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getREGIONDESCRIPTION() {
                return regiondescription;
            }

            /**
             * Sets the value of the regiondescription property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setREGIONDESCRIPTION(String value) {
                this.regiondescription = value;
            }

            /**
             * Gets the value of the regioncode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getREGIONCODE() {
                return regioncode;
            }

            /**
             * Sets the value of the regioncode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setREGIONCODE(String value) {
                this.regioncode = value;
            }

            /**
             * Gets the value of the regionmgr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getREGIONMGR() {
                return regionmgr;
            }

            /**
             * Sets the value of the regionmgr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setREGIONMGR(String value) {
                this.regionmgr = value;
            }

            /**
             * Gets the value of the regionmgrvm property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getREGIONMGRVM() {
                return regionmgrvm;
            }

            /**
             * Sets the value of the regionmgrvm property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setREGIONMGRVM(String value) {
                this.regionmgrvm = value;
            }

            /**
             * Gets the value of the districtdescription property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDISTRICTDESCRIPTION() {
                return districtdescription;
            }

            /**
             * Sets the value of the districtdescription property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDISTRICTDESCRIPTION(String value) {
                this.districtdescription = value;
            }

            /**
             * Gets the value of the districtmgr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDISTRICTMGR() {
                return districtmgr;
            }

            /**
             * Sets the value of the districtmgr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDISTRICTMGR(String value) {
                this.districtmgr = value;
            }

            /**
             * Gets the value of the districtmgrvm property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDISTRICTMGRVM() {
                return districtmgrvm;
            }

            /**
             * Sets the value of the districtmgrvm property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDISTRICTMGRVM(String value) {
                this.districtmgrvm = value;
            }

            /**
             * Gets the value of the areamgr property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAREAMGR() {
                return areamgr;
            }

            /**
             * Sets the value of the areamgr property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAREAMGR(String value) {
                this.areamgr = value;
            }

            /**
             * Gets the value of the generalmanager property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGENERALMANAGER() {
                return generalmanager;
            }

            /**
             * Sets the value of the generalmanager property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGENERALMANAGER(String value) {
                this.generalmanager = value;
            }

            /**
             * Gets the value of the manager1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMANAGER1() {
                return manager1;
            }

            /**
             * Sets the value of the manager1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMANAGER1(String value) {
                this.manager1 = value;
            }

            /**
             * Gets the value of the manager2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMANAGER2() {
                return manager2;
            }

            /**
             * Sets the value of the manager2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMANAGER2(String value) {
                this.manager2 = value;
            }

            /**
             * Gets the value of the associatemgr1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getASSOCIATEMGR1() {
                return associatemgr1;
            }

            /**
             * Sets the value of the associatemgr1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setASSOCIATEMGR1(String value) {
                this.associatemgr1 = value;
            }

            /**
             * Gets the value of the associatemgr2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getASSOCIATEMGR2() {
                return associatemgr2;
            }

            /**
             * Sets the value of the associatemgr2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setASSOCIATEMGR2(String value) {
                this.associatemgr2 = value;
            }

            /**
             * Gets the value of the associatemgr3 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getASSOCIATEMGR3() {
                return associatemgr3;
            }

            /**
             * Sets the value of the associatemgr3 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setASSOCIATEMGR3(String value) {
                this.associatemgr3 = value;
            }

            /**
             * Gets the value of the associatemgr4 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getASSOCIATEMGR4() {
                return associatemgr4;
            }

            /**
             * Sets the value of the associatemgr4 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setASSOCIATEMGR4(String value) {
                this.associatemgr4 = value;
            }

            /**
             * Gets the value of the associatemgr5 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getASSOCIATEMGR5() {
                return associatemgr5;
            }

            /**
             * Sets the value of the associatemgr5 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setASSOCIATEMGR5(String value) {
                this.associatemgr5 = value;
            }

            /**
             * Gets the value of the associatemgr6 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getASSOCIATEMGR6() {
                return associatemgr6;
            }

            /**
             * Sets the value of the associatemgr6 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setASSOCIATEMGR6(String value) {
                this.associatemgr6 = value;
            }

            /**
             * Gets the value of the assistantmgr1 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getASSISTANTMGR1() {
                return assistantmgr1;
            }

            /**
             * Sets the value of the assistantmgr1 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setASSISTANTMGR1(String value) {
                this.assistantmgr1 = value;
            }

            /**
             * Gets the value of the assistantmgr2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getASSISTANTMGR2() {
                return assistantmgr2;
            }

            /**
             * Sets the value of the assistantmgr2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setASSISTANTMGR2(String value) {
                this.assistantmgr2 = value;
            }

            /**
             * Gets the value of the assistantmgr3 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getASSISTANTMGR3() {
                return assistantmgr3;
            }

            /**
             * Sets the value of the assistantmgr3 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setASSISTANTMGR3(String value) {
                this.assistantmgr3 = value;
            }

            /**
             * Gets the value of the assistantmgr4 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getASSISTANTMGR4() {
                return assistantmgr4;
            }

            /**
             * Sets the value of the assistantmgr4 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setASSISTANTMGR4(String value) {
                this.assistantmgr4 = value;
            }

            /**
             * Gets the value of the assistantmgr5 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getASSISTANTMGR5() {
                return assistantmgr5;
            }

            /**
             * Sets the value of the assistantmgr5 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setASSISTANTMGR5(String value) {
                this.assistantmgr5 = value;
            }

            /**
             * Gets the value of the assistantmgr6 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getASSISTANTMGR6() {
                return assistantmgr6;
            }

            /**
             * Sets the value of the assistantmgr6 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setASSISTANTMGR6(String value) {
                this.assistantmgr6 = value;
            }

            /**
             * Gets the value of the assistantmgr7 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getASSISTANTMGR7() {
                return assistantmgr7;
            }

            /**
             * Sets the value of the assistantmgr7 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setASSISTANTMGR7(String value) {
                this.assistantmgr7 = value;
            }

        }

    }

}
