package com.coach.stores.service.beans;

import javax.xml.bind.annotation.XmlRegistry;
import com.coach.stores.service.beans.GetGeographyMasterResponse.Geographies;
import com.coach.stores.service.beans.GetGeographyMasterResponse.Geographies.GeographyT;
import com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyRequest.Geography;
import com.coach.stores.service.beans.GetStoreDateHistoryResponse.StoresHistory;
import com.coach.stores.service.beans.GetStoreDateHistoryResponse.StoresHistory.StoreHistory;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.coach.stores.service.beans package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */

/**
 * @author GalaxE.
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.coach.stores.service.beans
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetOpenStoreDescriptionByIDFault }
     * 
     */
    public GetOpenStoreDescriptionByIDFault createGetOpenStoreDescriptionByIDFault() {
        return new GetOpenStoreDescriptionByIDFault();
    }

    /**
     * Create an instance of {@link GetStoreDateHistoryRequest }
     * 
     */
    public GetStoreDateHistoryRequest createGetStoreDateHistoryRequest() {
        return new GetStoreDateHistoryRequest();
    }

    /**
     * Create an instance of {@link StoresHistory }
     * 
     */
    public StoresHistory createGetStoreDateHistoryResponseStoresHistory() {
        return new StoresHistory();
    }

    /**
     * Create an instance of {@link com.coach.stores.service.beans.GetOpenStoreDescriptionByIDResponse.StoresDesc }
     * 
     */
    public com.coach.stores.service.beans.GetOpenStoreDescriptionByIDResponse.StoresDesc createGetOpenStoreDescriptionByIDResponseStoresDesc() {
        return new com.coach.stores.service.beans.GetOpenStoreDescriptionByIDResponse.StoresDesc();
    }

    /**
     * Create an instance of {@link GetOpenStoreDescriptionByGeographyRequest }
     * 
     */
    public GetOpenStoreDescriptionByGeographyRequest createGetOpenStoreDescriptionByGeographyRequest() {
        return new GetOpenStoreDescriptionByGeographyRequest();
    }

    /**
     * Create an instance of {@link GetGeographyMasterRequest }
     * 
     */
    public GetGeographyMasterRequest createGetGeographyMasterRequest() {
        return new GetGeographyMasterRequest();
    }

    /**
     * Create an instance of {@link GetStoreDateHistoryResponse }
     * 
     */
    public GetStoreDateHistoryResponse createGetStoreDateHistoryResponse() {
        return new GetStoreDateHistoryResponse();
    }

    /**
     * Create an instance of {@link com.coach.stores.service.beans.GetOpenStoreByStoreNumberResponse.Stores }
     * 
     */
    public com.coach.stores.service.beans.GetOpenStoreByStoreNumberResponse.Stores createGetOpenStoreByStoreNumberResponseStores() {
        return new com.coach.stores.service.beans.GetOpenStoreByStoreNumberResponse.Stores();
    }

    /**
     * Create an instance of {@link com.coach.stores.service.beans.GetOpenStoreByStoreNumberResponse.Stores.Store }
     * 
     */
    public com.coach.stores.service.beans.GetOpenStoreByStoreNumberResponse.Stores.Store createGetOpenStoreByStoreNumberResponseStoresStore() {
        return new com.coach.stores.service.beans.GetOpenStoreByStoreNumberResponse.Stores.Store();
    }

    /**
     * Create an instance of {@link com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyResponse.StoresDesc.StoreDesc }
     * 
     */
    public com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyResponse.StoresDesc.StoreDesc createGetOpenStoreDescriptionByGeographyResponseStoresDescStoreDesc() {
        return new com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyResponse.StoresDesc.StoreDesc();
    }

    /**
     * Create an instance of {@link GetAllOpenStoreDescriptionRequest }
     * 
     */
    public GetAllOpenStoreDescriptionRequest createGetAllOpenStoreDescriptionRequest() {
        return new GetAllOpenStoreDescriptionRequest();
    }

    /**
     * Create an instance of {@link GetAllOpenStoreDescriptionFault }
     * 
     */
    public GetAllOpenStoreDescriptionFault createGetAllOpenStoreDescriptionFault() {
        return new GetAllOpenStoreDescriptionFault();
    }

    /**
     * Create an instance of {@link GetGeographyMasterFault }
     * 
     */
    public GetGeographyMasterFault createGetGeographyMasterFault() {
        return new GetGeographyMasterFault();
    }

    /**
     * Create an instance of {@link GetOpenStoreDescriptionByIDRequest }
     * 
     */
    public GetOpenStoreDescriptionByIDRequest createGetOpenStoreDescriptionByIDRequest() {
        return new GetOpenStoreDescriptionByIDRequest();
    }

    /**
     * Create an instance of {@link GetOpenStoreDescriptionByKeywordRequest }
     * 
     */
    public GetOpenStoreDescriptionByKeywordRequest createGetOpenStoreDescriptionByKeywordRequest() {
        return new GetOpenStoreDescriptionByKeywordRequest();
    }

    /**
     * Create an instance of {@link GetOpenStoreDescriptionByIDResponse }
     * 
     */
    public GetOpenStoreDescriptionByIDResponse createGetOpenStoreDescriptionByIDResponse() {
        return new GetOpenStoreDescriptionByIDResponse();
    }

    /**
     * Create an instance of {@link Geography }
     * 
     */
    public Geography createGetOpenStoreDescriptionByGeographyRequestGeography() {
        return new Geography();
    }

    /**
     * Create an instance of {@link GetOpenStoreByStoreNumberFault }
     * 
     */
    public GetOpenStoreByStoreNumberFault createGetOpenStoreByStoreNumberFault() {
        return new GetOpenStoreByStoreNumberFault();
    }

    /**
     * Create an instance of {@link com.coach.stores.service.beans.GetOpenStoreByGeographyResponse.Stores.Store }
     * 
     */
    public com.coach.stores.service.beans.GetOpenStoreByGeographyResponse.Stores.Store createGetOpenStoreByGeographyResponseStoresStore() {
        return new com.coach.stores.service.beans.GetOpenStoreByGeographyResponse.Stores.Store();
    }

    /**
     * Create an instance of {@link GetStoreDateHistoryFault }
     * 
     */
    public GetStoreDateHistoryFault createGetStoreDateHistoryFault() {
        return new GetStoreDateHistoryFault();
    }

    /**
     * Create an instance of {@link com.coach.stores.service.beans.GetOpenStoreDescriptionByKeywordResponse.StoresDesc }
     * 
     */
    public com.coach.stores.service.beans.GetOpenStoreDescriptionByKeywordResponse.StoresDesc createGetOpenStoreDescriptionByKeywordResponseStoresDesc() {
        return new com.coach.stores.service.beans.GetOpenStoreDescriptionByKeywordResponse.StoresDesc();
    }

    /**
     * Create an instance of {@link GetAllOpenStoreDescriptionResponse }
     * 
     */
    public GetAllOpenStoreDescriptionResponse createGetAllOpenStoreDescriptionResponse() {
        return new GetAllOpenStoreDescriptionResponse();
    }

    /**
     * Create an instance of {@link GetOpenStoreByGeographyRequest }
     * 
     */
    public GetOpenStoreByGeographyRequest createGetOpenStoreByGeographyRequest() {
        return new GetOpenStoreByGeographyRequest();
    }

    /**
     * Create an instance of {@link com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyResponse.StoresDesc }
     * 
     */
    public com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyResponse.StoresDesc createGetOpenStoreDescriptionByGeographyResponseStoresDesc() {
        return new com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyResponse.StoresDesc();
    }

    /**
     * Create an instance of {@link StoreHistory }
     * 
     */
    public StoreHistory createGetStoreDateHistoryResponseStoresHistoryStoreHistory() {
        return new StoreHistory();
    }

    /**
     * Create an instance of {@link GetOpenStoreByStoreNumberResponse }
     * 
     */
    public GetOpenStoreByStoreNumberResponse createGetOpenStoreByStoreNumberResponse() {
        return new GetOpenStoreByStoreNumberResponse();
    }

    /**
     * Create an instance of {@link GetGeographyMasterResponse }
     * 
     */
    public GetGeographyMasterResponse createGetGeographyMasterResponse() {
        return new GetGeographyMasterResponse();
    }

    /**
     * Create an instance of {@link com.coach.stores.service.beans.GetAllOpenStoreDescriptionResponse.StoresDesc }
     * 
     */
    public com.coach.stores.service.beans.GetAllOpenStoreDescriptionResponse.StoresDesc createGetAllOpenStoreDescriptionResponseStoresDesc() {
        return new com.coach.stores.service.beans.GetAllOpenStoreDescriptionResponse.StoresDesc();
    }

    /**
     * Create an instance of {@link com.coach.stores.service.beans.GetAllOpenStoreDescriptionResponse.StoresDesc.StoreDesc }
     * 
     */
    public com.coach.stores.service.beans.GetAllOpenStoreDescriptionResponse.StoresDesc.StoreDesc createGetAllOpenStoreDescriptionResponseStoresDescStoreDesc() {
        return new com.coach.stores.service.beans.GetAllOpenStoreDescriptionResponse.StoresDesc.StoreDesc();
    }

    /**
     * Create an instance of {@link com.coach.stores.service.beans.GetOpenStoreDescriptionByKeywordResponse.StoresDesc.StoreDesc }
     * 
     */
    public com.coach.stores.service.beans.GetOpenStoreDescriptionByKeywordResponse.StoresDesc.StoreDesc createGetOpenStoreDescriptionByKeywordResponseStoresDescStoreDesc() {
        return new com.coach.stores.service.beans.GetOpenStoreDescriptionByKeywordResponse.StoresDesc.StoreDesc();
    }

    /**
     * Create an instance of {@link com.coach.stores.service.beans.GetOpenStoreByGeographyResponse.Stores }
     * 
     */
    public com.coach.stores.service.beans.GetOpenStoreByGeographyResponse.Stores createGetOpenStoreByGeographyResponseStores() {
        return new com.coach.stores.service.beans.GetOpenStoreByGeographyResponse.Stores();
    }

    /**
     * Create an instance of {@link Geographies }
     * 
     */
    public Geographies createGetGeographyMasterResponseGeographies() {
        return new Geographies();
    }

    /**
     * Create an instance of {@link GetOpenStoreDescriptionByKeywordFault }
     * 
     */
    public GetOpenStoreDescriptionByKeywordFault createGetOpenStoreDescriptionByKeywordFault() {
        return new GetOpenStoreDescriptionByKeywordFault();
    }

    /**
     * Create an instance of {@link GetOpenStoreByGeographyResponse }
     * 
     */
    public GetOpenStoreByGeographyResponse createGetOpenStoreByGeographyResponse() {
        return new GetOpenStoreByGeographyResponse();
    }

    /**
     * Create an instance of {@link GetOpenStoreDescriptionByKeywordResponse }
     * 
     */
    public GetOpenStoreDescriptionByKeywordResponse createGetOpenStoreDescriptionByKeywordResponse() {
        return new GetOpenStoreDescriptionByKeywordResponse();
    }

    /**
     * Create an instance of {@link GetOpenStoreDescriptionByGeographyFault }
     * 
     */
    public GetOpenStoreDescriptionByGeographyFault createGetOpenStoreDescriptionByGeographyFault() {
        return new GetOpenStoreDescriptionByGeographyFault();
    }

    /**
     * Create an instance of {@link GeographyT }
     * 
     */
    public GeographyT createGetGeographyMasterResponseGeographiesGeographyT() {
        return new GeographyT();
    }

    /**
     * Create an instance of {@link com.coach.stores.service.beans.GetOpenStoreDescriptionByIDResponse.StoresDesc.StoreDesc }
     * 
     */
    public com.coach.stores.service.beans.GetOpenStoreDescriptionByIDResponse.StoresDesc.StoreDesc createGetOpenStoreDescriptionByIDResponseStoresDescStoreDesc() {
        return new com.coach.stores.service.beans.GetOpenStoreDescriptionByIDResponse.StoresDesc.StoreDesc();
    }

    /**
     * Create an instance of {@link GetOpenStoreByStoreNumberRequest }
     * 
     */
    public GetOpenStoreByStoreNumberRequest createGetOpenStoreByStoreNumberRequest() {
        return new GetOpenStoreByStoreNumberRequest();
    }

    /**
     * Create an instance of {@link GetOpenStoreDescriptionByGeographyResponse }
     * 
     */
    public GetOpenStoreDescriptionByGeographyResponse createGetOpenStoreDescriptionByGeographyResponse() {
        return new GetOpenStoreDescriptionByGeographyResponse();
    }

    /**
     * Create an instance of {@link GetOpenStoreByGeographyFault }
     * 
     */
    public GetOpenStoreByGeographyFault createGetOpenStoreByGeographyFault() {
        return new GetOpenStoreByGeographyFault();
    }

}
