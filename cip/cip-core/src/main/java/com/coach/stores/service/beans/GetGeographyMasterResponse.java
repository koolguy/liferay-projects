package com.coach.stores.service.beans;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/*import com.coach.stores.service.beans.GetGeographyMasterResponse.Geographies;
import com.coach.stores.service.beans.GetGeographyMasterResponse.Geographies.GeographyT;*/


/**
 * <p>Java class for getGeographyMasterResponse element declaration.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;element name="getGeographyMasterResponse">
 *   &lt;complexType>
 *     &lt;complexContent>
 *       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *         &lt;sequence>
 *           &lt;element name="Geographies">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="GeographyT" maxOccurs="unbounded">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="Geography" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/restriction>
 *     &lt;/complexContent>
 *   &lt;/complexType>
 * &lt;/element>
 * </pre>
 * 
 * 
 */

/**
 * @author GalaxE.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "geographies"
})
@XmlRootElement(name = "getGeographyMasterResponse")
public class GetGeographyMasterResponse {

    @XmlElement(name = "Geographies", required = true)
    protected Geographies geographies;

    /**
     * Gets the value of the geographies property.
     * 
     * @return
     *     possible object is
     *     {@link Geographies }
     *     
     */
    public Geographies getGeographies() {
        return geographies;
    }

    /**
     * Sets the value of the geographies property.
     * 
     * @param value
     *     allowed object is
     *     {@link Geographies }
     *     
     */
    public void setGeographies(Geographies value) {
        this.geographies = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="GeographyT" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="Geography" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "geographyT"
    })
    public static class Geographies {

        @XmlElement(name = "GeographyT", required = true)
        protected List<GeographyT> geographyT;

        /**
         * Gets the value of the geographyT property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the geographyT property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getGeographyT().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link GeographyT }
         * 
         * 
         */
        public List<GeographyT> getGeographyT() {
            if (geographyT == null) {
                geographyT = new ArrayList<GeographyT>();
            }
            return this.geographyT;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="Geography" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "geography"
        })
        public static class GeographyT {

            @XmlElement(name = "Geography", required = true)
            protected String geography;

            /**
             * Gets the value of the geography property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGeography() {
                return geography;
            }

            /**
             * Sets the value of the geography property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGeography(String value) {
                this.geography = value;
            }

        }

    }

}
