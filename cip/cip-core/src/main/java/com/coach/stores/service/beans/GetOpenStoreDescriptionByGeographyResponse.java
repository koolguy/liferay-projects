package com.coach.stores.service.beans;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
/*import com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyResponse.StoresDesc;
import com.coach.stores.service.beans.GetOpenStoreDescriptionByGeographyResponse.StoresDesc.StoreDesc;*/


/**
 * <p>Java class for getOpenStoreDescriptionByGeographyResponse element declaration.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;element name="getOpenStoreDescriptionByGeographyResponse">
 *   &lt;complexType>
 *     &lt;complexContent>
 *       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *         &lt;sequence>
 *           &lt;element name="Stores_Desc">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="Store_Desc" maxOccurs="unbounded" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="STORE_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="STORE_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="GEOGRAPHY" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="PHONE" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="VM" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="DISTRICT_DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                               &lt;element name="FLOOR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *       &lt;/restriction>
 *     &lt;/complexContent>
 *   &lt;/complexType>
 * &lt;/element>
 * </pre>
 * 
 * 
 */

/**
 * @author GalaxE.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "storesDesc"
})
@XmlRootElement(name = "getOpenStoreDescriptionByGeographyResponse")
public class GetOpenStoreDescriptionByGeographyResponse {

    @XmlElement(name = "Stores_Desc", required = true)
    protected StoresDesc storesDesc;

    /**
     * Gets the value of the storesDesc property.
     * 
     * @return
     *     possible object is
     *     {@link StoresDesc }
     *     
     */
    public StoresDesc getStoresDesc() {
        return storesDesc;
    }

    /**
     * Sets the value of the storesDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link StoresDesc }
     *     
     */
    public void setStoresDesc(StoresDesc value) {
        this.storesDesc = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Store_Desc" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="STORE_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="STORE_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="GEOGRAPHY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PHONE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="VM" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DISTRICT_DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FLOOR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "storeDesc"
    })
    public static class StoresDesc {

        @XmlElement(name = "Store_Desc", required = true)
        protected List<StoreDesc> storeDesc;

        /**
         * Gets the value of the storeDesc property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the storeDesc property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getStoreDesc().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link StoreDesc }
         * 
         * 
         */
        public List<StoreDesc> getStoreDesc() {
            if (storeDesc == null) {
                storeDesc = new ArrayList<StoreDesc>();
            }
            return this.storeDesc;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="STORE_NUMBER" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="STORE_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="GEOGRAPHY" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PHONE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="VM" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DISTRICT_DESCRIPTION" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FLOOR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "storenumber",
            "storename",
            "city",
            "state",
            "country",
            "geography",
            "phone",
            "vm",
            "districtdescription",
            "floor"
        })
        public static class StoreDesc {

            @XmlElement(name = "STORE_NUMBER", required = true)
            protected String storenumber;
            @XmlElement(name = "STORE_NAME", required = true)
            protected String storename;
            @XmlElement(name = "CITY", required = true)
            protected String city;
            @XmlElement(name = "STATE", required = true)
            protected String state;
            @XmlElement(name = "COUNTRY", required = true)
            protected String country;
            @XmlElement(name = "GEOGRAPHY", required = true)
            protected String geography;
            @XmlElement(name = "PHONE", required = true)
            protected String phone;
            @XmlElement(name = "VM", required = true)
            protected String vm;
            @XmlElement(name = "DISTRICT_DESCRIPTION", required = true)
            protected String districtdescription;
            @XmlElement(name = "FLOOR", required = true)
            protected String floor;

            /**
             * Gets the value of the storenumber property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSTORENUMBER() {
                return storenumber;
            }

            /**
             * Sets the value of the storenumber property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSTORENUMBER(String value) {
                this.storenumber = value;
            }

            /**
             * Gets the value of the storename property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSTORENAME() {
                return storename;
            }

            /**
             * Sets the value of the storename property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSTORENAME(String value) {
                this.storename = value;
            }

            /**
             * Gets the value of the city property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCITY() {
                return city;
            }

            /**
             * Sets the value of the city property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCITY(String value) {
                this.city = value;
            }

            /**
             * Gets the value of the state property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSTATE() {
                return state;
            }

            /**
             * Sets the value of the state property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSTATE(String value) {
                this.state = value;
            }

            /**
             * Gets the value of the country property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCOUNTRY() {
                return country;
            }

            /**
             * Sets the value of the country property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCOUNTRY(String value) {
                this.country = value;
            }

            /**
             * Gets the value of the geography property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGEOGRAPHY() {
                return geography;
            }

            /**
             * Sets the value of the geography property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGEOGRAPHY(String value) {
                this.geography = value;
            }

            /**
             * Gets the value of the phone property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPHONE() {
                return phone;
            }

            /**
             * Sets the value of the phone property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPHONE(String value) {
                this.phone = value;
            }

            /**
             * Gets the value of the vm property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getVM() {
                return vm;
            }

            /**
             * Sets the value of the vm property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setVM(String value) {
                this.vm = value;
            }

            /**
             * Gets the value of the districtdescription property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDISTRICTDESCRIPTION() {
                return districtdescription;
            }

            /**
             * Sets the value of the districtdescription property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDISTRICTDESCRIPTION(String value) {
                this.districtdescription = value;
            }

            /**
             * Gets the value of the floor property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFLOOR() {
                return floor;
            }

            /**
             * Sets the value of the floor property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFLOOR(String value) {
                this.floor = value;
            }

        }

    }

}
