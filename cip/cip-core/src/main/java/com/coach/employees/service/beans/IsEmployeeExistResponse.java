package com.coach.employees.service.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Class is used to IsEmployeeExistResponse. 
 * @author GalaxE.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "returnFlag"
})
@XmlRootElement(name = "IsEmployeeExistResponse")
public class IsEmployeeExistResponse {

    @XmlElement(name = "ReturnFlag", required = true)
    protected String returnFlag;

    /**
     * Gets the value of the returnFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReturnFlag() {
        return returnFlag;
    }

    /**
     * Sets the value of the returnFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReturnFlag(String value) {
        this.returnFlag = value;
    }

}
