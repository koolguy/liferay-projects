
package com.coach.employees.service.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Class is used to GetEmployeesDescriptionByTitleRequest. 
 * @author GalaxE.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "empDescByTitle"
})
@XmlRootElement(name = "getEmployeesDescriptionByTitleRequest")
public class GetEmployeesDescriptionByTitleRequest {

    @XmlElement(name = "EMP_DescByTitle", required = true)
    protected EMPDescByTitle empDescByTitle;

    /**
     * Gets the value of the empDescByTitle property.
     * 
     * @return
     *     possible object is
     *     {@link EMPDescByTitle }
     *     
     */
    public EMPDescByTitle getEMPDescByTitle() {
        return empDescByTitle;
    }

    /**
     * Sets the value of the empDescByTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link EMPDescByTitle }
     *     
     */
    public void setEMPDescByTitle(EMPDescByTitle value) {
        this.empDescByTitle = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="strTitle" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "strTitle"
    })
    public static class EMPDescByTitle {

        @XmlElement(required = true)
        protected String strTitle;

        /**
         * Gets the value of the strTitle property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStrTitle() {
            return strTitle;
        }

        /**
         * Sets the value of the strTitle property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStrTitle(String value) {
            this.strTitle = value;
        }

    }

}
