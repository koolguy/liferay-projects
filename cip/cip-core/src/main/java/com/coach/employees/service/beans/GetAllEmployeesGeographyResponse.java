
package com.coach.employees.service.beans;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Class is used to GetAllEmployeesGeographyResponse. 
 * @author GalaxE.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "geography"
})
@XmlRootElement(name = "getAllEmployeesGeographyResponse")
public class GetAllEmployeesGeographyResponse {

    @XmlElement(name = "Geography", required = true)
    protected List<String> geography;

    /**
     * Gets the value of the geography property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the geography property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getGeography().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getGeography() {
        if (geography == null) {
            geography = new ArrayList<String>();
        }
        return this.geography;
    }

}
