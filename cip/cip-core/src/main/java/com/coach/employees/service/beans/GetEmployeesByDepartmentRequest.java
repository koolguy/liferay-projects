package com.coach.employees.service.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;



/**
 * Class is used to GetEmployeesByDepartmentRequest. 
 * @author GalaxE.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "empByDepartment"
})
@XmlRootElement(name = "getEmployeesByDepartmentRequest")
public class GetEmployeesByDepartmentRequest {

    @XmlElement(name = "EMP_ByDepartment", required = true)
    protected EMPByDepartment empByDepartment;

    /**
     * Gets the value of the empByDepartment property.
     * 
     * @return
     *     possible object is
     *     {@link EMPByDepartment }
     *     
     */
    public EMPByDepartment getEMPByDepartment() {
        return empByDepartment;
    }

    /**
     * Sets the value of the empByDepartment property.
     * 
     * @param value
     *     allowed object is
     *     {@link EMPByDepartment }
     *     
     */
    public void setEMPByDepartment(EMPByDepartment value) {
        this.empByDepartment = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="strDeptName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "strDeptName"
    })
    public static class EMPByDepartment {

        @XmlElement(required = true)
        protected String strDeptName;

        /**
         * Gets the value of the strDeptName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStrDeptName() {
            return strDeptName;
        }

        /**
         * Sets the value of the strDeptName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStrDeptName(String value) {
            this.strDeptName = value;
        }

    }

}
