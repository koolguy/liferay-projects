
package com.coach.employees.service.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Class is used to GetAllEmployeesGeographyRequest. 
 * @author GalaxE.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "empGeography"
})
@XmlRootElement(name = "getAllEmployeesGeographyRequest")
public class GetAllEmployeesGeographyRequest {

    @XmlElement(name = "EMP_Geography", required = true)
    protected EMPGeography empGeography;

    /**
     * Gets the value of the empGeography property.
     * 
     * @return
     *     possible object is
     *     {@link EMPGeography }
     *     
     */
    public EMPGeography getEMPGeography() {
        return empGeography;
    }

    /**
     * Sets the value of the empGeography property.
     * 
     * @param value
     *     allowed object is
     *     {@link EMPGeography }
     *     
     */
    public void setEMPGeography(EMPGeography value) {
        this.empGeography = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class EMPGeography {


    }

}
