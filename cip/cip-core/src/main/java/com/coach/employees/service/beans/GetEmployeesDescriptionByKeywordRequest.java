package com.coach.employees.service.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Class is used to GetEmployeesDescriptionByKeywordRequest. 
 * @author GalaxE.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "empDescByKeyword"
})
@XmlRootElement(name = "getEmployeesDescriptionByKeywordRequest")
public class GetEmployeesDescriptionByKeywordRequest {

    @XmlElement(name = "EMP_DescByKeyword", required = true)
    protected EMPDescByKeyword empDescByKeyword;

    /**
     * Gets the value of the empDescByKeyword property.
     * 
     * @return
     *     possible object is
     *     {@link EMPDescByKeyword }
     *     
     */
    public EMPDescByKeyword getEMPDescByKeyword() {
        return empDescByKeyword;
    }

    /**
     * Sets the value of the empDescByKeyword property.
     * 
     * @param value
     *     allowed object is
     *     {@link EMPDescByKeyword }
     *     
     */
    public void setEMPDescByKeyword(EMPDescByKeyword value) {
        this.empDescByKeyword = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="strKeyword" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "strKeyword"
    })
    public static class EMPDescByKeyword {

        @XmlElement(required = true)
        protected String strKeyword;

        /**
         * Gets the value of the strKeyword property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStrKeyword() {
            return strKeyword;
        }

        /**
         * Sets the value of the strKeyword property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStrKeyword(String value) {
            this.strKeyword = value;
        }

    }

}
