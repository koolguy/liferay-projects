
package com.coach.employees.service.beans;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Class is used to GetEmployeesDescriptionByKeywordResponse. 
 * @author GalaxE.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "employeeDesc"
})
@XmlRootElement(name = "getEmployeesDescriptionByKeywordResponse")
public class GetEmployeesDescriptionByKeywordResponse {

    @XmlElement(name = "Employee_Desc", required = true)
    protected EmployeeDesc employeeDesc;

    /**
     * Gets the value of the employeeDesc property.
     * 
     * @return
     *     possible object is
     *     {@link EmployeeDesc }
     *     
     */
    public EmployeeDesc getEmployeeDesc() {
        return employeeDesc;
    }

    /**
     * Sets the value of the employeeDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmployeeDesc }
     *     
     */
    public void setEmployeeDesc(EmployeeDesc value) {
        this.employeeDesc = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="EmployeeDescription" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="USERID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FIRST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LAST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PHONE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="VM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="BUILDING_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="FLOOR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "employeeDescription"
    })
    public static class EmployeeDesc {

        @XmlElement(name = "EmployeeDescription", required = true)
        protected List<EmployeeDescription> employeeDescription;

        /**
         * Gets the value of the employeeDescription property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the employeeDescription property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEmployeeDescription().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link EmployeeDescription }
         * 
         * 
         */
        public List<EmployeeDescription> getEmployeeDescription() {
            if (employeeDescription == null) {
                employeeDescription = new ArrayList<EmployeeDescription>();
            }
            return this.employeeDescription;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="USERID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FIRST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LAST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PHONE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="VM" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="BUILDING_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="FLOOR" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "userid",
            "firstname",
            "lastname",
            "phone",
            "vm",
            "buildingname",
            "floor"
        })
        public static class EmployeeDescription {

            @XmlElement(name = "USERID", required = true)
            protected String userid;
            @XmlElement(name = "FIRST_NAME", required = true)
            protected String firstname;
            @XmlElement(name = "LAST_NAME", required = true)
            protected String lastname;
            @XmlElement(name = "PHONE")
            protected String phone;
            @XmlElement(name = "VM")
            protected String vm;
            @XmlElement(name = "BUILDING_NAME")
            protected String buildingname;
            @XmlElement(name = "FLOOR")
            protected String floor;

            /**
             * Gets the value of the userid property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUSERID() {
                return userid;
            }

            /**
             * Sets the value of the userid property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUSERID(String value) {
                this.userid = value;
            }

            /**
             * Gets the value of the firstname property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFIRSTNAME() {
                return firstname;
            }

            /**
             * Sets the value of the firstname property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFIRSTNAME(String value) {
                this.firstname = value;
            }

            /**
             * Gets the value of the lastname property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLASTNAME() {
                return lastname;
            }

            /**
             * Sets the value of the lastname property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLASTNAME(String value) {
                this.lastname = value;
            }

            /**
             * Gets the value of the phone property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPHONE() {
                return phone;
            }

            /**
             * Sets the value of the phone property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPHONE(String value) {
                this.phone = value;
            }

            /**
             * Gets the value of the vm property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getVM() {
                return vm;
            }

            /**
             * Sets the value of the vm property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setVM(String value) {
                this.vm = value;
            }

            /**
             * Gets the value of the buildingname property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBUILDINGNAME() {
                return buildingname;
            }

            /**
             * Sets the value of the buildingname property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBUILDINGNAME(String value) {
                this.buildingname = value;
            }

            /**
             * Gets the value of the floor property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFLOOR() {
                return floor;
            }

            /**
             * Sets the value of the floor property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFLOOR(String value) {
                this.floor = value;
            }

        }

    }

}
