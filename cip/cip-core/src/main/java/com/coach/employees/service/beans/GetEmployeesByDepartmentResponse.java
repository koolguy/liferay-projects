
package com.coach.employees.service.beans;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Class is used to GetEmployeesByDepartmentResponse. 
 * @author GalaxE.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "employees"
})
@XmlRootElement(name = "getEmployeesByDepartmentResponse")
public class GetEmployeesByDepartmentResponse {

    @XmlElement(name = "Employees", required = true)
    protected Employees employees;

    /**
     * Gets the value of the employees property.
     * 
     * @return
     *     possible object is
     *     {@link Employees }
     *     
     */
    public Employees getEmployees() {
        return employees;
    }

    /**
     * Sets the value of the employees property.
     * 
     * @param value
     *     allowed object is
     *     {@link Employees }
     *     
     */
    public void setEmployees(Employees value) {
        this.employees = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Employee" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="TITLE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FIRST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MIDDLE_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LAST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="USERID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LANGUAGE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="EMAIL" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PHONE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="PHONE_EXTN" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="VM" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FAX" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="GEOGRAPHY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DISTRICT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ADDRESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ADDRESS2" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ADDRESS3" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ZIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="OFFICE_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="BUILDING_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="FLOOR" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DEPARTMENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="MANAGER">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="FIRST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="LAST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="USERID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="DIRECT_REPORTS">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="DIRECT_REPORT" maxOccurs="unbounded" minOccurs="0">
     *                               &lt;complexType>
     *                                 &lt;complexContent>
     *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                                     &lt;sequence>
     *                                       &lt;element name="FIRST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="LAST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                       &lt;element name="USERID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                                     &lt;/sequence>
     *                                   &lt;/restriction>
     *                                 &lt;/complexContent>
     *                               &lt;/complexType>
     *                             &lt;/element>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="ASSISTANT">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="FIRST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="LAST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                             &lt;element name="USERID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="EMPLOYEE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="EMPLOYEE_STATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="LEADERSHIP_IND" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ENROLLEDBENEFITS_IND" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="DATEHIRE" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="ROLES">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="ROLE" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                   &lt;element name="EMPLOYEEID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "employee"
    })
    public static class Employees {

        @XmlElement(name = "Employee", required = true)
        protected List<Employee> employee;

        /**
         * Gets the value of the employee property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the employee property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEmployee().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Employee }
         * 
         * 
         */
        public List<Employee> getEmployee() {
            if (employee == null) {
                employee = new ArrayList<Employee>();
            }
            return this.employee;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="TITLE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FIRST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MIDDLE_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LAST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="USERID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LANGUAGE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="EMAIL" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PHONE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="PHONE_EXTN" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="VM" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FAX" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="GEOGRAPHY" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DISTRICT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ADDRESS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ADDRESS2" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ADDRESS3" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ZIP" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="OFFICE_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="BUILDING_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="FLOOR" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DEPARTMENT" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="MANAGER">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="FIRST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="LAST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="USERID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="DIRECT_REPORTS">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="DIRECT_REPORT" maxOccurs="unbounded" minOccurs="0">
         *                     &lt;complexType>
         *                       &lt;complexContent>
         *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                           &lt;sequence>
         *                             &lt;element name="FIRST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="LAST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                             &lt;element name="USERID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                           &lt;/sequence>
         *                         &lt;/restriction>
         *                       &lt;/complexContent>
         *                     &lt;/complexType>
         *                   &lt;/element>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="ASSISTANT">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="FIRST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="LAST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                   &lt;element name="USERID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="EMPLOYEE_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="EMPLOYEE_STATUS" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="LEADERSHIP_IND" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ENROLLEDBENEFITS_IND" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="DATEHIRE" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="ROLES">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="ROLE" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *         &lt;element name="EMPLOYEEID" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "title",
            "firstname",
            "middlename",
            "lastname",
            "userid",
            "language",
            "email",
            "phone",
            "phoneextn",
            "vm",
            "fax",
            "geography",
            "country",
            "district",
            "state",
            "city",
            "address",
            "address2",
            "address3",
            "zip",
            "officename",
            "buildingname",
            "floor",
            "department",
            "manager",
            "directreports",
            "assistant",
            "employeetype",
            "employeestatus",
            "leadershipind",
            "enrolledbenefitsind",
            "datehire",
            "roles",
            "employeeid"
        })
        public static class Employee {

            @XmlElement(name = "TITLE", required = true)
            protected String title;
            @XmlElement(name = "FIRST_NAME", required = true)
            protected String firstname;
            @XmlElement(name = "MIDDLE_NAME", required = true)
            protected String middlename;
            @XmlElement(name = "LAST_NAME", required = true)
            protected String lastname;
            @XmlElement(name = "USERID", required = true)
            protected String userid;
            @XmlElement(name = "LANGUAGE", required = true)
            protected String language;
            @XmlElement(name = "EMAIL", required = true)
            protected String email;
            @XmlElement(name = "PHONE", required = true)
            protected String phone;
            @XmlElement(name = "PHONE_EXTN", required = true)
            protected String phoneextn;
            @XmlElement(name = "VM", required = true)
            protected String vm;
            @XmlElement(name = "FAX", required = true)
            protected String fax;
            @XmlElement(name = "GEOGRAPHY", required = true)
            protected String geography;
            @XmlElement(name = "COUNTRY", required = true)
            protected String country;
            @XmlElement(name = "DISTRICT", required = true)
            protected String district;
            @XmlElement(name = "STATE", required = true)
            protected String state;
            @XmlElement(name = "CITY", required = true)
            protected String city;
            @XmlElement(name = "ADDRESS", required = true)
            protected String address;
            @XmlElement(name = "ADDRESS2", required = true)
            protected String address2;
            @XmlElement(name = "ADDRESS3", required = true)
            protected String address3;
            @XmlElement(name = "ZIP", required = true)
            protected String zip;
            @XmlElement(name = "OFFICE_NAME", required = true)
            protected String officename;
            @XmlElement(name = "BUILDING_NAME", required = true)
            protected String buildingname;
            @XmlElement(name = "FLOOR", required = true)
            protected String floor;
            @XmlElement(name = "DEPARTMENT", required = true)
            protected String department;
            @XmlElement(name = "MANAGER", required = true)
            protected MANAGER manager;
            @XmlElement(name = "DIRECT_REPORTS", required = true)
            protected DIRECTREPORTS directreports;
            @XmlElement(name = "ASSISTANT", required = true)
            protected ASSISTANT assistant;
            @XmlElement(name = "EMPLOYEE_TYPE", required = true)
            protected String employeetype;
            @XmlElement(name = "EMPLOYEE_STATUS", required = true)
            protected String employeestatus;
            @XmlElement(name = "LEADERSHIP_IND", required = true)
            protected String leadershipind;
            @XmlElement(name = "ENROLLEDBENEFITS_IND", required = true)
            protected String enrolledbenefitsind;
            @XmlElement(name = "DATEHIRE", required = true)
            protected String datehire;
            @XmlElement(name = "ROLES", required = true)
            protected ROLES roles;
            @XmlElement(name = "EMPLOYEEID", required = true)
            protected String employeeid;

            /**
             * Gets the value of the title property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTITLE() {
                return title;
            }

            /**
             * Sets the value of the title property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTITLE(String value) {
                this.title = value;
            }

            /**
             * Gets the value of the firstname property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFIRSTNAME() {
                return firstname;
            }

            /**
             * Sets the value of the firstname property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFIRSTNAME(String value) {
                this.firstname = value;
            }

            /**
             * Gets the value of the middlename property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getMIDDLENAME() {
                return middlename;
            }

            /**
             * Sets the value of the middlename property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setMIDDLENAME(String value) {
                this.middlename = value;
            }

            /**
             * Gets the value of the lastname property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLASTNAME() {
                return lastname;
            }

            /**
             * Sets the value of the lastname property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLASTNAME(String value) {
                this.lastname = value;
            }

            /**
             * Gets the value of the userid property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUSERID() {
                return userid;
            }

            /**
             * Sets the value of the userid property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUSERID(String value) {
                this.userid = value;
            }

            /**
             * Gets the value of the language property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLANGUAGE() {
                return language;
            }

            /**
             * Sets the value of the language property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLANGUAGE(String value) {
                this.language = value;
            }

            /**
             * Gets the value of the email property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEMAIL() {
                return email;
            }

            /**
             * Sets the value of the email property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEMAIL(String value) {
                this.email = value;
            }

            /**
             * Gets the value of the phone property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPHONE() {
                return phone;
            }

            /**
             * Sets the value of the phone property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPHONE(String value) {
                this.phone = value;
            }

            /**
             * Gets the value of the phoneextn property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPHONEEXTN() {
                return phoneextn;
            }

            /**
             * Sets the value of the phoneextn property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPHONEEXTN(String value) {
                this.phoneextn = value;
            }

            /**
             * Gets the value of the vm property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getVM() {
                return vm;
            }

            /**
             * Sets the value of the vm property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setVM(String value) {
                this.vm = value;
            }

            /**
             * Gets the value of the fax property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFAX() {
                return fax;
            }

            /**
             * Sets the value of the fax property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFAX(String value) {
                this.fax = value;
            }

            /**
             * Gets the value of the geography property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGEOGRAPHY() {
                return geography;
            }

            /**
             * Sets the value of the geography property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGEOGRAPHY(String value) {
                this.geography = value;
            }

            /**
             * Gets the value of the country property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCOUNTRY() {
                return country;
            }

            /**
             * Sets the value of the country property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCOUNTRY(String value) {
                this.country = value;
            }

            /**
             * Gets the value of the district property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDISTRICT() {
                return district;
            }

            /**
             * Sets the value of the district property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDISTRICT(String value) {
                this.district = value;
            }

            /**
             * Gets the value of the state property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSTATE() {
                return state;
            }

            /**
             * Sets the value of the state property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSTATE(String value) {
                this.state = value;
            }

            /**
             * Gets the value of the city property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCITY() {
                return city;
            }

            /**
             * Sets the value of the city property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCITY(String value) {
                this.city = value;
            }

            /**
             * Gets the value of the address property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getADDRESS() {
                return address;
            }

            /**
             * Sets the value of the address property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setADDRESS(String value) {
                this.address = value;
            }

            /**
             * Gets the value of the address2 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getADDRESS2() {
                return address2;
            }

            /**
             * Sets the value of the address2 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setADDRESS2(String value) {
                this.address2 = value;
            }

            /**
             * Gets the value of the address3 property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getADDRESS3() {
                return address3;
            }

            /**
             * Sets the value of the address3 property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setADDRESS3(String value) {
                this.address3 = value;
            }

            /**
             * Gets the value of the zip property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getZIP() {
                return zip;
            }

            /**
             * Sets the value of the zip property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setZIP(String value) {
                this.zip = value;
            }

            /**
             * Gets the value of the officename property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOFFICENAME() {
                return officename;
            }

            /**
             * Sets the value of the officename property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOFFICENAME(String value) {
                this.officename = value;
            }

            /**
             * Gets the value of the buildingname property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBUILDINGNAME() {
                return buildingname;
            }

            /**
             * Sets the value of the buildingname property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBUILDINGNAME(String value) {
                this.buildingname = value;
            }

            /**
             * Gets the value of the floor property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFLOOR() {
                return floor;
            }

            /**
             * Sets the value of the floor property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFLOOR(String value) {
                this.floor = value;
            }

            /**
             * Gets the value of the department property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDEPARTMENT() {
                return department;
            }

            /**
             * Sets the value of the department property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDEPARTMENT(String value) {
                this.department = value;
            }

            /**
             * Gets the value of the manager property.
             * 
             * @return
             *     possible object is
             *     {@link MANAGER }
             *     
             */
            public MANAGER getMANAGER() {
                return manager;
            }

            /**
             * Sets the value of the manager property.
             * 
             * @param value
             *     allowed object is
             *     {@link MANAGER }
             *     
             */
            public void setMANAGER(MANAGER value) {
                this.manager = value;
            }

            /**
             * Gets the value of the directreports property.
             * 
             * @return
             *     possible object is
             *     {@link DIRECTREPORTS }
             *     
             */
            public DIRECTREPORTS getDIRECTREPORTS() {
                return directreports;
            }

            /**
             * Sets the value of the directreports property.
             * 
             * @param value
             *     allowed object is
             *     {@link DIRECTREPORTS }
             *     
             */
            public void setDIRECTREPORTS(DIRECTREPORTS value) {
                this.directreports = value;
            }

            /**
             * Gets the value of the assistant property.
             * 
             * @return
             *     possible object is
             *     {@link ASSISTANT }
             *     
             */
            public ASSISTANT getASSISTANT() {
                return assistant;
            }

            /**
             * Sets the value of the assistant property.
             * 
             * @param value
             *     allowed object is
             *     {@link ASSISTANT }
             *     
             */
            public void setASSISTANT(ASSISTANT value) {
                this.assistant = value;
            }

            /**
             * Gets the value of the employeetype property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEMPLOYEETYPE() {
                return employeetype;
            }

            /**
             * Sets the value of the employeetype property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEMPLOYEETYPE(String value) {
                this.employeetype = value;
            }

            /**
             * Gets the value of the employeestatus property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEMPLOYEESTATUS() {
                return employeestatus;
            }

            /**
             * Sets the value of the employeestatus property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEMPLOYEESTATUS(String value) {
                this.employeestatus = value;
            }

            /**
             * Gets the value of the leadershipind property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getLEADERSHIPIND() {
                return leadershipind;
            }

            /**
             * Sets the value of the leadershipind property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setLEADERSHIPIND(String value) {
                this.leadershipind = value;
            }

            /**
             * Gets the value of the enrolledbenefitsind property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getENROLLEDBENEFITSIND() {
                return enrolledbenefitsind;
            }

            /**
             * Sets the value of the enrolledbenefitsind property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setENROLLEDBENEFITSIND(String value) {
                this.enrolledbenefitsind = value;
            }

            /**
             * Gets the value of the datehire property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDATEHIRE() {
                return datehire;
            }

            /**
             * Sets the value of the datehire property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDATEHIRE(String value) {
                this.datehire = value;
            }

            /**
             * Gets the value of the roles property.
             * 
             * @return
             *     possible object is
             *     {@link ROLES }
             *     
             */
            public ROLES getROLES() {
                return roles;
            }

            /**
             * Sets the value of the roles property.
             * 
             * @param value
             *     allowed object is
             *     {@link ROLES }
             *     
             */
            public void setROLES(ROLES value) {
                this.roles = value;
            }

            /**
             * Gets the value of the employeeid property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getEMPLOYEEID() {
                return employeeid;
            }

            /**
             * Sets the value of the employeeid property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setEMPLOYEEID(String value) {
                this.employeeid = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="FIRST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="LAST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="USERID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "firstname",
                "lastname",
                "userid"
            })
            public static class ASSISTANT {

                @XmlElement(name = "FIRST_NAME", required = true)
                protected String firstname;
                @XmlElement(name = "LAST_NAME", required = true)
                protected String lastname;
                @XmlElement(name = "USERID", required = true)
                protected String userid;

                /**
                 * Gets the value of the firstname property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFIRSTNAME() {
                    return firstname;
                }

                /**
                 * Sets the value of the firstname property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFIRSTNAME(String value) {
                    this.firstname = value;
                }

                /**
                 * Gets the value of the lastname property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLASTNAME() {
                    return lastname;
                }

                /**
                 * Sets the value of the lastname property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLASTNAME(String value) {
                    this.lastname = value;
                }

                /**
                 * Gets the value of the userid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUSERID() {
                    return userid;
                }

                /**
                 * Sets the value of the userid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUSERID(String value) {
                    this.userid = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="DIRECT_REPORT" maxOccurs="unbounded" minOccurs="0">
             *           &lt;complexType>
             *             &lt;complexContent>
             *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *                 &lt;sequence>
             *                   &lt;element name="FIRST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="LAST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                   &lt;element name="USERID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *                 &lt;/sequence>
             *               &lt;/restriction>
             *             &lt;/complexContent>
             *           &lt;/complexType>
             *         &lt;/element>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "directreport"
            })
            public static class DIRECTREPORTS {

                @XmlElement(name = "DIRECT_REPORT", required = true)
                protected List<DIRECTREPORT> directreport;

                /**
                 * Gets the value of the directreport property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the directreport property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getDIRECTREPORT().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link DIRECTREPORT }
                 * 
                 * 
                 */
                public List<DIRECTREPORT> getDIRECTREPORT() {
                    if (directreport == null) {
                        directreport = new ArrayList<DIRECTREPORT>();
                    }
                    return this.directreport;
                }


                /**
                 * <p>Java class for anonymous complex type.
                 * 
                 * <p>The following schema fragment specifies the expected content contained within this class.
                 * 
                 * <pre>
                 * &lt;complexType>
                 *   &lt;complexContent>
                 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
                 *       &lt;sequence>
                 *         &lt;element name="FIRST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="LAST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *         &lt;element name="USERID" type="{http://www.w3.org/2001/XMLSchema}string"/>
                 *       &lt;/sequence>
                 *     &lt;/restriction>
                 *   &lt;/complexContent>
                 * &lt;/complexType>
                 * </pre>
                 * 
                 * 
                 */
                @XmlAccessorType(XmlAccessType.FIELD)
                @XmlType(name = "", propOrder = {
                    "firstname",
                    "lastname",
                    "userid"
                })
                public static class DIRECTREPORT {

                    @XmlElement(name = "FIRST_NAME", required = true)
                    protected String firstname;
                    @XmlElement(name = "LAST_NAME", required = true)
                    protected String lastname;
                    @XmlElement(name = "USERID", required = true)
                    protected String userid;

                    /**
                     * Gets the value of the firstname property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getFIRSTNAME() {
                        return firstname;
                    }

                    /**
                     * Sets the value of the firstname property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setFIRSTNAME(String value) {
                        this.firstname = value;
                    }

                    /**
                     * Gets the value of the lastname property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getLASTNAME() {
                        return lastname;
                    }

                    /**
                     * Sets the value of the lastname property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setLASTNAME(String value) {
                        this.lastname = value;
                    }

                    /**
                     * Gets the value of the userid property.
                     * 
                     * @return
                     *     possible object is
                     *     {@link String }
                     *     
                     */
                    public String getUSERID() {
                        return userid;
                    }

                    /**
                     * Sets the value of the userid property.
                     * 
                     * @param value
                     *     allowed object is
                     *     {@link String }
                     *     
                     */
                    public void setUSERID(String value) {
                        this.userid = value;
                    }

                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="FIRST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="LAST_NAME" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *         &lt;element name="USERID" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "firstname",
                "lastname",
                "userid"
            })
            public static class MANAGER {

                @XmlElement(name = "FIRST_NAME", required = true)
                protected String firstname;
                @XmlElement(name = "LAST_NAME", required = true)
                protected String lastname;
                @XmlElement(name = "USERID", required = true)
                protected String userid;

                /**
                 * Gets the value of the firstname property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getFIRSTNAME() {
                    return firstname;
                }

                /**
                 * Sets the value of the firstname property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setFIRSTNAME(String value) {
                    this.firstname = value;
                }

                /**
                 * Gets the value of the lastname property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLASTNAME() {
                    return lastname;
                }

                /**
                 * Sets the value of the lastname property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLASTNAME(String value) {
                    this.lastname = value;
                }

                /**
                 * Gets the value of the userid property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getUSERID() {
                    return userid;
                }

                /**
                 * Sets the value of the userid property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setUSERID(String value) {
                    this.userid = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="ROLE" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "role"
            })
            public static class ROLES {

                @XmlElement(name = "ROLE", required = true)
                protected List<String> role;

                /**
                 * Gets the value of the role property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the role property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getROLE().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link String }
                 * 
                 * 
                 */
                public List<String> getROLE() {
                    if (role == null) {
                        role = new ArrayList<String>();
                    }
                    return this.role;
                }

            }

        }

    }

}
