package com.coach.employees.service.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 *  Class is used to GetAllEmployeesDescriptionRequest. 
 * @author GalaxE.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "empDesc"
})
@XmlRootElement(name = "getAllEmployeesDescriptionRequest")
public class GetAllEmployeesDescriptionRequest {

    @XmlElement(name = "EMP_Desc", required = true)
    protected EMPDesc empDesc;

    /**
     * Gets the value of the empDesc property.
     * 
     * @return
     *     possible object is
     *     {@link EMPDesc }
     *     
     */
    public EMPDesc getEMPDesc() {
        return empDesc;
    }

    /**
     * Sets the value of the empDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link EMPDesc }
     *     
     */
    public void setEMPDesc(EMPDesc value) {
        this.empDesc = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class EMPDesc {


    }

}
