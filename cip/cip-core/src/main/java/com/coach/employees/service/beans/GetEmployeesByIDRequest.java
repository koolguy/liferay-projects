
package com.coach.employees.service.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Class is used to GetEmployeesByIDRequest. 
 * @author GalaxE.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "empByUserId"
})
@XmlRootElement(name = "getEmployeesByIDRequest")
public class GetEmployeesByIDRequest {

    @XmlElement(name = "EMP_ByUserId", required = true)
    protected EMPByUserId empByUserId;

    /**
     * Gets the value of the empByUserId property.
     * 
     * @return
     *     possible object is
     *     {@link EMPByUserId }
     *     
     */
    public EMPByUserId getEMPByUserId() {
        return empByUserId;
    }

    /**
     * Sets the value of the empByUserId property.
     * 
     * @param value
     *     allowed object is
     *     {@link EMPByUserId }
     *     
     */
    public void setEMPByUserId(EMPByUserId value) {
        this.empByUserId = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="strADUserID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "strADUserID"
    })
    public static class EMPByUserId {

        @XmlElement(required = true)
        protected String strADUserID;

        /**
         * Gets the value of the strADUserID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStrADUserID() {
            return strADUserID;
        }

        /**
         * Sets the value of the strADUserID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStrADUserID(String value) {
            this.strADUserID = value;
        }

    }

}
