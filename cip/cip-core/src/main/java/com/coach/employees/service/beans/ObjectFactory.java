
package com.coach.employees.service.beans;

import javax.xml.bind.annotation.XmlRegistry;
import com.coach.employees.service.beans.GetAllEmployeesDepartmentsRequest.EMPDepartment;
import com.coach.employees.service.beans.GetAllEmployeesDescriptionRequest.EMPDesc;
import com.coach.employees.service.beans.GetAllEmployeesGeographyRequest.EMPGeography;
import com.coach.employees.service.beans.GetAllEmployeesTitleRequest.EMPTitle;
import com.coach.employees.service.beans.GetEmployeesByDepartmentRequest.EMPByDepartment;
import com.coach.employees.service.beans.GetEmployeesByEmailRequest.EMPByEmail;
import com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsRequest.EMPByDetails;
import com.coach.employees.service.beans.GetEmployeesByIDRequest.EMPByUserId;
import com.coach.employees.service.beans.GetEmployeesByKeywordRequest.EMPByKeyword;
import com.coach.employees.service.beans.GetEmployeesByLocationRequest.EMPByLocation;
import com.coach.employees.service.beans.GetEmployeesByLocationRequest.EMPByLocation.Location;
import com.coach.employees.service.beans.GetEmployeesByNameLocationRequest.EMPByNameLocation;
import com.coach.employees.service.beans.GetEmployeesByNameRequest.EMPByName;
import com.coach.employees.service.beans.GetEmployeesByPhoneRequest.EMPByPhone;
import com.coach.employees.service.beans.GetEmployeesDescriptionByKeywordRequest.EMPDescByKeyword;
import com.coach.employees.service.beans.GetEmployeesDescriptionByTitleRequest.EMPDescByTitle;
import com.coach.employees.service.beans.IsEmployeeEnabledRequest.EMPEnabled;
import com.coach.employees.service.beans.IsEmployeeExistRequest.EMPExist;
import com.coach.employees.service.beans.IsUSEmployeeEnabledRequest.EMPUSEnabled;
import com.coach.employees.service.beans.IsUSEmployeeExistRequest.EMPUSExist;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.coach.employees.service.beans package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.coach.employees.service.beans
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployessByLocationResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT }
     * 
     */
    public com.coach.employees.service.beans.GetEmployessByLocationResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT createGetEmployessByLocationResponseEmployeesEmployeeDIRECTREPORTSDIRECTREPORT() {
        return new com.coach.employees.service.beans.GetEmployessByLocationResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees.Employee }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees.Employee createGetEmployeesByEmployeeDetailsResponseEmployeesEmployee() {
        return new com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees.Employee();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees.Employee.ROLES }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees.Employee.ROLES createGetEmployeesByNameResponseEmployeesEmployeeROLES() {
        return new com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees.Employee.ROLES();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByNameLocationResponse.Employees.Employee.ROLES }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByNameLocationResponse.Employees.Employee.ROLES createGetEmployeesByNameLocationResponseEmployeesEmployeeROLES() {
        return new com.coach.employees.service.beans.GetEmployeesByNameLocationResponse.Employees.Employee.ROLES();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT createGetEmployeesByEmployeeDetailsResponseEmployeesEmployeeDIRECTREPORTSDIRECTREPORT() {
        return new com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT();
    }

    /**
     * Create an instance of {@link GetEmployeesByIDResponse }
     * 
     */
    public GetEmployeesByIDResponse createGetEmployeesByIDResponse() {
        return new GetEmployeesByIDResponse();
    }

    /**
     * Create an instance of {@link GetEmployeesByNameLocationRequest }
     * 
     */
    public GetEmployeesByNameLocationRequest createGetEmployeesByNameLocationRequest() {
        return new GetEmployeesByNameLocationRequest();
    }

    /**
     * Create an instance of {@link GetEmployeesByNameLocationFault }
     * 
     */
    public GetEmployeesByNameLocationFault createGetEmployeesByNameLocationFault() {
        return new GetEmployeesByNameLocationFault();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByEmailResponse.Employees.Employee.DIRECTREPORTS }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByEmailResponse.Employees.Employee.DIRECTREPORTS createGetEmployeesByEmailResponseEmployeesEmployeeDIRECTREPORTS() {
        return new com.coach.employees.service.beans.GetEmployeesByEmailResponse.Employees.Employee.DIRECTREPORTS();
    }

    /**
     * Create an instance of {@link GetEmployeesByDepartmentResponse }
     * 
     */
    public GetEmployeesByDepartmentResponse createGetEmployeesByDepartmentResponse() {
        return new GetEmployeesByDepartmentResponse();
    }

    /**
     * Create an instance of {@link EMPUSExist }
     * 
     */
    public EMPUSExist createIsUSEmployeeExistRequestEMPUSExist() {
        return new EMPUSExist();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees.Employee.DIRECTREPORTS }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees.Employee.DIRECTREPORTS createGetEmployeesByKeywordResponseEmployeesEmployeeDIRECTREPORTS() {
        return new com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees.Employee.DIRECTREPORTS();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesDescriptionByTitleResponse.EmployeeDesc }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesDescriptionByTitleResponse.EmployeeDesc createGetEmployeesDescriptionByTitleResponseEmployeeDesc() {
        return new com.coach.employees.service.beans.GetEmployeesDescriptionByTitleResponse.EmployeeDesc();
    }

    /**
     * Create an instance of {@link IsUSEmployeeExistRequest }
     * 
     */
    public IsUSEmployeeExistRequest createIsUSEmployeeExistRequest() {
        return new IsUSEmployeeExistRequest();
    }

    /**
     * Create an instance of {@link EMPEnabled }
     * 
     */
    public EMPEnabled createIsEmployeeEnabledRequestEMPEnabled() {
        return new EMPEnabled();
    }

    /**
     * Create an instance of {@link EMPGeography }
     * 
     */
    public EMPGeography createGetAllEmployeesGeographyRequestEMPGeography() {
        return new EMPGeography();
    }

    /**
     * Create an instance of {@link GetEmployeesByLocationFault }
     * 
     */
    public GetEmployeesByLocationFault createGetEmployeesByLocationFault() {
        return new GetEmployeesByLocationFault();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByEmailResponse.Employees.Employee.ROLES }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByEmailResponse.Employees.Employee.ROLES createGetEmployeesByEmailResponseEmployeesEmployeeROLES() {
        return new com.coach.employees.service.beans.GetEmployeesByEmailResponse.Employees.Employee.ROLES();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByNameLocationResponse.Employees.Employee.ASSISTANT }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByNameLocationResponse.Employees.Employee.ASSISTANT createGetEmployeesByNameLocationResponseEmployeesEmployeeASSISTANT() {
        return new com.coach.employees.service.beans.GetEmployeesByNameLocationResponse.Employees.Employee.ASSISTANT();
    }

    /**
     * Create an instance of {@link GetEmployeesByLocationRequest }
     * 
     */
    public GetEmployeesByLocationRequest createGetEmployeesByLocationRequest() {
        return new GetEmployeesByLocationRequest();
    }

    /**
     * Create an instance of {@link GetEmployeesByDepartmentRequest }
     * 
     */
    public GetEmployeesByDepartmentRequest createGetEmployeesByDepartmentRequest() {
        return new GetEmployeesByDepartmentRequest();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesDescriptionByKeywordResponse.EmployeeDesc.EmployeeDescription }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesDescriptionByKeywordResponse.EmployeeDesc.EmployeeDescription createGetEmployeesDescriptionByKeywordResponseEmployeeDescEmployeeDescription() {
        return new com.coach.employees.service.beans.GetEmployeesDescriptionByKeywordResponse.EmployeeDesc.EmployeeDescription();
    }

    /**
     * Create an instance of {@link GetEmployessByEmployeeDetailsFault }
     * 
     */
    public GetEmployessByEmployeeDetailsFault createGetEmployessByEmployeeDetailsFault() {
        return new GetEmployessByEmployeeDetailsFault();
    }

    /**
     * Create an instance of {@link GetEmployeesByEmailRequest }
     * 
     */
    public GetEmployeesByEmailRequest createGetEmployeesByEmailRequest() {
        return new GetEmployeesByEmailRequest();
    }

    /**
     * Create an instance of {@link GetEmployeesDescriptionByKeywordResponse }
     * 
     */
    public GetEmployeesDescriptionByKeywordResponse createGetEmployeesDescriptionByKeywordResponse() {
        return new GetEmployeesDescriptionByKeywordResponse();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployessByLocationResponse.Employees.Employee }
     * 
     */
    public com.coach.employees.service.beans.GetEmployessByLocationResponse.Employees.Employee createGetEmployessByLocationResponseEmployeesEmployee() {
        return new com.coach.employees.service.beans.GetEmployessByLocationResponse.Employees.Employee();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT createGetEmployeesByKeywordResponseEmployeesEmployeeDIRECTREPORTSDIRECTREPORT() {
        return new com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByDepartmentResponse.Employees }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByDepartmentResponse.Employees createGetEmployeesByDepartmentResponseEmployees() {
        return new com.coach.employees.service.beans.GetEmployeesByDepartmentResponse.Employees();
    }

    /**
     * Create an instance of {@link IsUSEmployeeExistFault }
     * 
     */
    public IsUSEmployeeExistFault createIsUSEmployeeExistFault() {
        return new IsUSEmployeeExistFault();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByNameLocationResponse.Employees.Employee.MANAGER }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByNameLocationResponse.Employees.Employee.MANAGER createGetEmployeesByNameLocationResponseEmployeesEmployeeMANAGER() {
        return new com.coach.employees.service.beans.GetEmployeesByNameLocationResponse.Employees.Employee.MANAGER();
    }

    /**
     * Create an instance of {@link EMPByDetails }
     * 
     */
    public EMPByDetails createGetEmployeesByEmployeeDetailsRequestEMPByDetails() {
        return new EMPByDetails();
    }

    /**
     * Create an instance of {@link IsEmployeeExistResponse }
     * 
     */
    public IsEmployeeExistResponse createIsEmployeeExistResponse() {
        return new IsEmployeeExistResponse();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees.Employee.MANAGER }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees.Employee.MANAGER createGetEmployeesByEmployeeDetailsResponseEmployeesEmployeeMANAGER() {
        return new com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees.Employee.MANAGER();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees.Employee }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees.Employee createGetEmployeesByKeywordResponseEmployeesEmployee() {
        return new com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees.Employee();
    }

    /**
     * Create an instance of {@link GetEmployeesByNameRequest }
     * 
     */
    public GetEmployeesByNameRequest createGetEmployeesByNameRequest() {
        return new GetEmployeesByNameRequest();
    }

    /**
     * Create an instance of {@link GetEmployeesByPhoneRequest }
     * 
     */
    public GetEmployeesByPhoneRequest createGetEmployeesByPhoneRequest() {
        return new GetEmployeesByPhoneRequest();
    }

    /**
     * Create an instance of {@link GetEmployeesByKeywordRequest }
     * 
     */
    public GetEmployeesByKeywordRequest createGetEmployeesByKeywordRequest() {
        return new GetEmployeesByKeywordRequest();
    }

    /**
     * Create an instance of {@link IsUSEmployeeEnabledRequest }
     * 
     */
    public IsUSEmployeeEnabledRequest createIsUSEmployeeEnabledRequest() {
        return new IsUSEmployeeEnabledRequest();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesDescriptionByTitleResponse.EmployeeDesc.EmployeeDescription }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesDescriptionByTitleResponse.EmployeeDesc.EmployeeDescription createGetEmployeesDescriptionByTitleResponseEmployeeDescEmployeeDescription() {
        return new com.coach.employees.service.beans.GetEmployeesDescriptionByTitleResponse.EmployeeDesc.EmployeeDescription();
    }

    /**
     * Create an instance of {@link EMPDesc }
     * 
     */
    public EMPDesc createGetAllEmployeesDescriptionRequestEMPDesc() {
        return new EMPDesc();
    }

    /**
     * Create an instance of {@link GetAllEmployeesTitleFault }
     * 
     */
    public GetAllEmployeesTitleFault createGetAllEmployeesTitleFault() {
        return new GetAllEmployeesTitleFault();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployessByLocationResponse.Employees.Employee.ASSISTANT }
     * 
     */
    public com.coach.employees.service.beans.GetEmployessByLocationResponse.Employees.Employee.ASSISTANT createGetEmployessByLocationResponseEmployeesEmployeeASSISTANT() {
        return new com.coach.employees.service.beans.GetEmployessByLocationResponse.Employees.Employee.ASSISTANT();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByDepartmentResponse.Employees.Employee.MANAGER }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByDepartmentResponse.Employees.Employee.MANAGER createGetEmployeesByDepartmentResponseEmployeesEmployeeMANAGER() {
        return new com.coach.employees.service.beans.GetEmployeesByDepartmentResponse.Employees.Employee.MANAGER();
    }

    /**
     * Create an instance of {@link EMPByKeyword }
     * 
     */
    public EMPByKeyword createGetEmployeesByKeywordRequestEMPByKeyword() {
        return new EMPByKeyword();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees.Employee.ROLES }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees.Employee.ROLES createGetEmployeesByKeywordResponseEmployeesEmployeeROLES() {
        return new com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees.Employee.ROLES();
    }

    /**
     * Create an instance of {@link GetEmployeesDescriptionByKeywordFault }
     * 
     */
    public GetEmployeesDescriptionByKeywordFault createGetEmployeesDescriptionByKeywordFault() {
        return new GetEmployeesDescriptionByKeywordFault();
    }

    /**
     * Create an instance of {@link GetAllEmployeesDescriptionRequest }
     * 
     */
    public GetAllEmployeesDescriptionRequest createGetAllEmployeesDescriptionRequest() {
        return new GetAllEmployeesDescriptionRequest();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByDepartmentResponse.Employees.Employee.ROLES }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByDepartmentResponse.Employees.Employee.ROLES createGetEmployeesByDepartmentResponseEmployeesEmployeeROLES() {
        return new com.coach.employees.service.beans.GetEmployeesByDepartmentResponse.Employees.Employee.ROLES();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByPhoneResponse.Employees.Employee.ASSISTANT }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByPhoneResponse.Employees.Employee.ASSISTANT createGetEmployeesByPhoneResponseEmployeesEmployeeASSISTANT() {
        return new com.coach.employees.service.beans.GetEmployeesByPhoneResponse.Employees.Employee.ASSISTANT();
    }

    /**
     * Create an instance of {@link GetEmployessByLocationResponse }
     * 
     */
    public GetEmployessByLocationResponse createGetEmployessByLocationResponse() {
        return new GetEmployessByLocationResponse();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByEmailResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByEmailResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT createGetEmployeesByEmailResponseEmployeesEmployeeDIRECTREPORTSDIRECTREPORT() {
        return new com.coach.employees.service.beans.GetEmployeesByEmailResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee.MANAGER }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee.MANAGER createGetEmployeesByIDResponseEmployeesEmployeeMANAGER() {
        return new com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee.MANAGER();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByDepartmentResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByDepartmentResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT createGetEmployeesByDepartmentResponseEmployeesEmployeeDIRECTREPORTSDIRECTREPORT() {
        return new com.coach.employees.service.beans.GetEmployeesByDepartmentResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT();
    }

    /**
     * Create an instance of {@link GetEmployeesByEmployeeDetailsRequest }
     * 
     */
    public GetEmployeesByEmployeeDetailsRequest createGetEmployeesByEmployeeDetailsRequest() {
        return new GetEmployeesByEmployeeDetailsRequest();
    }

    /**
     * Create an instance of {@link GetAllEmployeesDescriptionFault }
     * 
     */
    public GetAllEmployeesDescriptionFault createGetAllEmployeesDescriptionFault() {
        return new GetAllEmployeesDescriptionFault();
    }

    /**
     * Create an instance of {@link EMPByNameLocation }
     * 
     */
    public EMPByNameLocation createGetEmployeesByNameLocationRequestEMPByNameLocation() {
        return new EMPByNameLocation();
    }

    /**
     * Create an instance of {@link EMPDepartment }
     * 
     */
    public EMPDepartment createGetAllEmployeesDepartmentsRequestEMPDepartment() {
        return new EMPDepartment();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees.Employee.DIRECTREPORTS }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees.Employee.DIRECTREPORTS createGetEmployeesByEmployeeDetailsResponseEmployeesEmployeeDIRECTREPORTS() {
        return new com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees.Employee.DIRECTREPORTS();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees.Employee.ROLES }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees.Employee.ROLES createGetEmployeesByEmployeeDetailsResponseEmployeesEmployeeROLES() {
        return new com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees.Employee.ROLES();
    }

    /**
     * Create an instance of {@link GetAllEmployeesGeographyResponse }
     * 
     */
    public GetAllEmployeesGeographyResponse createGetAllEmployeesGeographyResponse() {
        return new GetAllEmployeesGeographyResponse();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByNameLocationResponse.Employees }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByNameLocationResponse.Employees createGetEmployeesByNameLocationResponseEmployees() {
        return new com.coach.employees.service.beans.GetEmployeesByNameLocationResponse.Employees();
    }

    /**
     * Create an instance of {@link GetEmployeesByPhoneFault }
     * 
     */
    public GetEmployeesByPhoneFault createGetEmployeesByPhoneFault() {
        return new GetEmployeesByPhoneFault();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByEmailResponse.Employees }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByEmailResponse.Employees createGetEmployeesByEmailResponseEmployees() {
        return new com.coach.employees.service.beans.GetEmployeesByEmailResponse.Employees();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetAllEmployeesDescriptionResponse.EmployeeDesc }
     * 
     */
    public com.coach.employees.service.beans.GetAllEmployeesDescriptionResponse.EmployeeDesc createGetAllEmployeesDescriptionResponseEmployeeDesc() {
        return new com.coach.employees.service.beans.GetAllEmployeesDescriptionResponse.EmployeeDesc();
    }

    /**
     * Create an instance of {@link GetAllEmployeesDescriptionResponse }
     * 
     */
    public GetAllEmployeesDescriptionResponse createGetAllEmployeesDescriptionResponse() {
        return new GetAllEmployeesDescriptionResponse();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees createGetEmployeesByKeywordResponseEmployees() {
        return new com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees createGetEmployeesByIDResponseEmployees() {
        return new com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees();
    }

    /**
     * Create an instance of {@link IsUSEmployeeExistResponse }
     * 
     */
    public IsUSEmployeeExistResponse createIsUSEmployeeExistResponse() {
        return new IsUSEmployeeExistResponse();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByEmailResponse.Employees.Employee.MANAGER }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByEmailResponse.Employees.Employee.MANAGER createGetEmployeesByEmailResponseEmployeesEmployeeMANAGER() {
        return new com.coach.employees.service.beans.GetEmployeesByEmailResponse.Employees.Employee.MANAGER();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee.ASSISTANT }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee.ASSISTANT createGetEmployeesByIDResponseEmployeesEmployeeASSISTANT() {
        return new com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee.ASSISTANT();
    }

    /**
     * Create an instance of {@link GetEmployeesByIDFault }
     * 
     */
    public GetEmployeesByIDFault createGetEmployeesByIDFault() {
        return new GetEmployeesByIDFault();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByNameLocationResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByNameLocationResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT createGetEmployeesByNameLocationResponseEmployeesEmployeeDIRECTREPORTSDIRECTREPORT() {
        return new com.coach.employees.service.beans.GetEmployeesByNameLocationResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT();
    }

    /**
     * Create an instance of {@link Location }
     * 
     */
    public Location createGetEmployeesByLocationRequestEMPByLocationLocation() {
        return new Location();
    }

    /**
     * Create an instance of {@link EMPByUserId }
     * 
     */
    public EMPByUserId createGetEmployeesByIDRequestEMPByUserId() {
        return new EMPByUserId();
    }

    /**
     * Create an instance of {@link EMPDescByKeyword }
     * 
     */
    public EMPDescByKeyword createGetEmployeesDescriptionByKeywordRequestEMPDescByKeyword() {
        return new EMPDescByKeyword();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByEmailResponse.Employees.Employee }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByEmailResponse.Employees.Employee createGetEmployeesByEmailResponseEmployeesEmployee() {
        return new com.coach.employees.service.beans.GetEmployeesByEmailResponse.Employees.Employee();
    }

    /**
     * Create an instance of {@link GetAllEmployeesTitleResponse }
     * 
     */
    public GetAllEmployeesTitleResponse createGetAllEmployeesTitleResponse() {
        return new GetAllEmployeesTitleResponse();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees.Employee.MANAGER }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees.Employee.MANAGER createGetEmployeesByKeywordResponseEmployeesEmployeeMANAGER() {
        return new com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees.Employee.MANAGER();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByPhoneResponse.Employees }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByPhoneResponse.Employees createGetEmployeesByPhoneResponseEmployees() {
        return new com.coach.employees.service.beans.GetEmployeesByPhoneResponse.Employees();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByDepartmentResponse.Employees.Employee.ASSISTANT }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByDepartmentResponse.Employees.Employee.ASSISTANT createGetEmployeesByDepartmentResponseEmployeesEmployeeASSISTANT() {
        return new com.coach.employees.service.beans.GetEmployeesByDepartmentResponse.Employees.Employee.ASSISTANT();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByDepartmentResponse.Employees.Employee }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByDepartmentResponse.Employees.Employee createGetEmployeesByDepartmentResponseEmployeesEmployee() {
        return new com.coach.employees.service.beans.GetEmployeesByDepartmentResponse.Employees.Employee();
    }

    /**
     * Create an instance of {@link GetEmployeesByEmailResponse }
     * 
     */
    public GetEmployeesByEmailResponse createGetEmployeesByEmailResponse() {
        return new GetEmployeesByEmailResponse();
    }

    /**
     * Create an instance of {@link IsEmployeeExistFault }
     * 
     */
    public IsEmployeeExistFault createIsEmployeeExistFault() {
        return new IsEmployeeExistFault();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees.Employee.MANAGER }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees.Employee.MANAGER createGetEmployeesByNameResponseEmployeesEmployeeMANAGER() {
        return new com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees.Employee.MANAGER();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees.Employee.ASSISTANT }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees.Employee.ASSISTANT createGetEmployeesByKeywordResponseEmployeesEmployeeASSISTANT() {
        return new com.coach.employees.service.beans.GetEmployeesByKeywordResponse.Employees.Employee.ASSISTANT();
    }

    /**
     * Create an instance of {@link IsEmployeeExistRequest }
     * 
     */
    public IsEmployeeExistRequest createIsEmployeeExistRequest() {
        return new IsEmployeeExistRequest();
    }

    /**
     * Create an instance of {@link EMPByPhone }
     * 
     */
    public EMPByPhone createGetEmployeesByPhoneRequestEMPByPhone() {
        return new EMPByPhone();
    }

    /**
     * Create an instance of {@link GetEmployeesByKeywordResponse }
     * 
     */
    public GetEmployeesByKeywordResponse createGetEmployeesByKeywordResponse() {
        return new GetEmployeesByKeywordResponse();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT createGetEmployeesByNameResponseEmployeesEmployeeDIRECTREPORTSDIRECTREPORT() {
        return new com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT();
    }

    /**
     * Create an instance of {@link EMPUSEnabled }
     * 
     */
    public EMPUSEnabled createIsUSEmployeeEnabledRequestEMPUSEnabled() {
        return new EMPUSEnabled();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByPhoneResponse.Employees.Employee.ROLES }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByPhoneResponse.Employees.Employee.ROLES createGetEmployeesByPhoneResponseEmployeesEmployeeROLES() {
        return new com.coach.employees.service.beans.GetEmployeesByPhoneResponse.Employees.Employee.ROLES();
    }

    /**
     * Create an instance of {@link GetEmployeesByNameFault }
     * 
     */
    public GetEmployeesByNameFault createGetEmployeesByNameFault() {
        return new GetEmployeesByNameFault();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployessByLocationResponse.Employees }
     * 
     */
    public com.coach.employees.service.beans.GetEmployessByLocationResponse.Employees createGetEmployessByLocationResponseEmployees() {
        return new com.coach.employees.service.beans.GetEmployessByLocationResponse.Employees();
    }

    /**
     * Create an instance of {@link GetAllEmployeesDepartmentsRequest }
     * 
     */
    public GetAllEmployeesDepartmentsRequest createGetAllEmployeesDepartmentsRequest() {
        return new GetAllEmployeesDepartmentsRequest();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee.DIRECTREPORTS }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee.DIRECTREPORTS createGetEmployeesByIDResponseEmployeesEmployeeDIRECTREPORTS() {
        return new com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee.DIRECTREPORTS();
    }

    /**
     * Create an instance of {@link GetAllEmployeesTitleRequest }
     * 
     */
    public GetAllEmployeesTitleRequest createGetAllEmployeesTitleRequest() {
        return new GetAllEmployeesTitleRequest();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByNameLocationResponse.Employees.Employee }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByNameLocationResponse.Employees.Employee createGetEmployeesByNameLocationResponseEmployeesEmployee() {
        return new com.coach.employees.service.beans.GetEmployeesByNameLocationResponse.Employees.Employee();
    }

    /**
     * Create an instance of {@link IsEmployeeEnabledFault }
     * 
     */
    public IsEmployeeEnabledFault createIsEmployeeEnabledFault() {
        return new IsEmployeeEnabledFault();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetAllEmployeesDescriptionResponse.EmployeeDesc.EmployeeDescription }
     * 
     */
    public com.coach.employees.service.beans.GetAllEmployeesDescriptionResponse.EmployeeDesc.EmployeeDescription createGetAllEmployeesDescriptionResponseEmployeeDescEmployeeDescription() {
        return new com.coach.employees.service.beans.GetAllEmployeesDescriptionResponse.EmployeeDesc.EmployeeDescription();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByPhoneResponse.Employees.Employee.MANAGER }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByPhoneResponse.Employees.Employee.MANAGER createGetEmployeesByPhoneResponseEmployeesEmployeeMANAGER() {
        return new com.coach.employees.service.beans.GetEmployeesByPhoneResponse.Employees.Employee.MANAGER();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByEmailResponse.Employees.Employee.ASSISTANT }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByEmailResponse.Employees.Employee.ASSISTANT createGetEmployeesByEmailResponseEmployeesEmployeeASSISTANT() {
        return new com.coach.employees.service.beans.GetEmployeesByEmailResponse.Employees.Employee.ASSISTANT();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByPhoneResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByPhoneResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT createGetEmployeesByPhoneResponseEmployeesEmployeeDIRECTREPORTSDIRECTREPORT() {
        return new com.coach.employees.service.beans.GetEmployeesByPhoneResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployessByLocationResponse.Employees.Employee.ROLES }
     * 
     */
    public com.coach.employees.service.beans.GetEmployessByLocationResponse.Employees.Employee.ROLES createGetEmployessByLocationResponseEmployeesEmployeeROLES() {
        return new com.coach.employees.service.beans.GetEmployessByLocationResponse.Employees.Employee.ROLES();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee createGetEmployeesByIDResponseEmployeesEmployee() {
        return new com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee();
    }

    /**
     * Create an instance of {@link GetEmployeesByIDRequest }
     * 
     */
    public GetEmployeesByIDRequest createGetEmployeesByIDRequest() {
        return new GetEmployeesByIDRequest();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByDepartmentResponse.Employees.Employee.DIRECTREPORTS }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByDepartmentResponse.Employees.Employee.DIRECTREPORTS createGetEmployeesByDepartmentResponseEmployeesEmployeeDIRECTREPORTS() {
        return new com.coach.employees.service.beans.GetEmployeesByDepartmentResponse.Employees.Employee.DIRECTREPORTS();
    }

    /**
     * Create an instance of {@link GetEmployeesDescriptionByTitleResponse }
     * 
     */
    public GetEmployeesDescriptionByTitleResponse createGetEmployeesDescriptionByTitleResponse() {
        return new GetEmployeesDescriptionByTitleResponse();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees.Employee.DIRECTREPORTS }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees.Employee.DIRECTREPORTS createGetEmployeesByNameResponseEmployeesEmployeeDIRECTREPORTS() {
        return new com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees.Employee.DIRECTREPORTS();
    }

    /**
     * Create an instance of {@link GetEmployeesByDepartmentFault }
     * 
     */
    public GetEmployeesByDepartmentFault createGetEmployeesByDepartmentFault() {
        return new GetEmployeesByDepartmentFault();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByPhoneResponse.Employees.Employee }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByPhoneResponse.Employees.Employee createGetEmployeesByPhoneResponseEmployeesEmployee() {
        return new com.coach.employees.service.beans.GetEmployeesByPhoneResponse.Employees.Employee();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees createGetEmployeesByEmployeeDetailsResponseEmployees() {
        return new com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees();
    }

    /**
     * Create an instance of {@link GetEmployeesDescriptionByTitleFault }
     * 
     */
    public GetEmployeesDescriptionByTitleFault createGetEmployeesDescriptionByTitleFault() {
        return new GetEmployeesDescriptionByTitleFault();
    }

    /**
     * Create an instance of {@link EMPByName }
     * 
     */
    public EMPByName createGetEmployeesByNameRequestEMPByName() {
        return new EMPByName();
    }

    /**
     * Create an instance of {@link GetAllEmployeesGeographyRequest }
     * 
     */
    public GetAllEmployeesGeographyRequest createGetAllEmployeesGeographyRequest() {
        return new GetAllEmployeesGeographyRequest();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByPhoneResponse.Employees.Employee.DIRECTREPORTS }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByPhoneResponse.Employees.Employee.DIRECTREPORTS createGetEmployeesByPhoneResponseEmployeesEmployeeDIRECTREPORTS() {
        return new com.coach.employees.service.beans.GetEmployeesByPhoneResponse.Employees.Employee.DIRECTREPORTS();
    }

    /**
     * Create an instance of {@link GetEmployeesByPhoneResponse }
     * 
     */
    public GetEmployeesByPhoneResponse createGetEmployeesByPhoneResponse() {
        return new GetEmployeesByPhoneResponse();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByNameLocationResponse.Employees.Employee.DIRECTREPORTS }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByNameLocationResponse.Employees.Employee.DIRECTREPORTS createGetEmployeesByNameLocationResponseEmployeesEmployeeDIRECTREPORTS() {
        return new com.coach.employees.service.beans.GetEmployeesByNameLocationResponse.Employees.Employee.DIRECTREPORTS();
    }

    /**
     * Create an instance of {@link IsUSEmployeeEnabledFault }
     * 
     */
    public IsUSEmployeeEnabledFault createIsUSEmployeeEnabledFault() {
        return new IsUSEmployeeEnabledFault();
    }

    /**
     * Create an instance of {@link EMPDescByTitle }
     * 
     */
    public EMPDescByTitle createGetEmployeesDescriptionByTitleRequestEMPDescByTitle() {
        return new EMPDescByTitle();
    }

    /**
     * Create an instance of {@link IsEmployeeEnabledResponse }
     * 
     */
    public IsEmployeeEnabledResponse createIsEmployeeEnabledResponse() {
        return new IsEmployeeEnabledResponse();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesDescriptionByKeywordResponse.EmployeeDesc }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesDescriptionByKeywordResponse.EmployeeDesc createGetEmployeesDescriptionByKeywordResponseEmployeeDesc() {
        return new com.coach.employees.service.beans.GetEmployeesDescriptionByKeywordResponse.EmployeeDesc();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployessByLocationResponse.Employees.Employee.MANAGER }
     * 
     */
    public com.coach.employees.service.beans.GetEmployessByLocationResponse.Employees.Employee.MANAGER createGetEmployessByLocationResponseEmployeesEmployeeMANAGER() {
        return new com.coach.employees.service.beans.GetEmployessByLocationResponse.Employees.Employee.MANAGER();
    }

    /**
     * Create an instance of {@link GetEmployeesDescriptionByTitleRequest }
     * 
     */
    public GetEmployeesDescriptionByTitleRequest createGetEmployeesDescriptionByTitleRequest() {
        return new GetEmployeesDescriptionByTitleRequest();
    }

    /**
     * Create an instance of {@link EMPTitle }
     * 
     */
    public EMPTitle createGetAllEmployeesTitleRequestEMPTitle() {
        return new EMPTitle();
    }

    /**
     * Create an instance of {@link GetAllEmployeesGeographyFault }
     * 
     */
    public GetAllEmployeesGeographyFault createGetAllEmployeesGeographyFault() {
        return new GetAllEmployeesGeographyFault();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees createGetEmployeesByNameResponseEmployees() {
        return new com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees.Employee }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees.Employee createGetEmployeesByNameResponseEmployeesEmployee() {
        return new com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees.Employee();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployessByLocationResponse.Employees.Employee.DIRECTREPORTS }
     * 
     */
    public com.coach.employees.service.beans.GetEmployessByLocationResponse.Employees.Employee.DIRECTREPORTS createGetEmployessByLocationResponseEmployeesEmployeeDIRECTREPORTS() {
        return new com.coach.employees.service.beans.GetEmployessByLocationResponse.Employees.Employee.DIRECTREPORTS();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT createGetEmployeesByIDResponseEmployeesEmployeeDIRECTREPORTSDIRECTREPORT() {
        return new com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee.DIRECTREPORTS.DIRECTREPORT();
    }

    /**
     * Create an instance of {@link GetAllEmployeesDepartmentsFault }
     * 
     */
    public GetAllEmployeesDepartmentsFault createGetAllEmployeesDepartmentsFault() {
        return new GetAllEmployeesDepartmentsFault();
    }

    /**
     * Create an instance of {@link GetEmployeesByEmailFault }
     * 
     */
    public GetEmployeesByEmailFault createGetEmployeesByEmailFault() {
        return new GetEmployeesByEmailFault();
    }

    /**
     * Create an instance of {@link IsUSEmployeeEnabledResponse }
     * 
     */
    public IsUSEmployeeEnabledResponse createIsUSEmployeeEnabledResponse() {
        return new IsUSEmployeeEnabledResponse();
    }

    /**
     * Create an instance of {@link GetAllEmployeesDepartmentsResponse }
     * 
     */
    public GetAllEmployeesDepartmentsResponse createGetAllEmployeesDepartmentsResponse() {
        return new GetAllEmployeesDepartmentsResponse();
    }

    /**
     * Create an instance of {@link GetEmployeesByNameResponse }
     * 
     */
    public GetEmployeesByNameResponse createGetEmployeesByNameResponse() {
        return new GetEmployeesByNameResponse();
    }

    /**
     * Create an instance of {@link GetEmployeesDescriptionByKeywordRequest }
     * 
     */
    public GetEmployeesDescriptionByKeywordRequest createGetEmployeesDescriptionByKeywordRequest() {
        return new GetEmployeesDescriptionByKeywordRequest();
    }

    /**
     * Create an instance of {@link GetEmployeesByEmployeeDetailsResponse }
     * 
     */
    public GetEmployeesByEmployeeDetailsResponse createGetEmployeesByEmployeeDetailsResponse() {
        return new GetEmployeesByEmployeeDetailsResponse();
    }

    /**
     * Create an instance of {@link EMPByDepartment }
     * 
     */
    public EMPByDepartment createGetEmployeesByDepartmentRequestEMPByDepartment() {
        return new EMPByDepartment();
    }

    /**
     * Create an instance of {@link EMPByLocation }
     * 
     */
    public EMPByLocation createGetEmployeesByLocationRequestEMPByLocation() {
        return new EMPByLocation();
    }

    /**
     * Create an instance of {@link GetEmployeesByNameLocationResponse }
     * 
     */
    public GetEmployeesByNameLocationResponse createGetEmployeesByNameLocationResponse() {
        return new GetEmployeesByNameLocationResponse();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees.Employee.ASSISTANT }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees.Employee.ASSISTANT createGetEmployeesByNameResponseEmployeesEmployeeASSISTANT() {
        return new com.coach.employees.service.beans.GetEmployeesByNameResponse.Employees.Employee.ASSISTANT();
    }

    /**
     * Create an instance of {@link EMPByEmail }
     * 
     */
    public EMPByEmail createGetEmployeesByEmailRequestEMPByEmail() {
        return new EMPByEmail();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees.Employee.ASSISTANT }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees.Employee.ASSISTANT createGetEmployeesByEmployeeDetailsResponseEmployeesEmployeeASSISTANT() {
        return new com.coach.employees.service.beans.GetEmployeesByEmployeeDetailsResponse.Employees.Employee.ASSISTANT();
    }

    /**
     * Create an instance of {@link EMPExist }
     * 
     */
    public EMPExist createIsEmployeeExistRequestEMPExist() {
        return new EMPExist();
    }

    /**
     * Create an instance of {@link com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee.ROLES }
     * 
     */
    public com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee.ROLES createGetEmployeesByIDResponseEmployeesEmployeeROLES() {
        return new com.coach.employees.service.beans.GetEmployeesByIDResponse.Employees.Employee.ROLES();
    }

    /**
     * Create an instance of {@link IsEmployeeEnabledRequest }
     * 
     */
    public IsEmployeeEnabledRequest createIsEmployeeEnabledRequest() {
        return new IsEmployeeEnabledRequest();
    }

    /**
     * Create an instance of {@link GetEmployeesByKeywordFault }
     * 
     */
    public GetEmployeesByKeywordFault createGetEmployeesByKeywordFault() {
        return new GetEmployeesByKeywordFault();
    }

}
