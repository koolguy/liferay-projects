
package com.coach.employees.service.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;



/**
 * Class is used to GetEmployeesByEmployeeDetailsRequest. 
 * @author GalaxE.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "empByDetails"
})
@XmlRootElement(name = "getEmployeesByEmployeeDetailsRequest")
public class GetEmployeesByEmployeeDetailsRequest {

    @XmlElement(name = "EMP_ByDetails", required = true)
    protected EMPByDetails empByDetails;

    /**
     * Gets the value of the empByDetails property.
     * 
     * @return
     *     possible object is
     *     {@link EMPByDetails }
     *     
     */
    public EMPByDetails getEMPByDetails() {
        return empByDetails;
    }

    /**
     * Sets the value of the empByDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link EMPByDetails }
     *     
     */
    public void setEMPByDetails(EMPByDetails value) {
        this.empByDetails = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="strFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="strLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="strTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="strDeptName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Location" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="strPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="strEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "strFirstName",
        "strLastName",
        "strTitle",
        "strDeptName",
        "location",
        "strPhone",
        "strEmail"
    })
    public static class EMPByDetails {

        protected String strFirstName;
        protected String strLastName;
        protected String strTitle;
        protected String strDeptName;
        @XmlElement(name = "Location")
        protected String location;
        protected String strPhone;
        protected String strEmail;

        /**
         * Gets the value of the strFirstName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStrFirstName() {
            return strFirstName;
        }

        /**
         * Sets the value of the strFirstName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStrFirstName(String value) {
            this.strFirstName = value;
        }

        /**
         * Gets the value of the strLastName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStrLastName() {
            return strLastName;
        }

        /**
         * Sets the value of the strLastName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStrLastName(String value) {
            this.strLastName = value;
        }

        /**
         * Gets the value of the strTitle property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStrTitle() {
            return strTitle;
        }

        /**
         * Sets the value of the strTitle property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStrTitle(String value) {
            this.strTitle = value;
        }

        /**
         * Gets the value of the strDeptName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStrDeptName() {
            return strDeptName;
        }

        /**
         * Sets the value of the strDeptName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStrDeptName(String value) {
            this.strDeptName = value;
        }

        /**
         * Gets the value of the location property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLocation() {
            return location;
        }

        /**
         * Sets the value of the location property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLocation(String value) {
            this.location = value;
        }

        /**
         * Gets the value of the strPhone property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStrPhone() {
            return strPhone;
        }

        /**
         * Sets the value of the strPhone property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStrPhone(String value) {
            this.strPhone = value;
        }

        /**
         * Gets the value of the strEmail property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStrEmail() {
            return strEmail;
        }

        /**
         * Sets the value of the strEmail property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStrEmail(String value) {
            this.strEmail = value;
        }

    }

}
