
package com.coach.employees.service.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Class is used to GetEmployeesByNameLocationRequest. 
 * @author GalaxE.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "empByNameLocation"
})
@XmlRootElement(name = "getEmployeesByNameLocationRequest")
public class GetEmployeesByNameLocationRequest {

    @XmlElement(name = "EMP_ByNameLocation", required = true)
    protected EMPByNameLocation empByNameLocation;

    /**
     * Gets the value of the empByNameLocation property.
     * 
     * @return
     *     possible object is
     *     {@link EMPByNameLocation }
     *     
     */
    public EMPByNameLocation getEMPByNameLocation() {
        return empByNameLocation;
    }

    /**
     * Sets the value of the empByNameLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link EMPByNameLocation }
     *     
     */
    public void setEMPByNameLocation(EMPByNameLocation value) {
        this.empByNameLocation = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="strFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="strLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="strTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="Location" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "strFirstName",
        "strLastName",
        "strTitle",
        "location"
    })
    public static class EMPByNameLocation {

        protected String strFirstName;
        protected String strLastName;
        protected String strTitle;
        @XmlElement(name = "Location")
        protected String location;

        /**
         * Gets the value of the strFirstName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStrFirstName() {
            return strFirstName;
        }

        /**
         * Sets the value of the strFirstName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStrFirstName(String value) {
            this.strFirstName = value;
        }

        /**
         * Gets the value of the strLastName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStrLastName() {
            return strLastName;
        }

        /**
         * Sets the value of the strLastName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStrLastName(String value) {
            this.strLastName = value;
        }

        /**
         * Gets the value of the strTitle property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStrTitle() {
            return strTitle;
        }

        /**
         * Sets the value of the strTitle property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStrTitle(String value) {
            this.strTitle = value;
        }

        /**
         * Gets the value of the location property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLocation() {
            return location;
        }

        /**
         * Sets the value of the location property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLocation(String value) {
            this.location = value;
        }

    }

}
