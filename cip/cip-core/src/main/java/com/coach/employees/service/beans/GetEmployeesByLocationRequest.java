
package com.coach.employees.service.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Class is used to GetEmployeesByLocationRequest. 
 * @author GalaxE.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "empByLocation"
})
@XmlRootElement(name = "getEmployeesByLocationRequest")
public class GetEmployeesByLocationRequest {

    @XmlElement(name = "EMP_ByLocation", required = true)
    protected EMPByLocation empByLocation;

    /**
     * Gets the value of the empByLocation property.
     * 
     * @return
     *     possible object is
     *     {@link EMPByLocation }
     *     
     */
    public EMPByLocation getEMPByLocation() {
        return empByLocation;
    }

    /**
     * Sets the value of the empByLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link EMPByLocation }
     *     
     */
    public void setEMPByLocation(EMPByLocation value) {
        this.empByLocation = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="Location">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="GEOGRAPHY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="OFFICE_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="BUILDING_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "location"
    })
    public static class EMPByLocation {

        @XmlElement(name = "Location", required = true)
        protected Location location;

        /**
         * Gets the value of the location property.
         * 
         * @return
         *     possible object is
         *     {@link Location }
         *     
         */
        public Location getLocation() {
            return location;
        }

        /**
         * Sets the value of the location property.
         * 
         * @param value
         *     allowed object is
         *     {@link Location }
         *     
         */
        public void setLocation(Location value) {
            this.location = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="GEOGRAPHY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="COUNTRY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="STATE" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="CITY" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="OFFICE_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="BUILDING_NAME" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "geography",
            "country",
            "state",
            "city",
            "officename",
            "buildingname"
        })
        public static class Location {

            @XmlElement(name = "GEOGRAPHY")
            protected String geography;
            @XmlElement(name = "COUNTRY")
            protected String country;
            @XmlElement(name = "STATE")
            protected String state;
            @XmlElement(name = "CITY")
            protected String city;
            @XmlElement(name = "OFFICE_NAME")
            protected String officename;
            @XmlElement(name = "BUILDING_NAME")
            protected String buildingname;

            /**
             * Gets the value of the geography property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getGEOGRAPHY() {
                return geography;
            }

            /**
             * Sets the value of the geography property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setGEOGRAPHY(String value) {
                this.geography = value;
            }

            /**
             * Gets the value of the country property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCOUNTRY() {
                return country;
            }

            /**
             * Sets the value of the country property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCOUNTRY(String value) {
                this.country = value;
            }

            /**
             * Gets the value of the state property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSTATE() {
                return state;
            }

            /**
             * Sets the value of the state property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSTATE(String value) {
                this.state = value;
            }

            /**
             * Gets the value of the city property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCITY() {
                return city;
            }

            /**
             * Sets the value of the city property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCITY(String value) {
                this.city = value;
            }

            /**
             * Gets the value of the officename property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getOFFICENAME() {
                return officename;
            }

            /**
             * Sets the value of the officename property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setOFFICENAME(String value) {
                this.officename = value;
            }

            /**
             * Gets the value of the buildingname property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBUILDINGNAME() {
                return buildingname;
            }

            /**
             * Sets the value of the buildingname property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBUILDINGNAME(String value) {
                this.buildingname = value;
            }

        }

    }

}
