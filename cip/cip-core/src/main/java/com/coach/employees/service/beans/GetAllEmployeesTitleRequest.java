package com.coach.employees.service.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Class is used to GetAllEmployeesTitleRequest. 
 * @author GalaxE.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "empTitle"
})
@XmlRootElement(name = "getAllEmployeesTitleRequest")
public class GetAllEmployeesTitleRequest {

    @XmlElement(name = "EMP_Title", required = true)
    protected EMPTitle empTitle;

    /**
     * Gets the value of the empTitle property.
     * 
     * @return
     *     possible object is
     *     {@link EMPTitle }
     *     
     */
    public EMPTitle getEMPTitle() {
        return empTitle;
    }

    /**
     * Sets the value of the empTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link EMPTitle }
     *     
     */
    public void setEMPTitle(EMPTitle value) {
        this.empTitle = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class EMPTitle {


    }

}
