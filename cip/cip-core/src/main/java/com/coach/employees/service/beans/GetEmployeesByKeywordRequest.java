
package com.coach.employees.service.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Class is used to GetEmployeesByKeywordRequest. 
 * @author GalaxE.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "empByKeyword"
})
@XmlRootElement(name = "getEmployeesByKeywordRequest")
public class GetEmployeesByKeywordRequest {

    @XmlElement(name = "EMP_ByKeyword", required = true)
    protected EMPByKeyword empByKeyword;

    /**
     * Gets the value of the empByKeyword property.
     * 
     * @return
     *     possible object is
     *     {@link EMPByKeyword }
     *     
     */
    public EMPByKeyword getEMPByKeyword() {
        return empByKeyword;
    }

    /**
     * Sets the value of the empByKeyword property.
     * 
     * @param value
     *     allowed object is
     *     {@link EMPByKeyword }
     *     
     */
    public void setEMPByKeyword(EMPByKeyword value) {
        this.empByKeyword = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="strKeyword" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "strKeyword"
    })
    public static class EMPByKeyword {

        @XmlElement(required = true)
        protected String strKeyword;

        /**
         * Gets the value of the strKeyword property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStrKeyword() {
            return strKeyword;
        }

        /**
         * Sets the value of the strKeyword property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStrKeyword(String value) {
            this.strKeyword = value;
        }

    }

}
