package com.coach.employees.service.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Class is used to GetEmployeesByNameRequest. 
 * @author GalaxE.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "empByName"
})
@XmlRootElement(name = "getEmployeesByNameRequest")
public class GetEmployeesByNameRequest {

    @XmlElement(name = "EMP_ByName", required = true)
    protected EMPByName empByName;

    /**
     * Gets the value of the empByName property.
     * 
     * @return
     *     possible object is
     *     {@link EMPByName }
     *     
     */
    public EMPByName getEMPByName() {
        return empByName;
    }

    /**
     * Sets the value of the empByName property.
     * 
     * @param value
     *     allowed object is
     *     {@link EMPByName }
     *     
     */
    public void setEMPByName(EMPByName value) {
        this.empByName = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="strFirstName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="strLastName" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "strFirstName",
        "strLastName"
    })
    public static class EMPByName {

        @XmlElement(required = true)
        protected String strFirstName;
        @XmlElement(required = true)
        protected String strLastName;

        /**
         * Gets the value of the strFirstName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStrFirstName() {
            return strFirstName;
        }

        /**
         * Sets the value of the strFirstName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStrFirstName(String value) {
            this.strFirstName = value;
        }

        /**
         * Gets the value of the strLastName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStrLastName() {
            return strLastName;
        }

        /**
         * Sets the value of the strLastName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStrLastName(String value) {
            this.strLastName = value;
        }

    }

}
