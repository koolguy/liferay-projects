
package com.coach.employees.service.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Class is used to GetEmployeesByEmailRequest. 
 * @author GalaxE.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "empByEmail"
})
@XmlRootElement(name = "getEmployeesByEmailRequest")
public class GetEmployeesByEmailRequest {

    @XmlElement(name = "EMP_ByEmail", required = true)
    protected EMPByEmail empByEmail;

    /**
     * Gets the value of the empByEmail property.
     * 
     * @return
     *     possible object is
     *     {@link EMPByEmail }
     *     
     */
    public EMPByEmail getEMPByEmail() {
        return empByEmail;
    }

    /**
     * Sets the value of the empByEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link EMPByEmail }
     *     
     */
    public void setEMPByEmail(EMPByEmail value) {
        this.empByEmail = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="strEmail" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "strEmail"
    })
    public static class EMPByEmail {

        @XmlElement(required = true)
        protected String strEmail;

        /**
         * Gets the value of the strEmail property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStrEmail() {
            return strEmail;
        }

        /**
         * Sets the value of the strEmail property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStrEmail(String value) {
            this.strEmail = value;
        }

    }

}
