
package com.coach.employees.service.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Class is used to IsUSEmployeeEnabledRequest. 
 * @author GalaxE.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "empusEnabled"
})
@XmlRootElement(name = "IsUSEmployeeEnabledRequest")
public class IsUSEmployeeEnabledRequest {

    @XmlElement(name = "EMP_USEnabled", required = true)
    protected EMPUSEnabled empusEnabled;

    /**
     * Gets the value of the empusEnabled property.
     * 
     * @return
     *     possible object is
     *     {@link EMPUSEnabled }
     *     
     */
    public EMPUSEnabled getEMPUSEnabled() {
        return empusEnabled;
    }

    /**
     * Sets the value of the empusEnabled property.
     * 
     * @param value
     *     allowed object is
     *     {@link EMPUSEnabled }
     *     
     */
    public void setEMPUSEnabled(EMPUSEnabled value) {
        this.empusEnabled = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="strADUserID" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "strADUserID"
    })
    public static class EMPUSEnabled {

        @XmlElement(required = true)
        protected String strADUserID;

        /**
         * Gets the value of the strADUserID property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStrADUserID() {
            return strADUserID;
        }

        /**
         * Sets the value of the strADUserID property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStrADUserID(String value) {
            this.strADUserID = value;
        }

    }

}
