
package com.coach.employees.service.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Class is used to GetEmployeesByPhoneRequest. 
 * @author GalaxE.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "empByPhone"
})
@XmlRootElement(name = "getEmployeesByPhoneRequest")
public class GetEmployeesByPhoneRequest {

    @XmlElement(name = "EMP_ByPhone", required = true)
    protected EMPByPhone empByPhone;

    /**
     * Gets the value of the empByPhone property.
     * 
     * @return
     *     possible object is
     *     {@link EMPByPhone }
     *     
     */
    public EMPByPhone getEMPByPhone() {
        return empByPhone;
    }

    /**
     * Sets the value of the empByPhone property.
     * 
     * @param value
     *     allowed object is
     *     {@link EMPByPhone }
     *     
     */
    public void setEMPByPhone(EMPByPhone value) {
        this.empByPhone = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="strPhone" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "strPhone"
    })
    public static class EMPByPhone {

        @XmlElement(required = true)
        protected String strPhone;

        /**
         * Gets the value of the strPhone property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStrPhone() {
            return strPhone;
        }

        /**
         * Sets the value of the strPhone property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStrPhone(String value) {
            this.strPhone = value;
        }

    }

}
