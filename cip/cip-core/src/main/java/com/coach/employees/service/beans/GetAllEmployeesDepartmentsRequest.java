
package com.coach.employees.service.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 *  Class is used to GetAllEmployeesDepartmentsRequest. 
 * @author GalaxE.
 *
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "empDepartment"
})
@XmlRootElement(name = "getAllEmployeesDepartmentsRequest")
public class GetAllEmployeesDepartmentsRequest {

    @XmlElement(name = "EMP_Department", required = true)
    protected EMPDepartment empDepartment;

    /**
     * Gets the value of the empDepartment property.
     * 
     * @return
     *     possible object is
     *     {@link EMPDepartment }
     *     
     */
    public EMPDepartment getEMPDepartment() {
        return empDepartment;
    }

    /**
     * Sets the value of the empDepartment property.
     * 
     * @param value
     *     allowed object is
     *     {@link EMPDepartment }
     *     
     */
    public void setEMPDepartment(EMPDepartment value) {
        this.empDepartment = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class EMPDepartment {


    }

}
