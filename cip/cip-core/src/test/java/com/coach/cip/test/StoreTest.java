package com.coach.cip.test;

import java.util.Set;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.coach.cip.model.entity.CIEmployee;
import com.coach.cip.model.entity.User;
import com.coach.cip.services.UserService;

public class StoreTest {

	ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
			"ApplicationContext-service.xml");

	@SuppressWarnings("unused")
	@Test
	public void testStores() {
		UserService storeService = (UserService) context.getBean("userService");
		User user = storeService.findUserByUserId(10158l);
		Set<CIEmployee> employee = user.getCIEmployeesForUserId();
	}
}
