
package com.coach.cip.test;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import com.coach.cip.model.entity.Layout;
import com.coach.cip.services.QuickLinksService;


/*
 * @author GalaxE.
 */

public class QuickLinksTest {
	
	ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("ApplicationContext-service.xml");

	@SuppressWarnings("unused")
	@Test
	public void testQuickLinks(){		
		
		QuickLinksService quickLinksService = (QuickLinksService)context.getBean("quickLinksService");
		Layout layout = quickLinksService.getLayout(10371l);
		
	}
}
